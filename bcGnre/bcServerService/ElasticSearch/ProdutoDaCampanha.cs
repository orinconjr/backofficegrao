﻿using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bcServerService.ElasticSearch
{
    public class ProdutoDaCampanha
    {
        #region Properties
        public int IdProduto { get; set; }
        public decimal? PrecoNaCampanha { get; set; }
        #endregion
    }
}
