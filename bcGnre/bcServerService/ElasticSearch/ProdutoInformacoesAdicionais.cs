﻿using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bcServerService.ElasticSearch
{
    public class ProdutoInformacoesAdicionais
    {
        public int informacaoAdicionalId { get; set; }
        [String(Index = FieldIndexOption.No)]
        public string nome { get; set; }
        [String(Index = FieldIndexOption.No)]
        public string conteudo { get; set; }
    }
}
