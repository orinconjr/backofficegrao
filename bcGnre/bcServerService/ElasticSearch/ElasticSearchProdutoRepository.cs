﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nest;
using System.Transactions;
using Sensible.PredictionIO.NET.Domain;
using SDK.PredictionIO.NET;

namespace bcServerService.ElasticSearch
{
    public class CategoriasFormatadas
    {
        
    }
    public sealed class ElasticSearchProdutoRepository
    {
        private static readonly ElasticSearchProdutoRepository instance = new ElasticSearchProdutoRepository();

        ElasticClient client;
        string connectionString = ConfigurationManager.AppSettings["elasticSearchUri"];

        private ElasticSearchProdutoRepository()
        {
            var node = new Uri(connectionString);
            var settings = new ConnectionSettings(node).RequestTimeout(new TimeSpan(0, 1, 0));
            settings.BasicAuthentication(ConfigurationManager.AppSettings["elasticSearchUser"], ConfigurationManager.AppSettings["elasticSearchPass"]);
            client = new ElasticClient(settings);
        }

        static ElasticSearchProdutoRepository()
        {
        }

        public static ElasticSearchProdutoRepository Instance
        {
            get
            {
                return instance;
            }
        }
        public void AtualizarProduto(int produtoId)
        {
            AtualizarProduto(produtoId, new List<tbProdutos>(), new List<tbProdutoFornecedor>(), new List<tbJuncaoProdutoCategoria>(), new List<tbProdutoCategoria>(), new List<tbJuncaoProdutoColecao>(), new List<tbColecao>(), new List<tbProdutoFoto>(), new List<tbProdutoVideo>(), new List<tbInformacaoAdcional>());
        }

        public void AtualizarProduto(int produtoId, List<tbProdutos> listaProdutos, List<tbProdutoFornecedor> listaFornecedores, List<tbJuncaoProdutoCategoria> juncaoProdutoCategorias, List<tbProdutoCategoria> produtoCategorias, List<tbJuncaoProdutoColecao> listaJuncaoColecoes, List<tbColecao> listaColecoes, List<tbProdutoFoto> listaFotos, List<tbProdutoVideo> listaVideos, List<tbInformacaoAdcional> listaInformacaoAdicional)
        {
            //if (rnConfiguracoes.AtualizarElasticSearch == false) return;
            //AtualizarProdutoV2(produtoId, listaProdutos, listaFornecedores, juncaoProdutoCategorias, produtoCategorias, listaJuncaoColecoes, listaColecoes, listaFotos, listaVideos, listaInformacaoAdicional);
            //return;
            //ExcluirProduto2(produtoId);


            try
            {

                var eventClient = new SDK.PredictionIO.NET.Clients.EventClient(ConfigurationManager.AppSettings["bcwanalytics_apiUrl"], int.Parse(ConfigurationManager.AppSettings["bcwanalytics_appId"]), ConfigurationManager.AppSettings["bcwanalytics_appKey"]);

                using (var data = new dbSiteEntities())
                {
                    var produto = new tbProdutos();
                    if (listaProdutos.Count > 0)
                    {
                        produto = (from c in listaProdutos select c).FirstOrDefault(x => x.produtoId == produtoId);
                    }
                    else
                    {
                        produto = (from c in data.tbProdutos select c).FirstOrDefault(x => x.produtoId == produtoId);
                    }


                    var produtoAdicionar = new Produto();
                    if (produto != null)
                    {
                        try
                        {

                            Item produtoPio = new Item(produto.produtoId.ToString(), DateTime.UtcNow, DateTime.UtcNow.AddYears(1));


                            bool ativo = ((produto.produtoAtivo ?? "").ToLower() == "true"
                                ? true
                                : false);
                            if (ativo == false)
                            {
                                ExcluirProduto(produtoId);
                                return;
                            }
                            List<string> termosFiltro = new List<string>();
                            var precoExibicao = produto.produtoPrecoPromocional > 0 &&
                                                produto.produtoPrecoPromocional < produto.produtoPreco
                                ? (decimal) produto.produtoPrecoPromocional
                                : produto.produtoPreco;
                            var parcelamento = calculaParcelamento((decimal) precoExibicao);
                            var desconto = CalculaDesconto((decimal) produto.produtoPreco,
                                (decimal) produto.produtoPrecoPromocional);
                            var descontoAVista = CalculaDescontoAVista((decimal) produto.produtoPreco,
                                (decimal) produto.produtoPrecoPromocional);
                            bool alterado = false;
                            if (produto.parcelamentoMaximo != parcelamento.parcelas)
                            {
                                produto.parcelamentoMaximo = parcelamento.parcelas;
                                alterado = true;
                            }
                            if (produto.parcelamentoMaximoValorParcela.ToString("C") !=
                                parcelamento.valor.ToString("C"))
                            {
                                alterado = true;
                                produto.parcelamentoMaximoValorParcela = parcelamento.valor;
                            }
                            if (alterado) data.SaveChanges();
                            var categorias = new List<tbProdutoCategoria>();
                            if (produtoCategorias.Count == 0)
                            {
                                categorias = (from c in data.tbJuncaoProdutoCategoria
                                    where c.produtoId == produto.produtoId
                                    select c.tbProdutoCategoria).ToList();
                            }
                            else
                            {
                                categorias = (from c in juncaoProdutoCategorias
                                    join d in produtoCategorias on c.categoriaId equals d.categoriaId
                                    where c.produtoId == produto.produtoId
                                    select d).ToList();
                            }
                            var categoriaRaiz =
                                categorias.FirstOrDefault(x => x.categoriaPaiId == 0 && x.exibirSite == true);
                            if (categoriaRaiz == null)
                                categoriaRaiz = categorias.FirstOrDefault(x => x.categoriaPaiId == 0);
                            if (categoriaRaiz != null)
                            {
                                produtoAdicionar.id = produto.produtoId;
                                produtoAdicionar.categoria = categoriaRaiz.categoriaNome;
                                produtoAdicionar.categoriaurl = categoriaRaiz.categoriaUrl;

                                produtoPio.addPropertie(Constants.Property.Category, new List<string>() { categoriaRaiz.categoriaUrl });

                                var colecoes = new List<tbColecao>();
                                if (listaJuncaoColecoes.Count == 0)
                                {
                                    colecoes = (from c in data.tbJuncaoProdutoColecao
                                        where c.produtoId == produto.produtoId
                                        select c.tbColecao).ToList();
                                }
                                else
                                {
                                    colecoes = (from c in listaJuncaoColecoes
                                        join d in listaColecoes on c.colecaoId equals d.colecaoId
                                        where c.produtoId == produto.produtoId
                                        select d).ToList();
                                }

                                produtoAdicionar.colecaoids = colecoes.Select(x => x.colecaoId).ToList();
                                produtoAdicionar.colecaonomes = colecoes.Select(x => x.colecaoNome).ToList();

                                produtoAdicionar.filtroids = categorias.Select(x => x.categoriaId).ToList();
                                produtoAdicionar.filtronomes = categorias.Select(x => x.categoriaNome).ToList();

                                produtoAdicionar.tags = "";
                                var tagsTermos = (produto.produtoTags ?? "").Replace(",", " ").Split(' ').ToList();
                                foreach (var tagsTermo in tagsTermos)
                                {
                                    if (!produto.produtoNome.Contains(tagsTermo)
                                        && !produtoAdicionar.tags.Contains(tagsTermo)
                                        && !categoriaRaiz.categoriaNome.Contains(tagsTermo))
                                    {
                                        produtoAdicionar.tags += " " + tagsTermo;
                                    }
                                }

                                foreach (var filtroNome in categorias.Where(x => x.utilizarBusca == true).Select(x => x.categoriaNome).ToList())
                                {
                                    var filtroTermos = filtroNome.Split(' ').ToList();
                                    foreach (var filtroTermo in filtroTermos)
                                    {
                                        if (!produto.produtoNome.Contains(filtroTermo)
                                            && !produtoAdicionar.tags.Contains(filtroTermo)
                                            && !categoriaRaiz.categoriaNome.Contains(filtroTermo))
                                        {
                                            produtoAdicionar.tags += " " + filtroTermo;
                                        }
                                    }
                                }

                                var filtroMenino = categorias.Any(x => x.categoriaId == 692);
                                var filtroMenina = categorias.Any(x => x.categoriaId == 693);

                                var gender = new List<string>();
                                if (filtroMenino) gender.Add("masculino");
                                if (filtroMenina) gender.Add("feminino");
                                if(gender.Any()) produtoPio.addPropertie(Constants.Property.Gender, gender);


                                var colors = new List<string>();
                                var filtroCores = (categorias.Where(x => x.categoriaPaiId == 723)).ToList();
                                foreach(var filtroCor in filtroCores)
                                {
                                    colors.Add(filtroCor.categoriaNome);
                                }

                                if (colors.Any())
                                {
                                    produtoPio.addPropertie(Constants.Property.Color, colors);
                                }


                                produtoAdicionar.tags += " " + produto.produtoId;

                                produtoAdicionar.filtronomes = termosFiltro.Distinct().ToList();
                                produtoAdicionar.fotodestaque = produto.fotoDestaque == null
                                    ? ""
                                    : produto.fotoDestaque;

                                produtoAdicionar.preco = Convert.ToDouble(precoExibicao);

                                int brindes = 0;
                                int.TryParse(produto.produtoBrindes, out brindes);
                                produtoAdicionar.brindes = brindes;
                                produtoAdicionar.exclusivo = ((produto.produtoExclusivo ?? "").ToLower() == "true"
                                    ? true
                                    : false);
                                produtoAdicionar.lancamento = ((produto.produtoLancamento ?? "").ToLower() == "true"
                                    ? true
                                    : false);
                                produtoAdicionar.nome = produto.produtoNome;
                                produtoAdicionar.nomeraw = rnFuncoes.removeAcentos(produto.produtoNome).ToLower();
                                if (!string.IsNullOrEmpty(produto.produtoNomeTitle)) produtoAdicionar.nometitle = produto.produtoNomeTitle;


                                int pecas = 0;
                                int.TryParse(produto.produtoPecas, out pecas);
                                produtoAdicionar.pecas = pecas;
                                produtoAdicionar.produtopreco = Convert.ToDouble(produto.produtoPreco);
                                produtoAdicionar.precopromocional = Convert.ToDouble(produto.produtoPrecoPromocional);
                                produtoAdicionar.precoDeCusto = Convert.ToDouble(produto.produtoPrecoDeCusto);
                                produtoAdicionar.promocao = ((produto.produtoPromocao ?? "").ToLower() == "true"
                                    ? true
                                    : false);
                                produtoAdicionar.produtourl = produto.produtoUrl.Replace("\\", "-");
                                produtoAdicionar.relevancia = produto.relevancia ?? 0;

                                produtoAdicionar.categoriatags = (categoriaRaiz.categoriaTags ?? "").Replace(",",
                                    " ");
                                produtoAdicionar.ativo = ((produto.produtoAtivo ?? "").ToLower() == "true"
                                    ? true
                                    : false);
                                produtoAdicionar.parcelamentomaximo = produto.parcelamentoMaximo;
                                produtoAdicionar.parcelamentomaximovalorparcela =
                                    Convert.ToDouble(produto.parcelamentoMaximoValorParcela);
                                var fornecedor = new tbProdutoFornecedor();
                                if (listaFornecedores.Count > 0)
                                {
                                    fornecedor =
                                        (from c in listaFornecedores
                                            where c.fornecedorId == produto.produtoFornecedor
                                            select c).First();
                                }
                                else
                                {
                                    fornecedor =
                                        (from c in data.tbProdutoFornecedor
                                            where c.fornecedorId == produto.produtoFornecedor
                                            select c).First();
                                }
                                produtoAdicionar.relevanciafornecedor = fornecedor.relevanciaProdutos;
                                produtoAdicionar.relevanciamanual = produto.relevanciaManual;

                                bool priorizarComPai = false;
                                if(produto.produtoPaiId > 0)
                                {
                                    var produtopai = (from c in data.tbProdutos where c.produtoId == produto.produtoPaiId select c.ocultarLista).FirstOrDefault();
                                    if(produtopai != null)
                                    {
                                        if(produtopai == true)
                                        {
                                            priorizarComPai = true;
                                        }
                                    }
                                }
                                if(produto.produtoPaiId == produto.produtoId)
                                {
                                    priorizarComPai = true;
                                }


                                if (produto.produtoPaiId == 0 | priorizarComPai)
                                {
                                    var dataAntes = DateTime.Now.AddDays(-20);
                                    if (produto.dataAtivacao > dataAntes && produto.produtoMarca == 152)
                                    {
                                        produtoAdicionar.relevanciamanual = Convert.ToInt32((produto.dataAtivacao.Value.Date - dataAntes.Date).TotalDays) + produto.relevanciaManual;
                                    }
                                }

                                if (produto.produtoMarca == 152 | produto.criacaoPropria == true)
                                {
                                    produtoAdicionar.relevanciamanual += 1;
                                    produtoAdicionar.relevanciafornecedor = 0;
                                    if (produtoAdicionar.nome.ToLower().Contains("kit b") | produtoAdicionar.nome.ToLower().Contains("quarto"))
                                    {
                                        produtoAdicionar.relevanciamanual += 10;
                                    }
                                }

                                

                                produtoAdicionar.prazopedidos = produto.prazoDeEntrega;
                                produtoAdicionar.desconto = Convert.ToDouble(desconto);
                                produtoAdicionar.descontototalavista = Convert.ToDouble(descontoAVista);
                                produtoAdicionar.estoquevirtual = produto.produtoEstoqueAtual > 0 ? 100 : 0;

                                produtoAdicionar.estoquereal = produto.estoqueReal ?? 0;

                                produtoAdicionar.estoqueminimo = (produto.produtoEstoqueMinimo ?? 0);

                                var listaFotosProduto = new List<tbProdutoFoto>();
                                if (listaFotos.Count > 0)
                                {
                                    listaFotosProduto = (from c in listaFotos
                                        where c.produtoId == produto.produtoId
                                        select c).ToList();
                                }
                                else
                                {
                                    listaFotosProduto = (from c in data.tbProdutoFoto
                                        where c.produtoId == produto.produtoId
                                        select c).ToList();
                                }
                                var fotos = (from c in listaFotosProduto
                                    where c.produtoId == produto.produtoId && c.produtoFoto != "foto360"
                                             orderby (c.ordenacao == null ? 99 : c.ordenacao)
                                             select new ProdutoFoto
                                    {
                                        descricao = c.produtoFotoDescricao,
                                        destaque = ((c.produtoFotoDestaque.ToLower() == "true") | c.produtoFotoId.ToString() == produtoAdicionar.fotodestaque),
                                        foto = c.produtoFoto,
                                        fotoid = c.produtoFotoId
                                    }).ToList();
                                produtoAdicionar.fotos = fotos;

                                var fotos360 = (from c in listaFotosProduto
                                             where c.produtoId == produto.produtoId && c.produtoFoto == "foto360" orderby (c.ordenacao == null ? 99 : c.ordenacao)
                                             select new ProdutoFoto
                                             {
                                                 descricao = c.produtoFotoDescricao,
                                                 destaque = ((c.produtoFotoDestaque.ToLower() == "true") | c.produtoFotoId.ToString() == produtoAdicionar.fotodestaque),
                                                 foto = c.produtoFoto,
                                                 fotoid = c.produtoFotoId
                                             }).FirstOrDefault();
                                if (fotos360 != null) produtoAdicionar.foto360 = "foto360";
                                if(!string.IsNullOrEmpty(produto.foto360)) produtoAdicionar.foto360 = produto.foto360;
                                if (colecoes.Any(x => !string.IsNullOrEmpty(x.imagem360)))
                                {
                                    produtoAdicionar.foto360 = colecoes.First(x => !string.IsNullOrEmpty(x.imagem360)).imagem360;
                                }

                                produtoAdicionar.produtoDescricao = produto.produtoDescricao;
                                produtoAdicionar.freteGratis = produto.produtoFreteGratis.ToLower() == "true";
                                produtoAdicionar.prontaEntrega = produto.prontaEntrega;
                                produtoAdicionar.foraDeLinha = produto.foraDeLinha;
                                if (fotos360 != null) produtoAdicionar.foto360Mini = "pequena_imagem360";
                                if (!string.IsNullOrEmpty(produto.foto360)) produtoAdicionar.foto360Mini = "pequena_imagem360";
                                if (colecoes.Any(x => !string.IsNullOrEmpty(x.imagem360)))
                                {
                                    produtoAdicionar.foto360Mini = "pequena_imagem360";
                                }
                                var informacoesAdicionais = new List<ProdutoInformacoesAdicionais>();
                                if (listaInformacaoAdicional.Count > 0)
                                {
                                    informacoesAdicionais = (from c in listaInformacaoAdicional
                                                                where c.produtoId == produto.produtoId
                                                                select new ProdutoInformacoesAdicionais()
                                                                {
                                                                    conteudo = c.informacaoAdcionalConteudo,
                                                                    nome = c.informacaoAdcionalNome,
                                                                    informacaoAdicionalId = c.informacaoAdcionalId
                                                                }).ToList();
                                }
                                else
                                {
                                    informacoesAdicionais = (from c in data.tbInformacaoAdcional
                                                                where c.produtoId == produto.produtoId
                                                                select new ProdutoInformacoesAdicionais()
                                                                {
                                                                    conteudo = c.informacaoAdcionalConteudo,
                                                                    nome = c.informacaoAdcionalNome,
                                                                    informacaoAdicionalId = c.informacaoAdcionalId
                                                                }).ToList();
                                }
                                informacoesAdicionais = (from c in informacoesAdicionais
                                                            select new ProdutoInformacoesAdicionais()
                                                            {
                                                                conteudo =
                                                                    c.conteudo.Replace(
                                                                        "<base /><style> body {padding:0;margin:0;}</style>\r\n", ""),
                                                                nome = c.nome,
                                                                informacaoAdicionalId = c.informacaoAdicionalId
                                                            }).ToList();
                                informacoesAdicionais = (from c in informacoesAdicionais
                                                            select new ProdutoInformacoesAdicionais()
                                                            {
                                                                conteudo =
                                                                    c.conteudo.Replace(
                                                                        "<link rel=\"stylesheet\" type=\"text/css\" href=\"css/templateDetalhes.css\">", ""),
                                                                nome = c.nome,
                                                                informacaoAdicionalId = c.informacaoAdicionalId
                                                            }).ToList();
                                informacoesAdicionais = (from c in informacoesAdicionais
                                                            select new ProdutoInformacoesAdicionais()
                                                            {
                                                                conteudo =
                                                                    c.conteudo.Replace("<link rel=\"stylesheet\" type=\"text/css\" href=\"css/templateDetalhes.css\" />", ""),
                                                                nome = c.nome,
                                                                informacaoAdicionalId = c.informacaoAdicionalId
                                                            }).ToList();

                                int tentativasInformacoes = 1;
                                while (informacoesAdicionais.Count == 0 && tentativasInformacoes <= 3)
                                {
                                    tentativasInformacoes++;
                                    informacoesAdicionais = (from c in data.tbInformacaoAdcional
                                                                where c.produtoId == produto.produtoId
                                                                select new ProdutoInformacoesAdicionais()
                                                                {
                                                                    conteudo = c.informacaoAdcionalConteudo,
                                                                    nome = c.informacaoAdcionalNome,
                                                                    informacaoAdicionalId = c.informacaoAdcionalId
                                                                }).ToList();
                                    informacoesAdicionais = (from c in informacoesAdicionais
                                                                select new ProdutoInformacoesAdicionais()
                                                                {
                                                                    conteudo =
                                                                        c.conteudo.Replace(
                                                                            "<base /><style> body {padding:0;margin:0;}</style>\r\n", ""),
                                                                    nome = c.nome,
                                                                    informacaoAdicionalId = c.informacaoAdicionalId
                                                                }).ToList();
                                    informacoesAdicionais = (from c in informacoesAdicionais
                                                                select new ProdutoInformacoesAdicionais()
                                                                {
                                                                    conteudo =
                                                                        c.conteudo.Replace(
                                                                            "<link rel=\"stylesheet\" type=\"text/css\" href=\"css/templateDetalhes.css\">", ""),
                                                                    nome = c.nome,
                                                                    informacaoAdicionalId = c.informacaoAdicionalId
                                                                }).ToList();
                                }

                                if(informacoesAdicionais.Count == 0)
                                {
                                    rnEmails.EnviaEmail("", "andre@bark.com.br", "", "", "", "", "(sem descricao) Exception atualizacao elastic " + produtoId.ToString());
                                }
                                if(!string.IsNullOrEmpty(fornecedor.textoMarcaSite))
                                {
                                    var informacaoDescricao = informacoesAdicionais.FirstOrDefault(x => x.nome.ToLower() == "descricao" | x.nome.ToLower() == "descrição" | x.nome.ToLower() == "descricão" | x.nome.ToLower() == "descriçao");
                                    if(informacaoDescricao != null)
                                    {
                                        informacaoDescricao.conteudo = informacaoDescricao.conteudo + fornecedor.textoMarcaSite;
                                    }
                                }
                                produtoAdicionar.informacoesAdicionais = informacoesAdicionais;
                                produtoAdicionar.produtoIdDaEmpresa = produto.produtoIdDaEmpresa;
                                produtoAdicionar.produtoPeso = produto.produtoPeso;

                                produtoAdicionar.ocultarLista = (produto.ocultarLista ?? false);
                                produtoAdicionar.ocultarBusca = (produto.ocultarBusca ?? false);

                                var listaVideosProduto = new List<tbProdutoVideo>();
                                if (listaVideos.Count > 0 | listaFotos.Count > 0)
                                {
                                    listaVideosProduto = (from c in listaVideos
                                        where c.produtoId == produto.produtoId
                                        select c).ToList();
                                }
                                else
                                {
                                    listaVideosProduto = (from c in data.tbProdutoVideo
                                        where c.produtoId == produto.produtoId
                                        select c).ToList();
                                }
                                var videos = (from c in listaVideosProduto
                                    where c.produtoId == produto.produtoId
                                    select new ProdutoVideo()
                                    {
                                        link = c.linkVideo,
                                        videoId = c.ID
                                    }).ToList();
                                produtoAdicionar.videos = videos;

                                //var node = new Uri(ConfigurationManager.AppSettings["elasticSearchUri"]);
                                //var settings = new ConnectionSettings(node).RequestTimeout(new TimeSpan(1, 0, 0));
                                //settings.BasicAuthentication(ConfigurationManager.AppSettings["elasticSearchUser"],
                                //    ConfigurationManager.AppSettings["elasticSearchPass"]);

                                ////gdgread:gdgread152029
                                //var client = new ElasticClient(settings);
                                //var descriptor = new CreateIndexDescriptor("produtos").Mappings(x => x.Map<Produto>(m => m.AutoMap()));
                                //var res1 = client.DeleteIndex("produtos");
                                //var res = client.CreateIndex(descriptor);
                                //var resp = client.CreateIndex("produtos");
                                produtoAdicionar.descontoProgressivoFrete = (produto.descontoProgressivoFrete.ToLower() == "true");
                                produtoAdicionar.descontoProgressivoFretePorcentagem = produto.descontoProgressivoFretePorcentagem;

                                var atributos = new List<ProdutoAtributos>();

                                var agora = DateTime.Now;
                                #region contador
                                var contador = (from c in data.tbProdutoContador where c.produtoId == produtoId && c.ativo == true && c.fimContador > agora orderby c.inicioContador select c).FirstOrDefault();
                                if(contador != null)
                                {
                                    if (contador.estilo != null)
                                    {
                                        var estilo = (from c in data.tbEstilos where c.id == (int)contador.estilo select c).FirstOrDefault();
                                        if (estilo != null)
                                        {
                                            var inicioContador = (contador.inicioContador ?? DateTime.Now);
                                            string inicioContadorString = inicioContador.ToString("yyyy-MM-dd HH:mm:sss");
                                            var fimContador = (contador.fimContador ?? DateTime.Now);
                                            string fimContadorString = fimContador.ToString("yyyy-MM-dd HH:mm:ss");
                                            atributos.Add(new ProdutoAtributos() { chave = "contadorinicio", valor = inicioContadorString });
                                            atributos.Add(new ProdutoAtributos() { chave = "contadorfim", valor = fimContadorString });
                                            atributos.Add(new ProdutoAtributos() { chave = "contadorestilo", valor = estilo.estilo });

                                            rnFuncoes.AgendarAtualizacaoProduto(produtoId, fimContador.AddSeconds(1));


                                        }
                                    }

                                }
                                #endregion



                                produtoAdicionar.Especificacoes = new List<Especificacao>();
                                var especificacoes = (from c in data.tbProdutoItemEspecificacao
                                                      join d in data.tbEspecificacao on c.especificacaoId equals d.especificacaoId
                                                      where c.produtoId == produto.produtoId select d).ToList().Distinct().ToList().OrderBy(x => (x.ordem ?? 0));
                                if (especificacoes.Any())
                                {
                                    var produtosNoGrupo = (from c in data.tbProdutoItemEspecificacao where c.produtoPaiId == produto.produtoPaiId select c).ToList();
                                    foreach (var especificacao in especificacoes)
                                    {
                                        var especificacaoProduto = new Especificacao();
                                        especificacaoProduto.exibirFoto = especificacao.exibirFoto ?? false;
                                        especificacaoProduto.Nome = especificacao.especificacaoNome;
                                        especificacaoProduto.especificacaoItens = new List<EspecificacaoItem>();

                                        //Quais as outras cores que tenho no grupo
                                        var listaVariacoes = (from c in produtosNoGrupo where c.especificacaoId == especificacao.especificacaoId && (c.produtoItemEspecificacaoId ?? 0) > 0 select c.tbEspecificacaoItens).Distinct().ToList();
                                        foreach (var variacao in listaVariacoes)
                                        {
                                            var listaProdutosVariacaoAtual = (from c in produtosNoGrupo where c.produtoItemEspecificacaoId == variacao.especificacaoItensId select c.produtoId).ToList();
                                            var listaProdutosComVariacao = (from c in produtosNoGrupo where listaProdutosVariacaoAtual.Contains(c.produtoId) select c).ToList();

                                            var outrasEspecificacoes = especificacoes.Where(x => x.especificacaoId != especificacao.especificacaoId).Distinct().ToList();
                                            foreach (var outraEspecificacao in outrasEspecificacoes)
                                            {
                                                var itemOutraEspecificacaoAtual = (from c in produtosNoGrupo where c.produtoId == produto.produtoId && c.especificacaoId == outraEspecificacao.especificacaoId select c).FirstOrDefault();
                                                if (itemOutraEspecificacaoAtual != null)
                                                {
                                                    var listaProdutosOutraVariacaoAtual = (from c in produtosNoGrupo where c.produtoItemEspecificacaoId == itemOutraEspecificacaoAtual.produtoItemEspecificacaoId select c.produtoId).ToList();
                                                    listaProdutosComVariacao = (from c in listaProdutosComVariacao where listaProdutosOutraVariacaoAtual.Contains(c.produtoId) select c).ToList();
                                                }
                                            }
                                            var produtoIdsVariacao = listaProdutosComVariacao.Select(x => x.produtoId).Distinct();
                                            if (produtoIdsVariacao.Any(x => x == produtoId)) produtoIdsVariacao = produtoIdsVariacao.Where(x => x == produtoId).Distinct();
                                            if (produtoIdsVariacao.Any())
                                            {
                                                var produtoIdVariacao = produtoIdsVariacao.First();
                                                var especificacaoItem = new EspecificacaoItem();
                                                especificacaoItem.estilo = variacao.estilo;
                                                especificacaoItem.nome = variacao.especificacaoItenNome;
                                                var produtoEspecificacao = (from c in data.tbProdutos where c.produtoId == produtoIdVariacao select new { c.produtoId, c.produtoUrl, c.fotoDestaque, c.produtoAtivo }).First();
                                                especificacaoItem.foto = produtoEspecificacao.fotoDestaque;
                                                especificacaoItem.produtoUrl = produtoEspecificacao.produtoUrl;
                                                especificacaoItem.produtoId = produtoEspecificacao.produtoId;
                                                especificacaoItem.ordem = variacao.ordem ?? 0;
                                                if(produtoEspecificacao.produtoAtivo.ToLower() == "true") especificacaoProduto.especificacaoItens.Add(especificacaoItem);
                                            }
                                            especificacaoProduto.especificacaoItens = especificacaoProduto.especificacaoItens.OrderBy(x => x.ordem).ToList();
                                        }
                                        produtoAdicionar.Especificacoes.Add(especificacaoProduto);
                                    }
                                }


                                produtoAdicionar.atributos = atributos;
                                var response2 = client.Index(produtoAdicionar, idx => idx.Index("produtosv2"));

                                var campanhasCompreJunto = (from c in data.tbCampanhaCompreJuntoProduto where c.idProduto == produtoId && c.tbCampanhaCompreJunto.ativo == true
                                                            select c.tbCampanhaCompreJunto).ToList().Distinct();
                                if (campanhasCompreJunto.Any())
                                {
                                    produtoAdicionar.compreJunto = new List<CompreJunto>();
                                }
                                foreach(var campanhaCompreJunto in campanhasCompreJunto)
                                {
                                    var compreJunto = new CompreJunto();
                                    compreJunto.ID = campanhaCompreJunto.idCampanhaCompreJunto;
                                    compreJunto.InicioDaVigencia = campanhaCompreJunto.inicioDaVigencia;
                                    compreJunto.FimDaVigencia = campanhaCompreJunto.fimDaVigencia;
                                    if (campanhaCompreJunto.usaTimer)
                                    {
                                        compreJunto.InicioDoTimer = (campanhaCompreJunto.inicioDoTimer ?? campanhaCompreJunto.inicioDaVigencia);
                                        compreJunto.FimDoTimer = (campanhaCompreJunto.fimDoTimer ?? campanhaCompreJunto.fimDaVigencia);
                                    }

                                    compreJunto.ProdutosDaCampanha = new List<ProdutoDaCampanha>();
                                    var produtosCompreJunto = (from c in data.tbCampanhaCompreJuntoProduto where c.idCampanhaCompreJunto == campanhaCompreJunto.idCampanhaCompreJunto select c).ToList();
                                    foreach(var produtoCompreJunto in produtosCompreJunto)
                                    {
                                        compreJunto.ProdutosDaCampanha.Add(new ProdutoDaCampanha()
                                        {
                                            IdProduto = produtoCompreJunto.idProduto,
                                            PrecoNaCampanha = produtoCompreJunto.precoNaCampanha
                                        });
                                    }

                                    produtoAdicionar.compreJunto.Add(compreJunto);
                                }
                                var response = client.Index(produtoAdicionar, idx => idx.Index("produtos"));
                                //rnAmp.GerarAmp(produtoAdicionar);
                                //eventClient.UpdateItem(produtoPio, Constants.SetEvent);
                            }
                            else
                            {
                                rnEmails.EnviaEmail("", "cadastro@graodegente.com.br", "andre@graodegente.com.br", "", "", produto.produtoNome + "<br>Produto ID: " + produto.produtoId, "Produto sem categoria");
                                ExcluirProduto(produtoId);
                            }

                        }
                        catch (Exception ex)
                        {
                           // rnEmails.EnviaEmail("", "andre@graodegente.com.br", "", "", "", GetLineNumber(ex) + "<br><br>" + ex.ToString() + "<br><br>" + ex.StackTrace + "<br><br>" + ex.InnerException + "<br><br>" + ex.Message, "Exception atualizacao elastic " + produtoId.ToString());
                            rnFuncoes.AgendarAtualizacaoProduto(produtoId, DateTime.Now);
                        }
                    }
                    else
                    {
                        ExcluirProduto(produtoId);
                    }
                }
                
            }
            catch (Exception ex)
            {
                rnEmails.EnviaEmail("", "andre@graodegente.com.br", "", "", "", ex.ToString() + "<br><br>" + ex.StackTrace, "Erro Elasticc");
            }

            //AtualizarProdutoV2(produtoId, listaProdutos, listaFornecedores, juncaoProdutoCategorias, produtoCategorias, listaJuncaoColecoes, listaColecoes, listaFotos, listaVideos, listaInformacaoAdicional);

        }

        public int GetLineNumber(Exception ex)
        {
            var lineNumber = 0;
            const string lineSearch = ":line ";
            var index = ex.StackTrace.LastIndexOf(lineSearch);
            if (index != -1)
            {
                var lineNumberText = ex.StackTrace.Substring(index + lineSearch.Length);
                if (int.TryParse(lineNumberText, out lineNumber))
                {
                }
            }
            return lineNumber;
        }

        public void ExcluirProduto(int produtoId)
        {
            if (rnConfiguracoes.AtualizarElasticSearch == false) return;

            //var node = new Uri(ConfigurationManager.AppSettings["elasticSearchUri"]);
            //var settings = new ConnectionSettings(node).RequestTimeout(new TimeSpan(1, 0, 0));
            //settings.BasicAuthentication(ConfigurationManager.AppSettings["elasticSearchUser"], ConfigurationManager.AppSettings["elasticSearchPass"]);
            //var client = new ElasticClient(settings);
            var response = client.Delete<Produto>(produtoId, d => d.Index("produtos"));
            var response2 = client.Delete<Produto>(produtoId, d => d.Index("produtosv2"));
        }

        public void CriarIndice()
        {
            var descriptor = new CreateIndexDescriptor("produtos").Mappings(x => x.Map<Produto>(m => m.AutoMap()));
            var descriptor2 = new CreateIndexDescriptor("produtosv2").Mappings(x => x.Map<Produto>(m => m.AutoMap()));
            //var res1 = client.DeleteIndex("produtosv2");
            var res = client.CreateIndex(descriptor);
            var res2 = client.CreateIndex(descriptor2);
        }
        public void ExcluirProduto2(int produtoId)
        {
            if (rnConfiguracoes.AtualizarElasticSearch == false) return;

            //var node = new Uri(ConfigurationManager.AppSettings["elasticSearchUri"]);
            //var settings = new ConnectionSettings(node).RequestTimeout(new TimeSpan(1, 0, 0));
            //settings.BasicAuthentication(ConfigurationManager.AppSettings["elasticSearchUser"], ConfigurationManager.AppSettings["elasticSearchPass"]);
            //var client = new ElasticClient(settings);
            //var response = client.Delete<Produto>(produtoId, d => d.Index("produtos"));
        }

        public class retornoParcelamento
        {
            public int parcelas { get; set; }
            public decimal valor { get; set; }

        }

        public static retornoParcelamento calculaParcelamento(decimal valor)
        {
            //var data = new dbSiteEntities();
            //var dvCondicoesDePagamento = (from c in data.tbCondicoesDePagamento where c.ativo.ToLower() == "true" select c).ToList();

            //string mensagemDesconto = "";

            //var condicao = dvCondicoesDePagamento.First(x => x.destaque.ToLower() == "true");
            int parcelasSemJuros = 12;
            //double taxaDeJuros = condicao.taxaDeJuros / 100;
            decimal valorMinimoDaParcela = 5;
            //decimal porcentagemDeDesconto = Convert.ToDecimal(condicao.porcentagemDeDesconto);
            int numeroMaximoDeParcelas = 12;
            decimal valorDaParcela = 0;

            decimal produtoPreco = valor;


            var retornoParcelamento = new retornoParcelamento();
            retornoParcelamento.parcelas = 0;
            retornoParcelamento.valor = 0;

            for (int i = numeroMaximoDeParcelas; i <= numeroMaximoDeParcelas && i > 0; i--)
            {
                valorDaParcela = produtoPreco / i;

                if (valorDaParcela >= valorMinimoDaParcela)
                {
                    if (i > 1)
                    {
                        if (i <= parcelasSemJuros)
                        {
                            retornoParcelamento.parcelas = i;
                            retornoParcelamento.valor = decimal.Parse(valorDaParcela.ToString());
                        }
                        break;
                    }
                }
            }

            return retornoParcelamento;
        }


        private static decimal CalculaDesconto(decimal produtoPreco, decimal produtoPrecoPromocional)
        {
            if (produtoPreco == 0) return 0;
            decimal precoAtivo = produtoPrecoPromocional == 0 ? produtoPreco : produtoPrecoPromocional;
            decimal desconto = 100 - ((precoAtivo * 100) / produtoPreco);
            return desconto;
        }
        private static decimal CalculaDescontoAVista(decimal produtoPreco, decimal produtoPrecoPromocional)
        {
            if (produtoPreco == 0) return 0;
            decimal precoAtivo = produtoPrecoPromocional == 0 ? produtoPreco : produtoPrecoPromocional;
            decimal precoComDesconto = precoAtivo - ((precoAtivo / 100) * 15);
            decimal desconto = 100 - ((precoComDesconto * 100) / produtoPreco);
            return desconto;
        }
    }
}
