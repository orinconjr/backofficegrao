﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nest;

namespace bcServerService.ElasticSearch
{
    public class ProdutoAtributos
    {
        [String(Index = FieldIndexOption.No)]
        public string chave { get; set; }
        [String(Index = FieldIndexOption.No)]
        public string valor { get; set; }
    }
}
