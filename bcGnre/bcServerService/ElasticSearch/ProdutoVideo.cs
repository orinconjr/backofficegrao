﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nest;

namespace bcServerService.ElasticSearch
{
    public class ProdutoVideo
    {
        public int videoId { get; set; }
        [String(Index = FieldIndexOption.No)]
        public string link { get; set; }
    }
}
