﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nest;

namespace bcServerService.ElasticSearch
{
    public class ProdutoFoto
    {
        public int fotoid { get; set; }
        [String(Index = FieldIndexOption.No)]
        public string foto { get; set; }
        [String(Index = FieldIndexOption.No)]
        public string descricao { get; set; }
        public bool destaque { get; set; }
    }
}
