﻿using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bcServerService.ElasticSearch
{
    [ElasticsearchType(Name = "banner", IdProperty = "id")]
    public class Banner
    {
        public int id { get; set; }
        public string Link { get; set; }
        [String(Index = FieldIndexOption.NotAnalyzed)]
        public string Foto { get; set; }
        public int Local { get; set; }
        public int Posicao { get; set; }
        public decimal Relevancia { get; set; }
        public bool Mobile { get; set; }
        public DateTime? InicioDoTimer { get; set; }
        public DateTime? FimDoTimer { get; set; }
        [String(Index = FieldIndexOption.NotAnalyzed)]
        public string EstiloDoTimer { get; set; }
        [String(Index = FieldIndexOption.NotAnalyzed)]
        public string LinkDoProduto { get; set; }
        public int? IdDoProduto { get; set; }
    }
}
