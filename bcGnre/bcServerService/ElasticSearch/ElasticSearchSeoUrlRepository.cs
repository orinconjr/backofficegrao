﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nest;
using System.Transactions;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;
using System.ComponentModel;

namespace bcServerService.ElasticSearch
{
    public sealed class ElasticSearchSeoUrlRepository
    {



        private static readonly ElasticSearchSeoUrlRepository instance = new ElasticSearchSeoUrlRepository();


        ElasticClient client;
        string connectionString = ConfigurationManager.AppSettings["elasticSearchUri"];

        private ElasticSearchSeoUrlRepository()
        {
            var node = new Uri(connectionString);
            var settings = new ConnectionSettings(node).RequestTimeout(new TimeSpan(0, 1, 0));
            settings.BasicAuthentication(ConfigurationManager.AppSettings["elasticSearchUser"], ConfigurationManager.AppSettings["elasticSearchPass"]);
            client = new ElasticClient(settings);
        }

        static ElasticSearchSeoUrlRepository()
        {
        }

        public static ElasticSearchSeoUrlRepository Instance
        {
            get
            {
                return instance;
            }
        }
        private void AtualizarSeoUrlElasticSearch(int idSeoUrl)
        {
            try
            {
                using (new TransactionScope(TransactionScopeOption.Required, new TransactionOptions
                {
                    IsolationLevel = IsolationLevel.ReadUncommitted
                }))
                {
                    using (var data = new dbSiteEntities())
                    {
                        var seoUrl = (from c in data.tbSeoUrl where c.idSeoUrl == idSeoUrl && c.redirect == true select c).FirstOrDefault();
                        if (seoUrl != null)
                        {
                            var url = new UrlDeRedirecionamento();
                            url.id = idSeoUrl;
                            url.Tipo = seoUrl.idSeoUrlTipo;
                            url.Url = seoUrl.url;
                            url.RedirectTo = seoUrl.urlRedirect;

                            var response = client.Index(url, idx => idx.Index("urlsderedirecionamento"));
                        }
                        else
                        {
                            ExcluirSeoUrl(idSeoUrl);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                rnEmails.EnviaEmail("", "andre@bark.com.br", "", "", "", ex.ToString(), "Exception atualizacao elastic seourl " + idSeoUrl.ToString());
            }
        }


        public void ExcluirSeoUrl(int idSeoUrl)
        {
            if (rnConfiguracoes.AtualizarElasticSearch == false) return;
            var response = client.Delete<UrlDeRedirecionamento>(idSeoUrl, d => d.Index("urlsderedirecionamento"));
        }   
        
        public void GerarIndice()
        {
            var descriptor = new CreateIndexDescriptor("urlsderedirecionamento").Mappings(x => x.Map<UrlDeRedirecionamento>(m => m.AutoMap()));
            //var res1 = client.DeleteIndex("seourl");
            var res = client.CreateIndex(descriptor);
        }

        public void AtualizarSeoUrlQueue(long? idQueue)
        {
            if (bwAtualizarSeoUrl.IsBusy != true)
            {
                bwAtualizarSeoUrl.DoWork += new DoWorkEventHandler(bwAtualizarSeoUrl_DoWork);
                bwAtualizarSeoUrl.RunWorkerAsync(idQueue);
            }
        }

        private BackgroundWorker bwAtualizarSeoUrl = new BackgroundWorker();
        private void bwAtualizarSeoUrl_DoWork(object sender, DoWorkEventArgs e)
        {

            #region atualizaQueue
            var data = new dbSiteEntities();
            int idQueue = Convert.ToInt32(e.Argument);
            var queue = (from c in data.tbQueue where c.idQueue == idQueue select c).First();
            queue.andamento = true;
            queue.dataInicio = DateTime.Now;
            data.SaveChanges();
            #endregion atualizaQueue

            int idSeoUrl = (int)queue.idRelacionado;


            var elastic = ElasticSearch.ElasticSearchSeoUrlRepository.Instance;
            elastic.AtualizarSeoUrlElasticSearch(idSeoUrl);


            #region finalizaQueue
            queue.concluido = true;
            queue.andamento = true;
            queue.dataConclusao = DateTime.Now;
            data.SaveChanges();
            #endregion

        }

    }
}
