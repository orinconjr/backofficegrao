//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace bcServerService
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbJuncaoProdutoColecao
    {
        public int idJuncaoProdutoColecao { get; set; }
        public int produtoId { get; set; }
        public int colecaoId { get; set; }
        public System.DateTime dataDaCriacao { get; set; }
    
        public virtual tbColecao tbColecao { get; set; }
        public virtual tbProdutos tbProdutos { get; set; }
    }
}
