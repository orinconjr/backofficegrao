﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using Newtonsoft.Json;

namespace bcServerService.pagamento
{
    class PagamentoV2
    {
        #region config

        #endregion

        #region Enums
        public enum TipoPagamento
        {
            NaoDefinido = 0,
            Boleto = 1,
            CartaoCredito = 2
        }

        public enum StatusPagtoBoleto
        {
            NaoDefinido = 0,
            NaoConfirmado = 1,
            AVencer = 2,
            Pago = 3,
            Vencido = 4,
            Cancelado = 5,
        }

        public enum StatusPagtoCartao
        {
            NaoDefinido = 0,
            Solicitado = 1,
            Autorizado = 2,
            Capturado = 3,
            Recebido = 4,
            CancelamentoSolicitado = 5,
            Cancelado = 6,
            NaoAutorizado = 7,
            EstornoSolicitado = 8, //Estorno solicitado para Maxipag//
            EstornoRealizado = 9, // Estorno realizado para o cliente
            CancelarReservaSolicitada = 10,
            CancelarReservaRealizada = 11
        }

        public enum StatusPagtoOrdem
        {
            NaoDefinido = 0,
            AguardandoConfirmacao = 1,
            EmAberto = 2,
            EmAtraso = 3,
            Finalizada = 4,
            Cancelado = 5
        }

        public enum Ativo
        {
            NaoDefinido = 0,
            Ativo = 1,
            Excluido = 2,
        }

        public enum CodAdquirente
        {
            NaoDefinido = 0,
            SimuladorTestes = 1,
            Rede = 2,
            Cielo = 4,
            TEF = 5,
            Elavon = 6,
            ChasePaymentech = 8
        }

        public enum BandeiraCartaoCredito
        {
            NaoDefinido = 0,
            Dinners = 1,
            MasterCard = 2,
            Visa = 3,
            Outro = 4,
            Amex = 5,
            HyperCard = 6,
            Aura = 7,
            Carrefour = 8,
        }

        public enum BoletoBanco
        {
            NaoDefinido = 0,
            Itau = 11,
            Bradesco = 12,
            HSBC = 14,
            Santander = 15,
            Caixa = 16
        }
        #endregion

        #region Model
        public class OrdemPagamentoModel
        {
            public string _id { get; set; }

            public int PedidoId { get; set; }

            public ClienteModel Cliente { get; set; }

            public decimal ValorTotalDosItens { get; set; }

            public decimal ValorDoFrete { get; set; }

            public decimal ValorDosJuros { get; set; }

            public decimal TaxaJuros { get; set; }

            public decimal ValorTotalDoPedido { get; set; }

            public decimal ValorDoDesconto { get; set; }

            public List<BoletoModel> Boletos { get; set; }

            public List<CartaoCreditoModel> CartoesCredito { get; set; }

            public ContratoMaxiPagoModel DadosContratoMaxiPago { get; set; }

            public List<ItensPedido> ItensDoPedido { get; set; }

            public string IpAddress { get; set; }
            public string CpfCnpjContrato { get; set; }

            public StatusPagtoOrdem Status { get; set; }

            public Ativo Ativo { get; set; }

        }

        public class ClienteModel
        {
            public string ClienteId { get; set; }
            public string NomeCompleto { get; set; }
            public string CpfCnpjCliente { get; set; }
            public Dictionary<string, string> Telefones { get; set; }
            public Dictionary<string, string> Emails { get; set; }
            public List<EnderecoModel> Enderecos { get; set; }
        }

        public class EnderecoModel
        {
            public string Logradouro { get; set; }
            public string Numero { get; set; }
            public string Complemento { get; set; }
            public string Cep { get; set; }
            public string Cidade { get; set; }
            public string Estado { get; set; }
            public string Bairro { get; set; }
            public string ReferenciaEntrega { get; set; }
            public bool IsEnderecoEntrega { get; set; }
        }


        public class BoletoModel
        {
            public string NroBoleto { get; set; }
            public string InstrucoesBoleto { get; set; }
            public decimal ValorBoleto { get; set; }
            public BoletoBanco Banco { get; set; }
            public StatusPagtoBoleto StatusPagamento { get; set; }
            public DateTime DataVencimento { get; set; }
            public DateTime? DataPagamento { get; set; }
            public MaxiPagoResponse RespostaGatewayPagtos { get; set; }

        }

        public class CartaoCreditoModel
        {
            public int IdPagamento { get; set; }
            public string NomePortadorCartao { get; set; }
            public string NroCartao { get; set; }
            public string expMonth { get; set; }
            public string expYear { get; set; }
            public string cvvNumber { get; set; }
            public int NroParcelas { get; set; }

            public decimal Valor { get; set; }

            public CodAdquirente Adquirente { get; set; }

            public BandeiraCartaoCredito BandeiraCartao { get; set; }

            public StatusPagtoCartao StatusPagamento { get; set; }

            public DateTime? DataPagamento { get; set; }

            public MaxiPagoResponse RespostaGatewayPagtos { get; set; }

            public ClearSaleModel AnaliseRisco { get; set; }

        }
        public class ContratoMaxiPagoModel
        {
            public string _id { get; set; }
            public string Descricao { get; set; }
            public string CpfCnpjDonoContrato { get; set; }
            public string merchantId { get; set; }
            public string merchantKey { get; set; }
            public string UsuarioPortal { get; set; }
            public string SenhaPortal { get; set; }
        }
        public class ClearSaleModel
        {
            public string UrlClearSale { get; set; }

            public string Score { get; set; }

            public string DecisaoTomada { get; set; }

            public string StatusDecisao { get; set; } //Resultado da análise de risco

        }
        public class MaxiPagoResponse
        {
            public string AuthorizationCode { get; set; }
            public string OrderId { get; set; }
            public string TransactionId { get; set; }
            public string TransactionTimestamp { get; set; }
            public int ResponseCode { get; set; }
            public string ResponseMessage { get; set; }
            public string AvsResponseCode { get; set; }
            public string CvvResponseCode { get; set; }
            public string ProcessorReturnCode { get; set; }
            public string ProcessorMessage { get; set; }
            public string ProcessorReferenceNumber { get; set; }
            public string ProcessorTransactionID { get; set; }
            public string ErrorMessage { get; set; }
            public string BoletoUrl { get; set; }
            public string AuthenticationUrl { get; set; }
            public string SaveOnFile { get; set; }
            public string OnlineDebitUrl { get; set; }
        }

        public class ItensPedido
        {
            public string ItemId { get; set; }
            public string Nome { get; set; }
            public int Qtde { get; set; }
            public decimal Valor { get; set; }
            public string Categoria { get; set; }
        }
        #endregion


        public static string GravarOrdemPagamento(int pedidoId)
        {

            var riscoRepo = new BarkCommerce.Core.Repositories.AnaliseRisco.ClearSaleRepository();
            var gatewayRepo = new BarkCommerce.Core.Repositories.Gateway.MaxipagoRepository();
            var pagamentoRepo = new BarkCommerce.Core.Repositories.Pagamento.PagamentoRepository(riscoRepo, gatewayRepo);
            var pagamentoBusiness = new BarkCommerce.Business.Pagamento.ProcessarPagamentosPedido(pagamentoRepo);
            var pagamentoEnviado = pagamentoBusiness.ProcessarPagamentos(new BarkCommerce.Business.Pagamento.ProcessarPagamentosPedidoRequest() { PedidoId = pedidoId });
            return "";

            try
            {
                var data = new dbSiteEntities();
                var pedido = (from c in data.tbPedidos where c.pedidoId == pedidoId select c).First();
                var clientePedido = (from c in data.tbClientes where c.clienteId == pedido.clienteId select c).First();
                var pagamento = new OrdemPagamentoModel();
                pagamento.PedidoId = pedido.pedidoId;
                pagamento.CpfCnpjContrato = rnConfiguracoes.ContratoMaxipago1;
                pagamento.IpAddress = pedido.ipDoCliente;

                #region Cliente
                var cliente = new ClienteModel();
                cliente.ClienteId = clientePedido.clienteId.ToString();
                cliente.CpfCnpjCliente = clientePedido.clienteCPFCNPJ;

                var telefones = new Dictionary<string, string>();
                if (!string.IsNullOrEmpty(clientePedido.clienteFoneResidencial)) telefones.Add("Residencial", clientePedido.clienteFoneResidencial);
                if (!string.IsNullOrEmpty(clientePedido.clienteFoneComercial)) telefones.Add("Comercial", clientePedido.clienteFoneComercial);
                if (!string.IsNullOrEmpty(clientePedido.clienteFoneCelular)) telefones.Add("Celular", clientePedido.clienteFoneCelular);
                cliente.Telefones = telefones;
                cliente.Emails = new Dictionary<string, string> { { "Principal", clientePedido.clienteEmail } };
                cliente.NomeCompleto = string.IsNullOrEmpty(clientePedido.clienteNome) ? clientePedido.clienteNomeDaEmpresa : clientePedido.clienteNome;

                var enderecoCliente = new EnderecoModel();
                enderecoCliente.Logradouro = clientePedido.clienteRua;
                enderecoCliente.Numero = clientePedido.clienteNumero;
                enderecoCliente.Complemento = clientePedido.clienteComplemento;
                enderecoCliente.Cep = clientePedido.clienteCep;
                enderecoCliente.Cidade = clientePedido.clienteCidade;
                enderecoCliente.Estado = clientePedido.clienteEstado;
                enderecoCliente.ReferenciaEntrega = clientePedido.clienteReferenciaParaEntrega;
                enderecoCliente.Bairro = clientePedido.clienteBairro;
                enderecoCliente.IsEnderecoEntrega = false;

                var enderecoEntrega = new EnderecoModel();
                enderecoEntrega.Logradouro = pedido.endRua;
                enderecoEntrega.Numero = pedido.endNumero;
                enderecoEntrega.Complemento = pedido.endComplemento;
                enderecoEntrega.Cep = pedido.endCep;
                enderecoEntrega.Cidade = pedido.endCidade;
                enderecoEntrega.Estado = pedido.endEstado;
                enderecoEntrega.ReferenciaEntrega = pedido.endReferenciaParaEntrega;
                enderecoEntrega.Bairro = pedido.endBairro;
                enderecoCliente.IsEnderecoEntrega = true;

                cliente.Enderecos = new List<EnderecoModel>();
                cliente.Enderecos.Add(enderecoCliente);
                cliente.Enderecos.Add(enderecoEntrega);
                #endregion

                pagamento.Cliente = cliente;
                pagamento.TaxaJuros = Convert.ToDecimal(pedido.porcentagemDoJuros);
                pagamento.ValorTotalDoPedido = Convert.ToDecimal(pedido.valorCobrado);
                pagamento.ValorTotalDosItens = Convert.ToDecimal(pedido.valorDosItens);
                pagamento.ValorDoFrete = Convert.ToDecimal(pedido.valorDoFrete);
                pagamento.ValorDosJuros = Convert.ToDecimal(pedido.valorDosJuros);
                pagamento.ValorDoDesconto = Convert.ToDecimal(pedido.valorDoDescontoDoPagamento);
                pagamento.Status = StatusPagtoOrdem.EmAberto;


                #region Itens do Pedido
                var itensPedido = new List<ItensPedido>();
                var itensDoPedido = (from c in data.tbItensPedido where c.pedidoId == pedidoId select c).ToList();
                foreach (var itemPedido in itensDoPedido)
                {
                    var categoria = (from c in data.tbJuncaoProdutoCategoria
                                     where
                                         c.produtoId == itemPedido.produtoId && c.tbProdutoCategoria.exibirSite == true &&
                                         c.tbProdutoCategoria.categoriaPaiId == 0
                                     select c).FirstOrDefault();
                    var itemDoPedido = new ItensPedido();
                    itemDoPedido.ItemId = itemPedido.produtoId.ToString();
                    itemDoPedido.Nome = itemPedido.tbProdutos.produtoNome;
                    itemDoPedido.Qtde = Convert.ToInt32(itemPedido.itemQuantidade);
                    itemDoPedido.Valor = itemPedido.itemValor;
                    itemDoPedido.Categoria = categoria == null ? "" : categoria.tbProdutoCategoria.categoriaNomeExibicao;
                    itensPedido.Add(itemDoPedido);
                }
                pagamento.ItensDoPedido = itensPedido;
                #endregion

                #region pagamentos boleto
                pagamento.Boletos = new List<BoletoModel>();
                var pagamentosPedido = (from c in data.tbPedidoPagamento where c.pedidoId == pedidoId && c.pagamentoPrincipal == true && c.cancelado == false && c.pagamentoNaoAutorizado == false && c.pago == false && (c.gateway ?? false) == false select c).ToList();
                if (rnConfiguracoes.ativarBoletoPagamentoV2)
                {
                    var pagamentosBoleto = pagamentosPedido.Where(x => x.condicaoDePagamentoId == 4).ToList();
                    if (pagamentosBoleto.Any())
                    {
                        foreach (var pagamentoBoleto in pagamentosBoleto)
                        {
                            var pagamentoAlterar =
                                (from c in data.tbPedidoPagamento
                                 where c.idPedidoPagamento == pagamentoBoleto.idPedidoPagamento
                                 select c).First();
                            pagamentoAlterar.gateway = true;
                            pagamentoAlterar.statusGateway = "Enviado";
                            data.SaveChanges();

                            var boleto = new BoletoModel();
                            boleto.NroBoleto = pagamentoBoleto.numeroCobranca;
                            boleto.ValorBoleto = pagamentoBoleto.valor;
                            boleto.Banco = BoletoBanco.Caixa;
                            boleto.StatusPagamento = StatusPagtoBoleto.AVencer;
                            boleto.DataVencimento = pagamentoBoleto.dataVencimento;
                            boleto.DataPagamento = null;
                            pagamento.Boletos.Add(boleto);
                        }
                    }
                }
                #endregion

                #region cartao
                pagamento.CartoesCredito = new List<CartaoCreditoModel>();
                var pagamentosCartao = pagamentosPedido.Where(x => x.tbCondicoesDePagamento.tipoDePagamentoId == 2).ToList();
                if (pagamentosCartao.Any())
                {
                    foreach (var pagamentoCartao in pagamentosCartao)
                    {
                        if (EnviarGateway(pagamentoCartao.condicaoDePagamentoId))
                        {
                            pagamento.CpfCnpjContrato = RetornaContratoMaxipago(pagamentoCartao.condicaoDePagamentoId);
                            var pagamentoAlterar =
                                (from c in data.tbPedidoPagamento
                                 where c.idPedidoPagamento == pagamentoCartao.idPedidoPagamento
                                 select c).First();
                            pagamentoAlterar.gateway = true;
                            pagamentoAlterar.statusGateway = "Enviado";
                            data.SaveChanges();

                            var cartao = new CartaoCreditoModel();
                            cartao.IdPagamento = pagamentoCartao.idPedidoPagamento;
                            cartao.NomePortadorCartao = clientePedido.clienteNome;
                            cartao.NroCartao = pagamentoCartao.numeroCartao;
                            cartao.expMonth = pagamentoCartao.validadeCartao.Split('/')[0];
                            cartao.expYear = pagamentoCartao.validadeCartao.Split('/')[1];
                            cartao.cvvNumber = pagamentoCartao.codSegurancaCartao;
                            cartao.NroParcelas = pagamentoCartao.parcelas;
                            cartao.Valor = pagamentoCartao.valor;
                            cartao.Adquirente = RetornaCodAdquirente(pagamentoCartao.condicaoDePagamentoId);
                            cartao.BandeiraCartao = RetornaBandeiraCartao(pagamentoCartao.condicaoDePagamentoId);
                            cartao.StatusPagamento = StatusPagtoCartao.Solicitado;
                            pagamento.CartoesCredito.Add(cartao);
                        }
                    }
                }
                #endregion


                if (pagamento.CartoesCredito.Any() | pagamento.Boletos.Any())
                {
                    var log = new rnLog();
                    log.usuario = "Sistema";
                    log.descricoes.Add("Enviando Pagamento Maxipago");
                    log.tiposRelacionados.Add(new rnEnums.TipoRelacionadoIds() { idRegistroRelacionado = pedidoId, tipoRelacionado = rnEnums.TipoRegistroRelacionado.Pedido });
                    log.tiposOperacao.Add(rnEnums.TipoOperacao.Pedido);

                    try
                    {
                        var objRequest = JsonConvert.SerializeObject(pagamento);
                        var request =
                            (HttpWebRequest)WebRequest.Create(rnConfiguracoes.ServicePagamentoUrl + "api/v1/pagamentos");
                        request.Method = "POST";
                        request.ContentType = "application/json";
                        var conteudo = Encoding.UTF8.GetBytes(objRequest);
                        request.GetRequestStream().Write(conteudo, 0, conteudo.Length);
                        var result = String.Empty;
                        var queueUpdate = new tbQueue();
                        queueUpdate.agendamento = DateTime.Now.AddMinutes(1);
                        queueUpdate.andamento = false;
                        queueUpdate.concluido = false;
                        queueUpdate.idRelacionado = pedidoId;
                        queueUpdate.mensagem = "";
                        queueUpdate.tipoQueue = 12;
                        data.tbQueue.Add(queueUpdate);
                        data.SaveChanges();
                        var response = (HttpWebResponse)request.GetResponse();
                        using (var stream = response.GetResponseStream())
                        using (var reader = new StreamReader(stream))
                        {
                            result = reader.ReadToEnd();
                        }
                        dynamic retorno = JsonConvert.DeserializeObject(result);

                        log.descricoes.Add("Pagamento Enviado");
                        log.descricoes.Add(result);
                        log.InsereLog();
                        return "";
                    }
                    catch (WebException ex)
                    {
                        var response = ex.Response;
                        var result = String.Empty;
                        using (var stream = response.GetResponseStream())
                        using (var reader = new StreamReader(stream))
                        {
                            result = reader.ReadToEnd();
                        }
                        log.descricoes.Add("Erro Pagamento");
                        log.descricoes.Add(result);
                        log.InsereLog();
                        return result;
                    }
                    catch (Exception ex)
                    {
                        log.descricoes.Add("Erro Pagamento");
                        log.descricoes.Add(ex.ToString());
                        log.InsereLog();
                        return ex.ToString();
                    }
                }
                return "";
            }
            catch (Exception ex)
            {

                return ex.ToString();
            }
        }
        public static string GravarPagamentoAvulso(int idPedidoPagamento)
        {
            var data = new dbSiteEntities();
            var pagamento = (from c in data.tbPedidoPagamento where c.idPedidoPagamento == idPedidoPagamento select c).First();

            var queueUpdate = new tbQueue();
            queueUpdate.idRelacionado = pagamento.pedidoId;
            queueUpdate.tipoQueue = 12;
            queueUpdate.andamento = false;
            queueUpdate.concluido = false;
            queueUpdate.agendamento = DateTime.Now.AddMinutes(2);
            queueUpdate.mensagem = "";
            data.tbQueue.Add(queueUpdate);
            data.SaveChanges();

            var riscoRepo = new BarkCommerce.Core.Repositories.AnaliseRisco.ClearSaleRepository();
            var gatewayRepo = new BarkCommerce.Core.Repositories.Gateway.MaxipagoRepository();
            var pagamentoRepo = new BarkCommerce.Core.Repositories.Pagamento.PagamentoRepository(riscoRepo, gatewayRepo);
            var pagamentoBusiness = new BarkCommerce.Business.Pagamento.ProcessarPagamentosPedido(pagamentoRepo);
            var pagamentoEnviado = pagamentoBusiness.ProcessarPagamentos(new BarkCommerce.Business.Pagamento.ProcessarPagamentosPedidoRequest() { PedidoId = pagamento.pedidoId });
            return "";



            var pagamentosGateway = (from c in data.tbPedidoPagamento where c.pedidoId == pagamento.pedidoId && c.gateway == true select c).Any();

            if (!pagamentosGateway)
            {
                return GravarOrdemPagamento(pagamento.pedidoId);
            }

            var pedido = (from c in data.tbPedidos where c.pedidoId == pagamento.pedidoId select c).First();
            var clientePedido = (from c in data.tbClientes where c.clienteId == pedido.clienteId select c).First();


            #region pagamentos boleto
            if (pagamento.condicaoDePagamentoId == 4)
            {
                if (rnConfiguracoes.ativarBoletoPagamentoV2)
                {
                    var pagamentoAlterar =
                        (from c in data.tbPedidoPagamento
                         where c.idPedidoPagamento == pagamento.idPedidoPagamento
                         select c).First();
                    pagamentoAlterar.gateway = true;
                    pagamentoAlterar.statusGateway = "Enviado";
                    data.SaveChanges();

                    var boleto = new BoletoModel();
                    boleto.NroBoleto = pagamento.numeroCobranca;
                    boleto.ValorBoleto = pagamento.valor;
                    boleto.Banco = BoletoBanco.Caixa;
                    boleto.StatusPagamento = StatusPagtoBoleto.AVencer;
                    boleto.DataVencimento = pagamento.dataVencimento;
                    boleto.DataPagamento = null;

                    try
                    {
                        var objRequest = JsonConvert.SerializeObject(boleto);
                        var request = (HttpWebRequest)WebRequest.Create(rnConfiguracoes.ServicePagamentoUrl + "api/v1/pagamentos/" + pagamento.pedidoId + "/boletos");
                        request.Method = "POST";
                        request.ContentType = "application/json";
                        var conteudo = Encoding.UTF8.GetBytes(objRequest);
                        request.GetRequestStream().Write(conteudo, 0, conteudo.Length);
                        var result = String.Empty;
                        var response = (HttpWebResponse)request.GetResponse();
                        using (var stream = response.GetResponseStream())
                        using (var reader = new StreamReader(stream))
                        {
                            result = reader.ReadToEnd();
                        }
                        dynamic retorno = JsonConvert.DeserializeObject(result);
                        return "";
                    }
                    catch (WebException ex)
                    {
                        var response = ex.Response;
                        var result = String.Empty;
                        using (var stream = response.GetResponseStream())
                        using (var reader = new StreamReader(stream))
                        {
                            result = reader.ReadToEnd();
                        }
                        return result;
                    }
                    catch (Exception ex)
                    {
                        return ex.ToString();
                    }
                }
            }

            #endregion

            #region cartao
            if (pagamento.tbCondicoesDePagamento.tipoDePagamentoId == 2)
            {
                if (EnviarGateway(pagamento.condicaoDePagamentoId))
                {
                    var pagamentoAlterar =
                        (from c in data.tbPedidoPagamento
                         where c.idPedidoPagamento == pagamento.idPedidoPagamento
                         select c).First();
                    pagamentoAlterar.gateway = true;
                    pagamentoAlterar.statusGateway = "Enviado";
                    data.SaveChanges();

                    var cartao = new CartaoCreditoModel();
                    cartao.IdPagamento = pagamento.idPedidoPagamento;
                    cartao.NomePortadorCartao = clientePedido.clienteNome;
                    cartao.NroCartao = pagamento.numeroCartao;
                    cartao.expMonth = pagamento.validadeCartao.Split('/')[0];
                    cartao.expYear = pagamento.validadeCartao.Split('/')[1];
                    cartao.cvvNumber = pagamento.codSegurancaCartao;
                    cartao.NroParcelas = pagamento.parcelas;
                    cartao.Valor = pagamento.valor;
                    cartao.Adquirente = RetornaCodAdquirente(pagamento.condicaoDePagamentoId);
                    cartao.BandeiraCartao = RetornaBandeiraCartao(pagamento.condicaoDePagamentoId);
                    cartao.StatusPagamento = StatusPagtoCartao.Solicitado;


                    try
                    {
                        var objRequest = JsonConvert.SerializeObject(cartao);
                        var request =
                            (HttpWebRequest)
                                WebRequest.Create(rnConfiguracoes.ServicePagamentoUrl + "api/v1/pagamentos/" +
                                                  pagamento.pedidoId + "/cartoes");
                        request.Method = "POST";
                        request.ContentType = "application/json";
                        var conteudo = Encoding.UTF8.GetBytes(objRequest);
                        request.GetRequestStream().Write(conteudo, 0, conteudo.Length);
                        var result = String.Empty;
                        var response = (HttpWebResponse)request.GetResponse();
                        using (var stream = response.GetResponseStream())
                        using (var reader = new StreamReader(stream))
                        {
                            result = reader.ReadToEnd();
                        }
                        dynamic retorno = JsonConvert.DeserializeObject(result);
                        data.SaveChanges();
                        return "";
                    }
                    catch (WebException ex)
                    {
                        var response = ex.Response;
                        var result = String.Empty;
                        using (var stream = response.GetResponseStream())
                        using (var reader = new StreamReader(stream))
                        {
                            result = reader.ReadToEnd();
                        }
                        return result;
                    }
                    catch (Exception ex)
                    {
                        return ex.ToString();
                    }
                }
            }
            #endregion
            return "";
        }

        public static string AtualizaPagamento(int pedidoId)
        {
            var data = new dbSiteEntities();
            var pedido = (from c in data.tbPedidos where c.pedidoId == pedidoId select c).First();
            var pagamentosPendentes = (from c in data.tbPedidoPagamento where c.pago == false && c.pagamentoNaoAutorizado == false && c.cancelado == false && c.pedidoId == pedidoId select c).Any();
            if(pagamentosPendentes)
            {
                pedido.statusDoPedido = 2;
                data.SaveChanges();
            }
            var pagamentos =
                (from c in data.tbPedidoPagamento
                    where c.pedidoId == pedidoId && c.gateway == true && c.pago == false && c.pagamentoNaoAutorizado == false && c.cancelado == false
                    select c).ToList();
            foreach (var pagamento in pagamentos)
            {
                var riscoRepo = new BarkCommerce.Core.Repositories.AnaliseRisco.ClearSaleRepository();
                var gatewayRepo = new BarkCommerce.Core.Repositories.Gateway.MaxipagoRepository();
                var pagamentoRepo = new BarkCommerce.Core.Repositories.Pagamento.PagamentoRepository(riscoRepo, gatewayRepo);
                var pagamentoBusiness = new BarkCommerce.Business.Pagamento.RetornaDetalhesPagamentoGateway(pagamentoRepo);
                var detalhesPagamento = pagamentoBusiness.RetornaPagamentoPedido(pagamento.idPedidoPagamento);

                if (detalhesPagamento != null)
                {
                    switch (detalhesPagamento.ResponseCode)
                    {
                        case 0:
                            if (detalhesPagamento.ResponseMessage.ToLower() == "captured")
                            {
                                confirmarPagamento(pagamento.idPedidoPagamento, detalhesPagamento.ProcessorTransactionId,
                                    "", false);
                            }
                            break;
                        case 200:
                            if (detalhesPagamento.ResponseMessage.ToLower() == "captured")
                            {
                                confirmarPagamento(pagamento.idPedidoPagamento, detalhesPagamento.ProcessorTransactionId,
                                    "", false);
                            }
                            break;
                        case 1:
                            pagamentoNaoAutorizado(pagamento.idPedidoPagamento, "");
                            break;
                        case 2:
                            pagamentoNaoAutorizado(pagamento.idPedidoPagamento, "");
                            break;
                        case 1024:
                            pagamentoNaoAutorizado(pagamento.idPedidoPagamento, "");
                            break;
                        case 2048:
                            pagamentoNaoAutorizado(pagamento.idPedidoPagamento, "");
                            break;
                    }
                }
                else
                {
                    var queueUpdate = new tbQueue();
                    queueUpdate.agendamento = DateTime.Now.AddMinutes(30);
                    queueUpdate.andamento = false;
                    queueUpdate.concluido = false;
                    queueUpdate.idRelacionado = pedidoId;
                    queueUpdate.mensagem = "";
                    queueUpdate.tipoQueue = 12;
                    data.tbQueue.Add(queueUpdate);
                    data.SaveChanges();
                }
            }

            return "";


            var log = new rnLog();
            log.usuario = "Sistema";
            log.descricoes.Add("Consultando Pagamento Maxipago");
            log.tiposRelacionados.Add(new rnEnums.TipoRelacionadoIds() { idRegistroRelacionado = pedidoId, tipoRelacionado = rnEnums.TipoRegistroRelacionado.Pedido });
            log.tiposOperacao.Add(rnEnums.TipoOperacao.Pedido);
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(rnConfiguracoes.ServicePagamentoUrl + "api/v1/pagamentos/" + pedidoId + "/cartoes");
                request.Method = "GET";
                var result = String.Empty;
                var response = (HttpWebResponse)request.GetResponse();
                using (var stream = response.GetResponseStream())
                using (var reader = new StreamReader(stream))
                {
                    result = reader.ReadToEnd();
                }
                log.descricoes.Add(result);
                var pagamentosCartao = JsonConvert.DeserializeObject<List<CartaoCreditoModel>>(result);
                bool consultarNovamente = false;
                foreach (var pagamentoCartao in pagamentosCartao)
                {
                    var pagamento =
                        (from c in data.tbPedidoPagamento
                         where c.idPedidoPagamento == pagamentoCartao.IdPagamento
                         select c).FirstOrDefault();
                    if (pagamento != null)
                    {
                        if (pagamento.pago == false && pagamento.pagamentoNaoAutorizado == false)
                        {
                            pagamento.statusGateway = pagamentoCartao.StatusPagamento.ToString();
                            if (pagamentoCartao.StatusPagamento == StatusPagtoCartao.Autorizado)
                            {
                                log.descricoes.Add("Pagamento Autorizado");
                                if (pagamentoCartao.AnaliseRisco != null)
                                {
                                    pagamento.statusGateway = pagamentoCartao.AnaliseRisco.DecisaoTomada + " - " + pagamentoCartao.AnaliseRisco.StatusDecisao;
                                }
                            }
                            data.SaveChanges();

                            if (pagamentoCartao.StatusPagamento == StatusPagtoCartao.Capturado)
                            {
                                log.descricoes.Add("Pagamento Capturado");
                                string mensagem = "";
                                if (pagamentoCartao.AnaliseRisco != null)
                                {
                                    mensagem = "Risco: " + pagamentoCartao.AnaliseRisco.Score + " - " +
                                               pagamentoCartao.AnaliseRisco.DecisaoTomada;
                                }
                                string numeroCobranca = "";
                                if (pagamentoCartao.RespostaGatewayPagtos != null)
                                {
                                    numeroCobranca = pagamentoCartao.RespostaGatewayPagtos.ProcessorTransactionID;
                                }
                                confirmarPagamento(pagamento.idPedidoPagamento, numeroCobranca, mensagem, false);
                            }
                            else if (pagamentoCartao.StatusPagamento == StatusPagtoCartao.NaoAutorizado |
                                     pagamentoCartao.StatusPagamento == StatusPagtoCartao.EstornoSolicitado |
                                     pagamentoCartao.StatusPagamento == StatusPagtoCartao.EstornoRealizado |
                                     pagamentoCartao.StatusPagamento == StatusPagtoCartao.CancelarReservaSolicitada |
                                     pagamentoCartao.StatusPagamento == StatusPagtoCartao.CancelarReservaRealizada
                                )
                            {
                                log.descricoes.Add("Pagamento Não Autorizado");
                                string mensagem = "";
                                if (pagamentoCartao.AnaliseRisco != null)
                                {
                                    mensagem = "Risco: " + pagamentoCartao.AnaliseRisco.Score + " - " +
                                               pagamentoCartao.AnaliseRisco.DecisaoTomada + " - " +
                                               pagamentoCartao.RespostaGatewayPagtos.ResponseMessage + " - " +
                                               pagamentoCartao.RespostaGatewayPagtos.ErrorMessage;
                                }
                                pagamentoNaoAutorizado(pagamento.idPedidoPagamento, mensagem);
                            }
                            else
                            {
                                consultarNovamente = true;
                            }
                        }
                    }
                }
                log.InsereLog();
                if (consultarNovamente)
                {
                    var queueUpdate = new tbQueue();
                    queueUpdate.agendamento = DateTime.Now.AddMinutes(30);
                    queueUpdate.andamento = false;
                    queueUpdate.concluido = false;
                    queueUpdate.idRelacionado = pedido.pedidoId;
                    queueUpdate.mensagem = "";
                    queueUpdate.tipoQueue = 12;
                    data.tbQueue.Add(queueUpdate);
                    data.SaveChanges();
                }
                return "";
            }
            catch (WebException ex)
            {
                var response = ex.Response;
                var result = String.Empty;
                using (var stream = response.GetResponseStream())
                using (var reader = new StreamReader(stream))
                {
                    result = reader.ReadToEnd();
                }
                log.descricoes.Add(result);
                log.InsereLog();
                return result;
            }
            catch (Exception ex)
            {
                log.descricoes.Add(ex.ToString());
                log.InsereLog();
                return ex.ToString();
            }

            return "";
        }


        public static string CancelarClearSaleFraude(int idPedido, string statusPedido)
        {
            var riscoRepo = new BarkCommerce.Core.Repositories.AnaliseRisco.ClearSaleRepository();
            var gatewayRepo = new BarkCommerce.Core.Repositories.Gateway.MaxipagoRepository();
            var pagamentoRepo = new BarkCommerce.Core.Repositories.Pagamento.PagamentoRepository(riscoRepo, gatewayRepo);
            var pagamentoBusiness = new BarkCommerce.Business.Pagamento.RetornaDetalhesPagamentoGateway(pagamentoRepo);
            var resultados = riscoRepo.ObterResultadoAnaliseRisco(idPedido);

            var data2 = new dbSiteEntities();
            //var statuspedidocad = (from c in data2.tbPedidos where c.pedidoId == idPedido select c.statusDoPedido).FirstOrDefault();
            //bool pago = statuspedidocad == 3 | statuspedidocad == 4 | statuspedidocad == 5 | statuspedidocad == 8 | statuspedidocad == 9 | statuspedidocad == 10 | statuspedidocad == 11;

            string interacao = "";
            int acao = 0;
            switch (statusPedido)
            {
                case "apa":
                    interacao =  " - Aprovação Automática";
                    acao = 1;
                    break;
                case "apm":
                    interacao = " - Aprovação Manual";
                    acao = 1;
                    break;
                case "rpm":
                    interacao = " - Reprovado Sem Suspeita";
                    acao = 2;
                    break;
                case "err":
                    interacao = " - Erro";
                    acao = 2;
                    break;
                case "sus":
                    interacao =  " - Suspensão Manual";
                    acao = 2;
                    break;
                case "can":
                    acao = 2;
                    interacao = " - Cancelado";
                    break;
                case "frd":
                    interacao = " - Fraude Confirmada";
                    acao = 2;
                    break;
                case "rpa":
                    interacao = " - Reprovação Automática";
                    acao = 2;
                    break;
                case "rpp":
                    interacao = " - Reprovação Por Política";
                    acao = 2;
                    break;
            }

            int pedidoId = Convert.ToInt32(idPedido);
            var data = new dbSiteEntities();
            var pagamentos =
                (from c in data.tbPedidoPagamento
                    where
                            c.pedidoId == pedidoId && c.gateway == true && c.pago == false &&
                            c.pagamentoNaoAutorizado == false && c.cancelado == false
                    select c).ToList();
            foreach (var pagamento in pagamentos)
            {
                if (acao == 1)
                {
                    var detalhesPagamento = pagamentoBusiness.RetornaPagamentoPedido(pagamento.idPedidoPagamento);
                    if (detalhesPagamento != null)
                    {
                        switch (detalhesPagamento.ResponseCode)
                        {
                            case 0:
                                if (detalhesPagamento.ResponseMessage.ToLower().Contains("authorized"))
                                {
                                    pagamentoRepo.CapturaPagamento(detalhesPagamento.IdPedidoPagamento);
                                    var queueUpdate = new tbQueue();
                                    queueUpdate.agendamento = DateTime.Now.AddMinutes(1);
                                    queueUpdate.andamento = false;
                                    queueUpdate.concluido = false;
                                    queueUpdate.idRelacionado = pedidoId;
                                    queueUpdate.mensagem = "";
                                    queueUpdate.tipoQueue = 12;
                                    data.tbQueue.Add(queueUpdate);
                                    data.SaveChanges();
                                }
                                break;
                        }
                    }
                }
                if (acao == 2)
                {
                    pagamentoRepo.EstornarPagamento(pagamento.idPedidoPagamento);
                    pagamentoNaoAutorizado(pagamento.idPedidoPagamento, "");
                    rnPedidos.CancelarPedido(pagamento.pedidoId, "Sistema", 7);
                    rnPedidos.AdicionarChamado(DateTime.Now.AddDays(1), pagamento.pedidoId, "Verificar pagamento cancelado Clearsale", "Verificar motivo do cancelamento da clearsale", 218);
                }
            }
            riscoRepo.AtualizaAnaliseProcessada(Convert.ToInt32(idPedido), "99", statusPedido);
            riscoRepo.BaixarAnaliseProcessada(idPedido);
            if(!string.IsNullOrEmpty(interacao)) rnInteracoes.interacaoInclui(idPedido, interacao, "Sistema", "False");
            return "";
        }


        public static string AtualizaClearsale(int idPedido)
        {
            var riscoRepo = new BarkCommerce.Core.Repositories.AnaliseRisco.ClearSaleRepository();
            var gatewayRepo = new BarkCommerce.Core.Repositories.Gateway.MaxipagoRepository();
            var pagamentoRepo = new BarkCommerce.Core.Repositories.Pagamento.PagamentoRepository(riscoRepo, gatewayRepo);
            var pagamentoBusiness = new BarkCommerce.Business.Pagamento.RetornaDetalhesPagamentoGateway(pagamentoRepo);
            var resultados = riscoRepo.ObterResultadoAnaliseRisco(idPedido);

            var data2 = new dbSiteEntities();
            //var statuspedidocad = (from c in data2.tbPedidos where c.pedidoId == idPedido select c.statusDoPedido).FirstOrDefault();
            //bool pago = statuspedidocad == 3 | statuspedidocad == 4 | statuspedidocad == 5 | statuspedidocad == 8 | statuspedidocad == 9 | statuspedidocad == 10 | statuspedidocad == 11;


            foreach (var resultado in resultados)
            {
                string interacao = "";
                //var segundaTentiva = (from c in data2.tbPedidoInteracoes where c.pedidoId == idPedido && c.interacao.Contains("Score: " + resultado.Score) select c).Any();
                var segundaTentiva = false;
                int acao = 0;
                string statusPedido = resultado.Status.ToLower();
                switch (statusPedido)
                {
                    case "apa":
                        interacao =  "Score: " + resultado.Score + " - Aprovação Automática";
                        acao = 1;
                        break;
                    case "apm":
                        interacao = "Score: " + resultado.Score + " - Aprovação Manual";
                        acao = 1;
                        break;
                    case "rpm":
                        interacao = "Score: " + resultado.Score + " - Reprovado Sem Suspeita";
                        acao = 2;
                        break;
                    case "err":
                        interacao = "Score: " + resultado.Score + " - Erro";
                        acao = 2;
                        break;
                    case "sus":
                        interacao = "Score: " + resultado.Score + " - Suspensão Manual";
                        acao = 2;
                        break;
                    case "can":
                        acao = 2;
                        interacao = "Score: " + resultado.Score + " - Cancelado";
                        break;
                    case "frd":
                        interacao = "Score: " + resultado.Score + " - Fraude Confirmada";
                        acao = 2;
                        break;
                    case "rpa":
                        interacao = "Score: " + resultado.Score + " - Reprovação Automática";
                        acao = 2;
                        break;
                    case "rpp":
                        interacao = "Score: " + resultado.Score + " - Reprovação Por Política";
                        acao = 2;
                        break;
                }

                int pedidoId = Convert.ToInt32(resultado.PedidoId);
                var data = new dbSiteEntities();
                var pagamentos =
                    (from c in data.tbPedidoPagamento
                     where
                             c.pedidoId == pedidoId && c.gateway == true && c.pago == false &&
                             c.pagamentoNaoAutorizado == false && c.cancelado == false
                     select c).ToList();
                foreach (var pagamento in pagamentos)
                {
                    if (acao == 1)
                    {
                        var detalhesPagamento = pagamentoBusiness.RetornaPagamentoPedido(pagamento.idPedidoPagamento);
                        if (detalhesPagamento != null)
                        {
                            switch (detalhesPagamento.ResponseCode)
                            {
                                case 0:
                                    if (detalhesPagamento.ResponseMessage.ToLower().Contains("authorized"))
                                    {
                                        pagamentoRepo.CapturaPagamento(detalhesPagamento.IdPedidoPagamento);
                                        var queueUpdate = new tbQueue();
                                        queueUpdate.agendamento = DateTime.Now.AddMinutes(1);
                                        queueUpdate.andamento = false;
                                        queueUpdate.concluido = false;
                                        queueUpdate.idRelacionado = pedidoId;
                                        queueUpdate.mensagem = "";
                                        queueUpdate.tipoQueue = 12;
                                        data.tbQueue.Add(queueUpdate);
                                        data.SaveChanges();
                                    }
                                    break;
                            }
                        }
                    }
                    if (acao == 2)
                    {
                        pagamentoRepo.EstornarPagamento(pagamento.idPedidoPagamento);
                        pagamentoNaoAutorizado(pagamento.idPedidoPagamento, "", false);
                        rnPedidos.CancelarPedido(pagamento.pedidoId, "Sistema", 7);
                        rnPedidos.AdicionarChamado(DateTime.Now.AddDays(1), pagamento.pedidoId, "Verificar pagamento cancelado Clearsale", "Verificar motivo do cancelamento da clearsale", 218);
                    }
                }
                riscoRepo.AtualizaAnaliseProcessada(Convert.ToInt32(resultado.PedidoId), resultado.Score, resultado.Status);
                riscoRepo.BaixarAnaliseProcessada(resultado.PedidoId);
                if (!string.IsNullOrEmpty(interacao)) rnInteracoes.interacaoInclui(resultado.PedidoId, interacao, "Sistema", "False");
            }


            return "";
        }


        public static string AtualizaClearsale()
        {
            var riscoRepo = new BarkCommerce.Core.Repositories.AnaliseRisco.ClearSaleRepository();
            var gatewayRepo = new BarkCommerce.Core.Repositories.Gateway.MaxipagoRepository();
            var pagamentoRepo = new BarkCommerce.Core.Repositories.Pagamento.PagamentoRepository(riscoRepo, gatewayRepo);
            var pagamentoBusiness = new BarkCommerce.Business.Pagamento.RetornaDetalhesPagamentoGateway(pagamentoRepo);
            var resultados = riscoRepo.ObterResultadosAnaliseRisco().ToList();
            resultados = resultados.Where(x => x.PedidoId == 708838).ToList();

            foreach (var resultado in resultados)
            {
                string interacao = "";
                int acao = 0;
                string statusPedido = resultado.Status.ToLower();
                switch (statusPedido)
                {
                    case "apa":
                        interacao = "Score: " + resultado.Score + " - Aprovação Automática";
                        acao = 1;
                        break;
                    case "apm":
                        interacao = "Score: " + resultado.Score + " - Aprovação Manual";
                        acao = 1;
                        break;
                    case "rpm":
                        interacao = "Score: " + resultado.Score + " - Reprovado Sem Suspeita";
                        acao = 2;
                        break;
                    case "err":
                        interacao = "Score: " + resultado.Score + " - Erro";
                        acao = 2;
                        break;
                    case "sus":
                        interacao = "Score: " + resultado.Score + " - Suspensão Manual";
                        acao = 2;
                        break;
                    case "can":
                        acao = 2;
                        interacao = "Score: " + resultado.Score + " - Cancelado";
                        break;
                    case "frd":
                        interacao = "Score: " + resultado.Score + " - Fraude Confirmada";
                        acao = 2;
                        break;
                    case "rpa":
                        interacao = "Score: " + resultado.Score + " - Reprovação Automática";
                        acao = 2;
                        break;
                    case "rpp":
                        interacao = "Score: " + resultado.Score + " - Reprovação Por Política";
                        acao = 2;
                        break;
                }

                int pedidoId = Convert.ToInt32(resultado.PedidoId);
                    var data = new dbSiteEntities();
                    var pagamentos =
                        (from c in data.tbPedidoPagamento
                            where
                                c.pedidoId == pedidoId && c.gateway == true && c.pago == false &&
                                c.pagamentoNaoAutorizado == false && c.cancelado == false
                            select c).ToList();
                    foreach (var pagamento in pagamentos)
                    {
                        if (acao == 1)
                        {
                            var detalhesPagamento = pagamentoBusiness.RetornaPagamentoPedido(pagamento.idPedidoPagamento);
                            if (detalhesPagamento != null)
                            {
                            switch (detalhesPagamento.ResponseCode)
                            {
                                case 0:
                                    if (detalhesPagamento.ResponseMessage.ToLower().Contains("authorized"))
                                    {
                                        pagamentoRepo.CapturaPagamento(detalhesPagamento.IdPedidoPagamento);
                                        var queueUpdate = new tbQueue();
                                        queueUpdate.agendamento = DateTime.Now.AddMinutes(1);
                                        queueUpdate.andamento = false;
                                        queueUpdate.concluido = false;
                                        queueUpdate.idRelacionado = pedidoId;
                                        queueUpdate.mensagem = "";
                                        queueUpdate.tipoQueue = 12;
                                        data.tbQueue.Add(queueUpdate);
                                        data.SaveChanges();
                                    }
                                    break;

                            }
                        }
                        }
                        if (acao == 2)
                    {
                        pagamentoRepo.EstornarPagamento(pagamento.idPedidoPagamento);
                        pagamentoNaoAutorizado(pagamento.idPedidoPagamento, "");
                        rnPedidos.CancelarPedido(pagamento.pedidoId, "Sistema", 7);
                        rnPedidos.AdicionarChamado(DateTime.Now.AddDays(1), pagamento.pedidoId, "Verificar pagamento cancelado Clearsale", "Verificar motivo do cancelamento da clearsale", 218);
                    }
                }
                riscoRepo.AtualizaAnaliseProcessada(Convert.ToInt32(resultado.PedidoId), resultado.Score, resultado.Status);
                riscoRepo.BaixarAnaliseProcessada(resultado.PedidoId);
                if (!string.IsNullOrEmpty(interacao)) rnInteracoes.interacaoInclui(resultado.PedidoId, interacao, "Sistema", "False");
            }



            return "";
        }
        public static CodAdquirente RetornaCodAdquirente(int condicaoDePagamentoId)
        {
            if (rnConfiguracoes.AmbienteDebugPagamentos) // Debug
            {
                return CodAdquirente.SimuladorTestes;
            }
            else if (condicaoDePagamentoId == 3) // Visa
            {
                return CodAdquirente.Rede;
            }
            else if (condicaoDePagamentoId == 5) // Mastercard
            {
                return CodAdquirente.Rede;
            }
            else if (condicaoDePagamentoId == 6) // Diners
            {
                return CodAdquirente.Cielo;
            }
            else if (condicaoDePagamentoId == 7) // American Express
            {
                return CodAdquirente.Cielo;
            }
            else if (condicaoDePagamentoId == 10) // Elo
            {
                return CodAdquirente.Cielo;
            }
            else if (condicaoDePagamentoId == 11) // Aura
            {
                return CodAdquirente.Cielo;
            }
            else if (condicaoDePagamentoId == 14) // Hipercard
            {
                return CodAdquirente.Rede;
            }
            else if (condicaoDePagamentoId == 22) // Sorocred
            {
                return CodAdquirente.Cielo;
            }
            else
            {
                return CodAdquirente.Cielo;
            }
        }
        public static BandeiraCartaoCredito RetornaBandeiraCartao(int condicaoDePagamentoId)
        {
            if (condicaoDePagamentoId == 3) // Visa
            {
                return BandeiraCartaoCredito.Visa;
            }
            else if (condicaoDePagamentoId == 5) // Mastercard
            {
                return BandeiraCartaoCredito.MasterCard;
            }
            else if (condicaoDePagamentoId == 6) // Diners
            {
                return BandeiraCartaoCredito.Dinners;
            }
            else if (condicaoDePagamentoId == 7) // American Express
            {
                return BandeiraCartaoCredito.Amex;
            }
            else if (condicaoDePagamentoId == 10) // Elo
            {
                return BandeiraCartaoCredito.Outro;
            }
            else if (condicaoDePagamentoId == 11) // Aura
            {
                return BandeiraCartaoCredito.Aura;
            }
            else if (condicaoDePagamentoId == 14) // Hipercard
            {
                return BandeiraCartaoCredito.HyperCard;
            }
            else if (condicaoDePagamentoId == 22) // Sorocred
            {
                return BandeiraCartaoCredito.Outro;
            }
            else
            {
                return BandeiraCartaoCredito.Outro;
            }
        }

        public static bool EnviarGateway(int condicaoDePagamentoId)
        {
            if (condicaoDePagamentoId == 3) // Visa
            {
                return true;
            }
            else if (condicaoDePagamentoId == 5) // Mastercard
            {
                return true;
            }
            else if (condicaoDePagamentoId == 6) // Diners
            {
                return true;
            }
            else if (condicaoDePagamentoId == 7) // American Express
            {
                return true;
            }
            else if (condicaoDePagamentoId == 10) // Elo
            {
                return true;
            }
            else if (condicaoDePagamentoId == 11) // Aura
            {
                return false;
            }
            else if (condicaoDePagamentoId == 14) // Hipercard
            {
                return true;
            }
            else if (condicaoDePagamentoId == 22) // Sorocred
            {
                return false;
            }
            else
            {
                return false;
            }
        }

        public static string RetornaContratoMaxipago(int condicaoDePagamentoId)
        {
            string contratoLindsay = "10924051000163";
            string contratoLuiz = "23549499000196";

            if (condicaoDePagamentoId == 3) // Visa
            {
                return contratoLindsay;
            }
            else if (condicaoDePagamentoId == 5) // Mastercard
            {
                return contratoLindsay;
            }
            else if (condicaoDePagamentoId == 6) // Diners
            {
                return contratoLindsay;
            }
            else if (condicaoDePagamentoId == 7) // American Express
            {
                return contratoLindsay;
            }
            else if (condicaoDePagamentoId == 10) // Elo
            {
                return contratoLindsay;
            }
            else if (condicaoDePagamentoId == 11) // Aura
            {
                return contratoLindsay;
            }
            else if (condicaoDePagamentoId == 14) // Hipercard
            {
                return contratoLindsay;
            }
            else if (condicaoDePagamentoId == 22) // Sorocred
            {
                return contratoLindsay;
            }
            else
            {
                return contratoLindsay;
            }
        }

        public static int confirmarPagamento(int idPedidoPagamento, string numeroCobranca, string observacao, bool consolidado)
        {
            string interacao = "<b>Pagamento Efetuado</b><br>";
            var data = new dbSiteEntities();
            var pagamento = (from c in data.tbPedidoPagamento where c.idPedidoPagamento == idPedidoPagamento select c).First();

            interacao += "<b>Observação:</b> " + observacao + "<br>";
            if (!string.IsNullOrEmpty(numeroCobranca)) pagamento.numeroCobranca = numeroCobranca;
            pagamento.dataPagamento = DateTime.Now;
            if (!string.IsNullOrEmpty(numeroCobranca) && pagamento.idOperadorPagamento != 1) pagamento.numeroCobranca = numeroCobranca;
            pagamento.consolidado = consolidado;
            pagamento.pago = true;
            pagamento.cancelado = false;
            data.SaveChanges();

            rnInteracoes.interacaoInclui(pagamento.pedidoId, interacao, "Sistema", "False");
            if (pagamento.tbPedidos.tipoDePagamentoId == 11)
            {
                rnEmails.enviaEmailPagamentoConfirmadoPlano(pagamento.tbPedidos.pedidoId);
            }
            return verificaPagamentoCompleto(pagamento.pedidoId);
        }

        public static int verificaPagamentoCompleto(int pedidoId)
        {
            var data = new dbSiteEntities();
            var pedido = (from c in data.tbPedidos where c.pedidoId == pedidoId select c).First();
            var pagamentos = (from c in data.tbPedidoPagamento where c.pagamentoPrincipal && c.pago && c.cancelado == false && c.pedidoId == pedidoId select c).ToList();

            if (pagamentos.Any() && pedido.statusDoPedido != 5 && pedido.statusDoPedido != 6)// se o pedido já tem pagamento e seu status é diferente de enviado ou cancelado faz a verificação
            {
                var totalPago = pagamentos.Sum(x => x.valor);
                if (pedido.valorCobrado <= (totalPago + Convert.ToDecimal("0,05")))
                {
                    return confirmarPagamentoPedido(pedidoId, "Sistema");
                }
                else
                {
                    if (pedido.statusDoPedido != 2)
                    {
                        pedido.statusDoPedido = 2;
                        data.SaveChanges();
                        rnInteracoes.interacaoInclui(pedidoId, "Status alterado para Aguardando Confirmação de Pagamento", "Sistema", "False");
                    }
                }
            }
            else
            {
                if (pedido.statusDoPedido != 2)
                {
                    pedido.statusDoPedido = 2;
                    data.SaveChanges();
                    rnInteracoes.interacaoInclui(pedidoId, "Status alterado para Aguardando Confirmação de Pagamento", "Sistema", "False");
                }
            }
            return 99; // não confirmado
        }


        public static void pagamentoNaoAutorizado(int idPedidoPagamento, string motivo)
        {
            pagamentoNaoAutorizado(idPedidoPagamento, motivo, true);
        }
        public static void pagamentoNaoAutorizado(int idPedidoPagamento, string motivo, bool alterarStatus)
        {
            var data = new dbSiteEntities();
            var pagamento = (from c in data.tbPedidoPagamento where c.idPedidoPagamento == idPedidoPagamento select c).FirstOrDefault();
            if (pagamento != null)
            {
                var pedido = (from c in data.tbPedidos where c.pedidoId == pagamento.pedidoId select c).First();
                if(alterarStatus) pedido.statusDoPedido = 7;
                string interacao = "<b>Pagamento Não Autorizado</b><br>";
                interacao += "Motivo: " + motivo + "<br>";
                interacao += "Valor: " + pagamento.valor.ToString("C");
                pagamento.pagamentoNaoAutorizado = true;
                pagamento.observacao = motivo;
                data.SaveChanges();
                rnInteracoes.interacaoInclui(pagamento.pedidoId, interacao, "Sistema", "False");
                rnEmails.enviaEmailPagamentoRecusado(pagamento.pedidoId);
            }
        }
        public static int confirmarPagamentoPedido(int pedidoId, string usuario)
        {
            int retorno = 0;

            using (var pedidoAlteraContext = new dbSiteEntities())
            {
                var pedido = (from c in pedidoAlteraContext.tbPedidos where c.pedidoId == pedidoId select c).First();
                var clientesDc = new dbSiteEntities();
                var cliente = (from c in clientesDc.tbClientes where c.clienteId == pedido.clienteId select c).First();
                pedido.dataConfirmacaoPagamento = DateTime.Now;

                if (pedido.statusDoPedido != 5)// só alterar o status se ainda não tiver sido enviado
                {
                    pedido.statusDoPedido = 3;
                    rnInteracoes.interacaoInclui(pedidoId, "Pagamento Confirmado", usuario, "True");
                }

                if (pedido.prazoMaximoPedidos != null)
                {

                    int prazoInicioFabricacao = 0;
                    if (pedido.prazoInicioFabricao != null)
                    {
                        int diferencaDias = (DateTime.Now - Convert.ToDateTime(pedido.dataHoraDoPedido)).Days;
                        prazoInicioFabricacao = (int)pedido.prazoInicioFabricao - diferencaDias;
                        if (prazoInicioFabricacao < 0) prazoInicioFabricacao = 0;
                    }

                    int prazoFinal = rnFuncoes.retornaPrazoDiasUteis((int)pedido.prazoMaximoPedidos, prazoInicioFabricacao);

                    // verifica se é necessário gravar prazoFinalPedido
                    if (pedido.prazoFinalPedido == null)
                    {
                        pedido.prazoFinalPedido = DateTime.Now.AddDays(prazoFinal);
                    }

                    //verificação especifica para plano maternidade
                    if (pedido.tipoDePagamentoId == 11)// plano maternidade
                    {
                        var cobrancas = (from c in pedidoAlteraContext.tbPedidoPagamento
                                         where c.pedidoId == pedidoId && c.cancelado == false
                                         orderby c.dataVencimento
                                         select c).ToList();

                        DateTime prazoFinalComparacao = cobrancas.OrderByDescending(x => x.dataVencimento).First().dataVencimento.AddDays(2);// colocado acréscimo de mais 2 dias pois a transportadora despacha no dia seguinte da postagem

                        if (prazoFinalComparacao < DateTime.Now.AddDays(prazoFinal))
                            pedido.prazoFinalPedido = prazoFinalComparacao;
                    }

                    pedidoAlteraContext.SaveChanges();
                }

                var queuePrazo = new tbQueue();
                queuePrazo.agendamento = DateTime.Now;
                queuePrazo.andamento = false;
                queuePrazo.concluido = false;
                queuePrazo.mensagem = "";
                queuePrazo.tipoQueue = 5;
                queuePrazo.idRelacionado = pedidoId;
                pedidoAlteraContext.tbQueue.Add(queuePrazo);
                pedidoAlteraContext.SaveChanges();

                var queue = new tbQueue();
                queue.agendamento = DateTime.Now;
                queue.andamento = false;
                queue.concluido = false;
                queue.mensagem = "";
                queue.tipoQueue = 6;
                queue.idRelacionado = pedidoId;
                pedidoAlteraContext.tbQueue.Add(queue);
                pedidoAlteraContext.SaveChanges();
                //rnEstoque.ReservarEstoque(pedidoId);

                var completo = rnEstoque.verificaReservasPedidoFornecedorCompleto(pedidoId);
                if (completo)
                {
                    pedido.statusDoPedido = 11;
                    rnInteracoes.interacaoInclui(pedidoId, "Separação de Estoque", usuario, "True");
                    rnEmails.enviaEmailPedidoProcessado(pedidoId);
                    //rnEmails.enviaAlteracaoDeStatus(cliente.clienteNome, pedidoId.ToString(), "Separação de Estoque", cliente.clienteEmail);
                    retorno = 1;
                }

                var queueCompleto = new tbQueue();
                queueCompleto.agendamento = DateTime.Now;
                queueCompleto.andamento = false;
                queueCompleto.concluido = false;
                queueCompleto.mensagem = "";
                queueCompleto.tipoQueue = 8;
                queueCompleto.idRelacionado = pedidoId;
                pedidoAlteraContext.tbQueue.Add(queueCompleto);
                pedidoAlteraContext.SaveChanges();

                var queueConfirmacaoDados = new tbQueue();
                queueConfirmacaoDados.agendamento = DateTime.Now.AddMinutes(1);
                queueConfirmacaoDados.andamento = false;
                queueConfirmacaoDados.concluido = false;
                queueConfirmacaoDados.mensagem = "";
                queueConfirmacaoDados.tipoQueue = 22;
                queueConfirmacaoDados.idRelacionado = pedidoId;
                pedidoAlteraContext.tbQueue.Add(queueConfirmacaoDados);
                pedidoAlteraContext.SaveChanges();
            }

            rnEmails.enviaEmailPagamentoConfirmado(pedidoId);

            return retorno;
        }
    }
}
