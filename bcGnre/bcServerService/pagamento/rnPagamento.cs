﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace bcServerService.pagamento
{
    class rnPagamento
    {

        public static void GerarPagamentoPedido(int pedidoId)
        {
            var data = new dbSiteEntities();
            var pedido = (from c in data.tbPedidos where c.pedidoId == pedidoId select c).First();
            if (pedido.tipoDePagamentoId == 1)
            {
                string numeroCobranca = (String.Format("{0:00000000}", pedido.pedidoId));
                var pedidoPagamento = new tbPedidoPagamento();
                pedidoPagamento.pedidoId = pedido.pedidoId;
                pedidoPagamento.pagamentoPrincipal = true;
                var dataPedido = pedido.dataHoraDoPedido == null ? DateTime.Now : (DateTime)pedido.dataHoraDoPedido;
                pedidoPagamento.dataVencimento = dataPedido.AddDays(4);
                pedidoPagamento.valor = Convert.ToDecimal(pedido.valorCobrado);
                bool pago = pedido.statusDoPedido == 3 | pedido.statusDoPedido == 4 | pedido.statusDoPedido == 5 |
                            pedido.statusDoPedido == 8 | pedido.statusDoPedido == 9 | pedido.statusDoPedido == 10 |
                            pedido.statusDoPedido == 11;
                pedidoPagamento.dataPagamento = pedido.dataConfirmacaoPagamento;
                pedidoPagamento.pago = pago;
                pedidoPagamento.valor = Convert.ToDecimal(pedido.valorCobrado);
                pedidoPagamento.pago = false;
                pedidoPagamento.numeroCobranca = numeroCobranca;
                pedidoPagamento.recebimentoParcelado = false;
                pedidoPagamento.observacao = "";
                pedidoPagamento.idOperadorPagamento = defineOperadorPagamento((int)pedido.condDePagamentoId);
                pedidoPagamento.consolidado = false;
                pedidoPagamento.parcelas = 1;
                pedidoPagamento.cancelado = false;
                pedidoPagamento.condicaoDePagamentoId = (int)pedido.condDePagamentoId;
                pedidoPagamento.valorParcela = Convert.ToDecimal(pedido.valorCobrado);
                pedidoPagamento.pagamentoNaoAutorizado = pedido.statusDoPedido == 7;
                pedidoPagamento.valorTotal = Convert.ToDecimal(pedido.valorTotalGeral);
                pedidoPagamento.valorDesconto = Convert.ToDecimal(pedido.valorDoDescontoDoPagamento);
                pedidoPagamento.nomeCartao = "";
                pedidoPagamento.numeroCartao = "";
                pedidoPagamento.validadeCartao = "";
                pedidoPagamento.codSegurancaCartao = "";
                pedidoPagamento.idContaPagamento = 1;
                data.tbPedidoPagamento.Add(pedidoPagamento);
                data.SaveChanges();
                
            }
            else if (pedido.tipoDePagamentoId == 8)
            {
                if (pedido.tipoDePagamentoId1 == 1)
                {
                    string numeroCobranca = (String.Format("{0:00000000}", pedido.pedidoId));
                    var pedidoPagamento = new tbPedidoPagamento();
                    pedidoPagamento.pedidoId = pedido.pedidoId;
                    pedidoPagamento.pagamentoPrincipal = true;
                    pedidoPagamento.valor = Convert.ToDecimal(pedido.valorPagamento1);
                    var dataPedido = pedido.dataHoraDoPedido == null ? DateTime.Now : (DateTime)pedido.dataHoraDoPedido;
                    pedidoPagamento.dataVencimento = dataPedido.AddDays(4);
                    bool pago = pedido.statusDoPedido == 3 | pedido.statusDoPedido == 4 | pedido.statusDoPedido == 5 |
                                pedido.statusDoPedido == 8 | pedido.statusDoPedido == 9 | pedido.statusDoPedido == 10 |
                                pedido.statusDoPedido == 11;
                    pedidoPagamento.dataPagamento = pedido.dataConfirmacaoPagamento;
                    pedidoPagamento.pago = pago;
                    pedidoPagamento.numeroCobranca = numeroCobranca;
                    pedidoPagamento.recebimentoParcelado = false;
                    pedidoPagamento.observacao = "";
                    pedidoPagamento.idOperadorPagamento = defineOperadorPagamento((int)pedido.condDePagamentoId1);
                    pedidoPagamento.consolidado = false;
                    pedidoPagamento.parcelas = 1;
                    pedidoPagamento.cancelado = false;
                    pedidoPagamento.condicaoDePagamentoId = (int)pedido.condDePagamentoId1;
                    pedidoPagamento.valorParcela = Convert.ToDecimal(pedido.valorPagamento1);
                    pedidoPagamento.pagamentoNaoAutorizado = pedido.statusDoPedido == 7;
                    pedidoPagamento.valorTotal = Convert.ToDecimal(pedido.valorPagamento1 + pedido.valorDoDescontoDoPagamento);
                    pedidoPagamento.valorDesconto = Convert.ToDecimal(pedido.valorDoDescontoDoPagamento);
                    pedidoPagamento.nomeCartao = "";
                    pedidoPagamento.numeroCartao = "";
                    pedidoPagamento.validadeCartao = "";
                    pedidoPagamento.codSegurancaCartao = "";
                    pedidoPagamento.idContaPagamento = defineOperadorPagamento((int)pedido.condDePagamentoId1);
                    data.tbPedidoPagamento.Add(pedidoPagamento);
                    data.SaveChanges();
                }
                if (pedido.tipoDePagamentoId2 == 1)
                {
                    string numeroCobranca = (String.Format("{0:00000000}", pedido.pedidoId));
                    var pedidoPagamento = new tbPedidoPagamento();
                    pedidoPagamento.pedidoId = pedido.pedidoId;
                    pedidoPagamento.pagamentoPrincipal = true;
                    pedidoPagamento.valor = Convert.ToDecimal(pedido.valorPagamento2);
                    var dataPedido = pedido.dataHoraDoPedido == null ? DateTime.Now : (DateTime)pedido.dataHoraDoPedido;
                    pedidoPagamento.dataVencimento = dataPedido.AddDays(4);
                    bool pago = pedido.statusDoPedido == 3 | pedido.statusDoPedido == 4 | pedido.statusDoPedido == 5 |
                                pedido.statusDoPedido == 8 | pedido.statusDoPedido == 9 | pedido.statusDoPedido == 10 |
                                pedido.statusDoPedido == 11;
                    pedidoPagamento.dataPagamento = pedido.dataConfirmacaoPagamento;
                    pedidoPagamento.pago = pago;
                    pedidoPagamento.numeroCobranca = numeroCobranca;
                    pedidoPagamento.recebimentoParcelado = false;
                    pedidoPagamento.observacao = "";
                    pedidoPagamento.idOperadorPagamento = defineOperadorPagamento((int)pedido.condDePagamentoId2);
                    pedidoPagamento.consolidado = false;
                    pedidoPagamento.parcelas = 1;
                    pedidoPagamento.cancelado = false;
                    pedidoPagamento.condicaoDePagamentoId = (int)pedido.condDePagamentoId2;
                    pedidoPagamento.valorParcela = Convert.ToDecimal(pedido.valorPagamento2);
                    pedidoPagamento.pagamentoNaoAutorizado = pedido.statusDoPedido == 7;
                    pedidoPagamento.valorTotal = Convert.ToDecimal(pedido.valorPagamento2 + pedido.valorDoDescontoDoPagamento);
                    pedidoPagamento.valorDesconto = Convert.ToDecimal(pedido.valorDoDescontoDoPagamento);
                    pedidoPagamento.nomeCartao = "";
                    pedidoPagamento.numeroCartao = "";
                    pedidoPagamento.validadeCartao = "";
                    pedidoPagamento.codSegurancaCartao = "";
                    pedidoPagamento.idContaPagamento = defineOperadorPagamento((int)pedido.condDePagamentoId2);
                    data.tbPedidoPagamento.Add(pedidoPagamento);
                    data.tbPedidoPagamento.Add(pedidoPagamento);
                    data.SaveChanges();
                }
                if (pedido.tipoDePagamentoId1 == 2)
                {
                    string numeroCobranca = "-";
                    var pedidoPagamento = new tbPedidoPagamento();
                    pedidoPagamento.pedidoId = pedido.pedidoId;
                    pedidoPagamento.pagamentoPrincipal = true;
                    pedidoPagamento.valor = Convert.ToDecimal(pedido.valorPagamento1);
                    var dataPedido = pedido.dataHoraDoPedido == null ? DateTime.Now : (DateTime)pedido.dataHoraDoPedido;
                    pedidoPagamento.dataVencimento = dataPedido.AddDays(4);
                    bool pago = pedido.statusDoPedido == 3 | pedido.statusDoPedido == 4 | pedido.statusDoPedido == 5 |
                                pedido.statusDoPedido == 8 | pedido.statusDoPedido == 9 | pedido.statusDoPedido == 10 |
                                pedido.statusDoPedido == 11;
                    pedidoPagamento.dataPagamento = pedido.dataConfirmacaoPagamento;
                    pedidoPagamento.pago = pago;
                    pedidoPagamento.numeroCobranca = numeroCobranca;
                    pedidoPagamento.recebimentoParcelado = true;
                    pedidoPagamento.observacao = "";
                    pedidoPagamento.idOperadorPagamento = defineOperadorPagamento((int)pedido.condDePagamentoId1);
                    pedidoPagamento.consolidado = false;
                    pedidoPagamento.parcelas = (int)pedido.parcelasPagamento1;
                    pedidoPagamento.cancelado = false;
                    pedidoPagamento.condicaoDePagamentoId = (int)pedido.condDePagamentoId1;
                    pedidoPagamento.valorParcela = Convert.ToDecimal(pedido.valorParcelaPagamento1);
                    pedidoPagamento.pagamentoNaoAutorizado = pedido.statusDoPedido == 7;
                    pedidoPagamento.valorTotal = Convert.ToDecimal(pedido.valorPagamento1);
                    pedidoPagamento.valorDesconto = Convert.ToDecimal(0);
                    pedidoPagamento.nomeCartao = pedido.nomeDoCartao;
                    pedidoPagamento.numeroCartao = pedido.numeroDoCartao;
                    pedidoPagamento.validadeCartao = pedido.validadeDoCartao;
                    pedidoPagamento.codSegurancaCartao = pedido.codDeSegurancaDoCartao;
                    pedidoPagamento.idContaPagamento = defineOperadorPagamento((int)pedido.condDePagamentoId1);
                    data.tbPedidoPagamento.Add(pedidoPagamento);
                    data.SaveChanges();
                }
                if (pedido.tipoDePagamentoId2 == 2)
                {
                    string numeroCobranca = "-";
                    var pedidoPagamento = new tbPedidoPagamento();
                    pedidoPagamento.pedidoId = pedido.pedidoId;
                    pedidoPagamento.pagamentoPrincipal = true;
                    pedidoPagamento.valor = Convert.ToDecimal(pedido.valorPagamento2);
                    var dataPedido = pedido.dataHoraDoPedido == null ? DateTime.Now : (DateTime)pedido.dataHoraDoPedido;
                    pedidoPagamento.dataVencimento = dataPedido.AddDays(4);
                    bool pago = pedido.statusDoPedido == 3 | pedido.statusDoPedido == 4 | pedido.statusDoPedido == 5 |
                                pedido.statusDoPedido == 8 | pedido.statusDoPedido == 9 | pedido.statusDoPedido == 10 |
                                pedido.statusDoPedido == 11;
                    pedidoPagamento.dataPagamento = pedido.dataConfirmacaoPagamento;
                    pedidoPagamento.pago = pago;
                    pedidoPagamento.numeroCobranca = numeroCobranca;
                    pedidoPagamento.recebimentoParcelado = true;
                    pedidoPagamento.observacao = "";
                    pedidoPagamento.idOperadorPagamento = defineOperadorPagamento((int)pedido.condDePagamentoId2);
                    pedidoPagamento.consolidado = false;
                    pedidoPagamento.parcelas = (int)pedido.parcelasPagamento2;
                    pedidoPagamento.cancelado = false;
                    pedidoPagamento.condicaoDePagamentoId = (int)pedido.condDePagamentoId2;
                    pedidoPagamento.valorParcela = Convert.ToDecimal(pedido.valorParcelaPagamento2);
                    pedidoPagamento.pagamentoNaoAutorizado = pedido.statusDoPedido == 7;
                    pedidoPagamento.valorTotal = Convert.ToDecimal(pedido.valorPagamento2);
                    pedidoPagamento.valorDesconto = Convert.ToDecimal(0);
                    pedidoPagamento.nomeCartao = pedido.nomeDoCartao2;
                    pedidoPagamento.numeroCartao = pedido.numeroDoCartao2;
                    pedidoPagamento.validadeCartao = pedido.validadeDoCartao2;
                    pedidoPagamento.codSegurancaCartao = pedido.codDeSegurancaDoCartao2;
                    pedidoPagamento.idContaPagamento = defineOperadorPagamento((int)pedido.condDePagamentoId2);
                    data.tbPedidoPagamento.Add(pedidoPagamento);
                    data.SaveChanges();
                }
            }
            else if (pedido.tipoDePagamentoId == 11)
            {
                for (int i = 1; i <= pedido.numeroDeParcelas; i++)
                {
                    int mes = i - 1;
                    string numeroCobranca = mes + (String.Format("{0:0000000}", pedido.pedidoId));
                    var pedidoPagamento = new tbPedidoPagamento();
                    pedidoPagamento.pedidoId = pedido.pedidoId;
                    pedidoPagamento.pagamentoPrincipal = true;
                    if (mes > 0) pedidoPagamento.dataVencimento = pedidoPagamento.dataVencimento.AddMonths(i);
                    var dataPedido = pedido.dataHoraDoPedido == null ? DateTime.Now : (DateTime)pedido.dataHoraDoPedido;
                    pedidoPagamento.dataVencimento = dataPedido.AddMonths(mes);
                    bool pago = pedido.statusDoPedido == 3 | pedido.statusDoPedido == 4 | pedido.statusDoPedido == 5 |
                                pedido.statusDoPedido == 8 | pedido.statusDoPedido == 9 | pedido.statusDoPedido == 10 |
                                pedido.statusDoPedido == 11;
                    pedidoPagamento.dataPagamento = pedido.dataConfirmacaoPagamento;
                    pedidoPagamento.pago = pago;
                    pedidoPagamento.numeroCobranca = numeroCobranca;
                    pedidoPagamento.recebimentoParcelado = false;
                    pedidoPagamento.observacao = "";
                    pedidoPagamento.idOperadorPagamento = defineOperadorPagamento((int)pedido.condDePagamentoId);
                    pedidoPagamento.consolidado = false;
                    pedidoPagamento.parcelas = 1;
                    pedidoPagamento.cancelado = false;
                    pedidoPagamento.condicaoDePagamentoId = 4;
                    pedidoPagamento.valorParcela = Convert.ToDecimal((Convert.ToDecimal(pedido.valorCobrado) / Convert.ToDecimal(pedido.numeroDeParcelas)).ToString("0.00"));
                    pedidoPagamento.pagamentoNaoAutorizado = pedido.statusDoPedido == 7;
                    pedidoPagamento.valorDesconto = Convert.ToDecimal((Convert.ToDecimal(pedido.valorDoDescontoDoPagamento) / Convert.ToDecimal(pedido.numeroDeParcelas)).ToString("0.00"));
                    pedidoPagamento.valor = Convert.ToDecimal(((Convert.ToDecimal(pedido.valorCobrado) / Convert.ToDecimal(pedido.numeroDeParcelas)) - (Convert.ToDecimal(pedido.valorDoDescontoDoPagamento) / Convert.ToDecimal(pedido.numeroDeParcelas))).ToString("0.00"));
                    pedidoPagamento.valorTotal = Convert.ToDecimal((Convert.ToDecimal(pedido.valorCobrado) / Convert.ToDecimal(pedido.numeroDeParcelas)).ToString("0.00"));
                    
                    pedidoPagamento.nomeCartao = "";
                    pedidoPagamento.numeroCartao = "";
                    pedidoPagamento.validadeCartao = "";
                    pedidoPagamento.codSegurancaCartao = "";
                    pedidoPagamento.idContaPagamento = 1;
                    data.tbPedidoPagamento.Add(pedidoPagamento);
                    data.SaveChanges();
                }
            }
            else if (pedido.tipoDePagamentoId == 2)
            {
                string numeroCobranca = "0";
                var pedidoPagamento = new tbPedidoPagamento();
                pedidoPagamento.pedidoId = pedido.pedidoId;
                pedidoPagamento.pagamentoPrincipal = true;
                pedidoPagamento.valor = Convert.ToDecimal(pedido.valorCobrado);
                var dataPedido = pedido.dataHoraDoPedido == null ? DateTime.Now : (DateTime)pedido.dataHoraDoPedido;
                pedidoPagamento.dataVencimento = dataPedido.AddDays(4);
                pedidoPagamento.valor = Convert.ToDecimal(pedido.valorCobrado);
                bool pago = pedido.statusDoPedido == 3 | pedido.statusDoPedido == 4 | pedido.statusDoPedido == 5 |
                            pedido.statusDoPedido == 8 | pedido.statusDoPedido == 9 | pedido.statusDoPedido == 10 |
                            pedido.statusDoPedido == 11;
                pedidoPagamento.dataPagamento = pedido.dataConfirmacaoPagamento;
                pedidoPagamento.pago = pago;
                pedidoPagamento.numeroCobranca = numeroCobranca;
                pedidoPagamento.recebimentoParcelado = false;
                pedidoPagamento.observacao = "";
                pedidoPagamento.cancelado = false;
                pedidoPagamento.idOperadorPagamento = defineOperadorPagamento((int)pedido.condDePagamentoId);
                pedidoPagamento.consolidado = false;
                pedidoPagamento.parcelas = (int)pedido.numeroDeParcelas;
                pedidoPagamento.cancelado = false;
                pedidoPagamento.condicaoDePagamentoId = (int)pedido.condDePagamentoId;
                pedidoPagamento.valorParcela = Convert.ToDecimal(pedido.valorDaParcela);
                pedidoPagamento.pagamentoNaoAutorizado = pedido.statusDoPedido == 7;
                pedidoPagamento.valorDesconto = 0;
                pedidoPagamento.valorTotal = Convert.ToDecimal(pedido.valorCobrado);
                pedidoPagamento.nomeCartao = pedido.nomeDoCartao;
                pedidoPagamento.numeroCartao = pedido.numeroDoCartao.Trim();
                pedidoPagamento.validadeCartao = pedido.validadeDoCartao;
                pedidoPagamento.codSegurancaCartao = pedido.codDeSegurancaDoCartao;
                pedidoPagamento.idContaPagamento = defineOperadorPagamento((int)pedido.condDePagamentoId);
                data.tbPedidoPagamento.Add(pedidoPagamento);
                data.SaveChanges();
            }
            else
            {
                if (pedido.condDePagamentoId != null)
                {
                    string numeroCobranca = "0";
                    var pedidoPagamento = new tbPedidoPagamento();
                    pedidoPagamento.pedidoId = pedido.pedidoId;
                    pedidoPagamento.pagamentoPrincipal = true;
                    pedidoPagamento.valor = Convert.ToDecimal(pedido.valorCobrado);
                    var dataPedido = pedido.dataHoraDoPedido == null ? DateTime.Now : (DateTime) pedido.dataHoraDoPedido;
                    pedidoPagamento.dataVencimento = dataPedido.AddDays(4);
                    pedidoPagamento.valor = Convert.ToDecimal(pedido.valorCobrado);
                    bool pago = pedido.statusDoPedido == 3 | pedido.statusDoPedido == 4 | pedido.statusDoPedido == 5 |
                                pedido.statusDoPedido == 8 | pedido.statusDoPedido == 9 | pedido.statusDoPedido == 10 |
                                pedido.statusDoPedido == 11;
                    pedidoPagamento.dataPagamento = pedido.dataConfirmacaoPagamento;
                    pedidoPagamento.pago = pago;
                    pedidoPagamento.numeroCobranca = numeroCobranca;
                    pedidoPagamento.recebimentoParcelado = false;
                    pedidoPagamento.observacao = "";
                    pedidoPagamento.cancelado = false;
                    pedidoPagamento.idOperadorPagamento = defineOperadorPagamento((int) pedido.condDePagamentoId);
                    pedidoPagamento.consolidado = false;
                    pedidoPagamento.parcelas = (int) pedido.numeroDeParcelas;
                    pedidoPagamento.cancelado = false;
                    pedidoPagamento.condicaoDePagamentoId = (int) pedido.condDePagamentoId;
                    pedidoPagamento.valorParcela = Convert.ToDecimal(pedido.valorDaParcela);
                    pedidoPagamento.pagamentoNaoAutorizado = pedido.statusDoPedido == 7;
                    pedidoPagamento.valorDesconto = Convert.ToDecimal(pedido.valorDoDescontoDoPagamento);
                    pedidoPagamento.valorTotal = Convert.ToDecimal(pedido.valorTotalGeral);
                    pedidoPagamento.nomeCartao = (pedido.nomeDoCartao ?? "").Trim();
                    pedidoPagamento.numeroCartao = (pedido.numeroDoCartao ?? "").Trim();
                    pedidoPagamento.validadeCartao = (pedido.validadeDoCartao ?? "").Trim();
                    pedidoPagamento.codSegurancaCartao = (pedido.codDeSegurancaDoCartao ?? "").Trim();
                    pedidoPagamento.idContaPagamento = defineOperadorPagamento((int)pedido.condDePagamentoId);
                    data.tbPedidoPagamento.Add(pedidoPagamento);
                    data.SaveChanges();
                }
            }

        }
        public static int defineOperadorPagamento(int condicaoDePagamento)
        {
            if (condicaoDePagamento == 3)
            {
                return 4; // Elavon
            }
            else if (condicaoDePagamento == 4)
            {
                return 1; // Boleto HSBC
            }
            else if (condicaoDePagamento == 5)
            {
                return 4; // Elavon
            }
            else if (condicaoDePagamento == 6)
            {
                return 3; // Cielo
            }
            else if (condicaoDePagamento == 7)
            {
                return 3; // Cielo
            }
            else if (condicaoDePagamento == 8)
            {
                return 1; // Cielo
            }
            else if (condicaoDePagamento == 9)
            {
                return 1; // Cielo
            }
            else if (condicaoDePagamento == 10)
            {
                return 3; // Cielo
            }
            else if (condicaoDePagamento == 11)
            {
                return 3; // Cielo
            }
            else if (condicaoDePagamento == 12)
            {
                return 3; // Cielo
            }
            else if (condicaoDePagamento == 13)
            {
                return 4; // Elavon
            }
            else if (condicaoDePagamento == 14)
            {
                return 2; // Moip
            }
            else if (condicaoDePagamento == 15)
            {
                return 2; // Moip
            }
            else if (condicaoDePagamento == 16)
            {
                return 3; // Cielo
            }
            else if (condicaoDePagamento == 17)
            {
                return 3; // Cielo
            }
            else if (condicaoDePagamento == 18)
            {
                return 2; // Moip
            }
            else if (condicaoDePagamento == 19)
            {
                return 2; // Moip
            }
            else if (condicaoDePagamento == 20)
            {
                return 2; // Moip
            }
            else if (condicaoDePagamento == 22)
            {
                return 3; // Cielo
            }
            else if (condicaoDePagamento == 23)
            {
                return 4; // Cielo
            }
            else if (condicaoDePagamento == 24)
            {
                return 6; // Rakuten
            }
            else if (condicaoDePagamento == 25)
            {
                return 7; // Extra
            }
            else if (condicaoDePagamento == 26)
            {
                return 8; // Mercado Livre
            }
            else if (condicaoDePagamento == 27)
            {
                return 1; // Boleto
            }
            else
            {
                return 1; // Boleto
            }
        }


    }
}
