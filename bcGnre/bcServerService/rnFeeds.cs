﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using Ionic.Zip;

namespace bcServerService
{
    public class idUsados
    {
        public string id { get; set; }
        public string fornecedor { get; set; }
    }

    public class rnFeeds
    {
        public static void GerarFeeds()
        {
            try
            {
                var data = new dbSiteEntities();
                var produtos = (from c in data.tbProdutos
                                where c.produtoAtivo == "True" && c.produtoEstoqueAtual > 0
                                select c).ToList();
                produtos = produtos.Distinct().ToList();
                var idsProdutos = produtos.Select(x => x.produtoId).ToList();

                var categoriasProdutos = (from c in data.tbJuncaoProdutoCategoria
                                          where
                                              idsProdutos.Contains(c.produtoId) && c.tbProdutoCategoria.categoriaPaiId == 0 &&
                                              c.tbProdutoCategoria.exibirSite == true
                                          select c).ToList();

                var categorias = (from c in data.tbProdutoCategoria select c).ToList();

                var condicoesPagamento = (from c in data.tbCondicoesDePagamento where c.ativo.ToLower() == "true" select c).ToList();

                var marcas = (from c in data.tbMarca select c).ToList();

                GerarListasFacebook();
                GerarBlogKit(produtos, categoriasProdutos, categorias, condicoesPagamento, marcas, "blogkitberco.xml", 413, "http://www.kitberco.com.br/");
                GerarBlogKit(produtos, categoriasProdutos, categorias, condicoesPagamento, marcas, "blogberco.xml", 1001, "http://www.berco.com.br/");
                GerarBlogKit(produtos, categoriasProdutos, categorias, condicoesPagamento, marcas, "blogquarto.xml", 412, "http://www.quartoparabebe.com.br/");
                GerarRichBanners(produtos, categoriasProdutos, categorias, condicoesPagamento, marcas);
                GerarMerchant(produtos, categoriasProdutos, categorias, condicoesPagamento, marcas);
                GerarMerchantPrice(produtos, categoriasProdutos, categorias, condicoesPagamento, marcas);
                GerarMerchantDra(produtos, categoriasProdutos, categorias, condicoesPagamento, marcas);
                GerarSitemap(produtos, categoriasProdutos, categorias, condicoesPagamento, marcas);                
                GerarMerchantAllIn(produtos, categoriasProdutos, categorias, condicoesPagamento, marcas);
                GerarChaordic(produtos, categoriasProdutos, categorias, condicoesPagamento, marcas);
                GerarCriteo(produtos, categoriasProdutos, categorias, condicoesPagamento, marcas);
                GerarRtbHouse(produtos, categoriasProdutos, categorias, condicoesPagamento, marcas);
                GerarJacotei(produtos, categoriasProdutos, categorias, condicoesPagamento, marcas);
                GerarLiveTarget(produtos, categoriasProdutos, categorias, condicoesPagamento, marcas);
                GerarMucca(produtos, categoriasProdutos, categorias, condicoesPagamento, marcas);
                GerarRoiHunter(produtos, categoriasProdutos, categorias, condicoesPagamento, marcas);
                //GerarTargetingMantra(produtos, categoriasProdutos, categorias, condicoesPagamento, marcas);
                GerarRich(produtos, categoriasProdutos, categorias, condicoesPagamento, marcas);
            }
            catch (Exception ex)
            {

                rnEmails.EnviaEmail("", "andre@bark.com.br", "", "", "", ex.ToString(), "Erro Feeds");
            }
        }

        private static void GerarListasFacebook()
        {
            var meses6 = DateTime.Now.AddMonths(-6);
            var meses3 = DateTime.Now.AddMonths(-3);
            var meses1 = DateTime.Now.AddMonths(-1);
            var dias15 = DateTime.Now.AddDays(-15);

            var data = new dbSiteEntities();
            var ComprouENaoPagouGeral = (from c in data.tbPedidos
                                    join d in data.tbPedidos on c.clienteId equals d.clienteId into pedidosCliente
                                    join e in data.tbClientes on c.clienteId equals e.clienteId
                                    where (c.statusDoPedido == 6 | c.statusDoPedido == 7) &&
                                          pedidosCliente.Count(
                                              x =>
                                                  (x.statusDoPedido == 2 | x.statusDoPedido == 3 | x.statusDoPedido == 4 | x.statusDoPedido == 5 |
                                                   x.statusDoPedido == 11) && x.pedidoId != c.pedidoId) == 0 &&
                                          c.dataHoraDoPedido > meses6
                                    select new {
                                        e.clienteEmail,
                                        e.clienteFoneCelular,
                                        e.clienteFoneComercial,
                                        e.clienteFoneResidencial,
                                        e.telRecado,
                                        c.dataHoraDoPedido
                                    }).ToList();

            FileInfo fbEmailQuebra15d = new FileInfo(ConfigurationManager.AppSettings["caminhoFisico"].ToString() + "fbEmailQuebra15d_create.txt");
            if (fbEmailQuebra15d.Exists)
            {
                fbEmailQuebra15d.Delete();
            }
            StreamWriter swfbEmailQuebra15d = fbEmailQuebra15d.CreateText();
            FileInfo fbTelefoneQuebra15d = new FileInfo(ConfigurationManager.AppSettings["caminhoFisico"].ToString() + "fbTelefoneQuebra15d_create.txt");
            if (fbTelefoneQuebra15d.Exists)
            {
                fbTelefoneQuebra15d.Delete();
            }
            StreamWriter swfbTelefoneQuebra15d = fbTelefoneQuebra15d.CreateText();
            var clientes15d = ComprouENaoPagouGeral.Where(x => x.dataHoraDoPedido >= dias15);
            foreach(var cliente15d in clientes15d)
            {
                swfbEmailQuebra15d.WriteLine(rnFuncoes.sha256(cliente15d.clienteEmail));
                if(!string.IsNullOrEmpty(cliente15d.clienteFoneCelular)) swfbTelefoneQuebra15d.WriteLine(rnFuncoes.sha256(cliente15d.clienteFoneCelular.Replace(" ", "").Replace("(", "").Replace(")", "").Replace("-", "").Replace(".", "")));
                if (!string.IsNullOrEmpty(cliente15d.clienteFoneComercial)) swfbTelefoneQuebra15d.WriteLine(rnFuncoes.sha256(cliente15d.clienteFoneComercial.Replace(" ", "").Replace("(", "").Replace(")", "").Replace("-", "").Replace(".", "")));
                if (!string.IsNullOrEmpty(cliente15d.clienteFoneResidencial)) swfbTelefoneQuebra15d.WriteLine(rnFuncoes.sha256(cliente15d.clienteFoneResidencial.Replace(" ", "").Replace("(", "").Replace(")", "").Replace("-", "").Replace(".", "")));

            }
            swfbEmailQuebra15d.WriteLine();
            swfbEmailQuebra15d.Close();
            swfbTelefoneQuebra15d.WriteLine();
            swfbTelefoneQuebra15d.Close();
            File.Copy(ConfigurationManager.AppSettings["caminhoFisico"].ToString() + "fbEmailQuebra15d_create.txt", rnConfiguracoes.caminhoXmls + "fbEmailQuebra15d.txt", true);
            File.Copy(ConfigurationManager.AppSettings["caminhoFisico"].ToString() + "fbTelefoneQuebra15d_create.txt", rnConfiguracoes.caminhoXmls + "fbTelefoneQuebra15d.txt", true);


            FileInfo fbEmailQuebra1m = new FileInfo(ConfigurationManager.AppSettings["caminhoFisico"].ToString() + "fbEmailQuebra1m_create.txt");
            if (fbEmailQuebra1m.Exists)
            {
                fbEmailQuebra1m.Delete();
            }
            StreamWriter swfbEmailQuebra1m = fbEmailQuebra1m.CreateText();
            FileInfo fbTelefoneQuebra1m = new FileInfo(ConfigurationManager.AppSettings["caminhoFisico"].ToString() + "fbTelefoneQuebra1m_create.txt");
            if (fbTelefoneQuebra1m.Exists)
            {
                fbTelefoneQuebra1m.Delete();
            }
            StreamWriter swfbTelefoneQuebra1m = fbTelefoneQuebra1m.CreateText();
            var clientes1m = ComprouENaoPagouGeral.Where(x => x.dataHoraDoPedido < dias15 && x.dataHoraDoPedido >= meses1);
            foreach (var cliente1m in clientes1m)
            {
                swfbEmailQuebra1m.WriteLine(rnFuncoes.sha256(cliente1m.clienteEmail));
                if (!string.IsNullOrEmpty(cliente1m.clienteFoneCelular)) swfbTelefoneQuebra1m.WriteLine(rnFuncoes.sha256(cliente1m.clienteFoneCelular.Replace(" ", "").Replace("(", "").Replace(")", "").Replace("-", "").Replace(".", "")));
                if (!string.IsNullOrEmpty(cliente1m.clienteFoneComercial)) swfbTelefoneQuebra1m.WriteLine(rnFuncoes.sha256(cliente1m.clienteFoneComercial.Replace(" ", "").Replace("(", "").Replace(")", "").Replace("-", "").Replace(".", "")));
                if (!string.IsNullOrEmpty(cliente1m.clienteFoneResidencial)) swfbTelefoneQuebra1m.WriteLine(rnFuncoes.sha256(cliente1m.clienteFoneResidencial.Replace(" ", "").Replace("(", "").Replace(")", "").Replace("-", "").Replace(".", "")));

            }
            swfbEmailQuebra1m.WriteLine();
            swfbEmailQuebra1m.Close();
            swfbTelefoneQuebra1m.WriteLine();
            swfbTelefoneQuebra1m.Close();
            File.Copy(ConfigurationManager.AppSettings["caminhoFisico"].ToString() + "fbEmailQuebra1m_create.txt", rnConfiguracoes.caminhoXmls + "fbEmailQuebra1m.txt", true);
            File.Copy(ConfigurationManager.AppSettings["caminhoFisico"].ToString() + "fbTelefoneQuebra1m_create.txt", rnConfiguracoes.caminhoXmls + "fbTelefoneQuebra1m.txt", true);


            FileInfo fbEmailQuebra3m = new FileInfo(ConfigurationManager.AppSettings["caminhoFisico"].ToString() + "fbEmailQuebra3m_create.txt");
            if (fbEmailQuebra3m.Exists)
            {
                fbEmailQuebra3m.Delete();
            }
            StreamWriter swfbEmailQuebra3m = fbEmailQuebra3m.CreateText();
            FileInfo fbTelefoneQuebra3m = new FileInfo(ConfigurationManager.AppSettings["caminhoFisico"].ToString() + "fbTelefoneQuebra3m_create.txt");
            if (fbTelefoneQuebra3m.Exists)
            {
                fbTelefoneQuebra3m.Delete();
            }
            StreamWriter swfbTelefoneQuebra3m = fbTelefoneQuebra3m.CreateText();
            var clientes3m = ComprouENaoPagouGeral.Where(x => x.dataHoraDoPedido < meses1 && x.dataHoraDoPedido >= meses3);
            foreach (var cliente3m in clientes3m)
            {
                swfbEmailQuebra3m.WriteLine(rnFuncoes.sha256(cliente3m.clienteEmail));
                if (!string.IsNullOrEmpty(cliente3m.clienteFoneCelular)) swfbTelefoneQuebra3m.WriteLine(rnFuncoes.sha256(cliente3m.clienteFoneCelular.Replace(" ", "").Replace("(", "").Replace(")", "").Replace("-", "").Replace(".", "")));
                if (!string.IsNullOrEmpty(cliente3m.clienteFoneComercial)) swfbTelefoneQuebra3m.WriteLine(rnFuncoes.sha256(cliente3m.clienteFoneComercial.Replace(" ", "").Replace("(", "").Replace(")", "").Replace("-", "").Replace(".", "")));
                if (!string.IsNullOrEmpty(cliente3m.clienteFoneResidencial)) swfbTelefoneQuebra3m.WriteLine(rnFuncoes.sha256(cliente3m.clienteFoneResidencial.Replace(" ", "").Replace("(", "").Replace(")", "").Replace("-", "").Replace(".", "")));

            }
            swfbEmailQuebra3m.WriteLine();
            swfbEmailQuebra3m.Close();
            swfbTelefoneQuebra3m.WriteLine();
            swfbTelefoneQuebra3m.Close();
            File.Copy(ConfigurationManager.AppSettings["caminhoFisico"].ToString() + "fbEmailQuebra3m_create.txt", rnConfiguracoes.caminhoXmls + "fbEmailQuebra3m.txt", true);
            File.Copy(ConfigurationManager.AppSettings["caminhoFisico"].ToString() + "fbTelefoneQuebra3m_create.txt", rnConfiguracoes.caminhoXmls + "fbTelefoneQuebra3m.txt", true);


            FileInfo fbEmailQuebra6m = new FileInfo(ConfigurationManager.AppSettings["caminhoFisico"].ToString() + "fbEmailQuebra6m_create.txt");
            if (fbEmailQuebra6m.Exists)
            {
                fbEmailQuebra6m.Delete();
            }
            StreamWriter swfbEmailQuebra6m = fbEmailQuebra6m.CreateText();
            FileInfo fbTelefoneQuebra6m = new FileInfo(ConfigurationManager.AppSettings["caminhoFisico"].ToString() + "fbTelefoneQuebra6m_create.txt");
            if (fbTelefoneQuebra6m.Exists)
            {
                fbTelefoneQuebra6m.Delete();
            }
            StreamWriter swfbTelefoneQuebra6m = fbTelefoneQuebra6m.CreateText();
            var clientes6m = ComprouENaoPagouGeral.Where(x => x.dataHoraDoPedido < meses3 && x.dataHoraDoPedido >= meses6);
            foreach (var cliente6m in clientes6m)
            {
                swfbEmailQuebra6m.WriteLine(rnFuncoes.sha256(cliente6m.clienteEmail));
                if (!string.IsNullOrEmpty(cliente6m.clienteFoneCelular)) swfbTelefoneQuebra6m.WriteLine(rnFuncoes.sha256(cliente6m.clienteFoneCelular.Replace(" ", "").Replace("(", "").Replace(")", "").Replace("-", "").Replace(".", "")));
                if (!string.IsNullOrEmpty(cliente6m.clienteFoneComercial)) swfbTelefoneQuebra6m.WriteLine(rnFuncoes.sha256(cliente6m.clienteFoneComercial.Replace(" ", "").Replace("(", "").Replace(")", "").Replace("-", "").Replace(".", "")));
                if (!string.IsNullOrEmpty(cliente6m.clienteFoneResidencial)) swfbTelefoneQuebra6m.WriteLine(rnFuncoes.sha256(cliente6m.clienteFoneResidencial.Replace(" ", "").Replace("(", "").Replace(")", "").Replace("-", "").Replace(".", "")));
            }
            swfbEmailQuebra6m.WriteLine();
            swfbEmailQuebra6m.Close();
            swfbTelefoneQuebra6m.WriteLine();
            swfbTelefoneQuebra6m.Close();
            File.Copy(ConfigurationManager.AppSettings["caminhoFisico"].ToString() + "fbEmailQuebra6m_create.txt", rnConfiguracoes.caminhoXmls + "fbEmailQuebra6m.txt", true);
            File.Copy(ConfigurationManager.AppSettings["caminhoFisico"].ToString() + "fbTelefoneQuebra6m_create.txt", rnConfiguracoes.caminhoXmls + "fbTelefoneQuebra6m.txt", true);



        }

        public static void GerarBlogKit(List<tbProdutos> produtos, List<tbJuncaoProdutoCategoria> categoriasProdutos, List<tbProdutoCategoria> categorias, List<tbCondicoesDePagamento> condicoesPagamento, List<tbMarca> marcas, string arquivo, int idCategoria, string urlBlog)
        {
            FileInfo file = new FileInfo(ConfigurationManager.AppSettings["caminhoFisico"].ToString() + arquivo);

            if (file.Exists)
            {
                file.Delete();
            }
            StreamWriter sw = file.CreateText();

            foreach (var produto in produtos)
            {
                var categoria = (from c in categoriasProdutos
                                 where c.produtoId == produto.produtoId
                                 && (c.categoriaId == idCategoria)
                                 select c).FirstOrDefault();

                var categoriaNome = "Decoração de Quarto de Bebê";
                if (categoria != null)
                {
                    categoriaNome = categorias.First(x => x.categoriaId == categoria.categoriaId).categoriaNomeExibicao;

                    categoriaNome = categoriaNome.Replace("&", "e");


                    #region parcelamento


                    var produtoPrecoUtilizar = (produto.produtoPrecoPromocional > 0 &&
                                      produto.produtoPrecoPromocional < produto.produtoPreco
                        ? (decimal)produto.produtoPrecoPromocional
                        : produto.produtoPreco);


                    var condicao = condicoesPagamento.First(x => x.destaque.ToLower() == "true");
                    int parcelasSemJuros = condicao.parcelasSemJuros;
                    double taxaDeJuros = condicao.taxaDeJuros / 100;
                    decimal valorMinimoDaParcela = condicao.valorMinimoDaParcela;
                    decimal porcentagemDeDesconto = Convert.ToDecimal(condicao.porcentagemDeDesconto);
                    int numeroMaximoDeParcelas = condicao.numeroMaximoDeParcelas;
                    decimal valorDaParcela = 0;
                    int parcelasMaximas = 0;

                    for (int i = numeroMaximoDeParcelas; i <= numeroMaximoDeParcelas && i > 0; i--)
                    {
                        valorDaParcela = produtoPrecoUtilizar / i;

                        if (valorDaParcela >= valorMinimoDaParcela)
                        {
                            if (i > 1)
                            {
                                if (i <= parcelasSemJuros)
                                {
                                    parcelasMaximas = i;
                                }
                                break;
                            }
                        }
                    }

                    #endregion

                    var valorVenda = (produto.produtoPrecoPromocional > 0 &&
                                      produto.produtoPrecoPromocional < produto.produtoPreco
                        ? (decimal)produto.produtoPrecoPromocional
                        : produto.produtoPreco);
                    var valorDesconto = (valorVenda - ((valorVenda / 100) * 15));


                    var data = new dbSiteEntities();
                    var informacoesAdicionais = (from c in data.tbInformacaoAdcional where c.produtoId == produto.produtoId select c).ToList();


                    string htmlDescricaoPlugin = "";
                    string htmlDescricao = "";
                    string metaKey = "";
                    if(idCategoria == 413) //Kit Berco
                    {
                        metaKey += "<p>Além de garantir mais conforto para o seu bebê, o <a href=\"" + rnConfiguracoes.caminhoVirtual + categorias.First(x => x.categoriaId == categoria.categoriaId).categoriaUrl + "/" + produto.produtoUrl + "\" title=\"" + produto.produtoNome + "\">\"" + produto.produtoNome + "\"</a> deixará a decoração do quarto do seu bebê mais elegante. Confira todos os diferencias desse modelo!</p><br><br>";
                        htmlDescricao += metaKey;
                        
                        htmlDescricao += "<img src = \"" + "https://s3-sa-east-1.amazonaws.com/cdn2.graodegente.com.br/fotos/" + produto.produtoId + "/" + produto.fotoDestaque + ".jpg" + "\" alt = \"" + produto.produtoNome + "\" title=\"" + produto.produtoNome + "\" ><br>";
                    }
                    if (idCategoria == 412) // Quarto
                    {
                        metaKey += "<p>O combo quarto de bebê <a href=\"" + rnConfiguracoes.caminhoVirtual + categorias.First(x => x.categoriaId == categoria.categoriaId).categoriaUrl + "/" + produto.produtoUrl + "\" title=\"" + produto.produtoNome + "\">\"" + produto.produtoNome + "\"</a> é a solução perfeita para decoração do quarto do seu bebê. Confira todos os detalhes aqui!</p><br><br>";
                        htmlDescricao += metaKey;

                        htmlDescricao += "<img src = \"" + "https://s3-sa-east-1.amazonaws.com/cdn2.graodegente.com.br/fotos/" + produto.produtoId + "/" + produto.fotoDestaque + ".jpg" + "\" alt = \"" + produto.produtoNome + "\" title=\"" + produto.produtoNome + "\" ><br>";
                    }
                    if (idCategoria == 1001) // Berço
                    {
                        metaKey += "<p>Garanta a segurança e o conforto do seu bebê com o <a href=\"" + rnConfiguracoes.caminhoVirtual + categorias.First(x => x.categoriaId == categoria.categoriaId).categoriaUrl + "/" + produto.produtoUrl + "\" title=\"" + produto.produtoNome + "\">\"" + produto.produtoNome + "\"</a>. Esse e muitos outros modelos estão disponíveis no site da Grão de Gente.</p><br><br>";
                        htmlDescricao += metaKey;
                        htmlDescricao += "<img src = \"" + "https://s3-sa-east-1.amazonaws.com/cdn2.graodegente.com.br/fotos/" + produto.produtoId + "/" + produto.fotoDestaque + ".jpg" + "\" alt = \"" + produto.produtoNome + "\" title=\"" + produto.produtoNome + "\" ><br>";
                    }
                    htmlDescricao += "<div copytext=\"\" id=\"descricaoCompleta\" class=\"conteudo Descrição\">";

                    int informacoes = 0;
                    foreach (var informacao in informacoesAdicionais)
                    {
                        informacoes++;
                        htmlDescricao += "<p class=\"title\">" + informacao.informacaoAdcionalNome + "</p>";
                        htmlDescricao += informacao.informacaoAdcionalConteudo + "<br>";

                        if (informacoes == 1)
                        {
                            htmlDescricaoPlugin += "<p class=\"title\">" + informacao.informacaoAdcionalNome + "</p>";
                            htmlDescricaoPlugin += informacao.informacaoAdcionalConteudo + "<br>";
                        }
                    }
                    htmlDescricao += "</div><br><br><br>";

                    string url = urlBlog;


                    sw.WriteLine("<item>");
                    sw.WriteLine("<title><![CDATA[" + produto.produtoNome + "]]></title>");
                    sw.WriteLine("<link>" + url + "produto/" + produto.produtoUrl + "</link>");
                    sw.WriteLine("<pubDate>thu, 12 May 2016 19:44:46 +0000</pubDate>");
                    sw.WriteLine("<dc:creator><![CDATA[redacao]]></dc:creator>");
                    sw.WriteLine("<guid isPermaLink=\"false\">" + url + "/?p=999999" + produto.produtoId + "</guid>");
                    sw.WriteLine("<description></description>");
                    sw.WriteLine("<content:encoded><![CDATA[" + htmlDescricao + "]]></content:encoded>");
                    sw.WriteLine("<excerpt:encoded><![CDATA[]]></excerpt:encoded>");
                    sw.WriteLine("<wp:post_id>999999" + produto.produtoId + "</wp:post_id>");
                    sw.WriteLine("sw.WriteLine(<wp:post_date >< ![CDATA[2016 - 05 - 12 19:44:46]] ></ wp:post_date>");
                
                    sw.WriteLine("<wp:post_date_gmt><![CDATA[2016-05-12 19:44:46]]></wp:post_date_gmt>");
                    sw.WriteLine("<wp:comment_status><![CDATA[open]]></wp:comment_status>");
                    sw.WriteLine("<wp:ping_status><![CDATA[open]]></wp:ping_status>");
                    sw.WriteLine("<wp:post_name><![CDATA[" + produto.produtoUrl  + "]]></wp:post_name>");
                    sw.WriteLine("<wp:status><![CDATA[publish]]></wp:status>");
                    sw.WriteLine("<wp:post_parent>0</wp:post_parent>");
                    sw.WriteLine("<wp:menu_order>0</wp:menu_order>");
                    sw.WriteLine("<wp:post_type><![CDATA[post]]></wp:post_type>");
                    sw.WriteLine("<wp:post_password><![CDATA[]]></wp:post_password>");
                    sw.WriteLine("<wp:is_sticky>0</wp:is_sticky>");
                    sw.WriteLine("<category domain=\"category\" nicename=\"produto\"><![CDATA[Produtos]]></category>");
                    sw.WriteLine("<wp:postmeta>");
                    sw.WriteLine("<wp:meta_key><![CDATA[_vc_post_settings]]></wp:meta_key>");
                    sw.WriteLine("<wp:meta_value><![CDATA[a:1:{s:10:\"vc_grid_id\";a:0:{}}]]></wp:meta_value>");
                    sw.WriteLine("</wp:postmeta>");
                    sw.WriteLine("<wp:postmeta>");
                    sw.WriteLine("<wp:meta_key><![CDATA[_vc_post_settings]]></wp:meta_key>");
                    sw.WriteLine("<wp:meta_value><![CDATA[a:1:{s:10:\"vc_grid_id\";a:0:{}}]]></wp:meta_value>");
                    sw.WriteLine("</wp:postmeta>");
                    sw.WriteLine("<wp:postmeta>");
                    sw.WriteLine("<wp:meta_key><![CDATA[_edit_last]]></wp:meta_key>");
                    sw.WriteLine("<wp:meta_value><![CDATA[]]></wp:meta_value>");
                    sw.WriteLine("</wp:postmeta>");
                    sw.WriteLine("<wp:postmeta>");
                    sw.WriteLine("<wp:meta_key><![CDATA[slide_template]]></wp:meta_key>");
                    sw.WriteLine("<wp:meta_value><![CDATA[default]]></wp:meta_value>");
                    sw.WriteLine("</wp:postmeta>");
                    sw.WriteLine("<wp:postmeta>");
                    sw.WriteLine("<wp:meta_key><![CDATA[wpo_postconfig]]></wp:meta_key>");
                    sw.WriteLine("<wp:meta_value><![CDATA[]]></wp:meta_value>");
                    sw.WriteLine("</wp:postmeta>");
                    sw.WriteLine("<wp:postmeta>");
                    sw.WriteLine("<wp:meta_key><![CDATA[ratings_users]]></wp:meta_key>");
                    sw.WriteLine("<wp:meta_value><![CDATA[0]]></wp:meta_value>");
                    sw.WriteLine("</wp:postmeta>");
                    sw.WriteLine("<wp:postmeta>");
                    sw.WriteLine("<wp:meta_key><![CDATA[ratings_score]]></wp:meta_key>");
                    sw.WriteLine("<wp:meta_value><![CDATA[0]]></wp:meta_value>");
                    sw.WriteLine("</wp:postmeta>");
                    sw.WriteLine("<wp:postmeta>");
                    sw.WriteLine("<wp:meta_key><![CDATA[ratings_average]]></wp:meta_key>");
                    sw.WriteLine("<wp:meta_value><![CDATA[0]]></wp:meta_value>");
                    sw.WriteLine("</wp:postmeta>");
                    sw.WriteLine("<wp:postmeta>");
                    sw.WriteLine("<wp:meta_key><![CDATA[_thumbnail_id]]></wp:meta_key>");
                    sw.WriteLine("<wp:meta_value><![CDATA[]]></wp:meta_value>");
                    sw.WriteLine("</wp:postmeta>");
                    sw.WriteLine("<wp:postmeta>");
                    sw.WriteLine("<wp:meta_key><![CDATA[_yoast_wpseo_primary_category]]></wp:meta_key>");
                    sw.WriteLine("<wp:meta_value><![CDATA[]]></wp:meta_value>");
                    sw.WriteLine("</wp:postmeta>");
                    sw.WriteLine("<wp:postmeta>");
                    sw.WriteLine("<wp:meta_key><![CDATA[count_post_views]]></wp:meta_key>");
                    sw.WriteLine("<wp:meta_value><![CDATA[]]></wp:meta_value>");
                    sw.WriteLine("</wp:postmeta>");
                    sw.WriteLine("<wp:postmeta>");
                    sw.WriteLine("<wp:meta_key><![CDATA[mkd_boxed_background_image_attachment_meta]]></wp:meta_key>");
                    sw.WriteLine("<wp:meta_value><![CDATA[fixed]]></wp:meta_value>");
                    sw.WriteLine("</wp:postmeta>");
                    sw.WriteLine("<wp:postmeta>");
                    sw.WriteLine("<wp:meta_key><![CDATA[mkd_show_featured_post]]></wp:meta_key>");
                    sw.WriteLine("<wp:meta_value><![CDATA[no]]></wp:meta_value>");
                    sw.WriteLine("</wp:postmeta>");
                    sw.WriteLine("<wp:postmeta>");
                    sw.WriteLine("<wp:meta_key><![CDATA[mkd_video_type_meta]]></wp:meta_key>");
                    sw.WriteLine("<wp:meta_value><![CDATA[youtube]]></wp:meta_value>");
                    sw.WriteLine("</wp:postmeta>");
                    sw.WriteLine("<wp:postmeta>");
                    sw.WriteLine("<wp:meta_key><![CDATA[mkd_hide_background_image_meta]]></wp:meta_key>");
                    sw.WriteLine("<wp:meta_value><![CDATA[no]]></wp:meta_value>");
                    sw.WriteLine("</wp:postmeta>");
                    sw.WriteLine("<wp:postmeta>");
                    sw.WriteLine("<wp:meta_key><![CDATA[link_do_produto]]></wp:meta_key>");
                    sw.WriteLine("<wp:meta_value><![CDATA[]]></wp:meta_value>");
                    sw.WriteLine("</wp:postmeta>");
                    sw.WriteLine("<wp:postmeta>");
                    sw.WriteLine("<wp:meta_key><![CDATA[_link_do_produto]]></wp:meta_key>");
                    sw.WriteLine("<wp:meta_value><![CDATA[]]></wp:meta_value>");
                    sw.WriteLine("</wp:postmeta>");
                    sw.WriteLine("<wp:postmeta>");
                    sw.WriteLine("<wp:meta_key><![CDATA[codigo_anuncio]]></wp:meta_key>");
                    sw.WriteLine("<wp:meta_value><![CDATA[]]></wp:meta_value>");
                    sw.WriteLine("</wp:postmeta>");
                    sw.WriteLine("<wp:postmeta>");
                    sw.WriteLine("<wp:meta_key><![CDATA[_codigo_anuncio]]></wp:meta_key>");
                    sw.WriteLine("<wp:meta_value><![CDATA[]]></wp:meta_value>");
                    sw.WriteLine("</wp:postmeta>");
                    sw.WriteLine("<wp:postmeta>");
                    sw.WriteLine("<wp:meta_key><![CDATA[_yoast_wpseo_focuskw_text_input]]></wp:meta_key>");
                    sw.WriteLine("<wp:meta_value><![CDATA[" + produto.produtoNome + "]]></wp:meta_value>");
                    sw.WriteLine("</wp:postmeta>");
                    sw.WriteLine("<wp:postmeta>");
                    sw.WriteLine("<wp:meta_key><![CDATA[_yoast_wpseo_focuskw]]></wp:meta_key>");
                    sw.WriteLine("<wp:meta_value><![CDATA[" + produto.produtoNome + "]]></wp:meta_value>");
                    sw.WriteLine("</wp:postmeta>");
                    sw.WriteLine("<wp:postmeta>");
                    sw.WriteLine("<wp:meta_key><![CDATA[_yoast_wpseo_title]]></wp:meta_key>");
                    sw.WriteLine("<wp:meta_value><![CDATA[" + produto.produtoNome + "]]></wp:meta_value>");
                    sw.WriteLine("</wp:postmeta>");
                    sw.WriteLine("<wp:postmeta>");
                    sw.WriteLine("<wp:meta_key><![CDATA[_yoast_wpseo_metadesc]]></wp:meta_key>");
                    sw.WriteLine("<wp:meta_value><![CDATA[" + metaKey + "]]></wp:meta_value>");
                    sw.WriteLine("</wp:postmeta>");
                    sw.WriteLine("<wp:postmeta>");
                    sw.WriteLine("<wp:meta_key><![CDATA[_yoast_wpseo_linkdex]]></wp:meta_key>");
                    sw.WriteLine("<wp:meta_value><![CDATA[999999" + produto.produtoId + "]]></wp:meta_value>");
                    sw.WriteLine("</wp:postmeta>");
                    sw.WriteLine("</item>");


                    
                }

            }
            
            sw.WriteLine();
            sw.Close();
            File.Copy(ConfigurationManager.AppSettings["caminhoFisico"].ToString() + arquivo, rnConfiguracoes.caminhoXmls + arquivo, true);
        }


        public static void GerarMerchantAllIn(List<tbProdutos> produtos, List<tbJuncaoProdutoCategoria> categoriasProdutos, List<tbProdutoCategoria> categorias, List<tbCondicoesDePagamento> condicoesPagamento, List<tbMarca> marcas)
        {
            FileInfo file = new FileInfo(ConfigurationManager.AppSettings["caminhoFisico"].ToString() + "allin_create.xml");

            if (file.Exists)
            {
                file.Delete();
            }
            StreamWriter sw = file.CreateText();
            sw.WriteLine("<?xml version=\"1.0\"?>");
            sw.WriteLine("<rss version=\"2.0\" xmlns:g=\"http://base.google.com/ns/1.0\">");
            sw.WriteLine("<channel>");
            sw.WriteLine("<title>Produtos</title>");
            sw.WriteLine("<link>http://www.graodegente.com.br</link>");
            sw.WriteLine("<description>Lista de Produtos do Site</description>");


            var ids = new List<idUsados>();

            foreach (var produto in produtos)
            {
                var idFornecedors = produto.produtoIdDaEmpresa.Split('.');
                string idFornecedor = idFornecedors.Count() > 1 ? idFornecedors[1] : produto.produtoIdDaEmpresa;
                var random = new Random();
                if (string.IsNullOrEmpty(idFornecedor))
                {
                    idFornecedor = random.Next(1, 5000).ToString();
                }
                var usado = true;
                while (usado)
                {
                    var idUsado = (from c in ids where c.id == idFornecedor && c.fornecedor == marcas.First(x => x.marcaId == produto.produtoMarca).marcaNome select c).Count();
                    if (idUsado == 0)
                    {
                        usado = false;
                    }
                    else
                    {
                        idFornecedor = random.Next(1, 5000).ToString();
                    }
                }
                var categoria = (from c in categoriasProdutos
                                 where
                                     c.produtoId == produto.produtoId
                                 select c).FirstOrDefault();

                var categoriaNome = "Decoração de Quarto de Bebê";
                if (categoria != null)
                {
                    categoriaNome = categorias.First(x => x.categoriaId == categoria.categoriaId).categoriaNomeExibicao;

                    categoriaNome = categoriaNome.Replace("&", "e");


                    #region parcelamento


                    var produtoPrecoUtilizar = (produto.produtoPrecoPromocional > 0 &&
                                      produto.produtoPrecoPromocional < produto.produtoPreco
                        ? (decimal)produto.produtoPrecoPromocional
                        : produto.produtoPreco);


                    var condicao = condicoesPagamento.First(x => x.destaque.ToLower() == "true");
                    int parcelasSemJuros = condicao.parcelasSemJuros;
                    double taxaDeJuros = condicao.taxaDeJuros / 100;
                    decimal valorMinimoDaParcela = condicao.valorMinimoDaParcela;
                    decimal porcentagemDeDesconto = Convert.ToDecimal(condicao.porcentagemDeDesconto);
                    int numeroMaximoDeParcelas = condicao.numeroMaximoDeParcelas;
                    decimal valorDaParcela = 0;
                    int parcelasMaximas = 0;

                    for (int i = numeroMaximoDeParcelas; i <= numeroMaximoDeParcelas && i > 0; i--)
                    {
                        valorDaParcela = produtoPrecoUtilizar / i;

                        if (valorDaParcela >= valorMinimoDaParcela)
                        {
                            if (i > 1)
                            {
                                if (i <= parcelasSemJuros)
                                {
                                    parcelasMaximas = i;
                                }
                                break;
                            }
                        }
                    }

                    #endregion

                    var valorVenda = (produto.produtoPrecoPromocional > 0 &&
                                      produto.produtoPrecoPromocional < produto.produtoPreco
                        ? (decimal)produto.produtoPrecoPromocional
                        : produto.produtoPreco);
                    var valorDesconto = (valorVenda - ((valorVenda / 100) * 15));

                    sw.WriteLine("<item>");
                    sw.WriteLine("<title><![CDATA[" + produto.produtoNome + "]]></title>");
                    sw.WriteLine("<link><![CDATA[" + rnConfiguracoes.caminhoVirtual +
                                 categorias.First(x => x.categoriaId == categoria.categoriaId).categoriaUrl + "/" + produto.produtoUrl +
                                 "/]]></link>");
                    sw.WriteLine("<description><![CDATA[" + produto.produtoNome + "]]></description>");
                    sw.WriteLine("<g:product_type><![CDATA[" + categoriaNome + "]]></g:product_type>");
                    sw.WriteLine("<g:image_link><![CDATA[" + rnConfiguracoes.caminhoCDN + "fotos/" + produto.produtoId + "/" + produto.fotoDestaque + ".jpg" + "]]></g:image_link>");
                    sw.WriteLine("<g:price>" + valorVenda.ToString("0.00").Replace(",", ".") + "</g:price>");
                    sw.WriteLine("<g:sale_price>" + valorDesconto.ToString("0.00").Replace(",", ".") + "</g:sale_price>");
                    sw.WriteLine("<g:id>" + produto.produtoId + "</g:id>");
                    if (parcelasMaximas > 1)
                    {
                        sw.WriteLine("<parcelas>" + parcelasMaximas + "</parcelas>");
                        sw.WriteLine("<valor_parcela><![CDATA[" + valorDaParcela.ToString("C") + "]]></valor_parcela>");
                    }
                    else
                    {
                        sw.WriteLine("<parcelas>1</parcelas>");
                        sw.WriteLine("<valor_parcela><![CDATA[" + valorVenda.ToString("C") + "]]></valor_parcela>");
                    }
                    sw.WriteLine("</item>");


                    var idusado = new idUsados();
                    idusado.id = idFornecedor;
                    idusado.fornecedor = marcas.First(x => x.marcaId == produto.produtoMarca).marcaNome;
                    ids.Add(idusado);
                }

            }

            sw.WriteLine("</channel>");
            sw.WriteLine("</rss>");
            sw.WriteLine();
            sw.Close();

            File.Copy(ConfigurationManager.AppSettings["caminhoFisico"].ToString() + "allin_create.xml", ConfigurationManager.AppSettings["caminhoFisico"].ToString() + "allin.xml", true);
            File.Copy(ConfigurationManager.AppSettings["caminhoFisico"].ToString() + "allin_create.xml", rnConfiguracoes.caminhoXmls + "allin.xml", true);

        }

        public static void GerarMerchant(List<tbProdutos> produtos, List<tbJuncaoProdutoCategoria> categoriasProdutos, List<tbProdutoCategoria> categorias, List<tbCondicoesDePagamento> condicoesPagamento, List<tbMarca> marcas)
        {
            FileInfo file = new FileInfo(ConfigurationManager.AppSettings["caminhoFisico"].ToString() + "shopping_create.xml");

            if (file.Exists)
            {
                file.Delete();
            }
            StreamWriter sw = file.CreateText();
            sw.WriteLine("<?xml version=\"1.0\"?>");
            sw.WriteLine("<rss version=\"2.0\" xmlns:g=\"http://base.google.com/ns/1.0\">");
            sw.WriteLine("<channel>");
            sw.WriteLine("<title>Produtos</title>");
            sw.WriteLine("<link>http://www.graodegente.com.br</link>");
            sw.WriteLine("<description>Lista de Produtos do Site</description>");


            var ids = new List<idUsados>();

            var dataCategoria = new dbSiteEntities();
            foreach (var produto in produtos)
            {
                var idFornecedors = produto.produtoIdDaEmpresa.Split('.');
                string idFornecedor = idFornecedors.Count() > 1 ? idFornecedors[1] : produto.produtoIdDaEmpresa;
                var random = new Random();
                if (string.IsNullOrEmpty(idFornecedor))
                {
                    idFornecedor = random.Next(1, 5000).ToString();
                }
                var usado = true;
                while (usado)
                {
                    var idUsado = (from c in ids where c.id == idFornecedor && c.fornecedor == marcas.First(x => x.marcaId == produto.produtoMarca).marcaNome select c).Count();
                    if (idUsado == 0)
                    {
                        usado = false;
                    }
                    else
                    {
                        idFornecedor = random.Next(1, 5000).ToString();
                    }
                }
                var categoria = (from c in categoriasProdutos
                                 where
                                     c.produtoId == produto.produtoId
                                 select c).FirstOrDefault();

                var categoriaNome = "Decoração de Quarto de Bebê";
                if (categoria != null)
                {
                    categoriaNome = categorias.First(x => x.categoriaId == categoria.categoriaId).categoriaNomeExibicao;

                    categoriaNome = categoriaNome.Replace("&", "e");

                    var categoriaSexo = (from c in dataCategoria.tbJuncaoProdutoCategoria
                                         where c.produtoId == produto.produtoId && c.tbProdutoCategoria.categoriaPaiId == 724
                                         select c).ToList();
                    string sexo = "";
                    if (categoriaSexo.Count() == 1)
                    {
                        sexo = categoriaSexo.First().tbProdutoCategoria.categoriaNomeExibicao;
                    }
                    sexo = sexo.ToLower().Contains("menino") ? "male" : sexo.ToLower().Contains("menina") ? "female" : "unissex";

                    var categoriaCor = (from c in dataCategoria.tbJuncaoProdutoCategoria
                                        where c.produtoId == produto.produtoId && c.tbProdutoCategoria.categoriaPaiId == 723
                                        select c).FirstOrDefault();
                    string cor = "";
                    if (categoriaCor != null)
                    {
                        cor = categoriaCor.tbProdutoCategoria.categoriaNomeExibicao;
                    }

                    string colecao = "";
                    var colecoes = (from c in dataCategoria.tbJuncaoProdutoColecao where c.produtoId == produto.produtoId select c).Distinct().ToList();
                    if (colecoes.Count() == 1)
                    {
                        colecao = colecoes.First().tbColecao.colecaoNome;
                    }

                    #region parcelamento


                    var produtoPrecoUtilizar = (produto.produtoPrecoPromocional > 0 &&
                                      produto.produtoPrecoPromocional < produto.produtoPreco
                        ? (decimal)produto.produtoPrecoPromocional
                        : produto.produtoPreco);


                    var condicao = condicoesPagamento.First(x => x.destaque.ToLower() == "true");
                    int parcelasSemJuros = condicao.parcelasSemJuros;
                    double taxaDeJuros = condicao.taxaDeJuros / 100;
                    decimal valorMinimoDaParcela = condicao.valorMinimoDaParcela;
                    decimal porcentagemDeDesconto = Convert.ToDecimal(condicao.porcentagemDeDesconto);
                    int numeroMaximoDeParcelas = condicao.numeroMaximoDeParcelas;
                    decimal valorDaParcela = 0;
                    int parcelasMaximas = 0;

                    for (int i = numeroMaximoDeParcelas; i <= numeroMaximoDeParcelas && i > 0; i--)
                    {
                        valorDaParcela = produtoPrecoUtilizar / i;

                        if (valorDaParcela >= valorMinimoDaParcela)
                        {
                            if (i > 1)
                            {
                                if (i <= parcelasSemJuros)
                                {
                                    parcelasMaximas = i;
                                }
                                break;
                            }
                        }
                    }

                    #endregion

                    var valorVenda = (produto.produtoPrecoPromocional > 0 &&
                                      produto.produtoPrecoPromocional < produto.produtoPreco
                        ? (decimal)produto.produtoPrecoPromocional
                        : produto.produtoPreco);
                    var valorDesconto = (valorVenda - ((valorVenda / 100) * 15));

                    string nome = produto.produtoNome;
                    if (!string.IsNullOrEmpty(produto.produtoNomeShopping)) nome = produto.produtoNomeShopping;
                    sw.WriteLine("<item>");
                    sw.WriteLine("<title><![CDATA[" + nome + "]]></title>");
                    sw.WriteLine("<link><![CDATA[" + rnConfiguracoes.caminhoVirtual +
                                 categorias.First(x => x.categoriaId == categoria.categoriaId).categoriaUrl + "/" + produto.produtoUrl +
                                 "]]></link>");
                    sw.WriteLine("<description><![CDATA[" + produto.produtoNome + "]]></description>");

                    sw.WriteLine("<gender><![CDATA[" + sexo + "]]></gender>");
                    sw.WriteLine("<color><![CDATA[" + cor + "]]></color>");
                    sw.WriteLine("<pattern><![CDATA[" + colecao + "]]></pattern>");
                    sw.WriteLine("<g:availability>in stock</g:availability>");
                    //sw.WriteLine("<g:google_product_category>Casa e jardim &gt; Lençóis &gt; Roupas de cama</g:google_product_category>");
                    sw.WriteLine("<g:google_product_category>Infantil</g:google_product_category>");
                    sw.WriteLine("<g:product_type>" + categoriaNome + "</g:product_type>");
                    sw.WriteLine("<g:image_link>" + rnConfiguracoes.caminhoCDN + "fotos/" + produto.produtoId + "/" + produto.fotoDestaque + ".jpg" + "</g:image_link>");
                    sw.WriteLine("<g:price>" + produto.produtoPreco.ToString("0.00").Replace(",", ".") + "</g:price>");
                    sw.WriteLine("<g:sale_price>" + valorDesconto.ToString("0.00").Replace(",", ".") + "</g:sale_price>");
                    sw.WriteLine("<g:condition>new</g:condition>");
                    sw.WriteLine("<g:id>" + produto.produtoId + "</g:id>");
                    sw.WriteLine("<g:brand><![CDATA[Grão de Gente]]></g:brand>");

                    if (!string.IsNullOrEmpty(produto.produtoCodDeBarras))
                    {
                        sw.WriteLine("<g:gtin>" + produto.produtoCodDeBarras + "</g:gtin>");
                    }
                    else
                    {
                        sw.WriteLine("<g:mpn>" + idFornecedor + "</g:mpn>");
                    }
                    
                    sw.WriteLine("<g:custom_label_0>" + categoriaNome + "</g:custom_label_0>");
                    string parametrizacaoCampanha = "";
                    if (produto.produtoNome.ToLower().Contains("kit ber")) parametrizacaoCampanha = "a";
                    if ((produto.criacaoPropria ?? false) == true) parametrizacaoCampanha = "b";

                    if(!string.IsNullOrEmpty(parametrizacaoCampanha)) sw.WriteLine("<g:custom_label_1>" + parametrizacaoCampanha + "</g:custom_label_1>");

                    if (parcelasMaximas > 0)
                    {
                        sw.WriteLine("<g:installment>");
                        sw.WriteLine("<g:months>" + parcelasMaximas + "</g:months>");
                        //sw.WriteLine("<g:amount>" + valorDaParcela.ToString("C") + "</g:amount>");
                        sw.WriteLine("<g:amount>" + valorDaParcela.ToString("0.00").Replace(",", ".") + " BRL</g:amount>");
                        sw.WriteLine("</g:installment>");
                    }
                    sw.WriteLine("</item>");


                    var idusado = new idUsados();
                    idusado.id = idFornecedor;
                    if (produto.marketplaceEnviarMarca.ToLower() == "true")
                    {
                        idusado.fornecedor = marcas.First(x => x.marcaId == produto.produtoMarca).marcaNome;
                    }
                    else
                    {
                        idusado.fornecedor = "Grão de Gente";
                    }
                    ids.Add(idusado);
                }

            }

            sw.WriteLine("</channel>");
            sw.WriteLine("</rss>");
            sw.WriteLine();
            sw.Close();

            File.Copy(ConfigurationManager.AppSettings["caminhoFisico"].ToString() + "shopping_create.xml", ConfigurationManager.AppSettings["caminhoFisico"].ToString() + "shopping.xml", true);
            File.Copy(ConfigurationManager.AppSettings["caminhoFisico"].ToString() + "shopping_create.xml", rnConfiguracoes.caminhoXmls + "shopping.xml", true);

        }

        public static void GerarMerchantPrice(List<tbProdutos> produtos, List<tbJuncaoProdutoCategoria> categoriasProdutos, List<tbProdutoCategoria> categorias, List<tbCondicoesDePagamento> condicoesPagamento, List<tbMarca> marcas)
        {
            FileInfo file = new FileInfo(ConfigurationManager.AppSettings["caminhoFisico"].ToString() + "shoppingprice_create.xml");

            if (file.Exists)
            {
                file.Delete();
            }
            StreamWriter sw = file.CreateText();
            sw.WriteLine("<?xml version=\"1.0\"?>");
            sw.WriteLine("<rss version=\"2.0\" xmlns:g=\"http://base.google.com/ns/1.0\">");
            sw.WriteLine("<channel>");
            sw.WriteLine("<title>Produtos</title>");
            sw.WriteLine("<link>https://www.graodegente.com.br</link>");
            sw.WriteLine("<description>Lista de Produtos do Site</description>");


            var ids = new List<idUsados>();

            foreach (var produto in produtos)
            {               
                var categoria = (from c in categoriasProdutos
                                 where
                                     c.produtoId == produto.produtoId
                                 select c).FirstOrDefault();

                var categoriaNome = "Decoração de Quarto de Bebê";
                if (categoria != null)
                {
                    categoriaNome = categorias.First(x => x.categoriaId == categoria.categoriaId).categoriaNomeExibicao;

                    categoriaNome = categoriaNome.Replace("&", "e");


                    #region parcelamento


                    var produtoPrecoUtilizar = (produto.produtoPrecoPromocional > 0 &&
                                      produto.produtoPrecoPromocional < produto.produtoPreco
                        ? (decimal)produto.produtoPrecoPromocional
                        : produto.produtoPreco);


                    var condicao = condicoesPagamento.First(x => x.destaque.ToLower() == "true");
                    int parcelasSemJuros = condicao.parcelasSemJuros;
                    double taxaDeJuros = condicao.taxaDeJuros / 100;
                    decimal valorMinimoDaParcela = condicao.valorMinimoDaParcela;
                    decimal porcentagemDeDesconto = Convert.ToDecimal(condicao.porcentagemDeDesconto);
                    int numeroMaximoDeParcelas = condicao.numeroMaximoDeParcelas;
                    decimal valorDaParcela = 0;
                    int parcelasMaximas = 0;

                    for (int i = numeroMaximoDeParcelas; i <= numeroMaximoDeParcelas && i > 0; i--)
                    {
                        valorDaParcela = produtoPrecoUtilizar / i;

                        if (valorDaParcela >= valorMinimoDaParcela)
                        {
                            if (i > 1)
                            {
                                if (i <= parcelasSemJuros)
                                {
                                    parcelasMaximas = i;
                                }
                                break;
                            }
                        }
                    }

                    #endregion

                    var valorVenda = (produto.produtoPrecoPromocional > 0 &&
                                      produto.produtoPrecoPromocional < produto.produtoPreco
                        ? (decimal)produto.produtoPrecoPromocional
                        : produto.produtoPreco);
                    var valorDesconto = (valorVenda - ((valorVenda / 100) * 15));

                    sw.WriteLine("<item>");                    
                    sw.WriteLine("<g:price>" + produto.produtoPreco.ToString("0.00").Replace(",", ".") + "</g:price>");
                    sw.WriteLine("<g:sale_price>" + valorDesconto.ToString("0.00").Replace(",", ".") + "</g:sale_price>");
                    sw.WriteLine("<g:sale_price_effective_date></g:sale_price_effective_date>");                    
                     sw.WriteLine("<g:id>" + produto.produtoId + "</g:id>");
                    sw.WriteLine("</item>");                    
                }

            }

            sw.WriteLine("</channel>");
            sw.WriteLine("</rss>");
            sw.WriteLine();
            sw.Close();

            File.Copy(ConfigurationManager.AppSettings["caminhoFisico"].ToString() + "shoppingprice_create.xml", ConfigurationManager.AppSettings["caminhoFisico"].ToString() + "shoppingprice.xml", true);
            File.Copy(ConfigurationManager.AppSettings["caminhoFisico"].ToString() + "shoppingprice_create.xml", rnConfiguracoes.caminhoXmls + "shoppingprice.xml", true);

        }

        public static void GerarSitemap(List<tbProdutos> produtos, List<tbJuncaoProdutoCategoria> categoriasProdutos, List<tbProdutoCategoria> categorias, List<tbCondicoesDePagamento> condicoesPagamento, List<tbMarca> marcas)
        {
            FileInfo file = new FileInfo(ConfigurationManager.AppSettings["caminhoFisico"].ToString() + "sitemap_create.xml");
            if (file.Exists)
            {
                file.Delete();
            }
            StreamWriter sw = file.CreateText();
            sw.WriteLine("<?xml version=\"1.0\" encoding=\"UTF-8\" ?> ");
            sw.WriteLine("<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd\">");

            var categoriasGeral = (from c in categorias where c.exibirSite == true select c).ToList();

            sw.WriteLine("<url>");
            sw.WriteLine("<loc>" + rnConfiguracoes.caminhoVirtual + "</loc>");
            sw.WriteLine("<priority>1.00</priority>");
            sw.WriteLine("<changefreq>daily</changefreq>");
            sw.WriteLine("</url>");
            foreach (var categoria in categoriasGeral)
            {
                sw.WriteLine("<url>");
                sw.WriteLine("<loc>" + rnConfiguracoes.caminhoVirtual + categoria.categoriaUrl + "/</loc>");
                sw.WriteLine("<priority>1.00</priority>");
                sw.WriteLine("<changefreq>daily</changefreq>");
                sw.WriteLine("</url>");
            }
            
            foreach (var produto in produtos)
            {
                var categoria = (from c in categoriasProdutos
                                 where
                                     c.produtoId == produto.produtoId
                                 select c).FirstOrDefault();
                var categoriaNome = "Decoração de Quarto de Bebê";
                if (categoria != null)
                {
                    var categoriaDetalhe = categorias.First(x => x.categoriaId == categoria.categoriaId);

                    categoriaNome = categorias.First(x => x.categoriaId == categoria.categoriaId).categoriaNomeExibicao;

                    categoriaNome = categoriaNome.Replace("&", "e");
                    
                    sw.WriteLine("<url>");
                    sw.WriteLine("<loc>" + rnConfiguracoes.caminhoVirtual +
                                    categoriaDetalhe.categoriaUrl + "/" + produto.produtoUrl +
                                    "/</loc>");
                    sw.WriteLine("<priority>1.00</priority>");
                    sw.WriteLine("<changefreq>daily</changefreq>");
                    sw.WriteLine("</url>");
                    
                }
            }

            sw.WriteLine("</urlset>");
            sw.WriteLine();
            sw.Close();

            File.Copy(ConfigurationManager.AppSettings["caminhoFisico"].ToString() + "sitemap_create.xml", ConfigurationManager.AppSettings["caminhoFisico"].ToString() + "sitemap.xml", true);
            File.Copy(ConfigurationManager.AppSettings["caminhoFisico"].ToString() + "sitemap_create.xml", rnConfiguracoes.caminhoXmls + "sitemap.xml", true);

        }

        public static void GerarMerchantDra(List<tbProdutos> produtos, List<tbJuncaoProdutoCategoria> categoriasProdutos, List<tbProdutoCategoria> categorias, List<tbCondicoesDePagamento> condicoesPagamento, List<tbMarca> marcas)
        {
            FileInfo file = new FileInfo(ConfigurationManager.AppSettings["caminhoFisico"].ToString() + "shoppingdra_create.xml");

            if (file.Exists)
            {
                file.Delete();
            }
            StreamWriter sw = file.CreateText();
            sw.WriteLine("<?xml version=\"1.0\"?>");
            sw.WriteLine("<rss version=\"2.0\" xmlns:g=\"http://base.google.com/ns/1.0\">");
            sw.WriteLine("<channel>");
            sw.WriteLine("<title>Produtos</title>");
            sw.WriteLine("<link>http://www.graodegente.com.br</link>");
            sw.WriteLine("<description>Lista de Produtos do Site</description>");


            var ids = new List<idUsados>();

            foreach (var produto in produtos)
            {
                var idFornecedors = produto.produtoIdDaEmpresa.Split('.');
                string idFornecedor = idFornecedors.Count() > 1 ? idFornecedors[1] : produto.produtoIdDaEmpresa;
                var random = new Random();
                if (string.IsNullOrEmpty(idFornecedor))
                {
                    idFornecedor = random.Next(1, 5000).ToString();
                }
                var usado = true;
                while (usado)
                {
                    var idUsado = (from c in ids where c.id == idFornecedor && c.fornecedor == marcas.First(x => x.marcaId == produto.produtoMarca).marcaNome select c).Count();
                    if (idUsado == 0)
                    {
                        usado = false;
                    }
                    else
                    {
                        idFornecedor = random.Next(1, 5000).ToString();
                    }
                }
                var categoria = (from c in categoriasProdutos
                                 where
                                     c.produtoId == produto.produtoId
                                 select c).FirstOrDefault();

                var categoriaNome = "Decoração de Quarto de Bebê";
                if (categoria != null)
                {
                    categoriaNome = categorias.First(x => x.categoriaId == categoria.categoriaId).categoriaNomeExibicao;

                    categoriaNome = categoriaNome.Replace("&", "e");


                    #region parcelamento


                    var produtoPrecoUtilizar = (produto.produtoPrecoPromocional > 0 &&
                                      produto.produtoPrecoPromocional < produto.produtoPreco
                        ? (decimal)produto.produtoPrecoPromocional
                        : produto.produtoPreco);


                    var condicao = condicoesPagamento.First(x => x.destaque.ToLower() == "true");
                    int parcelasSemJuros = condicao.parcelasSemJuros;
                    double taxaDeJuros = condicao.taxaDeJuros / 100;
                    decimal valorMinimoDaParcela = condicao.valorMinimoDaParcela;
                    decimal porcentagemDeDesconto = Convert.ToDecimal(condicao.porcentagemDeDesconto);
                    int numeroMaximoDeParcelas = condicao.numeroMaximoDeParcelas;
                    decimal valorDaParcela = 0;
                    int parcelasMaximas = 0;

                    for (int i = numeroMaximoDeParcelas; i <= numeroMaximoDeParcelas && i > 0; i--)
                    {
                        valorDaParcela = produtoPrecoUtilizar / i;

                        if (valorDaParcela >= valorMinimoDaParcela)
                        {
                            if (i > 1)
                            {
                                if (i <= parcelasSemJuros)
                                {
                                    parcelasMaximas = i;
                                }
                                break;
                            }
                        }
                    }

                    #endregion

                    var valorVenda = (produto.produtoPrecoPromocional > 0 &&
                                      produto.produtoPrecoPromocional < produto.produtoPreco
                        ? (decimal)produto.produtoPrecoPromocional
                        : produto.produtoPreco);
                    var valorDesconto = (valorVenda - ((valorVenda / 100) * 15));

                    string nome = produto.produtoNome;
                    if (!string.IsNullOrEmpty(produto.produtoNomeShopping)) nome = produto.produtoNomeShopping;


                    sw.WriteLine("<item>");
                    sw.WriteLine("<title><![CDATA[" + nome + "]]></title>");
                    sw.WriteLine("<link><![CDATA[" + rnConfiguracoes.caminhoVirtual +
                                 categorias.First(x => x.categoriaId == categoria.categoriaId).categoriaUrl + "/" + produto.produtoUrl +
                                 "]]></link>");
                    sw.WriteLine("<description><![CDATA[" + produto.produtoNome + "]]></description>");
                    sw.WriteLine("<g:availability>in stock</g:availability>");
                    //sw.WriteLine("<g:google_product_category>Casa e jardim &gt; Lençóis &gt; Roupas de cama</g:google_product_category>");
                    sw.WriteLine("<g:google_product_category>Infantil</g:google_product_category>");
                    sw.WriteLine("<g:product_type>" + categoriaNome + "</g:product_type>");
                    sw.WriteLine("<g:image_link>" + rnConfiguracoes.caminhoCDN + "fotos/" + produto.produtoId + "/" + produto.fotoDestaque + ".jpg" + "</g:image_link>");
                    sw.WriteLine("<g:price>" + valorVenda.ToString("0.00").Replace(",", ".") + "</g:price>");
                    sw.WriteLine("<g:sale_price>" + valorDesconto.ToString("0.00").Replace(",", ".") + "</g:sale_price>");
                    sw.WriteLine("<g:condition>new</g:condition>");
                    sw.WriteLine("<g:id>" + produto.produtoId + "</g:id>");
                    sw.WriteLine("<g:brand><![CDATA[" + marcas.First(x => x.marcaId == produto.produtoMarca).marcaNome + "]]></g:brand>");
                    sw.WriteLine("<g:mpn>" + idFornecedor + "</g:mpn>");
                    sw.WriteLine("<g:custom_label_0>" + categoriaNome + "</g:custom_label_0>");
                    sw.WriteLine("<g:custom_label_1>" + ((produto.criacaoPropria ?? false) == true ? "sim" : "nao") + "</g:custom_label_1>");
                    if (parcelasMaximas > 0)
                    {
                        sw.WriteLine("<g:installment>");
                        sw.WriteLine("<g:months>" + parcelasMaximas + "</g:months>");
                        //sw.WriteLine("<g:amount>" + valorDaParcela.ToString("C") + "</g:amount>");
                        sw.WriteLine("<g:amount>" + valorDaParcela.ToString("0.00").Replace(",", ".") + " BRL</g:amount>");
                        sw.WriteLine("</g:installment>");
                    }
                    sw.WriteLine("</item>");


                    var idusado = new idUsados();
                    idusado.id = idFornecedor;
                    idusado.fornecedor = marcas.First(x => x.marcaId == produto.produtoMarca).marcaNome;
                    ids.Add(idusado);
                }

            }

            sw.WriteLine("</channel>");
            sw.WriteLine("</rss>");
            sw.WriteLine();
            sw.Close();

            File.Copy(ConfigurationManager.AppSettings["caminhoFisico"].ToString() + "shoppingdra_create.xml", ConfigurationManager.AppSettings["caminhoFisico"].ToString() + "shoppingdra.xml", true);
            File.Copy(ConfigurationManager.AppSettings["caminhoFisico"].ToString() + "shoppingdra_create.xml", rnConfiguracoes.caminhoXmls + "shoppingdra.xml", true);
            
        }

        public static void GerarChaordic(List<tbProdutos> produtos, List<tbJuncaoProdutoCategoria> categoriasProdutos, List<tbProdutoCategoria> categorias, List<tbCondicoesDePagamento> condicoesPagamento, List<tbMarca> marcas)
        {
            FileInfo file = new FileInfo(ConfigurationManager.AppSettings["caminhoFisico"].ToString() + "chaordic_create.xml");

            if (file.Exists)
            {
                file.Delete();
            }
            StreamWriter sw = file.CreateText();
            sw.WriteLine("<?xml version=\"1.0\"?>");
            sw.WriteLine("<rss version=\"2.0\" xmlns:g=\"http://base.google.com/ns/1.0\">");
            sw.WriteLine("<channel>");
            sw.WriteLine("<title>Produtos</title>");
            sw.WriteLine("<link>http://www.graodegente.com.br</link>");
            sw.WriteLine("<description>Lista de Produtos do Site</description>");


            var ids = new List<idUsados>();

            foreach (var produto in produtos)
            {
                var idFornecedors = produto.produtoIdDaEmpresa.Split('.');
                string idFornecedor = idFornecedors.Count() > 1 ? idFornecedors[1] : produto.produtoIdDaEmpresa;
                var random = new Random();
                if (string.IsNullOrEmpty(idFornecedor))
                {
                    idFornecedor = random.Next(1, 5000).ToString();
                }
                var usado = true;
                while (usado)
                {
                    var idUsado = (from c in ids where c.id == idFornecedor && c.fornecedor == marcas.First(x => x.marcaId == produto.produtoMarca).marcaNome select c).Count();
                    if (idUsado == 0)
                    {
                        usado = false;
                    }
                    else
                    {
                        idFornecedor = random.Next(1, 5000).ToString();
                    }
                }
                var categoria = (from c in categoriasProdutos
                                 where
                                     c.produtoId == produto.produtoId
                                 select c).FirstOrDefault();
                var categoriaNome = "Decoração de Quarto de Bebê";
                if (categoria != null)
                {
                    if (categoria.categoriaId == 413)
                    {
                        categoriaNome =
                            categorias.First(x => x.categoriaId == categoria.categoriaId).categoriaNomeExibicao;

                        categoriaNome = categoriaNome.Replace("&", "e");


                        #region parcelamento


                        var produtoPrecoUtilizar = (produto.produtoPrecoPromocional > 0 &&
                                                    produto.produtoPrecoPromocional < produto.produtoPreco
                            ? (decimal) produto.produtoPrecoPromocional
                            : produto.produtoPreco);


                        var condicao = condicoesPagamento.First(x => x.destaque.ToLower() == "true");
                        int parcelasSemJuros = condicao.parcelasSemJuros;
                        double taxaDeJuros = condicao.taxaDeJuros/100;
                        decimal valorMinimoDaParcela = condicao.valorMinimoDaParcela;
                        decimal porcentagemDeDesconto = Convert.ToDecimal(condicao.porcentagemDeDesconto);
                        int numeroMaximoDeParcelas = condicao.numeroMaximoDeParcelas;
                        decimal valorDaParcela = 0;
                        int parcelasMaximas = 0;

                        for (int i = numeroMaximoDeParcelas; i <= numeroMaximoDeParcelas && i > 0; i--)
                        {
                            valorDaParcela = produtoPrecoUtilizar/i;

                            if (valorDaParcela >= valorMinimoDaParcela)
                            {
                                if (i > 1)
                                {
                                    if (i <= parcelasSemJuros)
                                    {
                                        parcelasMaximas = i;
                                    }
                                    break;
                                }
                            }
                        }

                        #endregion

                        var valorVenda = (produto.produtoPrecoPromocional > 0 &&
                                          produto.produtoPrecoPromocional < produto.produtoPreco
                            ? (decimal) produto.produtoPrecoPromocional
                            : produto.produtoPreco);
                        var valorDesconto = (valorVenda - ((valorVenda/100)*15));

                        sw.WriteLine("<item>");
                        sw.WriteLine("<title><![CDATA[" + produto.produtoNome + "/]]></title>");
                        sw.WriteLine("<link><![CDATA[" + rnConfiguracoes.caminhoVirtual +
                                     categorias.First(x => x.categoriaId == categoria.categoriaId).categoriaUrl + "/" +
                                     produto.produtoUrl +
                                     "/]]></link>");
                        sw.WriteLine("<description><![CDATA[" + produto.produtoNome + "]]></description>");
                        sw.WriteLine("<g:availability>in stock</g:availability>");
                        //sw.WriteLine("<g:google_product_category>Casa e jardim &gt; Lençóis &gt; Roupas de cama</g:google_product_category>");
                        sw.WriteLine("<g:google_product_category>Infantil</g:google_product_category>");
                        sw.WriteLine("<g:product_type>" + categoriaNome + "</g:product_type>");
                        sw.WriteLine("<g:image_link>" + rnConfiguracoes.caminhoCDN + "fotos/" + produto.produtoId + "/" +
                                     produto.fotoDestaque + ".jpg" + "</g:image_link>");
                        sw.WriteLine("<g:price>" + valorVenda.ToString("0.00").Replace(",", ".") + "</g:price>");
                        sw.WriteLine("<g:sale_price>" + valorDesconto.ToString("0.00").Replace(",", ".") +
                                     "</g:sale_price>");
                        sw.WriteLine("<g:condition>new</g:condition>");
                        sw.WriteLine("<g:id>" + produto.produtoId + "</g:id>");
                        sw.WriteLine("<g:brand>" + marcas.First(x => x.marcaId == produto.produtoMarca).marcaNome +
                                     "</g:brand>");
                        sw.WriteLine("<g:mpn>" + idFornecedor + "</g:mpn>");
                        sw.WriteLine("<g:custom_label_0>" + categoriaNome + "</g:custom_label_0>");
                        sw.WriteLine("<g:custom_label_1>" + ((produto.criacaoPropria ?? false) == true ? "sim" : "nao") + "</g:custom_label_1>");
                        if (parcelasMaximas > 0)
                        {
                            sw.WriteLine("<g:installment>");
                            sw.WriteLine("<g:months>" + parcelasMaximas + "</g:months>");
                            sw.WriteLine("<g:amount>" + valorDaParcela.ToString("C") + "</g:amount>");
                            sw.WriteLine("</g:installment>");
                        }
                        sw.WriteLine("</item>");


                        var idusado = new idUsados();
                        idusado.id = idFornecedor;
                        idusado.fornecedor = marcas.First(x => x.marcaId == produto.produtoMarca).marcaNome;
                        ids.Add(idusado);
                    }
                }
            }

            sw.WriteLine("</channel>");
            sw.WriteLine("</rss>");
            sw.WriteLine();
            sw.Close();

            File.Copy(ConfigurationManager.AppSettings["caminhoFisico"].ToString() + "chaordic_create.xml", ConfigurationManager.AppSettings["caminhoFisico"].ToString() + "chaordic.xml", true);
            File.Copy(ConfigurationManager.AppSettings["caminhoFisico"].ToString() + "chaordic_create.xml", rnConfiguracoes.caminhoXmls + "chaordic.xml", true);
        }



        
        public static void GerarRtbHouse(List<tbProdutos> produtos, List<tbJuncaoProdutoCategoria> categoriasProdutos, List<tbProdutoCategoria> categorias, List<tbCondicoesDePagamento> condicoesPagamento, List<tbMarca> marcas)
        {
            FileInfo file = new FileInfo(ConfigurationManager.AppSettings["caminhoFisico"].ToString() + "rtbhouse_create.xml");
            if (file.Exists)
            {
                file.Delete();
            }

            StreamWriter sw = file.CreateText();
            sw.WriteLine("<products>");

            var dataCategoria = new dbSiteEntities();
            foreach (var produto in produtos)
            {
                var categoria = (from c in categoriasProdutos
                                 where c.produtoId == produto.produtoId
                                 select c).FirstOrDefault();
                if (categoria != null)
                {
                    var valorVenda = (produto.produtoPrecoPromocional > 0 &&
                                      produto.produtoPrecoPromocional < produto.produtoPreco
                        ? (decimal)produto.produtoPrecoPromocional
                        : produto.produtoPreco);
                    var valorDesconto = (valorVenda - ((valorVenda / 100) * 15));


                    var categoriaSexo = (from c in dataCategoria.tbJuncaoProdutoCategoria
                                         where c.produtoId == produto.produtoId && c.tbProdutoCategoria.categoriaPaiId == 724
                                         select c).FirstOrDefault();
                    string sexo = "";
                    if (categoriaSexo != null)
                    {
                        sexo = categoriaSexo.tbProdutoCategoria.categoriaNomeExibicao;
                    }

                    var categoriaCor = (from c in dataCategoria.tbJuncaoProdutoCategoria
                                        where c.produtoId == produto.produtoId && c.tbProdutoCategoria.categoriaPaiId == 723
                                        select c).FirstOrDefault();
                    string cor = "";
                    if (categoriaCor != null)
                    {
                        cor = categoriaCor.tbProdutoCategoria.categoriaNomeExibicao;
                    }

                    string colecao = "";
                    var colecoes = (from c in dataCategoria.tbJuncaoProdutoColecao where c.produtoId == produto.produtoId select c).Distinct().ToList();
                    if (colecoes.Count() == 1)
                    {
                        colecao = colecoes.First().tbColecao.colecaoNome;
                    }


                    


                    string nome = produto.produtoNome;
                    sw.WriteLine("<product id=\"" + produto.produtoId + "\">");
                    sw.WriteLine("<name><![CDATA[" + nome + "]]></name>");
                    sw.WriteLine("<smallimage>" + rnConfiguracoes.caminhoCDN + "fotos/" + produto.produtoId + "/media_" + produto.fotoDestaque + ".jpg" + "</smallimage>");
                    sw.WriteLine("<bigimage>" + rnConfiguracoes.caminhoCDN + "fotos/" + produto.produtoId + "/" + produto.fotoDestaque + ".jpg" + "</bigimage>");
                    sw.WriteLine("<producturl><![CDATA[" + rnConfiguracoes.caminhoVirtual + categorias.First(x => x.categoriaId == categoria.categoriaId).categoriaUrl + "/" + produto.produtoUrl +
                                 "/]]></producturl>");
                    sw.WriteLine("<description><![CDATA[" + (string.IsNullOrEmpty(produto.produtoNomeShopping) ? produto.produtoNomeShopping : produto.produtoNome) + "]]></description>");
                    sw.WriteLine("<retail_price>" + produto.produtoPreco.ToString().Replace(",", ".") + "</retail_price>");
                    sw.WriteLine("<price>" + valorDesconto.ToString().Replace(",", ".") + "</price>");
                    sw.WriteLine("<criacao_propria><![CDATA[" +  ((produto.criacaoPropria ?? false) == true ? "sim" : "nao") + "]]></criacao_propria>");
                    sw.WriteLine("<cor><![CDATA[" + cor + "]]></cor>");
                    sw.WriteLine("<sexo><![CDATA[" + sexo + "]]></sexo>");
                    sw.WriteLine("<colecao><![CDATA[" + colecao + "]]></colecao>");
                    sw.WriteLine("<categoria><![CDATA[" + categorias.First(x => x.categoriaId == categoria.categoriaId).categoriaNomeExibicao + "]]></categoria>");


                    sw.WriteLine("<instock>1</instock>");
                    sw.WriteLine("</product>");
                }
            }

            sw.WriteLine("</products>");
            sw.WriteLine();
            sw.Close();

            File.Copy(ConfigurationManager.AppSettings["caminhoFisico"].ToString() + "rtbhouse_create.xml", ConfigurationManager.AppSettings["caminhoFisico"].ToString() + "rtbhouse.xml", true);
            File.Copy(ConfigurationManager.AppSettings["caminhoFisico"].ToString() + "rtbhouse_create.xml", rnConfiguracoes.caminhoXmls + "rtbhouse.xml", true);
        }

        public static void GerarCriteo(List<tbProdutos> produtos, List<tbJuncaoProdutoCategoria> categoriasProdutos, List<tbProdutoCategoria> categorias, List<tbCondicoesDePagamento> condicoesPagamento, List<tbMarca> marcas)
        {
            FileInfo file = new FileInfo(ConfigurationManager.AppSettings["caminhoFisico"].ToString() + "criteo_create.xml");
            if (file.Exists)
            {
                file.Delete();
            }

            StreamWriter sw = file.CreateText();
            sw.WriteLine("<products>");


            foreach (var produto in produtos)
            {
                var categoria = (from c in categoriasProdutos
                                 where c.produtoId == produto.produtoId
                                 select c).FirstOrDefault();
                if (categoria != null)
                {
                    var valorVenda = (produto.produtoPrecoPromocional > 0 &&
                                      produto.produtoPrecoPromocional < produto.produtoPreco
                        ? (decimal)produto.produtoPrecoPromocional
                        : produto.produtoPreco);
                    var valorDesconto = (valorVenda - ((valorVenda / 100) * 15));

                    string nome = produto.produtoNome;
                    sw.WriteLine("<product id=\"" + produto.produtoId + "\">");
                    sw.WriteLine("<name><![CDATA[" + nome + "]]></name>");
                    sw.WriteLine("<smallimage>" + rnConfiguracoes.caminhoCDN + "fotos/" + produto.produtoId + "/media_" + produto.fotoDestaque + ".jpg" + "</smallimage>");
                    sw.WriteLine("<bigimage>" + rnConfiguracoes.caminhoCDN + "fotos/" + produto.produtoId + "/" + produto.fotoDestaque + ".jpg" + "</bigimage>");
                    sw.WriteLine("<producturl><![CDATA[" + rnConfiguracoes.caminhoVirtual + categorias.First(x => x.categoriaId == categoria.categoriaId).categoriaUrl + "/" + produto.produtoUrl +
                                 "/]]></producturl>");
                    sw.WriteLine("<description><![CDATA[" + (string.IsNullOrEmpty(produto.produtoNomeShopping) ? produto.produtoNomeShopping : produto.produtoNome) + "]]></description>");
                    sw.WriteLine("<retail_price>" + produto.produtoPreco.ToString().Replace(",", ".") + "</retail_price>");
                    sw.WriteLine("<price>" + valorDesconto.ToString().Replace(",", ".") + "</price>");
                    sw.WriteLine("<instock>1</instock>");
                    sw.WriteLine("</product>");
                }
            }

            sw.WriteLine("</products>");
            sw.WriteLine();
            sw.Close();

            File.Copy(ConfigurationManager.AppSettings["caminhoFisico"].ToString() + "criteo_create.xml", ConfigurationManager.AppSettings["caminhoFisico"].ToString() + "criteo.xml", true);
            File.Copy(ConfigurationManager.AppSettings["caminhoFisico"].ToString() + "criteo_create.xml", rnConfiguracoes.caminhoXmls + "criteo.xml", true);
        }


        public static void GerarJacotei(List<tbProdutos> produtos, List<tbJuncaoProdutoCategoria> categoriasProdutos, List<tbProdutoCategoria> categorias, List<tbCondicoesDePagamento> condicoesPagamento, List<tbMarca> marcas)
        {
            FileInfo file = new FileInfo(ConfigurationManager.AppSettings["caminhoFisico"].ToString() + "jacotei_create.xml");
            if (file.Exists)
            {
                file.Delete();
            }
            StreamWriter sw = file.CreateText();
            sw.WriteLine("<?xml version=\"1.0\" encoding=\"utf-8\" ?>");
            sw.WriteLine("<produtos>");
            var ids = new List<idUsados>();

            foreach (var produto in produtos)
            {

                #region parcelamento

                var condicao = condicoesPagamento.First(x => x.destaque.ToLower() == "true");
                int parcelasSemJuros = condicao.parcelasSemJuros;
                double taxaDeJuros = condicao.taxaDeJuros / 100;
                decimal valorMinimoDaParcela = condicao.valorMinimoDaParcela;
                decimal porcentagemDeDesconto = Convert.ToDecimal(condicao.porcentagemDeDesconto);
                int numeroMaximoDeParcelas = condicao.numeroMaximoDeParcelas;
                decimal valorDaParcela = 0;
                decimal produtoPreco = produto.produtoPrecoPromocional != 0 ? Convert.ToDecimal(produto.produtoPrecoPromocional) : produto.produtoPreco;
                int parcelamentoMaximo = 1;
                decimal valorDaParcelaMaxima = 0;

                string parcelamento = "";

                for (int i = numeroMaximoDeParcelas; i <= numeroMaximoDeParcelas && i > 0; i--)
                {
                    valorDaParcela = produtoPreco / i;

                    if (valorDaParcela >= valorMinimoDaParcela)
                    {
                        if (i > 1)
                        {
                            parcelamentoMaximo = i;
                            valorDaParcelaMaxima = valorDaParcela;

                            if (i <= parcelasSemJuros)
                            {
                                parcelamento = i.ToString() + "x " + String.Format("{0:c}", decimal.Parse(valorDaParcela.ToString()));
                            }
                            else
                            {
                                parcelamento = i.ToString() + "x " + String.Format("{0:c}", (double)valorDaParcela * (Math.Pow((1 + taxaDeJuros), i)));
                            }
                            break;
                        }
                    }
                }
                #endregion

                var condicaoDesconto =
                    condicoesPagamento.Where(x => x.ativo.ToLower() == "true" && x.porcentagemDeDesconto > 0)
                        .OrderByDescending(x => x.porcentagemDeDesconto)
                        .FirstOrDefault();

                var categoria = (from c in categoriasProdutos
                                 where
                                     c.produtoId == produto.produtoId
                                 select c).FirstOrDefault();
                if (categoria != null)
                {
                    sw.WriteLine("<produto>");
                    sw.WriteLine("<codigo>" + produto.produtoId + "</codigo>");
                    sw.WriteLine("<nome><![CDATA[" + produto.produtoNome + "]]></nome>");
                    sw.WriteLine("<categoria><![CDATA[" + categorias.First(x => x.categoriaId == categoria.categoriaId).categoriaNomeExibicao + "]]></categoria>");
                    sw.WriteLine("<preco>" + produtoPreco.ToString("0.00") + "</preco>");
                    if (condicaoDesconto != null)
                    {
                        var precoDesconto = produtoPreco - ((produtoPreco / 100) * Convert.ToInt32(condicaoDesconto.porcentagemDeDesconto));
                        sw.WriteLine("<preco_especial>" + precoDesconto.ToString("0.00").Replace(",", ".") + "</preco_especial>");
                        sw.WriteLine("<condicao_especial>À vista no Boleto ou Débito Online</condicao_especial>");
                    }
                    sw.WriteLine("<link>" + rnConfiguracoes.caminhoVirtual + categorias.First(x => x.categoriaId == categoria.categoriaId).categoriaUrl + "/" + produto.produtoUrl + "/?utm_source=JaCotei&amp;utm_medium=" + produto.produtoUrl + "</link>");

                    sw.WriteLine("<imagem>" + rnConfiguracoes.caminhoCDN + "fotos/" + produto.produtoId + "/" + produto.tbProdutoFoto + ".jpg" + "</imagem>");
                    sw.WriteLine("<parcelamento>");
                    sw.WriteLine("<parcelas>" + parcelamentoMaximo + "</parcelas>");
                    sw.WriteLine("<valor>" + valorDaParcelaMaxima.ToString("0.00") + "</valor>");
                    sw.WriteLine("<max_parcelas_sem_juros>" + parcelamentoMaximo + "</max_parcelas_sem_juros>");
                    sw.WriteLine("</parcelamento>");
                    sw.WriteLine("</produto>");
                }
            }

            sw.WriteLine("</produtos>");
            sw.WriteLine();
            sw.Close();

            File.Copy(ConfigurationManager.AppSettings["caminhoFisico"].ToString() + "jacotei_create.xml", ConfigurationManager.AppSettings["caminhoFisico"].ToString() + "jacotei.xml", true);
            File.Copy(ConfigurationManager.AppSettings["caminhoFisico"].ToString() + "jacotei_create.xml", rnConfiguracoes.caminhoXmls + "jacotei.xml", true);
        }
        public static void GerarLiveTarget(List<tbProdutos> produtos, List<tbJuncaoProdutoCategoria> categoriasProdutos, List<tbProdutoCategoria> categorias, List<tbCondicoesDePagamento> condicoesPagamento, List<tbMarca> marcas)
        {
            FileInfo file = new FileInfo(ConfigurationManager.AppSettings["caminhoFisico"].ToString() + "livetarget_create.xml");

            if (file.Exists)
            {
                file.Delete();
            }

            StreamWriter sw = file.CreateText();
            sw.WriteLine("<products>");


            foreach (var produto in produtos)
            {

                var categoria = (from c in categoriasProdutos
                                 where c.produtoId == produto.produtoId
                                 select c).FirstOrDefault();
                if (categoria != null)
                {
                    #region parcelamento
                    var condicao = condicoesPagamento.First(x => x.destaque.ToLower() == "true");
                    int parcelasSemJuros = condicao.parcelasSemJuros;
                    double taxaDeJuros = condicao.taxaDeJuros / 100;
                    decimal valorMinimoDaParcela = condicao.valorMinimoDaParcela;
                    decimal porcentagemDeDesconto = Convert.ToDecimal(condicao.porcentagemDeDesconto);
                    int numeroMaximoDeParcelas = condicao.numeroMaximoDeParcelas;
                    decimal valorDaParcela = 0;
                    decimal produtoPreco = produto.produtoPrecoPromocional != 0 ? Convert.ToDecimal(produto.produtoPrecoPromocional) : produto.produtoPreco;

                    string parcelamento = "";

                    for (int i = numeroMaximoDeParcelas; i <= numeroMaximoDeParcelas && i > 0; i--)
                    {
                        valorDaParcela = produtoPreco / i;

                        if (valorDaParcela >= valorMinimoDaParcela)
                        {
                            if (i > 1)
                            {
                                if (i <= parcelasSemJuros)
                                {
                                    parcelamento = i.ToString() + "x de " + String.Format("{0:c}", decimal.Parse(valorDaParcela.ToString())) + " sem juros";
                                }
                                else
                                {
                                    parcelamento = i.ToString() + "x de " + String.Format("{0:c}", (double)valorDaParcela * (Math.Pow((1 + taxaDeJuros), i))) + " sem juros";
                                }
                                break;
                            }
                        }
                    }
                    #endregion


                    var valorVenda = (produto.produtoPrecoPromocional > 0 &&
                                      produto.produtoPrecoPromocional < produto.produtoPreco
                        ? (decimal)produto.produtoPrecoPromocional
                        : produto.produtoPreco);
                    var valorDesconto = (valorVenda - ((valorVenda / 100) * 15));
                    sw.WriteLine("<product>");
                    sw.WriteLine("<id>" + produto.produtoId + "</id>");
                    sw.WriteLine("<sku>" + produto.produtoId + "</sku>");
                    sw.WriteLine("<categoryid>" + categoria.categoriaId + "</categoryid>");
                    sw.WriteLine("<categoryname><![CDATA[" + categorias.First(x => x.categoriaId == categoria.categoriaId).categoriaNomeExibicao + "]]></categoryname>");
                    sw.WriteLine("<name>" + produto.produtoNome + "</name>");
                    sw.WriteLine("<description><![CDATA[" + produto.produtoNome + "]]></description>");
                    sw.WriteLine("<smallimage>" + rnConfiguracoes.caminhoCDN + "fotos/" + produto.produtoId + "/pequena_" + produto.fotoDestaque + ".jpg" + "</smallimage>");
                    sw.WriteLine("<bigimage>" + rnConfiguracoes.caminhoCDN + "fotos/media_" + produto.produtoId + "/" + produto.fotoDestaque + ".jpg" + "</bigimage>");
                    sw.WriteLine("<producturl><![CDATA[" + rnConfiguracoes.caminhoVirtual + categorias.First(x => x.categoriaId == categoria.categoriaId).categoriaUrl + "/" + produto.produtoUrl + "/?utm_source=LiveTarget&utm_campaign=EmailRetargeting&utm_medium=cpc]]></producturl>");
                    //sw.WriteLine("<description>" + produto.produtoNome + "</description>");
                    sw.WriteLine("<price>" + produto.produtoPreco.ToString().Replace(",", ".") + "</price>");
                    sw.WriteLine("<pricepromotion>" + valorDesconto.ToString().Replace(",", ".") + "</pricepromotion>");
                    sw.WriteLine("<parcelamento>" + parcelamento + "</parcelamento>");
                    sw.WriteLine("</product>");
                }
            }

            sw.WriteLine("</products>");
            sw.WriteLine();
            sw.Close();

            File.Copy(ConfigurationManager.AppSettings["caminhoFisico"].ToString() + "livetarget_create.xml", ConfigurationManager.AppSettings["caminhoFisico"].ToString() + "livetarget.xml", true);

            File.Copy(ConfigurationManager.AppSettings["caminhoFisico"].ToString() + "livetarget_create.xml", rnConfiguracoes.caminhoXmls + "livetarget.xml", true);
        }
        public static void GerarMucca(List<tbProdutos> produtos, List<tbJuncaoProdutoCategoria> categoriasProdutos, List<tbProdutoCategoria> categorias, List<tbCondicoesDePagamento> condicoesPagamento, List<tbMarca> marcas)
        {
            FileInfo file = new FileInfo(ConfigurationManager.AppSettings["caminhoFisico"].ToString() + "muccashop_create.xml");
            if (file.Exists)
            {
                file.Delete();
            }

            StreamWriter sw = file.CreateText();
            sw.WriteLine("<?xml version=\"1.0\" ?> ");
            sw.WriteLine("<loja>");
            foreach (var produto in produtos)
            {
                decimal valor = produto.produtoPrecoPromocional > 0
                                    ? (decimal)produto.produtoPrecoPromocional
                                    : produto.produtoPreco;


                #region parcelamento
                var condicao = condicoesPagamento.First(x => x.destaque.ToLower() == "true");
                int parcelasSemJuros = condicao.parcelasSemJuros;
                double taxaDeJuros = condicao.taxaDeJuros / 100;
                decimal valorMinimoDaParcela = condicao.valorMinimoDaParcela;
                decimal porcentagemDeDesconto = Convert.ToDecimal(condicao.porcentagemDeDesconto);
                int numeroMaximoDeParcelas = condicao.numeroMaximoDeParcelas;
                decimal valorDaParcela = 0;
                decimal produtoPreco = produto.produtoPrecoPromocional != 0 ? Convert.ToDecimal(produto.produtoPrecoPromocional) : produto.produtoPreco;

                string parcelamento = "";

                for (int i = numeroMaximoDeParcelas; i <= numeroMaximoDeParcelas && i > 0; i--)
                {
                    valorDaParcela = produtoPreco / i;

                    if (valorDaParcela >= valorMinimoDaParcela)
                    {
                        if (i > 1)
                        {
                            if (i <= parcelasSemJuros)
                            {
                                parcelamento = i.ToString() + "x " + String.Format("{0:c}", decimal.Parse(valorDaParcela.ToString()));
                            }
                            else
                            {
                                parcelamento = i.ToString() + "x " + String.Format("{0:c}", (double)valorDaParcela * (Math.Pow((1 + taxaDeJuros), i)));
                            }
                            break;
                        }
                    }
                }
                #endregion


                var valorVenda = (produto.produtoPrecoPromocional > 0 &&
                                  produto.produtoPrecoPromocional < produto.produtoPreco
                    ? (decimal)produto.produtoPrecoPromocional
                    : produto.produtoPreco);
                var valorDesconto = (valorVenda - ((valorVenda / 100) * 15));

                var categoria = (from c in categoriasProdutos
                                 where c.produtoId == produto.produtoId
                                 select c).FirstOrDefault();
                if (categoria != null)
                {


                    sw.WriteLine("<produto>");
                    sw.WriteLine("<link><![CDATA[" + rnConfiguracoes.caminhoVirtual + categorias.First(x => x.categoriaId == categoria.categoriaId).categoriaUrl + "/" + produto.produtoUrl + "/?utm_source=muccashop&utm_medium=" + categorias.First(x => x.categoriaId == categoria.categoriaId).categoriaNomeExibicao + "]]></link>");
                    sw.WriteLine("<imagem>" + rnConfiguracoes.caminhoCDN + "fotos/" + produto.produtoId + "/" + produto.fotoDestaque + ".jpg" + "</imagem>");
                    sw.WriteLine("<nome><![CDATA[" + produto.produtoNome + "]]></nome>");
                    sw.WriteLine("<categoria><![CDATA[" + categorias.First(x => x.categoriaId == categoria.categoriaId).categoriaNomeExibicao + "]]></categoria>");
                    sw.WriteLine("<descricao><![CDATA[" + produto.produtoNome.Replace("&", "&amp;") + "]]></descricao>");
                    sw.WriteLine("<valor><![CDATA[" + valor.ToString("0.00").Replace(".", ",") + "]]></valor>");
                    sw.WriteLine("<valor_promo><![CDATA[" + valorDesconto.ToString("0.00").Replace(".", ",") + "]]></valor_promo>");
                    sw.WriteLine("<parcelamento><![CDATA[" + parcelamento + "]]></parcelamento>");
                    sw.WriteLine("</produto>");
                }
            }

            sw.WriteLine("</loja>");
            sw.WriteLine();
            sw.Close();

            File.Copy(ConfigurationManager.AppSettings["caminhoFisico"].ToString() + "muccashop_create.xml", ConfigurationManager.AppSettings["caminhoFisico"].ToString() + "muccashop.xml", true);
            File.Copy(ConfigurationManager.AppSettings["caminhoFisico"].ToString() + "muccashop_create.xml", rnConfiguracoes.caminhoXmls + "muccashop.xml", true);
        }
        public static void GerarRoiHunter(List<tbProdutos> produtos, List<tbJuncaoProdutoCategoria> categoriasProdutos, List<tbProdutoCategoria> categorias, List<tbCondicoesDePagamento> condicoesPagamento, List<tbMarca> marcas)
        {
            FileInfo file = new FileInfo(ConfigurationManager.AppSettings["caminhoFisico"].ToString() + "roihunter_create.xml");

            if (file.Exists)
            {
                file.Delete();
            }
            StreamWriter sw = file.CreateText();
            sw.WriteLine("<?xml version=\"1.0\"?>");
            sw.WriteLine("<rss version=\"2.0\" xmlns:g=\"http://base.google.com/ns/1.0\">");
            sw.WriteLine("<channel>");
            sw.WriteLine("<title>Produtos</title>");
            sw.WriteLine("<link>http://www.graodegente.com.br</link>");
            sw.WriteLine("<description>Lista de Produtos do Site</description>");

            var data = new dbSiteEntities();
            var depoimentos = (from c in data.tbDepoimentoExterno where c.aprovado select c).ToList();
            int totalDepoimentos = depoimentos.Count;
            int depoimentoAtual = 0;

            var dataCategoria = new dbSiteEntities();
            foreach (var produto in produtos.Where(x => x.fotoDestaque != ""))
            {
                var depoimento = depoimentos[depoimentoAtual];
                depoimentoAtual++;
                if (depoimentoAtual == totalDepoimentos) depoimentoAtual = 0;

                var categoria = (from c in categoriasProdutos
                                 where
                                     c.produtoId == produto.produtoId
                                 select c).FirstOrDefault();

                var categoriaNome = "Decoração de Quarto de Bebê";
                if (categoria != null)
                {
                    categoriaNome = categorias.First(x => x.categoriaId == categoria.categoriaId).categoriaNomeExibicao;

                    categoriaNome = categoriaNome.Replace("&", "e");


                    #region parcelamento


                    var produtoPrecoUtilizar = (produto.produtoPrecoPromocional > 0 &&
                                      produto.produtoPrecoPromocional < produto.produtoPreco
                        ? (decimal)produto.produtoPrecoPromocional
                        : produto.produtoPreco);


                    var condicao = condicoesPagamento.First(x => x.destaque.ToLower() == "true");
                    int parcelasSemJuros = condicao.parcelasSemJuros;
                    double taxaDeJuros = condicao.taxaDeJuros / 100;
                    decimal valorMinimoDaParcela = condicao.valorMinimoDaParcela;
                    decimal porcentagemDeDesconto = Convert.ToDecimal(condicao.porcentagemDeDesconto);
                    int numeroMaximoDeParcelas = condicao.numeroMaximoDeParcelas;
                    decimal valorDaParcela = 0;
                    int parcelasMaximas = 0;

                    for (int i = numeroMaximoDeParcelas; i <= numeroMaximoDeParcelas && i > 0; i--)
                    {
                        valorDaParcela = produtoPrecoUtilizar / i;

                        if (valorDaParcela >= valorMinimoDaParcela)
                        {
                            if (i > 1)
                            {
                                if (i <= parcelasSemJuros)
                                {
                                    parcelasMaximas = i;
                                }
                                break;
                            }
                        }
                    }

                    #endregion


                    var valorVenda = (produto.produtoPrecoPromocional > 0 &&
                                      produto.produtoPrecoPromocional < produto.produtoPreco
                        ? (decimal)produto.produtoPrecoPromocional
                        : produto.produtoPreco);

                    var valorCheio = (produto.produtoPrecoPromocional > produto.produtoPreco
                        ? (decimal)produto.produtoPrecoPromocional
                        : produto.produtoPreco);

                    var valorDesconto = (valorVenda - ((valorVenda / 100) * 15));

                    var porcentagemDesconto = Convert.ToInt32(100 - ((valorDesconto * 100) / valorCheio));



                    var categoriaSexo = (from c in dataCategoria.tbJuncaoProdutoCategoria
                                         where c.produtoId == produto.produtoId && c.tbProdutoCategoria.categoriaPaiId == 724
                                         select c).FirstOrDefault();
                    string sexo = "";
                    if (categoriaSexo != null)
                    {
                        sexo = categoriaSexo.tbProdutoCategoria.categoriaNomeExibicao;
                    }

                    sexo = sexo.ToLower().Contains("menino") ? "male" : sexo.ToLower().Contains("menina") ? "female" : "unissex";

                    var categoriaCor = (from c in dataCategoria.tbJuncaoProdutoCategoria
                                        where c.produtoId == produto.produtoId && c.tbProdutoCategoria.categoriaPaiId == 723
                                        select c).FirstOrDefault();
                    string cor = "";
                    if (categoriaCor != null)
                    {
                        cor = categoriaCor.tbProdutoCategoria.categoriaNomeExibicao;
                    }

                    string colecao = "";
                    var colecoes = (from c in dataCategoria.tbJuncaoProdutoColecao where c.produtoId == produto.produtoId select c).Distinct().ToList();
                    if (colecoes.Count() == 1)
                    {
                        colecao = colecoes.First().tbColecao.colecaoNome;
                    }

                    sw.WriteLine("<item>");
                    sw.WriteLine("<g:id>" + produto.produtoId + "</g:id>");
                    sw.WriteLine("<g:availability>in stock</g:availability>");
                    sw.WriteLine("<g:condition>new</g:condition>");

                    sw.WriteLine("<gender><![CDATA[" + sexo + "]]></gender>");
                    sw.WriteLine("<color><![CDATA[" + cor + "]]></color>");
                    sw.WriteLine("<pattern><![CDATA[" + colecao + "]]></pattern>");

                    sw.WriteLine("<description><![CDATA[" + (String.IsNullOrEmpty(produto.produtoNome) ? produto.produtoNome : produto.produtoNome) + "]]></description>");
                    sw.WriteLine("<g:image_link>" + rnConfiguracoes.caminhoCDN + "fotos/" + produto.produtoId + "/" + produto.fotoDestaque + ".jpg" + "</g:image_link>");
                    sw.WriteLine("<g:price>" + valorVenda.ToString("0.00").Replace(",", ".") + "</g:price>");
                    sw.WriteLine("<g:brand><![CDATA[Grão de Gente]]></g:brand>");
                    //sw.WriteLine("<g:google_product_category>Casa e jardim &gt; Lençóis &gt; Roupas de cama</g:google_product_category>");
                    sw.WriteLine("<g:google_product_category>" + categoriaNome + "</g:google_product_category>");
                    sw.WriteLine("<link><![CDATA[" + rnConfiguracoes.caminhoVirtual + categorias.First(x => x.categoriaId == categoria.categoriaId).categoriaUrl + "/" + produto.produtoUrl + "]]></link>");
                    sw.WriteLine("<title><![CDATA[" + produto.produtoNome + "]]></title>");
                    sw.WriteLine("<g:age_group>newborn</g:age_group>");

                    sw.WriteLine("<g:product_type>" + categoriaNome + "</g:product_type>");
                    sw.WriteLine("<g:sale_price>" + valorDesconto.ToString("0.00").Replace(",", ".") + "</g:sale_price>");
                    sw.WriteLine("<display_price>" + valorDesconto.ToString("C") + "</display_price>");
                    sw.WriteLine("<valorCheio>" + valorCheio.ToString("C") + "</valorCheio>");
                    sw.WriteLine("<porcentagemDesconto>" + porcentagemDesconto + "</porcentagemDesconto>");
                    sw.WriteLine("<g:mpn>" + produto.produtoId + "</g:mpn>");
                    sw.WriteLine("<dep_titulo><![CDATA[" + rnFuncoes.UppercaseFirst(depoimento.titulo.ToLower()) + "]]></dep_titulo>");
                    sw.WriteLine("<dep_depoimento><![CDATA[" + rnFuncoes.UppercaseFirst(depoimento.opiniao.ToLower()) + "]]></dep_depoimento>");
                    var nomeDepoimento = "";
                    var depoimentoSplit = depoimento.nome.Split(' ');
                    nomeDepoimento = rnFuncoes.UppercaseFirst(depoimentoSplit[0].ToLower());
                    if (depoimentoSplit.Count() > 1) nomeDepoimento += " " + depoimentoSplit[1].Substring(0, 1);
                    sw.WriteLine("<dep_nome><![CDATA[" + nomeDepoimento + "]]></dep_nome>");
                    if (parcelasMaximas > 1)
                    {
                        sw.WriteLine("<parcelas>" + parcelasMaximas + "</parcelas>");
                        sw.WriteLine("<valor_parcela>" + valorDaParcela.ToString("C") + "</valor_parcela>");
                    }
                    else
                    {
                        sw.WriteLine("<parcelas>1</parcelas>");
                        sw.WriteLine("<valor_parcela>" + valorVenda.ToString("C") + "</valor_parcela>");
                    }
                    sw.WriteLine("</item>");
                }

            }

            sw.WriteLine("</channel>");
            sw.WriteLine("</rss>");
            sw.WriteLine();
            sw.Close();

            File.Copy(ConfigurationManager.AppSettings["caminhoFisico"].ToString() + "roihunter_create.xml", ConfigurationManager.AppSettings["caminhoFisico"].ToString() + "roihunter.xml", true);
            File.Copy(ConfigurationManager.AppSettings["caminhoFisico"].ToString() + "roihunter_create.xml", rnConfiguracoes.caminhoXmls + "roihunter.xml", true);
        }
        public static void GerarTargetingMantra(List<tbProdutos> produtos, List<tbJuncaoProdutoCategoria> categoriasProdutos, List<tbProdutoCategoria> categorias, List<tbCondicoesDePagamento> condicoesPagamento, List<tbMarca> marcas)
        {
            FileInfo file = new FileInfo(ConfigurationManager.AppSettings["caminhoFisico"].ToString() + "targetingmantra_create.csv");
            if (file.Exists)
            {
                file.Delete();
            }
            StreamWriter sw = file.CreateText();
            foreach (var produto in produtos)
            {

                var dataCategoria = new dbSiteEntities();

                var categoria = (from c in dataCategoria.tbJuncaoProdutoCategoria
                                 where c.produtoId == produto.produtoId && c.tbProdutoCategoria.exibirSite == true && c.tbProdutoCategoria.categoriaPaiId == 0
                                 select c).FirstOrDefault();
                if (categoria != null)
                {

                    #region parcelamento


                    var produtoPrecoUtilizar = (produto.produtoPrecoPromocional > 0 &&
                                      produto.produtoPrecoPromocional < produto.produtoPreco
                        ? (decimal)produto.produtoPrecoPromocional
                        : produto.produtoPreco);


                    var condicao = condicoesPagamento.First(x => x.destaque.ToLower() == "true");
                    int parcelasSemJuros = condicao.parcelasSemJuros;
                    double taxaDeJuros = condicao.taxaDeJuros / 100;
                    decimal valorMinimoDaParcela = condicao.valorMinimoDaParcela;
                    decimal porcentagemDeDesconto = Convert.ToDecimal(condicao.porcentagemDeDesconto);
                    int numeroMaximoDeParcelas = condicao.numeroMaximoDeParcelas;
                    decimal valorDaParcela = 0;
                    int parcelasMaximas = 0;

                    for (int i = numeroMaximoDeParcelas; i <= numeroMaximoDeParcelas && i > 0; i--)
                    {
                        valorDaParcela = produtoPrecoUtilizar / i;

                        if (valorDaParcela >= valorMinimoDaParcela)
                        {
                            if (i > 1)
                            {
                                if (i <= parcelasSemJuros)
                                {
                                    parcelasMaximas = i;
                                }
                                break;
                            }
                        }
                    }

                    #endregion

                    var valorVenda = (produto.produtoPrecoPromocional > 0 &&
                                      produto.produtoPrecoPromocional < produto.produtoPreco
                        ? (decimal)produto.produtoPrecoPromocional
                        : produto.produtoPreco);

                    var valorCheio = (produto.produtoPrecoPromocional > produto.produtoPreco
                        ? (decimal)produto.produtoPrecoPromocional
                        : produto.produtoPreco);

                    var valorDesconto = (valorVenda - ((valorVenda / 100) * 15));

                    var porcentagemDesconto = Convert.ToInt32(100 - ((valorDesconto * 100) / valorCheio));


                    var categoriaSexo = (from c in dataCategoria.tbJuncaoProdutoCategoria
                                         where c.produtoId == produto.produtoId && c.tbProdutoCategoria.categoriaPaiId == 724
                                         select c).FirstOrDefault();
                    string sexo = "";
                    if (categoriaSexo != null)
                    {
                        sexo = categoriaSexo.tbProdutoCategoria.categoriaNomeExibicao;
                    }

                    var categoriaCor = (from c in dataCategoria.tbJuncaoProdutoCategoria
                                        where c.produtoId == produto.produtoId && c.tbProdutoCategoria.categoriaPaiId == 723
                                        select c).FirstOrDefault();
                    string cor = "";
                    if (categoriaCor != null)
                    {
                        cor = categoriaCor.tbProdutoCategoria.categoriaNomeExibicao;
                    }

                    string colecao = "";
                    var colecoes = (from c in dataCategoria.tbJuncaoProdutoColecao where c.produtoId == produto.produtoId select c).Distinct().ToList();
                    if (colecoes.Count() == 1)
                    {
                        colecao = colecoes.First().tbColecao.colecaoNome;
                    }

                    var categoriaTema = (from c in dataCategoria.tbJuncaoProdutoCategoria
                                         where c.produtoId == produto.produtoId && c.tbProdutoCategoria.categoriaPaiId == 725
                                         select c).FirstOrDefault();
                    string tema = "";
                    if (categoriaTema != null)
                    {
                        tema = categoriaTema.tbProdutoCategoria.categoriaNomeExibicao;
                    }

                    string linha = "";
                    linha += produto.produtoId + ";";
                    linha += rnConfiguracoes.caminhoCDN + "fotos/" + produto.produtoId + "/" + produto.tbProdutoFoto + ".jpg" + ";";
                    linha += produto.produtoNome + ";";
                    linha += rnConfiguracoes.caminhoVirtual + categoria.tbProdutoCategoria.categoriaUrl + "/" + produto.produtoUrl + "/" + ";";
                    linha += valorVenda.ToString("0.00").Replace(",", ".") + ";";
                    linha += categoria.categoriaId + ";";
                    linha += String.Format("{0:MM/dd/yyyy}", produto.produtoDataDaCriacao) + ";";
                    linha += sexo + ";";
                    linha += cor + ";";
                    linha += colecao + ";";
                    linha += tema + ";";
                    linha += valorDesconto.ToString("0.00").Replace(",", ".") + ";";
                    linha += parcelasMaximas + " de " + valorDaParcela.ToString("C") + ";";

                    sw.WriteLine(linha);
                }
            }

            sw.Close();

            File.Copy(ConfigurationManager.AppSettings["caminhoFisico"].ToString() + "targetingmantra_create.csv", ConfigurationManager.AppSettings["caminhoFisico"].ToString() + "targetingmantra.csv", true);
            File.Copy(ConfigurationManager.AppSettings["caminhoFisico"].ToString() + "targetingmantra_create.csv", rnConfiguracoes.caminhoXmls + "targetingmantra.csv", true);
        }
        public static void GerarRich(List<tbProdutos> produtos, List<tbJuncaoProdutoCategoria> categoriasProdutos, List<tbProdutoCategoria> categorias, List<tbCondicoesDePagamento> condicoesPagamento, List<tbMarca> marcas)
        {
            FileInfo product_full = new FileInfo(rnConfiguracoes.caminhoFisico + "product_full.txt");
            if (product_full.Exists)
            {
                product_full.Delete();
            }
            StreamWriter swProductFull = product_full.CreateText();
            swProductFull.WriteLine("product_id|name|price|recommendable|image_url|link_url");


            FileInfo product_attribute = new FileInfo(rnConfiguracoes.caminhoFisico + "product_attribute.txt");
            if (product_attribute.Exists)
            {
                product_attribute.Delete();
            }
            StreamWriter swProductAttribute = product_attribute.CreateText();
            swProductAttribute.WriteLine("product_id|attribute.siteid|attribute.cor|attribute.sexo|attribute.pecas|attribute.brindes|attribute.colecao|attribute.tema|attribute.prazo");


            foreach (var produto in produtos)
            {
                var dataCategoria = new dbSiteEntities();
                var categoria = (from c in dataCategoria.tbJuncaoProdutoCategoria
                                 where c.produtoId == produto.produtoId && c.tbProdutoCategoria.exibirSite == true && c.tbProdutoCategoria.categoriaPaiId == 0
                                 select c).FirstOrDefault();
                if (categoria != null)
                {
                    #region parcelamento
                    var produtoPrecoUtilizar = (produto.produtoPrecoPromocional > 0 &&
                                      produto.produtoPrecoPromocional < produto.produtoPreco
                        ? (decimal)produto.produtoPrecoPromocional
                        : produto.produtoPreco);


                    var condicao = condicoesPagamento.First(x => x.destaque.ToLower() == "true");
                    int parcelasSemJuros = condicao.parcelasSemJuros;
                    double taxaDeJuros = condicao.taxaDeJuros / 100;
                    decimal valorMinimoDaParcela = condicao.valorMinimoDaParcela;
                    decimal porcentagemDeDesconto = Convert.ToDecimal(condicao.porcentagemDeDesconto);
                    int numeroMaximoDeParcelas = condicao.numeroMaximoDeParcelas;
                    decimal valorDaParcela = 0;
                    int parcelasMaximas = 0;

                    for (int i = numeroMaximoDeParcelas; i <= numeroMaximoDeParcelas && i > 0; i--)
                    {
                        valorDaParcela = produtoPrecoUtilizar / i;

                        if (valorDaParcela >= valorMinimoDaParcela)
                        {
                            if (i > 1)
                            {
                                if (i <= parcelasSemJuros)
                                {
                                    parcelasMaximas = i;
                                }
                                break;
                            }
                        }
                    }

                    #endregion
                    var valorVenda = (produto.produtoPrecoPromocional > 0 &&
                                      produto.produtoPrecoPromocional < produto.produtoPreco
                        ? (decimal)produto.produtoPrecoPromocional
                        : produto.produtoPreco);
                    var valorCheio = (produto.produtoPrecoPromocional > produto.produtoPreco
                        ? (decimal)produto.produtoPrecoPromocional
                        : produto.produtoPreco);
                    var valorDesconto = (valorVenda - ((valorVenda / 100) * 15));
                    var porcentagemDesconto = Convert.ToInt32(100 - ((valorDesconto * 100) / valorCheio));

                    var categoriaSexo = (from c in dataCategoria.tbJuncaoProdutoCategoria
                                         where c.produtoId == produto.produtoId && c.tbProdutoCategoria.categoriaPaiId == 724
                                         select c).FirstOrDefault();
                    string sexo = "";
                    if (categoriaSexo != null)
                    {
                        sexo = categoriaSexo.tbProdutoCategoria.categoriaNomeExibicao;
                    }

                    var categoriaCor = (from c in dataCategoria.tbJuncaoProdutoCategoria
                                        where c.produtoId == produto.produtoId && c.tbProdutoCategoria.categoriaPaiId == 723
                                        select c).FirstOrDefault();
                    string cor = "";
                    if (categoriaCor != null)
                    {
                        cor = categoriaCor.tbProdutoCategoria.categoriaNomeExibicao;
                    }

                    var categoriaTema = (from c in dataCategoria.tbJuncaoProdutoCategoria
                                         where c.produtoId == produto.produtoId && c.tbProdutoCategoria.categoriaPaiId == 725
                                         select c).FirstOrDefault();
                    string tema = "";
                    if (categoriaTema != null)
                    {
                        tema = categoriaTema.tbProdutoCategoria.categoriaNomeExibicao;
                    }

                    string colecao = "";
                    var colecoes = (from c in dataCategoria.tbJuncaoProdutoColecao where c.produtoId == produto.produtoId select c).Distinct().ToList();
                    if (colecoes.Count() == 1)
                    {
                        colecao = colecoes.First().tbColecao.colecaoNome;
                    }

                    string linha = "";
                    linha += produto.produtoId + "|";
                    linha += produto.produtoNome + "|";
                    linha += valorVenda.ToString("0.00").Replace(",", ".") + "|";
                    linha += "true|";
                    linha += rnConfiguracoes.caminhoCDN + "fotos/" + produto.produtoId + "/" + produto.fotoDestaque + ".jpg" + "|";
                    linha += rnConfiguracoes.caminhoVirtualSSL + categoria.tbProdutoCategoria.categoriaUrl + "/" + produto.produtoUrl + "/";
                    swProductFull.WriteLine(linha);


                    string linhaAtributo = "";
                    linhaAtributo += produto.produtoId + "|";
                    linhaAtributo += produto.siteId + "|";
                    linhaAtributo += cor + "|";
                    linhaAtributo += sexo + "|";
                    linhaAtributo += produto.produtoPecas + "|";
                    linhaAtributo += produto.produtoBrindes + "|";
                    linhaAtributo += colecao + "|";
                    linhaAtributo += tema + "|";
                    linhaAtributo += produto.prazoDeEntrega;

                    swProductAttribute.WriteLine(linhaAtributo);
                }
            }
            swProductFull.Close();


            FileInfo category_full = new FileInfo(rnConfiguracoes.caminhoFisico + "category_full.txt");
            if (category_full.Exists)
            {
                category_full.Delete();
            }
            StreamWriter swCategoryFull = category_full.CreateText();
            swCategoryFull.WriteLine("category_id|parent_id|name");
            var categoriasAtivas = categorias.Where(x => x.categoriaPaiId == 0 && x.exibirSite == true);
            foreach (var categoriasAtiva in categoriasAtivas)
            {
                swCategoryFull.WriteLine(categoriasAtiva.categoriaId + "||" + categoriasAtiva.categoriaNomeExibicao);
            }

            FileInfo product_in_category = new FileInfo(rnConfiguracoes.caminhoFisico + "product_in_category.txt");
            if (product_in_category.Exists)
            {
                product_in_category.Delete();
            }
            StreamWriter swProductInCategory = product_in_category.CreateText();
            swProductInCategory.WriteLine("category_id|product_id");
            foreach (var categoriaProduto in categoriasProdutos)
            {
                swProductInCategory.WriteLine(categoriaProduto.categoriaId + "|" + categoriaProduto.produtoId);
            }


            swProductFull.Close();
            swCategoryFull.Close();
            swProductInCategory.Close();
            swProductAttribute.Close();

            #region Compacta txt's em Zip

            using (ZipFile zip = new ZipFile())
            {
                var filesToInclude = new System.Collections.Generic.List<String>();

                filesToInclude.Add(rnConfiguracoes.caminhoFisico + "product_in_category.txt");
                filesToInclude.Add(rnConfiguracoes.caminhoFisico + "product_full.txt");
                filesToInclude.Add(rnConfiguracoes.caminhoFisico + "product_attribute.txt");
                filesToInclude.Add(rnConfiguracoes.caminhoFisico + "category_full.txt");
                zip.AddFiles(filesToInclude, false, "");

                zip.Save(rnConfiguracoes.caminhoFisico + "catalog_full_graodegente.com.br_" + DateTime.Now.ToString("yyyyMMdd") + ".zip");

            }

            #endregion Compacta txt's em Zip

            #region Envia para Ftp da RichRelevance

            //Caminho do arquivo para upload
            FileInfo fileInf = new FileInfo(rnConfiguracoes.caminhoFisico + "catalog_full_graodegente.com.br_" + DateTime.Now.ToString("yyyyMMdd") + ".zip");
            //Cria comunicação com o servidor
            FtpWebRequest request = (FtpWebRequest)FtpWebRequest.Create(rnConfiguracoes.ftpRichRelevance + "catalog_full_graodegente.com.br_" + DateTime.Now.ToString("yyyyMMdd") + ".zip");
            //Define que a ação vai ser de upload
            request.Method = WebRequestMethods.Ftp.UploadFile;
            //Credenciais para o login (usuario, senha)
            request.Credentials = new NetworkCredential(rnConfiguracoes.usuarioFtpRichRelevance, rnConfiguracoes.senhaFtpRichRelevance);
            //modo passivo
            //request.UsePassive = true;
            //dados binarios
            request.UseBinary = true;
            //setar o KeepAlive para false
            request.KeepAlive = false;

            request.ContentLength = fileInf.Length;
            //cria a stream que será usada para mandar o arquivo via FTP
            Stream responseStream = request.GetRequestStream();
            byte[] buffer = new byte[2048];

            //Lê o arquivo de origem
            FileStream fs = fileInf.OpenRead();
            try
            {
                //Enquanto vai lendo o arquivo de origem, vai escrevendo no FTP
                int readCount = fs.Read(buffer, 0, buffer.Length);
                while (readCount > 0)
                {
                    //Esceve o arquivo
                    responseStream.Write(buffer, 0, readCount);
                    readCount = fs.Read(buffer, 0, buffer.Length);
                }
            }
            finally
            {
                fs.Close();
                responseStream.Close();
            }

            #endregion Envia para Ftp da RichRelevance
        }

        public static void GerarRichBanners(List<tbProdutos> produtos, List<tbJuncaoProdutoCategoria> categoriasProdutos, List<tbProdutoCategoria> categorias, List<tbCondicoesDePagamento> condicoesPagamento, List<tbMarca> marcas)
        {
            FileInfo content_full = new FileInfo(rnConfiguracoes.caminhoFisico + "content_full_graodegente.com.br_" + DateTime.Now.ToString("yyyy_MM_dd") + ".txt");
            if (content_full.Exists)
            {
                content_full.Delete();
            }
            StreamWriter swProductFull = content_full.CreateText();
            swProductFull.WriteLine("content_id|name|image_url|recommendable|link_url");


            FileInfo content_attribute = new FileInfo(rnConfiguracoes.caminhoFisico + "content_attribute_graodegente.com.br_" + DateTime.Now.ToString("yyyy_MM_dd") + ".txt");
            if (content_attribute.Exists)
            {
                content_attribute.Delete();
            }
            StreamWriter swProductAttribute = content_attribute.CreateText();
            swProductAttribute.WriteLine("content_id|attribute.content_id|attribute.local|attribute.posicao|attribute.produtoid|attribute.siteid|attribute.cor|attribute.sexo|attribute.pecas|attribute.brindes|attribute.colecao|attribute.tema|attribute.ASSET_URL|attribute.promo_url");

            FileInfo product_in_category = new FileInfo(rnConfiguracoes.caminhoFisico + "content_in_category_graodegente.com.br_" + DateTime.Now.ToString("yyyy_MM_dd") + ".txt");
            if (product_in_category.Exists)
            {
                product_in_category.Delete();
            }
            StreamWriter swProductInCategory = product_in_category.CreateText();
            swProductInCategory.WriteLine("category_id|content_id");


            var data = new dbSiteEntities();
            var banners = (from c in data.tbBanners where (c.ativo ?? false) == true select c).ToList();
            foreach (var banner in banners)
            {
                int produtoId = (banner.idProduto ?? 0);
                string linhaBanner = "";
                linhaBanner += banner.Id + "|";
                linhaBanner += banner.foto.Replace(".jpg", "") + "|";
                linhaBanner += "http://www.graodegente.com.br/banners/";
                if(banner.local == 1) linhaBanner += "home/";
                linhaBanner += banner.foto + "|";
                linhaBanner += "true|";
                linhaBanner += banner.linkProduto;
                swProductFull.WriteLine(linhaBanner);

                var produtosLista = (from c in produtos where c.produtoId == produtoId select c).ToList();
                if(produtoId == 0)
                {
                    var categorialink = banner.linkProduto.Replace("http://www.graodegente.com.br/", "");
                    if(categorialink.Split('/').Count() > 0)
                    {
                        string categoriaUrl = categorialink.Split('/')[0];
                        var categoriaDoDestino = (from c in categorias where c.categoriaUrl == categoriaUrl select c).FirstOrDefault();
                        if(categoriaDoDestino != null)
                        {
                            var listaCategoriasProdutos = (from c in data.tbJuncaoProdutoCategoria where c.categoriaId == categoriaDoDestino.categoriaId select c).ToList();
                            produtosLista = (from c in listaCategoriasProdutos
                                             join d in produtos on c.produtoId equals d.produtoId
                                                 where c.categoriaId == categoriaDoDestino.categoriaId
                                                 select d).ToList();
                        }
                    }
                }
                //var produto = (from c in produtos where c.produtoId == produtoId select c).FirstOrDefault();
                if (produtosLista.Count() > 0)
                {

                    foreach (var produto in produtosLista)
                    {
                        var categoriaSexo = (from c in data.tbJuncaoProdutoCategoria
                                             where c.produtoId == produto.produtoId && c.tbProdutoCategoria.categoriaPaiId == 724
                                             select c).FirstOrDefault();
                        string sexo = "";
                        if (categoriaSexo != null)
                        {
                            sexo = categoriaSexo.tbProdutoCategoria.categoriaNomeExibicao;
                        }

                        var categoriaCor = (from c in data.tbJuncaoProdutoCategoria
                                            where c.produtoId == produto.produtoId && c.tbProdutoCategoria.categoriaPaiId == 723
                                            select c).FirstOrDefault();
                        string cor = "";
                        if (categoriaCor != null)
                        {
                            cor = categoriaCor.tbProdutoCategoria.categoriaNomeExibicao;
                        }

                        var categoriaTema = (from c in data.tbJuncaoProdutoCategoria
                                             where c.produtoId == produto.produtoId && c.tbProdutoCategoria.categoriaPaiId == 725
                                             select c).FirstOrDefault();
                        string tema = "";
                        if (categoriaTema != null)
                        {
                            tema = categoriaTema.tbProdutoCategoria.categoriaNomeExibicao;
                        }

                        string colecao = "";
                        var colecoes =
                            (from c in data.tbJuncaoProdutoColecao where c.produtoId == produto.produtoId select c).Distinct
                                ().ToList();
                        if (colecoes.Count() == 1)
                        {
                            colecao = colecoes.First().tbColecao.colecaoNome;
                        }


                        string linhaAtributo = "";
                        linhaAtributo += banner.Id + "|";
                        linhaAtributo += banner.Id + "|";
                        linhaAtributo += banner.local + ((banner.mobile ?? false) ? "_m" : "_d") + "|";
                        linhaAtributo += banner.posicao + "|";
                        linhaAtributo += produto.produtoId + "|";
                        linhaAtributo += produto.siteId + "|";
                        linhaAtributo += cor + "|";
                        linhaAtributo += sexo + "|";
                        linhaAtributo += produto.produtoPecas + "|";
                        linhaAtributo += produto.produtoBrindes + "|";
                        linhaAtributo += colecao + "|";
                        linhaAtributo += tema + "|";
                        linhaAtributo += "http://www.graodegente.com.br/banners/";
                        if (banner.local == 1) linhaAtributo += "home/";
                        linhaAtributo += banner.foto + "|";
                        linhaAtributo += banner.linkProduto;
                        swProductAttribute.WriteLine(linhaAtributo);


                        var categoriasBanners = categoriasProdutos.Where(x => x.produtoId == produto.produtoId);
                        foreach (var categoriaProduto in categoriasBanners)
                        {
                            swProductInCategory.WriteLine(categoriaProduto.categoriaId + "|" + banner.Id);
                        }
                    }
                }
                else
                {
                    string linhaAtributo = "";
                    linhaAtributo += banner.Id + "|";
                    linhaAtributo += banner.local + "|";
                    linhaAtributo += banner.posicao + "|";
                    linhaAtributo += "|";
                    linhaAtributo += "|";
                    linhaAtributo +=  "|";
                    linhaAtributo += "|";
                    linhaAtributo += "|";
                    linhaAtributo +=  "|";
                    linhaAtributo += "|";
                    linhaAtributo += "http://www.graodegente.com.br/banners/";
                    if (banner.local == 1) linhaAtributo += "home/";
                    linhaAtributo += banner.foto + "|";
                    linhaAtributo += banner.linkProduto;
                    swProductAttribute.WriteLine(linhaAtributo);
                }
            }
            swProductFull.Close();
            swProductAttribute.Close();
            swProductInCategory.Close();

            #region Compacta txt's em Zip

            using (ZipFile zip = new ZipFile())
            {
                var filesToInclude = new System.Collections.Generic.List<String>();

                filesToInclude.Add(rnConfiguracoes.caminhoFisico + "content_in_category_graodegente.com.br_" + DateTime.Now.ToString("yyyy_MM_dd") + ".txt");
                filesToInclude.Add(rnConfiguracoes.caminhoFisico + "content_full_graodegente.com.br_" + DateTime.Now.ToString("yyyy_MM_dd") + ".txt");
                filesToInclude.Add(rnConfiguracoes.caminhoFisico + "content_attribute_graodegente.com.br_" + DateTime.Now.ToString("yyyy_MM_dd") + ".txt");
                zip.AddFiles(filesToInclude, false, "");

                zip.Save(rnConfiguracoes.caminhoFisico + "content_full_graodegente.com.br_" + DateTime.Now.ToString("yyyyMMdd") + ".zip");

            }

            #endregion Compacta txt's em Zip

            #region Envia para Ftp da RichRelevance

            //Caminho do arquivo para upload
            FileInfo fileInf = new FileInfo(rnConfiguracoes.caminhoFisico + "content_full_graodegente.com.br_" + DateTime.Now.ToString("yyyyMMdd") + ".zip");
            //Cria comunicação com o servidor
            FtpWebRequest request = (FtpWebRequest)FtpWebRequest.Create(rnConfiguracoes.ftpRichRelevance + "content_full_graodegente.com.br_" + DateTime.Now.ToString("yyyyMMdd") + ".zip");
            //Define que a ação vai ser de upload
            request.Method = WebRequestMethods.Ftp.UploadFile;
            //Credenciais para o login (usuario, senha)
            request.Credentials = new NetworkCredential(rnConfiguracoes.usuarioFtpRichRelevance, rnConfiguracoes.senhaFtpRichRelevance);
            //modo passivo
            //request.UsePassive = true;
            //dados binarios
            request.UseBinary = true;
            //setar o KeepAlive para false
            request.KeepAlive = false;

            request.ContentLength = fileInf.Length;
            //cria a stream que será usada para mandar o arquivo via FTP
            Stream responseStream = request.GetRequestStream();
            byte[] buffer = new byte[2048];

            //Lê o arquivo de origem
            FileStream fs = fileInf.OpenRead();
            try
            {
                //Enquanto vai lendo o arquivo de origem, vai escrevendo no FTP
                int readCount = fs.Read(buffer, 0, buffer.Length);
                while (readCount > 0)
                {
                    //Esceve o arquivo
                    responseStream.Write(buffer, 0, readCount);
                    readCount = fs.Read(buffer, 0, buffer.Length);
                }
            }
            finally
            {
                fs.Close();
                responseStream.Close();
            }

            #endregion Envia para Ftp da RichRelevance
        }

    }

}