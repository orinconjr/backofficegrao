//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace bcServerService
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbPedidoFornecedor
    {
        public tbPedidoFornecedor()
        {
            this.tbFornecedorExportacao = new HashSet<tbFornecedorExportacao>();
            this.tbPedidoFornecedorInteracao = new HashSet<tbPedidoFornecedorInteracao>();
            this.tbPedidoFornecedorItem = new HashSet<tbPedidoFornecedorItem>();
            this.tbProcessoFabricaHistorico = new HashSet<tbProcessoFabricaHistorico>();
        }
    
        public int idPedidoFornecedor { get; set; }
        public System.DateTime data { get; set; }
        public string usuario { get; set; }
        public int idFornecedor { get; set; }
        public System.DateTime dataLimite { get; set; }
        public Nullable<bool> confirmado { get; set; }
        public Nullable<bool> avulso { get; set; }
        public Nullable<bool> pendente { get; set; }
        public bool impresso { get; set; }
        public Nullable<System.DateTime> dataEntrega { get; set; }
        public Nullable<System.DateTime> dataVencimento { get; set; }
        public Nullable<System.DateTime> dataPagamento { get; set; }
        public string numeroCobranca { get; set; }
        public Nullable<System.DateTime> dataPrevista { get; set; }
        public Nullable<System.DateTime> dataPrimeiraEntrega { get; set; }
        public int idProcessoFabrica { get; set; }
        public Nullable<System.DateTime> dataProcessoFabrica { get; set; }
    
        public virtual ICollection<tbFornecedorExportacao> tbFornecedorExportacao { get; set; }
        public virtual ICollection<tbPedidoFornecedorInteracao> tbPedidoFornecedorInteracao { get; set; }
        public virtual ICollection<tbPedidoFornecedorItem> tbPedidoFornecedorItem { get; set; }
        public virtual ICollection<tbProcessoFabricaHistorico> tbProcessoFabricaHistorico { get; set; }
    }
}
