﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using HtmlAgilityPack;

namespace bcServerService
{
    public class rnDepoimentos
    {
        public class Review
        {
            public string opiniao { get; set; }
            public string nome { get; set; }
            public string titulo { get; set; }
            public string nota { get; set; }
            public string link { get; set; }
            public DateTime data { get; set; }

        }

        public static void GravarDepoimentosTrusted()
        {
            List<string> links = new List<string>();
            links.Add("http://trustedcompany.com/br/graodegente.com.br-opini%C3%B5es?rating=5");
            for (int i = 1; i <= 25; i++)
            {
                links.Add("http://trustedcompany.com/br/graodegente.com.br-opini%C3%B5es?page=" +  i + "&rating=5");
            }

            foreach (var linkDepoimento in links)
            {
                List<Review> reviewlist = new List<Review>();
                using (WebClient client = new WebClient())
                {
                    try
                    {
                        client.Encoding = System.Text.Encoding.UTF8;
                        string htmlCode =
                            client.DownloadString(linkDepoimento);
                        HtmlDocument document = new HtmlDocument();
                        document.LoadHtml(htmlCode);

                        var reviewobjects = (from d in document.DocumentNode.Descendants()
                            where d.Name == "div" && d.Attributes["class"].Value == "review  "
                            select d).Take(30);
                        foreach (var item in reviewobjects)
                        {
                            Review reviewobject = new Review();
                            HtmlNode titleNode = (from o in item.Descendants()
                                where o.Name == "h5" && o.Attributes["class"].Value == "title"
                                select o).First();

                            HtmlNode titleLinkNode = (from o in titleNode.Descendants()
                                where o.Name == "a"
                                select o).First();
                            var link = titleLinkNode.Attributes.First().Value.ToString();
                            var titulo = titleLinkNode.InnerText.ToString();
                            reviewobject.link = link;
                            reviewobject.titulo = titulo;



                            HtmlNode ratingNode = (from o in item.Descendants()
                                where o.Name == "div" && o.Attributes["class"].Value == "rating"
                                select o).First();


                            HtmlNode dateNode = (from o in ratingNode.Descendants()
                                where o.Name == "div" && o.Attributes["class"].Value == "created"
                                select o).First();
                            var dateNotFormated = dateNode.InnerText.ToString();
                            var datePart = dateNotFormated.Split(',')[1];
                            datePart = datePart.Replace("-", "").Replace("  ", " ");
                            var dataCompleta = Convert.ToDateTime(datePart);
                            reviewobject.data = dataCompleta;

                            var rating = (from o in ratingNode.Descendants()
                                where o.Name == "meta" && o.Attributes["itemprop"].Value == "ratingValue"
                                select o).First().Attributes["content"].Value.ToString();
                            reviewobject.nota = rating;

                            HtmlNode opiniao = (from o in item.Descendants()
                                where o.Name == "p" && o.Attributes["class"].Value == "body"
                                select o).First();
                            reviewobject.opiniao = opiniao.InnerText;
                            HtmlNode userNode = (from u in item.Descendants()
                                where u.Name == "div" && u.Attributes["class"].Value == "user"
                                select u).First();




                            try
                            {
                                HtmlNode nome = (from n in userNode.Descendants()
                                    where n.Name == "div" && n.Attributes["class"].Value == "user__name"
                                    select n).First().Descendants().First();

                                reviewobject.nome = nome.InnerHtml;
                            }
                            catch (Exception)
                            {
                            }

                            reviewlist.Add(reviewobject);
                        }
                    }
                    catch (Exception wx)
                    {

                    }
                }

                foreach (var review in reviewlist)
                {
                    var data = new dbSiteEntities();
                    if (!string.IsNullOrEmpty(review.link))
                    {
                        var reviewCheck =
                            (from c in data.tbDepoimentoExterno
                             where c.idDepoimentoOrigem == 1 && c.link == review.link
                             select c).Any();
                        if (!reviewCheck)
                        {
                            var dataAdd = new dbSiteEntities();
                            var depoimento = new tbDepoimentoExterno();
                            depoimento.aprovado = false;
                            depoimento.data = review.data;
                            depoimento.idDepoimentoOrigem = 1;
                            depoimento.link = review.link;
                            depoimento.nome = review.nome;
                            depoimento.nota = review.nota;
                            depoimento.opiniao = review.opiniao;
                            depoimento.titulo = review.titulo.Replace("\"", "");
                            dataAdd.tbDepoimentoExterno.Add(depoimento);
                            dataAdd.SaveChanges();
                        }
                    }
                }
            }
        }

        public static void GravarDepoimentosFace()
        {
            var data = new dbSiteEntities();
            var depoimentos = (from c in data.tbDepoimento orderby c.idDepoimento descending select c).ToList();
            foreach (var depoimento in depoimentos)
            {
                try
                {
                    var wImportacao = new HtmlWeb();
                    var wDocumento = wImportacao.Load(depoimento.url);
                    foreach (HtmlNode conteudo in wDocumento.DocumentNode.SelectNodes("//code[1]"))
                    {
                        HtmlAgilityPack.HtmlDocument faceDocument = new HtmlAgilityPack.HtmlDocument();
                        faceDocument.LoadHtml(conteudo.InnerHtml.Replace("<!--", "").Replace("-->", ""));
                        string dataPostagem = "";
                        string linkPostagem = "";
                        foreach (HtmlNode div in faceDocument.DocumentNode.SelectNodes("//span[contains(@class,'timestampContent')]"))
                        {
                            dataPostagem = div.InnerText;
                            linkPostagem = div.ParentNode.ParentNode.Attributes["href"].Value.Replace("/graodegente/posts/", "http://www.facebook.com/graodegente/posts/");
                        }

                        string textoDepoimento = "";
                        foreach (HtmlNode div in faceDocument.DocumentNode.SelectNodes("//div[contains(@class,'userContent')]"))
                        {
                            textoDepoimento = div.InnerText;
                        }

                        string imgDepoimento = "";
                        foreach (HtmlNode div in faceDocument.DocumentNode.SelectNodes("//img[contains(@class,'img')]"))
                        {
                            if(imgDepoimento == "") imgDepoimento = div.Attributes["src"].Value;
                        }

                        string nomeDepoimento = "";
                        foreach (HtmlNode div in faceDocument.DocumentNode.SelectNodes("//span[contains(@class,'fwb')]//a"))
                        {
                            string nome = div.InnerText;
                            if (nome == "Grão de Gente") nome = "";
                            if (!string.IsNullOrEmpty(nome) && string.IsNullOrEmpty(nomeDepoimento)) nomeDepoimento = nome;
                        }

                        if (string.IsNullOrEmpty(nomeDepoimento))
                        {
                            try
                            {
                                foreach (HtmlNode div in faceDocument.DocumentNode.SelectNodes("//span[contains(@class,'fwb')]//div//span"))
                                {
                                    string nome = div.InnerText;
                                    if (nome == "Grão de Gente") nome = "";
                                    if (!string.IsNullOrEmpty(nome) && string.IsNullOrEmpty(nomeDepoimento)) nomeDepoimento = nome;
                                }
                            }
                            catch (Exception)
                            {

                            }
                        }

                        var dataAlt = new dbSiteEntities();
                        var depoimentoAlt =
                            (from c in dataAlt.tbDepoimento where c.idDepoimento == depoimento.idDepoimento select c).First
                                ();
                        depoimentoAlt.data = dataPostagem;
                        depoimentoAlt.texto = textoDepoimento;
                        depoimentoAlt.foto = imgDepoimento;
                        depoimentoAlt.nome = nomeDepoimento;
                        dataAlt.SaveChanges();
                    }
                }
                catch (Exception ex)
                {

                }
            }
        }
    }
}
