﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace bcServerService
{
    class rnLog
    {
        public rnLog()
        {
            descricoes = new List<string>();
            tiposRelacionados = new List<rnEnums.TipoRelacionadoIds>();
            tiposOperacao = new List<rnEnums.TipoOperacao>();
        }

        public string usuario { get; set; }
        public List<string> descricoes { get; set; }
        public List<rnEnums.TipoRelacionadoIds> tiposRelacionados { get; set; }
        public List<rnEnums.TipoOperacao> tiposOperacao { get; set; }
        public void InsereLog()
        {
            try
            {
                using (var db = new dbSiteEntities())
                {
                    var log = new tbLog();
                    log.data = DateTime.Now;
                    log.usuario = usuario;
                    db.tbLog.Add(log);
                    db.SaveChanges();

                    foreach (var descricao in descricoes)
                    {
                        var logDescricao = new tbLogDescricao();
                        logDescricao.idLog = log.idLog;
                        logDescricao.descricao = descricao;
                        db.tbLogDescricao.Add(logDescricao);
                    }

                    foreach (var tipoRelacionado in tiposRelacionados)
                    {
                        var logRegistroRelacionado = new tbLogRegistroRelacionado();
                        logRegistroRelacionado.idLog = log.idLog;
                        logRegistroRelacionado.idRegistroRelacionado = tipoRelacionado.idRegistroRelacionado;
                        logRegistroRelacionado.tipoRegistroRelacionado = (int)tipoRelacionado.tipoRelacionado;
                        db.tbLogRegistroRelacionado.Add(logRegistroRelacionado);
                    }

                    foreach (var tipoOperacao in tiposOperacao)
                    {
                        var logTipo = new tbLogTipo();
                        logTipo.idLog = log.idLog;
                        logTipo.tipo = (int)tipoOperacao;
                        db.tbLogTipo.Add(logTipo);
                    }
                    db.SaveChanges();
                }

            }
            catch (Exception)
            {

            }
        }
    }
}
