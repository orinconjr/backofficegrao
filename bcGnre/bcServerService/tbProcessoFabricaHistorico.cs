//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace bcServerService
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbProcessoFabricaHistorico
    {
        public int idProcessoFabricaHistorico { get; set; }
        public int idPedidoFornecedor { get; set; }
        public Nullable<int> idPedidoFornecedorItem { get; set; }
        public int idProcessoFabrica { get; set; }
        public System.DateTime dataProcesso { get; set; }
        public string nomeUsuario { get; set; }
    
        public virtual tbPedidoFornecedor tbPedidoFornecedor { get; set; }
        public virtual tbPedidoFornecedorItem tbPedidoFornecedorItem { get; set; }
    }
}
