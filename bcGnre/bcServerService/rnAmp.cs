﻿using Amazon;
using Amazon.Runtime;
using Amazon.S3.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bcServerService
{
    public class rnAmp
    {
        public static void GerarAmp(ElasticSearch.Produto produto)
        {
            string amp = carregarTemplateArquivo(rnConfiguracoes.caminhoFisico + "\\amp\\" + "amp.html");
            string brinde = carregarTemplateArquivo(rnConfiguracoes.caminhoFisico + "\\amp\\" + "brinde.html");
            string colecao = carregarTemplateArquivo(rnConfiguracoes.caminhoFisico + "\\amp\\" + "colecao.html");
            string css360 = carregarTemplateArquivo(rnConfiguracoes.caminhoFisico + "\\amp\\" + "css360.html");
            string cssespecificacoes = carregarTemplateArquivo(rnConfiguracoes.caminhoFisico + "\\amp\\" + "cssespecificacoes.html");
            string dots = carregarTemplateArquivo(rnConfiguracoes.caminhoFisico + "\\amp\\" + "dots.html");
            string especificacaoitem = carregarTemplateArquivo(rnConfiguracoes.caminhoFisico + "\\amp\\" + "especificacaoitem.html");
            string especificacaoitemproduto = carregarTemplateArquivo(rnConfiguracoes.caminhoFisico + "\\amp\\" + "especificacaoitemproduto.html");
            string especificacoes = carregarTemplateArquivo(rnConfiguracoes.caminhoFisico + "\\amp\\" + "especificacoes.html");
            string foto = carregarTemplateArquivo(rnConfiguracoes.caminhoFisico + "\\amp\\" + "foto.html");
            string pecas = carregarTemplateArquivo(rnConfiguracoes.caminhoFisico + "\\amp\\" + "pecas.html");
            string t360 = carregarTemplateArquivo(rnConfiguracoes.caminhoFisico + "\\amp\\" + "t360.html");
            string video = carregarTemplateArquivo(rnConfiguracoes.caminhoFisico + "\\amp\\" + "video.html");
            string videotag = carregarTemplateArquivo(rnConfiguracoes.caminhoFisico + "\\amp\\" + "videotag.html");


            amp = amp.Replace("{{categoriaurl}}", produto.categoriaurl);
            amp = amp.Replace("{{produtourl}}", produto.produtourl);
            amp = amp.Replace("{{produtonome}}", produto.nome);
            amp = amp.Replace("{{produtodescricao}}", produto.produtoDescricao);
            amp = amp.Replace("{{produtoid}}", produto.id.ToString());

            string fotos = "";
            foreach(var produtofoto in produto.fotos)
            {
                string fotoTemplate = foto;
                fotoTemplate = fotoTemplate.Replace("{{categoriaurl}}", produto.categoriaurl);
                fotoTemplate = fotoTemplate.Replace("{{produtourl}", produto.produtourl);
                fotoTemplate = fotoTemplate.Replace("{{produtonome}", produto.nome);
                fotoTemplate = fotoTemplate.Replace("{{produtofoto}}", produtofoto.foto);
                fotoTemplate = fotoTemplate.Replace("{{produtoid}}", produto.id.ToString());
                fotos += fotoTemplate;
            }
            amp = amp.Replace("{{fotos}}", fotos);

            string videos = "";
            foreach (var produtovideo in produto.videos)
            {
                string videoTemplate = video;
                videoTemplate = videoTemplate.Replace("{{videoid}}", produtovideo.link.Split('?')[1].Replace("v=", ""));
                videos += videoTemplate;
            }
            amp = amp.Replace("{{videos}}", videos);
            if (!string.IsNullOrEmpty(videos))
            {
                amp = amp.Replace("{{videotag}}", videotag);
            }
            else
            {
                amp = amp.Replace("{{videotag}}", "");
            }
            string dotsHtml = "";
            for(int i = 1; i <= produto.fotos.Count; i++)
            {
                var dotTemplate = dots;
                dotTemplate = dotTemplate.Replace("{{numero}}", i.ToString());
                dotsHtml += dotTemplate;
            }
            amp = amp.Replace("{{dots}}", dotsHtml);

            string templateCss360 = "";
            string template360 = "";
            if (!string.IsNullOrEmpty(produto.foto360))
            {
                template360 = t360;
                templateCss360 = css360.Replace("{{produtoid}}", produto.id.ToString());
            }

            amp = amp.Replace("{{t360}}", template360);
            amp = amp.Replace("{{css360}}", templateCss360);

            string tEspecificacoes = "";
            string tCssEspecificacoes = "";
            if (produto.Especificacoes.Count > 0)
            {
                string templateEspecificacoes = especificacoes;
                string templateCssEspecificacoes = cssespecificacoes;
                string templateCssItemEspecificacoes = "";
                string templateEspecificaoItens = "";

                foreach (var especificacao in produto.Especificacoes)
                {
                    string templateEspecificacaoItem = especificacaoitem;
                    templateEspecificacaoItem = templateEspecificacaoItem.Replace("{{especificacaonome}}", especificacao.Nome);

                    string templateEspecificaoItemProdutos = "";
                    foreach(var especificacaoItem in especificacao.especificacaoItens.OrderBy(x => x.ordem))
                    {
                        string templateEspecificacaoItemProduto = especificacaoitemproduto;
                        templateEspecificacaoItemProduto = templateEspecificacaoItemProduto.Replace("{{categoriaurl}}", produto.categoriaurl);
                        templateEspecificacaoItemProduto = templateEspecificacaoItemProduto.Replace("{{produtourl}}", especificacaoItem.produtoUrl);
                        templateEspecificacaoItemProduto = templateEspecificacaoItemProduto.Replace("{{produtoid}}", especificacaoItem.produtoId.ToString());
                        templateEspecificacaoItemProduto = templateEspecificacaoItemProduto.Replace("{{especificacaoitemnome}}", especificacaoItem.nome);
                        if(especificacaoItem.produtoId == produto.id)
                        {
                            templateEspecificacaoItem = templateEspecificacaoItem.Replace("{{especificacaoatual}}", especificacao.Nome);
                        }
                        templateCssItemEspecificacoes += "#item" + especificacaoItem.produtoId.ToString() + "{ " + especificacaoItem.estilo + " } ";
                        templateEspecificaoItemProdutos += templateEspecificacaoItemProduto;
                    }
                    templateEspecificacaoItem = templateEspecificacaoItem.Replace("{{especificacaoitemproduto}}", templateEspecificaoItemProdutos);

                    templateEspecificaoItens += templateEspecificacaoItem;
                }

                templateCssEspecificacoes = templateCssEspecificacoes.Replace("{{cssitemespecificacao}}", templateCssItemEspecificacoes);
                templateEspecificacoes = templateEspecificacoes.Replace("{{especificacaoitem}}", templateEspecificaoItens);

                tEspecificacoes = templateEspecificacoes;
                tCssEspecificacoes = templateCssEspecificacoes;
            }


            amp = amp.Replace("{{cssespecificacoes}}", tCssEspecificacoes);
            amp = amp.Replace("{{especificacoes}}", tEspecificacoes);

            string tPecas = "";
            if(produto.pecas > 0)
            {
                tPecas = pecas.Replace("{{pecas}}", produto.pecas.ToString());
            }
            amp = amp.Replace("{{pecas}}", tPecas);
            amp = amp.Replace("{{precode}}", produto.preco.ToString("0.00"));
            double precoAVista = produto.preco;
            if(produto.precopromocional > 0 && produto.precopromocional < produto.preco)
            {
                precoAVista = produto.precopromocional;
            }
            precoAVista = (precoAVista / 100) * 85;

            amp = amp.Replace("{{precoavista}}", precoAVista.ToString("0.00"));
            amp = amp.Replace("{{precoavistameta}}", precoAVista.ToString("0.00").Replace(",", "."));
            amp = amp.Replace("{{porcentagemoff}}", Convert.ToInt32(produto.descontototalavista).ToString());
            amp = amp.Replace("{{valorcartao}}", ((produto.precopromocional > 0 && produto.precopromocional < produto.preco) ? produto.precopromocional : produto.preco).ToString("0.00"));
            amp = amp.Replace("{{quantidadeparcela}}", Convert.ToInt32(produto.parcelamentomaximo).ToString());
            amp = amp.Replace("{{valorparcela}}", produto.parcelamentomaximovalorparcela.ToString("0.00"));

            string tBrinde = "";
            if (produto.brindes > 0)
            {
                tBrinde = brinde.Replace("{{brindes}}", produto.brindes.ToString());
            }
            amp = amp.Replace("{{brinde}}", tBrinde);

            string tColecao = "";
            if (produto.colecaoids.Count == 1)
            {
                tColecao = colecao.Replace("{{categoriaurl}}", produto.categoriaurl).Replace("{{produtourl}}", produto.produtourl);
            }
            amp = amp.Replace("{{colecao}}", tColecao);

            string nomeArquivo = produto.categoriaurl + "/" + produto.produtourl + ".html";
            SaveOnS3(amp, nomeArquivo);
        }


        private static void SaveOnS3(string html, string arquivo)
        {
                var credentials = new BasicAWSCredentials(ConfigurationManager.AppSettings["accessKey"].ToString(), ConfigurationManager.AppSettings["secretAccessKey"].ToString());
                var s3Client = new Amazon.S3.AmazonS3Client(credentials, RegionEndpoint.SAEast1);
                string bucketUrl = "amp.graodegente.com.br";            

                PutObjectRequest putRequestHtml = new PutObjectRequest
                {
                    BucketName = bucketUrl,
                    Key = arquivo,
                    ContentType = "text/html",
                    ContentBody = html,
                    CannedACL = Amazon.S3.S3CannedACL.PublicRead
                };
                PutObjectResponse responseHtml = s3Client.PutObject(putRequestHtml);        
        }


        public static string carregarTemplateArquivo(string pathArquivo)
        {
            try
            {
                if (!File.Exists(pathArquivo)) throw new Exception(String.Format("Arquivo '{0}' não encontrado!", pathArquivo));

                return File.ReadAllText(pathArquivo, Encoding.GetEncoding("ISO-8859-1"));
            }
            catch (Exception ex)
            {
                return "";
            }
        }
    }
}
