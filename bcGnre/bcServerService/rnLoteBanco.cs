﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace bcServerService
{
    public class rnLoteBanco
    {
        public static void GerarLote()
        {
            var data = new dbSiteEntities();
            int diaDaSemana = (int)DateTime.Now.DayOfWeek;
            var feriado = (from c in data.tbFeriados where c.data.Year == DateTime.Now.Year && c.data.Month == DateTime.Now.Month && c.data.Day == DateTime.Now.Day select c).Any();
            if (!feriado && diaDaSemana != 0 && diaDaSemana != 6)
            {
                var notas =
                    (from c in data.tbNotaFiscal where c.statusGnre == 2 && (c.statusPagamentoGnre ?? 0) == 0 select c)
                        .ToList();
                if (notas.Count > 0)
                {
                    int totalBoletos = 0;
                    decimal valorTotal = 0;
                    int registroAtual = 0;
                    string lote = "34100000      081210924051000163                    02973 000000015805 4LINDSAY FERRANDO ME           ITAU                                    12201201614550000000000000000                                                                     ";
                    lote += Environment.NewLine;
                    lote += "34100011C2291030 210924051000163                    02973 000000015805 4LINDSAY FERRANDO ME                                                   AV ANA COSTA                  00416CJ 21          SANTOS              11060002SP                  ";
                    lote += Environment.NewLine;
                    List<int> notasGeradas = new List<int>();

                    foreach (var nota in notas)
                    {
                        if (!string.IsNullOrEmpty(nota.resultado))
                        {
                            bool gerarPagamento = false;
                            var pedido = (from c in data.tbPedidos where c.pedidoId == nota.idPedido select c).FirstOrDefault();
                            var notaLiberarPedido = rnNotaFiscal.DefineNotaLiberarPedido(pedido.pedidoId,
                                pedido.endEstado.ToLower());
                            if (!notaLiberarPedido) gerarPagamento = true;
                            gerarPagamento = true;

                            if (gerarPagamento)
                            {
                                using (StringReader reader = new StringReader(nota.resultado))
                                {
                                    string line;
                                    while ((line = reader.ReadLine()) != null)
                                    {
                                        if (line.StartsWith("1"))
                                        {
                                            notasGeradas.Add(nota.idNotaFiscal);

                                            var registro = new RegistrodeGuia(line);
                                            registroAtual ++;
                                            totalBoletos++;
                                            valorTotal += registro.ValorPrincipal;
                                            var hoje = DateTime.Now;
                                            lote +=
                                                String.Format(
                                                    "34100013{0:00000}O000{1}GNRE                          {2:00}{3:00}{4:0000}REA000000000000000{5:0000000000000}{6:00}{7:00}{8:00}{9:0000}000000000000000               {10:00000000000000000000}                                              ",
                                                    registroAtual, registro.RepresentacaooNumerica,
                                                    registro.DataDeVencimento.Day, registro.DataDeVencimento.Month,
                                                    registro.DataDeVencimento.Year,
                                                    Convert.ToInt32(
                                                        registro.ValorPrincipal.ToString("0.00").Split(',')[0]),
                                                    Convert.ToInt32(
                                                        registro.ValorPrincipal.ToString("0.00").Split(',')[1]),
                                                    hoje.Day, hoje.Month, hoje.Year, nota.idNotaFiscal);
                                            lote += Environment.NewLine;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    lote += String.Format("34100015         {0:000000}{1:0000000000000000}{2:00}00000000000000000000000000000000000000                                                                                                                                                                 ", (totalBoletos + 2), Convert.ToInt32(valorTotal.ToString("0.00").Split(',')[0]), Convert.ToInt32(valorTotal.ToString("0.00").Split(',')[1]));
                    lote += Environment.NewLine;
                    lote += String.Format("34199999         000001{0:000000}                                                                                                                                                                                                                   ", (totalBoletos + 4));
                    lote += Environment.NewLine;

                    if (totalBoletos > 0)
                    {
                        var loteBanco = new tbLoteBanco();
                        loteBanco.dataHora = DateTime.Now;
                        loteBanco.arquivoLote = lote;
                        loteBanco.arquivoBaixado = false;
                        loteBanco.totalBoletos = totalBoletos;
                        loteBanco.valorTotal = valorTotal;
                        data.tbLoteBanco.Add(loteBanco);
                        data.SaveChanges();
                        foreach (var idNota in notasGeradas)
                        {
                            var notaDetalhe =
                                (from c in data.tbNotaFiscal where c.idNotaFiscal == idNota select c).First();
                            notaDetalhe.statusPagamentoGnre = 1;
                            notaDetalhe.idLoteBanco = loteBanco.idLoteBanco;
                            data.SaveChanges();
                        }
                    }
                }
                string teste = "";
            }
        }
    }
}
