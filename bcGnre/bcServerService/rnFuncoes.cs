﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;

namespace bcServerService
{
    class rnFuncoes
    {
        public static string limpaString(string texto)
        {
            string caracteresEspeciais = " ÄÅÁÂÀÃäáâàãÉÊËÈéêëèÍÎÏÌíîïìÖÓÔÒÕöóôòõÜÚÛüúûùÇç/`~!@#$%^&*()*+;:',<.>?|";
            string caracteresNormais = "-AAAAAAaaaaaEEEEeeeeIIIIiiiiOOOOOoooooUUUuuuuCc------------------------";

            for (int i = 0; i < caracteresEspeciais.Length; i++)
            {
                try
                {
                    texto = texto.Replace(caracteresEspeciais[i].ToString(), caracteresNormais[i].ToString());
                    texto = texto.Replace("\"", "");
                    texto = texto.Replace("'", "");
                }
                catch (Exception ex) { }
            }

            texto = RemoveAcentosComNormalizacao(texto).Replace("------", "-").Replace("-----", "-").Replace("----", "-").Replace("---", "-").Replace("--", "-");
            texto = texto.Trim('-');
            return texto;
        }


        public static string RemoveAcentosComNormalizacao(string inputString)
        {
            if ((inputString == "") || (inputString == null))
                return "";

            string normalizedString = inputString.Normalize(NormalizationForm.FormD);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < normalizedString.Length; i++)
            {
                UnicodeCategory uc = CharUnicodeInfo.GetUnicodeCategory(normalizedString[i]);
                if (uc != UnicodeCategory.NonSpacingMark)
                {
                    sb.Append(normalizedString[i]);
                }
            }
            return (sb.ToString().Normalize(NormalizationForm.FormC));
        }

        public static string limpaStringDeixaEspaco(string texto)
        {
            string caracteresEspeciais = "ÄÅÁÂÀÃäáâàãÉÊËÈéêëèÍÎÏÌíîïìÖÓÔÒÕöóôòõÜÚÛüúûùÇç/`~!@#$%^&*()*+;:',<.>?|";
            string caracteresNormais = "AAAAAAaaaaaEEEEeeeeIIIIiiiiOOOOOoooooUUUuuuuCc------------------------";

            for (int i = 0; i < caracteresEspeciais.Length; i++)
            {
                try
                {
                    texto = texto.Replace(caracteresEspeciais[i].ToString(), caracteresNormais[i].ToString());
                    texto = texto.Replace("\"", "");
                    texto = texto.Replace("'", "");
                }
                catch (Exception ex) { }
            }
            return texto;
        }

        public static string limpaStringDeixaPonto(string texto)
        {
            string caracteresEspeciais = " ÄÅÁÂÀÃäáâàãÉÊËÈéêëèÍÎÏÌíîïìÖÓÔÒÕöóôòõÜÚÛüúûùÇç/`~!@#$%^&*()*+;:',<>?|";
            string caracteresNormais = "-AAAAAAaaaaaEEEEeeeeIIIIiiiiOOOOOoooooUUUuuuuCc-----------------------";

            for (int i = 0; i < caracteresEspeciais.Length; i++)
            {
                texto = texto.Replace(caracteresEspeciais[i].ToString(), caracteresNormais[i].ToString());
                texto = texto.Replace("\"", "");
                texto = texto.Replace("'", "");
            }
            return texto;
        }

        public static string removeAcentos(string inputString)
        {
            if ((inputString == "") || (inputString == null))
                return "";

            string normalizedString = inputString.Normalize(NormalizationForm.FormD);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < normalizedString.Length; i++)
            {
                UnicodeCategory uc = CharUnicodeInfo.GetUnicodeCategory(normalizedString[i]);
                if (uc != UnicodeCategory.NonSpacingMark)
                {
                    sb.Append(normalizedString[i]);
                }
            }
            return (sb.ToString().Normalize(NormalizationForm.FormC));
        }

        public static string removeCaracteresEspeciais(string inputString)
        {
            StringBuilder sb = new StringBuilder();
            foreach (char c in inputString)
            {
                if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') | c == '.' || c == '_' || c == '-')
                {
                    sb.Append(c);
                }
            }
            return sb.ToString();
        }
        public static string removeCaracteresEspeciaisDeixaEspaco(string inputString)
        {
            StringBuilder sb = new StringBuilder();
            foreach (char c in inputString)
            {
                if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') | c == '.' || c == '_' || c == '-' || c == ' ')
                {
                    sb.Append(c);
                }
            }
            return sb.ToString();
        }
        public static int retornaIdCliente(int idPedido)
        {
            string magicNumber = "110010000011001111111100011101";
            var binario = Convert.ToString(idPedido, 2);
            var binarioCompleto = binario.PadLeft(30, '0');
            var binarioInvertido = ReverseString(binarioCompleto);
            var xor = xorIt(magicNumber, binarioInvertido).ToString();
            int idPedidoFinal = Convert.ToInt32(xor, 2);
            return idPedidoFinal;
        }

        public static int retornaIdInterno(int idCliente)
        {
            string magicNumber = "110010000011001111111100011101";
            var binarioXor = Convert.ToString(idCliente, 2).PadLeft(30, '0');
            var invertido = xorIt(magicNumber, binarioXor);
            var binario = ReverseString(invertido);
            int idPedido = Convert.ToInt32(binario, 2);
            return idPedido;
        }

        public static string ReverseString(string s)
        {
            char[] charArray = s.ToCharArray();
            Array.Reverse(charArray);
            return new string(charArray);
        }

        public byte[] bytesArray(string input)
        {
            var bytesAsStrings = input.Select((c, i) => new { Char = c, Index = i }).GroupBy(x => x.Index / 8).Select(g => new string(g.Select(x => x.Char).ToArray()));
            byte[] bytes = bytesAsStrings.Select(s => Convert.ToByte(s, 2)).ToArray();
            return bytes;
        }

        public static string xorIt(string key, string input)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < input.Length; i++)
            {
                var xor = Convert.ToInt32(input[i]) ^ Convert.ToInt32(key[i]);
                sb.Append(xor);
            }
            var result = sb.ToString();

            return result;
        }


        public static string carregarArquivoHtml(string pathArquivo)
        {
            try
            {
                if (!File.Exists(pathArquivo))
                    throw new Exception(String.Format("Arquivo '{0}' não encontrado!", pathArquivo));

                return File.ReadAllText(pathArquivo, Encoding.GetEncoding("ISO-8859-1"));
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        public static List<tbFeriados> listaFeriados(bool BypassCache)
        {

            var data = new dbSiteEntities();
            var feriados = (from c in data.tbFeriados select c).ToList();
            return feriados;
        }

        public static int retornaPrazoDiasUteis(int prazo, int prazoInicioFabricacao)
        {
            return retornaPrazoDiasUteis(prazo, prazoInicioFabricacao, DateTime.Now);
        }

        public static int retornaPrazoDiasUteis(int prazo, int prazoInicioFabricacao, DateTime dataInicio)
        {
            int dias = 0;
            int prazoTotal = prazo;
            var feriados = listaFeriados(false);
            if (feriados.Any(x => x.data.Date < dataInicio.Date)) feriados = listaFeriados(true);

            for (int i = 1 + prazoInicioFabricacao; i <= prazoTotal + prazoInicioFabricacao; i++)
            {
                dias++;
                bool fimDeSemana = dataInicio.AddDays(i + prazoInicioFabricacao).DayOfWeek == DayOfWeek.Saturday |
                               dataInicio.AddDays(i + prazoInicioFabricacao).DayOfWeek == DayOfWeek.Sunday;
                bool feriado = (from c in feriados where c.data.Date == dataInicio.AddDays(i + prazoInicioFabricacao).Date select c).Any();
                if (fimDeSemana | feriado) prazoTotal++;
            }

            return dias + prazoInicioFabricacao;
        }

        public static int retornaDiasUteisPeriodo(DateTime dataInicio, DateTime dataFim)
        {
            int dias = 0;
            int prazoTotal = 0;
            var feriados = listaFeriados(false);
            if (feriados.Any(x => x.data.Date < dataInicio.Date)) feriados = listaFeriados(true);
            bool possuiDias = true;
            DateTime dataAtual = dataInicio.AddDays(1);
            if (dataAtual.Date < dataFim.Date)
            {
                while (possuiDias)
                {
                    if (dataAtual.Date < dataFim)
                    {
                        bool fimDeSemana = dataAtual.DayOfWeek == DayOfWeek.Saturday |
                                       dataAtual.DayOfWeek == DayOfWeek.Sunday;
                        bool feriado = (from c in feriados where c.data.Date == dataAtual.Date select c).Any();
                        if (!fimDeSemana && !feriado) dias++;
                        dataAtual = dataAtual.AddDays(1);
                    }
                    else
                    {
                        possuiDias = false;
                    }
                }
            }

            return dias;
        }


        public static string gerarMd5(string input)
        {
            using (System.Security.Cryptography.MD5 md5Hash = System.Security.Cryptography.MD5.Create())
            {
                byte[] data = md5Hash.ComputeHash(System.Text.Encoding.UTF8.GetBytes(input));
                System.Text.StringBuilder sBuilder = new System.Text.StringBuilder();
                for (int i = 0; i < data.Length; i++)
                {
                    sBuilder.Append(data[i].ToString("x2"));
                }
                return sBuilder.ToString();
            }
        }

        public static string UppercaseFirst(string s)
        {
            // Check for empty string.
            if (string.IsNullOrEmpty(s))
            {
                return string.Empty;
            }
            // Return char and concat substring.
            return char.ToUpper(s[0]) + s.Substring(1);
        }

        public static void AgendarAtualizacaoProduto(int produtoId, DateTime agendamento)
        {
            var dataInserUpdate = new dbSiteEntities();
            var queueUpdate = new tbQueue();
            queueUpdate.agendamento = agendamento;
            queueUpdate.tipoQueue = 15;
            queueUpdate.idRelacionado = produtoId;
            queueUpdate.mensagem = "";
            queueUpdate.andamento = false;
            queueUpdate.concluido = false;
            dataInserUpdate.tbQueue.Add(queueUpdate);
            dataInserUpdate.SaveChanges();
        }

        public static string sha256(string valor)
        {
            return valor;
            UnicodeEncoding UE = new UnicodeEncoding();
            byte[] HashValue, MessageBytes = UE.GetBytes(valor);
            System.Security.Cryptography.SHA256Managed SHhash = new System.Security.Cryptography.SHA256Managed();
            string strHex = "";

            HashValue = SHhash.ComputeHash(MessageBytes);
            foreach (byte b in HashValue)
            {
                strHex += String.Format("{0:x2}", b);
            }
            return strHex;
        }

    }
}
