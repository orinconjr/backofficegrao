﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace bcServerService
{

    public class DadosEmissaoGnre
    {
        public class c03_idContribuinteEmitente
        {
            public string CNPJ { get; set; }
            public string CPF { get; set; }
        }
        public class c35_idContribuinteDestinatario
        {
            public string CNPJ { get; set; }
            public string CPF { get; set; }
        }
        public class campoExtra
        {
            public string codigo { get; set; }
            public string tipo { get; set; }
            public string valor { get; set; }
        }
        public class c05_referencia
        {
            public string periodo { get; set; }
            public string mes { get; set; }
            public string ano { get; set; }
            public string parcela { get; set; }
        }
        public class TDadosGNRE
        {
            public string c01_UfFavorecida { get; set; }
            public string c02_receita { get; set; }
            public string c25_detalhamentoReceita { get; set; }
            public string c27_tipoIdentificacaoEmitente { get; set; }
            public c03_idContribuinteEmitente c03_idContribuinteEmitente { get; set; }

            public string c28_tipoDocOrigem { get; set; }
            public string c04_docOrigem { get; set; }
            public string c06_valorPrincipal { get; set; }

            public string c10_valorTotal { get; set; }
            public string c14_dataVencimento { get; set; }
            public string c16_razaoSocialEmitente { get; set; }
            public string c18_enderecoEmitente { get; set; }
            public string c19_municipioEmitente { get; set; }
            public string c20_ufEnderecoEmitente { get; set; }
            public string c21_cepEmitente { get; set; }
            public string c22_telefoneEmitente { get; set; }
            public string c34_tipoIdentificacaoDestinatario { get; set; }
            public c35_idContribuinteDestinatario c35_idContribuinteDestinatario { get; set; }
            public string c37_razaoSocialDestinatario { get; set; }
            public string c38_municipioDestinatario { get; set; }
            public string c33_dataPagamento { get; set; }
            public c05_referencia c05_referencia { get; set; }
            public List<campoExtra> c39_camposExtras { get; set; }
        }

        public DadosEmissaoGnre()
        {
            DadosGnre = new TDadosGNRE();
        }
        public TDadosGNRE DadosGnre { get; set; }

        public string RetornaXmlCompleto()
        {
            string xmlFormatado = SerializarDados(DadosGnre);
            string xmlCompleto =
                "<?xml version=\"1.0\" encoding=\"UTF-8\"?><TLote_GNRE xmlns=\"http://www.gnre.pe.gov.br\"><guias>" +
                xmlFormatado + "</guias></TLote_GNRE>";
            return xmlCompleto;
        }


        private string SerializarDados(TDadosGNRE dados)
        {
            XmlSerializer xsSubmit = new XmlSerializer(typeof(TDadosGNRE));
            var settings = new XmlWriterSettings();
            settings.OmitXmlDeclaration = true;

            XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
            ns.Add("", "");
            using (StringWriter sww = new StringWriter())
            using (XmlWriter writer = XmlWriter.Create(sww, settings))
            {
                xsSubmit.Serialize(writer, dados, ns);
                var xml = sww.ToString();
                return xml;
            }
        }
    }
}
