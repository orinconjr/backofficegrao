//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace bcServerService
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbCampanhaCompreJuntoProduto
    {
        public int idCampanhaCompreJuntoProduto { get; set; }
        public int idCampanhaCompreJunto { get; set; }
        public int idProduto { get; set; }
        public Nullable<decimal> precoNaCampanha { get; set; }
    
        public virtual tbCampanhaCompreJunto tbCampanhaCompreJunto { get; set; }
        public virtual tbProdutos tbProdutos { get; set; }
    }
}
