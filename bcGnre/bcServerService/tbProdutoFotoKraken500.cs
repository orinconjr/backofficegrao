//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace bcServerService
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbProdutoFotoKraken500
    {
        public int Id { get; set; }
        public Nullable<int> produtoId { get; set; }
        public string produtoFoto { get; set; }
        public Nullable<int> produtoFotoId { get; set; }
        public string produtoFotoIdString { get; set; }
    }
}
