﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Text;
using Newtonsoft.Json;

namespace bcServerService
{

    public class rnIntegracoes
    {
        public static string marcaPadrao = "Grão de Gente";
        public static int marketplaceFormaDePagamentoId = 10;
        public static int marketplaceStatusPagamentoConfirmado = 3;
        public static bool sandbox = false;

        public static bool habilitarRakuten = true;
        public static string rakutenCNPJ = "10924051000163";
        public static string rakutenOrderKey = "114f25f2-62c5-41e9-b28c-ab99d705052a";
        public static string rakutenId = "8BD12812-0525-4473-8BCC-45D45123DF15";
        public static bool rakutenEnviarMarca = false;
        public static string rakutenFtpFotos = "177.154.155.117";
        public static string rakutenLoginFtpFotos = "rkimagensftp";
        public static string rakutenSenhaFtpFotos = "$OlyV3ks";
        public static string rakutenFtpProdutos = "177.154.155.116";
        public static string rakutenLoginFtpProdutos = "rkfilesftp";
        public static string rakutenSenhaFtpProdutos = "Skz-uE6z";
        public static int rakutenCondicaoDePagamentoId = 24;
        public static List<entregaRakuten> entregasRakutem()
        {
            var listaEntregas = new List<entregaRakuten>();
            var pac = new entregaRakuten();
            pac.codigoRakuten = "pac";
            pac.tipoDeEntregaId = 8;
            listaEntregas.Add(pac);

            var sedex = new entregaRakuten();
            sedex.codigoRakuten = "sedex";
            sedex.tipoDeEntregaId = 5;
            listaEntregas.Add(sedex);

            var fretegratis = new entregaRakuten();
            fretegratis.codigoRakuten = "fretegratis";
            fretegratis.tipoDeEntregaId = 8;
            listaEntregas.Add(fretegratis);

            return listaEntregas;
        }
        public static int rakutenOrigemClienteId = 2;
        public static string rakutenLogin = "comercial@bark.com.br";
        public static string rakutenSenha = "PDZXKZAD";


        public static int extraCondicaoDePagamentoId = 25;
        public static int extraOrigemClienteId = 3;
        public static bool habilitarExtra = true;
        public static string extraUrl = "http://api.extra.com.br/api/v1";
        public static string extraAuthToken = "1VGTeO50GN6K";
        public static string extraAppToken = "Aeo6VZw4Ut2k";
        public static bool extraHomologacao = false;

        public static string Zip(string text)
        {
            byte[] buffer = System.Text.Encoding.Unicode.GetBytes(text);
            MemoryStream ms = new MemoryStream();
            using (System.IO.Compression.GZipStream zip = new System.IO.Compression.GZipStream(ms, System.IO.Compression.CompressionMode.Compress, true))
            {
                zip.Write(buffer, 0, buffer.Length);
            }

            ms.Position = 0;
            MemoryStream outStream = new MemoryStream();

            byte[] compressed = new byte[ms.Length];
            ms.Read(compressed, 0, compressed.Length);

            byte[] gzBuffer = new byte[compressed.Length + 4];
            System.Buffer.BlockCopy(compressed, 0, gzBuffer, 4, compressed.Length);
            System.Buffer.BlockCopy(BitConverter.GetBytes(buffer.Length), 0, gzBuffer, 0, 4);
            return Convert.ToBase64String(gzBuffer);
        }

        public static byte[] Compress(byte[] raw)
        {
            using (MemoryStream memory = new MemoryStream())
            {
                using (GZipStream gzip = new GZipStream(memory, CompressionMode.Compress, true))
                {
                    gzip.Write(raw, 0, raw.Length);
                }
                return memory.ToArray();
            }
        }

        public class entregaRakuten
        {
            public string codigoRakuten { get; set; }
            public int tipoDeEntregaId { get; set; }
        }

        #region extraProduct
        public class ProductUdaList
        {
            public string udaNamed { get; set; }
            public string udaValue { get; set; }
        }

        public class ExtraProduct
        {
            public List<string> categoryList { get; set; }
            public string skuIdOrigin { get; set; }
            public string sellingTitle { get; set; }
            public string description { get; set; }
            public string brand { get; set; }
            public string EAN { get; set; }
            public double defaultPrice { get; set; }
            public double salePrice { get; set; }
            public List<ProductUdaList> productUdaLists { get; set; }
            public double Weight { get; set; }
            public double Length { get; set; }
            public double Width { get; set; }
            public double Height { get; set; }
            public int availableQuantity { get; set; }
            public int handlingTime { get; set; }
            public int installmentId { get; set; }
            public List<string> videos { get; set; }
            public List<string> images { get; set; }
        }

        public class OrderItem
        {
            public string orderItemId { get; set; }
            public string skuId { get; set; }
            public double salePrice { get; set; }
            public string deliveryDate { get; set; }
            public int freightTime { get; set; }
            public int leadTime { get; set; }
        }
        #endregion

        #region extraOrder
        public class BillingInformation
        {
            public string address { get; set; }
            public string addressNr { get; set; }
            public string additionalInfo { get; set; }
            public string quarter { get; set; }
            public string city { get; set; }
            public string state { get; set; }
            public string countryId { get; set; }
            public string postalCd { get; set; }
        }

        public class ShippingInformationsList
        {
            public string deliveryType { get; set; }
            public string recipientName { get; set; }
            public string address { get; set; }
            public string addressNr { get; set; }
            public string additionalInfo { get; set; }
            public string quarter { get; set; }
            public string city { get; set; }
            public string state { get; set; }
            public string countryId { get; set; }
            public string postalCd { get; set; }
        }

        public class ExtraOrder
        {
            public string orderId { get; set; }
            public string orderMasterId { get; set; }
            public double totalAmount { get; set; }
            public string purchaseDate { get; set; }
            public double freightChargedAmount { get; set; }
            public double freightActualAmount { get; set; }
            public List<OrderItem> orderItems { get; set; }
            public List<BillingInformation> billingInformations { get; set; }
            public List<ShippingInformationsList> shippingInformationsList { get; set; }
            public string documentNr { get; set; }
            public string customerEmail { get; set; }
            public string customerName { get; set; }
            public string customerGender { get; set; }
            public int paymentTpId { get; set; }
            public List<object> trackingList { get; set; }
            public string customerPhoneNumber { get; set; }
        }

        public class ExtraTracking
        {
            public List<string> orderItemId { get; set; }
            public string controlPoint { get; set; }
            public string extraDescription { get; set; }
            public string occurenceDt { get; set; }
            public string carrierName { get; set; }
            public string url { get; set; }
            public string objectId { get; set; }
            public string originDeliveryId { get; set; }
            public string accessKeyNfe { get; set; }
            public string linkNfe { get; set; }
            public string serieNfe { get; set; }
            public string nfe { get; set; }
        }
        #endregion


        public static string atualizaSkuExtra(int produtoId)
        {
            var integracoesDc = new dbSiteEntities();
            var produto = (from c in integracoesDc.tbExtraImportacaoProduto where c.produtoId == produtoId select c).FirstOrDefault();
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(rnIntegracoes.extraUrl + "/sellerItems/skuOrigin/" + produtoId);
                request.Headers.Add("nova-auth-token", rnIntegracoes.extraAuthToken);
                request.Headers.Add("nova-app-token", rnIntegracoes.extraAppToken);
                request.Method = "GET";
                var location = String.Empty;
                var result = String.Empty;
                using (var response = (HttpWebResponse)request.GetResponse())
                using (var stream = response.GetResponseStream())
                using (var reader = new StreamReader(stream))
                {
                    result = reader.ReadToEnd();
                }
                dynamic retorno = JsonConvert.DeserializeObject(result);
                string skuId = retorno.skuId;
                produto.skuId = skuId;
            }
            catch (Exception)
            {

            }
            integracoesDc.SaveChanges();
            return produto.skuId;
        }

        public static int atualizaProdutoIdExtra(string skuId)
        {
            var integracoesDc = new dbSiteEntities();
            int produtoId = 0;
            //
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(rnIntegracoes.extraUrl + "/sellerItems/" + skuId);
                request.Headers.Add("nova-auth-token", rnIntegracoes.extraAuthToken);
                request.Headers.Add("nova-app-token", rnIntegracoes.extraAppToken);
                request.Method = "GET";
                var location = String.Empty;
                var result = String.Empty;
                using (var response = (HttpWebResponse)request.GetResponse())
                using (var stream = response.GetResponseStream())
                using (var reader = new StreamReader(stream))
                {
                    result = reader.ReadToEnd();
                }
                dynamic retorno = JsonConvert.DeserializeObject(result);
                produtoId = Convert.ToInt32(retorno.skuOrigin);
                int skuOrigin = produtoId;
                var produto = (from c in integracoesDc.tbExtraImportacaoProduto where c.produtoId == skuOrigin select c).FirstOrDefault();
                produto.skuId = skuId;
            }
            catch (Exception)
            {

            }
            integracoesDc.SaveChanges();
            return produtoId;
        }

        public static List<int> listaPedidosPendentesExtra(bool bypass)
        {
            List<int> listaIds = new List<int>();

            var pedidoDc = new dbSiteEntities();
            var checagem = (from c in pedidoDc.tbMarketplaceChecagem orderby c.dataHora descending select c).FirstOrDefault();
            var ultimaChecagem = checagem == null ? DateTime.Now.AddHours(-2) : checagem.dataHora;
            if ((ultimaChecagem < DateTime.Now.AddMinutes(-120)) | bypass)
            {
                bool possuiMais = true;
                int offSetAtual = 0;
                while (possuiMais)
                {
                    try
                    {
                        var request =
                            (HttpWebRequest)
                                WebRequest.Create(rnIntegracoes.extraUrl + "/orders/status/approved/?_offset=" + offSetAtual + "&_limit=50");
                        request.Headers.Add("nova-auth-token", rnIntegracoes.extraAuthToken);
                        request.Headers.Add("nova-app-token", rnIntegracoes.extraAppToken);
                        request.Method = "GET";
                        var location = String.Empty;
                        var result = String.Empty;
                        using (var response = (HttpWebResponse)request.GetResponse())
                        using (var stream = response.GetResponseStream())
                        using (var reader = new StreamReader(stream))
                        {
                            result = reader.ReadToEnd();
                        }
                        var pedidos = JsonConvert.DeserializeObject<List<ExtraOrder>>(result);
                        possuiMais = false;
                        offSetAtual = offSetAtual + 50;

                        foreach (var pedidoEx in pedidos)
                        {
                            possuiMais = true;
                            var pedidoCheck = (from c in pedidoDc.tbPedidos
                                               where c.marketplaceId == pedidoEx.orderId && c.condDePagamentoId == extraCondicaoDePagamentoId
                                               select c).FirstOrDefault();
                            if (pedidoCheck != null)
                            {
                                listaIds.Add(pedidoCheck.pedidoId);
                            }
                        }
                    }
                    catch (Exception)
                    {
                        possuiMais = false;
                    }
                }
            }

            return listaIds;
        }

        public static List<int> listaPedidosEnviadosExtra(bool bypass, int limit)
        {
            List<int> listaIds = new List<int>();

            var pedidoDc = new dbSiteEntities();
            var checagem = (from c in pedidoDc.tbMarketplaceChecagem orderby c.dataHora descending select c).FirstOrDefault();
            var ultimaChecagem = checagem == null ? DateTime.Now.AddHours(-2) : checagem.dataHora;
            if ((ultimaChecagem < DateTime.Now.AddMinutes(-120)) | bypass)
            {
                bool possuiMais = true;
                int offSetAtual = 0;
                while (possuiMais)
                {
                    try
                    {
                        var request =
                            (HttpWebRequest)
                                WebRequest.Create(rnIntegracoes.extraUrl + "/orders/status/sent/?_offset=" + offSetAtual + "&_limit=50");
                        request.Headers.Add("nova-auth-token", rnIntegracoes.extraAuthToken);
                        request.Headers.Add("nova-app-token", rnIntegracoes.extraAppToken);
                        request.Method = "GET";
                        var location = String.Empty;
                        var result = String.Empty;
                        using (var response = (HttpWebResponse)request.GetResponse())
                        using (var stream = response.GetResponseStream())
                        using (var reader = new StreamReader(stream))
                        {
                            result = reader.ReadToEnd();
                        }
                        var pedidos = JsonConvert.DeserializeObject<List<ExtraOrder>>(result);
                        possuiMais = false;
                        offSetAtual = offSetAtual + 50;

                        foreach (var pedidoEx in pedidos)
                        {
                            possuiMais = true;
                            var pedidoCheck = (from c in pedidoDc.tbPedidos
                                               where c.marketplaceId == pedidoEx.orderId && c.condDePagamentoId == extraCondicaoDePagamentoId
                                               select c).FirstOrDefault();
                            if (pedidoCheck != null)
                            {
                                listaIds.Add(pedidoCheck.pedidoId);
                                if (listaIds.Count() == limit) possuiMais = false;
                            }
                        }
                    }
                    catch (Exception)
                    {
                        possuiMais = false;
                    }
                }
            }

            return listaIds;
        }

        public static string retornaAtualizaStatusPedidoExtra(int pedidoId, int situacaoId)
        {
            var pedidoDc = new dbSiteEntities();
            var pedido = (from c in pedidoDc.tbPedidos where c.pedidoId == pedidoId select c).First();
            var cliente = (from c in pedidoDc.tbClientes where c.clienteId == pedido.clienteId select c).First();
            var pacotes = (from c in pedidoDc.tbPedidoPacote where c.idPedido == pedidoId orderby c.numeroVolume select c);
            var notaDc = new dbSiteEntities();
            int status = situacaoId == 5 ? 17 : 16;
            string urlRastreio = "";
            string numeroRastreio = "";
            string nomeTransportadora = "";
            string invoiceNumber = pedido.nfeNumero == null ? "" : pedido.nfeNumero.ToString();
            string invoiceSerie = "01";
            string nfeAccessKey = pedido.nfeAccessKey == null ? "" : pedido.nfeAccessKey;
            if (string.IsNullOrEmpty(invoiceNumber))
            {
                if (pacotes.First().tbPedidoEnvio != null)
                {
                    invoiceNumber = pacotes.First().tbPedidoEnvio.nfeNumero == null
                        ? ""
                        : pacotes.First().tbPedidoEnvio.nfeNumero.ToString();
                    nfeAccessKey = pacotes.First().tbPedidoEnvio.nfeKey == null
                        ? ""
                        : pacotes.First().tbPedidoEnvio.nfeKey.ToString();
                }
            }
            if (situacaoId == 5)
            {
                string urlTracking = "http://websro.correios.com.br/sro_bin/txect01$.Inexistente?P_LINGUA=001&P_TIPO=002&P_COD_LIS=";
                foreach (var pacote in pacotes)
                {
                    if (pacote.numeroVolume > 1 &&
                        (pacote.formaDeEnvio.ToLower() == "pac" | pacote.formaDeEnvio.ToLower() == "sedex"))
                    {
                        urlTracking = urlTracking + "%2C+";
                        nomeTransportadora = "correios";
                    }
                    if (pacote.rastreio != "")
                    {
                        if (pacote.numeroVolume == 1)
                        {
                            if (pacote.formaDeEnvio.ToLower() == "jadlog")
                            {
                                nomeTransportadora = "Jadlog";
                                urlTracking = "http://jadlog.com.br/tracking.jsp?cte=";
                                urlTracking = urlTracking + pacote.rastreio;
                            }
                            else if (pacote.formaDeEnvio.ToLower() == "tnt")
                            {
                                nomeTransportadora = "TNT";
                                urlTracking = "http://app.tntbrasil.com.br/LocalizacaoSimplificada/resultado_localizacao.asp?cnpj=" + cliente.clienteCPFCNPJ + "&nota=" + invoiceNumber;
                            }
                        }
                        if (pacote.formaDeEnvio.ToLower() == "pac" | pacote.formaDeEnvio.ToLower() == "sedex")
                        {
                            nomeTransportadora = "correios";
                            urlTracking += pacote.rastreio;
                            numeroRastreio = pacote.rastreio;
                        }

                    }
                    if (string.IsNullOrEmpty(numeroRastreio) && pacote.formaDeEnvio.ToLower() == "tnt")
                    {
                        numeroRastreio = pacote.tbPedidoEnvio.nfeNumero.ToString();
                    }
                }
                urlRastreio = urlTracking;

                if (!string.IsNullOrEmpty(pedido.numeroDoTracking4))
                {
                    numeroRastreio = pedido.numeroDoTracking4;
                    nomeTransportadora = "Jadlog";
                }

                var requestOrder = (HttpWebRequest)WebRequest.Create(rnIntegracoes.extraUrl + "/orders/" + pedido.marketplaceId + "/");
                requestOrder.Headers.Add("nova-auth-token", rnIntegracoes.extraAuthToken);
                requestOrder.Headers.Add("nova-app-token", rnIntegracoes.extraAppToken);
                requestOrder.Method = "GET";
                var resultOrder = String.Empty;
                using (var response = (HttpWebResponse)requestOrder.GetResponse())
                using (var stream = response.GetResponseStream())
                using (var reader = new StreamReader(stream))
                {
                    resultOrder = reader.ReadToEnd();
                }
                var detalhesPedido = JsonConvert.DeserializeObject<ExtraOrder>(resultOrder);

                var listaIds = new List<string>();
                string rastreio = "";
                foreach (var itemPedido in detalhesPedido.orderItems)
                {
                    listaIds.Add(itemPedido.orderItemId);
                }
                var extraTrackingtracking = new ExtraTracking();
                extraTrackingtracking.orderItemId = listaIds;
                extraTrackingtracking.controlPoint = "EPR";
                extraTrackingtracking.extraDescription = "extraDescription";
                var dataEnvio = pedido.dataSendoEmbalado == null ? (DateTime)pacotes.First().tbPedidoEnvio.dataFimEmbalagem : (DateTime)pedido.dataSendoEmbalado;
                string ano = dataEnvio.Year.ToString();
                string mes = dataEnvio.Month < 10 ? "0" + dataEnvio.Month : dataEnvio.Month.ToString();
                string dia = dataEnvio.Day < 10 ? "0" + dataEnvio.Day : dataEnvio.Day.ToString();
                string hora = dataEnvio.Hour < 10 ? "0" + dataEnvio.Hour : dataEnvio.Hour.ToString();
                string minuto = dataEnvio.Minute < 10 ? "0" + dataEnvio.Minute : dataEnvio.Minute.ToString();
                string segundo = dataEnvio.Second < 10 ? "0" + dataEnvio.Second : dataEnvio.Second.ToString();
                //extraTrackingtracking.occurenceDt = "2013-09-30T10:34:48";
                extraTrackingtracking.occurenceDt = ano + "-" + mes + "-" + dia + "T" + hora + ":" + minuto + ":" + segundo;
                extraTrackingtracking.carrierName = nomeTransportadora;
                extraTrackingtracking.url = urlTracking;
                extraTrackingtracking.objectId = numeroRastreio;
                extraTrackingtracking.originDeliveryId = numeroRastreio;
                extraTrackingtracking.accessKeyNfe = nfeAccessKey;
                extraTrackingtracking.serieNfe = invoiceSerie;
                //extraTrackingtracking.linkNfe = "";
                extraTrackingtracking.nfe = invoiceNumber;
                extraTrackingtracking.linkNfe = "http://www.graodegente.com.br/pedidos.aspx";
                var tracking = JsonConvert.SerializeObject(extraTrackingtracking);
                return tracking;
            }
            return "";
        }

        public static string atualizaStatusPedidoExtra(int pedidoId, int situacaoId)
        {
            return atualizaStatusPedidoExtra(pedidoId, situacaoId, false);
        }

        public static string atualizaStatusPedidoExtra(int pedidoId, int situacaoId, bool entrega)
        {

            var pedidoDc = new dbSiteEntities();
            var pedido = (from c in pedidoDc.tbPedidos where c.pedidoId == pedidoId select c).First();
            var cliente = (from c in pedidoDc.tbClientes where c.clienteId == pedido.clienteId select c).First();
            var pacotes = (from c in pedidoDc.tbPedidoPacote where c.idPedido == pedidoId orderby c.numeroVolume select c);
            var notaDc = new dbSiteEntities();
            int status = situacaoId == 5 ? 17 : 16;
            string urlRastreio = "";
            string numeroRastreio = "";
            string nomeTransportadora = "";
            string invoiceNumber = pedido.nfeNumero == null ? "" : pedido.nfeNumero.ToString();
            string invoiceSerie = "01";

            string nfeAccessKey = pedido.nfeAccessKey == null ? "" : pedido.nfeAccessKey;
            if (string.IsNullOrEmpty(invoiceNumber))
            {
                if (pacotes.First().tbPedidoEnvio != null)
                {
                    if (pacotes.First().tbPedidoEnvio.nfeNumero == null)
                    {
                        var nota = (from c in pedidoDc.tbNotaFiscal where c.idPedido == pedidoId select c).FirstOrDefault();
                        if (nota != null)
                        {
                            invoiceNumber = nota.numeroNota.ToString();
                            nfeAccessKey = nota.nfeKey.ToString();
                        }
                    }
                    else
                    {
                        invoiceNumber = pacotes.First().tbPedidoEnvio.nfeNumero.ToString();
                    }
                    if (pacotes.First().tbPedidoEnvio.nfeKey != null)
                    {
                        nfeAccessKey = pacotes.First().tbPedidoEnvio.nfeKey == null
                            ? ""
                            : pacotes.First().tbPedidoEnvio.nfeKey.ToString();
                    }
                    else
                    {
                        var nota = (from c in pedidoDc.tbNotaFiscal where c.idPedido == pedidoId select c).FirstOrDefault();
                        if (nota != null)
                        {
                            nfeAccessKey = nota.nfeKey.ToString();
                        }
                    }
                }
            }
            else
            {
                if (string.IsNullOrEmpty(nfeAccessKey))
                {
                    int notaNumero = 0;
                    int.TryParse(invoiceNumber, out notaNumero);
                    var nota = (from c in pedidoDc.tbNotaFiscal where c.numeroNota == notaNumero select c).FirstOrDefault();
                    if (nota != null)
                    {
                        invoiceNumber = nota.numeroNota.ToString();
                        nfeAccessKey = nota.nfeKey.ToString();
                    }
                }
            }
            if (situacaoId == 5)
            {
                string urlTracking = "http://websro.correios.com.br/sro_bin/txect01$.Inexistente?P_LINGUA=001&P_TIPO=002&P_COD_LIS=";
                foreach (var pacote in pacotes)
                {
                    if (pacote.numeroVolume > 1 && (pacote.formaDeEnvio.ToLower() == "pac" | pacote.formaDeEnvio.ToLower() == "sedex"))
                    {
                        urlTracking = urlTracking + "%2C+";
                        nomeTransportadora = "correios";
                    }
                    if (pacote.rastreio != "")
                    {
                        if (pacote.numeroVolume == 1)
                        {
                            if (pacote.formaDeEnvio.ToLower() == "jadlog")
                            {
                                nomeTransportadora = "Jadlog";
                                urlTracking = "http://jadlog.com.br/tracking.jsp?cte=";
                                urlTracking = urlTracking + pacote.rastreio;
                                numeroRastreio = pacote.rastreio;
                            }
                            else if (pacote.formaDeEnvio.ToLower() == "tnt")
                            {
                                nomeTransportadora = "TNT";
                                urlTracking = "http://app.tntbrasil.com.br/LocalizacaoSimplificada/resultado_localizacao.asp?cnpj=" + cliente.clienteCPFCNPJ + "&nota=" + invoiceNumber;
                            }
                        }
                        if (pacote.formaDeEnvio.ToLower() == "pac" | pacote.formaDeEnvio.ToLower() == "sedex")
                        {
                            nomeTransportadora = "correios";
                            urlTracking += pacote.rastreio;
                            numeroRastreio = pacote.rastreio;
                        }
                    }
                    if (string.IsNullOrEmpty(numeroRastreio) && pacote.formaDeEnvio.ToLower() == "tnt")
                    {
                        numeroRastreio = pacote.tbPedidoEnvio.nfeNumero.ToString();
                    }
                }
                urlRastreio = urlTracking;


                if (string.IsNullOrEmpty(nomeTransportadora)) nomeTransportadora = "Correios";
                var requestOrder = (HttpWebRequest)WebRequest.Create(rnIntegracoes.extraUrl + "/orders/" + pedido.marketplaceId + "/");
                requestOrder.Headers.Add("nova-auth-token", rnIntegracoes.extraAuthToken);
                requestOrder.Headers.Add("nova-app-token", rnIntegracoes.extraAppToken);
                requestOrder.Method = "GET";
                var resultOrder = String.Empty;
                using (var response = (HttpWebResponse)requestOrder.GetResponse())
                using (var stream = response.GetResponseStream())
                using (var reader = new StreamReader(stream))
                {
                    resultOrder = reader.ReadToEnd();
                }
                var detalhesPedido = JsonConvert.DeserializeObject<ExtraOrder>(resultOrder);

                var listaIds = new List<string>();
                string rastreio = "";
                foreach (var itemPedido in detalhesPedido.orderItems)
                {
                    listaIds.Add(itemPedido.orderItemId);
                }
                var extraTrackingtracking = new ExtraTracking();
                extraTrackingtracking.orderItemId = listaIds;
                extraTrackingtracking.controlPoint = "EPR";
                if (entrega) extraTrackingtracking.controlPoint = "ENT";
                extraTrackingtracking.extraDescription = "extraDescription";
                var dataEnvio = pedido.dataSendoEmbalado == null ? (DateTime)pacotes.First().tbPedidoEnvio.dataFimEmbalagem : (DateTime)pedido.dataSendoEmbalado;
                string ano = dataEnvio.Year.ToString();
                string mes = dataEnvio.Month < 10 ? "0" + dataEnvio.Month : dataEnvio.Month.ToString();
                string dia = dataEnvio.Day < 10 ? "0" + dataEnvio.Day : dataEnvio.Day.ToString();
                string hora = dataEnvio.Hour < 10 ? "0" + dataEnvio.Hour : dataEnvio.Hour.ToString();
                string minuto = dataEnvio.Minute < 10 ? "0" + dataEnvio.Minute : dataEnvio.Minute.ToString();
                string segundo = dataEnvio.Second < 10 ? "0" + dataEnvio.Second : dataEnvio.Second.ToString();
                //extraTrackingtracking.occurenceDt = "2013-09-30T10:34:48";
                extraTrackingtracking.occurenceDt = ano + "-" + mes + "-" + dia + "T" + hora + ":" + minuto + ":" + segundo;
                extraTrackingtracking.carrierName = nomeTransportadora;
                extraTrackingtracking.url = urlTracking;
                extraTrackingtracking.objectId = numeroRastreio;
                extraTrackingtracking.originDeliveryId = numeroRastreio;
                extraTrackingtracking.accessKeyNfe = nfeAccessKey;
                extraTrackingtracking.serieNfe = invoiceSerie;
                //extraTrackingtracking.linkNfe = "";
                extraTrackingtracking.nfe = invoiceNumber;
                extraTrackingtracking.linkNfe = "http://www.graodegente.com.br/pedidos.aspx";
                var tracking = JsonConvert.SerializeObject(extraTrackingtracking);
                var request = (HttpWebRequest)WebRequest.Create(rnIntegracoes.extraUrl + "/orders/" + pedido.marketplaceId + "/ordersItems/trackings/");
                request.Headers.Add("nova-auth-token", rnIntegracoes.extraAuthToken);
                request.Headers.Add("nova-app-token", rnIntegracoes.extraAppToken);
                request.Method = "POST";
                request.ContentType = "application/json";
                var conteudo = Encoding.UTF8.GetBytes(tracking);
                request.GetRequestStream().Write(conteudo, 0, conteudo.Length);
                var location = String.Empty;
                var result = String.Empty;
                var response1 = (HttpWebResponse)request.GetResponse();
                using (var stream = response1.GetResponseStream())
                using (var reader = new StreamReader(stream))
                {
                    result = reader.ReadToEnd();
                }
                dynamic retorno = JsonConvert.DeserializeObject(result);

                return rnIntegracoes.extraUrl + "/orders/" + pedido.marketplaceId + "/ordersItems/trackings/" + tracking;
            }
            return "";
        }



        public static void checaPedidosExtra(bool bypass)
        {
            var pedidoDc = new dbSiteEntities();
                bool possuiMais = true;
                int offSetAtual = 0;
                while (possuiMais)
                {
                    try
                    {
                        var request = (HttpWebRequest)WebRequest.Create(rnIntegracoes.extraUrl + "/orders/status/approved/?_offset=" + offSetAtual + "&_limit=50");
                        request.Headers.Add("nova-auth-token", rnIntegracoes.extraAuthToken);
                        request.Headers.Add("nova-app-token", rnIntegracoes.extraAppToken);
                        request.Method = "GET";
                        var location = String.Empty;
                        var result = String.Empty;
                        using (var response = (HttpWebResponse)request.GetResponse())
                        using (var stream = response.GetResponseStream())
                        using (var reader = new StreamReader(stream))
                        {
                            result = reader.ReadToEnd();
                        }
                        var pedidos = JsonConvert.DeserializeObject<List<ExtraOrder>>(result);
                        possuiMais = false;
                        offSetAtual = offSetAtual + 50;

                        foreach (var pedidoEx in pedidos)
                        {
                            possuiMais = true;
                            var pedidoCheckMaster =
                                (from c in pedidoDc.tbPedidos where c.marketplaceId == pedidoEx.orderMasterId select c)
                                    .FirstOrDefault();
                            var pedidoCheck = (from c in pedidoDc.tbPedidos
                                               where c.marketplaceId == pedidoEx.orderId && c.condDePagamentoId == extraCondicaoDePagamentoId
                                               select c).FirstOrDefault();
                            if (pedidoCheckMaster != null)
                            {
                                var pedido = pedidoCheckMaster;
                                var clienteDc = new dbSiteEntities();
                                int clienteId = 0;
                                var clienteCheck = (from c in clienteDc.tbClientes
                                                    where c.clienteCPFCNPJ == pedidoEx.documentNr && c.idOrigemCliente == extraOrigemClienteId
                                                    select c).FirstOrDefault();
                                if (clienteCheck != null)
                                {
                                    clienteId = clienteCheck.clienteId;
                                }
                                else
                                {
                                    var cliente = new tbClientes();
                                    cliente.clienteNome = pedidoEx.customerName;
                                    cliente.clienteCPFCNPJ = pedidoEx.documentNr;
                                    cliente.clienteRGIE = "";
                                    cliente.clienteSexo = pedidoEx.customerGender == null ? "" : pedidoEx.customerGender;
                                    cliente.clienteRua = pedidoEx.billingInformations.FirstOrDefault().address;
                                    cliente.clienteNumero = pedidoEx.billingInformations.FirstOrDefault().addressNr;
                                    cliente.clienteComplemento = pedidoEx.billingInformations.FirstOrDefault().additionalInfo;
                                    cliente.clienteBairro = pedidoEx.billingInformations.FirstOrDefault().quarter;
                                    cliente.clienteCidade = pedidoEx.billingInformations.FirstOrDefault().city;
                                    cliente.clienteEstado = pedidoEx.billingInformations.FirstOrDefault().state;
                                    cliente.clientePais = "Brasil";
                                    cliente.clienteCep = pedidoEx.billingInformations.FirstOrDefault().postalCd;
                                    cliente.clienteEmail = "marketplace";
                                    cliente.clienteSenha = "marketplace";
                                    cliente.clienteDataNascimento = "01/01/1900";
                                    cliente.clienteFoneResidencial = "";
                                    cliente.clienteFoneComercial = "";
                                    cliente.clienteFoneCelular = "";
                                    cliente.clienteRecebeInformativo = "False";
                                    cliente.idOrigemCliente = extraOrigemClienteId;
                                    cliente.dataDaCriacao = DateTime.Now;

                                    clienteDc.tbClientes.Add(cliente);
                                    clienteDc.SaveChanges();
                                    clienteId = cliente.clienteId;
                                }

                                pedido.pedidoKey = "";
                                pedido.clienteId = clienteId;
                                pedido.ipDoCliente = "";
                                pedido.tipoDePagamentoId = marketplaceFormaDePagamentoId;
                                pedido.condDePagamentoId = extraCondicaoDePagamentoId;
                                int tipoDeEntrega = 8;
                                pedido.tipoDeEntregaId = tipoDeEntrega;

                                int prazoDeEntrega = 0;
                                foreach (var itemPedidoEx in pedidoEx.orderItems)
                                {
                                    if (itemPedidoEx.freightTime > prazoDeEntrega) prazoDeEntrega = itemPedidoEx.freightTime;
                                }
                                pedido.valorDoFrete = Convert.ToDecimal(pedidoEx.freightChargedAmount);
                                pedido.prazoDeEntrega = prazoDeEntrega.ToString();
                                pedido.valorDosItens = Convert.ToDecimal(pedidoEx.totalAmount);
                                pedido.valorTotalGeral = Convert.ToDecimal(pedidoEx.totalAmount);
                                pedido.valorCobrado = Convert.ToDecimal(pedidoEx.totalAmount);
                                pedido.valorDosJuros = 0;
                                pedido.valorDoEmbrulhoECartao = 0;
                                pedido.porcentagemDoJuros = 0;
                                pedido.numeroDeParcelas = 1;
                                pedido.valorDaParcela = Convert.ToDecimal(pedidoEx.totalAmount);
                                pedido.pesoDoPedido = 0;
                                pedido.dataHoraDoPedido = Convert.ToDateTime(pedidoEx.purchaseDate);
                                pedido.statusDoPedido = marketplaceStatusPagamentoConfirmado;
                                pedido.endNomeDoDestinatario = pedidoEx.shippingInformationsList.FirstOrDefault().recipientName;
                                pedido.endRua = pedidoEx.shippingInformationsList.FirstOrDefault().address;
                                pedido.endNumero = pedidoEx.shippingInformationsList.FirstOrDefault().addressNr;
                                pedido.endComplemento = pedidoEx.shippingInformationsList.FirstOrDefault().additionalInfo;
                                pedido.endBairro = pedidoEx.shippingInformationsList.FirstOrDefault().quarter;
                                pedido.endCidade = pedidoEx.shippingInformationsList.FirstOrDefault().city;
                                pedido.endEstado = pedidoEx.shippingInformationsList.FirstOrDefault().state;
                                pedido.endPais = "Brasil";
                                pedido.endCep = pedidoEx.shippingInformationsList.FirstOrDefault().postalCd;
                                pedido.marketplace = true;
                                pedido.marketplaceId = pedidoEx.orderId;
                                pedido.dataConfirmacaoPagamento = DateTime.Now;
                                pedidoDc.SaveChanges();

                                var itensPedidoDc = new dbSiteEntities();
                                var extraProdutoDc = new dbSiteEntities();

                                foreach (var itemPedidoEx in pedidoEx.orderItems)
                                {
                                    var produtoIdCheck =
                                        (from c in extraProdutoDc.tbExtraImportacaoProduto
                                         where c.skuId == itemPedidoEx.skuId
                                         select c).FirstOrDefault();
                                    int produtoId = 0;
                                    if (produtoIdCheck == null)
                                    {
                                        produtoId = atualizaProdutoIdExtra(itemPedidoEx.skuId);
                                    }
                                    else
                                    {
                                        produtoId = produtoIdCheck.produtoId;
                                    }

                                    var produtoDc = new dbSiteEntities();

                                    var produtosPedido =
                                        (from c in itensPedidoDc.tbItensPedido
                                         where c.pedidoId == pedidoCheckMaster.pedidoId
                                         select c);
                                    if (produtosPedido.Any())
                                    {
                                        foreach (var produtoPedido in produtosPedido)
                                        {
                                            itensPedidoDc.tbItensPedido.Remove(produtoPedido);
                                        }
                                    }
                                    itensPedidoDc.SaveChanges();

                                    var produto = (from c in produtoDc.tbProdutos where c.produtoId == produtoId select c).FirstOrDefault();
                                    if (produto != null)
                                    {
                                        var itemPedido = new tbItensPedido();
                                        itemPedido.pedidoId = pedido.pedidoId;
                                        itemPedido.produtoId = produtoId;
                                        itemPedido.produtoIdDaEmpresa = produto.produtoIdDaEmpresa;
                                        itemPedido.itemQuantidade = 1;
                                        itemPedido.itemValor = Convert.ToDecimal(itemPedidoEx.salePrice);
                                        itemPedido.dataDaCriacao = DateTime.Now;
                                        itemPedido.garantiaId = 0;
                                        itemPedido.itemPresente = "False";
                                        itemPedido.valorDoEmbrulhoECartao = 0;
                                        itensPedidoDc.tbItensPedido.Add(itemPedido);
                                        itensPedidoDc.SaveChanges();

                                        var itensFilho = (from c in produtoDc.tbProdutoRelacionado where c.idProdutoPai == produto.produtoId select c).ToList();
                                        if (itensFilho.Count > 0)
                                        {
                                            List<int> idsProdutosFilhos = new List<int>();
                                            idsProdutosFilhos.AddRange(itensFilho.Select(x => x.idProdutoFilho).ToList());
                                            var produtosFilhos = (from c in produtoDc.tbProdutos where idsProdutosFilhos.Contains(c.produtoId) select c).ToList();

                                            decimal valorTotalVendaCombo = 0;

                                            foreach (var itemFilho in itensFilho)
                                            {
                                                var produtoFilho = (from c in produtosFilhos where c.produtoId == itemFilho.idProdutoFilho select c).FirstOrDefault();
                                                if (produtoFilho != null)
                                                {
                                                    valorTotalVendaCombo += produtoFilho.produtoPreco;
                                                }
                                            }

                                            foreach (var itemFilho in itensFilho)
                                            {
                                                var produtoFilho = (from c in produtosFilhos where c.produtoId == itemFilho.idProdutoFilho select c).FirstOrDefault();
                                                decimal precoNoCombo = 0;
                                                try
                                                {
                                                    precoNoCombo = (produtoFilho.produtoPreco * produto.produtoPreco) / valorTotalVendaCombo;
                                                }
                                                catch (Exception)
                                                {

                                                }
                                                var itemPedidoCombo = new tbItensPedidoCombo();
                                                itemPedidoCombo.idItemPedido = itemPedido.itemPedidoId;
                                                itemPedidoCombo.produtoId = itemFilho.idProdutoFilho;
                                                try
                                                {
                                                    itemPedidoCombo.precoDeCusto = Convert.ToDecimal(produtoFilho.produtoPrecoDeCusto);
                                                }
                                                catch (Exception)
                                                {
                                                    itemPedidoCombo.precoDeCusto = 0;
                                                }
                                                if (itemFilho.desconto == 100)
                                                {
                                                    itemPedidoCombo.precoDeCusto = 0;
                                                }
                                                itemPedidoCombo.valorNoCombo = precoNoCombo;
                                                itemPedidoCombo.enviado = false;
                                                produtoDc.tbItensPedidoCombo.Add(itemPedidoCombo);
                                                produtoDc.SaveChanges();
                                            }
                                        }
                                    }
                                }


                                var queue = new dbSiteEntities();

                                var queueItensFilho = new tbQueue();
                                queueItensFilho.tipoQueue = 4;
                                queueItensFilho.agendamento = DateTime.Now;
                                queueItensFilho.idRelacionado = pedido.pedidoId;
                                queueItensFilho.mensagem = "";
                                queueItensFilho.concluido = false;
                                queueItensFilho.andamento = false;
                                queue.tbQueue.Add(queueItensFilho);
                                queue.SaveChanges();

                                var queueReservarEstoque = new tbQueue();
                                queueReservarEstoque.tipoQueue = 5;
                                queueReservarEstoque.agendamento = DateTime.Now;
                                queueReservarEstoque.idRelacionado = pedido.pedidoId;
                                queueReservarEstoque.mensagem = "";
                                queueReservarEstoque.concluido = false;
                                queueReservarEstoque.andamento = false;
                                queue.tbQueue.Add(queueReservarEstoque);
                                queue.SaveChanges();

                                var queuePrazo = new tbQueue();
                                queuePrazo.tipoQueue = 6;
                                queuePrazo.agendamento = DateTime.Now;
                                queuePrazo.idRelacionado = pedido.pedidoId;
                                queuePrazo.mensagem = "";
                                queuePrazo.concluido = false;
                                queuePrazo.andamento = false;
                                queue.tbQueue.Add(queuePrazo);
                                queue.SaveChanges();


                                itensPedidoDc.SaveChanges();
                                //checaPedidoAvulsoExtra(pedido.pedidoId);
                            }
                            else if (pedidoCheck == null)
                            {
                                var clienteDc = new dbSiteEntities();
                                int clienteId = 0;
                                var clienteCheck = (from c in clienteDc.tbClientes
                                                    where c.clienteCPFCNPJ == pedidoEx.documentNr && c.idOrigemCliente == extraOrigemClienteId
                                                    select c).FirstOrDefault();
                                if (clienteCheck != null)
                                {
                                    clienteId = clienteCheck.clienteId;
                                }
                                else
                                {
                                    var cliente = new tbClientes();
                                    cliente.clienteNome = pedidoEx.customerName;
                                    cliente.clienteCPFCNPJ = pedidoEx.documentNr;
                                    cliente.clienteRGIE = "";
                                    cliente.clienteSexo = pedidoEx.customerGender;
                                    cliente.clienteRua = pedidoEx.billingInformations.FirstOrDefault().address;
                                    cliente.clienteNumero = pedidoEx.billingInformations.FirstOrDefault().addressNr;
                                    cliente.clienteComplemento = pedidoEx.billingInformations.FirstOrDefault().additionalInfo;
                                    cliente.clienteBairro = pedidoEx.billingInformations.FirstOrDefault().quarter;
                                    cliente.clienteCidade = pedidoEx.billingInformations.FirstOrDefault().city;
                                    cliente.clienteEstado = pedidoEx.billingInformations.FirstOrDefault().state;
                                    cliente.clientePais = "Brasil";
                                    cliente.clienteCep = pedidoEx.billingInformations.FirstOrDefault().postalCd;
                                    cliente.clienteEmail = "marketplace";
                                    cliente.clienteSenha = "marketplace";
                                    cliente.clienteDataNascimento = "01/01/1900";
                                    cliente.clienteFoneResidencial = "";
                                    cliente.clienteFoneComercial = "";
                                    cliente.clienteFoneCelular = "";
                                    cliente.clienteRecebeInformativo = "False";
                                    cliente.idOrigemCliente = extraOrigemClienteId;
                                    //cliente.dataDaCriacao = DateTime.Now;

                                    clienteDc.tbClientes.Add(cliente);
                                    clienteDc.SaveChanges();
                                    clienteId = cliente.clienteId;
                                }

                                var pedido = new tbPedidos();
                                pedido.pedidoKey = "";
                                pedido.clienteId = clienteId;
                                pedido.ipDoCliente = "";
                                pedido.tipoDePagamentoId = marketplaceFormaDePagamentoId;
                                pedido.condDePagamentoId = extraCondicaoDePagamentoId;
                                int tipoDeEntrega = 8;
                                pedido.tipoDeEntregaId = tipoDeEntrega;

                                pedido.valorDoFrete = Convert.ToDecimal(pedidoEx.freightChargedAmount);
                                pedido.prazoDeEntrega = "0";
                                pedido.valorDosItens = Convert.ToDecimal(pedidoEx.totalAmount);
                                pedido.valorTotalGeral = Convert.ToDecimal(pedidoEx.totalAmount);
                                pedido.valorCobrado = Convert.ToDecimal(pedidoEx.totalAmount);
                                pedido.valorDosJuros = 0;
                                pedido.valorDoEmbrulhoECartao = 0;
                                pedido.porcentagemDoJuros = 0;
                                pedido.numeroDeParcelas = 1;
                                pedido.valorDaParcela = Convert.ToDecimal(pedidoEx.totalAmount);
                                pedido.pesoDoPedido = 0;
                                pedido.dataHoraDoPedido = Convert.ToDateTime(pedidoEx.purchaseDate);
                                pedido.statusDoPedido = marketplaceStatusPagamentoConfirmado;
                                pedido.endNomeDoDestinatario = pedidoEx.shippingInformationsList.FirstOrDefault().recipientName;
                                pedido.endRua = pedidoEx.shippingInformationsList.FirstOrDefault().address;
                                pedido.endNumero = pedidoEx.shippingInformationsList.FirstOrDefault().addressNr;
                                pedido.endComplemento = pedidoEx.shippingInformationsList.FirstOrDefault().additionalInfo;
                                pedido.endBairro = pedidoEx.shippingInformationsList.FirstOrDefault().quarter;
                                pedido.endCidade = pedidoEx.shippingInformationsList.FirstOrDefault().city;
                                pedido.endEstado = pedidoEx.shippingInformationsList.FirstOrDefault().state;
                                pedido.endPais = "Brasil";
                                pedido.endCep = pedidoEx.shippingInformationsList.FirstOrDefault().postalCd;
                                pedido.marketplace = true;
                                pedido.marketplaceId = pedidoEx.orderId;
                                pedido.dataConfirmacaoPagamento = DateTime.Now;
                                pedidoDc.tbPedidos.Add(pedido);
                                pedidoDc.SaveChanges();

                                var itensPedidoDc = new dbSiteEntities();
                                var extraProdutoDc = new dbSiteEntities();

                                foreach (var itemPedidoEx in pedidoEx.orderItems)
                                {
                                    var produtoIdCheck =
                                        (from c in extraProdutoDc.tbExtraImportacaoProduto
                                         where c.skuId == itemPedidoEx.skuId
                                         select c).FirstOrDefault();
                                    int produtoId = 0;
                                    if (produtoIdCheck == null)
                                    {
                                        produtoId = atualizaProdutoIdExtra(itemPedidoEx.skuId);
                                    }
                                    else
                                    {
                                        produtoId = produtoIdCheck.produtoId;
                                    }

                                    var produtoDc = new dbSiteEntities();
                                    var produto =
                                        (from c in produtoDc.tbProdutos where c.produtoId == produtoId select c).FirstOrDefault();
                                    if (produto != null)
                                    {
                                        var itemPedido = new tbItensPedido();
                                        itemPedido.pedidoId = pedido.pedidoId;
                                        itemPedido.produtoId = produtoId;
                                        itemPedido.produtoIdDaEmpresa = produto.produtoIdDaEmpresa;
                                        itemPedido.itemQuantidade = 1;
                                        itemPedido.itemValor = Convert.ToDecimal(itemPedidoEx.salePrice);
                                        itemPedido.dataDaCriacao = DateTime.Now;
                                        itemPedido.garantiaId = 0;
                                        itemPedido.itemPresente = "False";
                                        itemPedido.valorDoEmbrulhoECartao = 0;

                                        itensPedidoDc.tbItensPedido.Add(itemPedido);

                                        itensPedidoDc.SaveChanges();

                                        var itensFilho = (from c in produtoDc.tbProdutoRelacionado where c.idProdutoPai == produto.produtoId select c).ToList();
                                        if (itensFilho.Count > 0)
                                        {
                                            List<int> idsProdutosFilhos = new List<int>();
                                            idsProdutosFilhos.AddRange(itensFilho.Select(x => x.idProdutoFilho).ToList());
                                            var produtosFilhos = (from c in produtoDc.tbProdutos where idsProdutosFilhos.Contains(c.produtoId) select c).ToList();

                                            decimal valorTotalVendaCombo = 0;

                                            foreach (var itemFilho in itensFilho)
                                            {
                                                var produtoFilho = (from c in produtosFilhos where c.produtoId == itemFilho.idProdutoFilho select c).FirstOrDefault();
                                                if (produtoFilho != null)
                                                {
                                                    valorTotalVendaCombo += produtoFilho.produtoPreco;
                                                }
                                            }

                                            foreach (var itemFilho in itensFilho)
                                            {
                                                var produtoFilho = (from c in produtosFilhos where c.produtoId == itemFilho.idProdutoFilho select c).FirstOrDefault();
                                                decimal precoNoCombo = 0;
                                                try
                                                {
                                                    precoNoCombo = (produtoFilho.produtoPreco * produto.produtoPreco) / valorTotalVendaCombo;
                                                }
                                                catch (Exception)
                                                {

                                                }
                                                var itemPedidoCombo = new tbItensPedidoCombo();
                                                itemPedidoCombo.idItemPedido = itemPedido.itemPedidoId;
                                                itemPedidoCombo.produtoId = itemFilho.idProdutoFilho;
                                                try
                                                {
                                                    itemPedidoCombo.precoDeCusto = Convert.ToDecimal(produtoFilho.produtoPrecoDeCusto);
                                                }
                                                catch (Exception)
                                                {
                                                    itemPedidoCombo.precoDeCusto = 0;
                                                }
                                                if (itemFilho.desconto == 100)
                                                {
                                                    itemPedidoCombo.precoDeCusto = 0;
                                                }
                                                itemPedidoCombo.valorNoCombo = precoNoCombo;
                                                itemPedidoCombo.enviado = false;
                                                produtoDc.tbItensPedidoCombo.Add(itemPedidoCombo);
                                                produtoDc.SaveChanges();
                                            }
                                        }
                                    }
                                }


                                var queue = new dbSiteEntities();

                                var queueItensFilho = new tbQueue();
                                queueItensFilho.tipoQueue = 4;
                                queueItensFilho.agendamento = DateTime.Now;
                                queueItensFilho.idRelacionado = pedido.pedidoId;
                                queueItensFilho.mensagem = "";
                                queueItensFilho.concluido = false;
                                queueItensFilho.andamento = false;
                                queue.tbQueue.Add(queueItensFilho);
                                queue.SaveChanges();

                                var queueReservarEstoque = new tbQueue();
                                queueReservarEstoque.tipoQueue = 5;
                                queueReservarEstoque.agendamento = DateTime.Now;
                                queueReservarEstoque.idRelacionado = pedido.pedidoId;
                                queueReservarEstoque.mensagem = "";
                                queueReservarEstoque.concluido = false;
                                queueReservarEstoque.andamento = false;
                                queue.tbQueue.Add(queueReservarEstoque);
                                queue.SaveChanges();

                                var queuePrazo = new tbQueue();
                                queuePrazo.tipoQueue = 6;
                                queuePrazo.agendamento = DateTime.Now;
                                queuePrazo.idRelacionado = pedido.pedidoId;
                                queuePrazo.mensagem = "";
                                queuePrazo.concluido = false;
                                queuePrazo.andamento = false;
                                queue.tbQueue.Add(queuePrazo);
                                queue.SaveChanges();

                                itensPedidoDc.SaveChanges();
                                //checaPedidoAvulsoExtra(pedido.pedidoId);
                            }
                            else
                            {
                                if (pedidoCheck.statusDoPedido == 2)
                                {
                                    var clienteDc = new dbSiteEntities();
                                    int clienteId = 0;
                                    var clienteCheck = (from c in clienteDc.tbClientes
                                                        where
                                                            c.clienteCPFCNPJ == pedidoEx.documentNr && c.idOrigemCliente == extraOrigemClienteId
                                                        select c).FirstOrDefault();
                                    if (clienteCheck != null)
                                    {
                                        clienteId = clienteCheck.clienteId;
                                    }
                                    else
                                    {
                                        var cliente = new tbClientes();
                                        cliente.clienteNome = pedidoEx.customerName;
                                        cliente.clienteCPFCNPJ = pedidoEx.documentNr;
                                        cliente.clienteRGIE = "";
                                        cliente.clienteSexo = pedidoEx.customerGender;
                                        cliente.clienteRua = pedidoEx.billingInformations.FirstOrDefault().address;
                                        cliente.clienteNumero = pedidoEx.billingInformations.FirstOrDefault().addressNr;
                                        cliente.clienteComplemento =
                                            pedidoEx.billingInformations.FirstOrDefault().additionalInfo;
                                        cliente.clienteBairro = pedidoEx.billingInformations.FirstOrDefault().quarter;
                                        cliente.clienteCidade = pedidoEx.billingInformations.FirstOrDefault().city;
                                        cliente.clienteEstado = pedidoEx.billingInformations.FirstOrDefault().address;
                                        cliente.clientePais = "Brasil";
                                        cliente.clienteCep = pedidoEx.billingInformations.FirstOrDefault().postalCd;
                                        cliente.clienteEmail = "marketplace";
                                        cliente.clienteSenha = "marketplace";
                                        cliente.clienteDataNascimento = "01/01/1900";
                                        cliente.clienteFoneResidencial = "";
                                        cliente.clienteFoneComercial = "";
                                        cliente.clienteFoneCelular = "";
                                        cliente.clienteRecebeInformativo = "False";
                                        cliente.idOrigemCliente = extraOrigemClienteId;
                                        cliente.dataDaCriacao = DateTime.Now;
                                        clienteDc.tbClientes.Add(cliente);
                                        clienteDc.SaveChanges();
                                        clienteId = cliente.clienteId;
                                    }

                                    pedidoCheck.clienteId = clienteId;
                                    int tipoDeEntrega = 8;
                                    pedidoCheck.tipoDeEntregaId = tipoDeEntrega;

                                    pedidoCheck.valorDoFrete = Convert.ToDecimal(pedidoEx.freightChargedAmount);
                                    pedidoCheck.prazoDeEntrega = "0";
                                    pedidoCheck.valorDosItens = Convert.ToDecimal(pedidoEx.totalAmount);
                                    pedidoCheck.valorTotalGeral = Convert.ToDecimal(pedidoEx.totalAmount);
                                    pedidoCheck.valorCobrado = Convert.ToDecimal(pedidoEx.totalAmount);
                                    pedidoCheck.valorDosJuros = 0;
                                    pedidoCheck.valorDoEmbrulhoECartao = 0;
                                    pedidoCheck.porcentagemDoJuros = 0;
                                    pedidoCheck.numeroDeParcelas = 1;
                                    pedidoCheck.valorDaParcela = Convert.ToDecimal(pedidoEx.totalAmount);
                                    pedidoCheck.pesoDoPedido = 0;
                                    //pedidoCheck.dataHoraDoPedido = DateTime.Now;
                                    pedidoCheck.statusDoPedido = marketplaceStatusPagamentoConfirmado;
                                    pedidoCheck.endNomeDoDestinatario =
                                        pedidoEx.shippingInformationsList.FirstOrDefault().recipientName;
                                    pedidoCheck.endRua = pedidoEx.shippingInformationsList.FirstOrDefault().address;
                                    pedidoCheck.endNumero = pedidoEx.shippingInformationsList.FirstOrDefault().addressNr;
                                    pedidoCheck.endComplemento =
                                        pedidoEx.shippingInformationsList.FirstOrDefault().additionalInfo;
                                    pedidoCheck.endBairro = pedidoEx.shippingInformationsList.FirstOrDefault().quarter;
                                    pedidoCheck.endCidade = pedidoEx.shippingInformationsList.FirstOrDefault().city;
                                    pedidoCheck.endEstado = pedidoEx.shippingInformationsList.FirstOrDefault().state;
                                    pedidoCheck.endPais = "Brasil";
                                    pedidoCheck.endCep = pedidoEx.shippingInformationsList.FirstOrDefault().postalCd;
                                    pedidoCheck.marketplace = true;
                                    pedidoCheck.marketplaceId = pedidoEx.orderId;
                                    pedidoCheck.dataConfirmacaoPagamento = DateTime.Now;
                                    pedidoDc.SaveChanges();



                                    var queue = new dbSiteEntities();

                                    var queueItensFilho = new tbQueue();
                                    queueItensFilho.tipoQueue = 4;
                                    queueItensFilho.agendamento = DateTime.Now;
                                    queueItensFilho.idRelacionado = pedidoCheck.pedidoId;
                                    queueItensFilho.mensagem = "";
                                    queueItensFilho.concluido = false;
                                    queueItensFilho.andamento = false;
                                    queue.tbQueue.Add(queueItensFilho);
                                    queue.SaveChanges();

                                    var queueReservarEstoque = new tbQueue();
                                    queueReservarEstoque.tipoQueue = 5;
                                    queueReservarEstoque.agendamento = DateTime.Now;
                                    queueReservarEstoque.idRelacionado = pedidoCheck.pedidoId;
                                    queueReservarEstoque.mensagem = "";
                                    queueReservarEstoque.concluido = false;
                                    queueReservarEstoque.andamento = false;
                                    queue.tbQueue.Add(queueReservarEstoque);
                                    queue.SaveChanges();

                                    var queuePrazo = new tbQueue();
                                    queuePrazo.tipoQueue = 6;
                                    queuePrazo.agendamento = DateTime.Now;
                                    queuePrazo.idRelacionado = pedidoCheck.pedidoId;
                                    queuePrazo.mensagem = "";
                                    queuePrazo.concluido = false;
                                    queuePrazo.andamento = false;
                                    queue.tbQueue.Add(queuePrazo);
                                    queue.SaveChanges();
                                }
                            }
                        }
                    }
                    catch (Exception)
                    {
                        possuiMais = false;
                    }
                }
            
        }

    }
}
