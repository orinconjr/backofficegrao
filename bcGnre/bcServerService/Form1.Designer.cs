﻿namespace bcServerService
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lblStatus = new System.Windows.Forms.Label();
            this.timerGnre = new System.Windows.Forms.Timer(this.components);
            this.bwGnre = new System.ComponentModel.BackgroundWorker();
            this.txtData = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnGerarGuias = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus.Location = new System.Drawing.Point(12, 58);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(100, 31);
            this.lblStatus.TabIndex = 1;
            this.lblStatus.Text = "Status:";
            // 
            // timerGnre
            // 
            this.timerGnre.Interval = 1;
            this.timerGnre.Tick += new System.EventHandler(this.timerGnre_Tick);
            // 
            // bwGnre
            // 
            this.bwGnre.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwGnre_DoWork);
            this.bwGnre.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bwGnre_RunWorkerCompleted);
            // 
            // txtData
            // 
            this.txtData.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtData.Location = new System.Drawing.Point(12, 26);
            this.txtData.Name = "txtData";
            this.txtData.Size = new System.Drawing.Size(132, 29);
            this.txtData.TabIndex = 21;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(247, 13);
            this.label1.TabIndex = 22;
            this.label1.Text = "Data de Vencimento: (deixar em branco para o dia)";
            // 
            // btnGerarGuias
            // 
            this.btnGerarGuias.Location = new System.Drawing.Point(150, 26);
            this.btnGerarGuias.Name = "btnGerarGuias";
            this.btnGerarGuias.Size = new System.Drawing.Size(109, 29);
            this.btnGerarGuias.TabIndex = 23;
            this.btnGerarGuias.Text = "Gerar Guias";
            this.btnGerarGuias.UseVisualStyleBackColor = true;
            this.btnGerarGuias.Click += new System.EventHandler(this.btnGerarGuias_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(465, 115);
            this.Controls.Add(this.btnGerarGuias);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtData);
            this.Controls.Add(this.lblStatus);
            this.Name = "Form1";
            this.Text = "Guias GNRE";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.Timer timerGnre;
        private System.ComponentModel.BackgroundWorker bwGnre;
        private System.Windows.Forms.TextBox txtData;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnGerarGuias;
    }
}

