﻿using Amazon;
using Amazon.CloudFront;
using Amazon.CloudFront.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Linq;

namespace bcServerService.CloudFront
{
    public class AWSCloudFrontService : IAWSCloudFrontService
    {
        private readonly string _accessKey = ConfigurationManager.AppSettings["AWSCloudFront.AccessKey"];
        private readonly string _secretKey = ConfigurationManager.AppSettings["AWSCloudFront.SecretKey"];
        private readonly RegionEndpoint _region = RegionEndpoint.GetBySystemName(ConfigurationManager.AppSettings["AWSCloudFront.Region"]);
        private string _distributionID = ConfigurationManager.AppSettings["AWSCloudFront.DistributionId"];

        private readonly IAmazonCloudFront _client;
        public AWSCloudFrontService()
        {
            _client = AWSClientFactory.CreateAmazonCloudFrontClient(_accessKey, _secretKey, _region);
        }
        public void CreateInvalidation(List<string> paths)
        {
            if (paths == null)
                throw new ArgumentNullException("paths");

            if (!paths.Any())
                throw new InvalidArgumentException("O caminho de um ou mais objetos deve ser informado.");

            var invalidationRequest = new CreateInvalidationRequest
            {
                DistributionId = _distributionID,
                InvalidationBatch = new InvalidationBatch
                {
                    Paths = new Paths
                    {
                        Quantity = paths.Count,
                        Items = paths.ToList()
                    },
                    CallerReference = DateTime.Now.Ticks.ToString()
                }
            };

            _client.CreateInvalidation(invalidationRequest);
        }



        private BackgroundWorker bwInvalidarCache = new BackgroundWorker();
        public void InvalidarCacheQueue(long idQueue)
        {
            if (bwInvalidarCache.IsBusy != true)
            {
                bwInvalidarCache.DoWork += new DoWorkEventHandler(bwInvalidarCache_DoWork);
                bwInvalidarCache.RunWorkerAsync(idQueue);
            }

        }


        private void bwInvalidarCache_DoWork(object sender, DoWorkEventArgs e)
        {
            var dataInicio = DateTime.Now;

            long idQueue = Convert.ToInt64(e.Argument);
            #region atualizaQueue
            var dataQueue = new dbSiteEntities();
            var queue = (from c in dataQueue.tbQueue where c.idQueue == idQueue select c).First();
            queue.andamento = true;
            queue.dataInicio = DateTime.Now;
            dataQueue.SaveChanges();
            #endregion atualizaQueue


            var paths = new List<string>();
            paths.Add("/");
            var awsCloudFrontService = new CloudFront.AWSCloudFrontService();
            awsCloudFrontService.CreateInvalidation(paths);




            #region finalizaQueue

            queue.concluido = true;
            queue.andamento = true;
            queue.dataConclusao = DateTime.Now;
            dataQueue.SaveChanges();

            var queuesRelacionados = (from c in dataQueue.tbQueue
                                      where
                                          c.tipoQueue == queue.tipoQueue  &&
                                          c.agendamento < dataInicio && c.andamento == false && c.concluido == false
                                      select c).ToList();
            foreach (var queueRelacionado in queuesRelacionados)
            {
                queueRelacionado.dataInicio = dataInicio;
                queueRelacionado.andamento = true;
                queueRelacionado.concluido = true;
                queueRelacionado.dataConclusao = queue.dataConclusao;
            }
            dataQueue.SaveChanges();

            #endregion



        }


    }
}
