//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace bcServerService
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbCentroDistribuicao
    {
        public int idCentroDistribuicao { get; set; }
        public string nome { get; set; }
        public bool enderecamentoAutomatico { get; set; }
        public Nullable<int> idEnderecamentoArea { get; set; }
        public Nullable<int> idEnderecamentoRua { get; set; }
        public Nullable<int> idEnderecamentoPredio { get; set; }
        public Nullable<int> idEnderecamentoAndar { get; set; }
        public Nullable<int> idEnderecamentoApartamento { get; set; }
        public Nullable<int> idCentroDistribuicaoGrupo { get; set; }
    }
}
