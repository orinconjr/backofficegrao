//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace bcServerService
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbFornecedorUsuarioAcesso
    {
        public int idFornecedor { get; set; }
        public int idFornecedorUsuario { get; set; }
        public System.DateTime dataHora { get; set; }
        public string ip { get; set; }
        public int idFornecedorUsuarioAcesso { get; set; }
    
        public virtual tbFornecedorUsuario tbFornecedorUsuario { get; set; }
        public virtual tbProdutoFornecedor tbProdutoFornecedor { get; set; }
    }
}
