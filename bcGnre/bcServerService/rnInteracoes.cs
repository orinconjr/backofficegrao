﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace bcServerService
{
    class rnInteracoes
    {
        public static bool interacaoInclui(int pedidoId, string interacao, string usuarioQueInteragiu, string alteracaoDeEstatus)
        {
            var data = new dbSiteEntities();
            var interacaoAdd = new tbPedidoInteracoes();
            interacaoAdd.pedidoId = pedidoId;
            interacaoAdd.interacao = interacao;
            interacaoAdd.interacaoData = DateTime.Now;
            interacaoAdd.usuarioQueInteragiu = usuarioQueInteragiu;
            interacaoAdd.alteracaoDeEstatus = alteracaoDeEstatus;
            data.tbPedidoInteracoes.Add(interacaoAdd);
            data.SaveChanges();
            return true;
        }
    }
}
