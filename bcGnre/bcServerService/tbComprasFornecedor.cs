//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace bcServerService
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbComprasFornecedor
    {
        public tbComprasFornecedor()
        {
            this.tbComprasFornecedorCondicaoPagamento = new HashSet<tbComprasFornecedorCondicaoPagamento>();
            this.tbComprasOrdem = new HashSet<tbComprasOrdem>();
            this.tbComprasProdutoFornecedor = new HashSet<tbComprasProdutoFornecedor>();
            this.tbComprasProdutoFornecedorHistorico = new HashSet<tbComprasProdutoFornecedorHistorico>();
        }
    
        public int idComprasFornecedor { get; set; }
        public string fornecedor { get; set; }
        public string contato { get; set; }
        public string email { get; set; }
        public string telefone { get; set; }
        public int prazo { get; set; }
    
        public virtual ICollection<tbComprasFornecedorCondicaoPagamento> tbComprasFornecedorCondicaoPagamento { get; set; }
        public virtual ICollection<tbComprasOrdem> tbComprasOrdem { get; set; }
        public virtual ICollection<tbComprasProdutoFornecedor> tbComprasProdutoFornecedor { get; set; }
        public virtual ICollection<tbComprasProdutoFornecedorHistorico> tbComprasProdutoFornecedorHistorico { get; set; }
    }
}
