//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace bcServerService
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbComprasProduto
    {
        public tbComprasProduto()
        {
            this.tbComprasOrdemProduto = new HashSet<tbComprasOrdemProduto>();
            this.tbComprasProdutoFornecedor = new HashSet<tbComprasProdutoFornecedor>();
            this.tbComprasProdutoFornecedorHistorico = new HashSet<tbComprasProdutoFornecedorHistorico>();
            this.tbComprasSolicitacaoProduto = new HashSet<tbComprasSolicitacaoProduto>();
            this.tbProdutoMateriaPrima = new HashSet<tbProdutoMateriaPrima>();
        }
    
        public int idComprasProduto { get; set; }
        public string produto { get; set; }
        public System.DateTime dataCadastro { get; set; }
        public bool materiaPrima { get; set; }
    
        public virtual ICollection<tbComprasOrdemProduto> tbComprasOrdemProduto { get; set; }
        public virtual ICollection<tbComprasProdutoFornecedor> tbComprasProdutoFornecedor { get; set; }
        public virtual ICollection<tbComprasProdutoFornecedorHistorico> tbComprasProdutoFornecedorHistorico { get; set; }
        public virtual ICollection<tbComprasSolicitacaoProduto> tbComprasSolicitacaoProduto { get; set; }
        public virtual ICollection<tbProdutoMateriaPrima> tbProdutoMateriaPrima { get; set; }
    }
}
