//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace landingPages.context
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbEspecificacaoItens
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tbEspecificacaoItens()
        {
            this.tbProdutoItemEspecificacao = new HashSet<tbProdutoItemEspecificacao>();
        }
    
        public int especificacaoItensId { get; set; }
        public int especificacaoId { get; set; }
        public string especificacaoItenNome { get; set; }
        public Nullable<System.DateTime> dataDaCriacao { get; set; }
    
        public virtual tbEspecificacao tbEspecificacao { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbProdutoItemEspecificacao> tbProdutoItemEspecificacao { get; set; }
    }
}
