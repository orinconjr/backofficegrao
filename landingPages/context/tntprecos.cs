//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace landingPages.context
{
    using System;
    using System.Collections.Generic;
    
    public partial class tntprecos
    {
        public int idtntprecos { get; set; }
        public string tarifa { get; set; }
        public decimal valor { get; set; }
        public decimal valorkg { get; set; }
        public decimal seguro { get; set; }
        public string uf { get; set; }
    }
}
