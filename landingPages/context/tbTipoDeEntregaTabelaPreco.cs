//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace landingPages.context
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbTipoDeEntregaTabelaPreco
    {
        public int idTipoDeEntregaTabelaPreco { get; set; }
        public int idTipoDeEntrega { get; set; }
        public string nome { get; set; }
        public decimal valorKgAdicional { get; set; }
        public decimal valorSeguro { get; set; }
    }
}
