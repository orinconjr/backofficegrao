//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace landingPages.context
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbTipoDeEntregaTabelaPrecoValor
    {
        public int idTipoDeEntregaTabelaPrecoValor { get; set; }
        public int idTipoDeEntregaTabelaPreco { get; set; }
        public int pesoInicial { get; set; }
        public int pesoFinal { get; set; }
        public decimal valor { get; set; }
    }
}
