//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace landingPages.context
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbComprasOrdemProduto
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tbComprasOrdemProduto()
        {
            this.tbComprasOrdemProdutoHistorico = new HashSet<tbComprasOrdemProdutoHistorico>();
        }
    
        public int idComprasOrdemProduto { get; set; }
        public int idComprasProduto { get; set; }
        public int quantidade { get; set; }
        public decimal preco { get; set; }
        public int idComprasOrdem { get; set; }
        public int status { get; set; }
        public string comentario { get; set; }
        public Nullable<int> idComprasSolicitacaoProduto { get; set; }
        public System.DateTime dataCadastro { get; set; }
        public Nullable<int> quantidadeRecebida { get; set; }
        public Nullable<decimal> precoRecebido { get; set; }
    
        public virtual tbComprasOrdem tbComprasOrdem { get; set; }
        public virtual tbComprasProduto tbComprasProduto { get; set; }
        public virtual tbComprasSolicitacaoProduto tbComprasSolicitacaoProduto { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbComprasOrdemProdutoHistorico> tbComprasOrdemProdutoHistorico { get; set; }
    }
}
