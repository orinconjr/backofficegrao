//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace landingPages.context
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbDespesaCategoria
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tbDespesaCategoria()
        {
            this.tbDespesa = new HashSet<tbDespesa>();
        }
    
        public int idDespesaCategoria { get; set; }
        public string despesaCategoria { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbDespesa> tbDespesa { get; set; }
    }
}
