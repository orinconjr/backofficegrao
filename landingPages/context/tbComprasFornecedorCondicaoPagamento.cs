//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace landingPages.context
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbComprasFornecedorCondicaoPagamento
    {
        public int idComprasFornecedorCondicaoPagamento { get; set; }
        public int idComprasFornecedor { get; set; }
        public int idComprasCondicaoPagamento { get; set; }
        public int acrescimo { get; set; }
        public int desconto { get; set; }
    
        public virtual tbComprasCondicaoPagamento tbComprasCondicaoPagamento { get; set; }
        public virtual tbComprasFornecedor tbComprasFornecedor { get; set; }
    }
}
