//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace landingPages.context
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbInteracaoTransportadora
    {
        public int idInteracaoTransportadora { get; set; }
        public int pedidoId { get; set; }
        public string usuario { get; set; }
        public System.DateTime data { get; set; }
        public string interacao { get; set; }
    
        public virtual tbPedidos tbPedidos { get; set; }
    }
}
