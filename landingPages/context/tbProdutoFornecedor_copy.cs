//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace landingPages.context
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbProdutoFornecedor_copy
    {
        public int fornecedorId { get; set; }
        public string fornecedorIdDaEmpresa { get; set; }
        public string fornecedorNome { get; set; }
        public string fornecedorNomeFantasia { get; set; }
        public string fornecedorTelefone { get; set; }
        public string fornecedorEmail { get; set; }
        public string fornecedorSite { get; set; }
        public Nullable<long> fornecedorCNPJCPF { get; set; }
        public string fornecedorIERG { get; set; }
        public string fornecedorNomeDoContato { get; set; }
        public string fornecedorTelefoneDoContato { get; set; }
        public string fornecedorRua { get; set; }
        public string fornecedorNumero { get; set; }
        public string fornecedorComplemento { get; set; }
        public string fornecedorBairro { get; set; }
        public string fornecedorCidade { get; set; }
        public string fornecedorEstado { get; set; }
        public string fornecedorCep { get; set; }
        public Nullable<System.DateTime> dataDaCriacao { get; set; }
        public Nullable<int> fornecedorPrazoDeFabricacao { get; set; }
        public Nullable<System.DateTime> dataInicioFabricacao { get; set; }
        public Nullable<bool> estoqueConferido { get; set; }
        public Nullable<int> fornecedorPrazoPedidos { get; set; }
        public Nullable<bool> prazoFornecedorDiasUteis { get; set; }
        public bool gerarPedido { get; set; }
        public bool pedidoSegunda { get; set; }
        public bool pedidoTerca { get; set; }
        public bool pedidoQuarta { get; set; }
        public bool pedidoQuinta { get; set; }
        public bool pedidoSexta { get; set; }
        public bool pedidoSabado { get; set; }
        public bool pedidoDomingo { get; set; }
        public bool reposicaoAutomaticaEstoque { get; set; }
        public int reposicaoAutomaticaMinimoMensal { get; set; }
        public int relevanciaProdutos { get; set; }
        public Nullable<System.DateTime> dataFimFabricacao { get; set; }
        public string codigoEtiqueta { get; set; }
        public bool entrega { get; set; }
    }
}
