//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace landingPages.context
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbCompreJuntoGrupo
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tbCompreJuntoGrupo()
        {
            this.tbJuncaoProdutoCompreJunto = new HashSet<tbJuncaoProdutoCompreJunto>();
        }
    
        public int idDoGrupo { get; set; }
        public string nomeDoGrupo { get; set; }
        public double porcentagemDeDesconto { get; set; }
        public Nullable<System.DateTime> dataDaCriacao { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbJuncaoProdutoCompreJunto> tbJuncaoProdutoCompreJunto { get; set; }
    }
}
