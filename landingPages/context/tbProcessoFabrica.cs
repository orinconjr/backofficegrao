//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace landingPages.context
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbProcessoFabrica
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tbProcessoFabrica()
        {
            this.tbJuncaoProdutoProcessoFabrica = new HashSet<tbJuncaoProdutoProcessoFabrica>();
            this.tbSubProcessoFabrica = new HashSet<tbSubProcessoFabrica>();
        }
    
        public int idProcessoFabrica { get; set; }
        public string processo { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbJuncaoProdutoProcessoFabrica> tbJuncaoProdutoProcessoFabrica { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbSubProcessoFabrica> tbSubProcessoFabrica { get; set; }
    }
}
