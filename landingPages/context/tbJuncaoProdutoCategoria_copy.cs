//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace landingPages.context
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbJuncaoProdutoCategoria_copy
    {
        public int produtoId { get; set; }
        public int categoriaId { get; set; }
        public int produtoPaiId { get; set; }
        public Nullable<System.DateTime> dataDaCriacao { get; set; }
        public int juncaoProdutoCategoriaId { get; set; }
    
        public virtual tbProdutoCategoria tbProdutoCategoria { get; set; }
        public virtual tbProdutos tbProdutos { get; set; }
    }
}
