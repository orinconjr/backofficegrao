//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace landingPages.context
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbLandingPageTemplate
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tbLandingPageTemplate()
        {
            this.tbLandingPage = new HashSet<tbLandingPage>();
        }
    
        public int idLandingPageTemplate { get; set; }
        public string nome { get; set; }
        public string template { get; set; }
        public string templateProdutoAdicional { get; set; }
        public bool ativo { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbLandingPage> tbLandingPage { get; set; }
    }
}
