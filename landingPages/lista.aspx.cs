﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using landingPages.context;

namespace landingPages
{
    public partial class lista : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
            usuarioLogadoId = Request.Cookies["usuarioLogadoId"];

            bool valido = false;
            if (usuarioLogadoId != null)
            {
                int idUsuario = Convert.ToInt32(usuarioLogadoId.Value);
                var data = new dbCommerceEntities();
                var paginaPermitida = (from c in data.tbUsuarioPaginasPermitida
                    where
                        c.usuarioId == idUsuario &&
                        c.paginaPermitidaNome == "landingpage" select c).FirstOrDefault();
                if (paginaPermitida != null)
                {
                    valido = true;
                    lblIp.Text = Request.UserHostAddress.ToString();
                }
            }
            if (!valido)
            {
                Response.Redirect("Default.aspx");
            }


            if (!Page.IsPostBack)
            {
                CarregaGrid();
            }
        }

        private void CarregaGrid()
        {
            using (var data = new dbCommerceEntities())
            {
                var landingPages = (from c in data.tbLandingPage select c).ToList();

                if (!string.IsNullOrEmpty(txtIdProduto.Text))
                {
                    int idProduto = 0;
                    int.TryParse(txtIdProduto.Text, out idProduto);
                    landingPages = landingPages.Where(x => x.idProduto == idProduto).ToList();
                }
                if (!string.IsNullOrEmpty(txtUrl.Text))
                {
                    landingPages = landingPages.Where(x => x.enderecoHotsite.Contains(txtUrl.Text)).ToList();
                }

                GridView1.DataSource = landingPages;
                GridView1.DataBind();
            }
        }

        protected void btnCadastrar_OnClick(object sender, EventArgs e)
        {
            Response.Redirect("cadastro.aspx");
        }

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HtmlAnchor linkeditar = (HtmlAnchor)e.Row.FindControl("linkeditar");
                linkeditar.HRef = "cadastro.aspx?id=" + DataBinder.Eval(e.Row.DataItem, "idLandingPage");

            }
        }

        protected void btnFiltrar_Click(object sender, EventArgs e)
        {
            CarregaGrid();
        }

        protected void GridView1_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView1.PageIndex = e.NewPageIndex;
            CarregaGrid();
        }
    }
}