﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="cadastro.aspx.cs" Inherits="landingPages.cadastro" validateRequest="false" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>BarkCommerce - Sistema Administrativo</title>
    <link href="estilos/estilos.css" rel="stylesheet" type="text/css" />
    <script src="tinymce/tinymce.min.js"></script>
    <script type="text/javascript">
        tinymce.init({
            selector: 'textarea',
            height: 500,
            plugins: [
              'advlist autolink lists link image charmap print preview anchor',
              'searchreplace visualblocks code fullscreen'
            ],
            toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent'
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <table align="center" cellpadding="0" cellspacing="0" class="tablePrincipal" style="width: 100%">
            <tr>
                <td>
                    <table cellpadding="0" cellspacing="0" style="width: 920px" align="center">
                        <tr>
                            <td>
                                <table cellpadding="0" cellspacing="0" style="width: 920px">
                                    <tr>
                                        <td style="width: 210px; height: 97px"></td>
                                        <td style="width: 140px; height: 97px"></td>
                                        <td style="width: 189px; height: 97px"></td>
                                        <td style="width: 208px; height: 97px"></td>
                                        <td style="height: 97px"></td>
                                    </tr>
                                    <tr valign="bottom">
                                        <td style="width: 210px">&nbsp;</td>
                                        <td class="textoCabecalho" style="width: 140px"></td>
                                        <td class="textoCabecalho" style="width: 189px"></td>
                                        <td class="textoCabecalho" style="width: 208px"></td>
                                        <td></td>
                                        <td class="textoCabecalho">
                                            Seu IP:
                                            <asp:Label ID="lblIp" runat="server" Font-Bold="True" Text="200.258.110.123" EnableTheming="False"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height="12"></td>
                        </tr>

                        <tr>
                            <td height="30"></td>
                        </tr>
                        <tr>
                            <td bgcolor="White" style="height: 300px" valign="top">
                                <table cellpadding="0" cellspacing="0" style="width: 920px">
                                    <tr>
                                        <td class="tituloPaginas" valign="top">
                                            <asp:Label ID="lblAcao" runat="server">Cadastro de Landing Pages</asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
                                                <tr>
                                                    <td class="rotulos" style="font-size: 14px; text-align: right;">
                                                        <a href="lista.aspx">Voltar</a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="rotulos" style="font-size: 14px; padding-top: 30px">
                                                        <asp:HyperLink runat="server" Target="_blank" Visible="False" id="hplAbrirSite">Abrir Hot Site</asp:HyperLink>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="rotulos" style="padding-top: 15px;">
                                                        ID do Produto no Site:<br/>
                                                        <asp:HiddenField runat="server" id="hdfIdLanding"/>
                                                        <asp:TextBox runat="server" id="txtIdProduto" Width="150"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="rotulos" style="padding-top: 15px;">
                                                        Endereço do Hotsite (sem www):<br/>
                                                        <asp:TextBox runat="server" id="txtEnderecoHotSite" Width="350"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="rotulos" style="padding-top: 15px;">
                                                        Título:<br/>
                                                        <asp:TextBox runat="server" id="txtTitulo" Width="350"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="rotulos" style="padding-top: 15px;">
                                                        Resumo:<br/>
                                                        <asp:TextBox runat="server" ID="txtResumo" TextMode="MultiLine"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="rotulos" style="padding-top: 15px;">
                                                        Texto:<br/>
                                                        <asp:TextBox runat="server" ID="txtTexto" TextMode="MultiLine"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="rotulos" style="padding-top: 15px;">
                                                        <asp:Panel runat="server" DefaultButton="btnAdicionarProdutos" Visible="false">
                                                           Produtos adicionais:<br/>
                                                           <asp:HiddenField runat="server" ID="hdfProdutosAdicionais" />
                                                           <asp:TextBox runat="server" id="txtProdutosAdicionais"></asp:TextBox> <asp:Button runat="server" id="btnAdicionarProdutos" OnClick="btnAdicionarProdutos_OnClick" Text="Adicionar"/>
                                                        </asp:Panel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-top: 35px;">
                                                        <asp:DataList ID="dtlFotos" runat="server" CellPadding="0" RepeatColumns="4">
                                                        <ItemTemplate>
                                                            <table cellpadding="0" cellspacing="0" style="width: 200px">
                                                                <tr>
                                                                    <td align="center">

                                                                        <asp:Image ID="imgFotos" runat="server"
                                                                            ImageUrl='<%# "http://dmhxz00kguanp.cloudfront.net/fotos/" + Eval("ProdutoId") + "/media_" + Eval("ProdutoFoto") + ".jpg" %>'
                                                                            ToolTip='<%# Eval("ProdutoFoto") %>' Height="180px" Width="180px"></asp:Image>
                                                                    </td>
                                                                </tr>

                                                                <tr>
                                                                    <td align="center" class="rotulos">
                                                                        <%# Eval("ProdutoNome") %>
                                                                    </td>

                                                                </tr>
                                                                <tr>
                                                                    <td style="text-align: center;">
                                                                        <asp:Button runat="server" ID="btnExcluirFoto" OnCommand="btnExcluirFoto_OnCommand" CommandArgument='<%# Eval("ProdutoId") %>' Text="Remover" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ItemTemplate>
                                                    </asp:DataList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-top: 40px; text-align: right;">
                                                        <asp:Button runat="server" ID="btnGravar" Text="Gravar" OnClick="btnGravar_OnClick"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>

                            </td>
                        </tr>
                        <tr>
                            <td style="height: 176px; background-image: url('images/rodape.jpg');">
                                <table cellpadding="0" cellspacing="0" style="width: 920px">
                                    <tr>
                                        <td style="width: 705px; height: 63px"></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 705px">&nbsp;</td>
                                        <td>
                                            <asp:HyperLink ID="btEmail" runat="server" ImageUrl="images/email.gif" NavigateUrl="mailto:suporte@bark.com.br"></asp:HyperLink>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
