﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using landingPages.context;

namespace landingPages
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void imbEntrar_Click(object sender, ImageClickEventArgs e)
        {
            using (var data = new dbCommerceEntities())
            {
                var usuario =
                    (from c in data.tbUsuario
                        where c.usuarioNome == txtNome.Text && c.usuarioSenha == txtSenha.Text
                        select c).FirstOrDefault();
                if (usuario != null)
                {
                    string usuarioId = usuario.usuarioId.ToString();
                    if (!string.IsNullOrEmpty(usuarioId))
                    {
                        HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
                        usuarioLogadoId.Value = usuarioId;
                        usuarioLogadoId.Expires = DateTime.Now.AddHours(10);
                        Response.Cookies.Add(usuarioLogadoId);

                        Response.Redirect("lista.aspx");
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, typeof(string), "Error", "alert('Dados inválidos, Tente novamente.');", true);
                }
                    
            }
        }
    }
}