﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(landingPages.Startup))]
namespace landingPages
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
