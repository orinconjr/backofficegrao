﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="lista.aspx.cs" Inherits="landingPages.lista" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>BarkCommerce - Sistema Administrativo</title>
    <link href="estilos/estilos.css" rel="stylesheet" type="text/css" />
    <style>
        .fieldsetatualizarprecos {
            float: left;
            clear: left;
            border-radius: 5px;
            margin-left: 25px;
        }

            .fieldsetatualizarprecos input[type=checkbox] {
                margin-top: 15px;
            }

            .fieldsetatualizarprecos label {
                margin-left: 5px;
                margin-right: 20px;
            }

        .meugrid {
            background-color: #F4FAFB;
            border: 0px none;
            /* border-collapse: separate !important; */
            overflow: hidden;
            font: 9pt Tahoma;
            color: #000;
            cursor: pointer;
            white-space: nowrap;
            font-weight: normal;
            text-align: left;
            width: 834px;
            border-collapse: collapse;
            float: left;
            clear: left;
            margin-top: 10px;
            margin-bottom: 10px;
            margin-left: 25px;
        }

            .meugrid th {
                background-image: url(../../App_Themes/Glass/GridView/gvHeaderBackground.gif);
                background-position: center top;
                background-repeat: repeat-x;
                background-color: #C0DDE0;
                overflow: hidden;
                height: 23px;
                line-height: 23px;
                padding: 4px 6px 5px;
                font-weight: normal;
                border: 1px solid #7EACB1;
                /* border-collapse: separate;*/
            }

            .meugrid td {
                overflow: hidden;
                height: 23px;
                line-height: 23px;
                padding: 4px 6px 5px;
                border: 1px solid #7EACB1;
            }

            .meugrid tr td:nth-child(4) {
                text-align: center;
            }

            .meugrid tr td:last-of-type {
                text-align: center;
            }

        .tabrotulos {
            width: 848px;
            margin: auto;
        }

        .materiaprima {
            width: 250px;
        }

            .materiaprima ul {
                margin: 0px;
                padding-left: 10px;
            }

                .materiaprima ul li {
                    max-width: 230px;
                    white-space: pre-wrap;
                }

        .meugrid tr td:last-of-type {
            text-align: center;
        }

        .nome {
            width: 237px;
        }

            .nome div {
                max-width: 230px;
                white-space: pre-wrap;
            }

        .rotulos td {
            font: bold 12px tahoma;
            padding-top: 20px;
            color: rgb(65, 105, 126);
            padding-left: 5px;
            padding-bottom: 5px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <table align="center" cellpadding="0" cellspacing="0" class="tablePrincipal" style="width: 100%">
            <tr>
                <td>
                    <table cellpadding="0" cellspacing="0" style="width: 920px" align="center">
                        <tr>
                            <td>
                                <table cellpadding="0" cellspacing="0" style="width: 920px">
                                    <tr>
                                        <td style="width: 210px; height: 97px"></td>
                                        <td style="width: 140px; height: 97px"></td>
                                        <td style="width: 189px; height: 97px"></td>
                                        <td style="width: 208px; height: 97px"></td>
                                        <td style="height: 97px"></td>
                                    </tr>
                                    <tr valign="bottom">
                                        <td style="width: 210px">&nbsp;</td>
                                        <td class="textoCabecalho" style="width: 140px"></td>
                                        <td class="textoCabecalho" style="width: 189px"></td>
                                        <td class="textoCabecalho" style="width: 208px"></td>
                                        <td></td>
                                        <td class="textoCabecalho">
                                            Seu IP:
                                            <asp:Label ID="lblIp" runat="server" Font-Bold="True" Text="200.258.110.123" EnableTheming="False"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height="12"></td>
                        </tr>

                        <tr>
                            <td height="30"></td>
                        </tr>
                        <tr>
                            <td bgcolor="White" style="height: 300px" valign="top">
                                <table cellpadding="0" cellspacing="0" style="width: 920px">
                                    <tr>
                                        <td class="tituloPaginas" valign="top">
                                            <asp:Label ID="lblAcao" runat="server">Lista de Landing Pages</asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
                                                <tr>
                                                    <td style="text-align: right;">
                                                        <asp:Button runat="server" id="btnCadastrar" OnClick="btnCadastrar_OnClick" Text="Cadastrar Nova Página" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table class="tabrotulos">
                                                            <tr class="rotulos">
                                                                <td style="width: 150px;">
                                                                    Id do Produto:<br />
                                                                    <asp:TextBox runat="server" ID="txtIdProduto"></asp:TextBox>
                                                                </td>
                                                                <td style="width: 150px;">
                                                                    Endereço do Hotsite:<br />
                                                                    <asp:TextBox runat="server" ID="txtUrl"></asp:TextBox>
                                                                </td>
                                                                <td>&nbsp;<br />
                                                                    <asp:Button runat="server" ID="btnFiltrar" Text="Filtrar" OnClick="btnFiltrar_Click" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div align="center" style="min-height: 500px;">
                                                                <asp:GridView ID="GridView1" OnRowDataBound="GridView1_RowDataBound" CssClass="meugrid" runat="server" DataKeyNames="idLandingPage" AutoGenerateColumns="False" AllowPaging="True" PageSize="20" OnPageIndexChanging="GridView1_OnPageIndexChanging">
                                                                    <Columns>
                                                                        <asp:BoundField DataField="idLandingPage" HeaderText="Id" />
                                                                        <asp:BoundField DataField="idProduto" HeaderText="Produto ID" />
                                                                        <asp:BoundField DataField="titulo" HeaderText="Título" />
                                                                        <asp:BoundField DataField="enderecoHotsite" HeaderText="Endereço" />
                                                                        <asp:TemplateField HeaderText="Editar" ShowHeader="False">
                                                                            <ItemTemplate>
                                                                                <a id="linkeditar" runat="server">
                                                                                    <img src="images/btEditar.jpg" alt="Editar" style="border-style: none;" />
                                                                                </a>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                </asp:GridView>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        

                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>

                            </td>
                        </tr>
                        <tr>
                            <td style="height: 176px; background-image: url('images/rodape.jpg');">
                                <table cellpadding="0" cellspacing="0" style="width: 920px">
                                    <tr>
                                        <td style="width: 705px; height: 63px"></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 705px">&nbsp;</td>
                                        <td>
                                            <asp:HyperLink ID="btEmail" runat="server" ImageUrl="images/email.gif" NavigateUrl="mailto:suporte@bark.com.br"></asp:HyperLink>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
