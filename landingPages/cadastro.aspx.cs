﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Serialization;
using Amazon;
using Amazon.Runtime;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Util;
using HtmlAgilityPack;
using landingPages.context;

namespace landingPages
{
    public partial class cadastro : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
            usuarioLogadoId = Request.Cookies["usuarioLogadoId"];
            bool valido = false;
            if (usuarioLogadoId != null)
            {
                int idUsuario = Convert.ToInt32(usuarioLogadoId.Value);
                var data = new dbCommerceEntities();
                var paginaPermitida = (from c in data.tbUsuarioPaginasPermitida
                                       where
                                           c.usuarioId == idUsuario &&
                                           c.paginaPermitidaNome == "landingpage"
                                       select c).FirstOrDefault();
                if (paginaPermitida != null)
                {
                    valido = true;
                    lblIp.Text = Request.UserHostAddress.ToString();
                }
            }
            if (!valido)
            {
                Response.Redirect("Default.aspx");
            }

            if (!Page.IsPostBack)
            {
                if (Request.QueryString["id"] != null)
                {
                    hdfIdLanding.Value = Request.QueryString["id"];
                    fillLanding();
                }

                if (Request.QueryString["all"] != null)
                {
                    using (var data = new dbCommerceEntities())
                    {
                        int inicio = Convert.ToInt32(Request.QueryString["all"]);
                        var landings = (from c in data.tbLandingPage where c.idLandingPage > inicio orderby c.idLandingPage select c).ToList();
                        foreach (var landing in landings)
                        {
                            try
                            {
                                hdfIdLanding.Value = landing.idLandingPage.ToString();
                                fillLanding();
                                Gravar();
                            }
                            catch(Exception ex)
                            {

                            }
                        }
                    }
                }
            }
        }

        private void fillLanding()
        {
            hdfProdutosAdicionais.Value = "";
            int id = Convert.ToInt32(hdfIdLanding.Value);
            using (var data = new dbCommerceEntities())
            {
                var landing = (from c in data.tbLandingPage where c.idLandingPage == id select c).FirstOrDefault();
                if (landing != null)
                {
                    txtIdProduto.Text = landing.idProduto.ToString();
                    txtEnderecoHotSite.Text = landing.enderecoHotsite;
                    txtTitulo.Text = landing.titulo;
                    txtResumo.Text = landing.resumo;
                    txtTexto.Text = landing.texto;

                    hplAbrirSite.NavigateUrl = "http://www." + landing.enderecoHotsite + ".s3-website-sa-east-1.amazonaws.com";
                    hplAbrirSite.Visible = true;
                    var produtosAdicionais = (from c in data.tbLandingPageProduto where c.idLandingPage == id select c).ToList();
                    foreach (var produtoAdicional in produtosAdicionais)
                    {
                        adicionarProdutoLista(produtoAdicional.idProduto);
                    }
                }
            }
        }
        protected void btnGravar_OnClick(object sender, EventArgs e)
        {
            Gravar();
        }

        private void Gravar()
        {
            GeraLinkLandings();
            using (var data = new dbCommerceEntities())
            {
                int id = 0;
                if (hdfIdLanding.Value != "") int.TryParse(hdfIdLanding.Value, out id);
                var landingPage = id == 0
                    ? new tbLandingPage()
                    : (from c in data.tbLandingPage where c.idLandingPage == id select c).First();
                landingPage.idProduto = Convert.ToInt32(txtIdProduto.Text);
                landingPage.enderecoHotsite = txtEnderecoHotSite.Text.Replace("http://", "").Replace("www", "");
                landingPage.titulo = txtTitulo.Text;
                landingPage.resumo = txtResumo.Text;
                landingPage.texto = txtTexto.Text;
                if (id == 0)
                {
                    landingPage.idLandingPageTemplate = (from c in data.tbLandingPageTemplate
                                                         join d in data.tbLandingPage on c.idLandingPageTemplate equals d.idLandingPageTemplate into templates
                                                         where c.ativo == true
                                                         select new
                                                         {
                                                             c.idLandingPageTemplate,
                                                             quantidade = templates.Count()
                                                         }).OrderBy(x => x.quantidade).First().idLandingPageTemplate;

                    int idLandindPageHospedagem = (from c in data.tbLandingPageHospedagem select new { c.idLandingPageHospedagem, contagem = (from d in data.tbLandingPage where c.idLandingPageHospedagem == d.idLandingPageHospedagem select c).Count() }).ToList().OrderBy(x => x.contagem).First().idLandingPageHospedagem;
                    landingPage.idLandingPageHospedagem = idLandindPageHospedagem;
                    data.tbLandingPage.Add(landingPage);
                }
                data.SaveChanges();
                hdfIdLanding.Value = landingPage.idLandingPage.ToString();

                hplAbrirSite.NavigateUrl = "http://www." + landingPage.enderecoHotsite + ".s3-website-sa-east-1.amazonaws.com";
                hplAbrirSite.Visible = true;

                var listaProdutosAdicionais = new List<ProdutosAdicionais>();
                if (!string.IsNullOrEmpty(hdfProdutosAdicionais.Value))
                {
                    listaProdutosAdicionais = DesSerializarProdutosAdicionais(hdfProdutosAdicionais.Value);
                }
                var produtosAdiconais = (from c in data.tbLandingPageProduto where c.idLandingPage == landingPage.idLandingPage select c);
                foreach (var produtoAdicional in produtosAdiconais)
                {
                    if (listaProdutosAdicionais.All(x => x.ProdutoId != produtoAdicional.idProduto))
                    {
                        data.tbLandingPageProduto.Remove(produtoAdicional);
                    }
                }

                foreach (var produtoAdicional in listaProdutosAdicionais)
                {
                    if (produtosAdiconais.All(x => x.idProduto != produtoAdicional.ProdutoId))
                    {
                        data.tbLandingPageProduto.Add(new tbLandingPageProduto()
                        {
                            idLandingPage = landingPage.idLandingPage,
                            idProduto = produtoAdicional.ProdutoId
                        });
                    }
                }
                data.SaveChanges();
                SaveOnS3();
                if(Request.QueryString["all"] == null) Response.Redirect("~/cadastro.aspx?id=" + landingPage.idLandingPage);
            }
        }

        private void GeraLinkLandings()
        {
            var listaProdutosAdicionais = new List<ProdutosAdicionais>();
            if (!string.IsNullOrEmpty(hdfProdutosAdicionais.Value))
            {
                listaProdutosAdicionais = DesSerializarProdutosAdicionais(hdfProdutosAdicionais.Value);
            }
            int idProduto = Convert.ToInt32(txtIdProduto.Text);
            int produtoAdicionaisAtual = listaProdutosAdicionais.Count;
            if (produtoAdicionaisAtual < 4)
            {
                for (int i = 1; i <= (4 - produtoAdicionaisAtual); i++)
                {
                    using (var data = new dbCommerceEntities())
                    {
                        List<int> listaProdutos = listaProdutosAdicionais.Select(x => x.ProdutoId).ToList();
                        var landingProduto = (from c in data.tbLandingPage
                            join d in data.tbLandingPageProduto on c.idProduto equals d.idProduto into
                                produtos
                                              where c.idProduto != idProduto && !listaProdutos.Contains(c.idProduto)
                                              select new
                            {
                                c.idProduto,
                                quantidade = produtos.Count()
                            }).OrderBy(x => x.quantidade).FirstOrDefault();
                        if (landingProduto != null)
                        {
                            adicionarProdutoLista(landingProduto.idProduto);
                            listaProdutosAdicionais = DesSerializarProdutosAdicionais(hdfProdutosAdicionais.Value);
                        }
                    }
                }
            }
        }

        protected void btnAdicionarProdutos_OnClick(object sender, EventArgs e)
        {
            bool adicionado = false;
            try
            {
                int idProduto = 0;
                int.TryParse(txtProdutosAdicionais.Text, out idProduto);
                adicionarProdutoLista(idProduto);
            }
            catch (Exception)
            {
                    
            }
            txtProdutosAdicionais.Text = "";
        }


        private void adicionarProdutoLista(int produtoId)
        {
            using(var data = new dbCommerceEntities())
            {
                var produto = (from c in data.tbProdutos
                    where c.produtoId == produtoId
                    select new ProdutosAdicionais
                    {
                        ProdutoCategoria = (from d in data.tbJuncaoProdutoCategoria
                            where d.produtoId == c.produtoId
                            where d.tbProdutoCategoria.exibirSite == true && d.tbProdutoCategoria.categoriaPaiId == 0
                            select d).FirstOrDefault().tbProdutoCategoria.categoriaUrl,
                        ProdutoCategoriaId = (from d in data.tbJuncaoProdutoCategoria
                            where d.produtoId == c.produtoId
                            where d.tbProdutoCategoria.exibirSite == true && d.tbProdutoCategoria.categoriaPaiId == 0
                            select d).FirstOrDefault().tbProdutoCategoria.categoriaId,
                        ProdutoFoto = c.fotoDestaque,
                        ProdutoId = c.produtoId,
                        ProdutoUrl = c.produtoUrl,
                        ProdutoNome = c.produtoNome
                    }).FirstOrDefault();
                if (produto != null)
                {
                    AdicionarItemListaProdutos(produto);
                }
            }
        }

        private void AdicionarItemListaProdutos(ProdutosAdicionais item)
        {
            var lista = new List<ProdutosAdicionais>();
            if (!string.IsNullOrEmpty(hdfProdutosAdicionais.Value))
            {
                lista = DesSerializarProdutosAdicionais(hdfProdutosAdicionais.Value);
            }
            if (lista.All(x => x.ProdutoId != item.ProdutoId))
            {
                lista.Add(item);
                hdfProdutosAdicionais.Value = SerializarProdutosAdicionais(lista);
                FillGridProdutosAdicionais();
            }
        }

        private void FillGridProdutosAdicionais()
        {
            var lista = new List<ProdutosAdicionais>();
            if (!string.IsNullOrEmpty(hdfProdutosAdicionais.Value))
            {
                lista = DesSerializarProdutosAdicionais(hdfProdutosAdicionais.Value);
                dtlFotos.DataSource = lista;
                dtlFotos.DataBind();
            }
        }

        public static string SerializarProdutosAdicionais(List<ProdutosAdicionais> tData)
        {
            var serializer = new XmlSerializer(typeof(List<ProdutosAdicionais>));
            TextWriter writer = new StringWriter();
            serializer.Serialize(writer, tData);
            return writer.ToString();
        }

        public static List<ProdutosAdicionais> DesSerializarProdutosAdicionais(string tData)
        {
            var serializer = new XmlSerializer(typeof(List<ProdutosAdicionais>));
            TextReader reader = new StringReader(tData);
            return (List<ProdutosAdicionais>)serializer.Deserialize(reader);
        }

        public class ProdutosAdicionais
        {
            public int ProdutoId { get; set; }
            public string ProdutoUrl { get; set; }
            public string ProdutoCategoria { get; set; }
            public int ProdutoCategoriaId { get; set; }
            public string ProdutoFoto { get; set; }
            public string ProdutoNome { get; set; }
        }

        protected void btnExcluirFoto_OnCommand(object sender, CommandEventArgs e)
        {
            int produtoId = Convert.ToInt32(e.CommandArgument);
            var lista = new List<ProdutosAdicionais>();
            if (!string.IsNullOrEmpty(hdfProdutosAdicionais.Value))
            {
                lista = DesSerializarProdutosAdicionais(hdfProdutosAdicionais.Value);
                lista.RemoveAll(x => x.ProdutoId == produtoId);
                hdfProdutosAdicionais.Value = SerializarProdutosAdicionais(lista);
                FillGridProdutosAdicionais();
            }
        }


        private void SaveOnS3()
        {

            int id = Convert.ToInt32(hdfIdLanding.Value);
            using (var data = new dbCommerceEntities())
            {
                var landingPage = (from c in data.tbLandingPage where c.idLandingPage == id select c).First();

                //var credentials = new BasicAWSCredentials("AKIAJ5OIRTUJ23QBZ2RA", "ZKDwTWyrGEb/szAU0QTVaKtk69PgyMTyyMwAwaWv");
                var credentials = new BasicAWSCredentials(landingPage.tbLandingPageHospedagem.usuario, landingPage.tbLandingPageHospedagem.senha);
                var s3Client = new Amazon.S3.AmazonS3Client(credentials, RegionEndpoint.SAEast1);
                string bucketUrl = "www." + landingPage.enderecoHotsite;

                if (!(AmazonS3Util.DoesS3BucketExist(s3Client, bucketUrl)))
                {
                    CreateABucket(s3Client, bucketUrl);
                }
                string strPath = Server.MapPath("~\\templates\\" + landingPage.idLandingPageTemplate);
                var templateFolders = Directory.GetDirectories(strPath);
                foreach (var folder in templateFolders)
                {
                    foreach (string f in Directory.GetFiles(folder))
                    {
                        try
                        {
                            string FolderName = new DirectoryInfo(System.IO.Path.GetDirectoryName(f)).Name;
                            using (MemoryStream filebuffer = new MemoryStream(File.ReadAllBytes(f)))
                            {
                            PutObjectRequest putRequest2 = new PutObjectRequest
                            {
                                BucketName = bucketUrl,
                                Key = FolderName + "/" + Path.GetFileName(f),
                                CannedACL = S3CannedACL.PublicRead,
                                InputStream = filebuffer
                            };
                            PutObjectResponse response2 = s3Client.PutObject(putRequest2);
                            }
                        }
                        catch (Exception)
                        {
                            
                        }
                    }
                }

                var template = landingPage.tbLandingPageTemplate;

                string fotosFolder = "C:\\inetpub\\wwwroot\\fotos\\";
                string htmlProdutosAdicionais = "";
                var listaProdutosAdicionais = new List<ProdutosAdicionais>();
                if (!string.IsNullOrEmpty(hdfProdutosAdicionais.Value))
                {
                    listaProdutosAdicionais = DesSerializarProdutosAdicionais(hdfProdutosAdicionais.Value);
                }


                int produtoAdicionalAtual = 0;
                foreach (var produtoAdicional in listaProdutosAdicionais)
                {
                    produtoAdicionalAtual++;
                    var paginaProduto =
                        (from c in data.tbLandingPage where c.idProduto == produtoAdicional.ProdutoId select c)
                            .FirstOrDefault();
                    if (paginaProduto != null)
                    {
                        try
                        {
                            using (
                                MemoryStream filebuffer =
                                    new MemoryStream(
                                        File.ReadAllBytes(fotosFolder + produtoAdicional.ProdutoId + "\\media_" +
                                                          produtoAdicional.ProdutoFoto + ".jpg")))
                            {
                                PutObjectRequest putRequest2 = new PutObjectRequest
                                {
                                    BucketName = bucketUrl,
                                    Key = "fotos/" + produtoAdicional.ProdutoUrl + ".jpg",
                                    CannedACL = S3CannedACL.PublicRead,
                                    InputStream = filebuffer
                                };
                                PutObjectResponse response2 = s3Client.PutObject(putRequest2);
                            }
                        }
                        catch (Exception)
                        {

                        }
                        string templateProdutoAdicional = template.templateProdutoAdicional;
                        templateProdutoAdicional = templateProdutoAdicional.Replace("{produtonome}",
                            produtoAdicional.ProdutoNome);
                        templateProdutoAdicional = templateProdutoAdicional.Replace("{produtofoto}",
                            "fotos/" + produtoAdicional.ProdutoUrl + ".jpg");
                        if(produtoAdicionalAtual == 1)
                        {
                            if(produtoAdicional.ProdutoNome.ToLower().Contains("kit berço"))
                            {
                                templateProdutoAdicional = templateProdutoAdicional.Replace("{produtolink}", "http://www.kitberco.com.br");
                            }
                            else
                            {
                                templateProdutoAdicional = templateProdutoAdicional.Replace("{produtolink}", "http://www.quartoparabebe.com.br");
                            }
                        }
                        else
                        {
                            templateProdutoAdicional = templateProdutoAdicional.Replace("{produtolink}", "http://www." + paginaProduto.enderecoHotsite);
                        }
                        htmlProdutosAdicionais += templateProdutoAdicional;
                    }
                }


                var produtoDetalhe = (from c in data.tbProdutos
                    where c.produtoId == landingPage.idProduto
                    select new
                    {
                        c.produtoId,
                        c.produtoUrl,
                        c.fotoDestaque
                    }).First();
                var produtoCategoria = (from c in data.tbJuncaoProdutoCategoria
                    where c.produtoId == landingPage.idProduto && c.tbProdutoCategoria.exibirSite == true && c.tbProdutoCategoria.categoriaPaiId == 0
                    select new
                    {
                        c.tbProdutoCategoria.categoriaUrl
                    }).First();
                string templateHotSite = template.template;
                templateHotSite = templateHotSite.Replace("{produtosadicionais}", htmlProdutosAdicionais);
                templateHotSite = templateHotSite.Replace("{titulo}", landingPage.titulo);
                templateHotSite = templateHotSite.Replace("{resumo}", landingPage.resumo);
                templateHotSite = templateHotSite.Replace("{texto}", landingPage.texto);
                templateHotSite = templateHotSite.Replace("{url}", "http://www.graodegente.com.br/" + produtoCategoria.categoriaUrl + "/" + produtoDetalhe.produtoUrl + "/");


                try
                {
                    string urlFoto = fotosFolder + produtoDetalhe.produtoId + "\\media_" + produtoDetalhe.fotoDestaque + ".jpg";
                    using (MemoryStream filebuffer = new MemoryStream(File.ReadAllBytes(urlFoto)))
                    {
                        PutObjectRequest putRequest2 = new PutObjectRequest
                        {
                            BucketName = bucketUrl,
                            Key = "fotos/" + produtoDetalhe.produtoUrl + "_media.jpg",
                            CannedACL = S3CannedACL.PublicRead,
                            InputStream = filebuffer
                        };
                        PutObjectResponse response2 = s3Client.PutObject(putRequest2);
                    }
                    urlFoto = fotosFolder + produtoDetalhe.produtoId + "\\" + produtoDetalhe.fotoDestaque + ".jpg";
                    using (MemoryStream filebuffer = new MemoryStream(File.ReadAllBytes(urlFoto)))
                    {
                        PutObjectRequest putRequest2 = new PutObjectRequest
                        {
                            BucketName = bucketUrl,
                            Key = "fotos/" + produtoDetalhe.produtoUrl + ".jpg",
                            CannedACL = S3CannedACL.PublicRead,
                            InputStream = filebuffer
                        };
                        PutObjectResponse response2 = s3Client.PutObject(putRequest2);
                    }
                }
                catch (Exception)
                {

                }
                templateHotSite = templateHotSite.Replace("{foto1}", "fotos/" + produtoDetalhe.produtoUrl + ".jpg");

                int fotoIndice = 2;
                var fotos = (from c in data.tbProdutoFoto where c.produtoId == landingPage.idProduto && c.produtoFoto != produtoDetalhe.fotoDestaque select c).ToList();
                foreach(var foto in fotos)
                {
                    try
                    {
                        string urlFoto = fotosFolder + produtoDetalhe.produtoId + "\\media_" + foto.produtoFoto + ".jpg";
                        using (MemoryStream filebuffer = new MemoryStream(File.ReadAllBytes(fotosFolder + produtoDetalhe.produtoId + "\\" + foto.produtoFoto + ".jpg")))
                        {
                            PutObjectRequest putRequest2 = new PutObjectRequest
                            {
                                BucketName = bucketUrl,
                                Key = "fotos/" + produtoDetalhe.produtoUrl + fotoIndice + "_media.jpg",
                                CannedACL = S3CannedACL.PublicRead,
                                InputStream = filebuffer
                            };
                            PutObjectResponse response2 = s3Client.PutObject(putRequest2);
                            templateHotSite = templateHotSite.Replace("{fotomedia" + fotoIndice + "}", "fotos/" + produtoDetalhe.produtoUrl + fotoIndice + "_media.jpg");
                        }
                        urlFoto = fotosFolder + produtoDetalhe.produtoId + "\\" + foto.produtoFoto + ".jpg";
                        using (MemoryStream filebuffer = new MemoryStream(File.ReadAllBytes(urlFoto)))
                        {
                            PutObjectRequest putRequest2 = new PutObjectRequest
                            {
                                BucketName = bucketUrl,
                                Key = "fotos/" + produtoDetalhe.produtoUrl + fotoIndice + ".jpg",
                                CannedACL = S3CannedACL.PublicRead,
                                InputStream = filebuffer
                            };
                            PutObjectResponse response2 = s3Client.PutObject(putRequest2);
                            templateHotSite = templateHotSite.Replace("{foto" + fotoIndice + "}", "fotos/" + produtoDetalhe.produtoUrl + fotoIndice + ".jpg");
                            templateHotSite = templateHotSite.Replace("{fotostyle" + fotoIndice + "}", "");
                        }
                    }
                    catch (Exception)
                    {

                    }
                    fotoIndice++;
                }

                for (int i = fotoIndice; i < fotoIndice + 10; i++)
                {
                    templateHotSite = templateHotSite.Replace("{fotostyle" + i + "}", "display: none;");
                }
                

                int fotoIndiceNovo = 2;
                //foreach (var foto in fotos)
                //{
                //    try
                //    {
                //        templateHotSite = templateHotSite.Replace("{fotomedia" + fotoIndice + "}", "fotos/" + produtoDetalhe.produtoUrl + fotoIndiceNovo + "_media.jpg");
                //        templateHotSite = templateHotSite.Replace("{foto" + fotoIndice + "}", "fotos/" + produtoDetalhe.produtoUrl + fotoIndiceNovo + ".jpg");
                //    }
                //    catch (Exception)
                //    {

                //    }
                //    fotoIndice++;
                //    fotoIndiceNovo++;
                //}
                templateHotSite = templateHotSite.Replace("{canonica}", "http://www." + landingPage.enderecoHotsite);





                #region Depoimentos
                var depoimentos = (from c in data.tbDepoimento orderby Guid.NewGuid() select c).ToList().Take(10);
                int numeroDepoimento = 1;
                foreach (var depoimento in depoimentos)
                {
                    try
                    {
                            string nomeDepoimento = depoimento.nome;
                            string dataPostagem = depoimento.data;
                            string linkPostagem = depoimento.url;
                            string textoDepoimento = depoimento.texto;
                            string imgDepoimento = depoimento.foto;

                            templateHotSite = templateHotSite.Replace("{depoimentonome" + numeroDepoimento +"}", nomeDepoimento);
                            templateHotSite = templateHotSite.Replace("{depoimentoimg" + numeroDepoimento +"}", imgDepoimento);
                            templateHotSite = templateHotSite.Replace("{depoimentodata" + numeroDepoimento +"}", dataPostagem);
                            templateHotSite = templateHotSite.Replace("{depoimentotexto" + numeroDepoimento +"}", textoDepoimento);
                            templateHotSite = templateHotSite.Replace("{depoimentolink" + numeroDepoimento +"}", linkPostagem);


                            numeroDepoimento++;
                    }
                    catch (Exception ex)
                    {

                    }
                }
                #endregion

                PutObjectRequest putRequestHtml = new PutObjectRequest
                {
                    BucketName = bucketUrl,
                    Key = "index.html",
                    ContentType = "text/html",
                    ContentBody = templateHotSite,
                    CannedACL = S3CannedACL.PublicRead
                };
                PutObjectResponse responseHtml = s3Client.PutObject(putRequestHtml);
            }


        }


        private void checadepoimentos(int idDepoimento)
        {
            var data = new dbCommerceEntities();
            #region Depoimentos
            var depoimentos = (from c in data.tbDepoimento where c.idDepoimento == idDepoimento orderby Guid.NewGuid() select c).ToList().Take(10);
            int numeroDepoimento = 1;
            foreach (var depoimento in depoimentos)
            {
                try
                {
                    var wImportacao = new HtmlWeb();
                    var wDocumento = wImportacao.Load(depoimento.url);
                    foreach (HtmlNode conteudo in wDocumento.DocumentNode.SelectNodes("//code[1]"))
                    {
                        HtmlAgilityPack.HtmlDocument faceDocument = new HtmlAgilityPack.HtmlDocument();
                        faceDocument.LoadHtml(conteudo.InnerHtml.Replace("<!--", "").Replace("-->", ""));
                        string dataPostagem = "";
                        string linkPostagem = "";
                        foreach (HtmlNode div in faceDocument.DocumentNode.SelectNodes("//span[contains(@class,'timestampContent')]"))
                        {
                            dataPostagem = div.InnerText;
                            linkPostagem = div.ParentNode.ParentNode.Attributes["href"].Value.Replace("/graodegente/posts/", "http://www.facebook.com/graodegente/posts/");
                        }

                        string textoDepoimento = "";
                        foreach (HtmlNode div in faceDocument.DocumentNode.SelectNodes("//div[contains(@class,'userContent')]"))
                        {
                            textoDepoimento = div.InnerText;
                        }

                        string imgDepoimento = "";
                        foreach (HtmlNode div in faceDocument.DocumentNode.SelectNodes("//img[contains(@class,'img')]"))
                        {
                            imgDepoimento = div.Attributes["src"].Value;
                        }

                        string nomeDepoimento = "";
                        foreach (HtmlNode div in faceDocument.DocumentNode.SelectNodes("//span[contains(@class,'fwb')]//a"))
                        {
                            string nome = div.InnerText;
                            if (nome == "Grão de Gente") nome = "";
                            if (!string.IsNullOrEmpty(nome) && string.IsNullOrEmpty(nomeDepoimento)) nomeDepoimento = nome;
                        }
                        if (string.IsNullOrEmpty(nomeDepoimento))
                        {
                            try
                            {
                                foreach (HtmlNode div in faceDocument.DocumentNode.SelectNodes("//span[contains(@class,'fwb')]//div//span"))
                                {
                                    string nome = div.InnerText;
                                    if (nome == "Grão de Gente") nome = "";
                                    if (!string.IsNullOrEmpty(nome) && string.IsNullOrEmpty(nomeDepoimento)) nomeDepoimento = nome;
                                }
                            }
                            catch (Exception)
                            {
                                
                            }
                        }
                        numeroDepoimento++;
                    }
                }
                catch (Exception ex)
                {

                }
            }
            #endregion

        }
        static void CreateABucket(IAmazonS3 client, string bucketName)
        {
            try
            {
                PutBucketRequest putRequest = new PutBucketRequest
                {
                    BucketName = bucketName,
                    UseClientRegion = true
                };
                PutBucketResponse response = client.PutBucket(putRequest);


                PutBucketWebsiteRequest putWebsiteRequest = new PutBucketWebsiteRequest()
                {
                    BucketName = bucketName,
                    WebsiteConfiguration = new WebsiteConfiguration()
                    {
                        IndexDocumentSuffix = "index.html",
                        ErrorDocument = "erro.html"
                    }
                };
                client.PutBucketWebsite(putWebsiteRequest);
            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                if (amazonS3Exception.ErrorCode != null &&
                    (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId")
                    ||
                    amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                {
                    Console.WriteLine("Check the provided AWS Credentials.");
                    Console.WriteLine("For service sign up go to http://aws.amazon.com/s3");
                }
                else
                {
                    Console.WriteLine("Error occurred. Message:'{0}' when writing an object", amazonS3Exception.Message);
                }
            }
        }
    }
}