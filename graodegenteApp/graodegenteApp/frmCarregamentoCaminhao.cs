﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using Transitions;

namespace graodegenteApp
{
    public partial class frmCarregamentoCaminhao : Form
    {
        public string transportadora = "";
        public int usuarioId = 0;
        public int idCentroDistribuicao = 1;

        public frmCarregamentoCaminhao()
        {
            InitializeComponent();
        }

        private void frmCarregamentoCaminhao_Load(object sender, EventArgs e)
        {
            // AtualizaEmissoes();
            if (idCentroDistribuicao > 2) lblAguardandoCarregamentoCd1.Text = "Aguardando Carregamento CD" + idCentroDistribuicao;
        }

        private void pnDados_Paint(object sender, PaintEventArgs e)
        {

        }
        

        public void AtualizaEmissoes()
        {
            timerUpdate.Enabled = false;
            ListEmissoes();
        }
        
        private void ListEmissoes()
        {
            if(!AtualizandoEmissoes) ThreadPool.QueueUserWorkItem(ListEmissoes);
        }

        private bool AtualizandoEmissoes = false;
        private void ListEmissoes(object parametros)
        {
            try
            {
                AtualizandoEmissoes = true;
                var serviceEmissao = new wsEmissao.EmissaoWs();
                var emissoesPendentes = serviceEmissao.ListaAguardandoCarregamentoCaminhaoCds("glmp152029", transportadora, idCentroDistribuicao);

                var aguardandoCd1 = emissoesPendentes.ListaCarregamento.Where(x => x.totalPacotesCd1 > 0).ToList();
                var aguardandoCd2 = emissoesPendentes.ListaCarregamento.Where(x => x.totalPacotesCd2 > 0).ToList();
                var carregamentoPendente = emissoesPendentes.ListaCarregamento.Where(x => x.totalPacotesCd1 > 0 | x.totalPacotesCd2 > 0).Where(x => x.totalCarregados > 0).ToList();

                grdAguardandoCarregamentoCd1.Invoke((MethodInvoker)delegate
                {
                    grdAguardandoCarregamentoCd1.DataSource = aguardandoCd1;
                    grdAguardandoCarregamentoCd1.BestFitColumns();
                });
                lblAguardandoCarregamentoCd1.Invoke((MethodInvoker)delegate
                {
                    lblAguardandoCarregamentoCd1.Text = "Aguardando Carregamento CD1: " + aguardandoCd1.Count;
                });
                grdAguardandoCarregamentoCd2.Invoke((MethodInvoker)delegate
                {
                    grdAguardandoCarregamentoCd2.DataSource = aguardandoCd2;
                    grdAguardandoCarregamentoCd2.BestFitColumns();
                });
                lblAguardandoCarregamentoCd2.Invoke((MethodInvoker)delegate
                {
                    lblAguardandoCarregamentoCd2.Text = "Aguardando Carregamento CD2: " + aguardandoCd2.Count;
                });
                grdCarregamentoPendente.Invoke((MethodInvoker)delegate
                {
                    grdCarregamentoPendente.DataSource = carregamentoPendente;
                    grdCarregamentoPendente.BestFitColumns();
                });
                lblParcialmenteCarregado.Invoke((MethodInvoker)delegate
                {
                    lblParcialmenteCarregado.Text = "Parcialmente Carregado: " + carregamentoPendente.Count;
                });
                timerUpdate.Enabled = true;
            }
            catch (Exception ex)
            {

            }
            finally
            {
                AtualizandoEmissoes = false;
            }
        }
        

        private void grd_Click(object sender, EventArgs e)
        {

        }

        private void grd_MouseDoubleClick(object sender, MouseEventArgs e)
        {
           
        }
        public static string removeCaracteresEspeciaisDeixaEspaco(string inputString)
        {
            StringBuilder sb = new StringBuilder();
            foreach (char c in inputString)
            {
                if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') | c == '.' || c == '_' || c == '-' || c == ' ')
                {
                    sb.Append(c);
                }
            }
            return sb.ToString();
        }
        public static string limpaStringDeixaEspaco(string texto)
        {
            string caracteresEspeciais = "ÄÅÁÂÀÃäáâàãÉÊËÈéêëèÍÎÏÌíîïìÖÓÔÒÕöóôòõÜÚÛüúûùÇç/`~!@#$%^&*()*+;:',<.>?|";
            string caracteresNormais = "AAAAAAaaaaaEEEEeeeeIIIIiiiiOOOOOoooooUUUuuuuCc------------------------";

            for (int i = 0; i < caracteresEspeciais.Length; i++)
            {
                try
                {
                    texto = texto.Replace(caracteresEspeciais[i].ToString(), caracteresNormais[i].ToString());
                    texto = texto.Replace("\"", "");
                    texto = texto.Replace("'", "");
                }
                catch (Exception ex) { }
            }
            return texto;
        }

        private void txtNumeroPedido_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && txtNumeroPedido.Text != "")
            {
                try
                { 
                    string numero = txtNumeroPedido.Text.Replace(";", "-").Replace(";", "-");
                    var pedidoIdCliente = numero.Split('-')[0];
                    int idPedido = Utilidades.retornaIdInterno(Convert.ToInt32(pedidoIdCliente));

                    int volume = Convert.ToInt32(numero.Split('-')[1]);

                    var emissaoWs = new wsEmissao.EmissaoWs();
                    var carregarVolume = emissaoWs.CarregarVolumeCaminhao("glmp152029", usuarioId, idPedido, volume, transportadora);
                    lblMensagem.Text = carregarVolume.mensagem;
                    if(carregarVolume.sucesso == false)
                    {
                        lblMensagem.BackColor = Color.Red;
                        SystemSounds.Exclamation.Play();
                    }
                    else
                    {
                        lblMensagem.BackColor = Color.Transparent;
                    }
                    txtNumeroPedido.Text = "";
                    txtNumeroPedido.Focus();
                    AtualizaEmissoes();
                }
                catch (Exception)
                {
                    txtNumeroPedido.Text = "";
                    txtNumeroPedido.Focus();
                }
                Transition.run(lblMensagem, "BackColor", Color.Pink, new TransitionType_Flash(2, 300));
            }
        }

        private void lblMensagem_Click(object sender, EventArgs e)
        {

        }

        private void txtNumeroPedido_TextChanged(object sender, EventArgs e)
        {

        }

        private void timerUpdate_Tick(object sender, EventArgs e)
        {
            AtualizaEmissoes();
        }
    }
}
