﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using graodegenteApp.wsEmbalagem;

namespace graodegenteApp
{
    public partial class usrEmbalarQuantidadePacotes : UserControl
    {
        public int quantidadePacotes = 0;
        public int quantidadePacotesConfirmar = 0;
        private bool enviando = false;

        public usrEmbalarQuantidadePacotes()
        {
            InitializeComponent();
        }


        private void txtQuantidadePacotes_KeyDown_1(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtEntrar.PerformClick();
            }
        }

        private void usrEmbalarQuantidadePacotes_Load(object sender, EventArgs e)
        {
            txtQuantidadePacotes.Focus();
        }

        private void txtEntrar_Click(object sender, EventArgs e)
        {
            if (quantidadePacotes == 0)
            {
                int.TryParse(txtQuantidadePacotes.Value.ToString(), out quantidadePacotes);
                if (quantidadePacotes == 0)
                {
                    MessageBox.Show("Quantidade de pacotes inválido. Favor preencher corretamente");
                    txtQuantidadePacotes.Text = "0";
                    txtQuantidadePacotes.Value = 0;
                    txtQuantidadePacotes.Focus();
                    txtQuantidadePacotes.Select();
                    return;
                }
                else if (quantidadePacotes > 30)
                {
                    quantidadePacotes = 0;
                    MessageBox.Show("Quantidade de pacotes inválido. Favor preencher corretamente");
                    txtQuantidadePacotes.Text = "0";
                    txtQuantidadePacotes.Value = 0;
                    txtQuantidadePacotes.Focus();
                    txtQuantidadePacotes.Select();
                    return;
                }
                lblTitulo.Text = "Confirme a Quantidade de Pacotes";
                txtQuantidadePacotes.Text = "0";
                txtQuantidadePacotes.Value = 0;
                txtQuantidadePacotes.Focus();
                txtQuantidadePacotes.Select();
                return;
            }
            else
            {
                int quantidadeConfirmar = Convert.ToInt32(txtQuantidadePacotes.Value);
                if (quantidadeConfirmar == quantidadePacotes)
                {
                    var embalagem = new EmbalagemWs();
                    var formPrincipal = (this.Parent.Parent as frmPrincipal);
                    if(!enviando)
                    {
                        enviando = true;
                        //var retornoPedido = embalagem.GravaPacotesSemPesoFinalizaPedido(formPrincipal.idUsuarioExpedicao, formPrincipal.idPedidoEmbalagem, quantidadePacotes);
                        var retornoPedido = embalagem.GravaPacotesSemPesoAguardaConferenciaPacote(formPrincipal.idUsuarioExpedicao, formPrincipal.idPedidoEmbalagem, quantidadePacotes, false);
                        formPrincipal.carregaEnviarPor(retornoPedido);
                    }
                    //var retornoPedido = embalagem.GravaPacotesSemPesoFinalizaPedido(formPrincipal.idUsuarioExpedicao, formPrincipal.idPedidoEmbalagem, quantidadePacotes);
                    //formPrincipal.carregaEnviarPor(retornoPedido);
                }
                else
                {
                    lblTitulo.Text = "Quantidade Inválida - Digite a Quantidade de Pacotes";
                    quantidadePacotes = 0;
                    txtQuantidadePacotes.Text = "0";
                    txtQuantidadePacotes.Value = 0;
                    txtQuantidadePacotes.Focus();
                    txtQuantidadePacotes.Select();
                }
            }
        }

        private void btnImpressaoConcluida_Click(object sender, EventArgs e)
        {
            var formPrincipal = (this.Parent.Parent as frmPrincipal);
            formPrincipal.carregaEmbalarLogin();
        }

        private void btnImprimirNovamente_Click(object sender, EventArgs e)
        {
            int quantidadePacotes = 0;
            int.TryParse(txtQuantidadePacotes.Value.ToString(), out quantidadePacotes);
            var formPrincipal = (this.Parent.Parent as frmPrincipal);
            var pedidosWs = new wsEmbalagem.EmbalagemWs();
            var informacoesEtiqueta = pedidosWs.RetornaEtiqueta(formPrincipal.idUsuarioExpedicao, formPrincipal.idPedidoEmbalagem);
            for (int i = 1; i <= quantidadePacotes; i++)
            {
                clsImprimirEtiqueta.GerarEtiquetaEndereco(Utilidades.retornaIdCliente(formPrincipal.idPedidoEmbalagem).ToString() + "-" + i + "/" + quantidadePacotes.ToString(), informacoesEtiqueta.telResidencial, informacoesEtiqueta.telComercial, informacoesEtiqueta.telCelular, informacoesEtiqueta.nome, informacoesEtiqueta.rua, informacoesEtiqueta.numero, informacoesEtiqueta.complemento, informacoesEtiqueta.bairro, informacoesEtiqueta.cidade, informacoesEtiqueta.estado, informacoesEtiqueta.cep, informacoesEtiqueta.referencia);
            }
        }
    }
}
