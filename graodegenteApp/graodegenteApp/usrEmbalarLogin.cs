﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace graodegenteApp
{
    public partial class usrEmbalarLogin : UserControl
    {
        public int idCentroDistribuicao = 1;
        public int funcao = 1;
        public usrEmbalarLogin()
        {
            InitializeComponent();
        }

        private void txtEntrar_Click(object sender, EventArgs e)
        {
            var embalarWs = new wsEmbalagem.EmbalagemWs();
            var usuario = embalarWs.LogarCds(txtSenha.Text, idCentroDistribuicao);
            if (usuario.idUsuarioExpedicao > 0)
            {
                var formPrincipal = (this.Parent.Parent as frmPrincipal);
                formPrincipal.idUsuarioExpedicao = usuario.idUsuarioExpedicao;

                if (funcao == 2)
                {
                    formPrincipal.carregaEmbalarAlterarPacotes();
                }
                else
                {
                    if (idCentroDistribuicao == 1 | idCentroDistribuicao == 4)
                    {
                        if (usuario.funcao == "selecionarpedido")
                        {
                            formPrincipal.carregaEmbalarQuantidade();
                        }
                        if (usuario.funcao == "conferirprodutos")
                        {
                            formPrincipal.idPedidoEmbalagem = usuario.idPedido;
                            formPrincipal.carregaEmbalarConferirProdutos();
                        }
                        if (usuario.funcao == "quantidadepacotes")
                        {
                            formPrincipal.idPedidoEmbalagem = usuario.idPedido;
                            formPrincipal.carregaEmbalarQuantidadePacotes();
                        }
                        if (usuario.funcao == "pesopacotes")
                        {
                            formPrincipal.idPedidoEmbalagem = usuario.idPedido;
                            formPrincipal.carregaEmbalarPesoPacotes();
                        }
                        if (usuario.funcao == "conferirpacotes")
                        {
                            formPrincipal.idPedidoEmbalagem = usuario.idPedido;
                            formPrincipal.carregaEmbalarConferirPacotes();
                        }
                        if (usuario.funcao == "rastreio")
                        {
                            formPrincipal.idPedidoEmbalagem = usuario.idPedido;
                            formPrincipal.carregaEmbalarRastreio();
                        }
                    }
                    else
                    {
                        formPrincipal.carregaEmbalarQuantidadeCd2(idCentroDistribuicao);
                    }
                }

            }
            else
            {
                MessageBox.Show("Senha inválida, por favor tente novamente");
                txtSenha.Text = "";
                txtSenha.Focus();
            }
        }

        private void usrEmbalarLogin_Load(object sender, EventArgs e)
        {
            txtSenha.Focus();
        }

        private void txtSenha_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtEntrar.PerformClick();
            }
        }
    }
}
