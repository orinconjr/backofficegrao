﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace graodegenteApp
{
    public partial class usrEmbalarConferirProdutos : UserControl
    {
        string etiquetaNaoPercenteAoPedido = string.Empty;
        public usrEmbalarConferirProdutos()
        {
            InitializeComponent();
        }

        private void usrEmbalarConferirProdutos_Load(object sender, EventArgs e)
        {
            var formPrincipal = (this.Parent.Parent as frmPrincipal);
            var embalagemws = new wsEmbalagem.EmbalagemWs();
            var listaAguardando = embalagemws.RetornaProdutosAguardandoConferencia(formPrincipal.idUsuarioExpedicao, formPrincipal.idPedidoEmbalagem);
            lstProdutosFaltando.Items.Clear();
            foreach (var aguardando in listaAguardando)
            {
                lstProdutosFaltando.Items.Add(MascaraEtiqueta(aguardando.etiqueta) + " - " + aguardando.produtoNome);
            }

            txtCodigoDeBarras.Focus();
        }

        private void lblNomeProduto_Click(object sender, EventArgs e)
        {

        }

        public string MascaraEtiqueta(int etiqueta)
        {
            string etiquetaStr = etiqueta.ToString();
            string caracteresMeio = "";
            for (int i = 0; i < (etiquetaStr.Length - 2); i++)
            {
                caracteresMeio += "x";
            }
            return etiquetaStr.Substring(0, 1) + caracteresMeio + etiquetaStr.Substring(etiquetaStr.Length - 1, 1);
        }
        private void txtCodigoDeBarras_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (!string.IsNullOrEmpty(etiquetaNaoPercenteAoPedido))
                {
                    Utilidades.alerta();
                    if (MessageBox.Show("A etiqueta " + etiquetaNaoPercenteAoPedido + " não pertence ao pedido, foi tomada providência?", "Alerta", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                    {
                        etiquetaNaoPercenteAoPedido = string.Empty;
                    }
                }
                else
                {
                    int etiqueta = 0;
                    int.TryParse(txtCodigoDeBarras.Text, out etiqueta);
                    var embalagemws = new wsEmbalagem.EmbalagemWs();
                    var formPrincipal = (this.Parent.Parent as frmPrincipal);
                    var validacao = embalagemws.RegistraProdutoAguardandoConferencia(formPrincipal.idUsuarioExpedicao, formPrincipal.idPedidoEmbalagem, etiqueta);

                    if (validacao == "naofazparte")
                    {
                        Utilidades.alerta();
                        etiquetaNaoPercenteAoPedido = etiqueta.ToString();
                        MessageBox.Show("Produto não faz parte do pedido");
                    }

                    var listaAguardando = embalagemws.RetornaProdutosAguardandoConferencia(formPrincipal.idUsuarioExpedicao, formPrincipal.idPedidoEmbalagem);
                    if (listaAguardando.Any())
                    {
                        lstProdutosFaltando.Items.Clear();
                        foreach (var aguardando in listaAguardando)
                        {
                            //lstProdutosFaltando.Items.Add(aguardando.etiqueta + " - " + aguardando.produtoNome);
                            lstProdutosFaltando.Items.Add(MascaraEtiqueta(aguardando.etiqueta) + " - " + aguardando.produtoNome);
                        }
                    }
                    else
                    {
                        formPrincipal.carregaConferenciaOk();
                    }

                    txtCodigoDeBarras.Text = "";
                    txtCodigoDeBarras.Focus();
                }
            }
        }
    }
}
