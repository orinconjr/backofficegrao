﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;

namespace graodegenteApp
{
    public partial class frmEmbalarLogin : Form
    {
        public int idUsuario = 0;
        public string paginaAutorizacao = "";

        public frmEmbalarLogin()
        {
            InitializeComponent();
        }

        private void frmEmbalarLogin_Load(object sender, EventArgs e)
        {
            ThemeResolutionService.ApplicationThemeName = "TelerikMetroBlue";
            txtSenha.Focus();
        }

        private void txtEntrar_Click(object sender, EventArgs e)
        {
            /*var autenticacao = new wsAutenticacao.Autenticacao();
            var validaLogin = autenticacao.ValidaLoginAdmin(txtLogin.Text, txtSenha.Text, paginaAutorizacao);
            if (validaLogin > 0)
            {
                idUsuario = validaLogin;
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            else
            {
                MessageBox.Show("Usuário ou senha inválidos");
                txtSenha.Text = "";
                txtSenha.Focus();
            }*/
        }


        private void txtSenha_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtEntrar.PerformClick();
            }
        }
    }
}
