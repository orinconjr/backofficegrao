﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;

namespace graodegenteApp
{
    public partial class frmLogin : Form
    {
        public int idUsuario = 0;
        public string paginaAutorizacao = "";
        public string tipoAutorizacao = "";
        public string login = "";

        public frmLogin()
        {
            InitializeComponent();
        }

        private void frmLogin_Load(object sender, EventArgs e)
        {
            ThemeResolutionService.ApplicationThemeName = "TelerikMetroBlue";
        }

        private void txtEntrar_Click(object sender, EventArgs e)
        {
            var autenticacao = new wsAutenticacao.Autenticacao();
            var validaLogin = autenticacao.ValidaLoginAdmin(txtLogin.Text, txtSenha.Text, paginaAutorizacao);

            if(paginaAutorizacao == "carregamentocaminhao")
            {
                if (autenticacao.ValidaLoginAdmin(txtLogin.Text, txtSenha.Text, "carregamentocaminhaojadlog") > 0) {
                    tipoAutorizacao = "jadlog";
                    validaLogin = autenticacao.ValidaLoginAdmin(txtLogin.Text, txtSenha.Text, "carregamentocaminhaojadlog");
                }
                else if (autenticacao.ValidaLoginAdmin(txtLogin.Text, txtSenha.Text, "carregamentocaminhaocorreios") > 0) {
                    tipoAutorizacao = "correios";
                    validaLogin = autenticacao.ValidaLoginAdmin(txtLogin.Text, txtSenha.Text, "carregamentocaminhaocorreios");
                }
                else if (autenticacao.ValidaLoginAdmin(txtLogin.Text, txtSenha.Text, "carregamentocaminhaotnt") > 0) {
                    tipoAutorizacao = "tnt";
                    validaLogin = autenticacao.ValidaLoginAdmin(txtLogin.Text, txtSenha.Text, "carregamentocaminhaotnt");
                }
                else if (autenticacao.ValidaLoginAdmin(txtLogin.Text, txtSenha.Text, "carregamentocaminhaobelle") > 0) {
                    tipoAutorizacao = "belle";
                    validaLogin = autenticacao.ValidaLoginAdmin(txtLogin.Text, txtSenha.Text, "carregamentocaminhaobelle");
                }
                else if (autenticacao.ValidaLoginAdmin(txtLogin.Text, txtSenha.Text, "carregamentocaminhaoplimor") > 0)
                {
                    tipoAutorizacao = "plimor";
                    validaLogin = autenticacao.ValidaLoginAdmin(txtLogin.Text, txtSenha.Text, "carregamentocaminhaoplimor");
                }
            }


            if (validaLogin > 0)
            {
                idUsuario = validaLogin;
                login = txtLogin.Text;
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            else
            {
                MessageBox.Show("Usuário ou senha inválidos");
                txtSenha.Text = "";
                txtSenha.Focus();
            }
        }

        private void txtLogin_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtSenha.Focus();
            }
        }

        private void txtSenha_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtEntrar.PerformClick();
            }
        }
    }
}
