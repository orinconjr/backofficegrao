﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace graodegenteApp
{
    public partial class frmEmissaoGravarRastreio : Form
    {
        public int idPedidoEnvio;
        public frmEmissaoGravarRastreio()
        {
            InitializeComponent();
        }

        private void frmEmissaoGravarRastreio_Load(object sender, EventArgs e)
        {
            txtRastreio.Focus();
        }

        private void txtRastreio_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && txtRastreio.Text != "")
            {
                bool rastreioGravado = false;
                if (idPedidoEnvio != null)
                {
                    var emissaoWs = new wsEmissao.EmissaoWs();
                    rastreioGravado = emissaoWs.GravarRastreio("glmp152029", idPedidoEnvio, txtRastreio.Text);
                }
                if (rastreioGravado)
                {
                    this.DialogResult = DialogResult.OK;
                    this.Close();
                }
                else
                {
                    this.DialogResult = DialogResult.Abort;
                }
            }
        }
    }
}
