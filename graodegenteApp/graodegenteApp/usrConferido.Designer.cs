﻿namespace graodegenteApp
{
    partial class usrConferido
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnConfirmar = new Telerik.WinControls.UI.RadButton();
            this.lblEnviarPor = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.btnConfirmar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblEnviarPor)).BeginInit();
            this.SuspendLayout();
            // 
            // btnConfirmar
            // 
            this.btnConfirmar.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnConfirmar.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConfirmar.Location = new System.Drawing.Point(392, 364);
            this.btnConfirmar.Name = "btnConfirmar";
            this.btnConfirmar.Size = new System.Drawing.Size(230, 75);
            this.btnConfirmar.TabIndex = 19;
            this.btnConfirmar.Text = "CONFIRMAR";
            this.btnConfirmar.Click += new System.EventHandler(this.btnConfirmar_Click);
            // 
            // lblEnviarPor
            // 
            this.lblEnviarPor.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblEnviarPor.AutoSize = false;
            this.lblEnviarPor.Font = new System.Drawing.Font("Segoe UI", 46F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEnviarPor.Location = new System.Drawing.Point(122, 114);
            this.lblEnviarPor.Name = "lblEnviarPor";
            this.lblEnviarPor.Size = new System.Drawing.Size(767, 197);
            this.lblEnviarPor.TabIndex = 18;
            this.lblEnviarPor.Text = "Itens do Pedido conferidos com sucesso";
            this.lblEnviarPor.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            // 
            // usrConferido
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnConfirmar);
            this.Controls.Add(this.lblEnviarPor);
            this.Name = "usrConferido";
            this.Size = new System.Drawing.Size(1011, 622);
            ((System.ComponentModel.ISupportInitialize)(this.btnConfirmar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblEnviarPor)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadButton btnConfirmar;
        private Telerik.WinControls.UI.RadLabel lblEnviarPor;
    }
}
