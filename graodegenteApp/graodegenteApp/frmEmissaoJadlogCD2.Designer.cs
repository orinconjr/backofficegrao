﻿namespace graodegenteApp
{
    partial class frmEmissaoJadlogCD2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            this.lblContagemEmissoes = new System.Windows.Forms.Label();
            this.pnDados = new System.Windows.Forms.Panel();
            this.CPF = new System.Windows.Forms.Label();
            this.txtCpf = new System.Windows.Forms.TextBox();
            this.txtPeso20 = new System.Windows.Forms.TextBox();
            this.txtPeso19 = new System.Windows.Forms.TextBox();
            this.txtPeso18 = new System.Windows.Forms.TextBox();
            this.txtPeso17 = new System.Windows.Forms.TextBox();
            this.txtPeso16 = new System.Windows.Forms.TextBox();
            this.txtPeso15 = new System.Windows.Forms.TextBox();
            this.txtPeso14 = new System.Windows.Forms.TextBox();
            this.txtPeso13 = new System.Windows.Forms.TextBox();
            this.txtPeso12 = new System.Windows.Forms.TextBox();
            this.txtPeso11 = new System.Windows.Forms.TextBox();
            this.txtPeso10 = new System.Windows.Forms.TextBox();
            this.txtPeso9 = new System.Windows.Forms.TextBox();
            this.txtPeso8 = new System.Windows.Forms.TextBox();
            this.txtPeso7 = new System.Windows.Forms.TextBox();
            this.txtPeso6 = new System.Windows.Forms.TextBox();
            this.txtPeso5 = new System.Windows.Forms.TextBox();
            this.txtPeso4 = new System.Windows.Forms.TextBox();
            this.txtPeso3 = new System.Windows.Forms.TextBox();
            this.txtPeso2 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtTipoEnvio = new System.Windows.Forms.TextBox();
            this.txtPeso1 = new System.Windows.Forms.TextBox();
            this.txtPacotes = new System.Windows.Forms.TextBox();
            this.txtTelefone = new System.Windows.Forms.TextBox();
            this.txtDDD = new System.Windows.Forms.TextBox();
            this.txtCep = new System.Windows.Forms.TextBox();
            this.txtUf = new System.Windows.Forms.TextBox();
            this.txtBairro = new System.Windows.Forms.TextBox();
            this.txtCidade = new System.Windows.Forms.TextBox();
            this.txtRua = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtDestinatario = new System.Windows.Forms.TextBox();
            this.btnCarregarEmissao = new System.Windows.Forms.Button();
            this.btnImprimirNota = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.grd = new Telerik.WinControls.UI.RadGridView();
            this.grdNotas = new Telerik.WinControls.UI.RadGridView();
            this.label13 = new System.Windows.Forms.Label();
            this.pnDados.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grd.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdNotas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdNotas.MasterTemplate)).BeginInit();
            this.SuspendLayout();
            // 
            // lblContagemEmissoes
            // 
            this.lblContagemEmissoes.AutoSize = true;
            this.lblContagemEmissoes.Location = new System.Drawing.Point(12, 18);
            this.lblContagemEmissoes.Name = "lblContagemEmissoes";
            this.lblContagemEmissoes.Size = new System.Drawing.Size(108, 13);
            this.lblContagemEmissoes.TabIndex = 0;
            this.lblContagemEmissoes.Text = "Emissões Pendentes:";
            // 
            // pnDados
            // 
            this.pnDados.Controls.Add(this.CPF);
            this.pnDados.Controls.Add(this.txtCpf);
            this.pnDados.Controls.Add(this.txtPeso20);
            this.pnDados.Controls.Add(this.txtPeso19);
            this.pnDados.Controls.Add(this.txtPeso18);
            this.pnDados.Controls.Add(this.txtPeso17);
            this.pnDados.Controls.Add(this.txtPeso16);
            this.pnDados.Controls.Add(this.txtPeso15);
            this.pnDados.Controls.Add(this.txtPeso14);
            this.pnDados.Controls.Add(this.txtPeso13);
            this.pnDados.Controls.Add(this.txtPeso12);
            this.pnDados.Controls.Add(this.txtPeso11);
            this.pnDados.Controls.Add(this.txtPeso10);
            this.pnDados.Controls.Add(this.txtPeso9);
            this.pnDados.Controls.Add(this.txtPeso8);
            this.pnDados.Controls.Add(this.txtPeso7);
            this.pnDados.Controls.Add(this.txtPeso6);
            this.pnDados.Controls.Add(this.txtPeso5);
            this.pnDados.Controls.Add(this.txtPeso4);
            this.pnDados.Controls.Add(this.txtPeso3);
            this.pnDados.Controls.Add(this.txtPeso2);
            this.pnDados.Controls.Add(this.label11);
            this.pnDados.Controls.Add(this.txtTipoEnvio);
            this.pnDados.Controls.Add(this.txtPeso1);
            this.pnDados.Controls.Add(this.txtPacotes);
            this.pnDados.Controls.Add(this.txtTelefone);
            this.pnDados.Controls.Add(this.txtDDD);
            this.pnDados.Controls.Add(this.txtCep);
            this.pnDados.Controls.Add(this.txtUf);
            this.pnDados.Controls.Add(this.txtBairro);
            this.pnDados.Controls.Add(this.txtCidade);
            this.pnDados.Controls.Add(this.txtRua);
            this.pnDados.Controls.Add(this.label10);
            this.pnDados.Controls.Add(this.label9);
            this.pnDados.Controls.Add(this.label8);
            this.pnDados.Controls.Add(this.label7);
            this.pnDados.Controls.Add(this.label6);
            this.pnDados.Controls.Add(this.label5);
            this.pnDados.Controls.Add(this.label4);
            this.pnDados.Controls.Add(this.label3);
            this.pnDados.Controls.Add(this.label2);
            this.pnDados.Controls.Add(this.label1);
            this.pnDados.Controls.Add(this.txtDestinatario);
            this.pnDados.Location = new System.Drawing.Point(15, 80);
            this.pnDados.Name = "pnDados";
            this.pnDados.Size = new System.Drawing.Size(412, 552);
            this.pnDados.TabIndex = 1;
            this.pnDados.Paint += new System.Windows.Forms.PaintEventHandler(this.pnDados_Paint);
            // 
            // CPF
            // 
            this.CPF.AutoSize = true;
            this.CPF.Location = new System.Drawing.Point(162, 13);
            this.CPF.Name = "CPF";
            this.CPF.Size = new System.Drawing.Size(27, 13);
            this.CPF.TabIndex = 31;
            this.CPF.Text = "CPF";
            // 
            // txtCpf
            // 
            this.txtCpf.Location = new System.Drawing.Point(165, 29);
            this.txtCpf.Name = "txtCpf";
            this.txtCpf.Size = new System.Drawing.Size(150, 20);
            this.txtCpf.TabIndex = 1;
            // 
            // txtPeso20
            // 
            this.txtPeso20.Location = new System.Drawing.Point(240, 522);
            this.txtPeso20.Name = "txtPeso20";
            this.txtPeso20.Size = new System.Drawing.Size(73, 20);
            this.txtPeso20.TabIndex = 30;
            this.txtPeso20.Visible = false;
            // 
            // txtPeso19
            // 
            this.txtPeso19.Location = new System.Drawing.Point(161, 522);
            this.txtPeso19.Name = "txtPeso19";
            this.txtPeso19.Size = new System.Drawing.Size(73, 20);
            this.txtPeso19.TabIndex = 29;
            this.txtPeso19.Visible = false;
            // 
            // txtPeso18
            // 
            this.txtPeso18.Location = new System.Drawing.Point(82, 522);
            this.txtPeso18.Name = "txtPeso18";
            this.txtPeso18.Size = new System.Drawing.Size(73, 20);
            this.txtPeso18.TabIndex = 28;
            this.txtPeso18.Visible = false;
            // 
            // txtPeso17
            // 
            this.txtPeso17.Location = new System.Drawing.Point(3, 522);
            this.txtPeso17.Name = "txtPeso17";
            this.txtPeso17.Size = new System.Drawing.Size(73, 20);
            this.txtPeso17.TabIndex = 27;
            this.txtPeso17.Visible = false;
            // 
            // txtPeso16
            // 
            this.txtPeso16.Location = new System.Drawing.Point(240, 496);
            this.txtPeso16.Name = "txtPeso16";
            this.txtPeso16.Size = new System.Drawing.Size(73, 20);
            this.txtPeso16.TabIndex = 26;
            this.txtPeso16.Visible = false;
            // 
            // txtPeso15
            // 
            this.txtPeso15.Location = new System.Drawing.Point(161, 496);
            this.txtPeso15.Name = "txtPeso15";
            this.txtPeso15.Size = new System.Drawing.Size(73, 20);
            this.txtPeso15.TabIndex = 25;
            this.txtPeso15.Visible = false;
            // 
            // txtPeso14
            // 
            this.txtPeso14.Location = new System.Drawing.Point(82, 496);
            this.txtPeso14.Name = "txtPeso14";
            this.txtPeso14.Size = new System.Drawing.Size(73, 20);
            this.txtPeso14.TabIndex = 24;
            this.txtPeso14.Visible = false;
            // 
            // txtPeso13
            // 
            this.txtPeso13.Location = new System.Drawing.Point(3, 496);
            this.txtPeso13.Name = "txtPeso13";
            this.txtPeso13.Size = new System.Drawing.Size(73, 20);
            this.txtPeso13.TabIndex = 23;
            this.txtPeso13.Visible = false;
            // 
            // txtPeso12
            // 
            this.txtPeso12.Location = new System.Drawing.Point(240, 470);
            this.txtPeso12.Name = "txtPeso12";
            this.txtPeso12.Size = new System.Drawing.Size(73, 20);
            this.txtPeso12.TabIndex = 22;
            this.txtPeso12.Visible = false;
            // 
            // txtPeso11
            // 
            this.txtPeso11.Location = new System.Drawing.Point(161, 470);
            this.txtPeso11.Name = "txtPeso11";
            this.txtPeso11.Size = new System.Drawing.Size(73, 20);
            this.txtPeso11.TabIndex = 21;
            this.txtPeso11.Visible = false;
            // 
            // txtPeso10
            // 
            this.txtPeso10.Location = new System.Drawing.Point(82, 470);
            this.txtPeso10.Name = "txtPeso10";
            this.txtPeso10.Size = new System.Drawing.Size(73, 20);
            this.txtPeso10.TabIndex = 20;
            this.txtPeso10.Visible = false;
            // 
            // txtPeso9
            // 
            this.txtPeso9.Location = new System.Drawing.Point(3, 470);
            this.txtPeso9.Name = "txtPeso9";
            this.txtPeso9.Size = new System.Drawing.Size(73, 20);
            this.txtPeso9.TabIndex = 19;
            this.txtPeso9.Visible = false;
            // 
            // txtPeso8
            // 
            this.txtPeso8.Location = new System.Drawing.Point(240, 444);
            this.txtPeso8.Name = "txtPeso8";
            this.txtPeso8.Size = new System.Drawing.Size(73, 20);
            this.txtPeso8.TabIndex = 18;
            this.txtPeso8.Visible = false;
            // 
            // txtPeso7
            // 
            this.txtPeso7.Location = new System.Drawing.Point(161, 444);
            this.txtPeso7.Name = "txtPeso7";
            this.txtPeso7.Size = new System.Drawing.Size(73, 20);
            this.txtPeso7.TabIndex = 17;
            this.txtPeso7.Visible = false;
            // 
            // txtPeso6
            // 
            this.txtPeso6.Location = new System.Drawing.Point(82, 444);
            this.txtPeso6.Name = "txtPeso6";
            this.txtPeso6.Size = new System.Drawing.Size(73, 20);
            this.txtPeso6.TabIndex = 16;
            this.txtPeso6.Visible = false;
            // 
            // txtPeso5
            // 
            this.txtPeso5.Location = new System.Drawing.Point(3, 444);
            this.txtPeso5.Name = "txtPeso5";
            this.txtPeso5.Size = new System.Drawing.Size(73, 20);
            this.txtPeso5.TabIndex = 15;
            this.txtPeso5.Visible = false;
            // 
            // txtPeso4
            // 
            this.txtPeso4.Location = new System.Drawing.Point(240, 418);
            this.txtPeso4.Name = "txtPeso4";
            this.txtPeso4.Size = new System.Drawing.Size(73, 20);
            this.txtPeso4.TabIndex = 14;
            this.txtPeso4.Visible = false;
            // 
            // txtPeso3
            // 
            this.txtPeso3.Location = new System.Drawing.Point(161, 418);
            this.txtPeso3.Name = "txtPeso3";
            this.txtPeso3.Size = new System.Drawing.Size(73, 20);
            this.txtPeso3.TabIndex = 13;
            this.txtPeso3.Visible = false;
            // 
            // txtPeso2
            // 
            this.txtPeso2.Location = new System.Drawing.Point(82, 418);
            this.txtPeso2.Name = "txtPeso2";
            this.txtPeso2.Size = new System.Drawing.Size(73, 20);
            this.txtPeso2.TabIndex = 12;
            this.txtPeso2.Visible = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(3, 13);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(73, 13);
            this.label11.TabIndex = 22;
            this.label11.Text = "Tipo de Envio";
            // 
            // txtTipoEnvio
            // 
            this.txtTipoEnvio.Location = new System.Drawing.Point(5, 29);
            this.txtTipoEnvio.Name = "txtTipoEnvio";
            this.txtTipoEnvio.Size = new System.Drawing.Size(150, 20);
            this.txtTipoEnvio.TabIndex = 0;
            // 
            // txtPeso1
            // 
            this.txtPeso1.Location = new System.Drawing.Point(3, 418);
            this.txtPeso1.Name = "txtPeso1";
            this.txtPeso1.Size = new System.Drawing.Size(73, 20);
            this.txtPeso1.TabIndex = 11;
            this.txtPeso1.Visible = false;
            // 
            // txtPacotes
            // 
            this.txtPacotes.Location = new System.Drawing.Point(3, 379);
            this.txtPacotes.Name = "txtPacotes";
            this.txtPacotes.Size = new System.Drawing.Size(310, 20);
            this.txtPacotes.TabIndex = 10;
            // 
            // txtTelefone
            // 
            this.txtTelefone.Location = new System.Drawing.Point(3, 340);
            this.txtTelefone.Name = "txtTelefone";
            this.txtTelefone.Size = new System.Drawing.Size(310, 20);
            this.txtTelefone.TabIndex = 9;
            // 
            // txtDDD
            // 
            this.txtDDD.Location = new System.Drawing.Point(3, 301);
            this.txtDDD.Name = "txtDDD";
            this.txtDDD.Size = new System.Drawing.Size(310, 20);
            this.txtDDD.TabIndex = 8;
            // 
            // txtCep
            // 
            this.txtCep.Location = new System.Drawing.Point(3, 262);
            this.txtCep.Name = "txtCep";
            this.txtCep.Size = new System.Drawing.Size(310, 20);
            this.txtCep.TabIndex = 7;
            // 
            // txtUf
            // 
            this.txtUf.Location = new System.Drawing.Point(3, 223);
            this.txtUf.Name = "txtUf";
            this.txtUf.Size = new System.Drawing.Size(310, 20);
            this.txtUf.TabIndex = 6;
            // 
            // txtBairro
            // 
            this.txtBairro.Location = new System.Drawing.Point(3, 184);
            this.txtBairro.Name = "txtBairro";
            this.txtBairro.Size = new System.Drawing.Size(310, 20);
            this.txtBairro.TabIndex = 5;
            // 
            // txtCidade
            // 
            this.txtCidade.Location = new System.Drawing.Point(3, 145);
            this.txtCidade.Name = "txtCidade";
            this.txtCidade.Size = new System.Drawing.Size(310, 20);
            this.txtCidade.TabIndex = 4;
            // 
            // txtRua
            // 
            this.txtRua.Location = new System.Drawing.Point(3, 106);
            this.txtRua.Name = "txtRua";
            this.txtRua.Size = new System.Drawing.Size(310, 20);
            this.txtRua.TabIndex = 3;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(2, 402);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(74, 13);
            this.label10.TabIndex = 12;
            this.label10.Text = "Peso Volumes";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(2, 363);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(46, 13);
            this.label9.TabIndex = 11;
            this.label9.Text = "Pacotes";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(3, 324);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(49, 13);
            this.label8.TabIndex = 10;
            this.label8.Text = "Telefone";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(2, 285);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(31, 13);
            this.label7.TabIndex = 9;
            this.label7.Text = "DDD";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 246);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(28, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "CEP";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 207);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(21, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "UF";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(2, 168);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(34, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Bairro";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(2, 129);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Cidade";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 90);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(27, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Rua";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 51);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Destinatário";
            // 
            // txtDestinatario
            // 
            this.txtDestinatario.Location = new System.Drawing.Point(5, 67);
            this.txtDestinatario.Name = "txtDestinatario";
            this.txtDestinatario.Size = new System.Drawing.Size(310, 20);
            this.txtDestinatario.TabIndex = 2;
            // 
            // btnCarregarEmissao
            // 
            this.btnCarregarEmissao.Location = new System.Drawing.Point(15, 41);
            this.btnCarregarEmissao.Name = "btnCarregarEmissao";
            this.btnCarregarEmissao.Size = new System.Drawing.Size(210, 33);
            this.btnCarregarEmissao.TabIndex = 2;
            this.btnCarregarEmissao.Text = "Carregar Próxima Emissão";
            this.btnCarregarEmissao.UseVisualStyleBackColor = true;
            this.btnCarregarEmissao.Click += new System.EventHandler(this.btnCarregarEmissao_Click);
            // 
            // btnImprimirNota
            // 
            this.btnImprimirNota.BackColor = System.Drawing.Color.Red;
            this.btnImprimirNota.Location = new System.Drawing.Point(231, 41);
            this.btnImprimirNota.Name = "btnImprimirNota";
            this.btnImprimirNota.Size = new System.Drawing.Size(196, 33);
            this.btnImprimirNota.TabIndex = 3;
            this.btnImprimirNota.Text = "Imprimir Nota";
            this.btnImprimirNota.UseVisualStyleBackColor = false;
            this.btnImprimirNota.Visible = false;
            this.btnImprimirNota.Click += new System.EventHandler(this.btnImprimirNota_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(452, 18);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(108, 13);
            this.label12.TabIndex = 4;
            this.label12.Text = "Emissões Pendentes:";
            // 
            // grd
            // 
            this.grd.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grd.Location = new System.Drawing.Point(455, 46);
            // 
            // 
            // 
            this.grd.MasterTemplate.AllowAddNewRow = false;
            this.grd.MasterTemplate.AllowDeleteRow = false;
            this.grd.MasterTemplate.AllowEditRow = false;
            this.grd.MasterTemplate.AutoGenerateColumns = false;
            this.grd.MasterTemplate.AutoSizeColumnsMode = Telerik.WinControls.UI.GridViewAutoSizeColumnsMode.Fill;
            gridViewTextBoxColumn1.FieldName = "idPedidoEnvio";
            gridViewTextBoxColumn1.HeaderText = "ID";
            gridViewTextBoxColumn1.Name = "idPedidoEnvio";
            gridViewTextBoxColumn1.Width = 81;
            gridViewTextBoxColumn2.FieldName = "enderecos";
            gridViewTextBoxColumn2.HeaderText = "Endereços";
            gridViewTextBoxColumn2.Name = "enderecos";
            gridViewTextBoxColumn2.Width = 213;
            gridViewTextBoxColumn3.Expression = "";
            gridViewTextBoxColumn3.FieldName = "endNomeDoDestinatario";
            gridViewTextBoxColumn3.HeaderText = "Nome";
            gridViewTextBoxColumn3.Name = "endNomeDoDestinatario";
            gridViewTextBoxColumn3.Width = 80;
            gridViewTextBoxColumn4.FieldName = "endCidade";
            gridViewTextBoxColumn4.HeaderText = "Cidade";
            gridViewTextBoxColumn4.Name = "endCidade";
            gridViewTextBoxColumn4.Width = 112;
            gridViewTextBoxColumn5.FieldName = "endEstado";
            gridViewTextBoxColumn5.HeaderText = "Estado";
            gridViewTextBoxColumn5.Name = "endEstado";
            gridViewTextBoxColumn5.Width = 112;
            gridViewTextBoxColumn6.FieldName = "totalPacotes";
            gridViewTextBoxColumn6.HeaderText = "Volumes";
            gridViewTextBoxColumn6.Name = "totalPacotes";
            gridViewTextBoxColumn6.Width = 41;
            gridViewTextBoxColumn7.FieldName = "dataFimEmbalagem";
            gridViewTextBoxColumn7.HeaderText = "Data";
            gridViewTextBoxColumn7.Name = "dataFimEmbalagem";
            gridViewTextBoxColumn7.Width = 115;
            this.grd.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7});
            this.grd.MasterTemplate.EnableFiltering = true;
            this.grd.MasterTemplate.EnableGrouping = false;
            this.grd.MasterTemplate.MultiSelect = true;
            this.grd.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.grd.Name = "grd";
            this.grd.Size = new System.Drawing.Size(769, 254);
            this.grd.TabIndex = 5;
            this.grd.Text = "radGridView1";
            this.grd.Click += new System.EventHandler(this.grd_Click);
            this.grd.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.grd_MouseDoubleClick);
            // 
            // grdNotas
            // 
            this.grdNotas.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grdNotas.Location = new System.Drawing.Point(455, 341);
            // 
            // 
            // 
            this.grdNotas.MasterTemplate.AllowAddNewRow = false;
            this.grdNotas.MasterTemplate.AllowDeleteRow = false;
            this.grdNotas.MasterTemplate.AllowEditRow = false;
            this.grdNotas.MasterTemplate.AutoGenerateColumns = false;
            this.grdNotas.MasterTemplate.AutoSizeColumnsMode = Telerik.WinControls.UI.GridViewAutoSizeColumnsMode.Fill;
            gridViewTextBoxColumn8.FieldName = "idPedidoEnvio";
            gridViewTextBoxColumn8.HeaderText = "ID";
            gridViewTextBoxColumn8.Name = "idPedidoEnvio";
            gridViewTextBoxColumn8.Width = 154;
            gridViewTextBoxColumn9.Expression = "";
            gridViewTextBoxColumn9.FieldName = "dataFimEmbalagem";
            gridViewTextBoxColumn9.HeaderText = "Data";
            gridViewTextBoxColumn9.Name = "dataFimEmbalagem";
            gridViewTextBoxColumn9.Width = 327;
            gridViewTextBoxColumn10.FieldName = "nfeNumero";
            gridViewTextBoxColumn10.HeaderText = "Nota";
            gridViewTextBoxColumn10.Name = "nfeNumero";
            gridViewTextBoxColumn10.Width = 135;
            gridViewTextBoxColumn11.FieldName = "status";
            gridViewTextBoxColumn11.HeaderText = "Status";
            gridViewTextBoxColumn11.Name = "status";
            gridViewTextBoxColumn11.Width = 135;
            this.grdNotas.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn8,
            gridViewTextBoxColumn9,
            gridViewTextBoxColumn10,
            gridViewTextBoxColumn11});
            this.grdNotas.MasterTemplate.EnableFiltering = true;
            this.grdNotas.MasterTemplate.EnableGrouping = false;
            this.grdNotas.MasterTemplate.MultiSelect = true;
            this.grdNotas.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.grdNotas.Name = "grdNotas";
            this.grdNotas.Size = new System.Drawing.Size(769, 255);
            this.grdNotas.TabIndex = 7;
            this.grdNotas.Text = "radGridView1";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(452, 321);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(128, 13);
            this.label13.TabIndex = 6;
            this.label13.Text = "Notas em Processamento";
            // 
            // frmEmissaoJadlogCD2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1236, 640);
            this.Controls.Add(this.grdNotas);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.grd);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.btnImprimirNota);
            this.Controls.Add(this.btnCarregarEmissao);
            this.Controls.Add(this.pnDados);
            this.Controls.Add(this.lblContagemEmissoes);
            this.Name = "frmEmissaoJadlogCD2";
            this.Text = "Emissão CD2";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmEmissaoJadlogCD2_Load);
            this.pnDados.ResumeLayout(false);
            this.pnDados.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grd.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdNotas.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdNotas)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblContagemEmissoes;
        private System.Windows.Forms.Panel pnDados;
        private System.Windows.Forms.Button btnCarregarEmissao;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtDestinatario;
        private System.Windows.Forms.TextBox txtPeso20;
        private System.Windows.Forms.TextBox txtPeso19;
        private System.Windows.Forms.TextBox txtPeso18;
        private System.Windows.Forms.TextBox txtPeso17;
        private System.Windows.Forms.TextBox txtPeso16;
        private System.Windows.Forms.TextBox txtPeso15;
        private System.Windows.Forms.TextBox txtPeso14;
        private System.Windows.Forms.TextBox txtPeso13;
        private System.Windows.Forms.TextBox txtPeso12;
        private System.Windows.Forms.TextBox txtPeso11;
        private System.Windows.Forms.TextBox txtPeso10;
        private System.Windows.Forms.TextBox txtPeso9;
        private System.Windows.Forms.TextBox txtPeso8;
        private System.Windows.Forms.TextBox txtPeso7;
        private System.Windows.Forms.TextBox txtPeso6;
        private System.Windows.Forms.TextBox txtPeso5;
        private System.Windows.Forms.TextBox txtPeso4;
        private System.Windows.Forms.TextBox txtPeso3;
        private System.Windows.Forms.TextBox txtPeso2;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtTipoEnvio;
        private System.Windows.Forms.TextBox txtPeso1;
        private System.Windows.Forms.TextBox txtPacotes;
        private System.Windows.Forms.TextBox txtTelefone;
        private System.Windows.Forms.TextBox txtDDD;
        private System.Windows.Forms.TextBox txtCep;
        private System.Windows.Forms.TextBox txtUf;
        private System.Windows.Forms.TextBox txtBairro;
        private System.Windows.Forms.TextBox txtCidade;
        private System.Windows.Forms.TextBox txtRua;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label CPF;
        private System.Windows.Forms.TextBox txtCpf;
        private System.Windows.Forms.Button btnImprimirNota;
        private System.Windows.Forms.Label label12;
        private Telerik.WinControls.UI.RadGridView grd;
        private Telerik.WinControls.UI.RadGridView grdNotas;
        private System.Windows.Forms.Label label13;
    }
}