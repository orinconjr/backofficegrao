﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using graodegenteApp.wsEmbalagem;
using Telerik.WinControls;
using Telerik.WinControls.UI;

namespace graodegenteApp
{
    public partial class frmReimprimirEtiquetaPedido : Form
    {
        public frmReimprimirEtiquetaPedido()
        {
            InitializeComponent();
        }

        private void btnImprimir_Click(object sender, EventArgs e)
        {
            int idPedido = 0;
            if (txtIdItemPedido.Text.Contains("-"))
            {
                idPedido = Utilidades.retornaIdInterno(Convert.ToInt32(txtIdItemPedido.Text.Split('-')[0]));
            }
            else
            {
                int.TryParse(txtIdItemPedido.Text, out idPedido);
            }

            var embalagem = new EmbalagemWs();
            var retornoFinaliza = embalagem.ReimprimirEtiquetaPedidoCds(idPedido, Convert.ToInt32(Properties.Settings.Default["cd"].ToString()));
            
            int pacotes = Convert.ToInt32(retornoFinaliza.totalVolumes);
            for (int i = 1; i <= pacotes; i++)
            {
                clsImprimirEtiqueta.GerarEtiquetaEnderecoTnt(
                    Utilidades.retornaIdCliente(idPedido).ToString() + "-" + i + "/" +
                    pacotes,
                    retornoFinaliza.etiqueta.telResidencial,
                    retornoFinaliza.etiqueta.telComercial,
                    retornoFinaliza.etiqueta.telCelular,
                    retornoFinaliza.etiqueta.nome,
                    retornoFinaliza.etiqueta.rua,
                    retornoFinaliza.etiqueta.numero,
                    retornoFinaliza.etiqueta.complemento,
                    retornoFinaliza.etiqueta.bairro,
                    retornoFinaliza.etiqueta.cidade,
                    retornoFinaliza.etiqueta.estado,
                    retornoFinaliza.etiqueta.cep,
                    retornoFinaliza.etiqueta.referencia,
                    retornoFinaliza.cnpj,
                    Convert.ToInt32(retornoFinaliza.nfeNumero),
                    i,
                    pacotes);
            }
            
            txtIdItemPedido.Text = "";
            txtIdItemPedido.Focus();
        }

        private void txtIdItemPedido_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnImprimir.PerformClick();
            }
        }

        private void frmReimprimirEtiquetaPedido_Load(object sender, EventArgs e)
        {
            txtIdItemPedido.Focus();
        }
    }
}
