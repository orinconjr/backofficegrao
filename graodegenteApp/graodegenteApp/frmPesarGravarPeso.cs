﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace graodegenteApp
{
    public partial class frmPesarGravarPeso : Form
    {
        public int peso;
        public frmPesarGravarPeso()
        {
            InitializeComponent();
        }

        private void frmPesarGravarPeso_Load(object sender, EventArgs e)
        {
            txtRastreio.Focus();
        }

        private void txtRastreio_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && txtRastreio.Text != "")
            {
                int pesoInformado = 0;
                int.TryParse(txtRastreio.Text, out pesoInformado);

                if (pesoInformado > 0)
                {
                    peso = pesoInformado;
                    this.DialogResult = DialogResult.OK;
                    this.Close();
                }
                else
                {
                    this.DialogResult = DialogResult.Abort;
                }
            }
        }
    }
}
