﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;

namespace graodegenteApp
{
    public partial class frmConsultarRomaneioLista : Form
    {
        public int idPedidoFornecedor = 0;
        public frmConsultarRomaneioLista()
        {
            ThemeResolutionService.ApplicationThemeName = "TelerikMetroBlue";
            InitializeComponent();
        }

        private void frmConsultarRomaneioLista_Load(object sender, EventArgs e)
        {

            var produtos = new wsPedidosFornecedor.PedidosFornecedorWs();
            var itens = produtos.ListaProdutosPendentes(idPedidoFornecedor, clsConfiguracao.ChaveWs);
            radGridView1.DataSource = itens;
            radGridView1.BestFitColumns();
        }

        private void radGridView1_DoubleClick(object sender, EventArgs e)
        {
            var confirmacao = MessageBox.Show("Deseja imprimir a etiqueta deste produto?", "Confirmação", MessageBoxButtons.YesNo);
            if (confirmacao == DialogResult.Yes)
            {
                if (radGridView1.SelectedRows.Count() > 0)
                {
                    var cod = radGridView1.SelectedRows[0].Cells[0].Value;
                    var pedidosWs = new wsPedidosFornecedor.PedidosFornecedorWs();
                    var detalheProduto = pedidosWs.RetornaDetalhesItem(Convert.ToInt32(cod), clsConfiguracao.ChaveWs, false);
                    clsImprimirEtiqueta.GerarEtiqueta(cod.ToString(), detalheProduto.idPedidoFornecedor.ToString(), detalheProduto.fornecedorNome, detalheProduto.numeroItem.ToString(), detalheProduto.totalItens.ToString(), detalheProduto.produtoIdDaEmpresa, detalheProduto.complementoIdDaEmpresa, detalheProduto.produtoNome, detalheProduto.codigoEtiqueta, detalheProduto.produtoId.ToString());
                }
                 
            }
        }
    }
}
