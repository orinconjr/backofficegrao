﻿namespace graodegenteApp
{
    partial class usrEmbalarPesoPacotes
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtPesoPacote = new Telerik.WinControls.UI.RadSpinEditor();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.btnConfirmar = new Telerik.WinControls.UI.RadButton();
            this.txtNumeroPedido = new Telerik.WinControls.UI.RadTextBox();
            this.lblNumeroPedido = new Telerik.WinControls.UI.RadLabel();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.txtLargura = new Telerik.WinControls.UI.RadSpinEditor();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.txtAltura = new Telerik.WinControls.UI.RadSpinEditor();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.txtProfundidade = new Telerik.WinControls.UI.RadSpinEditor();
            this.panel1 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.txtPesoPacote)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnConfirmar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNumeroPedido)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNumeroPedido)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLargura)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAltura)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProfundidade)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtPesoPacote
            // 
            this.txtPesoPacote.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtPesoPacote.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPesoPacote.Location = new System.Drawing.Point(7, 48);
            this.txtPesoPacote.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.txtPesoPacote.Name = "txtPesoPacote";
            // 
            // 
            // 
            this.txtPesoPacote.RootElement.AccessibleDescription = null;
            this.txtPesoPacote.RootElement.AccessibleName = null;
            this.txtPesoPacote.RootElement.ControlBounds = new System.Drawing.Rectangle(0, 0, 100, 20);
            this.txtPesoPacote.RootElement.StretchVertically = true;
            this.txtPesoPacote.Size = new System.Drawing.Size(230, 31);
            this.txtPesoPacote.TabIndex = 14;
            this.txtPesoPacote.TabStop = false;
            this.txtPesoPacote.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtPesoPacote_KeyDown);
            // 
            // radLabel2
            // 
            this.radLabel2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radLabel2.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radLabel2.Font = new System.Drawing.Font("Segoe UI", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel2.Location = new System.Drawing.Point(209, 4);
            this.radLabel2.Name = "radLabel2";
            // 
            // 
            // 
            this.radLabel2.RootElement.AccessibleDescription = null;
            this.radLabel2.RootElement.AccessibleName = null;
            this.radLabel2.RootElement.ControlBounds = new System.Drawing.Rectangle(0, 0, 100, 18);
            this.radLabel2.Size = new System.Drawing.Size(276, 51);
            this.radLabel2.TabIndex = 13;
            this.radLabel2.Text = "Peso dos Pacotes";
            // 
            // btnConfirmar
            // 
            this.btnConfirmar.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnConfirmar.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnConfirmar.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConfirmar.Location = new System.Drawing.Point(225, 486);
            this.btnConfirmar.Name = "btnConfirmar";
            // 
            // 
            // 
            this.btnConfirmar.RootElement.AccessibleDescription = null;
            this.btnConfirmar.RootElement.AccessibleName = null;
            this.btnConfirmar.RootElement.ControlBounds = new System.Drawing.Rectangle(0, 0, 110, 24);
            this.btnConfirmar.Size = new System.Drawing.Size(230, 39);
            this.btnConfirmar.TabIndex = 12;
            this.btnConfirmar.Text = "CONFIRMAR";
            this.btnConfirmar.Click += new System.EventHandler(this.btnConfirmar_Click);
            // 
            // txtNumeroPedido
            // 
            this.txtNumeroPedido.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtNumeroPedido.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtNumeroPedido.Font = new System.Drawing.Font("Segoe UI", 16F);
            this.txtNumeroPedido.Location = new System.Drawing.Point(225, 112);
            this.txtNumeroPedido.Name = "txtNumeroPedido";
            // 
            // 
            // 
            this.txtNumeroPedido.RootElement.AccessibleDescription = null;
            this.txtNumeroPedido.RootElement.AccessibleName = null;
            this.txtNumeroPedido.RootElement.ControlBounds = new System.Drawing.Rectangle(0, 0, 100, 20);
            this.txtNumeroPedido.RootElement.StretchVertically = true;
            this.txtNumeroPedido.Size = new System.Drawing.Size(230, 34);
            this.txtNumeroPedido.TabIndex = 15;
            this.txtNumeroPedido.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNumeroPedido_KeyDown);
            // 
            // lblNumeroPedido
            // 
            this.lblNumeroPedido.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblNumeroPedido.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblNumeroPedido.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumeroPedido.Location = new System.Drawing.Point(245, 78);
            this.lblNumeroPedido.Name = "lblNumeroPedido";
            // 
            // 
            // 
            this.lblNumeroPedido.RootElement.AccessibleDescription = null;
            this.lblNumeroPedido.RootElement.AccessibleName = null;
            this.lblNumeroPedido.RootElement.ControlBounds = new System.Drawing.Rectangle(0, 0, 100, 18);
            this.lblNumeroPedido.Size = new System.Drawing.Size(192, 33);
            this.lblNumeroPedido.TabIndex = 14;
            this.lblNumeroPedido.Text = "Número do Pedido";
            // 
            // radLabel3
            // 
            this.radLabel3.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radLabel3.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel3.Location = new System.Drawing.Point(46, 9);
            this.radLabel3.Name = "radLabel3";
            // 
            // 
            // 
            this.radLabel3.RootElement.AccessibleDescription = null;
            this.radLabel3.RootElement.AccessibleName = null;
            this.radLabel3.RootElement.ControlBounds = new System.Drawing.Rectangle(0, 0, 100, 18);
            this.radLabel3.Size = new System.Drawing.Size(156, 33);
            this.radLabel3.TabIndex = 15;
            this.radLabel3.Text = "Peso do Pacote";
            // 
            // radLabel4
            // 
            this.radLabel4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radLabel4.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel4.Location = new System.Drawing.Point(86, 85);
            this.radLabel4.Name = "radLabel4";
            // 
            // 
            // 
            this.radLabel4.RootElement.AccessibleDescription = null;
            this.radLabel4.RootElement.AccessibleName = null;
            this.radLabel4.RootElement.ControlBounds = new System.Drawing.Rectangle(0, 0, 100, 18);
            this.radLabel4.Size = new System.Drawing.Size(82, 33);
            this.radLabel4.TabIndex = 17;
            this.radLabel4.Text = "Largura";
            this.radLabel4.Visible = false;
            // 
            // txtLargura
            // 
            this.txtLargura.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtLargura.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLargura.Location = new System.Drawing.Point(7, 124);
            this.txtLargura.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.txtLargura.Name = "txtLargura";
            // 
            // 
            // 
            this.txtLargura.RootElement.AccessibleDescription = null;
            this.txtLargura.RootElement.AccessibleName = null;
            this.txtLargura.RootElement.ControlBounds = new System.Drawing.Rectangle(0, 0, 100, 20);
            this.txtLargura.RootElement.StretchVertically = true;
            this.txtLargura.Size = new System.Drawing.Size(230, 31);
            this.txtLargura.TabIndex = 16;
            this.txtLargura.TabStop = false;
            this.txtLargura.Visible = false;
            this.txtLargura.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtLargura_KeyDown);
            // 
            // radLabel5
            // 
            this.radLabel5.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radLabel5.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel5.Location = new System.Drawing.Point(97, 161);
            this.radLabel5.Name = "radLabel5";
            // 
            // 
            // 
            this.radLabel5.RootElement.AccessibleDescription = null;
            this.radLabel5.RootElement.AccessibleName = null;
            this.radLabel5.RootElement.ControlBounds = new System.Drawing.Rectangle(0, 0, 100, 18);
            this.radLabel5.Size = new System.Drawing.Size(67, 33);
            this.radLabel5.TabIndex = 19;
            this.radLabel5.Text = "Altura";
            this.radLabel5.Visible = false;
            // 
            // txtAltura
            // 
            this.txtAltura.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtAltura.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAltura.Location = new System.Drawing.Point(7, 200);
            this.txtAltura.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.txtAltura.Name = "txtAltura";
            // 
            // 
            // 
            this.txtAltura.RootElement.AccessibleDescription = null;
            this.txtAltura.RootElement.AccessibleName = null;
            this.txtAltura.RootElement.ControlBounds = new System.Drawing.Rectangle(0, 0, 100, 20);
            this.txtAltura.RootElement.StretchVertically = true;
            this.txtAltura.Size = new System.Drawing.Size(230, 31);
            this.txtAltura.TabIndex = 18;
            this.txtAltura.TabStop = false;
            this.txtAltura.Visible = false;
            this.txtAltura.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtAltura_KeyDown);
            // 
            // radLabel6
            // 
            this.radLabel6.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radLabel6.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel6.Location = new System.Drawing.Point(63, 243);
            this.radLabel6.Name = "radLabel6";
            // 
            // 
            // 
            this.radLabel6.RootElement.AccessibleDescription = null;
            this.radLabel6.RootElement.AccessibleName = null;
            this.radLabel6.RootElement.ControlBounds = new System.Drawing.Rectangle(0, 0, 100, 18);
            this.radLabel6.Size = new System.Drawing.Size(139, 33);
            this.radLabel6.TabIndex = 21;
            this.radLabel6.Text = "Profundidade";
            this.radLabel6.Visible = false;
            // 
            // txtProfundidade
            // 
            this.txtProfundidade.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtProfundidade.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtProfundidade.Location = new System.Drawing.Point(7, 282);
            this.txtProfundidade.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.txtProfundidade.Name = "txtProfundidade";
            // 
            // 
            // 
            this.txtProfundidade.RootElement.AccessibleDescription = null;
            this.txtProfundidade.RootElement.AccessibleName = null;
            this.txtProfundidade.RootElement.ControlBounds = new System.Drawing.Rectangle(0, 0, 100, 20);
            this.txtProfundidade.RootElement.StretchVertically = true;
            this.txtProfundidade.Size = new System.Drawing.Size(230, 31);
            this.txtProfundidade.TabIndex = 20;
            this.txtProfundidade.TabStop = false;
            this.txtProfundidade.Visible = false;
            this.txtProfundidade.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtProfundidade_KeyDown);
            // 
            // panel1
            // 
            this.panel1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.panel1.Controls.Add(this.radLabel3);
            this.panel1.Controls.Add(this.radLabel6);
            this.panel1.Controls.Add(this.txtPesoPacote);
            this.panel1.Controls.Add(this.txtProfundidade);
            this.panel1.Controls.Add(this.txtLargura);
            this.panel1.Controls.Add(this.radLabel5);
            this.panel1.Controls.Add(this.radLabel4);
            this.panel1.Controls.Add(this.txtAltura);
            this.panel1.Location = new System.Drawing.Point(220, 160);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(242, 320);
            this.panel1.TabIndex = 22;
            // 
            // usrEmbalarPesoPacotes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Azure;
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.lblNumeroPedido);
            this.Controls.Add(this.txtNumeroPedido);
            this.Controls.Add(this.radLabel2);
            this.Controls.Add(this.btnConfirmar);
            this.Name = "usrEmbalarPesoPacotes";
            this.Size = new System.Drawing.Size(704, 590);
            this.Load += new System.EventHandler(this.usrEmbalarPesoPacotes_Load);
            ((System.ComponentModel.ISupportInitialize)(this.txtPesoPacote)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnConfirmar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNumeroPedido)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNumeroPedido)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLargura)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAltura)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProfundidade)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadSpinEditor txtPesoPacote;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadButton btnConfirmar;
        private Telerik.WinControls.UI.RadTextBox txtNumeroPedido;
        private Telerik.WinControls.UI.RadLabel lblNumeroPedido;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadSpinEditor txtLargura;
        private Telerik.WinControls.UI.RadSpinEditor txtAltura;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadSpinEditor txtProfundidade;
        private System.Windows.Forms.Panel panel1;
    }
}
