﻿namespace graodegenteApp
{
    partial class usrEmbalarAlterarPacotes
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblTitulo = new Telerik.WinControls.UI.RadLabel();
            this.txtEntrar = new Telerik.WinControls.UI.RadButton();
            this.txtQuantidadePacotes = new Telerik.WinControls.UI.RadSpinEditor();
            this.txtCodigoDeBarras = new Telerik.WinControls.UI.RadTextBox();
            this.lblNomeProduto = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitulo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEntrar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQuantidadePacotes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCodigoDeBarras)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNomeProduto)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTitulo
            // 
            this.lblTitulo.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblTitulo.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitulo.Location = new System.Drawing.Point(162, 140);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Size = new System.Drawing.Size(390, 37);
            this.lblTitulo.TabIndex = 10;
            this.lblTitulo.Text = "Quantidade de Pacotes Embalados";
            // 
            // txtEntrar
            // 
            this.txtEntrar.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtEntrar.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEntrar.Location = new System.Drawing.Point(228, 244);
            this.txtEntrar.Name = "txtEntrar";
            this.txtEntrar.Size = new System.Drawing.Size(230, 39);
            this.txtEntrar.TabIndex = 9;
            this.txtEntrar.Text = "CONFIRMAR";
            this.txtEntrar.Click += new System.EventHandler(this.txtEntrar_Click);
            // 
            // txtQuantidadePacotes
            // 
            this.txtQuantidadePacotes.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtQuantidadePacotes.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtQuantidadePacotes.Location = new System.Drawing.Point(228, 183);
            this.txtQuantidadePacotes.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.txtQuantidadePacotes.Name = "txtQuantidadePacotes";
            this.txtQuantidadePacotes.Size = new System.Drawing.Size(230, 31);
            this.txtQuantidadePacotes.TabIndex = 11;
            this.txtQuantidadePacotes.TabStop = false;
            this.txtQuantidadePacotes.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtQuantidadePacotes_KeyDown_1);
            // 
            // txtCodigoDeBarras
            // 
            this.txtCodigoDeBarras.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtCodigoDeBarras.Font = new System.Drawing.Font("Segoe UI", 16F);
            this.txtCodigoDeBarras.Location = new System.Drawing.Point(55, 84);
            this.txtCodigoDeBarras.Name = "txtCodigoDeBarras";
            this.txtCodigoDeBarras.Size = new System.Drawing.Size(613, 34);
            this.txtCodigoDeBarras.TabIndex = 16;
            this.txtCodigoDeBarras.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCodigoDeBarras_KeyDown);
            // 
            // lblNomeProduto
            // 
            this.lblNomeProduto.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblNomeProduto.AutoSize = false;
            this.lblNomeProduto.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNomeProduto.Location = new System.Drawing.Point(50, 31);
            this.lblNomeProduto.Name = "lblNomeProduto";
            this.lblNomeProduto.Size = new System.Drawing.Size(622, 44);
            this.lblNomeProduto.TabIndex = 17;
            this.lblNomeProduto.Text = "Selecione o Pedido (codigo de baixo)";
            this.lblNomeProduto.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            // 
            // usrEmbalarAlterarPacotes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Azure;
            this.Controls.Add(this.txtCodigoDeBarras);
            this.Controls.Add(this.lblNomeProduto);
            this.Controls.Add(this.txtQuantidadePacotes);
            this.Controls.Add(this.lblTitulo);
            this.Controls.Add(this.txtEntrar);
            this.Name = "usrEmbalarAlterarPacotes";
            this.Size = new System.Drawing.Size(713, 374);
            this.Load += new System.EventHandler(this.usrEmbalarAlterarPacotes_Load);
            ((System.ComponentModel.ISupportInitialize)(this.lblTitulo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEntrar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQuantidadePacotes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCodigoDeBarras)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNomeProduto)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadLabel lblTitulo;
        private Telerik.WinControls.UI.RadButton txtEntrar;
        private Telerik.WinControls.UI.RadSpinEditor txtQuantidadePacotes;
        private Telerik.WinControls.UI.RadTextBox txtCodigoDeBarras;
        private Telerik.WinControls.UI.RadLabel lblNomeProduto;
    }
}
