﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Windows.Forms;
using graodegenteApp.wsEmissao;

namespace graodegenteApp
{
    public partial class frmCarregamentoCorreios : Form
    {
        public string cnpj = "";
        public string numeroNota = "";
        public string volumeAtual = "";
        public string volumeTotal = "";
        public string linkDanfe = "";
        public string linkGnre = "";
        public string linkGnreRecibo = "";
        public int idUsuario = 0;

        public frmCarregamentoCorreios()
        {
            InitializeComponent(); 
        }

        private void frmCarregamentoCorreios_Load(object sender, EventArgs e)
        {
            txtCodigoDeBarras.Focus();
           // BaixaImprimeDanfe("", "http://localhost/graodegenteadmin/gnre.aspx?id=1", "http://localhost/graodegenteadmin/gnrerecibo.aspx?id=1");
        }

        private void txtCodigoDeBarras_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                string codigo = txtCodigoDeBarras.Text;
                if (codigo.Length == 31)
                {
                    cnpj = codigo.Substring(0, 14);
                    numeroNota = codigo.Substring(14, 9);
                    volumeAtual = codigo.Substring(23, 4);
                    volumeTotal = codigo.Substring(27, 4);

                    var emissaoWs = new EmissaoWs();
                    var emissao = emissaoWs.EmitirCorreiosNota(cnpj, Convert.ToInt32(numeroNota), Convert.ToInt32(volumeAtual), idUsuario);

                    if (emissao.sucesso)
                    {
                        lblNota.Text = "Pedido Totalmente Carregado";
                        string path = @"c:\correios\" + emissao.codigo + ".zvp";
                        if (!File.Exists(path))
                        {
                            File.WriteAllText(path, emissao.xml);
                        }

                        ProcessStartInfo startInfo = new ProcessStartInfo();
                        startInfo.FileName = @"C:\ZVP\ZVP.exe";
                        startInfo.Arguments = path;
                        Process.Start(startInfo);


                        linkDanfe = emissao.danfe;
                        BaixaImprimeDanfe(linkDanfe, linkGnre, linkGnreRecibo, 1);
                        lblNota.Visible = true;
                    }
                }
                else
                {
                    txtCodigoDeBarras.Text = txtCodigoDeBarras.Text.Replace(";", "/");

                    int numeroVolume = 1;
                    int.TryParse(txtCodigoDeBarras.Text.Split('-')[1].Split('/')[0], out numeroVolume);

                    int totalVolumes = 1;
                    int.TryParse(txtCodigoDeBarras.Text.Split('-')[1].Split('/')[1], out totalVolumes);

                    int numeroPedidoDigitado = 0;
                    int.TryParse(txtCodigoDeBarras.Text.Split('-')[0], out numeroPedidoDigitado);

                    int pedidoId = Utilidades.retornaIdInterno(numeroPedidoDigitado);

                    var emissaoWs = new EmissaoWs();
                    var emissao = emissaoWs.EmitirCorreios(pedidoId, numeroVolume, totalVolumes);

                    if (emissao.sucesso)
                    {
                        lblNota.Text = "Pedido Totalmente Carregado";
                        string path = @"c:\correios\" + emissao.codigo + ".zvp";
                        if (!File.Exists(path))
                        {
                            File.WriteAllText(path, emissao.xml);
                        }

                        ProcessStartInfo startInfo = new ProcessStartInfo();
                        startInfo.FileName = @"C:\ZVP\ZVP.exe";
                        startInfo.Arguments = path;
                        Process.Start(startInfo);
                    }
                }
                txtCodigoDeBarras.Text = "";
                txtCodigoDeBarras.Focus();
            }
        }


        private void BaixaImprimeDanfe(string danfe, string gnre, string gnrerecibo, int quantidade)
        {
            lblNota.Text = "Imprimindo nota";
            if (!string.IsNullOrEmpty(gnre)) lblNota.Text += "/gnre";
            if (!string.IsNullOrEmpty(gnrerecibo)) lblNota.Text += "/recibo";
            lblNota.Text += " " + numeroNota;

            string enderecoArquivo = "c:\\danfes\\" + cnpj + "_" + numeroNota + ".pdf";

            try
            {
                byte[] binaryData;
                binaryData = Convert.FromBase64String(danfe);
                System.IO.FileStream outFile = new FileStream(enderecoArquivo, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                outFile.Write(binaryData, 0, binaryData.Length);
                outFile.Close();
            }
            catch (Exception)
            {

            }

            try
            {
                PrinterSettings settings = new PrinterSettings();
                string nomeImpressora = settings.PrinterName;
                for (int i = 1; i <= quantidade; i++)
                {
                    clsImprimirEtiqueta.PrintPdf(enderecoArquivo, nomeImpressora);
                }

                var emissaoWs = new EmissaoWs();
                var emissao = emissaoWs.NotaImpressaTnt(cnpj, Convert.ToInt32(numeroNota), idUsuario);
            }
            catch (Exception)
            {

            }
        }
    }
}
