﻿namespace graodegenteApp
{
    partial class usrEmbalarConferirProdutos
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lstProdutosFaltando = new System.Windows.Forms.ListBox();
            this.lblNomeProduto = new Telerik.WinControls.UI.RadLabel();
            this.txtCodigoDeBarras = new Telerik.WinControls.UI.RadTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.lblNomeProduto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCodigoDeBarras)).BeginInit();
            this.SuspendLayout();
            // 
            // lstProdutosFaltando
            // 
            this.lstProdutosFaltando.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lstProdutosFaltando.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstProdutosFaltando.FormattingEnabled = true;
            this.lstProdutosFaltando.ItemHeight = 27;
            this.lstProdutosFaltando.Location = new System.Drawing.Point(52, 153);
            this.lstProdutosFaltando.Name = "lstProdutosFaltando";
            this.lstProdutosFaltando.Size = new System.Drawing.Size(618, 355);
            this.lstProdutosFaltando.TabIndex = 17;
            // 
            // lblNomeProduto
            // 
            this.lblNomeProduto.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblNomeProduto.AutoSize = false;
            this.lblNomeProduto.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNomeProduto.Location = new System.Drawing.Point(48, 48);
            this.lblNomeProduto.Name = "lblNomeProduto";
            this.lblNomeProduto.Size = new System.Drawing.Size(618, 44);
            this.lblNomeProduto.TabIndex = 15;
            this.lblNomeProduto.Text = "Passe o código de barras para conferir os produtos";
            this.lblNomeProduto.Click += new System.EventHandler(this.lblNomeProduto_Click);
            // 
            // txtCodigoDeBarras
            // 
            this.txtCodigoDeBarras.Font = new System.Drawing.Font("Segoe UI", 16F);
            this.txtCodigoDeBarras.Location = new System.Drawing.Point(53, 101);
            this.txtCodigoDeBarras.Name = "txtCodigoDeBarras";
            this.txtCodigoDeBarras.Size = new System.Drawing.Size(230, 34);
            this.txtCodigoDeBarras.TabIndex = 14;
            this.txtCodigoDeBarras.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCodigoDeBarras_KeyDown);
            // 
            // usrEmbalarConferirProdutos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.txtCodigoDeBarras);
            this.Controls.Add(this.lstProdutosFaltando);
            this.Controls.Add(this.lblNomeProduto);
            this.Name = "usrEmbalarConferirProdutos";
            this.Size = new System.Drawing.Size(716, 552);
            this.Load += new System.EventHandler(this.usrEmbalarConferirProdutos_Load);
            ((System.ComponentModel.ISupportInitialize)(this.lblNomeProduto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCodigoDeBarras)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lstProdutosFaltando;
        private Telerik.WinControls.UI.RadLabel lblNomeProduto;
        private Telerik.WinControls.UI.RadTextBox txtCodigoDeBarras;
    }
}
