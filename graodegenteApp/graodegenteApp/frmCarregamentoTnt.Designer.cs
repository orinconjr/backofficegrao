﻿namespace graodegenteApp
{
    partial class frmCarregamentoTnt
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn12 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn13 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn14 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn15 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn16 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            this.txtCodigoDeBarras = new Telerik.WinControls.UI.RadTextBox();
            this.btnNotaImpressa = new Telerik.WinControls.UI.RadButton();
            this.btnReimprimirNota = new Telerik.WinControls.UI.RadButton();
            this.lblCodigo = new Telerik.WinControls.UI.RadLabel();
            this.btnFalhaNota = new System.Windows.Forms.Button();
            this.lblNota = new Telerik.WinControls.UI.RadLabel();
            this.webGnre = new System.Windows.Forms.WebBrowser();
            this.webGnreRecibo = new System.Windows.Forms.WebBrowser();
            this.btnAtualizarEmissoes = new System.Windows.Forms.Button();
            this.grd = new Telerik.WinControls.UI.RadGridView();
            this.label12 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.txtCodigoDeBarras)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnNotaImpressa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnReimprimirNota)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCodigo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNota)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grd.MasterTemplate)).BeginInit();
            this.SuspendLayout();
            // 
            // txtCodigoDeBarras
            // 
            this.txtCodigoDeBarras.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtCodigoDeBarras.Font = new System.Drawing.Font("Segoe UI", 16F);
            this.txtCodigoDeBarras.Location = new System.Drawing.Point(305, 51);
            this.txtCodigoDeBarras.Name = "txtCodigoDeBarras";
            this.txtCodigoDeBarras.Size = new System.Drawing.Size(364, 34);
            this.txtCodigoDeBarras.TabIndex = 16;
            this.txtCodigoDeBarras.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCodigoDeBarras_KeyDown);
            // 
            // btnNotaImpressa
            // 
            this.btnNotaImpressa.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnNotaImpressa.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnNotaImpressa.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNotaImpressa.Location = new System.Drawing.Point(346, 129);
            this.btnNotaImpressa.Name = "btnNotaImpressa";
            // 
            // 
            // 
            this.btnNotaImpressa.RootElement.ControlBounds = new System.Drawing.Rectangle(346, 129, 110, 24);
            this.btnNotaImpressa.Size = new System.Drawing.Size(289, 39);
            this.btnNotaImpressa.TabIndex = 19;
            this.btnNotaImpressa.Text = "NOTA FISCAL IMPRESSA";
            this.btnNotaImpressa.Visible = false;
            this.btnNotaImpressa.Click += new System.EventHandler(this.btnNotaImpressa_Click);
            // 
            // btnReimprimirNota
            // 
            this.btnReimprimirNota.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnReimprimirNota.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnReimprimirNota.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReimprimirNota.Location = new System.Drawing.Point(346, 249);
            this.btnReimprimirNota.Name = "btnReimprimirNota";
            // 
            // 
            // 
            this.btnReimprimirNota.RootElement.ControlBounds = new System.Drawing.Rectangle(346, 249, 110, 24);
            this.btnReimprimirNota.Size = new System.Drawing.Size(289, 39);
            this.btnReimprimirNota.TabIndex = 20;
            this.btnReimprimirNota.Text = "REIMPRIMIR NOTA FISCAL";
            this.btnReimprimirNota.Visible = false;
            this.btnReimprimirNota.Click += new System.EventHandler(this.btnReimprimirNota_Click);
            // 
            // lblCodigo
            // 
            this.lblCodigo.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblCodigo.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblCodigo.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCodigo.Location = new System.Drawing.Point(370, 12);
            this.lblCodigo.Name = "lblCodigo";
            // 
            // 
            // 
            this.lblCodigo.RootElement.ControlBounds = new System.Drawing.Rectangle(370, 12, 100, 18);
            this.lblCodigo.Size = new System.Drawing.Size(232, 33);
            this.lblCodigo.TabIndex = 22;
            this.lblCodigo.Text = "Leia o código de Barras";
            // 
            // btnFalhaNota
            // 
            this.btnFalhaNota.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnFalhaNota.BackColor = System.Drawing.Color.Red;
            this.btnFalhaNota.Location = new System.Drawing.Point(346, 294);
            this.btnFalhaNota.Name = "btnFalhaNota";
            this.btnFalhaNota.Size = new System.Drawing.Size(289, 34);
            this.btnFalhaNota.TabIndex = 23;
            this.btnFalhaNota.Text = "Impossível Imprimir Nota";
            this.btnFalhaNota.UseVisualStyleBackColor = false;
            this.btnFalhaNota.Visible = false;
            this.btnFalhaNota.Click += new System.EventHandler(this.btnFalhaNota_Click);
            // 
            // lblNota
            // 
            this.lblNota.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblNota.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblNota.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNota.Location = new System.Drawing.Point(388, 91);
            this.lblNota.Name = "lblNota";
            // 
            // 
            // 
            this.lblNota.RootElement.ControlBounds = new System.Drawing.Rectangle(388, 91, 100, 18);
            this.lblNota.Size = new System.Drawing.Size(227, 33);
            this.lblNota.TabIndex = 24;
            this.lblNota.Text = "Imprimindo Nota 2504";
            this.lblNota.Visible = false;
            // 
            // webGnre
            // 
            this.webGnre.Location = new System.Drawing.Point(958, 249);
            this.webGnre.MinimumSize = new System.Drawing.Size(20, 20);
            this.webGnre.Name = "webGnre";
            this.webGnre.Size = new System.Drawing.Size(250, 96);
            this.webGnre.TabIndex = 25;
            this.webGnre.Visible = false;
            this.webGnre.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.webGnre_DocumentCompleted);
            // 
            // webGnreRecibo
            // 
            this.webGnreRecibo.Location = new System.Drawing.Point(961, 437);
            this.webGnreRecibo.MinimumSize = new System.Drawing.Size(20, 20);
            this.webGnreRecibo.Name = "webGnreRecibo";
            this.webGnreRecibo.Size = new System.Drawing.Size(250, 96);
            this.webGnreRecibo.TabIndex = 26;
            this.webGnreRecibo.Visible = false;
            this.webGnreRecibo.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.webGnreRecibo_DocumentCompleted);
            // 
            // btnAtualizarEmissoes
            // 
            this.btnAtualizarEmissoes.Location = new System.Drawing.Point(119, 355);
            this.btnAtualizarEmissoes.Name = "btnAtualizarEmissoes";
            this.btnAtualizarEmissoes.Size = new System.Drawing.Size(184, 23);
            this.btnAtualizarEmissoes.TabIndex = 32;
            this.btnAtualizarEmissoes.Text = "Atualizar Emissões Pendentes";
            this.btnAtualizarEmissoes.UseVisualStyleBackColor = true;
            this.btnAtualizarEmissoes.Click += new System.EventHandler(this.btnAtualizarEmissoes_Click);
            // 
            // grd
            // 
            this.grd.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grd.Location = new System.Drawing.Point(8, 384);
            // 
            // 
            // 
            this.grd.MasterTemplate.AllowAddNewRow = false;
            this.grd.MasterTemplate.AllowDeleteRow = false;
            this.grd.MasterTemplate.AllowEditRow = false;
            this.grd.MasterTemplate.AutoGenerateColumns = false;
            this.grd.MasterTemplate.AutoSizeColumnsMode = Telerik.WinControls.UI.GridViewAutoSizeColumnsMode.Fill;
            gridViewTextBoxColumn9.FieldName = "idPedidoEnvio";
            gridViewTextBoxColumn9.HeaderText = "ID";
            gridViewTextBoxColumn9.Name = "idPedidoEnvio";
            gridViewTextBoxColumn9.Width = 84;
            gridViewTextBoxColumn10.FieldName = "pedidoId";
            gridViewTextBoxColumn10.HeaderText = "ID Pedido";
            gridViewTextBoxColumn10.Name = "pedidoId";
            gridViewTextBoxColumn10.Width = 84;
            gridViewTextBoxColumn11.FieldName = "enderecos";
            gridViewTextBoxColumn11.HeaderText = "Enderecos";
            gridViewTextBoxColumn11.Multiline = true;
            gridViewTextBoxColumn11.Name = "enderecos";
            gridViewTextBoxColumn11.Width = 312;
            gridViewTextBoxColumn11.WrapText = true;
            gridViewTextBoxColumn12.Expression = "";
            gridViewTextBoxColumn12.FieldName = "endNomeDoDestinatario";
            gridViewTextBoxColumn12.HeaderText = "Nome";
            gridViewTextBoxColumn12.Name = "endNomeDoDestinatario";
            gridViewTextBoxColumn12.Width = 81;
            gridViewTextBoxColumn13.FieldName = "endCidade";
            gridViewTextBoxColumn13.HeaderText = "Cidade";
            gridViewTextBoxColumn13.Name = "endCidade";
            gridViewTextBoxColumn13.Width = 129;
            gridViewTextBoxColumn14.FieldName = "endEstado";
            gridViewTextBoxColumn14.HeaderText = "Estado";
            gridViewTextBoxColumn14.Name = "endEstado";
            gridViewTextBoxColumn14.Width = 129;
            gridViewTextBoxColumn15.FieldName = "totalPacotes";
            gridViewTextBoxColumn15.HeaderText = "Volumes";
            gridViewTextBoxColumn15.Name = "totalPacotes";
            gridViewTextBoxColumn15.Width = 48;
            gridViewTextBoxColumn16.FieldName = "dataFimEmbalagem";
            gridViewTextBoxColumn16.HeaderText = "Data";
            gridViewTextBoxColumn16.Name = "dataFimEmbalagem";
            gridViewTextBoxColumn16.Width = 59;
            this.grd.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn9,
            gridViewTextBoxColumn10,
            gridViewTextBoxColumn11,
            gridViewTextBoxColumn12,
            gridViewTextBoxColumn13,
            gridViewTextBoxColumn14,
            gridViewTextBoxColumn15,
            gridViewTextBoxColumn16});
            this.grd.MasterTemplate.EnableFiltering = true;
            this.grd.MasterTemplate.EnableGrouping = false;
            this.grd.MasterTemplate.MultiSelect = true;
            this.grd.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.grd.Name = "grd";
            this.grd.Size = new System.Drawing.Size(940, 283);
            this.grd.TabIndex = 31;
            this.grd.Text = "s";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(5, 360);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(108, 13);
            this.label12.TabIndex = 30;
            this.label12.Text = "Emissões Pendentes:";
            // 
            // frmCarregamentoTnt
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Azure;
            this.ClientSize = new System.Drawing.Size(960, 679);
            this.Controls.Add(this.btnAtualizarEmissoes);
            this.Controls.Add(this.grd);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.webGnreRecibo);
            this.Controls.Add(this.webGnre);
            this.Controls.Add(this.lblNota);
            this.Controls.Add(this.btnFalhaNota);
            this.Controls.Add(this.lblCodigo);
            this.Controls.Add(this.btnReimprimirNota);
            this.Controls.Add(this.btnNotaImpressa);
            this.Controls.Add(this.txtCodigoDeBarras);
            this.Name = "frmCarregamentoTnt";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Emissão TNT";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmCarregamentoTnt_Load);
            ((System.ComponentModel.ISupportInitialize)(this.txtCodigoDeBarras)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnNotaImpressa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnReimprimirNota)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCodigo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNota)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grd.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grd)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadTextBox txtCodigoDeBarras;
        private Telerik.WinControls.UI.RadButton btnNotaImpressa;
        private Telerik.WinControls.UI.RadButton btnReimprimirNota;
        private Telerik.WinControls.UI.RadLabel lblCodigo;
        private System.Windows.Forms.Button btnFalhaNota;
        private Telerik.WinControls.UI.RadLabel lblNota;
        private System.Windows.Forms.WebBrowser webGnre;
        private System.Windows.Forms.WebBrowser webGnreRecibo;
        private System.Windows.Forms.Button btnAtualizarEmissoes;
        private Telerik.WinControls.UI.RadGridView grd;
        private System.Windows.Forms.Label label12;
    }
}