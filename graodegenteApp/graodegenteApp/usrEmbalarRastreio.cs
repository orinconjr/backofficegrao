﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace graodegenteApp
{
    public partial class usrEmbalarRastreio : UserControl
    {
        private int totalVolumes = 0;
        private List<wsEmbalagem.rastreiosGravar> listaRastreios = new List<wsEmbalagem.rastreiosGravar>(); 
        public usrEmbalarRastreio()
        {
            InitializeComponent();
        }

        public class rastreiosGravar
        {
            public string rastreio { get; set; }
        }
        private void usrEmbalarRastreio_Load(object sender, EventArgs e)
        {
            var formPrincipal = (this.Parent.Parent as frmPrincipal);
            var embalarWs = new wsEmbalagem.EmbalagemWs();
            var pacotes = embalarWs.RetornaQuantidadePacotes(formPrincipal.idUsuarioExpedicao, formPrincipal.idPedidoEmbalagem);
            if (pacotes == 0)
            {
                MessageBox.Show("Falha ao carregar quantidade de pacotes. Por favor, tente novamente");
                formPrincipal.carregaEmbalarLogin();
            }
            else
            {
                totalVolumes = pacotes;
            }
            lblNumeroPedido.Text = Utilidades.retornaIdCliente(formPrincipal.idPedidoEmbalagem).ToString();
        }

        private void btnConfirmar_Click(object sender, EventArgs e)
        {
            if (txtRastreio.Text == "")
            {
                txtRastreio.Focus();
                MessageBox.Show("Código de Rastreio Inválido");
                return;
            }
            if (!txtRastreio.Text.ToLower().Contains("br"))
            {
                txtRastreio.Text = "";
                txtRastreio.Focus();
                MessageBox.Show("Código de Rastreio Inválido");
                return;
            }
            listaRastreios.Add(new wsEmbalagem.rastreiosGravar() { rastreio = txtRastreio.Text });
            txtRastreio.Text = "";
            txtRastreio.Focus();
            if (listaRastreios.Count == totalVolumes)
            {
                var formPrincipal = (this.Parent.Parent as frmPrincipal);
                var embalarWs = new wsEmbalagem.EmbalagemWs();
                var gravarRastreio = embalarWs.GravaRastreios(formPrincipal.idUsuarioExpedicao, formPrincipal.idPedidoEmbalagem, listaRastreios.ToArray());
                listaRastreios = new List<wsEmbalagem.rastreiosGravar>(); 
                formPrincipal.carregaEmbalarLogin();
            }
            else
            {
                lblRastreio.Text = "Rastreio volume " + (listaRastreios.Count + 1) + ":";
                txtRastreio.Text = "";
                txtRastreio.Focus();
            }
        }

        private void txtRastreio_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnConfirmar.PerformClick();
            }
        }
    }
}
