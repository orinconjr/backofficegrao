﻿namespace graodegenteApp
{
    partial class frmConsultarRomaneio
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.txtIdPedidoFornecedor = new Telerik.WinControls.UI.RadTextBox();
            this.btnConsultar = new Telerik.WinControls.UI.RadButton();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIdPedidoFornecedor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnConsultar)).BeginInit();
            this.SuspendLayout();
            // 
            // radLabel2
            // 
            this.radLabel2.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel2.Location = new System.Drawing.Point(12, 12);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(121, 37);
            this.radLabel2.TabIndex = 14;
            this.radLabel2.Text = "Romaneio";
            // 
            // txtIdPedidoFornecedor
            // 
            this.txtIdPedidoFornecedor.Font = new System.Drawing.Font("Segoe UI", 16F);
            this.txtIdPedidoFornecedor.Location = new System.Drawing.Point(12, 51);
            this.txtIdPedidoFornecedor.Name = "txtIdPedidoFornecedor";
            this.txtIdPedidoFornecedor.Size = new System.Drawing.Size(230, 34);
            this.txtIdPedidoFornecedor.TabIndex = 13;
            // 
            // btnConsultar
            // 
            this.btnConsultar.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConsultar.Location = new System.Drawing.Point(12, 101);
            this.btnConsultar.Name = "btnConsultar";
            this.btnConsultar.Size = new System.Drawing.Size(230, 39);
            this.btnConsultar.TabIndex = 12;
            this.btnConsultar.Text = "CONSULTAR";
            this.btnConsultar.Click += new System.EventHandler(this.btnConsultar_Click);
            // 
            // frmConsultarRomaneio
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Azure;
            this.ClientSize = new System.Drawing.Size(254, 175);
            this.Controls.Add(this.radLabel2);
            this.Controls.Add(this.txtIdPedidoFornecedor);
            this.Controls.Add(this.btnConsultar);
            this.Name = "frmConsultarRomaneio";
            this.Text = "frmConsultarRomaneio";
            this.Load += new System.EventHandler(this.frmConsultarRomaneio_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIdPedidoFornecedor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnConsultar)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadTextBox txtIdPedidoFornecedor;
        private Telerik.WinControls.UI.RadButton btnConsultar;
    }
}