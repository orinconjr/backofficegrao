﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace graodegenteApp
{
    public partial class frmImprimirEtiquetaAvulsa : Form
    {
        public int idUsuario = 0;
        public frmImprimirEtiquetaAvulsa()
        {
            InitializeComponent();
        }

        private void btnImprimir_Click(object sender, EventArgs e)
        {
            if (txtIdItemPedido.Text.Contains("-"))
            {
                int idInicial = Convert.ToInt32(txtIdItemPedido.Text.Split('-')[0]);
                int idFinal = Convert.ToInt32(txtIdItemPedido.Text.Split('-')[1]);

                for (int i = idInicial; i <= idFinal; i++)
                {
                    imprimePedido(i);
                }
                txtIdItemPedido.Text = "";
                txtIdItemPedido.Focus();
                return;
            }
            int idPedidoFornecedorItem = 0;

            int.TryParse(txtIdItemPedido.Text.Trim(), out idPedidoFornecedorItem);

            if (idPedidoFornecedorItem == 0)
            {
                MessageBox.Show("Número de pedido incorreto");
                return;
            }

            imprimePedido(idPedidoFornecedorItem);
        }

        private void imprimePedido(int idPedido)
        {
            var pedidosWs = new wsPedidosFornecedor.PedidosFornecedorWs();
            var detalheProduto = pedidosWs.RetornaDetalhesItem(idPedido, clsConfiguracao.ChaveWs, true);

            clsImprimirEtiqueta.GerarEtiqueta(idPedido.ToString(), detalheProduto.idPedidoFornecedor.ToString(), detalheProduto.fornecedorNome, detalheProduto.numeroItem.ToString(), detalheProduto.totalItens.ToString(), detalheProduto.produtoIdDaEmpresa, detalheProduto.complementoIdDaEmpresa, detalheProduto.produtoNome, detalheProduto.codigoEtiqueta, detalheProduto.produtoId.ToString());
            pedidosWs.InsereLogEtiqueta(idPedido, idUsuario, "Etiqueta avulsa impressa");
            txtIdItemPedido.Text = "";
            txtIdItemPedido.Focus();
        }
        private void txtIdItemPedido_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnImprimir.PerformClick();
            }
        }

        private void frmImprimirEtiquetaAvulsa_Load(object sender, EventArgs e)
        {
            txtIdItemPedido.Focus();
        }
    }
}
