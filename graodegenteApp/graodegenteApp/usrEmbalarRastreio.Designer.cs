﻿namespace graodegenteApp
{
    partial class usrEmbalarRastreio
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblRastreio = new Telerik.WinControls.UI.RadLabel();
            this.txtRastreio = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.lblNumeroPedido = new Telerik.WinControls.UI.RadLabel();
            this.btnConfirmar = new Telerik.WinControls.UI.RadButton();
            ((System.ComponentModel.ISupportInitialize)(this.lblRastreio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRastreio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNumeroPedido)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnConfirmar)).BeginInit();
            this.SuspendLayout();
            // 
            // lblRastreio
            // 
            this.lblRastreio.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblRastreio.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRastreio.Location = new System.Drawing.Point(189, 123);
            this.lblRastreio.Name = "lblRastreio";
            this.lblRastreio.Size = new System.Drawing.Size(185, 33);
            this.lblRastreio.TabIndex = 20;
            this.lblRastreio.Text = "Rastreio volume 1:";
            // 
            // txtRastreio
            // 
            this.txtRastreio.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtRastreio.Font = new System.Drawing.Font("Segoe UI", 16F);
            this.txtRastreio.Location = new System.Drawing.Point(161, 157);
            this.txtRastreio.Name = "txtRastreio";
            this.txtRastreio.Size = new System.Drawing.Size(230, 34);
            this.txtRastreio.TabIndex = 21;
            this.txtRastreio.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtRastreio_KeyDown);
            // 
            // radLabel2
            // 
            this.radLabel2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radLabel2.Font = new System.Drawing.Font("Segoe UI", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel2.Location = new System.Drawing.Point(155, 23);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(246, 51);
            this.radLabel2.TabIndex = 19;
            this.radLabel2.Text = "Enviar por: PAC";
            // 
            // lblNumeroPedido
            // 
            this.lblNumeroPedido.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblNumeroPedido.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumeroPedido.Location = new System.Drawing.Point(181, 69);
            this.lblNumeroPedido.Name = "lblNumeroPedido";
            this.lblNumeroPedido.Size = new System.Drawing.Size(192, 33);
            this.lblNumeroPedido.TabIndex = 21;
            this.lblNumeroPedido.Text = "Número do Pedido";
            // 
            // btnConfirmar
            // 
            this.btnConfirmar.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnConfirmar.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConfirmar.Location = new System.Drawing.Point(161, 223);
            this.btnConfirmar.Name = "btnConfirmar";
            this.btnConfirmar.Size = new System.Drawing.Size(230, 39);
            this.btnConfirmar.TabIndex = 22;
            this.btnConfirmar.Text = "CONFIRMAR";
            this.btnConfirmar.Click += new System.EventHandler(this.btnConfirmar_Click);
            // 
            // usrEmbalarRastreio
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Azure;
            this.Controls.Add(this.btnConfirmar);
            this.Controls.Add(this.lblNumeroPedido);
            this.Controls.Add(this.lblRastreio);
            this.Controls.Add(this.txtRastreio);
            this.Controls.Add(this.radLabel2);
            this.Name = "usrEmbalarRastreio";
            this.Size = new System.Drawing.Size(542, 426);
            this.Load += new System.EventHandler(this.usrEmbalarRastreio_Load);
            ((System.ComponentModel.ISupportInitialize)(this.lblRastreio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRastreio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNumeroPedido)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnConfirmar)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadLabel lblRastreio;
        private Telerik.WinControls.UI.RadLabel lblNumeroPedido;
        private Telerik.WinControls.UI.RadTextBox txtRastreio;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadButton btnConfirmar;
    }
}
