﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace graodegenteApp
{
    public partial class usrEmbalarConferirPacotes : UserControl
    {
        string etiquetaNaoPercenteAoPedido = string.Empty;
        int totalPacotes = 0;
        List<int> volumesConferidos = new List<int>();
        int idPedidoEmbalagem = 0;

        public usrEmbalarConferirPacotes()
        {
            InitializeComponent();
        }

        private void usrEmbalarConferirPacotes_Load(object sender, EventArgs e)
        {
            var formPrincipal = (this.Parent.Parent as frmPrincipal);
            var embalagemws = new wsEmbalagem.EmbalagemWs();
            totalPacotes = embalagemws.RetornaVolumesCd1(formPrincipal.idUsuarioExpedicao, formPrincipal.idPedidoEmbalagem);
            idPedidoEmbalagem = formPrincipal.idPedidoEmbalagem;
            txtCodigoDeBarras.Focus();
        }

        private void lblNomeProduto_Click(object sender, EventArgs e)
        {

        }


        private void txtCodigoDeBarras_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && txtCodigoDeBarras.Text != "")
            {
                try
                {
                    string numero = txtCodigoDeBarras.Text.Replace(";", "-").Replace(";", "-");
                    var pedidoIdCliente = numero.Split('-')[0];
                    int idPedido = Utilidades.retornaIdInterno(Convert.ToInt32(pedidoIdCliente));
                    int volume = Convert.ToInt32(numero.Split('-')[1].Split('/')[0]);

                    if (idPedido != idPedidoEmbalagem)
                    {
                        lblNomeProduto.Text = "Pedido Incorreto";
                        txtCodigoDeBarras.Text = "";
                        txtCodigoDeBarras.Focus();
                        return;
                    }

                    if (!volumesConferidos.Any(x => x == volume))
                    {
                        volumesConferidos.Add(volume);
                    }

                    lblNomeProduto.Text = "Volume Carregado " + volumesConferidos.Count + "/" + totalPacotes;

                    if (volumesConferidos.Count == totalPacotes)
                    {
                        var formPrincipal = (this.Parent.Parent as frmPrincipal);
                        var embalagemws = new wsEmbalagem.EmbalagemWs();
                        embalagemws.FinalizaEnvio(formPrincipal.idUsuarioExpedicao, formPrincipal.idPedidoEmbalagem);
                        lblNomeProduto.Text = "Pedido Finalizado";
                        txtCodigoDeBarras.Visible = false;
                    }
                    txtCodigoDeBarras.Text = "";
                    txtCodigoDeBarras.Focus();
                }
                catch(Exception ex)
                {
                    lblNomeProduto.Text = "Pedido Incorreto";
                    txtCodigoDeBarras.Text = "";
                    txtCodigoDeBarras.Focus();
                    return;
                }
            }
        }

        private void btnConfirmar_Click(object sender, EventArgs e)
        {
            var formPrincipal = (this.Parent.Parent as frmPrincipal);
            formPrincipal.carregaEmbalarLogin();
        }
    }
}
