﻿namespace graodegenteApp
{
    partial class frmImprimirEtiquetasRomaneio
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.txtIdPedidoFornecedor = new Telerik.WinControls.UI.RadTextBox();
            this.btnImprimir = new Telerik.WinControls.UI.RadButton();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.txtEtiquetaInicial = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.txtEtiquetaFinal = new Telerik.WinControls.UI.RadTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIdPedidoFornecedor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnImprimir)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEtiquetaInicial)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEtiquetaFinal)).BeginInit();
            this.SuspendLayout();
            // 
            // radLabel2
            // 
            this.radLabel2.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel2.Location = new System.Drawing.Point(12, 12);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(121, 37);
            this.radLabel2.TabIndex = 11;
            this.radLabel2.Text = "Romaneio";
            // 
            // txtIdPedidoFornecedor
            // 
            this.txtIdPedidoFornecedor.Font = new System.Drawing.Font("Segoe UI", 16F);
            this.txtIdPedidoFornecedor.Location = new System.Drawing.Point(12, 51);
            this.txtIdPedidoFornecedor.Name = "txtIdPedidoFornecedor";
            this.txtIdPedidoFornecedor.Size = new System.Drawing.Size(230, 34);
            this.txtIdPedidoFornecedor.TabIndex = 10;
            // 
            // btnImprimir
            // 
            this.btnImprimir.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnImprimir.Location = new System.Drawing.Point(12, 263);
            this.btnImprimir.Name = "btnImprimir";
            this.btnImprimir.Size = new System.Drawing.Size(230, 39);
            this.btnImprimir.TabIndex = 9;
            this.btnImprimir.Text = "IMPRIMIR";
            this.btnImprimir.Click += new System.EventHandler(this.btnImprimir_Click);
            // 
            // radLabel1
            // 
            this.radLabel1.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel1.Location = new System.Drawing.Point(12, 91);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(73, 37);
            this.radLabel1.TabIndex = 13;
            this.radLabel1.Text = "Inicial";
            // 
            // txtEtiquetaInicial
            // 
            this.txtEtiquetaInicial.Font = new System.Drawing.Font("Segoe UI", 16F);
            this.txtEtiquetaInicial.Location = new System.Drawing.Point(12, 130);
            this.txtEtiquetaInicial.Name = "txtEtiquetaInicial";
            this.txtEtiquetaInicial.Size = new System.Drawing.Size(230, 34);
            this.txtEtiquetaInicial.TabIndex = 12;
            // 
            // radLabel3
            // 
            this.radLabel3.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel3.Location = new System.Drawing.Point(12, 170);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(61, 37);
            this.radLabel3.TabIndex = 15;
            this.radLabel3.Text = "Final";
            // 
            // txtEtiquetaFinal
            // 
            this.txtEtiquetaFinal.Font = new System.Drawing.Font("Segoe UI", 16F);
            this.txtEtiquetaFinal.Location = new System.Drawing.Point(12, 209);
            this.txtEtiquetaFinal.Name = "txtEtiquetaFinal";
            this.txtEtiquetaFinal.Size = new System.Drawing.Size(230, 34);
            this.txtEtiquetaFinal.TabIndex = 14;
            // 
            // frmImprimirEtiquetasRomaneio
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Azure;
            this.ClientSize = new System.Drawing.Size(256, 320);
            this.Controls.Add(this.radLabel3);
            this.Controls.Add(this.txtEtiquetaFinal);
            this.Controls.Add(this.radLabel1);
            this.Controls.Add(this.txtEtiquetaInicial);
            this.Controls.Add(this.radLabel2);
            this.Controls.Add(this.txtIdPedidoFornecedor);
            this.Controls.Add(this.btnImprimir);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "frmImprimirEtiquetasRomaneio";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmImprimirEtiquetasRomaneio";
            this.Load += new System.EventHandler(this.frmImprimirEtiquetasRomaneio_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIdPedidoFornecedor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnImprimir)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEtiquetaInicial)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEtiquetaFinal)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadTextBox txtIdPedidoFornecedor;
        private Telerik.WinControls.UI.RadButton btnImprimir;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadTextBox txtEtiquetaInicial;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadTextBox txtEtiquetaFinal;

    }
}