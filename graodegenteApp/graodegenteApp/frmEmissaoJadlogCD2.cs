﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace graodegenteApp
{
    public partial class frmEmissaoJadlogCD2 : Form
    {
        public frmEmissaoJadlogCD2()
        {
            InitializeComponent();
        }
        public int idPedidoEnvio = 0;
        public wsEmissao.RetornoEmissao Emissao;

        private void frmEmissaoJadlogCD2_Load(object sender, EventArgs e)
        {
            pnDados.Visible = false;
            AtualizaEmissoes();
        }


        private void btnCarregarEmissao_Click(object sender, EventArgs e)
        {
            if (idPedidoEnvio == 0)
            {
                CarregarEmissao(0, 0);
            }
            else
            {
                btnCarregarEmissao.Text = "Gravar Rastreio";
                var rastreio = new frmEmissaoGravarRastreio();
                rastreio.idPedidoEnvio = idPedidoEnvio;
                var retorno = rastreio.ShowDialog();
                if (retorno == DialogResult.OK)
                {
                    btnCarregarEmissao.Text = "Carregar Próxima Emissão";
                    pnDados.Visible = false;
                    AtualizaEmissoes();
                    idPedidoEnvio = 0;
                    btnImprimirNota.Visible = false;
                }

            }

        }

        private void CarregarEmissao(int pedidoEnvio, int idPedido)
        {
            var serviceEmissao = new wsEmissao.EmissaoWs();
            var emissao = serviceEmissao.RetornaProximaEmissao("glmp152029", 2, pedidoEnvio, idPedido);
            if (emissao != null)
            {
                Emissao = emissao;
                pnDados.Visible = true;

                idPedidoEnvio = emissao.idPedidoEnvio;
                txtTipoEnvio.Text = emissao.formaDeEnvio.ToLower();
                txtCpf.Text = emissao.clienteCPFCNPJ.Trim().Replace("-", "");
                txtDestinatario.Text = removeCaracteresEspeciaisDeixaEspaco(limpaStringDeixaEspaco(emissao.endNomeDoDestinatario)).ToLower();
                txtRua.Text = removeCaracteresEspeciaisDeixaEspaco(limpaStringDeixaEspaco(emissao.endRua)).ToLower() + " - " + removeCaracteresEspeciaisDeixaEspaco(limpaStringDeixaEspaco(emissao.endNumero)).ToLower() + " - " + removeCaracteresEspeciaisDeixaEspaco(limpaStringDeixaEspaco(emissao.endComplemento)).ToLower();

                txtCidade.Text = removeCaracteresEspeciaisDeixaEspaco(limpaStringDeixaEspaco(emissao.endCidade)).ToLower();
                txtBairro.Text = removeCaracteresEspeciaisDeixaEspaco(limpaStringDeixaEspaco(emissao.endBairro)).ToLower();
                txtUf.Text = removeCaracteresEspeciaisDeixaEspaco(limpaStringDeixaEspaco(emissao.endEstado)).ToLower();
                txtCep.Text = emissao.endCep.Trim().Replace("-", "");

                #region Fone

                var telsemespaco = emissao.clienteFoneResidencial.Replace(" ", "");
                var telspli = telsemespaco.Split(')');
                string ddd = "";
                string telefone = "";
                if (telspli.Length > 1)
                {
                    ddd = telspli[0].Replace("(", "").Replace(")", "").Trim();
                    telefone = telspli[1].Replace("-", "").Trim();
                }
                txtDDD.Text = ddd;
                txtTelefone.Text = telefone;

                #endregion

                txtPacotes.Text = emissao.totalPacotes.ToString();

                for (int i = 1; i <= 20; i++)
                {
                    string nomeControle = "txtPeso" + i;
                    var controle = this.Controls.Find(nomeControle, true).FirstOrDefault() as TextBox;
                    if (controle != null)
                    {
                        controle.Text = "";
                        controle.Visible = false;
                    }
                }

                for (int i = 1; i <= emissao.totalPacotes; i++)
                {
                    string nomeControle = "txtPeso" + i;
                    var controle = this.Controls.Find(nomeControle, true).FirstOrDefault() as TextBox;
                    if (controle != null)
                    {
                        controle.Text = emissao.pesos[i - 1].ToString();
                        controle.Visible = true;
                    }

                }

                if (emissao.nota > 0)
                {
                    btnImprimirNota.Visible = true;
                }
                else
                {
                    btnImprimirNota.Visible = false;
                }
                btnCarregarEmissao.Text = "Gravar Rastreio";
                ImprimeEtiquetasEmissao();
            }
        }
        private void pnDados_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnImprimirNota_Click(object sender, EventArgs e)
        {
            var nota = new frmEmissaoNota();
            nota.link = Emissao.danfe;
            nota.ShowDialog();
        }

        private void ImprimeEtiquetasEmissao()
        {
            foreach (var produto in Emissao.produtos)
            {
                var etiqueta = new StringBuilder();
                etiqueta.Append("N");
                etiqueta.Append(Environment.NewLine);
                etiqueta.Append("q600");
                etiqueta.Append(Environment.NewLine);
                etiqueta.Append("Q800,26");
                etiqueta.Append(Environment.NewLine);
                int linhaAtual = 150;
                int incremento = 25;
                int limiteLinha = 35;
                int limiteLinha2 = 35;



                string nome = "";
                string produtoCompleto = clsImprimirEtiqueta.limpaStringDeixaEspaco(produto);
                produtoCompleto = produtoCompleto.Replace("    ", " ").Replace("   ", " ").Replace("  ", " ");
                int nomeLidos = 0;
                var arrayEndereco = produtoCompleto.Split(' ');
                foreach (var arrayNome in arrayEndereco)
                {
                    nomeLidos++;
                    bool adicionado = false;
                    if (!string.IsNullOrEmpty(nome))
                    {
                        nome += " ";
                    }
                    if (nome.Length + arrayNome.Length < limiteLinha)
                    {
                        nome += arrayNome;
                    }
                    else
                    {
                        etiqueta.AppendFormat("A50,{0},0,4,1,1,N,\"{1}\"", linhaAtual, nome.Trim().ToUpper());
                        etiqueta.Append(Environment.NewLine);
                        nome = arrayNome;
                        linhaAtual += incremento;
                        adicionado = true;
                    }
                    if (nomeLidos == arrayEndereco.Length && adicionado == false)
                    {
                        etiqueta.AppendFormat("A50,{0},0,4,1,1,N,\"{1}\"", linhaAtual, nome.Trim().ToUpper());
                        etiqueta.Append(Environment.NewLine);
                        nome = arrayNome;
                        linhaAtual += incremento;
                    }
                }


                etiqueta.AppendFormat("A50,{0},0,4,1,1,N,\"{1}\"", linhaAtual, "Destinatario:");
                etiqueta.Append(Environment.NewLine);
                linhaAtual += incremento;
                string nomeformatado = clsImprimirEtiqueta.limpaStringDeixaEspaco(Emissao.endNomeDoDestinatario + " ");
                nomeformatado = nomeformatado.ToLower().Replace("-", " ");
                etiqueta.AppendFormat("A50,{0},0,4,1,1,N,\"{1}\"", linhaAtual, nomeformatado.Trim().ToUpper());
                etiqueta.Append(Environment.NewLine);
                linhaAtual += incremento;

                string telefone = "";
                if (!string.IsNullOrEmpty(Emissao.clienteFoneResidencial)) telefone += "Tel.: " + Emissao.clienteFoneResidencial;
                if (!string.IsNullOrEmpty(telefone))
                {
                    linhaAtual += 10;
                    etiqueta.AppendFormat("A50,{0},0,3,1,1,N,\"{1}\"", linhaAtual, telefone.Trim().ToUpper());
                    etiqueta.Append(Environment.NewLine);
                    linhaAtual += incremento;
                }


                etiqueta.Append("P1,1");
                etiqueta.Append(Environment.NewLine);
                var etiquetaString = etiqueta.ToString();
                //MessageBox.Show(etiquetaString);
                //RawPrinterHelper.SendStringToPrinter("zebra", etiqueta.ToString());
                RawPrinterHelper.SendStringToCom("COM6", etiqueta.ToString());
                Thread.Sleep(1000);
            }
        }

        private void AtualizaEmissoes()
        {
            EmiteNotas();
            AtualizaListaEmissoes();
            ListEmissoes();
            ListNotasProcessamento();
        }

        private void ListNotasProcessamento()
        {
            ThreadPool.QueueUserWorkItem(ListNotasProcessamento);
        }

        private void ListNotasProcessamento(object parametros)
        {
            var serviceEmissao = new wsEmissao.EmissaoWs();
            var notasPendentes = serviceEmissao.ListaNotasProcessamento("glmp152029", 2);
            
            grdNotas.Invoke((MethodInvoker)delegate
            {
                grdNotas.DataSource = notasPendentes.ToList();
                grdNotas.BestFitColumns();
            });
        }
        private void ListEmissoes()
        {
            ThreadPool.QueueUserWorkItem(ListEmissoes);
        }

        private void ListEmissoes(object parametros)
        {
            try
            {
                var serviceEmissao = new wsEmissao.EmissaoWs();
                var emissoesPendentes = serviceEmissao.ListaEmissoes("glmp152029", 2);

                grd.Invoke((MethodInvoker) delegate
                {
                    grd.DataSource = emissoesPendentes.ToList();
                });
            }
            catch (Exception)
            {

            }
        }
        private void EmiteNotas()
        {
            ThreadPool.QueueUserWorkItem(EmiteNotas);
        }

        private void EmiteNotas(object parametros)
        {
            try
            {
                var serviceEmissao = new wsEmissao.EmissaoWs();
                serviceEmissao.EmiteNotas("glmp152029");
            }
            catch (Exception)
            {

            }
        }

        private void AtualizaListaEmissoes()
        {
            ThreadPool.QueueUserWorkItem(AtualizaListaEmissoes);
        }

        private void AtualizaListaEmissoes(object parametros)
        {
            try
            {
                var serviceEmissao = new wsEmissao.EmissaoWs();
                var emissoes = serviceEmissao.ContaEmissoesPendentes("glmp152029", 2);
                lblContagemEmissoes.Invoke((MethodInvoker) delegate
                {
                    lblContagemEmissoes.Text = "Emissões Pendentes: " + emissoes;
                });
            }
            catch (Exception)
            {

            }
        }

        private void grd_Click(object sender, EventArgs e)
        {

        }

        private void grd_MouseDoubleClick(object sender, MouseEventArgs e)
        {
           var confirmacao = MessageBox.Show("Deseja carregar o pedido selecionado?", "Confirmacao", MessageBoxButtons.YesNo);
            if (confirmacao == DialogResult.Yes)
            {
                for (int i = 0; i < grd.SelectedRows.Count; i++)
                {
                    var id = grd.SelectedRows[i].Cells[0].Value;
                    CarregarEmissao(Convert.ToInt32(id), 0);
                }
            }
        }
        public static string removeCaracteresEspeciaisDeixaEspaco(string inputString)
        {
            StringBuilder sb = new StringBuilder();
            foreach (char c in inputString)
            {
                if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') | c == '.' || c == '_' || c == '-' || c == ' ')
                {
                    sb.Append(c);
                }
            }
            return sb.ToString();
        }
        public static string limpaStringDeixaEspaco(string texto)
        {
            string caracteresEspeciais = "ÄÅÁÂÀÃäáâàãÉÊËÈéêëèÍÎÏÌíîïìÖÓÔÒÕöóôòõÜÚÛüúûùÇç/`~!@#$%^&*()*+;:',<.>?|";
            string caracteresNormais = "AAAAAAaaaaaEEEEeeeeIIIIiiiiOOOOOoooooUUUuuuuCc------------------------";

            for (int i = 0; i < caracteresEspeciais.Length; i++)
            {
                try
                {
                    texto = texto.Replace(caracteresEspeciais[i].ToString(), caracteresNormais[i].ToString());
                    texto = texto.Replace("\"", "");
                    texto = texto.Replace("'", "");
                }
                catch (Exception ex) { }
            }
            return texto;
        }
    }
}
