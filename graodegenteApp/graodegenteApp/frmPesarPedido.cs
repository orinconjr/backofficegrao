﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace graodegenteApp
{
    public partial class frmPesarPedido : Form
    {
        public int idUsuario = 0;
        private int pedidoId = 0;
        private wsEmissao.RetornoChecarVolumePesar volumesPesar = new wsEmissao.RetornoChecarVolumePesar();
        private List<wsEmissao.Cubagem> cubagens = new List<wsEmissao.Cubagem>();
        private int volume = 0;
        private bool lerProduto = false;
        private string numNota = "";

        public frmPesarPedido()
        {
            InitializeComponent(); 
        }

        private void frmPesarPedido_Load(object sender, EventArgs e)
        {
            txtCodigoDeBarras.Focus();
        }

        private void txtCodigoDeBarras_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                string transportadora = "";
                if(txtCodigoDeBarras.Text == "testar")
                {
                    var cubadora = Properties.Settings.Default["cubadora"].ToString();
                    if (cubadora == "perez")
                    {
                        var cubagem = new cubagemPerez();
                        var cubagemMedida = new cubagemMedida();
                        
                            var peso = cubagem.requisitaPeso();
                            cubagemMedida = cubagem.requisitaMedidas();

                            lblPeso.Text = "Peso: " + (peso * 1000).ToString() + " - Altura: " + (cubagemMedida.Altura).ToString() + " - Largura: " +
                                           (cubagemMedida.Largura).ToString() + " - Comprimento: " + (cubagemMedida.Comprimento).ToString();
                        
                    }
                    else
                    {
                        var cubometro = new VSIcubber.Cubomentro();
                        try
                        {
                            cubometro.LerDimensao(false);
                            if (Convert.ToInt32(Convert.ToDecimal(cubometro.Peso.Replace(".", ","))) > 50
                                && Convert.ToDecimal(cubometro.Largura.Replace(".", ",")) > 1
                                && Convert.ToDecimal(cubometro.Comprimento.Replace(".", ",")) > 1
                                && Convert.ToDecimal(cubometro.Altura.Replace(".", ",")) > 1)
                            {
                                var cubagem = new wsEmissao.Cubagem();
                                cubagem.numeroVolume = volume;
                                cubagem.altura = Convert.ToInt32(Convert.ToDecimal(cubometro.Altura.Replace(".", ",")));
                                cubagem.largura = Convert.ToInt32(Convert.ToDecimal(cubometro.Largura.Replace(".", ",")));
                                cubagem.comprimento = Convert.ToInt32(Convert.ToDecimal(cubometro.Comprimento.Replace(".", ",")));
                                cubagem.peso = Convert.ToInt32(Convert.ToDecimal(cubometro.Peso.Replace(".", ",")));
                                cubagem.cubado = false;
                                volume = 0;

                                lblPeso.Text = "Peso: " + cubagem.peso + " - Altura: " + cubagem.altura + " - Largura: " +
                                               cubagem.largura + " - Comprimento: " + cubagem.comprimento;
                            }
                            else
                            {
                                lblStatus.Text = "Falha ao Pesar." + Environment.NewLine + " Tente novamente ou leia o produto.";
                            }
                        }
                        catch (Exception ex)
                        {
                            lblStatus.Text = "Falha ao Pesar." + Environment.NewLine + " Tente novamente ou leia o produto.";
                        }
                    }
                    return;
                }
                if (txtCodigoDeBarras.Text != "")
                {
                    lblStatus.Text = "";
                    lblStatus.Invalidate();
                    lblStatus.Update();
                    lblStatus.Refresh();
                    Application.DoEvents();
                }
                if (txtCodigoDeBarras.Text.Contains("-") | txtCodigoDeBarras.Text.Length == 31)
                {
                    int idPedido = 0;

                    string cnpj = "";
                    string numeroNota = "";
                    string volAtual = "";
                    string volTotal = "";

                    if (txtCodigoDeBarras.Text.Length == 31)
                    {
                        cnpj = txtCodigoDeBarras.Text.Substring(0, 14);
                        numeroNota = txtCodigoDeBarras.Text.Substring(14, 9);
                        volAtual = txtCodigoDeBarras.Text.Substring(23, 4);
                        volTotal = txtCodigoDeBarras.Text.Substring(27, 4);
                    }
                    else
                    {
                        idPedido = Utilidades.retornaIdInterno(Convert.ToInt32(txtCodigoDeBarras.Text.Split('-')[0]));
                    }
                    if ((idPedido > 0 && (pedidoId == 0 | idPedido != pedidoId)) | (!string.IsNullOrEmpty(numeroNota) && (numNota == "" | numNota != numeroNota)))
                    {
                        var emissaoWs = new wsEmissao.EmissaoWs();

                        //ChecarVolumesPesarNota

                        var retornoVolumesPesar = new wsEmissao.RetornoChecarVolumePesar();
                        if (idPedido > 0)
                        {
                            retornoVolumesPesar = emissaoWs.ChecarVolumesPesarCds("glmp152029", idPedido, Convert.ToInt32(Properties.Settings.Default["cd"].ToString()));
                        }
                        else
                        {
                            retornoVolumesPesar = emissaoWs.ChecarVolumesPesarNota("glmp152029", cnpj, Convert.ToInt32(numeroNota), Convert.ToInt32(volAtual), Convert.ToInt32(Properties.Settings.Default["cd"].ToString()));
                        }
                        if (idPedido > 0)
                        {
                            pedidoId = idPedido;
                            numNota = "";
                        }
                        else
                        {
                            pedidoId = retornoVolumesPesar.pedidoId;
                            numNota = numeroNota;
                        }
                        volumesPesar = retornoVolumesPesar;
                        cubagens = new List<wsEmissao.Cubagem>();
                        lblVolumes.Text = "Volumes: " + volumesPesar.totalVolumes.ToString() + " - Volumes pesados: 0";
                        lblCodigo.Text = "Pesar Pacote";
                        lblStatus.Text = "";
                        lblPeso.Text = "";
                        transportadora = volumesPesar.transportadora;

                        
                        if(volumesPesar.produtos.Any(x => x.idCentroDistribuicao == 5))
                        {
                            lblVolumes.Text = "Volumes CD4: " + (volumesPesar.totalVolumes - volumesPesar.produtos.Count(x => x.idCentroDistribuicao == 5)).ToString() + " - Volumes CD5: " + volumesPesar.produtos.Count(x => x.idCentroDistribuicao == 5);
                            lblStatus.Text = "ATENÇÃO: Pedido com móveis no CD5";
                            int volumeAtual = (volumesPesar.totalVolumes - volumesPesar.produtos.Count(x => x.idCentroDistribuicao == 5));
                            foreach (var produto in volumesPesar.produtos.Where(x => x.idCentroDistribuicao == 5))
                            {
                                volumeAtual++;
                                var cubagem = new wsEmissao.Cubagem();
                                cubagem.numeroVolume = volumeAtual;
                                cubagem.altura = Convert.ToInt32(produto.altura);
                                cubagem.largura = Convert.ToInt32(produto.largura);
                                cubagem.comprimento = Convert.ToInt32(produto.comprimento);
                                cubagem.peso = Convert.ToInt32(produto.peso);
                                cubagem.cubado = true;
                                cubagens.RemoveAll(x => x.numeroVolume == volumeAtual);
                                cubagens.Add(cubagem);
                                lblPeso.Text = "Peso: " + cubagem.peso + " - Altura: " + cubagem.altura + " - Largura: " +
                                               cubagem.largura + " - Comprimento: " + cubagem.comprimento;
                                lerProduto = false;
                                lblVolumes.Text = "Volumes: " + volumesPesar.totalVolumes.ToString() +
                                                  " - Volumes pesados: " +
                                                  cubagens.Count;
                                lblCodigo.Text = "Leia a Caixa do Pedido";
                            }
                        }

                    }

                    if (volumesPesar.totalVolumes > 0)
                    {
                        if (txtCodigoDeBarras.Text.Length == 31)
                        {
                            cnpj = txtCodigoDeBarras.Text.Substring(0, 14);
                            numeroNota = txtCodigoDeBarras.Text.Substring(14, 9);
                            volAtual = txtCodigoDeBarras.Text.Substring(23, 4);
                            volTotal = txtCodigoDeBarras.Text.Substring(27, 4);
                        }
                        else
                        {
                            idPedido = Utilidades.retornaIdInterno(Convert.ToInt32(txtCodigoDeBarras.Text.Split('-')[0]));
                        }
                        
                        volume = Convert.ToInt32(volAtual);

                        var cubadora = Properties.Settings.Default["cubadora"].ToString();
                        if (cubadora == "perez")
                        {
                            var cubagemPerez = new cubagemPerez();
                            var cubagemMedida = new cubagemMedida();
                            var peso = cubagemPerez.requisitaPeso();
                            cubagemMedida = cubagemPerez.requisitaMedidas();

                            bool isValid = cubagemMedida.EhValido;
                            int tentativas = 0;
                            while(isValid == false && tentativas < 10)
                            {
                                Thread.Sleep(500);
                                cubagemMedida = cubagemPerez.requisitaMedidas();
                                isValid = cubagemMedida.EhValido;
                                if(isValid == false)
                                {
                                    tentativas++;
                                }
                            }

                            tentativas = 0;
                            while(cubagemMedida.Altura > 100 && tentativas < 15 && cubagemMedida.EhValido)
                            {
                                Thread.Sleep(500);
                                cubagemMedida = cubagemPerez.requisitaMedidas();
                                tentativas++;
                            }
                            if (isValid)
                            {
                                var cubagem = new wsEmissao.Cubagem();
                                cubagem.numeroVolume = volume;
                                cubagem.altura = Convert.ToInt32(cubagemMedida.Altura);
                                cubagem.largura = Convert.ToInt32(cubagemMedida.Largura);
                                cubagem.comprimento = Convert.ToInt32(cubagemMedida.Comprimento);
                                cubagem.peso = Convert.ToInt32(peso * 1000);
                                cubagem.cubado = false;
                                cubagens.RemoveAll(x => x.numeroVolume == volume);
                                cubagens.Add(cubagem);
                                volume = 0;

                                lblPeso.Text = "Peso: " + cubagem.peso + " - Altura: " + cubagem.altura + " - Largura: " +
                                               cubagem.largura + " - Comprimento: " + cubagem.comprimento;
                                lblVolumes.Text = "Volumes: " + volumesPesar.totalVolumes.ToString() + " - Volumes pesados: " +
                                                  cubagens.Count;
                                lblCodigo.Text = "Leia a Caixa do Pedido";
                            }
                            else
                            {
                                lblStatus.Text = "Falha ao Pesar." + Environment.NewLine + " Tente novamente ou leia o produto.";
                            }
                        }
                        else
                        {
                            var cubometro = new VSIcubber.Cubomentro();
                            try
                            {
                                cubometro.LerDimensao(false);
                                if (Convert.ToInt32(Convert.ToDecimal(cubometro.Peso.Replace(".", ","))) > 50
                                    && Convert.ToDecimal(cubometro.Largura.Replace(".", ",")) > 1
                                    && Convert.ToDecimal(cubometro.Comprimento.Replace(".", ",")) > 1
                                    && Convert.ToDecimal(cubometro.Altura.Replace(".", ",")) > 1)
                                {
                                    var cubagem = new wsEmissao.Cubagem();
                                    cubagem.numeroVolume = volume;
                                    cubagem.altura = Convert.ToInt32(Convert.ToDecimal(cubometro.Altura.Replace(".", ",")));
                                    cubagem.largura = Convert.ToInt32(Convert.ToDecimal(cubometro.Largura.Replace(".", ",")));
                                    cubagem.comprimento = Convert.ToInt32(Convert.ToDecimal(cubometro.Comprimento.Replace(".", ",")));
                                    cubagem.peso = Convert.ToInt32(Convert.ToDecimal(cubometro.Peso.Replace(".", ",")));
                                    cubagem.cubado = false;
                                    cubagens.RemoveAll(x => x.numeroVolume == volume);
                                    cubagens.Add(cubagem);
                                    volume = 0;

                                    lblPeso.Text = "Peso: " + cubagem.peso + " - Altura: " + cubagem.altura + " - Largura: " +
                                                   cubagem.largura + " - Comprimento: " + cubagem.comprimento;
                                    lblVolumes.Text = "Volumes: " + volumesPesar.totalVolumes.ToString() + " - Volumes pesados: " +
                                                      cubagens.Count;
                                    lblCodigo.Text = "Leia a Caixa do Pedido";
                                }
                                else
                                {
                                    lblStatus.Text = "Falha ao Pesar." + Environment.NewLine + " Tente novamente ou leia o produto.";
                                }
                            }
                            catch (Exception ex)
                            {
                                lblStatus.Text = "Falha ao Pesar." + Environment.NewLine + " Tente novamente ou leia o produto.";
                            }
                        }
                    }
                }
                else if (txtCodigoDeBarras.Text == "digitarpeso")
                {
                    var pesoInformado = new frmPesarGravarPeso();
                    pesoInformado.ShowDialog();
                    if (pesoInformado.DialogResult == DialogResult.OK)
                    {
                        lblStatus.Text = "";
                        if (pesoInformado.peso > 0)
                        {
                            var cubagem = new wsEmissao.Cubagem();
                            cubagem.numeroVolume = volume;
                            cubagem.altura = Convert.ToInt32(0);
                            cubagem.largura = Convert.ToInt32(0);
                            cubagem.comprimento = Convert.ToInt32(0);
                            cubagem.peso = Convert.ToInt32(pesoInformado.peso);
                            cubagem.cubado = true;
                            cubagens.RemoveAll(x => x.numeroVolume == volume);
                            cubagens.Add(cubagem);
                            lblPeso.Text = "Peso: " + cubagem.peso;
                            lerProduto = false;
                            lblVolumes.Text = "Volumes: " + volumesPesar.totalVolumes.ToString() +
                                              " - Volumes pesados: " +
                                              cubagens.Count;
                            lblCodigo.Text = "Leia a Caixa do Pedido";
                        }
                        else
                        {
                            lblStatus.Text = "Peso informado inválido";
                        }
                    }
                    else
                    {
                        lblStatus.Text = "Peso informado inválido";
                    }
                }
                else
                {
                    lblStatus.Text = "";
                    int etiqueta = 0;
                    int.TryParse(txtCodigoDeBarras.Text, out etiqueta);
                    if (etiqueta > 0)
                    {
                        var produto = volumesPesar.produtos.FirstOrDefault(x => x.etiqueta == etiqueta);
                        if (produto != null)
                        {
                            var cubagem = new wsEmissao.Cubagem();
                            cubagem.numeroVolume = volume;
                            cubagem.altura = Convert.ToInt32(produto.altura);
                            cubagem.largura = Convert.ToInt32(produto.largura);
                            cubagem.comprimento = Convert.ToInt32(produto.comprimento);
                            cubagem.peso = Convert.ToInt32(produto.peso);
                            cubagem.cubado = true;
                            cubagens.RemoveAll(x => x.numeroVolume == volume);
                            cubagens.Add(cubagem);
                            lblPeso.Text = "Peso: " + cubagem.peso + " - Altura: " + cubagem.altura + " - Largura: " +
                                           cubagem.largura + " - Comprimento: " + cubagem.comprimento;
                            lerProduto = false;
                            lblVolumes.Text = "Volumes: " + volumesPesar.totalVolumes.ToString() +
                                              " - Volumes pesados: " +
                                              cubagens.Count;
                            lblCodigo.Text = "Leia a Caixa do Pedido";
                        }
                        else
                        {
                            lblStatus.Text = "Produto não localizado neste pedido!";
                        }
                    }
                    else
                    {
                        lblStatus.Text = "Falha ao ler produto, tente novamente";
                    }
                }

                cubagens.RemoveAll(x => x.peso == 0);

                if (volumesPesar.totalVolumes > 0)
                {
                    if (cubagens.Count == volumesPesar.totalVolumes && (pedidoId > 0 | numNota != "") && volumesPesar.totalVolumes > 0)
                    {
                        var emissaoWs = new wsEmissao.EmissaoWs();
                        var retornoVolumesPesar = emissaoWs.GravarPesosFinalizarPedido("glmp152029", pedidoId,
                            volumesPesar.idPedidoEnvio, cubagens.ToArray(), idUsuario);
                        lblVolumes.Text = "Pedido Pesado com Sucesso";
                        lblStatus.Text = retornoVolumesPesar;

                        if(volumesPesar.produtos.Any(x => x.idCentroDistribuicao == 5))
                        {
                            lblStatus.Text = lblStatus.Text + Environment.NewLine + "ATENÇÃO: Pedidos possui MÓVEIS do cd5";
                        }

                        lblCodigo.Text = "Leia a Caixa do Pedido";
                        pedidoId = 0;
                        
                        volumesPesar = new wsEmissao.RetornoChecarVolumePesar();
                        cubagens = new List<wsEmissao.Cubagem>();
                        volume = 0;
                        lerProduto = false;
                    }
                }
                else
                {                    
                    lblVolumes.Text = "Pedido Pesado com Sucesso";
                    lblStatus.Text = transportadora;
                    if (volumesPesar.produtos.Any(x => x.idCentroDistribuicao == 5))
                    {
                        lblStatus.Text = lblStatus.Text + Environment.NewLine + "ATENÇÃO: Pedidos possui móveis do Setor de Móveis";
                    }

                    lblCodigo.Text = "Leia a Caixa do Pedido";
                    pedidoId = 0;
                    volumesPesar = new wsEmissao.RetornoChecarVolumePesar();
                    cubagens = new List<wsEmissao.Cubagem>();
                    volume = 0;
                    lerProduto = false;
                }

                txtCodigoDeBarras.Text = "";
                txtCodigoDeBarras.Focus();
            }
        }
        
    }
}
