﻿namespace graodegenteApp
{
    partial class frmPesarPedido
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtCodigoDeBarras = new Telerik.WinControls.UI.RadTextBox();
            this.lblCodigo = new Telerik.WinControls.UI.RadLabel();
            this.lblVolumes = new Telerik.WinControls.UI.RadLabel();
            this.lblStatus = new Telerik.WinControls.UI.RadLabel();
            this.lblPeso = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.txtCodigoDeBarras)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCodigo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblVolumes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPeso)).BeginInit();
            this.SuspendLayout();
            // 
            // txtCodigoDeBarras
            // 
            this.txtCodigoDeBarras.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCodigoDeBarras.AutoSize = false;
            this.txtCodigoDeBarras.Font = new System.Drawing.Font("Segoe UI", 16F);
            this.txtCodigoDeBarras.Location = new System.Drawing.Point(69, 51);
            this.txtCodigoDeBarras.Name = "txtCodigoDeBarras";
            this.txtCodigoDeBarras.Size = new System.Drawing.Size(839, 34);
            this.txtCodigoDeBarras.TabIndex = 16;
            this.txtCodigoDeBarras.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCodigoDeBarras_KeyDown);
            // 
            // lblCodigo
            // 
            this.lblCodigo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCodigo.AutoSize = false;
            this.lblCodigo.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblCodigo.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCodigo.Location = new System.Drawing.Point(12, 12);
            this.lblCodigo.Name = "lblCodigo";
            // 
            // 
            // 
            this.lblCodigo.RootElement.ControlBounds = new System.Drawing.Rectangle(12, 12, 100, 18);
            this.lblCodigo.Size = new System.Drawing.Size(947, 33);
            this.lblCodigo.TabIndex = 22;
            this.lblCodigo.Text = "Leia a Caixa do Pedido";
            this.lblCodigo.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblVolumes
            // 
            this.lblVolumes.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblVolumes.AutoSize = false;
            this.lblVolumes.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblVolumes.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVolumes.Location = new System.Drawing.Point(12, 95);
            this.lblVolumes.Name = "lblVolumes";
            // 
            // 
            // 
            this.lblVolumes.RootElement.ControlBounds = new System.Drawing.Rectangle(12, 95, 100, 18);
            this.lblVolumes.Size = new System.Drawing.Size(947, 33);
            this.lblVolumes.TabIndex = 25;
            this.lblVolumes.Text = "-";
            this.lblVolumes.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblStatus
            // 
            this.lblStatus.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblStatus.AutoSize = false;
            this.lblStatus.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblStatus.Font = new System.Drawing.Font("Segoe UI", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus.Location = new System.Drawing.Point(12, 205);
            this.lblStatus.Name = "lblStatus";
            // 
            // 
            // 
            this.lblStatus.RootElement.ControlBounds = new System.Drawing.Rectangle(12, 205, 100, 18);
            this.lblStatus.Size = new System.Drawing.Size(947, 262);
            this.lblStatus.TabIndex = 26;
            this.lblStatus.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblPeso
            // 
            this.lblPeso.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblPeso.AutoSize = false;
            this.lblPeso.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblPeso.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPeso.Location = new System.Drawing.Point(12, 136);
            this.lblPeso.Name = "lblPeso";
            // 
            // 
            // 
            this.lblPeso.RootElement.ControlBounds = new System.Drawing.Rectangle(12, 136, 100, 18);
            this.lblPeso.Size = new System.Drawing.Size(947, 33);
            this.lblPeso.TabIndex = 26;
            this.lblPeso.Text = "-";
            this.lblPeso.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            // 
            // frmPesarPedido
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Azure;
            this.ClientSize = new System.Drawing.Size(971, 492);
            this.Controls.Add(this.lblPeso);
            this.Controls.Add(this.lblStatus);
            this.Controls.Add(this.lblVolumes);
            this.Controls.Add(this.lblCodigo);
            this.Controls.Add(this.txtCodigoDeBarras);
            this.Name = "frmPesarPedido";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Pesar Pedido";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmPesarPedido_Load);
            ((System.ComponentModel.ISupportInitialize)(this.txtCodigoDeBarras)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCodigo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblVolumes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPeso)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadTextBox txtCodigoDeBarras;
        private Telerik.WinControls.UI.RadLabel lblCodigo;
        private Telerik.WinControls.UI.RadLabel lblVolumes;
        private Telerik.WinControls.UI.RadLabel lblStatus;
        private Telerik.WinControls.UI.RadLabel lblPeso;
    }
}