﻿using Microsoft.VisualBasic.CompilerServices;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Text;

namespace graodegenteApp
{
    public class cubagemPerez
    {
        string conexao = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=C:\\cubagem\\bd.mdb";


        public cubagemMedida requisitaMedidas()
        {
            cubagemMedida cubagemMedida = new cubagemMedida();
            cubagemMedida.EhValido = false;
            OleDbConnection selectConnection = new OleDbConnection(conexao);
            selectConnection.Open();
            OleDbDataAdapter oleDbDataAdapter = new OleDbDataAdapter("Select vlAltura,vllargura,vlComprimento From TbServiceInt where dtMedida >= #" + DateTime.Now.AddSeconds(-3.0).ToString("MM/dd/yyyy H:mm:ss") + "#", selectConnection);
            DataTable dataTable = new DataTable();
            oleDbDataAdapter.Fill(dataTable);
            if (dataTable.Rows.Count > 0)
            {
                cubagemMedida.EhValido = true;
                cubagemMedida.Altura = double.Parse(Conversions.ToString(dataTable.Rows[0]["vlAltura"]));
                cubagemMedida.Largura = double.Parse(Conversions.ToString(dataTable.Rows[0]["vlLargura"]));
                cubagemMedida.Comprimento = double.Parse(Conversions.ToString(dataTable.Rows[0]["vlComprimento"]));
            }
            dataTable.Dispose();
            oleDbDataAdapter.Dispose();
            selectConnection.Close();
            selectConnection.Dispose();
            return cubagemMedida;
        }

        public double requisitaPeso()
        {
            OleDbConnection selectConnection = new OleDbConnection(conexao);
            selectConnection.Open();
            OleDbDataAdapter oleDbDataAdapter = new OleDbDataAdapter("Select vlpeso From TbServiceInt where dtPeso >= #" + DateTime.Now.AddSeconds(-3.0).ToString("MM/dd/yyyy H:mm:ss") + "#", selectConnection);
            DataTable dataTable = new DataTable();
            oleDbDataAdapter.Fill(dataTable);
            double num = 0;
            if (dataTable.Rows.Count > 0)
                num = double.Parse(Conversions.ToString(dataTable.Rows[0]["vlPeso"]));
            dataTable.Dispose();
            oleDbDataAdapter.Dispose();
            selectConnection.Close();
            selectConnection.Dispose();
            return num;
        }
    }
}
