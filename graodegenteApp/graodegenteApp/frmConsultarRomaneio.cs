﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace graodegenteApp
{
    public partial class frmConsultarRomaneio : Form
    {
        public int idUsuario = 0;
        public frmConsultarRomaneio()
        {
            InitializeComponent();
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            var romaneio = new frmConsultarRomaneioLista();
            int idRomaneio = 0;
            int.TryParse(txtIdPedidoFornecedor.Text, out idRomaneio);
            romaneio.idPedidoFornecedor = idRomaneio;
            romaneio.Show();
        }

        private void frmConsultarRomaneio_Load(object sender, EventArgs e)
        {
            txtIdPedidoFornecedor.Focus();
        }
    }
}
