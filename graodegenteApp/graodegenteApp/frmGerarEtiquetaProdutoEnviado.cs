﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace graodegenteApp
{
    public partial class frmGerarEtiquetaProdutoEnviado : Form
    {
        public int idUsuario = 0;

        public frmGerarEtiquetaProdutoEnviado()
        {
            InitializeComponent();
        }

        private void btnImprimir_Click(object sender, EventArgs e)
        {
            int idPedidoFornecedorItem = 0;

            int.TryParse(txtIdItemPedido.Text.Trim(), out idPedidoFornecedorItem);

            if (idPedidoFornecedorItem == 0)
            {
                MessageBox.Show("Número de pedido incorreto");
                return;
            }

            var produtoWs = new wsProdutos.ProdutosWs();
            var novaEtiqueta = produtoWs.AdicionarProdutoEnviadoAoEstoqueUsuario("glmp152029", idPedidoFornecedorItem, idUsuario);
            if (!string.IsNullOrEmpty(novaEtiqueta))
            {
                imprimePedido(Convert.ToInt32(novaEtiqueta));
            }
        }

        private void imprimePedido(int idPedido)
        {
            var pedidosWs = new wsPedidosFornecedor.PedidosFornecedorWs();
            var detalheProduto = pedidosWs.RetornaDetalhesItem(idPedido, clsConfiguracao.ChaveWs, true);
            
            clsImprimirEtiqueta.GerarEtiqueta(idPedido.ToString(), detalheProduto.idPedidoFornecedor.ToString(), detalheProduto.fornecedorNome, detalheProduto.numeroItem.ToString(), detalheProduto.totalItens.ToString(), detalheProduto.produtoIdDaEmpresa, detalheProduto.complementoIdDaEmpresa, detalheProduto.produtoNome, detalheProduto.codigoEtiqueta, detalheProduto.produtoId.ToString());
            txtIdItemPedido.Text = "";
            txtIdItemPedido.Focus();
        }
        private void txtIdItemPedido_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnImprimir.PerformClick();
            }
        }

        private void frmGerarEtiquetaProdutoEnviado_Load(object sender, EventArgs e)
        {
            txtIdItemPedido.Focus();
        }
    }
}
