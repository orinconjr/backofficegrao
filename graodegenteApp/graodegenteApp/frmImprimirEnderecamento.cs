﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls.UI;

namespace graodegenteApp
{
    public partial class frmImprimirEnderecamento : Form
    {
        public frmImprimirEnderecamento()
        {
            InitializeComponent();
        }

        private void frmImprimirEnderecamento_Load(object sender, EventArgs e)
        {
            AlteraGrid();
            //grd.BestFitColumns();
        }

        private void btnImprimir_Click(object sender, EventArgs e)
        {
            if (grd.SelectedRows.Count() > 0)
            {
                var confirmacao = MessageBox.Show("Deseja imprimir a etiqueta dos endereços selecionados?", "Confirmação", MessageBoxButtons.YesNo);
                if (confirmacao == DialogResult.Yes)
                {
                    var etiqueta = new StringBuilder();

                    if (rdbEnderecamento.Checked)
                    {
                        for (int i = 0; i < grd.SelectedRows.Count; i++)
                        {
                            var idAndar = grd.SelectedRows[i].Cells[0].Value;
                            var area = grd.SelectedRows[i].Cells[1].Value;
                            var rua = grd.SelectedRows[i].Cells[2].Value;
                            var lado = grd.SelectedRows[i].Cells[3].Value;
                            var predio = grd.SelectedRows[i].Cells[4].Value;
                            var andar = grd.SelectedRows[i].Cells[5].Value;
                            etiqueta.Append("N");
                            etiqueta.Append(Environment.NewLine);
                            etiqueta.Append("q800");
                            etiqueta.Append(Environment.NewLine);
                            etiqueta.Append("Q600,26");
                            etiqueta.Append(Environment.NewLine);
                            etiqueta.AppendFormat("A40,20,0,4,1,1,N,\"A:{0}\"", area);
                            etiqueta.Append(Environment.NewLine);
                            etiqueta.AppendFormat("A40,40,0,4,1,1,N,\"R:{0} L:{1} P:{2} A:{3}\"", rua, lado, predio, andar);
                            etiqueta.Append(Environment.NewLine);
                            //etiqueta.AppendFormat("b46,150,Q,1,s5,eH,\"{0}\"", idAndar + "-4");
                            etiqueta.AppendFormat("B46,150,0,1,2,5,110,N,\"{0}\"", idAndar + "-4");
                            etiqueta.Append(Environment.NewLine);
                            etiqueta.AppendFormat("b580,150,Q,1,s5,eH,\"{0}\"", idAndar + "-4");
                            etiqueta.Append(Environment.NewLine);
                            etiqueta.AppendFormat("b46,400,Q,1,s5,eH,\"{0}\"", idAndar + "-4");
                            etiqueta.Append(Environment.NewLine);
                            etiqueta.AppendFormat("b580,400,Q,1,s5,eH,\"{0}\"", idAndar + "-4");
                            etiqueta.Append(Environment.NewLine);
                            etiqueta.Append("P1,1");
                            etiqueta.Append(Environment.NewLine);
                        }
                    }
                    else if (rdbEstantes.Checked)
                    {
                        for (int i = 0; i < grd.SelectedRows.Count; i++)
                        {
                            var idEnderecamentoSeparacao = grd.SelectedRows[i].Cells[0].Value;
                            var nome = grd.SelectedRows[i].Cells[1].Value;
                            etiqueta.Append("N");
                            etiqueta.Append(Environment.NewLine);
                            etiqueta.Append("q800");
                            etiqueta.Append(Environment.NewLine);
                            etiqueta.Append("Q600,26");
                            etiqueta.Append(Environment.NewLine);
                            etiqueta.AppendFormat("A40,20,0,4,1,1,N,\"{0}\"", nome);
                            etiqueta.Append(Environment.NewLine);
                            etiqueta.AppendFormat("b46,150,Q,1,s5,eH,\"{0}\"", idEnderecamentoSeparacao + "-esp");
                            etiqueta.Append(Environment.NewLine);
                            etiqueta.AppendFormat("b580,150,Q,1,s5,eH,\"{0}\"", idEnderecamentoSeparacao + "-esp");
                            etiqueta.Append(Environment.NewLine);
                            etiqueta.AppendFormat("b46,400,Q,1,s5,eH,\"{0}\"", idEnderecamentoSeparacao + "-esp");
                            etiqueta.Append(Environment.NewLine);
                            etiqueta.AppendFormat("b580,400,Q,1,s5,eH,\"{0}\"", idEnderecamentoSeparacao + "-esp");
                            etiqueta.Append(Environment.NewLine);
                            etiqueta.Append("P1,1");
                            etiqueta.Append(Environment.NewLine);
                        }

                    }
                    else if (rdbSenhas.Checked)
                    {
                        for (int i = 0; i < grd.SelectedRows.Count; i++)
                        {
                            var id = grd.SelectedRows[i].Cells[0].Value;
                            var nome = grd.SelectedRows[i].Cells[1].Value;
                            var senha = grd.SelectedRows[i].Cells[2].Value;
                            etiqueta.Append("N");
                            etiqueta.Append(Environment.NewLine);
                            etiqueta.Append("q800");
                            etiqueta.Append(Environment.NewLine);
                            etiqueta.Append("Q600,26");
                            etiqueta.Append(Environment.NewLine);
                            etiqueta.AppendFormat("A40,20,0,4,1,1,N,\"{0}\"", nome);
                            etiqueta.Append(Environment.NewLine);
                            //Código de barras
                            //etiqueta.AppendFormat("b46,150,Q,1,s5,eH,\"{0}\"", senha);
                            etiqueta.AppendFormat("B46,150,0,1,2,5,110,N,\"{0}\"", senha);
                            etiqueta.Append(Environment.NewLine);
                            etiqueta.AppendFormat("b580,150,Q,1,s5,eH,\"{0}\"", senha);
                            etiqueta.Append(Environment.NewLine);
                            etiqueta.AppendFormat("b46,400,Q,1,s5,eH,\"{0}\"", senha);
                            etiqueta.Append(Environment.NewLine);
                            etiqueta.AppendFormat("b580,400,Q,1,s5,eH,\"{0}\"", senha);
                            etiqueta.Append(Environment.NewLine);
                            etiqueta.Append("P1,1");
                            etiqueta.Append(Environment.NewLine);
                        }

                    }
                    else if (rdbCarrinhos.Checked)
                    {
                        for (int i = 0; i < grd.SelectedRows.Count; i++)
                        {
                            var idEnderecamentoSeparacao = grd.SelectedRows[i].Cells[0].Value;
                            var nome = grd.SelectedRows[i].Cells[1].Value;
                            etiqueta.Append("N");
                            etiqueta.Append(Environment.NewLine);
                            etiqueta.Append("q800");
                            etiqueta.Append(Environment.NewLine);
                            etiqueta.Append("Q600,26");
                            etiqueta.Append(Environment.NewLine);
                            etiqueta.AppendFormat("A40,20,0,4,1,1,N,\"{0}\"", nome);
                            etiqueta.Append(Environment.NewLine);
                            etiqueta.AppendFormat("B46,150,0,1,2,5,110,N,\"{0}\"", idEnderecamentoSeparacao + "-edt");
                            etiqueta.Append(Environment.NewLine);
                            etiqueta.AppendFormat("b46,400,Q,1,s5,eH,\"{0}\"", idEnderecamentoSeparacao + "-edt");
                            etiqueta.Append(Environment.NewLine);
                            etiqueta.AppendFormat("b580,400,Q,1,s5,eH,\"{0}\"", idEnderecamentoSeparacao + "-edt");
                            etiqueta.Append(Environment.NewLine);
                            etiqueta.Append("P1,1");
                            etiqueta.Append(Environment.NewLine);
                        }

                    }
                    else if (rdbEstantePacote.Checked)
                    {
                        for (int i = 0; i < grd.SelectedRows.Count; i++)
                        {
                            var idEnderecamentoSeparacao = grd.SelectedRows[i].Cells[0].Value;
                            var nome = grd.SelectedRows[i].Cells[1].Value;
                            etiqueta.Append("N");
                            etiqueta.Append(Environment.NewLine);
                            etiqueta.Append("q800");
                            etiqueta.Append(Environment.NewLine);
                            etiqueta.Append("Q600,26");
                            etiqueta.Append(Environment.NewLine);
                            etiqueta.AppendFormat("A40,20,0,4,1,1,N,\"{0}\"", nome);
                            etiqueta.Append(Environment.NewLine);
                            etiqueta.AppendFormat("b46,150,Q,1,s5,eH,\"{0}\"", idEnderecamentoSeparacao + "-pac");
                            etiqueta.Append(Environment.NewLine);
                            etiqueta.AppendFormat("b580,150,Q,1,s5,eH,\"{0}\"", idEnderecamentoSeparacao + "-pac");
                            etiqueta.Append(Environment.NewLine);
                            etiqueta.AppendFormat("b46,400,Q,1,s5,eH,\"{0}\"", idEnderecamentoSeparacao + "-pac");
                            etiqueta.Append(Environment.NewLine);
                            etiqueta.AppendFormat("b580,400,Q,1,s5,eH,\"{0}\"", idEnderecamentoSeparacao + "-pac");
                            etiqueta.Append(Environment.NewLine);
                            etiqueta.Append("P1,1");
                            etiqueta.Append(Environment.NewLine);
                        }

                    }

                    clsImprimirEtiqueta.ImprimeEtiqueta(etiqueta.ToString());

                }
            }
            else
            {
                MessageBox.Show("Nenhum endereço foi selecionado");
            }
        }

        private void rdbEnderecamento_CheckedChanged(object sender, EventArgs e)
        {
            AlteraGrid();
        }

        private void rdbEstantes_CheckedChanged(object sender, EventArgs e)
        {
            AlteraGrid();
        }

        private void rdbSenhas_CheckedChanged(object sender, EventArgs e)
        {
            AlteraGrid();
        }

        public void AlteraGrid()
        {
            int idCentroDistribuicao = Convert.ToInt32(Properties.Settings.Default["cd"].ToString());
            if (rdbEnderecamento.Checked)
            {
                bool possuiColunas = true;
                possuiColunas = grd.Columns.Count > 0;
                while (possuiColunas)
                {
                    grd.Columns.RemoveAt(0);
                    possuiColunas = grd.Columns.Count > 0;
                }
                grd.Columns.Add(new GridViewTextBoxColumn() { FieldName = "idEnderecamentoAndar", HeaderText = "ID", Name = "idEnderecamentoAndar" });
                grd.Columns.Add(new GridViewTextBoxColumn() { FieldName = "area", HeaderText = "Área", Name = "area" });
                grd.Columns.Add(new GridViewTextBoxColumn() { FieldName = "rua", HeaderText = "Rua", Name = "rua" });
                grd.Columns.Add(new GridViewTextBoxColumn() { FieldName = "lado", HeaderText = "Lado", Name = "lado" });
                grd.Columns.Add(new GridViewTextBoxColumn() { FieldName = "predio", HeaderText = "Prédio", Name = "predio" });
                grd.Columns.Add(new GridViewTextBoxColumn() { FieldName = "andar", HeaderText = "Andar", Name = "andar" });
                var enderecosWs = new wsProdutos.ProdutosWs();
                var itens = enderecosWs.RetornarListaEnderecos(clsConfiguracao.ChaveWs).ToList();
                itens = itens.Where(x => x.idCentroDistribuicao == idCentroDistribuicao).OrderBy(x => x.area).ThenBy(x => x.rua).ThenBy(x => x.lado).ThenBy(x => x.predio).ThenBy(x => x.andar).ToList();

                grd.DataSource = itens;

            }
            else if (rdbEstantes.Checked)
            {
                bool possuiColunas = true;
                possuiColunas = grd.Columns.Count > 0;
                while (possuiColunas)
                {
                    grd.Columns.RemoveAt(0);
                    possuiColunas = grd.Columns.Count > 0;
                }
                grd.Columns.Add(new GridViewTextBoxColumn() { FieldName = "idEnderecamentoSeparacao", HeaderText = "ID", Name = "idEnderecamentoSeparacao" });
                grd.Columns.Add(new GridViewTextBoxColumn() { FieldName = "enderecoSeparacao", HeaderText = "Nome da Estante", Name = "enderecoSeparacao" });
                var enderecosWs = new wsProdutos.ProdutosWs();
                var itens = enderecosWs.RetornarListaEstanteSeparacao(clsConfiguracao.ChaveWs).Where(x => x.idCentroDistribuicao == idCentroDistribuicao);
                grd.DataSource = itens;

            }
            else if (rdbSenhas.Checked)
            {
                bool possuiColunas = true;
                possuiColunas = grd.Columns.Count > 0;
                while (possuiColunas)
                {
                    grd.Columns.RemoveAt(0);
                    possuiColunas = grd.Columns.Count > 0;
                }
                grd.Columns.Add(new GridViewTextBoxColumn() { FieldName = "idUsuarioExpedicao", HeaderText = "ID", Name = "idUsuarioExpedicao" });
                grd.Columns.Add(new GridViewTextBoxColumn() { FieldName = "nome", HeaderText = "Nome", Name = "nome" });
                grd.Columns.Add(new GridViewTextBoxColumn() { FieldName = "senha", HeaderText = "Senha", Name = "senha", IsVisible = false });
                var enderecosWs = new wsProdutos.ProdutosWs();
                var itens = enderecosWs.RetornarListaSenhasEmbalagem(clsConfiguracao.ChaveWs).Where(x => x.idCentroDistribuicao == idCentroDistribuicao);
                grd.DataSource = itens;

            }
            else if (rdbCarrinhos.Checked)
            {
                bool possuiColunas = true;
                possuiColunas = grd.Columns.Count > 0;
                while (possuiColunas)
                {
                    grd.Columns.RemoveAt(0);
                    possuiColunas = grd.Columns.Count > 0;
                }
                grd.Columns.Add(new GridViewTextBoxColumn() { FieldName = "idEnderecamentoTransferencia", HeaderText = "ID", Name = "idEnderecamentoTransferencia" });
                grd.Columns.Add(new GridViewTextBoxColumn() { FieldName = "enderecamentoTransferencia", HeaderText = "Nome", Name = "enderecamentoTransferencia" });
                var enderecosWs = new wsProdutos.ProdutosWs();
                var itens = enderecosWs.RetornarListaCarrinhosEnderecamento(clsConfiguracao.ChaveWs).Where(x => x.idCentroDistribuicao == idCentroDistribuicao);
                grd.DataSource = itens;
            }
            else if (rdbEstantePacote.Checked)
            {
                bool possuiColunas = true;
                possuiColunas = grd.Columns.Count > 0;
                while (possuiColunas)
                {
                    grd.Columns.RemoveAt(0);
                    possuiColunas = grd.Columns.Count > 0;
                }
                grd.Columns.Add(new GridViewTextBoxColumn() { FieldName = "idEnderecoPacote", HeaderText = "ID", Name = "idEnderecoPacote" });
                grd.Columns.Add(new GridViewTextBoxColumn() { FieldName = "enderecoPacote", HeaderText = "Nome", Name = "enderecoPacote" });
                var enderecosWs = new wsProdutos.ProdutosWs();
                var itens = enderecosWs.RetornarListaEnderecoPacote(clsConfiguracao.ChaveWs);
                grd.DataSource = itens;
            }
        }

        private void rdbCarrinhos_CheckedChanged(object sender, EventArgs e)
        {
            AlteraGrid();
        }

        private void rdbEstantePacote_CheckedChanged(object sender, EventArgs e)
        {
            AlteraGrid();
        }
    }
}
