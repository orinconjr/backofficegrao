﻿namespace graodegenteApp
{
    partial class usrEmbalarQuantidade
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblQuantidadePedidos = new System.Windows.Forms.Label();
            this.lblQuantidadeEmbalados = new System.Windows.Forms.Label();
            this.btnEmbalarPedido = new Telerik.WinControls.UI.RadButton();
            ((System.ComponentModel.ISupportInitialize)(this.btnEmbalarPedido)).BeginInit();
            this.SuspendLayout();
            // 
            // lblQuantidadePedidos
            // 
            this.lblQuantidadePedidos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblQuantidadePedidos.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQuantidadePedidos.Location = new System.Drawing.Point(15, 21);
            this.lblQuantidadePedidos.Name = "lblQuantidadePedidos";
            this.lblQuantidadePedidos.Size = new System.Drawing.Size(624, 46);
            this.lblQuantidadePedidos.TabIndex = 0;
            this.lblQuantidadePedidos.Text = "label1";
            this.lblQuantidadePedidos.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblQuantidadeEmbalados
            // 
            this.lblQuantidadeEmbalados.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblQuantidadeEmbalados.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQuantidadeEmbalados.Location = new System.Drawing.Point(15, 86);
            this.lblQuantidadeEmbalados.Name = "lblQuantidadeEmbalados";
            this.lblQuantidadeEmbalados.Size = new System.Drawing.Size(624, 46);
            this.lblQuantidadeEmbalados.TabIndex = 1;
            this.lblQuantidadeEmbalados.Text = "label1";
            this.lblQuantidadeEmbalados.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lblQuantidadeEmbalados.Visible = false;
            // 
            // btnEmbalarPedido
            // 
            this.btnEmbalarPedido.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnEmbalarPedido.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnEmbalarPedido.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEmbalarPedido.Location = new System.Drawing.Point(155, 160);
            this.btnEmbalarPedido.Name = "btnEmbalarPedido";
            // 
            // 
            // 
            this.btnEmbalarPedido.RootElement.AccessibleDescription = null;
            this.btnEmbalarPedido.RootElement.AccessibleName = null;
            this.btnEmbalarPedido.RootElement.ControlBounds = new System.Drawing.Rectangle(0, 0, 110, 24);
            this.btnEmbalarPedido.Size = new System.Drawing.Size(328, 85);
            this.btnEmbalarPedido.TabIndex = 7;
            this.btnEmbalarPedido.Text = "Embalar um Pedido";
            this.btnEmbalarPedido.Click += new System.EventHandler(this.btnEmbalarPedido_Click);
            // 
            // usrEmbalarQuantidade
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Azure;
            this.Controls.Add(this.btnEmbalarPedido);
            this.Controls.Add(this.lblQuantidadeEmbalados);
            this.Controls.Add(this.lblQuantidadePedidos);
            this.Name = "usrEmbalarQuantidade";
            this.Size = new System.Drawing.Size(652, 502);
            this.Load += new System.EventHandler(this.usrEmbalarQuantidade_Load);
            ((System.ComponentModel.ISupportInitialize)(this.btnEmbalarPedido)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblQuantidadePedidos;
        private System.Windows.Forms.Label lblQuantidadeEmbalados;
        private Telerik.WinControls.UI.RadButton btnEmbalarPedido;
    }
}
