﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;

namespace graodegenteApp
{
    public partial class frmLoginExpedicao : Form
    {
        public int idUsuario = 0;
        public string paginaAutorizacao = "";
        public string tipoAutorizacao = "";
        public string login = "";

        public frmLoginExpedicao()
        {
            InitializeComponent();
        }

        private void frmLoginExpedicao_Load(object sender, EventArgs e)
        {
            ThemeResolutionService.ApplicationThemeName = "TelerikMetroBlue";
        }

        private void txtEntrar_Click(object sender, EventArgs e)
        {
           
        }

        private void txtLogin_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtSenha.Focus();
            }
        }

        private void txtSenha_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtEntrar.PerformClick();
            }
        }
    }
}
