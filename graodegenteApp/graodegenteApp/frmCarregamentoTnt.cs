﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using graodegenteApp.wsEmissao;

namespace graodegenteApp
{
    public partial class frmCarregamentoTnt : Form
    {
        public string cnpj = "";
        public string numeroNota = "";
        public string volumeAtual = "";
        public string volumeTotal = "";
        public string linkDanfe = "";
        public string linkGnre = "";
        public string linkGnreRecibo = "";
        public int idUsuario = 0;
        public string usuario = "";

        public frmCarregamentoTnt()
        {
            InitializeComponent(); 
        }

        private void frmCarregamentoTnt_Load(object sender, EventArgs e)
        {
            txtCodigoDeBarras.Focus();
            ListEmissoes();
           // BaixaImprimeDanfe("", "http://localhost/graodegenteadmin/gnre.aspx?id=1", "http://localhost/graodegenteadmin/gnrerecibo.aspx?id=1");
        }

        private void txtCodigoDeBarras_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                string codigo = txtCodigoDeBarras.Text;
                if (codigo.Length != 31)
                {
                    lblCodigo.Text = "Codigo de Barras Incorreto, favor ler novamente:";
                    txtCodigoDeBarras.Text = "";
                    txtCodigoDeBarras.Focus();
                    return;
                }
                cnpj = codigo.Substring(0, 14);
                numeroNota = codigo.Substring(14, 9);
                volumeAtual = codigo.Substring(23, 4);
                volumeTotal = codigo.Substring(27, 4);

                int quantidadeImpressao = 1;
                if(usuario.ToLower() == "plimor")
                {
                    quantidadeImpressao = 1;
                }
                var emissaoWs = new EmissaoWs();
                var emissao = emissaoWs.CarregaVolumeTnt(cnpj, Convert.ToInt32(numeroNota), Convert.ToInt32(volumeAtual), idUsuario);
                txtCodigoDeBarras.Text = "";
                txtCodigoDeBarras.Focus();
                
                if (emissao.pedidoTotalmenteCarregado && !string.IsNullOrEmpty(emissao.danfe))
                {
                    linkDanfe = emissao.danfe;
                    linkGnre = emissao.gnre;
                    linkGnreRecibo = emissao.comprovantePagamentoGnre;
                    BaixaImprimeDanfe(linkDanfe, linkGnre, linkGnreRecibo, quantidadeImpressao);
                    ExibeBotaoImprimir();
                    lblNota.Visible = true;
                }
                else if (emissao.pedidoTotalmenteCarregado && string.IsNullOrEmpty(emissao.danfe))
                {
                    lblNota.Text = "Pedido Carregado - Nota aguardando processamento";
                    lblNota.Visible = true;
                    cnpj = "";
                    numeroNota = "";
                    volumeAtual = "";
                    volumeTotal = "";
                    linkDanfe = "";
                }
                else if (!emissao.pedidoTotalmenteCarregado)
                {
                    OcultaBotaoImprimir();
                    lblNota.Text = "Pedido Parcialmente carregado - Volumes faltando: " + emissao.volumesFaltando;
                    lblNota.Visible = true;
                    cnpj = "";
                    numeroNota = "";
                    volumeAtual = "";
                    volumeTotal = "";
                    linkDanfe = "";
                }
            }
        }


        private void BaixaImprimeDanfe(string danfe, string gnre, string gnrerecibo, int quantidade)
        {
            lblNota.Text = "Imprimindo nota";
            if(!string.IsNullOrEmpty(gnre)) lblNota.Text += "/gnre";
            if(!string.IsNullOrEmpty(gnrerecibo)) lblNota.Text += "/recibo";
            lblNota.Text += " " + numeroNota;

            string enderecoArquivo = "c:\\danfes\\" + cnpj + "_" + numeroNota + ".pdf";

            try
            {
                byte[] binaryData;
                binaryData = Convert.FromBase64String(danfe);
                System.IO.FileStream outFile = new FileStream(enderecoArquivo, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                outFile.Write(binaryData, 0, binaryData.Length);
                outFile.Close();
            }
            catch (Exception)
            {
                
            }
            
            try
            {
                PrinterSettings settings = new PrinterSettings();
                string nomeImpressora = settings.PrinterName;
                for (int i = 1; i <= quantidade; i++)
                {
                    clsImprimirEtiqueta.PrintPdf(enderecoArquivo, nomeImpressora);
                }

                var emissaoWs = new EmissaoWs();
                var emissao = emissaoWs.NotaImpressaTnt(cnpj, Convert.ToInt32(numeroNota), idUsuario);
                ListEmissoes();
            }
            catch (Exception)
            {

            }

            if(!string.IsNullOrEmpty(gnre)) webGnre.Navigate(gnre);
            if(!string.IsNullOrEmpty(gnrerecibo)) webGnreRecibo.Navigate(gnrerecibo);
        }

        private void ExibeBotaoImprimir()
        {
            //txtCodigoDeBarras.Visible = false;
            btnFalhaNota.Visible = true;
            btnNotaImpressa.Visible = true;
            btnReimprimirNota.Visible = true;
            txtCodigoDeBarras.Focus();
        }
        private void OcultaBotaoImprimir()
        {
            txtCodigoDeBarras.Visible = true;
            btnFalhaNota.Visible = false;
            btnNotaImpressa.Visible = false;
            btnReimprimirNota.Visible = false;
            txtCodigoDeBarras.Text = "";
            txtCodigoDeBarras.Focus();
            lblNota.Text = "";
            cnpj = "";
            numeroNota = "";
            volumeAtual = "";
            volumeTotal = "";
            linkDanfe = "";
            lblNota.Visible = false;
        }



        public static string base64Decode(string data)
        {
            try
            {
                System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();
                System.Text.Decoder utf8Decode = encoder.GetDecoder();

                byte[] todecode_byte = Convert.FromBase64String(data);
                int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
                char[] decoded_char = new char[charCount];
                utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
                string result = new String(decoded_char);
                return result;
            }
            catch (Exception e)
            {
                throw new Exception("Error in base64Decode" + e.Message);
            }
        }

        private void btnReimprimirNota_Click(object sender, EventArgs e)
        {
            BaixaImprimeDanfe(linkDanfe, linkGnre, linkGnreRecibo, 1);
        }

        private void btnNotaImpressa_Click(object sender, EventArgs e)
        {
            var emissaoWs = new EmissaoWs();
            var emissao = emissaoWs.NotaImpressaTnt(cnpj, Convert.ToInt32(numeroNota), idUsuario);
            OcultaBotaoImprimir();
        }

        private void btnFalhaNota_Click(object sender, EventArgs e)
        {
            OcultaBotaoImprimir();
        }

        private void webGnre_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            webGnre.Print();
        }

        private void webGnreRecibo_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            webGnreRecibo.Print();
        }

        private void btnAtualizarEmissoes_Click(object sender, EventArgs e)
        {
            ListEmissoes();
        }

        private void ListEmissoes()
        {
            if (!AtualizandoEmissoes) ThreadPool.QueueUserWorkItem(ListEmissoes);
        }

        private bool AtualizandoEmissoes = false;
        private void ListEmissoes(object parametros)
        {
            try
            {
                AtualizandoEmissoes = true;
                var serviceEmissao = new wsEmissao.EmissaoWs();
                var emissoesPendentes = serviceEmissao.ListaEmissoesTransportadoras("glmp152029", usuario);
                grd.Invoke((MethodInvoker)delegate
                {
                    grd.AutoSizeRows = true;
                    grd.DataSource = emissoesPendentes.ToList();

                });
            }
            catch (Exception)
            {

            }
            finally
            {
                AtualizandoEmissoes = false;
            }
        }
    }
}
