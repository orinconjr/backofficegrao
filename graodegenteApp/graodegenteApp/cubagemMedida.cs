﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace graodegenteApp
{
    public class cubagemMedida
    {
        private bool stValido;
        private double vlLargura;
        private double vlAltura;
        private double vlComprimento;

        public cubagemMedida()
        {
            this.stValido = false;
            this.vlLargura = 0.0;
            this.vlAltura = 0.0;
            this.vlComprimento = 0.0;
        }

        public bool EhValido
        {
            get
            {
                return this.stValido;
            }
            set
            {
                this.stValido = value;
            }
        }

        public double Largura
        {
            get
            {
                return this.vlLargura;
            }
            set
            {
                this.vlLargura = value;
            }
        }

        public double Altura
        {
            get
            {
                return this.vlAltura;
            }
            set
            {
                this.vlAltura = value;
            }
        }

        public double Comprimento
        {
            get
            {
                return this.vlComprimento;
            }
            set
            {
                this.vlComprimento = value;
            }
        }
    }
}

