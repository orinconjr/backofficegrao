﻿namespace graodegenteApp
{
    partial class frmConfiguracoes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnGravar = new Telerik.WinControls.UI.RadButton();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.txtImpressora = new Telerik.WinControls.UI.RadTextBox();
            this.lblCD = new Telerik.WinControls.UI.RadLabel();
            this.txtCentroDeDistribuicao = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.txtCubadora = new Telerik.WinControls.UI.RadTextBox();
            this.btnListar = new Telerik.WinControls.UI.RadButton();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.txtComputador = new Telerik.WinControls.UI.RadTextBox();
            this.btnImprimirEtiquetaCompulador = new Telerik.WinControls.UI.RadButton();
            ((System.ComponentModel.ISupportInitialize)(this.btnGravar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtImpressora)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCentroDeDistribuicao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCubadora)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnListar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtComputador)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnImprimirEtiquetaCompulador)).BeginInit();
            this.SuspendLayout();
            // 
            // btnGravar
            // 
            this.btnGravar.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGravar.Location = new System.Drawing.Point(22, 326);
            this.btnGravar.Name = "btnGravar";
            this.btnGravar.Size = new System.Drawing.Size(230, 39);
            this.btnGravar.TabIndex = 6;
            this.btnGravar.Text = "GRAVAR";
            this.btnGravar.Click += new System.EventHandler(this.btnImprimir_Click);
            // 
            // radLabel2
            // 
            this.radLabel2.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel2.Location = new System.Drawing.Point(22, 9);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(131, 37);
            this.radLabel2.TabIndex = 8;
            this.radLabel2.Text = "Impressora";
            // 
            // txtImpressora
            // 
            this.txtImpressora.Font = new System.Drawing.Font("Segoe UI", 16F);
            this.txtImpressora.Location = new System.Drawing.Point(22, 48);
            this.txtImpressora.Name = "txtImpressora";
            this.txtImpressora.Size = new System.Drawing.Size(230, 34);
            this.txtImpressora.TabIndex = 7;
            this.txtImpressora.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtIdItemPedido_KeyDown);
            // 
            // lblCD
            // 
            this.lblCD.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCD.Location = new System.Drawing.Point(22, 82);
            this.lblCD.Name = "lblCD";
            this.lblCD.Size = new System.Drawing.Size(43, 37);
            this.lblCD.TabIndex = 10;
            this.lblCD.Text = "CD";
            // 
            // txtCentroDeDistribuicao
            // 
            this.txtCentroDeDistribuicao.Font = new System.Drawing.Font("Segoe UI", 16F);
            this.txtCentroDeDistribuicao.Location = new System.Drawing.Point(22, 121);
            this.txtCentroDeDistribuicao.Name = "txtCentroDeDistribuicao";
            this.txtCentroDeDistribuicao.Size = new System.Drawing.Size(230, 34);
            this.txtCentroDeDistribuicao.TabIndex = 9;
            // 
            // radLabel1
            // 
            this.radLabel1.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel1.Location = new System.Drawing.Point(22, 236);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(117, 37);
            this.radLabel1.TabIndex = 12;
            this.radLabel1.Text = "Cubadora";
            // 
            // txtCubadora
            // 
            this.txtCubadora.Font = new System.Drawing.Font("Segoe UI", 16F);
            this.txtCubadora.Location = new System.Drawing.Point(22, 275);
            this.txtCubadora.Name = "txtCubadora";
            this.txtCubadora.Size = new System.Drawing.Size(230, 34);
            this.txtCubadora.TabIndex = 11;
            // 
            // btnListar
            // 
            this.btnListar.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnListar.Location = new System.Drawing.Point(258, 48);
            this.btnListar.Name = "btnListar";
            this.btnListar.Size = new System.Drawing.Size(37, 34);
            this.btnListar.TabIndex = 13;
            this.btnListar.Text = "L";
            this.btnListar.Click += new System.EventHandler(this.btnListar_Click);
            // 
            // radLabel3
            // 
            this.radLabel3.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel3.Location = new System.Drawing.Point(22, 161);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(149, 37);
            this.radLabel3.TabIndex = 15;
            this.radLabel3.Text = "Computador";
            // 
            // txtComputador
            // 
            this.txtComputador.Font = new System.Drawing.Font("Segoe UI", 16F);
            this.txtComputador.Location = new System.Drawing.Point(22, 200);
            this.txtComputador.Name = "txtComputador";
            this.txtComputador.Size = new System.Drawing.Size(230, 34);
            this.txtComputador.TabIndex = 14;
            // 
            // btnImprimirEtiquetaCompulador
            // 
            this.btnImprimirEtiquetaCompulador.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnImprimirEtiquetaCompulador.Location = new System.Drawing.Point(258, 200);
            this.btnImprimirEtiquetaCompulador.Name = "btnImprimirEtiquetaCompulador";
            this.btnImprimirEtiquetaCompulador.Size = new System.Drawing.Size(37, 34);
            this.btnImprimirEtiquetaCompulador.TabIndex = 16;
            this.btnImprimirEtiquetaCompulador.Text = "I";
            this.btnImprimirEtiquetaCompulador.Click += new System.EventHandler(this.btnImprimirEtiquetaCompulador_Click);
            // 
            // frmConfiguracoes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Azure;
            this.ClientSize = new System.Drawing.Size(305, 389);
            this.Controls.Add(this.btnImprimirEtiquetaCompulador);
            this.Controls.Add(this.radLabel3);
            this.Controls.Add(this.txtComputador);
            this.Controls.Add(this.btnListar);
            this.Controls.Add(this.radLabel1);
            this.Controls.Add(this.txtCubadora);
            this.Controls.Add(this.lblCD);
            this.Controls.Add(this.txtCentroDeDistribuicao);
            this.Controls.Add(this.radLabel2);
            this.Controls.Add(this.txtImpressora);
            this.Controls.Add(this.btnGravar);
            this.Name = "frmConfiguracoes";
            this.Text = "Configurações";
            this.Load += new System.EventHandler(this.frmConfiguracoes_Load);
            ((System.ComponentModel.ISupportInitialize)(this.btnGravar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtImpressora)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCentroDeDistribuicao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCubadora)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnListar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtComputador)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnImprimirEtiquetaCompulador)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadButton btnGravar;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadTextBox txtImpressora;
        private Telerik.WinControls.UI.RadLabel lblCD;
        private Telerik.WinControls.UI.RadTextBox txtCentroDeDistribuicao;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadTextBox txtCubadora;
        private Telerik.WinControls.UI.RadButton btnListar;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadTextBox txtComputador;
        private Telerik.WinControls.UI.RadButton btnImprimirEtiquetaCompulador;
    }
}