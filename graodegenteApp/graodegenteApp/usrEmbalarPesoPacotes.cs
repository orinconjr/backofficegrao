﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using graodegenteApp.wsEmbalagem;

namespace graodegenteApp
{
    public partial class usrEmbalarPesoPacotes : UserControl
    {
        private bool numeroPedido = true;
        private int totalVolumes = 0;
        private List<wsEmbalagem.pesosPacotesGravar> pesos = new List<pesosPacotesGravar>();

        public usrEmbalarPesoPacotes()
        {
            InitializeComponent();
        }

        private void usrEmbalarPesoPacotes_Load(object sender, EventArgs e)
        {
            var formPrincipal = (this.Parent.Parent as frmPrincipal);
            mostrarNumeroPedido();
            var embalarWs = new wsEmbalagem.EmbalagemWs();
            var pacotes = embalarWs.RetornaQuantidadePacotes(formPrincipal.idUsuarioExpedicao, formPrincipal.idPedidoEmbalagem);
            if (pacotes == 0)
            {
                MessageBox.Show("Falha ao carregar quantidade de pacotes. Por favor, tente novamente");
                formPrincipal.carregaEmbalarLogin();
            }
            else
            {
                totalVolumes = pacotes;
            }
        }

        private void mostrarNumeroPedido()
        {
            numeroPedido = true;
            panel1.Visible = false;
            lblNumeroPedido.Visible = true;
            txtNumeroPedido.Visible = true;
            btnConfirmar.Top = 160;
            txtNumeroPedido.Text = "";
            txtNumeroPedido.Focus();
        }
        private void mostrarPesos()
        {
            numeroPedido = false;
            panel1.Visible = true;
            lblNumeroPedido.Visible = false;
            txtNumeroPedido.Visible = false;
            btnConfirmar.Top = 486;
            txtPesoPacote.Text = "0";
            txtLargura.Text = "0";
            txtAltura.Text = "0";
            txtPesoPacote.Value = 0;
            txtLargura.Value = 0;
            txtAltura.Value = 0;
            txtProfundidade.Text = "";
            txtPesoPacote.Focus();
        }

        private void btnConfirmar_Click(object sender, EventArgs e)
        {
            int numeroVolume = 1;
            txtNumeroPedido.Text = txtNumeroPedido.Text.Replace(";", "/");
            int.TryParse(txtNumeroPedido.Text.Split('-')[1].Split('/')[0], out numeroVolume);
            int numeroPedidoDigitado = 0;
            int.TryParse(txtNumeroPedido.Text.Split('-')[0], out numeroPedidoDigitado);
            
            var formPrincipal = (this.Parent.Parent as frmPrincipal);
            if (formPrincipal.idPedidoEmbalagem != Utilidades.retornaIdInterno(numeroPedidoDigitado))
            {
                MessageBox.Show("Pedido inválido");
                mostrarNumeroPedido();
                return;
            }
            if (numeroPedido)
            {
                var validaPacote = 0;
                if(pesos != null) validaPacote = pesos.Where(x => x.volume == numeroVolume).Count();
                if (validaPacote > 0)
                {
                    MessageBox.Show("Volume já embalado");
                    mostrarNumeroPedido();
                }
                else
                {
                    mostrarPesos();
                }
            }
            else
            {
                int peso = Convert.ToInt32(txtPesoPacote.Value);
                int altura = Convert.ToInt32(txtAltura.Value);
                int largura = Convert.ToInt32(txtLargura.Value);
                int profundidade = Convert.ToInt32(txtProfundidade.Value);

                if (peso == 0 | peso < 100 | peso > 100000)
                {
                    MessageBox.Show("Peso inválido.");
                    mostrarPesos();
                    return;
                }
                var pesosPacote = new wsEmbalagem.pesosPacotesGravar();
                pesosPacote.volume = numeroVolume;
                pesosPacote.peso = peso;
                pesosPacote.altura = altura;
                pesosPacote.largura = largura;
                pesosPacote.profundidade = profundidade;
                pesos.Add(pesosPacote);
                if (pesos.Count < totalVolumes)
                {
                    mostrarNumeroPedido();
                }
                else
                {
                    var embalarWs = new wsEmbalagem.EmbalagemWs();
                    var gravarPesos = embalarWs.GravaPesos(formPrincipal.idUsuarioExpedicao, formPrincipal.idPedidoEmbalagem, pesos.ToArray());
                    if (gravarPesos.ToLower() == "pac" | gravarPesos.ToLower() == "sedex")
                    {
                        formPrincipal.carregaEmbalarRastreio();
                    }
                    else
                    {
                        formPrincipal.carregaEnviarPor(gravarPesos);
                    }
                }
            }
        }

        private void txtNumeroPedido_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnConfirmar.PerformClick();
            }
        }

        private void txtPesoPacote_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtLargura.Text = "1";
                txtAltura.Text = "1";
                txtProfundidade.Text = "1";
                btnConfirmar.PerformClick();
                //txtLargura.Focus();
            }
        }

        private void txtLargura_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtAltura.Focus();
            }
        }

        private void txtAltura_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtProfundidade.Focus();
            }
        }

        private void txtProfundidade_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnConfirmar.PerformClick();
            }
        }
    }
}
