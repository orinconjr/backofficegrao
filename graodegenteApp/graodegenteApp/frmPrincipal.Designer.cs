﻿namespace graodegenteApp
{
    partial class frmPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.telerikMetroBlueTheme1 = new Telerik.WinControls.Themes.TelerikMetroBlueTheme();
            this.radRibbonBar1 = new Telerik.WinControls.UI.RadRibbonBar();
            this.ribbonTab1 = new Telerik.WinControls.UI.RibbonTab();
            this.radRibbonBarGroup1 = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.radRibbonBarButtonGroup1 = new Telerik.WinControls.UI.RadRibbonBarButtonGroup();
            this.btnImprimirEtiquetasRomaneio = new Telerik.WinControls.UI.RadButtonElement();
            this.btnEntradaRomaneio = new Telerik.WinControls.UI.RadButtonElement();
            this.btnImprimirEtiquetaAvulsa = new Telerik.WinControls.UI.RadButtonElement();
            this.btnConsultarRomaneio = new Telerik.WinControls.UI.RadButtonElement();
            this.btnChecarEstoque = new Telerik.WinControls.UI.RadButtonElement();
            this.btnEtiquetaEnderecamento = new Telerik.WinControls.UI.RadButtonElement();
            this.btnGerarEtiquetaProdutoEnviado = new Telerik.WinControls.UI.RadButtonElement();
            this.ribbonTab2 = new Telerik.WinControls.UI.RibbonTab();
            this.radRibbonBarGroup2 = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.radRibbonBarButtonGroup3 = new Telerik.WinControls.UI.RadRibbonBarButtonGroup();
            this.btnEmbalarPedido = new Telerik.WinControls.UI.RadButtonElement();
            this.btnListaDeProdutos = new Telerik.WinControls.UI.RadButtonElement();
            this.btnQuantidadePacotes = new Telerik.WinControls.UI.RadButtonElement();
            this.btnReimprimirEtiquetas = new Telerik.WinControls.UI.RadButtonElement();
            this.radRibbonBarGroup14 = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.radRibbonBarButtonGroup16 = new Telerik.WinControls.UI.RadRibbonBarButtonGroup();
            this.radButtonElement25 = new Telerik.WinControls.UI.RadButtonElement();
            this.ribbonTab3 = new Telerik.WinControls.UI.RibbonTab();
            this.radRibbonBarGroup3 = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.radRibbonBarButtonGroup8 = new Telerik.WinControls.UI.RadRibbonBarButtonGroup();
            this.btnEmissaoJadlog = new Telerik.WinControls.UI.RadButtonElement();
            this.radRibbonBarGroup10 = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.radRibbonBarButtonGroup12 = new Telerik.WinControls.UI.RadRibbonBarButtonGroup();
            this.radButtonElement23 = new Telerik.WinControls.UI.RadButtonElement();
            this.radRibbonBarGroup11 = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.radRibbonBarButtonGroup13 = new Telerik.WinControls.UI.RadRibbonBarButtonGroup();
            this.btnCarregamentoCorreios = new Telerik.WinControls.UI.RadButtonElement();
            this.radRibbonBarGroup12 = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.radRibbonBarButtonGroup14 = new Telerik.WinControls.UI.RadRibbonBarButtonGroup();
            this.radButtonElement24 = new Telerik.WinControls.UI.RadButtonElement();
            this.radRibbonBarGroup13 = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.radRibbonBarButtonGroup15 = new Telerik.WinControls.UI.RadRibbonBarButtonGroup();
            this.btnCarregamentoCaminhao = new Telerik.WinControls.UI.RadButtonElement();
            this.btnConfiguracoes = new Telerik.WinControls.UI.RadMenuItem();
            this.panel1 = new System.Windows.Forms.Panel();
            this.radRibbonBarButtonGroup4 = new Telerik.WinControls.UI.RadRibbonBarButtonGroup();
            this.radButtonElement1 = new Telerik.WinControls.UI.RadButtonElement();
            this.radButtonElement2 = new Telerik.WinControls.UI.RadButtonElement();
            this.radButtonElement3 = new Telerik.WinControls.UI.RadButtonElement();
            this.radRibbonBarButtonGroup5 = new Telerik.WinControls.UI.RadRibbonBarButtonGroup();
            this.radButtonElement4 = new Telerik.WinControls.UI.RadButtonElement();
            this.radButtonElement5 = new Telerik.WinControls.UI.RadButtonElement();
            this.radButtonElement6 = new Telerik.WinControls.UI.RadButtonElement();
            this.radRibbonBarGroup4 = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.radRibbonBarButtonGroup2 = new Telerik.WinControls.UI.RadRibbonBarButtonGroup();
            this.radButtonElement7 = new Telerik.WinControls.UI.RadButtonElement();
            this.radButtonElement8 = new Telerik.WinControls.UI.RadButtonElement();
            this.radButtonElement9 = new Telerik.WinControls.UI.RadButtonElement();
            this.radRibbonBarGroup5 = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.radRibbonBarButtonGroup6 = new Telerik.WinControls.UI.RadRibbonBarButtonGroup();
            this.radButtonElement10 = new Telerik.WinControls.UI.RadButtonElement();
            this.radButtonElement11 = new Telerik.WinControls.UI.RadButtonElement();
            this.radButtonElement12 = new Telerik.WinControls.UI.RadButtonElement();
            this.radRibbonBarGroup6 = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.radRibbonBarButtonGroup7 = new Telerik.WinControls.UI.RadRibbonBarButtonGroup();
            this.radButtonElement13 = new Telerik.WinControls.UI.RadButtonElement();
            this.radButtonElement14 = new Telerik.WinControls.UI.RadButtonElement();
            this.radButtonElement15 = new Telerik.WinControls.UI.RadButtonElement();
            this.radButtonElement16 = new Telerik.WinControls.UI.RadButtonElement();
            this.radRibbonBarGroup7 = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.radRibbonBarButtonGroup9 = new Telerik.WinControls.UI.RadRibbonBarButtonGroup();
            this.radButtonElement17 = new Telerik.WinControls.UI.RadButtonElement();
            this.radButtonElement18 = new Telerik.WinControls.UI.RadButtonElement();
            this.radRibbonBarGroup8 = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.radRibbonBarButtonGroup10 = new Telerik.WinControls.UI.RadRibbonBarButtonGroup();
            this.radButtonElement19 = new Telerik.WinControls.UI.RadButtonElement();
            this.radButtonElement20 = new Telerik.WinControls.UI.RadButtonElement();
            this.radRibbonBarGroup9 = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.radRibbonBarButtonGroup11 = new Telerik.WinControls.UI.RadRibbonBarButtonGroup();
            this.radButtonElement21 = new Telerik.WinControls.UI.RadButtonElement();
            this.radButtonElement22 = new Telerik.WinControls.UI.RadButtonElement();
            ((System.ComponentModel.ISupportInitialize)(this.radRibbonBar1)).BeginInit();
            this.SuspendLayout();
            // 
            // radRibbonBar1
            // 
            this.radRibbonBar1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radRibbonBar1.CommandTabs.AddRange(new Telerik.WinControls.RadItem[] {
            this.ribbonTab1,
            this.ribbonTab2,
            this.ribbonTab3});
            // 
            // 
            // 
            this.radRibbonBar1.ExitButton.Text = "Exit";
            this.radRibbonBar1.Location = new System.Drawing.Point(0, 0);
            this.radRibbonBar1.Name = "radRibbonBar1";
            // 
            // 
            // 
            this.radRibbonBar1.OptionsButton.Text = "Options";
            // 
            // 
            // 
            this.radRibbonBar1.RootElement.ControlBounds = new System.Drawing.Rectangle(0, 0, 400, 108);
            this.radRibbonBar1.RootElement.StretchVertically = true;
            this.radRibbonBar1.Size = new System.Drawing.Size(1004, 158);
            this.radRibbonBar1.StartMenuItems.AddRange(new Telerik.WinControls.RadItem[] {
            this.btnConfiguracoes});
            this.radRibbonBar1.TabIndex = 0;
            this.radRibbonBar1.Text = "Sistema Grão de Gente";
            // 
            // ribbonTab1
            // 
            this.ribbonTab1.AccessibleDescription = "Sistema";
            this.ribbonTab1.AccessibleName = "Sistema";
            this.ribbonTab1.IsSelected = false;
            this.ribbonTab1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radRibbonBarGroup1});
            this.ribbonTab1.Name = "ribbonTab1";
            this.ribbonTab1.Text = "Estoque";
            // 
            // radRibbonBarGroup1
            // 
            this.radRibbonBarGroup1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radRibbonBarButtonGroup1});
            this.radRibbonBarGroup1.Name = "radRibbonBarGroup1";
            this.radRibbonBarGroup1.Text = "Romaneios";
            // 
            // radRibbonBarButtonGroup1
            // 
            this.radRibbonBarButtonGroup1.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.radRibbonBarButtonGroup1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.btnImprimirEtiquetasRomaneio,
            this.btnEntradaRomaneio,
            this.btnImprimirEtiquetaAvulsa,
            this.btnConsultarRomaneio,
            this.btnChecarEstoque,
            this.btnEtiquetaEnderecamento,
            this.btnGerarEtiquetaProdutoEnviado});
            this.radRibbonBarButtonGroup1.Name = "radRibbonBarButtonGroup1";
            this.radRibbonBarButtonGroup1.StretchHorizontally = true;
            this.radRibbonBarButtonGroup1.StretchVertically = true;
            this.radRibbonBarButtonGroup1.Text = "radRibbonBarButtonGroup1";
            // 
            // btnImprimirEtiquetasRomaneio
            // 
            this.btnImprimirEtiquetasRomaneio.Name = "btnImprimirEtiquetasRomaneio";
            this.btnImprimirEtiquetasRomaneio.Text = "Imprimir Etiquetas do Romaneio";
            this.btnImprimirEtiquetasRomaneio.Click += new System.EventHandler(this.btnImprimirEtiquetasRomaneio_Click);
            // 
            // btnEntradaRomaneio
            // 
            this.btnEntradaRomaneio.Name = "btnEntradaRomaneio";
            this.btnEntradaRomaneio.Text = "Entrada de Romaneio";
            this.btnEntradaRomaneio.Click += new System.EventHandler(this.btnEntradaRomaneio_Click);
            // 
            // btnImprimirEtiquetaAvulsa
            // 
            this.btnImprimirEtiquetaAvulsa.Name = "btnImprimirEtiquetaAvulsa";
            this.btnImprimirEtiquetaAvulsa.Text = "Imprimir Etiqueta Avulsa";
            this.btnImprimirEtiquetaAvulsa.Click += new System.EventHandler(this.btnImprimirEtiquetaAvulsa_Click);
            // 
            // btnConsultarRomaneio
            // 
            this.btnConsultarRomaneio.Name = "btnConsultarRomaneio";
            this.btnConsultarRomaneio.Text = "Consultar Romaneio";
            this.btnConsultarRomaneio.Click += new System.EventHandler(this.btnConsultarRomaneio_Click);
            // 
            // btnChecarEstoque
            // 
            this.btnChecarEstoque.Name = "btnChecarEstoque";
            this.btnChecarEstoque.Text = "Conferencia de Estoque";
            this.btnChecarEstoque.Click += new System.EventHandler(this.btnChecarEstoque_Click);
            // 
            // btnEtiquetaEnderecamento
            // 
            this.btnEtiquetaEnderecamento.AccessibleDescription = "Aguardando Liberação";
            this.btnEtiquetaEnderecamento.AccessibleName = "Aguardando Liberação";
            this.btnEtiquetaEnderecamento.Name = "btnEtiquetaEnderecamento";
            this.btnEtiquetaEnderecamento.ShowBorder = false;
            this.btnEtiquetaEnderecamento.Text = "Etiqueta Endereçamento";
            this.btnEtiquetaEnderecamento.Click += new System.EventHandler(this.btnAguardandoLiberacao_Click);
            // 
            // btnGerarEtiquetaProdutoEnviado
            // 
            this.btnGerarEtiquetaProdutoEnviado.Name = "btnGerarEtiquetaProdutoEnviado";
            this.btnGerarEtiquetaProdutoEnviado.Text = "Gerar Etiqueta de Produto Enviado";
            this.btnGerarEtiquetaProdutoEnviado.Click += new System.EventHandler(this.btnGerarEtiquetaProdutoEnviado_Click);
            // 
            // ribbonTab2
            // 
            this.ribbonTab2.IsSelected = false;
            this.ribbonTab2.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radRibbonBarGroup2,
            this.radRibbonBarGroup14});
            this.ribbonTab2.Name = "ribbonTab2";
            this.ribbonTab2.Text = "Embalagem";
            // 
            // radRibbonBarGroup2
            // 
            this.radRibbonBarGroup2.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radRibbonBarButtonGroup3});
            this.radRibbonBarGroup2.Name = "radRibbonBarGroup2";
            this.radRibbonBarGroup2.Text = "Embalagem";
            // 
            // radRibbonBarButtonGroup3
            // 
            this.radRibbonBarButtonGroup3.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.btnEmbalarPedido,
            this.btnListaDeProdutos,
            this.btnQuantidadePacotes,
            this.btnReimprimirEtiquetas});
            this.radRibbonBarButtonGroup3.Name = "radRibbonBarButtonGroup3";
            this.radRibbonBarButtonGroup3.StretchVertically = true;
            this.radRibbonBarButtonGroup3.Text = "radRibbonBarButtonGroup3";
            // 
            // btnEmbalarPedido
            // 
            this.btnEmbalarPedido.Name = "btnEmbalarPedido";
            this.btnEmbalarPedido.Text = "Embalar Pedidos";
            this.btnEmbalarPedido.Click += new System.EventHandler(this.btnEmbalarPedido_Click);
            // 
            // btnListaDeProdutos
            // 
            this.btnListaDeProdutos.Enabled = false;
            this.btnListaDeProdutos.Name = "btnListaDeProdutos";
            this.btnListaDeProdutos.Text = "Lista de Produtos";
            this.btnListaDeProdutos.Click += new System.EventHandler(this.btnListaDeProdutos_Click);
            // 
            // btnQuantidadePacotes
            // 
            this.btnQuantidadePacotes.Enabled = false;
            this.btnQuantidadePacotes.Name = "btnQuantidadePacotes";
            this.btnQuantidadePacotes.Text = "Quantidade de Pacotes";
            this.btnQuantidadePacotes.Click += new System.EventHandler(this.btnQuantidadePacotes_Click);
            // 
            // btnReimprimirEtiquetas
            // 
            this.btnReimprimirEtiquetas.Name = "btnReimprimirEtiquetas";
            this.btnReimprimirEtiquetas.Text = "Reimprimir Etiquetas";
            this.btnReimprimirEtiquetas.Click += new System.EventHandler(this.btnReimprimirEtiquetas_Click);
            // 
            // radRibbonBarGroup14
            // 
            this.radRibbonBarGroup14.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radRibbonBarButtonGroup16});
            this.radRibbonBarGroup14.Name = "radRibbonBarGroup14";
            this.radRibbonBarGroup14.Text = "Alterar Quantidade de Pacotes";
            // 
            // radRibbonBarButtonGroup16
            // 
            this.radRibbonBarButtonGroup16.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radButtonElement25});
            this.radRibbonBarButtonGroup16.Name = "radRibbonBarButtonGroup16";
            this.radRibbonBarButtonGroup16.StretchVertically = true;
            this.radRibbonBarButtonGroup16.Text = "radRibbonBarButtonGroup16";
            // 
            // radButtonElement25
            // 
            this.radButtonElement25.Name = "radButtonElement25";
            this.radButtonElement25.Text = "Alterar Quantidade de Pacote";
            this.radButtonElement25.Click += new System.EventHandler(this.radButtonElement25_Click);
            // 
            // ribbonTab3
            // 
            this.ribbonTab3.IsSelected = true;
            this.ribbonTab3.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radRibbonBarGroup3,
            this.radRibbonBarGroup10,
            this.radRibbonBarGroup11,
            this.radRibbonBarGroup12,
            this.radRibbonBarGroup13});
            this.ribbonTab3.Name = "ribbonTab3";
            this.ribbonTab3.Text = "Emissão";
            this.ribbonTab3.Click += new System.EventHandler(this.ribbonTab3_Click);
            // 
            // radRibbonBarGroup3
            // 
            this.radRibbonBarGroup3.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radRibbonBarButtonGroup8});
            this.radRibbonBarGroup3.Name = "radRibbonBarGroup3";
            this.radRibbonBarGroup3.Text = "Jadlog";
            // 
            // radRibbonBarButtonGroup8
            // 
            this.radRibbonBarButtonGroup8.Alignment = System.Drawing.ContentAlignment.TopLeft;
            this.radRibbonBarButtonGroup8.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.btnEmissaoJadlog});
            this.radRibbonBarButtonGroup8.Name = "radRibbonBarButtonGroup8";
            this.radRibbonBarButtonGroup8.StretchVertically = true;
            this.radRibbonBarButtonGroup8.Text = "radRibbonBarButtonGroup8";
            // 
            // btnEmissaoJadlog
            // 
            this.btnEmissaoJadlog.Name = "btnEmissaoJadlog";
            this.btnEmissaoJadlog.Text = "Emissão Jadlog";
            this.btnEmissaoJadlog.Click += new System.EventHandler(this.btnEmissaoJadlog_Click);
            // 
            // radRibbonBarGroup10
            // 
            this.radRibbonBarGroup10.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radRibbonBarButtonGroup12});
            this.radRibbonBarGroup10.Name = "radRibbonBarGroup10";
            this.radRibbonBarGroup10.Text = "TNT";
            // 
            // radRibbonBarButtonGroup12
            // 
            this.radRibbonBarButtonGroup12.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radButtonElement23});
            this.radRibbonBarButtonGroup12.Name = "radRibbonBarButtonGroup12";
            this.radRibbonBarButtonGroup12.StretchVertically = true;
            this.radRibbonBarButtonGroup12.Text = "radRibbonBarButtonGroup12";
            // 
            // radButtonElement23
            // 
            this.radButtonElement23.AccessibleDescription = "Carregamento TNT";
            this.radButtonElement23.AccessibleName = "Carregamento TNT";
            this.radButtonElement23.Name = "radButtonElement23";
            this.radButtonElement23.Text = "Emissão TNT";
            this.radButtonElement23.Click += new System.EventHandler(this.radButtonElement23_Click);
            // 
            // radRibbonBarGroup11
            // 
            this.radRibbonBarGroup11.AccessibleDescription = "Carregamento Correios";
            this.radRibbonBarGroup11.AccessibleName = "Carregamento Correios";
            this.radRibbonBarGroup11.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radRibbonBarButtonGroup13});
            this.radRibbonBarGroup11.Name = "radRibbonBarGroup11";
            this.radRibbonBarGroup11.Text = "Correios";
            // 
            // radRibbonBarButtonGroup13
            // 
            this.radRibbonBarButtonGroup13.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.btnCarregamentoCorreios});
            this.radRibbonBarButtonGroup13.Name = "radRibbonBarButtonGroup13";
            this.radRibbonBarButtonGroup13.StretchVertically = true;
            this.radRibbonBarButtonGroup13.Text = "radRibbonBarButtonGroup13";
            // 
            // btnCarregamentoCorreios
            // 
            this.btnCarregamentoCorreios.AccessibleDescription = "Carregamento Correios";
            this.btnCarregamentoCorreios.AccessibleName = "Carregamento Correios";
            this.btnCarregamentoCorreios.Name = "btnCarregamentoCorreios";
            this.btnCarregamentoCorreios.Text = "Emissão Correios";
            this.btnCarregamentoCorreios.Click += new System.EventHandler(this.btnCarregamentoCorreios_Click);
            // 
            // radRibbonBarGroup12
            // 
            this.radRibbonBarGroup12.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radRibbonBarButtonGroup14});
            this.radRibbonBarGroup12.Name = "radRibbonBarGroup12";
            this.radRibbonBarGroup12.Text = "Pesar";
            // 
            // radRibbonBarButtonGroup14
            // 
            this.radRibbonBarButtonGroup14.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radButtonElement24});
            this.radRibbonBarButtonGroup14.Name = "radRibbonBarButtonGroup14";
            this.radRibbonBarButtonGroup14.StretchVertically = true;
            this.radRibbonBarButtonGroup14.Text = "radRibbonBarButtonGroup14";
            // 
            // radButtonElement24
            // 
            this.radButtonElement24.Name = "radButtonElement24";
            this.radButtonElement24.Text = "Pesar Pedido";
            this.radButtonElement24.Click += new System.EventHandler(this.radButtonElement24_Click);
            // 
            // radRibbonBarGroup13
            // 
            this.radRibbonBarGroup13.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radRibbonBarButtonGroup15});
            this.radRibbonBarGroup13.Name = "radRibbonBarGroup13";
            this.radRibbonBarGroup13.Text = "Carregamento Caminhão";
            // 
            // radRibbonBarButtonGroup15
            // 
            this.radRibbonBarButtonGroup15.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.btnCarregamentoCaminhao});
            this.radRibbonBarButtonGroup15.Name = "radRibbonBarButtonGroup15";
            this.radRibbonBarButtonGroup15.StretchVertically = true;
            this.radRibbonBarButtonGroup15.Text = "radRibbonBarButtonGroup15";
            // 
            // btnCarregamentoCaminhao
            // 
            this.btnCarregamentoCaminhao.Name = "btnCarregamentoCaminhao";
            this.btnCarregamentoCaminhao.Text = "Carregamento Caminhão";
            this.btnCarregamentoCaminhao.Click += new System.EventHandler(this.btnCarregamentoCaminhao_Click);
            // 
            // btnConfiguracoes
            // 
            this.btnConfiguracoes.Name = "btnConfiguracoes";
            this.btnConfiguracoes.Text = "Configuracoes";
            this.btnConfiguracoes.Click += new System.EventHandler(this.btnConfiguracoes_Click);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Location = new System.Drawing.Point(0, 156);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1004, 350);
            this.panel1.TabIndex = 1;
            // 
            // radRibbonBarButtonGroup4
            // 
            this.radRibbonBarButtonGroup4.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radButtonElement1,
            this.radButtonElement2,
            this.radButtonElement3});
            this.radRibbonBarButtonGroup4.Name = "radRibbonBarButtonGroup4";
            this.radRibbonBarButtonGroup4.StretchVertically = true;
            this.radRibbonBarButtonGroup4.Text = "radRibbonBarButtonGroup3";
            // 
            // radButtonElement1
            // 
            this.radButtonElement1.Name = "radButtonElement1";
            this.radButtonElement1.Text = "Embalar Pedidos";
            // 
            // radButtonElement2
            // 
            this.radButtonElement2.Enabled = false;
            this.radButtonElement2.Name = "radButtonElement2";
            this.radButtonElement2.Text = "Lista de Produtos";
            // 
            // radButtonElement3
            // 
            this.radButtonElement3.Enabled = false;
            this.radButtonElement3.Name = "radButtonElement3";
            this.radButtonElement3.Text = "Quantidade de Pacotes";
            // 
            // radRibbonBarButtonGroup5
            // 
            this.radRibbonBarButtonGroup5.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radButtonElement4,
            this.radButtonElement5,
            this.radButtonElement6});
            this.radRibbonBarButtonGroup5.Name = "radRibbonBarButtonGroup5";
            this.radRibbonBarButtonGroup5.StretchVertically = true;
            this.radRibbonBarButtonGroup5.Text = "radRibbonBarButtonGroup3";
            // 
            // radButtonElement4
            // 
            this.radButtonElement4.Name = "radButtonElement4";
            this.radButtonElement4.Text = "Embalar Pedidos";
            // 
            // radButtonElement5
            // 
            this.radButtonElement5.Enabled = false;
            this.radButtonElement5.Name = "radButtonElement5";
            this.radButtonElement5.Text = "Lista de Produtos";
            // 
            // radButtonElement6
            // 
            this.radButtonElement6.Enabled = false;
            this.radButtonElement6.Name = "radButtonElement6";
            this.radButtonElement6.Text = "Quantidade de Pacotes";
            // 
            // radRibbonBarGroup4
            // 
            this.radRibbonBarGroup4.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radRibbonBarButtonGroup2});
            this.radRibbonBarGroup4.Margin = new System.Windows.Forms.Padding(0);
            this.radRibbonBarGroup4.MaxSize = new System.Drawing.Size(0, 0);
            this.radRibbonBarGroup4.MinSize = new System.Drawing.Size(0, 0);
            this.radRibbonBarGroup4.Name = "radRibbonBarGroup4";
            this.radRibbonBarGroup4.Text = "Embalagem";
            // 
            // radRibbonBarButtonGroup2
            // 
            this.radRibbonBarButtonGroup2.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radButtonElement7,
            this.radButtonElement8,
            this.radButtonElement9});
            this.radRibbonBarButtonGroup2.Name = "radRibbonBarButtonGroup2";
            this.radRibbonBarButtonGroup2.StretchVertically = true;
            this.radRibbonBarButtonGroup2.Text = "radRibbonBarButtonGroup3";
            // 
            // radButtonElement7
            // 
            this.radButtonElement7.Name = "radButtonElement7";
            this.radButtonElement7.Text = "Embalar Pedidos";
            // 
            // radButtonElement8
            // 
            this.radButtonElement8.Enabled = false;
            this.radButtonElement8.Name = "radButtonElement8";
            this.radButtonElement8.Text = "Lista de Produtos";
            // 
            // radButtonElement9
            // 
            this.radButtonElement9.Enabled = false;
            this.radButtonElement9.Name = "radButtonElement9";
            this.radButtonElement9.Text = "Quantidade de Pacotes";
            // 
            // radRibbonBarGroup5
            // 
            this.radRibbonBarGroup5.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radRibbonBarButtonGroup6});
            this.radRibbonBarGroup5.Margin = new System.Windows.Forms.Padding(0);
            this.radRibbonBarGroup5.MaxSize = new System.Drawing.Size(0, 0);
            this.radRibbonBarGroup5.MinSize = new System.Drawing.Size(0, 0);
            this.radRibbonBarGroup5.Name = "radRibbonBarGroup5";
            this.radRibbonBarGroup5.Text = "Embalagem";
            // 
            // radRibbonBarButtonGroup6
            // 
            this.radRibbonBarButtonGroup6.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radButtonElement10,
            this.radButtonElement11,
            this.radButtonElement12});
            this.radRibbonBarButtonGroup6.Name = "radRibbonBarButtonGroup6";
            this.radRibbonBarButtonGroup6.StretchVertically = true;
            this.radRibbonBarButtonGroup6.Text = "radRibbonBarButtonGroup3";
            // 
            // radButtonElement10
            // 
            this.radButtonElement10.Name = "radButtonElement10";
            this.radButtonElement10.Text = "Embalar Pedidos";
            // 
            // radButtonElement11
            // 
            this.radButtonElement11.Enabled = false;
            this.radButtonElement11.Name = "radButtonElement11";
            this.radButtonElement11.Text = "Lista de Produtos";
            // 
            // radButtonElement12
            // 
            this.radButtonElement12.Enabled = false;
            this.radButtonElement12.Name = "radButtonElement12";
            this.radButtonElement12.Text = "Quantidade de Pacotes";
            // 
            // radRibbonBarGroup6
            // 
            this.radRibbonBarGroup6.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radRibbonBarButtonGroup7});
            this.radRibbonBarGroup6.Margin = new System.Windows.Forms.Padding(0);
            this.radRibbonBarGroup6.MaxSize = new System.Drawing.Size(0, 0);
            this.radRibbonBarGroup6.MinSize = new System.Drawing.Size(0, 0);
            this.radRibbonBarGroup6.Name = "radRibbonBarGroup6";
            this.radRibbonBarGroup6.Text = "Embalagem";
            // 
            // radRibbonBarButtonGroup7
            // 
            this.radRibbonBarButtonGroup7.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radButtonElement13,
            this.radButtonElement14,
            this.radButtonElement15});
            this.radRibbonBarButtonGroup7.Name = "radRibbonBarButtonGroup7";
            this.radRibbonBarButtonGroup7.StretchVertically = true;
            this.radRibbonBarButtonGroup7.Text = "radRibbonBarButtonGroup3";
            // 
            // radButtonElement13
            // 
            this.radButtonElement13.Name = "radButtonElement13";
            this.radButtonElement13.Text = "Embalar Pedidos";
            // 
            // radButtonElement14
            // 
            this.radButtonElement14.Enabled = false;
            this.radButtonElement14.Name = "radButtonElement14";
            this.radButtonElement14.Text = "Lista de Produtos";
            // 
            // radButtonElement15
            // 
            this.radButtonElement15.Enabled = false;
            this.radButtonElement15.Name = "radButtonElement15";
            this.radButtonElement15.Text = "Quantidade de Pacotes";
            // 
            // radButtonElement16
            // 
            this.radButtonElement16.Name = "radButtonElement16";
            this.radButtonElement16.Text = "Emissao CD2";
            // 
            // radRibbonBarGroup7
            // 
            this.radRibbonBarGroup7.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radRibbonBarButtonGroup9});
            this.radRibbonBarGroup7.Margin = new System.Windows.Forms.Padding(0);
            this.radRibbonBarGroup7.MaxSize = new System.Drawing.Size(0, 0);
            this.radRibbonBarGroup7.MinSize = new System.Drawing.Size(0, 0);
            this.radRibbonBarGroup7.Name = "radRibbonBarGroup7";
            this.radRibbonBarGroup7.Text = "Jadlog";
            // 
            // radRibbonBarButtonGroup9
            // 
            this.radRibbonBarButtonGroup9.Alignment = System.Drawing.ContentAlignment.TopLeft;
            this.radRibbonBarButtonGroup9.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radButtonElement17,
            this.radButtonElement18});
            this.radRibbonBarButtonGroup9.Name = "radRibbonBarButtonGroup9";
            this.radRibbonBarButtonGroup9.StretchVertically = true;
            this.radRibbonBarButtonGroup9.Text = "radRibbonBarButtonGroup8";
            // 
            // radButtonElement17
            // 
            this.radButtonElement17.Name = "radButtonElement17";
            this.radButtonElement17.Text = "Emissão Jadlog";
            // 
            // radButtonElement18
            // 
            this.radButtonElement18.Name = "radButtonElement18";
            this.radButtonElement18.Text = "Emissao CD2";
            // 
            // radRibbonBarGroup8
            // 
            this.radRibbonBarGroup8.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radRibbonBarButtonGroup10});
            this.radRibbonBarGroup8.Margin = new System.Windows.Forms.Padding(0);
            this.radRibbonBarGroup8.MaxSize = new System.Drawing.Size(0, 0);
            this.radRibbonBarGroup8.MinSize = new System.Drawing.Size(0, 0);
            this.radRibbonBarGroup8.Name = "radRibbonBarGroup8";
            this.radRibbonBarGroup8.Text = "Jadlog";
            // 
            // radRibbonBarButtonGroup10
            // 
            this.radRibbonBarButtonGroup10.Alignment = System.Drawing.ContentAlignment.TopLeft;
            this.radRibbonBarButtonGroup10.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radButtonElement19,
            this.radButtonElement20});
            this.radRibbonBarButtonGroup10.Name = "radRibbonBarButtonGroup10";
            this.radRibbonBarButtonGroup10.StretchVertically = true;
            this.radRibbonBarButtonGroup10.Text = "radRibbonBarButtonGroup8";
            // 
            // radButtonElement19
            // 
            this.radButtonElement19.Name = "radButtonElement19";
            this.radButtonElement19.Text = "Emissão Jadlog";
            // 
            // radButtonElement20
            // 
            this.radButtonElement20.Name = "radButtonElement20";
            this.radButtonElement20.Text = "Emissao CD2";
            // 
            // radRibbonBarGroup9
            // 
            this.radRibbonBarGroup9.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radRibbonBarButtonGroup11});
            this.radRibbonBarGroup9.Margin = new System.Windows.Forms.Padding(0);
            this.radRibbonBarGroup9.MaxSize = new System.Drawing.Size(0, 0);
            this.radRibbonBarGroup9.MinSize = new System.Drawing.Size(0, 0);
            this.radRibbonBarGroup9.Name = "radRibbonBarGroup9";
            this.radRibbonBarGroup9.Text = "Jadlog";
            // 
            // radRibbonBarButtonGroup11
            // 
            this.radRibbonBarButtonGroup11.Alignment = System.Drawing.ContentAlignment.TopLeft;
            this.radRibbonBarButtonGroup11.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radButtonElement21,
            this.radButtonElement22});
            this.radRibbonBarButtonGroup11.Name = "radRibbonBarButtonGroup11";
            this.radRibbonBarButtonGroup11.StretchVertically = true;
            this.radRibbonBarButtonGroup11.Text = "radRibbonBarButtonGroup8";
            // 
            // radButtonElement21
            // 
            this.radButtonElement21.Name = "radButtonElement21";
            this.radButtonElement21.Text = "Emissão Jadlog";
            // 
            // radButtonElement22
            // 
            this.radButtonElement22.Name = "radButtonElement22";
            this.radButtonElement22.Text = "Emissao CD2";
            // 
            // frmPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Azure;
            this.ClientSize = new System.Drawing.Size(1004, 509);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.radRibbonBar1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmPrincipal";
            this.Text = "Sistema Grão de Gente";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmPrincipal_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radRibbonBar1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.Themes.TelerikMetroBlueTheme telerikMetroBlueTheme1;
        private Telerik.WinControls.UI.RadRibbonBar radRibbonBar1;
        private Telerik.WinControls.UI.RibbonTab ribbonTab1;
        private Telerik.WinControls.UI.RadRibbonBarGroup radRibbonBarGroup1;
        private Telerik.WinControls.UI.RadRibbonBarButtonGroup radRibbonBarButtonGroup1;
        private Telerik.WinControls.UI.RadButtonElement btnImprimirEtiquetasRomaneio;
        private Telerik.WinControls.UI.RadButtonElement btnImprimirEtiquetaAvulsa;
        private Telerik.WinControls.UI.RadButtonElement btnEntradaRomaneio;
        private Telerik.WinControls.UI.RibbonTab ribbonTab2;
        private Telerik.WinControls.UI.RadRibbonBarGroup radRibbonBarGroup2;
        private Telerik.WinControls.UI.RadButtonElement btnConsultarRomaneio;
        private Telerik.WinControls.UI.RadButtonElement btnEtiquetaEnderecamento;
        private Telerik.WinControls.UI.RadButtonElement btnChecarEstoque;
        private System.Windows.Forms.Panel panel1;
        private Telerik.WinControls.UI.RadRibbonBarButtonGroup radRibbonBarButtonGroup3;
        private Telerik.WinControls.UI.RadButtonElement btnEmbalarPedido;
        private Telerik.WinControls.UI.RadButtonElement btnListaDeProdutos;
        private Telerik.WinControls.UI.RadButtonElement btnQuantidadePacotes;
        private Telerik.WinControls.UI.RibbonTab ribbonTab3;
        private Telerik.WinControls.UI.RadRibbonBarButtonGroup radRibbonBarButtonGroup4;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement1;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement2;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement3;
        private Telerik.WinControls.UI.RadRibbonBarButtonGroup radRibbonBarButtonGroup5;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement4;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement5;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement6;
        private Telerik.WinControls.UI.RadRibbonBarGroup radRibbonBarGroup4;
        private Telerik.WinControls.UI.RadRibbonBarButtonGroup radRibbonBarButtonGroup2;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement7;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement8;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement9;
        private Telerik.WinControls.UI.RadRibbonBarGroup radRibbonBarGroup5;
        private Telerik.WinControls.UI.RadRibbonBarButtonGroup radRibbonBarButtonGroup6;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement10;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement11;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement12;
        private Telerik.WinControls.UI.RadRibbonBarGroup radRibbonBarGroup6;
        private Telerik.WinControls.UI.RadRibbonBarButtonGroup radRibbonBarButtonGroup7;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement13;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement14;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement15;
        private Telerik.WinControls.UI.RadRibbonBarGroup radRibbonBarGroup3;
        private Telerik.WinControls.UI.RadRibbonBarButtonGroup radRibbonBarButtonGroup8;
        private Telerik.WinControls.UI.RadButtonElement btnEmissaoJadlog;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement16;
        private Telerik.WinControls.UI.RadRibbonBarGroup radRibbonBarGroup10;
        private Telerik.WinControls.UI.RadRibbonBarButtonGroup radRibbonBarButtonGroup12;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement23;
        private Telerik.WinControls.UI.RadRibbonBarGroup radRibbonBarGroup7;
        private Telerik.WinControls.UI.RadRibbonBarButtonGroup radRibbonBarButtonGroup9;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement17;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement18;
        private Telerik.WinControls.UI.RadRibbonBarGroup radRibbonBarGroup8;
        private Telerik.WinControls.UI.RadRibbonBarButtonGroup radRibbonBarButtonGroup10;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement19;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement20;
        private Telerik.WinControls.UI.RadRibbonBarGroup radRibbonBarGroup9;
        private Telerik.WinControls.UI.RadRibbonBarButtonGroup radRibbonBarButtonGroup11;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement21;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement22;
        private Telerik.WinControls.UI.RadButtonElement btnReimprimirEtiquetas;
        private Telerik.WinControls.UI.RadRibbonBarGroup radRibbonBarGroup11;
        private Telerik.WinControls.UI.RadRibbonBarButtonGroup radRibbonBarButtonGroup13;
        private Telerik.WinControls.UI.RadButtonElement btnCarregamentoCorreios;
        private Telerik.WinControls.UI.RadRibbonBarGroup radRibbonBarGroup12;
        private Telerik.WinControls.UI.RadRibbonBarButtonGroup radRibbonBarButtonGroup14;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement24;
        private Telerik.WinControls.UI.RadButtonElement btnGerarEtiquetaProdutoEnviado;
        private Telerik.WinControls.UI.RadRibbonBarGroup radRibbonBarGroup13;
        private Telerik.WinControls.UI.RadRibbonBarButtonGroup radRibbonBarButtonGroup15;
        private Telerik.WinControls.UI.RadButtonElement btnCarregamentoCaminhao;
        private Telerik.WinControls.UI.RadMenuItem btnConfiguracoes;
        private Telerik.WinControls.UI.RadRibbonBarGroup radRibbonBarGroup14;
        private Telerik.WinControls.UI.RadRibbonBarButtonGroup radRibbonBarButtonGroup16;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement25;
    }
}