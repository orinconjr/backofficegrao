﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace graodegenteApp
{
    public partial class usrEmbalarQuantidade : UserControl
    {
        public int idCentroDistribuicao = 1;
        public usrEmbalarQuantidade()
        {
            InitializeComponent();
        }

        private void usrEmbalarQuantidade_Load(object sender, EventArgs e)
        {
            idCentroDistribuicao = Convert.ToInt32(Properties.Settings.Default["cd"].ToString());
            calculaTotais();
        }

        private void btnEmbalarPedido_Click(object sender, EventArgs e)
        {
            var formPrincipal = (this.Parent.Parent as frmPrincipal);
            int idUsuario = formPrincipal.idUsuarioExpedicao;
            var embalarWs = new wsEmbalagem.EmbalagemWs();

            if (idCentroDistribuicao == 1)
            {
                var embalar = embalarWs.SelecionarPedido(idUsuario);
                if (embalar.sucesso)
                {
                    formPrincipal.idPedidoEmbalagem = embalar.pedidoId;
                    formPrincipal.carregaEmbalarListaProdutos();
                }
                else
                {
                    MessageBox.Show("Falha ao selecionar pedido embalagem. Por favor, tente novamente.");
                    calculaTotais();
                }
            }
            else if(idCentroDistribuicao == 2)
            {
                var embalar = embalarWs.SelecionarPedidoCd2(idUsuario);
                if (Convert.ToInt32(embalar.totalVolumes) > 0)
                {
                    formPrincipal.idPedidoEmbalagem = embalar.pedidoId;
                    formPrincipal.carregaEnviarPorCd2(embalar);
                }
                else
                {
                    MessageBox.Show("Falha ao selecionar pedido embalagem. Por favor, tente novamente.");
                    calculaTotais();
                }
            }
            else if (idCentroDistribuicao == 3)
            {
                var embalar = embalarWs.SelecionarPedidoCd3(idUsuario);
                if (Convert.ToInt32(embalar.totalVolumes) > 0)
                {
                    formPrincipal.idPedidoEmbalagem = embalar.pedidoId;
                    formPrincipal.idCentroDistribuicaoEtiqueta = 3;   
                    formPrincipal.carregaEnviarPorCd2(embalar);
                }
                else
                {
                    MessageBox.Show("Falha ao selecionar pedido embalagem. Por favor, tente novamente.");
                    calculaTotais();
                }
            }
            else if (idCentroDistribuicao == 4)
            {
                int computador = 0;
                int.TryParse(Properties.Settings.Default["computador"].ToString(), out computador);
                var embalar = embalarWs.SelecionarPedidoCd4Maquina(idUsuario, computador);
                if (embalar.sucesso)
                {
                    formPrincipal.idPedidoEmbalagem = embalar.pedidoId;
                    formPrincipal.carregaEmbalarListaProdutos();
                }
                else
                {
                    MessageBox.Show("Falha ao selecionar pedido embalagem. Por favor, tente novamente.");
                    calculaTotais();
                }
            }
            else if (idCentroDistribuicao == 5)
            {
                embalarWs.Timeout = 900000;
                var embalar = embalarWs.SelecionarPedidoCd5(idUsuario);
                if (Convert.ToInt32(embalar.totalVolumes) > 0)
                {
                    formPrincipal.idPedidoEmbalagem = embalar.pedidoId;
                    formPrincipal.idCentroDistribuicaoEtiqueta = 5;
                    formPrincipal.carregaEnviarPorCd2(embalar);
                }
                else
                {
                    MessageBox.Show("Falha ao selecionar pedido embalagem. Por favor, tente novamente.");
                    calculaTotais();
                }
            }
        }

        private void calculaTotais()
        {
            if (idCentroDistribuicao == 1)
            {
                var formPrincipal = (this.Parent.Parent as frmPrincipal);
                int idUsuario = formPrincipal.idUsuarioExpedicao;
                var embalarWs = new wsEmbalagem.EmbalagemWs();
                var contagem = embalarWs.ContarPedidos(idUsuario);

                lblQuantidadePedidos.Text = "Existem " + contagem.totalPedidos + " pedidos para serem embalados";
                lblQuantidadeEmbalados.Text = "Você embalou " + contagem.totalEmbalados + " pedidos hoje";
            }
            else if(idCentroDistribuicao == 2)
            {
                var formPrincipal = (this.Parent.Parent as frmPrincipal);
                int idUsuario = formPrincipal.idUsuarioExpedicao;
                var embalarWs = new wsEmbalagem.EmbalagemWs();
                var contagem = embalarWs.ContarPedidosCd2(idUsuario);

                lblQuantidadePedidos.Text = "Existem " + contagem.totalPedidos + " pedidos para serem embalados";
                //lblQuantidadeEmbalados.Text = "Você embalou " + contagem.totalEmbalados + " pedidos hoje";
            }
            else if (idCentroDistribuicao == 3)
            {
                var formPrincipal = (this.Parent.Parent as frmPrincipal);
                int idUsuario = formPrincipal.idUsuarioExpedicao;
                var embalarWs = new wsEmbalagem.EmbalagemWs();
                var contagem = embalarWs.ContarPedidosCd3(idUsuario);

                lblQuantidadePedidos.Text = "Existem " + contagem.totalPedidos + " pedidos para serem embalados";
                //lblQuantidadeEmbalados.Text = "Você embalou " + contagem.totalEmbalados + " pedidos hoje";
            }
            else if (idCentroDistribuicao == 4)
            {
                var formPrincipal = (this.Parent.Parent as frmPrincipal);
                int idUsuario = formPrincipal.idUsuarioExpedicao;
                var embalarWs = new wsEmbalagem.EmbalagemWs();
                var contagem = embalarWs.ContarPedidosCd4(idUsuario);

                lblQuantidadePedidos.Text = "Existem " + contagem.totalPedidos + " pedidos para serem embalados";
                //lblQuantidadeEmbalados.Text = "Você embalou " + contagem.totalEmbalados + " pedidos hoje";
            }
            else if (idCentroDistribuicao == 5)
            {
                var formPrincipal = (this.Parent.Parent as frmPrincipal);
                int idUsuario = formPrincipal.idUsuarioExpedicao;
                var embalarWs = new wsEmbalagem.EmbalagemWs();
                var contagem = embalarWs.ContarPedidosCd5(idUsuario);

                lblQuantidadePedidos.Text = "Existem " + contagem.totalPedidos + " pedidos para serem embalados";
                //lblQuantidadeEmbalados.Text = "Você embalou " + contagem.totalEmbalados + " pedidos hoje";
            }
        }
    }
}
