﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;

namespace graodegenteApp
{
    public partial class frmAguardandoLiberacao : Form
    {
        public int idPedidoFornecedor = 0;
        public frmAguardandoLiberacao()
        {
            ThemeResolutionService.ApplicationThemeName = "TelerikMetroBlue";
            InitializeComponent();
        }

        private void frmAguardandoLiberacao_Load(object sender, EventArgs e)
        {

            var produtos = new wsPedidosFornecedor.PedidosFornecedorWs();
            var itens = produtos.ListaRomaneiosPendentes(clsConfiguracao.ChaveWs);
            radGridView1.DataSource = itens;
            radGridView1.BestFitColumns();
        }

        private void radGridView1_DoubleClick(object sender, EventArgs e)
        {
            var confirmacao = MessageBox.Show("Deseja realmente liberar este romaneio para embalagem e separação?", "Confirmação", MessageBoxButtons.YesNo);
            if (confirmacao == DialogResult.Yes)
            {
                //if (radGridView1.SelectedRows.Count() > 0)
                //{
                //    var cod = radGridView1.SelectedRows[0].Cells[0].Value;

                //    var produtos = new wsPedidosFornecedor.PedidosFornecedorWs();
                //    var liberado = produtos.LiberarRomaneio(Convert.ToInt32(cod), clsConfiguracao.ChaveWs);

                //    if (liberado)
                //    {
                //        var itens = produtos.ListaRomaneiosPendentes(clsConfiguracao.ChaveWs);
                //        radGridView1.DataSource = itens;
                //        radGridView1.BestFitColumns();
                //    }
                //    else
                //    {
                //        MessageBox.Show("Falha ao liberar pedido. Por favor, tente novamente");
                //        var itens = produtos.ListaRomaneiosPendentes(clsConfiguracao.ChaveWs);
                //        radGridView1.DataSource = itens;
                //        radGridView1.BestFitColumns();
                //    }
                //}
                
            }
        }

        private void btnLiberarMercadorias_Click(object sender, EventArgs e)
        {

        }
    }
}
