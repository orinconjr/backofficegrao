﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace graodegenteApp
{
    public partial class frmConfiguracoes : Form
    {
        public frmConfiguracoes()
        {
            InitializeComponent();
        }

        private void btnImprimir_Click(object sender, EventArgs e)
        {

            if (!string.IsNullOrEmpty(txtImpressora.Text))
            {
                Properties.Settings.Default["impressoraZebra"] = txtImpressora.Text;
                Properties.Settings.Default["cd"] = txtCentroDeDistribuicao.Text;
                Properties.Settings.Default["cubadora"] = txtCubadora.Text;
                Properties.Settings.Default["computador"] = txtComputador.Text;
                Properties.Settings.Default.Save();
            }

            MessageBox.Show("Configurações salvas com sucesso");
        }
        
        private void txtIdItemPedido_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnGravar.PerformClick();
            }
        }

        private void frmConfiguracoes_Load(object sender, EventArgs e)
        {
            txtImpressora.Text = Properties.Settings.Default["impressoraZebra"].ToString();
            txtCentroDeDistribuicao.Text = Properties.Settings.Default["cd"].ToString();
            txtCubadora.Text = Properties.Settings.Default["cubadora"].ToString();
            txtComputador.Text = Properties.Settings.Default["computador"].ToString();
            txtImpressora.Focus();
        }

        private void btnListar_Click(object sender, EventArgs e)
        {
            foreach (string printer in System.Drawing.Printing.PrinterSettings.InstalledPrinters)
            {
                MessageBox.Show(printer);
            }
        }

        private void btnImprimirEtiquetaCompulador_Click(object sender, EventArgs e)
        {
            string ipLocal = Utilidades.RetornaIpLocal();

            if (!string.IsNullOrEmpty(ipLocal))
            {

                var etiqueta = new StringBuilder();


                etiqueta.Append("N");
                etiqueta.Append(Environment.NewLine);
                etiqueta.Append("q800");
                etiqueta.Append(Environment.NewLine);
                etiqueta.Append("Q600,26");
                etiqueta.Append(Environment.NewLine);
                etiqueta.AppendFormat("A40,40,0,4,1,1,N,\"{0}\"", "Endereço do Computador");
                etiqueta.Append(Environment.NewLine);
                //etiqueta.AppendFormat("b46,150,Q,1,s5,eH,\"{0}\"", idAndar + "-4");
                etiqueta.AppendFormat("B46,150,Q,1,s4,eH,\"{0}\"", ipLocal);
                etiqueta.Append(Environment.NewLine);
                etiqueta.AppendFormat("b580,150,Q,1,s8,eH,\"{0}\"", ipLocal);
                etiqueta.Append(Environment.NewLine);
                etiqueta.AppendFormat("b46,400,Q,1,s4,eH,\"{0}\"", ipLocal);
                etiqueta.Append(Environment.NewLine);
                etiqueta.AppendFormat("b580,400,Q,1,s8,eH,\"{0}\"", ipLocal);
                etiqueta.Append(Environment.NewLine);
                etiqueta.Append("P1,1");
                etiqueta.Append(Environment.NewLine);
            }
        }
    }
}
