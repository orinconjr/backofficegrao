﻿namespace graodegenteApp
{
    partial class usrEmbalarProdutos
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnImpressaoConcluida = new Telerik.WinControls.UI.RadButton();
            this.lblTitulo = new System.Windows.Forms.Label();
            this.webBrowser1 = new System.Windows.Forms.WebBrowser();
            ((System.ComponentModel.ISupportInitialize)(this.btnImpressaoConcluida)).BeginInit();
            this.SuspendLayout();
            // 
            // btnImpressaoConcluida
            // 
            this.btnImpressaoConcluida.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnImpressaoConcluida.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnImpressaoConcluida.Location = new System.Drawing.Point(326, 94);
            this.btnImpressaoConcluida.Name = "btnImpressaoConcluida";
            this.btnImpressaoConcluida.Size = new System.Drawing.Size(328, 45);
            this.btnImpressaoConcluida.TabIndex = 11;
            this.btnImpressaoConcluida.Text = "Impressão Concluída";
            this.btnImpressaoConcluida.Click += new System.EventHandler(this.btnImpressaoConcluida_Click);
            // 
            // lblTitulo
            // 
            this.lblTitulo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTitulo.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitulo.Location = new System.Drawing.Point(170, 24);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Size = new System.Drawing.Size(624, 46);
            this.lblTitulo.TabIndex = 9;
            this.lblTitulo.Text = "Imprimir Lista de Produtos";
            this.lblTitulo.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // webBrowser1
            // 
            this.webBrowser1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.webBrowser1.Location = new System.Drawing.Point(16, 166);
            this.webBrowser1.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser1.Name = "webBrowser1";
            this.webBrowser1.Size = new System.Drawing.Size(976, 410);
            this.webBrowser1.TabIndex = 10;
            this.webBrowser1.Url = new System.Uri("about:blank", System.UriKind.Absolute);
            // 
            // usrEmbalarProdutos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Azure;
            this.Controls.Add(this.btnImpressaoConcluida);
            this.Controls.Add(this.webBrowser1);
            this.Controls.Add(this.lblTitulo);
            this.Name = "usrEmbalarProdutos";
            this.Size = new System.Drawing.Size(1012, 590);
            this.Load += new System.EventHandler(this.usrEmbalarProdutos_Load);
            ((System.ComponentModel.ISupportInitialize)(this.btnImpressaoConcluida)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadButton btnImpressaoConcluida;
        private System.Windows.Forms.Label lblTitulo;
        private System.Windows.Forms.WebBrowser webBrowser1;
    }
}
