﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace graodegenteApp
{
    public partial class frmEmissaoJadlog : Form
    {
        public frmEmissaoJadlog()
        {
            InitializeComponent();
        }
        public int idPedidoEnvio = 0;
        public wsEmissao.RetornoEmissao Emissao;
        public int idCentroDistribuicao = 1;

        private void frmEmissaoJadlog_Load(object sender, EventArgs e)
        {
            pnDados.Visible = false;
            AtualizaEmissoes();
        }

        //public string cnpj = "";
        //public string numeroNota = "";
        //public string linkDanfe = "";
        //public string linkGnre = "";
        //public string linkGnreRecibo = "";

        private void BaixaImprimeDanfe(string danfe, string gnre, string gnrerecibo)
        {

            string enderecoArquivo = "c:\\danfes\\" + Emissao.cnpj + "_" + Emissao.nfeNumero + ".pdf";

            try
            {
                byte[] binaryData;
                binaryData = Convert.FromBase64String(danfe);
                System.IO.FileStream outFile = new FileStream(enderecoArquivo, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                outFile.Write(binaryData, 0, binaryData.Length);
                outFile.Close();
            }
            catch (Exception)
            {

            }

            try
            {
                PrinterSettings settings = new PrinterSettings();
                string nomeImpressora = "laser";
                clsImprimirEtiqueta.PrintPdf(enderecoArquivo, nomeImpressora);
            }
            catch (Exception)
            {

            }

            if (!string.IsNullOrEmpty(gnre)) webGnre.Navigate(gnre);
            if (!string.IsNullOrEmpty(gnrerecibo)) webGnreRecibo.Navigate(gnrerecibo);
        }

        private void btnCarregarEmissao_Click(object sender, EventArgs e)
        {
            if (idPedidoEnvio == 0)
            {
                CarregarEmissao(0, 0);
            }
            else
            {
                btnCarregarEmissao.Text = "Gravar Rastreio";
                var rastreio = new frmEmissaoGravarRastreio();
                rastreio.idPedidoEnvio = idPedidoEnvio;
                var retorno = rastreio.ShowDialog();
                if (retorno == DialogResult.OK)
                {
                    btnCarregarEmissao.Text = "Carregar Próxima Emissão";
                    pnDados.Visible = false;
                    AtualizaEmissoes();
                    idPedidoEnvio = 0;
                    btnImprimirNota.Visible = false;
                    btnCarregarEmissao.Visible = false;
                    txtNumeroPedido.Focus();
                    try
                    {
                        //System.Diagnostics.Process.Start(@"cscript //B //Nologo C:\AplicativosJadlog\ImpressaoCTE\Imprimir_CTE_NF.vbs CTE");
                    }
                    catch (Exception ex)
                    {
                        
                    }
                }

            }

        }

        private void CarregarEmissao(int pedidoEnvio, int idPedido)
        {
            var serviceEmissao = new wsEmissao.EmissaoWs();
            var emissao = serviceEmissao.RetornaProximaEmissao("glmp152029", idCentroDistribuicao, pedidoEnvio, idPedido);
            if (emissao != null)
            {
                Emissao = emissao;
                pnDados.Visible = true;

                txtCnpj.Text = emissao.cnpj;
                txtCodigo.Text = emissao.codJadlog;
                idPedidoEnvio = emissao.idPedidoEnvio;
                txtTipoEnvio.Text = emissao.formaDeEnvio.ToLower();
                txtCpf.Text = emissao.clienteCPFCNPJ.Trim().Replace("-", "");
                txtDestinatario.Text =
                    removeCaracteresEspeciaisDeixaEspaco(limpaStringDeixaEspaco(emissao.endNomeDoDestinatario))
                        .ToLower();
                txtRua.Text = removeCaracteresEspeciaisDeixaEspaco(limpaStringDeixaEspaco(emissao.endRua)).ToLower() +
                              " - " +
                              removeCaracteresEspeciaisDeixaEspaco(limpaStringDeixaEspaco(emissao.endNumero)).ToLower() +
                              " - " +
                              removeCaracteresEspeciaisDeixaEspaco(limpaStringDeixaEspaco(emissao.endComplemento))
                                  .ToLower();

                txtCidade.Text =
                    removeCaracteresEspeciaisDeixaEspaco(limpaStringDeixaEspaco(emissao.endCidade)).ToLower();
                txtBairro.Text =
                    removeCaracteresEspeciaisDeixaEspaco(limpaStringDeixaEspaco(emissao.endBairro)).ToLower();
                txtUf.Text = removeCaracteresEspeciaisDeixaEspaco(limpaStringDeixaEspaco(emissao.endEstado)).ToLower();
                txtCep.Text = emissao.endCep.Trim().Replace("-", "");

                #region Fone

                var telsemespaco = emissao.clienteFoneResidencial.Replace(" ", "");
                var telspli = telsemespaco.Split(')');
                string ddd = "";
                string telefone = "";
                if (telspli.Length > 1)
                {
                    ddd = telspli[0].Replace("(", "").Replace(")", "").Trim();
                    telefone = telspli[1].Replace("-", "").Trim();
                }
                txtDDD.Text = ddd;
                txtTelefone.Text = telefone;

                #endregion

                txtPacotes.Text = emissao.totalPacotes.ToString();

                for (int i = 1; i <= 20; i++)
                {
                    string nomeControle = "txtPeso" + i;
                    var controle = this.Controls.Find(nomeControle, true).FirstOrDefault() as TextBox;
                    if (controle != null)
                    {
                        controle.Text = "";
                        controle.Visible = false;
                    }
                }

                for (int i = 1; i <= emissao.totalPacotes; i++)
                {
                    string nomeControle = "txtPeso" + i;
                    var controle = this.Controls.Find(nomeControle, true).FirstOrDefault() as TextBox;
                    if (controle != null)
                    {
                        controle.Text = emissao.pesos[i - 1].ToString();
                        controle.Visible = true;
                    }

                }

                if (emissao.gerarReversa)
                {
                    txtReversa.Text = "1";
                }
                else
                {
                    txtReversa.Text = "0";
                }

                if (emissao.nota > 0)
                {
                    btnImprimirNota.Visible = true;


                    btnImprimirNota.Text = "Imprimir nota";
                    if (!string.IsNullOrEmpty(Emissao.gnre)) btnImprimirNota.Text += "/gnre";
                    if (!string.IsNullOrEmpty(Emissao.comprovantePagamentoGnre)) btnImprimirNota.Text += "/recibo";
                    
                }
                else
                {
                    btnImprimirNota.Visible = false;
                }
                btnCarregarEmissao.Text = "Gravar Rastreio";
                btnCarregarEmissao.Visible = true;
            }
            else
            {
                MessageBox.Show("Emissão não localizada!");
                btnCarregarEmissao.Text = "Carregar Próxima Emissão";
                pnDados.Visible = false;
                AtualizaEmissoes();
                idPedidoEnvio = 0;
                btnImprimirNota.Visible = false;
                btnCarregarEmissao.Visible = false;
                txtNumeroPedido.Focus();
            }
        }
        private void pnDados_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnImprimirNota_Click(object sender, EventArgs e)
        {
            BaixaImprimeDanfe(Emissao.danfe, Emissao.gnre, Emissao.comprovantePagamentoGnre);
            //var nota = new frmEmissaoNota();
            //nota.link = Emissao.danfe;
            //nota.ShowDialog();
        }


        private void AtualizaEmissoes()
        {
            EmiteNotas();
            //AtualizaListaEmissoes();
            ListEmissoes();
            EmiteNotas();
        }
        
        private void ListEmissoes()
        {
            if(!AtualizandoEmissoes) ThreadPool.QueueUserWorkItem(ListEmissoes);
        }

        private bool AtualizandoEmissoes = false;
        private void ListEmissoes(object parametros)
        {
            try
            {
                AtualizandoEmissoes = true;
                var serviceEmissao = new wsEmissao.EmissaoWs();
                var emissoesPendentes = serviceEmissao.ListaEmissoes("glmp152029", idCentroDistribuicao);
                grd.Invoke((MethodInvoker) delegate
                {
                    grd.AutoSizeRows = true;
                    grd.DataSource = emissoesPendentes.ToList();
                    
                });
                lblContagemEmissoes.Invoke((MethodInvoker)delegate
                {
                    lblContagemEmissoes.Text = "Emissões Pendentes: " + emissoesPendentes.Count();
                });
            }
            catch (Exception)
            {

            }
            finally
            {
                AtualizandoEmissoes = false;
            }
        }
        private void EmiteNotas()
        {
            if(!EmitindoNotas) ThreadPool.QueueUserWorkItem(EmiteNotas);
        }
        private bool EmitindoNotas = false;

        private void EmiteNotas(object parametros)
        {
            try
            {
                EmitindoNotas = true;
                var serviceEmissao = new wsEmissao.EmissaoWs();
                serviceEmissao.EmiteNotas("glmp152029");
            }
            catch (Exception)
            {

            }
            finally
            {
                EmitindoNotas = false;
            }
        }

        private void AtualizaListaEmissoes()
        {
            if(!AtualizandoListaEmissoes) ThreadPool.QueueUserWorkItem(AtualizaListaEmissoes);
        }

        private bool AtualizandoListaEmissoes = false;
        private void AtualizaListaEmissoes(object parametros)
        {
            try
            {
                AtualizandoListaEmissoes = true;
                var serviceEmissao = new wsEmissao.EmissaoWs();
                var emissoes = serviceEmissao.ContaEmissoesPendentes("glmp152029", idCentroDistribuicao);
                lblContagemEmissoes.Invoke((MethodInvoker)delegate
                {
                    lblContagemEmissoes.Text = "Emissões Pendentes: " + emissoes;
                });
            }
            catch (Exception)
            {

            }
            finally { AtualizandoListaEmissoes = false; }
        }

        private void grd_Click(object sender, EventArgs e)
        {

        }

        private void grd_MouseDoubleClick(object sender, MouseEventArgs e)
        {
           var confirmacao = MessageBox.Show("Deseja carregar o pedido selecionado?", "Confirmacao", MessageBoxButtons.YesNo);
            if (confirmacao == DialogResult.Yes)
            {
                for (int i = 0; i < grd.SelectedRows.Count; i++)
                {
                    var id = grd.SelectedRows[i].Cells[0].Value;
                    CarregarEmissao(Convert.ToInt32(id), 0);
                }
            }
        }
        public static string removeCaracteresEspeciaisDeixaEspaco(string inputString)
        {
            StringBuilder sb = new StringBuilder();
            foreach (char c in inputString)
            {
                if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') | c == '.' || c == '_' || c == '-' || c == ' ')
                {
                    sb.Append(c);
                }
            }
            return sb.ToString();
        }
        public static string limpaStringDeixaEspaco(string texto)
        {
            string caracteresEspeciais = "ÄÅÁÂÀÃäáâàãÉÊËÈéêëèÍÎÏÌíîïìÖÓÔÒÕöóôòõÜÚÛüúûùÇç/`~!@#$%^&*()*+;:',<.>?|";
            string caracteresNormais = "AAAAAAaaaaaEEEEeeeeIIIIiiiiOOOOOoooooUUUuuuuCc------------------------";

            for (int i = 0; i < caracteresEspeciais.Length; i++)
            {
                try
                {
                    texto = texto.Replace(caracteresEspeciais[i].ToString(), caracteresNormais[i].ToString());
                    texto = texto.Replace("\"", "");
                    texto = texto.Replace("'", "");
                }
                catch (Exception ex) { }
            }
            return texto;
        }

        private void txtNumeroPedido_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && txtNumeroPedido.Text != "")
            {
                var pedidoIdCliente = txtNumeroPedido.Text.Split('-')[0];
                try
                {
                    int idPedido = Utilidades.retornaIdInterno(Convert.ToInt32(pedidoIdCliente));
                    CarregarEmissao(0, idPedido);
                    txtNumeroPedido.Text = "";
                    txtCnpj.Focus();
                }
                catch (Exception)
                {
                    txtNumeroPedido.Text = "";
                    txtNumeroPedido.Focus();
                }
            }
        }


        private void webGnre_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            webGnre.Print();
        }

        private void webGnreRecibo_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            webGnreRecibo.Print();
        }

        private void btnAtualizarEmissoes_Click(object sender, EventArgs e)
        {
            AtualizaEmissoes();
        }
    }
}
