﻿namespace graodegenteApp
{
    partial class frmEmissaoGravarRastreio
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtRastreio = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // txtRastreio
            // 
            this.txtRastreio.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRastreio.Location = new System.Drawing.Point(32, 12);
            this.txtRastreio.MaxLength = 14;
            this.txtRastreio.Name = "txtRastreio";
            this.txtRastreio.Size = new System.Drawing.Size(343, 30);
            this.txtRastreio.TabIndex = 0;
            this.txtRastreio.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtRastreio_KeyDown);
            // 
            // frmEmissaoGravarRastreio
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(405, 56);
            this.Controls.Add(this.txtRastreio);
            this.Name = "frmEmissaoGravarRastreio";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Gravar Rastreio";
            this.Load += new System.EventHandler(this.frmEmissaoGravarRastreio_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtRastreio;
    }
}