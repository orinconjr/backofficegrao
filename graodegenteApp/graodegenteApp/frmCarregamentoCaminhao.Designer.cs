﻿namespace graodegenteApp
{
    partial class frmCarregamentoCaminhao
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn16 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn17 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn18 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn19 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn20 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn21 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn22 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn23 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn24 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn25 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn26 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn27 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn28 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn29 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn30 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            this.grdAguardandoCarregamentoCd1 = new Telerik.WinControls.UI.RadGridView();
            this.lblAguardandoCarregamentoCd1 = new System.Windows.Forms.Label();
            this.txtNumeroPedido = new System.Windows.Forms.TextBox();
            this.grdAguardandoCarregamentoCd2 = new Telerik.WinControls.UI.RadGridView();
            this.lblParcialmenteCarregado = new System.Windows.Forms.Label();
            this.grdCarregamentoPendente = new Telerik.WinControls.UI.RadGridView();
            this.lblMensagem = new Telerik.WinControls.UI.RadLabel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.lblAguardandoCarregamentoCd2 = new System.Windows.Forms.Label();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.timerUpdate = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.grdAguardandoCarregamentoCd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdAguardandoCarregamentoCd1.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdAguardandoCarregamentoCd2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdAguardandoCarregamentoCd2.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdCarregamentoPendente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdCarregamentoPendente.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMensagem)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // grdAguardandoCarregamentoCd1
            // 
            this.grdAguardandoCarregamentoCd1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grdAguardandoCarregamentoCd1.Location = new System.Drawing.Point(3, 3);
            // 
            // grdAguardandoCarregamentoCd1
            // 
            this.grdAguardandoCarregamentoCd1.MasterTemplate.AllowAddNewRow = false;
            this.grdAguardandoCarregamentoCd1.MasterTemplate.AllowDeleteRow = false;
            this.grdAguardandoCarregamentoCd1.MasterTemplate.AllowEditRow = false;
            this.grdAguardandoCarregamentoCd1.MasterTemplate.AutoGenerateColumns = false;
            this.grdAguardandoCarregamentoCd1.MasterTemplate.AutoSizeColumnsMode = Telerik.WinControls.UI.GridViewAutoSizeColumnsMode.Fill;
            gridViewTextBoxColumn16.FieldName = "pedidoId";
            gridViewTextBoxColumn16.HeaderText = "ID Pedido";
            gridViewTextBoxColumn16.Name = "pedidoId";
            gridViewTextBoxColumn16.Width = 67;
            gridViewTextBoxColumn17.FieldName = "endCidade";
            gridViewTextBoxColumn17.HeaderText = "Cidade";
            gridViewTextBoxColumn17.Name = "endCidade";
            gridViewTextBoxColumn17.Width = 104;
            gridViewTextBoxColumn18.FieldName = "endEstado";
            gridViewTextBoxColumn18.HeaderText = "Estado";
            gridViewTextBoxColumn18.Name = "endEstado";
            gridViewTextBoxColumn18.Width = 104;
            gridViewTextBoxColumn19.FieldName = "totalPacotes";
            gridViewTextBoxColumn19.HeaderText = "Volumes";
            gridViewTextBoxColumn19.Name = "totalPacotes";
            gridViewTextBoxColumn19.Width = 38;
            gridViewTextBoxColumn20.FieldName = "totalCarregados";
            gridViewTextBoxColumn20.HeaderText = "Volumes Carregados";
            gridViewTextBoxColumn20.Name = "totalCarregados";
            gridViewTextBoxColumn20.Width = 82;
            this.grdAguardandoCarregamentoCd1.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn16,
            gridViewTextBoxColumn17,
            gridViewTextBoxColumn18,
            gridViewTextBoxColumn19,
            gridViewTextBoxColumn20});
            this.grdAguardandoCarregamentoCd1.MasterTemplate.EnableFiltering = true;
            this.grdAguardandoCarregamentoCd1.MasterTemplate.EnableGrouping = false;
            this.grdAguardandoCarregamentoCd1.MasterTemplate.MultiSelect = true;
            this.grdAguardandoCarregamentoCd1.MinimumSize = new System.Drawing.Size(100, 100);
            this.grdAguardandoCarregamentoCd1.Name = "grdAguardandoCarregamentoCd1";
            // 
            // 
            // 
            this.grdAguardandoCarregamentoCd1.RootElement.MinSize = new System.Drawing.Size(100, 100);
            this.grdAguardandoCarregamentoCd1.Size = new System.Drawing.Size(411, 460);
            this.grdAguardandoCarregamentoCd1.TabIndex = 5;
            this.grdAguardandoCarregamentoCd1.Text = "radGridView1";
            this.grdAguardandoCarregamentoCd1.Click += new System.EventHandler(this.grd_Click);
            this.grdAguardandoCarregamentoCd1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.grd_MouseDoubleClick);
            // 
            // lblAguardandoCarregamentoCd1
            // 
            this.lblAguardandoCarregamentoCd1.AutoSize = true;
            this.lblAguardandoCarregamentoCd1.Location = new System.Drawing.Point(3, 0);
            this.lblAguardandoCarregamentoCd1.Name = "lblAguardandoCarregamentoCd1";
            this.lblAguardandoCarregamentoCd1.Size = new System.Drawing.Size(158, 13);
            this.lblAguardandoCarregamentoCd1.TabIndex = 6;
            this.lblAguardandoCarregamentoCd1.Text = "Aguardando Carregamento CD1";
            // 
            // txtNumeroPedido
            // 
            this.txtNumeroPedido.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNumeroPedido.Location = new System.Drawing.Point(15, 4);
            this.txtNumeroPedido.Name = "txtNumeroPedido";
            this.txtNumeroPedido.Size = new System.Drawing.Size(210, 30);
            this.txtNumeroPedido.TabIndex = 8;
            this.txtNumeroPedido.TextChanged += new System.EventHandler(this.txtNumeroPedido_TextChanged);
            this.txtNumeroPedido.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNumeroPedido_KeyDown);
            // 
            // grdAguardandoCarregamentoCd2
            // 
            this.grdAguardandoCarregamentoCd2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grdAguardandoCarregamentoCd2.Location = new System.Drawing.Point(420, 3);
            // 
            // grdAguardandoCarregamentoCd2
            // 
            this.grdAguardandoCarregamentoCd2.MasterTemplate.AllowAddNewRow = false;
            this.grdAguardandoCarregamentoCd2.MasterTemplate.AllowDeleteRow = false;
            this.grdAguardandoCarregamentoCd2.MasterTemplate.AllowEditRow = false;
            this.grdAguardandoCarregamentoCd2.MasterTemplate.AutoGenerateColumns = false;
            this.grdAguardandoCarregamentoCd2.MasterTemplate.AutoSizeColumnsMode = Telerik.WinControls.UI.GridViewAutoSizeColumnsMode.Fill;
            gridViewTextBoxColumn21.FieldName = "pedidoId";
            gridViewTextBoxColumn21.HeaderText = "ID Pedido";
            gridViewTextBoxColumn21.Name = "pedidoId";
            gridViewTextBoxColumn21.Width = 67;
            gridViewTextBoxColumn22.FieldName = "endCidade";
            gridViewTextBoxColumn22.HeaderText = "Cidade";
            gridViewTextBoxColumn22.Name = "endCidade";
            gridViewTextBoxColumn22.Width = 103;
            gridViewTextBoxColumn23.FieldName = "endEstado";
            gridViewTextBoxColumn23.HeaderText = "Estado";
            gridViewTextBoxColumn23.Name = "endEstado";
            gridViewTextBoxColumn23.Width = 103;
            gridViewTextBoxColumn24.FieldName = "totalPacotes";
            gridViewTextBoxColumn24.HeaderText = "Volumes";
            gridViewTextBoxColumn24.Name = "totalPacotes";
            gridViewTextBoxColumn24.Width = 35;
            gridViewTextBoxColumn25.FieldName = "totalCarregados";
            gridViewTextBoxColumn25.HeaderText = "Volumes Carregados";
            gridViewTextBoxColumn25.Name = "totalCarregados";
            gridViewTextBoxColumn25.Width = 87;
            this.grdAguardandoCarregamentoCd2.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn21,
            gridViewTextBoxColumn22,
            gridViewTextBoxColumn23,
            gridViewTextBoxColumn24,
            gridViewTextBoxColumn25});
            this.grdAguardandoCarregamentoCd2.MasterTemplate.EnableFiltering = true;
            this.grdAguardandoCarregamentoCd2.MasterTemplate.EnableGrouping = false;
            this.grdAguardandoCarregamentoCd2.MasterTemplate.MultiSelect = true;
            this.grdAguardandoCarregamentoCd2.Name = "grdAguardandoCarregamentoCd2";
            this.grdAguardandoCarregamentoCd2.Size = new System.Drawing.Size(411, 460);
            this.grdAguardandoCarregamentoCd2.TabIndex = 9;
            this.grdAguardandoCarregamentoCd2.Text = "radGridView1";
            // 
            // lblParcialmenteCarregado
            // 
            this.lblParcialmenteCarregado.AutoSize = true;
            this.lblParcialmenteCarregado.Location = new System.Drawing.Point(837, 0);
            this.lblParcialmenteCarregado.Name = "lblParcialmenteCarregado";
            this.lblParcialmenteCarregado.Size = new System.Drawing.Size(120, 13);
            this.lblParcialmenteCarregado.TabIndex = 12;
            this.lblParcialmenteCarregado.Text = "Parcialmente Carregado";
            // 
            // grdCarregamentoPendente
            // 
            this.grdCarregamentoPendente.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grdCarregamentoPendente.Location = new System.Drawing.Point(837, 3);
            // 
            // grdCarregamentoPendente
            // 
            this.grdCarregamentoPendente.MasterTemplate.AllowAddNewRow = false;
            this.grdCarregamentoPendente.MasterTemplate.AllowDeleteRow = false;
            this.grdCarregamentoPendente.MasterTemplate.AllowEditRow = false;
            this.grdCarregamentoPendente.MasterTemplate.AutoGenerateColumns = false;
            this.grdCarregamentoPendente.MasterTemplate.AutoSizeColumnsMode = Telerik.WinControls.UI.GridViewAutoSizeColumnsMode.Fill;
            gridViewTextBoxColumn26.FieldName = "pedidoId";
            gridViewTextBoxColumn26.HeaderText = "ID Pedido";
            gridViewTextBoxColumn26.Name = "pedidoId";
            gridViewTextBoxColumn26.Width = 70;
            gridViewTextBoxColumn27.FieldName = "endCidade";
            gridViewTextBoxColumn27.HeaderText = "Cidade";
            gridViewTextBoxColumn27.Name = "endCidade";
            gridViewTextBoxColumn27.Width = 111;
            gridViewTextBoxColumn28.FieldName = "endEstado";
            gridViewTextBoxColumn28.HeaderText = "Estado";
            gridViewTextBoxColumn28.Name = "endEstado";
            gridViewTextBoxColumn28.Width = 111;
            gridViewTextBoxColumn29.FieldName = "totalPacotes";
            gridViewTextBoxColumn29.HeaderText = "Volumes";
            gridViewTextBoxColumn29.Name = "totalPacotes";
            gridViewTextBoxColumn29.Width = 38;
            gridViewTextBoxColumn30.FieldName = "totalCarregados";
            gridViewTextBoxColumn30.HeaderText = "Volumes Carregados";
            gridViewTextBoxColumn30.Name = "totalCarregados";
            gridViewTextBoxColumn30.Width = 79;
            this.grdCarregamentoPendente.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn26,
            gridViewTextBoxColumn27,
            gridViewTextBoxColumn28,
            gridViewTextBoxColumn29,
            gridViewTextBoxColumn30});
            this.grdCarregamentoPendente.MasterTemplate.EnableFiltering = true;
            this.grdCarregamentoPendente.MasterTemplate.EnableGrouping = false;
            this.grdCarregamentoPendente.MasterTemplate.MultiSelect = true;
            this.grdCarregamentoPendente.Name = "grdCarregamentoPendente";
            this.grdCarregamentoPendente.Size = new System.Drawing.Size(425, 460);
            this.grdCarregamentoPendente.TabIndex = 11;
            this.grdCarregamentoPendente.Text = "radGridView2";
            // 
            // lblMensagem
            // 
            this.lblMensagem.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMensagem.AutoSize = false;
            this.lblMensagem.Font = new System.Drawing.Font("Segoe UI", 46F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMensagem.Location = new System.Drawing.Point(15, 39);
            this.lblMensagem.Name = "lblMensagem";
            this.lblMensagem.Size = new System.Drawing.Size(1254, 97);
            this.lblMensagem.TabIndex = 17;
            this.lblMensagem.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.lblMensagem.Click += new System.EventHandler(this.lblMensagem_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 34F));
            this.tableLayoutPanel1.Controls.Add(this.grdAguardandoCarregamentoCd1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.grdAguardandoCarregamentoCd2, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.grdCarregamentoPendente, 2, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(10, 179);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1265, 466);
            this.tableLayoutPanel1.TabIndex = 18;
            // 
            // lblAguardandoCarregamentoCd2
            // 
            this.lblAguardandoCarregamentoCd2.AutoSize = true;
            this.lblAguardandoCarregamentoCd2.Location = new System.Drawing.Point(420, 0);
            this.lblAguardandoCarregamentoCd2.Name = "lblAguardandoCarregamentoCd2";
            this.lblAguardandoCarregamentoCd2.Size = new System.Drawing.Size(158, 13);
            this.lblAguardandoCarregamentoCd2.TabIndex = 10;
            this.lblAguardandoCarregamentoCd2.Text = "Aguardando Carregamento CD2";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 34F));
            this.tableLayoutPanel2.Controls.Add(this.lblAguardandoCarregamentoCd1, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.lblAguardandoCarregamentoCd2, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.lblParcialmenteCarregado, 2, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(10, 145);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1265, 28);
            this.tableLayoutPanel2.TabIndex = 19;
            // 
            // timerUpdate
            // 
            this.timerUpdate.Enabled = true;
            this.timerUpdate.Interval = 30000;
            this.timerUpdate.Tick += new System.EventHandler(this.timerUpdate_Tick);
            // 
            // frmCarregamentoCaminhao
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1281, 650);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.lblMensagem);
            this.Controls.Add(this.txtNumeroPedido);
            this.Name = "frmCarregamentoCaminhao";
            this.Text = "Carregamento Caminhão";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmCarregamentoCaminhao_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grdAguardandoCarregamentoCd1.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdAguardandoCarregamentoCd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdAguardandoCarregamentoCd2.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdAguardandoCarregamentoCd2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdCarregamentoPendente.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdCarregamentoPendente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMensagem)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private Telerik.WinControls.UI.RadGridView grdAguardandoCarregamentoCd1;
        private System.Windows.Forms.Label lblAguardandoCarregamentoCd1;
        private System.Windows.Forms.TextBox txtNumeroPedido;
        private Telerik.WinControls.UI.RadGridView grdAguardandoCarregamentoCd2;
        private System.Windows.Forms.Label lblParcialmenteCarregado;
        private Telerik.WinControls.UI.RadGridView grdCarregamentoPendente;
        private Telerik.WinControls.UI.RadLabel lblMensagem;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label lblAguardandoCarregamentoCd2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Timer timerUpdate;
    }
}