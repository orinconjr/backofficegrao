﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace graodegenteApp
{
    public partial class frmEntradaRomaneio : Form
    {
        int idPedidoFornecedorItem = 0;
        public int idEnderecoTransferencia = 0;


        public int idUsuario = 0;
        public frmEntradaRomaneio()
        {
            InitializeComponent();
        }

        private void txtIdItemPedido_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                var pedidos = new wsPedidosFornecedor.PedidosFornecedorWs();
                var codigoSplit = txtIdItemPedido.Text.Split('-');
                if (codigoSplit.Count() == 2)
                {
                    if (codigoSplit[1] != "edt")
                    {
                        txtIdItemPedido.Text = "";
                        txtIdItemPedido.Focus();
                        MessageBox.Show("Local de armazenamento inválido");
                        return;
                    }

                    int idArmazenamento = 0;
                    int.TryParse(codigoSplit[0], out idArmazenamento);

                    if (idArmazenamento == 0)
                    {
                        txtIdItemPedido.Text = "";
                        txtIdItemPedido.Focus();
                        MessageBox.Show("Local de armazenamento inválido");
                        return;
                    }

                    string nomeTransferencia = pedidos.RetornaNomeEnderecoTransferencia(idArmazenamento, "glmp152029");
                    if (string.IsNullOrEmpty(nomeTransferencia))
                    {
                        txtIdItemPedido.Text = "";
                        txtIdItemPedido.Focus();
                        MessageBox.Show("Local de armazenamento inválido");
                        return;
                    }

                    txtIdItemPedido.Text = "";
                    txtIdItemPedido.Focus();

                    lblEnderecoTransferencia.Text = nomeTransferencia;
                    idEnderecoTransferencia = idArmazenamento;
                }
                else
                {
                    if (idEnderecoTransferencia == 0)
                    {
                        txtIdItemPedido.Text = "";
                        txtIdItemPedido.Focus();
                        MessageBox.Show("Favor ler o local onde será armazenado");
                        return;
                    }
                    //lstProdutosFaltando.Visible = false;
                    pictureBox1.Visible = true;
                    int id = 0;
                    int.TryParse(txtIdItemPedido.Text, out id);

                    if ((idPedidoFornecedorItem == 0 && id > 0) | (idPedidoFornecedorItem != id && id > 0))
                    {

                        int.TryParse(txtIdItemPedido.Text, out idPedidoFornecedorItem);

                        if (idPedidoFornecedorItem > 0)
                        {
                            var detalhes = pedidos.RetornaDetalhesItemUsuario(idPedidoFornecedorItem, clsConfiguracao.ChaveWs, false, idUsuario);
                            lblNomeProduto.Text = detalhes.produtoNome;
                            try
                            {
                                pictureBox1.WaitOnLoad = false;
                                pictureBox1.LoadAsync("https://s3-sa-east-1.amazonaws.com/cdn2.graodegente.com.br/fotos/" + detalhes.produtoId + "/media_" +
                                                detalhes.produtoFoto + ".jpg");
                                //Image img = null;
                                //pictureBox1.Image =
                                //    img.FromURL("https://s3-sa-east-1.amazonaws.com/cdn2.graodegente.com.br/fotos/" + detalhes.produtoId + "/media_" +
                                //                detalhes.produtoFoto + ".jpg");
                            }
                            catch (Exception)
                            {

                            }
                            txtIdItemPedido.Text = "";
                            txtIdItemPedido.Focus();
                        }
                    }
                    else
                    {
                        var entradaEstoque = pedidos.RegistrarEntradaRomaneioComEndereco(idPedidoFornecedorItem, idUsuario, idEnderecoTransferencia, clsConfiguracao.ChaveWs);

                        if (!string.IsNullOrEmpty(entradaEstoque.respostaEndereco))
                        {
                            lblEnderecoTransferencia.Text = entradaEstoque.respostaEndereco;
                            lblNomeProduto.Text = "";
                            idPedidoFornecedorItem = 0;
                            pictureBox1.Image = null;
                            txtIdItemPedido.Text = "";
                            txtIdItemPedido.Focus();
                            return;
                        }
                        else
                        {
                            string mensagem = "";
                            if (entradaEstoque.adicionado)
                            {
                                mensagem += "Entrada registrada";
                            }
                            else
                            {
                                mensagem += "PRODUTO JÁ REGISTRADO ANTERIORMENTE";
                            }
                            mensagem += Environment.NewLine;
                            mensagem += "FALTANDO: " + (entradaEstoque.totalProdutos - entradaEstoque.produtosEntregues);
                            mensagem += Environment.NewLine;
                            mensagem += "Entregues: " + entradaEstoque.produtosEntregues;
                            mensagem += Environment.NewLine;
                            mensagem += "Total do Romaneio: " + entradaEstoque.totalProdutos;
                            lblNomeProduto.Text = mensagem;

                            if (entradaEstoque.produtosEntregues < entradaEstoque.totalProdutos)
                            {
                                //lstProdutosFaltando.Visible = true;
                                pictureBox1.Visible = false;
                                //AtualizaRomaneioPendente(entradaEstoque.idPedidoFornecedor, idPedidoFornecedorItem);
                            }

                            idPedidoFornecedorItem = 0;
                            pictureBox1.Image = null;
                            txtIdItemPedido.Text = "";
                            txtIdItemPedido.Focus();

                        }
                    }
                }
            }
        }


        //private List<int> idsUpdate = new List<int>();
        private List<KeyValuePair<int, int>> idsUpdate = new List<KeyValuePair<int, int>>();
        private int lastIdPedidoFornecedor = 0;
        private void AtualizaRomaneioPendente(int idPedidoFornecedor, int etiquetaRemovida)
        {
            idPedidoFornecedorUpdate = idPedidoFornecedor;
            /*idsUpdate.Add(new KeyValuePair<int, int>(idPedidoFornecedor, etiquetaRemovida));
            idPedidoFornecedorUpdate = idPedidoFornecedor;
            dataSolicitacaoAtualizacao = DateTime.Now.AddMilliseconds(5);*/
            AtualizaRomaneio();
        }
        private void AtualizaRomaneio()
        {
            //if (idsUpdate.Any())
            //{
                bwAtualizarLista.WorkerSupportsCancellation = true;
                bwAtualizarLista.DoWork += new DoWorkEventHandler(bwAtualizarLista_DoWork);
                bwAtualizarLista.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bwAtualizarLista_RunWorkerCompleted);

                if (bwAtualizarLista.IsBusy != true)
                {
                    //dataUltimaAtualizacao = DateTime.Now.AddMilliseconds(50);                    
                    bwAtualizarLista.RunWorkerAsync();
                }
            //}
        }

        List<wsPedidosFornecedor.admin_pedidoFornecedorDetalhesPorIdResult> listaResultados = new List<wsPedidosFornecedor.admin_pedidoFornecedorDetalhesPorIdResult>();
        private void bwAtualizarLista_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                /*var firstUpdate = idsUpdate.First();
                if(lastIdPedidoFornecedor != firstUpdate.Key)
                {*/
                    //lblStatus.Text = "Romaneio diferente";
                    var pedidos = new wsPedidosFornecedor.PedidosFornecedorWs();
                    listaResultados = pedidos.ListaProdutosPendentes(idPedidoFornecedorUpdate, clsConfiguracao.ChaveWs).OrderBy(x => x.produtoNome).ToList();
                    //listaResultados = detalhesRomaneio;

                    var listaNomes = new List<string>();
                    listaNomes.AddRange(listaResultados.Select(x => x.produtoNome));
                    /*lstProdutosFaltando.Invoke((MethodInvoker)delegate
                    {
                        lstProdutosFaltando.Items.Clear();
                        lstProdutosFaltando.Items.AddRange(listaResultados.Select(x => x.produtoNome).ToArray());
                    });*/
                    lastIdPedidoFornecedor = idPedidoFornecedorUpdate;
                /*}
                else
                {
                    lblStatus.Text = "Mesmo romaneio";
                    listaResultados.RemoveAll(x => x.idPedidoFornecedorItem == firstUpdate.Value);
                    var listaNomes = new List<string>();
                    listaNomes.AddRange(listaResultados.Select(x => x.produtoNome));
                    lstProdutosFaltando.Invoke((MethodInvoker)delegate
                    {
                        lstProdutosFaltando.Items.Clear();
                        lstProdutosFaltando.Items.AddRange(listaResultados.Select(x => x.produtoNome).ToArray());
                    });
                }*/
                
            }
            catch (Exception)
            {

            }
        }

        private void bwAtualizarLista_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            /*if (idsUpdate.Any())
            {
                idsUpdate.Remove(idsUpdate.First());
            }
            if (idsUpdate.Any())
            {
                bwAtualizarLista.WorkerSupportsCancellation = true;
                bwAtualizarLista.DoWork += new DoWorkEventHandler(bwAtualizarLista_DoWork);
                bwAtualizarLista.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bwAtualizarLista_RunWorkerCompleted);

                if (bwAtualizarLista.IsBusy != true)
                {
                    dataUltimaAtualizacao = DateTime.Now.AddMilliseconds(50);
                    bwAtualizarLista.RunWorkerAsync();
                }
            }*/
        }


        private BackgroundWorker bwAtualizarLista = new BackgroundWorker();

        private int idPedidoFornecedorUpdate = 0;
        private DateTime dataSolicitacaoAtualizacao = DateTime.Now;
        private DateTime dataUltimaAtualizacao = DateTime.Now;

        private void frmEntradaRomaneio_Load(object sender, EventArgs e)
        {
            txtIdItemPedido.Focus();
        }

        private void lblNomeProduto_Click(object sender, EventArgs e)
        {

        }
    }
}
