﻿namespace graodegenteApp
{
    partial class frmEntradaRomaneio
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtIdItemPedido = new Telerik.WinControls.UI.RadTextBox();
            this.lblNomeProduto = new Telerik.WinControls.UI.RadLabel();
            this.lblEnderecoTransferencia = new Telerik.WinControls.UI.RadLabel();
            this.lblStatus = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.txtIdItemPedido)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNomeProduto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblEnderecoTransferencia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // txtIdItemPedido
            // 
            this.txtIdItemPedido.Font = new System.Drawing.Font("Segoe UI", 16F);
            this.txtIdItemPedido.Location = new System.Drawing.Point(12, 54);
            this.txtIdItemPedido.Name = "txtIdItemPedido";
            this.txtIdItemPedido.Size = new System.Drawing.Size(230, 34);
            this.txtIdItemPedido.TabIndex = 9;
            this.txtIdItemPedido.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtIdItemPedido_KeyDown);
            // 
            // lblNomeProduto
            // 
            this.lblNomeProduto.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblNomeProduto.AutoSize = false;
            this.lblNomeProduto.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNomeProduto.Location = new System.Drawing.Point(12, 89);
            this.lblNomeProduto.Name = "lblNomeProduto";
            this.lblNomeProduto.Size = new System.Drawing.Size(618, 94);
            this.lblNomeProduto.TabIndex = 11;
            this.lblNomeProduto.Text = "Passe o código de barras para dar entrada";
            this.lblNomeProduto.Click += new System.EventHandler(this.lblNomeProduto_Click);
            // 
            // lblEnderecoTransferencia
            // 
            this.lblEnderecoTransferencia.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblEnderecoTransferencia.AutoSize = false;
            this.lblEnderecoTransferencia.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEnderecoTransferencia.Location = new System.Drawing.Point(12, 12);
            this.lblEnderecoTransferencia.Name = "lblEnderecoTransferencia";
            this.lblEnderecoTransferencia.Size = new System.Drawing.Size(618, 36);
            this.lblEnderecoTransferencia.TabIndex = 14;
            this.lblEnderecoTransferencia.Text = "Leia o código onde será armazenado";
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Location = new System.Drawing.Point(249, 70);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(35, 13);
            this.lblStatus.TabIndex = 15;
            this.lblStatus.Text = "label1";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.Location = new System.Drawing.Point(12, 189);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(618, 370);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 12;
            this.pictureBox1.TabStop = false;
            // 
            // frmEntradaRomaneio
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Azure;
            this.ClientSize = new System.Drawing.Size(639, 566);
            this.Controls.Add(this.lblStatus);
            this.Controls.Add(this.lblEnderecoTransferencia);
            this.Controls.Add(this.lblNomeProduto);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.txtIdItemPedido);
            this.Name = "frmEntradaRomaneio";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmEntradaRomaneio";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmEntradaRomaneio_Load);
            ((System.ComponentModel.ISupportInitialize)(this.txtIdItemPedido)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNomeProduto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblEnderecoTransferencia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadTextBox txtIdItemPedido;
        private Telerik.WinControls.UI.RadLabel lblNomeProduto;
        private Telerik.WinControls.UI.RadLabel lblEnderecoTransferencia;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}