﻿namespace graodegenteApp
{
    partial class frmImprimirEnderecamento
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            this.grd = new Telerik.WinControls.UI.RadGridView();
            this.btnImprimir = new Telerik.WinControls.UI.RadButton();
            this.rdbEnderecamento = new System.Windows.Forms.RadioButton();
            this.rdbEstantes = new System.Windows.Forms.RadioButton();
            this.rdbSenhas = new System.Windows.Forms.RadioButton();
            this.rdbCarrinhos = new System.Windows.Forms.RadioButton();
            this.rdbEstantePacote = new System.Windows.Forms.RadioButton();
            ((System.ComponentModel.ISupportInitialize)(this.grd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grd.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnImprimir)).BeginInit();
            this.SuspendLayout();
            // 
            // grd
            // 
            this.grd.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grd.Location = new System.Drawing.Point(9, 98);
            // 
            // 
            // 
            this.grd.MasterTemplate.AllowAddNewRow = false;
            this.grd.MasterTemplate.AllowDeleteRow = false;
            this.grd.MasterTemplate.AllowEditRow = false;
            this.grd.MasterTemplate.AutoGenerateColumns = false;
            this.grd.MasterTemplate.AutoSizeColumnsMode = Telerik.WinControls.UI.GridViewAutoSizeColumnsMode.Fill;
            gridViewTextBoxColumn1.FieldName = "idEnderecamentoAndar";
            gridViewTextBoxColumn1.HeaderText = "ID";
            gridViewTextBoxColumn1.Name = "idEnderecamentoAndar";
            gridViewTextBoxColumn1.Width = 108;
            gridViewTextBoxColumn2.Expression = "";
            gridViewTextBoxColumn2.FieldName = "area";
            gridViewTextBoxColumn2.HeaderText = "Área";
            gridViewTextBoxColumn2.Name = "area";
            gridViewTextBoxColumn2.Width = 230;
            gridViewTextBoxColumn3.FieldName = "rua";
            gridViewTextBoxColumn3.HeaderText = "Rua";
            gridViewTextBoxColumn3.Name = "rua";
            gridViewTextBoxColumn3.Width = 94;
            gridViewTextBoxColumn4.FieldName = "lado";
            gridViewTextBoxColumn4.HeaderText = "Lado";
            gridViewTextBoxColumn4.Name = "lado";
            gridViewTextBoxColumn4.Width = 94;
            gridViewTextBoxColumn5.FieldName = "predio";
            gridViewTextBoxColumn5.HeaderText = "Prédio";
            gridViewTextBoxColumn5.Name = "predio";
            gridViewTextBoxColumn5.Width = 95;
            gridViewTextBoxColumn6.FieldName = "andar";
            gridViewTextBoxColumn6.HeaderText = "Andar";
            gridViewTextBoxColumn6.Name = "andar";
            gridViewTextBoxColumn6.Width = 300;
            this.grd.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6});
            this.grd.MasterTemplate.EnableFiltering = true;
            this.grd.MasterTemplate.EnableGrouping = false;
            this.grd.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.grd.Name = "grd";
            this.grd.Size = new System.Drawing.Size(937, 410);
            this.grd.TabIndex = 1;
            this.grd.Text = "radGridView1";
            // 
            // btnImprimir
            // 
            this.btnImprimir.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnImprimir.Location = new System.Drawing.Point(12, 12);
            this.btnImprimir.Name = "btnImprimir";
            this.btnImprimir.Size = new System.Drawing.Size(295, 39);
            this.btnImprimir.TabIndex = 3;
            this.btnImprimir.Text = "Imprimir Selecionadas";
            this.btnImprimir.Click += new System.EventHandler(this.btnImprimir_Click);
            // 
            // rdbEnderecamento
            // 
            this.rdbEnderecamento.AutoSize = true;
            this.rdbEnderecamento.Checked = true;
            this.rdbEnderecamento.Location = new System.Drawing.Point(12, 66);
            this.rdbEnderecamento.Name = "rdbEnderecamento";
            this.rdbEnderecamento.Size = new System.Drawing.Size(100, 17);
            this.rdbEnderecamento.TabIndex = 4;
            this.rdbEnderecamento.TabStop = true;
            this.rdbEnderecamento.Text = "Endereçamento";
            this.rdbEnderecamento.UseVisualStyleBackColor = true;
            this.rdbEnderecamento.CheckedChanged += new System.EventHandler(this.rdbEnderecamento_CheckedChanged);
            // 
            // rdbEstantes
            // 
            this.rdbEstantes.AutoSize = true;
            this.rdbEstantes.Location = new System.Drawing.Point(131, 66);
            this.rdbEstantes.Name = "rdbEstantes";
            this.rdbEstantes.Size = new System.Drawing.Size(136, 17);
            this.rdbEstantes.TabIndex = 5;
            this.rdbEstantes.Text = "Estantes de Separação";
            this.rdbEstantes.UseVisualStyleBackColor = true;
            this.rdbEstantes.CheckedChanged += new System.EventHandler(this.rdbEstantes_CheckedChanged);
            // 
            // rdbSenhas
            // 
            this.rdbSenhas.AutoSize = true;
            this.rdbSenhas.Location = new System.Drawing.Point(295, 66);
            this.rdbSenhas.Name = "rdbSenhas";
            this.rdbSenhas.Size = new System.Drawing.Size(61, 17);
            this.rdbSenhas.TabIndex = 6;
            this.rdbSenhas.Text = "Senhas";
            this.rdbSenhas.UseVisualStyleBackColor = true;
            this.rdbSenhas.CheckedChanged += new System.EventHandler(this.rdbSenhas_CheckedChanged);
            // 
            // rdbCarrinhos
            // 
            this.rdbCarrinhos.AutoSize = true;
            this.rdbCarrinhos.Location = new System.Drawing.Point(383, 66);
            this.rdbCarrinhos.Name = "rdbCarrinhos";
            this.rdbCarrinhos.Size = new System.Drawing.Size(69, 17);
            this.rdbCarrinhos.TabIndex = 7;
            this.rdbCarrinhos.Text = "Carrinhos";
            this.rdbCarrinhos.UseVisualStyleBackColor = true;
            this.rdbCarrinhos.CheckedChanged += new System.EventHandler(this.rdbCarrinhos_CheckedChanged);
            // 
            // rdbEstantePacote
            // 
            this.rdbEstantePacote.AutoSize = true;
            this.rdbEstantePacote.Location = new System.Drawing.Point(467, 66);
            this.rdbEstantePacote.Name = "rdbEstantePacote";
            this.rdbEstantePacote.Size = new System.Drawing.Size(113, 17);
            this.rdbEstantePacote.TabIndex = 8;
            this.rdbEstantePacote.Text = "Estante de Pacote";
            this.rdbEstantePacote.UseVisualStyleBackColor = true;
            this.rdbEstantePacote.CheckedChanged += new System.EventHandler(this.rdbEstantePacote_CheckedChanged);
            // 
            // frmImprimirEnderecamento
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(958, 520);
            this.Controls.Add(this.rdbEstantePacote);
            this.Controls.Add(this.rdbCarrinhos);
            this.Controls.Add(this.rdbSenhas);
            this.Controls.Add(this.rdbEstantes);
            this.Controls.Add(this.rdbEnderecamento);
            this.Controls.Add(this.btnImprimir);
            this.Controls.Add(this.grd);
            this.Name = "frmImprimirEnderecamento";
            this.Text = "frmImprimirEnderecamento";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmImprimirEnderecamento_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grd.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnImprimir)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadGridView grd;
        private Telerik.WinControls.UI.RadButton btnImprimir;
        private System.Windows.Forms.RadioButton rdbEnderecamento;
        private System.Windows.Forms.RadioButton rdbEstantes;
        private System.Windows.Forms.RadioButton rdbSenhas;
        private System.Windows.Forms.RadioButton rdbCarrinhos;
        private System.Windows.Forms.RadioButton rdbEstantePacote;
    }
}