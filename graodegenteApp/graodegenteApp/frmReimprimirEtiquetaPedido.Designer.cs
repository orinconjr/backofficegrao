﻿namespace graodegenteApp
{
    partial class frmReimprimirEtiquetaPedido
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnImprimir = new Telerik.WinControls.UI.RadButton();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.txtIdItemPedido = new Telerik.WinControls.UI.RadTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.btnImprimir)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIdItemPedido)).BeginInit();
            this.SuspendLayout();
            // 
            // btnImprimir
            // 
            this.btnImprimir.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnImprimir.Location = new System.Drawing.Point(22, 100);
            this.btnImprimir.Name = "btnImprimir";
            this.btnImprimir.Size = new System.Drawing.Size(230, 39);
            this.btnImprimir.TabIndex = 6;
            this.btnImprimir.Text = "IMPRIMIR";
            this.btnImprimir.Click += new System.EventHandler(this.btnImprimir_Click);
            // 
            // radLabel2
            // 
            this.radLabel2.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel2.Location = new System.Drawing.Point(22, 9);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(153, 37);
            this.radLabel2.TabIndex = 8;
            this.radLabel2.Text = "ID do Pedido";
            // 
            // txtIdItemPedido
            // 
            this.txtIdItemPedido.Font = new System.Drawing.Font("Segoe UI", 16F);
            this.txtIdItemPedido.Location = new System.Drawing.Point(22, 48);
            this.txtIdItemPedido.Name = "txtIdItemPedido";
            this.txtIdItemPedido.Size = new System.Drawing.Size(230, 34);
            this.txtIdItemPedido.TabIndex = 7;
            this.txtIdItemPedido.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtIdItemPedido_KeyDown);
            // 
            // frmReimprimirEtiquetaPedido
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Azure;
            this.ClientSize = new System.Drawing.Size(274, 149);
            this.Controls.Add(this.radLabel2);
            this.Controls.Add(this.txtIdItemPedido);
            this.Controls.Add(this.btnImprimir);
            this.Name = "frmReimprimirEtiquetaPedido";
            this.Text = "frmReimprimirEtiquetaPedido";
            this.Load += new System.EventHandler(this.frmReimprimirEtiquetaPedido_Load);
            ((System.ComponentModel.ISupportInitialize)(this.btnImprimir)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIdItemPedido)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadButton btnImprimir;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadTextBox txtIdItemPedido;
    }
}