﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Microsoft.Win32;

namespace graodegenteApp
{
    public class clsImprimirEtiqueta
    {
        public static string GerarComandoEtiquetaLote(string idPedidoFornecedorItem, string idPedidoFornecedor,
            string fornecedorNome,
            string numeroItem, string totalItens, string produtoIdDaEmpresa, string complementoIdDaEmpresa,
            string produtoNome, string codigoEtiqueta, string produtoId)
        {
            var etiqueta = new StringBuilder();
            etiqueta.Append("N");
            etiqueta.Append(Environment.NewLine);
            etiqueta.Append("q800");
            etiqueta.Append(Environment.NewLine);
            etiqueta.Append("Q600,26");
            etiqueta.Append(Environment.NewLine);
            etiqueta.AppendFormat("B90,20,0,3,2,5,110,N,\"{0}\"", idPedidoFornecedorItem);


            etiqueta.Append(Environment.NewLine);
            etiqueta.AppendFormat("A350,20,0,4,1,1,N,\"{0} - {1}\"", idPedidoFornecedor, limpaStringDeixaEspaco(fornecedorNome));
            etiqueta.Append(Environment.NewLine);
            etiqueta.AppendFormat("A350,50,0,4,1,1,N,\"Et: {0} - Prod: {1}/{2}\"",idPedidoFornecedorItem, numeroItem, totalItens);
            etiqueta.Append(Environment.NewLine);
            etiqueta.AppendFormat("A350,80,0,4,1,1,N,\"{0} - {1}\"", produtoId, produtoIdDaEmpresa);
            etiqueta.Append(Environment.NewLine);
            etiqueta.AppendFormat("A350,110,0,4,1,1,N,\"{0}\"", limpaStringDeixaEspaco(complementoIdDaEmpresa));
            etiqueta.Append(Environment.NewLine);
            string nomeformatado = limpaStringDeixaEspaco(produtoNome + " ");
            nomeformatado = nomeformatado.ToLower().Replace("quarto enxoval para bebe", "");
            //nomeformatado = nomeformatado.ToLower().Replace("meninos", "");
            //nomeformatado = nomeformatado.ToLower().Replace("meninas", "");
            nomeformatado = nomeformatado.ToLower().Replace("enxoval", "");
            nomeformatado = nomeformatado.ToLower().Replace("-", " ");
            nomeformatado = nomeformatado.ToLower().Replace("    ", " ");
            nomeformatado = nomeformatado.ToLower().Replace("   ", " ");
            nomeformatado = nomeformatado.ToLower().Replace("  ", " ");
            if (fornecedorNome.ToLower().Trim() == "decalta baby")
            {
                nomeformatado = nomeformatado.ToLower().Replace("padrao americano", "");
                nomeformatado = nomeformatado.ToLower().Replace("100 algodao", "");
                nomeformatado = nomeformatado.ToLower().Replace("120 fios", "");
            }

            var arrayNomes = nomeformatado.Split(' ');
            string nome = "";
            int nomeLinhaAtual = 160;
            int linhas = 0;
            int nomeLidos = 0;
            foreach (var arrayNome in arrayNomes)
            {
                nomeLidos++;
                if (linhas < 6)
                {
                    bool adicionado = false;
                    if (!string.IsNullOrEmpty(nome))
                    {
                        nome += " ";
                    }
                    if (nome.Length + arrayNome.Length < 40)
                    {
                        nome += arrayNome;
                    }
                    else
                    {
                        etiqueta.AppendFormat("A46,{0},0,4,1,1,N,\"{1}\"", nomeLinhaAtual, nome.Trim().ToUpper());
                        etiqueta.Append(Environment.NewLine);
                        nome = arrayNome;
                        nomeLinhaAtual += 30;
                        linhas += 1;
                        adicionado = true;
                    }
                    if (nomeLidos == arrayNomes.Length && adicionado == false)
                    {
                        etiqueta.AppendFormat("A46,{0},0,4,1,1,N,\"{1}\"", nomeLinhaAtual, nome.Trim().ToUpper());
                        etiqueta.Append(Environment.NewLine);
                        nome = arrayNome;
                        nomeLinhaAtual += 30;
                        linhas += 1;
                    }
                }
            }
            etiqueta.AppendFormat("A46,250,0,5,2,4,N,\"{0}\"", codigoEtiqueta.ToUpper());
            etiqueta.Append(Environment.NewLine);
            etiqueta.AppendFormat("b610,410,Q,1,s8,eH,\"{0}\"", idPedidoFornecedorItem);
            //etiqueta.AppendFormat("b680,450,Q,1,s8,eH,\"{0}\"", idPedidoFornecedorItem);
            etiqueta.Append(Environment.NewLine);
            etiqueta.AppendFormat("b46,470,Q,1,s4,eH,\"{0}\"", idPedidoFornecedorItem);
            etiqueta.Append(Environment.NewLine);
            etiqueta.Append("P1,1");
            etiqueta.Append(Environment.NewLine);
            return etiqueta.ToString();
        }

        public static void GerarEtiqueta(string idPedidoFornecedorItem, string idPedidoFornecedor, string fornecedorNome,
            string numeroItem, string totalItens, string produtoIdDaEmpresa, string complementoIdDaEmpresa,
            string produtoNome, string codigoEtiqueta, string produtoId)
        {
            var etiqueta = GerarComandoEtiquetaLote(idPedidoFornecedorItem, idPedidoFornecedor, fornecedorNome, numeroItem, totalItens, produtoIdDaEmpresa, complementoIdDaEmpresa, produtoNome, codigoEtiqueta, produtoId);
            RawPrinterHelper.SendStringToPrinter(Properties.Settings.Default["impressoraZebra"].ToString(), etiqueta.ToString());
            //RawPrinterHelper.SendStringToPrinter("zebra em ALECIO-PC", etiqueta.ToString());
        }
        public static void ImprimeEtiqueta(string etiqueta)
        {
            RawPrinterHelper.SendStringToPrinter(Properties.Settings.Default["impressoraZebra"].ToString(), etiqueta.ToString());
            //RawPrinterHelper.SendStringToPrinter("zebra em ALECIO-PC", etiqueta.ToString());
        }
        public static void TesteGprinter()
        {
            var etiqueta = new StringBuilder();
            etiqueta.Append("teste");
            etiqueta.Append(Environment.NewLine);
            etiqueta.Append("<J>");
            etiqueta.Append(Environment.NewLine);
            var etiquetaString = etiqueta.ToString();

            string ESC = "0x1B"; //ESC byte in hex notation
            string NewLine = "0x0A"; //LF byte in hex notation

            string cmds = ESC + "@"; //Initializes the printer (ESC @)
            cmds += ESC + "!" + "0x38"; //Emphasized + Double-height + Double-width mode selected (ESC ! (8 + 16 + 32)) 56 dec => 38 hex
            cmds += "BEST DEAL STORES"; //text to print
            cmds += NewLine + NewLine;
            cmds += ESC + "!" + "0x00"; //Character font A selected (ESC ! 0)
            cmds += "COOKIES                   5.00";
            cmds += NewLine;
            cmds += "MILK 65 Fl oz             3.78";
            cmds += NewLine + NewLine;
            cmds += "SUBTOTAL                  8.78";
            cmds += NewLine;
            cmds += "TAX 5%                    0.44";
            cmds += NewLine;
            cmds += "TOTAL                     9.22";
            cmds += NewLine;
            cmds += "CASH TEND                10.00";
            cmds += NewLine;
            cmds += "CASH DUE                  0.78";
            cmds += NewLine + NewLine;
            cmds += ESC + "!" + "0x18"; //Emphasized + Double-height mode selected (ESC ! (16 + 8)) 24 dec => 18 hex
            cmds += "# ITEMS SOLD 2";
            cmds += ESC + "!" + "0x00"; //Character font A selected (ESC ! 0)
            cmds += NewLine + NewLine;
            cmds += "11/03/13  19:53:17";

            cmds += NewLine + NewLine;

            RawPrinterHelper.SendStringToPrinter(Properties.Settings.Default["impressoraZebra"].ToString(), cmds.ToString());
        }

        public static void GerarEtiquetaEndereco(string pedido, string telResidencial, string telComercial, string telCelular, string nomeDestinatario, string rua, string numero, string complemento, string bairro, string cidade, string estado, string cep, string referencia)
        {
            var etiqueta = new StringBuilder();
            etiqueta.Append("N");
            etiqueta.Append(Environment.NewLine);
            etiqueta.Append("q800");
            etiqueta.Append(Environment.NewLine);
            etiqueta.Append("Q600,26");
            etiqueta.Append(Environment.NewLine);
            etiqueta.AppendFormat("B150,20,0,3,2,5,110,B,\"{0}\"", pedido);
            etiqueta.Append(Environment.NewLine);
            int linhaAtual = 180;
            int incremento = 25;
            int limiteLinha = 47;
            int limiteLinha2 = 53;
            
            etiqueta.AppendFormat("A50,{0},0,4,1,1,N,\"{1}\"", linhaAtual, "Destinatario:");
            etiqueta.Append(Environment.NewLine);
            linhaAtual += incremento;
            string nomeformatado = limpaStringDeixaEspaco(nomeDestinatario + " ");
            nomeformatado = nomeformatado.ToLower().Replace("-", " ");
            etiqueta.AppendFormat("A50,{0},0,4,1,1,N,\"{1}\"", linhaAtual, nomeformatado.Trim().ToUpper());
            etiqueta.Append(Environment.NewLine);
            linhaAtual += incremento;

            string nome = "";
            string enderecoCompleto = limpaStringDeixaEspaco(rua) + " " + limpaStringDeixaEspaco(numero) + ", " + limpaStringDeixaEspaco(complemento);
            enderecoCompleto = enderecoCompleto.Replace("    ", " ").Replace("   ", " ").Replace("  ", " ");
            int nomeLidos = 0;
            var arrayEndereco = enderecoCompleto.Split(' ');
            foreach (var arrayNome in arrayEndereco)
            {
                nomeLidos++;
                bool adicionado = false;
                if (!string.IsNullOrEmpty(nome))
                {
                    nome += " ";
                }
                if (nome.Length + arrayNome.Length < limiteLinha)
                {
                    nome += arrayNome;
                }
                else
                {
                    etiqueta.AppendFormat("A50,{0},0,4,1,1,N,\"{1}\"", linhaAtual, nome.Trim().ToUpper());
                    etiqueta.Append(Environment.NewLine);
                    nome = arrayNome;
                    linhaAtual += incremento;
                    adicionado = true;
                }
                if (nomeLidos == arrayEndereco.Length && adicionado == false)
                {
                    etiqueta.AppendFormat("A50,{0},0,4,1,1,N,\"{1}\"", linhaAtual, nome.Trim().ToUpper());
                    etiqueta.Append(Environment.NewLine);
                    nome = arrayNome;
                    linhaAtual += incremento;
                }
            }

            nome = "";
            nomeLidos = 0;
            string cidadeCompleta = limpaStringDeixaEspaco(bairro) + " - " + limpaStringDeixaEspaco(cidade) + " - " + limpaStringDeixaEspaco(estado);
            var arrayCidade = cidadeCompleta.Split(' ');
            foreach (var arrayNome in arrayCidade)
            {
                nomeLidos++;
                bool adicionado = false;
                if (!string.IsNullOrEmpty(nome))
                {
                    nome += " ";
                }
                if (nome.Length + arrayNome.Length < limiteLinha)
                {
                    nome += arrayNome;
                }
                else
                {
                    etiqueta.AppendFormat("A50,{0},0,4,1,1,N,\"{1}\"", linhaAtual, nome.Trim().ToUpper());
                    etiqueta.Append(Environment.NewLine);
                    nome = arrayNome;
                    linhaAtual += incremento;
                    adicionado = true;
                }
                if (nomeLidos == arrayCidade.Length && adicionado == false)
                {
                    etiqueta.AppendFormat("A50,{0},0,4,1,1,N,\"{1}\"", linhaAtual, nome.Trim().ToUpper());
                    etiqueta.Append(Environment.NewLine);
                    nome = arrayNome;
                    linhaAtual += incremento;
                }
            }


            etiqueta.AppendFormat("A50,{0},0,4,1,1,N,\"CEP: {1}\"", linhaAtual, cep);
            etiqueta.Append(Environment.NewLine);
            linhaAtual += incremento;

            nome = "";
            nomeLidos = 0;
            string referenciaCompleta = referencia;
            if (string.IsNullOrEmpty(referenciaCompleta)) referenciaCompleta += " ";
            var arrayReferencia = referenciaCompleta.Split(' ');
            linhaAtual += 10;
            foreach (var arrayNome in arrayReferencia)
            {
                nomeLidos++;
                bool adicionado = false;
                if (!string.IsNullOrEmpty(nome))
                {
                    nome += " ";
                }
                if (nome.Length + arrayNome.Length < limiteLinha2)
                {
                    nome += arrayNome;
                }
                else
                {
                    etiqueta.AppendFormat("A50,{0},0,3,1,1,N,\"{1}\"", linhaAtual, nome.Trim().ToUpper());
                    etiqueta.Append(Environment.NewLine);
                    nome = arrayNome;
                    linhaAtual += incremento;
                    adicionado = true;
                }
                if (nomeLidos == arrayReferencia.Length && adicionado == false)
                {
                    etiqueta.AppendFormat("A50,{0},0,3,1,1,N,\"{1}\"", linhaAtual, nome.Trim().ToUpper());
                    etiqueta.Append(Environment.NewLine);
                    nome = arrayNome;
                    linhaAtual += incremento;
                }
            }
            string telefone = "";
            if (!string.IsNullOrEmpty(telResidencial)) telefone += "Res.: " + telResidencial + " - ";
            if (!string.IsNullOrEmpty(telComercial)) telefone += "Com.: " + telComercial + " - ";
            if (!string.IsNullOrEmpty(telCelular)) telefone += "Cel.: " + telCelular;
            if (!string.IsNullOrEmpty(telefone))
            {
                linhaAtual += 10;
                etiqueta.AppendFormat("A50,{0},0,3,1,1,N,\"{1}\"", linhaAtual, telefone.Trim().ToUpper());
                etiqueta.Append(Environment.NewLine);
                linhaAtual += incremento;
            }

            /*nomeformatado = nomeformatado.ToLower().Replace("enxoval", "");
            nomeformatado = nomeformatado.ToLower().Replace("-", " ");
            nomeformatado = nomeformatado.ToLower().Replace("    ", " ");
            nomeformatado = nomeformatado.ToLower().Replace("   ", " ");
            nomeformatado = nomeformatado.ToLower().Replace("  ", " ");
            if (fornecedorNome.ToLower().Trim() == "decalta baby")
            {
                nomeformatado = nomeformatado.ToLower().Replace("padrao americano", "");
                nomeformatado = nomeformatado.ToLower().Replace("100 algodao", "");
                nomeformatado = nomeformatado.ToLower().Replace("120 fios", "");
            }
            var arrayNomes = nomeformatado.Split(' ');
            string nome = "";
            int nomeLinhaAtual = 160;
            int linhas = 0;
            int nomeLidos = 0;
            foreach (var arrayNome in arrayNomes)
            {
                nomeLidos++;
                if (linhas < 6)
                {
                    bool adicionado = false;
                    if (!string.IsNullOrEmpty(nome))
                    {
                        nome += " ";
                    }
                    if (nome.Length + arrayNome.Length < 19)
                    {
                        nome += arrayNome;
                    }
                    else
                    {
                        etiqueta.AppendFormat("A26,{0},0,5,1,1,N,\"{1}\"", nomeLinhaAtual, nome.Trim().ToUpper());
                        etiqueta.Append(Environment.NewLine);
                        nome = arrayNome;
                        nomeLinhaAtual += 70;
                        linhas += 1;
                        adicionado = true;
                    }
                    if (nomeLidos == arrayNomes.Length && adicionado == false)
                    {
                        etiqueta.AppendFormat("A26,{0},0,5,1,1,N,\"{1}\"", nomeLinhaAtual, nome.Trim().ToUpper());
                        etiqueta.Append(Environment.NewLine);
                        nome = arrayNome;
                        nomeLinhaAtual += 70;
                        linhas += 1;
                    }
                }
            }*/
            etiqueta.Append("P1,1");
            etiqueta.Append(Environment.NewLine);
            var etiquetaString = etiqueta.ToString();
            RawPrinterHelper.SendStringToPrinter(Properties.Settings.Default["impressoraZebra"].ToString(), etiqueta.ToString());
        }

        public static void GerarEtiquetaEnderecoTnt(string pedido, string telResidencial, string telComercial, string telCelular, string nomeDestinatario, string rua, string numero, string complemento, string bairro, string cidade, string estado, string cep, string referencia, string cnpj, int numeroNota, int volumeAtual, int totalVolumes)
        {
            var etiqueta = new StringBuilder();
            etiqueta.Append("N");
            etiqueta.Append(Environment.NewLine);
            etiqueta.Append("q800");
            etiqueta.Append(Environment.NewLine);
            etiqueta.Append("Q600,26");
            etiqueta.Append(Environment.NewLine);
            etiqueta.AppendFormat("B80,20,0,3,2,5,110,B,\"{0}\"", pedido);


            string numeroNotaFormatado = String.Format("{0:000000000}", numeroNota);
            string volumeTotalFormatado = String.Format("{0:0000}", totalVolumes);
            string volumeAtualFormatado = String.Format("{0:0000}", volumeAtual);

            etiqueta.Append(Environment.NewLine);
            etiqueta.AppendFormat("b610,20,Q,1,s6,eH,\"{0}{1}{2}{3}\"", cnpj, numeroNotaFormatado, volumeAtualFormatado, volumeTotalFormatado);

            etiqueta.Append(Environment.NewLine);
            int linhaAtual = 180;
            int incremento = 25;
            int limiteLinha = 47;
            int limiteLinha2 = 53;
            
            etiqueta.AppendFormat("A50,{0},0,4,1,1,N,\"{1}\"", linhaAtual, "Destinatario:");
            etiqueta.Append(Environment.NewLine);
            linhaAtual += incremento;
            string nomeformatado = limpaStringDeixaEspaco(nomeDestinatario + " ");
            nomeformatado = nomeformatado.ToLower().Replace("-", " ");
            etiqueta.AppendFormat("A50,{0},0,4,1,1,N,\"{1}\"", linhaAtual, nomeformatado.Trim().ToUpper());
            etiqueta.Append(Environment.NewLine);
            linhaAtual += incremento;

            string nome = "";
            string enderecoCompleto = limpaStringDeixaEspaco(rua) + " " + limpaStringDeixaEspaco(numero) + ", " + limpaStringDeixaEspaco(complemento);
            enderecoCompleto = enderecoCompleto.Replace("    ", " ").Replace("   ", " ").Replace("  ", " ");
            int nomeLidos = 0;
            var arrayEndereco = enderecoCompleto.Split(' ');
            foreach (var arrayNome in arrayEndereco)
            {
                nomeLidos++;
                bool adicionado = false;
                if (!string.IsNullOrEmpty(nome))
                {
                    nome += " ";
                }
                if (nome.Length + arrayNome.Length < limiteLinha)
                {
                    nome += arrayNome;
                }
                else
                {
                    etiqueta.AppendFormat("A50,{0},0,4,1,1,N,\"{1}\"", linhaAtual, nome.Trim().ToUpper());
                    etiqueta.Append(Environment.NewLine);
                    nome = arrayNome;
                    linhaAtual += incremento;
                    adicionado = true;
                }
                if (nomeLidos == arrayEndereco.Length && adicionado == false)
                {
                    etiqueta.AppendFormat("A50,{0},0,4,1,1,N,\"{1}\"", linhaAtual, nome.Trim().ToUpper());
                    etiqueta.Append(Environment.NewLine);
                    nome = arrayNome;
                    linhaAtual += incremento;
                }
            }

            nome = "";
            nomeLidos = 0;
            string cidadeCompleta = limpaStringDeixaEspaco(bairro) + " - " + limpaStringDeixaEspaco(cidade) + " - " + limpaStringDeixaEspaco(estado);
            var arrayCidade = cidadeCompleta.Split(' ');
            foreach (var arrayNome in arrayCidade)
            {
                nomeLidos++;
                bool adicionado = false;
                if (!string.IsNullOrEmpty(nome))
                {
                    nome += " ";
                }
                if (nome.Length + arrayNome.Length < limiteLinha)
                {
                    nome += arrayNome;
                }
                else
                {
                    etiqueta.AppendFormat("A50,{0},0,4,1,1,N,\"{1}\"", linhaAtual, nome.Trim().ToUpper());
                    etiqueta.Append(Environment.NewLine);
                    nome = arrayNome;
                    linhaAtual += incremento;
                    adicionado = true;
                }
                if (nomeLidos == arrayCidade.Length && adicionado == false)
                {
                    etiqueta.AppendFormat("A50,{0},0,4,1,1,N,\"{1}\"", linhaAtual, nome.Trim().ToUpper());
                    etiqueta.Append(Environment.NewLine);
                    nome = arrayNome;
                    linhaAtual += incremento;
                }
            }


            etiqueta.AppendFormat("A50,{0},0,4,1,1,N,\"CEP: {1}\"", linhaAtual, cep);
            etiqueta.Append(Environment.NewLine);
            linhaAtual += incremento;

            //nome = "";
            //nomeLidos = 0;
            //string referenciaCompleta = referencia;
            //if (string.IsNullOrEmpty(referenciaCompleta)) referenciaCompleta += " ";
            //var arrayReferencia = referenciaCompleta.Split(' ');
            //linhaAtual += 10;
            //foreach (var arrayNome in arrayReferencia)
            //{
            //    nomeLidos++;
            //    bool adicionado = false;
            //    if (!string.IsNullOrEmpty(nome))
            //    {
            //        nome += " ";
            //    }
            //    if (nome.Length + arrayNome.Length < limiteLinha2)
            //    {
            //        nome += arrayNome;
            //    }
            //    else
            //    {
            //        etiqueta.AppendFormat("A50,{0},0,3,1,1,N,\"{1}\"", linhaAtual, nome.Trim().ToUpper());
            //        etiqueta.Append(Environment.NewLine);
            //        nome = arrayNome;
            //        linhaAtual += incremento;
            //        adicionado = true;
            //    }
            //    if (nomeLidos == arrayReferencia.Length && adicionado == false)
            //    {
            //        etiqueta.AppendFormat("A50,{0},0,3,1,1,N,\"{1}\"", linhaAtual, nome.Trim().ToUpper());
            //        etiqueta.Append(Environment.NewLine);
            //        nome = arrayNome;
            //        linhaAtual += incremento;
            //    }
            //}


            string telefone = "";
            if (!string.IsNullOrEmpty(telResidencial)) telefone += "Res.: " + telResidencial + " - ";
            if (!string.IsNullOrEmpty(telComercial)) telefone += "Com.: " + telComercial + " - ";
            if (!string.IsNullOrEmpty(telCelular)) telefone += "Cel.: " + telCelular;
            if (!string.IsNullOrEmpty(telefone))
            {
                linhaAtual += 10;
                etiqueta.AppendFormat("A50,{0},0,3,1,1,N,\"{1}\"", linhaAtual, telefone.Trim().ToUpper());
                etiqueta.Append(Environment.NewLine);
                linhaAtual += incremento;
            }
            
            linhaAtual += incremento;

            etiqueta.AppendFormat("B90,400,0,1,2,5,110,B,\"{1}{2}{3}{4}\"", linhaAtual, cnpj, numeroNotaFormatado, volumeAtualFormatado, volumeTotalFormatado);
            etiqueta.Append(Environment.NewLine);

            etiqueta.Append("P1,1");
            etiqueta.Append(Environment.NewLine);
            var etiquetaString = etiqueta.ToString();
            RawPrinterHelper.SendStringToPrinter(Properties.Settings.Default["impressoraZebra"].ToString(), etiqueta.ToString());
        }

        public static void GerarEtiquetaEnderecoTntCd2(string pedido, string telResidencial, string telComercial, string telCelular, string nomeDestinatario, string rua, string numero, string complemento, string bairro, string cidade, string estado, string cep, string referencia, string cnpj, int numeroNota, int volumeAtual, int totalVolumes, string produto, string transportadora)
        {
            var etiqueta = new StringBuilder();
            etiqueta.Append("N");
            etiqueta.Append(Environment.NewLine);
            etiqueta.Append("q800");
            etiqueta.Append(Environment.NewLine);
            etiqueta.Append("Q600,26");
            etiqueta.Append(Environment.NewLine);
            etiqueta.AppendFormat("B80,20,0,3,2,5,110,B,\"{0}\"", pedido);

            string numeroNotaFormatado = String.Format("{0:000000000}", numeroNota);
            string volumeTotalFormatado = String.Format("{0:0000}", totalVolumes);
            string volumeAtualFormatado = String.Format("{0:0000}", volumeAtual);

            etiqueta.Append(Environment.NewLine);
            etiqueta.AppendFormat("b610,20,Q,1,s6,eH,\"{0}{1}{2}{3}\"", cnpj, numeroNotaFormatado, volumeAtualFormatado, volumeTotalFormatado);

            //etiqueta.AppendFormat("b46,20,Q,1,s4,eH,\"{0}\"", idPedidoFornecedorItem);

            etiqueta.Append(Environment.NewLine);
            int linhaAtual = 180;
            int incremento = 25;
            int limiteLinha = 47;
            int limiteLinha2 = 53;

            etiqueta.AppendFormat("A50,{0},0,4,1,1,N,\"{1}\"", linhaAtual, "Transportadora: " + transportadora);
            etiqueta.Append(Environment.NewLine);
            linhaAtual += incremento;

            string nome = "";
            string produtoCompleto = clsImprimirEtiqueta.limpaStringDeixaEspaco(produto);
            produtoCompleto = produtoCompleto.Replace("    ", " ").Replace("   ", " ").Replace("  ", " ");
            int nomeLidos = 0;
            var arrayEndereco = produtoCompleto.Split(' ');
            foreach (var arrayNome in arrayEndereco)
            {
                nomeLidos++;
                bool adicionado = false;
                if (!string.IsNullOrEmpty(nome))
                {
                    nome += " ";
                }
                if (nome.Length + arrayNome.Length < limiteLinha)
                {
                    nome += arrayNome;
                }
                else
                {
                    etiqueta.AppendFormat("A50,{0},0,4,1,1,N,\"{1}\"", linhaAtual, nome.Trim().ToUpper());
                    etiqueta.Append(Environment.NewLine);
                    nome = arrayNome;
                    linhaAtual += incremento;
                    adicionado = true;
                }
                if (nomeLidos == arrayEndereco.Length && adicionado == false)
                {
                    etiqueta.AppendFormat("A50,{0},0,4,1,1,N,\"{1}\"", linhaAtual, nome.Trim().ToUpper());
                    etiqueta.Append(Environment.NewLine);
                    nome = arrayNome;
                    linhaAtual += incremento;
                }
            }

            limiteLinha = 47;
            etiqueta.AppendFormat("A50,{0},0,4,1,1,N,\"{1}\"", linhaAtual, "Destinatario:");
            etiqueta.Append(Environment.NewLine);
            linhaAtual += incremento;
            string nomeformatado = limpaStringDeixaEspaco(nomeDestinatario + " ");
            nomeformatado = nomeformatado.ToLower().Replace("-", " ");
            etiqueta.AppendFormat("A50,{0},0,4,1,1,N,\"{1}\"", linhaAtual, nomeformatado.Trim().ToUpper());
            etiqueta.Append(Environment.NewLine);
            linhaAtual += incremento;

            nome = "";
            string enderecoCompleto = limpaStringDeixaEspaco(rua) + " " + limpaStringDeixaEspaco(numero) + ", " + limpaStringDeixaEspaco(complemento);
            enderecoCompleto = enderecoCompleto.Replace("    ", " ").Replace("   ", " ").Replace("  ", " ");
            nomeLidos = 0;
            arrayEndereco = enderecoCompleto.Split(' ');
            foreach (var arrayNome in arrayEndereco)
            {
                nomeLidos++;
                bool adicionado = false;
                if (!string.IsNullOrEmpty(nome))
                {
                    nome += " ";
                }
                if (nome.Length + arrayNome.Length < limiteLinha)
                {
                    nome += arrayNome;
                }
                else
                {
                    etiqueta.AppendFormat("A50,{0},0,4,1,1,N,\"{1}\"", linhaAtual, nome.Trim().ToUpper());
                    etiqueta.Append(Environment.NewLine);
                    nome = arrayNome;
                    linhaAtual += incremento;
                    adicionado = true;
                }
                if (nomeLidos == arrayEndereco.Length && adicionado == false)
                {
                    etiqueta.AppendFormat("A50,{0},0,4,1,1,N,\"{1}\"", linhaAtual, nome.Trim().ToUpper());
                    etiqueta.Append(Environment.NewLine);
                    nome = arrayNome;
                    linhaAtual += incremento;
                }
            }

            nome = "";
            nomeLidos = 0;
            string cidadeCompleta = limpaStringDeixaEspaco(bairro) + " - " + limpaStringDeixaEspaco(cidade) + " - " + limpaStringDeixaEspaco(estado);
            var arrayCidade = cidadeCompleta.Split(' ');
            foreach (var arrayNome in arrayCidade)
            {
                nomeLidos++;
                bool adicionado = false;
                if (!string.IsNullOrEmpty(nome))
                {
                    nome += " ";
                }
                if (nome.Length + arrayNome.Length < limiteLinha)
                {
                    nome += arrayNome;
                }
                else
                {
                    etiqueta.AppendFormat("A50,{0},0,4,1,1,N,\"{1}\"", linhaAtual, nome.Trim().ToUpper());
                    etiqueta.Append(Environment.NewLine);
                    nome = arrayNome;
                    linhaAtual += incremento;
                    adicionado = true;
                }
                if (nomeLidos == arrayCidade.Length && adicionado == false)
                {
                    etiqueta.AppendFormat("A50,{0},0,4,1,1,N,\"{1}\"", linhaAtual, nome.Trim().ToUpper());
                    etiqueta.Append(Environment.NewLine);
                    nome = arrayNome;
                    linhaAtual += incremento;
                }
            }

            etiqueta.AppendFormat("A50,{0},0,4,1,1,N,\"CEP: {1}\"", linhaAtual, cep);
            etiqueta.Append(Environment.NewLine);
            linhaAtual += incremento;

            //nome = "";
            //nomeLidos = 0;
            //string referenciaCompleta = referencia;
            //if (string.IsNullOrEmpty(referenciaCompleta)) referenciaCompleta += " ";
            //var arrayReferencia = referenciaCompleta.Split(' ');
            //linhaAtual += 10;
            //foreach (var arrayNome in arrayReferencia)
            //{
            //    nomeLidos++;
            //    bool adicionado = false;
            //    if (!string.IsNullOrEmpty(nome))
            //    {
            //        nome += " ";
            //    }
            //    if (nome.Length + arrayNome.Length < limiteLinha2)
            //    {
            //        nome += arrayNome;
            //    }
            //    else
            //    {
            //        etiqueta.AppendFormat("A50,{0},0,3,1,1,N,\"{1}\"", linhaAtual, nome.Trim().ToUpper());
            //        etiqueta.Append(Environment.NewLine);
            //        nome = arrayNome;
            //        linhaAtual += incremento;
            //        adicionado = true;
            //    }
            //    if (nomeLidos == arrayReferencia.Length && adicionado == false)
            //    {
            //        etiqueta.AppendFormat("A50,{0},0,3,1,1,N,\"{1}\"", linhaAtual, nome.Trim().ToUpper());
            //        etiqueta.Append(Environment.NewLine);
            //        nome = arrayNome;
            //        linhaAtual += incremento;
            //    }
            //}


            string telefone = "";
            if (!string.IsNullOrEmpty(telResidencial)) telefone += "Res.: " + telResidencial + " - ";
            if (!string.IsNullOrEmpty(telComercial)) telefone += "Com.: " + telComercial + " - ";
            if (!string.IsNullOrEmpty(telCelular)) telefone += "Cel.: " + telCelular;
            if (!string.IsNullOrEmpty(telefone))
            {
                linhaAtual += 10;
                etiqueta.AppendFormat("A50,{0},0,3,1,1,N,\"{1}\"", linhaAtual, telefone.Trim().ToUpper());
                etiqueta.Append(Environment.NewLine);
                linhaAtual += incremento;
            }

            linhaAtual += incremento;

            etiqueta.AppendFormat("B90,400,0,1,2,5,110,B,\"{1}{2}{3}{4}\"", linhaAtual, cnpj, numeroNotaFormatado, volumeAtualFormatado, volumeTotalFormatado);
            etiqueta.Append(Environment.NewLine);

            etiqueta.Append("P1,1");
            etiqueta.Append(Environment.NewLine);
            var etiquetaString = etiqueta.ToString();
            RawPrinterHelper.SendStringToPrinter(Properties.Settings.Default["impressoraZebra"].ToString(), etiqueta.ToString());
        }

        public static string limpaStringDeixaEspaco(string texto)
        {
            string caracteresEspeciais = "ÄÅÁÂÀÃäáâàãÉÊËÈéêëèÍÎÏÌíîïìÖÓÔÒÕöóôòõÜÚÛüúûùÇç/`~!@#$%^&*()*+;:',<.>?|";
            string caracteresNormais = "AAAAAAaaaaaEEEEeeeeIIIIiiiiOOOOOoooooUUUuuuuCc------------------------";

            for (int i = 0; i < caracteresEspeciais.Length; i++)
            {
                try
                {
                    texto = texto.Replace(caracteresEspeciais[i].ToString(), caracteresNormais[i].ToString());
                    texto = texto.Replace("\"", "");
                    texto = texto.Replace("'", "");
                }
                catch (Exception ex) { }
            }
            return texto;
        }


        public static bool PrintPdf(string file, string printer)
        {
            try
            {
                Process.Start(
                   Registry.LocalMachine.OpenSubKey(
                        @"SOFTWARE\Microsoft\Windows\CurrentVersion" +
                        @"\App Paths\AcroRd32.exe").GetValue("").ToString(),
                   string.Format("/n /s /h /t \"{0}\" \"{1}\"", file, printer));
                return true;
            }
            catch { }
            return false;
        }
    }
}
