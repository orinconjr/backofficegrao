﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Media;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace graodegenteApp
{
    public static class Utilidades
    {
        public static Image FromURL(this Image image, string Url)
        {
            HttpWebRequest request = HttpWebRequest.Create(Url) as HttpWebRequest;
            HttpWebResponse respone = request.GetResponse() as HttpWebResponse;
            return Image.FromStream(respone.GetResponseStream(), true);
        }
        public static Image FromURL(this Image image, string Url, string userName, string Password)
        {
            HttpWebRequest request = HttpWebRequest.Create(Url) as HttpWebRequest;
            request.Credentials = new NetworkCredential(userName, Password);
            HttpWebResponse respone = request.GetResponse() as HttpWebResponse;
            return Image.FromStream(respone.GetResponseStream(), true);
        }
        public static void alerta()
        {
            SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.alarme2);
            simpleSound.Play();
        }
        public static int retornaIdCliente(int idPedido)
        {
            string magicNumber = "110010000011001111111100011101";
            var binario = Convert.ToString(idPedido, 2);
            var binarioCompleto = binario.PadLeft(30, '0');
            var binarioInvertido = ReverseString(binarioCompleto);
            var xor = xorIt(magicNumber, binarioInvertido).ToString();
            int idPedidoFinal = Convert.ToInt32(xor, 2);
            return idPedidoFinal;
        }

        public static int retornaIdInterno(int idCliente)
        {
            string magicNumber = "110010000011001111111100011101";
            var binarioXor = Convert.ToString(idCliente, 2).PadLeft(30, '0');
            var invertido = xorIt(magicNumber, binarioXor);
            var binario = ReverseString(invertido);
            int idPedido = Convert.ToInt32(binario, 2);
            return idPedido;
        }

        public static string ReverseString(string s)
        {
            char[] charArray = s.ToCharArray();
            Array.Reverse(charArray);
            return new string(charArray);
        }

        public static byte[] bytesArray(string input)
        {
            var bytesAsStrings = input.Select((c, i) => new { Char = c, Index = i }).GroupBy(x => x.Index / 8).Select(g => new string(g.Select(x => x.Char).ToArray()));
            byte[] bytes = bytesAsStrings.Select(s => Convert.ToByte(s, 2)).ToArray();
            return bytes;
        }

        public static string xorIt(string key, string input)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < input.Length; i++)
            {
                var xor = Convert.ToInt32(input[i]) ^ Convert.ToInt32(key[i]);
                sb.Append(xor);
            }
            var result = sb.ToString();

            return result;
        }

        public static string RetornaIpLocal()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            string ipLocal = "";
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    ipLocal = ip.ToString();
                }
            }
            return ipLocal;
        }
    }
}
