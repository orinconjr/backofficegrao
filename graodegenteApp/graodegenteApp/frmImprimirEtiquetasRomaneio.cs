﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;

namespace graodegenteApp
{
    
    public partial class frmImprimirEtiquetasRomaneio : Form
    {
        public int idUsuario = 0;
        public frmImprimirEtiquetasRomaneio()
        {
            InitializeComponent();
        }

        private void frmImprimirEtiquetasRomaneio_Load(object sender, EventArgs e)
        {
            ThemeResolutionService.ApplicationThemeName = "TelerikMetroBlue";

            
        }

        private void btnImprimir_Click(object sender, EventArgs e)
        {

            int idPedidoFornecedor = 0;
            int inicio = 0;
            int fim = 0;

            int.TryParse(txtIdPedidoFornecedor.Text.Trim(), out idPedidoFornecedor);
            int.TryParse(txtEtiquetaInicial.Text.Trim(), out inicio);
            int.TryParse(txtEtiquetaFinal.Text.Trim(), out fim);

            if (idPedidoFornecedor == 0)
            {
                MessageBox.Show("Número de pedido incorreto");
                return;
            }

            var produtos = new wsPedidosFornecedor.PedidosFornecedorWs();
            produtos.Timeout = 150000000; // or -1 for infinite
            var pedidoCheck = produtos.ChecaRomaneioImpresso(idPedidoFornecedor);
            bool imprimir = true;
            if (pedidoCheck == true)
            {
                var confirmar = MessageBox.Show("Este romaneio já foi impresso anteriormente. Deseja reimprimir?", "Confirmação", MessageBoxButtons.YesNo);
                if (confirmar == DialogResult.No) imprimir = false;
            }
            if (imprimir)
            {
                var itens = produtos.ListaDetalhesItemRomaneioUsuario(idPedidoFornecedor, inicio, fim, clsConfiguracao.ChaveWs, true, idUsuario);

                string etiqueta = "";

                foreach (var detalheProduto in itens)
                {
                    //MessageBox.Show(detalheProduto.codigoEtiqueta);
                    etiqueta += clsImprimirEtiqueta.GerarComandoEtiquetaLote(detalheProduto.idPedidoFornecedorItem.ToString(),
                        detalheProduto.idPedidoFornecedor.ToString(), detalheProduto.fornecedorNome,
                        detalheProduto.numeroItem.ToString(), detalheProduto.totalItens.ToString(),
                        detalheProduto.produtoIdDaEmpresa, detalheProduto.complementoIdDaEmpresa,
                        detalheProduto.produtoNome, detalheProduto.codigoEtiqueta, detalheProduto.produtoId.ToString());
                    
                    /*clsImprimirEtiqueta.GerarEtiqueta(detalheProduto.idPedidoFornecedorItem.ToString(),
                        detalheProduto.idPedidoFornecedor.ToString(), detalheProduto.fornecedorNome,
                        detalheProduto.numeroItem.ToString(), detalheProduto.totalItens.ToString(),
                        detalheProduto.produtoIdDaEmpresa, detalheProduto.complementoIdDaEmpresa,
                        detalheProduto.produtoNome);*/
                }
                clsImprimirEtiqueta.ImprimeEtiqueta(etiqueta);
                txtIdPedidoFornecedor.Text = "";
                txtEtiquetaInicial.Text = "";
                txtEtiquetaFinal.Text = "";
                txtIdPedidoFornecedor.Focus();
                //imprimePedido(idPedidoFornecedorItem);
            }
        }
    }
}
