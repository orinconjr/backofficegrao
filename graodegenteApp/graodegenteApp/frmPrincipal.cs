﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Security.Policy;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Xml;
using graodegenteApp.wsEmbalagem;
using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;
using Telerik.WinControls;

namespace graodegenteApp
{
    public partial class frmPrincipal : Form
    {
        public int idUsuarioExpedicao = 0;
        public int idPedidoEmbalagem = 0;
        public int idCentroDistribuicaoEtiqueta = 0;
        public string nomeUsuarioEmbalagem = "";

        public static Thread SocketThread = new Thread(StartIpListener);
        public frmPrincipal()
        {
            InitializeComponent();
        }

        private void frmPrincipal_Load(object sender, EventArgs e)
        {
            SocketThread.Start();
            ThemeResolutionService.ApplicationThemeName = "TelerikMetroBlue";
            PrinterSettings settings = new PrinterSettings();
            string nomeImpressora = settings.PrinterName;
            //public static Thread SocketThread = new Thread(StartIpListener);
            //SocketThread.Start();

            //sendMessage("mensagem de teste", "192.168.100.2");
            //var Porta = new SerialPort("COM3", 115200, Parity.None, 8, StopBits.One);
            //string CommDATA = "";
            //Porta.Open();
            //while (string.IsNullOrEmpty(CommDATA))
            //{
            //    CommDATA = Porta.ReadExisting();
            //}
            //Porta.Close();
            //var cubometro = new VSIcubber.Cubomentro();
            //MessageBox.Show(CommDATA);
            //cubometro.LerDimensao();

            //MessageBox.Show(cubometro.Altura + " - " + cubometro.Comprimento + " - " + cubometro.Largura + " - " + cubometro.Peso);

            //clsImprimirEtiqueta.PrintPdf("C:\\danfes\\1.pdf", nomeImpressora);
            //clsImprimirEtiqueta.GerarEtiquetaEnderecoTnt(Utilidades.retornaIdCliente(270314).ToString() + "-" + 1 + "/" + 3, "(13) 34481-341", "", "", "Sabrina Ferreira de Lima", "AVENIDA MIGUEL MUNHOZ BONILHA", "8", "", "BAL. PLATAFORMA", "Mongaguá", "SP", "11730-000", "AVENIDA MONTEIRO LOBATO", "23549499000196", 2356, 1, 3);
        }

        public static void StartIpListener()
        {
            string ipLocal = Utilidades.RetornaIpLocal();
            if (!string.IsNullOrEmpty(ipLocal))
            {
                IPAddress localAdd = IPAddress.Parse(ipLocal);
                TcpListener listener = new TcpListener(localAdd, 5000);
                listener.Start();
                while (true)
                {
                    //---incoming client connected---
                    TcpClient client = listener.AcceptTcpClient();

                    //---get the incoming data through a network stream---
                    NetworkStream nwStream = client.GetStream();
                    byte[] buffer = new byte[client.ReceiveBufferSize];

                    //---read incoming stream---
                    int bytesRead = nwStream.Read(buffer, 0, client.ReceiveBufferSize);

                    //---convert the data received into a string---
                    string dataReceived = Encoding.ASCII.GetString(buffer, 0, bytesRead);
                    MessageBox.Show(dataReceived);

                    //---write back the text to the client---
                    //Console.WriteLine("Sending back : " + dataReceived);
                    //nwStream.Write(buffer, 0, bytesRead);
                    client.Close();
                }
            }            
        }

        private void sendMessage(string mensagem, string ip)
        {
            
            TcpClient client = new TcpClient(ip, 5000);
            NetworkStream nwStream = client.GetStream();
            byte[] bytesToSend = ASCIIEncoding.ASCII.GetBytes(mensagem);
            
            nwStream.Write(bytesToSend, 0, bytesToSend.Length);

            //---read back the text---
            byte[] bytesToRead = new byte[client.ReceiveBufferSize];
            int bytesRead = nwStream.Read(bytesToRead, 0, client.ReceiveBufferSize);
            //Console.WriteLine("Received : " + Encoding.ASCII.GetString(bytesToRead, 0, bytesRead));
            Console.ReadLine();
            client.Close();
        }
        private void btnImprimirEtiquetasRomaneio_Click(object sender, EventArgs e)
        {
            clsImprimirEtiqueta.TesteGprinter();
            bool valido = false;
            var login = new frmLogin();
            login.paginaAutorizacao = "imprimiretiquetasromaneio";
            login.ShowDialog();
            if (login.DialogResult == DialogResult.OK)
            {
                var romaneios = new frmImprimirEtiquetasRomaneio();
                romaneios.idUsuario = login.idUsuario;
                romaneios.Show();
                valido = true;
            }

            if (!valido)
            {
                MessageBox.Show("Usuário incorreto");
            }
        }

        private void btnImprimirEtiquetaAvulsa_Click(object sender, EventArgs e)
        {
            bool valido = false;
            var login = new frmLogin();
            login.paginaAutorizacao = "imprimiretiquetasromaneioavulso";
            login.ShowDialog();
            if (login.DialogResult == DialogResult.OK)
            {
                var romaneios = new frmImprimirEtiquetaAvulsa();
                romaneios.idUsuario = login.idUsuario;
                romaneios.Show();
                valido = true;
            }

            if (!valido)
            {
                MessageBox.Show("Usuário incorreto");
            }
        }

        private void btnEntradaRomaneio_Click(object sender, EventArgs e)
        {
            bool valido = false;
            var login = new frmLogin();
            login.paginaAutorizacao = "imprimiretiquetasromaneio";
            login.ShowDialog();
            if (login.DialogResult == DialogResult.OK)
            {
                var romaneios = new frmEntradaRomaneio();
                romaneios.idUsuario = login.idUsuario;
                romaneios.Show();
                valido = true;
            }

            if (!valido)
            {
                MessageBox.Show("Usuário incorreto");
            }
        }

        private void radButtonElement1_Click(object sender, EventArgs e)
        {
            
        }

        private void btnConsultarRomaneio_Click(object sender, EventArgs e)
        {
            bool valido = false;
            var login = new frmLogin();
            login.paginaAutorizacao = "imprimiretiquetasromaneio";
            login.ShowDialog();
            if (login.DialogResult == DialogResult.OK)
            {
                var romaneios = new frmConsultarRomaneio();
                romaneios.idUsuario = login.idUsuario;
                romaneios.Show();
                valido = true;
            }

            if (!valido)
            {
                MessageBox.Show("Usuário incorreto");
            }
        }
        private void btnChecarEstoque_Click(object sender, EventArgs e)
        {
            //bool valido = false;
            //var login = new frmLogin();
            //login.paginaAutorizacao = "imprimiretiquetasromaneio";
            //login.ShowDialog();
            //if (login.DialogResult == DialogResult.OK)
            //{
            //    var romaneios = new frmChecarEstoque();
            //    romaneios.idUsuario = login.idUsuario;
            //    romaneios.Show();
            //    valido = true;
            //}

            //if (!valido)
            //{
            //    MessageBox.Show("Usuário incorreto");
            //}
        }


        public void carregaEmbalarQuantidade()
        {
            panel1.Controls.Clear();

            var embalarQuantidade = new usrEmbalarQuantidade();
            embalarQuantidade.Dock = DockStyle.Fill;

            panel1.Controls.Add(embalarQuantidade);
        }

        public void carregaEmbalarQuantidadeCd2(int idCentroDistribuicao)
        {
            panel1.Controls.Clear();

            var embalarQuantidade = new usrEmbalarQuantidade();
            embalarQuantidade.idCentroDistribuicao = idCentroDistribuicao;
            embalarQuantidade.Dock = DockStyle.Fill;

            panel1.Controls.Add(embalarQuantidade);
        }

        public void carregaEmbalarConferirProdutos()
        {
            panel1.Controls.Clear();

            var embalarConferirProdutos = new usrEmbalarConferirProdutos();
            embalarConferirProdutos.Dock = DockStyle.Fill;
            panel1.Controls.Add(embalarConferirProdutos);
            btnListaDeProdutos.Enabled = true;
            btnQuantidadePacotes.Enabled = false;
        }
        public void carregaEmbalarConferirPacotes()
        {
            panel1.Controls.Clear();

            var embalarConferirPacotes = new usrEmbalarConferirPacotes();
            embalarConferirPacotes.Dock = DockStyle.Fill;
            panel1.Controls.Add(embalarConferirPacotes);
            btnListaDeProdutos.Enabled = true;
            btnQuantidadePacotes.Enabled = true;
        }
        public void carregaEmbalarListaProdutos()
        {
            panel1.Controls.Clear();

            var embalarListaProdutos = new usrEmbalarProdutos();
            embalarListaProdutos.Dock = DockStyle.Fill;
            panel1.Controls.Add(embalarListaProdutos);
            btnListaDeProdutos.Enabled = false;
            btnQuantidadePacotes.Enabled = false;
        }
        public void carregaEmbalarQuantidadePacotes()
        {
            panel1.Controls.Clear();
            var embalarQuantidade = new usrEmbalarQuantidadePacotes();
            embalarQuantidade.Dock = DockStyle.Fill;
            panel1.Controls.Add(embalarQuantidade);
            btnListaDeProdutos.Enabled = true;
            btnQuantidadePacotes.Enabled = false;
        }
        public void carregaEmbalarAlterarPacotes()
        {
            panel1.Controls.Clear();
            var embalarQuantidade = new usrEmbalarAlterarPacotes();
            embalarQuantidade.Dock = DockStyle.Fill;
            panel1.Controls.Add(embalarQuantidade);
            btnListaDeProdutos.Enabled = false;
            btnQuantidadePacotes.Enabled = false;
        }
        public void carregaEmbalarPesoPacotes()
        {
            panel1.Controls.Clear();
            var embalarQuantidade = new usrEmbalarPesoPacotes();
            embalarQuantidade.Dock = DockStyle.Fill;
            panel1.Controls.Add(embalarQuantidade);
            btnListaDeProdutos.Enabled = true;
            btnQuantidadePacotes.Enabled = true;
        }
        public void carregaEmbalarRastreio()
        {
            panel1.Controls.Clear();
            var embalarQuantidade = new usrEmbalarRastreio();
            embalarQuantidade.Dock = DockStyle.Fill;
            panel1.Controls.Add(embalarQuantidade);
            btnListaDeProdutos.Enabled = true;
            btnQuantidadePacotes.Enabled = false;
        }
        public void carregaEnviarPor(string formaDeEnvio)
        {
            panel1.Dock = DockStyle.Fill;
            var embalarLogin = new usrEmbalarEnviarPor();
            embalarLogin.enviarPor = formaDeEnvio;
            embalarLogin.Dock = DockStyle.Fill;
            panel1.Controls.Clear();
            panel1.Controls.Add(embalarLogin);
            btnListaDeProdutos.Enabled = false;
            btnQuantidadePacotes.Enabled = false;
        }
        public void carregaEnviarPor(retornoFinalizaPedido formaDeEnvio)
        {
            panel1.Dock = DockStyle.Fill;
            var embalarLogin = new usrEmbalarEnviarPor();
            embalarLogin.enviarPor = formaDeEnvio.formaEnvio;
            embalarLogin.retornoFinaliza = formaDeEnvio;
            embalarLogin.Dock = DockStyle.Fill;
            panel1.Controls.Clear();
            panel1.Controls.Add(embalarLogin);
            btnListaDeProdutos.Enabled = false;
            btnQuantidadePacotes.Enabled = false;
            embalarLogin.ImprimeEtiquetas();
        }
        public void carregaEnviarPorCd2(retornoFinalizaPedido formaDeEnvio)
        {
            panel1.Dock = DockStyle.Fill;
            var embalarLogin = new usrEmbalarEnviarPor();
            embalarLogin.enviarPor = formaDeEnvio.formaEnvio;
            embalarLogin.retornoFinaliza = formaDeEnvio;
            embalarLogin.Dock = DockStyle.Fill;
            panel1.Controls.Clear();
            panel1.Controls.Add(embalarLogin);
            btnListaDeProdutos.Enabled = false;
            btnQuantidadePacotes.Enabled = false;
            embalarLogin.ImprimeEtiquetasCd2();
        }
        public void carregaConferenciaOk()
        {
            panel1.Dock = DockStyle.Fill;
            var conferido = new usrConferido();
            conferido.Dock = DockStyle.Fill;
            panel1.Controls.Clear();
            panel1.Controls.Add(conferido);
            btnListaDeProdutos.Enabled = true;
            btnQuantidadePacotes.Enabled = false;
        }


        private void btnEmbalarPedido_Click(object sender, EventArgs e)
        {
            idPedidoEmbalagem = 0;
            carregaEmbalarLogin();
        }

        public void carregaEmbalarLogin()
        {
            carregaEmbalarLogin(1);
        }

        public void carregaEmbalarLogin(int funcao)
        {
            panel1.Dock = DockStyle.Fill;
            var embalarLogin = new usrEmbalarLogin();
            embalarLogin.funcao = funcao;
            embalarLogin.idCentroDistribuicao = Convert.ToInt32(Properties.Settings.Default["cd"]);
            embalarLogin.Dock = DockStyle.Fill;
            panel1.Controls.Clear();
            panel1.Controls.Add(embalarLogin);
            btnListaDeProdutos.Enabled = false;
            btnQuantidadePacotes.Enabled = false;
        }
        

        private void btnListaDeProdutos_Click(object sender, EventArgs e)
        {
            
            carregaEmbalarListaProdutos();
        }

        private void btnQuantidadePacotes_Click(object sender, EventArgs e)
        {
            carregaEmbalarQuantidadePacotes();
        }

        private void btnAguardandoLiberacao_Click(object sender, EventArgs e)
        {
            bool valido = false;
            var login = new frmLogin();
            login.paginaAutorizacao = "imprimiretiquetasromaneio";
            login.ShowDialog();
            if (login.DialogResult == DialogResult.OK)
            {
                var enderecamento = new frmImprimirEnderecamento();
                enderecamento.Show();
                valido = true;
            }

            if (!valido)
            {
                MessageBox.Show("Usuário incorreto");
            }
        }

        private void ribbonTab3_Click(object sender, EventArgs e)
        {

        }

        private void btnJadlogCD2_Click(object sender, EventArgs e)
        {
            //var emissao2 = new frmEmissaoJadlogCD2();
            //emissao2.Show();
            //return;

            bool valido = false;
            var login = new frmLogin();
            login.paginaAutorizacao = "emissao";
            login.ShowDialog();
            if (login.DialogResult == DialogResult.OK)
            {
                var emissao = new frmEmissaoJadlogCD2();
                emissao.Show();
                valido = true;
            }

            if (!valido)
            {
                MessageBox.Show("Usuário incorreto");
            }
        }

        private void btnEmissaoJadlog_Click(object sender, EventArgs e)
        {
            //var emissao2 = new frmEmissaoJadlogCD2();
            //emissao2.Show();
            //return;

            int idCentroDistribuicao = Convert.ToInt32(Properties.Settings.Default["cd"].ToString());
            if (idCentroDistribuicao == 2) idCentroDistribuicao = 1;
            bool valido = false;
            var login = new frmLogin();
            login.paginaAutorizacao = "emissao";
            login.ShowDialog();
            if (login.DialogResult == DialogResult.OK)
            {
                var emissao = new frmEmissaoJadlog();
                emissao.idCentroDistribuicao = idCentroDistribuicao;
                emissao.Show();
                valido = true;
            }

            if (!valido)
            {
                MessageBox.Show("Usuário incorreto");
            }
        }

        private void btnEmissaoJadlogCd3_Click(object sender, EventArgs e)
        {

            bool valido = false;
            var login = new frmLogin();
            login.paginaAutorizacao = "emissao";
            login.ShowDialog();
            if (login.DialogResult == DialogResult.OK)
            {
                var emissao = new frmEmissaoJadlog();
                emissao.idCentroDistribuicao = 3;
                emissao.Show();
                valido = true;
            }

            if (!valido)
            {
                MessageBox.Show("Usuário incorreto");
            }
        }
        private void radButtonElement23_Click(object sender, EventArgs e)
        {
            bool valido = false;
            var login = new frmLogin();
            login.paginaAutorizacao = "emissao";
            login.ShowDialog();
            if (login.DialogResult == DialogResult.OK)
            {
                var emissao = new frmCarregamentoTnt();
                emissao.idUsuario = login.idUsuario;
                emissao.usuario = login.login;
                emissao.Show();
                valido = true;
            }

            if (!valido)
            {
                MessageBox.Show("Usuário incorreto");
            }
        }

        private void btnReimprimirEtiquetas_Click(object sender, EventArgs e)
        {
            if (idPedidoEmbalagem > 0)
            {
                var embalagem = new EmbalagemWs();
                var retornoFinaliza = embalagem.ReimprimirEtiquetaPedido(idPedidoEmbalagem);

                int pacotes = Convert.ToInt32(retornoFinaliza.totalVolumes);
                for (int i = 1; i <= pacotes; i++)
                {
                    clsImprimirEtiqueta.GerarEtiquetaEnderecoTnt(
                        Utilidades.retornaIdCliente(idPedidoEmbalagem).ToString() + "-" + i + "/" +
                        pacotes,
                        retornoFinaliza.etiqueta.telResidencial,
                        retornoFinaliza.etiqueta.telComercial,
                        retornoFinaliza.etiqueta.telCelular,
                        retornoFinaliza.etiqueta.nome,
                        retornoFinaliza.etiqueta.rua,
                        retornoFinaliza.etiqueta.numero,
                        retornoFinaliza.etiqueta.complemento,
                        retornoFinaliza.etiqueta.bairro,
                        retornoFinaliza.etiqueta.cidade,
                        retornoFinaliza.etiqueta.estado,
                        retornoFinaliza.etiqueta.cep,
                        retornoFinaliza.etiqueta.referencia,
                        retornoFinaliza.cnpj,
                        Convert.ToInt32(retornoFinaliza.nfeNumero),
                        i,
                        pacotes);
                }
            }
            else
            {
                var emissao = new frmReimprimirEtiquetaPedido();
                emissao.Show();
            }
        }

        private void btnCarregamentoCorreios_Click(object sender, EventArgs e)
        {
            bool valido = false;
            var login = new frmLogin();
            login.paginaAutorizacao = "emissao";
            login.ShowDialog();
            if (login.DialogResult == DialogResult.OK)
            {
                var emissao = new frmCarregamentoCorreios();
                emissao.idUsuario = login.idUsuario;
                emissao.Show();
                valido = true;
            }

            if (!valido)
            {
                MessageBox.Show("Usuário incorreto");
            }
        }

        private void radButtonElement24_Click(object sender, EventArgs e)
        {
            bool valido = false;
            var login = new frmLogin();
            login.paginaAutorizacao = "pesar";
            login.ShowDialog();
            if (login.DialogResult == DialogResult.OK)
            {
                var emissao = new frmPesarPedido();
                emissao.idUsuario = login.idUsuario;
                emissao.Show();
                valido = true;
            }

            if (!valido)
            {
                MessageBox.Show("Usuário incorreto");
            }
        }

        private void btnGerarEtiquetaProdutoEnviado_Click(object sender, EventArgs e)
        {
            bool valido = false;
            var login = new frmLogin();
            login.paginaAutorizacao = "imprimiretiquetasromaneioavulso";
            login.ShowDialog();
            if (login.DialogResult == DialogResult.OK)
            {
                var romaneios = new frmGerarEtiquetaProdutoEnviado();
                romaneios.idUsuario = login.idUsuario;
                romaneios.Show();
                valido = true;
            }

            if (!valido)
            {
                MessageBox.Show("Usuário incorreto");
            }
        }


        private void btnCarregamentoCaminhao_Click(object sender, EventArgs e)
        {
            int idCentroDistribuicao = Convert.ToInt32(Properties.Settings.Default["cd"].ToString());
            if (idCentroDistribuicao == 2) idCentroDistribuicao = 1;
            bool valido = false;
            var login = new frmLogin();
            login.paginaAutorizacao = "carregamentocaminhao";
            login.ShowDialog();
            if (login.DialogResult == DialogResult.OK)
            {
                var carregamento = new frmCarregamentoCaminhao();
                carregamento.transportadora = login.tipoAutorizacao;
                carregamento.usuarioId = login.idUsuario;
                carregamento.idCentroDistribuicao = idCentroDistribuicao;
                carregamento.Show();
                valido = true;
            }

            if (!valido)
            {
                MessageBox.Show("Usuário incorreto");
            }
        }

        private void btnCarregamentoCaminhaoCd3_Click(object sender, EventArgs e)
        {
            bool valido = false;
            var login = new frmLogin();
            login.paginaAutorizacao = "carregamentocaminhao";
            login.ShowDialog();
            if (login.DialogResult == DialogResult.OK)
            {
                var carregamento = new frmCarregamentoCaminhao();
                carregamento.transportadora = login.tipoAutorizacao;
                carregamento.usuarioId = login.idUsuario;
                carregamento.idCentroDistribuicao = 3;
                carregamento.Show();
                valido = true;
            }

            if (!valido)
            {
                MessageBox.Show("Usuário incorreto");
            }
        }


        private void btnConfiguracoes_Click(object sender, EventArgs e)
        {
            bool valido = false;
            var login = new frmLogin();
            login.paginaAutorizacao = "configuracoesapp";
            login.ShowDialog();
            if (login.DialogResult == DialogResult.OK)
            {
                var configuracoes = new frmConfiguracoes();
                configuracoes.Show();
                valido = true;
            }

            if (!valido)
            {
                MessageBox.Show("Usuário incorreto");
            }
        }

        private void radButtonElement25_Click(object sender, EventArgs e)
        {
            carregaEmbalarLogin(2);
        }
    }
}
