﻿namespace graodegenteApp
{
    partial class frmCarregamentoCorreios
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtCodigoDeBarras = new Telerik.WinControls.UI.RadTextBox();
            this.lblCodigo = new Telerik.WinControls.UI.RadLabel();
            this.lblNota = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.txtCodigoDeBarras)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCodigo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNota)).BeginInit();
            this.SuspendLayout();
            // 
            // txtCodigoDeBarras
            // 
            this.txtCodigoDeBarras.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtCodigoDeBarras.Font = new System.Drawing.Font("Segoe UI", 16F);
            this.txtCodigoDeBarras.Location = new System.Drawing.Point(305, 51);
            this.txtCodigoDeBarras.Name = "txtCodigoDeBarras";
            this.txtCodigoDeBarras.Size = new System.Drawing.Size(364, 34);
            this.txtCodigoDeBarras.TabIndex = 16;
            this.txtCodigoDeBarras.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCodigoDeBarras_KeyDown);
            // 
            // lblCodigo
            // 
            this.lblCodigo.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblCodigo.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblCodigo.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCodigo.Location = new System.Drawing.Point(370, 12);
            this.lblCodigo.Name = "lblCodigo";
            // 
            // 
            // 
            this.lblCodigo.RootElement.ControlBounds = new System.Drawing.Rectangle(370, 12, 100, 18);
            this.lblCodigo.Size = new System.Drawing.Size(232, 33);
            this.lblCodigo.TabIndex = 22;
            this.lblCodigo.Text = "Leia o código de Barras";
            // 
            // lblNota
            // 
            this.lblNota.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblNota.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblNota.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNota.Location = new System.Drawing.Point(388, 91);
            this.lblNota.Name = "lblNota";
            // 
            // 
            // 
            this.lblNota.RootElement.ControlBounds = new System.Drawing.Rectangle(388, 91, 100, 18);
            this.lblNota.Size = new System.Drawing.Size(227, 33);
            this.lblNota.TabIndex = 24;
            this.lblNota.Text = "Imprimindo Nota 2504";
            this.lblNota.Visible = false;
            // 
            // frmCarregamentoCorreios
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Azure;
            this.ClientSize = new System.Drawing.Size(960, 492);
            this.Controls.Add(this.lblNota);
            this.Controls.Add(this.lblCodigo);
            this.Controls.Add(this.txtCodigoDeBarras);
            this.Name = "frmCarregamentoCorreios";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Emissão Correios";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmCarregamentoCorreios_Load);
            ((System.ComponentModel.ISupportInitialize)(this.txtCodigoDeBarras)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCodigo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNota)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadTextBox txtCodigoDeBarras;
        private Telerik.WinControls.UI.RadLabel lblCodigo;
        private Telerik.WinControls.UI.RadLabel lblNota;
    }
}