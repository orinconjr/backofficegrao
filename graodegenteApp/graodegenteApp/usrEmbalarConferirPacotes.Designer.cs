﻿namespace graodegenteApp
{
    partial class usrEmbalarConferirPacotes
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblNomeProduto = new Telerik.WinControls.UI.RadLabel();
            this.txtCodigoDeBarras = new Telerik.WinControls.UI.RadTextBox();
            this.btnConfirmar = new Telerik.WinControls.UI.RadButton();
            ((System.ComponentModel.ISupportInitialize)(this.lblNomeProduto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCodigoDeBarras)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnConfirmar)).BeginInit();
            this.SuspendLayout();
            // 
            // lblNomeProduto
            // 
            this.lblNomeProduto.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblNomeProduto.AutoSize = false;
            this.lblNomeProduto.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNomeProduto.Location = new System.Drawing.Point(37, 48);
            this.lblNomeProduto.Name = "lblNomeProduto";
            this.lblNomeProduto.Size = new System.Drawing.Size(622, 44);
            this.lblNomeProduto.TabIndex = 15;
            this.lblNomeProduto.Text = "Passe o código de barras para conferir os pacotes";
            this.lblNomeProduto.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.lblNomeProduto.Click += new System.EventHandler(this.lblNomeProduto_Click);
            // 
            // txtCodigoDeBarras
            // 
            this.txtCodigoDeBarras.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtCodigoDeBarras.Font = new System.Drawing.Font("Segoe UI", 16F);
            this.txtCodigoDeBarras.Location = new System.Drawing.Point(42, 101);
            this.txtCodigoDeBarras.Name = "txtCodigoDeBarras";
            this.txtCodigoDeBarras.Size = new System.Drawing.Size(613, 34);
            this.txtCodigoDeBarras.TabIndex = 14;
            this.txtCodigoDeBarras.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCodigoDeBarras_KeyDown);
            // 
            // btnConfirmar
            // 
            this.btnConfirmar.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnConfirmar.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConfirmar.Location = new System.Drawing.Point(234, 257);
            this.btnConfirmar.Name = "btnConfirmar";
            this.btnConfirmar.Size = new System.Drawing.Size(230, 39);
            this.btnConfirmar.TabIndex = 18;
            this.btnConfirmar.Text = "CONFIRMAR";
            this.btnConfirmar.Click += new System.EventHandler(this.btnConfirmar_Click);
            // 
            // usrEmbalarConferirPacotes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnConfirmar);
            this.Controls.Add(this.txtCodigoDeBarras);
            this.Controls.Add(this.lblNomeProduto);
            this.Name = "usrEmbalarConferirPacotes";
            this.Size = new System.Drawing.Size(699, 552);
            this.Load += new System.EventHandler(this.usrEmbalarConferirPacotes_Load);
            ((System.ComponentModel.ISupportInitialize)(this.lblNomeProduto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCodigoDeBarras)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnConfirmar)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private Telerik.WinControls.UI.RadLabel lblNomeProduto;
        private Telerik.WinControls.UI.RadTextBox txtCodigoDeBarras;
        private Telerik.WinControls.UI.RadButton btnConfirmar;
    }
}
