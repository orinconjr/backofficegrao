﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using graodegenteApp.wsEmbalagem;

namespace graodegenteApp
{
    public partial class usrEmbalarEnviarPor : UserControl
    {
        public string enviarPor = "";
        public retornoFinalizaPedido retornoFinaliza;
        public int idCentroDistribuicao = 1;

        public usrEmbalarEnviarPor()
        {
            InitializeComponent();
        }

        private void usrEmbalarEnviarPor_Load(object sender, EventArgs e)
        {
            lblEnviarPor.Text = "Pedido Embalado com Sucesso";
        }

        private void btnConfirmar_Click(object sender, EventArgs e)
        {
            var formPrincipal = (this.Parent.Parent as frmPrincipal);
            formPrincipal.carregaEmbalarLogin();
            
        }

        public void ImprimeEtiquetasCd1()
        {
            int pacotes = Convert.ToInt32(retornoFinaliza.totalVolumes);
            var formPrincipal = (this.Parent.Parent as frmPrincipal);
            for (int i = 1; i <= Convert.ToInt32(retornoFinaliza.totalVolumesCd1); i++)
            {
                clsImprimirEtiqueta.GerarEtiquetaEnderecoTnt(
                        Utilidades.retornaIdCliente(formPrincipal.idPedidoEmbalagem).ToString() + "-" + i + "/" +
                        pacotes,
                        retornoFinaliza.etiqueta.telResidencial,
                        retornoFinaliza.etiqueta.telComercial,
                        retornoFinaliza.etiqueta.telCelular,
                        retornoFinaliza.etiqueta.nome,
                        retornoFinaliza.etiqueta.rua,
                        retornoFinaliza.etiqueta.numero,
                        retornoFinaliza.etiqueta.complemento,
                        retornoFinaliza.etiqueta.bairro,
                        retornoFinaliza.etiqueta.cidade,
                        retornoFinaliza.etiqueta.estado,
                        retornoFinaliza.etiqueta.cep,
                        retornoFinaliza.etiqueta.referencia,
                        retornoFinaliza.cnpj,
                        Convert.ToInt32(retornoFinaliza.nfeNumero),
                        i,
                        pacotes);
            }
        }
        public void ImprimeEtiquetasCd2()
        {
            var formPrincipal = (this.Parent.Parent as frmPrincipal);
            int pacotes = Convert.ToInt32(retornoFinaliza.totalVolumes);
            int indiceAtual = 0;
            for (int i = 1 + Convert.ToInt32(retornoFinaliza.totalVolumesCd1); i <= pacotes; i++)
            {
                clsImprimirEtiqueta.GerarEtiquetaEnderecoTntCd2(
                        Utilidades.retornaIdCliente(formPrincipal.idPedidoEmbalagem).ToString() + "-" + i + "/" +
                        pacotes,
                        retornoFinaliza.etiqueta.telResidencial,
                        retornoFinaliza.etiqueta.telComercial,
                        retornoFinaliza.etiqueta.telCelular,
                        retornoFinaliza.etiqueta.nome,
                        retornoFinaliza.etiqueta.rua,
                        retornoFinaliza.etiqueta.numero,
                        retornoFinaliza.etiqueta.complemento,
                        retornoFinaliza.etiqueta.bairro,
                        retornoFinaliza.etiqueta.cidade,
                        retornoFinaliza.etiqueta.estado,
                        retornoFinaliza.etiqueta.cep,
                        retornoFinaliza.etiqueta.referencia,
                        retornoFinaliza.cnpj,
                        Convert.ToInt32(retornoFinaliza.nfeNumero),
                        i,
                        pacotes, retornoFinaliza.produtosCd2[indiceAtual], retornoFinaliza.formaEnvio);
                try
                {
                    if (formPrincipal.idCentroDistribuicaoEtiqueta == 3)
                    {
                   
                            var pedidosWs = new wsPedidosFornecedor.PedidosFornecedorWs();
                            var detalheProduto = pedidosWs.RetornaDetalhesItem(Convert.ToInt32(retornoFinaliza.produtosCd2[indiceAtual].Split('-')[0].Trim()), clsConfiguracao.ChaveWs, true);
                            clsImprimirEtiqueta.GerarEtiqueta(detalheProduto.idPedidoFornecedorItem.ToString(), detalheProduto.idPedidoFornecedor.ToString(), detalheProduto.fornecedorNome, detalheProduto.numeroItem.ToString(), detalheProduto.totalItens.ToString(), detalheProduto.produtoIdDaEmpresa, detalheProduto.complementoIdDaEmpresa, detalheProduto.produtoNome, detalheProduto.codigoEtiqueta, detalheProduto.produtoId.ToString());
                    
                    }
                    if(formPrincipal.idCentroDistribuicaoEtiqueta == 4 | formPrincipal.idCentroDistribuicaoEtiqueta == 5)
                    {
                        if(Convert.ToInt32(retornoFinaliza.totalVolumesCd1) > 0)
                        {
                            lblMoveis.Text = "Volumes no CD4: " + retornoFinaliza.totalVolumesCd1;
                        } 
                    }
                }
                catch (Exception ex)
                {

                }
                indiceAtual++;
            }

            var embalarWs = new wsEmbalagem.EmbalagemWs();
            embalarWs.FinalizaImpressaoCd2(formPrincipal.idPedidoEmbalagem);
            lblEnviarPor.Text = retornoFinaliza.formaEnvio;
        }

        public void ImprimeEtiquetas()
        {
            ImprimeEtiquetasCd1();
            return;

        }

        private void btnImprimirEtiquetas_Click(object sender, EventArgs e)
        {
            var formPrincipal = (this.Parent.Parent as frmPrincipal);
            if (formPrincipal.idCentroDistribuicaoEtiqueta == 3 | formPrincipal.idCentroDistribuicaoEtiqueta == 5)
            {
                ImprimeEtiquetasCd2();
            }
            else
            {

                ImprimeEtiquetas();
            }
        }
    }
}
