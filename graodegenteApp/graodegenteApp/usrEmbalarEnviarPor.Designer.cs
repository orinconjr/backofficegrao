﻿namespace graodegenteApp
{
    partial class usrEmbalarEnviarPor
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblEnviarPor = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.btnConfirmar = new Telerik.WinControls.UI.RadButton();
            this.btnImprimirEtiquetas = new Telerik.WinControls.UI.RadButton();
            this.lblMoveis = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.lblEnviarPor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnConfirmar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnImprimirEtiquetas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMoveis)).BeginInit();
            this.SuspendLayout();
            // 
            // lblEnviarPor
            // 
            this.lblEnviarPor.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblEnviarPor.AutoSize = false;
            this.lblEnviarPor.Font = new System.Drawing.Font("Segoe UI", 46F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEnviarPor.Location = new System.Drawing.Point(0, 101);
            this.lblEnviarPor.Name = "lblEnviarPor";
            this.lblEnviarPor.Size = new System.Drawing.Size(767, 91);
            this.lblEnviarPor.TabIndex = 16;
            this.lblEnviarPor.Text = "envio";
            this.lblEnviarPor.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            // 
            // radLabel2
            // 
            this.radLabel2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radLabel2.Font = new System.Drawing.Font("Segoe UI", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel2.Location = new System.Drawing.Point(239, 37);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(274, 31);
            this.radLabel2.TabIndex = 15;
            this.radLabel2.Text = "Seu pedido será enviado por:";
            // 
            // btnConfirmar
            // 
            this.btnConfirmar.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnConfirmar.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConfirmar.Location = new System.Drawing.Point(270, 298);
            this.btnConfirmar.Name = "btnConfirmar";
            this.btnConfirmar.Size = new System.Drawing.Size(230, 39);
            this.btnConfirmar.TabIndex = 17;
            this.btnConfirmar.Text = "CONFIRMAR";
            this.btnConfirmar.Click += new System.EventHandler(this.btnConfirmar_Click);
            // 
            // btnImprimirEtiquetas
            // 
            this.btnImprimirEtiquetas.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnImprimirEtiquetas.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnImprimirEtiquetas.Location = new System.Drawing.Point(270, 480);
            this.btnImprimirEtiquetas.Name = "btnImprimirEtiquetas";
            this.btnImprimirEtiquetas.Size = new System.Drawing.Size(230, 30);
            this.btnImprimirEtiquetas.TabIndex = 18;
            this.btnImprimirEtiquetas.Text = "Reimprimir Etiquetas";
            this.btnImprimirEtiquetas.Click += new System.EventHandler(this.btnImprimirEtiquetas_Click);
            // 
            // lblMoveis
            // 
            this.lblMoveis.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMoveis.AutoSize = false;
            this.lblMoveis.Font = new System.Drawing.Font("Segoe UI", 46F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMoveis.Location = new System.Drawing.Point(0, 198);
            this.lblMoveis.Name = "lblMoveis";
            this.lblMoveis.Size = new System.Drawing.Size(767, 91);
            this.lblMoveis.TabIndex = 17;
            this.lblMoveis.Text = "envio";
            this.lblMoveis.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            // 
            // usrEmbalarEnviarPor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Azure;
            this.Controls.Add(this.lblMoveis);
            this.Controls.Add(this.btnImprimirEtiquetas);
            this.Controls.Add(this.btnConfirmar);
            this.Controls.Add(this.lblEnviarPor);
            this.Controls.Add(this.radLabel2);
            this.Name = "usrEmbalarEnviarPor";
            this.Size = new System.Drawing.Size(770, 528);
            this.Load += new System.EventHandler(this.usrEmbalarEnviarPor_Load);
            ((System.ComponentModel.ISupportInitialize)(this.lblEnviarPor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnConfirmar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnImprimirEtiquetas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMoveis)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadLabel lblEnviarPor;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadButton btnConfirmar;
        private Telerik.WinControls.UI.RadButton btnImprimirEtiquetas;
        private Telerik.WinControls.UI.RadLabel lblMoveis;
    }
}
