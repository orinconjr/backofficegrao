﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace graodegenteApp
{
    public partial class usrEmbalarProdutos : UserControl
    {
        public usrEmbalarProdutos()
        {
            InitializeComponent();
        }

        private void usrEmbalarProdutos_Load(object sender, EventArgs e)
        {
            var formPrincipal = (this.Parent.Parent as frmPrincipal);
            
            string url = clsConfiguracao.baseUrl + "envio/listaProdutos.aspx?idPedido=" + formPrincipal.idPedidoEmbalagem + "&idCentroDistribuicao=" + Properties.Settings.Default["cd"].ToString();
            webBrowser1.Navigate(url);
        }

        private void btnImpressaoConcluida_Click(object sender, EventArgs e)
        {
            var formPrincipal = (this.Parent.Parent as frmPrincipal);
            formPrincipal.idPedidoEmbalagem = 0;
            formPrincipal.idUsuarioExpedicao = 0;
            formPrincipal.carregaEmbalarLogin();
        }
    }
}
