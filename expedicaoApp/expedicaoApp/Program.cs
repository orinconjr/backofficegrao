﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Windows.Forms;
using WmAutoUpdate;
namespace expedicaoApp
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [MTAThread]
        static void Main()
        {
            Updater updater = new Updater("http://app.graodegente.com.br/update.xml");
            updater.CheckForNewVersion();
            Application.Run(new Form1());
        }
    }
}