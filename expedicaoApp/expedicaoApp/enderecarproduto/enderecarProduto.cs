﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace expedicaoApp.enderecarproduto
{
    public partial class enderecarProduto : UserControl
    {
        private separarWs.RetornoAtribuirCarrinho carrinhoEnderecando;
        private bool enderecando = false;
        Form1 formPrincipal;
        int etiquetaProduto = 0;
        bool exibirLista = false;

        public enderecarProduto()
        {
            InitializeComponent();
        }


        public void load()
        {
            formPrincipal = this.Parent.Parent as Form1;
            txtEtiqueta.Focus();
        }

        private void txtEtiqueta_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && txtEtiqueta.Text != "")
            {
                if (enderecando == false)
                {
                    carregarCarrinho();
                }
                else
                {
                    enderecarEtiqueta();
                }
            }
        }


        private void enderecarEtiqueta()
        {
            if (etiquetaProduto == 0)
            {
                int idPedidoFornecedorItem = 0;
                try
                {
                    idPedidoFornecedorItem = Convert.ToInt32(txtEtiqueta.Text);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Produto Inválido");
                    txtEtiqueta.Text = "";
                    txtEtiqueta.Focus();
                    return;
                }
                var separar = new separarWs.SepararWs();
                var validacaoProduto = separar.VerificaProdutoEnderecamentoColetor(idPedidoFornecedorItem, carrinhoEnderecando.idTransferenciaEndereco, formPrincipal.detalhesUsuario.idUsuarioExpedicao);
                if (validacaoProduto.valido)
                {
                    etiquetaProduto = idPedidoFornecedorItem;
                    txtEtiqueta.Text = "";
                    txtEtiqueta.Focus();
                    lblMensagem.Text = "Leia o Endereço";
                }
                else
                {
                    MessageBox.Show(validacaoProduto.mensagem);
                    txtEtiqueta.Text = "";
                    txtEtiqueta.Focus();
                }
            }
            else
            {
                if (!txtEtiqueta.Text.Contains("-"))
                {
                    MessageBox.Show("Endereço inválido");
                    txtEtiqueta.Text = "";
                    txtEtiqueta.Focus();
                }
                else
                {
                    var separar = new separarWs.SepararWs();
                    var enderecamento = separar.EnderecarProdutoTransferenciaColetor(etiquetaProduto, txtEtiqueta.Text, carrinhoEnderecando.idTransferenciaEndereco, formPrincipal.detalhesUsuario.idUsuarioExpedicao);
                    if (enderecamento.valido)
                    {
                        lblMensagem.Text = "Leia o Produto";
                        lblTitulo.Text = carrinhoEnderecando.carrinhoNome + ": " + enderecamento.totalPendente + " pendentes";
                        txtEtiqueta.Text = "";
                        txtEtiqueta.Focus();
                        if (exibirLista) atualizarLista();
                        etiquetaProduto = 0;

                        if (enderecamento.totalPendente == 0)
                        {
                            MessageBox.Show("Carrinho totalmente endereçado!");
                            formPrincipal.carregarFormPrincipal();
                        }
                    }
                    else
                    {
                        MessageBox.Show(enderecamento.mensagem);
                        txtEtiqueta.Text = "";
                        txtEtiqueta.Focus();
                    }
                }
            }
        }


        private void carregarCarrinho()
        {
            var separar = new separarWs.SepararWs();
            var atribuirCarrinho = separar.AtribuiRetornaCarrinhoPendente(txtEtiqueta.Text, formPrincipal.detalhesUsuario.idUsuarioExpedicao);
            if (atribuirCarrinho.valido == false)
            {
                MessageBox.Show(atribuirCarrinho.mensagem);
                txtEtiqueta.Text = "";
                txtEtiqueta.Focus();
                return;
            }
            carrinhoEnderecando = atribuirCarrinho;
            lblTitulo.Text = atribuirCarrinho.carrinhoNome + ": " + atribuirCarrinho.totalPendente + " pendentes";
            lblMensagem.Text = "Leia o Produto";
            txtEtiqueta.Text = "";
            txtEtiqueta.Focus();
            btnExibirProdutosPendentes.Visible = true;
            enderecando = true;
        }

        private void atualizarLista()
        {
            var separar = new separarWs.SepararWs();
            var lista = separar.ListaProdutosPendentesEnderecamento(carrinhoEnderecando.idTransferenciaEndereco);
            lstProdutosEnderecamento.DataSource = lista;
        }

        private void btnExibirProdutosPendentes_Click(object sender, EventArgs e)
        {
            if (exibirLista == false)
            {
                exibirLista = true;
                lstProdutosEnderecamento.Visible = true;
                btnExibirProdutosPendentes.Text = "Ocultar Lista de Produtos";
                atualizarLista();
            }
            else
            {
                lstProdutosEnderecamento.Visible = false;
                exibirLista = false;
                btnExibirProdutosPendentes.Text = "Exibir Produtos Pendentes";
            }
        }
    }
}
