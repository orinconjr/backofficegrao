﻿namespace expedicaoApp.enderecarproduto
{
    partial class enderecarProduto
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblTitulo = new System.Windows.Forms.Label();
            this.txtEtiqueta = new System.Windows.Forms.TextBox();
            this.lblMensagem = new System.Windows.Forms.Label();
            this.lstProdutosEnderecamento = new System.Windows.Forms.ListBox();
            this.btnExibirProdutosPendentes = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblTitulo
            // 
            this.lblTitulo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTitulo.Location = new System.Drawing.Point(2, 4);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Size = new System.Drawing.Size(209, 19);
            this.lblTitulo.Text = "Endereçamento de Produtos";
            this.lblTitulo.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // txtEtiqueta
            // 
            this.txtEtiqueta.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtEtiqueta.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Regular);
            this.txtEtiqueta.Location = new System.Drawing.Point(3, 55);
            this.txtEtiqueta.Name = "txtEtiqueta";
            this.txtEtiqueta.Size = new System.Drawing.Size(209, 29);
            this.txtEtiqueta.TabIndex = 6;
            this.txtEtiqueta.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtEtiqueta_KeyUp);
            // 
            // lblMensagem
            // 
            this.lblMensagem.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMensagem.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Regular);
            this.lblMensagem.Location = new System.Drawing.Point(3, 23);
            this.lblMensagem.Name = "lblMensagem";
            this.lblMensagem.Size = new System.Drawing.Size(209, 33);
            this.lblMensagem.Text = "Leia o Carrinho";
            this.lblMensagem.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lstProdutosEnderecamento
            // 
            this.lstProdutosEnderecamento.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lstProdutosEnderecamento.Location = new System.Drawing.Point(3, 90);
            this.lstProdutosEnderecamento.Name = "lstProdutosEnderecamento";
            this.lstProdutosEnderecamento.Size = new System.Drawing.Size(208, 240);
            this.lstProdutosEnderecamento.TabIndex = 8;
            this.lstProdutosEnderecamento.Visible = false;
            // 
            // btnExibirProdutosPendentes
            // 
            this.btnExibirProdutosPendentes.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExibirProdutosPendentes.Location = new System.Drawing.Point(3, 336);
            this.btnExibirProdutosPendentes.Name = "btnExibirProdutosPendentes";
            this.btnExibirProdutosPendentes.Size = new System.Drawing.Size(208, 20);
            this.btnExibirProdutosPendentes.TabIndex = 11;
            this.btnExibirProdutosPendentes.Text = "Exibir Produtos Pendentes";
            this.btnExibirProdutosPendentes.Visible = false;
            this.btnExibirProdutosPendentes.Click += new System.EventHandler(this.btnExibirProdutosPendentes_Click);
            // 
            // enderecarProduto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.Controls.Add(this.btnExibirProdutosPendentes);
            this.Controls.Add(this.lstProdutosEnderecamento);
            this.Controls.Add(this.lblTitulo);
            this.Controls.Add(this.txtEtiqueta);
            this.Controls.Add(this.lblMensagem);
            this.Name = "enderecarProduto";
            this.Size = new System.Drawing.Size(215, 359);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblTitulo;
        private System.Windows.Forms.TextBox txtEtiqueta;
        private System.Windows.Forms.Label lblMensagem;
        private System.Windows.Forms.ListBox lstProdutosEnderecamento;
        private System.Windows.Forms.Button btnExibirProdutosPendentes;
    }
}
