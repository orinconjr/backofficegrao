﻿namespace expedicaoApp
{
    partial class usrPrincipal
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSepararPedido = new System.Windows.Forms.Button();
            this.btnCarregamentoCaminhao = new System.Windows.Forms.Button();
            this.btnEnderecarPacote = new System.Windows.Forms.Button();
            this.btnEnderecarProdutos = new System.Windows.Forms.Button();
            this.btnReenderecarProdutos = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnSepararPedido
            // 
            this.btnSepararPedido.Location = new System.Drawing.Point(0, 0);
            this.btnSepararPedido.Name = "btnSepararPedido";
            this.btnSepararPedido.Size = new System.Drawing.Size(254, 32);
            this.btnSepararPedido.TabIndex = 0;
            this.btnSepararPedido.Text = "Separar Pedido";
            this.btnSepararPedido.Click += new System.EventHandler(this.btnSepararPedido_Click);
            // 
            // btnCarregamentoCaminhao
            // 
            this.btnCarregamentoCaminhao.Location = new System.Drawing.Point(0, 159);
            this.btnCarregamentoCaminhao.Name = "btnCarregamentoCaminhao";
            this.btnCarregamentoCaminhao.Size = new System.Drawing.Size(254, 30);
            this.btnCarregamentoCaminhao.TabIndex = 1;
            this.btnCarregamentoCaminhao.Text = "Carregar Caminhão";
            this.btnCarregamentoCaminhao.Click += new System.EventHandler(this.btnCarregamentoCaminhao_Click);
            // 
            // btnEnderecarPacote
            // 
            this.btnEnderecarPacote.Location = new System.Drawing.Point(0, 120);
            this.btnEnderecarPacote.Name = "btnEnderecarPacote";
            this.btnEnderecarPacote.Size = new System.Drawing.Size(254, 31);
            this.btnEnderecarPacote.TabIndex = 2;
            this.btnEnderecarPacote.Text = "Enderecar Pacote de Pedidos";
            this.btnEnderecarPacote.Click += new System.EventHandler(this.btnEnderecarPacote_Click);
            // 
            // btnEnderecarProdutos
            // 
            this.btnEnderecarProdutos.Location = new System.Drawing.Point(0, 40);
            this.btnEnderecarProdutos.Name = "btnEnderecarProdutos";
            this.btnEnderecarProdutos.Size = new System.Drawing.Size(254, 31);
            this.btnEnderecarProdutos.TabIndex = 3;
            this.btnEnderecarProdutos.Text = "Endereçar Produtos";
            this.btnEnderecarProdutos.Click += new System.EventHandler(this.btnEnderecarProdutos_Click);
            // 
            // btnReenderecarProdutos
            // 
            this.btnReenderecarProdutos.Location = new System.Drawing.Point(0, 80);
            this.btnReenderecarProdutos.Name = "btnReenderecarProdutos";
            this.btnReenderecarProdutos.Size = new System.Drawing.Size(254, 31);
            this.btnReenderecarProdutos.TabIndex = 4;
            this.btnReenderecarProdutos.Text = "Reendereçar Produtos";
            this.btnReenderecarProdutos.Click += new System.EventHandler(this.btnReenderecarProdutos_Click);
            // 
            // usrPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.Controls.Add(this.btnReenderecarProdutos);
            this.Controls.Add(this.btnEnderecarProdutos);
            this.Controls.Add(this.btnEnderecarPacote);
            this.Controls.Add(this.btnCarregamentoCaminhao);
            this.Controls.Add(this.btnSepararPedido);
            this.Name = "usrPrincipal";
            this.Size = new System.Drawing.Size(254, 266);
            this.Click += new System.EventHandler(this.usrPrincipal_Click);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnSepararPedido;
        private System.Windows.Forms.Button btnCarregamentoCaminhao;
        private System.Windows.Forms.Button btnEnderecarPacote;
        private System.Windows.Forms.Button btnEnderecarProdutos;
        private System.Windows.Forms.Button btnReenderecarProdutos;
    }
}
