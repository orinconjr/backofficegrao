﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace expedicaoApp
{
    public partial class usrLogin : UserControl
    {
        public usrLogin()
        {
            InitializeComponent();
        }

        private void txtSenha_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtSenha_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                logar();
            }
        }

        private void logar()
        {
            var separar = new separarWs.SepararWs();
            var login = separar.LogarExpedicao(txtSenha.Text, 0);
            if (login.valido == false)
            {
                lblMensagem.Text = "Senha Incorreta";
                txtSenha.Text = "";
                txtSenha.Focus();
            }
            else
            {
                var formPrincipal = this.Parent.Parent as Form1;
                formPrincipal.setarUsuario(login);
                formPrincipal.carregarFormPrincipal();
            }

        }



        public void load()
        {
            txtSenha.Focus();
        }
    }
}
