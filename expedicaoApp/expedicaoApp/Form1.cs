﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace expedicaoApp
{
    public partial class Form1 : Form
    {
        public separarWs.UsuarioExpedicaoDetalhes detalhesUsuario;
        public Form1()
        {
            InitializeComponent();
        }


        private void Form1_Load(object sender, EventArgs e)
        {
            carregarFormLogin();
        }

        public void setarUsuario(separarWs.UsuarioExpedicaoDetalhes usuario)
        {
            detalhesUsuario = usuario;
            lblUsuario.Text = usuario.nome;
        }


        public void carregarFormLogin()
        {
            lblUsuario.Text = "";
            pnPrincipal.Controls.Clear();
            var telaPrincipal = new usrLogin();
            telaPrincipal.Dock = DockStyle.Fill;
            pnPrincipal.Controls.Add(telaPrincipal);
            telaPrincipal.load();
        }


        public void carregarFormPrincipal()
        {
            //lblUsuario.Text = "";
            pnPrincipal.Controls.Clear();
            var telaPrincipal = new usrPrincipal();
            telaPrincipal.Dock = DockStyle.Fill;
            pnPrincipal.Controls.Add(telaPrincipal);
            telaPrincipal.load();
        }



        public void carregarSepararPedido()
        {
            var separar = new separarWs.SepararWs();
            var login = separar.LogarExpedicao(detalhesUsuario.senha, 1);            
            setarUsuario(login);
            carregarSepararPedidoDetalhe();
        }

        public void carregarEnderecarPacote()
        {
            pnPrincipal.Controls.Clear();
            var enderecarpacote = new enderecarpacote.enderecarPacote();
            enderecarpacote.Dock = DockStyle.Fill;
            pnPrincipal.Controls.Add(enderecarpacote);
            enderecarpacote.load();
        }

        public void carregarEnderecarPedidos()
        {
            pnPrincipal.Controls.Clear();
            var enderecar = new enderecarproduto.enderecarProduto();
            enderecar.Dock = DockStyle.Fill;
            pnPrincipal.Controls.Add(enderecar);
            enderecar.load();
        }

        public void carregarReenderecar()
        {
            pnPrincipal.Controls.Clear();
            var reenderecar = new reenderecarproduto.reenderecarProduto();
            reenderecar.Dock = DockStyle.Fill;
            pnPrincipal.Controls.Add(reenderecar);
            reenderecar.load();
        }

        public void carregarCarregamentoCaminhao()
        {
            pnPrincipal.Controls.Clear();
            var caminhao = new usrCarregamentoCaminhaoLogin();
            caminhao.Dock = DockStyle.Fill;
            pnPrincipal.Controls.Add(caminhao);
            caminhao.load();
        }

        public void carregarSepararPedidoDetalhe()
        {
            if (detalhesUsuario.retornoServico == "separando" | detalhesUsuario.retornoServico == "separandolote")
            {
                pnPrincipal.Controls.Clear();
                var separar = new separarpedido.usrSepararPedidoLista();
                separar.Dock = DockStyle.Fill;
                pnPrincipal.Controls.Add(separar);
                separar.load();
            }
            else if (detalhesUsuario.retornoServico == "enderecar")
            {
                pnPrincipal.Controls.Clear();
                var separar = new separarpedido.usrSepararPedidoEnderecar();
                separar.Dock = DockStyle.Fill;
                pnPrincipal.Controls.Add(separar);
                separar.load();
            }
            else
            {
                pnPrincipal.Controls.Clear();
                var separar = new separarpedido.usrSepararPedidoQuantidade();
                separar.Dock = DockStyle.Fill;
                pnPrincipal.Controls.Add(separar);
                separar.load();
            }            
        }


        public void carregarCarregamentoCaminhaoDetalhe()
        {
            pnPrincipal.Controls.Clear();
            var carregamento = new carregamentoCaminhao.usrCarregamentoCaminhaoLista();
            carregamento.Dock = DockStyle.Fill;
            pnPrincipal.Controls.Add(carregamento);
            carregamento.load();
        }

        public void carregarCarregamentoCaminhaoCarregar(int idCarregamentoCaminhaoTransportadora, string transportadora)
        {
            pnPrincipal.Controls.Clear();
            var carregamento = new carregamentoCaminhao.usrCarregamentoCarregar();
            carregamento.Dock = DockStyle.Fill;
            pnPrincipal.Controls.Add(carregamento);
            carregamento.load(idCarregamentoCaminhaoTransportadora, transportadora);
        }


        public void carregarCarregamentoCaminhaoFinalizar(int idCarregamentoCaminhaoTransportadora)
        {
            pnPrincipal.Controls.Clear();
            var carregamento = new carregamentoCaminhao.usrCarregamentoCaminhaoFinalizar();
            carregamento.Dock = DockStyle.Fill;
            pnPrincipal.Controls.Add(carregamento);
            carregamento.load(idCarregamentoCaminhaoTransportadora);
        }


        private void btnSair_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void bntMenu_Click(object sender, EventArgs e)
        {
            //detalhesUsuario = new expedicaoApp.separarWs.UsuarioExpedicaoDetalhes();
            carregarFormPrincipal();
        }
    }
}