﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Media;
using System.Net;
using System.Text;
using System.IO;

namespace expedicaoApp
{
    public class Utilidades
    {
        public static void Alerta()
        {
            using (var audioStream = new MemoryStream(Properties.Resources.alarme2))
            {
                using (var player = new SoundPlayer(audioStream))
                {
                    player.Play();
                }
            }

        }
    }
}
