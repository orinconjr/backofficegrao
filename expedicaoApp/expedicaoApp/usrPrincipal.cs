﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace expedicaoApp
{
    public partial class usrPrincipal : UserControl
    {
        Form1 formPrincipal;
        public usrPrincipal()
        {
            InitializeComponent();
        }

        private void btnSepararPedido_Click(object sender, EventArgs e)
        {
            formPrincipal.carregarSepararPedido(); 
        }

        public void load()
        {
            formPrincipal = this.Parent.Parent as Form1;
            btnCarregamentoCaminhao.Visible = formPrincipal.detalhesUsuario.permissoes.Any(x => x.idExpedicaoTipoPermissao == 4);
            //btnEnderecarPacote.Visible = formPrincipal.detalhesUsuario.permissoes.Any(x => x.idExpedicaoTipoPermissao == 9);
        }
        private void webBrowser1_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
        }

       

        private void webBrowser1_DocumentCompleted_1(object sender, WebBrowserDocumentCompletedEventArgs e)
        {

        }

        private void usrPrincipal_Click(object sender, EventArgs e)
        {

        }

        private void btnCarregamentoCaminhao_Click(object sender, EventArgs e)
        {
            var formPrincipal = this.Parent.Parent as Form1;
            formPrincipal.carregarCarregamentoCaminhao(); 

        }

        private void btnEnderecarPacote_Click(object sender, EventArgs e)
        {
            var formPrincipal = this.Parent.Parent as Form1;
            formPrincipal.carregarEnderecarPacote();
        }

        private void btnEnderecarProdutos_Click(object sender, EventArgs e)
        {
            formPrincipal.carregarEnderecarPedidos();
        }

        private void btnReenderecarProdutos_Click(object sender, EventArgs e)
        {
            formPrincipal.carregarReenderecar();
        }
    }
}
