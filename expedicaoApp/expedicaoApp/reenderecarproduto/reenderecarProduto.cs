﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace expedicaoApp.reenderecarproduto
{
    public partial class reenderecarProduto : UserControl
    {
        Form1 formPrincipal;
        public reenderecarProduto()
        {
            InitializeComponent();
        }

        string etiqueta = "";

        public void load()
        {
            formPrincipal = this.Parent.Parent as Form1;
            txtEtiqueta.Focus();
        }

        private void txtEtiqueta_KeyUp(object sender, KeyEventArgs e)
        {
            var separar = new separarWs.SepararWs();
            if (e.KeyCode == Keys.Enter && txtEtiqueta.Text != "")
            {
                if (txtEtiqueta.Text.Contains("-") && etiqueta != "")
                {
                    var endereco = separar.ConferirEnderecoProdutoRegistrar(etiqueta, txtEtiqueta.Text, formPrincipal.detalhesUsuario.idUsuarioExpedicao.ToString());
                    lblMensagem.Text = endereco;
                    etiqueta = "";
                    txtEtiqueta.Text = "";
                    txtEtiqueta.Focus();
                }
                else if (txtEtiqueta.Text.Contains("-") && etiqueta == "")
                {
                    MessageBox.Show("Favor ler o produto primeiro");
                    txtEtiqueta.Text = "";
                    txtEtiqueta.Focus();
                }
                else if (etiqueta == "")
                {
                    etiqueta = txtEtiqueta.Text;
                    var endereco = separar.ConferirEnderecoProduto(etiqueta, formPrincipal.detalhesUsuario.idUsuarioExpedicao.ToString());
                    var retornoSplit = endereco.Split('|');
                    if (retornoSplit[0] == "0")
                    {
                        MessageBox.Show(retornoSplit[1]);
                        Utilidades.Alerta();
                    }
                    lblMensagem.Text = retornoSplit[1];
                    txtEtiqueta.Text = "";
                    txtEtiqueta.Focus();
                }
            }
        }
    }
}
