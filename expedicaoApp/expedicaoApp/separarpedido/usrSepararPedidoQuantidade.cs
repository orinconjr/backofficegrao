﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace expedicaoApp.separarpedido
{
    public partial class usrSepararPedidoQuantidade : UserControl
    {
        Form1 formPrincipal;

        public usrSepararPedidoQuantidade()
        {
            InitializeComponent();
        }

        private void usrSepararPedidoQuantidade_Click(object sender, EventArgs e)
        {

        }

        public void load()
        {
            formPrincipal = this.Parent.Parent as Form1;
            try
            {
                var separar = new separarWs.SepararWs();
                var contagem = separar.ContaEmbalarCds(formPrincipal.detalhesUsuario.idUsuarioExpedicao.ToString(), formPrincipal.detalhesUsuario.senha.ToString());
                lblSepararQuantidade.Text = contagem;
            }
            catch (Exception)
            {
                MessageBox.Show("Falha ao Acessar, tente novamente");
                formPrincipal.carregarSepararPedido();
            }
        }

        private void btnSepararPedido_Click(object sender, EventArgs e)
        {
            btnSepararPedido.Visible = false;
            var separar = new separarWs.SepararWs();
            var login = separar.LogarExpedicao(formPrincipal.detalhesUsuario.senha, 1);
            if (login.valido == false)
            {
                formPrincipal.carregarSepararPedido();
            }
            else
            {
                if (login.retornoServico == "separando" | login.retornoServico == "separandolote")
                {
                    formPrincipal.setarUsuario(login);
                    formPrincipal.carregarSepararPedidoDetalhe();
                }
                else
                {
                    if (formPrincipal.detalhesUsuario.permissoes.Any(x => x.idExpedicaoTipoPermissao == 7))
                    {
                        var retorno = separar.SepararPedidoCd5Lote(formPrincipal.detalhesUsuario.idUsuarioExpedicao.ToString(), formPrincipal.detalhesUsuario.senha, 1);
                        if (!retorno.ToList().Any(x => x > 0))
                        {
                            MessageBox.Show("Não existem pedidos para serem embalados");
                        }
                        else
                        {
                            formPrincipal.detalhesUsuario.retornoServico = "separandolote";
                            formPrincipal.detalhesUsuario.retornoServicoFiltro = Convert.ToInt32(retorno.First());
                            formPrincipal.carregarSepararPedidoDetalhe();
                        }
                    }
                    else
                    {
                        var retorno = separar.SepararPedido(formPrincipal.detalhesUsuario.idUsuarioExpedicao.ToString(), formPrincipal.detalhesUsuario.senha);
                        if (retorno == "0")
                        {
                            MessageBox.Show("Não existem pedidos para serem embalados");
                        }
                        else
                        {
                            formPrincipal.detalhesUsuario.retornoServico = "separando";
                            formPrincipal.detalhesUsuario.retornoServicoFiltro = Convert.ToInt32(retorno);
                            formPrincipal.carregarSepararPedidoDetalhe();
                        }
                    }
                }
            }

            btnSepararPedido.Visible = true;
        }
    }
}
