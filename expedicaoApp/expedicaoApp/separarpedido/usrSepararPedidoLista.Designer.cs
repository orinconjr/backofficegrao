﻿namespace expedicaoApp.separarpedido
{
    partial class usrSepararPedidoLista
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblPedido = new System.Windows.Forms.Label();
            this.listProdutos = new System.Windows.Forms.WebBrowser();
            this.txtEtiqueta = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // lblPedido
            // 
            this.lblPedido.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblPedido.Location = new System.Drawing.Point(0, 0);
            this.lblPedido.Name = "lblPedido";
            this.lblPedido.Size = new System.Drawing.Size(237, 20);
            this.lblPedido.Text = "-";
            this.lblPedido.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // listProdutos
            // 
            this.listProdutos.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.listProdutos.Location = new System.Drawing.Point(0, 72);
            this.listProdutos.Name = "listProdutos";
            this.listProdutos.Size = new System.Drawing.Size(240, 202);
            this.listProdutos.Navigated += new System.Windows.Forms.WebBrowserNavigatedEventHandler(this.listProdutos_Navigated);
            // 
            // txtEtiqueta
            // 
            this.txtEtiqueta.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtEtiqueta.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Regular);
            this.txtEtiqueta.Location = new System.Drawing.Point(3, 33);
            this.txtEtiqueta.Name = "txtEtiqueta";
            this.txtEtiqueta.Size = new System.Drawing.Size(234, 29);
            this.txtEtiqueta.TabIndex = 2;
            this.txtEtiqueta.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtEtiqueta_KeyUp);
            // 
            // usrSepararPedidoLista
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.Controls.Add(this.txtEtiqueta);
            this.Controls.Add(this.listProdutos);
            this.Controls.Add(this.lblPedido);
            this.Name = "usrSepararPedidoLista";
            this.Size = new System.Drawing.Size(240, 274);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblPedido;
        private System.Windows.Forms.WebBrowser listProdutos;
        private System.Windows.Forms.TextBox txtEtiqueta;
    }
}
