﻿namespace expedicaoApp.separarpedido
{
    partial class usrSepararPedidoQuantidade
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.lblSepararQuantidade = new System.Windows.Forms.Label();
            this.btnSepararPedido = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.Font = new System.Drawing.Font("Tahoma", 16F, System.Drawing.FontStyle.Regular);
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(224, 34);
            this.label1.Text = "Pedidos para Separar:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblSepararQuantidade
            // 
            this.lblSepararQuantidade.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSepararQuantidade.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Regular);
            this.lblSepararQuantidade.Location = new System.Drawing.Point(0, 34);
            this.lblSepararQuantidade.Name = "lblSepararQuantidade";
            this.lblSepararQuantidade.Size = new System.Drawing.Size(224, 22);
            this.lblSepararQuantidade.Text = "-";
            this.lblSepararQuantidade.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // btnSepararPedido
            // 
            this.btnSepararPedido.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.btnSepararPedido.Location = new System.Drawing.Point(3, 81);
            this.btnSepararPedido.Name = "btnSepararPedido";
            this.btnSepararPedido.Size = new System.Drawing.Size(218, 52);
            this.btnSepararPedido.TabIndex = 2;
            this.btnSepararPedido.Text = "Separar um Pedido";
            this.btnSepararPedido.Click += new System.EventHandler(this.btnSepararPedido_Click);
            // 
            // usrSepararPedidoQuantidade
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.Controls.Add(this.btnSepararPedido);
            this.Controls.Add(this.lblSepararQuantidade);
            this.Controls.Add(this.label1);
            this.Name = "usrSepararPedidoQuantidade";
            this.Size = new System.Drawing.Size(224, 213);
            this.Click += new System.EventHandler(this.usrSepararPedidoQuantidade_Click);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblSepararQuantidade;
        private System.Windows.Forms.Button btnSepararPedido;
    }
}
