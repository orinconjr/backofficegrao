﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace expedicaoApp.separarpedido
{
    public partial class usrSepararPedidoEnderecar : UserControl
    {
        Form1 formPrincipal;
        public usrSepararPedidoEnderecar()
        {
            InitializeComponent();
        }

        private void txtEtiqueta_KeyUp_1(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                var separar = new separarWs.SepararWs();
                var enderecar = separar.EnderecarPedidoCds(txtEtiqueta.Text, formPrincipal.detalhesUsuario.retornoServicoFiltro.ToString(), formPrincipal.detalhesUsuario.idUsuarioExpedicao.ToString());
                if (enderecar == "0")
                {
                    formPrincipal = this.Parent.Parent as Form1;
                    MessageBox.Show("Pedido endereçado");
                    formPrincipal.detalhesUsuario.retornoServico = "";
                    formPrincipal.carregarSepararPedidoDetalhe();
                }
                //Produto totalmente separado
                if (enderecar == "1")
                {
                    MessageBox.Show("Falha ao endereçar pedido");
                    txtEtiqueta.Text = "";
                    txtEtiqueta.Focus();
                }
            }
        }

        public void load()
        {
            formPrincipal = this.Parent.Parent as Form1;
            lblPedido.Text = "Pedido: " + " (" + formPrincipal.detalhesUsuario.retornoServicoFiltro.ToString() + ")";
            txtEtiqueta.Focus();
        }
    }
}
