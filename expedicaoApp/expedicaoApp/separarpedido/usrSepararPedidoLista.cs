﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace expedicaoApp.separarpedido
{
    public partial class usrSepararPedidoLista : UserControl
    {
        Form1 formPrincipal;
        public usrSepararPedidoLista()
        {
            InitializeComponent();
        }

        public void load()
        {
            formPrincipal = this.Parent.Parent as Form1;
            atualizarLista();
            txtEtiqueta.Focus();
        }

        private void atualizarLista()
        {
            atualizarLista(0);
        }
        private void atualizarLista(int id)
        {
            try
            {
                var separar = new separarWs.SepararWs();
                var contagem = formPrincipal.detalhesUsuario.retornoServico == "separandolote" ? separar.ListaProdutosSeparacaoLote(formPrincipal.detalhesUsuario.idUsuarioExpedicao.ToString(), 0) : separar.ListaProdutosSeparacao(formPrincipal.detalhesUsuario.retornoServicoFiltro.ToString(), formPrincipal.detalhesUsuario.idUsuarioExpedicao.ToString(), 0);
                if (contagem.funcao == 0)
                {
                    MessageBox.Show("Falha ao Carregar Pedido");
                    formPrincipal.detalhesUsuario.retornoServico = "";
                    formPrincipal.carregarSepararPedidoDetalhe();
                }
                else if (contagem.funcao == 1)
                {
                    if (formPrincipal.detalhesUsuario.idCentroDistribuicao == 5)
                    {
                        MessageBox.Show("Pedido totalmente separado");
                        formPrincipal.detalhesUsuario.retornoServico = "";
                        formPrincipal.carregarSepararPedidoDetalhe();
                    }
                    else
                    {
                        MessageBox.Show("Pedido separado totalmente. Endereçar a separação.");
                        formPrincipal.detalhesUsuario.retornoServico = "enderecar";
                        formPrincipal.carregarSepararPedidoDetalhe();
                    }
                }
                else
                {
                    var produtosNaoSeparados = contagem.produtosSeparar.Where(x => x.separados < x.quantidade);
                    var produtosSeparados = contagem.produtosSeparar.Where(x => x.separados == x.quantidade);
                    var primeiroProduto = contagem.produtosSeparar.FirstOrDefault();
                    if (primeiroProduto != null)
                    {
                        lblPedido.Text = "Pedido: " + primeiroProduto.pedidoId + " (" + formPrincipal.detalhesUsuario.retornoServicoFiltro.ToString() + ")";
                    }


                    StringBuilder sb = new StringBuilder();
                    sb.Append("<html><body style='font-size: 9px; width: 100%'>");
                    foreach (var produto in produtosNaoSeparados)
                    {
                        string produtoLista = "";
                        int quantidadeLista = 1;
                        foreach (var endereco in produto.enderecos)
                        {
                            var enderecoDetalhe = endereco.Split('|');
                            if(quantidadeLista == 1) produtoLista = "<span style='font-size: 14px; font-weight:bold'>" + "<a href=\"http://admin.graodegente.com.br/envio/etiquetas.html?id=" + produto.produtoId + "\">" + enderecoDetalhe[0] + "</a></span> - ";
                            produtoLista += "<b>R: </b>" + enderecoDetalhe[1];
                            produtoLista += " - <b>L: </b>" + enderecoDetalhe[2];
                            produtoLista += " - <b>P: </b>" + enderecoDetalhe[3];
                            produtoLista += " - <b>A: </b>" + enderecoDetalhe[4] + "<br>";
                            //produtoLista += "</a>";
                            quantidadeLista++;

                        }
                        produtoLista += "<br>" + produto.produtoNome + "<br>";
                        produtoLista += "<b>Qtd.: </b>" + produto.quantidade;
                        produtoLista += " - <b>Separados: </b>" + produto.separados + "<br>";
                        if (!string.IsNullOrEmpty(produto.etiqueta)) produtoLista += "<b>Etiqueta: </b>" + produto.etiqueta + "<br>";
                        produtoLista += "----------------------------------------" + "<br>";
                        sb.Append(produtoLista);
                    }
                    foreach (var produto in produtosSeparados)
                    {
                        string produtoLista = "<div style='background: #D2E9FF'><b>SEPARADO" + "<br>";
                        produtoLista += "<b>Etiqueta: </b>" + produto.etiqueta + "<br>";
                        produtoLista += "<b>Produto: </b>" + produto.produtoNome + "<br>";
                        produtoLista += "----------------------------------------" + "</div>";
                        sb.Append(produtoLista);
                    }
                    sb.Append("</body></html>");
                    listProdutos.DocumentText = sb.ToString();
                    txtEtiqueta.Focus();
                }
            }
            catch (Exception ex)
            {
                listProdutos.DocumentText = "Falha ao Acessar, tente novamente " + ex.InnerException + ex.StackTrace + ex.Message;
                //formPrincipal.carregarSepararPedido(idUsuario, senha);
                txtEtiqueta.Focus();
            }
            txtEtiqueta.Text = "";
            txtEtiqueta.Focus();
        }

        string produto = "";
        private void txtEtiqueta_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && txtEtiqueta.Text != "")
            {
                var separar = new separarWs.SepararWs();
                produto = txtEtiqueta.Text;
                var retorno = "";
                if(formPrincipal.detalhesUsuario.retornoServico == "separandolote")
                {
                    retorno = separar.VerificaProdutoLote(produto, formPrincipal.detalhesUsuario.idUsuarioExpedicao.ToString());
                }
                else{
                retorno = separar.VerificaProdutoV2cds(produto, formPrincipal.detalhesUsuario.retornoServicoFiltro.ToString(), formPrincipal.detalhesUsuario.idUsuarioExpedicao.ToString());
                }
                
                if (retorno == "0")
                {
                    produto = "";
                    MessageBox.Show("PRODUTO NÃO FAZ PARTE DO PEDIDO");
                }
                //Produto totalmente separado
                if (retorno == "1")
                {
                    produto = "";
                    MessageBox.Show("ESTE PRODUTO JÁ ESTÁ SEPARADO");
                    atualizarLista();
                }
                //Produto válido, adicionar ao pedido
                if (retorno.Contains("2_"))
                {
                    if (retorno == "2_0")
                    {
                        MessageBox.Show("ERRO AO ADICIONAR PRODUTO");
                        produto = "";
                    }
                    if (retorno == "2_1")
                    {
                        produto = "";
                        atualizarLista();
                    }
                    if (retorno == "2_2")
                    {
                        if (formPrincipal.detalhesUsuario.idCentroDistribuicao == 5)
                        {
                            MessageBox.Show("Pedido totalmente separado");
                            formPrincipal.detalhesUsuario.retornoServico = "";
                            formPrincipal.carregarSepararPedidoDetalhe();
                        }
                        else
                        {
                            MessageBox.Show("Pedido separado totalmente. Endereçar a separação.");
                            formPrincipal.detalhesUsuario.retornoServico = "enderecar";
                            formPrincipal.carregarSepararPedidoDetalhe();
                        }
                    }
                }
                // Produto separado, mas falta quantidade
                if (retorno == "3")
                {
                    produto = "";
                    atualizarLista();
                }
                // Etiqueta faz parte de outro pedido, apenas separado
                if (retorno == "4")
                {
                    produto = "";
                    MessageBox.Show("Este produto está separado para outro pedido.");
                    atualizarLista();
                }
                // Etiqueta faz parte de outro pedido, enviado
                if (retorno == "5")
                {
                    produto = "";
                    MessageBox.Show("Este consta como ENVIADO para outro pedido.");
                    atualizarLista();
                }
                // Etiqueta não encontrada
                if (retorno == "6")
                {
                    produto = "";
                    MessageBox.Show("Este produto não faz parte do pedido.");
                    atualizarLista();
                }
                if (retorno == "7")
                {
                    produto = "";
                    MessageBox.Show("Produto incorreto. Favor separar a etiqueta específica deste pedido.");
                    atualizarLista();
                }
            }
            else if (e.KeyCode == Keys.Enter && txtEtiqueta.Text == "")
            {
                atualizarLista();
            }
        }

        private void listProdutos_Navigated(object sender, WebBrowserNavigatedEventArgs e)
        {
            string url = listProdutos.Url.ToString();
            if (url.Contains("http://admin.graodegente.com.br/envio/etiquetas.html?id="))
            {
                string id = url.Replace("http://admin.graodegente.com.br/envio/etiquetas.html?id=", "");
                atualizarLista(Convert.ToInt32(id));
            }
        }
    }
}
