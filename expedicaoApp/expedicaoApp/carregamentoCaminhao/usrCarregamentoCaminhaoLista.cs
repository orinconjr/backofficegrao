﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace expedicaoApp.carregamentoCaminhao
{
    public partial class usrCarregamentoCaminhaoLista : UserControl
    {
        Form1 formPrincipal;
        List<carregamentoWs.CarregamentosEmAberto> carregamentosEmAberto = new List<expedicaoApp.carregamentoWs.CarregamentosEmAberto>();
        public usrCarregamentoCaminhaoLista()
        {
            InitializeComponent();
        }

        public void load()
        {
            formPrincipal = this.Parent.Parent as Form1;
            atualizarLista();
        }

        private void atualizarLista()
        {
            var carregamento = new carregamentoWs.CarregamentoWs();
            carregamentosEmAberto = carregamento.CarregamentosTransportadoraEmAberto(formPrincipal.detalhesUsuario.idUsuarioExpedicao).ToList();
            lstCarregamentos.Items.Clear();
            foreach (var carregamentoEmAberto in carregamentosEmAberto)
            {
                lstCarregamentos.Items.Add(carregamentoEmAberto.placaCaminhao + " - " + carregamentoEmAberto.transportadora);
            }
        }

        private void btnCarregar_Click(object sender, EventArgs e)
        {
            var item = lstCarregamentos.SelectedItem;
            if (item != null)
            {
                int idTransportadora = Convert.ToInt32(lstCarregamentos.SelectedIndex);
                var itemCarregamento = carregamentosEmAberto[idTransportadora];
                if (itemCarregamento.dataAbertura == null)
                {
                    var carregamentoAbertoCaminhao = carregamentosEmAberto.FirstOrDefault(x => x.idCarregamentoCaminhao == itemCarregamento.idCarregamentoCaminhao && x.dataAbertura != null);
                    if (carregamentoAbertoCaminhao != null)
                    {
                        MessageBox.Show("Para abrir o carregamento da " + itemCarregamento.transportadora + ", primeiro finalize o carregamento da " + carregamentoAbertoCaminhao.transportadora);
                        return;
                    }
                    else
                    {
                        abreCaminhao(itemCarregamento.idCarregamentoCaminhaoTransportadora);
                    }
                }
                else
                {
                    abreCaminhao(itemCarregamento.idCarregamentoCaminhaoTransportadora);
                }
            }
        }

        private void abreCaminhao(int idCarregamentoCaminhaoTransportadora)
        {
            var carregamentoAbertoCaminhao = carregamentosEmAberto.FirstOrDefault(x => x.idCarregamentoCaminhaoTransportadora == idCarregamentoCaminhaoTransportadora);
            if (carregamentoAbertoCaminhao.dataAbertura == null)
            {
                var carregamento = new carregamentoWs.CarregamentoWs();
                var abertura = carregamento.AbreCarregamentoCaminhaoTransportadora(idCarregamentoCaminhaoTransportadora, formPrincipal.detalhesUsuario.idUsuarioExpedicao);
                if (!abertura)
                {
                    MessageBox.Show("Falha ao carregar, tente novamente");
                    atualizarLista();
                    return;
                }
            }
            formPrincipal.carregarCarregamentoCaminhaoCarregar(idCarregamentoCaminhaoTransportadora, carregamentoAbertoCaminhao.transportadora);
        }

    }
}
