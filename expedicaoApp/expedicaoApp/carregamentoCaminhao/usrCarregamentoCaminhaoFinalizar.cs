﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace expedicaoApp.carregamentoCaminhao
{
    public partial class usrCarregamentoCaminhaoFinalizar : UserControl
    {
        Form1 formPrincipal;
        int idCarregamentoCaminhaoTransportadora = 0;
        bool finalizacaoParcial = false;
        int idUsuarioFinalizacao = 0;
        int idUsuarioFinalizacaoParcial = 0;

        public usrCarregamentoCaminhaoFinalizar()
        {
            InitializeComponent();
        }

        public void load(int carregamentoCaminhaoTransportadora)
        {
            idCarregamentoCaminhaoTransportadora = carregamentoCaminhaoTransportadora;
            formPrincipal = this.Parent.Parent as Form1;
            txtSenha.Focus();
            var carregamento = new carregamentoWs.CarregamentoWs();
            var pacotes = carregamento.CarregamentoListaPacotes(carregamentoCaminhaoTransportadora);
            if (pacotes.carregamentosParciais.Any()) finalizacaoParcial = true;
        }
        private void txtEtiqueta_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                var separar = new separarWs.SepararWs();
                var login = separar.LogarExpedicao(txtSenha.Text, 2);
                if (login.valido == false)
                {
                    lblMensagem.Text = "Senha Incorreta";
                    txtSenha.Text = "";
                    txtSenha.Focus();
                }
                else
                {
                    if (idUsuarioFinalizacao == 0)
                    {
                        idUsuarioFinalizacao = login.idUsuarioExpedicao;
                        idUsuarioFinalizacaoParcial = login.idUsuarioExpedicao;

                        if (finalizacaoParcial)
                        {
                            idUsuarioFinalizacaoParcial = 0;
                            lblMensagem.Text = "Existem volumes parcialmente carregados. Para finalizar a emissao, leia a senha de um usuario com permissao para finalizar parcialmente";
                            txtSenha.Text = "";
                            txtSenha.Focus();
                        }
                        else
                        {
                            finalizar();
                        }
                    }
                    else
                    {
                        if (login.permissoes.Any(x => x.idExpedicaoTipoPermissao == 8))
                        {
                            idUsuarioFinalizacaoParcial = login.idUsuarioExpedicao;
                            finalizar();
                        }
                        else
                        {
                            MessageBox.Show("Usuario sem permissao para finalizar carregamento parcialmente");
                            txtSenha.Text = "";
                            txtSenha.Focus();
                        }
                    }
                }
            }
        }

        private void finalizar()
        {
            var carregamento = new carregamentoWs.CarregamentoWs();
            var finalizacao = carregamento.FinalizarCarregamento(idCarregamentoCaminhaoTransportadora, idUsuarioFinalizacao, idUsuarioFinalizacaoParcial);
            if (finalizacao.carregado)
            {
                MessageBox.Show("Carregamento Finalizado");
                formPrincipal.carregarCarregamentoCaminhaoDetalhe();
            }
        }
    }
}
