﻿namespace expedicaoApp.carregamentoCaminhao
{
    partial class usrCarregamentoCarregar
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Label label1;
            this.txtEtiqueta = new System.Windows.Forms.TextBox();
            this.lstPedidosParciais = new System.Windows.Forms.ListBox();
            this.lblTotais = new System.Windows.Forms.Label();
            this.btnFinalizarCarregamento = new System.Windows.Forms.Button();
            label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            label1.Location = new System.Drawing.Point(11, 62);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(292, 20);
            label1.Text = "Pedidos Carregados Parcialmente:";
            label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // txtEtiqueta
            // 
            this.txtEtiqueta.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtEtiqueta.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Regular);
            this.txtEtiqueta.Location = new System.Drawing.Point(38, 3);
            this.txtEtiqueta.Name = "txtEtiqueta";
            this.txtEtiqueta.Size = new System.Drawing.Size(234, 29);
            this.txtEtiqueta.TabIndex = 4;
            this.txtEtiqueta.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtEtiqueta_KeyUp);
            // 
            // lstPedidosParciais
            // 
            this.lstPedidosParciais.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lstPedidosParciais.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Regular);
            this.lstPedidosParciais.Location = new System.Drawing.Point(11, 90);
            this.lstPedidosParciais.Name = "lstPedidosParciais";
            this.lstPedidosParciais.Size = new System.Drawing.Size(292, 232);
            this.lstPedidosParciais.TabIndex = 6;
            // 
            // lblTotais
            // 
            this.lblTotais.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTotais.Location = new System.Drawing.Point(11, 38);
            this.lblTotais.Name = "lblTotais";
            this.lblTotais.Size = new System.Drawing.Size(292, 20);
            this.lblTotais.Text = "-";
            this.lblTotais.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // btnFinalizarCarregamento
            // 
            this.btnFinalizarCarregamento.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFinalizarCarregamento.Location = new System.Drawing.Point(11, 329);
            this.btnFinalizarCarregamento.Name = "btnFinalizarCarregamento";
            this.btnFinalizarCarregamento.Size = new System.Drawing.Size(292, 25);
            this.btnFinalizarCarregamento.TabIndex = 7;
            this.btnFinalizarCarregamento.Text = "Finalizar Carregamento";
            this.btnFinalizarCarregamento.Click += new System.EventHandler(this.btnFinalizarCarregamento_Click);
            // 
            // usrCarregamentoCarregar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.Controls.Add(this.btnFinalizarCarregamento);
            this.Controls.Add(this.lblTotais);
            this.Controls.Add(label1);
            this.Controls.Add(this.lstPedidosParciais);
            this.Controls.Add(this.txtEtiqueta);
            this.Name = "usrCarregamentoCarregar";
            this.Size = new System.Drawing.Size(313, 363);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox txtEtiqueta;
        private System.Windows.Forms.ListBox lstPedidosParciais;
        private System.Windows.Forms.Label lblTotais;
        private System.Windows.Forms.Button btnFinalizarCarregamento;
    }
}
