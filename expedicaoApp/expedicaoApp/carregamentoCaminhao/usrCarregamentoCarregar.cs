﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace expedicaoApp.carregamentoCaminhao
{
    public partial class usrCarregamentoCarregar : UserControl
    {
        Form1 formPrincipal;
        int carregamentoCaminhaoTransportadora = 0;
        string nomeTransportadora = "";
        public usrCarregamentoCarregar()
        {
            InitializeComponent();
        }

        private void txtEtiqueta_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                string etiqueta = txtEtiqueta.Text;
                if (etiqueta.Length != 31)
                {
                    MessageBox.Show("Etiqueta inválida");
                    txtEtiqueta.Text = "";
                    txtEtiqueta.Focus();
                    Utilidades.Alerta();
                    return;
                }
                var carregamento = new carregamentoWs.CarregamentoWs();
                var carregamentoPacote = carregamento.CarregarPacote(carregamentoCaminhaoTransportadora, formPrincipal.detalhesUsuario.idUsuarioExpedicao, txtEtiqueta.Text);
                if (carregamentoPacote.carregado == false)
                {
                    MessageBox.Show(carregamentoPacote.mensagem);
                    txtEtiqueta.Text = "";
                    txtEtiqueta.Focus();
                    Utilidades.Alerta();
                }
                else
                {
                    txtEtiqueta.Text = "";
                    txtEtiqueta.Focus();
                    atualizarLista();
                }
            }
        }

        public void load(int idCarregamentoCaminhaoTransportadora, string transportadora)
        {
            formPrincipal = this.Parent.Parent as Form1;
            carregamentoCaminhaoTransportadora = idCarregamentoCaminhaoTransportadora;
            nomeTransportadora = transportadora;
            atualizarLista();
            txtEtiqueta.Focus();
            btnFinalizarCarregamento.Text = "Finalizar Carregamento " + transportadora;
        }

        private void atualizarLista()
        {
            var carregamento = new carregamentoWs.CarregamentoWs();
            var pacotes = carregamento.CarregamentoListaPacotes(carregamentoCaminhaoTransportadora);
            lstPedidosParciais.Items.Clear();
            foreach (var pacote in pacotes.carregamentosParciais)
            {
                lstPedidosParciais.Items.Add(pacote.idPedido + " (" + pacote.totalCarregado + "/" + pacote.totalPacotes + ")");
            }
            lblTotais.Text = "Pedidos: " + pacotes.totalPedidosCarregadosTotal + " | Pacotes: " + pacotes.totalPacotesCarregadosTotal;
        }

        private void lblTotais_ParentChanged(object sender, EventArgs e)
        {

        }

        private void btnFinalizarCarregamento_Click(object sender, EventArgs e)
        {
            formPrincipal.carregarCarregamentoCaminhaoFinalizar(carregamentoCaminhaoTransportadora);
        }

    }
}
