﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace expedicaoApp.enderecarpacote
{
    public partial class enderecarPacote : UserControl
    {
        Form1 formPrincipal;
        public enderecarPacote()
        {
            InitializeComponent();
        }

        string etiqueta = "";

        public void load()
        {
            formPrincipal = this.Parent.Parent as Form1;
            txtEtiqueta.Focus();
        }

        private void txtEtiqueta_KeyUp(object sender, KeyEventArgs e)
        {
            var embalagem = new embalagemWs.EmbalagemWs();
            if (e.KeyCode == Keys.Enter && txtEtiqueta.Text != "")
            {
                if (txtEtiqueta.Text.Contains("-") && etiqueta != "")
                {
                    var endereco = embalagem.ConferirEnderecoProdutoRegistrar(txtEtiqueta.Text, etiqueta, formPrincipal.detalhesUsuario.idUsuarioExpedicao.ToString());
                    var retornoSplit = endereco.Split('|');
                    if (retornoSplit[0] == "0")
                    {
                        MessageBox.Show(retornoSplit[1]);
                        Utilidades.Alerta();
                    }
                    lblMensagem.Text = "Leia o Pacote";
                    etiqueta = "";
                    txtEtiqueta.Text = "";
                    txtEtiqueta.Focus();
                }
                else if (txtEtiqueta.Text.Contains("-") && etiqueta == "")
                {
                    MessageBox.Show("Favor ler o pacote primeiro");
                    txtEtiqueta.Text = "";
                    txtEtiqueta.Focus();
                }
                else if (etiqueta == "")
                {
                    etiqueta = txtEtiqueta.Text;
                    var endereco = embalagem.ConferirEnderecoPacote(etiqueta, formPrincipal.detalhesUsuario.idUsuarioExpedicao.ToString());
                    var retornoSplit = endereco.Split('|');
                    if (retornoSplit[0] == "0")
                    {
                        MessageBox.Show(retornoSplit[1]);
                        Utilidades.Alerta();
                    }
                    lblMensagem.Text = "Leia o Endereço";
                    txtEtiqueta.Text = "";
                    txtEtiqueta.Focus();
                }
            }
        }
    }
}
