﻿namespace GraoDeGente.Infrastructure.Utility.Serializer
{
    internal static class JsonSerializerExtension
    {
        private static readonly JsonSerializer _serializer = new JsonSerializer();

        public static string Serialize(this object value)
        {
            return _serializer.SerializeObject(value);
        }

        public static T Deserialize<T>(this string value)
        {
            return _serializer.DeserializeObject<T>(value);
        }
    }
}
