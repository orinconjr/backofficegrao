﻿using System.Collections.Specialized;
using System.Linq;

namespace GraoDeGente.Infrastructure.Utility.Http
{
    public static class HttpUtilityHelper
    {
        public static string ToQueryString(this NameValueCollection queryParams)
        {
            return string.Join("&", queryParams.AllKeys.Where(key => !string.IsNullOrWhiteSpace(queryParams[key]))
            .Select(key => string.Join("&", queryParams.GetValues(key).Select(val => string.Format("{0}={1}", key, val)))));
        }
    }
}
