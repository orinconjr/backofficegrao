﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraoDeGente.Infrastructure.Utility.Http
{
    /// <summary>
    /// Formato das mensagens de requisição e resposta para o gateway. XML ou JSON.
    /// </summary>
    internal enum HttpContentTypeEnum
    {

        Xml,
        Json
    }
}
