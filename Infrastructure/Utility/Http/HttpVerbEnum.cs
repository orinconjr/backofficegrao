﻿namespace GraoDeGente.Infrastructure.Utility.Http
{
    internal enum HttpVerbEnum
    {
        Get,
        Post,
        Put,
        Delete,
        Patch
    }
}
