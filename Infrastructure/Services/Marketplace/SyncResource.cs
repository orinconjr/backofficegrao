﻿using GraoDeGente.Infrastructure.Services.Marketplace.Interfaces;
using GraoDeGente.Infrastructure.Services.Marketplace.Models;
using GraoDeGente.Infrastructure.Services.Marketplace.Models.Products;
using GraoDeGente.Infrastructure.Services.Marketplace.Models.SellerItems;
using GraoDeGente.Infrastructure.Services.Marketplace.Orders;
using GraoDeGente.Infrastructure.Services.Marketplace.Products;
using GraoDeGente.Infrastructure.Services.Marketplace.SellerItems;
using GraoDeGente.Infrastructure.Utility.Http;
using System;
using System.Collections.Specialized;
using System.Net;

namespace GraoDeGente.Infrastructure.Services.Marketplace
{
    public class SyncResource : BaseResource, ISyncResource
    {
        public SyncResource() 
            : this(null, null) { }

        public SyncResource(Uri hostUri, NameValueCollection customHeaders) 
            : base("/sync", hostUri, customHeaders) { }

        public DefaultResponse PostProducts(CreateProductsRequest createProductRequest)
        {
            var headers = this.GetHeaders();

            var uri = string.Concat(this.HostUri, this.ResourceName, "/", createProductRequest.Marketplace, "/products");
            var result = this.HttpUtility.SubmitRequest(createProductRequest, uri, HttpVerbEnum.Post, HttpContentTypeEnum.Json, headers);
            var response = new DefaultResponse
            {
                Success = result.HttpStatusCode.Equals(HttpStatusCode.OK),
                Exception = !result.HttpStatusCode.Equals(HttpStatusCode.OK) ? new Exception(result.RawResponse) : null
            };

            return response;
        }

        public DefaultResponse PostProductsStatus(SyncProductsStatusRequest syncProductsStatusRequest)
        {
            var headers = this.GetHeaders();

            var uri = string.Concat(this.HostUri, this.ResourceName, "/", syncProductsStatusRequest.Marketplace, "/products/status");
            var result = this.HttpUtility.SubmitRequest(syncProductsStatusRequest, uri, HttpVerbEnum.Post, HttpContentTypeEnum.Json, headers);
            var response = new DefaultResponse
            {
                Success = result.HttpStatusCode.Equals(HttpStatusCode.OK),
                Exception = !result.HttpStatusCode.Equals(HttpStatusCode.OK) ? new Exception(result.RawResponse) : null
            };

            return response;
        }

        public DefaultResponse PostSellerItemsPrices(UpdateSellerItemsPricesRequest updateProductsPricesRequest)
        {
            var headers = this.GetHeaders();

            var uri = string.Concat(this.HostUri, this.ResourceName, "/", updateProductsPricesRequest.Marketplace, "/sellerItems/prices");
            var result = this.HttpUtility.SubmitRequest(updateProductsPricesRequest, uri, HttpVerbEnum.Post, HttpContentTypeEnum.Json, headers);
            var response = new DefaultResponse
            {
                Success = result.HttpStatusCode.Equals(HttpStatusCode.OK),
                Exception = !result.HttpStatusCode.Equals(HttpStatusCode.OK) ? new Exception(result.RawResponse) : null
            };

            return response;
        }

        public DefaultResponse PostSellerItemsStock(SyncSellerItemsStockRequest syncSellerItemsStockRequest)
        {
            var headers = this.GetHeaders();

            var uri = string.Concat(this.HostUri, this.ResourceName, "/", syncSellerItemsStockRequest.Marketplace, "/sellerItems/stock");
            var result = this.HttpUtility.SubmitRequest(syncSellerItemsStockRequest, uri, HttpVerbEnum.Post, HttpContentTypeEnum.Json, headers);
            var response = new DefaultResponse
            {
                Success = result.HttpStatusCode.Equals(HttpStatusCode.OK),
                Exception = !result.HttpStatusCode.Equals(HttpStatusCode.OK) ? new Exception(result.RawResponse) : null
            };

            return response;
        }

        public DefaultResponse PostOrdersStatusNew(SyncOrdersRequest syncOrdersRequest)
        {
            var headers = this.GetHeaders();

            var uri = string.Concat(this.HostUri, this.ResourceName, "/", syncOrdersRequest.Marketplace, "/orders/status/new");
            var result = this.HttpUtility.SubmitRequest(syncOrdersRequest, uri, HttpVerbEnum.Post, HttpContentTypeEnum.Json, headers);
            var response = new DefaultResponse
            {
                Success = result.HttpStatusCode.Equals(HttpStatusCode.OK),
                Exception = !result.HttpStatusCode.Equals(HttpStatusCode.OK) ? new Exception(result.RawResponse) : null
            };

            return response;
        }

        public DefaultResponse PostOrdersStatusApproved(SyncOrdersRequest syncOrdersRequest)
        {
            var headers = this.GetHeaders();

            var uri = string.Concat(this.HostUri, this.ResourceName, "/", syncOrdersRequest.Marketplace, "/orders/status/approved");
            var result = this.HttpUtility.SubmitRequest(syncOrdersRequest, uri, HttpVerbEnum.Post, HttpContentTypeEnum.Json, headers);
            var response = new DefaultResponse
            {
                Success = result.HttpStatusCode.Equals(HttpStatusCode.OK),
                Exception = !result.HttpStatusCode.Equals(HttpStatusCode.OK) ? new Exception(result.RawResponse) : null
            };

            return response;
        }

        public DefaultResponse PostOrdersStatusCanceled(SyncOrdersRequest syncOrdersRequest)
        {
            var headers = this.GetHeaders();

            var uri = string.Concat(this.HostUri, this.ResourceName, "/", syncOrdersRequest.Marketplace, "/orders/status/canceled");
            var result = this.HttpUtility.SubmitRequest(syncOrdersRequest, uri, HttpVerbEnum.Post, HttpContentTypeEnum.Json, headers);
            var response = new DefaultResponse
            {
                Success = result.HttpStatusCode.Equals(HttpStatusCode.OK),
                Exception = !result.HttpStatusCode.Equals(HttpStatusCode.OK) ? new Exception(result.RawResponse) : null
            };

            return response;
        }
    }
}
