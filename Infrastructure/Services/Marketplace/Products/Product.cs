﻿using System.Runtime.Serialization;

namespace GraoDeGente.Infrastructure.Services.Marketplace.Models.Products
{
    [DataContract(Namespace = "")]
    public class Product
    {
 
        [DataMember]
        public int Id { get; set; }
    }
}
