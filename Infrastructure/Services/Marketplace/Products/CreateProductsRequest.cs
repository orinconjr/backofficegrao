﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace GraoDeGente.Infrastructure.Services.Marketplace.Models.Products
{
    [DataContract(Namespace = "")]
    public class CreateProductsRequest : BaseRequest
    {
        [DataMember]
        public List<Product> Products { get; set; }
    }
}
