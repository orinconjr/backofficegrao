﻿using GraoDeGente.Infrastructure.Services.Marketplace.Models;
using System.Runtime.Serialization;

namespace GraoDeGente.Infrastructure.Services.Marketplace.Products
{
    [DataContract(Namespace = "")]
    public class SyncProductsStatusRequest : BaseRequest
    {
    }
}
