﻿using System;
using System.Runtime.Serialization;

namespace GraoDeGente.Infrastructure.Services.Marketplace.Models
{
    [DataContract(Namespace = "")]
    public abstract class BaseResponse
    {
        /// <summary>
        /// Exceção lançada pelo método executado
        /// </summary>
        public Exception Exception { get; set; }
    }
}
