﻿namespace GraoDeGente.Infrastructure.Services.Marketplace.Models
{
    public enum IntegrationStatus : byte
    {
        Pending,
        Integrated,
        Error
    }
}
