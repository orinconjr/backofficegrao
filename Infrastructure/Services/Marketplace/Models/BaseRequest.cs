﻿using System;
using System.Runtime.Serialization;

namespace GraoDeGente.Infrastructure.Services.Marketplace.Models
{
    [DataContract]
    public abstract class BaseRequest
    {
        /// <summary>
        /// Marketplace
        /// </summary>
        [DataMember(Name = "Marketplace")]
        private string MarketplaceField
        {
            get
            {
                return this.Marketplace.ToString();
            }
            set
            {
                this.Marketplace = (MarketplaceEnum)Enum.Parse(typeof(MarketplaceEnum), value);
            }
        }

        public MarketplaceEnum Marketplace { get; set; }
    }

}
