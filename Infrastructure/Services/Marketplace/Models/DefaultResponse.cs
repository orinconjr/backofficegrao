﻿using System;
using System.Runtime.Serialization;

namespace GraoDeGente.Infrastructure.Services.Marketplace.Models
{
    [DataContract(Namespace = "")]
    public class DefaultResponse : BaseResponse
    {
        /// <summary>
        /// Sucesso.
        /// </summary>
        [DataMember]
        public bool Success { get; set; }
    }
}
