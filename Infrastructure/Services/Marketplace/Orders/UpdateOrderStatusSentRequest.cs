﻿using GraoDeGente.Infrastructure.Services.Marketplace.Models;
using System.Runtime.Serialization;

namespace GraoDeGente.Infrastructure.Services.Marketplace.Orders
{
    [DataContract(Namespace = "")]
    public class UpdateOrderStatusSentRequest : BaseRequest
    {
        [DataMember]
        public int OrderId { get; set; }
    }
}
