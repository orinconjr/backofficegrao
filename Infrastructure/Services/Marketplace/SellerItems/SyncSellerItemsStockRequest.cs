﻿using GraoDeGente.Infrastructure.Services.Marketplace.Models;
using System.Runtime.Serialization;

namespace GraoDeGente.Infrastructure.Services.Marketplace.SellerItems
{
    [DataContract(Namespace = "")]
    public class SyncSellerItemsStockRequest : BaseRequest
    {
    }
}
