﻿using GraoDeGente.Infrastructure.Services.Marketplace.Models;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace GraoDeGente.Infrastructure.Services.Marketplace.SellerItems
{
    public class GetSellerItemStatusResponse : BaseResponse
    {
        [DataMember]
        public List<SiteStatus> Status { get; set; }

        [DataMember]
        public bool Success { get; set; }
    }

}
