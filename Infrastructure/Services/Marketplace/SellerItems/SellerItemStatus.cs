﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace GraoDeGente.Infrastructure.Services.Marketplace.SellerItems
{
    [DataContract(Namespace = "")]
    public class SellerItemStatus
    {
        [DataMember]
        public int ProductId { get; set; }
        [DataMember]
        public List<SiteStatus> Status { get; set; }
    }
}