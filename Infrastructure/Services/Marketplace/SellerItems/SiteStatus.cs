﻿using System.Runtime.Serialization;

namespace GraoDeGente.Infrastructure.Services.Marketplace.SellerItems
{
    [DataContract(Namespace = "")]
    public class SiteStatus
    {
        [DataMember]
        public bool Active { get; set; }
        [DataMember]
        public string SiteCode { get; set; }
    }
}