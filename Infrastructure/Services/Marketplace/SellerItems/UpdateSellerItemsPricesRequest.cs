﻿using GraoDeGente.Infrastructure.Services.Marketplace.Models.Products;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace GraoDeGente.Infrastructure.Services.Marketplace.Models.SellerItems
{
    [DataContract(Namespace = "")]
    public class UpdateSellerItemsPricesRequest : BaseRequest
    {
        [DataMember]
        public List<Product> Products { get; set; }
    }
}
