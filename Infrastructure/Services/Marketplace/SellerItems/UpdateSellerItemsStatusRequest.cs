﻿using GraoDeGente.Infrastructure.Services.Marketplace.Models;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace GraoDeGente.Infrastructure.Services.Marketplace.SellerItems
{
    [DataContract(Namespace = "")]
    public class UpdateSellerItemsStatusRequest : BaseRequest
    {
        [DataMember]
        public List<SellerItemStatus> Status { get; set; }
    }
}
