﻿using GraoDeGente.Infrastructure.Services.Marketplace.Interfaces;
using GraoDeGente.Infrastructure.Services.Marketplace.Models;
using GraoDeGente.Infrastructure.Services.Marketplace.SellerItems;
using GraoDeGente.Infrastructure.Utility.Http;
using System;
using System.Collections.Specialized;
using System.Net;

namespace GraoDeGente.Infrastructure.Services.Marketplace
{
    public class SellerItemsResource : BaseResource, ISellerItemsResource
    {
        public SellerItemsResource() 
            : this(null, null) { }

        public SellerItemsResource(Uri hostUri, NameValueCollection customHeaders) 
            : base("/sellerItems", hostUri, customHeaders) { }

        public GetSellerItemStatusResponse GetStatus(GetSellerItemStatusRequest getSellerItemStatusRequest)
        {
            var headers = this.GetHeaders();

            var uri = string.Concat(this.HostUri, this.ResourceName, "/", getSellerItemStatusRequest.Marketplace, "/", getSellerItemStatusRequest.ProductId, "/status");
            var result = this.HttpUtility.SubmitRequest<GetSellerItemStatusResponse>(uri, HttpVerbEnum.Get, HttpContentTypeEnum.Json, headers);

            return result.Response;
        }

        public DefaultResponse PostStatus(UpdateSellerItemsStatusRequest updateSellerItemsStatusRequest)
        {
            var headers = this.GetHeaders();

            var uri = string.Concat(this.HostUri, this.ResourceName, "/", updateSellerItemsStatusRequest.Marketplace, "/status");
            var result = this.HttpUtility.SubmitRequest(updateSellerItemsStatusRequest, uri, HttpVerbEnum.Post, HttpContentTypeEnum.Json, headers);

            var response = new DefaultResponse
            {
                Success = result.HttpStatusCode.Equals(HttpStatusCode.OK),
                Exception = !result.HttpStatusCode.Equals(HttpStatusCode.OK) ? new Exception(result.RawResponse) : null
            };

            return response;
        }
    }
}
