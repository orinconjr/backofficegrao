﻿using GraoDeGente.Infrastructure.Services.Marketplace.Models;
using GraoDeGente.Infrastructure.Services.Marketplace.Models.Products;
using GraoDeGente.Infrastructure.Services.Marketplace.Models.SellerItems;
using GraoDeGente.Infrastructure.Services.Marketplace.Orders;
using GraoDeGente.Infrastructure.Services.Marketplace.Products;
using GraoDeGente.Infrastructure.Services.Marketplace.SellerItems;

namespace GraoDeGente.Infrastructure.Services.Marketplace.Interfaces
{
    public interface ISyncResource
    {
        DefaultResponse PostProducts(CreateProductsRequest createProductsRequest);
        DefaultResponse PostSellerItemsPrices(UpdateSellerItemsPricesRequest updateProductsPricesRequest);
        DefaultResponse PostSellerItemsStock(SyncSellerItemsStockRequest syncSellerItemsStockRequest);
        DefaultResponse PostProductsStatus(SyncProductsStatusRequest syncProductsStatusRequest);
        DefaultResponse PostOrdersStatusNew(SyncOrdersRequest syncOrdersRequest);
        DefaultResponse PostOrdersStatusApproved(SyncOrdersRequest syncOrdersRequest);
        DefaultResponse PostOrdersStatusCanceled(SyncOrdersRequest syncOrdersRequest);
    }
}
