﻿using GraoDeGente.Infrastructure.Services.Marketplace.Models;
using GraoDeGente.Infrastructure.Services.Marketplace.Orders;

namespace GraoDeGente.Infrastructure.Services.Marketplace.Interfaces
{
    public interface IOrdersResource
    {
        DefaultResponse PostStatusSent(UpdateOrderStatusSentRequest updateOrderStatusSentRequest);
    }
}
