﻿using GraoDeGente.Infrastructure.Services.Marketplace.Models;
using GraoDeGente.Infrastructure.Services.Marketplace.SellerItems;

namespace GraoDeGente.Infrastructure.Services.Marketplace.Interfaces
{
    public interface ISellerItemsResource
    {
        GetSellerItemStatusResponse GetStatus(GetSellerItemStatusRequest getSellerItemStatusRequest);
        DefaultResponse PostStatus(UpdateSellerItemsStatusRequest updateSellerItemsStatusRequest);
    }
}
