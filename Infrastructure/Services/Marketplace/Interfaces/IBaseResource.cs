﻿namespace GraoDeGente.Infrastructure.Services.Marketplace.Interfaces
{
    public interface IBaseResource
    {
        string ResourceName { get; }
    }
}
