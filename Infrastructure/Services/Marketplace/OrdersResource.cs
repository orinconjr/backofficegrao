﻿using GraoDeGente.Infrastructure.Services.Marketplace.Interfaces;
using GraoDeGente.Infrastructure.Services.Marketplace.Models;
using GraoDeGente.Infrastructure.Services.Marketplace.Orders;
using GraoDeGente.Infrastructure.Utility.Http;
using System;
using System.Collections.Specialized;
using System.Net;

namespace GraoDeGente.Infrastructure.Services.Marketplace
{
    public class OrdersResource : BaseResource, IOrdersResource
    {
        public OrdersResource() 
            : this(null, null) { }

        public OrdersResource(Uri hostUri, NameValueCollection customHeaders) 
            : base("/orders", hostUri, customHeaders) { }

        public DefaultResponse PostStatusSent(UpdateOrderStatusSentRequest updateOrderStatusSentRequest)
        {
            var headers = this.GetHeaders();

            var uri = string.Concat(this.HostUri, this.ResourceName, "/", updateOrderStatusSentRequest.OrderId, "/status/sent");
            var result = this.HttpUtility.SubmitRequest(updateOrderStatusSentRequest, uri, HttpVerbEnum.Post, HttpContentTypeEnum.Json, headers);
            
            var response = new DefaultResponse
            {
                Success = result.HttpStatusCode.Equals(HttpStatusCode.OK),
                Exception = !result.HttpStatusCode.Equals(HttpStatusCode.OK) ? new Exception(result.RawResponse) : null
            };

            return response;
        }
    }
}
