﻿using System;
using System.Collections.Specialized;
using GraoDeGente.Infrastructure.Services.Marketplace.Interfaces;
using GraoDeGente.Infrastructure.Utility.Http;

namespace GraoDeGente.Infrastructure.Services.Marketplace
{
    public abstract class BaseResource : IBaseResource
    {

        private string _resourceName;
        public string ResourceName { get { return _resourceName; } }

        public string ClientId { get; set; }
        public string AccessToken { get; set; }

        private string _hostUri;
        protected string HostUri { get { return _hostUri; } }

        private NameValueCollection _customHeader = null;

        internal HttpUtility HttpUtility { get; set; }

        protected BaseResource(string resourceName, Uri hostUri, NameValueCollection customHeaders)
        {
            this.HttpUtility = new HttpUtility();
            System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

            if (hostUri != null)
            {
                this._hostUri = hostUri.ToString();
                this._hostUri = this._hostUri.Remove(this._hostUri.Length - 1);
            }
            else
            {
                this._hostUri = this.GetServiceUri();
            }
            this._resourceName = resourceName;

            this._customHeader = customHeaders;
        }

        private string GetServiceUri()
        {
            return ConfigurationUtility.GetConfigurationString("Marketplace.HostUri");
        }

        protected NameValueCollection GetHeaders()
        {

            NameValueCollection headers = new NameValueCollection();

            if (this._customHeader != null)
            {
                foreach (string headerName in this._customHeader)
                {
                    headers.Add(headerName, this._customHeader[headerName]);
                }
            }
            
            return headers;
        }
    }
}
