﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BarkCommerce.Core.Interfaces.Boleto
{
    public interface IBoletoRepository
    {
        void RegistrarBoleto(int idPedidoPagamento);
        void RegistrarBoleto(int idPedidoPagamento, string pCep);
        void NotificarRegistroBoleto(int idPedidoPagamento);
        void ConsultarBoleto(int idPedidoPagamento);
    }
}
