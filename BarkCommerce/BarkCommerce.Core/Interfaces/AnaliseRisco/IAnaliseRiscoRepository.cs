﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BarkCommerce.Model.AnaliseRisco;
using BarkCommerce.Model.Pagamento;

namespace BarkCommerce.Core.Interfaces.AnaliseRisco
{
    public interface IAnaliseRiscoRepository
    {
        void EnviarAnaliseRiscoAsync(PagamentoPedidoDados dadosPagamento);
        void EnviarAnaliseRisco(PagamentoPedidoDados dadosPagamento);
        List<ResultadoAnalise> ObterResultadosAnaliseRisco();
        List<ResultadoAnalise> ObterResultadoAnaliseRisco(int pedidoId);
        bool BaixarAnaliseProcessada(int pedidoId);
        bool AtualizaAnaliseProcessada(int pedidoId, string score, string status);
        void EnviarReAnaliseRisco(PagamentoPedidoDados dadosPagamento);
    }
}
