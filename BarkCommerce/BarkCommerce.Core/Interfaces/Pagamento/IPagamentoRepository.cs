﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BarkCommerce.Model.Gateway;
using BarkCommerce.Model.Pagamento;

namespace BarkCommerce.Core.Interfaces.Pagamento
{
    public interface IPagamentoRepository
    {
        List<PagamentoPedido> ListaPagamentosPedido(int pedidoId);
        bool ProcessaPagamento(int idPedidoPagamento);
        PagamentoPedidoGateway RetornaPagamentoPedidoGateway(int idPedidoPagamento);
        PagamentoPedidoGateway RetornaPagamentoPedidoGateway(int idPedidoPagamento, bool verificarGateway);
        GatewayContrato RetornaContratoGateway(int idGatewayContrato);
        PagamentoPedidoDados RetornaDetalhesPagamento(int idPedidoPagamento);
    }
}
