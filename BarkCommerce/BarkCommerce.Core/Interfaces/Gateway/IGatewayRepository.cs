﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BarkCommerce.Model.Gateway;
using BarkCommerce.Model.Pagamento;

namespace BarkCommerce.Core.Interfaces.Gateway
{
    public interface IGatewayRepository
    {
        PagamentoPedidoGateway SolicitarReservaCartaoCredito(PagamentoConfiguracao configuracao, PagamentoPedidoDados dadosPagamento);

        PagamentoPedidoGateway RealizarVendaDiretaCartaoCredito(PagamentoConfiguracao configuracao, PagamentoPedidoDados dadosPagamento);

        PagamentoPedidoGateway CapturarPagamentoCartaoCredito(PagamentoPedidoGateway pagamentoPedido,
            PagamentoPedidoDados dadosPagamento, GatewayContrato contrato);

        PagamentoPedidoGateway EstornarReservaCartaoCredito(PagamentoPedidoGateway pagamentoPedido,
            PagamentoPedidoDados dadosPagamento, GatewayContrato contrato);
    }
}
