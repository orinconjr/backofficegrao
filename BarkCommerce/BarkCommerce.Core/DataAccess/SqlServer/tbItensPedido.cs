//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BarkCommerce.Core.DataAccess.SqlServer
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbItensPedido
    {
        public int itemPedidoId { get; set; }
        public int pedidoId { get; set; }
        public int produtoId { get; set; }
        public string produtoIdDaEmpresa { get; set; }
        public decimal itemQuantidade { get; set; }
        public decimal itemValor { get; set; }
        public string itemPresente { get; set; }
        public Nullable<int> garantiaId { get; set; }
        public Nullable<int> compreJuntoId { get; set; }
        public string ItemMensagemDoCartao { get; set; }
        public Nullable<decimal> valorDoEmbrulhoECartao { get; set; }
        public System.DateTime dataDaCriacao { get; set; }
        public string produtoCodDeBarras { get; set; }
        public Nullable<decimal> valorCusto { get; set; }
        public Nullable<System.DateTime> prazoDeFabricacao { get; set; }
        public Nullable<bool> entreguePeloFornecedor { get; set; }
        public Nullable<bool> cancelado { get; set; }
        public Nullable<bool> brinde { get; set; }
        public string motivoCancelamento { get; set; }
        public string motivoAdicao { get; set; }
        public Nullable<bool> enviado { get; set; }
        public Nullable<int> idPedidoEnvio { get; set; }
        public Nullable<int> statusTransferencia { get; set; }
        public Nullable<int> IDDaCampanha { get; set; }
        public Nullable<decimal> ValorDoDescontoDaCampanha { get; set; }
        public Nullable<int> idItemPedidoTipoAdicao { get; set; }
        public Nullable<int> idUsuarioAdicao { get; set; }
    
        public virtual tbProdutos tbProdutos { get; set; }
        public virtual tbPedidos tbPedidos { get; set; }
    }
}
