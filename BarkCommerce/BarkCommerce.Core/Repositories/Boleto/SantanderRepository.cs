﻿using BarkCommerce.Core.DataAccess.SqlServer;
using BarkCommerce.Core.Interfaces.Boleto;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace BarkCommerce.Core.Repositories.Boleto
{

    public class cepCapital
    {
        public string Uf { get; set; }
        public string Cep { get; set; }
    }

    public static class StringExt
    {
        public static string Truncate(this string value, int maxLength)
        {
            if (string.IsNullOrEmpty(value)) return value;
            return value.Length <= maxLength ? value : value.Substring(0, maxLength);
        }
    }

    public class SantanderRepository : IBoletoRepository
    {
        public void NotificarRegistroBoleto(int idPedidoPagamento)
        {
            var data = new dbCommerceEntities();
            var agora = DateTime.Now;
            var queue = new tbQueue();
            queue.agendamento = agora;
            queue.tipoQueue = 45;
            queue.idRelacionado = idPedidoPagamento;
            queue.mensagem = "";
            queue.concluido = false;
            queue.andamento = false;
            data.tbQueue.Add(queue);
            data.SaveChanges();
        }
        
        public void RegistrarBoleto(int idPEdidoPagamento)
        {
            RegistrarBoleto(idPEdidoPagamento, "");
        }

        public void RegistrarBoleto(int idPedidoPagamento, string pCep = null)
        {
            try
            {
                var data = new dbCommerceEntities();
                var cobranca = (from c in data.tbPedidoPagamento where c.idPedidoPagamento == idPedidoPagamento select c).First();
                if ((cobranca.registrado ?? false) == false)
                {
                    var conta = (from d in data.tbContaPagamento where d.idContaPagamento == cobranca.idContaPagamento select d).FirstOrDefault();
                    var cliente = (from clie in data.tbPedidos
                                   join d in data.tbClientes on clie.clienteId equals d.clienteId
                                   where clie.pedidoId == cobranca.pedidoId
                                   select d).FirstOrDefault();

                    string xmlTransmissao = "<impl:create><TicketRequest>";
                    xmlTransmissao += "<dados>";
                    //xmlTransmissao += retornaEntrada("TP-AMB", "T");
                    xmlTransmissao += retornaEntrada("CONVENIO.COD-BANCO", "0033");
                    xmlTransmissao += retornaEntrada("CONVENIO.COD-CONVENIO", conta.codCedente);
                    var clienteCpfCnpj = cliente.clienteCPFCNPJ.Replace(".", "").Replace("-", "").Replace("/", "").Replace(" ", "");
                    if (clienteCpfCnpj.Length == 14 | clienteCpfCnpj.Length == 15)
                    {
                        xmlTransmissao += retornaEntrada("PAGADOR.TP-DOC", "02");
                    }
                    else
                    {
                        xmlTransmissao += retornaEntrada("PAGADOR.TP-DOC", "01");
                    }
                        xmlTransmissao += retornaEntrada("PAGADOR.NUM-DOC", (String.Format("{0:000000000000000}", clienteCpfCnpj)));
                    
                    xmlTransmissao += retornaEntrada("PAGADOR.NOME", removeCaracteresEspeciaisDeixaEspaco(removeAcentos(cliente.clienteNome)).Truncate(40));
                    xmlTransmissao += retornaEntrada("PAGADOR.ENDER", (removeCaracteresEspeciaisDeixaEspaco(removeAcentos(cliente.clienteRua)) + ", " + removeCaracteresEspeciaisDeixaEspaco(removeAcentos(cliente.clienteNumero))).Truncate(40));
                    xmlTransmissao += retornaEntrada("PAGADOR.BAIRRO", removeCaracteresEspeciaisDeixaEspaco(removeAcentos(cliente.clienteBairro)).Truncate(30));
                    xmlTransmissao += retornaEntrada("PAGADOR.CIDADE", removeCaracteresEspeciaisDeixaEspaco(removeAcentos(cliente.clienteCidade)).Truncate(20));
                    xmlTransmissao += retornaEntrada("PAGADOR.UF", removeCaracteresEspeciaisDeixaEspaco(removeAcentos(cliente.clienteEstado)).Truncate(2));

                    if (!string.IsNullOrEmpty(pCep))
                        xmlTransmissao += retornaEntrada("PAGADOR.CEP", pCep.Replace("-", "").Replace(" ", "").Replace(".", ""));
                    else
                        xmlTransmissao += retornaEntrada("PAGADOR.CEP", cliente.clienteCep.Replace("-", "").Replace(" ", "").Replace(".", ""));

                    xmlTransmissao += retornaEntrada("TITULO.NOSSO-NUMERO", (String.Format("{0:000000000}", Convert.ToInt64(cobranca.idPedidoPagamento))));
                    xmlTransmissao += retornaEntrada("TITULO.SEU-NUMERO", cobranca.idPedidoPagamento.ToString());
                    var vencimento = RetornaProximoDiaUtil(cobranca.dataVencimento);
                    xmlTransmissao += retornaEntrada("TITULO.DT-VENCTO", vencimento.ToString("ddMMyyyy"));
                    xmlTransmissao += retornaEntrada("TITULO.DT-EMISSAO", (cobranca.tbPedidos.dataHoraDoPedido ?? vencimento).ToString("ddMMyyyy"));
                    xmlTransmissao += retornaEntrada("TITULO.ESPECIE", "17");
                    xmlTransmissao += retornaEntrada("TITULO.VL-NOMINAL", (String.Format("{0:0000000000000}", (Decimal.Round(Convert.ToDecimal(cobranca.valor), 2) * 100))));
                    xmlTransmissao += retornaEntrada("TITULO.PC-MULTA", "");
                    xmlTransmissao += retornaEntrada("TITULO.QT-DIAS-MULTA", "");
                    xmlTransmissao += retornaEntrada("TITULO.PC-JURO", "");
                    xmlTransmissao += retornaEntrada("TITULO.TP-DESC", "0");
                    xmlTransmissao += retornaEntrada("TITULO.VL-DESC", "0");
                    xmlTransmissao += retornaEntrada("TITULO.DT-LIMI-DESC", vencimento.ToString("ddMMyyyy"));
                    xmlTransmissao += retornaEntrada("TITULO.VL-ABATIMENTO", "0");
                    xmlTransmissao += retornaEntrada("TITULO.TP-PROTESTO", "0");
                    xmlTransmissao += retornaEntrada("TITULO.QT-DIAS-PROTESTO", "0");
                    xmlTransmissao += retornaEntrada("TITULO.QT-DIAS-BAIXA", "5");

                    xmlTransmissao += retornaEntrada("MENSAGEM", "Nao receber apos vencimento");

                    xmlTransmissao += "</dados>";
                    xmlTransmissao += "<expiracao>100</expiracao>";
                    xmlTransmissao += "<sistema>YMB</sistema>";
                    xmlTransmissao += "</TicketRequest></impl:create>";


                    var retornoTicket = postXMLData("https://ymbdlb.santander.com.br/dl-ticket-services/TicketEndpointService", xmlTransmissao);


                    XmlDocument docTicket = new XmlDocument();
                    docTicket.LoadXml(retornoTicket);
                    var returnCode = docTicket.ChildNodes[1].ChildNodes[0].ChildNodes[0].ChildNodes[0].ChildNodes[0].InnerText;
                    var ticketCode = docTicket.ChildNodes[1].ChildNodes[0].ChildNodes[0].ChildNodes[0].ChildNodes[1].InnerText;

                    var xmlRegistro = "<impl:registraTitulo>";
                    xmlRegistro += "<dto>";
                    xmlRegistro += "<dtNsu>" + (cobranca.tbPedidos.dataHoraDoPedido ?? vencimento).ToString("ddMMyyyy") + "</dtNsu>";
                    xmlRegistro += "<estacao>1ZRF</estacao>";
                    xmlRegistro += "<nsu>" + cobranca.idPedidoPagamento.ToString() + "</nsu>";
                    xmlRegistro += "<ticket>" + ticketCode + "</ticket>";
                    xmlRegistro += "<tpAmbiente>P</tpAmbiente>";
                    xmlRegistro += "</dto>";
                    xmlRegistro += "</impl:registraTitulo>";


                    var retornoRegistro = postXMLData("https://ymbcash.santander.com.br/ymbsrv/CobrancaEndpointService", xmlRegistro);
                    XmlDocument docRegistro = new XmlDocument();
                    docRegistro.LoadXml(retornoRegistro);

                    cobranca.gateway = true;
                    cobranca.idLoteCobranca = 1;
                    var registroBoleto = new tbPedidoPagamentoRegistroBoleto();
                    registroBoleto.dataNsu = (cobranca.tbPedidos.dataHoraDoPedido ?? vencimento).ToString("ddMMyyyy");
                    registroBoleto.dataSolicitacaoTicket = DateTime.Now;
                    registroBoleto.idPedidoPagamento = idPedidoPagamento;
                    registroBoleto.nsu = cobranca.idPedidoPagamento.ToString();
                    registroBoleto.ticket = ticketCode;
                    registroBoleto.ticketReturnCode = returnCode;
                    registroBoleto.tipoSolicitacao = 1; // registrar
                    registroBoleto.xmlRetornoTicket = retornoTicket;
                    registroBoleto.xmlSolicitacaoTicket = xmlTransmissao;
                    registroBoleto.xmlRequisicao = xmlRegistro;
                    registroBoleto.xmlRetorno = retornoRegistro;
                    data.tbPedidoPagamentoRegistroBoleto.Add(registroBoleto);
                    data.SaveChanges();

                    string retorno = VerificaRetornoErro(registroBoleto.xmlRetorno);
                    if (!string.IsNullOrEmpty(retorno))
                    {

                        registroBoleto.RetornoErroDescricao = retorno;

                        data.SaveChanges();
                        if (retorno.ToLower().Contains("00000 - título registrado") | retorno.ToLower().Contains("00000 - titulo registrado"))
                        {
                            cobranca.registrado = true;
                            data.SaveChanges();
                        }
                        else if (retorno.ToLower().Contains("cep do sacado nao encontrado") | retorno.ToLower().Contains("unidade da federacao incorreta"))
                        {
                            #region Faixa de cep capitais
                            List<cepCapital> ceps = new List<cepCapital>();
                            ceps.Add(new cepCapital() { Cep = "69900-970", Uf = "AC" });
                            ceps.Add(new cepCapital() { Cep = "57010-000", Uf = "AL" });
                            ceps.Add(new cepCapital() { Cep = "68900-010", Uf = "AP" });
                            ceps.Add(new cepCapital() { Cep = "69001-009", Uf = "AM" });
                            ceps.Add(new cepCapital() { Cep = "40020-000", Uf = "BA" });
                            ceps.Add(new cepCapital() { Cep = "60010-000", Uf = "CE" });
                            ceps.Add(new cepCapital() { Cep = "70002-900", Uf = "DF" });
                            ceps.Add(new cepCapital() { Cep = "29002-900", Uf = "ES" });
                            ceps.Add(new cepCapital() { Cep = "72800-010", Uf = "GO" });
                            ceps.Add(new cepCapital() { Cep = "65002-900", Uf = "MA" });
                            ceps.Add(new cepCapital() { Cep = "78005-000", Uf = "MT" });
                            ceps.Add(new cepCapital() { Cep = "79002-000", Uf = "MS" });
                            ceps.Add(new cepCapital() { Cep = "30110-000", Uf = "MG" });
                            ceps.Add(new cepCapital() { Cep = "66010-000", Uf = "PA" });
                            ceps.Add(new cepCapital() { Cep = "58010-000", Uf = "PB" });
                            ceps.Add(new cepCapital() { Cep = "80002-900", Uf = "PR" });
                            ceps.Add(new cepCapital() { Cep = "50010-000", Uf = "PE" });
                            ceps.Add(new cepCapital() { Cep = "64000-010", Uf = "PI" });
                            ceps.Add(new cepCapital() { Cep = "20010-000", Uf = "RJ" });
                            ceps.Add(new cepCapital() { Cep = "59010-000", Uf = "RN" });
                            ceps.Add(new cepCapital() { Cep = "90002-900", Uf = "RS" });
                            ceps.Add(new cepCapital() { Cep = "76801-000", Uf = "RO" });
                            ceps.Add(new cepCapital() { Cep = "69301-000", Uf = "RR" });
                            ceps.Add(new cepCapital() { Cep = "88010-000", Uf = "SC" });
                            ceps.Add(new cepCapital() { Cep = "01001-000", Uf = "SP" });
                            ceps.Add(new cepCapital() { Cep = "49000-001", Uf = "SE" });
                            ceps.Add(new cepCapital() { Cep = "77001-002", Uf = "TO" });
                            #endregion

                            var cepCapital = "";
                            foreach (var item in ceps)
                            {
                                if (cliente.clienteEstado.ToLower() == item.Uf.ToLower())
                                {
                                    cepCapital = item.Cep;
                                    break;
                                }
                            }

                            //Gerar fila com parametro de alteração de cep
                            var agora = DateTime.Now;
                            var queue = new tbQueue();
                            queue.agendamento = agora;
                            queue.tipoQueue = 45;
                            queue.idRelacionado = idPedidoPagamento;
                            queue.mensagem = cepCapital;
                            queue.concluido = false;
                            queue.andamento = false;
                            data.tbQueue.Add(queue);
                            data.SaveChanges();
                        }
                        else if (retorno.ToLower().Contains("nosso numero ja existente")) { }
                        else
                        {
                            var validaMensagem = (from c in data.tbPedidoPagamentoRegistroBoleto where c.idPedidoPagamento == idPedidoPagamento && c.RetornoErroDescricao == retorno select c).Any();
                            if (!validaMensagem)
                            {
                                //Gerar fila com parametro de alteração de cep
                                var agora = DateTime.Now;
                                var queue = new tbQueue();
                                queue.agendamento = agora;
                                queue.tipoQueue = 45;
                                queue.idRelacionado = idPedidoPagamento;
                                queue.mensagem = "";
                                queue.concluido = false;
                                queue.andamento = false;
                                data.tbQueue.Add(queue);
                                data.SaveChanges();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var data = new dbCommerceEntities();
                var agora = DateTime.Now;
                var queue = new tbQueue();
                queue.agendamento = agora;
                queue.tipoQueue = 25;
                queue.mensagem = "Erro registro Boleto " + " - " + ex.InnerException.ToString();
                queue.concluido = false;
                queue.andamento = false;
                data.tbQueue.Add(queue);
                data.SaveChanges();
            }
        }

        private string VerificaRetornoErro(string pXmlRetorno)
        {
            var retorno = "";
            try
            {
                XmlDocument xmlDocumento = new XmlDocument();
                xmlDocumento.LoadXml(pXmlRetorno);

                XmlNode node = xmlDocumento.SelectSingleNode("//return//descricaoErro");

                //Verifica retorno com erro no cep 
                if (!string.IsNullOrEmpty(node.InnerText))
                    retorno = node.InnerText;

                return retorno;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ConsultarBoleto(int idPedidoPagamento)
        {

        }


        private static string retornaEntrada(string key, string value)
        {
            return "<entry><key>" + key + "</key><value>" + value + "</value></entry>";
        }



        private static List<tbFeriados> listaFeriados(bool BypassCache)
        {

            var data = new dbCommerceEntities();
            var feriados = (from c in data.tbFeriados select c).ToList();
            return feriados;
        }

        private static DateTime RetornaProximoDiaUtil(DateTime dia)
        {
            DateTime dataFinal = dia;
            var feriados = listaFeriados(true);
            bool diaUtil = false;
            int maximoDias = 0;

            while (diaUtil == false && maximoDias < 5)
            {
                maximoDias++;
                bool fimDeSemana = dataFinal.DayOfWeek == DayOfWeek.Saturday | dataFinal.DayOfWeek == DayOfWeek.Sunday;
                bool feriado = (from c in feriados where c.data.Date == dataFinal.Date select c).Any();
                if (fimDeSemana == false && feriado == false)
                {
                    diaUtil = true;
                }
                else
                {
                    dataFinal = dataFinal.AddDays(1);
                }
            }
            return dataFinal;
        }

        private static string postXMLData(string destinationUrl, string requestXml)
        {

            System.Net.ServicePointManager.ServerCertificateValidationCallback = ((sender, certificate, chain, sslPolicyErrors) => true);
            string file = System.IO.Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, System.AppDomain.CurrentDomain.RelativeSearchPath ?? "") + "\\sslgrao.pfx";
            //string file = "c:\\certificados\\sslgrao.pfx";
            //setting the request

            string soapStr =
            @"<?xml version=""1.0"" encoding=""utf-8""?>
            <soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/""
            xmlns:impl=""http://impl.webservice.dl.app.bsbr.altec.com/""><soapenv:Header/>
            <soapenv:Body>" + requestXml + "</soapenv:Body></soapenv:Envelope>";

            //HttpWebRequest req = (HttpWebRequest)WebRequest.Create(Url);



            HttpWebRequest req;
            req = (HttpWebRequest)HttpWebRequest.Create(destinationUrl);
            req.ContentType = "text/xml;charset=\"utf-8\"";
            req.Accept = "text/xml";
            req.Method = "POST";
            req.ClientCertificates.Add(new System.Security.Cryptography.X509Certificates.X509Certificate(file, "123"));

            string retorno = "";
            //setting the request content
            byte[] byteArray = Encoding.UTF8.GetBytes(soapStr);
            Stream dataStream = req.GetRequestStream();
            dataStream.Write(byteArray, 0, byteArray.Length);
            dataStream.Close();

            using (HttpWebResponse webResponse = (HttpWebResponse)req.GetResponse())
            {
                using (Stream responseStream = webResponse.GetResponseStream())
                {
                    using (StreamReader responseStreamReader = new StreamReader(responseStream, true))
                    {
                        retorno = responseStreamReader.ReadToEnd();
                        responseStreamReader.Close();
                    }

                    responseStream.Close();
                }
                webResponse.Close();
            }
            return retorno;
        }

        private static string removeAcentos(string inputString)
        {
            if ((inputString == "") || (inputString == null))
                return "";

            string normalizedString = inputString.Normalize(NormalizationForm.FormD);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < normalizedString.Length; i++)
            {
                UnicodeCategory uc = CharUnicodeInfo.GetUnicodeCategory(normalizedString[i]);
                if (uc != UnicodeCategory.NonSpacingMark)
                {
                    sb.Append(normalizedString[i]);
                }
            }
            return (sb.ToString().Normalize(NormalizationForm.FormC));
        }

        private static string removeCaracteresEspeciaisDeixaEspaco(string inputString)
        {
            StringBuilder sb = new StringBuilder();
            foreach (char c in inputString)
            {
                if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') | c == '.' || c == '_' || c == '-' || c == ' ')
                {
                    sb.Append(c);
                }
            }
            return sb.ToString();
        }

    }
}
