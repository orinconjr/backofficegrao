﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BarkCommerce.Core.Interfaces.Gateway;
using BarkCommerce.Model.Gateway;
using BarkCommerce.Model.Pagamento;
using Newtonsoft.Json;
using BarkCommerce.Core.DataAccess.SqlServer;
using GatewayApiClient.DataContracts;
using GatewayApiClient.DataContracts.EnumTypes;
using GatewayApiClient;
using System.Collections.ObjectModel;
using GatewayApiClient.EnumTypes;
using System.Net;
using System.Configuration;

namespace BarkCommerce.Core.Repositories.Gateway
{
    public class MundipaggRepository : IGatewayRepository
    {
        public PagamentoPedidoGateway ProcessarCartaoCredito(PagamentoConfiguracao configuracao, PagamentoPedidoDados dadosPagamento, bool vendaDireta)
        {
            try
            {
                var transaction = new CreditCardTransaction()
                {
                    AmountInCents = Convert.ToInt64(dadosPagamento.Valor * 100),
                    CreditCard = new CreditCard()
                    {
                        CreditCardBrand = (CreditCardBrandEnum)configuracao.IdBandeiraCartaoGateway,
                        CreditCardNumber = new String(dadosPagamento.NumeroCartao.Where(Char.IsDigit).ToArray()),
                        ExpMonth = Convert.ToInt32(new String(dadosPagamento.ValidadeCartaoMes.Where(Char.IsDigit).ToArray())),
                        ExpYear = Convert.ToInt32((new String(dadosPagamento.ValidadeCartaoAno.Where(Char.IsDigit).ToArray())).Substring(2, 2)),
                        HolderName = dadosPagamento.NomeCartao,
                        SecurityCode = new String(dadosPagamento.CvvCartao.Where(Char.IsDigit).ToArray())
                    },
                    CreditCardOperation = vendaDireta ? CreditCardOperationEnum.AuthAndCapture : CreditCardOperationEnum.AuthOnly,
                    InstallmentCount = dadosPagamento.QuantidadeParcelas,
                    Options = new CreditCardTransactionOptions()
                    {
                        SoftDescriptorText = "Grao de Gente"
                        //PaymentMethodCode = 37 // Forcar adquirente
                    }
                };

                // Cria requisição.
                var createSaleRequest = new CreateSaleRequest()
                {
                    // Adiciona a transação na requisição.
                    CreditCardTransactionCollection = new Collection<CreditCardTransaction>(new CreditCardTransaction[] { transaction }),
                    Order = new Order()
                    {
                        OrderReference = dadosPagamento.IdPedidoPagamento.ToString()
                    }
                };
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                // Coloque a sua MerchantKey aqui.
                Guid merchantKey = Guid.Parse(configuracao.GatewayContrato.GatewayKey);

                // Cria o client que enviará a transação.
                var serviceClient = new GatewayServiceClient(merchantKey, new Uri(ConfigurationManager.AppSettings["Mundipagg_Endpoint"]));

                // Autoriza a transação e recebe a resposta do gateway.
                
                var httpResponse = serviceClient.Sale.Create(createSaleRequest);
                
                if (httpResponse.Response.CreditCardTransactionResultCollection != null)
                {
                    int responseCode = -1;
                    try
                    {
                        responseCode = Convert.ToInt32(httpResponse.Response.CreditCardTransactionResultCollection.FirstOrDefault().AcquirerReturnCode);
                    }
                    catch (Exception ex)
                    {
                        responseCode = -1;
                    }
                    string respondeStatus = httpResponse.Response.CreditCardTransactionResultCollection.FirstOrDefault().CreditCardTransactionStatus.ToString().ToLower();
                    if (respondeStatus == "captured" | respondeStatus == "authorized" | respondeStatus == "authorizedpendingcapture")
                    {
                        responseCode = 0;
                    }
                    var response = new PagamentoPedidoGateway()
                    {
                        AuthenticationUrl = "",
                        AuthorizationCode = httpResponse.Response.CreditCardTransactionResultCollection.FirstOrDefault().AuthorizationCode,
                        AvsResponseCode = httpResponse.Response.CreditCardTransactionResultCollection.FirstOrDefault().AffiliationCode,
                        BoletoUrl = "",
                        CvvResponseCode = httpResponse.Response.CreditCardTransactionResultCollection.FirstOrDefault().AcquirerReturnCode,
                        DataSolicitacao = DateTime.Now,
                        DataSolicitacaoCaptura = null,
                        DataSolicitacaoEstorno = null,
                        ErrorMessage = "",
                        OnlineDebitUrl = "",
                        TransactionId = httpResponse.Response.CreditCardTransactionResultCollection.FirstOrDefault().TransactionIdentifier,
                        PagamentoPedidoGatewayId = 0,
                        ProcessorMessage = httpResponse.Response.CreditCardTransactionResultCollection.FirstOrDefault().AcquirerMessage,
                        ResponseMessage = httpResponse.Response.CreditCardTransactionResultCollection.FirstOrDefault().CreditCardTransactionStatus.ToString(),
                        ResponseCode = responseCode,
                        VendaDireta = vendaDireta,
                        SaveOnFile = "",
                        ProcessorTransactionId = httpResponse.Response.CreditCardTransactionResultCollection.FirstOrDefault().TransactionIdentifier,
                        ProcessorReturnCode = httpResponse.Response.CreditCardTransactionResultCollection.FirstOrDefault().AcquirerReturnCode,
                        OrderId = httpResponse.Response.OrderResult.OrderKey.ToString(),
                        ProcessorReferenceNumber = httpResponse.Response.CreditCardTransactionResultCollection.FirstOrDefault().TransactionReference,
                        TransactionTimestamp = httpResponse.Response.CreditCardTransactionResultCollection.FirstOrDefault().DueDate.ToString(),
                        IdGatewayContrato = configuracao.IdGatewayContrato,
                        IdPedidoPagamento = dadosPagamento.IdPedidoPagamento
                    };
                    return response;
                }
                else
                {
                    var data = new dbCommerceEntities();
                    var agora = DateTime.Now;
                    var queue = new tbQueue();
                    queue.agendamento = agora;
                    queue.tipoQueue = 25;
                    queue.mensagem = dadosPagamento.IdPedido + " - " + httpResponse.Response.ToString();
                    queue.concluido = false;
                    queue.andamento = false;
                    data.tbQueue.Add(queue);
                    data.SaveChanges();
                    return null;
                }


            }
            catch (Exception ex)
            {
                var data = new dbCommerceEntities();
                var agora = DateTime.Now;
                var queue = new tbQueue();
                queue.agendamento = agora;
                queue.tipoQueue = 25;
                queue.mensagem = dadosPagamento.IdPedido + " - " + ex.InnerException.ToString();
                queue.concluido = false;
                queue.andamento = false;
                data.tbQueue.Add(queue);
                data.SaveChanges();
                return null;

            }
        }

        public PagamentoPedidoGateway SolicitarReservaCartaoCredito(PagamentoConfiguracao configuracao, PagamentoPedidoDados dadosPagamento)
        {
            return ProcessarCartaoCredito(configuracao, dadosPagamento, false);
        }

        public PagamentoPedidoGateway RealizarVendaDiretaCartaoCredito(PagamentoConfiguracao configuracao, PagamentoPedidoDados dadosPagamento)
        {
            return ProcessarCartaoCredito(configuracao, dadosPagamento, true);
        }

        public PagamentoPedidoGateway CapturarPagamentoCartaoCredito(PagamentoPedidoGateway pagamentoPedido, PagamentoPedidoDados dadosPagamento, GatewayContrato contrato)
        {
            try
            {

                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                Guid merchantKey = Guid.Parse(contrato.GatewayKey);
                // Chave do pedido
                Guid orderKey = Guid.Parse(pagamentoPedido.OrderId);

                // Cria o cliente para capturar as transações.
                IGatewayServiceClient client = new GatewayServiceClient(merchantKey, new Uri(ConfigurationManager.AppSettings["Mundipagg_Endpoint"]));

                // Captura as transações de cartão de crédito do pedido.
                var httpResponse = client.Sale.Manage(ManageOperationEnum.Capture, orderKey);

                if (httpResponse.HttpStatusCode == HttpStatusCode.OK
                    && httpResponse.Response.CreditCardTransactionResultCollection.Any()
                    && httpResponse.Response.CreditCardTransactionResultCollection.All(p => p.Success == true))
                {
                    Console.WriteLine("Transações capturadas.");
                    var response = pagamentoPedido;
                    response.DataSolicitacaoCaptura = DateTime.Now;
                    response.ErrorMessage = "";
                    response.TransactionId = httpResponse.Response.CreditCardTransactionResultCollection.First().TransactionIdentifier;
                    response.ProcessorMessage = httpResponse.Response.CreditCardTransactionResultCollection.First().AcquirerMessage;
                    response.ResponseMessage = httpResponse.Response.CreditCardTransactionResultCollection.FirstOrDefault().CreditCardTransactionStatus.ToString();
                    response.ResponseCode = Convert.ToInt32(httpResponse.HttpStatusCode);
                    response.ProcessorTransactionId = httpResponse.Response.CreditCardTransactionResultCollection.FirstOrDefault().TransactionIdentifier;
                    response.ProcessorReturnCode = httpResponse.Response.CreditCardTransactionResultCollection.FirstOrDefault().AcquirerReturnCode;
                    response.OrderId = pagamentoPedido.OrderId;
                    response.ProcessorReferenceNumber = httpResponse.Response.CreditCardTransactionResultCollection.FirstOrDefault().TransactionReference;
                    response.TransactionTimestamp = httpResponse.Response.CreditCardTransactionResultCollection.FirstOrDefault().DueDate.ToString();
                    return response;
                }
                else
                {
                    var data = new dbCommerceEntities();
                    var agora = DateTime.Now;
                    var queue = new tbQueue();
                    queue.agendamento = agora;
                    queue.tipoQueue = 25;
                    queue.mensagem = dadosPagamento.IdPedido + " - " + httpResponse.Response.ToString();
                    queue.concluido = false;
                    queue.andamento = false;
                    data.tbQueue.Add(queue);
                    data.SaveChanges();
                    return null;
                }
            }
            catch (Exception ex)
            {
                var data = new dbCommerceEntities();
                var agora = DateTime.Now;
                var queue = new tbQueue();
                queue.agendamento = agora;
                queue.tipoQueue = 25;
                queue.mensagem = dadosPagamento.IdPedido + " - " + ex.InnerException.ToString();
                queue.concluido = false;
                queue.andamento = false;
                data.tbQueue.Add(queue);
                data.SaveChanges();
                return null;
            }
        }
        public PagamentoPedidoGateway EstornarReservaCartaoCredito(PagamentoPedidoGateway pagamentoPedido, PagamentoPedidoDados dadosPagamento, GatewayContrato contrato)
        {
            try
            {

                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                Guid merchantKey = Guid.Parse(contrato.GatewayKey);
                // Chave do pedido
                Guid orderKey = Guid.Parse(pagamentoPedido.OrderId);

                // Cria o cliente para capturar as transações.
                IGatewayServiceClient client = new GatewayServiceClient(merchantKey, new Uri(ConfigurationManager.AppSettings["Mundipagg_Endpoint"]));

                // Transação específica que será cancelada.
                var transactionToCancel = new ManageCreditCardTransaction()
                {
                    AmountInCents = Convert.ToInt64(dadosPagamento.Valor * 100)
                };

                // Cancela as transações de cartão de crédito do pedido.
                var httpResponse = client.Sale.Manage(ManageOperationEnum.Cancel, orderKey);

                if (httpResponse.HttpStatusCode == HttpStatusCode.OK
                    && httpResponse.Response.CreditCardTransactionResultCollection.Any()
                    && httpResponse.Response.CreditCardTransactionResultCollection.All(p => p.Success == true))
                {
                    Console.WriteLine("Transações capturadas.");
                    var response = pagamentoPedido;
                    response.DataSolicitacaoEstorno = DateTime.Now;
                    response.ErrorMessage = "";
                    response.TransactionId = httpResponse.Response.CreditCardTransactionResultCollection.First().TransactionIdentifier;
                    response.ProcessorMessage = httpResponse.Response.CreditCardTransactionResultCollection.First().AcquirerMessage;
                    response.ResponseMessage = httpResponse.Response.CreditCardTransactionResultCollection.FirstOrDefault().CreditCardTransactionStatus.ToString();
                    response.ResponseCode = Convert.ToInt32(httpResponse.HttpStatusCode);
                    response.ProcessorTransactionId = httpResponse.Response.CreditCardTransactionResultCollection.FirstOrDefault().TransactionIdentifier;
                    response.ProcessorReturnCode = httpResponse.Response.CreditCardTransactionResultCollection.FirstOrDefault().AcquirerReturnCode;
                    response.OrderId = pagamentoPedido.OrderId;
                    response.ProcessorReferenceNumber = httpResponse.Response.CreditCardTransactionResultCollection.FirstOrDefault().TransactionReference;
                    response.TransactionTimestamp = httpResponse.Response.CreditCardTransactionResultCollection.FirstOrDefault().DueDate.ToString();
                    return response;
                }
                else
                {
                    var data = new dbCommerceEntities();
                    var agora = DateTime.Now;
                    var queue = new tbQueue();
                    queue.agendamento = agora;
                    queue.tipoQueue = 25;
                    queue.mensagem = dadosPagamento.IdPedido + " - " + httpResponse.Response.ToString();
                    queue.concluido = false;
                    queue.andamento = false;
                    data.tbQueue.Add(queue);
                    data.SaveChanges();
                    return null;
                }

            }
            catch (Exception ex)
            {
                var data = new dbCommerceEntities();
                var agora = DateTime.Now;
                var queue = new tbQueue();
                queue.agendamento = agora;
                queue.tipoQueue = 25;
                queue.mensagem = dadosPagamento.IdPedido + " - " + ex.InnerException.ToString();
                queue.concluido = false;
                queue.andamento = false;
                data.tbQueue.Add(queue);
                data.SaveChanges();
                return null;
            }
        }
    }
}
