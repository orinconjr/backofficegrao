﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BarkCommerce.Core.Interfaces.Gateway;
using BarkCommerce.Model.Gateway;
using BarkCommerce.Model.Pagamento;
using Newtonsoft.Json;
using BarkCommerce.Core.DataAccess.SqlServer;
using Uol.PagSeguro.Constants;
using Uol.PagSeguro.Domain;
using Uol.PagSeguro.Domain.Direct;
using Uol.PagSeguro.Exception;
using Uol.PagSeguro.Resources;
using Uol.PagSeguro.Service;
using System.Configuration;

namespace BarkCommerce.Core.Repositories.Gateway
{
    public class PagseguroRepository : IGatewayRepository
    {
        public PagamentoPedidoGateway ProcessarCartaoCredito(PagamentoConfiguracao configuracao, PagamentoPedidoDados dadosPagamento, bool vendaDireta)
        {
            try
            {
                var data = new dbCommerceEntities();
                var pedido = (from c in data.tbPedidos
                              join d in data.tbClientes on c.clienteId equals d.clienteId
                              where c.pedidoId == dadosPagamento.IdPedido select new
                              {
                                  c.pedidoId,
                                  d.clienteEmail,
                                  c.endEstado,
                                  c.endCidade,
                                  c.endBairro,
                                  c.endCep,
                                  c.endRua,
                                  c.endNumero,
                                  c.endComplemento,
                                  d.clienteEstado,
                                  d.clienteCidade,
                                  d.clienteBairro,
                                  d.clienteCep,
                                  d.clienteRua,
                                  d.clienteNumero,
                                  d.clienteComplemento,
                                  d.clienteFoneCelular,
                                  d.clienteFoneComercial,
                                  d.clienteFoneResidencial,
                                  d.clienteCPFCNPJ,
                                  d.clienteDataNascimento,
                                  d.clienteNome
                              }).First();

                PagSeguroConfiguration.UrlXmlConfiguration = System.IO.Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, System.AppDomain.CurrentDomain.RelativeSearchPath ?? "") + "\\" + ConfigurationManager.AppSettings["PagSeguroConfigurationXml"];

                bool isSandbox = Convert.ToBoolean(ConfigurationManager.AppSettings["PagSeguroConfigurationSandbox"]);
                EnvironmentConfiguration.ChangeEnvironment(isSandbox);

                // Instantiate a new checkout
                CreditCardCheckout checkout = new CreditCardCheckout();

                // Sets the payment mode
                checkout.PaymentMode = PaymentMode.DEFAULT;

                // Sets the receiver e-mail should will get paid
                checkout.ReceiverEmail = pedido.clienteEmail;

                // Sets the currency
                checkout.Currency = Currency.Brl;

                var itensPedido = (from c in data.tbItensPedido
                                   where c.pedidoId == dadosPagamento.IdPedido
                                   select new
                                   {
                                       c.produtoId,
                                       c.tbProdutos.produtoNome,
                                       c.itemQuantidade,
                                       c.itemValor
                                   }).ToList();
               foreach(var itemPedido in itensPedido)
                {
                    checkout.Items.Add(new Item(itemPedido.produtoId.ToString(), itemPedido.produtoNome, Convert.ToInt32(itemPedido.itemQuantidade), itemPedido.itemValor));

                }

                // Sets a reference code for this checkout, it is useful to identify this payment in future notifications.
                checkout.Reference = dadosPagamento.IdPedidoPagamento.ToString();

                // Sets shipping information.
                checkout.Shipping = new Shipping();
                checkout.Shipping.ShippingType = ShippingType.NotSpecified;
                checkout.Shipping.Cost = 0.00m;
                checkout.Shipping.Address = new Address(
                    "BRA",
                    pedido.endEstado,
                    pedido.endCidade,
                    pedido.endBairro,
                    pedido.endCep.Replace("-", "").Replace(" ", "").Replace(".", ""),
                    pedido.endRua,
                    pedido.endNumero,
                    pedido.endComplemento
                );

                // Sets shipping information.
                checkout.Billing = new Billing();
                checkout.Billing.Address = new Address(
                    "BRA",
                    pedido.clienteEstado,
                    pedido.clienteCidade,
                    pedido.clienteBairro,
                    pedido.clienteCep.Replace("-", "").Replace(" ", "").Replace(".", ""),
                    pedido.clienteRua,
                    pedido.clienteNumero,
                    pedido.clienteComplemento
                );
                var phone = new Phone();
                if (!string.IsNullOrEmpty(pedido.clienteFoneCelular))
                {
                    if (pedido.clienteFoneCelular.Contains(")"))
                    {
                        phone = new Phone()
                        {

                            AreaCode = pedido.clienteFoneCelular.Split(')')[0].Replace("(", "").Replace(" ", ""),
                            Number = pedido.clienteFoneCelular.Split(')')[1].Replace(" ", "").Replace("-", "")
                        };
                    }
                    else
                    {
                        phone = new Phone()
                        {

                            AreaCode = pedido.clienteFoneCelular.StartsWith("0") ? pedido.clienteFoneCelular.Substring(0, 3) : pedido.clienteFoneCelular.Substring(0, 2),
                            Number = pedido.clienteFoneCelular.StartsWith("0") ? pedido.clienteFoneCelular.Substring(3, pedido.clienteFoneCelular.Length - 3).Replace(" ", "").Replace("-", "") : pedido.clienteFoneCelular.Substring(2, pedido.clienteFoneCelular.Length - 2).Replace(" ", "").Replace("-", "")
                        };
                    }
                }
                else if (!string.IsNullOrEmpty(pedido.clienteFoneResidencial))
                {
                    if (pedido.clienteFoneResidencial.Contains(")"))
                    {
                        phone = new Phone()
                        {
                            AreaCode = pedido.clienteFoneResidencial.Split(')')[0].Replace("(", "").Replace(" ", ""),
                            Number = pedido.clienteFoneResidencial.Split(')')[1].Replace(" ", "").Replace("-", "")
                        };
                    }
                    else
                    {
                        phone = new Phone()
                        {

                            AreaCode = pedido.clienteFoneResidencial.StartsWith("0") ? pedido.clienteFoneResidencial.Substring(0, 3) : pedido.clienteFoneResidencial.Substring(0, 2),
                            Number = pedido.clienteFoneResidencial.StartsWith("0") ? pedido.clienteFoneResidencial.Substring(3, pedido.clienteFoneResidencial.Length - 3).Replace(" ", "").Replace("-", "") : pedido.clienteFoneResidencial.Substring(2, pedido.clienteFoneResidencial.Length - 2).Replace(" ", "").Replace("-", "")
                        };
                    }
                }

                var dataNascimento = DateTime.Now;
                if (!string.IsNullOrEmpty(pedido.clienteDataNascimento) && pedido.clienteDataNascimento != " ")
                {
                    try
                    {
                        dataNascimento = Convert.ToDateTime(pedido.clienteDataNascimento);
                    }
                    catch (Exception ex2) { }
                }

                // Sets credit card holder information.
                checkout.Holder = new Holder(
                    dadosPagamento.NomeCartao,
                    phone,
                    new HolderDocument(Documents.GetDocumentByType("CPF"), pedido.clienteCPFCNPJ),
                    dataNascimento.ToShortDateString()
                );

                // Sets your customer information.
                // If you using SANDBOX you must use an email @sandbox.pagseguro.com.br
                if (isSandbox)
                {
                    checkout.Sender = new Sender(
                        pedido.clienteNome,
                        "comprador@sandbox.pagseguro.com.br",
                        phone
                    );
                }
                else
                {
                    checkout.Sender = new Sender(
                        pedido.clienteNome,
                        pedido.clienteEmail,
                        phone
                    );
                }

                var pagamento = (from c in data.tbPedidoPagamento
                                 where c.idPedidoPagamento == dadosPagamento.IdPedidoPagamento
                                 select new
                                 {
                                     c.userToken,
                                     c.paymentToken
                                 }).FirstOrDefault();
                checkout.Sender.Hash = pagamento.userToken;
                SenderDocument senderCPF = new SenderDocument(Documents.GetDocumentByType("CPF"), pedido.clienteCPFCNPJ);
                checkout.Sender.Documents.Add(senderCPF);

                // Sets a credit card token.
                checkout.Token = pagamento.paymentToken;

                //Sets the installments information
                checkout.Installment = new Installment(dadosPagamento.QuantidadeParcelas, dadosPagamento.ValorParcela, dadosPagamento.QuantidadeParcelas);

                // Sets the notification url
                checkout.NotificationURL = "http://admin.graodegente.com.br/pagseguro.aspx";

                try
                {
                    AccountCredentials credentials = PagSeguroConfiguration.Credentials(isSandbox);
                    Transaction result = TransactionService.CreateCheckout(credentials, checkout);
                    //Console.WriteLine(result);
                    //Console.ReadKey();
                }
                catch (PagSeguroServiceException exception)
                {
                    Console.WriteLine(exception.Message + "\n");
                    foreach (ServiceError element in exception.Errors)
                    {
                        Console.WriteLine(element + "\n");
                    }
                    Console.ReadKey();
                }

                return null;

            }
            catch (Exception ex)
            {
                var data = new dbCommerceEntities();
                var agora = DateTime.Now;
                var queue = new tbQueue();
                queue.agendamento = agora;
                queue.tipoQueue = 25;
                queue.mensagem = dadosPagamento.IdPedido + " - " + ex.InnerException.ToString();
                queue.concluido = false;
                queue.andamento = false;
                data.tbQueue.Add(queue);
                data.SaveChanges();
                return null;

            }
        }

        public PagamentoPedidoGateway SolicitarReservaCartaoCredito(PagamentoConfiguracao configuracao, PagamentoPedidoDados dadosPagamento)
        {
            return ProcessarCartaoCredito(configuracao, dadosPagamento, false);
        }

        public PagamentoPedidoGateway RealizarVendaDiretaCartaoCredito(PagamentoConfiguracao configuracao, PagamentoPedidoDados dadosPagamento)
        {
            return ProcessarCartaoCredito(configuracao, dadosPagamento, true);
        }

        public PagamentoPedidoGateway CapturarPagamentoCartaoCredito(PagamentoPedidoGateway pagamentoPedido, PagamentoPedidoDados dadosPagamento, GatewayContrato contrato)
        {
            try
            {
                return null;
            }
            catch (Exception ex)
            {
                var data = new dbCommerceEntities();
                var agora = DateTime.Now;
                var queue = new tbQueue();
                queue.agendamento = agora;
                queue.tipoQueue = 25;
                queue.mensagem = dadosPagamento.IdPedido + " - " + ex.InnerException.ToString();
                queue.concluido = false;
                queue.andamento = false;
                data.tbQueue.Add(queue);
                data.SaveChanges();
                return null;
            }
        }
        public PagamentoPedidoGateway EstornarReservaCartaoCredito(PagamentoPedidoGateway pagamentoPedido, PagamentoPedidoDados dadosPagamento, GatewayContrato contrato)
        {
            try
            {
                return null;

            }
            catch (Exception ex)
            {
                var data = new dbCommerceEntities();
                var agora = DateTime.Now;
                var queue = new tbQueue();
                queue.agendamento = agora;
                queue.tipoQueue = 25;
                queue.mensagem = dadosPagamento.IdPedido + " - " + ex.InnerException.ToString();
                queue.concluido = false;
                queue.andamento = false;
                data.tbQueue.Add(queue);
                data.SaveChanges();
                return null;
            }
        }
    }
}
