﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BarkCommerce.Core.Interfaces.Gateway;
using BarkCommerce.Model.Gateway;
using BarkCommerce.Model.Pagamento;
using MaxiPago.DataContract;
using Newtonsoft.Json;
using BarkCommerce.Core.DataAccess.SqlServer;

namespace BarkCommerce.Core.Repositories.Gateway
{
    public class MaxipagoRepository : IGatewayRepository
    {
        public PagamentoPedidoGateway SolicitarReservaCartaoCredito(PagamentoConfiguracao configuracao, PagamentoPedidoDados dadosPagamento)
        {
            try
            {
                MaxiPago.Gateway.Transaction auth = new MaxiPago.Gateway.Transaction();
                Task<ResponseBase> resp = Task.Factory.StartNew<ResponseBase>(() =>
                {
                    return auth.Auth(
                            merchantId: configuracao.GatewayContrato.GatewayId
                            , merchantKey: configuracao.GatewayContrato.GatewayKey
                            , referenceNum: dadosPagamento.IdPedidoPagamento.ToString()
                            , chargeTotal: dadosPagamento.Valor
                            , creditCardNumber: dadosPagamento.NumeroCartao
                            , expMonth: string.Format("{0:0#}", dadosPagamento.ValidadeCartaoMes)
                            , expYear: dadosPagamento.ValidadeCartaoAno
                            , cvvInd: null
                            , cvvNumber: dadosPagamento.CvvCartao
                            , authentication: null
                            , processorId: string.Format("{0}", configuracao.IdCodigoAdquirenteGateway)
                            , numberOfInstallments: dadosPagamento.QuantidadeParcelas.ToString()
                            , chargeInterest: "N"
                            , ipAddress: dadosPagamento.Ip
                            , customerIdExt: null
                            , currencyCode: null
                            , fraudCheck: null
                            , softDescriptor: null
                            , iataFee: null
                    );
                });
                resp.Wait();
                var result = resp.Result;

                var json = JsonConvert.SerializeObject(result, Formatting.None, new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
                MaxiPagoResponse maxiResponse = JsonConvert.DeserializeObject<MaxiPagoResponse>(json);
                var response = new PagamentoPedidoGateway()
                {
                    AuthenticationUrl = maxiResponse.AuthenticationUrl,
                    AuthorizationCode = maxiResponse.AuthorizationCode,
                    AvsResponseCode = maxiResponse.AvsResponseCode,
                    BoletoUrl = maxiResponse.BoletoUrl,
                    CvvResponseCode = maxiResponse.CvvResponseCode,
                    DataSolicitacao = DateTime.Now,
                    DataSolicitacaoCaptura = null,
                    DataSolicitacaoEstorno = null,
                    ErrorMessage = maxiResponse.ErrorMessage,
                    OnlineDebitUrl = maxiResponse.OnlineDebitUrl,
                    TransactionId = maxiResponse.TransactionId,
                    PagamentoPedidoGatewayId = 0,
                    ProcessorMessage = maxiResponse.ProcessorMessage,
                    ResponseMessage = maxiResponse.ResponseMessage,
                    ResponseCode = maxiResponse.ResponseCode,
                    VendaDireta = false,
                    SaveOnFile = maxiResponse.SaveOnFile,
                    ProcessorTransactionId = maxiResponse.ProcessorTransactionID,
                    ProcessorReturnCode = maxiResponse.ProcessorReturnCode,
                    OrderId = maxiResponse.OrderId,
                    ProcessorReferenceNumber = maxiResponse.ProcessorReferenceNumber,
                    TransactionTimestamp = maxiResponse.TransactionTimestamp,
                    IdGatewayContrato = configuracao.IdGatewayContrato,
                    IdPedidoPagamento = dadosPagamento.IdPedidoPagamento
                };
                return response;
            }
            catch (Exception ex)
            {
                var data = new dbCommerceEntities();
                var agora = DateTime.Now;
                var queue = new tbQueue();
                queue.agendamento = agora;
                queue.tipoQueue = 25;
                queue.mensagem = ex.InnerException.ToString();
                queue.concluido = false;
                queue.andamento = false;
                data.tbQueue.Add(queue);
                data.SaveChanges();
                return null;
            }
        }

        public PagamentoPedidoGateway RealizarVendaDiretaCartaoCredito(PagamentoConfiguracao configuracao, PagamentoPedidoDados dadosPagamento)
        {
            try
            {
                MaxiPago.Gateway.Transaction auth = new MaxiPago.Gateway.Transaction();
                Task<ResponseBase> resp = Task.Factory.StartNew<ResponseBase>(() =>
                {
                    return auth.Sale(
                             merchantId: configuracao.GatewayContrato.GatewayId
                            , merchantKey: configuracao.GatewayContrato.GatewayKey
                            , referenceNum: dadosPagamento.IdPedidoPagamento.ToString()
                            , chargeTotal: dadosPagamento.Valor
                            , creditCardNumber: dadosPagamento.NumeroCartao
                            , expMonth: string.Format("{0:0#}", dadosPagamento.ValidadeCartaoMes)
                            , expYear: dadosPagamento.ValidadeCartaoAno
                           , cvvInd: null
                            , cvvNumber: dadosPagamento.CvvCartao
                            , authentication: null
                            , processorId: string.Format("{0}", configuracao.IdCodigoAdquirenteGateway)
                            , numberOfInstallments: dadosPagamento.QuantidadeParcelas.ToString()
                            , chargeInterest: "N"
                            , ipAddress: dadosPagamento.Ip
                            , customerIdExt: null
                            , currencyCode: null
                            , fraudCheck: null
                            , softDescriptor: null
                            , iataFee: null
                    );
                });
                resp.Wait();
                var result = resp.Result;

                var json = JsonConvert.SerializeObject(result, Formatting.None, new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
                MaxiPagoResponse maxiResponse = JsonConvert.DeserializeObject<MaxiPagoResponse>(json);
                var response = new PagamentoPedidoGateway()
                {
                    AuthenticationUrl = maxiResponse.AuthenticationUrl,
                    AuthorizationCode = maxiResponse.AuthorizationCode,
                    AvsResponseCode = maxiResponse.AvsResponseCode,
                    BoletoUrl = maxiResponse.BoletoUrl,
                    CvvResponseCode = maxiResponse.CvvResponseCode,
                    DataSolicitacao = DateTime.Now,
                    DataSolicitacaoCaptura = DateTime.Now,
                    DataSolicitacaoEstorno = null,
                    ErrorMessage = maxiResponse.ErrorMessage,
                    OnlineDebitUrl = maxiResponse.OnlineDebitUrl,
                    TransactionId = maxiResponse.TransactionId,
                    PagamentoPedidoGatewayId = 0,
                    ProcessorMessage = maxiResponse.ProcessorMessage,
                    ResponseMessage = maxiResponse.ResponseMessage,
                    ResponseCode = maxiResponse.ResponseCode,
                    VendaDireta = true,
                    SaveOnFile = maxiResponse.SaveOnFile,
                    ProcessorTransactionId = maxiResponse.ProcessorTransactionID,
                    ProcessorReturnCode = maxiResponse.ProcessorReturnCode,
                    OrderId = maxiResponse.OrderId,
                    ProcessorReferenceNumber = maxiResponse.ProcessorReferenceNumber,
                    TransactionTimestamp = maxiResponse.TransactionTimestamp,
                    IdGatewayContrato = configuracao.IdGatewayContrato,
                    IdPedidoPagamento = dadosPagamento.IdPedidoPagamento
                };
                return response;
            }
            catch (Exception ex)
            {
                var data = new dbCommerceEntities();
                var agora = DateTime.Now;
                var queue = new tbQueue();
                queue.agendamento = agora;
                queue.tipoQueue = 25;
                queue.mensagem = ex.InnerException.ToString();
                queue.concluido = false;
                queue.andamento = false;
                data.tbQueue.Add(queue);
                data.SaveChanges();
                return null;
            }
        }

        public PagamentoPedidoGateway CapturarPagamentoCartaoCredito(PagamentoPedidoGateway pagamentoPedido, PagamentoPedidoDados dadosPagamento, GatewayContrato contrato)
        {
            try
            {
                MaxiPago.Gateway.Transaction captura = new MaxiPago.Gateway.Transaction();
                Task<ResponseBase> resp = Task.Factory.StartNew<ResponseBase>(() =>
                {
                    return captura.Capture(
                                merchantId: contrato.GatewayId,
                                merchantKey: contrato.GatewayKey,
                                orderID: pagamentoPedido.OrderId,
                                referenceNum: pagamentoPedido.IdPedidoPagamento.ToString(),
                                chargeTotal: dadosPagamento.ValorTotal
                                );
                });
                resp.Wait();
                var result = resp.Result;

                var json = JsonConvert.SerializeObject(result, Formatting.None, new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
                MaxiPagoResponse maxiResponse = JsonConvert.DeserializeObject<MaxiPagoResponse>(json);
                var response = pagamentoPedido;
                response.DataSolicitacaoCaptura = DateTime.Now;
                response.ErrorMessage = maxiResponse.ErrorMessage;
                response.TransactionId = maxiResponse.TransactionId;
                response.ProcessorMessage = maxiResponse.ProcessorMessage;
                response.ResponseMessage = maxiResponse.ResponseMessage;
                response.ResponseCode = maxiResponse.ResponseCode;
                response.ProcessorTransactionId = maxiResponse.ProcessorTransactionID;
                response.ProcessorReturnCode = maxiResponse.ProcessorReturnCode;
                response.OrderId = maxiResponse.OrderId;
                response.ProcessorReferenceNumber = maxiResponse.ProcessorReferenceNumber;
                response.TransactionTimestamp = maxiResponse.TransactionTimestamp;
                return response;
            }
            catch (Exception ex)
            {
                var data = new dbCommerceEntities();
                var agora = DateTime.Now;
                var queue = new tbQueue();
                queue.agendamento = agora;
                queue.tipoQueue = 25;
                queue.mensagem = ex.InnerException.ToString();
                queue.concluido = false;
                queue.andamento = false;
                data.tbQueue.Add(queue);
                data.SaveChanges();
                return null;
            }
        }
        public PagamentoPedidoGateway EstornarReservaCartaoCredito(PagamentoPedidoGateway pagamentoPedido, PagamentoPedidoDados dadosPagamento, GatewayContrato contrato)
        {
            try
            {
                MaxiPago.Gateway.Transaction operation = new MaxiPago.Gateway.Transaction();
                Task<ResponseBase> resp = Task.Factory.StartNew<ResponseBase>(() =>
                {
                    return operation.Void(
                                        merchantId: contrato.GatewayId,
                                        merchantKey: contrato.GatewayKey,
                                        transactionID: pagamentoPedido.OrderId,
                                        ipAddress: dadosPagamento.Ip
                                    );
                });
                resp.Wait();
                var result = resp.Result;
                var json = JsonConvert.SerializeObject(result, Formatting.None, new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
                MaxiPagoResponse maxiResponse = JsonConvert.DeserializeObject<MaxiPagoResponse>(json);
                var response = pagamentoPedido;
                response.DataSolicitacaoCaptura = DateTime.Now;
                response.ErrorMessage = maxiResponse.ErrorMessage;
                response.TransactionId = maxiResponse.TransactionId;
                response.ProcessorMessage = maxiResponse.ProcessorMessage;
                response.ResponseMessage = maxiResponse.ResponseMessage;
                response.ResponseCode = maxiResponse.ResponseCode;
                response.ProcessorTransactionId = maxiResponse.ProcessorTransactionID;
                response.ProcessorReturnCode = maxiResponse.ProcessorReturnCode;
                response.OrderId = maxiResponse.OrderId;
                response.ProcessorReferenceNumber = maxiResponse.ProcessorReferenceNumber;
                response.TransactionTimestamp = maxiResponse.TransactionTimestamp;
                return response;
            }
            catch (Exception ex)
            {
                var data = new dbCommerceEntities();
                var agora = DateTime.Now;
                var queue = new tbQueue();
                queue.agendamento = agora;
                queue.tipoQueue = 25;
                queue.mensagem = ex.InnerException.ToString();
                queue.concluido = false;
                queue.andamento = false;
                data.tbQueue.Add(queue);
                data.SaveChanges();
                return null;
            }
        }

    }
}
