﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using BarkCommerce.Core.DataAccess.SqlServer;
using BarkCommerce.Core.Interfaces.AnaliseRisco;
using BarkCommerce.Model.AnaliseRisco;
using BarkCommerce.Model.Pagamento;

namespace BarkCommerce.Core.Repositories.AnaliseRisco
{
    public class ClearSaleRepository : IAnaliseRiscoRepository
    {
        private string entityCode = "3C557DF3-C307-4107-9BCD-E7BDC0EBBB56";

        public void EnviarAnaliseRiscoAsync(PagamentoPedidoDados dadosPagamento)
        {
            using (var data = new dbCommerceEntities())
            {
                var agora = DateTime.Now;
                var queue = new tbQueue();
                queue.agendamento = agora;
                queue.tipoQueue = 24;
                queue.mensagem = dadosPagamento.IdPedidoPagamento.ToString();
                queue.concluido = false;
                queue.andamento = false;
                data.tbQueue.Add(queue);
                data.SaveChanges();
            }
        }

        public void EnviarReAnaliseRisco(PagamentoPedidoDados dadosPagamento)
        {
            try
            {
                using (var data = new dbCommerceEntities())
                {
                    var pedido = (from c in data.tbPedidos where c.pedidoId == dadosPagamento.IdPedido select c).FirstOrDefault();
                    if (pedido != null)
                    {
                        var cliente = (from c in data.tbClientes where c.clienteId == pedido.clienteId select c).First();
                        var clearSale = new ClearSaleService.Service();
                        var clearOrder = new ClearSaleOrder.Order();
                        clearOrder.ID = dadosPagamento.IdPedido.ToString();
                        clearOrder.FingerPrint = new ClearSaleOrder.FingerPrint() { SessionID = pedido.pedidoKey };
                        clearOrder.Date = (pedido.dataHoraDoPedido ?? DateTime.Now).ToString("s");
                        clearOrder.Email = cliente.clienteEmail;
                        clearOrder.ShippingPrice = (pedido.valorDoFrete ?? 0).ToString("0.00").Replace(",", ".").Replace(",", ".");
                        clearOrder.TotalItems = (pedido.valorDosItens ?? 0).ToString("0.00").Replace(",", ".");
                        clearOrder.TotalOrder = (pedido.valorCobrado ?? 0).ToString("0.00").Replace(",", ".");
                        clearOrder.QtyInstallments = dadosPagamento.QuantidadeParcelas.ToString();
                        clearOrder.IP = pedido.ipDoCliente;
                        clearOrder.Origin = "Site"; // Confirmar
                        clearOrder.Reanalise = "1";
                        var dataNascimento = DateTime.Now;
                        if (!string.IsNullOrEmpty(cliente.clienteDataNascimento) && cliente.clienteDataNascimento != " ")
                        {
                            try
                            {
                                dataNascimento = Convert.ToDateTime(cliente.clienteDataNascimento);
                            }
                            catch (Exception ex2) { }
                        }

                        var phones = new ClearSaleOrder.Phones();
                        if (!string.IsNullOrEmpty(cliente.clienteFoneCelular))
                        {
                            if (cliente.clienteFoneCelular.Contains(")"))
                            {
                                phones.Phone = new ClearSaleOrder.Phone()
                                {

                                    DDD = cliente.clienteFoneCelular.Split(')')[0].Replace("(", "").Replace(" ", ""),
                                    Number = cliente.clienteFoneCelular.Split(')')[1].Replace(" ", "").Replace("-", ""),
                                    Type = "6"
                                };
                            }
                            else
                            {
                                phones.Phone = new ClearSaleOrder.Phone()
                                {

                                    DDD = cliente.clienteFoneCelular.StartsWith("0") ? cliente.clienteFoneCelular.Substring(0, 3) : cliente.clienteFoneCelular.Substring(0, 2),
                                    Number = cliente.clienteFoneCelular.StartsWith("0") ? cliente.clienteFoneCelular.Substring(3, cliente.clienteFoneCelular.Length - 3).Replace(" ", "").Replace("-", "") : cliente.clienteFoneCelular.Substring(2, cliente.clienteFoneCelular.Length - 2).Replace(" ", "").Replace("-", ""),
                                    Type = "6"
                                };
                            }
                        }
                        else if (!string.IsNullOrEmpty(cliente.clienteFoneResidencial))
                        {
                            if (cliente.clienteFoneResidencial.Contains(")"))
                            {
                                phones.Phone = new ClearSaleOrder.Phone()
                                {
                                    DDD = cliente.clienteFoneResidencial.Split(')')[0].Replace("(", "").Replace(" ", ""),
                                    Number = cliente.clienteFoneResidencial.Split(')')[1].Replace(" ", "").Replace("-", ""),
                                    Type = "1"
                                };
                            }
                            else
                            {
                                phones.Phone = new ClearSaleOrder.Phone()
                                {

                                    DDD = cliente.clienteFoneResidencial.StartsWith("0") ? cliente.clienteFoneResidencial.Substring(0, 3) : cliente.clienteFoneResidencial.Substring(0, 2),
                                    Number = cliente.clienteFoneResidencial.StartsWith("0") ? cliente.clienteFoneResidencial.Substring(3, cliente.clienteFoneResidencial.Length - 3).Replace(" ", "").Replace("-", "") : cliente.clienteFoneResidencial.Substring(2, cliente.clienteFoneResidencial.Length - 2).Replace(" ", "").Replace("-", ""),
                                    Type = "6"
                                };
                            }
                        }
                        clearOrder.BillingData = new ClearSaleOrder.BillingData
                        {
                            ID = cliente.clienteId.ToString(),
                            Type = "1",
                            LegalDocument1 = cliente.clienteCPFCNPJ,
                            LegalDocument2 = cliente.clienteRGIE,
                            Name = cliente.clienteNome,
                            BirthDate = dataNascimento.ToString("s"),
                            Email = cliente.clienteEmail,
                            Address = new ClearSaleOrder.Address()
                            {
                                Street = cliente.clienteRua,
                                Number = cliente.clienteNumero,
                                Comp = cliente.clienteComplemento,
                                County = cliente.clienteBairro,
                                City = cliente.clienteCidade,
                                State = cliente.clienteEstado,
                                ZipCode = cliente.clienteCep.Replace("-", "").Replace(" ", "").Replace(".", "")
                            },
                            Phones = phones
                        };
                        clearOrder.ShippingData = new ClearSaleOrder.ShippingData()
                        {
                            ID = cliente.clienteId.ToString(),
                            Type = "1",
                            LegalDocument1 = cliente.clienteCPFCNPJ,
                            LegalDocument2 = cliente.clienteRGIE,
                            Name = cliente.clienteNome,
                            BirthDate = dataNascimento.ToString("s"),
                            Email = cliente.clienteEmail,
                            Address = new ClearSaleOrder.Address()
                            {
                                Street = pedido.endRua,
                                Number = pedido.endNumero,
                                Comp = pedido.endComplemento,
                                County = pedido.endBairro,
                                City = pedido.endCidade,
                                State = pedido.endEstado,
                                ZipCode = pedido.endCep.Replace("-", "").Replace(" ", "").Replace(".", "")
                            },
                            Phones = phones
                        };

                        clearOrder.Payments = new ClearSaleOrder.Payments()
                        {
                            Payment = new ClearSaleOrder.Payment()
                            {
                                Date = (pedido.dataHoraDoPedido ?? DateTime.Now).ToString("s"),
                                Amount = dadosPagamento.Valor.ToString("0.00").Replace(",", "."),
                                PaymentTypeID = "1",
                                QtyInstallments = dadosPagamento.QuantidadeParcelas.ToString(),
                                CardNumber = dadosPagamento.NumeroCartao,
                                CardExpirationDate = dadosPagamento.ValidadeCartaoMes + "/" + dadosPagamento.ValidadeCartaoAno,
                                Name = dadosPagamento.NomeCartao

                            }
                        };

                        clearOrder.Items = new ClearSaleOrder.Items();
                        clearOrder.Items.Item = new List<ClearSaleOrder.Item>();
                        var itensPedido =
                            (from c in data.tbItensPedido where c.pedidoId == pedido.pedidoId select c).ToList();
                        foreach (var itemPedido in itensPedido)
                        {
                            clearOrder.Items.Item.Add(new ClearSaleOrder.Item()
                            {
                                ID = itemPedido.produtoId.ToString(),
                                Name = itemPedido.tbProdutos.produtoNome,
                                ItemValue = itemPedido.itemValor.ToString("0.00").Replace(",", "."),
                                Qty = Convert.ToInt32(itemPedido.itemQuantidade).ToString()
                            });
                        }

                        var orderString = "<ClearSale><Orders>" + clearOrder.ToString() + "</Orders></ClearSale>";
                        //var retorno = "<?xml version=\"1.0\" encoding=\"utf - 16\"?><PackageStatus><TransactionID>157f181c-7f26-46cc-8deb-505ae029e32d</TransactionID><StatusCode>00</StatusCode><Message>OK</Message><Orders><Order><ID>327382</ID><Status>AMA</Status><Score>7.2300</Score></Order></Orders></PackageStatus>";
                        var retorno = clearSale.SendOrders(entityCode, orderString);
                        ProcessaRetornoEnvio(retorno, dadosPagamento);
                    }
                }
            }
            catch (Exception ex)
            {
                var data = new dbCommerceEntities();
                var agora = DateTime.Now;
                var queue = new tbQueue();
                queue.agendamento = agora;
                queue.tipoQueue = 25;
                queue.mensagem = ex.InnerException.ToString();
                queue.concluido = false;
                queue.andamento = false;
                data.tbQueue.Add(queue);
                data.SaveChanges();
            }
        }

        public void EnviarAnaliseRisco(PagamentoPedidoDados dadosPagamento)
        {
            try
            {
                using (var data = new dbCommerceEntities())
                {
                    var pedido = (from c in data.tbPedidos where c.pedidoId == dadosPagamento.IdPedido select c).FirstOrDefault();
                    if (pedido != null)
                    {
                        var cliente = (from c in data.tbClientes where c.clienteId == pedido.clienteId select c).First();
                        var clearSale = new ClearSaleService.Service();
                        var clearOrder = new ClearSaleOrder.Order();
                        clearOrder.ID = dadosPagamento.IdPedido.ToString();
                        clearOrder.FingerPrint = new ClearSaleOrder.FingerPrint() {SessionID = pedido.pedidoKey};
                        clearOrder.Date = (pedido.dataHoraDoPedido ?? DateTime.Now).ToString("s");
                        clearOrder.Email = cliente.clienteEmail;
                        clearOrder.ShippingPrice = (pedido.valorDoFrete ?? 0).ToString("0.00").Replace(",", ".").Replace(",", ".");
                        clearOrder.TotalItems = (pedido.valorDosItens ?? 0).ToString("0.00").Replace(",", ".");
                        clearOrder.TotalOrder = (pedido.valorCobrado ?? 0).ToString("0.00").Replace(",", ".");
                        clearOrder.QtyInstallments = dadosPagamento.QuantidadeParcelas.ToString();
                        clearOrder.IP = pedido.ipDoCliente;
                        clearOrder.Origin = "Site"; // Confirmar

                        var dataNascimento = DateTime.Now;
                        if (!string.IsNullOrEmpty(cliente.clienteDataNascimento) && cliente.clienteDataNascimento != " ")
                        {
                            try
                            {
                                dataNascimento = Convert.ToDateTime(cliente.clienteDataNascimento);
                            }
                            catch(Exception ex2) { }
                        }

                        var phones = new ClearSaleOrder.Phones();
                        if (!string.IsNullOrEmpty(cliente.clienteFoneCelular))
                        {
                            if (cliente.clienteFoneCelular.Contains(")"))
                            {
                                phones.Phone = new ClearSaleOrder.Phone()
                                {
                            
                                    DDD = cliente.clienteFoneCelular.Split(')')[0].Replace("(", "").Replace(" ", ""),
                                    Number = cliente.clienteFoneCelular.Split(')')[1].Replace(" ", "").Replace("-", ""),
                                    Type = "6"
                                };
                            }
                            else
                            {
                                phones.Phone = new ClearSaleOrder.Phone()
                                {

                                    DDD = cliente.clienteFoneCelular.StartsWith("0") ? cliente.clienteFoneCelular.Substring(0, 3) : cliente.clienteFoneCelular.Substring(0, 2),
                                    Number = cliente.clienteFoneCelular.StartsWith("0") ? cliente.clienteFoneCelular.Substring(3, cliente.clienteFoneCelular.Length - 3).Replace(" ", "").Replace("-", "") : cliente.clienteFoneCelular.Substring(2, cliente.clienteFoneCelular.Length - 2).Replace(" ", "").Replace("-", ""),
                                    Type = "6"
                                };
                            }
                        }
                        else if (!string.IsNullOrEmpty(cliente.clienteFoneResidencial))
                        {
                            if (cliente.clienteFoneResidencial.Contains(")"))
                            {
                                phones.Phone = new ClearSaleOrder.Phone()
                                {
                                    DDD = cliente.clienteFoneResidencial.Split(')')[0].Replace("(", "").Replace(" ", ""),
                                    Number = cliente.clienteFoneResidencial.Split(')')[1].Replace(" ", "").Replace("-", ""),
                                    Type = "1"
                                };
                            }
                            else
                            {
                                phones.Phone = new ClearSaleOrder.Phone()
                                {

                                    DDD = cliente.clienteFoneResidencial.StartsWith("0") ? cliente.clienteFoneResidencial.Substring(0, 3) : cliente.clienteFoneResidencial.Substring(0, 2),
                                    Number = cliente.clienteFoneResidencial.StartsWith("0") ? cliente.clienteFoneResidencial.Substring(3, cliente.clienteFoneResidencial.Length - 3).Replace(" ", "").Replace("-", "") : cliente.clienteFoneResidencial.Substring(2, cliente.clienteFoneResidencial.Length - 2).Replace(" ", "").Replace("-", ""),
                                    Type = "6"
                                };
                            }
                        }
                        clearOrder.BillingData = new ClearSaleOrder.BillingData
                        {
                            ID = cliente.clienteId.ToString(),
                            Type = "1",
                            LegalDocument1 = cliente.clienteCPFCNPJ,
                            LegalDocument2 = cliente.clienteRGIE,
                            Name = cliente.clienteNome,
                            BirthDate = dataNascimento.ToString("s"),
                            Email = cliente.clienteEmail,
                            Address = new ClearSaleOrder.Address()
                            {
                                Street = cliente.clienteRua,
                                Number = cliente.clienteNumero,
                                Comp = cliente.clienteComplemento,
                                County = cliente.clienteBairro,
                                City = cliente.clienteCidade,
                                State = cliente.clienteEstado,
                                ZipCode = cliente.clienteCep.Replace("-","").Replace(" ", "").Replace(".", "")
                            },
                            Phones = phones
                        };
                        clearOrder.ShippingData = new ClearSaleOrder.ShippingData()
                        {
                            ID = cliente.clienteId.ToString(),
                            Type = "1",
                            LegalDocument1 = cliente.clienteCPFCNPJ,
                            LegalDocument2 = cliente.clienteRGIE,
                            Name = cliente.clienteNome,
                            BirthDate = dataNascimento.ToString("s"),
                            Email = cliente.clienteEmail,
                            Address = new ClearSaleOrder.Address()
                            {
                                Street = pedido.endRua,
                                Number = pedido.endNumero,
                                Comp = pedido.endComplemento,
                                County = pedido.endBairro,
                                City = pedido.endCidade,
                                State = pedido.endEstado,
                                ZipCode = pedido.endCep.Replace("-", "").Replace(" ", "").Replace(".", "")
                            },
                            Phones = phones
                        };

                        clearOrder.Payments = new ClearSaleOrder.Payments()
                        {
                            Payment = new ClearSaleOrder.Payment()
                            {
                                Date = (pedido.dataHoraDoPedido ?? DateTime.Now).ToString("s"),
                                Amount = dadosPagamento.Valor.ToString("0.00").Replace(",", "."),
                                PaymentTypeID = "1",
                                QtyInstallments = dadosPagamento.QuantidadeParcelas.ToString(),
                                CardNumber = dadosPagamento.NumeroCartao,
                                CardExpirationDate = dadosPagamento.ValidadeCartaoMes + "/" + dadosPagamento.ValidadeCartaoAno,
                                Name = dadosPagamento.NomeCartao

                            }
                        };

                        clearOrder.Items = new ClearSaleOrder.Items();
                        clearOrder.Items.Item = new List<ClearSaleOrder.Item>();
                        var itensPedido =
                            (from c in data.tbItensPedido where c.pedidoId == pedido.pedidoId select c).ToList();
                        foreach (var itemPedido in itensPedido)
                        {
                            clearOrder.Items.Item.Add(new ClearSaleOrder.Item()
                            {
                                ID = itemPedido.produtoId.ToString(),
                                Name = itemPedido.tbProdutos.produtoNome,
                                ItemValue = itemPedido.itemValor.ToString("0.00").Replace(",", "."),
                                Qty = Convert.ToInt32(itemPedido.itemQuantidade).ToString()
                            });
                        }

                        var orderString = "<ClearSale><Orders>" + clearOrder.ToString() + "</Orders></ClearSale>";
                        //var retorno = "<?xml version=\"1.0\" encoding=\"utf - 16\"?><PackageStatus><TransactionID>157f181c-7f26-46cc-8deb-505ae029e32d</TransactionID><StatusCode>00</StatusCode><Message>OK</Message><Orders><Order><ID>327382</ID><Status>AMA</Status><Score>7.2300</Score></Order></Orders></PackageStatus>";
                        var retorno = clearSale.SendOrders(entityCode, orderString);
                        ProcessaRetornoEnvio(retorno, dadosPagamento);
                    }
                }
            }
            catch (Exception ex)
            {
                var data = new dbCommerceEntities();
                var agora = DateTime.Now;
                var queue = new tbQueue();
                queue.agendamento = agora;
                queue.tipoQueue = 25;
                queue.mensagem = ex.InnerException.ToString();
                queue.concluido = false;
                queue.andamento = false;
                data.tbQueue.Add(queue);
                data.SaveChanges();
            }
        }
        private void ProcessaRetornoEnvio(string retorno, PagamentoPedidoDados dadosPagamento)
        {
            try
            { 
                XmlSerializer serializer = new XmlSerializer(typeof(ClearSalePackageStatus.PackageStatus));
                ClearSalePackageStatus.PackageStatus clearSalePackage;
                using (XmlReader reader = XmlReader.Create(new StringReader(retorno)))
                {
                    clearSalePackage = (ClearSalePackageStatus.PackageStatus)serializer.Deserialize(reader);
                    using (var data = new dbCommerceEntities())
                    {
                        var pagamentoRisco = new tbPedidoPagamentoRisco();
                        pagamentoRisco.idPedidoPagamento = dadosPagamento.IdPedidoPagamento;
                        pagamentoRisco.dataEnvio = DateTime.Now;
                        pagamentoRisco.statusInterno = Convert.ToInt32(clearSalePackage.StatusCode) == 0 ? 1 : 0; // 1 Se enviado, 0 se erro
                        pagamentoRisco.transactionId = clearSalePackage.TransactionID;
                        if (pagamentoRisco.statusInterno == 1)
                        {
                            pagamentoRisco.score = clearSalePackage.Orders.Order.FirstOrDefault().Score;
                            pagamentoRisco.statusGateway = clearSalePackage.Orders.Order.FirstOrDefault().Status;
                        }
                        else
                        {
                            pagamentoRisco.score = "";
                            pagamentoRisco.statusGateway = "";
                        }
                        pagamentoRisco.message = clearSalePackage.Message;
                        data.tbPedidoPagamentoRisco.Add(pagamentoRisco);
                        data.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                var data = new dbCommerceEntities();
                var agora = DateTime.Now;
                var queue = new tbQueue();
                queue.agendamento = agora;
                queue.tipoQueue = 25;
                queue.mensagem = ex.InnerException.ToString();
                queue.concluido = false;
                queue.andamento = false;
                data.tbQueue.Add(queue);
                data.SaveChanges();
            }
        }
        public List<ResultadoAnalise> ObterResultadosAnaliseRisco()
        {

            var clearSale = new ClearSaleService.Service();
            var retorno = clearSale.GetReturnAnalysis(entityCode);
            XmlSerializer serializer = new XmlSerializer(typeof(ClearSaleAnalysis.ClearSale));
            ClearSaleAnalysis.ClearSale clearSalePackage;
            using (XmlReader reader = XmlReader.Create(new StringReader(retorno)))
            {
                clearSalePackage = (ClearSaleAnalysis.ClearSale)serializer.Deserialize(reader);
            }

            var resultados = new List<ResultadoAnalise>();
            foreach (var order in clearSalePackage.Orders.Order)
            {
                resultados.Add(new ResultadoAnalise()
                {
                    PedidoId = Convert.ToInt32(order.ID),
                    Score = order.Score,
                    Status = order.Status
                });
            }

            return resultados;
        }
        public List<ResultadoAnalise> ObterResultadoAnaliseRisco(int pedidoId)
        {

            var clearSale = new ClearSaleService.Service();
            var retorno = clearSale.GetOrderStatus(entityCode, pedidoId.ToString());
            XmlSerializer serializer = new XmlSerializer(typeof(ClearSaleAnalysis.ClearSale));
            ClearSaleAnalysis.ClearSale clearSalePackage;
            using (XmlReader reader = XmlReader.Create(new StringReader(retorno)))
            {
                clearSalePackage = (ClearSaleAnalysis.ClearSale)serializer.Deserialize(reader);
            }

            var resultados = new List<ResultadoAnalise>();
            foreach (var order in clearSalePackage.Orders.Order)
            {
                resultados.Add(new ResultadoAnalise()
                {
                    PedidoId = Convert.ToInt32(order.ID),
                    Score = order.Score,
                    Status = order.Status
                });
            }

            return resultados;
        }
        public bool BaixarAnaliseProcessada(int pedidoId)
        {
            var clearSale = new ClearSaleService.Service();
            var retorno = clearSale.SetOrderAsReturned(entityCode, pedidoId.ToString());
            return true;
        }
        public bool AtualizaAnaliseProcessada(int pedidoId, string score, string status)
        {
            using (var data = new dbCommerceEntities())
            {
                var pagamentos = (from c in data.tbPedidoPagamento where c.pedidoId == pedidoId select c).ToList();
                foreach (var pagamento in pagamentos)
                {
                    var risco =
                        (from c in data.tbPedidoPagamentoRisco
                         where c.idPedidoPagamento == pagamento.idPedidoPagamento
                         select c).FirstOrDefault();
                    if (risco != null)
                    {
                        risco.score = score;
                        risco.statusGateway = status;

                        switch (status.ToLower())
                        {
                            case "apa":
                                risco.statusInterno = 2; //Aprovado
                                break;
                            case "apm":
                                risco.statusInterno = 2; //Aprovado
                                break;
                            case "rpm":
                                risco.statusInterno = 3; //Reprovado
                                break;
                            case "err":
                                risco.statusInterno = 3; //Reprovado
                                break;
                            case "sus":
                                risco.statusInterno = 3; //Reprovado
                                break;
                            case "can":
                                risco.statusInterno = 3; //Reprovado
                                break;
                            case "frd":
                                risco.statusInterno = 3; //Reprovado
                                break;
                            case "rpa":
                                risco.statusInterno = 3; //Reprovado
                                break;
                            case "rpp":
                                risco.statusInterno = 3; //Reprovado
                                break;
                        }
                        data.SaveChanges();
                    }
                }
            }
            return true;
        }


    }
}
