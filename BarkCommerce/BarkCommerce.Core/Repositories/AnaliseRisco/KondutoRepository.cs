﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using BarkCommerce.Core.DataAccess.SqlServer;
using BarkCommerce.Core.Interfaces.AnaliseRisco;
using BarkCommerce.Model;
using BarkCommerce.Model.AnaliseRisco;
using BarkCommerce.Model.Pagamento;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace BarkCommerce.Core.Repositories.AnaliseRisco
{
    public class KondutoRepository : IAnaliseRiscoRepository
    {
        private string entityCode = "3C557DF3-C307-4107-9BCD-E7BDC0EBBB56";

        #region Atualiza Analise Processada
        public bool AtualizaAnaliseProcessada(int pedidoId, string score, string status)
        {
            using (var data = new dbCommerceEntities())
            {
                var pagamentos = (from c in data.tbPedidoPagamento where c.pedidoId == pedidoId select c).ToList();
                foreach (var pagamento in pagamentos)
                {
                    var risco =
                        (from c in data.tbPedidoPagamentoRisco
                         where c.idPedidoPagamento == pagamento.idPedidoPagamento
                         select c).FirstOrDefault();
                    if (risco != null)
                    {
                        risco.score = score;
                        risco.statusGateway = status;

                        switch (status.ToLower())
                        {
                            case "apa":
                                risco.statusInterno = 2; //Aprovado
                                break;
                            case "apm":
                                risco.statusInterno = 2; //Aprovado
                                break;
                            case "rpm":
                                risco.statusInterno = 3; //Reprovado
                                break;
                            case "err":
                                risco.statusInterno = 3; //Reprovado
                                break;
                            case "sus":
                                risco.statusInterno = 3; //Reprovado
                                break;
                            case "can":
                                risco.statusInterno = 3; //Reprovado
                                break;
                            case "frd":
                                risco.statusInterno = 3; //Reprovado
                                break;
                            case "rpa":
                                risco.statusInterno = 3; //Reprovado
                                break;
                            case "rpp":
                                risco.statusInterno = 3; //Reprovado
                                break;
                        }
                        data.SaveChanges();
                    }
                }
            }
            return true;
        }
        #endregion

        #region Baixar Analise Processada
        public bool BaixarAnaliseProcessada(int pedidoId)
        {
            return true;
        }
        #endregion

        #region Enviar Analise Risco 
        public void EnviarAnaliseRisco(PagamentoPedidoDados dadosPagamento)
        {
            try
            {
                JObject ANALYZE_ORDER_RESPONSE;
                KondutoOrder ORDER_FROM_FILE;
                JObject NOT_ANALYZE_ORDER_RESPONSE;
                String ORDER_ID;

                var apiKey = "T738D516F09CAB3A2C1EE";

                Konduto konduto = new Konduto(apiKey);

                using (var data = new dbCommerceEntities())
                {
                    var pedido = (from c in data.tbPedidos where c.pedidoId == dadosPagamento.IdPedido select c).FirstOrDefault();
                    if (pedido != null)
                    {
                        var cliente = (from c in data.tbClientes where c.clienteId == pedido.clienteId select c).First();

                        #region Konduto Cliente
                        KondutoCustomer customer = new KondutoCustomer()
                        {
                            Id = cliente.clienteId.ToString(),
                            Name = cliente.clienteNome,
                            Email = cliente.clienteEmail,
                            TaxId = cliente.clienteCPFCNPJ,
                            Phone1 = cliente.clienteFoneCelular,
                            Phone2 = cliente.clienteFoneResidencial,
                            IsNew = false,
                            IsVip = false,
                            CreatedAt = cliente.dataDaCriacao.ToString("yyyy-MM-dd")
                        };
                        #endregion

                        #region Konduto Cartão de crédido
                        KondutoPayment payment = new KondutoCreditCardPayment()
                        {
                            Type = KondutoPaymentType.credit,
                            Status = KondutoCreditCardPaymentStatus.pending,
                            Bin = dadosPagamento.NumeroCartao.Substring(0, 4),
                            Last4 = dadosPagamento.NumeroCartao.Substring(12, 4),
                            ExpirationDate = "052021"// (dadosPagamento.ValidadeCartaoMes.ToString() + dadosPagamento.ValidadeCartaoAno.ToString())
                        };
                        #endregion

                        #region Konduto Endereço Faturamento
                        KondutoAddress billing = new KondutoAddress()
                        {
                            Name = cliente.clienteNome,
                            Address1 = cliente.clienteRua + cliente.clienteNumero,
                            Address2 = cliente.clienteComplemento,
                            City = cliente.clienteCidade,
                            State = cliente.clienteEstado,
                            Zip = cliente.clienteCep.Replace("-", ""),
                            Country = "BR"
                        };
                        #endregion

                        #region Konduto Endereço de Entrega
                        KondutoAddress shipping = new KondutoAddress
                        {
                            Name = cliente.clienteNome,
                            Address1 = cliente.clienteRua + cliente.clienteNumero,
                            Address2 = cliente.clienteComplemento,
                            City = cliente.clienteCidade,
                            State = cliente.clienteEstado,
                            Zip = cliente.clienteCep,
                            Country = "BR"
                        };
                        #endregion

                        #region Lista De Pagamentos
                        List<KondutoPayment> payments = new List<KondutoPayment>()
                        {
                            payment
                        };
                        #endregion

                        #region Konduto Itens Pedido
                        List<KondutoItem> ItensPedido = new List<KondutoItem>();

                        var itensPedido =
                            (from c in data.tbItensPedido where c.pedidoId == pedido.pedidoId select c).ToList();
                        foreach (var itemPedido in itensPedido)
                        {
                            ItensPedido.Add(new KondutoItem()
                            {
                                Sku = "",
                                ProductCode = itemPedido.produtoId.ToString(),
                                Category = 100,
                                Name = itemPedido.tbProdutos.produtoNome,
                                Description = itemPedido.tbProdutos.produtoDescricao,
                                UnitCost = Convert.ToDouble(itemPedido.itemValor.ToString("0.00").Replace(",", ".")),
                                Quantity = Convert.ToInt32(itemPedido.itemQuantidade),
                                CreatedAt = itemPedido.dataDaCriacao.ToString("yyyy-MM-dd"),
                                Discount = itemPedido.ValorDoDescontoDaCampanha.HasValue ? Convert.ToDouble(itemPedido.ValorDoDescontoDaCampanha.Value) : 0,
                            });
                        }

                        #endregion

                        #region Konduto Pedido
                        KondutoOrder order = new KondutoOrder
                        {
                            Id = pedido.pedidoId.ToString(),
                            Visitor = string.Empty,
                            TotalAmount = Convert.ToDouble(pedido.valorTotalGeral),
                            ShippingAmount = Convert.ToDouble(ItensPedido.Count),
                            TaxAmount = Convert.ToDouble(pedido.valorDoFrete),
                            Ip = pedido.ipDoCliente,
                            Currency = "BRL",
                            Customer = customer,
                            Payments = payments,
                            BillingAddress = billing,
                            ShippingAddress = shipping,
                            MessagesExchanged = 0,
                            PurchasedAt = pedido.dataHoraDoPedido.Value.ToString("yyyy-MM-dd") + "T" + pedido.dataHoraDoPedido.Value.ToString("HH:mm:ss") + "Z",
                            FirstMessage = string.Empty,
                            ShoppingCart = ItensPedido,
                            Analyze = true
                        };
                        #endregion

                        try
                        {
                            var retorno = konduto.Analyze(order);

                            if (retorno.Recommendation != KondutoRecommendation.none)
                            {
                                var pagamentoRisco = new tbPedidoPagamentoRisco();
                                pagamentoRisco.idPedidoPagamento = dadosPagamento.IdPedidoPagamento;
                                pagamentoRisco.dataEnvio = DateTime.Now;
                                pagamentoRisco.statusInterno = Convert.ToInt32(retorno.Analyze) == 0 ? 1 : 0; // 1 Se enviado, 0 se erro
                                pagamentoRisco.transactionId = retorno.Id.ToString();
                                pagamentoRisco.tipoServico = 2; //Konduto
                                if (pagamentoRisco.statusInterno == 1)
                                {
                                    pagamentoRisco.score = retorno.Score.ToString();
                                    pagamentoRisco.statusGateway = retorno.Recommendation.ToString();
                                }
                                else
                                {
                                    pagamentoRisco.score = "";
                                    pagamentoRisco.statusGateway = "";
                                }
                                pagamentoRisco.message = string.Empty;
                                data.tbPedidoPagamentoRisco.Add(pagamentoRisco);
                                data.SaveChanges();
                            }
                        }
                        catch (KondutoException ex)
                        {
                            throw ex;
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                var data = new dbCommerceEntities();
                var agora = DateTime.Now;
                var queue = new tbQueue();
                queue.agendamento = agora;
                queue.tipoQueue = 25;
                queue.mensagem = ex.Message.ToString();
                queue.concluido = false;
                queue.andamento = false;
                data.tbQueue.Add(queue);
                data.SaveChanges();
            }
        }

        #endregion

        #region Enviar Analise Risco Async
        public void EnviarAnaliseRiscoAsync(PagamentoPedidoDados dadosPagamento)
        {

        }
        #endregion

        #region Enviar Reanalise Risco 
        public void EnviarReAnaliseRisco(PagamentoPedidoDados dadosPagamento)
        {

        }
        #endregion

        #region Obter Resultado Analise Risco 
        public List<ResultadoAnalise> ObterResultadoAnaliseRisco(int pedidoId)
        {
            return new List<ResultadoAnalise>();
        }
        #endregion

        #region Obter Resultados Analise Risco
        public List<ResultadoAnalise> ObterResultadosAnaliseRisco()
        {
            return new List<ResultadoAnalise>();
        }
        #endregion
    }
}
