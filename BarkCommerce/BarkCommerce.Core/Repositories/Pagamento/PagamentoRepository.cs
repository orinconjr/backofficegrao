﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BarkCommerce.Core.DataAccess.SqlServer;
using BarkCommerce.Core.Interfaces.AnaliseRisco;
using BarkCommerce.Core.Interfaces.Boleto;
using BarkCommerce.Core.Interfaces.Gateway;
using BarkCommerce.Core.Interfaces.Pagamento;
using BarkCommerce.Model.Gateway;
using BarkCommerce.Model.Pagamento;

namespace BarkCommerce.Core.Repositories.Pagamento
{
    public class PagamentoRepository : IPagamentoRepository
    {
        private IAnaliseRiscoRepository _analiseRiscoRepository;

        public PagamentoRepository(IAnaliseRiscoRepository analiseRiscoRepository, IGatewayRepository gatewayRepository)
        {
            _analiseRiscoRepository = analiseRiscoRepository;
        }

        public PagamentoRepository(IAnaliseRiscoRepository analiseRiscoRepository)
        {
            _analiseRiscoRepository = analiseRiscoRepository;
        }

        public List<PagamentoPedido> ListaPagamentosPedido(int pedidoId)
        {
            using (var data = new dbCommerceEntities())
            {
                var pagamentosPedido = (from c in data.tbPedidoPagamento where c.pedidoId == pedidoId select new PagamentoPedido
                {
                    Cancelado = c.cancelado,
                    DataPagamento = c.dataPagamento,
                    DataVencimento = c.dataVencimento,
                    IdPedidoPagamento = c.idPedidoPagamento,
                    PagamentoNaoAutorizado = c.pagamentoNaoAutorizado,
                    Pago = c.pago,
                    Valor = c.valor,
                    PagamentoPedidoGateway = (from d in data.tbPedidoPagamentoGateway where c.idPedidoPagamento == d.idPedidoPagamento select new PagamentoPedidoGateway
                    {
                        AuthenticationUrl = d.authenticationUrl,
                        AuthorizationCode = d.authorizationCode,
                        AvsResponseCode = d.avsResponseCode,
                        BoletoUrl = d.boletoUrl,
                        CvvResponseCode = d.cvvResponseCode,
                        DataSolicitacao = d.dataSolicitacao,
                        DataSolicitacaoCaptura = d.dataSolicitacaoCaptura,
                        DataSolicitacaoEstorno = d.dataSolicitacaoEstorno,
                        ErrorMessage = d.errorMessage,
                        OnlineDebitUrl = d.onlineDebitUrl,
                        OrderId = d.orderId,
                        PagamentoPedidoGatewayId = d.idPedidoPagamentoGateway,
                        ProcessorMessage = d.processorMessage,
                        ProcessorReferenceNumber = d.processorReferenceNumber,
                        ProcessorReturnCode = d.processorReturnCode,
                        ProcessorTransactionId = d.transactionId,
                        ResponseCode = (d.responseCode ?? 0),
                        ResponseMessage = d.responseMessage,
                        SaveOnFile = d.saveOnFile,
                        TransactionId = d.transactionId,
                        TransactionTimestamp = d.transactionTimestamp,
                        VendaDireta = d.vendaDireta
                    }).FirstOrDefault(),
                    EnviarAnaliseRisco = (c.enviarAnaliseRisco ?? false),
                    Gateway = (c.gateway ?? false)
                }).ToList();
                return pagamentosPedido;
            }
        }

        public bool ProcessaPagamento(int idPedidoPagamento)
        {
            bool sucesso = true;
            using (var data = new dbCommerceEntities())
            {
                var pagamento = (from c in data.tbPedidoPagamento where c.idPedidoPagamento == idPedidoPagamento select c).FirstOrDefault();
                if (pagamento != null)
                {
                    var validaEnvioGateway = (from c in data.tbPedidoPagamentoGateway where c.idPedidoPagamento == idPedidoPagamento select c).FirstOrDefault();
                    if (validaEnvioGateway != null)
                    {
                        sucesso = false;
                    }
                    else
                    {
                        if (pagamento.pago == false && pagamento.cancelado == false && pagamento.pagamentoNaoAutorizado == false && (pagamento.gateway ?? false) == false && (pagamento.registrado ?? false) == false)
                        {
                            var configuracaoPagamento = RetornaConfiguracaoPagamento(pagamento.condicaoDePagamentoId, pagamento.pedidoId);
                            if (configuracaoPagamento.EnviarGateway)
                            {
                                bool enviarAnaliseRisco = configuracaoPagamento.EnviarAnaliseRisco;
                                if (configuracaoPagamento.ValorMinimoAnaliseRisco > pagamento.valor)
                                {
                                    enviarAnaliseRisco = false;
                                }
                                var validaAnaliseAprovada = (from c in data.tbPedidoPagamentoRisco where c.tbPedidoPagamento.pedidoId == pagamento.pedidoId && c.statusInterno == 2 select c).Any();
                                if(validaAnaliseAprovada)
                                {
                                    enviarAnaliseRisco = false;
                                }
                                pagamento.enviarAnaliseRisco = enviarAnaliseRisco;
                                var dadosPagamento = new PagamentoPedidoDados()
                                {
                                    CvvCartao = pagamento.codSegurancaCartao,
                                    IdPedidoPagamento = pagamento.idPedidoPagamento,
                                    NomeCartao = pagamento.nomeCartao,
                                    NumeroCartao = pagamento.numeroCartao.Replace(" ", "").Replace(".", ""),
                                    QuantidadeParcelas = pagamento.parcelas,
                                    ValidadeCartaoMes = pagamento.validadeCartao.Contains("/") ? pagamento.validadeCartao.Split('/')[0] : pagamento.validadeCartao,
                                    ValidadeCartaoAno = pagamento.validadeCartao.Contains("/") ? pagamento.validadeCartao.Split('/')[1] : pagamento.validadeCartao,
                                    Valor = pagamento.valor,
                                    ValorDesconto = (pagamento.valorDesconto ?? 0),
                                    ValorParcela = (pagamento.valorParcela ?? pagamento.valor),
                                    ValorTotal = (pagamento.valorTotal ?? pagamento.valor),
                                    Ip = pagamento.tbPedidos.ipDoCliente,
                                    AnaliseRisco = enviarAnaliseRisco,
                                    IdPedido = pagamento.pedidoId
                                };

                                IGatewayRepository gatewayRepository;
                                //if (pagamento.condicaoDePagamentoId == 22) gatewayRepository = new Gateway.PagseguroRepository();
                                if (configuracaoPagamento.GatewayContrato.GatewayTipo == 1) gatewayRepository = new Gateway.MaxipagoRepository();
                                else if (configuracaoPagamento.GatewayContrato.GatewayTipo == 2) gatewayRepository = new Gateway.MundipaggRepository();
                                //else if (configuracaoPagamento.GatewayContrato.GatewayTipo == 3 | pagamento.condicaoDePagamentoId == 22) gatewayRepository = new Gateway.PagseguroRepository();
                                else gatewayRepository = new Gateway.MundipaggRepository();
                                var pagamentoPedidoGatewayResponse = enviarAnaliseRisco
                                                                        ? gatewayRepository.SolicitarReservaCartaoCredito(configuracaoPagamento, dadosPagamento)
                                                                        : gatewayRepository.RealizarVendaDiretaCartaoCredito(configuracaoPagamento, dadosPagamento);
                                pagamento.gateway = true;
                                data.SaveChanges();
                                GravaPagamentoPedidoGateway(pagamentoPedidoGatewayResponse);
                                ProcessaRespostaGateway(pagamentoPedidoGatewayResponse, dadosPagamento);
                                ProcessaDados(dadosPagamento.IdPedidoPagamento);
                            }
                            else if (configuracaoPagamento.EnviarBanco)
                            {
                                IBoletoRepository boletoRepository = new Boleto.SantanderRepository();
                                boletoRepository.NotificarRegistroBoleto(idPedidoPagamento);
                            }
                        }
                    }
                }
                else
                {
                    sucesso = false;
                }
            }
            return sucesso;
        }

        public bool RegistraBoleto(int idPedidoPagamento, string cep)
        {
            try
            {
                IBoletoRepository boletoRepository = new Boleto.SantanderRepository();
                boletoRepository.RegistrarBoleto(idPedidoPagamento, cep);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private void ProcessaDados(int idPedidoPagamento)
        {
            using (var data = new dbCommerceEntities())
            {
                var pagamento = (from c in data.tbPedidoPagamento where c.idPedidoPagamento == idPedidoPagamento select c).FirstOrDefault();
                if(pagamento != null)
                {
                    string cartao = pagamento.numeroCartao;
                    var cartaoParcial = "";
                    for (int i = 0; i <= cartao.Length - 1; i++)
                    {
                        string digito = cartao[i].ToString();
                        if (i < 4 || (i > (cartao.Length - 6)))
                        {
                            cartaoParcial += digito;
                        }
                        else
                        {
                            cartaoParcial += "*";
                        }
                    }

                    pagamento.numeroCartao = cartaoParcial;
                    pagamento.codSegurancaCartao = "";
                    pagamento.validadeCartao = "";
                    data.SaveChanges();

                    var pedido = (from c in data.tbPedidos where c.pedidoId == pagamento.pedidoId select c).First();
                    pedido.numeroDoCartao = "";
                    pedido.numeroDoCartao2 = "";
                    pedido.codDeSegurancaDoCartao = "";
                    pedido.codDeSegurancaDoCartao2 = "";
                    pedido.validadeDoCartao = "";
                    pedido.validadeDoCartao2 = "";
                    data.SaveChanges();
                }

            }
        }
        public PagamentoConfiguracao RetornaConfiguracaoPagamento(int condicaoPagamentoId)
        {
            using (var data = new dbCommerceEntities())
            {
                var condicaoPagamento = (from c in data.tbCondicoesDePagamento
                    where c.condicaoId == condicaoPagamentoId
                    select new PagamentoConfiguracao
                    {
                        EnviarGateway = (c.enviarGateway ?? false),
                        EnviarBanco = (c.enviarBanco ?? false),
                        EnviarAnaliseRisco = (c.enviarAnaliseRisco ?? false),
                        ValorMinimoAnaliseRisco = (c.valorMinimoAnaliseRisco ?? 0),
                        IdGatewayContrato = (c.idGatewayContrato ?? 0),
                        IdCodigoAdquirenteGateway = (c.idCodigoAdquirenteGateway ?? 0),
                        IdBandeiraCartaoGateway = (c.idBandeiraCartaoGateway ?? 0),
                        GatewayContrato = (from d in data.tbGatewayContrato where d.idGatewayContrato == c.idGatewayContrato select new GatewayContrato
                        {
                            Nome = d.nome,
                            Cnpj = d.cnpj,
                            GatewayId = d.gatewayId,
                            GatewayKey = d.gatewayKey,
                            GatewayUser = d.gatewayUser,
                            GatewayPassword = d.gatewayPassword,
                            GatewayTipo = d.idGatewayTipo
                        }).FirstOrDefault()
                    }).First();
                return condicaoPagamento;
            }
        }

        public PagamentoConfiguracao RetornaConfiguracaoPagamento(int condicaoPagamentoId, int pedidoId)
        {
            using (var data = new dbCommerceEntities())
            {
                var condicaoPagamento = (from c in data.tbCondicoesDePagamento
                                         where c.condicaoId == condicaoPagamentoId
                                         select new PagamentoConfiguracao
                                         {
                                             EnviarGateway = (c.enviarGateway ?? false),
                                             EnviarBanco = (c.enviarBanco ?? false),
                                             EnviarAnaliseRisco = (c.enviarAnaliseRisco ?? false),
                                             ValorMinimoAnaliseRisco = (c.valorMinimoAnaliseRisco ?? 0),
                                             IdGatewayContrato = (c.idGatewayContrato ?? 0),
                                             IdCodigoAdquirenteGateway = (c.idCodigoAdquirenteGateway ?? 0),
                                             IdBandeiraCartaoGateway = (c.idBandeiraCartaoGateway ?? 0),
                                             GatewayContrato = (from d in data.tbGatewayContrato
                                                                where d.idGatewayContrato == c.idGatewayContrato
                                                                select new GatewayContrato
                                                                {
                                                                    Nome = d.nome,
                                                                    Cnpj = d.cnpj,
                                                                    GatewayId = d.gatewayId,
                                                                    GatewayKey = d.gatewayKey,
                                                                    GatewayUser = d.gatewayUser,
                                                                    GatewayPassword = d.gatewayPassword,
                                                                    GatewayTipo = d.idGatewayTipo
                                                                }).FirstOrDefault()
                                         }).First();
                return condicaoPagamento;
            }
        }


        private void GravaPagamentoPedidoGateway(PagamentoPedidoGateway pagamentoPedidoGateway)
        {
            using (var data = new dbCommerceEntities())
            {
                var pagamentoGateway = pagamentoPedidoGateway.PagamentoPedidoGatewayId > 0
                    ? (from c in data.tbPedidoPagamentoGateway
                        where c.idPedidoPagamentoGateway == pagamentoPedidoGateway.PagamentoPedidoGatewayId
                        select c).First()
                    : new tbPedidoPagamentoGateway();
                pagamentoGateway.idPedidoPagamento = pagamentoPedidoGateway.IdPedidoPagamento;
                pagamentoGateway.idGatewayContrato = pagamentoPedidoGateway.IdGatewayContrato;
                pagamentoGateway.vendaDireta = pagamentoPedidoGateway.VendaDireta;
                pagamentoGateway.authorizationCode = pagamentoPedidoGateway.AuthorizationCode;
                pagamentoGateway.orderId = pagamentoPedidoGateway.OrderId;
                pagamentoGateway.transactionId = pagamentoPedidoGateway.TransactionId;
                pagamentoGateway.transactionTimestamp = pagamentoPedidoGateway.TransactionTimestamp;
                pagamentoGateway.responseCode = pagamentoPedidoGateway.ResponseCode;
                pagamentoGateway.responseMessage = pagamentoPedidoGateway.ResponseMessage;
                pagamentoGateway.avsResponseCode = pagamentoPedidoGateway.AvsResponseCode;
                pagamentoGateway.cvvResponseCode = pagamentoPedidoGateway.CvvResponseCode;
                pagamentoGateway.processorReturnCode = pagamentoPedidoGateway.ProcessorReturnCode;
                pagamentoGateway.processorMessage = pagamentoPedidoGateway.ProcessorMessage;
                pagamentoGateway.processorReferenceNumber = pagamentoPedidoGateway.ProcessorReferenceNumber;
                pagamentoGateway.processorTransactionId = pagamentoPedidoGateway.ProcessorTransactionId;
                pagamentoGateway.errorMessage = pagamentoPedidoGateway.ErrorMessage;
                pagamentoGateway.boletoUrl = pagamentoPedidoGateway.BoletoUrl;
                pagamentoGateway.authenticationUrl = pagamentoPedidoGateway.AuthenticationUrl;
                pagamentoGateway.saveOnFile = pagamentoPedidoGateway.SaveOnFile;
                pagamentoGateway.onlineDebitUrl = pagamentoPedidoGateway.OnlineDebitUrl;
                pagamentoGateway.dataSolicitacao = pagamentoPedidoGateway.DataSolicitacao;
                pagamentoGateway.dataSolicitacaoCaptura = pagamentoPedidoGateway.DataSolicitacaoCaptura;
                pagamentoGateway.dataSolicitacaoEstorno = pagamentoPedidoGateway.DataSolicitacaoEstorno;
                if (pagamentoPedidoGateway.PagamentoPedidoGatewayId == 0) data.tbPedidoPagamentoGateway.Add(pagamentoGateway);
                data.SaveChanges();
            }
        }



        private void ProcessaRespostaGateway(PagamentoPedidoGateway pagamentoPedidoGateway, PagamentoPedidoDados dadosPagamento)
        {
            switch (pagamentoPedidoGateway.ResponseCode)
            {
                case 0:
                    if (pagamentoPedidoGateway.ResponseMessage.ToLower().Contains("authorized"))
                    {
                        if (dadosPagamento.AnaliseRisco) _analiseRiscoRepository.EnviarAnaliseRiscoAsync(dadosPagamento);
                        else CapturaPagamento(dadosPagamento.IdPedidoPagamento);
                    }
                    else if (pagamentoPedidoGateway.ResponseMessage.ToLower().Contains("captured"))
                    {
                        AutorizarPagamento(dadosPagamento.IdPedidoPagamento);
                    }
                    else if (pagamentoPedidoGateway.ResponseMessage.ToLower().Contains("witherror"))
                    {
                        NaoAutorizarPagamento(dadosPagamento.IdPedidoPagamento);
                    }
                    break;
                case 200:
                    if (pagamentoPedidoGateway.ResponseMessage.ToLower().Contains("authorized"))
                    {
                        if (dadosPagamento.AnaliseRisco) _analiseRiscoRepository.EnviarAnaliseRiscoAsync(dadosPagamento);
                        else CapturaPagamento(dadosPagamento.IdPedidoPagamento);
                    }
                    else if (pagamentoPedidoGateway.ResponseMessage.ToLower().Contains("captured"))
                    {
                        AutorizarPagamento(dadosPagamento.IdPedidoPagamento);
                    }
                    break;
                case 1:
                    NaoAutorizarPagamento(dadosPagamento.IdPedidoPagamento);
                    break;
                case 2:
                    NaoAutorizarPagamento(dadosPagamento.IdPedidoPagamento);
                    break;
                case 1024:
                    NaoAutorizarPagamento(dadosPagamento.IdPedidoPagamento);
                    break;
                case 2048:
                    NaoAutorizarPagamento(dadosPagamento.IdPedidoPagamento);
                    break;
                default:
                    NaoAutorizarPagamento(dadosPagamento.IdPedidoPagamento);
                    break;
            }
            
        }

        public bool CapturaPagamento(int idPedidoPagamento)
        {
            bool sucesso = true;
            using (var data = new dbCommerceEntities())
            {
                var pagamento = (from c in data.tbPedidoPagamento where c.idPedidoPagamento == idPedidoPagamento select c).FirstOrDefault();



                if (pagamento != null)
                {
                    var envioGateway = RetornaPagamentoPedidoGateway(idPedidoPagamento);

                    if (envioGateway == null)
                    {
                        sucesso = false;
                    }
                    else
                    {
                        var contratoGateway = RetornaContratoGateway(envioGateway.IdGatewayContrato);
                        if (pagamento.pago == false && contratoGateway != null)
                        {
                            var dadosPagamento = new PagamentoPedidoDados()
                            {
                                CvvCartao = pagamento.codSegurancaCartao,
                                IdPedidoPagamento = pagamento.idPedidoPagamento,
                                NomeCartao = pagamento.nomeCartao,
                                NumeroCartao = pagamento.numeroCartao.Replace(" ", "").Replace(".", ""),
                                QuantidadeParcelas = pagamento.parcelas,
                                ValidadeCartaoMes = pagamento.validadeCartao.Contains("/") ? pagamento.validadeCartao.Split('/')[0] : pagamento.validadeCartao,
                                ValidadeCartaoAno = pagamento.validadeCartao.Contains("/") ? pagamento.validadeCartao.Split('/')[1] : pagamento.validadeCartao,
                                Valor = pagamento.valor,
                                ValorDesconto = (pagamento.valorDesconto ?? 0),
                                ValorParcela = (pagamento.valorParcela ?? pagamento.valor),
                                ValorTotal = (pagamento.valorTotal ?? pagamento.valor),
                                Ip = pagamento.tbPedidos.ipDoCliente,
                                AnaliseRisco = (pagamento.enviarAnaliseRisco ?? false),
                                IdPedido = pagamento.pedidoId
                            };

                            IGatewayRepository gatewayRepository;
                            if (contratoGateway.GatewayTipo == 1) gatewayRepository = new Gateway.MaxipagoRepository();
                            else if (contratoGateway.GatewayTipo == 2) gatewayRepository = new Gateway.MundipaggRepository();
                            else gatewayRepository = new Gateway.MundipaggRepository();
                            var pagamentoPedidoGatewayResponse = gatewayRepository.CapturarPagamentoCartaoCredito(envioGateway, dadosPagamento, contratoGateway);

                            if(pagamentoPedidoGatewayResponse == null)
                            {

                                envioGateway = RetornaPagamentoPedidoGateway(idPedidoPagamento, false);
                                contratoGateway = RetornaContratoGateway(envioGateway.IdGatewayContrato);
                                dadosPagamento = new PagamentoPedidoDados()
                                {
                                    CvvCartao = pagamento.codSegurancaCartao,
                                    IdPedidoPagamento = pagamento.idPedidoPagamento,
                                    NomeCartao = pagamento.nomeCartao,
                                    NumeroCartao = pagamento.numeroCartao.Replace(" ", "").Replace(".", ""),
                                    QuantidadeParcelas = pagamento.parcelas,
                                    ValidadeCartaoMes = pagamento.validadeCartao.Contains("/") ? pagamento.validadeCartao.Split('/')[0] : pagamento.validadeCartao,
                                    ValidadeCartaoAno = pagamento.validadeCartao.Contains("/") ? pagamento.validadeCartao.Split('/')[1] : pagamento.validadeCartao,
                                    Valor = pagamento.valor,
                                    ValorDesconto = (pagamento.valorDesconto ?? 0),
                                    ValorParcela = (pagamento.valorParcela ?? pagamento.valor),
                                    ValorTotal = (pagamento.valorTotal ?? pagamento.valor),
                                    Ip = pagamento.tbPedidos.ipDoCliente,
                                    AnaliseRisco = (pagamento.enviarAnaliseRisco ?? false),
                                    IdPedido = pagamento.pedidoId
                                };
                                pagamentoPedidoGatewayResponse = gatewayRepository.CapturarPagamentoCartaoCredito(envioGateway, dadosPagamento, contratoGateway);
                            }
                            GravaPagamentoPedidoGateway(pagamentoPedidoGatewayResponse);
                            ProcessaRespostaGateway(pagamentoPedidoGatewayResponse, dadosPagamento);
                            pagamentoPedidoGatewayResponse = gatewayRepository.CapturarPagamentoCartaoCredito(envioGateway, dadosPagamento, contratoGateway);
                        }
                    }
                }
                else
                {
                    sucesso = false;
                }
            }
            return sucesso;
        }

        public bool EstornarPagamento(int idPedidoPagamento)
        {
            bool sucesso = true;
            using (var data = new dbCommerceEntities())
            {
                var pagamento = (from c in data.tbPedidoPagamento where c.idPedidoPagamento == idPedidoPagamento select c).FirstOrDefault();
                if (pagamento != null)
                {
                    var envioGateway = RetornaPagamentoPedidoGateway(idPedidoPagamento);

                    if (envioGateway == null)
                    {
                        sucesso = false;
                    }
                    else
                    {
                        var contratoGateway = RetornaContratoGateway(envioGateway.IdGatewayContrato);
                        if (pagamento.pago == false && contratoGateway != null)
                        {
                            var dadosPagamento = new PagamentoPedidoDados()
                            {
                                CvvCartao = pagamento.codSegurancaCartao,
                                IdPedidoPagamento = pagamento.idPedidoPagamento,
                                NomeCartao = pagamento.nomeCartao,
                                NumeroCartao = pagamento.numeroCartao.Replace(" ", "").Replace(".", ""),
                                QuantidadeParcelas = pagamento.parcelas,
                                ValidadeCartaoMes = pagamento.validadeCartao.Contains("/") ? pagamento.validadeCartao.Split('/')[0] : pagamento.validadeCartao,
                                ValidadeCartaoAno = pagamento.validadeCartao.Contains("/") ? pagamento.validadeCartao.Split('/')[1] : pagamento.validadeCartao,
                                Valor = pagamento.valor,
                                ValorDesconto = (pagamento.valorDesconto ?? 0),
                                ValorParcela = (pagamento.valorParcela ?? pagamento.valor),
                                ValorTotal = (pagamento.valorTotal ?? pagamento.valor),
                                Ip = pagamento.tbPedidos.ipDoCliente,
                                AnaliseRisco = (pagamento.enviarAnaliseRisco ?? false),
                                IdPedido = pagamento.pedidoId
                            };

                            IGatewayRepository gatewayRepository;
                            if (contratoGateway.GatewayTipo == 1) gatewayRepository = new Gateway.MaxipagoRepository();
                            else if (contratoGateway.GatewayTipo == 2) gatewayRepository = new Gateway.MundipaggRepository();
                            else gatewayRepository = new Gateway.MundipaggRepository();
                            var pagamentoPedidoGatewayResponse = gatewayRepository.EstornarReservaCartaoCredito(envioGateway, dadosPagamento, contratoGateway);
                            GravaPagamentoPedidoGateway(pagamentoPedidoGatewayResponse);
                        }
                    }
                }
                else
                {
                    sucesso = false;
                }
            }
            return sucesso;
        }

        private void AutorizarPagamento(int idPedidoPagamento)
        {
            using (var data = new dbCommerceEntities())
            {
                var pagamento = RetornaDetalhesPagamento(idPedidoPagamento);
                var queueUpdate = new tbQueue();
                queueUpdate.agendamento = DateTime.Now.AddMinutes(1);
                queueUpdate.andamento = false;
                queueUpdate.concluido = false;
                queueUpdate.idRelacionado = pagamento.IdPedido;
                queueUpdate.mensagem = "";
                queueUpdate.tipoQueue = 12;
                data.tbQueue.Add(queueUpdate);
                data.SaveChanges();
            }
        }
        private void NaoAutorizarPagamento(int idPedidoPagamento)
        {
            using (var data = new dbCommerceEntities())
            {
                var pagamento = (from c in data.tbPedidoPagamento where c.idPedidoPagamento == idPedidoPagamento select c).FirstOrDefault();
                if (pagamento != null)
                {
                    var pedido = (from c in data.tbPedidos where c.pedidoId == pagamento.pedidoId select c).First();
                    pedido.statusDoPedido = 7;
                    pagamento.pagamentoNaoAutorizado = true;
                    pagamento.observacao = "";
                    data.SaveChanges();
                    //TODO: Adicionar interação
                }
            }
        }
        public PagamentoPedidoGateway RetornaPagamentoPedidoGateway(int idPedidoPagamento)
        {
            return RetornaPagamentoPedidoGateway(idPedidoPagamento, true);
        }
        public PagamentoPedidoGateway RetornaPagamentoPedidoGateway(int idPedidoPagamento, bool verificarGateway)
        {
            using (var data = new dbCommerceEntities())
            {
                var pagamentoGateway = (from c in data.tbPedidoPagamentoGateway
                                        where c.idPedidoPagamento == idPedidoPagamento
                                        select c).FirstOrDefault();

                var validaCd = (from c in data.tbItensPedido where c.pedidoId == pagamentoGateway.tbPedidoPagamento.pedidoId && c.tbProdutos.idCentroDistribuicao < 4 select c).Any();
                if (!validaCd && verificarGateway)
                {
                    pagamentoGateway.idGatewayContrato = 6;
                }

                var gateway = new PagamentoPedidoGateway
                               {
                                   IdPedidoPagamento = pagamentoGateway.idPedidoPagamento,
                                   DataSolicitacaoCaptura = pagamentoGateway.dataSolicitacaoCaptura,
                                   AvsResponseCode = pagamentoGateway.avsResponseCode,
                                   DataSolicitacao = pagamentoGateway.dataSolicitacao,
                                   DataSolicitacaoEstorno = pagamentoGateway.dataSolicitacaoEstorno,
                                   CvvResponseCode = pagamentoGateway.cvvResponseCode,
                                   AuthorizationCode = pagamentoGateway.authorizationCode,
                                   ErrorMessage = pagamentoGateway.errorMessage,
                                   BoletoUrl = pagamentoGateway.boletoUrl,
                                   AuthenticationUrl = pagamentoGateway.authenticationUrl,
                                   IdGatewayContrato = (pagamentoGateway.idGatewayContrato ?? 0),
                                   OnlineDebitUrl = pagamentoGateway.onlineDebitUrl,
                                   TransactionId = pagamentoGateway.transactionId,
                                   OrderId = pagamentoGateway.orderId,
                                   ProcessorMessage = pagamentoGateway.processorMessage,
                                   ResponseMessage = pagamentoGateway.responseMessage,
                                   ResponseCode = (pagamentoGateway.responseCode ?? -1),
                                   VendaDireta = pagamentoGateway.vendaDireta,
                                   SaveOnFile = pagamentoGateway.saveOnFile,
                                   ProcessorTransactionId = pagamentoGateway.processorTransactionId,
                                   ProcessorReturnCode = pagamentoGateway.processorReturnCode,
                                   ProcessorReferenceNumber = pagamentoGateway.processorReferenceNumber,
                                   TransactionTimestamp = pagamentoGateway.transactionTimestamp,
                                   PagamentoPedidoGatewayId = pagamentoGateway.idPedidoPagamentoGateway
                               };
                return gateway;

            }
        }
        public GatewayContrato RetornaContratoGateway(int idGatewayContrato)
        {
            using (var data = new dbCommerceEntities())
            {
                return (from c in data.tbGatewayContrato where c.idGatewayContrato == idGatewayContrato select new GatewayContrato
                {
                    GatewayKey = c.gatewayKey,
                    GatewayPassword = c.gatewayPassword,
                    GatewayId = c.gatewayId,
                    GatewayUser = c.gatewayUser,
                    Nome = c.nome,
                    Cnpj = c.cnpj,
                    GatewayTipo = c.idGatewayTipo
                }).FirstOrDefault();
            }
        }


        public PagamentoPedidoDados RetornaDetalhesPagamento(int idPedidoPagamento)
        {
            using (var data = new dbCommerceEntities())
            {
                var pagamento =
                    (from c in data.tbPedidoPagamento where c.idPedidoPagamento == idPedidoPagamento select c)
                        .FirstOrDefault();
                if (pagamento != null)
                {
                    var dadosPagamento = new PagamentoPedidoDados()
                    {
                        CvvCartao = pagamento.codSegurancaCartao,
                        IdPedidoPagamento = pagamento.idPedidoPagamento,
                        NomeCartao = pagamento.nomeCartao,
                        NumeroCartao = pagamento.numeroCartao.Replace(" ", "").Replace(".", ""),
                        QuantidadeParcelas = pagamento.parcelas,
                        ValidadeCartaoMes =
                            pagamento.validadeCartao.Contains("/")
                                ? pagamento.validadeCartao.Split('/')[0]
                                : pagamento.validadeCartao,
                        ValidadeCartaoAno =
                            pagamento.validadeCartao.Contains("/")
                                ? pagamento.validadeCartao.Split('/')[1]
                                : pagamento.validadeCartao,
                        Valor = pagamento.valor,
                        ValorDesconto = (pagamento.valorDesconto ?? 0),
                        ValorParcela = (pagamento.valorParcela ?? pagamento.valor),
                        ValorTotal = (pagamento.valorTotal ?? pagamento.valor),
                        Ip = pagamento.tbPedidos.ipDoCliente,
                        AnaliseRisco = (pagamento.enviarAnaliseRisco ?? false),
                        IdPedido = pagamento.pedidoId
                    };

                    return dadosPagamento;
                    
                }
            }

            return null;
        }
    }
}
