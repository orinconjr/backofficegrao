﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BarkCommerce.Core.Interfaces.AnaliseRisco;
using BarkCommerce.Core.Interfaces.Pagamento;

namespace BarkCommerce.Business.AnaliseRisco
{

    public class EnviarAnaliseRisco
    {
        private readonly IAnaliseRiscoRepository _riscoRepository;
        private readonly IPagamentoRepository _pagamentoRepository;

        public EnviarAnaliseRisco(IAnaliseRiscoRepository riscoRepository, IPagamentoRepository pagamentoRepository)
        {
            _riscoRepository = riscoRepository;
            _pagamentoRepository = pagamentoRepository;
        }

        public EnviarAnaliseRiscoResponse Enviar(EnviarAnaliseRiscoRequest request)
        {
            var response = new EnviarAnaliseRiscoResponse();
            response.Enviado = true;

            try
            {
                var pagamento = _pagamentoRepository.RetornaDetalhesPagamento(request.IdPedidoPagamento);
                _riscoRepository.EnviarAnaliseRisco(pagamento);
            }
            catch (Exception ex)
            {
                response.Enviado = false;
            }

            return response;
        }
    }
    public class EnviarAnaliseRiscoRequest
    {
        public int IdPedidoPagamento { get; set; }
    }
    public class EnviarAnaliseRiscoResponse
    {
        public bool Enviado { get; set; }
    }
}
