﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BarkCommerce.Core.Interfaces.AnaliseRisco;
using BarkCommerce.Core.Interfaces.Gateway;
using BarkCommerce.Core.Interfaces.Pagamento;
using BarkCommerce.Core.Repositories.AnaliseRisco;
using BarkCommerce.Core.Repositories.Gateway;
using BarkCommerce.Core.Repositories.Pagamento;

namespace BarkCommerce.Business.Pagamento
{
    public class ProcessarPagamentosPedido
    {
        private readonly IPagamentoRepository _pagamentoRepository;

        public ProcessarPagamentosPedido(IPagamentoRepository pagamentoRepository)
        {
            _pagamentoRepository = pagamentoRepository;
        }

        public ProcessarPagamentosPedidoResponse ProcessarPagamentos(ProcessarPagamentosPedidoRequest request)
        {
            var response = new ProcessarPagamentosPedidoResponse();
            var listaPagamentosPendentes = _pagamentoRepository.ListaPagamentosPedido(request.PedidoId).Where(x => x.Pago == false && x.Cancelado == false && x.PagamentoNaoAutorizado == false && x.Gateway == false);
            foreach (var pagamento in listaPagamentosPendentes)
            {
                _pagamentoRepository.ProcessaPagamento(pagamento.IdPedidoPagamento);
            }
            var listaPagamentosProcessados = _pagamentoRepository.ListaPagamentosPedido(request.PedidoId);
            response.PedidoId = request.PedidoId;
            response.PagamentosAutorizados = listaPagamentosProcessados.Count(x => x.Pago);
            response.PagamentosNaoAutorizados = listaPagamentosProcessados.Count(x => x.PagamentoNaoAutorizado);
            response.PagamentosPendentes = listaPagamentosProcessados.Count(x => x.Pago == false && x.Cancelado == false && x.PagamentoNaoAutorizado == false);
            return response;
        }
    }

    public class ProcessarPagamentosPedidoRequest
    {
        public int PedidoId { get; set; }
    }
    public class ProcessarPagamentosPedidoResponse
    {
        public int PedidoId { get; set; }
        public int PagamentosPendentes { get; set; }
        public int PagamentosAutorizados { get; set; }
        public int PagamentosNaoAutorizados { get; set; }
    }
}
