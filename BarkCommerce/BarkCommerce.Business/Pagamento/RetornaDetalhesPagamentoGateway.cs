﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BarkCommerce.Core.Interfaces.Pagamento;
using BarkCommerce.Model.Gateway;

namespace BarkCommerce.Business.Pagamento
{
    public class RetornaDetalhesPagamentoGateway
    {
        private readonly IPagamentoRepository _pagamentoRepository;

        public RetornaDetalhesPagamentoGateway(IPagamentoRepository pagamentoRepository)
        {
            _pagamentoRepository = pagamentoRepository;
        }

        public PagamentoPedidoGateway RetornaPagamentoPedido(int idPedidoPagamento)
        {
            return _pagamentoRepository.RetornaPagamentoPedidoGateway(idPedidoPagamento);
        }
    }
}
