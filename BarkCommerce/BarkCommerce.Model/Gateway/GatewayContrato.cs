﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BarkCommerce.Model.Gateway
{
    public class GatewayContrato
    {
        public string Nome { get; set; }
        public string Cnpj { get; set; }
        public string GatewayId { get; set; }
        public string GatewayKey { get; set; }
        public string GatewayUser { get; set; }
        public string GatewayPassword { get; set; }
        public int GatewayTipo { get; set; }
    }
}
