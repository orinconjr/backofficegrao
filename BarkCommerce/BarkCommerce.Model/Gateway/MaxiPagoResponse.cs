﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BarkCommerce.Model.Gateway
{
    public class MaxiPagoResponse
    {
        public MaxiPagoResponse() { }

        public string AuthorizationCode { get; set; }
        public string OrderId { get; set; }
        public string TransactionId { get; set; }
        public string TransactionTimestamp { get; set; }
        public int ResponseCode { get; set; }
        public string ResponseMessage { get; set; }
        public string AvsResponseCode { get; set; }
        public string CvvResponseCode { get; set; }
        public string ProcessorReturnCode { get; set; }
        public string ProcessorMessage { get; set; }
        public string ProcessorReferenceNumber { get; set; }
        public string ProcessorTransactionID { get; set; }
        public string ErrorMessage { get; set; }
        public string BoletoUrl { get; set; }
        public string AuthenticationUrl { get; set; }
        public string SaveOnFile { get; set; }
        public string OnlineDebitUrl { get; set; }


        public override string ToString()
        {
            return string.Format("{ AuthorizationCode: {0} - OrderId: {1} - TransactionId: {2} - ResponseCode: {3} - ResponseMessage: {4} - ErrorMessage: {5}",
                AuthorizationCode,
                OrderId,
                TransactionId,
                ResponseCode,
                ResponseMessage,
                ErrorMessage
            );
        }
    }
}
