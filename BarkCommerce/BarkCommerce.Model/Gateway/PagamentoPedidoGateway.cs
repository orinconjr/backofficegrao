﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BarkCommerce.Model.Gateway
{
    public class PagamentoPedidoGateway
    {
        public int PagamentoPedidoGatewayId { get; set; }
        public int IdGatewayContrato { get; set; }
        public int IdPedidoPagamento { get; set; }
        public bool VendaDireta { get; set; }
        public string AuthorizationCode { get; set; }
        public string OrderId { get; set; }
        public string TransactionId { get; set; }
        public string TransactionTimestamp { get; set; }
        public int ResponseCode { get; set; }
        public string ResponseMessage { get; set; }
        public string AvsResponseCode { get; set; }
        public string CvvResponseCode { get; set; }
        public string ProcessorReturnCode { get; set; }
        public string ProcessorMessage { get; set; }
        public string ProcessorReferenceNumber { get; set; }
        public string ProcessorTransactionId { get; set; }
        public string ErrorMessage { get; set; }
        public string BoletoUrl { get; set; }
        public string AuthenticationUrl { get; set; }
        public string SaveOnFile { get; set; }
        public string OnlineDebitUrl { get; set; }
        public DateTime DataSolicitacao { get; set; }
        public DateTime? DataSolicitacaoCaptura { get; set; }
        public DateTime? DataSolicitacaoEstorno { get; set; }
    }
}
