﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BarkCommerce.Model.Gateway;

namespace BarkCommerce.Model.Pagamento
{
    public class PagamentoConfiguracao
    {
        public bool EnviarGateway { get; set; }
        public bool EnviarBanco { get; set; }
        public bool EnviarAnaliseRisco { get; set; }
        public decimal ValorMinimoAnaliseRisco { get; set; }
        public int IdGatewayContrato { get; set; }
        public int IdCodigoAdquirenteGateway { get; set; }
        public int IdBandeiraCartaoGateway { get; set; }
        public GatewayContrato GatewayContrato { get; set; }
    }
}
