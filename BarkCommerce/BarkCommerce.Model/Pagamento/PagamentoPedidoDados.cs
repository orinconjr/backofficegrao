﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BarkCommerce.Model.Pagamento
{
    public class PagamentoPedidoDados
    {
        public int IdPedidoPagamento { get; set; }
        public int IdPedido { get; set; }
        public decimal Valor { get; set; }
        public decimal ValorTotal { get; set; }
        public decimal ValorDesconto{ get; set; }
        public int QuantidadeParcelas{ get; set; }
        public decimal ValorParcela{ get; set; }
        public string NomeCartao { get; set; }
        public string NumeroCartao { get; set; }
        public string ValidadeCartaoMes { get; set; }
        public string ValidadeCartaoAno { get; set; }
        public string CvvCartao { get; set; }
        public string Ip { get; set; }
        public bool AnaliseRisco { get; set; }
    }
}
