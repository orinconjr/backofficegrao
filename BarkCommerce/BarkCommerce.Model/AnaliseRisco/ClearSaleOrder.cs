﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace BarkCommerce.Model.AnaliseRisco
{
    public class ClearSaleOrder
    {
        

        [XmlRoot(ElementName = "FingerPrint")]
        public class FingerPrint
        {
            [XmlElement(ElementName = "SessionID")]
            public string SessionID { get; set; }
        }

        [XmlRoot(ElementName = "Address")]
        public class Address
        {
            [XmlElement(ElementName = "Street")]
            public string Street { get; set; }
            [XmlElement(ElementName = "Number")]
            public string Number { get; set; }
            [XmlElement(ElementName = "Comp")]
            public string Comp { get; set; }
            [XmlElement(ElementName = "County")]
            public string County { get; set; }
            [XmlElement(ElementName = "City")]
            public string City { get; set; }
            [XmlElement(ElementName = "State")]
            public string State { get; set; }
            [XmlElement(ElementName = "Country")]
            public string Country { get; set; }
            [XmlElement(ElementName = "ZipCode")]
            public string ZipCode { get; set; }
            [XmlElement(ElementName = "Reference")]
            public string Reference { get; set; }
        }

        [XmlRoot(ElementName = "Phone")]
        public class Phone
        {
            [XmlElement(ElementName = "Type")]
            public string Type { get; set; }
            [XmlElement(ElementName = "DDI")]
            public string DDI { get; set; }
            [XmlElement(ElementName = "DDD")]
            public string DDD { get; set; }
            [XmlElement(ElementName = "Number")]
            public string Number { get; set; }
            [XmlElement(ElementName = "Extension")]
            public string Extension { get; set; }
        }

        [XmlRoot(ElementName = "Phones")]
        public class Phones
        {
            [XmlElement(ElementName = "Phone")]
            public Phone Phone { get; set; }
        }

        [XmlRoot(ElementName = "BillingData")]
        public class BillingData
        {
            [XmlElement(ElementName = "ID")]
            public string ID { get; set; }
            [XmlElement(ElementName = "Type")]
            public string Type { get; set; }
            [XmlElement(ElementName = "LegalDocument1")]
            public string LegalDocument1 { get; set; }
            [XmlElement(ElementName = "LegalDocument2")]
            public string LegalDocument2 { get; set; }
            [XmlElement(ElementName = "Name")]
            public string Name { get; set; }
            [XmlElement(ElementName = "BirthDate")]
            public string BirthDate { get; set; }
            [XmlElement(ElementName = "Email")]
            public string Email { get; set; }
            [XmlElement(ElementName = "Gender")]
            public string Gender { get; set; }
            [XmlElement(ElementName = "Address")]
            public Address Address { get; set; }
            [XmlElement(ElementName = "Phones")]
            public Phones Phones { get; set; }
        }

        [XmlRoot(ElementName = "ShippingData")]
        public class ShippingData
        {
            [XmlElement(ElementName = "ID")]
            public string ID { get; set; }
            [XmlElement(ElementName = "Type")]
            public string Type { get; set; }
            [XmlElement(ElementName = "LegalDocument1")]
            public string LegalDocument1 { get; set; }
            [XmlElement(ElementName = "LegalDocument2")]
            public string LegalDocument2 { get; set; }
            [XmlElement(ElementName = "Name")]
            public string Name { get; set; }
            [XmlElement(ElementName = "BirthDate")]
            public string BirthDate { get; set; }
            [XmlElement(ElementName = "Email")]
            public string Email { get; set; }
            [XmlElement(ElementName = "Gender")]
            public string Gender { get; set; }
            [XmlElement(ElementName = "Address")]
            public Address Address { get; set; }
            [XmlElement(ElementName = "Phones")]
            public Phones Phones { get; set; }
        }

        [XmlRoot(ElementName = "Payment")]
        public class Payment
        {
            [XmlElement(ElementName = "Sequential")]
            public string Sequential { get; set; }
            [XmlElement(ElementName = "Date")]
            public string Date { get; set; }
            [XmlElement(ElementName = "Amount")]
            public string Amount { get; set; }
            [XmlElement(ElementName = "PaymentTypeID")]
            public string PaymentTypeID { get; set; }
            [XmlElement(ElementName = "QtyInstallments")]
            public string QtyInstallments { get; set; }
            [XmlElement(ElementName = "Interest")]
            public string Interest { get; set; }
            [XmlElement(ElementName = "InterestValue")]
            public string InterestValue { get; set; }
            [XmlElement(ElementName = "CardNumber")]
            public string CardNumber { get; set; }
            [XmlElement(ElementName = "CardBin")]
            public string CardBin { get; set; }
            [XmlElement(ElementName = "CardEndNumber")]
            public string CardEndNumber { get; set; }
            [XmlElement(ElementName = "CardType")]
            public string CardType { get; set; }
            [XmlElement(ElementName = "CardExpirationDate")]
            public string CardExpirationDate { get; set; }
            [XmlElement(ElementName = "Name")]
            public string Name { get; set; }
            [XmlElement(ElementName = "LegalDocument")]
            public string LegalDocument { get; set; }
            [XmlElement(ElementName = "Address")]
            public Address Address { get; set; }
            [XmlElement(ElementName = "Nsu")]
            public string Nsu { get; set; }
            [XmlElement(ElementName = "Currency")]
            public string Currency { get; set; }
        }

        [XmlRoot(ElementName = "Payments")]
        public class Payments
        {
            [XmlElement(ElementName = "Payment")]
            public Payment Payment { get; set; }
        }

        [XmlRoot(ElementName = "Item")]
        public class Item
        {
            [XmlElement(ElementName = "ID")]
            public string ID { get; set; }
            [XmlElement(ElementName = "Name")]
            public string Name { get; set; }
            [XmlElement(ElementName = "ItemValue")]
            public string ItemValue { get; set; }
            [XmlElement(ElementName = "Qty")]
            public string Qty { get; set; }
        }

        [XmlRoot(ElementName = "Items")]
        public class Items
        {
            [XmlElement(ElementName = "Item")]
            public List<Item> Item { get; set; }
        }


        [XmlRoot(ElementName = "Passenger")]
        public class Passenger
        {
            [XmlElement(ElementName = "Name")]
            public string Name { get; set; }
            [XmlElement(ElementName = "FrequentFlyerCard")]
            public string FrequentFlyerCard { get; set; }
            [XmlElement(ElementName = "LegalDocumentType")]
            public string LegalDocumentType { get; set; }
            [XmlElement(ElementName = "LegalDocument")]
            public string LegalDocument { get; set; }
            [XmlElement(ElementName = "BirthDate")]
            public string BirthDate { get; set; }
        }

        [XmlRoot(ElementName = "Passengers")]
        public class Passengers
        {
            [XmlElement(ElementName = "Passenger")]
            public Passenger Passenger { get; set; }
        }

        [XmlRoot(ElementName = "Connection")]
        public class Connection
        {
            [XmlElement(ElementName = "Company")]
            public string Company { get; set; }
            [XmlElement(ElementName = "FlightNumber")]
            public string FlightNumber { get; set; }
            [XmlElement(ElementName = "FlightDate")]
            public string FlightDate { get; set; }
            [XmlElement(ElementName = "Class")]
            public string Class { get; set; }
            [XmlElement(ElementName = "From")]
            public string From { get; set; }
            [XmlElement(ElementName = "To")]
            public string To { get; set; }
            [XmlElement(ElementName = "DepartureDate")]
            public string DepartureDate { get; set; }
            [XmlElement(ElementName = "ArrivalDate")]
            public string ArrivalDate { get; set; }
        }

        [XmlRoot(ElementName = "Connections")]
        public class Connections
        {
            [XmlElement(ElementName = "Connection")]
            public Connection Connection { get; set; }
        }

        [XmlRoot(ElementName = "HotelReservation")]
        public class HotelReservation
        {
            [XmlElement(ElementName = "Hotel")]
            public string Hotel { get; set; }
            [XmlElement(ElementName = "City")]
            public string City { get; set; }
            [XmlElement(ElementName = "State")]
            public string State { get; set; }
            [XmlElement(ElementName = "Country")]
            public string Country { get; set; }
            [XmlElement(ElementName = "ReservationDate")]
            public string ReservationDate { get; set; }
            [XmlElement(ElementName = "ReservationExpirationDate")]
            public string ReservationExpirationDate { get; set; }
            [XmlElement(ElementName = "CheckInDate")]
            public string CheckInDate { get; set; }
            [XmlElement(ElementName = "CheckOutDate")]
            public string CheckOutDate { get; set; }
        }

        [XmlRoot(ElementName = "HotelReservations")]
        public class HotelReservations
        {
            [XmlElement(ElementName = "HotelReservation")]
            public HotelReservation HotelReservation { get; set; }
        }

        [XmlRoot(ElementName = "Order")]
        public class Order
        {
            public override string ToString()
            {
                XmlSerializer xsSubmit = new XmlSerializer(typeof(Order));
                var settings = new XmlWriterSettings();
                settings.OmitXmlDeclaration = true;

                XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
                ns.Add("", "");
                using (StringWriter sww = new StringWriter())
                using (XmlWriter writer = XmlWriter.Create(sww, settings))
                {
                    xsSubmit.Serialize(writer, this, ns);
                    var xml = sww.ToString();
                    return xml;
                }
            }
            [XmlElement(ElementName = "ID")]
            public string ID { get; set; }
            [XmlElement(ElementName = "FingerPrint")]
            public FingerPrint FingerPrint { get; set; }
            [XmlElement(ElementName = "Date")]
            public string Date { get; set; }
            [XmlElement(ElementName = "Email")]
            public string Email { get; set; }
            [XmlElement(ElementName = "B2B_B2C")]
            public string B2B_B2C { get; set; }
            [XmlElement(ElementName = "ShippingPrice")]
            public string ShippingPrice { get; set; }
            [XmlElement(ElementName = "TotalItems")]
            public string TotalItems { get; set; }
            [XmlElement(ElementName = "TotalOrder")]
            public string TotalOrder { get; set; }
            [XmlElement(ElementName = "QtyInstallments")]
            public string QtyInstallments { get; set; }
            [XmlElement(ElementName = "DeliveryTimeCD")]
            public string DeliveryTimeCD { get; set; }
            [XmlElement(ElementName = "QtyItems")]
            public string QtyItems { get; set; }
            [XmlElement(ElementName = "QtyPaymentTypes")]
            public string QtyPaymentTypes { get; set; }
            [XmlElement(ElementName = "IP")]
            public string IP { get; set; }
            [XmlElement(ElementName = "Gift")]
            public string Gift { get; set; }
            [XmlElement(ElementName = "GiftMessage")]
            public string GiftMessage { get; set; }
            [XmlElement(ElementName = "Obs")]
            public string Obs { get; set; }
            [XmlElement(ElementName = "Status")]
            public string Status { get; set; }
            [XmlElement(ElementName = "Reanalise")]
            public string Reanalise { get; set; }
            [XmlElement(ElementName = "Origin")]
            public string Origin { get; set; }
            [XmlElement(ElementName = "ReservationDate")]
            public string ReservationDate { get; set; }
            [XmlElement(ElementName = "Country")]
            public string Country { get; set; }
            [XmlElement(ElementName = "Nationality")]
            public string Nationality { get; set; }
            [XmlElement(ElementName = "Product")]
            public string Product { get; set; }
            [XmlElement(ElementName = "ListTypeID")]
            public string ListTypeID { get; set; }
            [XmlElement(ElementName = "ListID")]
            public string ListID { get; set; }
            [XmlElement(ElementName = "BillingData")]
            public BillingData BillingData { get; set; }
            [XmlElement(ElementName = "ShippingData")]
            public ShippingData ShippingData { get; set; }
            [XmlElement(ElementName = "Payments")]
            public Payments Payments { get; set; }
            [XmlElement(ElementName = "Passengers")]
            public Passengers Passengers { get; set; }
            [XmlElement(ElementName = "Connections")]
            public Connections Connections { get; set; }
            [XmlElement(ElementName = "HotelReservations")]
            public HotelReservations HotelReservations { get; set; }
            [XmlElement(ElementName = "Items")]
            public Items Items { get; set; }
        }
    }
}
