﻿
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace BarkCommerce.Model.AnaliseRisco
{
    public class KondutoPayment : KondutoModel
    {
        [JsonProperty("type"), JsonConverter(typeof(StringEnumConverter))]
        public KondutoPaymentType Type { get; set; }

        public KondutoPayment(KondutoPaymentType type)
        {
            this.Type = type;
        }
    }
}
