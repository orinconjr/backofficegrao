﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BarkCommerce.Model.AnaliseRisco
{
    /// <summary>
    /// This exception is thrown when a {@link KondutoModel} instance is invalid.
    /// </summary>
    public class KondutoInvalidEntityException : KondutoException
    {
        public KondutoInvalidEntityException(KondutoModel entity)
            : base(String.Format("{0} is invalid: {1}", entity.ToString(), entity.GetError())) { }
    }
}
