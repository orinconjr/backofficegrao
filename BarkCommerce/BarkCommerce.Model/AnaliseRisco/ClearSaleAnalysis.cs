﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace BarkCommerce.Model.AnaliseRisco
{
    public class ClearSaleAnalysis
    {
        [XmlRoot(ElementName = "Order")]
        public class Order
        {
            [XmlElement(ElementName = "ID")]
            public string ID { get; set; }
            [XmlElement(ElementName = "Status")]
            public string Status { get; set; }
            [XmlElement(ElementName = "Score")]
            public string Score { get; set; }
        }

        [XmlRoot(ElementName = "Orders")]
        public class Orders
        {
            [XmlElement(ElementName = "Order")]
            public List<Order> Order { get; set; }
        }

        [XmlRoot(ElementName = "ClearSale")]
        public class ClearSale
        {
            [XmlElement(ElementName = "Orders")]
            public Orders Orders { get; set; }
        }
    }
}
