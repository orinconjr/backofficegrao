﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BarkCommerce.Model.AnaliseRisco
{
    public class PagamentoPedidoRisco
    {
        public int IdPagamentoPedido { get; set; }
        public int StatusInterno { get; set; }
        public DateTime DataEnvio { get; set; }
        public string TransactionId { get; set; }
        public string Score { get; set; }
        public string Message { get; set; }
    }
}
