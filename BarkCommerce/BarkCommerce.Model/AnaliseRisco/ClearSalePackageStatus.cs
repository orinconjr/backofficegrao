﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace BarkCommerce.Model.AnaliseRisco
{
    public class ClearSalePackageStatus
    {
        [XmlRoot(ElementName = "Order")]
        public class Order
        {
            [XmlElement(ElementName = "ID")]
            public string ID { get; set; }
            [XmlElement(ElementName = "Status")]
            public string Status { get; set; }
            [XmlElement(ElementName = "Score")]
            public string Score { get; set; }
        }

        [XmlRoot(ElementName = "Orders")]
        public class Orders
        {
            [XmlElement(ElementName = "Order")]
            public List<Order> Order { get; set; }
        }

        [XmlRoot(ElementName = "PackageStatus")]
        public class PackageStatus
        {
            [XmlElement(ElementName = "TransactionID")]
            public string TransactionID { get; set; }
            [XmlElement(ElementName = "StatusCode")]
            public string StatusCode { get; set; }
            [XmlElement(ElementName = "Message")]
            public string Message { get; set; }
            [XmlElement(ElementName = "Orders")]
            public Orders Orders { get; set; }
        }

    }
}
