﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BarkCommerce.Model.AnaliseRisco
{
    public class ResultadoAnalise
    {
        public int PedidoId { get; set; }
        public string Status { get; set; }
        public string Score { get; set; }
    }
}
