﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BarkCommerce.Model.AnaliseRisco
{
    /// <summary>
    /// Payment type enum.
    /// @see <a href="http://docs.konduto.com">Konduto API Spec</a>
    /// </summary>
    public enum KondutoPaymentType
    {
        credit,
        boleto,
        debit,
        voucher,
        transfer,
        error
    }
}
