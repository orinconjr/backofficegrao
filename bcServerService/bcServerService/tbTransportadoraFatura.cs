//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace bcServerService
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbTransportadoraFatura
    {
        public tbTransportadoraFatura()
        {
            this.tbTransportadoraFaturaRegistro = new HashSet<tbTransportadoraFaturaRegistro>();
        }
    
        public int idTransportadoraFatura { get; set; }
        public string transportadora { get; set; }
        public string codigoFatura { get; set; }
        public Nullable<System.DateTime> dataEmissao { get; set; }
        public Nullable<System.DateTime> dataVencimento { get; set; }
        public Nullable<decimal> valorTotal { get; set; }
        public Nullable<int> idTransportadora { get; set; }
    
        public virtual tbTransportadora tbTransportadora { get; set; }
        public virtual ICollection<tbTransportadoraFaturaRegistro> tbTransportadoraFaturaRegistro { get; set; }
    }
}
