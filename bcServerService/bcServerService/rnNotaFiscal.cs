﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace bcServerService
{
    class rnNotaFiscal
    {
        private static string cUF = "35";
        private static string cMunFG = "3548500";
        public static string tpAmb = "1";
        public static string cnpjEmitente = "10924051000163";
        private static string nomeEmitente = "LINDSAY FERRANDO ME";
        private static string nomeFantasiaEmitente = "GRAO DE GENTE";
        private static string logradouroEmitente = "AV ANA COSTA";
        private static string numeroEmitente = "416";
        private static string bairroEmitente = "GONZAGA";
        private static string codigoMunicipioEmitente = "3552700";
        private static string municipioEmitente = "SANTOS";
        private static string ufEmitente = "SP";
        private static string cepEmitente = "11060002";
        private static string foneEmitente = "1135228379";
        private static string ieEmitente = "633459940118";

        public static string chaveDeAcesso = "VfIVYjA/xTjRVKoNFshvHSrWZqanJ3bg";
        //public static string chaveDeAcesso = "bkXnxecdoeQNOHCQhKIk85sRPXzbVNNK";
        public static string EmpPK = "6rRJmEhQmdMKCIcVwD22vw==";


        public static bool DefineNotaLiberarPedido(int pedidoId, string estado)
        {
            var data = new dbSiteEntities();
            var configuracaoEstado =
                (from c in data.tbGnreConfiguracao where c.uf == estado.ToLower() select c).FirstOrDefault();
            if (configuracaoEstado == null) return true;

            if (configuracaoEstado.notaAntecipada == true)
            {
                GeraNotaLiberarPedido(pedidoId, estado);
            }


            if (configuracaoEstado.gerarGnre == true && configuracaoEstado.pagamentoGnre == true)
            {
                var nota =
                    (from c in data.tbNotaFiscal
                        where c.idPedido == pedidoId && c.statusNfe == 2 && c.statusGnre == 2 && c.statusPagamentoGnre == 2
                        select c).Any();
                if (nota)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else if (configuracaoEstado.gerarGnre == true)
            {
                var nota =
                    (from c in data.tbNotaFiscal
                     where c.idPedido == pedidoId && c.statusNfe == 2 && c.statusGnre == 2
                     select c).Any();
                if (nota)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else if (configuracaoEstado.notaAntecipada == true)
            {
                var nota =
                    (from c in data.tbNotaFiscal
                     where c.idPedido == pedidoId && c.statusNfe == 2
                     select c).Any();
                if (nota)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            return true;
        }
        public static void GeraNotaLiberarPedido(int pedidoId, string estado)
        {
            var data = new dbSiteEntities();
            var quinze = DateTime.Now.AddDays(-15);
            var notaGerada = (from c in data.tbNotaFiscal where c.idPedido == pedidoId && c.dataHora > quinze select c).Any();

            if (!notaGerada)
            {
                gerarNumeroNotaEmpresa(pedidoId, 0, false, 5);
            }
        }


        public static tbNotaFiscal gerarNumeroNotaEmpresa(int pedidoId, int idPedidoEnvio, bool gerarNovaNota, int idEmpresa)
        {
            var notaDc = new dbSiteEntities();
            var notas = (from c in notaDc.tbNotaFiscal where c.idPedido == pedidoId && c.linkDanfe == null select c);
            if (idPedidoEnvio > 0)
            {
                notas = notas.Where(x => x.idPedidoEnvio == idPedidoEnvio);
            }
            var nota = notas.FirstOrDefault();
            if (nota != null && !gerarNovaNota)
            {
                nota.ultimaChecagem = DateTime.Now.AddSeconds(-270);
                notaDc.SaveChanges();
                return nota;
            }
            else
            {
                #region Define CNPJ de Emissão

                var empresaSelecionada = (from c in notaDc.tbEmpresa where c.idEmpresa == idEmpresa select c).First();
                #endregion

                int numero = 1;
                var numeroNota = (from c in notaDc.tbNotaFiscal where c.idCNPJ == idEmpresa orderby c.numeroNota descending select c).FirstOrDefault();
                if (numeroNota != null)
                {
                    numero = (numeroNota.numeroNota + 1);
                }
                var notaNova = new tbNotaFiscal();
                notaNova.idPedido = pedidoId;
                notaNova.numeroNota = numero;
                notaNova.dataHora = DateTime.Now;
                notaNova.idPedidoEnvio = idPedidoEnvio;
                notaNova.ultimaChecagem = DateTime.Now.AddMinutes(-270);
                notaNova.ultimoStatus = "Aguardando Processamento";
                notaNova.idCNPJ = idEmpresa;
                notaNova.statusNfe = 0;

                string chave = "";
                chave += empresaSelecionada.codigoUfEmitente;
                chave += DateTime.Now.Year.ToString().Substring(2, 2);
                chave += DateTime.Now.Month.ToString().PadLeft(2, '0');
                chave += empresaSelecionada.cnpj;
                chave += "55";
                chave += "001";
                chave += numero.ToString().PadLeft(9, '0');
                chave += "1";
                chave += pedidoId.ToString().PadLeft(8, '0');

                notaNova.nfeKey = chave;

                notaDc.tbNotaFiscal.Add(notaNova);
                notaDc.SaveChanges();

                var dataCheck = new dbSiteEntities();
                var notaCheck =
                    (from c in dataCheck.tbNotaFiscal where c.idCNPJ == idEmpresa && c.numeroNota == numero select c)
                        .Count();
                if (notaCheck > 1)
                {
                    var numeroNotaNovo = (from c in notaDc.tbNotaFiscal where c.idCNPJ == idEmpresa orderby c.numeroNota descending select c).FirstOrDefault();
                    if (numeroNotaNovo != null)
                    {
                        numero = (numeroNotaNovo.numeroNota + 1);
                    }
                    notaNova.numeroNota = numero;
                    notaDc.SaveChanges();
                }
                return notaNova;
            }
        }
        public static string gerarNotaFiscalEnvio(int idPedidoEnvio, string codigoIbge)
        {
            bool totalBrinde = true;

            string cfop = "";

            var pedidoDc = new dbSiteEntities();
            var pedido = (from c in pedidoDc.tbPedidoEnvio where c.idPedidoEnvio == idPedidoEnvio select c.tbPedidos).FirstOrDefault();
            var envio = (from c in pedidoDc.tbPedidoEnvio where c.idPedidoEnvio == idPedidoEnvio select c).FirstOrDefault();

            if (pedido == null) return "";
            int pedidoId = pedido.pedidoId;

            var clienteDc = new dbSiteEntities();
            var cliente = (from c in clienteDc.tbClientes where c.clienteId == pedido.clienteId select c).FirstOrDefault();

            var gerarNota = gerarNumeroNota(pedido.pedidoId, idPedidoEnvio, false);

            string numeroNota = gerarNota.numeroNota.ToString();
            string chave = gerarNota.nfeKey;
            string cDV = GerarDigitoVerificadorNFe(chave).ToString();
            chave += cDV;

            string idDest = "";
            if (cliente.clienteCPFCNPJ.Length == 11 && pedido.endEstado.ToLower() == "sp")
            {
                cfop = "5403";
                idDest = "1";
            }
            else if (cliente.clienteCPFCNPJ.Length > 11 && pedido.endEstado.ToLower() == "sp")
            {
                cfop = "5102";
                idDest = "1";
            }
            else if (cliente.clienteCPFCNPJ.Length == 11 && pedido.endEstado.ToLower() != "sp")
            {
                cfop = "6108";
                idDest = "2";
            }
            else
            {
                cfop = "6102";
                idDest = "2";
            }



            var nota = new StringBuilder();

            #region Identificadores da NF-e
            //nota.Append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
            //nota.Append("<NFe xmlns=\"http://www.portalfiscal.inf.br/nfe\">");
            //nota.AppendFormat("<infNFe versao=\"2.00\" Id=\"NFe{0}\">", chave);
            nota.Append("<ide>");
            nota.AppendFormat("<cUF>{0}</cUF>", cUF);
            nota.AppendFormat("<cNF>{0}</cNF>", pedidoId.ToString().PadLeft(8, '0'));
            nota.Append("<natOp>VENDA DE MERCADORIA</natOp>");
            nota.Append("<indPag>0</indPag>");
            nota.Append("<mod>55</mod>");
            nota.Append("<serie>1</serie>");
            nota.AppendFormat("<nNF>{0}</nNF>", numeroNota);
            nota.AppendFormat("<dhEmi>{0}T{1}</dhEmi>", DateTime.Now.ToString("yyyy-MM-dd"), DateTime.Now.ToString("HH:mm:ss"));
            nota.Append("<fusoHorario>-03:00</fusoHorario>");
            nota.Append("<tpNf>1</tpNf>");
            nota.AppendFormat("<idDest>{0}</idDest>", idDest);
            nota.AppendFormat("<indFinal>1</indFinal>");
            nota.AppendFormat("<indPres>2</indPres>");
            nota.AppendFormat("<cMunFg>{0}</cMunFg>", cMunFG);
            nota.Append("<tpImp>1</tpImp>");
            nota.Append("<tpEmis>1</tpEmis>");
            nota.AppendFormat("<tpAmb>{0}</tpAmb>", tpAmb);
            nota.Append("<finNFe>1</finNFe>");
            nota.Append("</ide>");
            #endregion

            #region Emitente

            nota.Append("<emit>");
            nota.AppendFormat("<CNPJ_emit>{0}</CNPJ_emit>", cnpjEmitente);
            nota.AppendFormat("<xNome>{0}</xNome>", nomeEmitente);
            nota.AppendFormat("<xFant>{0}</xFant>", nomeFantasiaEmitente);
            nota.Append("<enderEmit>");
            nota.AppendFormat("<xLgr>{0}</xLgr>", logradouroEmitente);
            nota.AppendFormat("<nro>{0}</nro>", numeroEmitente);
            nota.AppendFormat("<xBairro>{0}</xBairro>", bairroEmitente);
            nota.AppendFormat("<cMun>{0}</cMun>", codigoMunicipioEmitente);
            nota.AppendFormat("<xMun>{0}</xMun>", municipioEmitente);
            nota.AppendFormat("<UF>{0}</UF>", ufEmitente);
            nota.AppendFormat("<CEP>{0}</CEP>", cepEmitente);
            nota.Append("<cPais>1058</cPais>");
            nota.Append("<xPais>BRASIL</xPais>");
            nota.AppendFormat("<fone>{0}</fone>", foneEmitente);
            nota.Append("</enderEmit>");
            nota.AppendFormat("<IE>{0}</IE>", ieEmitente);
            nota.Append("<CRT>1</CRT>");
            nota.Append("</emit>");

            #endregion

            #region Destinatario

            nota.Append("<dest>");

            if (cliente.clienteCPFCNPJ.Length == 11)
            {
                nota.AppendFormat("<CPF_dest>{0}</CPF_dest>", cliente.clienteCPFCNPJ);
                if (tpAmb == "2")
                {
                    nota.Append("<xNome_dest>NF-E EMITIDA EM AMBIENTE DE HOMOLOGACAO - SEM VALOR FISCAL</xNome_dest>");
                }
                else
                {
                    nota.AppendFormat("<xNome_dest>{0}</xNome_dest>", cliente.clienteNome.Trim());
                }
                nota.Append("<indIEDest>2</indIEDest>");
            }
            else
            {
                nota.AppendFormat("<CNPJ_dest>{0}</CNPJ_dest>", cliente.clienteCPFCNPJ.Trim());
                nota.AppendFormat("<xNome_dest>{0}</xNome_dest>", cliente.clienteNomeDaEmpresa.Trim());
                if (tpAmb == "2")
                {
                    nota.Append("<xNome_dest>NF-E EMITIDA EM AMBIENTE DE HOMOLOGACAO - SEM VALOR FISCAL</xNome_dest>");
                }
                else
                {
                    if (!string.IsNullOrEmpty(cliente.clienteRGIE))
                    {
                        nota.Append("<indIEDest>1</indIEDest>");
                        nota.AppendFormat("<IE_dest>{0}</IE_dest>", cliente.clienteRGIE.Trim());
                    }
                    else
                    {
                        nota.Append("<indIEDest>2</indIEDest>");
                    }
                }
            }

            nota.Append("<enderDest>");
            nota.AppendFormat("<xLgr_dest>{0}</xLgr_dest>", rnFuncoes.removeCaracteresEspeciaisDeixaEspaco(rnFuncoes.limpaStringDeixaEspaco(pedido.endRua.Length > 58 ? pedido.endRua.Substring(0, 58) : pedido.endRua)).ToUpper().Replace("-", " ").Trim().Trim());
            nota.AppendFormat("<nro_dest>{0}</nro_dest>", pedido.endNumero.Trim());
            if (!string.IsNullOrEmpty(pedido.endComplemento)) nota.AppendFormat("<xCpl_dest>{0}</xCpl_dest>", rnFuncoes.removeCaracteresEspeciaisDeixaEspaco(rnFuncoes.limpaStringDeixaEspaco(pedido.endComplemento.Length > 58 ? pedido.endComplemento.Substring(0, 58) : pedido.endComplemento)).ToUpper().Replace("-", " ").Trim().Trim());
            nota.AppendFormat("<xBairro_dest>{0}</xBairro_dest>", pedido.endBairro.Trim());
            nota.AppendFormat("<cMun_dest>{0}</cMun_dest>", codigoIbge.Trim());
            nota.AppendFormat("<xMun_dest>{0}</xMun_dest>", pedido.endCidade.Trim());
            nota.AppendFormat("<UF_dest>{0}</UF_dest>", pedido.endEstado.Trim());
            nota.AppendFormat("<CEP_dest>{0}</CEP_dest>", pedido.endCep.Replace("-", "").Trim());
            nota.Append("<cPais_dest>1058</cPais_dest>");
            nota.Append("<xPais_dest>BRASIL</xPais_dest>");
            if (!string.IsNullOrEmpty(cliente.clienteFoneResidencial)) nota.AppendFormat("<fone_dest>{0}</fone_dest>", cliente.clienteFoneResidencial.Replace("(", "").Replace(")", "").Replace(" ", "").Replace("-", "").Trim());
            nota.Append("</enderDest>");
            if (cliente.clienteCPFCNPJ.Length == 11)
            {
                //nota.Append("<IE_dest>ISENTO</IE_dest>");
            }
            else
            {
                //nota.AppendFormat("<IE_dest>{0}</IE_dest>", cliente.clienteRGIE);
            }
            nota.AppendFormat("<Email_dest>{0}</Email_dest>", cliente.clienteEmail.Trim());
            nota.Append("</dest>");
            #endregion

            #region Autorizacao Emissao
            nota.Append("<autXML>");
            nota.Append("<autXMLItem>");
            nota.AppendFormat("<CNPJ_aut>{0}</CNPJ_aut>", cnpjEmitente);
            nota.Append("</autXMLItem>");
            nota.Append("</autXML>");
            #endregion

            #region Detalhamento dos Itens

            nota.Append("<det>");

            int nItem = 1;
            var itensDc = new dbSiteEntities();
            decimal valorDoDescontoDoCupom = pedido.valorDoDescontoDoCupom > 0 ? (decimal)pedido.valorDoDescontoDoCupom : 0;
            decimal valorDoDescontoDoPagamento = pedido.valorDoDescontoDoPagamento > 0 ? (decimal)pedido.valorDoDescontoDoPagamento : 0;
            decimal totalDesconto = valorDoDescontoDoCupom + valorDoDescontoDoPagamento;
            decimal valorDoDesconto = 0;
            decimal totalDoPedidoUnitario = 0;
            decimal totalDoPedido = 0;
            decimal totalDescontoCalculado = 0;
            decimal totalIcms = 0;

            var produtos = (from c in pedidoDc.tbProdutoEstoque where c.pedidoId == pedidoId && c.produtoId != 28735 && c.idPedidoEnvio == idPedidoEnvio select new { c.idPedidoEnvio, itemPedidoId = (c.itemPedidoId ?? 0), c.produtoId }).ToList();
            var itensPedido = (from c in itensDc.tbItensPedido where c.pedidoId == pedidoId && c.produtoId != 28735 && produtos.Select(x => x.itemPedidoId).Contains(c.itemPedidoId) && (c.cancelado ?? false) == false select c).ToList();
            //var itensPedido = (from c in pedidoDc.tbProdutoEstoques where c.idPedidoEnvio == idPedidoEnvio select c);
            var pacotes = (from c in pedidoDc.tbPedidoPacote where c.idPedidoEnvio == idPedidoEnvio select c);
            decimal valorFrete = 0;
            decimal valorFreteProduto = 0;
            int totalItens = produtos.Count;
            if (Convert.ToDecimal(pedido.valorDoFrete) > 0)
            {
                valorFreteProduto = Convert.ToDecimal(pedido.valorDoFrete) / totalItens;
            }

            decimal pesoTotal = 0;
            if (pacotes.Any())
            {
                pesoTotal = pacotes.Sum(x => x.peso);
            }
            else
            {
                foreach (var item in itensPedido)
                {
                    var produtoDc = new dbSiteEntities();
                    var produto = (from c in produtoDc.tbProdutos where c.produtoId == item.produtoId select c).First();
                    pesoTotal += Convert.ToDecimal(produto.produtoPeso);
                }
            }

            int indiceProduto = 0;
            foreach (var produtoEnviado in produtos)
            {
                var produtoDc = new dbSiteEntities();
                var produto = (from c in produtoDc.tbProdutos where c.produtoId == produtoEnviado.produtoId select c).First();
                var itemPedido = itensPedido.FirstOrDefault(x => x.itemPedidoId == produtoEnviado.itemPedidoId);
                if (itemPedido != null)
                {
                    bool cancelado = itemPedido.cancelado == null ? false : (bool)itemPedido.cancelado;
                    bool brinde = itemPedido.brinde == null ? false : (bool)itemPedido.brinde;
                    decimal valorDoProduto = itemPedido.itemValor;
                    string cfopProduto = cfop;
                    decimal freteDoProduto = 0;
                    decimal valorDesconto = 0;
                    bool adicionarFrete = false;
                    if (pedido.valorDoFrete > 0)
                    {
                        freteDoProduto = valorFreteProduto;
                        valorFrete += valorFreteProduto;
                    }

                    if (brinde)
                    {
                        if (cliente.clienteEstado.ToLower() == "sp")
                        {
                            cfopProduto = "5910";
                        }
                        else
                        {
                            cfopProduto = "6910";
                        }
                    }
                    else
                    {
                        totalBrinde = false;
                    }

                    if (!cancelado)
                    {
                        var produtosDc = new dbSiteEntities();
                        var produtosFilho =
                            (from c in produtosDc.tbProdutoRelacionado
                             where c.idProdutoPai == itemPedido.produtoId
                             select c);
                        if (produtosFilho.Any())
                        {
                            var produtoFilho = produtosFilho.FirstOrDefault(x => x.idProdutoFilho == produto.produtoId | x.idProdutoFilho == produto.produtoAlternativoId);
                            if (produtoFilho != null)
                            {

                                decimal totalCombo = 0;
                                foreach (var produtoRelacionado in produtosFilho)
                                {
                                    if (produtoRelacionado.desconto == 0)
                                    {
                                        var valorDoProdutoCombo = produtoRelacionado.tbProdutos.produtoPrecoPromocional > 0
                                            ? produtoRelacionado.tbProdutos.produtoPrecoPromocional
                                            : produtoRelacionado.tbProdutos.produtoPreco;
                                        totalCombo += Convert.ToDecimal(valorDoProdutoCombo);
                                        indiceProduto++;
                                    }
                                }

                                valorDoProduto = produto.produtoPrecoPromocional > 0 ? Convert.ToDecimal(produto.produtoPrecoPromocional) : produto.produtoPreco;
                                try
                                {
                                    valorDoProduto = (valorDoProduto * (itemPedido.itemValor == 0 ? itemPedido.tbProdutos.produtoPreco : itemPedido.itemValor)) / totalCombo;
                                }
                                catch (Exception ex)
                                {

                                }

                                totalDoPedidoUnitario += valorDoProduto;
                                totalDoPedido += valorDoProduto;

                                if (totalDesconto > 0 && brinde == false)
                                {
                                    //valorDesconto = (Convert.ToDecimal((valorDoProduto * totalDesconto) / pedido.valorDosItens));
                                    //valorDoDesconto += valorDesconto;

                                    valorDesconto = (Convert.ToDecimal((valorDoProduto * totalDesconto) / pedido.valorDosItens));
                                    if ((valorDoDesconto + valorDesconto) > totalDesconto) valorDesconto = totalDesconto - valorDoDesconto;
                                    valorDoDesconto += valorDesconto;
                                    totalDescontoCalculado += valorDesconto;
                                }

                                if (produtoFilho.desconto > 0)
                                {
                                    if (cliente.clienteEstado.ToLower() == "sp")
                                    {
                                        cfopProduto = "5910";
                                    }
                                    else
                                    {
                                        cfopProduto = "6910";
                                    }
                                }
                            }
                            else
                            {

                                decimal totalCombo = 0;
                                valorDoProduto = produto.produtoPrecoPromocional > 0 ? Convert.ToDecimal(produto.produtoPrecoPromocional) : produto.produtoPreco;
                                totalDoPedidoUnitario += valorDoProduto;
                                totalDoPedido += valorDoProduto;

                                if (totalDesconto > 0 && brinde == false)
                                {
                                    //valorDesconto = (Convert.ToDecimal((valorDoProduto * totalDesconto) / pedido.valorDosItens));
                                    //valorDoDesconto += valorDesconto;

                                    valorDesconto = (Convert.ToDecimal((itemPedido.itemValor * totalDesconto) / pedido.valorDosItens));
                                    valorDoDesconto += valorDesconto;
                                    totalDescontoCalculado += valorDesconto;
                                }
                            }
                        }
                        else
                        {
                            indiceProduto++;
                            valorDoProduto = itemPedido.itemValor == 0 ? itemPedido.tbProdutos.produtoPreco : itemPedido.itemValor;
                            if (totalDesconto > 0 && brinde == false)
                            {
                                valorDesconto = (Convert.ToDecimal((itemPedido.itemValor * totalDesconto) / pedido.valorDosItens));
                                valorDoDesconto += valorDesconto;
                            }
                            else
                            {
                                totalBrinde = false;
                            }

                            totalDoPedidoUnitario += valorDoProduto;
                            totalDoPedido += valorDoProduto;

                        }

                        var aliquota = (produto.icms ?? 0) == 0 ? 18 : (int)produto.icms;
                        var valorIcms = ((Convert.ToDecimal(valorDoProduto) * Convert.ToInt32(itemPedido.itemQuantidade)) / 100) *
                                        aliquota;

                        totalIcms += valorIcms;

                        nota.AppendFormat("<detItem>", nItem);
                        nota.Append("<prod>");
                        nota.AppendFormat("<cProd>{0}</cProd>", produto.produtoId);
                        nota.AppendFormat("<xProd>{0}</xProd>",
                            limitaTexto(
                                rnFuncoes.removeCaracteresEspeciaisDeixaEspaco(
                                    rnFuncoes.limpaStringDeixaEspaco(produto.produtoNome))
                                    .ToUpper()
                                    .Replace("-", " ")
                                    .Trim(), 119).Trim());
                        nota.AppendFormat("<NCM>{0}</NCM>", produto.ncm.Trim());
                        nota.AppendFormat("<CFOP>{0}</CFOP>", cfopProduto);
                        nota.AppendFormat("<uCOM>{0}</uCOM>", "UN");
                        nota.AppendFormat("<qCOM>{0}</qCOM>", "1.0000");
                        nota.AppendFormat("<vUnCom>{0}</vUnCom>", valorDoProduto.ToString("0.00").Replace(",", "."));
                        nota.AppendFormat("<vProd>{0}</vProd>", (valorDoProduto).ToString("0.00").Replace(",", "."));
                        nota.Append("<cEANTrib/>");
                        nota.AppendFormat("<uTrib>{0}</uTrib>", "UN");
                        nota.AppendFormat("<qTrib>{0}</qTrib>", "1.00");
                        nota.AppendFormat("<vUnTrib>{0}</vUnTrib>", valorDoProduto.ToString("0.00").Replace(",", "."));
                        if (freteDoProduto > 0)
                            nota.AppendFormat("<vFrete>{0}</vFrete>", freteDoProduto.ToString("0.00").Replace(",", "."));
                        if (valorDesconto > 0)
                            nota.AppendFormat("<vDesc>{0}</vDesc>", valorDesconto.ToString("0.00").Replace(",", "."));
                        nota.AppendFormat("<indTot>{0}</indTot>", "1");
                        nota.AppendFormat("<nTipoItem>{0}</nTipoItem>", "0");
                        nota.Append("</prod>");
                        nota.Append("<imposto>");
                        nota.Append("<ICMS>");
                        nota.Append("<orig>0</orig>");
                        nota.Append("<CST>102</CST>");
                        nota.Append("</ICMS>");
                        nota.Append("<PIS>");
                        nota.Append("<CST_pis>07</CST_pis>");
                        nota.Append("</PIS>");
                        nota.Append("<COFINS>");
                        nota.Append("<CST_cofins>07</CST_cofins>");
                        nota.Append("</COFINS>");
                        nota.Append("</imposto>");
                        nota.Append("</detItem>");
                        nItem++;
                    }
                }
            }

            nota.Append("</det>");
            #endregion

            #region Totais
            nota.Append("<total>");
            nota.Append("<ICMStot>");
            if (gerarNota.tbEmpresa.simples)
            {
                nota.Append("<vBC_ttlnfe>0.00</vBC_ttlnfe>");
                nota.Append("<vICMS_ttlnfe>0.00</vICMS_ttlnfe>");
            }
            else
            {
                nota.AppendFormat("<vBC_ttlnfe>{0}</vBC_ttlnfe>", Convert.ToDecimal(totalDoPedido).ToString("0.00").Replace(",", "."));
                nota.AppendFormat("<vICMS_ttlnfe>{0}</vICMS_ttlnfe>", Convert.ToDecimal(totalIcms).ToString("0.00").Replace(",", "."));
            }
            nota.Append("<vBCST_ttlnfe>0.00</vBCST_ttlnfe>");
            nota.Append("<vST_ttlnfe>0.00</vST_ttlnfe>");
            nota.AppendFormat("<vProd_ttlnfe>{0}</vProd_ttlnfe>", totalDoPedido.ToString("0.00").Replace(",", "."));
            nota.AppendFormat("<vFrete_ttlnfe>{0}</vFrete_ttlnfe>", valorFrete.ToString("0.00").Replace(",", "."));
            nota.Append("<vSeg_ttlnfe>0.00</vSeg_ttlnfe>");
            nota.AppendFormat("<vDesc_ttlnfe>{0}</vDesc_ttlnfe>", totalDescontoCalculado.ToString("0.00").Replace(",", "."));
            //nota.Append("<vII>0.00</vII>");
            nota.Append("<vIPI_ttlnfe>0.00</vIPI_ttlnfe>");
            nota.Append("<vPIS_ttlnfe>0.00</vPIS_ttlnfe>");
            nota.Append("<vCOFINS_ttlnfe>0.00</vCOFINS_ttlnfe>");
            nota.Append("<vOutro>0.00</vOutro>");
            nota.Append("<vICMSDeson_ttlnfe>0.00</vICMSDeson_ttlnfe>");
            nota.AppendFormat("<vNF>{0}</vNF>", Convert.ToDecimal(totalDoPedido + valorFrete - valorDoDesconto).ToString("0.00").Replace(",", "."));
            nota.Append("<vTotTrib_ttlnfe>0.00</vTotTrib_ttlnfe>");
            nota.Append("</ICMStot>");
            nota.Append("</total>");
            #endregion

            #region transportadora
            nota.Append("<transp>");
            nota.Append("<modFrete>0</modFrete>");

            if (envio.formaDeEnvio == "jadlog" | envio.formaDeEnvio == "jadlogexpressa")
            {
                nota.Append("<transporta>");
                nota.Append("<CNPJ_transp>14135995000102</CNPJ_transp>");
                nota.Append("<xNome_transp>CALLILI EXPRESS LTDA ME</xNome_transp>");
                nota.Append("<IE_transp>344063890114</IE_transp>");
                nota.Append("<xEnder>RUA DANIEL DE FREITAS , 928 - CENTRO</xEnder>");
                nota.Append("<xMun_transp>Ibitinga</xMun_transp>");
                nota.Append("<UF_transp>SP</UF_transp>");
                nota.Append("</transporta>");
            }
            else if (envio.formaDeEnvio == "tnt")
            {
                nota.Append("<transporta>");
                nota.Append("<CNPJ_transp>95591723008284</CNPJ_transp>");
                nota.Append("<xNome_transp>TNT Mercurio Cargas e Encomendas Expressas SA</xNome_transp>");
                nota.Append("<IE_transp>209199664118</IE_transp>");
                nota.Append("<xEnder>RUA JOAQUIM PELEGRINA LOPES,1-80 - Dist. Ind. III</xEnder>");
                nota.Append("<xMun_transp>Bauru</xMun_transp>");
                nota.Append("<UF_transp>SP</UF_transp>");
                nota.Append("</transporta>");
            }
            /*else
            {
                nota.Append("<transporta>");
                nota.Append("<CNPJ>14135995000102</CNPJ>");
                nota.Append("<xNome>CALLILI EXPRESS LTDA-ME</xNome>");
                nota.Append("<IE>344063890114</IE>");
                nota.Append("<xEnder>RUA DANIEL DE FREITAS , 928 - CENTRO</xEnder>");
                nota.Append("<xMun>Ibitinga</xMun>");
                nota.Append("<UF>SP</UF>");
                nota.Append("</transporta>");
            }*/
            int vol = 1;
            nota.Append("<vol>");
            foreach (var pacote in pacotes.OrderBy(x => x.numeroVolume))
            {
                nota.Append("<volItem>");
                nota.AppendFormat("<qVol>{0}</qVol>", 1);
                nota.Append("<esp>VOLUME</esp>");
                nota.AppendFormat("<nVol>{0}</nVol>", vol);
                nota.AppendFormat("<pesoB_transp>{0}</pesoB_transp>", (Convert.ToDecimal(pacote.peso) / 1000).ToString("0.000").Replace(",", "."));
                nota.Append("</volItem>");
                vol++;
            }
            nota.Append("</vol>");
            nota.Append("</transp>");
            nota.Append("<infAdic>");
            nota.AppendFormat("<infCpl>PEDIDO {0} - I DOCUMENTO EMITIDO POR EMPRESA ME OU EPP OPTANTE PELO SIMPLES NACIONAL II- NÃO GERA CREDITO FISCAL DE IPI E ISS." + (gerarNota.idCNPJ == 2 ? " 7,60% de tributos a união 3,91% de tributos estaduais 0,00% de tributos municipais - total de 11,51% de tributos." : "") + ";</infCpl>", rnFuncoes.retornaIdCliente(pedido.pedidoId));
            nota.Append("</infAdic>");
            #endregion

            //nota.Append("</infNFe>");
            //nota.Append("</NFe>");

            envio.nfeNumero = Convert.ToInt32(numeroNota);
            envio.nfeKey = chave;
            envio.nfeValor = Convert.ToDecimal(totalDoPedido);

            pedidoDc.SaveChanges();

            try
            {
                //System.IO.File.WriteAllText(Application.StartupPath + "\\admin\\notas\\" + pedidoId + "_" + idPedidoEnvio + ".xml", nota.ToString());
            }
            catch (Exception ex)
            { }
            string notaFormatado = nota.ToString();
            if (totalBrinde) notaFormatado.Replace("<natOp>VENDA DE MERCADORIA</natOp>", "<natOp>REMESSA EM BONIFICAÇÃO/BRINDE</natOp>");
            return nota.ToString();
        }

        private static tbNotaFiscal gerarNumeroNota(int pedidoId, int idPedidoEnvio, bool gerarNovaNota)
        {
            var notaDc = new dbSiteEntities();
            var notas = (from c in notaDc.tbNotaFiscal where c.idPedido == pedidoId select c);
            if (idPedidoEnvio > 0)
            {
                notas = notas.Where(x => x.idPedidoEnvio == idPedidoEnvio);
            }
            var nota = notas.FirstOrDefault();
            if (nota != null && !gerarNovaNota)
            {
                nota.ultimaChecagem = DateTime.Now;
                notaDc.SaveChanges();
                return nota;
            }
            else
            {
                int numero = 1;
                var numeroNota = (from c in notaDc.tbNotaFiscal orderby c.numeroNota descending select c).FirstOrDefault();
                if (numeroNota != null)
                {
                    numero = (numeroNota.numeroNota + 1);
                }
                var notaNova = new tbNotaFiscal();
                notaNova.idPedido = pedidoId;
                notaNova.numeroNota = numero;
                notaNova.dataHora = DateTime.Now;
                notaNova.idPedidoEnvio = idPedidoEnvio;
                notaNova.ultimaChecagem = DateTime.Now.AddMinutes(4);
                notaNova.ultimoStatus = "Aguardando Processamento";

                string chave = "";
                chave += cUF;
                chave += DateTime.Now.Year.ToString().Substring(2, 2);
                chave += DateTime.Now.Month.ToString().PadLeft(2, '0');
                chave += cnpjEmitente;
                chave += "55";
                chave += "001";
                chave += numero.ToString().PadLeft(9, '0');
                chave += "1";
                chave += pedidoId.ToString().PadLeft(8, '0');

                notaNova.nfeKey = chave;

                notaDc.tbNotaFiscal.Add(notaNova);
                notaDc.SaveChanges();
                return notaNova;
            }
        }

        public static void autorizarNota(string xmlNotaFiscal, int idPedidoEnvio)
        {

            var notas = new List<serviceNfe.InvoiCyRecepcaoDadosItem>();
            var serviceRecepcao = new serviceNfe.InvoiCy();
            var cabecalho = new serviceNfe.InvoiCyRecepcaoCabecalho();

            cabecalho.EmpPK = rnNotaFiscal.EmpPK;
            serviceRecepcao.Cabecalho = cabecalho;

            var nota = new serviceNfe.InvoiCyRecepcaoDadosItem();
            string documentoRequisicao = "<Envio><ModeloDocumento>NFe</ModeloDocumento><Versao>3.10</Versao>" + xmlNotaFiscal + "</Envio>";
            nota.Documento = documentoRequisicao;

            string valorParaHash = rnNotaFiscal.chaveDeAcesso + documentoRequisicao;
            cabecalho.EmpCK = rnFuncoes.gerarMd5(valorParaHash);


            notas.Add(nota);
            //gerarNotaFiscalEnvio
            serviceRecepcao.Dados = notas.ToArray();

            var nfe = new serviceNfe.recepcao();
            var retornoNota = nfe.Execute(serviceRecepcao);
            try
            {
                if (idPedidoEnvio > 0)
                {
                    var pedidosDc = new dbSiteEntities();
                    var pedidoEnvio = (from c in pedidosDc.tbPedidoEnvio where c.idPedidoEnvio == idPedidoEnvio select c).First();
                    pedidoEnvio.nfeStatus = 1;
                    pedidosDc.SaveChanges();
                }
            }
            catch (Exception)
            {

            }
            var data = new dbSiteEntities();
        }

        public static int GerarDigitoVerificadorNFe(string chave)
        {
            int soma = 0; // Vai guardar a Soma
            int mod = -1; // Vai guardar o Resto da divisão
            int dv = -1;  // Vai guardar o DigitoVerificador
            int pesso = 2; // vai guardar o pesso de multiplicacao

            //percorrendo cada caracter da chave da direita para esquerda para fazer os calculos com o pesso
            for (int i = chave.Length - 1; i != -1; i--)
            {
                int ch = Convert.ToInt32(chave[i].ToString());
                soma += ch * pesso;
                //sempre que for 9 voltamos o pesso a 2
                if (pesso < 9)
                    pesso += 1;
                else
                    pesso = 2;
            }

            //Agora que tenho a soma vamos pegar o resto da divisão por 11
            mod = soma % 11;
            //Aqui temos uma regrinha, se o resto da divisão for 0 ou 1 então o dv vai ser 0
            if (mod == 0 || mod == 1)
                dv = 0;
            else
                dv = 11 - mod;

            return dv;
        }

        private static string limitaTexto(string texto, int tamanho)
        {
            string textoLimpo = Regex.Replace(texto, "<(.|\\n)*?>", string.Empty);
            bool bl = textoLimpo.Length <= tamanho;
            string textoFormatado = textoLimpo;
            if (!bl)
            {
                textoFormatado = textoLimpo.Substring(0, tamanho);
            }
            return textoFormatado;
        }

    }
}
