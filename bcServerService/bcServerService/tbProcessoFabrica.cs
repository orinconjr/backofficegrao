//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace bcServerService
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbProcessoFabrica
    {
        public tbProcessoFabrica()
        {
            this.tbJuncaoProdutoProcessoFabrica = new HashSet<tbJuncaoProdutoProcessoFabrica>();
            this.tbSubProcessoFabrica = new HashSet<tbSubProcessoFabrica>();
        }
    
        public int idProcessoFabrica { get; set; }
        public string processo { get; set; }
    
        public virtual ICollection<tbJuncaoProdutoProcessoFabrica> tbJuncaoProdutoProcessoFabrica { get; set; }
        public virtual ICollection<tbSubProcessoFabrica> tbSubProcessoFabrica { get; set; }
    }
}
