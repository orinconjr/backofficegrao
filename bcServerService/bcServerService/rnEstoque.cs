﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Transactions;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;
using System.ComponentModel;

namespace bcServerService
{
    class rnEstoque
    {
        public static void AtualizarEstoqueRealProdutoQueue(long idQueue)
        {

            var dataInicio = DateTime.Now;

            #region atualizaQueue

            var data = new dbSiteEntities();
            var queue = (from c in data.tbQueue where c.idQueue == idQueue && c.andamento == false && c.concluido == false select c).FirstOrDefault();
            if (queue != null)
            {
                queue.andamento = true;
                queue.dataInicio = dataInicio;
                data.SaveChanges();

                #endregion atualizaQueue


                AtualizarEstoqueRealProduto((queue.idRelacionado ?? 0));
                var relacionados = (from c in data.tbProdutoRelacionado where c.idProdutoFilho == queue.idRelacionado && c.idProdutoPai != queue.idRelacionado select c.idProdutoPai).ToList();
                foreach (var relacionado in relacionados)
                {
                    AtualizarEstoqueRealProduto(relacionado);
                }

                #region finalizaQueue

                queue.concluido = true;
                queue.andamento = true;
                queue.dataConclusao = DateTime.Now;
                data.SaveChanges();

                var queuesRelacionados = (from c in data.tbQueue
                                          where
                                              c.tipoQueue == queue.tipoQueue && c.idRelacionado == queue.idRelacionado &&
                                              c.agendamento < dataInicio && c.andamento == false && c.concluido == false
                                          select c).ToList();
                foreach (var queueRelacionado in queuesRelacionados)
                {
                    queueRelacionado.dataInicio = dataInicio;
                    queueRelacionado.andamento = true;
                    queueRelacionado.concluido = true;
                    queueRelacionado.dataConclusao = queue.dataConclusao;
                }
                data.SaveChanges();

                #endregion

            }
        }
        public static void AtualizarEstoqueRealProduto(int produtoId)
        {
            var data = new dbSiteEntities();
            var estoqueAtual = (from c in data.tbProdutos where c.produtoId == produtoId select new { c.estoqueReal, c.prazoDeEntrega }).FirstOrDefault();
            if(estoqueAtual != null)
            {
                int estoque = EstoqueProduto(produtoId);
                if (estoqueAtual.estoqueReal != estoque)
                {
                    data.estoque_atualizaEstoquereal(produtoId, estoque);
                }


                /*int prazo = PrazoProduto(produtoId);
                if (estoqueAtual.prazoDeEntrega != prazo)
                {
                    data.estoque_atualizaPrazoDeEntrega(produtoId, prazo);
                }*/
            }
            /*try
            {
                var produto = (from c in dataAlt.tbProdutos where c.produtoId == produtoId select c).FirstOrDefault();
                if (produto != null)
                {
                    if (produto.estoqueReal != estoque)
                    {
                        produto.estoqueReal = estoque;
                        dataAlt.SaveChanges();
                    }
                }
                dataAlt.Dispose();
            }
            catch (Exception ex)
            {

            }*/
        }

        public static bool VerificaNecessidadeReservaEstoque(int itemPedidoId, int produtoId, int pedidoId)
        {
            using (var data = new dbSiteEntities())
            {
                var itemPedido = (from c in data.tbItensPedido where c.itemPedidoId == itemPedidoId select c).First();
                var pagamentos =
                    (from c in data.tbPedidoPagamento
                     where c.pedidoId == pedidoId && c.cancelado == false && c.pago == true
                     select c).Any();
                bool adicionarFila = false;
                if (itemPedido.tbPedidos.tipoDePagamentoId != 11 && pagamentos)
                {
                    adicionarFila = true;
                }
                else if (itemPedido.tbPedidos.tipoDePagamentoId == 11)
                {
                    if (pagamentos)
                    {
                        adicionarFila = VerificaNecessidadeReservaEstoquePlanoPaternidade(itemPedidoId, produtoId);
                    }
                }
                return adicionarFila;
            }
        }

        public static bool VerificaNecessidadeReservaEstoquePlanoPaternidade(int itemPedidoId, int produtoId)
        {
            using (var data = new dbSiteEntities())
            {
                DateTime dataEntregaHoje = DateTime.Now;
                DateTime dataEntregaProximo = DateTime.Now;

                var itemPedido = (from c in data.tbItensPedido where c.itemPedidoId == itemPedidoId select c).First();
                var ultimaParcela = (from c in data.tbPedidoPagamento
                                     where c.cancelado == false && c.pedidoId == itemPedido.pedidoId
                                     orderby c.dataVencimento descending
                                     select c).FirstOrDefault();
                if (ultimaParcela != null)
                {
                    var produto = (from c in data.tbProdutos where c.produtoId == produtoId select c).First();
                    var proximoPrazoFornecedor = DateTime.Now.AddDays(1);
                    bool gerarProximoPrazo = false;
                    while (gerarProximoPrazo == false)
                    {
                        int diaSemanaProximo = (int)proximoPrazoFornecedor.DayOfWeek;
                        if (produto.tbProdutoFornecedor.pedidoDomingo | produto.tbProdutoFornecedor.pedidoSegunda | produto.tbProdutoFornecedor.pedidoTerca |
                            produto.tbProdutoFornecedor.pedidoQuarta | produto.tbProdutoFornecedor.pedidoQuinta | produto.tbProdutoFornecedor.pedidoSexta | produto.tbProdutoFornecedor.pedidoSabado)
                        {
                            if (diaSemanaProximo == 0 && produto.tbProdutoFornecedor.pedidoDomingo) gerarProximoPrazo = true;
                            else if (diaSemanaProximo == 1 && produto.tbProdutoFornecedor.pedidoSegunda) gerarProximoPrazo = true;
                            else if (diaSemanaProximo == 2 && produto.tbProdutoFornecedor.pedidoTerca) gerarProximoPrazo = true;
                            else if (diaSemanaProximo == 3 && produto.tbProdutoFornecedor.pedidoQuarta) gerarProximoPrazo = true;
                            else if (diaSemanaProximo == 4 && produto.tbProdutoFornecedor.pedidoQuinta) gerarProximoPrazo = true;
                            else if (diaSemanaProximo == 5 && produto.tbProdutoFornecedor.pedidoSexta) gerarProximoPrazo = true;
                            else if (diaSemanaProximo == 6 && produto.tbProdutoFornecedor.pedidoSabado) gerarProximoPrazo = true;
                            else proximoPrazoFornecedor = proximoPrazoFornecedor.AddDays(1);
                        }
                        else
                        {
                            gerarProximoPrazo = true;
                        }
                    }

                    if (produto.tbProdutoFornecedor.fornecedorPrazoPedidos > 0)
                    {
                        int diasPrazo = Convert.ToInt32(produto.tbProdutoFornecedor.fornecedorPrazoPedidos);
                        int diasPrazoProximo = Convert.ToInt32(produto.tbProdutoFornecedor.fornecedorPrazoPedidos);

                        if ((produto.tbProdutoFornecedor.prazoFornecedorDiasUteis ?? false) == true)
                        {
                            int diasPrazoFinal = rnFuncoes.retornaPrazoDiasUteis(diasPrazo, 0, DateTime.Now);
                            diasPrazo = diasPrazoFinal;
                            int diasPrazoFinalProximo = rnFuncoes.retornaPrazoDiasUteis(diasPrazo, 1, proximoPrazoFornecedor);
                            diasPrazoProximo = diasPrazoFinalProximo;
                        }
                        dataEntregaHoje = DateTime.Now.AddDays(diasPrazo);
                        dataEntregaProximo = DateTime.Now.AddDays(diasPrazoProximo);
                    }
                    else
                    {
                        dataEntregaHoje = DateTime.Now.AddDays(1);
                        dataEntregaProximo = dataEntregaHoje.AddDays(1);
                    }
                    var gerarPedido = dataEntregaHoje < ultimaParcela.dataVencimento ? false : true;
                    if (!gerarPedido)
                    {
                        gerarPedido = dataEntregaProximo > ultimaParcela.dataVencimento ? true : false;
                    }

                    return gerarPedido;
                }
                return true;
            }
        }
        public static bool verificaReservasPedidoFornecedorCompleto(int pedidoId)
        {
            var data = new dbSiteEntities();
            var produtosAguardandoEstoque = (from c in data.tbItemPedidoEstoque
                                             where
                                                 c.dataLimite != null && c.cancelado == false && c.reservado == false &&
                                                 c.tbItensPedido.pedidoId == pedidoId
                                             select c).ToList();
            bool totalItensPedidos = true;
            foreach (var produtoAguardandoEstoque in produtosAguardandoEstoque)
            {
                if (totalItensPedidos)
                {
                    var aguardando = (from c in data.tbItemPedidoEstoque
                                      where
                                          c.dataLimite != null && c.cancelado == false && c.reservado == false &&
                                          c.dataLimite < produtoAguardandoEstoque.dataLimite
                                      select c).Count();
                    var pedidoFornecedor = (from c in data.tbPedidoFornecedorItem
                                            where
                                                (c.entregue == false | (c.entregue == true && (c.liberado ?? false) == false)) &&
                                                c.idProduto == produtoAguardandoEstoque.produtoId
                                            select c).Count();
                    if (aguardando >= pedidoFornecedor)
                    {
                        totalItensPedidos = false;
                    }
                }
            }

            return totalItensPedidos;


            var produtosDc = new dbSiteEntities();
            var pedidosDc = new dbSiteEntities();
            var itensDc = new dbSiteEntities();
            var itensPedido = (from c in itensDc.tbItensPedido where c.pedidoId == pedidoId select c);
            bool completo = true;

            foreach (var item in itensPedido)
            {
                bool cancelado = item.cancelado ?? false;
                if (!cancelado)
                {
                    var relacionados = (from c in produtosDc.tbProdutoRelacionado where c.idProdutoPai == item.produtoId select c);
                    if (relacionados.Any())
                    {
                        foreach (var relacionado in relacionados)
                        {
                            var reservasAtuais = (from c in produtosDc.tbProdutoReservaEstoque
                                                  where c.idPedido == pedidoId && c.idItemPedido == item.itemPedidoId && c.idProduto == relacionado.idProdutoFilho
                                                  select c).Count();
                            var pedidosAtuais = (from c in pedidosDc.tbPedidoFornecedorItem
                                                 where c.idPedido == pedidoId && c.idItemPedido == item.itemPedidoId && c.idProduto == relacionado.idProdutoFilho
                                                 select c).Count();
                            if (reservasAtuais + pedidosAtuais < Convert.ToInt32(item.itemQuantidade))
                            {
                                completo = false;
                            }
                        }
                    }
                    else
                    {
                        var reservasAtuais = (from c in produtosDc.tbProdutoReservaEstoque
                                              where c.idPedido == pedidoId && c.idItemPedido == item.itemPedidoId && c.idProduto == item.produtoId
                                              select c).Count();
                        var pedidosAtuais = (from c in pedidosDc.tbPedidoFornecedorItem
                                             where c.idPedido == pedidoId && c.idItemPedido == item.itemPedidoId && c.idProduto == item.produtoId
                                             select c).Count();
                        if (reservasAtuais + pedidosAtuais < Convert.ToInt32(item.itemQuantidade))
                        {
                            completo = false;
                        }
                    }

                }
            }

            return completo;
        }


        public static DateTime? RetornaPrazoPrevistoFornecedorAtual(int idItemPedidoEstoque)
        {
            var data = new dbSiteEntities();
            var itemEstoque =
                (from c in data.tbItemPedidoEstoque
                 where c.idItemPedidoEstoque == idItemPedidoEstoque
                 select c).First();
            int produtoId = itemEstoque.produtoId;

            var aguardandoGeral = (from c in data.tbItemPedidoEstoque
                                   where c.produtoId == produtoId && c.reservado == false && c.cancelado == false && c.dataLimite != null
                                   orderby c.dataLimite
                                   select new
                                   {
                                       c.idItemPedidoEstoque,
                                       c.tbItensPedido.pedidoId,
                                       c.dataCriacao,
                                       dataLimiteReserva = c.dataLimite,
                                       idProduto = c.produtoId,
                                       idItemPedido = c.itemPedidoId
                                   }).ToList();
            var pedidosFornecedorGeral = (from c in data.tbPedidoFornecedorItem
                                          join d in data.tbProdutoFornecedor on c.tbPedidoFornecedor.idFornecedor equals d.fornecedorId
                                          where (c.entregue == false | (c.entregue == true && (c.liberado ?? false) == false)) && c.idProduto == produtoId
                                          orderby c.tbPedidoFornecedor.dataLimite, c.tbPedidoFornecedor.idPedidoFornecedor
                                          select new
                                          {
                                              c.idPedidoFornecedor,
                                              c.idPedidoFornecedorItem,
                                              dataLimiteFornecedor = c.tbPedidoFornecedor.dataLimite,
                                              c.idProduto,
                                              d.fornecedorNome,
                                              c.entregue
                                          }).ToList();
            var listaFinal = (from c in aguardandoGeral.Select((item, index) => new { item, index })
                              join d in pedidosFornecedorGeral.Select((item, index) => new { item, index }) on c.index equals d.index into
                                  pedidosFornecedorListaInterna
                              from f in pedidosFornecedorListaInterna.DefaultIfEmpty()
                              select new
                              {
                                  c.item.idItemPedidoEstoque,
                                  c.item.pedidoId,
                                  c.item.dataCriacao,
                                  c.item.dataLimiteReserva,
                                  c.item.idItemPedido,
                                  entregue = f != null ? f.item.entregue : false,
                                  idPedidoFornecedor = f != null ? f.item.idPedidoFornecedor : 0,
                                  dataLimiteFornecedor = f != null ? (DateTime?)f.item.dataLimiteFornecedor : null,
                                  idPedidoFornecedorItem = f != null ? (int?)f.item.idPedidoFornecedorItem : null,
                                  fornecedorNome = f != null ? f.item.fornecedorNome : ""
                              }
                ).ToList();
            var itemNaLista = listaFinal.FirstOrDefault(x => x.idItemPedidoEstoque == idItemPedidoEstoque);
            if (itemNaLista != null)
            {
                return itemNaLista.dataLimiteFornecedor;
            }
            return null;
        }




        private static int EstoqueProduto(int produtoId)
        {
            using (var data = new dbSiteEntities())
            {
                int estoqueLivre = -1;
                var combo =
                    (from c in data.tbProdutoRelacionado where c.idProdutoPai == produtoId select new { c.idProdutoFilho })
                        .ToList();
                if (combo.Any())
                {
                    foreach (var itemCombo in combo)
                    {
                        if (estoqueLivre != 0)
                        {
                            int estoque = EstoqueLivreProdutoAvulso(itemCombo.idProdutoFilho);
                            if ((estoque < estoqueLivre) | estoqueLivre == -1)
                            {
                                estoqueLivre = estoque;
                                if (estoqueLivre < 0) estoqueLivre = 0;
                            }
                        }
                    }
                    return estoqueLivre;
                }
                return EstoqueLivreProdutoAvulso(produtoId);
            }
        }

        private static int PrazoProduto(int produtoId)
        {
            var data = new dbSiteEntities();
            var produtosComboGeral = (from c in data.tbProdutoRelacionado
                                      where c.idProdutoPai == produtoId 
                                      select new
                                      {
                                          c.idProdutoPai,
                                          c.idProdutoFilho,
                                          prazo = c.tbProdutos.tbProdutoFornecedor.fornecedorPrazoDeFabricacao ?? 1,
                                          c.tbProdutos.tbProdutoFornecedor.dataFimFabricacao,
                                          c.tbProdutos.tbProdutoFornecedor.dataInicioFabricacao
                                      }).ToList();
            int prazoAtualizado = 0;
            if (produtosComboGeral.Any())
            {
                foreach (var itemCombo in produtosComboGeral)
                {
                    int estoqueItemRelacionado = EstoqueProduto(itemCombo.idProdutoFilho);
                    if (estoqueItemRelacionado > 0)
                    {
                        produtosComboGeral = produtosComboGeral.Where(x => x.idProdutoFilho != itemCombo.idProdutoFilho).ToList();
                    }
                    if (itemCombo.dataFimFabricacao != null && itemCombo.dataFimFabricacao < DateTime.Now.AddDays(2) &&
                        itemCombo.dataInicioFabricacao != null && itemCombo.dataInicioFabricacao > DateTime.Now)
                    {
                        int prazo = rnFuncoes.retornaDiasUteisPeriodo(DateTime.Now, (DateTime)itemCombo.dataInicioFabricacao) + itemCombo.prazo;
                        if (prazo > prazoAtualizado) prazoAtualizado = prazo;
                    }
                }
                if (produtosComboGeral.OrderByDescending(x => x.prazo).First().prazo > prazoAtualizado) prazoAtualizado = produtosComboGeral.OrderByDescending(x => x.prazo).First().prazo;

            }
            else
            {
                var prazosProduto = (from c in data.tbProdutos
                                     where c.produtoId == produtoId
                                     select new
                                     {
                                         prazo = c.tbProdutoFornecedor.fornecedorPrazoDeFabricacao ?? 1,
                                         c.tbProdutoFornecedor.dataFimFabricacao,
                                         c.tbProdutoFornecedor.dataInicioFabricacao
                                     }).First();
                if (prazosProduto.dataFimFabricacao != null && prazosProduto.dataFimFabricacao < DateTime.Now.AddDays(2) &&
                    prazosProduto.dataInicioFabricacao != null && prazosProduto.dataInicioFabricacao > DateTime.Now)
                {
                    int prazo = rnFuncoes.retornaDiasUteisPeriodo(DateTime.Now, (DateTime)prazosProduto.dataInicioFabricacao) + prazosProduto.prazo;
                    prazoAtualizado = prazo;
                }
                else
                {
                    prazoAtualizado = prazosProduto.prazo;
                }
            }

            return prazoAtualizado;
        }

        private static int EstoqueLivreProdutoAvulso(int produtoId)
        {
            using (var data = new dbSiteEntities())
            {
                var aguardando = Convert.ToInt32(data.v1_ItensAguardandoEstoqueCount(produtoId).First());
                var estoque = Convert.ToInt32(data.v1_EstoqueFisicoCount(produtoId).First());
                int estoqueLivre = estoque - aguardando;
                if (estoqueLivre < 0) estoqueLivre = 0;
                return estoqueLivre;
            }
        }





        public static void bwAtualizarTodoEstoqueRunQueue(long idQueue)
        {

            #region atualizaQueue
            var dataQueue = new dbSiteEntities();
            var queue = (from c in dataQueue.tbQueue where c.idQueue == idQueue select c).First();
            queue.andamento = true;
            queue.dataInicio = DateTime.Now;
            dataQueue.SaveChanges();
            #endregion atualizaQueue


            bwAtualizarTodoEstoqueRun();



            #region finalizaQueue
            queue.concluido = true;
            queue.andamento = true;
            queue.dataConclusao = DateTime.Now;
            dataQueue.SaveChanges();

            var dataNovaQueue = Convert.ToDateTime(DateTime.Now.AddMinutes(120));
            var novoQueue = new tbQueue();
            novoQueue.agendamento = dataNovaQueue;
            novoQueue.tipoQueue = queue.tipoQueue;
            novoQueue.mensagem = "";
            novoQueue.concluido = false;
            novoQueue.andamento = false;
            dataQueue.tbQueue.Add(novoQueue);
            dataQueue.SaveChanges();

            #endregion
        }

        public static void bwAtualizarTodoEstoqueRun()
        {            

            var data = new dbSiteEntities();
            var produtos = new List<int>();
            var estoque = new List<v1_ListaEstoque_Result>();
            var produtosComboGeral = (from c in data.tbProdutoRelacionado
                                      where c.idProdutoPai == 0 && c.idProdutoRelacionado == 0
                                      select new
                                      {
                                          c.idProdutoPai,
                                          c.idProdutoFilho,
                                          prazo = c.tbProdutos.tbProdutoFornecedor.fornecedorPrazoDeFabricacao ?? 1,
                                          c.tbProdutos.tbProdutoFornecedor.dataFimFabricacao,
                                          c.tbProdutos.tbProdutoFornecedor.dataInicioFabricacao
                                      }).ToList();
            var todosPrazosProduto = (from c in data.tbProdutos
                                      where c.produtoId == 0
                                      select new
                                      {
                                          c.produtoId,
                                          prazo = c.tbProdutoFornecedor.fornecedorPrazoDeFabricacao ?? 1,
                                          c.tbProdutoFornecedor.dataFimFabricacao,
                                          c.tbProdutoFornecedor.dataInicioFabricacao
                                      }).ToList();

            using (var dataCol = new dbSiteEntities())
            {
                estoque = data.v1_ListaEstoque().ToList();
                produtos = (from c in dataCol.tbProdutos orderby c.estoqueReal descending, c.produtoId descending select c.produtoId).ToList();
                produtosComboGeral = (from c in dataCol.tbProdutoRelacionado
                                      select new
                                      {
                                          c.idProdutoPai,
                                          c.idProdutoFilho,
                                          prazo = c.tbProdutos.tbProdutoFornecedor.fornecedorPrazoDeFabricacao ?? 1,
                                          c.tbProdutos.tbProdutoFornecedor.dataFimFabricacao,
                                          c.tbProdutos.tbProdutoFornecedor.dataInicioFabricacao
                                      }).ToList();
                todosPrazosProduto = (from c in data.tbProdutos
                                      select new
                                      {
                                          c.produtoId,
                                          prazo = c.tbProdutoFornecedor.fornecedorPrazoDeFabricacao ?? 1,
                                          c.tbProdutoFornecedor.dataFimFabricacao,
                                          c.tbProdutoFornecedor.dataInicioFabricacao
                                      }).ToList();
            }


            int total = produtos.Count;
            int atual = 0;
            foreach (var produtolista in produtos)
            {
                try
                {
                    var dataAlt = new dbSiteEntities();
                    int estoqueLivre = 0;
                    bool estoqueIniciado = false;
                    var produtosCombo = (from c in produtosComboGeral
                                         where c.idProdutoPai == produtolista
                                         select new
                                         {
                                             c.idProdutoFilho,
                                             prazo = c.prazo,
                                             c.dataFimFabricacao,
                                             c.dataInicioFabricacao
                                         }).ToList();
                    var produtosComboPrazo = produtosCombo.ToList();
                    if (produtosCombo.Any())
                    {
                        foreach (var produtoRelacionado in produtosCombo)
                        {
                            var estoqueItemRelacionadoLista = estoque.FirstOrDefault(x => x.produtoId == produtoRelacionado.idProdutoFilho);
                            int estoqueItemRelacionado = estoqueItemRelacionadoLista == null
                                ? 0
                                : (int)estoqueItemRelacionadoLista.estoqueLivre;
                            if (estoqueIniciado == false)
                            {
                                estoqueLivre = estoqueItemRelacionado;
                                estoqueIniciado = true;
                            }
                            else
                            {
                                if (estoqueItemRelacionado < estoqueLivre)
                                {
                                    estoqueLivre = estoqueItemRelacionado;
                                }
                            }
                        }
                    }
                    else
                    {
                        var estoqueItemRelacionadoLista = estoque.FirstOrDefault(x => x.produtoId == produtolista);
                        int estoqueItemRelacionado = estoqueItemRelacionadoLista == null
                            ? 0
                            : (int)estoqueItemRelacionadoLista.estoqueLivre;
                        estoqueLivre = estoqueItemRelacionado;
                    }
                    if (estoqueLivre < 0) estoqueLivre = 0;

                    var produto = (from c in dataAlt.tbProdutos where c.produtoId == produtolista select c).FirstOrDefault();
                    if (produto != null)
                    {
                        if (produto.estoqueReal != estoqueLivre)
                        {
                            produto.estoqueReal = estoqueLivre;
                            dataAlt.SaveChanges();
                        }

                        /*if (produto.foraDeLinha && estoqueLivre < 1)
                        {
                            produto.produtoAtivo = "False";
                            dataAlt.SaveChanges();
                        }*/
                    }

                    int prazoAtualizado = 0;
                    if (produtosComboPrazo.Any())
                    {
                        foreach (var itemCombo in produtosComboPrazo)
                        {
                            var estoqueItemRelacionadoLista = estoque.FirstOrDefault(x => x.produtoId == itemCombo.idProdutoFilho);
                            int estoqueItemRelacionado = estoqueItemRelacionadoLista == null
                                ? 0
                                : (int)estoqueItemRelacionadoLista.estoqueLivre;
                            if (estoqueItemRelacionado > 0)
                            {
                                produtosComboPrazo = produtosComboPrazo.Where(x => x.idProdutoFilho != itemCombo.idProdutoFilho).ToList();
                            }
                            var prazosProduto = (from c in todosPrazosProduto
                                                 where c.produtoId == itemCombo.idProdutoFilho
                                                 select new
                                                 {
                                                     prazo = c.prazo,
                                                     c.dataFimFabricacao,
                                                     c.dataInicioFabricacao
                                                 }).First();
                            if (prazosProduto.dataFimFabricacao != null && prazosProduto.dataFimFabricacao < DateTime.Now.AddDays(2) &&
                                prazosProduto.dataInicioFabricacao != null && prazosProduto.dataInicioFabricacao > DateTime.Now)
                            {
                                int prazo = rnFuncoes.retornaDiasUteisPeriodo(DateTime.Now, (DateTime)prazosProduto.dataInicioFabricacao) + prazosProduto.prazo;
                                if (prazo > prazoAtualizado) prazoAtualizado = prazo;
                            }
                        }
                        if (produtosComboPrazo.OrderByDescending(x => x.prazo).First().prazo > prazoAtualizado) prazoAtualizado = produtosComboPrazo.OrderByDescending(x => x.prazo).First().prazo;

                    }
                    else
                    {
                        var prazosProduto = (from c in todosPrazosProduto
                                             where c.produtoId == produto.produtoId
                                             select new
                                             {
                                                 prazo = c.prazo,
                                                 c.dataFimFabricacao,
                                                 c.dataInicioFabricacao
                                             }).First();
                        if (prazosProduto.dataFimFabricacao != null && prazosProduto.dataFimFabricacao < DateTime.Now.AddDays(2) &&
                            prazosProduto.dataInicioFabricacao != null && prazosProduto.dataInicioFabricacao > DateTime.Now)
                        {
                            int prazo = rnFuncoes.retornaDiasUteisPeriodo(DateTime.Now, (DateTime)prazosProduto.dataInicioFabricacao) + prazosProduto.prazo;
                            prazoAtualizado = prazo;
                        }
                        else
                        {
                            prazoAtualizado = prazosProduto.prazo;
                        }
                    }

                    if (prazoAtualizado != produto.prazoDeEntrega)
                    {
                        var dataAlterar = new dbSiteEntities();
                        var produtoAlterar =
                            (from c in dataAlterar.tbProdutos where c.produtoId == produto.produtoId select c).First();
                        produtoAlterar.prazoDeEntrega = prazoAtualizado;
                        dataAlterar.SaveChanges();
                    }
                    dataAlt.Dispose();
                }
                catch (Exception ex)
                {

                }
            }

            
        }


    }
}
