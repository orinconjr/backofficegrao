﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace bcServerService
{
    public class rnConfiguracoes
    {
        public const string fromAddress = "atendimento@graodegente.com.br";
        public const string toAddress = "atendimento@graodegente.com.br";
        public const string emailPosVendas = "posvendas@graodegente.com.br";
        public const string emailLogin = "42a1276f-e120-4038-8f71-7b4606ae6424";
        public const string nomeDoSite = "Grão de Gente";
        public const string emailSmtp = "smtp.postmarkapp.com";
        public const string emailSenha = "42a1276f-e120-4038-8f71-7b4606ae6424";
        public const string caminhoVirtual = "https://www.graodegente.com.br/";
        public const string caminhoVirtualAmp = "https://www.graodegente.com.br/amp/";
        public const string caminhoVirtualSSL = "https://www.graodegente.com.br/";
        public const string caminhoVirtualAdmin = "http://admin.graodegente.com.br/";
        public const string caminhoFisico = "C:\\inetpub\\wwwroot\\";
        //public const string caminhoFisico = "C:\\";
        public const string caminhoXmls = "C:\\inetpub\\xmls\\";
        public const string ftpRichRelevance = "ftp://ftp.richrelevance.com/";
        public const string usuarioFtpRichRelevance = "graodegente";
        public const string senhaFtpRichRelevance = "dVKjQMg9";
        //public const string caminhoFisico = "C:\\reps\\graodegente\\programacao\\site";
        public const string caminhoCDN = "https://dmhxz00kguanp.cloudfront.net/";

        public const bool ambienteTeste = false;
        public const string emailDebug = "andre@bark.com.br";

        public static bool ativarBoletoPagamentoV2 = Convert.ToBoolean(ConfigurationManager.AppSettings["ServicePagamentoAtivarBoleto"]);
        public static string ServicePagamentoUrl = ConfigurationManager.AppSettings["ServicePagamentoUrl"];


        public static bool AmbienteDebugProcessaPagamento = Convert.ToBoolean(ConfigurationManager.AppSettings["AmbienteDebugProcessaPagamento"]);
        public static bool AmbienteDebugUtilizarEstoque = Convert.ToBoolean(ConfigurationManager.AppSettings["AmbienteDebugUtilizarEstoque"]);
        public static bool AmbienteDebugEnviarEmailConfirmacao = Convert.ToBoolean(ConfigurationManager.AppSettings["AmbienteDebugEnviarEmailConfirmacao"]);
        public static bool AmbienteDebugCalcularPrazoMaximoPedido = Convert.ToBoolean(ConfigurationManager.AppSettings["AmbienteDebugCalcularPrazoMaximoPedido"]);
        public static bool AmbienteDebugReservarEstoque = Convert.ToBoolean(ConfigurationManager.AppSettings["AmbienteDebugReservarEstoque"]);
        public static bool AmbienteDebugGravarItensCombo = Convert.ToBoolean(ConfigurationManager.AppSettings["AmbienteDebugGravarItensCombo"]);
        public static bool AmbienteDebugEmailTnt = Convert.ToBoolean(ConfigurationManager.AppSettings["AmbienteDebugEmailTnt"]);
        public static bool AmbienteDebugEmailCarregamento = Convert.ToBoolean(ConfigurationManager.AppSettings["AmbienteDebugEmailCarregamento"]);
        public static bool AmbienteDebugPedidoFornecedor = Convert.ToBoolean(ConfigurationManager.AppSettings["AmbienteDebugPedidoFornecedor"]);

        public static bool AmbienteDebugTimerAtualizarEstoque = Convert.ToBoolean(ConfigurationManager.AppSettings["AmbienteDebugTimerAtualizarEstoque"]);
        public static bool AmbienteDebugTimerCancelarPedidos = Convert.ToBoolean(ConfigurationManager.AppSettings["AmbienteDebugTimerCancelarPedidos"]);
        public static bool AmbienteDebugTimerChecaCompletos = Convert.ToBoolean(ConfigurationManager.AppSettings["AmbienteDebugTimerChecaCompletos"]);
        public static bool AmbienteDebugTimerVerificaEstoque = Convert.ToBoolean(ConfigurationManager.AppSettings["AmbienteDebugTimerVerificaEstoque"]);
        public static bool AmbienteDebugPagamentos = Convert.ToBoolean(ConfigurationManager.AppSettings["AmbienteDebugPagamentos"]);
        public static bool AmbienteDebugAtualizarRelevancia = Convert.ToBoolean(ConfigurationManager.AppSettings["AmbienteDebugAtualizarRelevancia"]);
        public static bool AmbienteDebugCarregamentoJadLog = Convert.ToBoolean(ConfigurationManager.AppSettings["AmbienteDebugCarregamentoJadLog"]);
        public static bool AmbienteDebugAtualizarElastic = Convert.ToBoolean(ConfigurationManager.AppSettings["AmbienteDebugAtualizarElastic"]);


        public static bool AtualizarElasticSearch = Convert.ToBoolean(ConfigurationManager.AppSettings["AtualizarElasticSearch"]);
        public static bool AtualizarCloudSearch = Convert.ToBoolean(ConfigurationManager.AppSettings["AtualizarCloudSearch"]);
        
        public static bool HabilitarTimerDesativarProdutos = Convert.ToBoolean(ConfigurationManager.AppSettings["HabilitarTimerDesativarProdutos"]);
        public static bool HabilitarTimerAtualizarEstoque = Convert.ToBoolean(ConfigurationManager.AppSettings["HabilitarTimerAtualizarEstoque"]);
        public static bool HabilitarTimerCancelarPedidos = Convert.ToBoolean(ConfigurationManager.AppSettings["HabilitarTimerCancelarPedidos"]);
        public static bool HabilitarTimerChecarCompletos = Convert.ToBoolean(ConfigurationManager.AppSettings["HabilitarTimerChecarCompletos"]);
        public static bool HabilitarTimerVerificaEstoque = Convert.ToBoolean(ConfigurationManager.AppSettings["HabilitarTimerVerificaEstoque"]);
        public static bool HabilitarTimerPendenciasJadlog = Convert.ToBoolean(ConfigurationManager.AppSettings["HabilitarTimerPendenciasJadlog"]);
        public static bool HabilitarTimerGnre = Convert.ToBoolean(ConfigurationManager.AppSettings["HabilitarTimerGnre"]);
        public static bool HabilitarTimerFeeds = Convert.ToBoolean(ConfigurationManager.AppSettings["HabilitarTimerFeeds"]);
        public static bool HabilitarTimerCarregamentoJadlog = Convert.ToBoolean(ConfigurationManager.AppSettings["HabilitarTimerCarregamentoJadlog"]);
        public static bool HabilitarTimerNotas = Convert.ToBoolean(ConfigurationManager.AppSettings["HabilitarTimerNotas"]);
        public static bool HabilitarTimerPrazo = Convert.ToBoolean(ConfigurationManager.AppSettings["HabilitarTimerPrazo"]);
        public static bool HabilitarPendentesTnt = Convert.ToBoolean(ConfigurationManager.AppSettings["HabilitarPendentesTnt"]);
        public static bool HabilitarRastreamentoTnt = Convert.ToBoolean(ConfigurationManager.AppSettings["HabilitarRastreamentoTnt"]);
        public static bool HabilitarRastreamentoJadlog = Convert.ToBoolean(ConfigurationManager.AppSettings["HabilitarRastreamentoJadlog"]);
        public static bool HabilitarRastreamentoCorreios = Convert.ToBoolean(ConfigurationManager.AppSettings["HabilitarRastreamentoCorreios"]);
        public static bool HabilitarTimerClearsale = Convert.ToBoolean(ConfigurationManager.AppSettings["HabilitarTimerClearsale"]);
        public static bool HabilitarTimerClearsalePendentes = Convert.ToBoolean(ConfigurationManager.AppSettings["HabilitarTimerClearsalePendentes"]);
        public static bool HabilitarTimerProceda = Convert.ToBoolean(ConfigurationManager.AppSettings["HabilitarTimerProceda"]);
        public static bool HabilitarTimerLoteCobranca = Convert.ToBoolean(ConfigurationManager.AppSettings["HabilitarTimerLoteCobranca"]);
        public static bool HabilitarTimerRegistroBoleto = Convert.ToBoolean(ConfigurationManager.AppSettings["HabilitarTimerRegistroBoleto"]);
        public static bool HabilitarTimerProdutos = Convert.ToBoolean(ConfigurationManager.AppSettings["HabilitarTimerProdutos"]);
        public static bool HabilitarTimerQueue = Convert.ToBoolean(ConfigurationManager.AppSettings["HabilitarTimerQueue"]);
        public static bool HabilitarTimerQueuesParalelos = Convert.ToBoolean(ConfigurationManager.AppSettings["HabilitarTimerQueuesParalelos"]);

        


        public static bool HabilitarQueue0 = Convert.ToBoolean(ConfigurationManager.AppSettings["HabilitarQueue0"]);
        public static bool HabilitarQueue1 = Convert.ToBoolean(ConfigurationManager.AppSettings["HabilitarQueue1"]);
        public static bool HabilitarQueue2 = Convert.ToBoolean(ConfigurationManager.AppSettings["HabilitarQueue2"]);
        public static bool HabilitarQueue3 = Convert.ToBoolean(ConfigurationManager.AppSettings["HabilitarQueue3"]);
        public static bool HabilitarQueue4 = Convert.ToBoolean(ConfigurationManager.AppSettings["HabilitarQueue4"]);
        public static bool HabilitarQueue5 = Convert.ToBoolean(ConfigurationManager.AppSettings["HabilitarQueue5"]);
        public static bool HabilitarQueue6 = Convert.ToBoolean(ConfigurationManager.AppSettings["HabilitarQueue6"]);
        public static bool HabilitarQueue7 = Convert.ToBoolean(ConfigurationManager.AppSettings["HabilitarQueue7"]);
        public static bool HabilitarQueue8 = Convert.ToBoolean(ConfigurationManager.AppSettings["HabilitarQueue8"]);
        public static bool HabilitarQueue9 = Convert.ToBoolean(ConfigurationManager.AppSettings["HabilitarQueue9"]);
        public static bool HabilitarQueue10 = Convert.ToBoolean(ConfigurationManager.AppSettings["HabilitarQueue10"]);
        public static bool HabilitarQueue11 = Convert.ToBoolean(ConfigurationManager.AppSettings["HabilitarQueue11"]);
        public static bool HabilitarQueue12 = Convert.ToBoolean(ConfigurationManager.AppSettings["HabilitarQueue12"]);
        public static bool HabilitarQueue13 = Convert.ToBoolean(ConfigurationManager.AppSettings["HabilitarQueue13"]);
        public static bool HabilitarQueue14 = Convert.ToBoolean(ConfigurationManager.AppSettings["HabilitarQueue14"]);
        public static bool HabilitarQueue15 = Convert.ToBoolean(ConfigurationManager.AppSettings["HabilitarQueue15"]);
        public static bool HabilitarQueue16 = Convert.ToBoolean(ConfigurationManager.AppSettings["HabilitarQueue16"]);
        public static bool HabilitarQueue17 = Convert.ToBoolean(ConfigurationManager.AppSettings["HabilitarQueue17"]);
        public static bool HabilitarQueue18 = Convert.ToBoolean(ConfigurationManager.AppSettings["HabilitarQueue18"]);
        public static bool HabilitarQueue19 = Convert.ToBoolean(ConfigurationManager.AppSettings["HabilitarQueue19"]);
        public static bool HabilitarQueue20 = Convert.ToBoolean(ConfigurationManager.AppSettings["HabilitarQueue20"]);
        public static bool HabilitarQueue21 = Convert.ToBoolean(ConfigurationManager.AppSettings["HabilitarQueue21"]);
        public static bool HabilitarQueue22 = Convert.ToBoolean(ConfigurationManager.AppSettings["HabilitarQueue22"]);
        public static bool HabilitarQueue23 = Convert.ToBoolean(ConfigurationManager.AppSettings["HabilitarQueue23"]);
        public static bool HabilitarQueue24 = Convert.ToBoolean(ConfigurationManager.AppSettings["HabilitarQueue24"]);
        public static bool HabilitarQueue25 = Convert.ToBoolean(ConfigurationManager.AppSettings["HabilitarQueue25"]);
        public static bool HabilitarQueue26 = Convert.ToBoolean(ConfigurationManager.AppSettings["HabilitarQueue26"]);
        public static bool HabilitarQueue27 = Convert.ToBoolean(ConfigurationManager.AppSettings["HabilitarQueue27"]);
        public static bool HabilitarQueue28 = Convert.ToBoolean(ConfigurationManager.AppSettings["HabilitarQueue28"]);
        public static bool HabilitarQueue29 = Convert.ToBoolean(ConfigurationManager.AppSettings["HabilitarQueue29"]);
        public static bool HabilitarQueue30 = Convert.ToBoolean(ConfigurationManager.AppSettings["HabilitarQueue30"]);
        public static bool HabilitarQueue31 = Convert.ToBoolean(ConfigurationManager.AppSettings["HabilitarQueue31"]);
        public static bool HabilitarQueue32 = Convert.ToBoolean(ConfigurationManager.AppSettings["HabilitarQueue32"]);
        public static bool HabilitarQueue33 = Convert.ToBoolean(ConfigurationManager.AppSettings["HabilitarQueue33"]);
        public static bool HabilitarQueue34 = Convert.ToBoolean(ConfigurationManager.AppSettings["HabilitarQueue34"]);
        public static bool HabilitarQueue35 = Convert.ToBoolean(ConfigurationManager.AppSettings["HabilitarQueue35"]);
        public static bool HabilitarQueue36 = Convert.ToBoolean(ConfigurationManager.AppSettings["HabilitarQueue36"]);
        public static bool HabilitarQueue37 = Convert.ToBoolean(ConfigurationManager.AppSettings["HabilitarQueue37"]);
        public static bool HabilitarQueue38 = Convert.ToBoolean(ConfigurationManager.AppSettings["HabilitarQueue38"]);
        public static bool HabilitarQueue39 = Convert.ToBoolean(ConfigurationManager.AppSettings["HabilitarQueue39"]);
        public static bool HabilitarQueue40 = Convert.ToBoolean(ConfigurationManager.AppSettings["HabilitarQueue40"]);
        public static bool HabilitarQueue41 = Convert.ToBoolean(ConfigurationManager.AppSettings["HabilitarQueue41"]);
        public static bool HabilitarQueue42 = Convert.ToBoolean(ConfigurationManager.AppSettings["HabilitarQueue42"]);
        public static bool HabilitarQueue43 = Convert.ToBoolean(ConfigurationManager.AppSettings["HabilitarQueue43"]);
        public static bool HabilitarQueue44 = Convert.ToBoolean(ConfigurationManager.AppSettings["HabilitarQueue44"]);
        public static bool HabilitarQueue45 = Convert.ToBoolean(ConfigurationManager.AppSettings["HabilitarQueue45"]);
        public static bool HabilitarQueue46 = Convert.ToBoolean(ConfigurationManager.AppSettings["HabilitarQueue46"]);
        public static bool HabilitarQueue47 = Convert.ToBoolean(ConfigurationManager.AppSettings["HabilitarQueue47"]);



        public static string ContratoMaxipago1 = ConfigurationManager.AppSettings["ContratoMaxipago1"];


        public static string bucketName = ConfigurationManager.AppSettings["bucketName"];
        public static string accessKey = ConfigurationManager.AppSettings["accessKey"];
        public static string secretAccessKey = ConfigurationManager.AppSettings["secretAccessKey"];
        public static string s3Region = ConfigurationManager.AppSettings["s3Region"];
    }
}
