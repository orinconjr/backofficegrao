﻿namespace bcServerService
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lblStatus = new System.Windows.Forms.Label();
            this.lblStatusGerarPedido = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.timerQueue = new System.Windows.Forms.Timer(this.components);
            this.bwPedidoFornecedor = new System.ComponentModel.BackgroundWorker();
            this.timerAtualizarEstoque = new System.Windows.Forms.Timer(this.components);
            this.dwAtualizarEstoque = new System.ComponentModel.BackgroundWorker();
            this.bwCancelarPedidos = new System.ComponentModel.BackgroundWorker();
            this.timerCancelarPedidos = new System.Windows.Forms.Timer(this.components);
            this.timerChecaCompletos = new System.Windows.Forms.Timer(this.components);
            this.bwChecaCompletos = new System.ComponentModel.BackgroundWorker();
            this.timerVerificaEstoque = new System.Windows.Forms.Timer(this.components);
            this.timerPendenciasJadLog = new System.Windows.Forms.Timer(this.components);
            this.bwPendenciasJadLog = new System.ComponentModel.BackgroundWorker();
            this.bwEmailTnt = new System.ComponentModel.BackgroundWorker();
            this.bwEnviaEmailCarregamento = new System.ComponentModel.BackgroundWorker();
            this.lblEstoque = new System.Windows.Forms.Label();
            this.lblErro = new System.Windows.Forms.Label();
            this.bwUtilizarEstoque = new System.ComponentModel.BackgroundWorker();
            this.lblChecarCompletos = new System.Windows.Forms.Label();
            this.bwListaTransferencia = new System.ComponentModel.BackgroundWorker();
            this.btnChecarCompletos = new System.Windows.Forms.Button();
            this.bwAtualizarProdutoElastic = new System.ComponentModel.BackgroundWorker();
            this.bwAtualizarCategoriaElastic = new System.ComponentModel.BackgroundWorker();
            this.bwAtualizarRelevancia = new System.ComponentModel.BackgroundWorker();
            this.lblRelevancia = new System.Windows.Forms.Label();
            this.bwAtualizarTodosElastic = new System.ComponentModel.BackgroundWorker();
            this.lblElastic = new System.Windows.Forms.Label();
            this.bwAtualizarFornecedorElastic = new System.ComponentModel.BackgroundWorker();
            this.timerFeeds = new System.Windows.Forms.Timer(this.components);
            this.bwFeeds = new System.ComponentModel.BackgroundWorker();
            this.bwCarregamentoJadLog = new System.ComponentModel.BackgroundWorker();
            this.timerCarramentoJadLog = new System.Windows.Forms.Timer(this.components);
            this.lblCarregamentoJadLog = new System.Windows.Forms.Label();
            this.btnChecarEnviosJadLog = new System.Windows.Forms.Button();
            this.lblProcessoEmAndamento1 = new System.Windows.Forms.Label();
            this.lblProcessoEmAndamento2 = new System.Windows.Forms.Label();
            this.lblProcessoEmAndamento3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.bwGerarFilaEstoque = new System.ComponentModel.BackgroundWorker();
            this.btnFechar = new System.Windows.Forms.Button();
            this.btnGerarRelatorioFila = new System.Windows.Forms.Button();
            this.bwGerarRelatorioFila = new System.ComponentModel.BackgroundWorker();
            this.lblAndamentoRelatorio = new System.Windows.Forms.Label();
            this.btnFeeds = new System.Windows.Forms.Button();
            this.btnJadlogAgora = new System.Windows.Forms.Button();
            this.bwBlack = new System.ComponentModel.BackgroundWorker();
            this.button1 = new System.Windows.Forms.Button();
            this.timerNotas = new System.Windows.Forms.Timer(this.components);
            this.bwNotas = new System.ComponentModel.BackgroundWorker();
            this.bwAtualizarPrazo = new System.ComponentModel.BackgroundWorker();
            this.timerPrazo = new System.Windows.Forms.Timer(this.components);
            this.label5 = new System.Windows.Forms.Label();
            this.lblPrazos = new System.Windows.Forms.Label();
            this.btnPrazos = new System.Windows.Forms.Button();
            this.bwConfirmarDados = new System.ComponentModel.BackgroundWorker();
            this.btnDepoimentos = new System.Windows.Forms.Button();
            this.bwDepoimentos = new System.ComponentModel.BackgroundWorker();
            this.timerGnre = new System.Windows.Forms.Timer(this.components);
            this.bwGnre = new System.ComponentModel.BackgroundWorker();
            this.bwLoteBanco = new System.ComponentModel.BackgroundWorker();
            this.timerHabilitarRastreamentoTnt = new System.Windows.Forms.Timer(this.components);
            this.timerRastreamentoJadlog = new System.Windows.Forms.Timer(this.components);
            this.timerRastreamentoCorreios = new System.Windows.Forms.Timer(this.components);
            this.timerClearsale = new System.Windows.Forms.Timer(this.components);
            this.bwCompactarFotoKraken = new System.ComponentModel.BackgroundWorker();
            this.bwEmailBelle = new System.ComponentModel.BackgroundWorker();
            this.timerQueueProdutos = new System.Windows.Forms.Timer(this.components);
            this.btnGerarPedidoCarolinaMoveis = new System.Windows.Forms.Button();
            this.bwGerarPedidoCarolinaMoveis = new System.ComponentModel.BackgroundWorker();
            this.timerUpdateClearPagamentos = new System.Windows.Forms.Timer(this.components);
            this.lblLastListUpdate = new System.Windows.Forms.Label();
            this.bwChecarPedidoCompleto = new System.ComponentModel.BackgroundWorker();
            this.timerProceda = new System.Windows.Forms.Timer(this.components);
            this.timerLoteBancario = new System.Windows.Forms.Timer(this.components);
            this.button2 = new System.Windows.Forms.Button();
            this.timerCache = new System.Windows.Forms.Timer(this.components);
            this.lblUltimaAtualizacaoEstoque = new System.Windows.Forms.Label();
            this.timerEstoqueAvulso = new System.Windows.Forms.Timer(this.components);
            this.bwEmailPlimor = new System.ComponentModel.BackgroundWorker();
            this.label6 = new System.Windows.Forms.Label();
            this.lblCancelamentoPedidos = new System.Windows.Forms.Label();
            this.btnZerarTimerCancelamentoPedido = new System.Windows.Forms.Button();
            this.btnAtivarTimerCancelamentoProdutos = new System.Windows.Forms.Button();
            this.btnAtivarTimerPedidosCompletos = new System.Windows.Forms.Button();
            this.btnZerarTimerPedidosCompletos = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.lblStatusCompleto = new System.Windows.Forms.Label();
            this.timerEmailAndamento = new System.Windows.Forms.Timer(this.components);
            this.bwEmailAndamento = new System.ComponentModel.BackgroundWorker();
            this.bwCartao = new System.ComponentModel.BackgroundWorker();
            this.timerRegistroBoleto = new System.Windows.Forms.Timer(this.components);
            this.bwRegistroBoleto = new System.ComponentModel.BackgroundWorker();
            this.bwEnviarNotaTransportadora = new System.ComponentModel.BackgroundWorker();
            this.bwEnviarEmailsConfirmacaoPedido = new System.ComponentModel.BackgroundWorker();
            this.bwProcessarPagamentos = new System.ComponentModel.BackgroundWorker();
            this.label8 = new System.Windows.Forms.Label();
            this.lblNotasTransportadora = new System.Windows.Forms.Label();
            this.timerQueuesParalelos = new System.Windows.Forms.Timer(this.components);
            this.bwQueuesParalelos = new System.ComponentModel.BackgroundWorker();
            this.bwAtualizarEstoqueReal = new System.ComponentModel.BackgroundWorker();
            this.bwGerarSimulacaoFrete = new System.ComponentModel.BackgroundWorker();
            this.timerLembreteBoleto = new System.Windows.Forms.Timer(this.components);
            this.bwLembreteBoleto = new System.ComponentModel.BackgroundWorker();
            this.SuspendLayout();
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus.Location = new System.Drawing.Point(12, 9);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(431, 31);
            this.lblStatus.TabIndex = 1;
            this.lblStatus.Text = "Aguardando Checagem de Queue ";
            // 
            // lblStatusGerarPedido
            // 
            this.lblStatusGerarPedido.AutoSize = true;
            this.lblStatusGerarPedido.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatusGerarPedido.Location = new System.Drawing.Point(209, 53);
            this.lblStatusGerarPedido.Name = "lblStatusGerarPedido";
            this.lblStatusGerarPedido.Size = new System.Drawing.Size(240, 24);
            this.lblStatusGerarPedido.TabIndex = 2;
            this.lblStatusGerarPedido.Text = "Aguardando Agendamento";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(14, 53);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(189, 24);
            this.label1.TabIndex = 3;
            this.label1.Text = "Pedidos Fornecedor:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(14, 88);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(157, 24);
            this.label2.TabIndex = 5;
            this.label2.Text = "Rastreio Pedidos:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(209, 88);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(240, 24);
            this.label3.TabIndex = 4;
            this.label3.Text = "Aguardando Agendamento";
            // 
            // timerQueue
            // 
            this.timerQueue.Interval = 1;
            this.timerQueue.Tick += new System.EventHandler(this.timerQueue_Tick);
            // 
            // bwPedidoFornecedor
            // 
            this.bwPedidoFornecedor.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwPedidoFornecedor_DoWork);
            this.bwPedidoFornecedor.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bwPedidoFornecedor_RunWorkerCompleted);
            // 
            // timerAtualizarEstoque
            // 
            this.timerAtualizarEstoque.Interval = 3600000;
            this.timerAtualizarEstoque.Tick += new System.EventHandler(this.timerAtualizarEstoque_Tick);
            // 
            // dwAtualizarEstoque
            // 
            this.dwAtualizarEstoque.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.dwAtualizarEstoque_RunWorkerCompleted);
            // 
            // bwCancelarPedidos
            // 
            this.bwCancelarPedidos.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwCancelarPedidos_DoWork);
            this.bwCancelarPedidos.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bwCancelarPedidos_RunWorkerCompleted);
            // 
            // timerCancelarPedidos
            // 
            this.timerCancelarPedidos.Interval = 1;
            this.timerCancelarPedidos.Tick += new System.EventHandler(this.timerCancelarPedidos_Tick);
            // 
            // timerChecaCompletos
            // 
            this.timerChecaCompletos.Interval = 7200000;
            this.timerChecaCompletos.Tick += new System.EventHandler(this.timerChecaCompletos_Tick);
            // 
            // bwChecaCompletos
            // 
            this.bwChecaCompletos.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwChecaCompletos_DoWork);
            this.bwChecaCompletos.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bwChecaCompletos_RunWorkerCompleted);
            // 
            // timerVerificaEstoque
            // 
            this.timerVerificaEstoque.Interval = 1800000;
            this.timerVerificaEstoque.Tick += new System.EventHandler(this.timerVerificaEstoque_Tick);
            // 
            // timerPendenciasJadLog
            // 
            this.timerPendenciasJadLog.Interval = 1;
            this.timerPendenciasJadLog.Tick += new System.EventHandler(this.timerPendenciasJadLog_Tick);
            // 
            // bwPendenciasJadLog
            // 
            this.bwPendenciasJadLog.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwPendenciasJadLog_DoWork);
            this.bwPendenciasJadLog.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bwPendenciasJadLog_RunWorkerCompleted);
            // 
            // bwEmailTnt
            // 
            this.bwEmailTnt.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwEmailTnt_DoWork);
            // 
            // bwEnviaEmailCarregamento
            // 
            this.bwEnviaEmailCarregamento.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwEnviaEmailCarregamento_DoWork);
            // 
            // lblEstoque
            // 
            this.lblEstoque.AutoSize = true;
            this.lblEstoque.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEstoque.Location = new System.Drawing.Point(14, 121);
            this.lblEstoque.Name = "lblEstoque";
            this.lblEstoque.Size = new System.Drawing.Size(115, 24);
            this.lblEstoque.TabIndex = 8;
            this.lblEstoque.Text = "Aguardando";
            // 
            // lblErro
            // 
            this.lblErro.AutoSize = true;
            this.lblErro.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblErro.Location = new System.Drawing.Point(169, 121);
            this.lblErro.Name = "lblErro";
            this.lblErro.Size = new System.Drawing.Size(115, 24);
            this.lblErro.TabIndex = 9;
            this.lblErro.Text = "Aguardando";
            // 
            // bwUtilizarEstoque
            // 
            this.bwUtilizarEstoque.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwUtilizarEstoque_DoWork);
            this.bwUtilizarEstoque.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bwUtilizarEstoque_RunWorkerCompleted);
            // 
            // lblChecarCompletos
            // 
            this.lblChecarCompletos.AutoSize = true;
            this.lblChecarCompletos.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChecarCompletos.Location = new System.Drawing.Point(14, 158);
            this.lblChecarCompletos.Name = "lblChecarCompletos";
            this.lblChecarCompletos.Size = new System.Drawing.Size(115, 24);
            this.lblChecarCompletos.TabIndex = 10;
            this.lblChecarCompletos.Text = "Aguardando";
            // 
            // bwListaTransferencia
            // 
            this.bwListaTransferencia.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwListaTransferencia_DoWork);
            this.bwListaTransferencia.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bwListaTransferencia_RunWorkerCompleted);
            // 
            // btnChecarCompletos
            // 
            this.btnChecarCompletos.Location = new System.Drawing.Point(18, 301);
            this.btnChecarCompletos.Name = "btnChecarCompletos";
            this.btnChecarCompletos.Size = new System.Drawing.Size(137, 33);
            this.btnChecarCompletos.TabIndex = 11;
            this.btnChecarCompletos.Text = "Checar Completos";
            this.btnChecarCompletos.UseVisualStyleBackColor = true;
            this.btnChecarCompletos.Click += new System.EventHandler(this.btnChecarCompletos_Click);
            // 
            // bwAtualizarProdutoElastic
            // 
            this.bwAtualizarProdutoElastic.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwAtualizarProdutoElastic_DoWork);
            this.bwAtualizarProdutoElastic.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bwAtualizarProdutoElastic_RunWorkerCompleted);
            // 
            // bwAtualizarCategoriaElastic
            // 
            this.bwAtualizarCategoriaElastic.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwAtualizarCategoriaElastic_DoWork);
            this.bwAtualizarCategoriaElastic.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bwAtualizarCategoriaElastic_RunWorkerCompleted);
            // 
            // bwAtualizarRelevancia
            // 
            this.bwAtualizarRelevancia.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwAtualizarRelevancia_DoWork);
            this.bwAtualizarRelevancia.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bwAtualizarRelevancia_RunWorkerCompleted);
            // 
            // lblRelevancia
            // 
            this.lblRelevancia.AutoSize = true;
            this.lblRelevancia.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRelevancia.Location = new System.Drawing.Point(14, 194);
            this.lblRelevancia.Name = "lblRelevancia";
            this.lblRelevancia.Size = new System.Drawing.Size(103, 24);
            this.lblRelevancia.TabIndex = 12;
            this.lblRelevancia.Text = "Relevancia";
            // 
            // bwAtualizarTodosElastic
            // 
            this.bwAtualizarTodosElastic.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwAtualizarTodosElastic_DoWork);
            this.bwAtualizarTodosElastic.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bwAtualizarTodosElastic_RunWorkerCompleted);
            // 
            // lblElastic
            // 
            this.lblElastic.AutoSize = true;
            this.lblElastic.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblElastic.Location = new System.Drawing.Point(14, 226);
            this.lblElastic.Name = "lblElastic";
            this.lblElastic.Size = new System.Drawing.Size(64, 24);
            this.lblElastic.TabIndex = 13;
            this.lblElastic.Text = "Elastic";
            // 
            // bwAtualizarFornecedorElastic
            // 
            this.bwAtualizarFornecedorElastic.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwAtualizarFornecedorElastic_DoWork);
            // 
            // timerFeeds
            // 
            this.timerFeeds.Interval = 7200000;
            this.timerFeeds.Tick += new System.EventHandler(this.timerFeeds_Tick);
            // 
            // bwFeeds
            // 
            this.bwFeeds.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwFeeds_DoWork);
            this.bwFeeds.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bwFeeds_RunWorkerCompleted);
            // 
            // bwCarregamentoJadLog
            // 
            this.bwCarregamentoJadLog.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwCarregamentoJadLog_DoWork);
            this.bwCarregamentoJadLog.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bwCarregamentoJadLog_RunWorkerCompleted);
            // 
            // timerCarramentoJadLog
            // 
            this.timerCarramentoJadLog.Interval = 1;
            this.timerCarramentoJadLog.Tick += new System.EventHandler(this.timerCarramentoJadLog_Tick);
            // 
            // lblCarregamentoJadLog
            // 
            this.lblCarregamentoJadLog.AutoSize = true;
            this.lblCarregamentoJadLog.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCarregamentoJadLog.Location = new System.Drawing.Point(14, 260);
            this.lblCarregamentoJadLog.Name = "lblCarregamentoJadLog";
            this.lblCarregamentoJadLog.Size = new System.Drawing.Size(197, 24);
            this.lblCarregamentoJadLog.TabIndex = 14;
            this.lblCarregamentoJadLog.Text = "Carregamento JadLog";
            // 
            // btnChecarEnviosJadLog
            // 
            this.btnChecarEnviosJadLog.Location = new System.Drawing.Point(18, 340);
            this.btnChecarEnviosJadLog.Name = "btnChecarEnviosJadLog";
            this.btnChecarEnviosJadLog.Size = new System.Drawing.Size(137, 33);
            this.btnChecarEnviosJadLog.TabIndex = 15;
            this.btnChecarEnviosJadLog.Text = "Checar Envios JadLog";
            this.btnChecarEnviosJadLog.UseVisualStyleBackColor = true;
            this.btnChecarEnviosJadLog.Click += new System.EventHandler(this.btnChecarEnviosJadLog_Click);
            // 
            // lblProcessoEmAndamento1
            // 
            this.lblProcessoEmAndamento1.AutoSize = true;
            this.lblProcessoEmAndamento1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.lblProcessoEmAndamento1.Location = new System.Drawing.Point(18, 387);
            this.lblProcessoEmAndamento1.Name = "lblProcessoEmAndamento1";
            this.lblProcessoEmAndamento1.Size = new System.Drawing.Size(245, 24);
            this.lblProcessoEmAndamento1.TabIndex = 16;
            this.lblProcessoEmAndamento1.Text = "lblProcessoEmAndamento1";
            // 
            // lblProcessoEmAndamento2
            // 
            this.lblProcessoEmAndamento2.AutoSize = true;
            this.lblProcessoEmAndamento2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.lblProcessoEmAndamento2.Location = new System.Drawing.Point(18, 420);
            this.lblProcessoEmAndamento2.Name = "lblProcessoEmAndamento2";
            this.lblProcessoEmAndamento2.Size = new System.Drawing.Size(245, 24);
            this.lblProcessoEmAndamento2.TabIndex = 17;
            this.lblProcessoEmAndamento2.Text = "lblProcessoEmAndamento2";
            // 
            // lblProcessoEmAndamento3
            // 
            this.lblProcessoEmAndamento3.AutoSize = true;
            this.lblProcessoEmAndamento3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.lblProcessoEmAndamento3.Location = new System.Drawing.Point(18, 453);
            this.lblProcessoEmAndamento3.Name = "lblProcessoEmAndamento3";
            this.lblProcessoEmAndamento3.Size = new System.Drawing.Size(245, 24);
            this.lblProcessoEmAndamento3.TabIndex = 18;
            this.lblProcessoEmAndamento3.Text = "lblProcessoEmAndamento3";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(417, 226);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(115, 24);
            this.label4.TabIndex = 19;
            this.label4.Text = "Fila Estoque";
            // 
            // bwGerarFilaEstoque
            // 
            this.bwGerarFilaEstoque.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwGerarFilaEstoque_DoWork);
            this.bwGerarFilaEstoque.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bwGerarFilaEstoque_RunWorkerCompleted);
            // 
            // btnFechar
            // 
            this.btnFechar.Location = new System.Drawing.Point(439, 646);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(137, 33);
            this.btnFechar.TabIndex = 20;
            this.btnFechar.Text = "Fechar";
            this.btnFechar.UseVisualStyleBackColor = true;
            this.btnFechar.Click += new System.EventHandler(this.btnFechar_Click);
            // 
            // btnGerarRelatorioFila
            // 
            this.btnGerarRelatorioFila.Location = new System.Drawing.Point(439, 339);
            this.btnGerarRelatorioFila.Name = "btnGerarRelatorioFila";
            this.btnGerarRelatorioFila.Size = new System.Drawing.Size(137, 33);
            this.btnGerarRelatorioFila.TabIndex = 21;
            this.btnGerarRelatorioFila.Text = "Gerar Relatorio Fila";
            this.btnGerarRelatorioFila.UseVisualStyleBackColor = true;
            this.btnGerarRelatorioFila.Click += new System.EventHandler(this.btnGerarRelatorioFila_Click);
            // 
            // bwGerarRelatorioFila
            // 
            this.bwGerarRelatorioFila.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwGerarRelatorioFila_DoWork);
            // 
            // lblAndamentoRelatorio
            // 
            this.lblAndamentoRelatorio.AutoSize = true;
            this.lblAndamentoRelatorio.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.lblAndamentoRelatorio.Location = new System.Drawing.Point(18, 488);
            this.lblAndamentoRelatorio.Name = "lblAndamentoRelatorio";
            this.lblAndamentoRelatorio.Size = new System.Drawing.Size(60, 24);
            this.lblAndamentoRelatorio.TabIndex = 22;
            this.lblAndamentoRelatorio.Text = "label5";
            // 
            // btnFeeds
            // 
            this.btnFeeds.Location = new System.Drawing.Point(439, 301);
            this.btnFeeds.Name = "btnFeeds";
            this.btnFeeds.Size = new System.Drawing.Size(137, 33);
            this.btnFeeds.TabIndex = 23;
            this.btnFeeds.Text = "Feeds";
            this.btnFeeds.UseVisualStyleBackColor = true;
            this.btnFeeds.Click += new System.EventHandler(this.btnFeeds_Click);
            // 
            // btnJadlogAgora
            // 
            this.btnJadlogAgora.Location = new System.Drawing.Point(161, 340);
            this.btnJadlogAgora.Name = "btnJadlogAgora";
            this.btnJadlogAgora.Size = new System.Drawing.Size(123, 33);
            this.btnJadlogAgora.TabIndex = 24;
            this.btnJadlogAgora.Text = "Jadlog Agora";
            this.btnJadlogAgora.UseVisualStyleBackColor = true;
            this.btnJadlogAgora.Click += new System.EventHandler(this.btnJadlogAgora_Click);
            // 
            // bwBlack
            // 
            this.bwBlack.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwBlack_DoWork);
            this.bwBlack.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bwBlack_RunWorkerCompleted);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(439, 377);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(137, 33);
            this.button1.TabIndex = 25;
            this.button1.Text = "Black";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // timerNotas
            // 
            this.timerNotas.Interval = 1;
            this.timerNotas.Tick += new System.EventHandler(this.timerNotas_Tick);
            // 
            // bwNotas
            // 
            this.bwNotas.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwNotas_DoWork);
            this.bwNotas.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bwNotas_RunWorkerCompleted);
            // 
            // bwAtualizarPrazo
            // 
            this.bwAtualizarPrazo.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwAtualizarPrazo_DoWork);
            this.bwAtualizarPrazo.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bwAtualizarPrazo_RunWorkerCompleted);
            // 
            // timerPrazo
            // 
            this.timerPrazo.Interval = 50000;
            this.timerPrazo.Tick += new System.EventHandler(this.timerPrazo_Tick);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.label5.Location = new System.Drawing.Point(18, 518);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(60, 24);
            this.label5.TabIndex = 26;
            this.label5.Text = "label5";
            // 
            // lblPrazos
            // 
            this.lblPrazos.AutoSize = true;
            this.lblPrazos.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.lblPrazos.Location = new System.Drawing.Point(18, 547);
            this.lblPrazos.Name = "lblPrazos";
            this.lblPrazos.Size = new System.Drawing.Size(60, 24);
            this.lblPrazos.TabIndex = 27;
            this.lblPrazos.Text = "label6";
            // 
            // btnPrazos
            // 
            this.btnPrazos.Location = new System.Drawing.Point(439, 415);
            this.btnPrazos.Name = "btnPrazos";
            this.btnPrazos.Size = new System.Drawing.Size(137, 33);
            this.btnPrazos.TabIndex = 28;
            this.btnPrazos.Text = "Atualizar Prazos";
            this.btnPrazos.UseVisualStyleBackColor = true;
            this.btnPrazos.Click += new System.EventHandler(this.btnPrazos_Click);
            // 
            // btnDepoimentos
            // 
            this.btnDepoimentos.Location = new System.Drawing.Point(439, 453);
            this.btnDepoimentos.Name = "btnDepoimentos";
            this.btnDepoimentos.Size = new System.Drawing.Size(137, 33);
            this.btnDepoimentos.TabIndex = 29;
            this.btnDepoimentos.Text = "Depoimentos";
            this.btnDepoimentos.UseVisualStyleBackColor = true;
            this.btnDepoimentos.Click += new System.EventHandler(this.btnDepoimentos_Click);
            // 
            // bwDepoimentos
            // 
            this.bwDepoimentos.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwDepoimentos_DoWork);
            // 
            // timerGnre
            // 
            this.timerGnre.Interval = 1;
            this.timerGnre.Tick += new System.EventHandler(this.timerGnre_Tick);
            // 
            // bwGnre
            // 
            this.bwGnre.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwGnre_DoWork);
            this.bwGnre.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bwGnre_RunWorkerCompleted);
            // 
            // bwLoteBanco
            // 
            this.bwLoteBanco.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwLoteBanco_DoWork);
            this.bwLoteBanco.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bwLoteBanco_RunWorkerCompleted);
            // 
            // timerHabilitarRastreamentoTnt
            // 
            this.timerHabilitarRastreamentoTnt.Interval = 1000;
            this.timerHabilitarRastreamentoTnt.Tick += new System.EventHandler(this.timerHabilitarRastreamentoTnt_Tick);
            // 
            // timerRastreamentoJadlog
            // 
            this.timerRastreamentoJadlog.Interval = 1500;
            this.timerRastreamentoJadlog.Tick += new System.EventHandler(this.timerRastreamentoJadlog_Tick);
            // 
            // timerRastreamentoCorreios
            // 
            this.timerRastreamentoCorreios.Interval = 1500;
            this.timerRastreamentoCorreios.Tick += new System.EventHandler(this.timerRastreamentoCorreios_Tick);
            // 
            // timerClearsale
            // 
            this.timerClearsale.Interval = 1;
            this.timerClearsale.Tick += new System.EventHandler(this.timerClearsale_Tick);
            // 
            // bwEmailBelle
            // 
            this.bwEmailBelle.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwEmailBelle_DoWork);
            this.bwEmailBelle.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bwEmailBelle_RunWorkerCompleted);
            // 
            // timerQueueProdutos
            // 
            this.timerQueueProdutos.Interval = 1;
            this.timerQueueProdutos.Tick += new System.EventHandler(this.timerQueueProdutos_Tick);
            // 
            // btnGerarPedidoCarolinaMoveis
            // 
            this.btnGerarPedidoCarolinaMoveis.Location = new System.Drawing.Point(439, 491);
            this.btnGerarPedidoCarolinaMoveis.Name = "btnGerarPedidoCarolinaMoveis";
            this.btnGerarPedidoCarolinaMoveis.Size = new System.Drawing.Size(137, 33);
            this.btnGerarPedidoCarolinaMoveis.TabIndex = 30;
            this.btnGerarPedidoCarolinaMoveis.Text = "Pedido Carolina Moveis";
            this.btnGerarPedidoCarolinaMoveis.UseVisualStyleBackColor = true;
            this.btnGerarPedidoCarolinaMoveis.Click += new System.EventHandler(this.btnGerarPedidoCarolinaMoveis_Click);
            // 
            // bwGerarPedidoCarolinaMoveis
            // 
            this.bwGerarPedidoCarolinaMoveis.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwGerarPedidoCarolinaMoveis_DoWork);
            this.bwGerarPedidoCarolinaMoveis.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bwGerarPedidoCarolinaMoveis_RunWorkerCompleted);
            // 
            // timerUpdateClearPagamentos
            // 
            this.timerUpdateClearPagamentos.Interval = 1;
            this.timerUpdateClearPagamentos.Tick += new System.EventHandler(this.timerUpdateClearPagamentos_Tick);
            // 
            // lblLastListUpdate
            // 
            this.lblLastListUpdate.AutoSize = true;
            this.lblLastListUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.lblLastListUpdate.Location = new System.Drawing.Point(18, 582);
            this.lblLastListUpdate.Name = "lblLastListUpdate";
            this.lblLastListUpdate.Size = new System.Drawing.Size(60, 24);
            this.lblLastListUpdate.TabIndex = 32;
            this.lblLastListUpdate.Text = "label6";
            // 
            // bwChecarPedidoCompleto
            // 
            this.bwChecarPedidoCompleto.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwChecarPedidoCompleto_DoWork);
            // 
            // timerProceda
            // 
            this.timerProceda.Interval = 1;
            this.timerProceda.Tick += new System.EventHandler(this.timerProceda_Tick);
            // 
            // timerLoteBancario
            // 
            this.timerLoteBancario.Interval = 1;
            this.timerLoteBancario.Tick += new System.EventHandler(this.timerLoteBancario_Tick);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(439, 530);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(137, 33);
            this.button2.TabIndex = 33;
            this.button2.Text = "Dimensoes e Pesos";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // timerCache
            // 
            this.timerCache.Interval = 1;
            this.timerCache.Tick += new System.EventHandler(this.timerCache_Tick);
            // 
            // lblUltimaAtualizacaoEstoque
            // 
            this.lblUltimaAtualizacaoEstoque.AutoSize = true;
            this.lblUltimaAtualizacaoEstoque.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.lblUltimaAtualizacaoEstoque.Location = new System.Drawing.Point(18, 615);
            this.lblUltimaAtualizacaoEstoque.Name = "lblUltimaAtualizacaoEstoque";
            this.lblUltimaAtualizacaoEstoque.Size = new System.Drawing.Size(60, 24);
            this.lblUltimaAtualizacaoEstoque.TabIndex = 34;
            this.lblUltimaAtualizacaoEstoque.Text = "label6";
            // 
            // timerEstoqueAvulso
            // 
            this.timerEstoqueAvulso.Interval = 1;
            this.timerEstoqueAvulso.Tick += new System.EventHandler(this.timerEstoqueAvulso_Tick);
            // 
            // bwEmailPlimor
            // 
            this.bwEmailPlimor.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwEmailPlimor_DoWork);
            this.bwEmailPlimor.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bwEmailPlimor_RunWorkerCompleted);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(899, 26);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(134, 13);
            this.label6.TabIndex = 35;
            this.label6.Text = "Cancelamento de Pedidos:";
            // 
            // lblCancelamentoPedidos
            // 
            this.lblCancelamentoPedidos.AutoSize = true;
            this.lblCancelamentoPedidos.Location = new System.Drawing.Point(1039, 26);
            this.lblCancelamentoPedidos.Name = "lblCancelamentoPedidos";
            this.lblCancelamentoPedidos.Size = new System.Drawing.Size(10, 13);
            this.lblCancelamentoPedidos.TabIndex = 36;
            this.lblCancelamentoPedidos.Text = "-";
            // 
            // btnZerarTimerCancelamentoPedido
            // 
            this.btnZerarTimerCancelamentoPedido.Location = new System.Drawing.Point(818, 19);
            this.btnZerarTimerCancelamentoPedido.Name = "btnZerarTimerCancelamentoPedido";
            this.btnZerarTimerCancelamentoPedido.Size = new System.Drawing.Size(75, 23);
            this.btnZerarTimerCancelamentoPedido.TabIndex = 37;
            this.btnZerarTimerCancelamentoPedido.Text = "Zerar Timer";
            this.btnZerarTimerCancelamentoPedido.UseVisualStyleBackColor = true;
            this.btnZerarTimerCancelamentoPedido.Click += new System.EventHandler(this.btnZerarTimerCancelamentoPedido_Click);
            // 
            // btnAtivarTimerCancelamentoProdutos
            // 
            this.btnAtivarTimerCancelamentoProdutos.Location = new System.Drawing.Point(714, 19);
            this.btnAtivarTimerCancelamentoProdutos.Name = "btnAtivarTimerCancelamentoProdutos";
            this.btnAtivarTimerCancelamentoProdutos.Size = new System.Drawing.Size(98, 23);
            this.btnAtivarTimerCancelamentoProdutos.TabIndex = 38;
            this.btnAtivarTimerCancelamentoProdutos.Text = "Ativar Timer";
            this.btnAtivarTimerCancelamentoProdutos.UseVisualStyleBackColor = true;
            this.btnAtivarTimerCancelamentoProdutos.Click += new System.EventHandler(this.btnAtivarTimerCancelamentoProdutos_Click);
            // 
            // btnAtivarTimerPedidosCompletos
            // 
            this.btnAtivarTimerPedidosCompletos.Location = new System.Drawing.Point(714, 48);
            this.btnAtivarTimerPedidosCompletos.Name = "btnAtivarTimerPedidosCompletos";
            this.btnAtivarTimerPedidosCompletos.Size = new System.Drawing.Size(98, 23);
            this.btnAtivarTimerPedidosCompletos.TabIndex = 41;
            this.btnAtivarTimerPedidosCompletos.Text = "Ativar Timer";
            this.btnAtivarTimerPedidosCompletos.UseVisualStyleBackColor = true;
            this.btnAtivarTimerPedidosCompletos.Click += new System.EventHandler(this.btnAtivarTimerPedidosCompletos_Click);
            // 
            // btnZerarTimerPedidosCompletos
            // 
            this.btnZerarTimerPedidosCompletos.Location = new System.Drawing.Point(818, 48);
            this.btnZerarTimerPedidosCompletos.Name = "btnZerarTimerPedidosCompletos";
            this.btnZerarTimerPedidosCompletos.Size = new System.Drawing.Size(75, 23);
            this.btnZerarTimerPedidosCompletos.TabIndex = 40;
            this.btnZerarTimerPedidosCompletos.Text = "Zerar Timer";
            this.btnZerarTimerPedidosCompletos.UseVisualStyleBackColor = true;
            this.btnZerarTimerPedidosCompletos.Click += new System.EventHandler(this.btnZerarTimerPedidosCompletos_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(899, 55);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(128, 13);
            this.label7.TabIndex = 39;
            this.label7.Text = "Checagem de Completos:";
            // 
            // lblStatusCompleto
            // 
            this.lblStatusCompleto.AutoSize = true;
            this.lblStatusCompleto.Location = new System.Drawing.Point(1039, 55);
            this.lblStatusCompleto.Name = "lblStatusCompleto";
            this.lblStatusCompleto.Size = new System.Drawing.Size(10, 13);
            this.lblStatusCompleto.TabIndex = 42;
            this.lblStatusCompleto.Text = "-";
            // 
            // timerEmailAndamento
            // 
            this.timerEmailAndamento.Enabled = true;
            this.timerEmailAndamento.Interval = 1;
            this.timerEmailAndamento.Tick += new System.EventHandler(this.timerEmailAndamento_Tick);
            // 
            // bwEmailAndamento
            // 
            this.bwEmailAndamento.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwEmailAndamento_DoWork);
            this.bwEmailAndamento.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bwEmailAndamento_RunWorkerCompleted);
            // 
            // bwCartao
            // 
            this.bwCartao.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwCartao_DoWork);
            // 
            // timerRegistroBoleto
            // 
            this.timerRegistroBoleto.Enabled = true;
            this.timerRegistroBoleto.Interval = 1;
            this.timerRegistroBoleto.Tick += new System.EventHandler(this.timerRegistroBoleto_Tick);
            // 
            // bwRegistroBoleto
            // 
            this.bwRegistroBoleto.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwRegistroBoleto_DoWork);
            this.bwRegistroBoleto.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bwRegistroBoleto_RunWorkerCompleted);
            // 
            // bwEnviarNotaTransportadora
            // 
            this.bwEnviarNotaTransportadora.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwEnviarNotaTransportadora_DoWork);
            this.bwEnviarNotaTransportadora.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bwEnviarNotaTransportadora_RunWorkerCompleted);
            // 
            // bwEnviarEmailsConfirmacaoPedido
            // 
            this.bwEnviarEmailsConfirmacaoPedido.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwEnviarEmailsConfirmacaoPedido_DoWork);
            // 
            // bwProcessarPagamentos
            // 
            this.bwProcessarPagamentos.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwProcessarPagamentos_DoWork);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(899, 79);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(113, 13);
            this.label8.TabIndex = 43;
            this.label8.Text = "Notas Transportadora:";
            // 
            // lblNotasTransportadora
            // 
            this.lblNotasTransportadora.AutoSize = true;
            this.lblNotasTransportadora.Location = new System.Drawing.Point(1039, 79);
            this.lblNotasTransportadora.Name = "lblNotasTransportadora";
            this.lblNotasTransportadora.Size = new System.Drawing.Size(10, 13);
            this.lblNotasTransportadora.TabIndex = 44;
            this.lblNotasTransportadora.Text = "-";
            // 
            // timerQueuesParalelos
            // 
            this.timerQueuesParalelos.Interval = 1;
            this.timerQueuesParalelos.Tick += new System.EventHandler(this.timerQueuesParalelos_Tick);
            // 
            // bwQueuesParalelos
            // 
            this.bwQueuesParalelos.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwQueuesParalelos_DoWork);
            this.bwQueuesParalelos.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bwQueuesParalelos_RunWorkerCompleted);
            // 
            // bwAtualizarEstoqueReal
            // 
            this.bwAtualizarEstoqueReal.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwAtualizarEstoqueReal_DoWork);
            // 
            // bwGerarSimulacaoFrete
            // 
            this.bwGerarSimulacaoFrete.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwGerarSimulacaoFrete_DoWork);
            // 
            // timerLembreteBoleto
            // 
            this.timerLembreteBoleto.Enabled = true;
            this.timerLembreteBoleto.Interval = 1;
            this.timerLembreteBoleto.Tick += new System.EventHandler(this.timerLembreteBoleto_Tick);
            // 
            // bwLembreteBoleto
            // 
            this.bwLembreteBoleto.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwLembreteBoleto_DoWork);
            this.bwLembreteBoleto.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bwLembreteBoleto_RunWorkerCompleted);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1224, 680);
            this.Controls.Add(this.lblNotasTransportadora);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.lblStatusCompleto);
            this.Controls.Add(this.btnAtivarTimerPedidosCompletos);
            this.Controls.Add(this.btnZerarTimerPedidosCompletos);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.btnAtivarTimerCancelamentoProdutos);
            this.Controls.Add(this.btnZerarTimerCancelamentoPedido);
            this.Controls.Add(this.lblCancelamentoPedidos);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.lblUltimaAtualizacaoEstoque);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.lblLastListUpdate);
            this.Controls.Add(this.btnGerarPedidoCarolinaMoveis);
            this.Controls.Add(this.btnDepoimentos);
            this.Controls.Add(this.btnPrazos);
            this.Controls.Add(this.lblPrazos);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnJadlogAgora);
            this.Controls.Add(this.btnFeeds);
            this.Controls.Add(this.lblAndamentoRelatorio);
            this.Controls.Add(this.btnGerarRelatorioFila);
            this.Controls.Add(this.btnFechar);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lblProcessoEmAndamento3);
            this.Controls.Add(this.lblProcessoEmAndamento2);
            this.Controls.Add(this.lblProcessoEmAndamento1);
            this.Controls.Add(this.btnChecarEnviosJadLog);
            this.Controls.Add(this.lblCarregamentoJadLog);
            this.Controls.Add(this.lblElastic);
            this.Controls.Add(this.lblRelevancia);
            this.Controls.Add(this.btnChecarCompletos);
            this.Controls.Add(this.lblChecarCompletos);
            this.Controls.Add(this.lblErro);
            this.Controls.Add(this.lblEstoque);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblStatusGerarPedido);
            this.Controls.Add(this.lblStatus);
            this.Name = "Form1";
            this.Text = "Server Service";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.Label lblStatusGerarPedido;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Timer timerQueue;
        private System.ComponentModel.BackgroundWorker bwPedidoFornecedor;
        private System.Windows.Forms.Timer timerAtualizarEstoque;
        private System.ComponentModel.BackgroundWorker dwAtualizarEstoque;
        private System.ComponentModel.BackgroundWorker bwCancelarPedidos;
        private System.Windows.Forms.Timer timerCancelarPedidos;
        private System.Windows.Forms.Timer timerChecaCompletos;
        private System.ComponentModel.BackgroundWorker bwChecaCompletos;
        private System.Windows.Forms.Timer timerVerificaEstoque;
        private System.Windows.Forms.Timer timerPendenciasJadLog;
        private System.ComponentModel.BackgroundWorker bwPendenciasJadLog;
        private System.ComponentModel.BackgroundWorker bwEmailTnt;
        private System.ComponentModel.BackgroundWorker bwEnviaEmailCarregamento;
        private System.Windows.Forms.Label lblEstoque;
        private System.Windows.Forms.Label lblErro;
        private System.ComponentModel.BackgroundWorker bwUtilizarEstoque;
        private System.Windows.Forms.Label lblChecarCompletos;
        private System.ComponentModel.BackgroundWorker bwListaTransferencia;
        private System.Windows.Forms.Button btnChecarCompletos;
        private System.ComponentModel.BackgroundWorker bwAtualizarProdutoElastic;
        private System.ComponentModel.BackgroundWorker bwAtualizarCategoriaElastic;
        private System.ComponentModel.BackgroundWorker bwAtualizarRelevancia;
        private System.Windows.Forms.Label lblRelevancia;
        private System.ComponentModel.BackgroundWorker bwAtualizarTodosElastic;
        private System.Windows.Forms.Label lblElastic;
        private System.ComponentModel.BackgroundWorker bwAtualizarFornecedorElastic;
        private System.Windows.Forms.Timer timerFeeds;
        private System.ComponentModel.BackgroundWorker bwFeeds;
        private System.ComponentModel.BackgroundWorker bwCarregamentoJadLog;
        private System.Windows.Forms.Timer timerCarramentoJadLog;
        private System.Windows.Forms.Label lblCarregamentoJadLog;
        private System.Windows.Forms.Button btnChecarEnviosJadLog;
        private System.Windows.Forms.Label lblProcessoEmAndamento1;
        private System.Windows.Forms.Label lblProcessoEmAndamento2;
        private System.Windows.Forms.Label lblProcessoEmAndamento3;
        private System.Windows.Forms.Label label4;
        private System.ComponentModel.BackgroundWorker bwGerarFilaEstoque;
        private System.Windows.Forms.Button btnFechar;
        private System.Windows.Forms.Button btnGerarRelatorioFila;
        private System.ComponentModel.BackgroundWorker bwGerarRelatorioFila;
        private System.Windows.Forms.Label lblAndamentoRelatorio;
        private System.Windows.Forms.Button btnFeeds;
        private System.Windows.Forms.Button btnJadlogAgora;
        private System.ComponentModel.BackgroundWorker bwBlack;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Timer timerNotas;
        private System.ComponentModel.BackgroundWorker bwNotas;
        private System.ComponentModel.BackgroundWorker bwAtualizarPrazo;
        private System.Windows.Forms.Timer timerPrazo;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblPrazos;
        private System.Windows.Forms.Button btnPrazos;
        private System.ComponentModel.BackgroundWorker bwConfirmarDados;
        private System.Windows.Forms.Button btnDepoimentos;
        private System.ComponentModel.BackgroundWorker bwDepoimentos;
        private System.Windows.Forms.Timer timerGnre;
        private System.ComponentModel.BackgroundWorker bwGnre;
        private System.ComponentModel.BackgroundWorker bwLoteBanco;
        private System.Windows.Forms.Timer timerHabilitarRastreamentoTnt;
        private System.Windows.Forms.Timer timerRastreamentoJadlog;
        private System.Windows.Forms.Timer timerRastreamentoCorreios;
        private System.Windows.Forms.Timer timerClearsale;
        private System.ComponentModel.BackgroundWorker bwCompactarFotoKraken;
        private System.ComponentModel.BackgroundWorker bwEmailBelle;
        private System.Windows.Forms.Timer timerQueueProdutos;
        private System.Windows.Forms.Button btnGerarPedidoCarolinaMoveis;
        private System.ComponentModel.BackgroundWorker bwGerarPedidoCarolinaMoveis;
        private System.Windows.Forms.Timer timerUpdateClearPagamentos;
        private System.Windows.Forms.Label lblLastListUpdate;
        private System.ComponentModel.BackgroundWorker bwChecarPedidoCompleto;
        private System.Windows.Forms.Timer timerProceda;
        private System.Windows.Forms.Timer timerLoteBancario;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Timer timerCache;
        private System.Windows.Forms.Label lblUltimaAtualizacaoEstoque;
        private System.Windows.Forms.Timer timerEstoqueAvulso;
        private System.ComponentModel.BackgroundWorker bwEmailPlimor;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblCancelamentoPedidos;
        private System.Windows.Forms.Button btnZerarTimerCancelamentoPedido;
        private System.Windows.Forms.Button btnAtivarTimerCancelamentoProdutos;
        private System.Windows.Forms.Button btnAtivarTimerPedidosCompletos;
        private System.Windows.Forms.Button btnZerarTimerPedidosCompletos;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblStatusCompleto;
        private System.Windows.Forms.Timer timerEmailAndamento;
        private System.ComponentModel.BackgroundWorker bwEmailAndamento;
        private System.ComponentModel.BackgroundWorker bwCartao;
        private System.Windows.Forms.Timer timerRegistroBoleto;
        private System.ComponentModel.BackgroundWorker bwRegistroBoleto;
        private System.ComponentModel.BackgroundWorker bwEnviarNotaTransportadora;
        private System.ComponentModel.BackgroundWorker bwEnviarEmailsConfirmacaoPedido;
        private System.ComponentModel.BackgroundWorker bwProcessarPagamentos;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblNotasTransportadora;
        private System.Windows.Forms.Timer timerQueuesParalelos;
        private System.ComponentModel.BackgroundWorker bwQueuesParalelos;
        private System.ComponentModel.BackgroundWorker bwAtualizarEstoqueReal;
        private System.ComponentModel.BackgroundWorker bwGerarSimulacaoFrete;
        private System.Windows.Forms.Timer timerLembreteBoleto;
        private System.ComponentModel.BackgroundWorker bwLembreteBoleto;
    }
}

