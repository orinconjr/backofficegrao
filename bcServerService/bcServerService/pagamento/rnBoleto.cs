﻿using BoletoNet;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bcServerService.pagamento
{
    class rnBoleto
    {
        public static int GerarLotesFinanceiros(int idContaPagamento)
        {
            var data = new dbSiteEntities();
            var boletos = new Boletos();
            var pagamentosTotal = (from c in data.tbPedidoPagamento
                                   where
                                   c.cancelado == false &&
                                   c.pago == false &&
                                   c.pagamentoNaoAutorizado == false &&
                                   c.idLoteCobranca == null &&
                                   c.idContaPagamento == idContaPagamento
                                   && c.condicaoDePagamentoId == 4
                                   //c.idPedidoPagamento == 707491
                                   select c).ToList();
            var pagamentos = pagamentosTotal.Take(100).ToList();
            if (pagamentos.Any())
            {
                var conta = (from d in data.tbContaPagamento where d.idContaPagamento == idContaPagamento select d).FirstOrDefault();
                var cedente = new Cedente(conta.cnpj, conta.cedente, conta.agenciaNumero, conta.contaNumero);
                cedente.Codigo = (conta.codCedente);
                //cedente.CodigoTransmissao = "36500787438301300321";
                cedente.CodigoTransmissao = conta.codigoTransmissao;
                cedente.ContaBancaria.DigitoAgencia = conta.agenciaDigito;
                cedente.ContaBancaria.DigitoConta = conta.contaDigito;

                var ultimoLote = (from c in data.tbLoteCobranca orderby c.idLoteCobranca descending select c).FirstOrDefault();
                int idUltimoLote = (ultimoLote == null ? 0 : ultimoLote.idLoteCobranca) + 1;

                var novoLote = new tbLoteCobranca();
                novoLote.dataCriacao = DateTime.Now;
                novoLote.idLoteCobranca = idUltimoLote;
                novoLote.arquivoLote = "";
                data.tbLoteCobranca.Add(novoLote);
                data.SaveChanges();

                foreach (var pagamento in pagamentos)
                {
                    var cliente = (from clie in data.tbPedidos
                                   join d in data.tbClientes on clie.clienteId equals d.clienteId
                                   where clie.pedidoId == pagamento.pedidoId
                                   select d).FirstOrDefault();
                    if (cliente != null)
                    {
                        if (cliente.clienteCPFCNPJ.Length != 11 && cliente.clienteCPFCNPJ.Length != 14)
                        {
                            rnEmails.EnviaEmail("", "andre@bark.com.br", "", "", "", "CPF Inválido: " + pagamento.pedidoId, "CPF inválido");
                        }
                        else
                        {
                            try
                            {
                                var vencimento = RetornaProximoDiaUtil(pagamento.dataVencimento);

                                var b = new Boleto(vencimento, Convert.ToDecimal(pagamento.valor), conta.carteira, (String.Format("{0:0000000}", Convert.ToInt64(pagamento.idPedidoPagamento))), cedente);
                                b.NumeroDocumento = pagamento.numeroCobranca;
                                b.DataDocumento = pagamento.tbPedidos.dataHoraDoPedido ?? pagamento.dataVencimento;

                                //Informa os dados do sacado
                                if (cliente.clienteCPFCNPJ.Length == 11)
                                    b.Sacado = new Sacado(cliente.clienteCPFCNPJ, rnFuncoes.removeCaracteresEspeciaisDeixaEspaco(rnFuncoes.removeAcentos(cliente.clienteNome)));
                                else if (cliente.clienteCPFCNPJ.Length == 14)
                                    b.Sacado = new Sacado(cliente.clienteCPFCNPJ, rnFuncoes.removeCaracteresEspeciaisDeixaEspaco(rnFuncoes.removeAcentos(cliente.clienteNomeDaEmpresa)));

                                b.Sacado.Endereco.End = rnFuncoes.removeCaracteresEspeciaisDeixaEspaco(rnFuncoes.removeAcentos(cliente.clienteRua)) + ", " + rnFuncoes.removeCaracteresEspeciaisDeixaEspaco(rnFuncoes.removeAcentos(cliente.clienteNumero));
                                b.Sacado.Endereco.Bairro = rnFuncoes.removeCaracteresEspeciaisDeixaEspaco(rnFuncoes.removeAcentos(cliente.clienteBairro));
                                b.Sacado.Endereco.Cidade = rnFuncoes.removeCaracteresEspeciaisDeixaEspaco(rnFuncoes.removeAcentos(cliente.clienteCidade));
                                if (string.IsNullOrEmpty(b.Sacado.Endereco.Bairro)) b.Sacado.Endereco.Bairro = rnFuncoes.removeCaracteresEspeciaisDeixaEspaco(rnFuncoes.removeAcentos(cliente.clienteCidade));
                                b.Sacado.Endereco.CEP = cliente.clienteCep.Replace("-", "").Replace(" ", "").Replace(".", "");
                                b.Sacado.Endereco.UF = rnFuncoes.removeCaracteresEspeciaisDeixaEspaco(rnFuncoes.removeAcentos(cliente.clienteEstado));

                                Instrucao_Santander i = new Instrucao_Santander();
                                i.Descricao = conta.instrucoes;
                                b.Instrucoes.Add(i);
                                b.LocalPagamento = "Ate o vencimento, preferencialmente no Banco Santander";

                                //Espécie do Documento - [R] Recibo
                                b.EspecieDocumento = new EspecieDocumento_Santander("17");
                                b.Remessa = new Remessa();
                                b.Remessa.CodigoOcorrencia = "01";
                                /*BoletoBancario bb = new BoletoBancario();
                                bb.CodigoBanco = short.Parse(conta.bancoNumero.ToString());
                                //-> Referente ao código do Santander
                                bb.Boleto = b;
                                bb.GerarArquivoRemessa = true;
                                bb.Boleto.Valida();*/

                                boletos.Add(b);
                                pagamento.idLoteCobranca = novoLote.idLoteCobranca;
                            }
                            catch (Exception ex)
                            {
                                rnEmails.EnviaEmail("", "andre@bark.com.br", "", "", "", ex.ToString(), "Erro Boleto");
                            }
                        }
                    }
                    else
                    {
                        rnEmails.EnviaEmail("", "andre@bark.com.br", "", "", "", pagamento.pedidoId.ToString(), "Erro Boleto Cliente Não Encontrado");
                    }
                }

                var memoryStr = new MemoryStream();
                var objREMESSA = new ArquivoRemessa(TipoArquivo.CNAB240);
                objREMESSA.GerarArquivoRemessa(conta.convenio, new Banco(Convert.ToInt32(conta.bancoNumero)), cedente, boletos, memoryStr, 1);
                var arquivoRemessa = Encoding.ASCII.GetString(memoryStr.ToArray());
                string nomeArquivo = idUltimoLote + "_" + DateTime.Now.Year.ToString() + "_" + DateTime.Now.Month.ToString() + "_" + DateTime.Now.Day.ToString();

                if (idContaPagamento == 11)
                {
                    System.IO.File.WriteAllText(@"C:\Santander\AFTData\OUTBOX\GE201965\" + nomeArquivo + ".txt", arquivoRemessa);
                }
                else
                {
                    System.IO.File.WriteAllText(conta.caminhoRemessa + nomeArquivo + ".txt", arquivoRemessa);
                }

                novoLote.arquivoLote = arquivoRemessa;
                data.SaveChanges();
            }
            return pagamentosTotal.Count();
        }



        public static DateTime RetornaProximoDiaUtil(DateTime dia)
        {
            DateTime dataFinal = dia;
            var feriados = rnFuncoes.listaFeriados(false);
            if (feriados.Any(x => x.data.Date < DateTime.Now.Date)) feriados = rnFuncoes.listaFeriados(true);
            bool diaUtil = false;
            int maximoDias = 0;

            while (diaUtil == false && maximoDias < 5)
            {
                maximoDias++;
                bool fimDeSemana = dataFinal.DayOfWeek == DayOfWeek.Saturday | dataFinal.DayOfWeek == DayOfWeek.Sunday;
                bool feriado = (from c in feriados where c.data.Date == dataFinal.Date select c).Any();
                if (fimDeSemana == false && feriado == false)
                {
                    diaUtil = true;
                }
                else
                {
                    dataFinal = dataFinal.AddDays(1);
                }
            }
            return dataFinal;
        }


        public static void ProcessarRetornosSantander(string pastaEntrada, string pastaSaida)
        {
            //string caminhoArquivoProcessado = @"c:\santander_processados\GE201965\";
            //DirectoryInfo dir = new DirectoryInfo(@"C:\Santander\AFTData\INBOX\GE201965\");
            string caminhoArquivoProcessado = pastaSaida;
            DirectoryInfo dir = new DirectoryInfo(pastaEntrada);
            FileInfo[] files = dir.GetFiles("*.txt", SearchOption.AllDirectories);

            List<string> listaErros = new List<string>();
            using (var data = new dbSiteEntities())
            {
                foreach (FileInfo file in files)
                {
                    if (file.Name.Contains("_MOV"))
                    {
                        using (FileStream fs = file.OpenRead())
                        {
                            var boletoRetorno = new BoletoNet.ArquivoRetornoCNAB240();
                            boletoRetorno.LerArquivoRetorno(new Banco(353), fs);
                            foreach (var linhaRetorno in boletoRetorno.ListaDetalhes)
                            {
                                decimal valorPago = linhaRetorno.SegmentoU.ValorPagoPeloSacado;
                                if (valorPago > 0)
                                {
                                    string nossoNumero = linhaRetorno.SegmentoT.NossoNumero.ToString();
                                    int idPedidoPagamento = Convert.ToInt32(Convert.ToInt64(linhaRetorno.SegmentoT.NossoNumero.ToString()).ToString().Substring(0, Convert.ToInt64(linhaRetorno.SegmentoT.NossoNumero.ToString()).ToString().Length - 1));
                                    if (idPedidoPagamento < 1000000)
                                    {
                                        idPedidoPagamento = Convert.ToInt32(Convert.ToInt64(linhaRetorno.SegmentoT.NossoNumero.ToString()).ToString());
                                    }
                                    var pedidoPagamento = (from c in data.tbPedidoPagamento where c.idPedidoPagamento == idPedidoPagamento select c).FirstOrDefault();
                                   
                                    var detalheRetorno = new tbLoteCobrancaRetornoRegistro();
                                    detalheRetorno.nossoNumero = nossoNumero;
                                    detalheRetorno.dataProcessamento = DateTime.Now;
                                    detalheRetorno.arquivoRetorno = file.Name.ToString();
                                    detalheRetorno.valorPago = valorPago;
                                    if (pedidoPagamento != null)
                                    {
                                        detalheRetorno.idPedidoPagamento = pedidoPagamento.idPedidoPagamento;
                                        var pedido = pedidoPagamento.tbPedidos;

                                        if (pedido.statusDoPedido == 2)
                                        {
                                            if (pedidoPagamento.valor >= (valorPago - Convert.ToDecimal("0,10")) && pedidoPagamento.valor <= (valorPago + Convert.ToDecimal("0,10")))
                                            {
                                                detalheRetorno.mensagem = "Valor Correto";

                                                try
                                                {
                                                    int retornoConfirmacao = pagamento.PagamentoV2.confirmarPagamento(pedidoPagamento.idPedidoPagamento, pedidoPagamento.numeroCobranca, "Pagamento confirmado via importação de arquivo do banco", true);


                                                    if (retornoConfirmacao == 0) detalheRetorno.mensagem = "Pagamento Confirmado";
                                                    if (retornoConfirmacao == 1) detalheRetorno.mensagem = "Pagamento Confirmado / Separação de Estoque";
                                                    if (retornoConfirmacao == 99) detalheRetorno.mensagem = "Pagamento parcialmente confirmado. Aguardando restante do pagamento";
                                                }
                                                catch (Exception)
                                                {
                                                    detalheRetorno.mensagem = "Erro no processamento";
                                                    listaErros.Add("Pedido: " + pedidoPagamento.pedidoId + " - Valor Pago: " + valorPago.ToString("C") + " - " + detalheRetorno.mensagem);
                                                }
                                            }
                                            else
                                            {
                                                detalheRetorno.mensagem = "Valor Incorreto";
                                                listaErros.Add("Pedido: " + pedidoPagamento.pedidoId + " - Valor Pago: " + valorPago.ToString("C") + " - " + detalheRetorno.mensagem);
                                            }
                                            detalheRetorno.status = 1;
                                        }
                                        else if (pedido.statusDoPedido == 7 && (pedido.tipoDePagamentoId == 8 | pedido.tipoDePagamentoId == 1))
                                        {
                                            if (pedidoPagamento.valor >= (valorPago - Convert.ToDecimal("0,10")) && pedidoPagamento.valor <= (valorPago + Convert.ToDecimal("0,10")))
                                            {
                                                detalheRetorno.mensagem = "Valor Correto";
                                                try
                                                {

                                                    int retornoConfirmacao = pagamento.PagamentoV2.confirmarPagamento(pedidoPagamento.idPedidoPagamento, pedidoPagamento.numeroCobranca, "Pagamento confirmado via importação de arquivo do banco", true);

                                                    if (retornoConfirmacao == 0) detalheRetorno.mensagem = "Pagamento Confirmado";
                                                    if (retornoConfirmacao == 1) detalheRetorno.mensagem = "Pagamento Confirmado / Separação de Estoque";
                                                    if (retornoConfirmacao == 99) detalheRetorno.mensagem = "Pagamento parcialmente confirmado. Aguardando restante do pagamento";
                                                }
                                                catch (Exception)
                                                {
                                                    detalheRetorno.mensagem = "Erro no processamento";
                                                    listaErros.Add("Pedido: " + pedidoPagamento.pedidoId + " - Valor Pago: " + valorPago.ToString("C") + " - " + detalheRetorno.mensagem);
                                                }
                                            }
                                            else
                                            {
                                                detalheRetorno.mensagem = "Valor Incorreto";
                                                listaErros.Add("Pedido: " + pedidoPagamento.pedidoId + " - Valor Pago: " + valorPago.ToString("C") + " - " + detalheRetorno.mensagem);
                                            }
                                            detalheRetorno.status = 1;
                                        }
                                        else
                                        {
                                            if (pedidoPagamento.pago == false)
                                            {
                                                detalheRetorno.status = 0;
                                                detalheRetorno.mensagem = "Status do pedido incorreto";
                                                listaErros.Add("Pedido: " + pedidoPagamento.pedidoId + " - Valor Pago: " + valorPago.ToString("C") + " - " + detalheRetorno.mensagem);
                                            }
                                            else
                                            {
                                                detalheRetorno.status = 1;
                                                detalheRetorno.mensagem = "Pagamento Pago Anteriormente - Status do pedido incorreto";

                                            }
                                        }
                                        detalheRetorno.status = 1;
                                    }
                                    else
                                    {
                                        detalheRetorno.status = 0;
                                        detalheRetorno.mensagem = "Pagamento não Localizado";
                                        listaErros.Add("Nosso numero: " + nossoNumero + " - Valor Pago: " + valorPago.ToString("C") + " - " + detalheRetorno.mensagem);
                                    }
                                    data.tbLoteCobrancaRetornoRegistro.Add(detalheRetorno);
                                    data.SaveChanges();
                                }
                            }
                        }
                        File.Copy(file.FullName, caminhoArquivoProcessado + file.Name, true);
                        File.Delete(file.FullName);
                    }
                    else if (file.Name.Contains("_FRA"))
                    {
                        using (FileStream fs = file.OpenRead())
                        {
                            rnEmails.EnviaEmailAnexoAws("andre@bark.com.br, suellen.carvalho@graodegente.com.br", fs, file.Name, "Francesinha " + file.Name);
                            rnEmails.EnviaEmailAnexoAws("suellen.carvalho@graodegente.com.br", fs, file.Name, "Francesinha " + file.Name);
                        }
                        File.Copy(file.FullName, caminhoArquivoProcessado + file.Name, true);
                        File.Delete(file.FullName);
                    }
                    else
                    {
                        File.Copy(file.FullName, caminhoArquivoProcessado + file.Name, true);
                        File.Delete(file.FullName);
                    }
                }
            }

            if (listaErros.Any())
            {
                string email = "";
                foreach (var listaerro in listaErros)
                {
                    email += listaerro + "<br>";
                }
                rnEmails.EnviaEmail("", "andre@bark.com.br", "ariane.graodegente@gmail.com", "", "", email, "BOLETOS - Pedidos com Problemas");
            }
        }
    }
}
