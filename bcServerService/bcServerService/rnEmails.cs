﻿using System;
using System.Collections.Generic;
using System.Compat.Web;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Security.Cryptography;
using System.Text;
using Amazon.SimpleEmail;
using Amazon.SimpleEmail.Model;
using PostmarkDotNet;
using Attachment = System.Net.Mail.Attachment;
using System.Configuration;

namespace bcServerService
{
    public static class rnEmails
    {
        public static bool EnviaEmail(string de, string para, string cc, string cco, string responder, string mensagem, string assunto)
        {
            //return EnviaEmailAutenticado(de, para, cc, cco, responder, mensagem, assunto);

            string remetente = "";
            if (de != "")
            {
                remetente = de;
            }
            else
            {
                remetente = rnConfiguracoes.fromAddress;
            }


            var destinatarios = new List<string>();
            if (para == "")
            {
                destinatarios.Add(rnConfiguracoes.fromAddress);
            }
            else
            {
                var emailsPara = para.Split(',');
                foreach (var emailPara in emailsPara)
                {
                    destinatarios.Add(emailPara);
                }
            }


            var copias = new List<string>();
            if (cc != "")
            {
                var emailsCC = cc.Split(',');
                foreach (var emailCC in emailsCC)
                {
                    copias.Add(emailCC);
                }
            }


            var copiasocultas = new List<string>();
            if (cco != "")
            {
                var emailsCCO = cco.Split(',');
                foreach (var emailCCO in emailsCCO)
                {
                    copiasocultas.Add(emailCCO);
                }
            }

            string replyTo = responder == "" ? rnConfiguracoes.fromAddress : responder;

            //return false;
            var client = new AmazonSimpleEmailServiceClient("AKIAIP7IE6WVNK2K6TIQ", "9UkGi7cYKpBGFG60a/J0SWmziuu2rYNWLnoqVWi/", Amazon.RegionEndpoint.USEast1);
            //var client = new AmazonSimpleEmailServiceClient("AKIAJ4O4RLJPW6OMBYHA", "x5lnx5CWv3QbYvZGbxX5cfMs611/UUsiBgxTb6/I", Amazon.RegionEndpoint.USEast1);
            var request = new Amazon.SimpleEmail.Model.SendEmailRequest();
            request.Destination = new Destination() { ToAddresses = destinatarios, BccAddresses = copiasocultas, CcAddresses = copias };
            request.Source = remetente;
            request.Message = new Message();
            request.Message.Body = new Body();
            request.Message.Body.Html = new Content() { Data = mensagem };
            request.Message.Subject = new Content() { Data = assunto };
            try
            {
                var response2 = client.SendEmail(request);
            }
            catch (Exception ex)
            {
                return false;
            }
            //var amazonMail = new AmazonSESWrapper("AKIAIP7IE6WVNK2K6TIQ", "9UkGi7cYKpBGFG60a/J0SWmziuu2rYNWLnoqVWi/");
            //var amazonMail = new AmazonSESWrapper("AKIAJ5BITS7F4JL5WPAA", "As+n2bYTRiRiMSxjjfBkfKircGMILqtqEVkggTh6jjF4");
            // var retorno = amazonMail.SendEmail(destinatarios, copias, copiasocultas, remetente, replyTo, assunto, mensagem);
            return true;
        }

        public static bool EnviaEmailPostMark(string de, string para, string cc, string cco, string responder, string mensagem, string assunto)
        {

            string remetente = "";
            if (de != "")
            {
                remetente = de;
            }
            else
            {
                remetente = rnConfiguracoes.fromAddress;
            }


            var destinatarios = "";
            if (para == "")
            {
                destinatarios = rnConfiguracoes.fromAddress;
            }
            else
            {
                destinatarios = para;
            }

            //string replyTo = responder == "" ? ConfigurationManager.AppSettings["fromAddress"] : responder;

            var message = new PostmarkMessage();
            message.From = remetente;
            message.To = destinatarios;
            if (!string.IsNullOrEmpty(cc)) message.Cc = cc;
            if (!string.IsNullOrEmpty(cco)) message.Bcc = cco;

            message.Subject = assunto;
            message.HtmlBody = mensagem;
            message.TrackOpens = true;
            try
            {
                PostmarkClient client = new PostmarkClient("42a1276f-e120-4038-8f71-7b4606ae6424");
                // PostmarkResponse response = client.SendMessageAsync(message);
            }
            catch (Exception ex)
            {
                FileInfo file = new FileInfo(ConfigurationManager.AppSettings["caminhoFisico"].ToString() + "erroemail.txt");
                if (file.Exists)
                {
                    file.Delete();
                }
                StreamWriter sw = file.CreateText();
                sw.WriteLine(ex.ToString());
                sw.WriteLine();
                sw.Close();
            }
            return true;

            //var amazonMail = new AmazonSESWrapper("AKIAIP7IE6WVNK2K6TIQ", "9UkGi7cYKpBGFG60a/J0SWmziuu2rYNWLnoqVWi/");
            //var retorno = amazonMail.SendEmail(destinatarios, copias, copiasocultas, remetente, replyTo, assunto, mensagem);
            //return retorno.HasError;
        }

        public static bool EnviaEmailPedidoFornecedor(int idPedidoFornecedor)
        {
            var data = new dbSiteEntities();
            var pedidoFornecedor =
                (from c in data.tbPedidoFornecedor where c.idPedidoFornecedor == idPedidoFornecedor select c)
                    .FirstOrDefault();
            if (pedidoFornecedor != null)
            {
                var fornecedor =
                    (from c in data.tbProdutoFornecedor where c.fornecedorId == pedidoFornecedor.idFornecedor select c)
                        .FirstOrDefault();
                var itens = (from c in data.tbPedidoFornecedorItem where c.idPedidoFornecedor == idPedidoFornecedor select c).Count();
                var valorTotal = (from c in data.tbPedidoFornecedorItem where c.idPedidoFornecedor == idPedidoFornecedor select c).Sum(x => x.custo);
                var valorTotalDiferenca = (from c in data.tbPedidoFornecedorItem where c.idPedidoFornecedor == idPedidoFornecedor select c).Sum(x => x.diferenca);

                string assunto = "Pedido " + idPedidoFornecedor + " - " + fornecedor.fornecedorNome + " (" + itens + ")";
                string mensagem = "Pedido ao fornecedor finalizado<br>";
                mensagem += "Fornecedor: " + fornecedor.fornecedorNome + "<br>";
                mensagem += "Quantidade de Itens: " + itens + "<br>";
                mensagem += "Número do Pedido: " + idPedidoFornecedor + "<br>";
                mensagem += "Valor : " + (valorTotal + valorTotalDiferenca).ToString("C") + "<br>";
                return EnviaEmail("", "comercial@graodegente.com.br,cadastro@graodegente.com.br,iving@bark.com.br", "", "", "", mensagem, assunto);
                //return EnviaEmail("", "andre@bark.com.br", "", "", "", mensagem, "Pedido " + idPedidoFornecedor + " finalizado");
            }
            return false;
        }
        public static bool EnviaEmailAutenticado(string de, string para, string cc, string cco, string responder, string mensagem, string assunto)
        {
            MailMessage mailMessage = new MailMessage();
            if (de != "")
            {
                mailMessage.From = new MailAddress(rnConfiguracoes.fromAddress, de);
            }
            else
            {
                mailMessage.From = new MailAddress(rnConfiguracoes.fromAddress, rnConfiguracoes.nomeDoSite);
            }

            if (rnConfiguracoes.ambienteTeste)
            {
                mailMessage.To.Add(rnConfiguracoes.emailDebug);
            }
            else
            {

                if (para == "")
                {
                    mailMessage.To.Add(rnConfiguracoes.emailLogin);
                }
                else
                {
                    var emailsPara = para.Split(',');
                    foreach (var emailPara in emailsPara)
                    {
                        mailMessage.To.Add(new MailAddress(emailPara));
                    }
                }

                if (cc != "")
                {
                    var emailsCC = cc.Split(',');
                    foreach (var emailCC in emailsCC)
                    {
                        mailMessage.CC.Add(new MailAddress(emailCC));
                    }
                }

                if (cco != "")
                {
                    var emailsCCO = cco.Split(',');
                    foreach (var emailCCO in emailsCCO)
                    {
                        mailMessage.Bcc.Add(new MailAddress(emailCCO));
                    }
                }

                if (responder != "")
                {
                    try
                    {
                        mailMessage.ReplyTo = new MailAddress(responder, de);
                    }
                    catch
                    {
                    }
                }
            }
            mailMessage.Subject = assunto;
            mailMessage.Body = mensagem;
            mailMessage.IsBodyHtml = true;
            mailMessage.Priority = MailPriority.Normal;

            var smtpClient = new SmtpClient(rnConfiguracoes.emailSmtp);
            smtpClient.EnableSsl = true;
            smtpClient.Port = 587;
            smtpClient.Credentials = new System.Net.NetworkCredential(rnConfiguracoes.emailLogin, rnConfiguracoes.emailSenha);

            // Send Mail via SmtpClient
            //return true;
            smtpClient.Send(mailMessage);

            return true;
        }

        public static bool EnviaEmailComAnexo(string de, string para, string responder, string mensagem, string assunto, string anexo, string nomeAnexo, string contentTypeName)
        {

            MailMessage mailMessage = new MailMessage();
            mailMessage.From = new MailAddress(de);
            var emailsPara = para.Split(',');
            if (rnConfiguracoes.ambienteTeste)
            {
                mailMessage.To.Add(rnConfiguracoes.emailDebug);
            }
            else
            {
                foreach (var emailPara in emailsPara)
                {
                    mailMessage.To.Add(new MailAddress(emailPara));
                }
            }
            mailMessage.ReplyTo = new MailAddress(responder);
            mailMessage.Subject = assunto;
            mailMessage.Body = mensagem;
            mailMessage.IsBodyHtml = true;
            mailMessage.Priority = MailPriority.Normal;

            var smtpClient = new SmtpClient(rnConfiguracoes.emailSmtp);
            smtpClient.EnableSsl = true;
            smtpClient.Credentials = new System.Net.NetworkCredential(rnConfiguracoes.emailLogin, rnConfiguracoes.emailSenha);

            using (MemoryStream memoryStream = new MemoryStream())
            {
                byte[] bom = { 0xEF, 0xBB, 0xBF };
                memoryStream.Write(bom, 0, bom.Length);

                byte[] contentAsBytes = Encoding.UTF8.GetBytes(anexo);
                memoryStream.Write(contentAsBytes, 0, contentAsBytes.Length);

                // Set the position to the beginning of the stream.
                memoryStream.Seek(0, SeekOrigin.Begin);

                // Create attachment
                ContentType contentType = new ContentType();
                contentType.MediaType = contentTypeName;
                contentType.Name = nomeAnexo;
                contentType.CharSet = "UTF-8";
                Attachment attachment = new Attachment(memoryStream, contentType);

                // Add the attachment
                mailMessage.Attachments.Add(attachment);

                //return true;
                // Send Mail via SmtpClient
                smtpClient.Send(mailMessage);
            }
            return true;
        }

        public static bool EnviaNotaTnt(int numeroNota)
        {
            return EnviaNotaTnt(numeroNota, "");
        }

        public static bool EnviaNotaTnt(int numeroNota, string xmlnota)
        {
            return EnviaNotaTntAWS(numeroNota, xmlnota);

            try
            {
                //string xmlNota = rnNotaFiscal.retornaXmlNota(numeroNota);
                string xmlNota = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" + xmlnota;

                MailMessage mailMessage = new MailMessage();
                mailMessage.From = new MailAddress("atendimento@graodegente.com.br");
                //mailMessage.To.Add(new MailAddress("andre@bark.com.br"));
                mailMessage.To.Add(new MailAddress("notaseletronicas@tntbrasil.com.br"));
                mailMessage.To.Add(new MailAddress("edi@tntbrasil.com.br"));
                mailMessage.Subject = "NFe " + numeroNota;
                mailMessage.Body = "";
                mailMessage.IsBodyHtml = true;
                mailMessage.Priority = MailPriority.Normal;

                var smtpClient = new SmtpClient(rnConfiguracoes.emailSmtp);
                smtpClient.Port = 587;
                smtpClient.EnableSsl = true;
                smtpClient.Credentials = new System.Net.NetworkCredential(rnConfiguracoes.emailLogin, rnConfiguracoes.emailSenha);

                using (MemoryStream memoryStream = new MemoryStream())
                {
                    byte[] bom = { 0xEF, 0xBB, 0xBF };
                    memoryStream.Write(bom, 0, bom.Length);

                    byte[] contentAsBytes = Encoding.UTF8.GetBytes(xmlNota);
                    memoryStream.Write(contentAsBytes, 0, contentAsBytes.Length);

                    // Set the position to the beginning of the stream.
                    memoryStream.Seek(0, SeekOrigin.Begin);

                    // Create attachment
                    ContentType contentType = new ContentType();
                    contentType.MediaType = "text/xml";
                    contentType.Name = numeroNota + ".xml";
                    contentType.CharSet = "UTF-8";
                    Attachment attachment = new Attachment(memoryStream, contentType);

                    // Add the attachment
                    mailMessage.Attachments.Add(attachment);

                    mailMessage.Body = "Nota";
                    //return true;
                    // Send Mail via SmtpClient
                    smtpClient.Send(mailMessage);
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        #region Envio de Emails com Anexos - Amazon
        public static bool EnviaNotaTntAWS(int numeroNota, string xmlnota)
        {
            var data = new dbSiteEntities();
            var envio = (from c in data.tbNotaFiscal
                         join d in data.tbPedidoEnvio on c.idPedidoEnvio equals d.idPedidoEnvio
                         where c.numeroNota == numeroNota && c.idCNPJ == 6
                         select d).FirstOrDefault();
            string transportadora = "tnt";
            if (envio != null)
            {
                transportadora = envio.formaDeEnvio.ToLower();
            }
            try
            {
                string xmlNota = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" + xmlnota;
                var mailMessage = new MimeKit.MimeMessage();
                mailMessage.From.Add(new MimeKit.MailboxAddress("atendimento@graodegente.com.br", "atendimento@graodegente.com.br"));

                if (transportadora == "tnt" | transportadora == "tntjacutinga")
                {
                    mailMessage.To.Add(new MimeKit.MailboxAddress("notaseletronicas@tntbrasil.com.br", "notaseletronicas@tntbrasil.com.br"));
                    mailMessage.To.Add(new MimeKit.MailboxAddress("edi@tntbrasil.com.br", "edi@tntbrasil.com.br"));
                }
                else if (transportadora == "lbr")
                {
                    mailMessage.To.Add(new MimeKit.MailboxAddress("expedicao@lbrexpress.com.br", "expedicao@lbrexpress.com.br"));
                    mailMessage.To.Add(new MimeKit.MailboxAddress("suporte@lbrexpress.com.br", "suporte@lbrexpress.com.br"));
                    //mailMessage.To.Add(new MimeKit.MailboxAddress("andre@bark.com.br", "andre@bark.com.br"));
                }
                else if (transportadora == "jamef")
                {
                    mailMessage.To.Add(new MimeKit.MailboxAddress("notaseletronicas@tntbrasil.com.br", "notaseletronicas@tntbrasil.com.br"));
                    mailMessage.To.Add(new MimeKit.MailboxAddress("edi@tntbrasil.com.br", "edi@tntbrasil.com.br"));
                }
                else if (transportadora == "transfolha")
                {
                    return true;
                }
                /*else if (transportadora == "totalexpress")
                {
                    mailMessage.To.Add(new MimeKit.MailboxAddress("notaseletronicas@tntbrasil.com.br", "notaseletronicas@tntbrasil.com.br"));
                }
                /*else if (transportadora == "nowlog")
                {
                    mailMessage.To.Add(new MimeKit.MailboxAddress("recebimento@nowlogistica.com", "recebimento@nowlogistica.com"));
                    mailMessage.To.Add(new MimeKit.MailboxAddress("alberto.gomes@nowlogistica.com", "alberto.gomes@nowlogistica.com"));
                }
                /*else if (transportadora == "dialogo")
                {
                    mailMessage.To.Add(new MimeKit.MailboxAddress("notaseletronicas@tntbrasil.com.br", "notaseletronicas@tntbrasil.com.br"));
                }*/
                mailMessage.Subject = "NFe " + numeroNota;

                var builder = new MimeKit.BodyBuilder();
                builder.TextBody = "";


                Stream s = GenerateStreamFromString(xmlNota);
                MimeKit.ContentType contentType = new MimeKit.ContentType("text/xml", "subtype=gml/2.1.2");
                contentType.Format = "UTF-8";
                builder.Attachments.Add(numeroNota + ".xml", s);
                mailMessage.Body = builder.ToMessageBody();

                var stream = new MemoryStream();
                mailMessage.WriteTo(stream);
                var client = new AmazonSimpleEmailServiceClient("AKIAIP7IE6WVNK2K6TIQ", "9UkGi7cYKpBGFG60a/J0SWmziuu2rYNWLnoqVWi/", Amazon.RegionEndpoint.USEast1);
                //var client = new AmazonSimpleEmailServiceClient("AKIAJXNT6EMMG2JBAOAA", "Hg2EzydsS1k3s4lVNW86TE29rw44NXilnoeMC5gJ", Amazon.RegionEndpoint.USEast1);

                var request = new Amazon.SimpleEmail.Model.SendRawEmailRequest(new RawMessage { Data = stream });

                var response2 = client.SendRawEmail(request);

                return true;
            }
            catch (Exception)
            {
                return false;
            }


        }

        public static bool EnviaEmailAnexoAws(string destinatario, Stream arquivo, string nomeArquivo, string assunto)
        {
            try
            {
                var mailMessage = new MimeKit.MimeMessage();
                mailMessage.From.Add(new MimeKit.MailboxAddress("atendimento@graodegente.com.br", "atendimento@graodegente.com.br"));
                var destinatarios = destinatario.Split(',');
                foreach (var dest in destinatarios)
                {
                    mailMessage.To.Add(new MimeKit.MailboxAddress(destinatario, destinatario));
                }
                mailMessage.Subject = assunto;

                var builder = new MimeKit.BodyBuilder();
                builder.TextBody = "";


                MimeKit.ContentType contentType = new MimeKit.ContentType("text/plain", "subtype=gml/2.1.2");
                contentType.Format = "UTF-8";
                builder.Attachments.Add(nomeArquivo, arquivo);
                mailMessage.Body = builder.ToMessageBody();

                var stream = new MemoryStream();
                mailMessage.WriteTo(stream);
                var client = new AmazonSimpleEmailServiceClient("AKIAIP7IE6WVNK2K6TIQ", "9UkGi7cYKpBGFG60a/J0SWmziuu2rYNWLnoqVWi/", Amazon.RegionEndpoint.USEast1);
                //var client = new AmazonSimpleEmailServiceClient("AKIAJ5BITS7F4JL5WPAA", "As+n2bYTRiRiMSxjjfBkfKircGMILqtqEVkggTh6jjF4");

                var request = new Amazon.SimpleEmail.Model.SendRawEmailRequest(new RawMessage { Data = stream });

                var response2 = client.SendRawEmail(request);

                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }
        public static bool EnviaNotaBelle(int numeroNota, string xmlnota)
        {
            try
            {
                string xmlNota = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" + xmlnota;
                var mailMessage = new MimeKit.MimeMessage();
                mailMessage.From.Add(new MimeKit.MailboxAddress("atendimento@graodegente.com.br", "atendimento@graodegente.com.br"));
                mailMessage.To.Add(new MimeKit.MailboxAddress("andre@bark.com.br", "andre@bark.com.br"));
                mailMessage.To.Add(new MimeKit.MailboxAddress("xml@bellexpress.com.br", "xml@bellexpress.com.br"));
                mailMessage.Subject = "NFe " + numeroNota;

                var builder = new MimeKit.BodyBuilder();
                builder.TextBody = "";


                Stream s = GenerateStreamFromString(xmlNota);
                MimeKit.ContentType contentType = new MimeKit.ContentType("text/xml", "subtype=gml/2.1.2");
                contentType.Format = "UTF-8";
                builder.Attachments.Add(numeroNota + ".xml", s);
                mailMessage.Body = builder.ToMessageBody();

                var stream = new MemoryStream();
                mailMessage.WriteTo(stream);
                var client = new AmazonSimpleEmailServiceClient("AKIAIP7IE6WVNK2K6TIQ", "9UkGi7cYKpBGFG60a/J0SWmziuu2rYNWLnoqVWi/", Amazon.RegionEndpoint.USEast1);
                //var client = new AmazonSimpleEmailServiceClient("AKIAJ5BITS7F4JL5WPAA", "As+n2bYTRiRiMSxjjfBkfKircGMILqtqEVkggTh6jjF4");

                var request = new Amazon.SimpleEmail.Model.SendRawEmailRequest(new RawMessage { Data = stream });

                var response2 = client.SendRawEmail(request);

                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }

        public static bool EnviaNotaPlimor(int numeroNota, string xmlnota)
        {
            return EnviaNotaPlimorAWS(numeroNota, xmlnota);
            try
            {
                //string xmlNota = rnNotaFiscal.retornaXmlNota(numeroNota);
                string xmlNota = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" + xmlnota;

                MailMessage mailMessage = new MailMessage();
                mailMessage.From = new MailAddress("atendimento@graodegente.com.br");
                //mailMessage.To.Add(new MailAddress("andre@bark.com.br"));
                mailMessage.To.Add(new MailAddress("nfeclientes@plimor.com.br"));
                mailMessage.To.Add(new MailAddress("bu_isaque@plimor.com.br"));
                mailMessage.Subject = "NFe " + numeroNota;
                mailMessage.Body = "";
                mailMessage.IsBodyHtml = true;
                mailMessage.Priority = MailPriority.Normal;

                var smtpClient = new SmtpClient(rnConfiguracoes.emailSmtp);
                smtpClient.Port = 587;
                smtpClient.EnableSsl = true;
                smtpClient.Credentials = new System.Net.NetworkCredential(rnConfiguracoes.emailLogin, rnConfiguracoes.emailSenha);

                using (MemoryStream memoryStream = new MemoryStream())
                {
                    byte[] bom = { 0xEF, 0xBB, 0xBF };
                    memoryStream.Write(bom, 0, bom.Length);

                    byte[] contentAsBytes = Encoding.UTF8.GetBytes(xmlNota);
                    memoryStream.Write(contentAsBytes, 0, contentAsBytes.Length);

                    // Set the position to the beginning of the stream.
                    memoryStream.Seek(0, SeekOrigin.Begin);

                    // Create attachment
                    ContentType contentType = new ContentType();
                    contentType.MediaType = "text/xml";
                    contentType.Name = numeroNota + ".xml";
                    contentType.CharSet = "UTF-8";
                    Attachment attachment = new Attachment(memoryStream, contentType);

                    // Add the attachment
                    mailMessage.Attachments.Add(attachment);

                    mailMessage.Body = "Nota";
                    //return true;
                    // Send Mail via SmtpClient
                    smtpClient.Send(mailMessage);
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public static bool EnviaNotaPlimorAWS(int numeroNota, string xmlnota)
        {
            try
            {
                string xmlNota = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" + xmlnota;
                var mailMessage = new MimeKit.MimeMessage();
                mailMessage.From.Add(new MimeKit.MailboxAddress("atendimento@graodegente.com.br", "atendimento@graodegente.com.br"));
                mailMessage.To.Add(new MimeKit.MailboxAddress("nfeclientes@plimor.com.br", "nfeclientes@plimor.com.br"));
                mailMessage.To.Add(new MimeKit.MailboxAddress("bu_isaque@plimor.com.br", "bu_isaque@plimor.com.br"));
                mailMessage.Subject = "NFe " + numeroNota;

                var builder = new MimeKit.BodyBuilder();
                builder.TextBody = "";


                Stream s = GenerateStreamFromString(xmlNota);
                MimeKit.ContentType contentType = new MimeKit.ContentType("text/xml", "subtype=gml/2.1.2");
                contentType.Format = "UTF-8";
                builder.Attachments.Add(numeroNota + ".xml", s);
                mailMessage.Body = builder.ToMessageBody();

                var stream = new MemoryStream();
                mailMessage.WriteTo(stream);

                var client = new AmazonSimpleEmailServiceClient("AKIAIP7IE6WVNK2K6TIQ", "9UkGi7cYKpBGFG60a/J0SWmziuu2rYNWLnoqVWi/", Amazon.RegionEndpoint.USEast1);
                //var client = new AmazonSimpleEmailServiceClient("AKIAJ5BITS7F4JL5WPAA", "As+n2bYTRiRiMSxjjfBkfKircGMILqtqEVkggTh6jjF4");

                var request = new Amazon.SimpleEmail.Model.SendRawEmailRequest(new RawMessage { Data = stream });

                var response2 = client.SendRawEmail(request);

                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }
        public static Stream GenerateStreamFromString(string s)
        {
            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);
            writer.Write(s);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }
        #endregion
        public static bool enviaAlteracaoDeStatus(string clienteNome, string pedidoId, string status, string clienteEmail)
        {
            int idPedido = Convert.ToInt32(pedidoId);
            var pedidoDc = new dbSiteEntities();
            var pedido = (from c in pedidoDc.tbPedidos where c.pedidoId == idPedido select c).First();
            if (pedido.statusDoPedido == 3)
            {
                string frasePrazoPrevisto = "";
                /*if (pedido.prazoFinalPedido != null)
                {
                    frasePrazoPrevisto = "A data prevista de postagem do seu produto é: " +
                                         pedido.prazoFinalPedido.Value.ToShortDateString() + "<br>";
                }*/
                StringBuilder conteudo = new StringBuilder();
                conteudo.Append("<table cellpadding='0' cellspacing='0' style='width: 650px; height:601px; background-image: url(" + rnConfiguracoes.caminhoVirtual + "images/bkEmailAlteracaoDoPedido.jpg); background-repeat: no-repeat;'><tr><td valign='top' style='font-family:Arial; color: #000000; padding-left:50px; '><div style='font-size: 14px; padding-top:127px'><b>ALTERAÇÃO DA SITUAÇÃO DO PEDIDO</b></div><div style='font-size: 11px; padding-top:25px'>Prezado (a) Sr.(a)<br /><b>" + clienteNome + ",</b><br />Informamos que a situação do seu pedido <b>nº " + rnFuncoes.retornaIdCliente(Convert.ToInt32(pedidoId)) + "</b> foi alterada para:</div><div style='font-size: 20px; padding-top:21px'><b>&quot;" + status + "&quot;</b></div><div style='font-size: 11px; padding-top:10px'><b>" + frasePrazoPrevisto + "Para acompanhá-lo, Clique no link abaixo</b></div><div'><a  style='font-size: 14px; padding-top:21px; color: #000000;' href=" + rnConfiguracoes.caminhoVirtual.ToString() + "/pedidos.aspx" + ">MEUS PEDIDOS</a></div><br /><div style='font-size: 11px; padding-top:35px'><b>Anteciosamente,<br />Equipe " + rnConfiguracoes.nomeDoSite.ToString() + "</b></div><div style='font-size: 11px; padding-top:120px; padding-right:50px' align='center'><a style='font-family:Arial; font-size:11px; color:#000000' href=" + rnConfiguracoes.caminhoVirtual.ToString() + ">" + rnConfiguracoes.caminhoVirtual.ToString() + "</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a style='font-family:Arial; font-size:11px; color:#000000' href=mailto:" + rnConfiguracoes.toAddress.ToString() + ">" + rnConfiguracoes.toAddress.ToString() + "</a></div></td></tr></table>");
                return rnEmails.EnviaEmail("", clienteEmail, "", "", "", conteudo.ToString(), "Alteração de status " + rnConfiguracoes.nomeDoSite + "");
            }
            else
            {
                StringBuilder conteudo = new StringBuilder();
                conteudo.Append("<table cellpadding='0' cellspacing='0' style='width: 650px; height:601px; background-image: url(" + rnConfiguracoes.caminhoVirtual + "images/bkEmailAlteracaoDoPedido.jpg); background-repeat: no-repeat;'><tr><td valign='top' style='font-family:Arial; color: #000000; padding-left:50px; '><div style='font-size: 14px; padding-top:127px'><b>ALTERAÇÃO DA SITUAÇÃO DO PEDIDO</b></div><div style='font-size: 11px; padding-top:25px'>Prezado (a) Sr.(a)<br /><b>" + clienteNome + ",</b><br />Informamos que a situação do seu pedido <b>nº " + rnFuncoes.retornaIdCliente(Convert.ToInt32(pedidoId)) + "</b> foi alterada para:</div><div style='font-size: 20px; padding-top:21px'><b>&quot;" + status + "&quot;</b></div><div style='font-size: 11px; padding-top:10px'><b>Para acompanhá-lo, Clique no link abaixo</b></div><div'><a  style='font-size: 14px; padding-top:21px; color: #000000;' href=" + rnConfiguracoes.caminhoVirtual.ToString() + "/pedidos.aspx" + ">MEUS PEDIDOS</a></div><br /><div style='font-size: 11px; padding-top:35px'><b>Anteciosamente,<br />Equipe " + rnConfiguracoes.nomeDoSite.ToString() + "</b></div><div style='font-size: 11px; padding-top:120px; padding-right:50px' align='center'><a style='font-family:Arial; font-size:11px; color:#000000' href=" + rnConfiguracoes.caminhoVirtual.ToString() + ">" + rnConfiguracoes.caminhoVirtual.ToString() + "</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a style='font-family:Arial; font-size:11px; color:#000000' href=mailto:" + rnConfiguracoes.toAddress.ToString() + ">" + rnConfiguracoes.toAddress.ToString() + "</a></div></td></tr></table>");
                return rnEmails.EnviaEmail(rnConfiguracoes.emailPosVendas, clienteEmail, "", "", "", conteudo.ToString(), "Alteração de status " + rnConfiguracoes.nomeDoSite + "");
            }
            //Pagamento Confirmado - ID 3

            //Pedido sendo embalado - ID 4

            //Pedido enviado - ID 5

            //Pedido cancelado - ID 6


            //Pagamento não autorizado - ID 7


            //Separação de Estoque - ID 11



        }

        public static bool enviaCodigoDoTraking(string clienteNome, string pedidoId, string tracking, string tracking2, string tracking3, string tracking4, string clienteEmail)
        {
            try
            {
                StringBuilder conteudo = new StringBuilder();

                int remessas = 0;
                if (tracking != "") remessas = remessas + 1;
                if (tracking2 != "") remessas = remessas + 1;
                if (tracking3 != "") remessas = remessas + 1;
                if (tracking4 != "") remessas = 1;

                string codigoTracking = "";
                if (tracking != "") codigoTracking = tracking;
                if (tracking2 != "") codigoTracking = codigoTracking + ", " + tracking2;
                if (tracking3 != "") codigoTracking = codigoTracking + ", " + tracking3;
                if (tracking4 != "") codigoTracking = tracking4;

                string urlTracking = "http://www2.correios.com.br/sistemas/rastreamento";
                /*if (tracking != "") urlTracking = urlTracking + tracking;
                if (tracking2 != "") urlTracking = urlTracking + "%2C+" + tracking2;
                if (tracking3 != "") urlTracking = urlTracking + "%2C+" + tracking3;*/

                //if (tracking4 != "") codigoTracking = tracking;

                conteudo.Append("<table cellpadding='0' cellspacing='0' style='width: 650px; height:601px; background-image: url(" + rnConfiguracoes.caminhoVirtual + "images/bkEmailAlteracaoDoPedido.jpg); background-repeat: no-repeat;'><tr><td valign='top' style='font-family:Arial; color: #000000; padding-left:50px; '><div style='font-size: 14px; padding-top:127px'><b>ENVIO DO CÓDIGO DE RASTREAMENTO</b></div>");
                conteudo.Append("<div style='font-size: 11px; padding-top:25px'>Prezado (a) Sr.(a)<br /><b>" + clienteNome + ",</b><br />");
                if (remessas == 1) conteudo.Append("Seu pedido <b>nº " + rnFuncoes.retornaIdCliente(Convert.ToInt32(pedidoId)) + "</b> foi enviado em " + remessas + " remessa. Para rastreá-lo acesse os códigos abaixo:</div>");
                else if (remessas > 1) conteudo.Append("Seu pedido <b>nº " + rnFuncoes.retornaIdCliente(Convert.ToInt32(pedidoId)) + "</b> foi enviado em " + remessas + " remessas. Para rastreá-lo acesse os códigos abaixo:</div>");
                if (tracking != "" | tracking2 != "" | tracking3 != "") conteudo.Append("<div style='font-size: 11px; padding-top:30px'><b>Para acompanhá-lo, <a  style='font-size: 11px; padding-top:21px; color: #000000;' href=" + urlTracking + ">Clique Aqui</a></b></div>");
                else if (tracking4 != "") conteudo.Append("<div style='font-size: 11px; padding-top:30px'><b>Para acompanhá-lo, <a  style='font-size: 11px; padding-top:21px; color: #000000;' href=http://jadlog.com/sitejadlog/tracking.jad>Clique Aqui</a></b></div>");
                conteudo.Append("<div style='font-size: 11px; padding-top:10px'><b>Código de rastreamento:</b> " + codigoTracking + "</div>");
                conteudo.Append("<div style='padding-top:35px'><br /><div style='font-size: 11px; padding-top:35px'><b>Atenciosamente,<br />Equipe " + rnConfiguracoes.nomeDoSite.ToString() + "</b></div><div style='font-size: 11px; padding-top:120px; padding-right:50px' align='center'><a style='font-family:Arial; font-size:11px; color:#000000' href=" + rnConfiguracoes.caminhoVirtual.ToString() + ">" + rnConfiguracoes.caminhoVirtual.ToString() + "</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a style='font-family:Arial; font-size:11px; color:#000000' href=mailto:" + rnConfiguracoes.toAddress.ToString() + ">" + rnConfiguracoes.toAddress.ToString() + "</a></div></td></tr></table>");

                return rnEmails.EnviaEmail(rnConfiguracoes.emailPosVendas, clienteEmail, "", "", "", conteudo.ToString(), "Código do rastreamento");

                /*MailMessage msg = new MailMessage(rnConfiguracoes.toAddress, clienteEmail);
                msg.Subject = "Código do rastreamento";
                msg.SubjectEncoding = Encoding.Default;
                msg.Body = conteudo.ToString();
                msg.BodyEncoding = Encoding.Default;
                msg.IsBodyHtml = true;
			
                var smtpClient = new SmtpClient(rnConfiguracoes.emailSmtp);
                smtpClient.EnableSsl = true;
                smtpClient.Credentials = new System.Net.NetworkCredential(rnConfiguracoes.emailLogin, rnConfiguracoes.emailSenha);
                smtpClient.Send(msg);*/
            }
            catch (System.Net.Mail.SmtpException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
            return true;
        }

        public static bool enviaCodigoDoTrakingJadlog(int idPedido)
        {
            return enviaCodigoDoTrakingJadlog(idPedido, "", "");
        }
        public static bool enviaCodigoDoTrakingJadlogAsync(int idPedido, string numeroTracking)
        {
            return enviaCodigoDoTrakingJadlog(idPedido, numeroTracking);
        }
        public static bool enviaCodigoDoTrakingJadlog(int idPedido, string numeroTracking)
        {
            return enviaCodigoDoTrakingJadlog(idPedido, numeroTracking, "");
        }

        public static bool enviaCodigoDoTrakingTnt(int idPedidoEnvio)
        {
            return rnEmails.enviaCodigoDoTrakingTnt(idPedidoEnvio, "");
        }


        public static bool enviaEmailPagamentoConfirmado(int pedidoId)
        {
            string corpo = carregarTemplateArquivo(rnConfiguracoes.caminhoFisico + "\\templateemails\\pagamentoconfirmado\\" + "pagamento.html");
            string templateProdutos = carregarTemplateArquivo(rnConfiguracoes.caminhoFisico + "\\templateemails\\pedidos\\" + "produtos.html");
            string templateparcelas = carregarTemplateArquivo(rnConfiguracoes.caminhoFisico + "\\templateemails\\pedidos\\" + "parcelas.html");
            string templateboleto = carregarTemplateArquivo(rnConfiguracoes.caminhoFisico + "\\templateemails\\pedidos\\" + "boleto.html");

            var data = new dbSiteEntities();
            int idPedido = Convert.ToInt32(pedidoId);
            var pedido = (from c in data.tbPedidos where c.pedidoId == idPedido select c).First();
            var cliente = (from c in data.tbClientes where c.clienteId == pedido.clienteId select c).FirstOrDefault();
            var produtos = (from c in data.tbItensPedido where c.pedidoId == idPedido select c).ToList();

            string listaProdutos = "";
            foreach (var produto in produtos)
            {
                if ((produto.cancelado ?? false) == false)
                {
                    string linhaProduto = templateProdutos;
                    linhaProduto = linhaProduto.Replace("{{imagemproduto}}", rnConfiguracoes.caminhoCDN + "fotos/" + produto.produtoId + "/pequena_" + produto.tbProdutos.fotoDestaque + ".jpg");
                    linhaProduto = linhaProduto.Replace("{{produtonome}}", produto.tbProdutos.produtoNome);
                    linhaProduto = linhaProduto.Replace("{{quantidadeproduto}}", produto.itemQuantidade.ToString());
                    linhaProduto = linhaProduto.Replace("{{valorproduto}}", produto.itemValor.ToString("C"));
                    listaProdutos += linhaProduto;
                }
            }
            corpo = corpo.Replace("{{numeropedido}}", rnFuncoes.retornaIdCliente(Convert.ToInt32(pedidoId)).ToString());
            corpo = corpo.Replace("{{nomecliente}}", cliente.clienteNome);
            corpo = corpo.Replace("{{produtos}}", listaProdutos);




            if (pedido.tipoDePagamentoId == 11)
            {
                var cobrancas =
                    (from c in data.tbPedidoPagamento
                     where c.pedidoId == pedidoId && c.cancelado == false
                     orderby c.dataVencimento
                     select c).ToList();


                corpo = corpo.Replace("{{prazo}}", cobrancas.OrderByDescending(x => x.dataVencimento)
                    .First()
                    .dataVencimento.AddDays(3)
                    .AddDays(Convert.ToInt32(3))
                    .ToShortDateString());
                corpo = corpo.Replace("{{processamento}}", "3");
                corpo = corpo.Replace("{{entrega}}", pedido.prazoDeEntrega.ToString());
            }
            else
            {
                int prazoFinal = rnFuncoes.retornaPrazoDiasUteis(Convert.ToInt32(pedido.prazoDeEntrega), 0, (DateTime)pedido.prazoFinalPedido);
                var dataEntrega = pedido.prazoFinalPedido.Value.AddDays(prazoFinal).ToShortDateString();
                corpo = corpo.Replace("{{prazo}}", dataEntrega);
                corpo = corpo.Replace("{{processamento}}", pedido.prazoMaximoPedidos.ToString());
                corpo = corpo.Replace("{{entrega}}", pedido.prazoDeEntrega.ToString());
            }


            string urlBoleto = "";
            int tipoDePagamentoId = Convert.ToInt32(pedido.tipoDePagamentoId);
            int condDePagamentoId = Convert.ToInt32(pedido.condDePagamentoId);
            string imgCondicao = pedido.tbCondicoesDePagamento.imagemMini;
            string imgCondicao2 = "";
            string condicaoNome = pedido.tbCondicoesDePagamento.condicaoNome;
            string condicaoNome2 = "";

            string clienteCPFCNPJ = cliente.clienteCPFCNPJ;
            string clienteCidade = cliente.clienteCidade;
            string clienteRua = cliente.clienteRua;
            string clienteBairro = cliente.clienteBairro;
            string clienteCep = cliente.clienteCep;
            string clienteEstado = cliente.clienteEstado;


            if (tipoDePagamentoId == 8)
            {
                string templateformas =
                    carregarTemplateArquivo(rnConfiguracoes.caminhoFisico +
                                            "\\templateemails\\pedidos\\" + "duasformas.html");
                int condDePagamentoId1 = Convert.ToInt32(pedido.condDePagamentoId1);
                int condDePagamentoId2 = Convert.ToInt32(pedido.condDePagamentoId2);
                var condicao1 =
                    (from c in data.tbCondicoesDePagamento where c.condicaoId == condDePagamentoId1 select c).FirstOrDefault
                        ();
                var condicao2 =
                    (from c in data.tbCondicoesDePagamento where c.condicaoId == condDePagamentoId2 select c).FirstOrDefault
                        ();


                templateformas = templateformas.Replace("{{imagempagamento1}}", condicao1.imagemMini);
                templateformas = templateformas.Replace("{{imagempagamento2}}", condicao2.imagemMini);
                templateformas = templateformas.Replace("{{nomepagamento1}}", condicao1.condicaoNome);
                templateformas = templateformas.Replace("{{nomepagamento2}}", condicao2.condicaoNome);
                corpo = corpo.Replace("{{formapagamento}}", templateformas);

                string parcelas2Formas = templateparcelas;
                parcelas2Formas = parcelas2Formas.Replace("{{quantidadeparcelas}}", pedido.parcelasPagamento1.ToString());
                parcelas2Formas = parcelas2Formas.Replace("{{valorparcelas}}", Convert.ToDecimal(pedido.valorParcelaPagamento1).ToString("C"));
                parcelas2Formas += templateparcelas;
                parcelas2Formas = parcelas2Formas.Replace("{{quantidadeparcelas}}", pedido.parcelasPagamento2.ToString());
                parcelas2Formas = parcelas2Formas.Replace("{{valorparcelas}}",
                    Convert.ToDecimal(pedido.valorParcelaPagamento2).ToString("C"));
                corpo = corpo.Replace("{{parcelas}}", parcelas2Formas);

                if (pedido.tipoDePagamentoId1 == 1 | pedido.tipoDePagamentoId2 == 1)
                {
                    var valorBoleto = Convert.ToInt32(pedido.tipoDePagamentoId1) == 1
                        ? Convert.ToDecimal(pedido.valorPagamento1)
                        : Convert.ToDecimal(pedido.valorPagamento2);
                    urlBoleto = rnConfiguracoes.caminhoVirtual + "boleto.aspx?sacado=" + cliente.clienteNome.Replace(" ", "+") +
                                "&CPFCNPJ=" + clienteCPFCNPJ + "&endereco=" + clienteRua.Replace(" ", "+") + "&bairro=" +
                                clienteBairro.Replace(" ", "+") + "&cidade=" + clienteCidade.Replace(" ", "+") + "&cep=" +
                                clienteCep + "&uf=" + clienteEstado + "&numeroDocumento=" + pedidoId + "&nossoNumero=" +
                                pedidoId + "&valor=" + valorBoleto + "";
                    corpo = corpo.Replace("{{linkboleto}}", templateboleto.Replace("{{linkboleto}}", urlBoleto));
                }
                else
                {
                    corpo = corpo.Replace("{{linkboleto}}", "");
                }
            }
            else
            {
                string templateformas =
                    carregarTemplateArquivo(rnConfiguracoes.caminhoFisico +
                                            "\\templateemails\\pedidos\\" + "umaforma.html");

                templateformas = templateformas.Replace("{{imagempagamento1}}", pedido.tbCondicoesDePagamento.imagemMini);
                templateformas = templateformas.Replace("{{nomepagamento1}}", pedido.tbCondicoesDePagamento.condicaoNome);
                corpo = corpo.Replace("{{formapagamento}}", templateformas);



                var pagamentosPedido = (from c in data.tbPedidoPagamento
                                        where
                                            c.pedidoId == pedido.pedidoId && c.cancelado == false && c.pago == true &&
                                            c.pagamentoNaoAutorizado == false
                                        select c).ToList();

                if (pagamentosPedido.Count > 1)
                {
                    string parcelas2Formas = "";
                    foreach (var pagamentoPedido in pagamentosPedido)
                    {
                        var tempParcela = templateparcelas;
                        tempParcela = tempParcela.Replace("{{quantidadeparcelas}}", pagamentoPedido.parcelas.ToString());
                        tempParcela = tempParcela.Replace("{{valorparcelas}}", Convert.ToDecimal(pagamentoPedido.valorParcela).ToString("C"));
                        parcelas2Formas += tempParcela;
                    }
                    corpo = corpo.Replace("{{parcelas}}", parcelas2Formas);
                }
                else
                {
                    string parcelasUmaForma = templateparcelas;
                    //parcelasUmaForma = parcelasUmaForma.Replace("{{quantidadeparcelas}}", pedido.numeroDeParcelas.ToString());
                    parcelasUmaForma = parcelasUmaForma.Replace("{{quantidadeparcelas}}", pagamentosPedido.FirstOrDefault().parcelas.ToString());
                    parcelasUmaForma = parcelasUmaForma.Replace("{{valorparcelas}}", Convert.ToDecimal(pedido.valorDaParcela).ToString("C"));
                    corpo = corpo.Replace("{{parcelas}}", parcelasUmaForma);
                }



                if (tipoDePagamentoId == 1)
                {
                    urlBoleto = rnConfiguracoes.caminhoVirtual + "boleto.aspx?sacado=" + cliente.clienteNome.Replace(" ", "+") + "&CPFCNPJ=" + clienteCPFCNPJ + "&endereco=" + clienteRua.Replace(" ", "+") + "&bairro=" + clienteBairro.Replace(" ", "+") + "&cidade=" + clienteCidade.Replace(" ", "+") + "&cep=" + clienteCep + "&uf=" + clienteEstado + "&numeroDocumento=" + pedidoId + "&nossoNumero=" + pedidoId + "&valor=" + pedido.valorCobrado + "";
                    corpo = corpo.Replace("{{linkboleto}}", templateboleto.Replace("{{linkboleto}}", urlBoleto));
                }
                else
                {
                    corpo = corpo.Replace("{{linkboleto}}", "");
                }
            }

            corpo = corpo.Replace("{{subtotal}}", Convert.ToDecimal(pedido.valorTotalGeral).ToString("C"));
            corpo = corpo.Replace("{{frete}}", Convert.ToDecimal(pedido.valorDoFrete).ToString("C"));
            corpo = corpo.Replace("{{desconto}}", Convert.ToDecimal(pedido.valorDoDescontoDoPagamento).ToString("C"));
            corpo = corpo.Replace("{{total}}", Convert.ToDecimal(pedido.valorCobrado).ToString("C"));
            corpo = corpo.Replace("{{enderecocliente}}", pedido.endRua + ", " + pedido.endNumero + " " + pedido.endComplemento + "<br>" + pedido.endBairro + "<br>Cep: " + pedido.endCep + " - " + pedido.endCidade + " - " + pedido.endEstado);

            return EnviaEmail("", cliente.clienteEmail, "", "automaticos2.graodegente@gmail.com", "", corpo, "Pagamento Confirmado");

        }
        public static bool enviaEmailPagamentoConfirmadoPlano(int pedidoId)
        {
            string corpo = carregarTemplateArquivo(rnConfiguracoes.caminhoFisico + "\\templateemails\\pagamentoconfirmadoplano\\" + "pagamento.html");
            string templateProdutos = carregarTemplateArquivo(rnConfiguracoes.caminhoFisico + "\\templateemails\\pedidos\\" + "produtos.html");
            string templateparcelas = carregarTemplateArquivo(rnConfiguracoes.caminhoFisico + "\\templateemails\\pedidos\\" + "parcelas.html");
            string templateboleto = carregarTemplateArquivo(rnConfiguracoes.caminhoFisico + "\\templateemails\\pedidos\\" + "boleto.html");
            string templateboletoplano = carregarTemplateArquivo(rnConfiguracoes.caminhoFisico + "\\templateemails\\pagamentoconfirmadoplano\\" + "boletoplano.html");
            string templateplanoparcelas = carregarTemplateArquivo(rnConfiguracoes.caminhoFisico + "\\templateemails\\pagamentoconfirmadoplano\\" + "planoparcelas.html");
            string templateplanoparcela = carregarTemplateArquivo(rnConfiguracoes.caminhoFisico + "\\templateemails\\pagamentoconfirmadoplano\\" + "planoparcela.html");
            string templateplanoparcelapaga = carregarTemplateArquivo(rnConfiguracoes.caminhoFisico + "\\templateemails\\pagamentoconfirmadoplano\\" + "planoparcelapaga.html");

            var data = new dbSiteEntities();
            int idPedido = Convert.ToInt32(pedidoId);
            var pedido = (from c in data.tbPedidos where c.pedidoId == idPedido select c).First();
            var cliente = (from c in data.tbClientes where c.clienteId == pedido.clienteId select c).FirstOrDefault();
            var produtos = (from c in data.tbItensPedido where c.pedidoId == idPedido select c).ToList();

            string listaProdutos = "";
            foreach (var produto in produtos)
            {
                if ((produto.cancelado ?? false) == false)
                {
                    string linhaProduto = templateProdutos;
                    linhaProduto = linhaProduto.Replace("{{imagemproduto}}", rnConfiguracoes.caminhoCDN + "fotos/" + produto.produtoId + "/pequena_" + produto.tbProdutos.fotoDestaque + ".jpg");
                    linhaProduto = linhaProduto.Replace("{{produtonome}}", produto.tbProdutos.produtoNome);
                    linhaProduto = linhaProduto.Replace("{{quantidadeproduto}}", produto.itemQuantidade.ToString());
                    linhaProduto = linhaProduto.Replace("{{valorproduto}}", produto.itemValor.ToString("C"));
                    listaProdutos += linhaProduto;
                }
            }
            corpo = corpo.Replace("{{numeropedido}}", rnFuncoes.retornaIdCliente(Convert.ToInt32(pedidoId)).ToString());
            corpo = corpo.Replace("{{nomecliente}}", cliente.clienteNome);
            corpo = corpo.Replace("{{produtos}}", listaProdutos);



            var cobrancas =
                            (from c in data.tbPedidoPagamento
                             where c.pedidoId == pedidoId && c.cancelado == false
                             orderby c.dataVencimento
                             select c).ToList();


            corpo = corpo.Replace("{{prazo}}", cobrancas.OrderByDescending(x => x.dataVencimento)
                    .First()
                    .dataVencimento.AddDays(3)
                    .AddDays(Convert.ToInt32(3))
                    .ToShortDateString());
            corpo = corpo.Replace("{{processamento}}", "3");
            corpo = corpo.Replace("{{entrega}}", pedido.prazoDeEntrega.ToString());


            string urlBoleto = "";
            int tipoDePagamentoId = Convert.ToInt32(pedido.tipoDePagamentoId);
            int condDePagamentoId = Convert.ToInt32(pedido.condDePagamentoId);
            string imgCondicao = pedido.tbCondicoesDePagamento.imagemMini;
            string imgCondicao2 = "";
            string condicaoNome = pedido.tbCondicoesDePagamento.condicaoNome;
            string condicaoNome2 = "";

            string clienteCPFCNPJ = cliente.clienteCPFCNPJ;
            string clienteCidade = cliente.clienteCidade;
            string clienteRua = cliente.clienteRua;
            string clienteBairro = cliente.clienteBairro;
            string clienteCep = cliente.clienteCep;
            string clienteEstado = cliente.clienteEstado;


            if (tipoDePagamentoId == 8)
            {
                string templateformas =
                    carregarTemplateArquivo(rnConfiguracoes.caminhoFisico +
                                            "\\templateemails\\pedidos\\" + "duasformas.html");
                int condDePagamentoId1 = Convert.ToInt32(pedido.condDePagamentoId1);
                int condDePagamentoId2 = Convert.ToInt32(pedido.condDePagamentoId2);
                var condicao1 =
                    (from c in data.tbCondicoesDePagamento where c.condicaoId == condDePagamentoId1 select c).FirstOrDefault
                        ();
                var condicao2 =
                    (from c in data.tbCondicoesDePagamento where c.condicaoId == condDePagamentoId2 select c).FirstOrDefault
                        ();


                templateformas = templateformas.Replace("{{imagempagamento1}}", condicao1.imagemMini);
                templateformas = templateformas.Replace("{{imagempagamento2}}", condicao2.imagemMini);
                templateformas = templateformas.Replace("{{nomepagamento1}}", condicao1.condicaoNome);
                templateformas = templateformas.Replace("{{nomepagamento2}}", condicao2.condicaoNome);
                corpo = corpo.Replace("{{formapagamento}}", templateformas);

                string parcelas2Formas = templateparcelas;
                parcelas2Formas = parcelas2Formas.Replace("{{quantidadeparcelas}}", pedido.parcelasPagamento1.ToString());
                parcelas2Formas = parcelas2Formas.Replace("{{valorparcelas}}", Convert.ToDecimal(pedido.valorParcelaPagamento1).ToString("C"));
                parcelas2Formas += templateparcelas;
                parcelas2Formas = parcelas2Formas.Replace("{{quantidadeparcelas}}", pedido.parcelasPagamento2.ToString());
                parcelas2Formas = parcelas2Formas.Replace("{{valorparcelas}}",
                    Convert.ToDecimal(pedido.valorParcelaPagamento2).ToString("C"));
                corpo = corpo.Replace("{{parcelas}}", parcelas2Formas);

                var pagamentosBoleto =
                (from c in data.tbPedidoPagamento
                 where c.pedidoId == idPedido && c.idOperadorPagamento == 1
                 select c).FirstOrDefault();
                if (pagamentosBoleto != null)
                {
                    urlBoleto = "https://apiv2.graodegente.com.br/v1/pagamentos/" + rnFuncoes.retornaIdCliente(idPedido) + "/boleto/" + pagamentosBoleto.numeroCobranca + "?autoImpressao=undefined";
                    //urlBoleto = rnConfiguracoes.caminhoVirtual + "boleto.aspx?cobranca=" + pagamentosBoleto.numeroCobranca + "&pedido=" + rnFuncoes.retornaIdCliente(idPedido);
                    corpo = corpo.Replace("{{linkboleto}}", templateboleto.Replace("{{linkboleto}}", urlBoleto));
                    corpo = corpo.Replace("{{styleplano}}", "display: none;");
                }
                else
                {
                    corpo = corpo.Replace("{{linkboleto}}", "");
                    corpo = corpo.Replace("{{styleplano}}", "display: none;");
                }
            }
            else
            {
                string templateformas =
                    carregarTemplateArquivo(rnConfiguracoes.caminhoFisico +
                                            "\\templateemails\\pedidos\\" + "umaforma.html");

                templateformas = templateformas.Replace("{{imagempagamento1}}", pedido.tbCondicoesDePagamento.imagemMini);
                templateformas = templateformas.Replace("{{nomepagamento1}}", pedido.tbCondicoesDePagamento.condicaoNome);
                corpo = corpo.Replace("{{formapagamento}}", templateformas);

                string parcelasUmaForma = templateparcelas;
                parcelasUmaForma = parcelasUmaForma.Replace("{{quantidadeparcelas}}", pedido.numeroDeParcelas.ToString());
                parcelasUmaForma = parcelasUmaForma.Replace("{{valorparcelas}}", Convert.ToDecimal(pedido.valorDaParcela).ToString("C"));
                corpo = corpo.Replace("{{parcelas}}", parcelasUmaForma);

                var pagamentosBoleto =
                (from c in data.tbPedidoPagamento
                 where c.pedidoId == idPedido && c.idOperadorPagamento == 1
                 select c).FirstOrDefault();

                if (pagamentosBoleto != null && pedido.tipoDePagamentoId != 11)
                {
                    urlBoleto = "https://apiv2.graodegente.com.br/v1/pagamentos/" + rnFuncoes.retornaIdCliente(idPedido) + "/boleto/" + pagamentosBoleto.numeroCobranca + "?autoImpressao=undefined";
                    corpo = corpo.Replace("{{linkboleto}}", templateboleto.Replace("{{linkboleto}}", urlBoleto));
                    corpo = corpo.Replace("{{styleplano}}", "display: none;");
                }
                else if (pagamentosBoleto != null && pedido.tipoDePagamentoId == 11)
                {
                    urlBoleto = "https://apiv2.graodegente.com.br/v1/pagamentos/" + rnFuncoes.retornaIdCliente(idPedido) + "/boleto/" + pagamentosBoleto.numeroCobranca + "?autoImpressao=undefined";
                    corpo = corpo.Replace("{{linkboleto}}", templateboletoplano.Replace("{{linkboleto}}", urlBoleto));

                    string pagamentosplano = "";
                    var pagamentosPlanoBoleto =
                    (from c in data.tbPedidoPagamento
                     where c.pedidoId == idPedido && c.idOperadorPagamento == 1
                     select c).ToList();
                    foreach (var pagamentoPlano in pagamentosPlanoBoleto)
                    {
                        if (pagamentoPlano.pago)
                        {
                            var linhaPagamento = templateplanoparcelapaga;
                            linhaPagamento = linhaPagamento.Replace("{{datavencimento}}", pagamentoPlano.dataVencimento.ToShortDateString());
                            pagamentosplano += linhaPagamento;
                        }
                        else
                        {
                            /*string urlBoletoPlano = rnConfiguracoes.caminhoVirtual +
                                                    "boleto.aspx?pedido=" + rnFuncoes.retornaIdCliente(idPedido) +
                                                    "&cobranca=" + pagamentoPlano.numeroCobranca;*/
                            string urlBoletoPlano = "https://apiv2.graodegente.com.br/v1/pagamentos/" + rnFuncoes.retornaIdCliente(idPedido) + "/boleto/" + pagamentosBoleto.numeroCobranca + "?autoImpressao=undefined";

                            var linhaPagamento = templateplanoparcela;
                            linhaPagamento = linhaPagamento.Replace("{{linkboleto}}", urlBoletoPlano);
                            linhaPagamento = linhaPagamento.Replace("{{datavencimento}}",
                                pagamentoPlano.dataVencimento.ToShortDateString());
                            pagamentosplano += linhaPagamento;
                        }
                    }
                    templateplanoparcelas = templateplanoparcelas.Replace("{{planoparcela}}", pagamentosplano);
                    corpo = corpo.Replace("{{planoparcelas}}", templateplanoparcelas);

                }
                else
                {
                    corpo = corpo.Replace("{{linkboleto}}", "");
                    corpo = corpo.Replace("{{styleplano}}", "display: none;");
                }
            }

            corpo = corpo.Replace("{{subtotal}}", Convert.ToDecimal(pedido.valorTotalGeral).ToString("C"));
            corpo = corpo.Replace("{{frete}}", Convert.ToDecimal(pedido.valorDoFrete).ToString("C"));
            corpo = corpo.Replace("{{desconto}}", Convert.ToDecimal(pedido.valorDoDescontoDoPagamento).ToString("C"));
            corpo = corpo.Replace("{{total}}", Convert.ToDecimal(pedido.valorCobrado).ToString("C"));
            corpo = corpo.Replace("{{enderecocliente}}", pedido.endRua + ", " + pedido.endNumero + " " + pedido.endComplemento + "<br>" + pedido.endBairro + "<br>Cep: " + pedido.endCep + " - " + pedido.endCidade + " - " + pedido.endEstado);

            return EnviaEmail("", cliente.clienteEmail, "", "automaticos2.graodegente@gmail.com", "", corpo, "Pagamento Confirmado");

        }


        public static bool enviaEmailPedidoProcessado(int pedidoId)
        {
            string corpo = carregarTemplateArquivo(rnConfiguracoes.caminhoFisico + "\\templateemails\\processado\\" + "processado.html");
            string templateProdutos = carregarTemplateArquivo(rnConfiguracoes.caminhoFisico + "\\templateemails\\pedidos\\" + "produtos.html");
            string templateparcelas = carregarTemplateArquivo(rnConfiguracoes.caminhoFisico + "\\templateemails\\pedidos\\" + "parcelas.html");
            string templateboleto = carregarTemplateArquivo(rnConfiguracoes.caminhoFisico + "\\templateemails\\pedidos\\" + "boleto.html");

            var data = new dbSiteEntities();
            int idPedido = Convert.ToInt32(pedidoId);
            var pedido = (from c in data.tbPedidos where c.pedidoId == idPedido select c).First();
            var cliente = (from c in data.tbClientes where c.clienteId == pedido.clienteId select c).FirstOrDefault();
            var produtos = (from c in data.tbItensPedido where c.pedidoId == idPedido select c).ToList();

            string listaProdutos = "";
            foreach (var produto in produtos)
            {
                if ((produto.cancelado ?? false) == false)
                {
                    string linhaProduto = templateProdutos;
                    linhaProduto = linhaProduto.Replace("{{imagemproduto}}", rnConfiguracoes.caminhoCDN + "fotos/" + produto.produtoId + "/pequena_" + produto.tbProdutos.fotoDestaque + ".jpg");
                    linhaProduto = linhaProduto.Replace("{{produtonome}}", produto.tbProdutos.produtoNome);
                    linhaProduto = linhaProduto.Replace("{{quantidadeproduto}}", produto.itemQuantidade.ToString());
                    linhaProduto = linhaProduto.Replace("{{valorproduto}}", produto.itemValor.ToString("C"));
                    listaProdutos += linhaProduto;
                }
            }
            corpo = corpo.Replace("{{numeropedido}}", rnFuncoes.retornaIdCliente(Convert.ToInt32(pedidoId)).ToString());
            corpo = corpo.Replace("{{nomecliente}}", cliente.clienteNome);
            corpo = corpo.Replace("{{produtos}}", listaProdutos);


            if (pedido.tipoDePagamentoId == 11)
            {
                var cobrancas =
                    (from c in data.tbPedidoPagamento
                     where c.pedidoId == pedidoId && c.cancelado == false
                     orderby c.dataVencimento
                     select c).ToList();


                corpo = corpo.Replace("{{prazo}}", cobrancas.OrderByDescending(x => x.dataVencimento)
                    .First()
                    .dataVencimento.AddDays(3)
                    .AddDays(Convert.ToInt32(3))
                    .ToShortDateString());
                corpo = corpo.Replace("{{processamento}}", "3");
                corpo = corpo.Replace("{{entrega}}", pedido.prazoDeEntrega.ToString());
            }
            else
            {
                int prazoFinal = rnFuncoes.retornaPrazoDiasUteis(Convert.ToInt32(pedido.prazoDeEntrega), 0, (DateTime)pedido.prazoFinalPedido);
                var dataEntrega = pedido.prazoFinalPedido.Value.AddDays(prazoFinal).ToShortDateString();
                corpo = corpo.Replace("{{prazo}}", dataEntrega);
                corpo = corpo.Replace("{{processamento}}", pedido.prazoMaximoPedidos.ToString());
                corpo = corpo.Replace("{{entrega}}", pedido.prazoDeEntrega.ToString());
            }


            string urlBoleto = "";
            int tipoDePagamentoId = Convert.ToInt32(pedido.tipoDePagamentoId);
            int condDePagamentoId = Convert.ToInt32(pedido.condDePagamentoId);
            string imgCondicao = pedido.tbCondicoesDePagamento.imagemMini;
            string imgCondicao2 = "";
            string condicaoNome = pedido.tbCondicoesDePagamento.condicaoNome;
            string condicaoNome2 = "";

            string clienteCPFCNPJ = cliente.clienteCPFCNPJ;
            string clienteCidade = cliente.clienteCidade;
            string clienteRua = cliente.clienteRua;
            string clienteBairro = cliente.clienteBairro;
            string clienteCep = cliente.clienteCep;
            string clienteEstado = cliente.clienteEstado;


            if (tipoDePagamentoId == 8)
            {
                string templateformas =
                    carregarTemplateArquivo(rnConfiguracoes.caminhoFisico +
                                            "\\templateemails\\pedidos\\" + "duasformas.html");
                int condDePagamentoId1 = Convert.ToInt32(pedido.condDePagamentoId1);
                int condDePagamentoId2 = Convert.ToInt32(pedido.condDePagamentoId2);
                var condicao1 =
                    (from c in data.tbCondicoesDePagamento where c.condicaoId == condDePagamentoId1 select c).FirstOrDefault
                        ();
                var condicao2 =
                    (from c in data.tbCondicoesDePagamento where c.condicaoId == condDePagamentoId2 select c).FirstOrDefault
                        ();


                templateformas = templateformas.Replace("{{imagempagamento1}}", condicao1.imagemMini);
                templateformas = templateformas.Replace("{{imagempagamento2}}", condicao2.imagemMini);
                templateformas = templateformas.Replace("{{nomepagamento1}}", condicao1.condicaoNome);
                templateformas = templateformas.Replace("{{nomepagamento2}}", condicao2.condicaoNome);
                corpo = corpo.Replace("{{formapagamento}}", templateformas);

                string parcelas2Formas = templateparcelas;
                parcelas2Formas = parcelas2Formas.Replace("{{quantidadeparcelas}}", pedido.parcelasPagamento1.ToString());
                parcelas2Formas = parcelas2Formas.Replace("{{valorparcelas}}", Convert.ToDecimal(pedido.valorParcelaPagamento1).ToString("C"));
                parcelas2Formas += templateparcelas;
                parcelas2Formas = parcelas2Formas.Replace("{{quantidadeparcelas}}", pedido.parcelasPagamento2.ToString());
                parcelas2Formas = parcelas2Formas.Replace("{{valorparcelas}}",
                    Convert.ToDecimal(pedido.valorParcelaPagamento2).ToString("C"));
                corpo = corpo.Replace("{{parcelas}}", parcelas2Formas);

                if (pedido.tipoDePagamentoId1 == 1 | pedido.tipoDePagamentoId2 == 1)
                {
                    var valorBoleto = Convert.ToInt32(pedido.tipoDePagamentoId1) == 1
                        ? Convert.ToDecimal(pedido.valorPagamento1)
                        : Convert.ToDecimal(pedido.valorPagamento2);
                    urlBoleto = rnConfiguracoes.caminhoVirtual + "boleto.aspx?sacado=" + cliente.clienteNome.Replace(" ", "+") +
                                "&CPFCNPJ=" + clienteCPFCNPJ + "&endereco=" + clienteRua.Replace(" ", "+") + "&bairro=" +
                                clienteBairro.Replace(" ", "+") + "&cidade=" + clienteCidade.Replace(" ", "+") + "&cep=" +
                                clienteCep + "&uf=" + clienteEstado + "&numeroDocumento=" + pedidoId + "&nossoNumero=" +
                                pedidoId + "&valor=" + valorBoleto + "";
                    corpo = corpo.Replace("{{linkboleto}}", templateboleto.Replace("{{linkboleto}}", urlBoleto));
                }
                else
                {
                    corpo = corpo.Replace("{{linkboleto}}", "");
                }
            }
            else
            {
                string templateformas =
                    carregarTemplateArquivo(rnConfiguracoes.caminhoFisico +
                                            "\\templateemails\\pedidos\\" + "umaforma.html");

                templateformas = templateformas.Replace("{{imagempagamento1}}", pedido.tbCondicoesDePagamento.imagemMini);
                templateformas = templateformas.Replace("{{nomepagamento1}}", pedido.tbCondicoesDePagamento.condicaoNome);
                corpo = corpo.Replace("{{formapagamento}}", templateformas);

                string parcelasUmaForma = templateparcelas;
                parcelasUmaForma = parcelasUmaForma.Replace("{{quantidadeparcelas}}", pedido.numeroDeParcelas.ToString());
                parcelasUmaForma = parcelasUmaForma.Replace("{{valorparcelas}}", Convert.ToDecimal(pedido.valorDaParcela).ToString("C"));
                corpo = corpo.Replace("{{parcelas}}", parcelasUmaForma);

                if (tipoDePagamentoId == 1)
                {
                    urlBoleto = "https://apiv2.graodegente.com.br/v1/pagamentos/" + rnFuncoes.retornaIdCliente(idPedido) + "/boleto/" + pedido.tbPedidoPagamento.First().numeroCobranca + "?autoImpressao=undefined";
                    corpo = corpo.Replace("{{linkboleto}}", templateboleto.Replace("{{linkboleto}}", urlBoleto));
                }
                else
                {
                    corpo = corpo.Replace("{{linkboleto}}", "");
                }
            }

            corpo = corpo.Replace("{{subtotal}}", Convert.ToDecimal(pedido.valorTotalGeral).ToString("C"));
            corpo = corpo.Replace("{{frete}}", Convert.ToDecimal(pedido.valorDoFrete).ToString("C"));
            corpo = corpo.Replace("{{desconto}}", Convert.ToDecimal(pedido.valorDoDescontoDoPagamento).ToString("C"));
            corpo = corpo.Replace("{{total}}", Convert.ToDecimal(pedido.valorCobrado).ToString("C"));
            corpo = corpo.Replace("{{enderecocliente}}", pedido.endRua + ", " + pedido.endNumero + " " + pedido.endComplemento + "<br>" + pedido.endBairro + "<br>Cep: " + pedido.endCep + " - " + pedido.endCidade + " - " + pedido.endEstado);

            return EnviaEmail("", cliente.clienteEmail, "", "automaticos2.graodegente@gmail.com", "", corpo, "Pedido Processado");

        }

        public static bool enviaEmailPagamentoRecusado(int pedidoId)
        {
            string corpo = carregarTemplateArquivo(rnConfiguracoes.caminhoFisico + "\\templateemails\\naoprocessado\\" + "naoprocessado.html");

            var data = new dbSiteEntities();
            int idPedido = Convert.ToInt32(pedidoId);
            var pedido = (from c in data.tbPedidos where c.pedidoId == idPedido select c).First();
            var cliente = (from c in data.tbClientes where c.clienteId == pedido.clienteId select c).FirstOrDefault();

            corpo = corpo.Replace("{{nomecliente}}", cliente.clienteNome);

            return EnviaEmail("", cliente.clienteEmail.Trim(), "", "automaticos2.graodegente@gmail.com", "", corpo, "Pagamento Não Processado");

        }

        public static bool enviaEmailPedidoCancelado(int pedidoId)
        {
            string corpo = carregarTemplateArquivo(rnConfiguracoes.caminhoFisico + "\\templateemails\\cancelado\\" + "cancelado.html");

            var data = new dbSiteEntities();
            int idPedido = Convert.ToInt32(pedidoId);
            var pedido = (from c in data.tbPedidos where c.pedidoId == idPedido select c).First();
            var cliente = (from c in data.tbClientes where c.clienteId == pedido.clienteId select c).FirstOrDefault();

            corpo = corpo.Replace("{{nomecliente}}", cliente.clienteNome);
            corpo = corpo.Replace("{{numeropedido}}", rnFuncoes.retornaIdCliente(Convert.ToInt32(pedidoId)).ToString());

            return EnviaEmail("", cliente.clienteEmail, "", "automaticos2.graodegente@gmail.com", "", corpo, "Pedido Cancelado");

        }

        public static bool enviaSegundaViaDoBoleto(string clienteNome, string pedidoId, string linkDoBoleto, string clienteEmail)
        {
            string corpo = carregarTemplateArquivo(rnConfiguracoes.caminhoFisico + "\\templateemails\\segundavia\\" + "segundavia.html");
            corpo = corpo.Replace("{{nomecliente}}", clienteNome);
            corpo = corpo.Replace("{{numeropedido}}", rnFuncoes.retornaIdCliente(Convert.ToInt32(pedidoId)).ToString());
            corpo = corpo.Replace("{{linkboleto}}", linkDoBoleto);
            return EnviaEmail("", clienteEmail, "", "automaticos2.graodegente@gmail.com", "", corpo, "Segunda via de Boleto");
            //return EnviaEmailPostMark("", clienteEmail, "", "automaticos2.graodegente@gmail.com", "", corpo, "Segunda via de Boleto");

        }

        public static bool enviaCodigoDoTrakingCorreios(int idPedidoEnvio, int pedidoId)
        {
            try
            {
                var pedidosDc = new dbSiteEntities();
                var volumesPedido = (from c in pedidosDc.tbPedidoPacote where c.idPedidoEnvio == idPedidoEnvio select c);
                var pacotes = (from c in pedidosDc.tbPedidoPacote where c.idPedidoEnvio == idPedidoEnvio select c).FirstOrDefault();
                var pedido = (from c in pedidosDc.tbPedidos where c.pedidoId == pedidoId select c).First();
                int volumeAtual = 0;
                string codigoTracking = "";
                string urlTracking = "http://www2.correios.com.br/sistemas/rastreamento";

                var prazoData = DateTime.Now;

                if (pacotes.dataDeCarregamento != null)
                {
                    prazoData = (DateTime)pacotes.dataDeCarregamento;
                }
                else if (pacotes.prazoFinalEntrega != null)
                {
                    prazoData = (DateTime)pacotes.prazoFinalEntrega;
                }

                string corpo = carregarTemplateArquivo(rnConfiguracoes.caminhoFisico + "\\templateemails\\enviado\\" + "enviado.html");
                string templateRastreio = carregarTemplateArquivo(rnConfiguracoes.caminhoFisico + "\\templateemails\\enviado\\" + "codigorastreio.htm");

                if (pedido.condDePagamentoId == 26)
                {
                    corpo = carregarTemplateArquivo(rnConfiguracoes.caminhoFisico + "\\templateemails\\enviado\\" + "enviado_ml.html");
                    templateRastreio = carregarTemplateArquivo(rnConfiguracoes.caminhoFisico + "\\templateemails\\enviado\\" + "codigorastreio_ml.htm");
                }
                var data = new dbSiteEntities();
                var cliente = (from c in data.tbClientes where c.clienteId == pedido.clienteId select c).FirstOrDefault();


                int prazoFinal = rnFuncoes.retornaPrazoDiasUteis(Convert.ToInt32(pedido.prazoDeEntrega), 0);
                //var dataEntrega = DateTime.Now.AddDays(prazoFinal).ToShortDateString();
                var dataEntrega = prazoData.AddDays(prazoFinal).ToShortDateString();
                corpo = corpo.Replace("{{prazo}}", dataEntrega);
                corpo = corpo.Replace("{{processamento}}", pedido.prazoMaximoPedidos.ToString());
                corpo = corpo.Replace("{{entrega}}", pedido.prazoDeEntrega.ToString());
                corpo = corpo.Replace("{{nomecliente}}", cliente.clienteNome);
                corpo = corpo.Replace("{{numeropedido}}", rnFuncoes.retornaIdCliente(Convert.ToInt32(pedidoId)).ToString());

                string urlBoleto = "";
                int tipoDePagamentoId = Convert.ToInt32(pedido.tipoDePagamentoId);
                int condDePagamentoId = Convert.ToInt32(pedido.condDePagamentoId);
                string imgCondicao = pedido.tbCondicoesDePagamento.imagemMini;
                string imgCondicao2 = "";
                string condicaoNome = pedido.tbCondicoesDePagamento.condicaoNome;
                string condicaoNome2 = "";

                string clienteCPFCNPJ = cliente.clienteCPFCNPJ;
                string clienteCidade = cliente.clienteCidade;
                string clienteRua = cliente.clienteRua;
                string clienteBairro = cliente.clienteBairro;
                string clienteCep = cliente.clienteCep;
                string clienteEstado = cliente.clienteEstado;


                if (tipoDePagamentoId == 8)
                    corpo = corpo.Replace("{{subtotal}}", Convert.ToDecimal(pedido.valorTotalGeral).ToString("C"));
                corpo = corpo.Replace("{{frete}}", Convert.ToDecimal(pedido.valorDoFrete).ToString("C"));
                corpo = corpo.Replace("{{desconto}}", Convert.ToDecimal(pedido.valorDoDescontoDoPagamento).ToString("C"));
                corpo = corpo.Replace("{{total}}", Convert.ToDecimal(pedido.valorCobrado).ToString("C"));
                corpo = corpo.Replace("{{enderecocliente}}", pedido.endRua + ", " + pedido.endNumero + " " + pedido.endComplemento + "<br>" + pedido.endBairro + "<br>Cep: " + pedido.endCep + " - " + pedido.endCidade + " - " + pedido.endEstado);



                string nome = cliente.clienteNome;
                string email = cliente.clienteEmail;
                //if (!string.IsNullOrEmpty(clienteEmail)) email = clienteEmail;


                foreach (var pacote in volumesPedido)
                {
                    if (volumeAtual == 0)
                    {
                        codigoTracking = pacote.rastreio;
                        //urlTracking = urlTracking + pacote.rastreio;
                    }
                    else
                    {
                        codigoTracking = codigoTracking + ", " + pacote.rastreio;
                        //urlTracking = urlTracking + "%2C+" + pacote.rastreio;
                    }
                    volumeAtual++;
                }

                string mensagem = "foi enviado em " + volumeAtual;
                if (volumeAtual == 1) mensagem += " remessa";
                else mensagem += " remessas";

                corpo = corpo.Replace("{{fraseenviado}}", mensagem);
                corpo = corpo.Replace("{{diasentrega}}", pedido.prazoDeEntrega);

                templateRastreio = templateRastreio.Replace("{{codigorastreio}}", codigoTracking);
                corpo = corpo.Replace("{{codigorastreio}}", templateRastreio);


                corpo = corpo.Replace("{{linkrastreio}}", urlTracking);

                string emailTrusted = "";
                if (pedido.endEstado.ToLower() == "sp" | pedido.endEstado.ToLower() == "rs" |
                    pedido.endEstado.ToLower() == "mg")
                {
                    emailTrusted = "system+58d17adb42a26@inbound.trustedcompany.com";

                }

                return rnEmails.EnviaEmail(rnConfiguracoes.emailPosVendas, email, "", emailTrusted, "", corpo, "Pedido Enviado");
                //return EnviaEmailPostMark(rnConfiguracoes.emailPosVendas, email, "", emailTrusted, "", corpo, "Pedido Enviado");

            }
            catch (System.Net.Mail.SmtpException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
            return true;
        }

        public static bool enviaCodigoDoTrakingCorreios(string clienteNome, string pedidoId, string clienteEmail)
        {
            try
            {
                int idPedido = Convert.ToInt32(pedidoId);
                var pedidosDc = new dbSiteEntities();
                var volumesPedido = (from c in pedidosDc.tbPedidoPacote where c.idPedido == idPedido select c);
                var pedido = (from c in pedidosDc.tbPedidos where c.pedidoId == idPedido select c).First();
                var pacotes = (from c in pedidosDc.tbPedidoPacote where c.idPedidoPacote == idPedido select c).FirstOrDefault();
                int volumeAtual = 0;
                string codigoTracking = "";
                string urlTracking = "http://www2.correios.com.br/sistemas/rastreamento";

                var prazoData = DateTime.Now;

                if (pacotes.dataDeCarregamento != null)
                {
                    prazoData = (DateTime)pacotes.dataDeCarregamento;
                }
                else if (pacotes.prazoFinalEntrega != null)
                {
                    prazoData = (DateTime)pacotes.prazoFinalEntrega;
                }

                string corpo = carregarTemplateArquivo(rnConfiguracoes.caminhoFisico + "\\templateemails\\enviado\\" + "enviado.html");
                string templateRastreio = carregarTemplateArquivo(rnConfiguracoes.caminhoFisico + "\\templateemails\\enviado\\" + "codigorastreio.htm");

                if (pedido.condDePagamentoId == 26)
                {
                    corpo = carregarTemplateArquivo(rnConfiguracoes.caminhoFisico + "\\templateemails\\enviado\\" + "enviado_ml.html");
                    templateRastreio = carregarTemplateArquivo(rnConfiguracoes.caminhoFisico + "\\templateemails\\enviado\\" + "codigorastreio_ml.htm");
                }
                var data = new dbSiteEntities();
                var cliente = (from c in data.tbClientes where c.clienteId == pedido.clienteId select c).FirstOrDefault();


                int prazoFinal = rnFuncoes.retornaPrazoDiasUteis(Convert.ToInt32(pedido.prazoDeEntrega), 0);
                //var dataEntrega = DateTime.Now.AddDays(prazoFinal).ToShortDateString();
                var dataEntrega = prazoData.AddDays(prazoFinal).ToShortDateString();
                corpo = corpo.Replace("{{prazo}}", dataEntrega);
                corpo = corpo.Replace("{{processamento}}", pedido.prazoMaximoPedidos.ToString());
                corpo = corpo.Replace("{{entrega}}", pedido.prazoDeEntrega.ToString());
                corpo = corpo.Replace("{{nomecliente}}", cliente.clienteNome);
                corpo = corpo.Replace("{{numeropedido}}", rnFuncoes.retornaIdCliente(Convert.ToInt32(pedidoId)).ToString());

                string urlBoleto = "";
                int tipoDePagamentoId = Convert.ToInt32(pedido.tipoDePagamentoId);
                int condDePagamentoId = Convert.ToInt32(pedido.condDePagamentoId);
                string imgCondicao = pedido.tbCondicoesDePagamento.imagemMini;
                string imgCondicao2 = "";
                string condicaoNome = pedido.tbCondicoesDePagamento.condicaoNome;
                string condicaoNome2 = "";

                string clienteCPFCNPJ = cliente.clienteCPFCNPJ;
                string clienteCidade = cliente.clienteCidade;
                string clienteRua = cliente.clienteRua;
                string clienteBairro = cliente.clienteBairro;
                string clienteCep = cliente.clienteCep;
                string clienteEstado = cliente.clienteEstado;


                if (tipoDePagamentoId == 8)
                    corpo = corpo.Replace("{{subtotal}}", Convert.ToDecimal(pedido.valorTotalGeral).ToString("C"));
                corpo = corpo.Replace("{{frete}}", Convert.ToDecimal(pedido.valorDoFrete).ToString("C"));
                corpo = corpo.Replace("{{desconto}}", Convert.ToDecimal(pedido.valorDoDescontoDoPagamento).ToString("C"));
                corpo = corpo.Replace("{{total}}", Convert.ToDecimal(pedido.valorCobrado).ToString("C"));
                corpo = corpo.Replace("{{enderecocliente}}", pedido.endRua + ", " + pedido.endNumero + " " + pedido.endComplemento + "<br>" + pedido.endBairro + "<br>Cep: " + pedido.endCep + " - " + pedido.endCidade + " - " + pedido.endEstado);



                string nome = cliente.clienteNome;
                string email = cliente.clienteEmail;
                if (!string.IsNullOrEmpty(clienteEmail)) email = clienteEmail;


                foreach (var pacote in volumesPedido)
                {
                    if (volumeAtual == 0)
                    {
                        codigoTracking = pacote.rastreio;
                        // urlTracking = urlTracking + pacote.rastreio;
                    }
                    else
                    {
                        codigoTracking = codigoTracking + ", " + pacote.rastreio;
                        // urlTracking = urlTracking + "%2C+" + pacote.rastreio;
                    }
                    volumeAtual++;
                }

                string mensagem = "foi enviado em " + volumeAtual;
                if (volumeAtual == 1) mensagem += " remessa";
                else mensagem += " remessas";

                corpo = corpo.Replace("{{fraseenviado}}", mensagem);
                corpo = corpo.Replace("{{diasentrega}}", pedido.prazoDeEntrega);

                templateRastreio = templateRastreio.Replace("{{codigorastreio}}", codigoTracking);
                corpo = corpo.Replace("{{codigorastreio}}", templateRastreio);


                corpo = corpo.Replace("{{linkrastreio}}", urlTracking);

                string emailTrusted = "";
                if (pedido.endEstado.ToLower() == "sp" | pedido.endEstado.ToLower() == "rs" |
                    pedido.endEstado.ToLower() == "mg")
                {
                    emailTrusted = "system+58d17adb42a26@inbound.trustedcompany.com";

                }


                return rnEmails.EnviaEmail(rnConfiguracoes.emailPosVendas, email, "", emailTrusted, "", corpo, "Pedido Enviado");
                // return EnviaEmailPostMark(rnConfiguracoes.emailPosVendas, email, "", emailTrusted, "", corpo, "Pedido Enviado");

            }
            catch (System.Net.Mail.SmtpException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
            return true;
        }

        public static bool enviaCodigoDoTrakingJadlog(int idPedido, string numeroTracking, string email)
        {
            try
            {

                var pedidoDc = new dbSiteEntities();
                var pedido = (from c in pedidoDc.tbPedidos where c.pedidoId == idPedido select c).First();
                //var pacote = (from c in pedidoDc.tbPedidoPacote where c.idPedidoPacote == idPedido select c).FirstOrDefault();
                var pacote = (from c in pedidoDc.tbPedidoPacote where c.idPedido == idPedido select c).FirstOrDefault();
                var clienteDc = new dbSiteEntities();
                var cliente = (from c in clienteDc.tbClientes where c.clienteId == pedido.clienteId select c).First();

                var prazoData = DateTime.Now;

                if (pacote.dataDeCarregamento != null)
                {
                    prazoData = (DateTime)pacote.dataDeCarregamento;
                }
                else if (pacote.prazoFinalEntrega != null)
                {
                    prazoData = (DateTime)pacote.prazoFinalEntrega;
                }

                string clienteNome = cliente.clienteNome;
                string clienteEmail = cliente.clienteEmail;

                if (!string.IsNullOrEmpty(email)) clienteEmail = email;

                string codigoTracking = numeroTracking;
                string urlTracking = "";
                if (!string.IsNullOrEmpty(numeroTracking))
                {
                    urlTracking = "http://jadlog.com/sitejadlog/tracking.jad?cte=" + numeroTracking;
                }
                else
                {
                    urlTracking = "http://jadlog.com/sitejadlog/tracking.jad?cte=" + pedido.numeroDoTracking4;
                    codigoTracking = pedido.numeroDoTracking4;
                }


                string corpo = carregarTemplateArquivo(rnConfiguracoes.caminhoFisico + "\\templateemails\\enviado\\" + "enviado.html");
                string templateRastreio = carregarTemplateArquivo(rnConfiguracoes.caminhoFisico + "\\templateemails\\enviado\\" + "codigorastreio.htm");
                if (pedido.condDePagamentoId == 26)
                {
                    corpo = carregarTemplateArquivo(rnConfiguracoes.caminhoFisico + "\\templateemails\\enviado\\" + "enviado_ml.html");
                    templateRastreio = carregarTemplateArquivo(rnConfiguracoes.caminhoFisico + "\\templateemails\\enviado\\" + "codigorastreio_ml.htm");
                }
                var data = new dbSiteEntities();


                int prazoFinal = rnFuncoes.retornaPrazoDiasUteis(Convert.ToInt32(pedido.prazoDeEntrega), 0);
                //var dataEntrega = DateTime.Now.AddDays(prazoFinal).ToShortDateString();
                var dataEntrega = prazoData.AddDays(prazoFinal).ToShortDateString();
                corpo = corpo.Replace("{{prazo}}", dataEntrega);
                corpo = corpo.Replace("{{processamento}}", pedido.prazoMaximoPedidos.ToString());
                corpo = corpo.Replace("{{entrega}}", pedido.prazoDeEntrega.ToString());
                corpo = corpo.Replace("{{nomecliente}}", cliente.clienteNome);
                corpo = corpo.Replace("{{numeropedido}}", rnFuncoes.retornaIdCliente(idPedido).ToString());

                int tipoDePagamentoId = Convert.ToInt32(pedido.tipoDePagamentoId);


                if (tipoDePagamentoId == 8)


                    corpo = corpo.Replace("{{subtotal}}", Convert.ToDecimal(pedido.valorTotalGeral).ToString("C"));
                corpo = corpo.Replace("{{frete}}", Convert.ToDecimal(pedido.valorDoFrete).ToString("C"));
                corpo = corpo.Replace("{{desconto}}", Convert.ToDecimal(pedido.valorDoDescontoDoPagamento).ToString("C"));
                corpo = corpo.Replace("{{total}}", Convert.ToDecimal(pedido.valorCobrado).ToString("C"));
                corpo = corpo.Replace("{{enderecocliente}}", pedido.endRua + ", " + pedido.endNumero + " " + pedido.endComplemento + "<br>" + pedido.endBairro + "<br>Cep: " + pedido.endCep + " - " + pedido.endCidade + " - " + pedido.endEstado);

                string mensagem = "foi coletado pela transportadora Jadlog";

                corpo = corpo.Replace("{{fraseenviado}}", mensagem);
                corpo = corpo.Replace("{{diasentrega}}", pedido.prazoDeEntrega);

                templateRastreio = templateRastreio.Replace("{{codigorastreio}}", codigoTracking);
                corpo = corpo.Replace("{{codigorastreio}}", templateRastreio);


                corpo = corpo.Replace("{{linkrastreio}}", urlTracking);



                string emailTrusted = "";

                //if ((prazoData - (DateTime)pedido.dataConfirmacaoPagamento).TotalDays <= 15)
                //    emailTrusted = "system+58d17adb42a26@inbound.trustedcompany.com";

                if (pedido.endEstado.ToLower() == "sp" | pedido.endEstado.ToLower() == "rs" |
                    pedido.endEstado.ToLower() == "mg")
                {
                    emailTrusted = "system+58d17adb42a26@inbound.trustedcompany.com";

                }


                return rnEmails.EnviaEmail(rnConfiguracoes.emailPosVendas, clienteEmail, "", emailTrusted, "", corpo, "Pedido Enviado");//renato@bark.com.br,andre@bark.com.br

            }
            catch (System.Net.Mail.SmtpException)
            {
                //rnEmails.EnviaEmail("", "andre@bark.com.br", "", "", "", "Erro ao enviar email carregamento PedidoID: " + idPedido.ToString() + " email: " + clienteEmail, "GDG - Erro ao enviar email carregamento");
                throw;
            }
            catch (Exception)
            {
                throw;
            }
            return true;
        }

        public static bool enviaCodigoDoTrakingTntAsync(int idPedidoEnvio, string email)
        {
            return enviaCodigoDoTrakingTnt(idPedidoEnvio, email);
        }

        public static bool enviaCodigoDoTrakingBelle(int idPedidoEnvio, string email)
        {
            try
            {
                StringBuilder conteudo = new StringBuilder();

                var pedidoDc = new dbSiteEntities();
                var envio = (from c in pedidoDc.tbPedidoEnvio where c.idPedidoEnvio == idPedidoEnvio select c).First();
                var pedido = (from c in pedidoDc.tbPedidoEnvio where c.idPedidoEnvio == idPedidoEnvio select c.tbPedidos).First();
                var pacote = (from c in pedidoDc.tbPedidoEnvio where c.idPedidoEnvio == idPedidoEnvio select c.tbPedidoPacote).FirstOrDefault();
                var clienteDc = new dbSiteEntities();
                var cliente = (from c in clienteDc.tbClientes where c.clienteId == pedido.clienteId select c).First();

                var prazoData = DateTime.Now;

                if (pacote.Select(x => x).FirstOrDefault().dataDeCarregamento != null)
                {
                    prazoData = (DateTime)pacote.Select(x => x).FirstOrDefault().dataDeCarregamento;
                }
                else if (pacote.Select(x => x).FirstOrDefault().prazoFinalEntrega != null)
                {
                    prazoData = (DateTime)pacote.Select(x => x).FirstOrDefault().prazoFinalEntrega;
                }

                string clienteNome = cliente.clienteNome;
                string clienteEmail = cliente.clienteEmail;

                if (!string.IsNullOrEmpty(email)) clienteEmail = email;

                string urlTracking = "http://rodoviario.sislognet.com.br/rastrdetail2.aspx?id=3440&amp;lu=graodegente&amp;lp=rastgdg16&amp;NF=" + envio.nfeNumero;

                string corpo = carregarTemplateArquivo(rnConfiguracoes.caminhoFisico + "\\templateemails\\enviado\\" + "enviado.html");
                if (pedido.condDePagamentoId == 26)
                {
                    corpo = carregarTemplateArquivo(rnConfiguracoes.caminhoFisico + "\\templateemails\\enviado\\" + "enviado_ml.html");
                }
                int prazoFinal = rnFuncoes.retornaPrazoDiasUteis(Convert.ToInt32(pedido.prazoDeEntrega), 0);
                //var dataEntrega = DateTime.Now.AddDays(prazoFinal).ToShortDateString();
                var dataEntrega = prazoData.AddDays(prazoFinal).ToShortDateString();
                corpo = corpo.Replace("{{prazo}}", dataEntrega);
                corpo = corpo.Replace("{{processamento}}", pedido.prazoMaximoPedidos.ToString());
                corpo = corpo.Replace("{{entrega}}", pedido.prazoDeEntrega.ToString());
                corpo = corpo.Replace("{{nomecliente}}", cliente.clienteNome);
                corpo = corpo.Replace("{{numeropedido}}", rnFuncoes.retornaIdCliente(pedido.pedidoId).ToString());

                corpo = corpo.Replace("{{subtotal}}", Convert.ToDecimal(pedido.valorTotalGeral).ToString("C"));
                corpo = corpo.Replace("{{frete}}", Convert.ToDecimal(pedido.valorDoFrete).ToString("C"));
                corpo = corpo.Replace("{{desconto}}", Convert.ToDecimal(pedido.valorDoDescontoDoPagamento).ToString("C"));
                corpo = corpo.Replace("{{total}}", Convert.ToDecimal(pedido.valorCobrado).ToString("C"));
                corpo = corpo.Replace("{{enderecocliente}}", pedido.endRua + ", " + pedido.endNumero + " " + pedido.endComplemento + "<br>" + pedido.endBairro + "<br>Cep: " + pedido.endCep + " - " + pedido.endCidade + " - " + pedido.endEstado);

                string mensagem = "foi coletado pela transportadora Belle Express";

                corpo = corpo.Replace("{{fraseenviado}}", mensagem);
                corpo = corpo.Replace("{{diasentrega}}", pedido.prazoDeEntrega);

                corpo = corpo.Replace("{{codigorastreio}}", "");


                corpo = corpo.Replace("{{linkrastreio}}", urlTracking);


                string emailTrusted = "";

                //if ((prazoData - (DateTime)pedido.dataConfirmacaoPagamento).TotalDays <= 15)
                //    emailTrusted = "system+58d17adb42a26@inbound.trustedcompany.com";

                if (pedido.endEstado.ToLower() == "sp" | pedido.endEstado.ToLower() == "rs" |
                    pedido.endEstado.ToLower() == "mg")
                {
                    emailTrusted = "system+58d17adb42a26@inbound.trustedcompany.com";

                }
                return rnEmails.EnviaEmail(rnConfiguracoes.emailPosVendas, clienteEmail, "", emailTrusted, "", corpo, "Pedido Enviado");//renato@bark.com.br,andre@bark.com.br

            }
            catch (System.Net.Mail.SmtpException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
            return true;
        }
        public static bool enviaCodigoDoTrakingTnt(int idPedidoEnvio, string email)
        {
            try
            {
                StringBuilder conteudo = new StringBuilder();

                var pedidoDc = new dbSiteEntities();
                var envio = (from c in pedidoDc.tbPedidoEnvio where c.idPedidoEnvio == idPedidoEnvio select c).First();
                var pedido = (from c in pedidoDc.tbPedidoEnvio where c.idPedidoEnvio == idPedidoEnvio select c.tbPedidos).First();
                var pacote = (from c in pedidoDc.tbPedidoEnvio where c.idPedidoEnvio == idPedidoEnvio select c.tbPedidoPacote).FirstOrDefault();
                var clienteDc = new dbSiteEntities();
                var cliente = (from c in clienteDc.tbClientes where c.clienteId == pedido.clienteId select c).First();

                var prazoData = DateTime.Now;

                if (pacote.Select(x => x).FirstOrDefault().dataDeCarregamento != null)
                {
                    prazoData = (DateTime)pacote.Select(x => x).FirstOrDefault().dataDeCarregamento;
                }
                else if (pacote.Select(x => x).FirstOrDefault().prazoFinalEntrega != null)
                {
                    prazoData = (DateTime)pacote.Select(x => x).FirstOrDefault().prazoFinalEntrega;
                }

                string clienteNome = cliente.clienteNome;
                string clienteEmail = cliente.clienteEmail;

                if (!string.IsNullOrEmpty(email)) clienteEmail = email;

                string urlTracking = "http://app.tntbrasil.com.br/LocalizacaoSimplificada/resultado_localizacao.asp?cnpj=" + cliente.clienteCPFCNPJ + "&amp;nota=" + envio.nfeNumero;

                string corpo = carregarTemplateArquivo(rnConfiguracoes.caminhoFisico + "\\templateemails\\enviado\\" + "enviado.html");
                if (pedido.condDePagamentoId == 26)
                {
                    corpo = carregarTemplateArquivo(rnConfiguracoes.caminhoFisico + "\\templateemails\\enviado\\" + "enviado_ml.html");
                }
                int prazoFinal = rnFuncoes.retornaPrazoDiasUteis(Convert.ToInt32(pedido.prazoDeEntrega), 0);
                //var dataEntrega = DateTime.Now.AddDays(prazoFinal).ToShortDateString();
                var dataEntrega = prazoData.AddDays(prazoFinal).ToShortDateString();
                corpo = corpo.Replace("{{prazo}}", dataEntrega);
                corpo = corpo.Replace("{{processamento}}", pedido.prazoMaximoPedidos.ToString());
                corpo = corpo.Replace("{{entrega}}", pedido.prazoDeEntrega.ToString());
                corpo = corpo.Replace("{{nomecliente}}", cliente.clienteNome);
                corpo = corpo.Replace("{{numeropedido}}", rnFuncoes.retornaIdCliente(pedido.pedidoId).ToString());

                corpo = corpo.Replace("{{subtotal}}", Convert.ToDecimal(pedido.valorTotalGeral).ToString("C"));
                corpo = corpo.Replace("{{frete}}", Convert.ToDecimal(pedido.valorDoFrete).ToString("C"));
                corpo = corpo.Replace("{{desconto}}", Convert.ToDecimal(pedido.valorDoDescontoDoPagamento).ToString("C"));
                corpo = corpo.Replace("{{total}}", Convert.ToDecimal(pedido.valorCobrado).ToString("C"));
                corpo = corpo.Replace("{{enderecocliente}}", pedido.endRua + ", " + pedido.endNumero + " " + pedido.endComplemento + "<br>" + pedido.endBairro + "<br>Cep: " + pedido.endCep + " - " + pedido.endCidade + " - " + pedido.endEstado);

                string mensagem = "foi coletado pela transportadora TNT";

                corpo = corpo.Replace("{{fraseenviado}}", mensagem);
                corpo = corpo.Replace("{{diasentrega}}", pedido.prazoDeEntrega);

                corpo = corpo.Replace("{{codigorastreio}}", "");


                corpo = corpo.Replace("{{linkrastreio}}", urlTracking);


                string emailTrusted = "";

                //if ((prazoData - (DateTime)pedido.dataConfirmacaoPagamento).TotalDays <= 15)
                //    emailTrusted = "system+58d17adb42a26@inbound.trustedcompany.com";

                if (pedido.endEstado.ToLower() == "sp" | pedido.endEstado.ToLower() == "rs" |
                    pedido.endEstado.ToLower() == "mg")
                {
                    emailTrusted = "system+58d17adb42a26@inbound.trustedcompany.com";

                }
                return rnEmails.EnviaEmail(rnConfiguracoes.emailPosVendas, clienteEmail, "", emailTrusted, "", corpo, "Pedido Enviado");//renato@bark.com.br,andre@bark.com.br

            }
            catch (System.Net.Mail.SmtpException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
            return true;
        }


        public static bool enviaConfirmacaoDePedido(int pedidoId)
        {
            try
            {

                string corpo = carregarTemplateArquivo(rnConfiguracoes.caminhoFisico + "\\templateemails\\pedidos\\" + "pedido.html");
                string templateProdutos = carregarTemplateArquivo(rnConfiguracoes.caminhoFisico + "\\templateemails\\pedidos\\" + "produtos.html");
                string templateparcelas = carregarTemplateArquivo(rnConfiguracoes.caminhoFisico + "\\templateemails\\pedidos\\" + "parcelas.html");
                string templateboleto = carregarTemplateArquivo(rnConfiguracoes.caminhoFisico + "\\templateemails\\pedidos\\" + "boleto.html");
                string templateboletoplano = carregarTemplateArquivo(rnConfiguracoes.caminhoFisico + "\\templateemails\\pedidos\\" + "boletoplano.html");
                string templateplanoparcelas = carregarTemplateArquivo(rnConfiguracoes.caminhoFisico + "\\templateemails\\pedidos\\" + "planoparcelas.html");
                string templateplanoparcela = carregarTemplateArquivo(rnConfiguracoes.caminhoFisico + "\\templateemails\\pedidos\\" + "planoparcela.html");

                var data = new dbSiteEntities();
                int idPedido = Convert.ToInt32(pedidoId);
                var pedido = (from c in data.tbPedidos where c.pedidoId == idPedido select c).First();
                var cliente = (from c in data.tbClientes where c.clienteId == pedido.clienteId select c).FirstOrDefault();
                var produtos = (from c in data.tbItensPedido where c.pedidoId == idPedido select c).ToList();


                string listaProdutos = "";
                foreach (var produto in produtos)
                {
                    if ((produto.cancelado ?? false) == false)
                    {
                        string linhaProduto = templateProdutos;
                        linhaProduto = linhaProduto.Replace("{{imagemproduto}}", rnConfiguracoes.caminhoCDN + "fotos/" + produto.produtoId + "/pequena_" + produto.tbProdutos.fotoDestaque + ".jpg");
                        linhaProduto = linhaProduto.Replace("{{produtonome}}", produto.tbProdutos.produtoNome);
                        linhaProduto = linhaProduto.Replace("{{quantidadeproduto}}", produto.itemQuantidade.ToString());
                        linhaProduto = linhaProduto.Replace("{{valorproduto}}", produto.itemValor.ToString("C"));
                        listaProdutos += linhaProduto;
                    }
                }
                corpo = corpo.Replace("{{numeropedido}}", rnFuncoes.retornaIdCliente(Convert.ToInt32(pedidoId)).ToString());
                corpo = corpo.Replace("{{nomecliente}}", cliente.clienteNome);
                corpo = corpo.Replace("{{produtos}}", listaProdutos);

                string urlBoleto = "";
                int tipoDePagamentoId = Convert.ToInt32(pedido.tipoDePagamentoId);
                int condDePagamentoId = Convert.ToInt32(pedido.condDePagamentoId);
                string imgCondicao = pedido.tbCondicoesDePagamento.imagemMini;
                string imgCondicao2 = "";
                string condicaoNome = pedido.tbCondicoesDePagamento.condicaoNome;
                string condicaoNome2 = "";

                string clienteCPFCNPJ = cliente.clienteCPFCNPJ;
                string clienteCidade = cliente.clienteCidade;
                string clienteRua = cliente.clienteRua;
                string clienteBairro = cliente.clienteBairro;
                string clienteCep = cliente.clienteCep;
                string clienteEstado = cliente.clienteEstado;


                if (tipoDePagamentoId == 8)
                {
                    string templateformas =
                        carregarTemplateArquivo(rnConfiguracoes.caminhoFisico +
                                                "\\templateemails\\pedidos\\" + "duasformas.html");
                    int condDePagamentoId1 = Convert.ToInt32(pedido.condDePagamentoId1);
                    int condDePagamentoId2 = Convert.ToInt32(pedido.condDePagamentoId2);
                    var condicao1 =
                        (from c in data.tbCondicoesDePagamento where c.condicaoId == condDePagamentoId1 select c).FirstOrDefault
                            ();
                    var condicao2 =
                        (from c in data.tbCondicoesDePagamento where c.condicaoId == condDePagamentoId2 select c).FirstOrDefault
                            ();


                    templateformas = templateformas.Replace("{{imagempagamento1}}", condicao1.imagemMini);
                    templateformas = templateformas.Replace("{{imagempagamento2}}", condicao2.imagemMini);
                    templateformas = templateformas.Replace("{{nomepagamento1}}", condicao1.condicaoNome);
                    templateformas = templateformas.Replace("{{nomepagamento2}}", condicao2.condicaoNome);
                    corpo = corpo.Replace("{{formapagamento}}", templateformas);

                    string parcelas2Formas = templateparcelas;
                    parcelas2Formas = parcelas2Formas.Replace("{{quantidadeparcelas}}", pedido.parcelasPagamento1.ToString());
                    parcelas2Formas = parcelas2Formas.Replace("{{valorparcelas}}", Convert.ToDecimal(pedido.valorParcelaPagamento1).ToString("C"));
                    parcelas2Formas += templateparcelas;
                    parcelas2Formas = parcelas2Formas.Replace("{{quantidadeparcelas}}", pedido.parcelasPagamento2.ToString());
                    parcelas2Formas = parcelas2Formas.Replace("{{valorparcelas}}",
                        Convert.ToDecimal(pedido.valorParcelaPagamento2).ToString("C"));
                    corpo = corpo.Replace("{{parcelas}}", parcelas2Formas);

                    var pagamentosBoleto =
                    (from c in data.tbPedidoPagamento
                     where c.pedidoId == idPedido && c.idOperadorPagamento == 1
                     select c).FirstOrDefault();

                    if (pagamentosBoleto != null)
                    {
                        urlBoleto = "https://apiv2.graodegente.com.br/v1/pagamentos/" + rnFuncoes.retornaIdCliente(idPedido) + "/boleto/" + pagamentosBoleto.numeroCobranca + "?autoImpressao=undefined";
                        corpo = corpo.Replace("{{linkboleto}}", templateboleto.Replace("{{linkboleto}}", urlBoleto));
                    }
                    else
                    {
                        corpo = corpo.Replace("{{linkboleto}}", "");
                    }
                }
                else
                {
                    string templateformas =
                        carregarTemplateArquivo(rnConfiguracoes.caminhoFisico +
                                                "\\templateemails\\pedidos\\" + "umaforma.html");

                    templateformas = templateformas.Replace("{{imagempagamento1}}", pedido.tbCondicoesDePagamento.imagemMini);
                    templateformas = templateformas.Replace("{{nomepagamento1}}", pedido.tbCondicoesDePagamento.condicaoNome);
                    corpo = corpo.Replace("{{formapagamento}}", templateformas);

                    string parcelasUmaForma = templateparcelas;
                    parcelasUmaForma = parcelasUmaForma.Replace("{{quantidadeparcelas}}", pedido.numeroDeParcelas.ToString());
                    parcelasUmaForma = parcelasUmaForma.Replace("{{valorparcelas}}", Convert.ToDecimal(pedido.valorDaParcela).ToString("C"));
                    corpo = corpo.Replace("{{parcelas}}", parcelasUmaForma);

                    var pagamentosBoleto =
                    (from c in data.tbPedidoPagamento
                     where c.pedidoId == idPedido && c.idOperadorPagamento == 1
                     select c).FirstOrDefault();

                    if (pagamentosBoleto != null && pedido.tipoDePagamentoId != 11)
                    {
                        urlBoleto = "https://apiv2.graodegente.com.br/v1/pagamentos/" + rnFuncoes.retornaIdCliente(idPedido) + "/boleto/" + pagamentosBoleto.numeroCobranca + "?autoImpressao=undefined";
                        corpo = corpo.Replace("{{linkboleto}}", templateboleto.Replace("{{linkboleto}}", urlBoleto));
                        corpo = corpo.Replace("{{styleplano}}", "display: none;");
                    }
                    else if (pagamentosBoleto != null && pedido.tipoDePagamentoId == 11)
                    {
                        urlBoleto = "https://apiv2.graodegente.com.br/v1/pagamentos/" + rnFuncoes.retornaIdCliente(idPedido) + "/boleto/" + pagamentosBoleto.numeroCobranca + "?autoImpressao=undefined";
                        corpo = corpo.Replace("{{linkboleto}}", templateboletoplano.Replace("{{linkboleto}}", urlBoleto));

                        string pagamentosplano = "";
                        var pagamentosPlanoBoleto =
                        (from c in data.tbPedidoPagamento
                         where c.pedidoId == idPedido && c.idOperadorPagamento == 1
                         select c).ToList();
                        foreach (var pagamentoPlano in pagamentosPlanoBoleto)
                        {
                            string urlBoletoPlano = "https://apiv2.graodegente.com.br/v1/pagamentos/" + rnFuncoes.retornaIdCliente(idPedido) + "/boleto/" + pagamentosBoleto.numeroCobranca + "?autoImpressao=undefined";
                            var linhaPagamento = templateplanoparcela;
                            linhaPagamento = linhaPagamento.Replace("{{linkboleto}}", urlBoletoPlano);
                            linhaPagamento = linhaPagamento.Replace("{{datavencimento}}", pagamentoPlano.dataVencimento.ToShortDateString());
                            pagamentosplano += linhaPagamento;
                        }
                        templateplanoparcelas = templateplanoparcelas.Replace("{{planoparcela}}", pagamentosplano);
                        corpo = corpo.Replace("{{planoparcelas}}", templateplanoparcelas);

                    }
                    else
                    {
                        corpo = corpo.Replace("{{linkboleto}}", "");
                        corpo = corpo.Replace("{{styleplano}}", "display: none;");
                    }

                }

                corpo = corpo.Replace("{{subtotal}}", Convert.ToDecimal(pedido.valorTotalGeral).ToString("C"));
                corpo = corpo.Replace("{{frete}}", Convert.ToDecimal(pedido.valorDoFrete).ToString("C"));
                corpo = corpo.Replace("{{desconto}}", Convert.ToDecimal(pedido.valorDoDescontoDoPagamento).ToString("C"));
                corpo = corpo.Replace("{{total}}", Convert.ToDecimal(pedido.valorCobrado).ToString("C"));
                corpo = corpo.Replace("{{enderecocliente}}", pedido.endRua + ", " + pedido.endNumero + " " + pedido.endComplemento + "<br>" + pedido.endBairro + "<br>Cep: " + pedido.endCep + " - " + pedido.endCidade + " - " + pedido.endEstado);

                return EnviaEmail("", cliente.clienteEmail, "", "automaticos2.graodegente@gmail.com", rnConfiguracoes.toAddress, corpo, rnConfiguracoes.nomeDoSite + " – Recebemos seu Pedido");
                //return EnviaEmailPostMark("", cliente.clienteEmail, "", "automaticos2.graodegente@gmail.com", rnConfiguracoes.toAddress, corpo, rnConfiguracoes.nomeDoSite + " – Recebemos seu Pedido");

            }
            catch (Exception)
            {
                return false;
            }
        }


        public static bool enviaConfirmacaoDadosPedido(int pedidoId)
        {
            try
            {

                //string corpo = carregarTemplateArquivo(rnConfiguracoes.caminhoFisico + "\\templateemails\\pedidos\\" + "pedido.html");

                var data = new dbSiteEntities();
                int idPedido = Convert.ToInt32(pedidoId);
                var pedido = (from c in data.tbPedidos where c.pedidoId == idPedido select c).First();
                var cliente = (from c in data.tbClientes where c.clienteId == pedido.clienteId select c).FirstOrDefault();


                string corpo = carregarTemplateArquivo(rnConfiguracoes.caminhoFisico + "\\templateemails\\pedidos\\" + "confirmacaodados.htm");

                corpo = corpo.Replace("{{linkconfirmacao}}", rnConfiguracoes.caminhoVirtual + "cadastroConfirmacaoDados.aspx?p=" + Encrypt(idPedido.ToString()));
                corpo = corpo.Replace("{{nomecliente}}", cliente.clienteNome);

                return EnviaEmail(rnConfiguracoes.emailPosVendas, cliente.clienteEmail, "", "automaticos2.graodegente@gmail.com", rnConfiguracoes.emailPosVendas, corpo, "Dados para Entrega do seu Pedido");
                //return EnviaEmailPostMark(rnConfiguracoes.emailPosVendas, cliente.clienteEmail, "", "automaticos2.graodegente@gmail.com", rnConfiguracoes.emailPosVendas, corpo, "Dados para Entrega do seu Pedido");

            }
            catch (Exception)
            {
                return false;
            }
        }


        public static string carregarTemplateArquivo(string pathArquivo)
        {
            try
            {
                if (!File.Exists(pathArquivo)) throw new Exception(String.Format("Arquivo '{0}' não encontrado!", pathArquivo));

                return File.ReadAllText(pathArquivo, Encoding.GetEncoding("ISO-8859-1"));
            }
            catch (Exception ex)
            {
                return "";
            }
        }


        public static string Encrypt(string plainText)
        {
            string EncryptionKey = "GraoDeGe";
            byte[] clearBytes = Encoding.Unicode.GetBytes(plainText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    plainText = Convert.ToBase64String(ms.ToArray());
                }
            }
            return plainText;
        }


        public static bool enviaCodigoDoTrakingBelleNovo(int idPedidoEnvio, string email)
        {
            try
            {
                StringBuilder conteudo = new StringBuilder();

                var data = new dbSiteEntities();
                var envio = (from c in data.tbPedidoEnvio where c.idPedidoEnvio == idPedidoEnvio select c).First();
                var pedido = (from c in data.tbPedidoEnvio where c.idPedidoEnvio == idPedidoEnvio select c.tbPedidos).First();
                var pacote = (from c in data.tbPedidoEnvio where c.idPedidoEnvio == idPedidoEnvio select c.tbPedidoPacote).FirstOrDefault();
                var clienteDc = new dbSiteEntities();
                var cliente = (from c in clienteDc.tbClientes where c.clienteId == pedido.clienteId select c).First();
                var produtos = (from c in data.tbProdutoEstoque
                                where c.idPedidoEnvio == idPedidoEnvio
                                select new
                                {
                                    c.tbProdutos.produtoNome,
                                    c.tbProdutos.fotoDestaque,
                                    c.tbProdutos.produtoId
                                });


                var prazoData = DateTime.Now;

                if (pacote.Select(x => x).FirstOrDefault().dataDeCarregamento != null)
                {
                    prazoData = (DateTime)pacote.Select(x => x).FirstOrDefault().dataDeCarregamento;
                }
                else if (pacote.Select(x => x).FirstOrDefault().prazoFinalEntrega != null)
                {
                    prazoData = (DateTime)pacote.Select(x => x).FirstOrDefault().prazoFinalEntrega;
                }

                string clienteNome = cliente.clienteNome;
                string clienteEmail = cliente.clienteEmail;

                if (!string.IsNullOrEmpty(email)) clienteEmail = email;

                string urlTracking = "http://rodoviario.sislognet.com.br/rastrdetail2.aspx?id=3440&amp;lu=graodegente&amp;lp=rastgdg16&amp;NF=" + envio.nfeNumero;

                string corpo = carregarTemplateArquivo(rnConfiguracoes.caminhoFisico + "\\templateemails\\enviado\\" + "enviado.html");
                if (pedido.condDePagamentoId == 26)
                {
                    corpo = carregarTemplateArquivo(rnConfiguracoes.caminhoFisico + "\\templateemails\\enviado\\" + "enviado_ml.html");
                }
                int prazoFinal = rnFuncoes.retornaPrazoDiasUteis(Convert.ToInt32(pedido.prazoDeEntrega), 0);
                //var dataEntrega = DateTime.Now.AddDays(prazoFinal).ToShortDateString();
                var dataEntrega = prazoData.AddDays(prazoFinal).ToShortDateString();
                corpo = corpo.Replace("{{prazo}}", dataEntrega);
                corpo = corpo.Replace("{{processamento}}", pedido.prazoMaximoPedidos.ToString());
                corpo = corpo.Replace("{{entrega}}", pedido.prazoDeEntrega.ToString());
                corpo = corpo.Replace("{{nomecliente}}", cliente.clienteNome);
                corpo = corpo.Replace("{{numeropedido}}", rnFuncoes.retornaIdCliente(pedido.pedidoId).ToString());

                corpo = corpo.Replace("{{subtotal}}", Convert.ToDecimal(pedido.valorTotalGeral).ToString("C"));
                corpo = corpo.Replace("{{frete}}", Convert.ToDecimal(pedido.valorDoFrete).ToString("C"));
                corpo = corpo.Replace("{{desconto}}", Convert.ToDecimal(pedido.valorDoDescontoDoPagamento).ToString("C"));
                corpo = corpo.Replace("{{total}}", Convert.ToDecimal(pedido.valorCobrado).ToString("C"));
                corpo = corpo.Replace("{{enderecocliente}}", pedido.endRua + ", " + pedido.endNumero + " " + pedido.endComplemento + "<br>" + pedido.endBairro + "<br>Cep: " + pedido.endCep + " - " + pedido.endCidade + " - " + pedido.endEstado);

                string mensagem = "foi coletado pela transportadora Belle Express";

                corpo = corpo.Replace("{{fraseenviado}}", mensagem);
                corpo = corpo.Replace("{{diasentrega}}", pedido.prazoDeEntrega);

                corpo = corpo.Replace("{{codigorastreio}}", "");


                corpo = corpo.Replace("{{linkrastreio}}", urlTracking);


                string emailTrusted = "";

                //if ((prazoData - (DateTime)pedido.dataConfirmacaoPagamento).TotalDays <= 15)
                //    emailTrusted = "system+58d17adb42a26@inbound.trustedcompany.com";

                if (pedido.endEstado.ToLower() == "sp" | pedido.endEstado.ToLower() == "rs" |
                    pedido.endEstado.ToLower() == "mg")
                {
                    emailTrusted = "system+58d17adb42a26@inbound.trustedcompany.com";

                }
                return rnEmails.EnviaEmail(rnConfiguracoes.emailPosVendas, clienteEmail, "", emailTrusted, "", corpo, "Pedido Enviado");//renato@bark.com.br,andre@bark.com.br
                //return EnviaEmailPostMark(rnConfiguracoes.emailPosVendas, clienteEmail, "", emailTrusted, "", corpo, "Pedido Enviado");

            }
            catch (System.Net.Mail.SmtpException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
            return true;
        }

        public static bool enviaCodigoDoTrakingNovo(int idPedidoEnvio, string email)
        {
            try
            {
                StringBuilder conteudo = new StringBuilder();

                var data = new dbSiteEntities();
                var envio = (from c in data.tbPedidoEnvio where c.idPedidoEnvio == idPedidoEnvio select c).First();
                var pedido = (from c in data.tbPedidoEnvio where c.idPedidoEnvio == idPedidoEnvio select c.tbPedidos).First();
                var pacote = (from c in data.tbPedidoEnvio where c.idPedidoEnvio == idPedidoEnvio select c.tbPedidoPacote).FirstOrDefault();
                var cliente = (from c in data.tbClientes where c.clienteId == pedido.clienteId select c).First();
                var produtos = (from c in data.tbProdutoEstoque
                                where c.idPedidoEnvio == idPedidoEnvio
                                select new
                                {
                                    c.tbProdutos.produtoNome,
                                    c.tbProdutos.fotoDestaque,
                                    c.tbProdutos.produtoId
                                });


                var prazoData = DateTime.Now;

                if (pacote.Select(x => x).FirstOrDefault().dataDeCarregamento != null)
                {
                    prazoData = (DateTime)pacote.Select(x => x).FirstOrDefault().dataDeCarregamento;
                }
                else if (pacote.Select(x => x).FirstOrDefault().prazoFinalEntrega != null)
                {
                    prazoData = (DateTime)pacote.Select(x => x).FirstOrDefault().prazoFinalEntrega;
                }

                string clienteNome = cliente.clienteNome;
                string clienteEmail = cliente.clienteEmail;

                if (!string.IsNullOrEmpty(email)) clienteEmail = email;


                string corpo = carregarTemplateArquivo(rnConfiguracoes.caminhoFisico + "\\templateemails\\enviadonovo\\" + "enviado.html");
                string corpoProdutos = carregarTemplateArquivo(rnConfiguracoes.caminhoFisico + "\\templateemails\\enviadonovo\\" + "produto.html");
                if (pedido.condDePagamentoId == 26)
                {
                    corpo = carregarTemplateArquivo(rnConfiguracoes.caminhoFisico + "\\templateemails\\enviado\\" + "enviado_ml.html");
                }

                string htmlProdutos = "";
                foreach (var produto in produtos)
                {
                    if (!string.IsNullOrEmpty(produto.fotoDestaque)) htmlProdutos += corpoProdutos.Replace("{{nomeproduto}}", produto.produtoNome).Replace("{{imagemproduto}}", rnConfiguracoes.caminhoCDN + "fotos/" + produto.produtoId + "/" + "pequena_" + produto.fotoDestaque + ".jpg");
                    else htmlProdutos += corpoProdutos.Replace("{{nomeproduto}}", produto.produtoNome).Replace("{{imagemproduto}}", "http://emkt.graodegente.com.br/nova-newsletter2/imagem/noImage.jpg");
                }
                corpo = corpo.Replace("{{produtos}}", htmlProdutos);

                int prazoFinal = rnFuncoes.retornaPrazoDiasUteis(Convert.ToInt32(pedido.prazoDeEntrega), 0);
                //var dataEntrega = DateTime.Now.AddDays(prazoFinal).ToShortDateString();
                var dataEntrega = prazoData.AddDays(prazoFinal).ToShortDateString();
                corpo = corpo.Replace("{{prazo}}", dataEntrega);
                corpo = corpo.Replace("{{processamento}}", pedido.prazoMaximoPedidos.ToString());
                corpo = corpo.Replace("{{entrega}}", pedido.prazoDeEntrega.ToString());
                corpo = corpo.Replace("{{nomecliente}}", cliente.clienteNome);
                corpo = corpo.Replace("{{numeropedido}}", rnFuncoes.retornaIdCliente(pedido.pedidoId).ToString());

                corpo = corpo.Replace("{{subtotal}}", Convert.ToDecimal(pedido.valorTotalGeral).ToString("C"));
                corpo = corpo.Replace("{{frete}}", Convert.ToDecimal(pedido.valorDoFrete).ToString("C"));
                corpo = corpo.Replace("{{desconto}}", Convert.ToDecimal(pedido.valorDoDescontoDoPagamento).ToString("C"));
                corpo = corpo.Replace("{{total}}", Convert.ToDecimal(pedido.valorCobrado).ToString("C"));
                corpo = corpo.Replace("{{enderecocliente}}", pedido.endRua + ", " + pedido.endNumero + " " + pedido.endComplemento + "<br>" + pedido.endBairro + "<br>Cep: " + pedido.endCep + " - " + pedido.endCidade + " - " + pedido.endEstado);

                string fraseenviado = "Seu pedido foi coletado pela transportadora";
                string tituloenviado = "Pedido Enviado";
                if (pedido.statusDoPedido == 11)
                {
                    fraseenviado = "Adiantamos a entrega do(s) produto(s) listados abaixo";
                    tituloenviado = "Pedido Parcialmente Enviado";

                }
                corpo = corpo.Replace("{{fraseenviado}}", fraseenviado);
                corpo = corpo.Replace("{{tituloenviado}}", tituloenviado);
                corpo = corpo.Replace("{{diasentrega}}", pedido.prazoDeEntrega);
                corpo = corpo.Replace("{{quantidadevolumes}}", (envio.volumesEmbalados ?? 1).ToString());


                string urlTracking = "http://app.tntbrasil.com.br/LocalizacaoSimplificada/resultado_localizacao.asp?cnpj=" + cliente.clienteCPFCNPJ + "&amp;nota=" + envio.nfeNumero;

                if (envio.formaDeEnvio == "tnt")
                {
                    urlTracking = "http://app.tntbrasil.com.br/LocalizacaoSimplificada/resultado_localizacao.asp?cnpj=" + cliente.clienteCPFCNPJ + "&amp;nota=" + envio.nfeNumero;
                }
                else if (envio.formaDeEnvio == "jadlog")
                {
                    urlTracking = "http://jadlog.com/sitejadlog/tracking.jad?cte=" + pacote.First().rastreio;
                }
                else if (envio.formaDeEnvio == "pac" | envio.formaDeEnvio == "sedex" | envio.formaDeEnvio == "correios")
                {
                    urlTracking = "http://www2.correios.com.br/sistemas/rastreamento/resultado.cfm";
                }
                else if (envio.formaDeEnvio == "plimor")
                {
                    urlTracking = "http://www.plimor.com.br/Consultas/PosicaoCarga.aspx?remetente=10924051000163&amp;destinatario=" + cliente.clienteCPFCNPJ + "&amp;notafiscal=" + envio.nfeNumero + "&amp;serie=1";
                }
                else if (envio.formaDeEnvio == "nowlog")
                {
                    urlTracking = "https://ssw.inf.br/cgi-local/tracking/26384531000119/" + envio.nfeNumero;
                }
                else if (envio.formaDeEnvio == "lbr")
                {
                    urlTracking = "http://rodoviario.sislognet.com.br/rastrdetail2.aspx?id=1042&lu=lgf&lp=rstlgf18&NF=" + envio.nfeNumero;
                }
                else if (envio.formaDeEnvio == "dialogo")
                {
                    urlTracking = "https://ssw.inf.br/cgi-local/tracking/26384531000119/" + envio.nfeNumero;
                }
                else if (envio.formaDeEnvio == "totalexpress")
                {
                    urlTracking = "http://tracking.totalexpress.com.br/poupup_track.php?reid=5982&pedido=" + rnFuncoes.retornaIdCliente(pedido.pedidoId).ToString() + "&nfiscal=" + envio.nfeNumero;
                }



                if (envio.formaDeEnvio == "tnt" | envio.formaDeEnvio == "belle" | envio.formaDeEnvio == "plimor")
                {
                    corpo = corpo.Replace("{{codigorastreio}}", envio.nfeNumero == null ? "" : envio.nfeNumero.ToString());
                }
                else if (envio.formaDeEnvio == "jadlog" | envio.formaDeEnvio == "pac" | envio.formaDeEnvio == "sedex")
                {
                    corpo = corpo.Replace("{{codigorastreio}}", pacote.First().rastreio);
                }
                else
                {
                    corpo = corpo.Replace("{{codigorastreio}}", "");
                }


                corpo = corpo.Replace("{{linkrastreio}}", urlTracking);


                string emailTrusted = "";

                //if ((prazoData - (DateTime)pedido.dataConfirmacaoPagamento).TotalDays <= 15)
                //    emailTrusted = "system+58d17adb42a26@inbound.trustedcompany.com";

                if (pedido.endEstado.ToLower() == "sp" | pedido.endEstado.ToLower() == "rs" |
                    pedido.endEstado.ToLower() == "mg")
                {
                    emailTrusted = "system+58d17adb42a26@inbound.trustedcompany.com";

                }
                return rnEmails.EnviaEmail(rnConfiguracoes.emailPosVendas, clienteEmail, "", emailTrusted, "", corpo, "Pedido Enviado");//renato@bark.com.br,andre@bark.com.br
                //return rnEmails.EnviaEmail(rnConfiguracoes.emailPosVendas, "andre@graodegente.com.br", "", "", "", corpo, "Pedido Enviado");//renato@bark.com.br,andre@bark.com.br
                //return EnviaEmailPostMark(rnConfiguracoes.emailPosVendas, clienteEmail, "", emailTrusted, "", corpo, "Pedido Enviado");

            }
            catch (System.Net.Mail.SmtpException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
            return true;
        }


        public static bool EnviaEmailAndamentoPedido(int pedidoId)
        {
            try
            {
                StringBuilder conteudo = new StringBuilder();

                var data = new dbSiteEntities();
                var pedido = (from c in data.tbPedidos where c.pedidoId == pedidoId select c).First();
                var cliente = (from c in data.tbClientes where c.clienteId == pedido.clienteId select c).First();
                string clienteNome = cliente.clienteNome;
                string clienteEmail = cliente.clienteEmail;


                string corpo = carregarTemplateArquivo(rnConfiguracoes.caminhoFisico + "\\templateemails\\" + "andamentodopedido.html");
                //string corpo = carregarTemplateArquivo("C:\\repositorios\\backofficegrao\\bcServerService\\bcServerService\\templateemails\\" + "andamentodopedido.html");

                corpo = corpo.Replace("{{pedido}}", rnFuncoes.retornaIdCliente(pedidoId).ToString());
                corpo = corpo.Replace("{{nomecliente}}", clienteNome);

                var agora = DateTime.Now;
                var emailEnviado = new tbEmailEnvio();
                emailEnviado.dataEnvio = agora;
                emailEnviado.pedidoId = pedidoId;
                emailEnviado.idEmailTipo = 1;
                data.tbEmailEnvio.Add(emailEnviado);
                data.SaveChanges();
                corpo = AppendTracker(corpo, emailEnviado.idEmailEnvio);
                return rnEmails.EnviaEmail(rnConfiguracoes.emailPosVendas, clienteEmail, "", "", "", corpo, "Grão de Gente - Informações sobre seu pedido");

            }
            catch (System.Net.Mail.SmtpException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
            return true;
        }


        public static bool EnviaEmailPedidoAtrasadoGrao(int pedidoId)
        {
            try
            {
                StringBuilder conteudo = new StringBuilder();

                var data = new dbSiteEntities();
                var pedido = (from c in data.tbPedidos where c.pedidoId == pedidoId select c).First();
                var cliente = (from c in data.tbClientes where c.clienteId == pedido.clienteId select c).First();
                string clienteNome = cliente.clienteNome;
                string clienteEmail = cliente.clienteEmail;


                string corpo = carregarTemplateArquivo(rnConfiguracoes.caminhoFisico + "\\templateemails\\" + "pedidoatrasadograo.html");
                //string corpo = carregarTemplateArquivo("C:\\repositorios\\backofficegrao\\bcServerService\\bcServerService\\templateemails\\" + "pedidoatrasadograo.html");

                corpo = corpo.Replace("{{pedido}}", rnFuncoes.retornaIdCliente(pedidoId).ToString());
                corpo = corpo.Replace("{{nomecliente}}", clienteNome);

                var agora = DateTime.Now;
                var emailEnviado = new tbEmailEnvio();
                emailEnviado.dataEnvio = agora;
                emailEnviado.pedidoId = pedidoId;
                emailEnviado.idEmailTipo = 2;
                data.tbEmailEnvio.Add(emailEnviado);
                data.SaveChanges();
                corpo = AppendTracker(corpo, emailEnviado.idEmailEnvio);
                return rnEmails.EnviaEmail(rnConfiguracoes.emailPosVendas, clienteEmail, "", "", "", corpo, "Informações sobre seu pedido - Grão de Gente");

            }
            catch (System.Net.Mail.SmtpException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
            return true;
        }


        private static string AppendTracker(string html, int idEmailEnvio)
        {
            return html += "<img height=\"1\" src=\"http://admin.graodegente.com.br/tracker/default.aspx?idev=" + idEmailEnvio + "\" width=\"1\">";
        }
    }


}
