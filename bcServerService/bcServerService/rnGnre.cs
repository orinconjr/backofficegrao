﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Xml;

namespace bcServerService
{
    public class rnGnre
    {
        public static void emiteGnres()
        {
            try
            {
                var data = new dbSiteEntities();
                var pedidosNota =
                    (from c in data.tbNotaFiscal
                     where c.statusNfe == 2 && (c.statusGnre == null | c.statusGnre == 0) && c.idCNPJ == 6
                     select c);
                int limite = 0;
                foreach (var pedidoNota in pedidosNota)
                {
                    var empresaNota = pedidoNota.tbEmpresa;
                    var pedidoDetalhe = (from c in data.tbPedidos where c.pedidoId == pedidoNota.idPedido select c).FirstOrDefault();
                    if (pedidoDetalhe != null)
                    {
                        /*if (pedidoDetalhe.endEstado.ToLower() == "al" | pedidoDetalhe.endEstado.ToLower() == "ce" | 
                            pedidoDetalhe.endEstado.ToLower() == "pi" | pedidoDetalhe.endEstado.ToLower() == "rj" | pedidoDetalhe.endEstado.ToLower() == "ro")
                        {
                            var itensPedido = (from c in data.tbProdutoEstoque where c.idPedidoEnvio == pedidoNota.idPedidoEnvio select c).ToList();
                            decimal totalDifal = 0;
                            foreach(var itemPedido in itensPedido)
                            {
                                var valorNota = (from c in data.tbNotaFiscalItem where c.idNotaFiscal == pedidoNota.idNotaFiscal && c.produtoId == itemPedido.produtoId select c).FirstOrDefault();
                                if (valorNota != null)
                                {
                                    var diferencial = CalculaDiferencialAliquota("mg", pedidoDetalhe.endEstado, valorNota.vUnTrib ?? 0, DateTime.Now);
                                    valorNota.pICMSUFDest = diferencial.icmsDestino;
                                    valorNota.vICMSUFDest = diferencial.diferencialDestino;
                                    valorNota.vICMSUFRemet = diferencial.diferencialOrigem;
                                    totalDifal += diferencial.diferencialDestino;
                                    data.SaveChanges();
                                }
                            }

                            pedidoNota.valorPartilha = totalDifal;
                            data.SaveChanges();
                        }*/

                        if (pedidoDetalhe.endEstado != "mg")
                        {
                            bool gerarGnre = true;
                            if (!string.IsNullOrEmpty(empresaNota.ieES) && pedidoDetalhe.endEstado.Trim().ToLower() == "es")
                            {
                                gerarGnre = false;
                            }
                            else if (pedidoDetalhe.endEstado.Trim().ToLower() == "sp")
                            {
                                gerarGnre = false;
                            }
                            else if (!string.IsNullOrEmpty(empresaNota.ieMA) && pedidoDetalhe.endEstado.Trim().ToLower() == "ma")
                            {

                                gerarGnre = false;
                            }
                            else if (!string.IsNullOrEmpty(empresaNota.iePR) && pedidoDetalhe.endEstado.Trim().ToLower() == "pr")
                            {
                                gerarGnre = false;
                            }
                            else if (!string.IsNullOrEmpty(empresaNota.ieDF) && pedidoDetalhe.endEstado.Trim().ToLower() == "df")
                            {
                                gerarGnre = false;
                            }
                            else if (!string.IsNullOrEmpty(empresaNota.ieSE) && pedidoDetalhe.endEstado.Trim().ToLower() == "se")
                            {
                                gerarGnre = false;
                            }
                            else if (!string.IsNullOrEmpty(empresaNota.ieTO) && pedidoDetalhe.endEstado.Trim().ToLower() == "to")
                            {
                                gerarGnre = false;
                            }
                            else if (!string.IsNullOrEmpty(empresaNota.ieRJ) && pedidoDetalhe.endEstado.Trim().ToLower() == "rj")
                            {
                                gerarGnre = false;
                            }
                            else if (!string.IsNullOrEmpty(empresaNota.iePB) && pedidoDetalhe.endEstado.Trim().ToLower() == "pb")
                            {
                                gerarGnre = false;
                            }
                            else if (!string.IsNullOrEmpty(empresaNota.ieMT) && pedidoDetalhe.endEstado.Trim().ToLower() == "mt")
                            {
                                gerarGnre = false;
                            }
                            else if (!string.IsNullOrEmpty(empresaNota.ieRS) && pedidoDetalhe.endEstado.Trim().ToLower() == "rs")
                            {
                                gerarGnre = false;
                            }
                            else if (!string.IsNullOrEmpty(empresaNota.ieAP) && pedidoDetalhe.endEstado.Trim().ToLower() == "ap")
                            {
                                gerarGnre = false;
                            }
                            else if (!string.IsNullOrEmpty(empresaNota.ieSC) && pedidoDetalhe.endEstado.Trim().ToLower() == "sc")
                            {
                                gerarGnre = false;
                            }
                            else if (!string.IsNullOrEmpty(empresaNota.ieCE) && pedidoDetalhe.endEstado.Trim().ToLower() == "ce")
                            {
                                gerarGnre = false;
                            }
                            else if (!string.IsNullOrEmpty(empresaNota.ieBA) && pedidoDetalhe.endEstado.Trim().ToLower() == "ba")
                            {
                                gerarGnre = false;
                            }
                            else if (pedidoDetalhe.endEstado.Trim().ToLower() == "mg")
                            {
                                gerarGnre = false;
                            }
                            else if (!string.IsNullOrEmpty(empresaNota.ieMS) && pedidoDetalhe.endEstado.Trim().ToLower() == "ms")
                            {
                                gerarGnre = false;
                            }
                            else if (!string.IsNullOrEmpty(empresaNota.iePE) && pedidoDetalhe.endEstado.Trim().ToLower() == "pe")
                            {
                                gerarGnre = false;
                            }
                            else if (!string.IsNullOrEmpty(empresaNota.ieAM) && pedidoDetalhe.endEstado.Trim().ToLower() == "am")
                            {
                                gerarGnre = false;
                            }
                            else if (!string.IsNullOrEmpty(empresaNota.ieRN) && pedidoDetalhe.endEstado.Trim().ToLower() == "rn")
                            {
                                gerarGnre = false;
                            }
                            else if (!string.IsNullOrEmpty(empresaNota.ieAC) && pedidoDetalhe.endEstado.Trim().ToLower() == "ac")
                            {
                                gerarGnre = false;
                            }
                            else if (!string.IsNullOrEmpty(empresaNota.iePA) && pedidoDetalhe.endEstado.Trim().ToLower() == "pa")
                            {
                                gerarGnre = false;
                            }
                            else if (!string.IsNullOrEmpty(empresaNota.ieGO) && pedidoDetalhe.endEstado.Trim().ToLower() == "go")
                            {
                                gerarGnre = false;
                            }
                            else if (!string.IsNullOrEmpty(empresaNota.iePI) && pedidoDetalhe.endEstado.Trim().ToLower() == "pi")
                            {
                                gerarGnre = false;
                            }
                            else if (!string.IsNullOrEmpty(empresaNota.ieRO) && pedidoDetalhe.endEstado.Trim().ToLower() == "ro")
                            {
                                gerarGnre = false;
                            }
                            else if (!string.IsNullOrEmpty(empresaNota.ieAL) && pedidoDetalhe.endEstado.Trim().ToLower() == "al")
                            {
                                gerarGnre = false;
                            }
                            else if (!string.IsNullOrEmpty(empresaNota.ieRR) && pedidoDetalhe.endEstado.Trim().ToLower() == "rr")
                            {
                                gerarGnre = false;
                            }
                            if (gerarGnre && pedidoNota.valorPartilha > 0)
                            {
                                limite++;
                                var cliente =
                                    (from c in data.tbClientes where c.clienteId == pedidoDetalhe.clienteId select c).First();
                                var dados = new DadosEmissaoGnre();
                                dados.DadosGnre.c01_UfFavorecida = pedidoDetalhe.endEstado.ToUpper();
                                dados.DadosGnre.c02_receita = "100102";
                                dados.DadosGnre.c27_tipoIdentificacaoEmitente = "1";
                                dados.DadosGnre.c03_idContribuinteEmitente = new DadosEmissaoGnre.c03_idContribuinteEmitente { CNPJ = "26384531000119" };
                                //if (pedidoDetalhe.endEstado.ToLower() == "mg" | pedidoDetalhe.endEstado.ToLower() == "ce" | pedidoDetalhe.endEstado.ToLower() == "pi" | pedidoDetalhe.endEstado.ToLower() == "to" | pedidoDetalhe.endEstado.ToLower() == "rr")
                                //{
                                    dados.DadosGnre.c28_tipoDocOrigem = "10"; //MG e CE tipo
                                    dados.DadosGnre.c04_docOrigem = pedidoNota.numeroNota.ToString(); //MG e CE nota 
                                //}
                                dados.DadosGnre.c06_valorPrincipal = ((decimal)pedidoNota.valorPartilha).ToString("0.00").Replace(",", ".");

                                var vencimento = pedidoNota.dataHora;
                                var vencimentoString = vencimento.ToString("yyyy-MM-dd").Replace("_", "-");
                                dados.DadosGnre.c14_dataVencimento = vencimentoString;

                                #region dadosEmitente
                                dados.DadosGnre.c16_razaoSocialEmitente = "LGF COMERCIO ELETRONICO LTDA.";
                                dados.DadosGnre.c18_enderecoEmitente = "R CORONEL HERCULANO COBRA";
                                dados.DadosGnre.c19_municipioEmitente = "52501";
                                dados.DadosGnre.c20_ufEnderecoEmitente = "MG";
                                dados.DadosGnre.c21_cepEmitente = "37550000";
                                dados.DadosGnre.c22_telefoneEmitente = "1135228379";
                                #endregion

                                dados.DadosGnre.c34_tipoIdentificacaoDestinatario = "2";
                                dados.DadosGnre.c35_idContribuinteDestinatario = new DadosEmissaoGnre.c35_idContribuinteDestinatario();
                                dados.DadosGnre.c35_idContribuinteDestinatario.CPF = cliente.clienteCPFCNPJ.Replace(".", "").Replace("-", "").Replace("/", "").Replace(" ", "");
                                dados.DadosGnre.c37_razaoSocialDestinatario = cliente.clienteNome.Trim();
                                dados.DadosGnre.c38_municipioDestinatario = pedidoNota.municioDestinatario.Substring(2, 5);

                                var dataPagamento = DateTime.Now;
                                var dataPagamentoString = dataPagamento.ToString("yyyy-MM-dd").Replace("_", "-");
                                dados.DadosGnre.c33_dataPagamento = dataPagamentoString;


                                dados.DadosGnre.c39_camposExtras = new List<DadosEmissaoGnre.campoExtra>();
                                dados.DadosGnre.c05_referencia = new DadosEmissaoGnre.c05_referencia()
                                {
                                    ano = DateTime.Now.Year.ToString(),
                                    mes = String.Format("{0:00}", DateTime.Now.Month)
                                };
                                if (pedidoDetalhe.endEstado.ToLower() == "pb")
                                {
                                    
                                    dados.DadosGnre.c39_camposExtras.Add(new DadosEmissaoGnre.campoExtra() { codigo = "99", tipo = "T", valor = pedidoNota.nfeKey + rnNotaFiscal.GerarDigitoVerificadorNFe(pedidoNota.nfeKey) });
                                }
                                else if (pedidoDetalhe.endEstado.ToLower() == "ac")
                                {
                                    dados.DadosGnre.c39_camposExtras.Add(new DadosEmissaoGnre.campoExtra() { codigo = "76", tipo = "T", valor = pedidoNota.nfeKey + rnNotaFiscal.GerarDigitoVerificadorNFe(pedidoNota.nfeKey) });
                                }
                                else if (pedidoDetalhe.endEstado.ToLower() == "al")
                                {
                                    dados.DadosGnre.c39_camposExtras.Add(new DadosEmissaoGnre.campoExtra() { codigo = "65", tipo = "T", valor = pedidoNota.nfeKey + rnNotaFiscal.GerarDigitoVerificadorNFe(pedidoNota.nfeKey) });
                                }
                                else if (pedidoDetalhe.endEstado.ToLower() == "am")
                                {
                                    dados.DadosGnre.c39_camposExtras.Add(new DadosEmissaoGnre.campoExtra() { codigo = "12", tipo = "T", valor = pedidoNota.nfeKey + rnNotaFiscal.GerarDigitoVerificadorNFe(pedidoNota.nfeKey) });
                                }
                                else if (pedidoDetalhe.endEstado.ToLower() == "ap")
                                {
                                    dados.DadosGnre.c05_referencia = new DadosEmissaoGnre.c05_referencia()
                                    {
                                        ano = DateTime.Now.Year.ToString(),
                                        mes = String.Format("{0:00}", DateTime.Now.Month)
                                    };
                                    dados.DadosGnre.c39_camposExtras.Add(new DadosEmissaoGnre.campoExtra() { codigo = "47", tipo = "T", valor = pedidoNota.nfeKey + rnNotaFiscal.GerarDigitoVerificadorNFe(pedidoNota.nfeKey) });
                                }
                                else if (pedidoDetalhe.endEstado.ToLower() == "ba")
                                {
                                    dados.DadosGnre.c05_referencia = new DadosEmissaoGnre.c05_referencia()
                                    {
                                        ano = DateTime.Now.Year.ToString(),
                                        mes = String.Format("{0:00}", DateTime.Now.Month),
                                        periodo = "0"
                                    };
                                    dados.DadosGnre.c39_camposExtras.Add(new DadosEmissaoGnre.campoExtra() { codigo = "86", tipo = "T", valor = pedidoNota.nfeKey + rnNotaFiscal.GerarDigitoVerificadorNFe(pedidoNota.nfeKey) });
                                }
                                else if (pedidoDetalhe.endEstado.ToLower() == "go")
                                {
                                    dados.DadosGnre.c39_camposExtras.Add(new DadosEmissaoGnre.campoExtra() { codigo = "102", tipo = "T", valor = pedidoNota.nfeKey + rnNotaFiscal.GerarDigitoVerificadorNFe(pedidoNota.nfeKey) });
                                }
                                else if (pedidoDetalhe.endEstado.ToLower() == "ma")
                                {
                                    dados.DadosGnre.c39_camposExtras.Add(new DadosEmissaoGnre.campoExtra() { codigo = "94", tipo = "T", valor = pedidoNota.nfeKey + rnNotaFiscal.GerarDigitoVerificadorNFe(pedidoNota.nfeKey) });
                                }
                                else if (pedidoDetalhe.endEstado.ToLower() == "ms")
                                {
                                    dados.DadosGnre.c39_camposExtras.Add(new DadosEmissaoGnre.campoExtra() { codigo = "88", tipo = "T", valor = pedidoNota.nfeKey + rnNotaFiscal.GerarDigitoVerificadorNFe(pedidoNota.nfeKey) });
                                }
                                else if (pedidoDetalhe.endEstado.ToLower() == "mt")
                                {
                                    dados.DadosGnre.c25_detalhamentoReceita = "000055";
                                    dados.DadosGnre.c39_camposExtras.Add(new DadosEmissaoGnre.campoExtra() { codigo = "17", tipo = "T", valor = pedidoNota.nfeKey + rnNotaFiscal.GerarDigitoVerificadorNFe(pedidoNota.nfeKey) });
                                }
                                else if (pedidoDetalhe.endEstado.ToLower() == "pe")
                                {
                                    dados.DadosGnre.c39_camposExtras.Add(new DadosEmissaoGnre.campoExtra() { codigo = "92", tipo = "T", valor = pedidoNota.nfeKey + rnNotaFiscal.GerarDigitoVerificadorNFe(pedidoNota.nfeKey) });
                                }
                                else if (pedidoDetalhe.endEstado.ToLower() == "pr")
                                {
                                    dados.DadosGnre.c39_camposExtras.Add(new DadosEmissaoGnre.campoExtra() { codigo = "92", tipo = "T", valor = pedidoNota.nfeKey + rnNotaFiscal.GerarDigitoVerificadorNFe(pedidoNota.nfeKey) });
                                }
                                else if (pedidoDetalhe.endEstado.ToLower() == "rn")
                                {
                                    dados.DadosGnre.c39_camposExtras.Add(new DadosEmissaoGnre.campoExtra() { codigo = "97", tipo = "T", valor = pedidoNota.nfeKey + rnNotaFiscal.GerarDigitoVerificadorNFe(pedidoNota.nfeKey) });
                                }
                                else if (pedidoDetalhe.endEstado.ToLower() == "rr")
                                {
                                    dados.DadosGnre.c05_referencia = new DadosEmissaoGnre.c05_referencia()
                                    {
                                        ano = DateTime.Now.Year.ToString(),
                                        mes = String.Format("{0:00}", DateTime.Now.Month)
                                    };
                                    dados.DadosGnre.c39_camposExtras.Add(new DadosEmissaoGnre.campoExtra() { codigo = "36", tipo = "T", valor = pedidoNota.nfeKey + rnNotaFiscal.GerarDigitoVerificadorNFe(pedidoNota.nfeKey) });
                                }
                                else if (pedidoDetalhe.endEstado.ToLower() == "sc")
                                {
                                    dados.DadosGnre.c14_dataVencimento = dataPagamentoString;
                                    dados.DadosGnre.c10_valorTotal = ((decimal)pedidoNota.valorPartilha).ToString("0.00").Replace(",", ".");
                                    dados.DadosGnre.c39_camposExtras.Add(new DadosEmissaoGnre.campoExtra() { codigo = "84", tipo = "T", valor = pedidoNota.nfeKey + rnNotaFiscal.GerarDigitoVerificadorNFe(pedidoNota.nfeKey) });
                                }
                                else if (pedidoDetalhe.endEstado.ToLower() == "se")
                                {
                                    dados.DadosGnre.c39_camposExtras.Add(new DadosEmissaoGnre.campoExtra() { codigo = "77", tipo = "T", valor = pedidoNota.nfeKey + rnNotaFiscal.GerarDigitoVerificadorNFe(pedidoNota.nfeKey) });
                                }
                                else if (pedidoDetalhe.endEstado.ToLower() == "to")
                                {
                                    dados.DadosGnre.c05_referencia = new DadosEmissaoGnre.c05_referencia()
                                    {
                                        ano = DateTime.Now.Year.ToString(),
                                        mes = String.Format("{0:00}", DateTime.Now.Month)
                                    };
                                    dados.DadosGnre.c39_camposExtras.Add(new DadosEmissaoGnre.campoExtra() { codigo = "80", tipo = "T", valor = pedidoNota.nfeKey + rnNotaFiscal.GerarDigitoVerificadorNFe(pedidoNota.nfeKey) });
                                }

                                else if (pedidoDetalhe.endEstado.ToLower() == "ce")
                                {
                                    dados.DadosGnre.c39_camposExtras.Add(new DadosEmissaoGnre.campoExtra() { codigo = "11", tipo = "T", valor = pedidoNota.nfeKey + rnNotaFiscal.GerarDigitoVerificadorNFe(pedidoNota.nfeKey) });
                                }
                                else if (pedidoDetalhe.endEstado.ToLower() == "pi")
                                {
                                    dados.DadosGnre.c39_camposExtras.Add(new DadosEmissaoGnre.campoExtra() { codigo = "11", tipo = "T", valor = pedidoNota.nfeKey + rnNotaFiscal.GerarDigitoVerificadorNFe(pedidoNota.nfeKey) });
                                }
                                else if (pedidoDetalhe.endEstado.ToLower() == "rs")
                                {
                                    dados.DadosGnre.c39_camposExtras.Add(new DadosEmissaoGnre.campoExtra() { codigo = "74", tipo = "T", valor = pedidoNota.nfeKey + rnNotaFiscal.GerarDigitoVerificadorNFe(pedidoNota.nfeKey) });
                                }
                                else if (pedidoDetalhe.endEstado.ToLower() == "pa")
                                {
                                    dados.DadosGnre.c39_camposExtras.Add(new DadosEmissaoGnre.campoExtra() { codigo = "101", tipo = "T", valor = pedidoNota.nfeKey + rnNotaFiscal.GerarDigitoVerificadorNFe(pedidoNota.nfeKey) });
                                }
                                else if (pedidoDetalhe.endEstado.ToLower() == "ro")
                                {
                                    dados.DadosGnre.c39_camposExtras.Add(new DadosEmissaoGnre.campoExtra() { codigo = "83", tipo = "T", valor = pedidoNota.nfeKey + rnNotaFiscal.GerarDigitoVerificadorNFe(pedidoNota.nfeKey) });
                                }
                                else
                                {
                                    dados.DadosGnre.c39_camposExtras.Add(new DadosEmissaoGnre.campoExtra() { codigo = "11", tipo = "T", valor = pedidoNota.nfeKey + rnNotaFiscal.GerarDigitoVerificadorNFe(pedidoNota.nfeKey) });
                                }
                                string xml = dados.RetornaXmlCompleto();



                                System.Net.ServicePointManager.ServerCertificateValidationCallback = ((sender, certificate, chain, sslPolicyErrors) => true);
                                string file = @"C:\certificados\lgf.pfx";
                                X509Certificate Cert = new X509Certificate();
                                Cert.Import(file, "123", X509KeyStorageFlags.PersistKeySet);
                                XmlDocument soapEnvelopeXml = new XmlDocument();
                                soapEnvelopeXml.LoadXml(xml);

                                var recepcaoGnre = new RecepcaoGnre.GnreLoteRecepcao();
                                recepcaoGnre.gnreCabecMsgValue = new RecepcaoGnre.gnreCabecMsg { versaoDados = "1.00" };
                                recepcaoGnre.ClientCertificates.Add(Cert);
                                var result = recepcaoGnre.processar(soapEnvelopeXml);
                                string codigoStatus = result.ChildNodes[1].ChildNodes[0].InnerText;
                                string status = result.ChildNodes[1].ChildNodes[1].InnerText;
                                string recibo = "";
                                if (result.ChildNodes[2] != null) recibo = result.ChildNodes[2].ChildNodes[0].InnerText;

                                if (!string.IsNullOrEmpty(recibo)) pedidoNota.statusGnre = 1;
                                else
                                {
                                    pedidoNota.statusGnre = -1;
                                }
                                pedidoNota.codigoStatusRequisicaoGnre = codigoStatus;
                                pedidoNota.statusRequisicaoGnre = status;
                                pedidoNota.reciboRequisicaoGnre = recibo;
                                data.SaveChanges();
                            }
                            else
                            {
                                pedidoNota.statusGnre = 99;
                                data.SaveChanges();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                rnEmails.EnviaEmail("", "andre@graodegente.com.br", "", "", "", ex.InnerException.ToString(), "Erro gnre");
            }
        }
        public static void consultaGnres()
        {
            var data = new dbSiteEntities();
            var pedidosNota =
                (from c in data.tbNotaFiscal
                 where c.statusNfe == 2 && c.statusGnre == 1 && c.idCNPJ == 6
                 select c).ToList();
            foreach (var pedidoNota in pedidosNota)
            {

                string xmlConsulta = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><TConsLote_GNRE xmlns=\"http://www.gnre.pe.gov.br\"><ambiente>1</ambiente><numeroRecibo>" + pedidoNota.reciboRequisicaoGnre + "</numeroRecibo></TConsLote_GNRE>";

                System.Net.ServicePointManager.ServerCertificateValidationCallback = ((sender, certificate, chain, sslPolicyErrors) => true);
                string file = @"C:\certificados\lgf.pfx";
                X509Certificate Cert = new X509Certificate();
                Cert.Import(file, "123", X509KeyStorageFlags.PersistKeySet);
                XmlDocument soapEnvelopeXml = new XmlDocument();
                soapEnvelopeXml.LoadXml(xmlConsulta);

                var recepcaoGnre = new ResultadoGnre.GnreResultadoLote();
                recepcaoGnre.gnreCabecMsgValue = new ResultadoGnre.gnreCabecMsg { versaoDados = "1.00" };
                recepcaoGnre.ClientCertificates.Add(Cert);
                var result = recepcaoGnre.consultar(soapEnvelopeXml);

                string codigoStatus = result.ChildNodes[2].ChildNodes[0].InnerText;
                string status = result.ChildNodes[2].ChildNodes[1].InnerText;
                string resultado = "";
                if (result.ChildNodes[3] != null)
                {
                    resultado = result.ChildNodes[3].InnerText;
                }
                pedidoNota.codigoStatusConsultaLote = codigoStatus;
                pedidoNota.statusConsultaLote = status;
                pedidoNota.resultado = resultado;
                data.SaveChanges();

                using (StringReader reader = new StringReader(resultado))
                {
                    string line;
                    while ((line = reader.ReadLine()) != null)
                    {
                        if (line.StartsWith("1"))
                        {
                            var registro = new RegistrodeGuia(line);
                            if (!string.IsNullOrEmpty(registro.RepresentacaooNumerica))
                            {
                                long codigoBarras = Convert.ToInt64(registro.RepresentacaooNumerica.Substring(0, 10));
                                if (codigoBarras > 0)
                                {
                                    pedidoNota.statusGnre = 2;
                                    pedidoNota.linhaDigitavelCodigoDeBarras = registro.RepresentacaooNumerica;
                                    data.SaveChanges();
                                }
                                else
                                {
                                    pedidoNota.statusGnre = -1;
                                    pedidoNota.linhaDigitavelCodigoDeBarras = registro.RepresentacaooNumerica;
                                    data.SaveChanges();
                                }
                            }
                        }
                    }
                }

            }
            int idLoteBoleto = 1;
            var ultimoLote = (from c in data.tbNotaFiscal where c.idLoteImpressaoGnre != null orderby c.idLoteImpressaoGnre descending select c).FirstOrDefault();
            if (ultimoLote != null)
            {
                idLoteBoleto = (ultimoLote.idLoteImpressaoGnre ?? 0) + 1;
            }
            var pedidosSemLoteBoleto = (from c in data.tbNotaFiscal where c.statusGnre == 2 && c.idCNPJ == 6 && c.idLoteImpressaoGnre == null orderby c.dataHora select c).ToList();
            if (pedidosSemLoteBoleto.Any())
            {
                foreach (var pedidoSemLote in pedidosSemLoteBoleto)
                {
                    pedidoSemLote.idLoteImpressaoGnre = idLoteBoleto;
                    data.SaveChanges();
                }
             }
       }

        public static void AtualizarNotasFiscais()
        {

            string caminhoArquivoProcessado = @"c:\notas\";
            DirectoryInfo dir = new DirectoryInfo(@"C:\notas\");
            FileInfo[] files = dir.GetFiles("*.xml", SearchOption.AllDirectories);

            List<string> listaErros = new List<string>();
            using (var data = new dbSiteEntities())
            {
                foreach (FileInfo file in files)
                {
                    XmlDocument doc = new XmlDocument();
                    doc.Load(caminhoArquivoProcessado + file.Name);
                    foreach (XmlNode rootNode in doc)
                    {
                        foreach (XmlNode childNfe in rootNode.ChildNodes)
                        {
                            if (childNfe.Name == "NFe")
                            {
                                foreach (XmlNode childInfNFE in childNfe.ChildNodes)
                                {
                                    if (childInfNFE.Name.Contains("infNFe"))
                                    {
                                        int numeroNota = 0;
                                        decimal totalIcmsOrigem = 0;
                                        decimal totalIcmsDestino = 0;
                                        var notaDetalhe = (from c in data.tbNotaFiscal where c.numeroNota == numeroNota && c.idCNPJ == 6 select c).FirstOrDefault();

                                        foreach (XmlNode childDetNFE in childInfNFE.ChildNodes)
                                        {
                                            if (childDetNFE.Name == "ide")
                                            {
                                                foreach (XmlNode childDetImpostNFE in childDetNFE.ChildNodes)
                                                {
                                                    if (childDetImpostNFE.Name == "nNF")
                                                    {
                                                        numeroNota = Convert.ToInt32(childDetImpostNFE.FirstChild.Value);
                                                        notaDetalhe = (from c in data.tbNotaFiscal where c.numeroNota == numeroNota && c.idCNPJ == 6 select c).FirstOrDefault();
                                                    }
                                                }
                                            }
                                        }


                                        if (notaDetalhe != null)
                                        {
                                            var pedidoDetalhe = (from c in data.tbPedidos where c.pedidoId == notaDetalhe.idPedido select c).FirstOrDefault();

                                            foreach (XmlNode childDetNFE in childInfNFE.ChildNodes)
                                            {
                                                if (childDetNFE.Name == "det" && (pedidoDetalhe.endEstado.ToLower() == "al" | pedidoDetalhe.endEstado.ToLower() == "ce" | pedidoDetalhe.endEstado.ToLower() == "pi" | pedidoDetalhe.endEstado.ToLower() == "rj" | pedidoDetalhe.endEstado.ToLower() == "ro"))
                                                {
                                                    foreach (XmlNode childDetImpostNFE in childDetNFE.ChildNodes)
                                                    {
                                                        decimal vBCUFDest = 0;
                                                        decimal pICMSUFDest = 0;
                                                        decimal pICMSInter = 0;
                                                        decimal vICMSUFDest = 0;
                                                        decimal vICMSUFRemet = 0;
                                                        int produtoId = 0;

                                                        if (childDetImpostNFE.Name == "prod")
                                                        {
                                                            foreach (XmlNode childDetImpostdetalhesNFE in childDetImpostNFE.ChildNodes)
                                                            {
                                                                if (childDetImpostdetalhesNFE.Name == "cProd")
                                                                {
                                                                    produtoId = Convert.ToInt32(childDetImpostdetalhesNFE.FirstChild.Value);
                                                                }
                                                            }
                                                        }

                                                        if (childDetImpostNFE.Name == "imposto")
                                                        {
                                                            foreach (XmlNode childDetImpostdetalhesNFE in childDetImpostNFE.ChildNodes)
                                                            {
                                                                if (childDetImpostdetalhesNFE.Name == "ICMSUFDest")
                                                                {


                                                                    foreach (XmlNode childDetImpostdetalhesFilhosNFE in childDetImpostdetalhesNFE.ChildNodes)
                                                                    {
                                                                        if (childDetImpostdetalhesFilhosNFE.Name == "vBCUFDest")
                                                                        {
                                                                            vBCUFDest = Convert.ToDecimal(childDetImpostdetalhesFilhosNFE.FirstChild.Value.Replace(".", ","));
                                                                        }
                                                                        if (childDetImpostdetalhesFilhosNFE.Name == "pICMSUFDest")
                                                                        {
                                                                            pICMSUFDest = Convert.ToDecimal(childDetImpostdetalhesFilhosNFE.FirstChild.Value.Replace(".", ","));
                                                                        }
                                                                        if (childDetImpostdetalhesFilhosNFE.Name == "pICMSInter")
                                                                        {
                                                                            pICMSInter = Convert.ToDecimal(childDetImpostdetalhesFilhosNFE.FirstChild.Value.Replace(".", ","));
                                                                        }
                                                                        if (childDetImpostdetalhesFilhosNFE.Name == "vICMSUFDest")
                                                                        {
                                                                            vICMSUFDest = Convert.ToDecimal(childDetImpostdetalhesFilhosNFE.FirstChild.Value.Replace(".", ","));
                                                                        }
                                                                        if (childDetImpostdetalhesFilhosNFE.Name == "vICMSUFRemet")
                                                                        {
                                                                            vICMSUFRemet = Convert.ToDecimal(childDetImpostdetalhesFilhosNFE.FirstChild.Value.Replace(".", ","));
                                                                        }
                                                                    }

                                                                }
                                                            }
                                                        }

                                                        var difal = rnGnre.CalculaDiferencialAliquota("mg", pedidoDetalhe.endEstado.ToLower(), vBCUFDest, notaDetalhe.dataHora);
                                                        pICMSUFDest = difal.icmsDestino;
                                                        pICMSInter = difal.icmsOrigem;
                                                        vICMSUFDest = difal.diferencialDestino;
                                                        vICMSUFRemet = difal.diferencialOrigem;

                                                        totalIcmsOrigem += vICMSUFRemet;
                                                        totalIcmsDestino += vICMSUFDest;


                                                        if (childDetImpostNFE.Name == "imposto")
                                                        {
                                                            foreach (XmlNode childDetImpostdetalhesNFE in childDetImpostNFE.ChildNodes)
                                                            {
                                                                if (childDetImpostdetalhesNFE.Name == "ICMSUFDest")
                                                                {

                                                                    foreach (XmlNode childDetImpostdetalhesFilhosNFE in childDetImpostdetalhesNFE.ChildNodes)
                                                                    {
                                                                        if (childDetImpostdetalhesFilhosNFE.Name == "pICMSUFDest")
                                                                        {
                                                                            childDetImpostdetalhesFilhosNFE.FirstChild.Value = pICMSUFDest.ToString("0.00").Replace(",", ".");
                                                                        }
                                                                        if (childDetImpostdetalhesFilhosNFE.Name == "pICMSInter")
                                                                        {
                                                                            childDetImpostdetalhesFilhosNFE.FirstChild.Value = pICMSInter.ToString("0.00").Replace(",", ".");
                                                                        }
                                                                        if (childDetImpostdetalhesFilhosNFE.Name == "vICMSUFDest")
                                                                        {
                                                                            childDetImpostdetalhesFilhosNFE.FirstChild.Value = vICMSUFDest.ToString("0.00").Replace(",", ".");
                                                                        }
                                                                        if (childDetImpostdetalhesFilhosNFE.Name == "vICMSUFRemet")
                                                                        {
                                                                            childDetImpostdetalhesFilhosNFE.FirstChild.Value = vICMSUFRemet.ToString("0.00").Replace(",", ".");
                                                                        }
                                                                    }

                                                                }
                                                            }
                                                        }

                                                    }

                                                }






                                            }


                                            foreach (XmlNode childDetNFE in childInfNFE.ChildNodes)
                                            {

                                                if (childDetNFE.Name == "total" && (pedidoDetalhe.endEstado.ToLower() == "al" | pedidoDetalhe.endEstado.ToLower() == "ce" | pedidoDetalhe.endEstado.ToLower() == "pi" | pedidoDetalhe.endEstado.ToLower() == "rj" | pedidoDetalhe.endEstado.ToLower() == "ro"))
                                                {
                                                    foreach (XmlNode childDetImpostdetalhesNFE in childDetNFE.ChildNodes)
                                                    {
                                                        if (childDetImpostdetalhesNFE.Name == "ICMSTot")
                                                        {
                                                            foreach (XmlNode childDetImpostdetalhesIcmsNFE in childDetImpostdetalhesNFE.ChildNodes)
                                                            {
                                                                if (childDetImpostdetalhesIcmsNFE.Name == "vICMSUFDest")
                                                                {
                                                                    childDetImpostdetalhesIcmsNFE.FirstChild.Value = totalIcmsDestino.ToString("0.00").Replace(",", ".");
                                                                }
                                                                if (childDetImpostdetalhesIcmsNFE.Name == "vICMSUFRemet")
                                                                {
                                                                    childDetImpostdetalhesIcmsNFE.FirstChild.Value = totalIcmsOrigem.ToString("0.00").Replace(",", ".");
                                                                }
                                                            }
                                                        }
                                                    }



                                                    notaDetalhe.valorPartilha = totalIcmsDestino;
                                                    data.SaveChanges();

                                                    doc.Save(caminhoArquivoProcessado + pedidoDetalhe.endEstado + "_alterado_" + file.Name);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        public static DiferencialAliquota CalculaDiferencialAliquota(string estadoOrigem, string estadoDestino, decimal valor, DateTime dataEmissao)
        {
            decimal aliquotaInterestadual = 7;
            decimal aliquotaInterna = 17;
            if (estadoOrigem == "sp")
            {
                switch (estadoDestino.ToLower())
                {
                    case "ac":
                        aliquotaInterna = 17;
                        break;
                    case "al":
                        aliquotaInterna = 17;
                        break;
                    case "am":
                        aliquotaInterna = 18;
                        break;
                    case "ap":
                        aliquotaInterna = 18;
                        break;
                    case "ba":
                        aliquotaInterna = 18;
                        break;
                    case "ce":
                        aliquotaInterna = 17;
                        break;
                    case "df":
                        aliquotaInterna = 18;
                        break;
                    case "es":
                        aliquotaInterna = 17;
                        aliquotaInterestadual = 7;
                        break;
                    case "go":
                        aliquotaInterna = 17;
                        break;
                    case "ma":
                        aliquotaInterna = 18;
                        break;
                    case "mt":
                        aliquotaInterna = 17;
                        break;
                    case "ms":
                        aliquotaInterna = 17;
                        break;
                    case "mg":
                        aliquotaInterna = 18;
                        aliquotaInterestadual = 12;
                        break;
                    case "pa":
                        aliquotaInterna = 17;
                        break;
                    case "pb":
                        aliquotaInterna = 18;
                        break;
                    case "pr":
                        aliquotaInterna = 18;
                        aliquotaInterestadual = 12;
                        break;
                    case "pe":
                        aliquotaInterna = 18;
                        break;
                    case "pi":
                        aliquotaInterna = 17;
                        break;
                    case "rn":
                        aliquotaInterna = 18;
                        break;
                    case "rs":
                        aliquotaInterna = 18;
                        aliquotaInterestadual = 12;
                        break;
                    case "rj":
                        aliquotaInterna = 18;
                        aliquotaInterestadual = 12;
                        break;
                    case "ro":
                        aliquotaInterna = 17;
                        break;
                    case "rr":
                        aliquotaInterna = 17;
                        break;
                    case "sc":
                        aliquotaInterna = 17;
                        aliquotaInterestadual = 12;
                        break;
                    case "sp":
                        aliquotaInterna = 18;
                        aliquotaInterestadual = 12;
                        break;
                    case "se":
                        aliquotaInterna = 18;
                        break;
                    case "to":
                        aliquotaInterna = 18;
                        break;
                }
            }
            if (estadoOrigem == "mg")
            {
                switch (estadoDestino.ToLower())
                {
                    case "ac":
                        aliquotaInterna = 17;
                        break;
                    case "al":
                        aliquotaInterna = 18;
                        break;
                    case "am":
                        aliquotaInterna = 18;
                        break;
                    case "ap":
                        aliquotaInterna = 18;
                        break;
                    case "ba":
                        aliquotaInterna = 18;
                        break;
                    case "ce":
                        aliquotaInterna = 18;
                        break;
                    case "df":
                        aliquotaInterna = 18;
                        break;
                    case "es":
                        aliquotaInterna = 17;
                        aliquotaInterestadual = 7;
                        break;
                    case "go":
                        aliquotaInterna = 17;
                        break;
                    case "ma":
                        aliquotaInterna = 18;
                        break;
                    case "mt":
                        aliquotaInterna = 17;
                        break;
                    case "ms":
                        aliquotaInterna = 17;
                        break;
                    case "mg":
                        aliquotaInterna = 18;
                        aliquotaInterestadual = 12;
                        break;
                    case "pa":
                        aliquotaInterna = 17;
                        break;
                    case "pb":
                        aliquotaInterna = 18;
                        break;
                    case "pr":
                        aliquotaInterna = 18;
                        aliquotaInterestadual = 12;
                        break;
                    case "pe":
                        aliquotaInterna = 18;
                        break;
                    case "pi":
                        aliquotaInterna = 18;
                        break;
                    case "rn":
                        aliquotaInterna = 18;
                        break;
                    case "rs":
                        aliquotaInterna = 18;
                        aliquotaInterestadual = 12;
                        break;
                    case "rj":
                        aliquotaInterna = 20;
                        aliquotaInterestadual = 12;
                        break;
                    case "ro":
                        aliquotaInterna = Convert.ToDecimal("17,5");
                        break;
                    case "rr":
                        aliquotaInterna = 17;
                        break;
                    case "sc":
                        aliquotaInterna = 17;
                        aliquotaInterestadual = 12;
                        break;
                    case "sp":
                        aliquotaInterna = 18;
                        aliquotaInterestadual = 12;
                        break;
                    case "se":
                        aliquotaInterna = 18;
                        break;
                    case "to":
                        aliquotaInterna = 18;
                        break;
                }
            }

            decimal partilhaDestino = 40;
            if (dataEmissao.Year == 2017) partilhaDestino = 60;

            decimal diferencialAliquota = aliquotaInterna - aliquotaInterestadual;
            decimal valorDiferencial = (valor / 100) * diferencialAliquota;

            var retorno = new DiferencialAliquota();
            retorno.totalDiferencial = valorDiferencial;
            retorno.diferencialOrigem = Convert.ToDecimal(((valorDiferencial / 100) * (100 - partilhaDestino)).ToString("0.00"));
            retorno.diferencialDestino = Convert.ToDecimal(((valorDiferencial / 100) * (partilhaDestino)).ToString("0.00"));

            retorno.icmsOrigem = aliquotaInterestadual;
            retorno.icmsDestino = aliquotaInterna;
            retorno.partilhaDestino = partilhaDestino;

            return retorno;
        }


    }


    public class DiferencialAliquota
    {
        public decimal icmsOrigem { get; set; }
        public decimal icmsDestino { get; set; }
        public decimal totalDiferencial { get; set; }
        public decimal diferencialOrigem { get; set; }
        public decimal diferencialDestino { get; set; }
        public decimal partilhaDestino { get; set; }
    }

}
