﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace bcServerService
{
    class rnEnums
    {
        public enum TipoRegistroRelacionado
        {
            Queue = 1,
            Pedido = 2,
            ItemPedido = 3,
            Produto = 4,
            Banner = 6,
            Contador = 7,
            Etiqueta = 8
        }

        public enum TipoOperacao
        {
            Estoque = 1,
            Pedido = 2,
            Produto = 3,
            Prazo = 4,
            Banner = 5
        }

        public class TipoRelacionadoIds
        {
            public long idRegistroRelacionado { get; set; }
            public TipoRegistroRelacionado tipoRelacionado { get; set; }
        }
    }
}
