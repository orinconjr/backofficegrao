//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace bcServerService
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbDescricaoTabelaProdutoGrupo
    {
        public tbDescricaoTabelaProdutoGrupo()
        {
            this.tbDescricaoTabelaProdutoGrupoItem = new HashSet<tbDescricaoTabelaProdutoGrupoItem>();
            this.tbProdutoDescricaoTabelaGrupo = new HashSet<tbProdutoDescricaoTabelaGrupo>();
        }
    
        public int idDescricaoTabelaProdutoGrupo { get; set; }
        public string nomeInterno { get; set; }
        public string nomeAmigavel { get; set; }
        public int idSite { get; set; }
    
        public virtual ICollection<tbDescricaoTabelaProdutoGrupoItem> tbDescricaoTabelaProdutoGrupoItem { get; set; }
        public virtual tbSite tbSite { get; set; }
        public virtual ICollection<tbProdutoDescricaoTabelaGrupo> tbProdutoDescricaoTabelaGrupo { get; set; }
    }
}
