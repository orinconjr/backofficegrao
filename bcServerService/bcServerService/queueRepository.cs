﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bcServerService
{
    public static class queueRepository
    {
        public static void AtualizaProdutoElastic(int produtoId)
        {
            using (var data = new dbSiteEntities())
            {
                var queue = new tbQueue()
                {
                    agendamento = DateTime.Now,
                    andamento = false,
                    concluido = false,
                    dataConclusao = null,
                    dataInicio = null,
                    idRelacionado = produtoId,
                    mensagem = "",
                    tipoQueue = 15
                };
                data.tbQueue.Add(queue);
                data.SaveChanges();
            }
        }
    }
}
