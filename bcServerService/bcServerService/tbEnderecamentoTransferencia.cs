//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace bcServerService
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbEnderecamentoTransferencia
    {
        public tbEnderecamentoTransferencia()
        {
            this.tbTransferenciaEndereco = new HashSet<tbTransferenciaEndereco>();
        }
    
        public int idEnderecamentoTransferencia { get; set; }
        public string enderecamentoTransferencia { get; set; }
        public Nullable<int> idCentroDistribuicao { get; set; }
    
        public virtual ICollection<tbTransferenciaEndereco> tbTransferenciaEndereco { get; set; }
    }
}
