﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bcServerService.CloudFront
{
    interface IAWSCloudFrontService
    {
        void CreateInvalidation(List<string> paths);
    }
}
