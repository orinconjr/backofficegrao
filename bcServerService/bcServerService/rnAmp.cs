﻿using Amazon;
using Amazon.Runtime;
using Amazon.S3.Model;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bcServerService
{
    public class rnAmp
    {
        public static void GerarAmp(ElasticSearch.Produto produto)
        {
            string amp = carregarTemplateArquivo(rnConfiguracoes.caminhoFisico + "\\amp\\" + "amp.html");
            string brinde = carregarTemplateArquivo(rnConfiguracoes.caminhoFisico + "\\amp\\" + "brinde.html");
            string colecao = carregarTemplateArquivo(rnConfiguracoes.caminhoFisico + "\\amp\\" + "colecao.html");
            string css360 = carregarTemplateArquivo(rnConfiguracoes.caminhoFisico + "\\amp\\" + "css360.html");
            string cssespecificacoes = carregarTemplateArquivo(rnConfiguracoes.caminhoFisico + "\\amp\\" + "cssespecificacoes.html");
            string dots = carregarTemplateArquivo(rnConfiguracoes.caminhoFisico + "\\amp\\" + "dots.html");
            string especificacaoitem = carregarTemplateArquivo(rnConfiguracoes.caminhoFisico + "\\amp\\" + "especificacaoitem.html");
            string especificacaoitemproduto = carregarTemplateArquivo(rnConfiguracoes.caminhoFisico + "\\amp\\" + "especificacaoitemproduto.html");
            string especificacoes = carregarTemplateArquivo(rnConfiguracoes.caminhoFisico + "\\amp\\" + "especificacoes.html");
            string foto = carregarTemplateArquivo(rnConfiguracoes.caminhoFisico + "\\amp\\" + "foto.html");
            string pecas = carregarTemplateArquivo(rnConfiguracoes.caminhoFisico + "\\amp\\" + "pecas.html");
            string t360 = carregarTemplateArquivo(rnConfiguracoes.caminhoFisico + "\\amp\\" + "t360.html");
            string video = carregarTemplateArquivo(rnConfiguracoes.caminhoFisico + "\\amp\\" + "video.html");
            string videotag = carregarTemplateArquivo(rnConfiguracoes.caminhoFisico + "\\amp\\" + "videotag.html");
            string recomendados = carregarTemplateArquivo(rnConfiguracoes.caminhoFisico + "\\amp\\" + "recomendados.html");
            string cssRecomendados = carregarTemplateArquivo(rnConfiguracoes.caminhoFisico + "\\amp\\" + "cssrecomendados.html");
            string originaisSelo = carregarTemplateArquivo(rnConfiguracoes.caminhoFisico + "\\amp\\" + "originaisSelo.html");


            amp = amp.Replace("{{categoriaurl}}", produto.categoriaurl);
            amp = amp.Replace("{{produtourl}}", produto.produtourl);
            amp = amp.Replace("{{produtonome}}", produto.nome);
            amp = amp.Replace("{{produtodescricao}}", produto.produtoDescricao);
            amp = amp.Replace("{{produtoid}}", produto.id.ToString());


            if(produto.exclusivo){
                amp = amp.Replace("{{originaisSelo}}", originaisSelo);
            }else{
                amp = amp.Replace("{{originaisSelo}}", "");
            }

            string fotos = "";
            int numero = 1;
            foreach(var produtofoto in produto.fotos)
            {
                string fotoTemplate = foto;
                fotoTemplate = fotoTemplate.Replace("{{categoriaurl}}", produto.categoriaurl);
                fotoTemplate = fotoTemplate.Replace("{{produtourl}", produto.produtourl);
                fotoTemplate = fotoTemplate.Replace("{{produtonome}", produto.nome);
                fotoTemplate = fotoTemplate.Replace("{{produtofoto}}", produtofoto.foto);
                fotoTemplate = fotoTemplate.Replace("{{produtoid}}", produto.id.ToString());
                fotoTemplate = fotoTemplate.Replace("{{tabindex}}", numero.ToString());
                fotos += fotoTemplate;
                numero++;
            }
            amp = amp.Replace("{{fotos}}", fotos);

            string videos = "";
            foreach (var produtovideo in produto.videos)
            {
                string videoTemplate = video;
                videoTemplate = videoTemplate.Replace("{{videoid}}", produtovideo.link.Split('?')[1].Replace("v=", ""));
                videoTemplate = videoTemplate.Replace("{{tabindex}}", numero.ToString());
                videos += videoTemplate;
                numero++;
            }
            amp = amp.Replace("{{videos}}", videos);
            if (!string.IsNullOrEmpty(videos))
            {
                amp = amp.Replace("{{videotag}}", videotag);
            }
            else
            {
                amp = amp.Replace("{{videotag}}", "");
            }
            string dotsHtml = "";
            for(int i = 1; i < produto.fotos.Count; i++)
            {
                var dotTemplate = dots;
                dotTemplate = dotTemplate.Replace("{{numero}}", i.ToString());
                dotsHtml += dotTemplate;
            }
            amp = amp.Replace("{{dots}}", dotsHtml);

            string templateCss360 = "";
            string template360 = "";
            if (!string.IsNullOrEmpty(produto.foto360))
            {
                template360 = t360;
                templateCss360 = css360.Replace("{{produtoid}}", produto.id.ToString());
                template360 = template360.Replace("{{produtourl}}", produto.produtourl);
                template360 = template360.Replace("{{categoriaurl}}", produto.categoriaurl);
            }

            amp = amp.Replace("{{t360}}", template360);
            amp = amp.Replace("{{css360}}", templateCss360);

            string tEspecificacoes = "";
            string tCssEspecificacoes = "";
            if (produto.Especificacoes.Count > 0)
            {
                string templateEspecificacoes = especificacoes;
                string templateCssEspecificacoes = cssespecificacoes;
                string templateCssItemEspecificacoes = "";
                string templateEspecificaoItens = "";

                foreach (var especificacao in produto.Especificacoes)
                {
                    string templateEspecificacaoItem = especificacaoitem;
                    templateEspecificacaoItem = templateEspecificacaoItem.Replace("{{especificacaonome}}", especificacao.Nome);

                    if(especificacao.exibirFoto == true)
                    {
                        templateEspecificacaoItem = templateEspecificacaoItem.Replace("{{cssulespecificacao}}", "listaImagem");
                    }
                    else
                    {
                        templateEspecificacaoItem = templateEspecificacaoItem.Replace("{{cssulespecificacao}}", "");
                    }
                    string templateEspecificaoItemProdutos = "";
                    foreach(var especificacaoItem in especificacao.especificacaoItens.OrderBy(x => x.ordem))
                    {
                        string templateEspecificacaoItemProduto = especificacaoitemproduto;
                        templateEspecificacaoItemProduto = templateEspecificacaoItemProduto.Replace("{{categoriaurl}}", produto.categoriaurl);
                        templateEspecificacaoItemProduto = templateEspecificacaoItemProduto.Replace("{{produtourl}}", especificacaoItem.produtoUrl);
                        templateEspecificacaoItemProduto = templateEspecificacaoItemProduto.Replace("{{produtoid}}", especificacaoItem.produtoId.ToString());
                        templateEspecificacaoItemProduto = templateEspecificacaoItemProduto.Replace("{{especificacaoitemnome}}", especificacaoItem.nome);
                        if(especificacaoItem.produtoId == produto.id)
                        {
                            templateEspecificacaoItem = templateEspecificacaoItem.Replace("{{especificacaoatual}}", especificacaoItem.nome);
                            templateEspecificacaoItemProduto = templateEspecificacaoItemProduto.Replace("{{cssespecificacaoativa}}", "class=\"active\"");
                        }
                        else
                        {
                            templateEspecificacaoItemProduto = templateEspecificacaoItemProduto.Replace("{{cssespecificacaoativa}}", "");
                        }

                        if (especificacao.exibirFoto == true)
                        {
                            templateEspecificacaoItemProduto = templateEspecificacaoItemProduto.Replace("{{idspanespecificacao}}", "id=\"item" + especificacaoItem.produtoId + "\"");
                            templateEspecificacaoItemProduto = templateEspecificacaoItemProduto.Replace("{{cssspanitemespecificacao}}", "class=\"blocoCor\"");
                            templateCssItemEspecificacoes += "#item" + especificacaoItem.produtoId.ToString() + "{ " + especificacaoItem.estilo + " } ";
                        }
                        else
                        {
                            templateEspecificacaoItemProduto = templateEspecificacaoItemProduto.Replace("{{idspanespecificacao}}", "");
                            templateEspecificacaoItemProduto = templateEspecificacaoItemProduto.Replace("{{cssspanitemespecificacao}}", "");
                        }
                        templateEspecificaoItemProdutos += templateEspecificacaoItemProduto;
                    }
                    templateEspecificacaoItem = templateEspecificacaoItem.Replace("{{especificacaoitemproduto}}", templateEspecificaoItemProdutos);

                    templateEspecificaoItens += templateEspecificacaoItem;
                }

                templateCssEspecificacoes = templateCssEspecificacoes.Replace("{{cssitemespecificacao}}", templateCssItemEspecificacoes);
                templateEspecificacoes = templateEspecificacoes.Replace("{{especificacaoitem}}", templateEspecificaoItens);

                tEspecificacoes = templateEspecificacoes;
                tCssEspecificacoes = templateCssEspecificacoes;
            }

            var descricao = produto.informacoesAdicionais.FirstOrDefault(x => x.nome.ToLower() == "descricao" | x.nome.ToLower() == "descrição");
            var composicao = produto.informacoesAdicionais.FirstOrDefault(x => x.nome.ToLower() == "composicao" | x.nome.ToLower() == "composição");

            amp = amp.Replace("{{htmldescricao}}", descricao == null ? "" : removeStyles(descricao.conteudo));
            amp = amp.Replace("{{htmlcomposicao}}", composicao == null ? "" : removeStyles(composicao.conteudo));

            amp = amp.Replace("{{cssespecificacoes}}", tCssEspecificacoes);
            amp = amp.Replace("{{especificacoes}}", tEspecificacoes);

            int prazoAte = produto.prazopedidos + 1;
            int prazoDe = Convert.ToInt32(prazoAte / 2);

            amp = amp.Replace("{{prazode}}", prazoDe.ToString());
            amp = amp.Replace("{{prazoate}}", prazoAte.ToString());

            string tPecas = "";
            if(produto.pecas > 0)
            {
                tPecas = pecas.Replace("{{pecas}}", produto.pecas.ToString());
            }
            amp = amp.Replace("{{pecas}}", tPecas);
            amp = amp.Replace("{{precode}}", produto.produtopreco.ToString("0.00"));
            double precoAVista = produto.preco;
            if(produto.precopromocional > 0 && produto.precopromocional < produto.preco)
            {
                precoAVista = produto.precopromocional;
            }
            precoAVista = (precoAVista / 100) * 85;

            amp = amp.Replace("{{precoavista}}", precoAVista.ToString("0.00"));
            amp = amp.Replace("{{valorface}}", precoAVista.ToString("0.00").Replace(",", "."));


            var parcelamento = "";
            for(int i = 1; i <= produto.parcelamentomaximo; i++)
            {
                var valor = Convert.ToDecimal((produto.parcelamentomaximo * produto.parcelamentomaximovalorparcela) / i);
                parcelamento += "<li> <p> " + i + "x s/juros <strong>" + valor.ToString("C") + "</strong> </p> </li>";
            }
            amp = amp.Replace("{{parcelamentocartao}}", parcelamento);

            amp = amp.Replace("{{parcelamentoboleto}}", "<li> <p> 1x s/juros <strong>" + precoAVista.ToString("0.00") + " </strong> - à vista com 15% de desconto </p> </li>");
            amp = amp.Replace("{{precoavistameta}}", precoAVista.ToString("0.00").Replace(",", "."));
            amp = amp.Replace("{{porcentagemoff}}", Convert.ToInt32(produto.descontototalavista).ToString());
            amp = amp.Replace("{{valorcartao}}", ((produto.precopromocional > 0 && produto.precopromocional < produto.preco) ? produto.precopromocional : produto.preco).ToString("0.00"));
            amp = amp.Replace("{{quantidadeparcela}}", Convert.ToInt32(produto.parcelamentomaximo).ToString());
            amp = amp.Replace("{{valorparcela}}", produto.parcelamentomaximovalorparcela.ToString("0.00"));
            amp = amp.Replace("{{recomendados}}", "");

            string tBrinde = "";
            if (produto.brindes > 0)
            {
                if (produto.brindes == 1)
                {
                    tBrinde = brinde.Replace("{{brindes}}", produto.brindes.ToString() + " BRINDE");
                }
                else
                {
                    tBrinde = brinde.Replace("{{brindes}}", produto.brindes.ToString() + " BRINDES");
                }
            }
            amp = amp.Replace("{{brinde}}", tBrinde);

            string tColecao = "";
            if (produto.colecaoids.Count == 1)
            {
                tColecao = colecao.Replace("{{categoriaurl}}", produto.categoriaurl).Replace("{{produtourl}}", produto.produtourl);
            }
            amp = amp.Replace("{{colecao}}", tColecao);

            string nomeArquivo = produto.categoriaurl + "/" + produto.produtourl + ".html";
            SaveOnS3(amp, nomeArquivo);
        }


        private static void SaveOnS3(string html, string arquivo)
        {
                var credentials = new BasicAWSCredentials(ConfigurationManager.AppSettings["accessKey"].ToString(), ConfigurationManager.AppSettings["secretAccessKey"].ToString());
                var s3Client = new Amazon.S3.AmazonS3Client(credentials, RegionEndpoint.SAEast1);
                string bucketUrl = "amp.graodegente.com.br";            

                PutObjectRequest putRequestHtml = new PutObjectRequest
                {
                    BucketName = bucketUrl,
                    Key = arquivo,
                    ContentType = "text/html",
                    ContentBody = html,
                    CannedACL = Amazon.S3.S3CannedACL.PublicRead
                };
                PutObjectResponse responseHtml = s3Client.PutObject(putRequestHtml);        
        }

        public static string removeStyles(string html)
        {
            HtmlDocument wDocumento = new HtmlDocument();
            wDocumento.LoadHtml(html);
            if (wDocumento.DocumentNode.SelectNodes("//@style") != null)
            {
                foreach (HtmlNode style in wDocumento.DocumentNode.SelectNodes("//@style"))
                {
                    RemoveElementKeepText(style);
                }
            }
            return wDocumento.DocumentNode.InnerHtml;
        }

        private static void RemoveElementKeepText(HtmlNode node)
        {
            //node.ParentNode.RemoveChild(node, true);
            HtmlNode parent = node.ParentNode;
            HtmlNode prev = node.PreviousSibling;
            HtmlNode next = node.NextSibling;

            foreach (HtmlNode child in node.ChildNodes)
            {
                if (prev != null)
                    parent.InsertAfter(child, prev);
                else if (next != null)
                    parent.InsertBefore(child, next);
                else
                    parent.AppendChild(child);

            }
            node.Remove();
        }
        public static string carregarTemplateArquivo(string pathArquivo)
        {
            try
            {
                if (!File.Exists(pathArquivo)) throw new Exception(String.Format("Arquivo '{0}' não encontrado!", pathArquivo));

                return File.ReadAllText(pathArquivo, Encoding.GetEncoding("ISO-8859-1"));
            }
            catch (Exception ex)
            {
                return "";
            }
        }
    }
}
