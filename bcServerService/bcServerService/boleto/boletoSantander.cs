﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace bcServerService.boleto
{
    public class boletoSantander
    {

        public static void transmiteBoleto()
        {
            var data = new dbSiteEntities();
            var cobranca = (from c in data.tbPedidoPagamento where c.idPedidoPagamento == 1358427 select c).First();
            var conta = (from d in data.tbContaPagamento where d.idContaPagamento == cobranca.idContaPagamento select d).FirstOrDefault();
            var cliente = (from clie in data.tbPedidos
                           join d in data.tbClientes on clie.clienteId equals d.clienteId
                           where clie.pedidoId == cobranca.pedidoId
                           select d).FirstOrDefault();
            string xmlTransmissao = "<TicketRequest>";
            xmlTransmissao += "<dados>";
            //xmlTransmissao += retornaEntrada("TP-AMB", "T");
            xmlTransmissao += retornaEntrada("CONVENIO.COD-BANCO", "0033");
            xmlTransmissao += retornaEntrada("CONVENIO.COD-CONVENIO", conta.codCedente);
            xmlTransmissao += retornaEntrada("PAGADOR.TP-DOC", "99");
            xmlTransmissao += retornaEntrada("PAGADOR.NUM-DOC", cobranca.numeroCobranca);
            xmlTransmissao += retornaEntrada("PAGADOR.NOME", rnFuncoes.removeCaracteresEspeciaisDeixaEspaco(rnFuncoes.removeAcentos(cliente.clienteNome)));
            xmlTransmissao += retornaEntrada("PAGADOR.ENDER", rnFuncoes.removeCaracteresEspeciaisDeixaEspaco(rnFuncoes.removeAcentos(cliente.clienteRua)) + ", " + rnFuncoes.removeCaracteresEspeciaisDeixaEspaco(rnFuncoes.removeAcentos(cliente.clienteNumero)));
            xmlTransmissao += retornaEntrada("PAGADOR.BAIRRO", rnFuncoes.removeCaracteresEspeciaisDeixaEspaco(rnFuncoes.removeAcentos(cliente.clienteBairro)));
            xmlTransmissao += retornaEntrada("PAGADOR.CIDADE", rnFuncoes.removeCaracteresEspeciaisDeixaEspaco(rnFuncoes.removeAcentos(cliente.clienteCidade)));
            xmlTransmissao += retornaEntrada("PAGADOR.UF", rnFuncoes.removeCaracteresEspeciaisDeixaEspaco(rnFuncoes.removeAcentos(cliente.clienteEstado)));
            xmlTransmissao += retornaEntrada("PAGADOR.CEP", cliente.clienteCep.Replace("-", "").Replace(" ", "").Replace(".", ""));
            xmlTransmissao += retornaEntrada("TITULO.NOSSO-NUMERO", (String.Format("{0:000000000}", Convert.ToInt64(cobranca.idPedidoPagamento))));
            xmlTransmissao += retornaEntrada("TITULO.SEU-NUMERO", cobranca.idPedidoPagamento.ToString());
            var vencimento = pagamento.rnBoleto.RetornaProximoDiaUtil(cobranca.dataVencimento);
            xmlTransmissao += retornaEntrada("TITULO.DT-VENCTO", vencimento.ToString("DDMMYYYY"));
            xmlTransmissao += retornaEntrada("TITULO.DT-EMISSAO", (cobranca.tbPedidos.dataHoraDoPedido ?? vencimento).ToString("DDMMYYYY"));
            xmlTransmissao += retornaEntrada("TITULO.ESPECIE", "17");
            xmlTransmissao += retornaEntrada("TITULO.VL-NOMINAL", (Decimal.Round(Convert.ToDecimal(cobranca.valor), 2) * 100).ToString("{0}"));
            xmlTransmissao += retornaEntrada("TITULO.PC-MULTA", "");
            xmlTransmissao += retornaEntrada("TITULO.QT-DIAS-MULTA", "");
            xmlTransmissao += retornaEntrada("TITULO.PC-JURO", "");
            xmlTransmissao += retornaEntrada("TITULO.TP-DESC", "0");
            xmlTransmissao += retornaEntrada("TITULO.VL-DESC", "0");
            xmlTransmissao += retornaEntrada("TITULO.DT-LIMI-DESC", vencimento.ToString("DDMMYYYY"));
            xmlTransmissao += retornaEntrada("TITULO.VL-ABATIMENTO", "0");
            xmlTransmissao += retornaEntrada("TITULO.TP-PROTESTO", "0");
            xmlTransmissao += retornaEntrada("TITULO.QT-DIAS-PROTESTO", "0");
            xmlTransmissao += retornaEntrada("TITULO.QT-DIAS-BAIXA", "5");
            xmlTransmissao += retornaEntrada("MENSAGEM", conta.instrucoes);

            xmlTransmissao += "</dados>";
            xmlTransmissao += "<expiracao>100</expiracao>";
            xmlTransmissao += "<sistema>YMB</sistema>";
            xmlTransmissao += "</TicketRequest>";

            /*System.Net.ServicePointManager.ServerCertificateValidationCallback = ((sender, certificate, chain, sslPolicyErrors) => true);
            string file = @"C:\certificados\lgf.pfx";
            X509Certificate Cert = new X509Certificate();
            Cert.Import(file, "123", X509KeyStorageFlags.PersistKeySet);
            XmlDocument soapEnvelopeXml = new XmlDocument();
            soapEnvelopeXml.LoadXml(xmlTransmissao);*/


            var retorno = postXMLData("https://ymbdlb.santander.com.br/dl-ticket-services/TicketEndpointService", xmlTransmissao);


        }

        private static string retornaEntrada(string key, string value)
        {
            return "<entry><key>" + key + "</key><value>" + value + "</value></entry>";
        }

        public static string postXMLData(string destinationUrl, string requestXml)
        {
            System.Net.ServicePointManager.ServerCertificateValidationCallback = ((sender, certificate, chain, sslPolicyErrors) => true);
            string file = @"C:\certificados\sslgrao.pfx";
            //setting the request

            string soapStr =
            @"<?xml version=""1.0"" encoding=""utf-8""?>
            <soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/""
            xmlns:impl=""http://impl.webservice.dl.app.bsbr.altec.com/""><soapenv:Header/>
              <soapenv:Body><impl:create>" + requestXml + "</impl:create></soapenv:Body></soapenv:Envelope>";

            //HttpWebRequest req = (HttpWebRequest)WebRequest.Create(Url);
            


            HttpWebRequest req;
            req = (HttpWebRequest)HttpWebRequest.Create(destinationUrl);
            req.ContentType = "text/xml;charset=\"utf-8\"";
            req.Accept = "text/xml";
            req.Method = "POST";
            req.ClientCertificates.Add(new System.Security.Cryptography.X509Certificates.X509Certificate(file, "123"));

            string retorno = "";
            //setting the request content
            byte[] byteArray = Encoding.UTF8.GetBytes(soapStr);
            Stream dataStream = req.GetRequestStream();
            dataStream.Write(byteArray, 0, byteArray.Length);
            dataStream.Close();

            using (HttpWebResponse webResponse = (HttpWebResponse)req.GetResponse())
            {
                using (Stream responseStream = webResponse.GetResponseStream())
                {
                    using (StreamReader responseStreamReader = new StreamReader(responseStream, true))
                    {
                        retorno = responseStreamReader.ReadToEnd();
                        responseStreamReader.Close();
                    }

                    responseStream.Close();
                }
                webResponse.Close();
            }

            return retorno;
        }
    }
}
