﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bcServerService
{
    public static class rnProceda
    {
        public static void LerArquivo_OCOREN_tnt(string transportadora)
        {
            try
            {
                List<string> mensagemLinha = new List<string>();
                string mensagem = "";
                string identificadorDeRegistro, cnpjEmpresaEmissora, nfeSerie, nfeNumero, ocorrenciaDaEntrega, dataHoraOcorrencia, codObsOcorrencia;
                string dataMesAno, hora;
                string caminhoArquivoProcessado = @"c:\editnt_processados\";
                const string formatData = "ddMMyyyy";

                DirectoryInfo dir = new DirectoryInfo(@"C:\editnt\");
                // Busca automaticamente todos os arquivos em todos os subdiretórios
                FileInfo[] files = dir.GetFiles("*.txt", SearchOption.AllDirectories);

                using (var data = new dbSiteEntities())
                {
                    foreach (FileInfo file in files)
                    {
                        if (file.Name.Contains("_OCO_"))
                        {
                            using (StreamReader texto = new StreamReader(file.FullName))
                            {
                                int index = 0;
                                while ((mensagem = texto.ReadLine()) != null)
                                {
                                    index++;
                                    if (index < 4) continue;

                                    mensagemLinha.Add(mensagem);

                                    identificadorDeRegistro = mensagem.Substring(0, 3);
                                    cnpjEmpresaEmissora = mensagem.Substring(3, 14);
                                    nfeSerie = mensagem.Substring(17, 3);
                                    nfeNumero = mensagem.Substring(20, 8);
                                    ocorrenciaDaEntrega = mensagem.Substring(28, 2);
                                    dataMesAno = mensagem.Substring(30, 8);
                                    hora = mensagem.Substring(38, 4);
                                    int numeroNota = 0;
                                    int.TryParse(nfeNumero, out numeroNota);

                                    dataHoraOcorrencia = DateTime.ParseExact(dataMesAno, formatData, CultureInfo.InvariantCulture).ToString("d") + " " + hora.Substring(0, 2) + ":" + hora.Substring(2, 2);
                                    codObsOcorrencia = mensagem.Substring(42, 2);

                                    //#region Atualiza registro no banco de dados
                                    var nota =
                                        (from c in data.tbNotaFiscal
                                            where
                                                c.tbEmpresa.cnpj.Contains(cnpjEmpresaEmissora) &&
                                                c.numeroNota == numeroNota && c.idPedidoEnvio > 0
                                            select c).FirstOrDefault();
                                    if (nota != null)
                                    {
                                        var envio = (from c in data.tbPedidoEnvio
                                            join d in data.tbPedidoPacote on c.idPedidoEnvio equals d.idPedidoEnvio
                                                     where
                                                         c.idPedidoEnvio == nota.idPedidoEnvio
                                                     orderby c.idPedidoEnvio descending
                                            orderby c.idPedidoEnvio descending
                                            select new
                                            {
                                                c.idPedidoEnvio,
                                                c.idPedido,
                                                d.rastreio,
                                                statusAtual = (d.statusAtual ?? "")
                                            }).FirstOrDefault();

                                        if (envio != null)
                                        {
                                            string statusAtual = "";
                                            try
                                            {
                                                statusAtual = Enum.GetName(typeof(CodigosDeOcorrenciaOcoren), Convert.ToInt32(ocorrenciaDaEntrega));
                                            }
                                            catch (Exception)
                                            {
                                                //codigo de ocorrencia não cadastrado
                                                rnEmails.EnviaEmail("", "andre@bark.com.br,renato@bark.com.br", "", "", "", "codigo de ocorrencia OCOREN numero " + ocorrenciaDaEntrega + " não cadastrado", "Falha Atualizar Status de Entrega OCOREN");
                                            }

                                            if (statusAtual != null && (statusAtual.ToLower() != envio.statusAtual.ToLower()))
                                            {
                                                var lastEvent = DateTime.Now.AddYears(-5);
                                                string descricao = "";
                                                string observacao = "";
                                                string dataHora = dataHoraOcorrencia;

                                                try
                                                {

                                                    var dataHoraEvento = Convert.ToDateTime(dataHora);

                                                    if (dataHoraEvento > lastEvent)
                                                    {
                                                        lastEvent = dataHoraEvento;
                                                        descricao = "";
                                                        observacao = codObsOcorrencia;
                                                    }
                                                }
                                                catch (Exception)
                                                {

                                                }

                                                var pedidoPacotes = (from c in data.tbPedidoPacote where c.idPedidoEnvio == envio.idPedidoEnvio select c);
                                                foreach (var pacote in pedidoPacotes)
                                                {
                                                    pacote.statusAtual = statusAtual;
                                                    pacote.dataStatusAtual = DateTime.Now;
                                                    pacote.dataStatusAtualTransportadora = DateTime.Now;
                                                    if (Convert.ToInt32(ocorrenciaDaEntrega) == 1)
                                                    {
                                                        pacote.rastreamentoConcluido = true;
                                                        pacote.dataEntrega = DateTime.Now;
                                                        pacote.dataEntregaTransportadora = lastEvent;

                                                        if (pacote.dataEntrega.Value.Date.AddDays(-5) > pacote.dataEntregaTransportadora)
                                                            pacote.dataEntrega = pacote.dataEntregaTransportadora;
                                                    }
                                                }

                                                var status = new tbPedidoEnvioStatusTransporte
                                                {
                                                    idPedidoEnvio = envio.idPedidoEnvio,
                                                    dataStatus = DateTime.Now,
                                                    dataStatusTransportadora = lastEvent,
                                                    status = statusAtual + " - " + descricao + " - " + observacao,
                                                    observacao = ""
                                                };
                                                data.tbPedidoEnvioStatusTransporte.Add(status);
                                                data.SaveChanges();
                                            }

                                        }

                                    }
                                    //#endregion Atualiza registro no banco de dados

                                    //Response.Write((Enum.GetName(typeof(CodigosDeOcorrenciaOcoren), Convert.ToInt32(ocorrenciaDaEntrega)) ?? ocorrenciaDaEntrega) + " pedidoId: " + (envio != null ? envio.idPedido : 0) + "</br>");
                                }
                            }

                            File.Copy(file.FullName, caminhoArquivoProcessado + file.Name, true);
                            File.Delete(file.FullName);
                        }
                        else
                        {
                            /*File.Copy(file.FullName, caminhoArquivoProcessado + file.Name, true);
                            File.Delete(file.FullName);*/
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                rnEmails.EnviaEmail("", "andre@bark.com.br", "", "", "", ex.ToString() + "<br>" + ex.InnerException, "Erro Proceda OCO");
            }
        }

        public static void LerArquivo_DocCob_tnt(string transportadora)
        {
            string arquivo = "";
            int linhaAtual = 0;
            try
            {
                List<string> mensagemLinha = new List<string>();
                string mensagem = "";
                string identificadorDeRegistro, cnpjEmpresaEmissora, nfeSerie, nfeNumero, ocorrenciaDaEntrega, dataHoraOcorrencia, codObsOcorrencia;
                string dataMesAno, hora;
                string caminhoArquivoProcessado = @"c:\editnt_processados\";
                const string formatData = "ddMMyyyy";

                DirectoryInfo dir = new DirectoryInfo(@"C:\editnt\");
                // Busca automaticamente todos os arquivos em todos os subdiretórios
                FileInfo[] files = dir.GetFiles("*.txt", SearchOption.AllDirectories);

                string numeroDocumentoCobranca = "";
                string dataEmissaoString = "";
                string dataVencimentoString = "";
                decimal valorDocumentoCobranca = 0;
                DateTime? dataEmissao = null;
                DateTime? dataVencimento = null;


                string serieConhecimento = "";
                string numeroConhecimento = "";
                decimal valorFrete = 0;
                bool processarLinha = false;
                string serieNota = "";
                string nota = "";
                string dataEmissaoNotaString = "";
                DateTime? dataEmissaoNota = null;
                decimal peso = 0;
                decimal valorNota = 0;
                string cnpjEmissor = "";

                int idTransportadoraFatura = 0;

                using (var data = new dbSiteEntities())
                {
                    foreach (FileInfo file in files)
                    {
                        linhaAtual = 0;
                        arquivo = file.Name;
                        if (file.Name.Contains("_FAT_"))
                        {
                            using (StreamReader texto = new StreamReader(file.FullName))
                            {
                                int index = 0;
                                while ((mensagem = texto.ReadLine()) != null)
                                {
                                    linhaAtual++;
                                    index++;
                                    mensagemLinha.Add(mensagem);

                                    string identificadorRegistro = mensagem.Substring(0, 3);

                                    if(identificadorRegistro == "352")
                                    {
                                        numeroDocumentoCobranca = mensagem.Substring(17, 10);
                                        dataEmissaoString = mensagem.Substring(27, 8);
                                        dataVencimentoString = mensagem.Substring(35, 8);

                                        dataEmissao = Convert.ToDateTime(dataEmissaoString.Substring(0,2) + "/" + dataEmissaoString.Substring(2, 2) + "/" + dataEmissaoString.Substring(4, 4));
                                        dataVencimento = Convert.ToDateTime(dataVencimentoString.Substring(0, 2) + "/" + dataVencimentoString.Substring(2, 2) + "/" + dataVencimentoString.Substring(4, 4));

                                        valorDocumentoCobranca = Convert.ToDecimal(mensagem.Substring(43, 15)) / 100;

                                        var fatura = new tbTransportadoraFatura();
                                        fatura.idTransportadora = 1;
                                        fatura.transportadora = "tnt";
                                        fatura.dataEmissao = dataEmissao;
                                        fatura.dataVencimento = dataVencimento;
                                        fatura.valorTotal = valorDocumentoCobranca;
                                        fatura.codigoFatura = numeroDocumentoCobranca;

                                        data.tbTransportadoraFatura.Add(fatura);
                                        data.SaveChanges();
                                        idTransportadoraFatura = fatura.idTransportadoraFatura;
                                    }
                                    


                                    if (identificadorRegistro == "353")
                                    {
                                        serieConhecimento = "";
                                        numeroConhecimento = "";
                                        valorFrete = 0;
                                        processarLinha = false;
                                        serieNota = "";
                                        nota = "";
                                        dataEmissaoNotaString = "";
                                        peso = 0;
                                        valorNota = 0;
                                        cnpjEmissor = "";


                                        serieConhecimento = mensagem.Substring(13, 5);
                                        numeroConhecimento = mensagem.Substring(18, 12);
                                        valorFrete = Convert.ToDecimal(mensagem.Substring(30, 15)) / 100;
                                        processarLinha = true;
                                    }

                                    if (identificadorRegistro == "354")
                                    {
                                        serieNota = mensagem.Substring(3, 3);
                                        nota = mensagem.Substring(6, 8);
                                        dataEmissaoNotaString = mensagem.Substring(14, 8);
                                        dataEmissaoNota = Convert.ToDateTime(dataEmissaoNotaString.Substring(0, 2) + "/" + dataEmissaoNotaString.Substring(2, 2) + "/" + dataEmissaoNotaString.Substring(4, 4));
                                        peso = Convert.ToDecimal(mensagem.Substring(22, 7)) / 100;
                                        valorNota = Convert.ToDecimal(mensagem.Substring(29, 15)) / 100;
                                        cnpjEmissor = mensagem.Substring(44, 14);

                                        bool linhaProcessada = false;

                                        int numeroNota = 0;
                                        int.TryParse(nota, out numeroNota);

                                        int idPedidoEnvio = 0;
                                        int idPedido = 0;

                                        var faturaLinha = new tbTransportadoraFaturaRegistro()
                                        {
                                            idTransportadoraFatura = idTransportadoraFatura,
                                            serieConhecimento = serieConhecimento,
                                            numeroConhecimento = numeroConhecimento,
                                            cnpjEmissor = cnpjEmissor,
                                            dataEmissaoNota = dataEmissaoNota,
                                            peso = peso,
                                            numeroNota = numeroNota,
                                            valorFrete = valorFrete,
                                            serieNota = serieNota,
                                            valorNota = valorNota

                                        };

                                        var empresa = (from c in data.tbEmpresa where c.cnpj == cnpjEmissor select c).FirstOrDefault();
                                        if (empresa != null)
                                        {
                                            faturaLinha.idEmpresa = empresa.idEmpresa;
                                            if (numeroNota > 0)
                                            {
                                                var notaEnvio = (from c in data.tbNotaFiscal where c.numeroNota == numeroNota && c.idCNPJ == empresa.idEmpresa select c).FirstOrDefault();
                                                if (notaEnvio != null)
                                                {
                                                    var envio = (from c in data.tbPedidoEnvio where c.idPedidoEnvio == notaEnvio.idPedidoEnvio select c).FirstOrDefault();
                                                    if (envio != null)
                                                    {
                                                        idPedidoEnvio = envio.idPedidoEnvio;
                                                        envio.pesoAferidoTransportadora = peso;
                                                        envio.valorCobradoTransportadora = valorFrete;
                                                        envio.numeroFaturaTransportadora = numeroDocumentoCobranca;
                                                        envio.dataEmissaoFaturaTransportadora = dataEmissao;
                                                        envio.dataVencimentoFaturaTransportadora = dataVencimento;
                                                        linhaProcessada = true;
                                                        data.SaveChanges();

                                                    }

                                                    if (notaEnvio.idPedidoEnvio > 0) faturaLinha.idPedidoEnvio = notaEnvio.idPedidoEnvio;
                                                    if (notaEnvio.idPedido > 0) faturaLinha.idPedido = notaEnvio.idPedido;
                                                }
                                            }
                                        }

                                        data.tbTransportadoraFaturaRegistro.Add(faturaLinha);
                                        data.SaveChanges();

                                    }
                                    

                                }
                            }

                            File.Copy(file.FullName, caminhoArquivoProcessado + file.Name, true);
                            File.Delete(file.FullName);
                        }
                        else
                        {
                            /*File.Copy(file.FullName, caminhoArquivoProcessado + file.Name, true);
                            File.Delete(file.FullName);*/
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                rnEmails.EnviaEmail("", "andre@bark.com.br", "", "", "", arquivo + " - Linha:" + linhaAtual + "<br>" + ex.ToString() + "<br>" + ex.InnerException, "Erro Proceda FAT");
            }
        }

        public static void LerArquivo_OCOREN_nowlog(string transportadora)
        {
            try
            {
                List<string> mensagemLinha = new List<string>();
                string mensagem = "";
                string identificadorDeRegistro, cnpjEmpresaEmissora, nfeSerie, nfeNumero, ocorrenciaDaEntrega, dataHoraOcorrencia, codObsOcorrencia;
                string dataMesAno, hora;
                string caminhoArquivoProcessado = @"c:\edinowlog_processados\";
                const string formatData = "ddMMyyyy";

                DirectoryInfo dir = new DirectoryInfo(@"C:\edinowlog\");
                // Busca automaticamente todos os arquivos em todos os subdiretórios
                FileInfo[] files = dir.GetFiles("*.txt", SearchOption.AllDirectories);

                using (var data = new dbSiteEntities())
                {
                    foreach (FileInfo file in files)
                    {
                        if (file.Name.Contains("OCORREN_"))
                        {
                            using (StreamReader texto = new StreamReader(file.FullName))
                            {
                                int index = 0;
                                while ((mensagem = texto.ReadLine()) != null)
                                {
                                    index++;
                                    if (index < 4) continue;

                                    mensagemLinha.Add(mensagem);

                                    identificadorDeRegistro = mensagem.Substring(0, 3);
                                    cnpjEmpresaEmissora = mensagem.Substring(3, 14);
                                    nfeSerie = mensagem.Substring(17, 3);
                                    nfeNumero = mensagem.Substring(20, 8);
                                    ocorrenciaDaEntrega = mensagem.Substring(28, 2);
                                    dataMesAno = mensagem.Substring(30, 8);
                                    hora = mensagem.Substring(38, 4);
                                    int numeroNota = 0;
                                    int.TryParse(nfeNumero, out numeroNota);

                                    dataHoraOcorrencia = DateTime.ParseExact(dataMesAno, formatData, CultureInfo.InvariantCulture).ToString("d") + " " + hora.Substring(0, 2) + ":" + hora.Substring(2, 2);
                                    codObsOcorrencia = mensagem.Substring(42, 2);

                                    //#region Atualiza registro no banco de dados
                                    var nota =
                                        (from c in data.tbNotaFiscal
                                         where
                                             c.tbEmpresa.cnpj.Contains(cnpjEmpresaEmissora) &&
                                             c.numeroNota == numeroNota && c.idPedidoEnvio > 0
                                         select c).FirstOrDefault();
                                    if (nota != null)
                                    {
                                        var envio = (from c in data.tbPedidoEnvio
                                                     join d in data.tbPedidoPacote on c.idPedidoEnvio equals d.idPedidoEnvio
                                                     where
                                                         c.idPedidoEnvio == nota.idPedidoEnvio
                                                     orderby c.idPedidoEnvio descending
                                                     orderby c.idPedidoEnvio descending
                                                     select new
                                                     {
                                                         c.idPedidoEnvio,
                                                         c.idPedido,
                                                         d.rastreio,
                                                         statusAtual = (d.statusAtual ?? "")
                                                     }).FirstOrDefault();

                                        if (envio != null)
                                        {
                                            string statusAtual = "";
                                            try
                                            {
                                                statusAtual = Enum.GetName(typeof(CodigosDeOcorrenciaOcoren), Convert.ToInt32(ocorrenciaDaEntrega));
                                            }
                                            catch (Exception)
                                            {
                                                //codigo de ocorrencia não cadastrado
                                                rnEmails.EnviaEmail("", "andre@bark.com.br", "", "", "", "codigo de ocorrencia OCOREN numero " + ocorrenciaDaEntrega + " não cadastrado", "Falha Atualizar Status de Entrega OCOREN");
                                            }

                                            if (statusAtual != null && (statusAtual.ToLower() != envio.statusAtual.ToLower()))
                                            {
                                                var lastEvent = DateTime.Now.AddYears(-5);
                                                string descricao = "";
                                                string observacao = "";
                                                string dataHora = dataHoraOcorrencia;

                                                try
                                                {

                                                    var dataHoraEvento = Convert.ToDateTime(dataHora);

                                                    if (dataHoraEvento > lastEvent)
                                                    {
                                                        lastEvent = dataHoraEvento;
                                                        descricao = "";
                                                        observacao = codObsOcorrencia;
                                                    }
                                                }
                                                catch (Exception)
                                                {

                                                }

                                                var pedidoPacotes = (from c in data.tbPedidoPacote where c.idPedidoEnvio == envio.idPedidoEnvio select c);
                                                foreach (var pacote in pedidoPacotes)
                                                {
                                                    pacote.statusAtual = statusAtual;
                                                    pacote.dataStatusAtual = DateTime.Now;
                                                    pacote.dataStatusAtualTransportadora = DateTime.Now;
                                                    if (Convert.ToInt32(ocorrenciaDaEntrega) == 1)
                                                    {
                                                        pacote.rastreamentoConcluido = true;
                                                        pacote.dataEntrega = DateTime.Now;
                                                        pacote.dataEntregaTransportadora = lastEvent;

                                                        if (pacote.dataEntrega.Value.Date.AddDays(-5) > pacote.dataEntregaTransportadora)
                                                            pacote.dataEntrega = pacote.dataEntregaTransportadora;
                                                    }
                                                }

                                                var status = new tbPedidoEnvioStatusTransporte
                                                {
                                                    idPedidoEnvio = envio.idPedidoEnvio,
                                                    dataStatus = DateTime.Now,
                                                    dataStatusTransportadora = lastEvent,
                                                    status = statusAtual + " - " + descricao + " - " + observacao,
                                                    observacao = ""
                                                };
                                                data.tbPedidoEnvioStatusTransporte.Add(status);
                                                data.SaveChanges();
                                            }

                                        }

                                    }
                                    //#endregion Atualiza registro no banco de dados

                                    //Response.Write((Enum.GetName(typeof(CodigosDeOcorrenciaOcoren), Convert.ToInt32(ocorrenciaDaEntrega)) ?? ocorrenciaDaEntrega) + " pedidoId: " + (envio != null ? envio.idPedido : 0) + "</br>");
                                }
                            }

                            File.Copy(file.FullName, caminhoArquivoProcessado + file.Name, true);
                            File.Delete(file.FullName);
                        }
                        else
                        {
                            /*File.Copy(file.FullName, caminhoArquivoProcessado + file.Name, true);
                            File.Delete(file.FullName);*/
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                rnEmails.EnviaEmail("", "andre@bark.com.br", "", "", "", ex.ToString() + "<br>" + ex.InnerException, "Erro Proceda OCO");
            }
        }

        public static void LerArquivo_OCOREN_plimor(string transportadora)
        {
            try
            {
                List<string> mensagemLinha = new List<string>();
                string mensagem = "";
                string identificadorDeRegistro, cnpjEmpresaEmissora, nfeSerie, nfeNumero, ocorrenciaDaEntrega, dataHoraOcorrencia, codObsOcorrencia;
                string dataMesAno, hora;
                string caminhoArquivoProcessado = @"c:\ediplimor_processados\";
                const string formatData = "ddMMyyyy";

                DirectoryInfo dir = new DirectoryInfo(@"C:\ediplimor\");
                // Busca automaticamente todos os arquivos em todos os subdiretórios
                FileInfo[] files = dir.GetFiles("*.txt", SearchOption.AllDirectories);

                using (var data = new dbSiteEntities())
                {
                    foreach (FileInfo file in files)
                    {
                        if (file.Name.Contains("_OCO_"))
                        {
                            using (StreamReader texto = new StreamReader(file.FullName))
                            {
                                int index = 0;
                                while ((mensagem = texto.ReadLine()) != null)
                                {
                                    index++;
                                    if (index < 4) continue;

                                    mensagemLinha.Add(mensagem);

                                    identificadorDeRegistro = mensagem.Substring(0, 3);
                                    cnpjEmpresaEmissora = mensagem.Substring(3, 14);
                                    nfeSerie = mensagem.Substring(17, 3);
                                    nfeNumero = mensagem.Substring(20, 8);
                                    ocorrenciaDaEntrega = mensagem.Substring(28, 2);
                                    dataMesAno = mensagem.Substring(30, 8);
                                    hora = mensagem.Substring(38, 4);
                                    int numeroNota = 0;
                                    int.TryParse(nfeNumero, out numeroNota);

                                    dataHoraOcorrencia = DateTime.ParseExact(dataMesAno, formatData, CultureInfo.InvariantCulture).ToString("d") + " " + hora.Substring(0, 2) + ":" + hora.Substring(2, 2);
                                    codObsOcorrencia = mensagem.Substring(42, 2);

                                    //#region Atualiza registro no banco de dados
                                    var nota =
                                        (from c in data.tbNotaFiscal
                                         where
                                             c.tbEmpresa.cnpj.Contains(cnpjEmpresaEmissora) &&
                                             c.numeroNota == numeroNota && c.idPedidoEnvio > 0
                                         select c).FirstOrDefault();
                                    if (nota != null)
                                    {
                                        var envio = (from c in data.tbPedidoEnvio
                                                     join d in data.tbPedidoPacote on c.idPedidoEnvio equals d.idPedidoEnvio
                                                     where
                                                         c.idPedidoEnvio == nota.idPedidoEnvio
                                                     orderby c.idPedidoEnvio descending
                                                     orderby c.idPedidoEnvio descending
                                                     select new
                                                     {
                                                         c.idPedidoEnvio,
                                                         c.idPedido,
                                                         d.rastreio,
                                                         statusAtual = (d.statusAtual ?? "")
                                                     }).FirstOrDefault();

                                        if (envio != null)
                                        {
                                            string statusAtual = "";
                                            try
                                            {
                                                statusAtual = Enum.GetName(typeof(CodigosDeOcorrenciaOcoren), Convert.ToInt32(ocorrenciaDaEntrega));
                                            }
                                            catch (Exception)
                                            {
                                                //codigo de ocorrencia não cadastrado
                                                rnEmails.EnviaEmail("", "andre@bark.com.br", "", "", "", "codigo de ocorrencia OCOREN numero " + ocorrenciaDaEntrega + " não cadastrado", "Falha Atualizar Status de Entrega OCOREN");
                                            }

                                            if (statusAtual != null && (statusAtual.ToLower() != envio.statusAtual.ToLower()))
                                            {
                                                var lastEvent = DateTime.Now.AddYears(-5);
                                                string descricao = "";
                                                string observacao = "";
                                                string dataHora = dataHoraOcorrencia;

                                                try
                                                {

                                                    var dataHoraEvento = Convert.ToDateTime(dataHora);

                                                    if (dataHoraEvento > lastEvent)
                                                    {
                                                        lastEvent = dataHoraEvento;
                                                        descricao = "";
                                                        observacao = codObsOcorrencia;
                                                    }
                                                }
                                                catch (Exception)
                                                {

                                                }

                                                var pedidoPacotes = (from c in data.tbPedidoPacote where c.idPedidoEnvio == envio.idPedidoEnvio select c);
                                                foreach (var pacote in pedidoPacotes)
                                                {
                                                    pacote.statusAtual = statusAtual;
                                                    pacote.dataStatusAtual = DateTime.Now;
                                                    pacote.dataStatusAtualTransportadora = DateTime.Now;
                                                    if (Convert.ToInt32(ocorrenciaDaEntrega) == 1)
                                                    {
                                                        pacote.rastreamentoConcluido = true;
                                                        pacote.dataEntrega = DateTime.Now;
                                                        pacote.dataEntregaTransportadora = lastEvent;

                                                        if (pacote.dataEntrega.Value.Date.AddDays(-5) > pacote.dataEntregaTransportadora)
                                                            pacote.dataEntrega = pacote.dataEntregaTransportadora;
                                                    }
                                                }

                                                var status = new tbPedidoEnvioStatusTransporte
                                                {
                                                    idPedidoEnvio = envio.idPedidoEnvio,
                                                    dataStatus = DateTime.Now,
                                                    dataStatusTransportadora = lastEvent,
                                                    status = statusAtual + " - " + descricao + " - " + observacao,
                                                    observacao = ""
                                                };
                                                data.tbPedidoEnvioStatusTransporte.Add(status);
                                                data.SaveChanges();
                                            }

                                        }

                                    }
                                    //#endregion Atualiza registro no banco de dados

                                    //Response.Write((Enum.GetName(typeof(CodigosDeOcorrenciaOcoren), Convert.ToInt32(ocorrenciaDaEntrega)) ?? ocorrenciaDaEntrega) + " pedidoId: " + (envio != null ? envio.idPedido : 0) + "</br>");
                                }
                            }

                            File.Copy(file.FullName, caminhoArquivoProcessado + file.Name, true);
                            File.Delete(file.FullName);
                        }
                        else
                        {
                            /*File.Copy(file.FullName, caminhoArquivoProcessado + file.Name, true);
                            File.Delete(file.FullName);*/
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                rnEmails.EnviaEmail("", "andre@bark.com.br", "", "", "", ex.ToString() + "<br>" + ex.InnerException, "Erro Proceda OCO");
            }
        }

        public static void LerArquivo_DocCob_plimor(string transportadora)
        {
            string arquivo = "";
            int linhaAtual = 0;
            try
            {
                List<string> mensagemLinha = new List<string>();
                string mensagem = "";
                string identificadorDeRegistro, cnpjEmpresaEmissora, nfeSerie, nfeNumero, ocorrenciaDaEntrega, dataHoraOcorrencia, codObsOcorrencia;
                string dataMesAno, hora;
                string caminhoArquivoProcessado = @"c:\ediplimor_processados\";
                const string formatData = "ddMMyyyy";

                DirectoryInfo dir = new DirectoryInfo(@"C:\ediplimor\");
                // Busca automaticamente todos os arquivos em todos os subdiretórios
                FileInfo[] files = dir.GetFiles("*.txt", SearchOption.AllDirectories);

                string numeroDocumentoCobranca = "";
                string dataEmissaoString = "";
                string dataVencimentoString = "";
                decimal valorDocumentoCobranca = 0;
                DateTime? dataEmissao = null;
                DateTime? dataVencimento = null;


                string serieConhecimento = "";
                string numeroConhecimento = "";
                decimal valorFrete = 0;
                bool processarLinha = false;
                string serieNota = "";
                string nota = "";
                string dataEmissaoNotaString = "";
                DateTime? dataEmissaoNota = null;
                decimal peso = 0;
                decimal valorNota = 0;
                string cnpjEmissor = "";

                int idTransportadoraFatura = 0;

                using (var data = new dbSiteEntities())
                {
                    foreach (FileInfo file in files)
                    {
                        linhaAtual = 0;
                        arquivo = file.Name;
                        if (file.Name.Contains("_FAT_"))
                        {
                            using (StreamReader texto = new StreamReader(file.FullName))
                            {
                                int index = 0;
                                while ((mensagem = texto.ReadLine()) != null)
                                {
                                    linhaAtual++;
                                    index++;
                                    mensagemLinha.Add(mensagem);

                                    string identificadorRegistro = mensagem.Substring(0, 3);

                                    if (identificadorRegistro == "352")
                                    {
                                        numeroDocumentoCobranca = mensagem.Substring(17, 10);
                                        dataEmissaoString = mensagem.Substring(27, 8);
                                        dataVencimentoString = mensagem.Substring(35, 8);

                                        dataEmissao = Convert.ToDateTime(dataEmissaoString.Substring(0, 2) + "/" + dataEmissaoString.Substring(2, 2) + "/" + dataEmissaoString.Substring(4, 4));
                                        dataVencimento = Convert.ToDateTime(dataVencimentoString.Substring(0, 2) + "/" + dataVencimentoString.Substring(2, 2) + "/" + dataVencimentoString.Substring(4, 4));

                                        valorDocumentoCobranca = Convert.ToDecimal(mensagem.Substring(43, 15)) / 100;

                                        var fatura = new tbTransportadoraFatura();
                                        fatura.idTransportadora = 4;
                                        fatura.transportadora = "plimor";
                                        fatura.dataEmissao = dataEmissao;
                                        fatura.dataVencimento = dataVencimento;
                                        fatura.valorTotal = valorDocumentoCobranca;
                                        fatura.codigoFatura = numeroDocumentoCobranca;

                                        data.tbTransportadoraFatura.Add(fatura);
                                        data.SaveChanges();
                                        idTransportadoraFatura = fatura.idTransportadoraFatura;
                                    }



                                    if (identificadorRegistro == "353")
                                    {
                                        serieConhecimento = "";
                                        numeroConhecimento = "";
                                        valorFrete = 0;
                                        processarLinha = false;
                                        serieNota = "";
                                        nota = "";
                                        dataEmissaoNotaString = "";
                                        peso = 0;
                                        valorNota = 0;
                                        cnpjEmissor = "";


                                        serieConhecimento = mensagem.Substring(13, 5);
                                        numeroConhecimento = mensagem.Substring(18, 12);
                                        valorFrete = Convert.ToDecimal(mensagem.Substring(30, 15)) / 100;
                                        processarLinha = true;
                                    }

                                    if (identificadorRegistro == "354")
                                    {
                                        serieNota = mensagem.Substring(3, 3);
                                        nota = mensagem.Substring(6, 8);
                                        dataEmissaoNotaString = mensagem.Substring(14, 8);
                                        dataEmissaoNota = Convert.ToDateTime(dataEmissaoNotaString.Substring(0, 2) + "/" + dataEmissaoNotaString.Substring(2, 2) + "/" + dataEmissaoNotaString.Substring(4, 4));
                                        peso = Convert.ToDecimal(mensagem.Substring(22, 7)) / 100;
                                        valorNota = Convert.ToDecimal(mensagem.Substring(29, 15)) / 100;
                                        cnpjEmissor = mensagem.Substring(44, 14);

                                        bool linhaProcessada = false;

                                        int numeroNota = 0;
                                        int.TryParse(nota, out numeroNota);

                                        int idPedidoEnvio = 0;
                                        int idPedido = 0;

                                        var faturaLinha = new tbTransportadoraFaturaRegistro()
                                        {
                                            idTransportadoraFatura = idTransportadoraFatura,
                                            serieConhecimento = serieConhecimento,
                                            numeroConhecimento = numeroConhecimento,
                                            cnpjEmissor = cnpjEmissor,
                                            dataEmissaoNota = dataEmissaoNota,
                                            peso = peso,
                                            numeroNota = numeroNota,
                                            valorFrete = valorFrete,
                                            serieNota = serieNota,
                                            valorNota = valorNota

                                        };

                                        var empresa = (from c in data.tbEmpresa where c.cnpj == cnpjEmissor select c).FirstOrDefault();
                                        if (empresa != null)
                                        {
                                            faturaLinha.idEmpresa = empresa.idEmpresa;
                                            if (numeroNota > 0)
                                            {
                                                var notaEnvio = (from c in data.tbNotaFiscal where c.numeroNota == numeroNota && c.idCNPJ == empresa.idEmpresa select c).FirstOrDefault();
                                                if (notaEnvio != null)
                                                {
                                                    var envio = (from c in data.tbPedidoEnvio where c.idPedidoEnvio == notaEnvio.idPedidoEnvio select c).FirstOrDefault();
                                                    if (envio != null)
                                                    {
                                                        idPedidoEnvio = envio.idPedidoEnvio;
                                                        envio.pesoAferidoTransportadora = peso;
                                                        envio.valorCobradoTransportadora = valorFrete;
                                                        envio.numeroFaturaTransportadora = numeroDocumentoCobranca;
                                                        envio.dataEmissaoFaturaTransportadora = dataEmissao;
                                                        envio.dataVencimentoFaturaTransportadora = dataVencimento;
                                                        linhaProcessada = true;
                                                        data.SaveChanges();

                                                    }

                                                    if (notaEnvio.idPedidoEnvio > 0) faturaLinha.idPedidoEnvio = notaEnvio.idPedidoEnvio;
                                                    if (notaEnvio.idPedido > 0) faturaLinha.idPedido = notaEnvio.idPedido;
                                                }
                                            }
                                        }

                                        data.tbTransportadoraFaturaRegistro.Add(faturaLinha);
                                        data.SaveChanges();

                                    }


                                }
                            }

                            File.Copy(file.FullName, caminhoArquivoProcessado + file.Name, true);
                            File.Delete(file.FullName);
                        }
                        else
                        {
                            /*File.Copy(file.FullName, caminhoArquivoProcessado + file.Name, true);
                            File.Delete(file.FullName);*/
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                rnEmails.EnviaEmail("", "andre@bark.com.br", "", "", "", arquivo + " - Linha:" + linhaAtual + "<br>" + ex.ToString() + "<br>" + ex.InnerException, "Erro Proceda FAT");
            }
        }




        public static void LerArquivo_OCOREN(string transportadora, string padraoarquivo)
        {
            try
            {
                List<string> mensagemLinha = new List<string>();
                string mensagem = "";
                string identificadorDeRegistro, cnpjEmpresaEmissora, nfeSerie, nfeNumero, ocorrenciaDaEntrega, dataHoraOcorrencia, codObsOcorrencia;
                string dataMesAno, hora;
                string caminhoArquivoProcessado = @"c:\edi\" + transportadora + @"_processados\";
                const string formatData = "ddMMyyyy";

                DirectoryInfo dir = new DirectoryInfo(@"C:\edi\" + transportadora + @"\");
                // Busca automaticamente todos os arquivos em todos os subdiretórios
                FileInfo[] files = dir.GetFiles("*.txt", SearchOption.AllDirectories);

                using (var data = new dbSiteEntities())
                {
                    foreach (FileInfo file in files)
                    {
                        if (file.Name.ToLower().Contains(padraoarquivo.ToLower()))
                        {
                            using (StreamReader texto = new StreamReader(file.FullName))
                            {
                                int index = 0;
                                while ((mensagem = texto.ReadLine()) != null)
                                {
                                    index++;
                                    if (index < 4) continue;

                                    mensagemLinha.Add(mensagem);

                                    identificadorDeRegistro = mensagem.Substring(0, 3);
                                    cnpjEmpresaEmissora = mensagem.Substring(3, 14);
                                    nfeSerie = mensagem.Substring(17, 3);
                                    nfeNumero = mensagem.Substring(20, 8);
                                    ocorrenciaDaEntrega = mensagem.Substring(28, 2);
                                    dataMesAno = mensagem.Substring(30, 8);
                                    hora = mensagem.Substring(38, 4);
                                    int numeroNota = 0;
                                    int.TryParse(nfeNumero, out numeroNota);

                                    dataHoraOcorrencia = DateTime.ParseExact(dataMesAno, formatData, CultureInfo.InvariantCulture).ToString("d") + " " + hora.Substring(0, 2) + ":" + hora.Substring(2, 2);
                                    codObsOcorrencia = mensagem.Substring(42, 2);

                                    //#region Atualiza registro no banco de dados
                                    var nota =
                                        (from c in data.tbNotaFiscal
                                         where
                                             c.tbEmpresa.cnpj.Contains(cnpjEmpresaEmissora) &&
                                             c.numeroNota == numeroNota && c.idPedidoEnvio > 0
                                         select c).FirstOrDefault();
                                    if (nota != null)
                                    {
                                        var envio = (from c in data.tbPedidoEnvio
                                                     join d in data.tbPedidoPacote on c.idPedidoEnvio equals d.idPedidoEnvio
                                                     where
                                                         c.idPedidoEnvio == nota.idPedidoEnvio
                                                     orderby c.idPedidoEnvio descending
                                                     orderby c.idPedidoEnvio descending
                                                     select new
                                                     {
                                                         c.idPedidoEnvio,
                                                         c.idPedido,
                                                         d.rastreio,
                                                         statusAtual = (d.statusAtual ?? "")
                                                     }).FirstOrDefault();

                                        if (envio != null)
                                        {
                                            string statusAtual = "";
                                            try
                                            {
                                                statusAtual = Enum.GetName(typeof(CodigosDeOcorrenciaOcoren), Convert.ToInt32(ocorrenciaDaEntrega));
                                            }
                                            catch (Exception)
                                            {
                                                //codigo de ocorrencia não cadastrado
                                                rnEmails.EnviaEmail("", "andre@bark.com.br", "", "", "", "codigo de ocorrencia OCOREN numero " + ocorrenciaDaEntrega + " não cadastrado", "Falha Atualizar Status de Entrega OCOREN");
                                            }

                                            if (statusAtual != null && (statusAtual.ToLower() != envio.statusAtual.ToLower()))
                                            {
                                                var lastEvent = DateTime.Now.AddYears(-5);
                                                string descricao = "";
                                                string observacao = "";
                                                string dataHora = dataHoraOcorrencia;

                                                try
                                                {

                                                    var dataHoraEvento = Convert.ToDateTime(dataHora);

                                                    if (dataHoraEvento > lastEvent)
                                                    {
                                                        lastEvent = dataHoraEvento;
                                                        descricao = "";
                                                        observacao = codObsOcorrencia;
                                                    }
                                                }
                                                catch (Exception)
                                                {

                                                }

                                                var pedidoPacotes = (from c in data.tbPedidoPacote where c.idPedidoEnvio == envio.idPedidoEnvio select c);
                                                foreach (var pacote in pedidoPacotes)
                                                {
                                                    pacote.statusAtual = statusAtual;
                                                    pacote.dataStatusAtual = DateTime.Now;
                                                    pacote.dataStatusAtualTransportadora = DateTime.Now;
                                                    if (Convert.ToInt32(ocorrenciaDaEntrega) == 1)
                                                    {
                                                        pacote.rastreamentoConcluido = true;
                                                        pacote.dataEntrega = DateTime.Now;
                                                        pacote.dataEntregaTransportadora = lastEvent;

                                                        if (pacote.dataEntrega.Value.Date.AddDays(-5) > pacote.dataEntregaTransportadora)
                                                            pacote.dataEntrega = pacote.dataEntregaTransportadora;
                                                    }
                                                }

                                                var status = new tbPedidoEnvioStatusTransporte
                                                {
                                                    idPedidoEnvio = envio.idPedidoEnvio,
                                                    dataStatus = DateTime.Now,
                                                    dataStatusTransportadora = lastEvent,
                                                    status = statusAtual + " - " + descricao + " - " + observacao,
                                                    observacao = ""
                                                };
                                                data.tbPedidoEnvioStatusTransporte.Add(status);
                                                data.SaveChanges();
                                            }

                                        }

                                    }
                                    //#endregion Atualiza registro no banco de dados

                                    //Response.Write((Enum.GetName(typeof(CodigosDeOcorrenciaOcoren), Convert.ToInt32(ocorrenciaDaEntrega)) ?? ocorrenciaDaEntrega) + " pedidoId: " + (envio != null ? envio.idPedido : 0) + "</br>");
                                }
                            }

                            File.Copy(file.FullName, caminhoArquivoProcessado + file.Name, true);
                            File.Delete(file.FullName);
                        }
                        else
                        {
                            /*File.Copy(file.FullName, caminhoArquivoProcessado + file.Name, true);
                            File.Delete(file.FullName);*/
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                rnEmails.EnviaEmail("", "andre@bark.com.br", "", "", "", ex.ToString() + "<br>" + ex.InnerException, "Erro Proceda OCO");
            }
        }

        private enum CodigosDeOcorrenciaOcoren
        {
            EntregaRealizadaNormalmente = 1,
            EntregaForaDaDataProgramada = 2,
            RecusaPorFaltaDePedidoDeCompra = 3,
            RecusaPorPedidoDeCompraCancelado = 4,
            EnderecoDoClienteDestinoNaoLocalizado = 6,
            MercadoriaEmDesacordoComPedidoCompra = 9,
            RecusaPorDeficienciaEmbalagemMercadoria = 11,
            TransportadoraNaoAtendeACidadeDoClienteDestino = 13,
            MercadoriaSinistrada = 14,
            EmbalagemSinistrada = 15,
            PedidoDeComprasEmDuplicidade = 16,
            ReentregaSolicitadaPeloCliente = 19,
            EntregaPrejudicadaPorHorarioFaltaDeTempoHabil = 20,
            EstabelecimentoFechado = 21,
            ExtravioDeMercadoriaEmTransito = 23,
            RouboDeCarga = 27,
            ClienteRetiraMercadoriaNaTransportadora = 29,
            ProblemaComDocumentacaoNotaFiscalOuCtrc = 30,
            QuantidadeDeProdutoEmDesacordoNotaFiscalOuPedido = 35,
            ResponsavelDeRecebimentoAusente = 46,
            QuebraDoVeiculoDeEntrega = 58,
            EnderecoDeEntregaErrado = 60,
            IdentificacaoDoClienteNaoInformadaEnviadaInsuficiente = 63,
            EstradaEntradaDeAcessoInterditada = 76,
            ClienteDestinoMudouDeEndereco = 77,
            ExtravioTotal = 80,
            ExtravioParcial = 81,
            ApreensaoFiscalDaMercadoria = 85,
            EntregaProgramada = 91,
            ChegadaNaCidadeOuFilialDeDestino = 98,
            OutrosTiposDeOcorrenciasNaoEspecificadosAcima = 99
        }
    }
}
