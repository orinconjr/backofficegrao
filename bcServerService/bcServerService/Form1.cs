﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;
using System.Windows.Forms;
using Amazon.SimpleDB.Model;
using bcServerService.pagamento;
using BarkCommerce.Business.AnaliseRisco;
using BarkCommerce.Core.Repositories.AnaliseRisco;
using BarkCommerce.Core.Repositories.Gateway;
using BarkCommerce.Core.Repositories.Pagamento;
using HtmlAgilityPack;
using System.Xml;
using Newtonsoft.Json;

namespace bcServerService
{
    public partial class Form1 : Form
    {
        private BackgroundWorker bwGerarBoletos = new BackgroundWorker();
        private BackgroundWorker bwGerarCombos = new BackgroundWorker();
        private BackgroundWorker bwChecarQueue = new BackgroundWorker();
        private BackgroundWorker bwChecarQueueProdutos = new BackgroundWorker();
        private string urlTrackingBean = "http://www.jadlog.com:8080/JadlogEdiWs/services/TrackingBean?wsdl";
        private bool forceDisableTimerQueue = false;
        private bool fechar = false;
        private bool jadlogAgora = false;

        public Form1()
        {
            InitializeComponent();
        }


        private void bwCartao_DoWork(object sender, DoWorkEventArgs e)
        {
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {

            //rnEmails.EnviaEmailAndamentoPedido(1141661);
            //timerCarramentoJadLog.Enabled = true;
            //rnBoleto.GerarLotesFinanceiros(12);
            //rnGnre.AtualizarNotasFiscais();
            /*timerQueue.Enabled = false;
            timerEstoqueAvulso.Enabled = false;
            timerQueueProdutos.Enabled = false;
            timerGnre.Enabled = false;

            timerQueueProdutos.Enabled = false;

            timerEmailAndamento.Enabled = false;
            timerRegistroBoleto.Enabled = false;
            
            /*var riscoRepo = new BarkCommerce.Core.Repositories.AnaliseRisco.ClearSaleRepository();
            var gatewayRepo = new BarkCommerce.Core.Repositories.Gateway.MaxipagoRepository();
            var pagamentoRepo = new BarkCommerce.Core.Repositories.Pagamento.PagamentoRepository(riscoRepo, gatewayRepo);
            var pagamentoBusiness = new BarkCommerce.Business.Pagamento.ProcessarPagamentosPedido(pagamentoRepo);
            var pagamentoEnviado = pagamentoRepo.RegistraBoleto(1436591, "");
            /*try
            {
                var emissaoWs = new wsEmissao.EmissaoWs();
                emissaoWs.AtualizaEntregasPedido(720277);
            }
            catch(Exception ex) { }*/
            /*var elasticProduto = ElasticSearch.ElasticSearchProdutoRepository.Instance;
            var listaProdutos = new List<int>();
            listaProdutos.Add(95387);
            elasticProduto.AtualizarListas(listaProdutos);
            elasticProduto.AtualizarProdutoV2(95387, DateTime.Now);*/



            //boleto.boletoSantander.transmiteBoleto();

            //ReservarEstoqueProduto(92399903);
            //return;
            //var rastreio = rnFrete.consultaCodigoRastreioJadlog("75057594", urlTrackingBean);
            //ReservarItens(77960);
            //rnBoleto.GerarLotesFinanceiros(13);
            //PagamentoV2.AtualizaClearsale(971763);
            //rnEmails.enviaCodigoDoTrakingNovo(619519, "");
            //return;
            /*ChecaPedidoCompletoPorPedido(889296);
            this.Close();
            return;*/
            /*var resultado = rnFrete.consultaStatusCorreios("PO024372032BR");
            this.Close();*/

            //var retornoCorreios = correios.buscaEventos("01926746", "8784362", "L", "U", "101", "PO402414037BR");
            /*this.Close();
            return;*/

            /*var correios = new serviceCorreiosRastreio.rastro();
            var retornoCorreios = correios.buscaEventos("10924051000163", "qpmc3s", "L", "U", "101", "PO402414037BR");
            this.Close();
            return; */


            /*enviarSegundaViaBoleto(45384268);
            this.Close();
            return;*/
            //var data = new dbSiteEntities();


            /*var elasticProduto = ElasticSearch.ElasticSearchProdutoRepository.Instance;
            var listaProdutos = new List<int>();
            listaProdutos.Add(77947);
            elasticProduto.AtualizarListas(listaProdutos);
            elasticProduto.AtualizarProdutoV2(77947, DateTime.Now);

            return;


            //rnDepoimentos.GravarDepoimentosFace();

            //GerarAguardandoEstoque(32079015);*/

            /*var elastic1 = ElasticSearch.ElasticSearchBannerRepository.Instance;
            var elastic2 = ElasticSearch.ElasticSearchCategoriaRepository.Instance;
            var elastic3 = ElasticSearch.ElasticSearchCondicaoDePagamentoRepository.Instance;
            var elastic4 = ElasticSearch.ElasticSearchProdutoRepository.Instance;
            var elastic5 = ElasticSearch.ElasticSearchSeoUrlRepository.Instance;*/
            /*elastic1.GerarIndice();
            elastic2.GerarIndice();
            elastic3.GerarIndice();
            elastic4.CriarIndice();
            elastic5.GerarIndice();*/
            /*using (var data = new dbSiteEntities())
            {
                /*var banners = (from c in data.tbBanners  select new { c.Id }).ToList();
                foreach(var banner in banners)
                {
                    elastic1.AtualizarBannerElasticSearch(banner.Id);
                }
                var condicoes = (from c in data.tbCondicoesDePagamento select new { c.condicaoId }).ToList();
                foreach (var banner in condicoes)
                {
                    elastic3.AtualizarCondicaoDePagamentoElasticSearch(banner.condicaoId);
                }
                /*var categorias = (from c in data.tbProdutoCategoria select new { c.categoriaId }).ToList();
                foreach (var banner in categorias)
                {
                    elastic2.AtualizarCategoriaElasticSearch(banner.categoriaId);
                }
            }
            this.Close();
            return;*/

            //rnProceda.LerArquivo_OCOREN("tnt");
            //this.Close();
            //return;
            /*var data = new dbSiteEntities();
            var pedidos = (from c in data.tbPedidos where c.ipDoCliente.Contains("187.17.106.") && c.statusDoPedido != 6 select c).ToList();
            foreach (var pedido in pedidos)
            {
                try
                {
                    PagamentoV2.CancelarClearSaleFraude(pedido.pedidoId, "sus");
                }
                catch(Exception ex) { }
            }
            return;*/


            timerAtualizarEstoque.Interval = 1;
            if (rnConfiguracoes.HabilitarTimerAtualizarEstoque)
            {
                timerAtualizarEstoque.Enabled = true;
                timerEstoqueAvulso.Enabled = true;
            }

            /*var elasticProduto = ElasticSearch.ElasticSearchColecaoRepository.Instance;
            elasticProduto.GerarIndice();*/

            HabilitarTimerCancelamentoPedidos(rnConfiguracoes.HabilitarTimerCancelarPedidos);
            HabilitarTimerPedidosCompletos(rnConfiguracoes.HabilitarTimerChecarCompletos);

            timerEmailAndamento.Enabled = false;
            if (rnConfiguracoes.HabilitarTimerVerificaEstoque) timerVerificaEstoque.Enabled = true;
            if (rnConfiguracoes.HabilitarTimerPendenciasJadlog) timerPendenciasJadLog.Enabled = true;
            if (rnConfiguracoes.HabilitarTimerGnre) timerGnre.Enabled = true;
            if (rnConfiguracoes.HabilitarTimerFeeds) timerFeeds.Enabled = true;
            if (rnConfiguracoes.HabilitarTimerCarregamentoJadlog) timerCarramentoJadLog.Enabled = true;
            if (rnConfiguracoes.HabilitarTimerNotas) timerNotas.Enabled = true;
            if (rnConfiguracoes.HabilitarTimerPrazo) timerPrazo.Enabled = true;
            if (rnConfiguracoes.HabilitarRastreamentoTnt) timerHabilitarRastreamentoTnt.Enabled = true;
            if (rnConfiguracoes.HabilitarRastreamentoJadlog) timerRastreamentoJadlog.Enabled = true;
            if (rnConfiguracoes.HabilitarRastreamentoCorreios) timerRastreamentoCorreios.Enabled = true;
            if (rnConfiguracoes.HabilitarTimerClearsale) timerClearsale.Enabled = true;
            if (rnConfiguracoes.HabilitarTimerClearsalePendentes) timerUpdateClearPagamentos.Enabled = true;
            if (rnConfiguracoes.HabilitarTimerProceda) timerProceda.Enabled = true;
            if (rnConfiguracoes.HabilitarTimerLoteCobranca) timerLoteBancario.Enabled = true;
            if (rnConfiguracoes.HabilitarTimerRegistroBoleto) timerRegistroBoleto.Enabled = true;
            if (rnConfiguracoes.HabilitarTimerProdutos) timerQueueProdutos.Enabled = true;
            if (rnConfiguracoes.HabilitarTimerQueue) timerQueue.Enabled = true;
            if (rnConfiguracoes.HabilitarTimerQueuesParalelos) timerQueuesParalelos.Enabled = true;


            timerUpdateClearPagamentos.Interval = 1;

            //if (!rnConfiguracoes.AmbienteDebugTimerAtualizarEstoque) timerAtualizarEstoque.Enabled = true;
            //if (!rnConfiguracoes.AmbienteDebugTimerAtualizarEstoque) timerPrazo.Enabled = true;
            //if (!rnConfiguracoes.AmbienteDebugTimerCancelarPedidos) timerCancelarPedidos.Enabled = true;
            //if (!rnConfiguracoes.AmbienteDebugTimerChecaCompletos) timerChecaCompletos.Enabled = true;
            //if (!rnConfiguracoes.AmbienteDebugTimerVerificaEstoque) timerVerificaEstoque.Enabled = true;
            //if (!rnConfiguracoes.AmbienteDebugTimerVerificaEstoque) timerFeeds.Enabled = true;
            //if (!rnConfiguracoes.AmbienteDebugCarregamentoJadLog) timerCarramentoJadLog.Enabled = true;


            if (urlTrackingBean == "http://www.jadlog.com.br:8080/JadlogEdiWs/services/TrackingBean?wsdl")
                btnChecarEnviosJadLog.Text = "Checar Envios JadLog - " + "www.jadlog.com";
            else
                btnChecarEnviosJadLog.Text = "Checar Envios JadLog - " + "www.jadlog.com.br";
        }

        
        #region checarqueue

        private void timerQueueProdutos_Tick(object sender, EventArgs e)
        {
            timerQueueProdutos.Enabled = false;
            bwChecarQueueProdutos.WorkerSupportsCancellation = true;
            bwChecarQueueProdutos.DoWork += new DoWorkEventHandler(bwChecarQueueProdutos_DoWork);
            bwChecarQueueProdutos.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bwChecarQueueProdutos_RunWorkerCompleted);

            if (bwChecarQueueProdutos.IsBusy != true)
            {
                bwChecarQueueProdutos.RunWorkerAsync();
            }
        }

        private void bwChecarQueueProdutos_DoWork(object sender, DoWorkEventArgs e)
        {
            lblElastic.Invoke((MethodInvoker)delegate
            {
                lblElastic.Text = "Consultando filas de atualizacao Produtos";
            });
            var data = new dbSiteEntities();
            var agora = DateTime.Now.AddSeconds(-1);
            var agendamentosBanners = (from c in data.tbBannerAgendamento
                                       where
    (c.queueConcluidoContador == false && (c.inicioContador < agora | c.fimContador < agora)) | (c.queueConcluidoEntrada == false && c.dataEntrada <= agora) | (c.queueConcluidoSaida == false && c.dataSaida <= agora)
                                       select c).ToList();
            foreach (var agendamento in agendamentosBanners)
            {
                processaQueueAgendamentoBanners(agendamento.idBannerAgendamento);
            }


            var queuesList = new List<tbQueue>();
            using (new TransactionScope(TransactionScopeOption.Required, new TransactionOptions
            {
                IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
            }))
            {
                using (var dataTrans = new dbSiteEntities())
                {
                    queuesList = (from c in dataTrans.tbQueue
                                  where c.andamento == false && c.concluido == false && (c.idRelacionado ?? 0) > 0 &&
                                  (c.tipoQueue == 15 | c.tipoQueue == 16 | c.tipoQueue == 18 | 
                                  c.tipoQueue == 19 | c.tipoQueue == 35 | c.tipoQueue == 36 | c.tipoQueue == 37 | c.tipoQueue == 46)
                                  select c).ToList();
                    /*if(queuesList.Count(x => x.tipoQueue == 36) > 0)
                    {
                        queuesList = queuesList.Where(x => x.tipoQueue == 36).ToList();
                    }*/
                }
            }

            var queues = queuesList.ToList().GroupBy(x => new { x.idQueue, x.tipoQueue, x.idRelacionado, x.mensagem })
                    .Select(y => new
                    {
                        y.Key.tipoQueue,
                        idQueue = y.FirstOrDefault() == null ? null : (long?)y.FirstOrDefault().idQueue,
                        idRelacionado = y.FirstOrDefault() == null ? null : (long?)y.FirstOrDefault().idRelacionado,
                        agendamento = y.FirstOrDefault() == null ? null : (DateTime?)y.FirstOrDefault().agendamento,
                        mensagem = y.FirstOrDefault() == null ? null : (string)y.FirstOrDefault().mensagem,
                    }).Where(x => x.agendamento <= agora).ToList();

            /*if (queues.Any(x => x.tipoQueue == 35 | x.tipoQueue == 36 | x.tipoQueue == 37))
            {
                queues = queues.Where(x => x.tipoQueue == 35 | x.tipoQueue == 36 | x.tipoQueue == 37).ToList();
            }*/

            if (queues.Any(x => x.tipoQueue == 15 && x.mensagem == "1")) queues = queues.Where(x => x.tipoQueue == 15 && x.mensagem == "1").ToList();

            if (queues.Any(x => x.tipoQueue == 37))
            {
                queues = queues.Where(x => x.tipoQueue == 37).ToList();
            }
            if (queues.Any(x => x.tipoQueue == 36))
            {
                queues = queues.Where(x => x.tipoQueue == 36).ToList();
            }
            if (queues.Any(x => x.tipoQueue == 16))
            {
                queues = queues.Where(x => x.tipoQueue == 16).ToList();
            }
            if (queues.Any(x => x.tipoQueue == 46))
            {
                queues = queues.Where(x => x.tipoQueue == 46).ToList();
            }
            bool hasItens = false;
            List<long> idsAtualizados = new List<long>();
            List<long> idsCorrompidos = new List<long>();
            var queuesGeral = queues.Where(x => x.idQueue > 0).ToList();
            queues = queues.Take(1000).ToList();
            idsCorrompidos = queues.Where(x => x.tipoQueue == 15 && x.idRelacionado == null).Select(x => x.idQueue ?? 0).ToList();
            var dataInicio = DateTime.Now;
            var dataFim = DateTime.Now;
            int total = queues.Count();
            int atual = 0;
            bool listaProcessada = false;
            foreach (var queue in queues)
            {
                atual++;
                lblStatus.Invoke((MethodInvoker)delegate
                {
                    lblStatus.Text = queue.idQueue.ToString();
                });
                hasItens = true;
                if (queue.tipoQueue == 15 && rnConfiguracoes.HabilitarQueue15) //Atualizar Elastic Search
                {
                    /*if (queuesGeral.Where(x => x.tipoQueue == 15).Count() > 300)
                    {
                        utilizarListasProduto = true;
                        if (lastListUpdates < queue.agendamento)
                        {
                            lblLastListUpdate.Invoke((MethodInvoker)delegate
                           {
                               lblLastListUpdate.Text = "Atualizando Listas";
                           });
                            lastListUpdates = DateTime.Now;
                            using (new TransactionScope(TransactionScopeOption.Required, new TransactionOptions
                            {
                                IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                            }))
                            {
                                using (var dataCol = new dbSiteEntities())
                                {
                                    colecoes = (from c in dataCol.tbColecao select c).ToList();
                                    juncaoColecoes = (from c in dataCol.tbJuncaoProdutoColecao select c).ToList();
                                    categorias = (from c in dataCol.tbProdutoCategoria select c).ToList();
                                    juncaoCategorias =
                                        (from c in dataCol.tbJuncaoProdutoCategoria select c).ToList();
                                    fotos = (from c in dataCol.tbProdutoFoto select c).ToList();
                                    videos = (from c in dataCol.tbProdutoVideo select c).ToList();
                                    listaProdutos = (from c in dataCol.tbProdutos select c).ToList();
                                    listaFornecedores = (from c in dataCol.tbProdutoFornecedor select c).ToList();
                                    listaInformacoes = (from c in dataCol.tbQueue
                                                        where c.tipoQueue == 15 && c.concluido == false
                                                        join d in dataCol.tbInformacaoAdcional on c.idRelacionado equals d.produtoId
                                                        select d).ToList().Distinct().ToList();
                                }
                            }

                            lblLastListUpdate.Invoke((MethodInvoker)delegate
                           {
                               lblLastListUpdate.Text = "Ultimo Update Lista:" + lastListUpdates.ToString();
                           });
                        }
                    }
                    else
                    {
                        utilizarListasProduto = false;
                        lblLastListUpdate.Invoke((MethodInvoker)delegate
                        {
                            lblLastListUpdate.Text = "Sem utilizar listas";
                        });
                    }*/
                    /*if (utilizarListasProduto)
                    {
                        lblStatus.Invoke((MethodInvoker)delegate
                        {
                            lblStatus.Text = "Atualizando utilizando listas:" + atual + "/" + total;
                        });
                        idsAtualizados.Add((long)queue.idQueue);

                        bool processado = false;

                        var elastic = ElasticSearch.ElasticSearchProdutoRepository.Instance;
                        try
                        {
                            elastic.AtualizarProduto((int)queue.idRelacionado, listaProdutos, listaFornecedores, juncaoCategorias, categorias, juncaoColecoes, colecoes, fotos, videos, listaInformacoes);
                        }
                        catch (Exception ex)
                        {
                            var dataNovaQueue = new dbSiteEntities();
                            var novaQueue = new tbQueue() { idRelacionado = (int)queue.idRelacionado, agendamento = DateTime.Now, andamento = false, concluido = false, mensagem = "", tipoQueue = 15 };
                            dataNovaQueue.tbQueue.Add(novaQueue);
                            dataNovaQueue.SaveChanges();
                            rnEmails.EnviaEmail("", "andre@bark.com.br", "", "", "", ex.InnerException + "<br>" + ex.ToString(), "Erro Elastic " + queue.idRelacionado);
                        }
                    }
                    else
                    {
                        if (bwAtualizarProdutoElastic.IsBusy != true)
                        {
                            bwAtualizarProdutoElastic.RunWorkerAsync(queue.idQueue);
                        }
                    }*/
                    lblElastic.Invoke((MethodInvoker)delegate
                    {
                        lblElastic.Text = "Instanciando elastic";
                    });
                    var elastic = ElasticSearch.ElasticSearchProdutoRepository.Instance;
                    if(elastic.ultimoUpdateLista < queue.agendamento | listaProcessada == false)
                    {
                        lblElastic.Invoke((MethodInvoker)delegate
                        {
                            lblElastic.Text = "Atualiazndo lista atualizacao Produtos";
                        });
                        elastic.AtualizarListas(queues.Select(x => (int)(x.idRelacionado ?? 0)).ToList());
                        listaProcessada = true;
                        
                    }
                    lblElastic.Invoke((MethodInvoker)delegate
                    {
                        lblElastic.Text = "Atualizando utilizando listas:" + atual + "/" + total;
                    });

                    bool processado = false;

                    try
                    {
                        elastic.AtualizarProdutoV2((int)queue.idRelacionado, DateTime.Now);
                        if(queuesList.Count(x => x.idRelacionado == queue.idRelacionado) > 1)
                        {
                            data.admin_concluiQueueProduto((int)queue.idRelacionado);
                        }
                        else
                        {
                            data.admin_concluiQueue((int)queue.idQueue);
                        }
                    }
                    catch (Exception ex)
                    {
                        var dataNovaQueue = new dbSiteEntities();
                        var novaQueue = new tbQueue() { idRelacionado = (int)queue.idRelacionado, agendamento = DateTime.Now, andamento = false, concluido = false, mensagem = "", tipoQueue = 15 };
                        dataNovaQueue.tbQueue.Add(novaQueue);
                        dataNovaQueue.SaveChanges();
                       // rnEmails.EnviaEmail("", "andre@bark.com.br", "", "", "", ex.InnerException + "<br>" + ex.ToString(), "Erro Elastic " + queue.idRelacionado);
                    }

                }
                if (queue.tipoQueue == 16 && rnConfiguracoes.HabilitarQueue16) //Atualizar Elastic Search
                {
                    if (bwAtualizarCategoriaElastic.IsBusy != true && (!rnConfiguracoes.AmbienteDebugAtualizarElastic))
                    {
                        bwAtualizarCategoriaElastic.RunWorkerAsync(queue.idQueue);
                    }
                }
                if (queue.tipoQueue == 18 && rnConfiguracoes.HabilitarQueue18) //Atualizar toda a busca
                {
                    if (bwAtualizarTodosElastic.IsBusy != true && (!rnConfiguracoes.AmbienteDebugAtualizarElastic))
                    {
                        bwAtualizarTodosElastic.RunWorkerAsync(queue.idQueue);
                    }
                }
                if (queue.tipoQueue == 19 && rnConfiguracoes.HabilitarQueue19) //Atualizar fornecedor
                {
                    if (bwAtualizarFornecedorElastic.IsBusy != true && (!rnConfiguracoes.AmbienteDebugAtualizarElastic))
                    {
                        bwAtualizarFornecedorElastic.RunWorkerAsync(queue.idQueue);
                    }
                }
                if (queue.tipoQueue == 35 && rnConfiguracoes.HabilitarQueue35) //Atualizar fornecedor
                {
                    lblElastic.Invoke((MethodInvoker)delegate
                    {
                        lblElastic.Text = "Atualizando seourl:" + atual + "/" + total;
                    });
                    var elasticSeo = ElasticSearch.ElasticSearchSeoUrlRepository.Instance;
                    elasticSeo.AtualizarSeoUrlQueue(queue.idQueue);
                }
                if (queue.tipoQueue == 36 && rnConfiguracoes.HabilitarQueue36) //Atualizar Banner no Elastic
                {
                    var elasticBanner = ElasticSearch.ElasticSearchBannerRepository.Instance;
                    lblElastic.Invoke((MethodInvoker)delegate
                    {
                        lblElastic.Text = "Atualizando banner:" + atual + "/" + total;
                    });
                    elasticBanner.AtualizarBannerQueue(queue.idQueue);
                }
                if (queue.tipoQueue == 37 && rnConfiguracoes.HabilitarQueue37) //Atualizar Banner no Elastic
                {
                    lblElastic.Invoke((MethodInvoker)delegate
                    {
                        lblElastic.Text = "Atualizando categoria:" + atual + "/" + total;
                    });
                    var elasticCategoria = ElasticSearch.ElasticSearchCategoriaRepository.Instance;
                    elasticCategoria.AtualizarCategoriaQueue(queue.idQueue);
                }
                if (queue.tipoQueue == 46 && rnConfiguracoes.HabilitarQueue37) //Atualizar Banner no Elastic
                {
                    lblElastic.Invoke((MethodInvoker)delegate
                    {
                        lblElastic.Text = "Atualizando colecao:" + atual + "/" + total;
                    });
                    var elasticColecao = ElasticSearch.ElasticSearchColecaoRepository.Instance;
                    elasticColecao.AtualizarColecaoQueue(queue.idQueue);
                }
                dataFim = DateTime.Now;
            }

            if (idsAtualizados.Count > 0)
            {
                /*lblStatus.Invoke((MethodInvoker)delegate
                {
                    lblStatus.Text = "Iniciando Atualizando queues produtos";
                });*/
                lblElastic.Invoke((MethodInvoker)delegate
                {
                    lblElastic.Text = "Iniciando Atualizando queues produtos";
                });
                var dataUpdate = new dbSiteEntities();
                int totalQueuesUpdate = idsAtualizados.Count();
                int queuesUpdateAtual = 0;
                /*foreach (var idAtualizado in idsAtualizados)
                {
                    queuesUpdateAtual++;
                    lblElastic.Invoke((MethodInvoker)delegate
                    {
                        lblElastic.Text = "Iniciand queues " + queuesUpdateAtual + "/" + totalQueuesUpdate;
                    });
                    dataUpdate.admin_concluiQueue((int)idAtualizado);
                }*/
                /*var queuesUpdate = (from c in dataUpdate.tbQueue where idsAtualizados.Contains(c.idQueue) select c).ToList();
                int totalQueuesUpdate = queuesUpdate.Count();
                int queuesUpdateAtual = 0;
                foreach (var queueUpdate in queuesUpdate)
                {
                    queuesUpdateAtual++;
                    lblElastic.Invoke((MethodInvoker)delegate
                    {
                        lblElastic.Text = "Iniciand queues " + queuesUpdateAtual + "/" + totalQueuesUpdate;
                    });
                    queueUpdate.dataInicio = dataInicio;
                    queueUpdate.dataConclusao = dataFim;
                    queueUpdate.andamento = true;
                    queueUpdate.concluido = true;

                    /*var queuesRelacionados = (from c in dataUpdate.tbQueue where c.tipoQueue == queueUpdate.tipoQueue && c.idRelacionado == queueUpdate.idRelacionado && c.agendamento < dataInicio && c.andamento == false && c.concluido == false select c).ToList();
                    foreach (var queueRelacionado in queuesRelacionados)
                    {
                        queueRelacionado.dataInicio = dataInicio;
                        queueRelacionado.dataConclusao = dataFim;
                        queueRelacionado.andamento = true;
                        queueRelacionado.concluido = true;
                    }*/
                    //dataUpdate.SaveChanges();
                
                lblStatus.Invoke((MethodInvoker)delegate
                {
                    lblStatus.Text = "Gravando Atualizando queues produtos";
                });
                dataUpdate.SaveChanges();
            }

            if (idsCorrompidos.Count > 0)
            {
                lblStatus.Invoke((MethodInvoker)delegate
                {
                    lblStatus.Text = "Iniciando Atualizando queues corrompidas";
                });
                var dataUpdate = new dbSiteEntities();
                var queuesUpdate = (from c in dataUpdate.tbQueue where idsCorrompidos.Contains(c.idQueue) select c).ToList();
                foreach (var queueUpdate in queuesUpdate)
                {
                    queueUpdate.dataInicio = dataInicio;
                    queueUpdate.dataConclusao = dataFim;
                    queueUpdate.andamento = true;
                    queueUpdate.concluido = true;
                }
                dataUpdate.SaveChanges();
            }


            if (hasItens)
            {
                timerQueueProdutos.Interval = 1;
            }
            else
            {
                timerQueueProdutos.Interval = 15000;
            }
        }



        private void processaQueueAgendamentoBanners(long idBannerAgendamento)
        {
            var data = new dbSiteEntities();
            var agendamento = (from c in data.tbBannerAgendamento where c.idBannerAgendamento == idBannerAgendamento select c).First();
            var banner = (from c in data.tbBanners where c.Id == agendamento.idBanner select c).First();
            var agora = DateTime.Now.AddSeconds(-1);
            if (agendamento.queueConcluidoEntrada == false)
            {
                if (agendamento.dataEntrada == null)
                {
                    agendamento.queueConcluidoEntrada = true;
                }
                else if (agendamento.dataEntrada < agora)
                {
                    banner.ativo = true;
                    agendamento.queueConcluidoEntrada = true;
                }
            }
            if (agendamento.queueConcluidoSaida == false)
            {
                if (agendamento.dataSaida == null)
                {
                    agendamento.queueConcluidoSaida = true;
                }
                else if (agendamento.dataSaida < agora)
                {
                    banner.ativo = false;
                    agendamento.queueConcluidoSaida = true;
                }
            }
            if (agendamento.queueConcluidoContador == false)
            {
                if (agendamento.inicioContador == null && agendamento.fimContador == null)
                {
                    agendamento.queueConcluidoContador = true;
                }
                else if (agendamento.inicioContador < agora)
                {
                    banner.inicioContador = agendamento.inicioContador;
                    banner.fimContador = agendamento.fimContador;
                    agendamento.queueConcluidoContador = true;
                }
            }

            data.SaveChanges();

        }
        private void bwChecarQueueProdutos_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            timerQueueProdutos.Enabled = true;
        }



        private void timerQueue_Tick(object sender, EventArgs e)
        {
            timerQueue.Enabled = false;
            bwChecarQueue.WorkerSupportsCancellation = true;
            bwChecarQueue.DoWork += new DoWorkEventHandler(bwChecarQueue_DoWork);
            bwChecarQueue.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bwChecarQueue_RunWorkerCompleted);

            if (bwChecarQueue.IsBusy != true)
            {
                lblStatus.Text = "Consultando Queue";
                bwChecarQueue.RunWorkerAsync();
            }
        }


        private void bwChecarQueue_DoWork(object sender, DoWorkEventArgs e)
        {
            if (fechar)
            {
                //geraLista();
                bool isBusy = bwPedidoFornecedor.IsBusy | bwEnviaEmailCarregamento.IsBusy | bwEmailTnt.IsBusy;
                if (!isBusy) this.Close();
                return;
            }
            var data = new dbSiteEntities();
            var agora = DateTime.Now.AddSeconds(-1);


            var queues = (from c in data.tbQueue
                          where c.andamento == false && c.concluido == false &&
                          c.agendamento <= agora && (c.tipoQueue != 15 && c.tipoQueue != 16 &&
                          c.tipoQueue != 18 && c.tipoQueue != 19 && c.tipoQueue != 26 && c.tipoQueue != 35
                          && c.tipoQueue != 38 && c.tipoQueue != 8 && c.tipoQueue != 21 && c.tipoQueue != 3
                          && c.tipoQueue != 7
                          && c.tipoQueue != 10
                          && c.tipoQueue != 11
                          && c.tipoQueue != 12
                          && c.tipoQueue != 23
                          && c.tipoQueue != 24
                          && c.tipoQueue != 32
                          && c.tipoQueue != 33
                          && c.tipoQueue != 41
                          && c.tipoQueue != 42
                          && c.tipoQueue != 44
                          && c.tipoQueue != 38
                          && c.tipoQueue != 34
                          && c.tipoQueue != 2)
                          orderby c.idQueue, c.agendamento
                          select new
                          {
                              c.tipoQueue,
                              c.idQueue
                          }).Take(50).ToList();
            // if (queues.Any(x => x.tipoQueue == 0)) queues = queues.Where(x => x.tipoQueue == 0).ToList();
            bool hasItens = false;

            /*if (queues.Any(x => x.tipoQueue == 38)) // Atualizar todo estoque real
            {
                queues = queues.Where(x => x.tipoQueue != 38).ToList();*/
            
            //ConsultarAtualizarEstoqueQueue();
            //}

            foreach (var queue in queues)
            {
                lblStatus.Invoke((MethodInvoker)delegate
                {
                    lblStatus.Text = queue.idQueue.ToString();
                });
                hasItens = true;
                if (queue.tipoQueue == 1 && rnConfiguracoes.HabilitarQueue1) //Pedidos ao Fornecedor
                {
                    if (bwPedidoFornecedor.IsBusy != true && (!rnConfiguracoes.AmbienteDebugPedidoFornecedor))
                    {
                        lblStatusGerarPedido.Invoke((MethodInvoker)delegate
                        {
                            lblStatusGerarPedido.Text = "Gerando pedidos ao fornecedor";
                        });
                        bwPedidoFornecedor.RunWorkerAsync(queue.idQueue);
                    }
                }
                if (queue.tipoQueue == 4 && rnConfiguracoes.HabilitarQueue4) //Pedido -> Gravar itens do combo
                {
                    try
                    {
                        if (!rnConfiguracoes.AmbienteDebugGravarItensCombo) GravarItensComboQueue(queue.idQueue);
                    }
                    catch (Exception)
                    {

                    }
                }
                if (queue.tipoQueue == 5 && rnConfiguracoes.HabilitarQueue5) //Pedido -> Calcular prazo do Pedido / Gerar lista aguardando estoque
                {
                    try
                    {
                        if (!rnConfiguracoes.AmbienteDebugCalcularPrazoMaximoPedido) GerarAguardandoEstoque(queue.idQueue);
                    }
                    catch (Exception)
                    {

                    }
                }
                if (queue.tipoQueue == 6 && rnConfiguracoes.HabilitarQueue6) //Pedido -> Reservar estoque do pedido
                {
                    try
                    {
                        if (!rnConfiguracoes.AmbienteDebugReservarEstoque) ReservarEstoquePedido(queue.idQueue);
                    }
                    catch (Exception)
                    {

                    }
                }
                if (queue.tipoQueue == 9 && rnConfiguracoes.HabilitarQueue9) //Utilizar Estoque
                {
                    if (bwUtilizarEstoque.IsBusy != true)
                    {
                        if (!rnConfiguracoes.AmbienteDebugUtilizarEstoque) bwUtilizarEstoque.RunWorkerAsync(queue.idQueue);
                    }
                }                
                if (queue.tipoQueue == 13 && rnConfiguracoes.HabilitarQueue13) //Pedido -> Atualiza Prazo Maximo Pedidos
                {
                    try
                    {
                        if (!rnConfiguracoes.AmbienteDebugCalcularPrazoMaximoPedido) GerarAguardandoEstoque(queue.idQueue);
                    }
                    catch (Exception)
                    {

                    }
                }
                if (queue.tipoQueue == 14 && rnConfiguracoes.HabilitarQueue14) //Lista transferencia
                {
                    if (bwListaTransferencia.IsBusy != true && (!rnConfiguracoes.AmbienteDebugTimerChecaCompletos))
                    {
                        bwListaTransferencia.RunWorkerAsync(queue.idQueue);
                    }
                }
                if (queue.tipoQueue == 17 && rnConfiguracoes.HabilitarQueue17) //Atualizar Relevancia
                {
                    if (bwAtualizarRelevancia.IsBusy != true && (!rnConfiguracoes.AmbienteDebugAtualizarRelevancia))
                    {
                        bwAtualizarRelevancia.RunWorkerAsync(queue.idQueue);
                    }
                }
                if (queue.tipoQueue == 20 && rnConfiguracoes.HabilitarQueue20) //Libera produtos cancelados
                {
                    if (!rnConfiguracoes.AmbienteDebugReservarEstoque)
                        LiberarProdutosCanceladosPedido(queue.idQueue);
                }
                if (queue.tipoQueue == 22 && rnConfiguracoes.HabilitarQueue22) //Pedido -> Enviar Email Pedido
                {
                    try
                    {
                        if (!rnConfiguracoes.AmbienteDebugEnviarEmailConfirmacao) EnviarEmailConfirmacaoDadosPedidoQueue(queue.idQueue);
                    }
                    catch (Exception)
                    {

                    }
                }
                if (queue.tipoQueue == 25) //Envia Erro
                {
                    ProcessaQueueEnviaErro(queue.idQueue);
                }
                if (queue.tipoQueue == 27 && rnConfiguracoes.HabilitarQueue3) //Carregamento -> Enviar XML tnt
                {
                    if (bwEmailBelle.IsBusy != true)
                    {
                        bwEmailBelle.RunWorkerAsync(queue.idQueue);
                    }
                }
                if (queue.tipoQueue == 28 && rnConfiguracoes.HabilitarQueue28) //Desativar banner
                {
                    DesativarBanner(queue.idQueue);
                }
                if (queue.tipoQueue == 29 && rnConfiguracoes.HabilitarQueue29) //Ativar banner
                {
                    AtivarBanner(queue.idQueue);
                }
                if (queue.tipoQueue == 30 && rnConfiguracoes.HabilitarQueue30) //Alterar preço promocional
                {
                    AlterarPrecoPromocional(queue.idQueue);
                }
                if (queue.tipoQueue == 31 && rnConfiguracoes.HabilitarQueue30) //Ativar produto promoção
                {
                    AlterarProdutoPromocao(queue.idQueue);
                }
                if (queue.tipoQueue == 39) // Atualizar todo estoque real
                {
                    //AtualizarTodoEstoqueRealQueue(queue.idQueue);
                }
                if (queue.tipoQueue == 40) // Atualizar todo estoque real
                {
                    var awsCloudFrontService = new CloudFront.AWSCloudFrontService();
                    awsCloudFrontService.InvalidarCacheQueue(queue.idQueue);
                }
                if (queue.tipoQueue == 43) //Ativar produto promoção
                {
                    AtivarDesativarProduto(queue.idQueue);
                }

            }

            if (hasItens)
            {
                timerQueue.Interval = 1;
            }
            else
            {
                timerQueue.Interval = 15000;
            }
        }


        private void GerarLotesCobrancas(long idQueue)
        {
            #region atualizaQueue
            var data = new dbSiteEntities();
            var queue = (from c in data.tbQueue where c.idQueue == idQueue select c).First();
            queue.andamento = true;
            queue.dataInicio = DateTime.Now;
            data.SaveChanges();
            #endregion atualizaQueue

            int totalBoletos = 0;
            try
            {
                totalBoletos = rnBoleto.GerarLotesFinanceiros(11);
                totalBoletos = rnBoleto.GerarLotesFinanceiros(12);
                totalBoletos = rnBoleto.GerarLotesFinanceiros(13);
            }
            catch (Exception ex)
            {
                rnEmails.EnviaEmail("", "andre@bark.com.br", "", "", "", ex.ToString(), "Erro gerar lote boleto");
            }
            #region finalizaQueue
            queue.concluido = true;
            queue.andamento = true;
            queue.dataConclusao = DateTime.Now;
            data.SaveChanges();

            var novoQueue = new tbQueue();
            if (totalBoletos > 100)
            {
                var agora = DateTime.Now;
                novoQueue.agendamento = agora;
            }
            else
            {
                novoQueue.agendamento = Convert.ToDateTime(DateTime.Now.AddMinutes(60));
            }
            novoQueue.tipoQueue = 34;
            novoQueue.mensagem = "";
            novoQueue.concluido = false;
            novoQueue.andamento = false;
            data.tbQueue.Add(novoQueue);
            data.SaveChanges();

            #endregion

        }
        private void ProcessaQueueEnviaErro(long idQueue)
        {
            #region atualizaQueue
            var dataQueue = new dbSiteEntities();
            var queue = (from c in dataQueue.tbQueue where c.idQueue == idQueue select c).First();
            queue.andamento = true;
            queue.dataInicio = DateTime.Now;
            dataQueue.SaveChanges();
            #endregion

            rnEmails.EnviaEmail("", "andre@bark.com.br", "", "", "", queue.mensagem, "Erro na Fila");

            #region finalizaQueue
            var queueFinalizar = (from c in dataQueue.tbQueue where c.idQueue == idQueue select c).First();
            queueFinalizar.concluido = true;
            queueFinalizar.dataConclusao = DateTime.Now;
            dataQueue.SaveChanges();
            #endregion
        }

        private void bwChecarQueue_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //timerQueue.Interval = 15000;
            if (!forceDisableTimerQueue) timerQueue.Enabled = true;
            lblStatus.Text = "Aguardando Checagem de Queue " + timerQueue.Interval.ToString();
        }
        #endregion


        private static void GerarAguardandoEstoque(long idQueue)
        {
            #region atualizaQueue
            var dataQueue = new dbSiteEntities();
            var queue = (from c in dataQueue.tbQueue where c.idQueue == idQueue select c).First();
            bool firstTry = (queue.dataInicio == null);
            queue.andamento = true;
            queue.dataInicio = DateTime.Now;
            dataQueue.SaveChanges();
            #endregion

            bool gravado = true;



            try
            {
                var pedidoId = Convert.ToInt32(queue.idRelacionado);
                var data = new dbSiteEntities();
                var pedido = (from c in data.tbPedidos where c.pedidoId == pedidoId select c).First();
                var prazoPedidos = 0;

                if (pedido.dataConfirmacaoPagamento != null && pedido.prazoMaximoPedidos != null && pedido.prazoFinalPedido == null)
                {

                    int prazoInicioFabricacao = 0;
                    if (pedido.prazoInicioFabricao != null)
                    {
                        int diferencaDias = (DateTime.Now - Convert.ToDateTime(pedido.dataHoraDoPedido)).Days;
                        prazoInicioFabricacao = (int)pedido.prazoInicioFabricao - diferencaDias;
                        if (prazoInicioFabricacao < 0) prazoInicioFabricacao = 0;
                    }

                    int prazoFinal = rnFuncoes.retornaPrazoDiasUteis((int)pedido.prazoMaximoPedidos, prazoInicioFabricacao, (DateTime)pedido.dataConfirmacaoPagamento);

                    // verifica se é necessário gravar prazoFinalPedido
                    if (pedido.prazoFinalPedido == null)
                    {
                        pedido.prazoFinalPedido = ((DateTime)pedido.dataConfirmacaoPagamento).AddDays(prazoFinal);
                        pedido.prazoMaximoPostagemAtualizado = ((DateTime)pedido.dataConfirmacaoPagamento).AddDays(prazoFinal);
                        data.SaveChanges();
                    }                    
                }
                var itensCancelados =
                    (from c in data.tbItemPedidoEstoque
                     where c.tbItensPedido.pedidoId == pedidoId
                     where c.cancelado == true
                     select c).ToList();
                foreach (var itemCancelado in itensCancelados)
                {
                    if ((itemCancelado.tbItensPedido.cancelado ?? false) == false && pedido.statusDoPedido != 6)
                    {
                        itemCancelado.cancelado = false;
                        data.SaveChanges();
                    }
                }

                var itensPedidoGeral = (from c in data.tbItemPedidoEstoque where c.tbItensPedido.pedidoId == pedidoId where c.cancelado == false && c.enviado == false && c.reservado == false
                                        select c).ToList();
                var itensComEstoque = new Dictionary<int, int>();


                var log = new rnLog();
                log.usuario = "Sistema";
                log.descricoes.Add("Atualizando prazo máximo do pedido");
                log.tiposRelacionados.Add(new rnEnums.TipoRelacionadoIds() { idRegistroRelacionado = pedidoId, tipoRelacionado = rnEnums.TipoRegistroRelacionado.Pedido });
                log.tiposOperacao.Add(rnEnums.TipoOperacao.Pedido);
                log.tiposOperacao.Add(rnEnums.TipoOperacao.Prazo);

                int totalItens = itensPedidoGeral.Count();
                foreach (var itemPedidoGeral in itensPedidoGeral)
                {
                    log.descricoes.Add("Verificando produto " + itemPedidoGeral.tbProdutos.produtoNome);
                    log.tiposRelacionados.Add(new rnEnums.TipoRelacionadoIds()
                    {
                        idRegistroRelacionado = itemPedidoGeral.itemPedidoId,
                        tipoRelacionado = rnEnums.TipoRegistroRelacionado.ItemPedido
                    });

                    int estoqueItem = RetornaEstoqueLivreProduto(itemPedidoGeral.produtoId);
                    int prazoFornecedor = itemPedidoGeral.tbProdutos.tbProdutoFornecedor.fornecedorPrazoDeFabricacao ?? 0;
                    if (itemPedidoGeral.tbProdutos.tbProdutoFornecedor.dataFimFabricacao != null && itemPedidoGeral.tbProdutos.tbProdutoFornecedor.dataFimFabricacao < DateTime.Now && itemPedidoGeral.tbProdutos.tbProdutoFornecedor.dataInicioFabricacao != null && itemPedidoGeral.tbProdutos.tbProdutoFornecedor.dataInicioFabricacao > DateTime.Now)
                    {
                        string descricaoRecesso = "Fornecedor em recesso. ";
                        descricaoRecesso += "Inicio de fabricação: " +
                                            ((DateTime)
                                                itemPedidoGeral.tbProdutos.tbProdutoFornecedor.dataInicioFabricacao).ToShortDateString();
                        int prazo = rnFuncoes.retornaDiasUteisPeriodo(DateTime.Now, (DateTime)itemPedidoGeral.tbProdutos.tbProdutoFornecedor.dataInicioFabricacao);
                        descricaoRecesso += " - Dias úteis até inicio: " + prazo.ToString();
                        prazo = prazo + prazoFornecedor;
                        descricaoRecesso += " - Prazo Final calculado: " + prazo.ToString();
                        log.descricoes.Add(descricaoRecesso);
                        prazoFornecedor = prazo;
                    }

                    int quantidadeTotalProduto = itensPedidoGeral.Count(x => x.produtoId == itemPedidoGeral.produtoId);

                    log.descricoes.Add("Estoque: " + estoqueItem + " - Quantidade total: " + quantidadeTotalProduto);

                    if (estoqueItem < quantidadeTotalProduto)
                    {
                        //int proximaLivre = RetornaDiasProximaEntregaLivreProduto(itemPedidoGeral.produtoId, quantidadeTotalProduto);
                        int proximaLivre = 0; // Lindsay pediu para não considerar o prazo da próxima entrega livre do fornecedor
                        if (proximaLivre > 0)
                        {
                            log.descricoes.Add("Proxima entrega em " + proximaLivre + " dias");
                            if (!itensComEstoque.Any(x => x.Key == itemPedidoGeral.itemPedidoId)) itensComEstoque.Add(itemPedidoGeral.itemPedidoId, itemPedidoGeral.produtoId);
                            if (proximaLivre > prazoPedidos && proximaLivre < prazoFornecedor)
                            {
                                prazoPedidos = proximaLivre;
                            }
                            else if (prazoFornecedor > prazoPedidos)
                            {
                                log.descricoes.Add("Prazo do fornecedor " + itemPedidoGeral.tbProdutos.tbProdutoFornecedor.fornecedorNome + " " + prazoFornecedor + " dias (maior que proxima entrega livre)");
                                prazoPedidos = prazoFornecedor;
                            }
                        }
                        else
                        {
                            log.descricoes.Add("Prazo do fornecedor " + itemPedidoGeral.tbProdutos.tbProdutoFornecedor.fornecedorNome + " " + prazoFornecedor + " dias");
                            if (prazoFornecedor > prazoPedidos)
                            {
                                prazoPedidos = prazoFornecedor;
                            }
                        }
                    }
                    else
                    {
                        if (!itensComEstoque.Any(x => x.Key == itemPedidoGeral.itemPedidoId)) itensComEstoque.Add(itemPedidoGeral.itemPedidoId, itemPedidoGeral.produtoId);
                    }
                }
                prazoPedidos = prazoPedidos + 1;
                log.descricoes.Add("Prazo do pedido:" + prazoPedidos + " dias");
                if (pedido.prazoMaximoPedidos == null && totalItens > 0)
                {
                    log.descricoes.Add("Gravando Prazo do Pedido");
                    pedido.prazoMaximoPedidos = prazoPedidos;
                    data.SaveChanges();
                }

                var prazoReserva = prazoPedidos == 0 ? DateTime.Now : DateTime.Now.AddDays(rnFuncoes.retornaPrazoDiasUteis(prazoPedidos, 0));
                foreach (var itemPedido in itensPedidoGeral)
                {
                    if (itemPedido.dataLimite == null)
                    {
                        int quantidadeTotalProduto = itensPedidoGeral.Count(x => x.produtoId == itemPedido.produtoId);
                        bool possuiEstoque =
                            itensComEstoque.Any(x => x.Key == itemPedido.itemPedidoId && x.Value == itemPedido.produtoId);
                        if (pedido.dataConfirmacaoPagamento == null)
                        {
                            if (possuiEstoque && pedido.tipoDePagamentoId != 11 && itemPedido.tbProdutos.naoReservarEstoque == false)
                            {
                                log.descricoes.Add("Item Possui Estoque. Adicionando à reserva antes de confirmar o pagamento");
                                itemPedido.dataLimite = prazoReserva;
                                itemPedido.prazoLimiteInicial = prazoReserva;
                            }
                        }
                        else
                        {
                            //int proximaLivre = RetornaDiasProximaEntregaLivreProduto(itemPedido.produtoId, quantidadeTotalProduto);
                            int proximaLivre = 0; // Lindsay pediu para não considerar o prazo da próxima entrega livre do fornecedor
                            /*if (proximaLivre > 0)
                            {
                                //itemPedido.dataLimite = DateTime.Now.AddDays(proximaLivre);
                                itemPedido.prazoLimiteInicial = DateTime.Now.AddDays(proximaLivre);
                            }
                            else
                            {
                                var prazoDiasProduto = rnFuncoes.retornaPrazoDiasUteis((itemPedido.tbProdutos.tbProdutoFornecedor.fornecedorPrazoDeFabricacao ?? 0), 0);
                                //itemPedido.dataLimite = itemPedido.dataCriacao.AddDays(prazoDiasProduto);
                                itemPedido.prazoLimiteInicial = DateTime.Now.AddDays(prazoDiasProduto);
                                //itemPedido.prazoLimiteInicial = itemPedido.dataCriacao.AddDays(prazoDiasProduto);
                            }*/
                            var prazoDiasProduto = rnFuncoes.retornaPrazoDiasUteis((itemPedido.tbProdutos.tbProdutoFornecedor.fornecedorPrazoDeFabricacao ?? 0), 0);
                            itemPedido.prazoLimiteInicial = DateTime.Now.AddDays(prazoDiasProduto);
                        }
                    }
                }
                foreach (var itemPedido in itensPedidoGeral)
                {
                    if (itemPedido.dataLimite == null && itemPedido.prazoLimiteInicial != null)
                    {
                        var prazoMaximo = itensPedidoGeral.Where(x => x.enviado == false && x.cancelado == false && x.reservado == false && x.prazoLimiteInicial != null).OrderByDescending(x => x.prazoLimiteInicial).FirstOrDefault();

                        if (itensComEstoque.Any())
                            prazoMaximo = itensPedidoGeral.Where(x => x.enviado == false && x.cancelado == false && x.reservado == false && x.prazoLimiteInicial != null && !itensComEstoque.Values.Contains(x.produtoId)).OrderByDescending(x => x.prazoLimiteInicial).FirstOrDefault();

                        itemPedido.dataLimite = prazoMaximo == null ? DateTime.Now : prazoMaximo.prazoLimiteInicial;
                    }
                    if (itemPedido.dataLimite != null && itemPedido.prazoLimiteInicial == null)
                    {
                        itemPedido.prazoLimiteInicial = itemPedido.dataLimite;
                    }
                }
                data.SaveChanges();


                if (pedido.prazoMaximoPedidos == null && totalItens > 0)
                {
                    pedido.prazoMaximoPedidos = prazoPedidos;
                    data.SaveChanges();
                }


                if (pedido.dataConfirmacaoPagamento != null && totalItens > 0)
                {
                    var itensPedidoComPrazos = (from c in data.tbItemPedidoEstoque where c.tbItensPedido.pedidoId == pedidoId where c.cancelado == false && c.enviado == false && c.reservado == false && c.dataLimite != null select c).ToList();
                    if (itensPedidoComPrazos.Count > 0)
                    {
                        var prazoMaximoAguardandoAtual = itensPedidoComPrazos.OrderByDescending(x => x.prazoLimiteInicial).First();
                        if (prazoMaximoAguardandoAtual.dataLimite.Value.Date > prazoMaximoAguardandoAtual.prazoLimiteInicial.Value.Date)
                        {
                            foreach (var itemComPrazo in itensPedidoComPrazos)
                            {
                                var prazoAtualDoItem = rnEstoque.RetornaPrazoPrevistoFornecedorAtual(itemComPrazo.idItemPedidoEstoque);
                                if (prazoAtualDoItem != null)
                                {
                                    if (prazoAtualDoItem < itemComPrazo.dataLimite)
                                    {
                                        itemComPrazo.dataLimite = prazoAtualDoItem;
                                    }
                                }
                            }
                            var maiorPrazoAtualizado = itensPedidoComPrazos.OrderByDescending(x => x.dataLimite).First().dataLimite.Value.Date;
                            if (maiorPrazoAtualizado < prazoMaximoAguardandoAtual.dataLimite.Value.Date)
                            {
                                foreach (var itemComPrazo in itensPedidoComPrazos)
                                {
                                    itemComPrazo.dataLimite = maiorPrazoAtualizado;
                                }
                            }

                        }
                    }
                }

                if ((pedido.dataConfirmacaoPagamento == null | pedido.prazoMaximoPedidos == null) && totalItens > 0)
                {
                    pedido.prazoMaximoPedidos = prazoPedidos;
                }
                else
                {
                    var prazoChecagemAlteracao = (pedido.dataHoraDoPedido ?? DateTime.Now).AddMinutes(10);
                    var itensAlterados =
                        (from c in data.tbItensPedido
                         where c.pedidoId == pedidoId && ((c.dataDaCriacao > pedido.dataConfirmacaoPagamento) | (c.cancelado ?? false) | (c.dataDaCriacao > prazoChecagemAlteracao))
                         select c).Any();
                    if ((itensAlterados) && totalItens > 0)
                    {
                        log.descricoes.Add("Pedido possui alteração nos itens");
                        var ultimoAguardando = (from c in data.tbItemPedidoEstoque
                                                where
                                                    c.tbItensPedido.tbPedidos.pedidoId == pedidoId && c.cancelado == false
                                                    //&& c.reservado == false 
                                                    && c.dataLimite != null
                                                orderby c.dataLimite descending
                                                select c).FirstOrDefault(); // Pegando sempre o prazo maior, independente de ser reservado ou não
                        if (ultimoAguardando != null)
                        {
                            log.descricoes.Add("Maior prazo: " + ultimoAguardando.tbProdutos.produtoNome + " - " + ultimoAguardando.dataLimite.Value.ToShortDateString());
                            int prazoAdicionar = rnFuncoes.retornaPrazoDiasUteis(1, 0, (DateTime)ultimoAguardando.dataLimite);
                            pedido.prazoMaximoPostagemAtualizado = ((DateTime)ultimoAguardando.dataLimite).AddDays(prazoAdicionar);
                        }
                        else
                        {
                            pedido.prazoMaximoPostagemAtualizado = pedido.prazoFinalPedido;
                        }
                    }
                    else
                    {
                        if (totalItens > 0) pedido.prazoMaximoPostagemAtualizado = pedido.prazoFinalPedido;
                    }

                }
                data.SaveChanges();
                log.InsereLog();
            }
            catch (Exception ex)
            {
                gravado = false;
            }

            if (gravado)
            {
                #region finalizaQueue

                var queueFinalizar = (from c in dataQueue.tbQueue where c.idQueue == idQueue select c).First();
                queueFinalizar.concluido = true;
                queueFinalizar.dataConclusao = DateTime.Now;
                dataQueue.SaveChanges();

                #endregion
            }
            else if (firstTry)
            {
                rnEmails.EnviaEmail("", "andre@bark.com.br", "", "", "", "Erro ao gerar fila de estoque pedido", queue.idQueue.ToString());
                #region retornaQueue
                var queueFinalizar = (from c in dataQueue.tbQueue where c.idQueue == idQueue select c).First();
                queueFinalizar.andamento = false;
                dataQueue.SaveChanges();
                #endregion
            }
        }

        private static int RetornaEstoqueLivreProduto(int produtoId)
        {
            var data = new dbSiteEntities();
            var aguardando = Convert.ToInt32(data.v1_ItensAguardandoEstoqueCount(produtoId).First());
            var estoque = Convert.ToInt32(data.v1_EstoqueFisicoCount(produtoId).First());
            //var aguardando = (from c in data.tbItemPedidoEstoque
            //                  where c.produtoId == produtoId && c.reservado == false && c.cancelado == false && c.dataLimite != null
            ////                  select c).Count();
            //var estoque = (from c in data.tbProdutoEstoque
            //               where c.pedidoIdReserva == null && c.produtoId == produtoId && c.enviado == false
            //               select c).Count();

            int estoqueLivre = estoque - aguardando;
            if (estoqueLivre < 0) estoqueLivre = 0;
            return estoqueLivre;
        }

        private static int RetornaDiasProximaEntregaLivreProduto(int produtoId, int quantidade)
        {
            var data = new dbSiteEntities();
            var aguardando = Convert.ToInt32(data.v1_ItensAguardandoEstoqueCount(produtoId).First()) + (quantidade - 1);
            var estoque = data.v1_DataEntregaPedidoFornecedorLivre(produtoId, aguardando).FirstOrDefault();
            //var estoque = (from c in data.tbPedidoFornecedorItem
            //               where c.entregue == false && c.idProduto == produtoId
            //               orderby c.tbPedidoFornecedor.dataLimite
            //               select c).Skip(aguardando).FirstOrDefault();
            int dia = 0;
            if (estoque != null)
            {
                var dataEntrega = Convert.ToDateTime(estoque);
                if (dataEntrega > DateTime.Now)
                {
                    dia = rnFuncoes.retornaDiasUteisPeriodo(DateTime.Now, dataEntrega);
                }
            }
            return dia;
        }


        private static void ReservarEstoquePedido(long idQueue)
        {
            #region atualizaQueue
            var dataQueue = new dbSiteEntities();
            var queue = (from c in dataQueue.tbQueue where c.idQueue == idQueue select c).First();
            bool firstTry = (queue.dataInicio == null);
            queue.andamento = true;
            queue.dataInicio = DateTime.Now;
            dataQueue.SaveChanges();
            #endregion

            bool gravado = true;

            try
            {
                var pedidoId = Convert.ToInt32(queue.idRelacionado);
                var data = new dbSiteEntities();
                var itensPedidoGeral = (from c in data.tbItemPedidoEstoque
                                        where
                                            c.tbItensPedido.pedidoId == pedidoId && c.cancelado == false && c.enviado == false
                                        select c.produtoId
                    ).ToList();
                foreach (var itemPedidoGeral in itensPedidoGeral)
                {
                    AdicionaQueueReservarProduto(itemPedidoGeral);
                }

            }
            catch (Exception)
            {
                gravado = false;
            }

            if (gravado)
            {
                #region finalizaQueue

                var queueFinalizar = (from c in dataQueue.tbQueue where c.idQueue == idQueue select c).First();
                queueFinalizar.concluido = true;
                queueFinalizar.dataConclusao = DateTime.Now;
                dataQueue.SaveChanges();

                #endregion
            }
            else if (firstTry)
            {
                rnEmails.EnviaEmail("", "andre@bark.com.br", "", "", "", "Erro ao gerar fila de estoque pedido", queue.idQueue.ToString());
                #region retornaQueue
                var queueFinalizar = (from c in dataQueue.tbQueue where c.idQueue == idQueue select c).First();
                queueFinalizar.andamento = false;
                dataQueue.SaveChanges();
                #endregion
            }
        }

        #region pedidosAoFornecedor


        private void bwPedidoFornecedor_DoWork(object sender, DoWorkEventArgs e)
        {
            #region atualizaQueue
            var data = new dbSiteEntities();
            int idQueue = Convert.ToInt32(e.Argument);
            var queue = (from c in data.tbQueue where c.idQueue == idQueue select c).First();
            queue.andamento = true;
            queue.dataInicio = DateTime.Now;
            data.SaveChanges();
            #endregion atualizaQueue

            int idFornecedorFiltro = 0;
            if (queue.idRelacionado != null)
            {
                try
                {
                    idFornecedorFiltro = Convert.ToInt32(queue.idRelacionado);
                }
                catch (Exception ex) { }
            }
            var todosFornecedores = (from c in data.tbProdutoFornecedor select c).ToList();
            alteraLabelStatusPedido("Obtendo Lista Estoque");

            alteraLabelStatusPedido("Carregando Itens aguardando Estoque");
            var itensAguardando = (from c in data.tbItemPedidoEstoque
                                   where c.reservado == false && c.cancelado == false &&
                                   c.dataLimite != null
                                   select new
                                   {
                                       c.produtoId,
                                       c.tbProdutos.produtoFornecedor,
                                       c.tbProdutos.produtoPrecoDeCusto,
                                       c.dataLimite,
                                       c.itemPedidoId,
                                       c.tbItensPedido.tbProdutos.compraComboCompleto,
                                       c.idItemPedidoEstoque,
                                       c.tbItensPedido.pedidoId,
                                       c.tbItensPedido.tbPedidos.dataHoraDoPedido,
                                       c.precoDeCusto
                                   }).ToList().OrderBy(x => x.dataLimite);

            int totalItensPedido = itensAguardando.Count();
            int itemPedidoAtual = 0;

            var fornecedoresEstoque = (from c in todosFornecedores where c.gerarPedido == false select c).ToList();
            var feriado = (from c in data.tbFeriados where c.data.Year == DateTime.Now.Year && c.data.Month == DateTime.Now.Month && c.data.Day == DateTime.Now.Day select c).Any();
            if (!feriado)
            {
                int diaDaSemana = (int)DateTime.Now.DayOfWeek;
                var fornecedores = (from c in todosFornecedores where c.gerarPedido == true select c).ToList();
                if (diaDaSemana == 0) fornecedores = fornecedores.Where(x => x.pedidoDomingo).ToList();
                if (diaDaSemana == 1) fornecedores = fornecedores.Where(x => x.pedidoSegunda).ToList();
                if (diaDaSemana == 2) fornecedores = fornecedores.Where(x => x.pedidoTerca).ToList();
                if (diaDaSemana == 3) fornecedores = fornecedores.Where(x => x.pedidoQuarta).ToList();
                if (diaDaSemana == 4) fornecedores = fornecedores.Where(x => x.pedidoQuinta).ToList();
                if (diaDaSemana == 5) fornecedores = fornecedores.Where(x => x.pedidoSexta).ToList();
                if (diaDaSemana == 6) fornecedores = fornecedores.Where(x => x.pedidoSabado).ToList();

                fornecedores = fornecedores.Where(x => (((x.dataInicioFabricacao ?? DateTime.Now.AddDays(-1)) > DateTime.Now && (x.dataUltimoRomaneio ?? DateTime.Now.AddDays(1)) < DateTime.Now) ? false : true) == true).ToList();
                //fornecedores = fornecedores.Where(x => x.fornecedorId == 0).ToList();
                if (idFornecedorFiltro > 0)
                {
                    fornecedores = fornecedores.Where(x => x.fornecedorId == idFornecedorFiltro).ToList();
                }
                //fornecedores = fornecedores.Where(x => x.fornecedorId == 82 | x.fornecedorId == 81).ToList();



                #region Gera Pedidos

                var pedidosFornecedorPendentes = new List<tbPedidoFornecedor>();
                foreach (var item in itensAguardando)
                {
                    itemPedidoAtual++;
                    alteraLabelStatusPedido("Gerando item: " + itemPedidoAtual + "/" + totalItensPedido);
                    var gerarPedidoFornecedor = fornecedores.Any(x => x.fornecedorId == item.produtoFornecedor);

                    if (gerarPedidoFornecedor)
                    {
                        //var fornecedor = fornecedores.FirstOrDefault(c => c.fornecedorId == 72);

                        //if (item.produtoFornecedor == 72)
                        //{
                        //    //if (!(DateTime.Now.Day == 1 | DateTime.Now.Day == 15) && !(DateTime.Now.Day == 4 && DateTime.Now.Month == 1 && DateTime.Now.Year == 2016))
                        //    if (!(DateTime.Now.Day == 1 | DateTime.Now.Day == 15))
                        //    {
                        //        gerarPedidoFornecedor = false;
                        //    }
                        //}
                        //var fornecedor = fornecedores.First(x => x.fornecedorId == item.produtoFornecedor);
                        //var inicioFabricacaoFornecedor = fornecedor.dataInicioFabricacao == null ? DateTime.Now : (DateTime)fornecedor.dataInicioFabricacao;
                        //if (fornecedor.dataFimFabricacao != null)
                        //{
                        //    if (((DateTime)fornecedor.dataFimFabricacao).Date < DateTime.Now.Date && inicioFabricacaoFornecedor.Date > DateTime.Now)
                        //    {
                        //        gerarPedidoFornecedor = false;
                        //    }
                        //}
                    }

                    var fornecedorEstoque = fornecedoresEstoque.Any(x => x.fornecedorId == item.produtoFornecedor);
                    var pedidoFornecedorPendente = pedidosFornecedorPendentes.FirstOrDefault(x => x.idFornecedor == item.produtoFornecedor);


                    if (gerarPedidoFornecedor && !fornecedorEstoque)
                    {
                        var totalItensNaoEntregues = (from c in data.tbPedidoFornecedorItem
                                                      where c.idProduto == item.produtoId && c.entregue == false
                                                      select c).Count();

                        var totalItensAguardandoLiberacao = (from c in data.tbProdutoEstoque
                                                             where c.produtoId == item.produtoId && (c.liberado ?? false) == false
                                                                   && c.enviado == false && c.pedidoId == null && c.pedidoIdReserva == null
                                                             select c).Count();

                        var totalItensAguardandoReserva = (from c in data.tbItemPedidoEstoque
                                                           where c.produtoId == item.produtoId
                                                                 && c.reservado == false && c.cancelado == false && c.dataLimite != null
                                                           select c).Count();

                        int totalNecessario = totalItensAguardandoReserva - totalItensNaoEntregues -
                                              totalItensAguardandoLiberacao;

                        if (totalNecessario > 0)
                        {
                            #region gera pedido pendente ao fornecedor
                            if (pedidoFornecedorPendente == null)
                            {
                                pedidoFornecedorPendente = retornaPedidoFornecedorPendente(item.produtoFornecedor,
                                    todosFornecedores);
                                pedidosFornecedorPendentes.Add(pedidoFornecedorPendente);
                            }
                            #endregion

                            //for (int i = 1; i <= totalNecessario; i++)
                            //{
                            bool gerarProduto = true;
                            if (item.produtoFornecedor == 82)
                            {
                                var totalItensNoRomaneio =
                                    (from c in data.tbPedidoFornecedorItem
                                     where c.idPedidoFornecedor == pedidoFornecedorPendente.idPedidoFornecedor
                                     select c).Count();
                                if (totalItensNoRomaneio >= 50000) gerarProduto = false;
                            }
                            if (gerarProduto)
                            {
                                alteraLabelStatusPedido("Adicionando a pedido fornecedor: " + itemPedidoAtual + "/" + totalItensPedido);
                                var pedidoFornecedorItem = new tbPedidoFornecedorItem();
                                //pedidoFornecedorItem.custo = (decimal)item.produtoPrecoDeCusto;
                                pedidoFornecedorItem.custo = Math.Round(((item.precoDeCusto ?? 0) == 0 ? 0 : (decimal)item.produtoPrecoDeCusto), 2);
                                pedidoFornecedorItem.brinde = false;
                                pedidoFornecedorItem.diferenca = 0;
                                pedidoFornecedorItem.idPedidoFornecedor = pedidoFornecedorPendente.idPedidoFornecedor;
                                pedidoFornecedorItem.idProduto = item.produtoId;
                                pedidoFornecedorItem.entregue = false;
                                //pedidoFornecedorItem.idPedido = item.pedidoId;
                                //pedidoFornecedorItem.idItemPedido = item.itemPedidoId;
                                pedidoFornecedorItem.quantidade = 1;
                                pedidoFornecedorItem.idProcessoFabrica = 7;

                                pedidoFornecedorItem.pedidosPendentes = totalItensAguardandoReserva;
                                pedidoFornecedorItem.produtosNaoEnderecados = totalItensAguardandoLiberacao;
                                pedidoFornecedorItem.encomendasNaoEntregues = totalItensNaoEntregues;


                                var dataAdd = new dbSiteEntities();
                                dataAdd.tbPedidoFornecedorItem.Add(pedidoFornecedorItem);
                                dataAdd.SaveChanges();

                                if (item.compraComboCompleto)
                                {
                                    var itensDoCombo = (from c in data.tbItemPedidoEstoque
                                                        where
                                                            c.itemPedidoId == item.itemPedidoId &&
                                                            c.tbProdutos.produtoFornecedor == item.produtoFornecedor && c.idItemPedidoEstoque != item.idItemPedidoEstoque
                                                        select new
                                                        {
                                                            c.produtoId,
                                                            c.tbProdutos.produtoPrecoDeCusto,
                                                            c.precoDeCusto
                                                        }).ToList();
                                    foreach (var itemDoCombo in itensDoCombo)
                                    {
                                        var pedidoFornecedorItemCombo = new tbPedidoFornecedorItem();
                                        //pedidoFornecedorItemCombo.custo = (decimal)itemDoCombo.produtoPrecoDeCusto;
                                        pedidoFornecedorItemCombo.custo = (decimal)itemDoCombo.precoDeCusto;
                                        pedidoFornecedorItemCombo.brinde = false;
                                        pedidoFornecedorItemCombo.diferenca = 0;
                                        pedidoFornecedorItemCombo.idPedidoFornecedor = pedidoFornecedorPendente.idPedidoFornecedor;
                                        pedidoFornecedorItemCombo.idProduto = itemDoCombo.produtoId;
                                        pedidoFornecedorItemCombo.entregue = false;
                                        pedidoFornecedorItemCombo.quantidade = 1;
                                        pedidoFornecedorItemCombo.idProcessoFabrica = 7;
                                        pedidoFornecedorItemCombo.motivoAdicao = "Adicionado combo completo do produto";
                                        dataAdd.tbPedidoFornecedorItem.Add(pedidoFornecedorItemCombo);
                                        dataAdd.SaveChanges();
                                    }
                                }
                            }
                            //}
                        }
                    }
                }

                #endregion

                int totalFornecedoresPendentes = pedidosFornecedorPendentes.Count();
                int fornecedorPendenteAtual = 0;
                foreach (var pedidoFornecedorPendentes in pedidosFornecedorPendentes)
                {
                    fornecedorPendenteAtual++;
                    alteraLabelStatusPedido("Fechando pedido ao fornecedor: " + fornecedorPendenteAtual + "/" + totalFornecedoresPendentes);
                    fechaPedidoFornecedor(pedidoFornecedorPendentes.idFornecedor);
                }

                var pedidosGeral = (from c in itensAguardando select new { c.pedidoId, c.dataHoraDoPedido }).Distinct().ToList();

                int pedidosPrazoPendentes = pedidosGeral.Count();
                int pedidosPrazoAtual = 0;
                foreach (var pedido in pedidosGeral)
                {
                    pedidosPrazoAtual++;
                    var agora = DateTime.Now;
                    alteraLabelStatusPedido("Verificando se necessita atualziar prazo: " + pedidosPrazoAtual + "/" + pedidosPrazoPendentes);
                    try
                    {
                        var itens = (from c in data.tbItensPedido
                                     where c.pedidoId == pedido.pedidoId
                                     select c
                            ).ToList();
                        var pedidoAlterado = (from c in itens
                                              where c.pedidoId == pedido.pedidoId &&
                                     ((c.cancelado ?? false) == true | c.dataDaCriacao > (c.tbPedidos.dataHoraDoPedido ?? agora).AddMinutes(2))
                                              select c
                            ).Any();
                        if (pedidoAlterado)
                        {
                            AdicionaQueueAtualizaPrazo(pedido.pedidoId);
                        }
                    }
                    catch (Exception ex)
                    {
                        //MessageBox.Show(ex.ToString());
                    }
                }
                alteraPedidosSeparacaoEstoque();
            }
            #region finalizaQueue
            queue.concluido = true;
            queue.andamento = true;
            queue.dataConclusao = DateTime.Now;
            data.SaveChanges();

            if (idFornecedorFiltro == 0)
            {
                var novoQueue = new tbQueue();
                novoQueue.agendamento = Convert.ToDateTime(DateTime.Now.AddDays(1).ToShortDateString() + " 06:00:00");
                novoQueue.tipoQueue = 1;
                novoQueue.mensagem = "";
                novoQueue.concluido = false;
                novoQueue.andamento = false;
                data.tbQueue.Add(novoQueue);
                data.SaveChanges();
            }
            #endregion
        }
        private void alteraPedidosSeparacaoEstoque()
        {
            var data = new dbSiteEntities();
            var pedidos = (from c in data.tbPedidos where c.statusDoPedido == 3 select c).ToList();

            int totalPedidos = pedidos.Count();
            int pedidoAtual = 0;

            foreach (var pedido in pedidos)
            {
                bool alterar = true;
                pedidoAtual++;
                alteraLabelStatusPedido("Alterando status pedido: " + pedidoAtual + "/" + totalPedidos);
                var produtosAguardandoEstoque = (from c in data.tbItemPedidoEstoque
                                                 where
                                                     c.dataLimite != null && c.cancelado == false && c.reservado == false &&
                                                     c.tbItensPedido.pedidoId == pedido.pedidoId
                                                 select c).ToList();
                if (produtosAguardandoEstoque.Any())
                {
                    //var aguardandoGeral = (from c in data.tbItemPedidoEstoque
                    //                       where
                    //                           produtosAguardandoEstoque.Select(x => x.produtoId).Contains(c.produtoId) &&
                    //                           c.reservado == false &&
                    //                           c.cancelado == false && c.dataLimite != null
                    //                       orderby c.dataLimite
                    //                       select new
                    //                       {
                    //                           c.idItemPedidoEstoque,
                    //                           c.tbItensPedido.pedidoId,
                    //                           c.dataCriacao,
                    //                           dataLimiteReserva = c.dataLimite,
                    //                           c.produtoId,
                    //                           c.itemPedidoId
                    //                       }).ToList();
                    //var pedidosFornecedorGeral = (from c in data.tbPedidoFornecedorItem
                    //                              join d in data.tbProdutoFornecedor on c.tbPedidoFornecedor.idFornecedor equals d.fornecedorId
                    //                              where
                    //                                  c.entregue == false &&
                    //                                  produtosAguardandoEstoque.Select(x => x.produtoId).Contains(c.idProduto)
                    //                              orderby c.tbPedidoFornecedor.dataLimite, c.tbPedidoFornecedor.idPedidoFornecedor
                    //                              select new
                    //                              {
                    //                                  c.idPedidoFornecedor,
                    //                                  c.idPedidoFornecedorItem,
                    //                                  dataLimiteFornecedor = c.tbPedidoFornecedor.dataLimite,
                    //                                  c.idProduto,
                    //                                  d.fornecedorNome
                    //                              }).ToList();
                    foreach (var produtoAguardandoEstoque in produtosAguardandoEstoque)
                    {
                        var aguardando = (from c in data.tbItemPedidoEstoque
                                          where
                                              c.reservado == false &&
                                              c.cancelado == false && c.dataLimite != null
                                                && c.produtoId == produtoAguardandoEstoque.produtoId
                                          orderby c.dataLimite
                                          select new
                                          {
                                              c.idItemPedidoEstoque,
                                              c.tbItensPedido.pedidoId,
                                              c.dataCriacao,
                                              dataLimiteReserva = c.dataLimite,
                                              c.produtoId,
                                              c.itemPedidoId
                                          }).ToList().OrderBy(x => x.dataLimiteReserva).ToList();
                        var pedidoFornecedor = (from c in data.tbPedidoFornecedorItem
                                                join d in data.tbProdutoFornecedor on c.tbPedidoFornecedor.idFornecedor equals
                                                    d.fornecedorId
                                                where
                                                    (c.entregue == false | (c.entregue == true && (c.liberado ?? false) == false)) &&
                                                    c.idProduto == produtoAguardandoEstoque.produtoId
                                                orderby c.tbPedidoFornecedor.dataLimite, c.tbPedidoFornecedor.idPedidoFornecedor
                                                select new
                                                {
                                                    c.idPedidoFornecedor,
                                                    c.idPedidoFornecedorItem,
                                                    dataLimiteFornecedor = c.tbPedidoFornecedor.dataLimite,
                                                    c.idProduto,
                                                    d.fornecedorNome
                                                }).ToList().OrderBy(x => x.dataLimiteFornecedor).ThenBy(x => x.idPedidoFornecedor).ToList();
                        var listaFinal = (from c in aguardando.Select((item, index) => new { item, index })
                                          join d in pedidoFornecedor.Select((item, index) => new { item, index }) on c.index equals
                                              d.index
                                              into
                                              pedidosFornecedorListaInterna
                                          from e in pedidosFornecedorListaInterna.DefaultIfEmpty()
                                          select new
                                          {
                                              c.item.idItemPedidoEstoque,
                                              c.item.pedidoId,
                                              c.item.dataCriacao,
                                              c.item.dataLimiteReserva,
                                              c.item.itemPedidoId,
                                              idPedidoFornecedor = e != null ? e.item.idPedidoFornecedor : 0,
                                              dataLimiteFornecedor = e != null ? (DateTime?)e.item.dataLimiteFornecedor : null,
                                              idPedidoFornecedorItem = e != null ? (int?)e.item.idPedidoFornecedorItem : null,
                                              fornecedorNome = e != null ? e.item.fornecedorNome : ""
                                          }
                            ).ToList();
                        var itemNaLista =
                            listaFinal.FirstOrDefault(
                                x => x.idItemPedidoEstoque == produtoAguardandoEstoque.idItemPedidoEstoque);
                        if (itemNaLista == null)
                        {
                            alterar = false;
                        }
                        else
                        {
                            if (itemNaLista.idPedidoFornecedor == 0) alterar = false;
                        }
                    }
                }

                if (alterar)
                {
                    var pedidoCheck = (from c in data.tbPedidos where c.pedidoId == pedido.pedidoId select c).First();
                    if (pedidoCheck.statusDoPedido == 3)
                    {
                        try
                        {
                            rnPedidos.pedidoAlteraStatus(11, pedidoCheck.pedidoId);
                            rnInteracoes.interacaoInclui(pedidoCheck.pedidoId, "Separação de Estoque", "Sistema", "True");
                        }
                        catch (Exception ex)
                        {

                        }
                    }
                }
            }
        }


        //private void bwPedidoFornecedor_DoWork(object sender, DoWorkEventArgs e)
        //{
        //    #region atualizaQueue
        //    var data = new dbSiteEntities();
        //    int idQueue = Convert.ToInt32(e.Argument);
        //    var queue = (from c in data.tbQueue where c.idQueue == idQueue select c).First();
        //    queue.andamento = true;
        //    queue.dataInicio = DateTime.Now;
        //    data.SaveChanges();
        //    #endregion atualizaQueue

        //    var todosFornecedores = (from c in data.tbProdutoFornecedor select c).ToList();
        //    alteraLabelStatusPedido("Obtendo Lista Estoque");
        //    var listaEstoque = data.site_listaProdutosEmEstoque().ToList();
        //    //var pedidos = (from c in data.tbPedidos where (c.statusDoPedido == 3 | (c.statusDoPedido == 2 && c.tipoDePagamentoId == 11)) select c).ToList();            

        //    alteraLabelStatusPedido("Carregando Itens do Pedido");
        //    var tbItensPedido = (from c in data.tbItensPedido
        //                         join d in data.tbItensPedidoCombo on c.itemPedidoId equals d.idItemPedido into combo
        //                         where !combo.Any() && (c.tbPedidos.statusDoPedido == 3 | (c.tbPedidos.statusDoPedido == 2 && c.tbPedidos.tipoDePagamentoId == 11))
        //                         select new
        //                         {
        //                             c.itemPedidoId,
        //                             c.produtoId,
        //                             c.tbProdutos.produtoFornecedor,
        //                             c.tbPedidos.tipoDePagamentoId,
        //                             c.tbPedidos.pedidoId,
        //                             c.tbPedidos.statusDoPedido,
        //                             c.cancelado,
        //                             c.itemQuantidade,
        //                             c.tbProdutos.produtoPrecoDeCusto
        //                         }).ToList();

        //    alteraLabelStatusPedido("Carregando Itens do Pedido Combo");
        //    var tbItensPedidoCombo = (from c in data.tbItensPedidoCombo
        //                              where (c.tbItensPedido.tbPedidos.statusDoPedido == 3 | (c.tbItensPedido.tbPedidos.statusDoPedido == 2 && c.tbItensPedido.tbPedidos.tipoDePagamentoId == 11))
        //                              select new
        //                              {
        //                                  itemPedidoId = c.idItemPedido,
        //                                  c.produtoId,
        //                                  c.tbProdutos.produtoFornecedor,
        //                                  c.tbItensPedido.tbPedidos.tipoDePagamentoId,
        //                                  c.tbItensPedido.tbPedidos.pedidoId,
        //                                  c.tbItensPedido.tbPedidos.statusDoPedido,
        //                                  c.tbItensPedido.cancelado,
        //                                  c.tbItensPedido.itemQuantidade,
        //                                  c.tbProdutos.produtoPrecoDeCusto
        //                              }).ToList();
        //    var itensPedidoGeral = tbItensPedido.Union(tbItensPedidoCombo).ToList();
        //    var pedidos = itensPedidoGeral.Select(x => x.pedidoId).Distinct();
        //    var tbPagamentos = (from c in data.tbPedidoPagamento where (c.tbPedidos.statusDoPedido == 3 | (c.tbPedidos.statusDoPedido == 2 && c.tbPedidos.tipoDePagamentoId == 11)) select c).ToList();

        //    int totalItensPedido = itensPedidoGeral.Count();
        //    int itemPedidoAtual = 0;
        //    #region reserva estoque
        //    alteraLabelStatusPedido("Verificando itens aguardando estoque");
        //    var produtosAguardandoEstoque = (from c in data.tbItemPedidoAguardandoEstoque where c.reservado == false && (c.tbItensPedido.cancelado ?? false) == false select c).ToList();
        //    foreach (var aguardandoEstoque in produtosAguardandoEstoque)
        //    {
        //        var estoqueDoItem = data.admin_produtoEmEstoque(aguardandoEstoque.idProduto).FirstOrDefault();
        //        int estoqueTotalDoItem = estoqueDoItem == null ? 0 : Convert.ToInt32(estoqueDoItem.estoqueLivre);
        //        if (estoqueTotalDoItem >= 1)
        //        {
        //            var reserva = new tbProdutoReservaEstoque();
        //            reserva.idPedido = aguardandoEstoque.tbItensPedido.pedidoId;
        //            reserva.idProduto = aguardandoEstoque.idProduto;
        //            reserva.dataHora = DateTime.Now;
        //            reserva.idItemPedido = aguardandoEstoque.idItemPedido;
        //            reserva.quantidade = 1;
        //            data.tbProdutoReservaEstoque.Add(reserva);

        //            aguardandoEstoque.dataReserva = DateTime.Now;
        //            aguardandoEstoque.reservado = true;
        //            data.SaveChanges();
        //        }
        //    }
        //    var fornecedoresEstoque = (from c in todosFornecedores where c.gerarPedido == false select c).ToList();

        //    #endregion

        //    var feriado = (from c in data.tbFeriados where c.data.Year == DateTime.Now.Year && c.data.Month == DateTime.Now.Month && c.data.Day == DateTime.Now.Day select c).Any();
        //    if (!feriado)
        //    {
        //        int diaDaSemana = (int)DateTime.Now.DayOfWeek;
        //        var fornecedores = (from c in todosFornecedores where c.gerarPedido == true select c).ToList();
        //        if (diaDaSemana == 0) fornecedores = fornecedores.Where(x => x.pedidoDomingo).ToList();
        //        if (diaDaSemana == 1) fornecedores = fornecedores.Where(x => x.pedidoSegunda).ToList();
        //        if (diaDaSemana == 2) fornecedores = fornecedores.Where(x => x.pedidoTerca).ToList();
        //        if (diaDaSemana == 3) fornecedores = fornecedores.Where(x => x.pedidoQuarta).ToList();
        //        if (diaDaSemana == 4) fornecedores = fornecedores.Where(x => x.pedidoQuinta).ToList();
        //        if (diaDaSemana == 5) fornecedores = fornecedores.Where(x => x.pedidoSexta).ToList();
        //        if (diaDaSemana == 6) fornecedores = fornecedores.Where(x => x.pedidoSabado).ToList();
        //        //fornecedores = fornecedores.Where(x => x.fornecedorId == 0).ToList();

        //        #region Gera Pedidos

        //        var pedidosFornecedorPendentes = new List<tbPedidoFornecedor>();
        //        //itensPedidoGeral = itensPedidoGeral.Where(x => x.produtoId == 26818).ToList();
        //        foreach (var item in itensPedidoGeral)
        //        {
        //            if (item.pedidoId == 87599)
        //            {
        //                var teste = "";
        //            }
        //            itemPedidoAtual++;
        //            alteraLabelStatusPedido("Gerando item: " + itemPedidoAtual + "/" + totalItensPedido);
        //            var gerarPedidoFornecedor = fornecedores.Any(x => x.fornecedorId == item.produtoFornecedor);

        //            if (gerarPedidoFornecedor)
        //            {
        //                var fornecedor = fornecedores.First(x => x.fornecedorId == item.produtoFornecedor);
        //                var inicioFabricacaoFornecedor = fornecedor.dataInicioFabricacao == null ? DateTime.Now : (DateTime)fornecedor.dataInicioFabricacao;
        //                if (fornecedor.dataFimFabricacao != null)
        //                {
        //                    if (((DateTime)fornecedor.dataFimFabricacao).Date < DateTime.Now.Date && inicioFabricacaoFornecedor.Date > DateTime.Now)
        //                    {
        //                        gerarPedidoFornecedor = false;
        //                    }
        //                }
        //            }

        //            var fornecedorEstoque = fornecedoresEstoque.Any(x => x.fornecedorId == item.produtoFornecedor);
        //            var gerarPedidoItem = false;

        //            var dataLimiteAtual = retornaDataLimitePedido(item.produtoFornecedor, todosFornecedores);
        //            var dataLimiteProximo = retornaDataLimiteProximoPedido(item.produtoFornecedor, todosFornecedores);

        //            #region verifica plano maternidade
        //            if (item.tipoDePagamentoId == 11)
        //            {
        //                var pagamentos = (from c in tbPagamentos
        //                                  where
        //                                      c.pedidoId == item.pedidoId &&
        //                                      c.cancelado == false
        //                                  orderby c.dataVencimento descending
        //                                  select c).ToList();
        //                if (pagamentos.Any(x => x.pago))
        //                {
        //                    var ultimoVencimento = pagamentos.Where(x => x.pago == false).FirstOrDefault();
        //                    if (ultimoVencimento != null)
        //                    {
        //                        if (ultimoVencimento.dataVencimento.AddDays(-1).Date <= dataLimiteAtual.Date)
        //                        {
        //                            gerarPedidoItem = true;
        //                        }
        //                        else if (ultimoVencimento.dataVencimento.AddDays(-1).Date <= dataLimiteProximo)
        //                        {
        //                            gerarPedidoItem = true;
        //                        }
        //                    }
        //                    else
        //                    {
        //                        gerarPedidoItem = true;
        //                    }
        //                }
        //                if (item.statusDoPedido == 3)
        //                {
        //                    gerarPedidoItem = true;
        //                }
        //            }
        //            else
        //            {
        //                gerarPedidoItem = true;
        //            }
        //            #endregion


        //            var pedidoFornecedorPendente = pedidosFornecedorPendentes.FirstOrDefault(x => x.idFornecedor == item.produtoFornecedor);


        //            bool cancelado = item.cancelado == null ? false : (bool)item.cancelado;
        //            if (cancelado == false && gerarPedidoItem == true)
        //            {
        //                int itemPedidoQtdGeral =
        //                    (from c in itensPedidoGeral
        //                     where c.itemPedidoId == item.itemPedidoId && c.produtoId == item.produtoId
        //                     select c).Count();

        //                if (itemPedidoQtdGeral <= 0)
        //                {
        //                    itemPedidoQtdGeral = 1;
        //                }

        //                itemPedidoQtdGeral = itemPedidoQtdGeral * Convert.ToInt32(item.itemQuantidade);

        //                for (int i = 1; i <= itemPedidoQtdGeral; i++)
        //                {
        //                    alteraLabelStatusPedido("Checando se possui pedido: " + itemPedidoAtual + "/" + totalItensPedido);
        //                    var checagemProduto = (from c in data.tbPedidoFornecedorItem where c.idProduto == item.produtoId && c.idItemPedido == item.itemPedidoId select c).Count();
        //                    alteraLabelStatusPedido("Checando se possui reserva: " + itemPedidoAtual + "/" + totalItensPedido);
        //                    var checagemProdutoFilhoEstoque = (from c in data.tbProdutoReservaEstoque where c.idProduto == item.produtoId && c.idItemPedido == item.itemPedidoId select c).Count();
        //                    if ((checagemProduto + checagemProdutoFilhoEstoque) < itemPedidoQtdGeral)
        //                    {
        //                        alteraLabelStatusPedido("Checando estoque lista: " + itemPedidoAtual + "/" + totalItensPedido);
        //                        var estoqueDoItemLista = listaEstoque.FirstOrDefault(x => x.produtoId == item.produtoId);
        //                        //var estoqueDoItem = data.admin_produtoEmEstoque(item.produtoId).FirstOrDefault();
        //                        int estoqueTotalDoItem = estoqueDoItemLista == null ? 0 : Convert.ToInt32(estoqueDoItemLista.estoqueLivre);
        //                        alteraLabelStatusPedido("Checando pedido sem vinculo: " + itemPedidoAtual + "/" + totalItensPedido);
        //                        var pedidosFornecedorSemVinculo =
        //                                (from c in data.tbPedidoFornecedorItem
        //                                 where
        //                                     c.entregue == false && c.idPedido == null &&
        //                                     c.idItemPedido == null && c.idProduto == item.produtoId
        //                                 orderby c.tbPedidoFornecedor.dataLimite
        //                                 select c).FirstOrDefault();
        //                        if (estoqueTotalDoItem >= 1)
        //                        {
        //                            alteraLabelStatusPedido("Checando estoque banco: " + itemPedidoAtual + "/" +
        //                                                    totalItensPedido);
        //                            var estoqueDoItem = data.admin_produtoEmEstoque(item.produtoId).FirstOrDefault();
        //                            estoqueTotalDoItem = estoqueDoItem == null
        //                                ? 0
        //                                : Convert.ToInt32(estoqueDoItem.estoqueLivre);
        //                        }
        //                        if (estoqueTotalDoItem >= 1)
        //                        {
        //                            alteraLabelStatusPedido("Reservando estoque: " + itemPedidoAtual + "/" + totalItensPedido);
        //                            var reserva = new tbProdutoReservaEstoque();
        //                            reserva.idPedido = item.pedidoId;
        //                            reserva.idProduto = item.produtoId;
        //                            reserva.dataHora = DateTime.Now;
        //                            reserva.idItemPedido = item.itemPedidoId;
        //                            reserva.quantidade = 1;
        //                            var dataAdd = new dbSiteEntities();
        //                            dataAdd.tbProdutoReservaEstoque.Add(reserva);
        //                            dataAdd.SaveChanges();
        //                            alteraLabelStatusPedido("Atualizando lista estoque: " + itemPedidoAtual + "/" + totalItensPedido);
        //                            listaEstoque = dataAdd.site_listaProdutosEmEstoque().ToList();

        //                            dataAdd.SaveChanges();
        //                            dataAdd.Dispose();
        //                        }
        //                        else if (pedidosFornecedorSemVinculo != null)
        //                        {
        //                            var dataAdd = new dbSiteEntities();
        //                            var pedidoSemVinculoAlterar = (from c in dataAdd.tbPedidoFornecedorItem
        //                                                           where
        //                                                               c.idPedidoFornecedorItem ==
        //                                                               pedidosFornecedorSemVinculo.idPedidoFornecedorItem
        //                                                           select c).First();

        //                            alteraLabelStatusPedido("Vinculando em pedido existente: " + itemPedidoAtual + "/" + totalItensPedido);
        //                            pedidoSemVinculoAlterar.idItemPedido = item.itemPedidoId;
        //                            pedidoSemVinculoAlterar.idPedido = item.pedidoId;

        //                            var produtoAguardandoEstoque = (from c in dataAdd.tbItemPedidoAguardandoEstoque
        //                                                            where
        //                                                                c.idItemPedido == item.itemPedidoId &&
        //                                                                c.idProduto == item.produtoId && c.reservado == false
        //                                                            select c).FirstOrDefault();
        //                            if (produtoAguardandoEstoque != null)
        //                            {
        //                                produtoAguardandoEstoque.reservado = true;
        //                                produtoAguardandoEstoque.dataReserva = DateTime.Now;
        //                            }
        //                            dataAdd.SaveChanges();
        //                            dataAdd.Dispose();
        //                        }
        //                        else
        //                        {
        //                            if (!fornecedorEstoque && gerarPedidoFornecedor)
        //                            {
        //                                #region gera pedido pendente ao fornecedor
        //                                if (gerarPedidoFornecedor && gerarPedidoItem)
        //                                {
        //                                    if (pedidoFornecedorPendente == null)
        //                                    {
        //                                        pedidoFornecedorPendente = retornaPedidoFornecedorPendente(item.produtoFornecedor, todosFornecedores);
        //                                        pedidosFornecedorPendentes.Add(pedidoFornecedorPendente);
        //                                    }
        //                                }
        //                                #endregion

        //                                alteraLabelStatusPedido("Adicionando a pedido fornecedor: " + itemPedidoAtual + "/" + totalItensPedido);
        //                                var pedidoFornecedorItem = new tbPedidoFornecedorItem();
        //                                pedidoFornecedorItem.custo = (decimal)item.produtoPrecoDeCusto;
        //                                pedidoFornecedorItem.brinde = false;
        //                                pedidoFornecedorItem.diferenca = 0;
        //                                pedidoFornecedorItem.idPedidoFornecedor = pedidoFornecedorPendente.idPedidoFornecedor;
        //                                pedidoFornecedorItem.idProduto = item.produtoId;
        //                                pedidoFornecedorItem.entregue = false;
        //                                pedidoFornecedorItem.idPedido = item.pedidoId;
        //                                pedidoFornecedorItem.idItemPedido = item.itemPedidoId;
        //                                pedidoFornecedorItem.quantidade = 1;
        //                                var dataAdd = new dbSiteEntities();
        //                                dataAdd.tbPedidoFornecedorItem.Add(pedidoFornecedorItem);
        //                                dataAdd.SaveChanges();
        //                                var produtoAguardandoEstoque = (from c in dataAdd.tbItemPedidoAguardandoEstoque
        //                                                                where
        //                                                                    c.idItemPedido == item.itemPedidoId &&
        //                                                                    c.idProduto == item.produtoId && c.reservado == false
        //                                                                select c).FirstOrDefault();
        //                                if (produtoAguardandoEstoque != null)
        //                                {
        //                                    produtoAguardandoEstoque.reservado = true;
        //                                    produtoAguardandoEstoque.dataReserva = DateTime.Now;
        //                                }
        //                                dataAdd.SaveChanges();
        //                                dataAdd.Dispose();
        //                            }
        //                            else
        //                            {
        //                                if (fornecedorEstoque)
        //                                {
        //                                    alteraLabelStatusPedido("Adicionando a aguardando estoque: " + itemPedidoAtual + "/" + totalItensPedido);
        //                                    var aguardandoCheck = (from c in data.tbItemPedidoAguardandoEstoque
        //                                                           where
        //                                                               c.idItemPedido == item.itemPedidoId &&
        //                                                               c.idProduto == item.produtoId && c.reservado == false
        //                                                           select c).Count();
        //                                    if (aguardandoCheck < i)
        //                                    {
        //                                        var aguardandoEstoque = new tbItemPedidoAguardandoEstoque();
        //                                        aguardandoEstoque.idItemPedido = item.itemPedidoId;
        //                                        aguardandoEstoque.idProduto = item.produtoId;
        //                                        aguardandoEstoque.dataSolicitacao = DateTime.Now;
        //                                        aguardandoEstoque.reservado = false;
        //                                        var dataAdd = new dbSiteEntities();
        //                                        dataAdd.tbItemPedidoAguardandoEstoque.Add(aguardandoEstoque);
        //                                        dataAdd.SaveChanges();
        //                                        dataAdd.Dispose();
        //                                    }
        //                                }
        //                            }
        //                        }
        //                    }

        //                }
        //            }
        //        }

        //        #endregion

        //        var fornecedoresIds = fornecedores.Select(x => x.fornecedorId).ToList();

        //        pedidosFornecedorPendentes =
        //            (from c in data.tbPedidoFornecedor
        //             where c.pendente == true && fornecedoresIds.Contains(c.idFornecedor)
        //             select c).ToList();
        //        int totalFornecedoresPendentes = pedidosFornecedorPendentes.Count();
        //        int fornecedorPendenteAtual = 0;
        //        foreach (var pedidoFornecedorPendentes in pedidosFornecedorPendentes)
        //        {
        //            fornecedorPendenteAtual++;
        //            alteraLabelStatusPedido("Fechando pedido ao fornecedor: " + fornecedorPendenteAtual + "/" + totalFornecedoresPendentes);
        //            fechaPedidoFornecedor(pedidoFornecedorPendentes.idFornecedor);
        //        }


        //        int totalPedidos = pedidos.Count();
        //        int pedidoAtual = 0;
        //        foreach (var pedido in pedidos)
        //        {
        //            pedidoAtual++;
        //            alteraLabelStatusPedido("Alterando status pedido: " + pedidoAtual + "/" + totalPedidos);
        //            var pedidoCheck = (from c in data.tbPedidos where c.pedidoId == pedido select c).First();
        //            var completo = rnPedidos.verificaReservasPedidoFornecedorCompleto(pedidoCheck.pedidoId);
        //            if (completo && pedidoCheck.statusDoPedido == 3)
        //            {
        //                try
        //                {
        //                    rnPedidos.pedidoAlteraStatus(11, pedidoCheck.pedidoId);
        //                    var clientesDc = new dbSiteEntities();
        //                    var cliente = (from c in clientesDc.tbClientes where c.clienteId == pedidoCheck.clienteId select c).First();
        //                    rnInteracoes.interacaoInclui(pedidoCheck.pedidoId, "Separação de Estoque", "Sistema", "True");
        //                    //rnEmails.enviaAlteracaoDeStatus(cliente.clienteNome, pedido.pedidoId.ToString(), "Separação de Estoque", cliente.clienteEmail);
        //                    // rnEmails.enviaAlteracaoDeStatus(cliente.clienteNome, pedido.pedidoId.ToString(), "Separação de Estoque", "andre@bark.com.br");
        //                }
        //                catch (Exception ex)
        //                {

        //                }
        //            }
        //        }
        //    }
        //    #region finalizaQueue
        //    queue.concluido = true;
        //    queue.andamento = true;
        //    queue.dataConclusao = DateTime.Now;
        //    data.SaveChanges();

        //    var novoQueue = new tbQueue();
        //    novoQueue.agendamento = Convert.ToDateTime(DateTime.Now.AddDays(1).ToShortDateString() + " 10:00:00");
        //    novoQueue.tipoQueue = 1;
        //    novoQueue.mensagem = "";
        //    novoQueue.concluido = false;
        //    novoQueue.andamento = false;
        //    data.tbQueue.Add(novoQueue);
        //    data.SaveChanges();

        //    #endregion
        //}

        private void alteraLabelStatusPedido(string mensagem)
        {
            try
            {
                lblStatusGerarPedido.Invoke((MethodInvoker)delegate
                {
                    lblStatusGerarPedido.Text = mensagem;
                });
            }
            catch (Exception ex)
            {

            }
        }
        private tbPedidoFornecedor retornaPedidoFornecedorPendente(int fornecedorId, List<tbProdutoFornecedor> todosFornecedores)
        {
            var data = new dbSiteEntities();
            var fornecedor = (from c in todosFornecedores where c.fornecedorId == fornecedorId select c).First();
            var pedidoFornecedorCheck = (from c in data.tbPedidoFornecedor where c.idFornecedor == fornecedorId && c.pendente == true select c).FirstOrDefault();
            var pedidoFornecedor = pedidoFornecedorCheck == null ? new tbPedidoFornecedor() : pedidoFornecedorCheck;
            pedidoFornecedor.data = DateTime.Now;
            pedidoFornecedor.idFornecedor = fornecedorId;
            pedidoFornecedor.confirmado = false;
            pedidoFornecedor.avulso = false;
            pedidoFornecedor.pendente = true;
            pedidoFornecedor.idProcessoFabrica = 7;
            var proximoPrazoFornecedor = DateTime.Now.AddDays(1);
            bool gerarProximoPrazo = false;
            while (gerarProximoPrazo == false)
            {
                int diaSemanaProximo = (int)proximoPrazoFornecedor.DayOfWeek;
                if (fornecedor.pedidoDomingo | fornecedor.pedidoSegunda | fornecedor.pedidoTerca |
                    fornecedor.pedidoQuarta | fornecedor.pedidoQuinta | fornecedor.pedidoSexta | fornecedor.pedidoSabado)
                {
                    if (diaSemanaProximo == 0 && fornecedor.pedidoDomingo) gerarProximoPrazo = true;
                    else if (diaSemanaProximo == 1 && fornecedor.pedidoSegunda) gerarProximoPrazo = true;
                    else if (diaSemanaProximo == 2 && fornecedor.pedidoTerca) gerarProximoPrazo = true;
                    else if (diaSemanaProximo == 3 && fornecedor.pedidoQuarta) gerarProximoPrazo = true;
                    else if (diaSemanaProximo == 4 && fornecedor.pedidoQuinta) gerarProximoPrazo = true;
                    else if (diaSemanaProximo == 5 && fornecedor.pedidoSexta) gerarProximoPrazo = true;
                    else if (diaSemanaProximo == 6 && fornecedor.pedidoSabado) gerarProximoPrazo = true;
                    else proximoPrazoFornecedor = proximoPrazoFornecedor.AddDays(1);
                }
                else
                {
                    gerarProximoPrazo = true;
                }
            }
            var dataLimiteProximo = proximoPrazoFornecedor;
            if (fornecedor.fornecedorPrazoPedidos > 0)
            {
                int diasPrazo = Convert.ToInt32(fornecedor.fornecedorPrazoPedidos);
                if ((fornecedor.prazoFornecedorDiasUteis ?? false) == true)
                {
                    int diasPrazoFinal = rnFuncoes.retornaPrazoDiasUteis(diasPrazo, 0);
                    diasPrazo = diasPrazoFinal;
                }
                pedidoFornecedor.dataLimite = DateTime.Now.AddDays(diasPrazo);
                dataLimiteProximo = proximoPrazoFornecedor.AddDays(diasPrazo);
            }
            else
            {
                pedidoFornecedor.dataLimite = DateTime.Now.AddDays(1);
                dataLimiteProximo = proximoPrazoFornecedor.AddDays(1);
            }
            pedidoFornecedor.usuario = "";
            pedidoFornecedor.usuario = "Sistema";

            if (pedidoFornecedorCheck == null) data.tbPedidoFornecedor.Add(pedidoFornecedor);
            data.SaveChanges();

            return pedidoFornecedor;
        }

        //private DateTime retornaDataLimitePedido(int fornecedorId, List<tbProdutoFornecedor> todosFornecedores)
        //{
        //    var fornecedor = (from c in todosFornecedores where c.fornecedorId == fornecedorId select c).First();
        //    var prazoFornecedor = DateTime.Now.AddDays(1);
        //    if (fornecedor.fornecedorPrazoPedidos > 0)
        //    {
        //        int diasPrazo = Convert.ToInt32(fornecedor.fornecedorPrazoPedidos);
        //        if ((fornecedor.prazoFornecedorDiasUteis ?? false) == true)
        //        {
        //            int diasPrazoFinal = rnFuncoes.retornaPrazoDiasUteis(diasPrazo, 0);
        //            diasPrazo = diasPrazoFinal;
        //        }
        //        prazoFornecedor = DateTime.Now.AddDays(diasPrazo);
        //    }
        //    else
        //    {
        //        prazoFornecedor = DateTime.Now.AddDays(1);
        //    }
        //    return prazoFornecedor;
        //}


        private DateTime retornaDataLimiteProximoPedido(int fornecedorId, List<tbProdutoFornecedor> todosFornecedores)
        {
            var fornecedor = (from c in todosFornecedores where c.fornecedorId == fornecedorId select c).First();
            var proximoPrazoFornecedor = DateTime.Now.AddDays(1);
            bool gerarProximoPrazo = false;
            while (gerarProximoPrazo == false)
            {
                int diaSemanaProximo = (int)proximoPrazoFornecedor.DayOfWeek;
                if (fornecedor.pedidoDomingo | fornecedor.pedidoSegunda | fornecedor.pedidoTerca |
                    fornecedor.pedidoQuarta | fornecedor.pedidoQuinta | fornecedor.pedidoSexta | fornecedor.pedidoSabado)
                {
                    if (diaSemanaProximo == 0 && fornecedor.pedidoDomingo) gerarProximoPrazo = true;
                    else if (diaSemanaProximo == 1 && fornecedor.pedidoSegunda) gerarProximoPrazo = true;
                    else if (diaSemanaProximo == 2 && fornecedor.pedidoTerca) gerarProximoPrazo = true;
                    else if (diaSemanaProximo == 3 && fornecedor.pedidoQuarta) gerarProximoPrazo = true;
                    else if (diaSemanaProximo == 4 && fornecedor.pedidoQuinta) gerarProximoPrazo = true;
                    else if (diaSemanaProximo == 5 && fornecedor.pedidoSexta) gerarProximoPrazo = true;
                    else if (diaSemanaProximo == 6 && fornecedor.pedidoSabado) gerarProximoPrazo = true;
                    else proximoPrazoFornecedor = proximoPrazoFornecedor.AddDays(1);
                }
                else
                {
                    gerarProximoPrazo = true;
                }
            }
            var dataLimiteProximo = proximoPrazoFornecedor;
            var inicioFabricacaoFornecedor = fornecedor.dataInicioFabricacao == null ? DateTime.Now : (DateTime)fornecedor.dataInicioFabricacao;
            if (fornecedor.dataFimFabricacao != null)
            {
                if (((DateTime)fornecedor.dataFimFabricacao).Date < proximoPrazoFornecedor.Date && inicioFabricacaoFornecedor.Date > proximoPrazoFornecedor.Date)
                {
                    proximoPrazoFornecedor = inicioFabricacaoFornecedor;
                }
            }
            if (fornecedor.fornecedorPrazoPedidos > 0)
            {
                int diasPrazo = Convert.ToInt32(fornecedor.fornecedorPrazoPedidos);
                if ((fornecedor.prazoFornecedorDiasUteis ?? false) == true)
                {
                    int diasPrazoFinal = rnFuncoes.retornaPrazoDiasUteis(diasPrazo, 0, proximoPrazoFornecedor);
                    diasPrazo = diasPrazoFinal;
                }
                dataLimiteProximo = proximoPrazoFornecedor.AddDays(diasPrazo);
            }
            else
            {
                dataLimiteProximo = proximoPrazoFornecedor.AddDays(1);
            }

            return dataLimiteProximo;
        }


        //private bool gerarPedidosFornecedor(tbProdutoFornecedor fornecedor, bool gerar, List<tbPedidos> pedidos, bool apenasEstoque, List<tbItensPedido> tbItensPedido,
        //    List<tbItensPedidoCombo> tbItensPedidoCombo, List<tbPedidoPagamento> tbPagamentos, List<tbProdutos> tbProdutosCombo, List<tbProdutos> tbProdutos)
        //{
        //    var data = new dbSiteEntities();
        //    var pedidoFornecedorCheck = (from c in data.tbPedidoFornecedor where c.idFornecedor == fornecedor.fornecedorId && c.pendente == true select c).FirstOrDefault();
        //    var pedidoFornecedor = pedidoFornecedorCheck == null ? new tbPedidoFornecedor() : pedidoFornecedorCheck;
        //    pedidoFornecedor.data = DateTime.Now;
        //    pedidoFornecedor.idFornecedor = fornecedor.fornecedorId;
        //    pedidoFornecedor.confirmado = false;
        //    pedidoFornecedor.avulso = false;
        //    pedidoFornecedor.pendente = true;
        //    var proximoPrazoFornecedor = DateTime.Now.AddDays(1);
        //    bool gerarProximoPrazo = false;
        //    while (gerarProximoPrazo == false)
        //    {
        //        int diaSemanaProximo = (int)proximoPrazoFornecedor.DayOfWeek;
        //        if (fornecedor.pedidoDomingo | fornecedor.pedidoSegunda | fornecedor.pedidoTerca |
        //            fornecedor.pedidoQuarta | fornecedor.pedidoQuinta | fornecedor.pedidoSexta | fornecedor.pedidoSabado)
        //        {
        //            if (diaSemanaProximo == 0 && fornecedor.pedidoDomingo) gerarProximoPrazo = true;
        //            else if (diaSemanaProximo == 1 && fornecedor.pedidoSegunda) gerarProximoPrazo = true;
        //            else if (diaSemanaProximo == 2 && fornecedor.pedidoTerca) gerarProximoPrazo = true;
        //            else if (diaSemanaProximo == 3 && fornecedor.pedidoQuarta) gerarProximoPrazo = true;
        //            else if (diaSemanaProximo == 4 && fornecedor.pedidoQuinta) gerarProximoPrazo = true;
        //            else if (diaSemanaProximo == 5 && fornecedor.pedidoSexta) gerarProximoPrazo = true;
        //            else if (diaSemanaProximo == 6 && fornecedor.pedidoSabado) gerarProximoPrazo = true;
        //            else proximoPrazoFornecedor = proximoPrazoFornecedor.AddDays(1);
        //        }
        //        else
        //        {
        //            gerarProximoPrazo = true;
        //        }
        //    }
        //    var dataLimiteProximo = proximoPrazoFornecedor;
        //    if (fornecedor.fornecedorPrazoPedidos > 0)
        //    {
        //        int diasPrazo = Convert.ToInt32(fornecedor.fornecedorPrazoPedidos);
        //        if ((fornecedor.prazoFornecedorDiasUteis ?? false) == true)
        //        {
        //            int diasPrazoFinal = rnFuncoes.retornaPrazoDiasUteis(diasPrazo, 0);
        //            diasPrazo = diasPrazoFinal;
        //        }
        //        pedidoFornecedor.dataLimite = DateTime.Now.AddDays(diasPrazo);
        //        dataLimiteProximo = proximoPrazoFornecedor.AddDays(diasPrazo);
        //    }
        //    else
        //    {
        //        pedidoFornecedor.dataLimite = DateTime.Now.AddDays(1);
        //        dataLimiteProximo = proximoPrazoFornecedor.AddDays(1);
        //    }
        //    pedidoFornecedor.usuario = "";
        //    pedidoFornecedor.usuario = "Sistema";

        //    if (pedidoFornecedorCheck == null) data.tbPedidoFornecedor.Add(pedidoFornecedor);
        //    if (gerar) data.SaveChanges();


        //    int idPedidoFornecedor = pedidoFornecedor.idPedidoFornecedor;
        //    bool estoqueConferido = fornecedor.estoqueConferido == null ? true : (bool)fornecedor.estoqueConferido;

        //    foreach (var pedido in pedidos)
        //    {
        //        int totalItens = 0;
        //        bool gerarPedidoItem = false;
        //        if (pedido.tipoDePagamentoId == 11)
        //        {
        //            var pagamentos = (from c in tbPagamentos
        //                              where
        //                                  c.pedidoId == pedido.pedidoId &&
        //                                  c.cancelado == false
        //                              orderby c.dataVencimento descending
        //                              select c).ToList();
        //            if (pagamentos.Any(x => x.pago))
        //            {
        //                var ultimoVencimento = pagamentos.Where(x => x.pago == false).FirstOrDefault();
        //                if (ultimoVencimento != null)
        //                {
        //                    if (ultimoVencimento.dataVencimento.AddDays(-1).Date <= pedidoFornecedor.dataLimite.Date)
        //                    {
        //                        gerarPedidoItem = true;
        //                    }
        //                    else if (ultimoVencimento.dataVencimento.AddDays(-1).Date <= dataLimiteProximo)
        //                    {
        //                        gerarPedidoItem = true;
        //                    }
        //                }
        //                else
        //                {
        //                    gerarPedidoItem = true;
        //                }
        //            }
        //            if (pedido.statusDoPedido == 3)
        //            {
        //                gerarPedidoItem = true;
        //            }
        //        }
        //        else
        //        {
        //            gerarPedidoItem = true;
        //        }
        //        var itensPedido = (from c in tbItensPedido where c.pedidoId == pedido.pedidoId select c).ToList();
        //        foreach (var itemPedido in itensPedido)
        //        {
        //            bool cancelado = itemPedido.cancelado == null ? false : (bool)itemPedido.cancelado;
        //            if (cancelado == false && gerarPedidoItem == true)
        //            {
        //                for (int i = 1; i <= Convert.ToInt32(itemPedido.itemQuantidade); i++)
        //                {
        //                    var produtosFilho = (from c in tbItensPedidoCombo where c.idItemPedido == itemPedido.itemPedidoId select c).ToList();
        //                    int totalProdutosFilho = produtosFilho.Count();
        //                    if (totalProdutosFilho > 0)
        //                    {
        //                        bool possuiEstoqueTotal = true;
        //                        foreach (var produtoRelacionado in produtosFilho)
        //                        {
        //                            var produtoNoCombo = tbProdutosCombo.FirstOrDefault(x => x.produtoId == produtoRelacionado.produtoId);
        //                            if (produtoNoCombo != null)
        //                            {
        //                                if (produtoNoCombo.produtoFornecedor == fornecedor.fornecedorId)
        //                                {
        //                                    //AQUI
        //                                    var estoqueDoItem = data.admin_produtoEmEstoque(produtoRelacionado.produtoId).FirstOrDefault();
        //                                    int estoqueTotalDoItem = estoqueDoItem == null ? 0 : Convert.ToInt32(estoqueDoItem.estoqueLivre);
        //                                    if (estoqueTotalDoItem < 1 | fornecedor.estoqueConferido == false | fornecedor.estoqueConferido == null)
        //                                    {
        //                                        possuiEstoqueTotal = false;
        //                                    }
        //                                }
        //                            }
        //                        }
        //                        foreach (var produtoRelacionado in produtosFilho)
        //                        {
        //                            var produtoNoCombo = tbProdutosCombo.FirstOrDefault(x => x.produtoId == produtoRelacionado.produtoId);
        //                            if (produtoNoCombo != null)
        //                            {
        //                                if (produtoNoCombo.produtoFornecedor == fornecedor.fornecedorId)
        //                                {
        //                                    var checagemProdutoFilho = (from c in data.tbPedidoFornecedorItem
        //                                                                where
        //                                                                    c.idProduto == produtoRelacionado.produtoId &&
        //                                                                    c.idItemPedido == itemPedido.itemPedidoId
        //                                                                select c).Count();
        //                                    var checagemProdutoFilhoEstoque =
        //                                        (from c in data.tbProdutoReservaEstoque
        //                                         where
        //                                             c.idProduto == produtoRelacionado.produtoId &&
        //                                             c.idItemPedido == itemPedido.itemPedidoId
        //                                         select c).Count();

        //                                    if ((checagemProdutoFilho + checagemProdutoFilhoEstoque) <
        //                                        itemPedido.itemQuantidade)
        //                                    {
        //                                        //AQUI
        //                                        var estoqueDoItem =
        //                                            data.admin_produtoEmEstoque(produtoRelacionado.produtoId)
        //                                                .FirstOrDefault();
        //                                        int estoqueTotalDoItem = estoqueDoItem == null
        //                                            ? 0
        //                                            : Convert.ToInt32(estoqueDoItem.estoqueLivre);
        //                                        var pedidosFornecedorSemVinculo =
        //                                            (from c in data.tbPedidoFornecedorItem
        //                                             where
        //                                                 c.entregue == false && c.idPedido == null &&
        //                                                 c.idItemPedido == null &&
        //                                                 c.idProduto == produtoRelacionado.produtoId
        //                                             orderby c.tbPedidoFornecedor.dataLimite
        //                                             select c).FirstOrDefault();
        //                                        if (estoqueTotalDoItem >= 1 && estoqueConferido &&
        //                                            (possuiEstoqueTotal |
        //                                             (fornecedor.fornecedorId != 21 && fornecedor.fornecedorId != 41)))
        //                                        {
        //                                            //AQUI
        //                                            var reserva = new tbProdutoReservaEstoque();
        //                                            reserva.idPedido = pedido.pedidoId;
        //                                            reserva.idProduto = produtoRelacionado.produtoId;
        //                                            reserva.dataHora = DateTime.Now;
        //                                            reserva.idItemPedido = itemPedido.itemPedidoId;
        //                                            reserva.quantidade = 1;
        //                                            data.tbProdutoReservaEstoque.Add(reserva);
        //                                            data.SaveChanges();
        //                                        }
        //                                        else if (pedidosFornecedorSemVinculo != null)
        //                                        {
        //                                            pedidosFornecedorSemVinculo.idItemPedido = itemPedido.itemPedidoId;
        //                                            pedidosFornecedorSemVinculo.idPedido = itemPedido.pedidoId;

        //                                            var produtoAguardandoEstoque =
        //                                                (from c in data.tbItemPedidoAguardandoEstoque
        //                                                 where
        //                                                     c.idItemPedido == itemPedido.itemPedidoId &&
        //                                                     c.idProduto == produtoRelacionado.produtoId &&
        //                                                     c.reservado == false
        //                                                 select c).FirstOrDefault();
        //                                            if (produtoAguardandoEstoque != null)
        //                                            {
        //                                                produtoAguardandoEstoque.reservado = true;
        //                                                produtoAguardandoEstoque.dataReserva = DateTime.Now;
        //                                            }

        //                                            data.SaveChanges();
        //                                        }
        //                                        else
        //                                        {
        //                                            if (!apenasEstoque)
        //                                            {
        //                                                var pedidoFornecedorItem = new tbPedidoFornecedorItem();
        //                                                pedidoFornecedorItem.custo = produtoRelacionado.precoDeCusto;
        //                                                pedidoFornecedorItem.brinde = produtoRelacionado.precoDeCusto == 0
        //                                                    ? true
        //                                                    : false;
        //                                                pedidoFornecedorItem.diferenca = 0;
        //                                                pedidoFornecedorItem.idPedidoFornecedor = idPedidoFornecedor;
        //                                                pedidoFornecedorItem.idProduto = produtoRelacionado.produtoId;
        //                                                pedidoFornecedorItem.entregue = false;
        //                                                pedidoFornecedorItem.idPedido = pedido.pedidoId;
        //                                                pedidoFornecedorItem.idItemPedido = itemPedido.itemPedidoId;
        //                                                pedidoFornecedorItem.quantidade = 1;
        //                                                if (!gerar) return true;
        //                                                data.tbPedidoFornecedorItem.Add(pedidoFornecedorItem);
        //                                                if (gerar) data.SaveChanges();
        //                                            }
        //                                            else
        //                                            {
        //                                                var aguardandoCheck = (from c in data.tbItemPedidoAguardandoEstoque
        //                                                                       where
        //                                                                           c.idItemPedido == itemPedido.itemPedidoId &&
        //                                                                           c.idProduto == produtoRelacionado.produtoId
        //                                                                       select c).Count();
        //                                                if (aguardandoCheck < i)
        //                                                {
        //                                                    var aguardandoEstoque = new tbItemPedidoAguardandoEstoque();
        //                                                    aguardandoEstoque.idItemPedido = itemPedido.itemPedidoId;
        //                                                    aguardandoEstoque.idProduto = produtoRelacionado.produtoId;
        //                                                    aguardandoEstoque.dataSolicitacao = DateTime.Now;
        //                                                    aguardandoEstoque.reservado = false;
        //                                                    data.tbItemPedidoAguardandoEstoque.Add(aguardandoEstoque);
        //                                                    data.SaveChanges();

        //                                                }
        //                                            }
        //                                        }
        //                                    }
        //                                }
        //                            }
        //                        }

        //                    }
        //                    else
        //                    {
        //                        totalItens++;
        //                        var produto = (from c in tbProdutos where c.produtoId == itemPedido.produtoId select c).First();

        //                        if (produto.produtoFornecedor == fornecedor.fornecedorId)
        //                        {
        //                            var checagemProduto = (from c in data.tbPedidoFornecedorItem where c.idProduto == produto.produtoId && c.idItemPedido == itemPedido.itemPedidoId select c).Count();
        //                            var checagemProdutoFilhoEstoque = (from c in data.tbProdutoReservaEstoque where c.idProduto == produto.produtoId && c.idItemPedido == itemPedido.itemPedidoId select c).Count();


        //                            if ((checagemProduto + checagemProdutoFilhoEstoque) < Convert.ToInt32(itemPedido.itemQuantidade))
        //                            {
        //                                //AQUI
        //                                var estoqueDoItem = data.admin_produtoEmEstoque(produto.produtoId).FirstOrDefault();
        //                                int estoqueTotalDoItem = estoqueDoItem == null ? 0 : Convert.ToInt32(estoqueDoItem.estoqueLivre);
        //                                var pedidosFornecedorSemVinculo =
        //                                        (from c in data.tbPedidoFornecedorItem
        //                                         where
        //                                             c.entregue == false && c.idPedido == null &&
        //                                             c.idItemPedido == null && c.idProduto == produto.produtoId
        //                                         orderby c.tbPedidoFornecedor.dataLimite
        //                                         select c).FirstOrDefault();
        //                                if (estoqueTotalDoItem >= 1 && estoqueConferido)
        //                                {
        //                                    var reserva = new tbProdutoReservaEstoque();
        //                                    reserva.idPedido = pedido.pedidoId;
        //                                    reserva.idProduto = produto.produtoId;
        //                                    reserva.dataHora = DateTime.Now;
        //                                    reserva.idItemPedido = itemPedido.itemPedidoId;
        //                                    reserva.quantidade = 1;
        //                                    data.tbProdutoReservaEstoque.Add(reserva);
        //                                    data.SaveChanges();
        //                                }
        //                                else if (pedidosFornecedorSemVinculo != null)
        //                                {
        //                                    pedidosFornecedorSemVinculo.idItemPedido = itemPedido.itemPedidoId;
        //                                    pedidosFornecedorSemVinculo.idPedido = itemPedido.pedidoId;

        //                                    var produtoAguardandoEstoque = (from c in data.tbItemPedidoAguardandoEstoque
        //                                                                    where
        //                                                                        c.idItemPedido == itemPedido.itemPedidoId &&
        //                                                                        c.idProduto == produto.produtoId && c.reservado == false
        //                                                                    select c).FirstOrDefault();
        //                                    if (produtoAguardandoEstoque != null)
        //                                    {
        //                                        produtoAguardandoEstoque.reservado = true;
        //                                        produtoAguardandoEstoque.dataReserva = DateTime.Now;
        //                                    }
        //                                    data.SaveChanges();
        //                                }
        //                                else
        //                                {
        //                                    if (!apenasEstoque)
        //                                    {
        //                                        var pedidoFornecedorItem = new tbPedidoFornecedorItem();
        //                                        pedidoFornecedorItem.custo = (decimal)produto.produtoPrecoDeCusto;
        //                                        pedidoFornecedorItem.brinde = false;
        //                                        pedidoFornecedorItem.diferenca = 0;
        //                                        pedidoFornecedorItem.idPedidoFornecedor = idPedidoFornecedor;
        //                                        pedidoFornecedorItem.idProduto = produto.produtoId;
        //                                        pedidoFornecedorItem.entregue = false;
        //                                        pedidoFornecedorItem.idPedido = pedido.pedidoId;
        //                                        pedidoFornecedorItem.idItemPedido = itemPedido.itemPedidoId;
        //                                        pedidoFornecedorItem.quantidade = 1;
        //                                        if (!gerar) return true;
        //                                        data.tbPedidoFornecedorItem.Add(pedidoFornecedorItem);
        //                                        data.SaveChanges();
        //                                    }
        //                                    else
        //                                    {
        //                                        var aguardandoCheck = (from c in data.tbItemPedidoAguardandoEstoque
        //                                                               where
        //                                                                   c.idItemPedido == itemPedido.itemPedidoId &&
        //                                                                   c.idProduto == produto.produtoId
        //                                                               select c).Count();
        //                                        if (aguardandoCheck < i)
        //                                        {
        //                                            var aguardandoEstoque = new tbItemPedidoAguardandoEstoque();
        //                                            aguardandoEstoque.idItemPedido = itemPedido.itemPedidoId;
        //                                            aguardandoEstoque.idProduto = produto.produtoId;
        //                                            aguardandoEstoque.dataSolicitacao = DateTime.Now;
        //                                            aguardandoEstoque.reservado = false;
        //                                            data.tbItemPedidoAguardandoEstoque.Add(aguardandoEstoque);
        //                                            data.SaveChanges();
        //                                        }
        //                                    }
        //                                }
        //                            }
        //                        }
        //                    }
        //                    itemPedido.prazoDeFabricacao = pedidoFornecedor.dataLimite;
        //                }
        //            }
        //        }
        //        if (gerar) data.SaveChanges();
        //    }
        //    return false;
        //}

        //List<necessidadeCombo> necessidadesPeriodo = new List<necessidadeCombo>();
        //List<necessidadeCombo> necessidadesProximoPeriodo = new List<necessidadeCombo>();

        //private void gerarPedidoEstoque(int fornecedorId)
        //{
        //    var data = new dbSiteEntities();
        //    var fornecedor = (from c in data.tbProdutoFornecedor where c.fornecedorId == fornecedorId select c).First();
        //    int prazoProximaEntrega = 1;
        //    int prazoEntregaPosterior = 1;
        //    var dataProximaEntrega = DateTime.Now;
        //    var dataEntregaPosterior = DateTime.Now;

        //    if (fornecedor.fornecedorPrazoPedidos > 0)
        //    {
        //        prazoProximaEntrega = Convert.ToInt32(fornecedor.fornecedorPrazoPedidos);
        //        if ((fornecedor.prazoFornecedorDiasUteis ?? false) == true)
        //        {
        //            int diasPrazoFinal = rnFuncoes.retornaPrazoDiasUteis(prazoProximaEntrega, 0);
        //            prazoProximaEntrega = diasPrazoFinal;
        //        }
        //        dataProximaEntrega = DateTime.Now.AddDays(prazoProximaEntrega);

        //        prazoEntregaPosterior = prazoProximaEntrega + Convert.ToInt32(fornecedor.fornecedorPrazoPedidos);
        //        if ((fornecedor.prazoFornecedorDiasUteis ?? false) == true)
        //        {
        //            int diasPrazoFinal = rnFuncoes.retornaPrazoDiasUteis(Convert.ToInt32(fornecedor.fornecedorPrazoPedidos), prazoProximaEntrega);
        //            prazoEntregaPosterior = diasPrazoFinal;
        //        }
        //        dataEntregaPosterior = DateTime.Now.AddDays(prazoEntregaPosterior);
        //    }

        //    var itensMesAnteriorProdutos = (from c in data.tbItensPedido
        //                                    where
        //                                        c.tbProdutos.produtoAtivo.ToLower() == "true" && c.tbProdutos.produtoFornecedor == fornecedorId &&
        //                                        (c.tbPedidos.statusDoPedido == 3 | c.tbPedidos.statusDoPedido == 4 | c.tbPedidos.statusDoPedido == 5 |
        //                                         c.tbPedidos.statusDoPedido == 11)
        //                                    select c.produtoId).ToList().Distinct().ToList();

        //    var itensMesAnteriorProdutosCombo = (from c in data.tbItensPedido
        //                                         join d in data.tbProdutoRelacionado on c.produtoId equals d.idProdutoPai
        //                                         where
        //                                             c.tbProdutos.produtoAtivo.ToLower() == "true" && d.tbProdutos.produtoFornecedor == fornecedorId &&
        //                                             (c.tbPedidos.statusDoPedido == 3 | c.tbPedidos.statusDoPedido == 4 | c.tbPedidos.statusDoPedido == 5 |
        //                                              c.tbPedidos.statusDoPedido == 11)
        //                                         select c.produtoId).ToList().Distinct().ToList();

        //    var itensMesAnterior = itensMesAnteriorProdutos;
        //    itensMesAnterior.AddRange(itensMesAnteriorProdutosCombo);

        //    itensMesAnterior = itensMesAnterior.Distinct().ToList();
        //    var agora = DateTime.Now;
        //    var mesAnterior = agora.AddMonths(-1);
        //    int totalDias = Convert.ToInt32((agora - mesAnterior).TotalDays);

        //    foreach (var idProduto in itensMesAnterior)
        //    {
        //        var produto = (from c in data.tbProdutos where c.produtoId == idProduto select c).First();
        //        var combo = (from c in data.tbProdutoRelacionado where c.idProdutoPai == idProduto select c).ToList();
        //        var vendasProduto = (from c in data.tbItensPedido
        //                             where
        //                                 c.tbProdutos.produtoAtivo.ToLower() == "true" && c.tbProdutos.produtoFornecedor == fornecedorId &&
        //                                 (c.tbPedidos.statusDoPedido == 3 | c.tbPedidos.statusDoPedido == 4 |
        //                                  c.tbPedidos.statusDoPedido == 5 |
        //                                  c.tbPedidos.statusDoPedido == 11) && c.tbPedidos.dataHoraDoPedido >= mesAnterior && c.produtoId == idProduto
        //                             select c.produtoId).Count();
        //        if (vendasProduto >= fornecedor.reposicaoAutomaticaMinimoMensal && fornecedor.reposicaoAutomaticaMinimoMensal > 0)
        //        {
        //            var mediaVendasPeriodoAtual = (int)Math.Ceiling(Convert.ToDecimal(vendasProduto) / (totalDias / prazoProximaEntrega));
        //            var mediaVendasProximoPeriodo = (int)Math.Ceiling(Convert.ToDecimal(vendasProduto) / (totalDias / (prazoEntregaPosterior - prazoProximaEntrega)));
        //            if (combo.Count > 0)
        //            {
        //                foreach (var produtoRelacionado in combo)
        //                {
        //                    var produtoRelacionadoAd =
        //                        (from c in data.tbProdutos
        //                         where c.produtoId == produtoRelacionado.idProdutoFilho
        //                         select c).First();
        //                    if (produtoRelacionadoAd.produtoFornecedor == fornecedorId)
        //                    {
        //                        CalculaEstoqueItem(produtoRelacionado.idProdutoFilho, dataProximaEntrega, dataEntregaPosterior, mediaVendasPeriodoAtual, mediaVendasProximoPeriodo);
        //                    }
        //                }
        //            }
        //            else
        //            {
        //                CalculaEstoqueItem(idProduto, dataProximaEntrega, dataEntregaPosterior, mediaVendasPeriodoAtual, mediaVendasProximoPeriodo);
        //            }
        //        }
        //    }
        //}

        //private void CalculaEstoqueItem(int produtoId, DateTime dataProximaEntrega, DateTime dataEntregaPosterior, int mediaVendasPeriodoAtual, int mediaVendasProximoPeriodo)
        //{
        //    var data = new dbSiteEntities();
        //    var estoqueLivre = data.admin_produtoEmEstoque(produtoId).FirstOrDefault();

        //    var proxima = Convert.ToDateTime(dataProximaEntrega.ToShortDateString());
        //    var posterior = Convert.ToDateTime(dataEntregaPosterior.ToShortDateString());

        //    int estoqueLivreAtual = estoqueLivre == null
        //        ? 0
        //        : Convert.ToInt32(estoqueLivre.estoqueLivre);
        //    var encomendasSemVinculoAtePedido = (from c in data.tbPedidoFornecedorItem
        //                                         where
        //                                             c.entregue == false && c.idProduto == produtoId &&
        //                                             c.idPedido == null && c.idItemPedido == null &&
        //                                             c.tbPedidoFornecedor.dataLimite < proxima
        //                                         select c).Count();
        //    var encomendasSemVinculoAposPedido = (from c in data.tbPedidoFornecedorItem
        //                                          where
        //                                              c.entregue == false && c.idProduto == produtoId &&
        //                                              c.idPedido == null && c.idItemPedido == null &&
        //                                              c.tbPedidoFornecedor.dataLimite >= proxima &&
        //                                              c.tbPedidoFornecedor.dataLimite < posterior
        //                                          select c).Count();

        //    int estoquePrevistoProximaEntrega = (estoqueLivreAtual + encomendasSemVinculoAtePedido - mediaVendasPeriodoAtual);
        //    var necessidadeNaLista =
        //        necessidadesPeriodo.Where(x => x.produtoId == produtoId).FirstOrDefault();
        //    if (necessidadeNaLista != null)
        //    {
        //        estoquePrevistoProximaEntrega -= necessidadeNaLista.quantidade;
        //    }
        //    if (estoquePrevistoProximaEntrega < 0) estoquePrevistoProximaEntrega = 0;

        //    if (necessidadeNaLista != null)
        //    {
        //        necessidadeNaLista.quantidade += mediaVendasPeriodoAtual;
        //    }
        //    else
        //    {
        //        var necessidadeComboSemana = new necessidadeCombo();
        //        necessidadeComboSemana.produtoId = produtoId;
        //        necessidadeComboSemana.quantidade = mediaVendasPeriodoAtual;
        //        necessidadesPeriodo.Add(necessidadeComboSemana);
        //    }


        //    int estoquePrevistoProximaEntregaPosterior = (estoquePrevistoProximaEntrega +
        //                                                  encomendasSemVinculoAposPedido);
        //    var necessidadeNaListaPosterior =
        //        necessidadesProximoPeriodo.Where(
        //            x => x.produtoId == produtoId).FirstOrDefault();
        //    if (necessidadeNaListaPosterior != null)
        //    {
        //        estoquePrevistoProximaEntregaPosterior -= necessidadeNaListaPosterior.quantidade;
        //    }
        //    if (estoquePrevistoProximaEntregaPosterior < 0)
        //        estoquePrevistoProximaEntregaPosterior = 0;

        //    if (necessidadeNaListaPosterior != null)
        //    {
        //        necessidadeNaListaPosterior.quantidade += mediaVendasProximoPeriodo;
        //    }
        //    else
        //    {
        //        var necessidadeComboSemana = new necessidadeCombo();
        //        necessidadeComboSemana.produtoId = produtoId;
        //        necessidadeComboSemana.quantidade = mediaVendasProximoPeriodo;
        //        necessidadesProximoPeriodo.Add(necessidadeComboSemana);
        //    }


        //    int compraFinal = mediaVendasProximoPeriodo - estoquePrevistoProximaEntregaPosterior;
        //    if (compraFinal > 0)
        //    {
        //        for (int i = 1; i <= compraFinal; i++)
        //        {
        //            AdicionaProdutoPedidoFornecedor(produtoId);
        //        }
        //    }
        //}

        //private void AdicionaProdutoPedidoFornecedor(int produtoId)
        //{
        //    var data = new dbSiteEntities();
        //    var produto = (from c in data.tbProdutos where c.produtoId == produtoId select c).First();

        //    int fornecedorId = produto.produtoFornecedor;
        //    var fornecedor = produto.tbProdutoFornecedor;
        //    if (fornecedor.gerarPedido)
        //    {
        //        var pedidoFornecedorCheck =
        //            (from c in data.tbPedidoFornecedor
        //             where c.idFornecedor == fornecedorId && c.pendente == true
        //             select c).FirstOrDefault();
        //        var pedidoFornecedor = pedidoFornecedorCheck == null ? new tbPedidoFornecedor() : pedidoFornecedorCheck;
        //        pedidoFornecedor.data = DateTime.Now;
        //        pedidoFornecedor.idFornecedor = fornecedorId;
        //        pedidoFornecedor.confirmado = false;
        //        pedidoFornecedor.avulso = true;
        //        pedidoFornecedor.pendente = true;
        //        if (fornecedor.fornecedorPrazoPedidos > 0)
        //        {
        //            int diasPrazo = Convert.ToInt32(fornecedor.fornecedorPrazoPedidos);
        //            if ((fornecedor.prazoFornecedorDiasUteis ?? false) == true)
        //            {
        //                int diasPrazoFinal = rnFuncoes.retornaPrazoDiasUteis(diasPrazo, 0);
        //                diasPrazo = diasPrazoFinal;
        //            }
        //            pedidoFornecedor.dataLimite = DateTime.Now.AddDays(diasPrazo);
        //        }
        //        else
        //        {
        //            pedidoFornecedor.dataLimite = DateTime.Now.AddDays(1);
        //        }
        //        pedidoFornecedor.usuario = "Sistema";
        //        if (pedidoFornecedorCheck == null) data.tbPedidoFornecedor.Add(pedidoFornecedor);
        //        data.SaveChanges();

        //        int idPedidoFornecedor = pedidoFornecedor.idPedidoFornecedor;

        //        var pedidoFornecedorItem = new tbPedidoFornecedorItem();
        //        pedidoFornecedorItem.custo = (decimal)produto.produtoPrecoDeCusto;
        //        pedidoFornecedorItem.brinde = false;
        //        pedidoFornecedorItem.diferenca = 0;
        //        pedidoFornecedorItem.idPedidoFornecedor = idPedidoFornecedor;
        //        pedidoFornecedorItem.idProduto = produto.produtoId;
        //        pedidoFornecedorItem.entregue = false;
        //        pedidoFornecedorItem.quantidade = 1;
        //        pedidoFornecedorItem.motivo = "Estoque";
        //        data.tbPedidoFornecedorItem.Add(pedidoFornecedorItem);
        //        data.SaveChanges();
        //    }
        //}
        //private class necessidadeCombo
        //{
        //    public int produtoId { get; set; }
        //    public int quantidade { get; set; }
        //}
        private void fechaPedidoFornecedor(int fornecedorId)
        {
            var data = new dbSiteEntities();
            var pedidoFornecedor =
                (from c in data.tbPedidoFornecedor where c.idFornecedor == fornecedorId && c.pendente == true select c)
                    .FirstOrDefault();
            if (pedidoFornecedor != null)
            {
                var pedidoFornecedorAlterar =
                    (from c in data.tbPedidoFornecedor
                     where c.idPedidoFornecedor == pedidoFornecedor.idPedidoFornecedor
                     select c).First();
                pedidoFornecedorAlterar.pendente = false;
                pedidoFornecedorAlterar.avulso = false;
                pedidoFornecedorAlterar.dataLimite = pedidoFornecedor.dataLimite;
                pedidoFornecedorAlterar.data = DateTime.Now;
                data.SaveChanges();
                rnEmails.EnviaEmailPedidoFornecedor(pedidoFornecedorAlterar.idPedidoFornecedor);
            }
        }
        private void bwPedidoFornecedor_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            lblStatusGerarPedido.Invoke((MethodInvoker)delegate
            {
                lblStatusGerarPedido.Text = "Aguardando agendamento";
            });
        }
        #endregion


        #region Email Carregamento

        private static void EnviaEmailCarregamento(long idQueue)
        {
            #region atualizaQueue
            var data = new dbSiteEntities();
            var queue = (from c in data.tbQueue where c.idQueue == idQueue select c).First();
            queue.andamento = true;
            queue.dataInicio = DateTime.Now;
            data.SaveChanges();
            #endregion

            int idPedidoEnvio = queue.idRelacionado ?? 0;
            int pedidoId = 0;

            var pedidoPacote = (from c in data.tbPedidoPacote where c.idPedidoEnvio == idPedidoEnvio orderby c.idPedidoEnvio descending select c);
            string rastreio = "";
            bool tnt = false;
            bool jadlog = false;
            bool correios = false;
            bool belle = false;
            bool plimor = false;
            foreach (var pacote in pedidoPacote)
            {
                try
                {
                    var dataAlt = new dbSiteEntities();
                    var pacoteAlt = (from c in data.tbPedidoPacote where c.idPedidoPacote == pacote.idPedidoPacote select c).First();
                    pedidoId = pacoteAlt.idPedido;
                    pacoteAlt.despachado = true;
                    dataAlt.SaveChanges();
                    idPedidoEnvio = (int)pacoteAlt.idPedidoEnvio;
                    if (pacoteAlt.formaDeEnvio.ToLower() == "tnt") tnt = true;
                    if (pacoteAlt.formaDeEnvio.ToLower() == "pac") correios = true;
                    if (pacoteAlt.formaDeEnvio.ToLower() == "belle") belle = true;
                    if (pacoteAlt.formaDeEnvio.ToLower() == "plimor") plimor = true;
                    if (pacoteAlt.formaDeEnvio.ToLower() == "jadlog" | pacoteAlt.formaDeEnvio.ToLower() == "jadlogexpressa")
                    {
                        jadlog = true;
                        rastreio = pacote.rastreio;
                    }
                    dataAlt.Dispose();
                }
                catch (Exception)
                {

                }
            }

            try
            {
                rnEmails.enviaCodigoDoTrakingNovo(idPedidoEnvio, "");
            }
            catch (Exception)
            {

            }

            var pedido = (from c in data.tbPedidos where c.pedidoId == pedidoId select c).FirstOrDefault();
            if (pedido.condDePagamentoId == 25)
            {
                rnIntegracoes.atualizaStatusPedidoExtra(pedido.pedidoId, 5);
            }
            #region finalizaQueue
            queue.concluido = true;
            queue.dataConclusao = DateTime.Now;
            data.SaveChanges();
            #endregion
        }

        private static void EnviaEmailNotaTnt(long idQueue)
        {

            #region atualizaQueue

            var data = new dbSiteEntities();
            var queue = (from c in data.tbQueue where c.idQueue == idQueue select c).First();
            queue.andamento = true;
            queue.dataInicio = DateTime.Now;
            data.SaveChanges();

            #endregion

            var enviado = rnEmails.EnviaNotaTnt((int)queue.idRelacionado, queue.mensagem);

            if (enviado)
            {
                #region finalizaQueue
                queue.concluido = true;
                queue.dataConclusao = DateTime.Now;
                data.SaveChanges();
                #endregion
            }
            else
            {
                #region retornaQueue
                queue.andamento = false;
                data.SaveChanges();
                #endregion
            }
        }


        private static void EnviaEmailNotaBelle(long idQueue)
        {

            #region atualizaQueue

            var data = new dbSiteEntities();
            var queue = (from c in data.tbQueue where c.idQueue == idQueue select c).First();
            queue.andamento = true;
            queue.dataInicio = DateTime.Now;
            data.SaveChanges();

            #endregion

            var enviado = rnEmails.EnviaNotaBelle((int)queue.idRelacionado, queue.mensagem);

            if (enviado)
            {
                #region finalizaQueue
                queue.concluido = true;
                queue.dataConclusao = DateTime.Now;
                data.SaveChanges();
                #endregion
            }
            else
            {
                #region retornaQueue
                queue.andamento = false;
                data.SaveChanges();
                #endregion
            }
        }

        private static void EnviaEmailNotaPlimor(long idQueue)
        {

            #region atualizaQueue

            var data = new dbSiteEntities();
            var queue = (from c in data.tbQueue where c.idQueue == idQueue select c).First();
            queue.andamento = true;
            queue.dataInicio = DateTime.Now;
            data.SaveChanges();

            #endregion

            var enviado = rnEmails.EnviaNotaPlimor((int)queue.idRelacionado, queue.mensagem);

            if (enviado)
            {
                #region finalizaQueue
                queue.concluido = true;
                queue.dataConclusao = DateTime.Now;
                data.SaveChanges();
                #endregion
            }
            else
            {
                #region retornaQueue
                queue.andamento = false;
                data.SaveChanges();
                #endregion
            }
        }
        #endregion


        #region atualizarEstoque
        private void timerAtualizarEstoque_Tick(object sender, EventArgs e)
        {
            timerAtualizarEstoque.Enabled = false;
            /*if (dwAtualizarEstoque.IsBusy != true)
            {
                dwAtualizarEstoque.RunWorkerAsync();
            }*/
        }

        private void dwAtualizarEstoque_DoWork(object sender, DoWorkEventArgs e)
        {
            var data = new dbSiteEntities();
            var produtos = new List<int>();
            var estoque = new List<v1_ListaEstoque_Result>();
            var produtosComboGeral = (from c in data.tbProdutoRelacionado
                                      where c.idProdutoPai == 0 && c.idProdutoRelacionado == 0
                                      select new
                                      {
                                          c.idProdutoPai,
                                          c.idProdutoFilho,
                                          prazo = c.tbProdutos.tbProdutoFornecedor.fornecedorPrazoDeFabricacao ?? 1,
                                          c.tbProdutos.tbProdutoFornecedor.dataFimFabricacao,
                                          c.tbProdutos.tbProdutoFornecedor.dataInicioFabricacao
                                      }).ToList();
            var todosPrazosProduto = (from c in data.tbProdutos
                                      where c.produtoId == 0
                                      select new
                                      {
                                          c.produtoId,
                                          prazo = c.tbProdutoFornecedor.fornecedorPrazoDeFabricacao ?? 1,
                                          c.tbProdutoFornecedor.dataFimFabricacao,
                                          c.tbProdutoFornecedor.dataInicioFabricacao
                                      }).ToList();
            using (new TransactionScope(TransactionScopeOption.Required, new TransactionOptions
            {
                IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
            }))
            {
                using (var dataCol = new dbSiteEntities())
                {
                    estoque = data.v1_ListaEstoque().ToList();
                    produtos = (from c in dataCol.tbProdutos orderby c.estoqueReal descending, c.produtoId descending select c.produtoId).ToList();
                    produtosComboGeral = (from c in dataCol.tbProdutoRelacionado
                                          select new
                                          {
                                              c.idProdutoPai,
                                              c.idProdutoFilho,
                                              prazo = c.tbProdutos.tbProdutoFornecedor.fornecedorPrazoDeFabricacao ?? 1,
                                              c.tbProdutos.tbProdutoFornecedor.dataFimFabricacao,
                                              c.tbProdutos.tbProdutoFornecedor.dataInicioFabricacao
                                          }).ToList();
                    todosPrazosProduto = (from c in data.tbProdutos
                                          select new
                                          {
                                              c.produtoId,
                                              prazo = c.tbProdutoFornecedor.fornecedorPrazoDeFabricacao ?? 1,
                                              c.tbProdutoFornecedor.dataFimFabricacao,
                                              c.tbProdutoFornecedor.dataInicioFabricacao
                                          }).ToList();
                }
            }

            int total = produtos.Count;
            int atual = 0;
            foreach (var produtolista in produtos)
            {
                lblEstoque.Invoke((MethodInvoker)delegate
                {
                    lblEstoque.Text = produtolista.ToString();
                });
                atual++;
                lblPrazos.Invoke((MethodInvoker)delegate
                {
                    lblPrazos.Text = "Prazos: " + atual + "/" + total;
                });
                try
                {
                    var dataAlt = new dbSiteEntities();
                    int estoqueLivre = -1;
                    var produtosCombo = (from c in produtosComboGeral
                                         where c.idProdutoPai == produtolista
                                         select new
                                         {
                                             c.idProdutoFilho,
                                             prazo = c.prazo,
                                             c.dataFimFabricacao,
                                             c.dataInicioFabricacao
                                         }).ToList();
                    var produtosComboPrazo = produtosCombo.ToList();
                    if (produtosCombo.Any())
                    {
                        foreach (var produtoRelacionado in produtosCombo)
                        {
                            var estoqueItemRelacionadoLista = estoque.FirstOrDefault(x => x.produtoId == produtoRelacionado.idProdutoFilho);
                            int estoqueItemRelacionado = estoqueItemRelacionadoLista == null
                                ? 0
                                : (int)estoqueItemRelacionadoLista.estoqueLivre;
                            if (estoqueLivre == -1)
                            {
                                estoqueLivre = estoqueItemRelacionado;
                            }
                            else
                            {
                                if (estoqueItemRelacionado < estoqueLivre)
                                {
                                    estoqueLivre = estoqueItemRelacionado;
                                }
                            }
                        }
                    }
                    else
                    {
                        var estoqueItemRelacionadoLista = estoque.FirstOrDefault(x => x.produtoId == produtolista);
                        int estoqueItemRelacionado = estoqueItemRelacionadoLista == null
                            ? 0
                            : (int)estoqueItemRelacionadoLista.estoqueLivre;
                        estoqueLivre = estoqueItemRelacionado;
                    }
                    if (estoqueLivre < 0) estoqueLivre = 0;

                    var produto = (from c in dataAlt.tbProdutos where c.produtoId == produtolista select c).FirstOrDefault();
                    if (produto != null)
                    {
                        if (produto.estoqueReal != estoqueLivre)
                        {
                            produto.estoqueReal = estoqueLivre;
                            dataAlt.SaveChanges();
                        }

                        /*if (produto.foraDeLinha && estoqueLivre < 1)
                        {
                            produto.produtoAtivo = "False";
                            dataAlt.SaveChanges();
                        }*/
                    }

                    int prazoAtualizado = 0;
                    if (produtosComboPrazo.Any())
                    {
                        foreach (var itemCombo in produtosComboPrazo)
                        {
                            var estoqueItemRelacionadoLista = estoque.FirstOrDefault(x => x.produtoId == itemCombo.idProdutoFilho);
                            int estoqueItemRelacionado = estoqueItemRelacionadoLista == null
                                ? 0
                                : (int)estoqueItemRelacionadoLista.estoqueLivre;
                            if (estoqueItemRelacionado > 0)
                            {
                                produtosComboPrazo = produtosComboPrazo.Where(x => x.idProdutoFilho != itemCombo.idProdutoFilho).ToList();
                            }
                            var prazosProduto = (from c in todosPrazosProduto
                                                 where c.produtoId == itemCombo.idProdutoFilho
                                                 select new
                                                 {
                                                     prazo = c.prazo,
                                                     c.dataFimFabricacao,
                                                     c.dataInicioFabricacao
                                                 }).First();
                            if (prazosProduto.dataFimFabricacao != null && prazosProduto.dataFimFabricacao < DateTime.Now.AddDays(2) &&
                                prazosProduto.dataInicioFabricacao != null && prazosProduto.dataInicioFabricacao > DateTime.Now)
                            {
                                int prazo = rnFuncoes.retornaDiasUteisPeriodo(DateTime.Now, (DateTime)prazosProduto.dataInicioFabricacao) + prazosProduto.prazo;
                                if (prazo > prazoAtualizado) prazoAtualizado = prazo;
                            }
                        }
                        if (produtosComboPrazo.OrderByDescending(x => x.prazo).First().prazo > prazoAtualizado) prazoAtualizado = produtosComboPrazo.OrderByDescending(x => x.prazo).First().prazo;

                    }
                    else
                    {
                        var prazosProduto = (from c in todosPrazosProduto
                                             where c.produtoId == produto.produtoId
                                             select new
                                             {
                                                 prazo = c.prazo,
                                                 c.dataFimFabricacao,
                                                 c.dataInicioFabricacao
                                             }).First();
                        if (prazosProduto.dataFimFabricacao != null && prazosProduto.dataFimFabricacao < DateTime.Now.AddDays(2) &&
                            prazosProduto.dataInicioFabricacao != null && prazosProduto.dataInicioFabricacao > DateTime.Now)
                        {
                            int prazo = rnFuncoes.retornaDiasUteisPeriodo(DateTime.Now, (DateTime)prazosProduto.dataInicioFabricacao) + prazosProduto.prazo;
                            prazoAtualizado = prazo;
                        }
                        else
                        {
                            prazoAtualizado = prazosProduto.prazo;
                        }
                    }

                    if (prazoAtualizado != produto.prazoDeEntrega)
                    {
                        var dataAlterar = new dbSiteEntities();
                        var produtoAlterar =
                            (from c in dataAlterar.tbProdutos where c.produtoId == produto.produtoId select c).First();
                        produtoAlterar.prazoDeEntrega = prazoAtualizado;
                        dataAlterar.SaveChanges();
                    }
                    dataAlt.Dispose();
                }
                catch (Exception ex)
                {
                    lblErro.Invoke((MethodInvoker)delegate
                    {
                        lblErro.Text = ex.ToString();
                    });
                }
            }
        }


        private void dwAtualizarEstoque_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            lblEstoque.Invoke((MethodInvoker)delegate
            {
                lblEstoque.Text = "Aguardando";
            });
            timerAtualizarEstoque.Interval = 300000;
            timerAtualizarEstoque.Enabled = true;
        }
        #endregion

        #region Cancelar Pedidos Vencidos

        private void bwCancelarPedidos_DoWork(object sender, DoWorkEventArgs e)
        {
            var data30d = DateTime.Now.AddDays(-10);
            var data3d = DateTime.Now.AddDays(-3);

            lblCancelamentoPedidos.Invoke((MethodInvoker)delegate
            {
                lblCancelamentoPedidos.Text = "Carregando Pedidos";
            });

            var data = new dbSiteEntities();
            var pedidosBoleto =
                (from c in data.tbPedidos
                 where (c.tipoDePagamentoId == 1 && (c.statusDoPedido == 7 | c.statusDoPedido == 2))
                 orderby c.dataHoraDoPedido
                 select c).ToList();
            var pedidosMoipDebito =
                (from c in data.tbPedidos
                 where (c.tipoDePagamentoId == 7 && (c.statusDoPedido == 7 | c.statusDoPedido == 2))
                 orderby c.dataHoraDoPedido
                 select c).ToList();
            var pedidosPagseguro =
                (from c in data.tbPedidos
                 where (c.tipoDePagamentoId == 9 && (c.statusDoPedido == 7 | c.statusDoPedido == 2))
                 orderby c.dataHoraDoPedido
                 select c).ToList();
            var pedidosCartao =
                (from c in data.tbPedidos
                 where (c.tipoDePagamentoId == 2 && (c.statusDoPedido == 7 | c.statusDoPedido == 2) && c.dataHoraDoPedido < data30d)
                 orderby c.dataHoraDoPedido
                 select c).ToList();
            var pedidosCartaoNaoAutorizado =
                (from c in data.tbPedidos
                 where (c.tipoDePagamentoId == 2 && (c.statusDoPedido == 7) && c.dataHoraDoPedido < data3d)
                 orderby c.dataHoraDoPedido
                 select c).ToList();
            var pedidosDuasFormas =
                (from c in data.tbPedidos
                 where (c.tipoDePagamentoId == 8 && (c.statusDoPedido == 7 | c.statusDoPedido == 2) && c.dataHoraDoPedido < data30d)
                 orderby c.dataHoraDoPedido
                 select c).ToList();
            var pagamentosGeral =
                (from c in data.tbPedidoPagamento
                 where (c.tbPedidos.tipoDePagamentoId == 1 && (c.tbPedidos.statusDoPedido == 7 | c.tbPedidos.statusDoPedido == 2)) | (c.tbPedidos.tipoDePagamentoId == 7 && (c.tbPedidos.statusDoPedido == 7 | c.tbPedidos.statusDoPedido == 2)) | (c.tbPedidos.tipoDePagamentoId == 9 && (c.tbPedidos.statusDoPedido == 7 | c.tbPedidos.statusDoPedido == 2)) | (c.tbPedidos.tipoDePagamentoId == 2 && (c.tbPedidos.statusDoPedido == 7 | c.tbPedidos.statusDoPedido == 2) && c.tbPedidos.dataHoraDoPedido < data30d) | (c.tbPedidos.tipoDePagamentoId == 8 && (c.tbPedidos.statusDoPedido == 7 | c.tbPedidos.statusDoPedido == 2) && c.tbPedidos.dataHoraDoPedido < data30d)
                 select c).ToList();


            lblCancelamentoPedidos.Invoke((MethodInvoker)delegate
            {
                lblCancelamentoPedidos.Text = "Cancelando pedidos boleto";
            });
            foreach (var pedido in pedidosBoleto)
            {
                var pagamentos = (from c in pagamentosGeral where c.pago == true && c.pedidoId == pedido.pedidoId select c).Any();
                if (!pagamentos)
                {
                    var pagamentoMaiorVencimento = (from c in pagamentosGeral
                                                    where c.pedidoId == pedido.pedidoId
                                                    orderby c.dataVencimento descending
                                                    select c).FirstOrDefault();
                    if (pagamentoMaiorVencimento != null)
                    {
                        var diasUteis = rnFuncoes.retornaPrazoDiasUteis(3, 0, pagamentoMaiorVencimento.dataVencimento);
                        var vencimento = pagamentoMaiorVencimento.dataVencimento.AddDays(diasUteis);
                        if (vencimento < DateTime.Now)
                        {
                            try
                            {
                                rnPedidos.CancelarPedido(pedido.pedidoId, "Sistema", 8);
                            }
                            catch (Exception ex)
                            {
                                string erro = ex.ToString();
                            }
                        }
                    }
                }
            }

            lblCancelamentoPedidos.Invoke((MethodInvoker)delegate
            {
                lblCancelamentoPedidos.Text = "Cancelando moip debito";
            });
            foreach (var pedido in pedidosMoipDebito)
            {
                var pagamentos = (from c in pagamentosGeral where c.pago == true && c.pedidoId == pedido.pedidoId select c).Any();
                if (!pagamentos)
                {
                    var pagamentoMaiorVencimento = (from c in pagamentosGeral
                                                    where c.pedidoId == pedido.pedidoId && c.cancelado == false
                                                    orderby c.dataVencimento descending
                                                    select c).FirstOrDefault();
                    if (pagamentoMaiorVencimento != null)
                    {
                        var diasUteis = rnFuncoes.retornaPrazoDiasUteis(3, 0, pagamentoMaiorVencimento.dataVencimento);
                        var vencimento = pagamentoMaiorVencimento.dataVencimento.AddDays(diasUteis);
                        if (vencimento < DateTime.Now)
                        {
                            try
                            {
                                rnPedidos.CancelarPedido(pedido.pedidoId, "Sistema", 8);
                            }
                            catch (Exception ex)
                            {
                                string erro = ex.ToString();
                            }
                        }
                    }
                }
            }

            lblCancelamentoPedidos.Invoke((MethodInvoker)delegate
            {
                lblCancelamentoPedidos.Text = "Cancelando pagseguro";
            });
            foreach (var pedido in pedidosPagseguro)
            {
                var pagamentos = (from c in pagamentosGeral where c.pago == true && c.pedidoId == pedido.pedidoId select c).Any();
                if (!pagamentos)
                {
                    var pagamentoMaiorVencimento = (from c in pagamentosGeral
                                                    where c.pedidoId == pedido.pedidoId && c.cancelado == false
                                                    orderby c.dataVencimento descending
                                                    select c).FirstOrDefault();
                    if (pagamentoMaiorVencimento != null)
                    {
                        var diasUteis = rnFuncoes.retornaPrazoDiasUteis(10, 0, pagamentoMaiorVencimento.dataVencimento);
                        var vencimento = pagamentoMaiorVencimento.dataVencimento.AddDays(diasUteis);
                        if (vencimento < DateTime.Now)
                        {
                            try
                            {
                                rnPedidos.CancelarPedido(pedido.pedidoId, "Sistema", 8);
                            }
                            catch (Exception ex)
                            {
                                string erro = ex.ToString();
                            }
                        }
                    }
                }
            }

            lblCancelamentoPedidos.Invoke((MethodInvoker)delegate
            {
                lblCancelamentoPedidos.Text = "Cancelando pedidos cartão";
            });
            foreach (var pedido in pedidosCartao)
            {
                var pagamentos = (from c in pagamentosGeral where c.pago == true && c.pedidoId == pedido.pedidoId select c).Any();
                if (!pagamentos)
                {
                    var pagamentoMaiorVencimento = (from c in pagamentosGeral
                                                    where c.pedidoId == pedido.pedidoId
                                                    orderby c.dataVencimento descending
                                                    select c).FirstOrDefault();
                    if (pagamentoMaiorVencimento != null)
                    {
                        var diasUteis = rnFuncoes.retornaPrazoDiasUteis(3, 0, pagamentoMaiorVencimento.dataVencimento);
                        var vencimento = pagamentoMaiorVencimento.dataVencimento.AddDays(diasUteis);
                        if (vencimento < DateTime.Now)
                        {
                            try
                            {
                                rnPedidos.CancelarPedido(pedido.pedidoId, "Sistema", 9);
                            }
                            catch (Exception ex)
                            {
                                string erro = ex.ToString();
                            }
                        }
                    }
                }
            }

            lblCancelamentoPedidos.Invoke((MethodInvoker)delegate
            {
                lblCancelamentoPedidos.Text = "Cancelando pedidos não autorizados";
            });
            foreach (var pedido in pedidosCartaoNaoAutorizado)
            {
                var pagamentos = (from c in pagamentosGeral where c.pago == true && c.pedidoId == pedido.pedidoId select c).Any();
                if (!pagamentos)
                {
                    var pagamentosPendentes = (from c in pagamentosGeral
                                               where c.pedidoId == pedido.pedidoId && c.pago == false && c.cancelado == false && c.pagamentoNaoAutorizado == false
                                               select c).Any();
                    if (!pagamentosPendentes)
                    {
                        try
                        {
                            rnPedidos.CancelarPedido(pedido.pedidoId, "Sistema", 9);
                        }
                        catch (Exception ex)
                        {
                            string erro = ex.ToString();
                        }
                    }
                }
            }

            lblCancelamentoPedidos.Invoke((MethodInvoker)delegate
            {
                lblCancelamentoPedidos.Text = "Cancelando duas formas de pagamento";
            });
            foreach (var pedido in pedidosDuasFormas)
            {
                var pagamentos = (from c in pagamentosGeral where c.pago == true && c.pedidoId == pedido.pedidoId select c).Any();
                if (!pagamentos)
                {
                    var pagamentoMaiorVencimento = (from c in pagamentosGeral
                                                    where c.pedidoId == pedido.pedidoId && c.cancelado == false
                                                    orderby c.dataVencimento descending
                                                    select c).FirstOrDefault();
                    if (pagamentoMaiorVencimento != null)
                    {
                        var diasUteis = rnFuncoes.retornaPrazoDiasUteis(3, 0, pagamentoMaiorVencimento.dataVencimento);
                        var vencimento = pagamentoMaiorVencimento.dataVencimento.AddDays(diasUteis);
                        if (vencimento < DateTime.Now)
                        {
                            try
                            {
                                rnPedidos.CancelarPedido(pedido.pedidoId, "Sistema", 8);
                            }
                            catch (Exception ex)
                            {
                                string erro = ex.ToString();
                            }
                        }
                    }
                }
            }

        }


        private void bwCancelarPedidos_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            lblCancelamentoPedidos.Invoke((MethodInvoker)delegate
            {
                lblCancelamentoPedidos.Text = "Próximo agendamento: " + DateTime.Now.AddMilliseconds(3600000).ToShortTimeString();
            });
            timerCancelarPedidos.Interval = 3600000;
            timerCancelarPedidos.Enabled = true;
            btnZerarTimerCancelamentoPedido.Enabled = true;
            btnAtivarTimerCancelamentoProdutos.Enabled = true;
        }

        private void timerCancelarPedidos_Tick(object sender, EventArgs e)
        {
            btnZerarTimerCancelamentoPedido.Enabled = false;
            btnAtivarTimerCancelamentoProdutos.Enabled = false;
            btnZerarTimerCancelamentoPedido.Enabled = false;
            timerCancelarPedidos.Enabled = false;
            if (bwCancelarPedidos.IsBusy != true)
            {
                bwCancelarPedidos.RunWorkerAsync();
            }
        }

        private void btnZerarTimerCancelamentoPedido_Click(object sender, EventArgs e)
        {
            timerCancelarPedidos.Interval = 1;
        }

        private void HabilitarTimerCancelamentoPedidos(bool habilitar)
        {
            if (habilitar)
            {
                timerCancelarPedidos.Enabled = true;
                btnAtivarTimerCancelamentoProdutos.Enabled = true;
                btnZerarTimerCancelamentoPedido.Enabled = true;
                btnAtivarTimerCancelamentoProdutos.Text = "Desabilitar Timer";
            }
            else
            {
                timerCancelarPedidos.Enabled = false;
                btnAtivarTimerCancelamentoProdutos.Enabled = false;
                btnZerarTimerCancelamentoPedido.Enabled = false;
                btnAtivarTimerCancelamentoProdutos.Text = "Desabilitar Timer";
            }
        }
        private void btnAtivarTimerCancelamentoProdutos_Click(object sender, EventArgs e)
        {
            if (btnAtivarTimerCancelamentoProdutos.Enabled)
            {
                HabilitarTimerCancelamentoPedidos(true);
            }
            else
            {
                HabilitarTimerCancelamentoPedidos(false);
            }
        }
        #endregion

        #region Checa Pedidos Completos

        private void HabilitarTimerPedidosCompletos(bool habilitar)
        {
            if (habilitar)
            {
                timerChecaCompletos.Enabled = true;
                btnAtivarTimerPedidosCompletos.Enabled = true;
                btnZerarTimerPedidosCompletos.Enabled = true;
                btnAtivarTimerPedidosCompletos.Text = "Desabilitar Timer";
            }
            else
            {
                timerChecaCompletos.Enabled = false;
                btnAtivarTimerPedidosCompletos.Enabled = false;
                btnZerarTimerPedidosCompletos.Enabled = false;
                btnAtivarTimerPedidosCompletos.Text = "Desabilitar Timer";
            }
        }
        private void btnAtivarTimerPedidosCompletos_Click(object sender, EventArgs e)
        {
            if (btnAtivarTimerPedidosCompletos.Enabled)
            {
                HabilitarTimerPedidosCompletos(true);
            }
            else
            {
                HabilitarTimerPedidosCompletos(false);
            }
        }

        private void btnZerarTimerPedidosCompletos_Click(object sender, EventArgs e)
        {
            timerChecaCompletos.Interval = 1;
        }
        private void timerChecaCompletos_Tick(object sender, EventArgs e)
        {
            btnZerarTimerPedidosCompletos.Enabled = false;
            btnAtivarTimerPedidosCompletos.Enabled = false;
            timerChecaCompletos.Enabled = false;
            btnChecarCompletos.Enabled = false;
            if (bwChecaCompletos.IsBusy != true)
            {
                bwChecaCompletos.RunWorkerAsync();
            }
        }

        private class produtoQuantidade
        {
            public int produtoId { get; set; }
            public int quantidade { get; set; }
        }


        private void bwChecaCompletos_DoWork(object sender, DoWorkEventArgs e)
        {

            var data = new dbSiteEntities();
            lblChecarCompletos.Invoke((MethodInvoker)delegate
            {
                lblStatusCompleto.Text = "Checando completos";
            });
            int totalSeparacao = 0;
            int pedidoAtual = 0;
            var pedidosPendentes = (from c in data.consultaPedidosCompletos select c).ToList();
            totalSeparacao = pedidosPendentes.Count();
            pedidoAtual = 0;
            foreach (var pedidoPendente in pedidosPendentes)
            {
                pedidoAtual++;
                lblChecarCompletos.Invoke((MethodInvoker)delegate
                {
                    lblStatusCompleto.Text = "Checando completos " + pedidoAtual + "/" + totalSeparacao;
                });

                bool checarCompleto = false;
                if ((pedidoPendente.itensPendenteEnvioCd1 != pedidoPendente.pendentesEstoqueCd1) | (pedidoPendente.itensPendetenEnvioCd2 != pedidoPendente.pendentesEstoqueCd2) | (pedidoPendente.itensPendetenEnvioCd3 != pedidoPendente.pendentesEstoqueCd3) | (pedidoPendente.itensPendentesEnvioCd4 != pedidoPendente.pendentesEstoqueCd4) | (pedidoPendente.itensPendentesEnvioCd5 != pedidoPendente.pendentesEstoqueCd5))
                {
                    checarCompleto = true;
                }
                if (((pedidoPendente.itensPendenteEnvioCd1 == pedidoPendente.reservadosEstoqueCd1) && pedidoPendente.envioLiberado == false) && pedidoPendente.itensPendenteEnvioCd1 > 0)
                {
                    checarCompleto = true;
                }
                if (((pedidoPendente.itensPendetenEnvioCd2 == pedidoPendente.reservadosEstoqueCd2) && pedidoPendente.envioLiberadoCd2 == false) && pedidoPendente.itensPendetenEnvioCd2 > 0)
                {
                    checarCompleto = true;
                }
                if (((pedidoPendente.itensPendetenEnvioCd3 == pedidoPendente.reservadosEstoqueCd3) && pedidoPendente.envioLiberadoCd3 == false) && pedidoPendente.itensPendetenEnvioCd3 > 0)
                {
                    checarCompleto = true;
                }
                if (((pedidoPendente.itensPendentesEnvioCd4 == pedidoPendente.reservadosEstoqueCd4) && pedidoPendente.envioLiberadoCd4 == false) && pedidoPendente.itensPendentesEnvioCd4 > 0)
                {
                    checarCompleto = true;
                }
                if (((pedidoPendente.itensPendentesEnvioCd5 == pedidoPendente.reservadosEstoqueCd5) && pedidoPendente.envioLiberadoCd5 == false) && pedidoPendente.itensPendentesEnvioCd5 > 0)
                {
                    checarCompleto = true;
                }
                if ((pedidoPendente.itensPendenteEnvioCd1 != pedidoPendente.reservadosEstoqueCd1) && pedidoPendente.envioLiberado == true)
                {
                    checarCompleto = true;
                }
                if ((pedidoPendente.itensPendetenEnvioCd2 != pedidoPendente.reservadosEstoqueCd2) && pedidoPendente.envioLiberadoCd2 == true)
                {
                    checarCompleto = true;
                }
                if ((pedidoPendente.itensPendetenEnvioCd3 != pedidoPendente.reservadosEstoqueCd3) && pedidoPendente.envioLiberadoCd3 == true)
                {
                    checarCompleto = true;
                }
                if ((pedidoPendente.itensPendentesEnvioCd4 != pedidoPendente.reservadosEstoqueCd4) && pedidoPendente.envioLiberadoCd4 == true)
                {
                    checarCompleto = true;
                }
                if ((pedidoPendente.itensPendentesEnvioCd5 != pedidoPendente.reservadosEstoqueCd5) && pedidoPendente.envioLiberadoCd5 == true)
                {
                    checarCompleto = true;
                }
                if (checarCompleto) AdicionaQueueChecagemPedidoCompleto(pedidoPendente.pedidoId);
            }

            return;




            /*var pedidosCompletos = (from c in data.tbPedidos where c.statusDoPedido != 5 && c.envioLiberado == true select new
            { c.separado, c.pedidoId, c.statusDoPedido, c.envioLiberado, c.endEstado, c.prazoFinalPedido, c.prazoMaximoPostagemAtualizado }).ToList();
            var itensCompletoFaltandoEntregar = (from c in data.tbItemPedidoEstoque
                                                 where c.tbItensPedido.tbPedidos.statusDoPedido != 5 && c.tbItensPedido.tbPedidos.envioLiberado == true && c.enviado == false && c.cancelado == false && (c.reservado == false && c.autorizarParcial == false)
                                                 select new { c.tbItensPedido.pedidoId, c.enviado, c.cancelado, c.reservado, c.autorizarParcial, c.idCentroDistribuicao, c.tbProdutos.produtoFornecedor }).ToList();
            
            foreach (var pedidoTotal in pedidosCompletos)
            {
                pedidoAtual++;
                lblChecarCompletos.Invoke((MethodInvoker)delegate
                {
                    lblChecarCompletos.Text = "Checando completos " + pedidoAtual + "/" + totalSeparacao;
                });
                bool totalCompleto = true;
                if (pedidoTotal.statusDoPedido != 11 && pedidoTotal.statusDoPedido != 3) totalCompleto = false;
                var itensFaltandoEntregar = (from c in itensCompletoFaltandoEntregar
                                             where c.pedidoId == pedidoTotal.pedidoId && c.enviado == false && c.cancelado == false && (c.reservado == false && c.autorizarParcial == false)
                                             select c).Any();

                if (itensFaltandoEntregar) totalCompleto = false;

                var itensCd1 = (from c in data.tbItemPedidoEstoque
                                where c.tbItensPedido.tbPedidos.pedidoId == pedidoTotal.pedidoId && c.enviado == false &&
                                c.cancelado == false && c.idCentroDistribuicao < 2
                                select c).Any();
                //if (itensCd1 && pedidoTotal.pedidoId != 655678)
                //{
                //    var limite = Convert.ToDateTime("17/02/2017");
                //    var prazoMaximo = pedidoTotal.prazoMaximoPostagemAtualizado;
                //    if (prazoMaximo == null) prazoMaximo = pedidoTotal.prazoFinalPedido;
                //    if (prazoMaximo == null) prazoMaximo = DateTime.Now;

                //    if (prazoMaximo.Value.Date > limite.Date)
                //    {
                //        totalCompleto = false;
                //    }
                //}

                if (pedidoTotal.separado == false && totalCompleto)
                {
                    var notaLiberar = rnNotaFiscal.DefineNotaLiberarPedido(pedidoTotal.pedidoId, pedidoTotal.endEstado.ToLower());
                    if (!notaLiberar) totalCompleto = false;
                }

                if (totalCompleto)
                {
                    if (pedidoTotal.statusDoPedido == 3 | pedidoTotal.envioLiberado == false)
                    {
                        var pedido =
                            (from c in data.tbPedidos where c.pedidoId == pedidoTotal.pedidoId select c).First();
                        if (pedidoTotal.statusDoPedido == 3)
                        {
                            pedido.statusDoPedido = 11;
                        }
                        //pedido.envioLiberado = true;
                        AdicionaQueueChecagemPedidoCompleto(pedido.pedidoId);
                        //data.SaveChanges();
                    }
                }
                else
                {
                    if (pedidoTotal.envioLiberado == true)
                    {
                        //var pedido = (from c in data.tbPedidos where c.pedidoId == pedidoTotal.pedidoId select c).First();
                        AdicionaQueueChecagemPedidoCompleto(pedidoTotal.pedidoId);
                        //pedido.envioLiberado = false;
                        //data.SaveChanges();
                    }
                }
            }




            var pedidosTotal = (from c in data.tbProdutoEstoque
                                join d in data.tbPedidos on c.pedidoIdReserva equals d.pedidoId
                                where c.pedidoIdReserva != null && c.enviado == false
                                select new { d.pedidoId, d.statusDoPedido, d.envioLiberado, d.endEstado, d.separado, d.prazoMaximoPostagemAtualizado, d.prazoFinalPedido }).Distinct().ToList();
            var itensFaltandoEntregarGeral = (from c in data.tbItemPedidoEstoque
                                              where (c.tbItensPedido.tbPedidos.statusDoPedido == 11 | c.tbItensPedido.tbPedidos.statusDoPedido == 3) && c.enviado == false && c.cancelado == false && c.reservado == false
                                              select new { c.tbItensPedido.pedidoId, c.enviado, c.cancelado, c.reservado, c.autorizarParcial, c.idCentroDistribuicao }).ToList();
            totalSeparacao = pedidosTotal.Count();
            pedidoAtual = 0;
            foreach (var pedidoTotal in pedidosTotal)
            {
                pedidoAtual++;
                lblChecarCompletos.Invoke((MethodInvoker)delegate
                {
                    lblChecarCompletos.Text = "Checando completos " + pedidoAtual + "/" + totalSeparacao;
                });
                //ChecaPedidoCompletoPorPedido(pedidoTotal);

                bool totalCompleto = true;
                if (pedidoTotal.statusDoPedido != 11 && pedidoTotal.statusDoPedido != 3) totalCompleto = false;
                var itensFaltandoEntregar = (from c in itensFaltandoEntregarGeral
                                             where c.pedidoId == pedidoTotal.pedidoId && c.enviado == false && c.cancelado == false && (c.reservado == false && c.autorizarParcial == false)
                                             select c).Any();
                if (itensFaltandoEntregar) totalCompleto = false;

                if (pedidoTotal.separado == false && totalCompleto)
                {
                    var notaLiberar = rnNotaFiscal.DefineNotaLiberarPedido(pedidoTotal.pedidoId, pedidoTotal.endEstado.ToLower());
                    if (!notaLiberar) totalCompleto = false;
                }

                //var itensCd1 = (from c in data.tbItemPedidoEstoque
                //                where c.tbItensPedido.tbPedidos.pedidoId == pedidoTotal.pedidoId && c.enviado == false &&
                //                c.cancelado == false && c.idCentroDistribuicao < 2
                //                select c).Any();
                //if (itensCd1 && pedidoTotal.pedidoId != 655678)
                //{
                //    var limite = Convert.ToDateTime("17/02/2017");
                //    var prazoMaximo = pedidoTotal.prazoMaximoPostagemAtualizado;
                //    if (prazoMaximo == null) prazoMaximo = pedidoTotal.prazoFinalPedido;
                //    if (prazoMaximo == null) prazoMaximo = DateTime.Now;

                //    if (prazoMaximo.Value.Date > limite.Date)
                //    {
                //        totalCompleto = false;
                //    }
                //}
                if (totalCompleto)
                {
                    if (pedidoTotal.statusDoPedido == 3 | pedidoTotal.envioLiberado == false)
                    {
                        var pedido =
                            (from c in data.tbPedidos where c.pedidoId == pedidoTotal.pedidoId select c).First();
                        if (pedidoTotal.statusDoPedido == 3)
                        {
                            pedido.statusDoPedido = 11;
                        }
                        //pedido.envioLiberado = true;
                        AdicionaQueueChecagemPedidoCompleto(pedido.pedidoId);
                        //data.SaveChanges();
                    }
                }
                else
                {
                    if (pedidoTotal.envioLiberado == true)
                    {
                        var pedido = (from c in data.tbPedidos where c.pedidoId == pedidoTotal.pedidoId select c).First();
                        AdicionaQueueChecagemPedidoCompleto(pedido.pedidoId);
                        //pedido.envioLiberado = false;
                        //data.SaveChanges();
                    }
                }
            }*/
        }

        private void bwChecaCompletos_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            lblStatusCompleto.Text = "Próximo agendamento: " + DateTime.Now.AddMilliseconds(7200000).ToShortTimeString();
            timerChecaCompletos.Interval = 7200000;
            timerChecaCompletos.Enabled = true;
            btnChecarCompletos.Enabled = true;
            btnZerarTimerPedidosCompletos.Enabled = true;
            btnAtivarTimerPedidosCompletos.Enabled = true;
        }


        private void bwChecarPedidoCompleto_DoWork(object sender, DoWorkEventArgs e)
        {
            checaPedidoCompletoParaEmbalar(Convert.ToInt64(e.Argument));
        }

        private void checaPedidoCompletoParaEmbalar(long idQueue)
        {
            var dataInicio = DateTime.Now;
            #region atualizaQueue
            var data = new dbSiteEntities();
            var queue = (from c in data.tbQueue where c.idQueue == idQueue select c).First();
            queue.andamento = true;
            queue.dataInicio = dataInicio;
            data.SaveChanges();
            #endregion

            int pedidoId = (int)queue.idRelacionado;
            ChecaPedidoCompletoPorPedido(pedidoId);

            #region finalizaQueue
            queue.concluido = true;
            queue.dataConclusao = DateTime.Now;
            data.SaveChanges();


            var queuesRelacionados = (from c in data.tbQueue
                                      where
                                          c.tipoQueue == queue.tipoQueue && c.idRelacionado == queue.idRelacionado &&
                                          c.agendamento < dataInicio && c.andamento == false && c.concluido == false
                                      select c).ToList();
            foreach (var queueRelacionado in queuesRelacionados)
            {
                queueRelacionado.dataInicio = dataInicio;
                queueRelacionado.andamento = true;
                queueRelacionado.concluido = true;
                queueRelacionado.dataConclusao = queue.dataConclusao;
            }
            data.SaveChanges();
            #endregion

        }

        private void ChecaPedidoCompletoPorPedido(int pedidoId)
        {
            try
            {
                var dataAlt = new dbSiteEntities();
                var pedido = (from c in dataAlt.tbPedidos where c.pedidoId == pedidoId select c).First();
                bool totalCompleto = true;
                using (var data = new dbSiteEntities())
                {
                    if (pedido.statusDoPedido != 11 && pedido.statusDoPedido != 3) totalCompleto = false;


                    var itensEstoqueGeral = (from c in data.tbItemPedidoEstoque
                                             where c.tbItensPedido.pedidoId == pedidoId
                                             select c).ToList();

                    var itensFaltandoEntregar = (from c in itensEstoqueGeral
                                                 where c.enviado == false &&
                                                 c.cancelado == false && (c.reservado == false && c.autorizarParcial == false)
                                                 && c.idCentroDistribuicao < 3
                                                 select c).Any();
                    if (itensFaltandoEntregar) totalCompleto = false;


                    if (pedido.separado == false && totalCompleto)
                    {
                        //var notaLiberar = rnNotaFiscal.DefineNotaLiberarPedido(pedido.pedidoId, pedido.endEstado.ToLower());
                        //if (!notaLiberar) totalCompleto = false;
                    }

                    var itensPendentesGeral = (from c in itensEstoqueGeral
                                               where c.cancelado == false && c.enviado == false
                                               select new
                                               {
                                                   c.idCentroDistribuicao,
                                                   c.idItemPedidoEstoque,
                                                   c.tbProdutos.aguardarEnvioCd,
                                                   c.reservado,
                                                   c.autorizarParcial
                                               }).ToList();
                    pedido.itensPendenteEnvioCd1 = itensPendentesGeral.Count(x => x.idCentroDistribuicao == 1);
                    pedido.itensPendetenEnvioCd2 = itensPendentesGeral.Count(x => x.idCentroDistribuicao == 2);
                    pedido.itensPendetenEnvioCd3 = itensPendentesGeral.Count(x => x.idCentroDistribuicao == 3);
                    pedido.itensPendentesEnvioCd4 = itensPendentesGeral.Count(x => x.idCentroDistribuicao == 4);
                    pedido.itensPendentesEnvioCd5 = itensPendentesGeral.Count(x => x.idCentroDistribuicao == 5);
                    if (pedido.itensPendetenEnvioCd2 == 0) pedido.envioLiberadoCd2 = false;
                    if (pedido.itensPendetenEnvioCd3 == 0) pedido.envioLiberadoCd3 = false;
                    if (pedido.itensPendentesEnvioCd4 == 0) pedido.envioLiberadoCd4 = false;
                    if (pedido.itensPendentesEnvioCd5 == 0) pedido.envioLiberadoCd5 = false;
                    data.SaveChanges();

                    var produtosEstoque = (from c in data.tbProdutoEstoque where c.pedidoId == pedidoId select c).ToList();

                    if (!itensPendentesGeral.Any(x => x.idCentroDistribuicao < 3))
                    {
                        pedido.envioLiberado = false;
                    }


                    if (itensFaltandoEntregar) totalCompleto = false;

                    if (!pedido.envioLiberado)
                    {
                        if (itensPendentesGeral.Any(x => x.idCentroDistribuicao == 3) && !itensPendentesGeral.Any(x => x.idCentroDistribuicao == 3 && x.reservado == false)) totalCompleto = false;
                        if ((itensPendentesGeral.Any(x => x.aguardarEnvioCd == 3) && (itensPendentesGeral.Count(x => x.idCentroDistribuicao < 3) == 1)) && itensPendentesGeral.Any(x => x.idCentroDistribuicao == 3)) totalCompleto = false;
                        if (itensPendentesGeral.Any(x => x.idCentroDistribuicao == 3) && !itensPendentesGeral.Any(x => x.idCentroDistribuicao == 3 && x.reservado == false)) totalCompleto = false;
                    }

                    if (totalCompleto)
                    {
                        if (pedido.statusDoPedido == 3)
                        {
                            pedido.statusDoPedido = 11;
                        }
                        pedido.envioLiberado = true;

                        pedido.itensPendenteEnvioCd1 = itensPendentesGeral.Count(x => x.idCentroDistribuicao == 0 | x.idCentroDistribuicao == 1);
                        pedido.itensPendetenEnvioCd2 = itensPendentesGeral.Count(x => x.idCentroDistribuicao == 2);

                        if (pedido.itensPendenteEnvioCd1 > 0)
                        {
                            bool separadoCd1 = false;
                            if ((itensEstoqueGeral.Count(x => x.cancelado == false && x.enviado == false && x.reservado == true && x.idCentroDistribuicao == 1) <= produtosEstoque.Count(x => x.idCentroDistribuicao == 1 && x.enviado == false)))
                            {
                                separadoCd1 = true;
                            }
                            pedido.separadoCd1 = separadoCd1;
                        }

                        if (pedido.itensPendetenEnvioCd2 > 0)
                        {
                            var pedidoEnvios = (from c in data.tbPedidoEnvio where c.idPedido == pedidoId select c).ToList();
                            bool envioLiberadoCd2 = false;
                            bool separadoCd2 = false;
                            var itensReservadosCd1 = itensEstoqueGeral.Count(x => x.cancelado == false && x.enviado == false && x.reservado == true && x.idCentroDistribuicao == 1);
                            var produtosSeparadosCd1 = produtosEstoque.Count(x => x.idCentroDistribuicao == 1 && x.enviado == false);
                            var itensReservadosCd2 = itensEstoqueGeral.Count(x => x.cancelado == false && x.enviado == false && x.reservado == true && x.idCentroDistribuicao == 2);
                            var produtosSeparadosCd2 = produtosEstoque.Count(x => x.idCentroDistribuicao == 2 && x.enviado == false);

                            if (itensReservadosCd1 <= produtosSeparadosCd1 && itensReservadosCd2 >= produtosSeparadosCd2)
                            {
                                envioLiberadoCd2 = true;
                            }
                            if ((itensEstoqueGeral.Count(x => x.cancelado == false && x.enviado == false && x.reservado == true && x.idCentroDistribuicao == 1) <= produtosEstoque.Count(x => x.idCentroDistribuicao == 1 && x.enviado == false) && itensEstoqueGeral.Count(x => x.cancelado == false && x.enviado == false && x.reservado == true && x.idCentroDistribuicao == 2) <= produtosEstoque.Count(x => x.idCentroDistribuicao == 2 && x.enviado == false)))
                            {
                                separadoCd2 = true;
                            }
                            if (pedidoEnvios.Count(x => x.idUsuarioSeparacaoCd2 != null && x.dataFimSeparacaoCd2 == null && x.idCentroDeDistribuicao < 3) > 0)
                            {
                                envioLiberadoCd2 = false;
                                separadoCd2 = false;
                            }
                            if (itensEstoqueGeral.Count(x => x.cancelado == false && x.enviado == false && x.reservado == false && (x.reservado == false && x.autorizarParcial == true) == false && x.idCentroDistribuicao == 2) > 0)
                            {
                                envioLiberadoCd2 = false;
                                separadoCd2 = false;
                            }
                            if (itensEstoqueGeral.Count(x => x.cancelado == false && x.enviado == false && x.idCentroDistribuicao == 2) == 0)
                            {
                                envioLiberadoCd2 = false;
                                separadoCd2 = false;
                            }
                            pedido.envioLiberadoCd2 = envioLiberadoCd2;
                            pedido.separadoCd2 = separadoCd2;
                        }
                        data.SaveChanges();
                    }
                    else
                    {
                        if (pedido.envioLiberado == true)
                        {
                            pedido.envioLiberado = false;
                            data.SaveChanges();
                        }
                    }


                    if (pedido.itensPendetenEnvioCd3 > 0)
                    {
                        var pedidoEnvios = (from c in data.tbPedidoEnvio where c.idPedido == pedidoId select c).ToList();
                        bool envioLiberadoCd3 = false;
                        bool separadoCd3 = false;
                        int itensEstoquesReservadosCd3 = itensEstoqueGeral.Count(x => x.cancelado == false && x.enviado == false && x.reservado == true && x.idCentroDistribuicao == 3);
                        int produtosNoEstoqueCd3 = produtosEstoque.Count(x => x.idCentroDistribuicao == 3 && x.enviado == false);
                        //Se itens estoque não cancelados, não enviados e reservados for MAIOR que itens faltando reservar E os itens pendentes envio for menor ou igual que itens reservados
                        if ((itensEstoquesReservadosCd3 >= produtosNoEstoqueCd3) && (pedido.itensPendetenEnvioCd3 ?? 0) <= itensEstoqueGeral.Count(x => x.cancelado == false && x.enviado == false && x.reservado == true && x.idCentroDistribuicao == 3))
                        {
                            envioLiberadoCd3 = true;
                        }
                        //Se itens estoque reservados for MENOR OU IGUALos itens reservados
                        if ((itensEstoqueGeral.Count(x => x.cancelado == false && x.enviado == false && x.reservado == true && x.idCentroDistribuicao == 3) <= produtosEstoque.Count(x => x.idCentroDistribuicao == 3 && x.enviado == false && x.pedidoId == pedidoId)))
                        {
                            envioLiberadoCd3 = true;
                            separadoCd3 = true;
                        }
                        //Se falta itens para reservar
                        if (itensEstoqueGeral.Count(x => x.cancelado == false && x.enviado == false && x.reservado == false && (x.reservado == false && x.autorizarParcial == true) == false && x.idCentroDistribuicao == 3) > 0)
                        {
                            envioLiberadoCd3 = false;
                            separadoCd3 = false;
                        }
                        if (itensEstoqueGeral.Count(x => x.cancelado == false && x.enviado == false && x.idCentroDistribuicao == 3) == 0)
                        {
                            envioLiberadoCd3 = false;
                            separadoCd3 = false;
                        }
                        pedido.envioLiberadoCd3 = envioLiberadoCd3;
                        pedido.separadoCd3 = separadoCd3;
                        data.SaveChanges();
                    }

                    if (pedido.itensPendentesEnvioCd4 > 0)
                    {
                        var pedidoEnvios = (from c in data.tbPedidoEnvio where c.idPedido == pedidoId select c).ToList();
                        bool envioLiberadoCd4 = false;
                        bool separadoCd4 = false;
                        int itensEstoquesReservadosCd4 = itensEstoqueGeral.Count(x => x.cancelado == false && x.enviado == false && (x.reservado == true | x.autorizarParcial == true) && x.idCentroDistribuicao == 4);
                        int produtosNoEstoqueCd4 = produtosEstoque.Count(x => x.idCentroDistribuicao == 4 && x.enviado == false);
                        //Se itens estoque não cancelados, não enviados e reservados for MAIOR que itens faltando reservar E os itens pendentes envio for menor ou igual que itens reservados
                        if ((itensEstoquesReservadosCd4 >= produtosNoEstoqueCd4) && (pedido.itensPendentesEnvioCd4 ?? 0) <= itensEstoqueGeral.Count(x => x.cancelado == false && x.enviado == false && (x.reservado == true | x.autorizarParcial == true) && x.idCentroDistribuicao == 4))
                        {
                            envioLiberadoCd4 = true;
                        }
                        //Se itens estoque reservados for MENOR OU IGUALos itens reservados
                        if ((itensEstoqueGeral.Count(x => x.cancelado == false && x.enviado == false && x.reservado == true && x.autorizarParcial == false && x.idCentroDistribuicao == 4) <= produtosEstoque.Count(x => x.idCentroDistribuicao == 4 && x.enviado == false && x.pedidoId == pedidoId)))
                        {
                            envioLiberadoCd4 = true;
                            separadoCd4 = true;
                        }
                        //Se falta itens para reservar
                        if (itensEstoqueGeral.Count(x => x.cancelado == false && x.enviado == false && x.reservado == false && (x.reservado == false && x.autorizarParcial == true) == false && x.idCentroDistribuicao == 4) > 0)
                        {
                            envioLiberadoCd4 = false;
                            separadoCd4 = false;
                        }
                        if (itensEstoqueGeral.Count(x => x.cancelado == false && x.enviado == false && x.idCentroDistribuicao == 4) == 0)
                        {
                            envioLiberadoCd4 = false;
                            separadoCd4 = false;
                        }
                        pedido.envioLiberadoCd4 = envioLiberadoCd4;
                        pedido.separadoCd4 = separadoCd4;
                        if (pedido.itensPendentesEnvioCd5 > 0)
                        {

                        }
                    }

                    if (pedido.itensPendentesEnvioCd5 > 0)
                    {

                        var pedidoEnvios = (from c in data.tbPedidoEnvio where c.idPedido == pedidoId select c).ToList();
                        bool envioLiberadoCd5 = false;
                        bool separadoCd5 = false;
                        int itensEstoquesReservadosCd5 = itensEstoqueGeral.Count(x => x.cancelado == false && x.enviado == false && (x.reservado == true | x.autorizarParcial == true) && x.idCentroDistribuicao == 4);
                        int produtosNoEstoqueCd5 = produtosEstoque.Count(x => x.idCentroDistribuicao == 4 && x.enviado == false);
                        //Se itens estoque não cancelados, não enviados e reservados for MAIOR que itens faltando reservar E os itens pendentes envio for menor ou igual que itens reservados
                        if ((itensEstoquesReservadosCd5 >= produtosNoEstoqueCd5) && (pedido.itensPendentesEnvioCd5 ?? 0) <= itensEstoqueGeral.Count(x => x.cancelado == false && x.enviado == false && (x.reservado == true | x.autorizarParcial == true) && x.idCentroDistribuicao == 5))
                        {
                            envioLiberadoCd5 = true;
                        }
                        //Se itens estoque reservados for MENOR OU IGUALos itens reservados
                        if ((itensEstoqueGeral.Count(x => x.cancelado == false && x.enviado == false && x.reservado == true && x.autorizarParcial == false && x.idCentroDistribuicao == 5) <= produtosEstoque.Count(x => x.idCentroDistribuicao == 5 && x.enviado == false && x.pedidoId == pedidoId)))
                        {
                            envioLiberadoCd5 = true;
                            separadoCd5 = true;
                        }
                        //Se falta itens para reservar
                        if (itensEstoqueGeral.Count(x => x.cancelado == false && x.enviado == false && x.reservado == false && (x.reservado == false && x.autorizarParcial == true) == false && x.idCentroDistribuicao == 5) > 0)
                        {
                            envioLiberadoCd5 = false;
                            separadoCd5 = false;
                        }
                        if (itensEstoqueGeral.Count(x => x.cancelado == false && x.enviado == false && x.idCentroDistribuicao == 5) == 0)
                        {
                            envioLiberadoCd5 = false;
                            separadoCd5 = false;
                        }
                        pedido.envioLiberadoCd5 = envioLiberadoCd5;
                        pedido.separadoCd5 = separadoCd5;
                    }

                    if (pedido.envioLiberadoCd4 == true && (pedido.itensPendentesEnvioCd5 > 0 && pedido.envioLiberadoCd5 == false))
                    {
                        pedido.envioLiberadoCd4 = false;
                    }
                    if (!((pedido.itensPendentesEnvioCd4 > 0 && pedido.envioLiberadoCd4 == true && pedido.separadoCd4 == true) | pedido.itensPendentesEnvioCd4 == 0))
                    {
                        pedido.envioLiberadoCd5 = false;
                    }

                    data.SaveChanges();

                    if (pedido.itensPendetenEnvioCd3 > 0 && pedido.envioLiberadoCd3 == true)
                    {
                        pedido.envioLiberado = false;
                        data.SaveChanges();
                    }

                    if (pedido.prazoMaximoPostagemAtualizado == null && pedido.envioLiberado == true)
                    {
                        pedido.prazoMaximoPostagemAtualizado = pedido.prazoFinalPedido;
                    }
                }

                dataAlt.SaveChanges();
            }
            catch (Exception ex)
            {
                rnEmails.EnviaEmail("", "andre@bark.com.br", "", "", "", ex.ToString(), "Erro Checar Completo");
            }
        }

        #endregion

        #region Gravar Itens do Combo
        private bool GravarItensCombo(int pedidoId)
        {
            using (var data = new dbSiteEntities())
            {
                {
                    try
                    {
                        //var itensPedido = (from c in data.tbItensPedido where c.pedidoId == pedidoId select c).ToList();
                        var itensPedido = (from c in data.tbItensPedido
                                           where c.pedidoId == pedidoId
                                           select new
                                           {
                                               c.itemPedidoId,
                                               c.produtoId,
                                               c.itemQuantidade,
                                               c.valorCusto,
                                               c.dataDaCriacao,
                                               c.tbProdutos,
                                               c.tbItemPedidoTipoAdicao
                                           }).ToList();
                        foreach (var itemPedido in itensPedido)
                        {
                            //var produto = (from c in data.tbProdutos where c.produtoId == itemPedido.produtoId select c).FirstOrDefault();
                            var produto = (from c in data.tbProdutos where c.produtoId == itemPedido.produtoId select new { c.produtoId }).FirstOrDefault();
                            var itensFilho = (from c in data.tbProdutoRelacionado where c.idProdutoPai == produto.produtoId select c).ToList();
                            var itensEnviados = (from c in data.tbItemPedidoEstoque where c.itemPedidoId == itemPedido.itemPedidoId && c.enviado == true select c).Any();
                            if (!itensEnviados)
                            {
                                if (itensFilho.Any())
                                {
                                    foreach (var itemCombo in itensFilho)
                                    {
                                        var totalDoCombo = itensFilho.Count(x => x.idProdutoFilho == itemCombo.idProdutoFilho) * Convert.ToInt32(itemPedido.itemQuantidade);
                                        var totalAtual = (from c in data.tbItemPedidoEstoque
                                                          where c.itemPedidoId == itemPedido.itemPedidoId && c.produtoId == itemCombo.idProdutoFilho
                                                          select c).Count();
                                        var produtoPersonalizado = (from c in data.tbProdutoPersonalizacao where c.idProdutoPai == itemCombo.idProdutoFilho select new { c.idProdutoFilhoPersonalizacao }).ToList();
                                        if (produtoPersonalizado.Any())
                                        {
                                            List<int> listaPersonalizacoes = produtoPersonalizado.Select(x => x.idProdutoFilhoPersonalizacao).ToList();
                                            var totalPersonalizacoesInformadas = (from c in data.tbItemPedidoEstoque
                                                                                  where c.itemPedidoId == itemPedido.itemPedidoId && listaPersonalizacoes.Contains(c.produtoId)
                                                                                  select c).Count();
                                            totalAtual += totalPersonalizacoesInformadas;
                                        }
                                        if (totalAtual < totalDoCombo)
                                        {
                                            for (int i = totalAtual; i < totalDoCombo; i++)
                                            {
                                                var itemEstoque = new tbItemPedidoEstoque
                                                {
                                                    itemPedidoId = itemPedido.itemPedidoId,
                                                    produtoId = itemCombo.idProdutoFilho,
                                                    precoDeCusto = itemPedido.valorCusto == 0 ? 0 : itemCombo.tbProdutos.produtoPrecoDeCusto,
                                                    enviado = false,
                                                    dataCriacao = itemPedido.dataDaCriacao,
                                                    reservado = false,
                                                    cancelado = false,
                                                    autorizarParcial = false
                                                };
                                                //List<int> itensVerificarCd = new List<int> { 43303, 43301, 43302, 47681, 47682, 47688 };
                                                //if (itemEstoque.produtoId == 43303 | itemEstoque.produtoId == 43301 | itemEstoque.produtoId == 43302 | itemEstoque.produtoId == 47681 | itemEstoque.produtoId == 47682 | itemEstoque.produtoId == 47688) itemEstoque.dataLimite = DateTime.Now;
                                                //if (itensVerificarCd.Contains(itemEstoque.produtoId)) itemEstoque.dataLimite = DateTime.Now;
                                                if (itemPedido.tbItemPedidoTipoAdicao != null)
                                                {
                                                    if (itemPedido.tbItemPedidoTipoAdicao.priorizarEnvio)
                                                    {
                                                        var itemMenorPrazo = (from c in data.tbItemPedidoEstoque where c.tbItensPedido.pedidoId == pedidoId && c.dataLimite != null orderby c.dataLimite select c).FirstOrDefault();
                                                        if (itemMenorPrazo != null)
                                                        {
                                                            itemEstoque.dataLimite = itemMenorPrazo.dataLimite;
                                                        }
                                                        else
                                                        {
                                                            itemEstoque.dataLimite = DateTime.Now;
                                                        }
                                                    }
                                                }
                                                itemEstoque.idCentroDistribuicao = (itemCombo.tbProdutos.tbProdutoFornecedor.idCentroDistribuicao ?? 4);

                                                if (itemEstoque.idCentroDistribuicao == 0)
                                                    itemEstoque.idCentroDistribuicao = 4;

                                                data.tbItemPedidoEstoque.Add(itemEstoque);
                                                data.SaveChanges();

                                                var produtoAlt = (from c in data.tbProdutos where c.produtoId == itemCombo.idProdutoFilho select c).First();
                                                produtoAlt.produtoEstoqueAtual -= 1;
                                                data.SaveChanges();
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    var produtoPersonalizado = (from c in data.tbProdutoPersonalizacao where c.idProdutoPai == produto.produtoId select new { c.idProdutoFilhoPersonalizacao }).ToList();
                                    var totalDoProduto = Convert.ToInt32(itemPedido.itemQuantidade);
                                    var totalAtual = (from c in data.tbItemPedidoEstoque
                                                      where c.itemPedidoId == itemPedido.itemPedidoId && c.produtoId == itemPedido.produtoId
                                                      select c).Count();
                                    if (produtoPersonalizado.Any())
                                    {
                                        List<int> listaPersonalizacoes = produtoPersonalizado.Select(x => x.idProdutoFilhoPersonalizacao).ToList();
                                        var totalPersonalizacoesInformadas = (from c in data.tbItemPedidoEstoque
                                                                              where c.itemPedidoId == itemPedido.itemPedidoId && listaPersonalizacoes.Contains(c.produtoId)
                                                                              select c).Count();
                                        totalAtual += totalPersonalizacoesInformadas;
                                    }

                                    if (totalAtual < totalDoProduto)
                                    {
                                        for (int i = totalAtual; i < totalDoProduto; i++)
                                        {
                                            var itemEstoque = new tbItemPedidoEstoque
                                            {
                                                itemPedidoId = itemPedido.itemPedidoId,
                                                produtoId = itemPedido.produtoId,
                                                precoDeCusto = itemPedido.valorCusto,
                                                enviado = false,
                                                dataCriacao = itemPedido.dataDaCriacao,
                                                reservado = false,
                                                cancelado = false,
                                                autorizarParcial = false
                                            };

                                            if (itemPedido.tbItemPedidoTipoAdicao != null)
                                            {
                                                if (itemPedido.tbItemPedidoTipoAdicao.priorizarEnvio)
                                                {
                                                    var itemMenorPrazo = (from c in data.tbItemPedidoEstoque where c.tbItensPedido.pedidoId == pedidoId && c.dataLimite != null orderby c.dataLimite select c).FirstOrDefault();
                                                    if (itemMenorPrazo != null)
                                                    {
                                                        itemEstoque.dataLimite = itemMenorPrazo.dataLimite;
                                                    }
                                                    else
                                                    {
                                                        itemEstoque.dataLimite = DateTime.Now;
                                                    }
                                                }
                                            }

                                            itemEstoque.idCentroDistribuicao = (itemPedido.tbProdutos.tbProdutoFornecedor.idCentroDistribuicao ?? 4);


                                            if (itemEstoque.idCentroDistribuicao == 0)
                                                itemEstoque.idCentroDistribuicao = 4;


                                            data.tbItemPedidoEstoque.Add(itemEstoque);
                                            data.SaveChanges();

                                            var produtoAlt = (from c in data.tbProdutos where c.produtoId == produto.produtoId select c).First();
                                            produtoAlt.produtoEstoqueAtual -= 1;
                                            data.SaveChanges();
                                        }
                                    }
                                }
                            }
                        }

                        return true;
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                }
            }
        }

        private void GravarItensComboQueue(long idQueue)
        {
            #region atualizaQueue
            var dataQueue = new dbSiteEntities();
            var queue = (from c in dataQueue.tbQueue where c.idQueue == idQueue select c).First();
            bool firsTry = (queue.dataInicio == null);
            queue.andamento = true;
            queue.dataInicio = DateTime.Now;
            dataQueue.SaveChanges();
            #endregion

            bool gravado = GravarItensCombo((int)queue.idRelacionado);

            if (gravado)
            {
                #region finalizaQueue

                var queueFinalizar = (from c in dataQueue.tbQueue where c.idQueue == idQueue select c).First();
                queueFinalizar.concluido = true;
                queueFinalizar.dataConclusao = DateTime.Now;
                dataQueue.SaveChanges();

                #endregion
            }
            else if (firsTry)
            {
                rnEmails.EnviaEmail("", "andre@bark.com.br", "", "", "", "Erro ao gravar itens do combo", queue.idQueue.ToString());
                #region retornaQueue
                var queueFinalizar = (from c in dataQueue.tbQueue where c.idQueue == idQueue select c).First();
                queueFinalizar.andamento = false;
                dataQueue.SaveChanges();
                #endregion
            }


        }
        #endregion


        #region Enviar Email Confirmacao Dados
        private void EnviarEmailConfirmacaoDadosPedidoQueue(long idQueue)
        {
            #region atualizaQueue
            var dataQueue = new dbSiteEntities();
            var queue = (from c in dataQueue.tbQueue where c.idQueue == idQueue select c).First();
            bool firstTry = (queue.dataInicio == null);
            queue.andamento = true;
            queue.dataInicio = DateTime.Now;
            dataQueue.SaveChanges();
            #endregion

            //bool enviado = rnEmails.enviaConfirmacaoDadosPedido((int)queue.idRelacionado);
            bool enviado = true;
            if (enviado)
            {
                #region finalizaQueue

                var queueFinalizar = (from c in dataQueue.tbQueue where c.idQueue == idQueue select c).First();
                queueFinalizar.concluido = true;
                queueFinalizar.dataConclusao = DateTime.Now;
                dataQueue.SaveChanges();

                #endregion
            }
            else if (firstTry)
            {
                rnEmails.EnviaEmail("", "andre@bark.com.br", "", "", "", "Erro ao calcular prazo maximo pedidos", queue.idQueue.ToString());
                #region retornaQueue
                var queueFinalizar = (from c in dataQueue.tbQueue where c.idQueue == idQueue select c).First();
                queueFinalizar.andamento = false;
                dataQueue.SaveChanges();
                #endregion
            }


        }
        #endregion

        #region Verifica Estoque
        private void timerVerificaEstoque_Tick(object sender, EventArgs e)
        {
            timerVerificaEstoque.Enabled = false;
        }

        //private void bwVerificaEstoque_DoWork(object sender, DoWorkEventArgs e)
        //{
        //    var data = new dbSiteEntities();
        //    var produtosAguardandoEstoque = (from c in data.tbItemPedidoAguardandoEstoque where c.reservado == false && (c.tbItensPedido.cancelado ?? false) == false select c).ToList();
        //    List<int> idsChecados = new List<int>();
        //    foreach (var aguardandoEstoque in produtosAguardandoEstoque)
        //    {
        //        bool itemChecado = idsChecados.Any(x => x == aguardandoEstoque.idProduto);
        //        if (!itemChecado)
        //        {
        //            var estoqueDoItem = data.admin_produtoEmEstoque(aguardandoEstoque.idProduto).FirstOrDefault();
        //            int estoqueTotalDoItem = estoqueDoItem == null ? 0 : Convert.ToInt32(estoqueDoItem.estoqueLivre);
        //            if (estoqueTotalDoItem >= 1)
        //            {
        //                var reserva = new tbProdutoReservaEstoque();
        //                reserva.idPedido = aguardandoEstoque.tbItensPedido.pedidoId;
        //                reserva.idProduto = aguardandoEstoque.idProduto;
        //                reserva.dataHora = DateTime.Now;
        //                reserva.idItemPedido = aguardandoEstoque.idItemPedido;
        //                reserva.quantidade = 1;
        //                data.tbProdutoReservaEstoque.Add(reserva);

        //                aguardandoEstoque.dataReserva = DateTime.Now;
        //                aguardandoEstoque.reservado = true;
        //                data.SaveChanges();
        //            }
        //            else
        //            {
        //                idsChecados.Add(aguardandoEstoque.idProduto);
        //            }
        //        }
        //    }

        //}

        private void bwVerificaEstoque_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            timerVerificaEstoque.Interval = 3600000;
            timerVerificaEstoque.Enabled = true;
        }
        #endregion

        #region Metodos jadLog
        private void timerPendenciasJadLog_Tick(object sender, EventArgs e)
        {
            timerPendenciasJadLog.Enabled = false;
            if (bwPendenciasJadLog.IsBusy != true)
            {
                bwPendenciasJadLog.RunWorkerAsync();
            }
        }

        private void bwPendenciasJadLog_DoWork(object sender, DoWorkEventArgs e)
        {
            var pedidosDc = new dbSiteEntities();
            var pendentesJadlog = (from c in pedidosDc.tbPedidoPacote
                                   where (c.codJadlog ?? "") == "" && (c.tbPedidoEnvio.formaDeEnvio.Contains("jadlog")) &&
                                       (c.rastreio == "" | c.rastreio == null)
                                   select c.tbPedidoEnvio).ToList().Distinct().ToList();
            foreach (var pendenteJadlog in pendentesJadlog)
            {
                if (pendenteJadlog.idPedidoEnvio != null)
                {
                    //pedidosDc.SaveChanges();
                }
                try
                {
                    //Jadlog Aqui
                    rnFrete.GravaPedidoWebserviceJadlog(pendenteJadlog.idPedidoEnvio);
                }
                catch (Exception ex)
                {
                    //rnEmails.EnviaEmail("", "andre@bark.com.br", "", "", "", ex.ToString(), "Falha ao gravar no webservice jadlog " + pendenteJadlog.idPedido);
                }

            }

            //var pedidosJad2 = (from c in pedidosDc.tbPedidoPacote
            //                   where c.codJadlog != null && c.codJadlog != "" && (c.rastreio == "" | c.rastreio == null)
            //                   select new { c.codJadlog, c.idPedido }).Distinct().ToList();
            //foreach (var pedido in pedidosJad2)
            //{
            //    string numeroTracking = rnPedidos.checaRastreioJad(pedido.codJadlog);
            //    if (!string.IsNullOrEmpty(numeroTracking))
            //    {
            //        var itensCodigo = (from c in pedidosDc.tbPedidoPacote where c.codJadlog == pedido.codJadlog select c);
            //        foreach (var pacote in itensCodigo)
            //        {
            //            pacote.rastreio = numeroTracking;
            //        }
            //        pedidosDc.SaveChanges();
            //        rnEmails.enviaCodigoDoTrakingJadlog(pedido.idPedido, numeroTracking);
            //    }
            //}

        }

        private void bwPendenciasJadLog_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            timerPendenciasJadLog.Interval = 15000;
            timerPendenciasJadLog.Enabled = true;
        }

        #endregion Metodos jadLog

        private void bwEmailTnt_DoWork(object sender, DoWorkEventArgs e)
        {
            var idQueue = Convert.ToInt64(e.Argument);
            try
            {

                lblStatusGerarPedido.Invoke((MethodInvoker)delegate
                {
                    lblProcessoEmAndamento1.Text = "Enviando emails tnt";
                });

                EnviaEmailNotaTnt(idQueue);

            }
            catch (Exception ex)
            {

            }

            lblStatusGerarPedido.Invoke((MethodInvoker)delegate
            {
                lblProcessoEmAndamento1.Text = "";
            });

        }


        private void bwEmailBelle_DoWork(object sender, DoWorkEventArgs e)
        {

            var idQueue = Convert.ToInt64(e.Argument);
            try
            {

                lblStatusGerarPedido.Invoke((MethodInvoker)delegate
                {
                    lblProcessoEmAndamento1.Text = "Enviando emails Belle";
                });

                EnviaEmailNotaBelle(idQueue);

            }
            catch (Exception ex)
            {

            }

            lblStatusGerarPedido.Invoke((MethodInvoker)delegate
            {
                lblProcessoEmAndamento1.Text = "";
            });
        }

        private void bwEmailBelle_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {

        }

        private void bwEnviaEmailCarregamento_DoWork(object sender, DoWorkEventArgs e)
        {
            var idQueue = Convert.ToInt64(e.Argument);
            try
            {

                lblProcessoEmAndamento2.Invoke((MethodInvoker)delegate
                {
                    lblProcessoEmAndamento2.Text = "Enviando emails Carregamento";
                });

                EnviaEmailCarregamento(idQueue);

            }
            catch (Exception)
            {

            }

            lblProcessoEmAndamento2.Invoke((MethodInvoker)delegate
            {
                lblProcessoEmAndamento2.Text = "";
            });
        }



        private void bwUtilizarEstoque_DoWork(object sender, DoWorkEventArgs e)
        {
            #region atualizaQueue
            var data = new dbSiteEntities();
            int idQueue = Convert.ToInt32(e.Argument);
            var queue = (from c in data.tbQueue where c.idQueue == idQueue select c).First();
            queue.andamento = true;
            queue.dataInicio = DateTime.Now;
            data.SaveChanges();
            #endregion atualizaQueue


            //bool estoqueUtilizado = true;
            //while (estoqueUtilizado)
            //{
            //    estoqueUtilizado = utilizarEstoque();
            //}

            //ReservarItens(0);

            #region finalizaQueue
            queue.concluido = true;
            queue.andamento = true;
            queue.dataConclusao = DateTime.Now;
            data.SaveChanges();

            var novoQueue = new tbQueue();
            novoQueue.agendamento = Convert.ToDateTime(DateTime.Now.AddHours(12));
            novoQueue.tipoQueue = 9;
            novoQueue.mensagem = "";
            novoQueue.concluido = false;
            novoQueue.andamento = false;
            data.tbQueue.Add(novoQueue);
            data.SaveChanges();

            #endregion
        }

        //private bool utilizarEstoque()
        //{
        //    var data = new dbSiteEntities();
        //    List<int> statusPedido = new List<int> { 3, 11 };
        //    var produtoEmEstoque = data.admin_produtosEmEstoque().Where(x => x.estoqueLivre > 0).Select(x => x.produtoId).ToList();
        //    var pedidosItensAoFornecedor = (from f in data.tbPedidoFornecedorItem
        //                                    join p in data.tbPedidos on f.idPedido equals p.pedidoId
        //                                    where f.entregue == false && produtoEmEstoque.Contains(f.idProduto) && statusPedido.Contains(p.statusDoPedido)
        //                                    orderby f.tbPedidoFornecedor.dataLimite
        //                                    select new
        //                                    {
        //                                        p.pedidoId
        //                                    }).ToList();
        //    foreach (var pedidoItem in pedidosItensAoFornecedor)
        //    {
        //        var pedido = (from c in data.tbPedidos where c.pedidoId == pedidoItem.pedidoId select c).First();
        //        var itensPedido = (from c in data.tbItensPedido where c.pedidoId == pedidoItem.pedidoId select c);

        //        int totalPedidosNecessarioReserva = 0;
        //        int totalPedidosReservados = 0;
        //        var dataAdicionar = new dbSiteEntities();

        //        foreach (var item in itensPedido)
        //        {
        //            bool cancelado = item.cancelado ?? false;
        //            if (!cancelado)
        //            {
        //                List<int> idsProdutos = new List<int>();

        //                var relacionados = (from c in data.tbItensPedidoCombo where c.idItemPedido == item.itemPedidoId select c);
        //                if (relacionados.Any())
        //                {
        //                    idsProdutos.AddRange(relacionados.Select(x => x.produtoId).ToList());
        //                }
        //                else
        //                {
        //                    idsProdutos.Add(item.produtoId);
        //                }

        //                foreach (var idProdutoReservar in idsProdutos)
        //                {
        //                    bool necessarioReserva = pedido.tipoDePagamentoId == 11 ? rnEstoque.VerificaNecessidadeReservaEstoque(item.itemPedidoId, idProdutoReservar, item.pedidoId) : true;
        //                    if (necessarioReserva)
        //                    {
        //                        var reservasAtuais = (from c in data.tbProdutoReservaEstoque
        //                                              where
        //                                                  c.idPedido == pedidoItem.pedidoId && c.idItemPedido == item.itemPedidoId &&
        //                                                  c.idProduto == idProdutoReservar
        //                                              select c).Count();
        //                        var pedidosAtuais = (from c in data.tbPedidoFornecedorItem
        //                                             where
        //                                                 c.idPedido == pedidoItem.pedidoId && c.idItemPedido == item.itemPedidoId &&
        //                                                 c.idProduto == idProdutoReservar && c.entregue == true
        //                                             select c).Count();
        //                        if (reservasAtuais + pedidosAtuais < Convert.ToInt32(item.itemQuantidade))
        //                        {
        //                            int totalPedir = Convert.ToInt32(item.itemQuantidade) - reservasAtuais - pedidosAtuais;
        //                            totalPedidosNecessarioReserva += totalPedir;
        //                            var estoque = data.admin_produtoEmEstoque(idProdutoReservar).FirstOrDefault();
        //                            if (estoque != null)
        //                            {
        //                                int estoqueLivre = Convert.ToInt32(estoque.estoqueLivre);
        //                                if (estoqueLivre > 0)
        //                                {
        //                                    if (estoqueLivre < totalPedir)
        //                                    {
        //                                        totalPedir = 0;
        //                                    }
        //                                    for (int i = 1; i <= totalPedir; i++)
        //                                    {
        //                                        var reserva = new tbProdutoReservaEstoque();
        //                                        reserva.idPedido = pedidoItem.pedidoId;
        //                                        reserva.idProduto = idProdutoReservar;
        //                                        reserva.dataHora = DateTime.Now;
        //                                        reserva.idItemPedido = item.itemPedidoId;
        //                                        reserva.quantidade = 1;
        //                                        dataAdicionar.tbProdutoReservaEstoque.Add(reserva);
        //                                        totalPedidosReservados++;
        //                                    }
        //                                }
        //                            }
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //        if (totalPedidosReservados > 0 && totalPedidosNecessarioReserva == totalPedidosReservados)
        //        {
        //            dataAdicionar.SaveChanges();
        //            return true;
        //        }
        //    }
        //    return false;
        //}

        private void bwUtilizarEstoque_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {

        }

        private void ProcessaPagamentoPedido(long idQueue)
        {
            #region atualizaQueue
            var dataQueue = new dbSiteEntities();
            var queue = (from c in dataQueue.tbQueue where c.idQueue == idQueue select c).First();
            bool firstTry = (queue.dataInicio == null);
            queue.andamento = true;
            queue.dataInicio = DateTime.Now;
            dataQueue.SaveChanges();
            #endregion

            string retorno = PagamentoV2.GravarOrdemPagamento((int)queue.idRelacionado);
            bool processado = string.IsNullOrEmpty(retorno);

            var queueUpdate = new tbQueue();
            queueUpdate.idRelacionado = queue.idRelacionado;
            queueUpdate.tipoQueue = 12;
            queueUpdate.andamento = false;
            queueUpdate.concluido = false;
            queueUpdate.agendamento = DateTime.Now.AddMinutes(2);
            queueUpdate.mensagem = "";
            dataQueue.tbQueue.Add(queueUpdate);
            if (processado)
            {
                #region finalizaQueue

                var queueFinalizar = (from c in dataQueue.tbQueue where c.idQueue == idQueue select c).First();
                queueFinalizar.concluido = true;
                queueFinalizar.dataConclusao = DateTime.Now;
                dataQueue.SaveChanges();

                #endregion
            }
            else if (firstTry)
            {
                rnEmails.EnviaEmail("", "andre@bark.com.br", "", "", "", retorno, "Erro ao gravar pagamentos " + queue.idQueue.ToString());
                #region retornaQueue
                var queueFinalizar = (from c in dataQueue.tbQueue where c.idQueue == idQueue select c).First();
                queueFinalizar.concluido = true;
                queueFinalizar.dataConclusao = DateTime.Now;
                dataQueue.SaveChanges();
                #endregion
            }
        }
        private void ProcessaPagamento(long idQueue)
        {
            #region atualizaQueue
            var dataQueue = new dbSiteEntities();
            var queue = (from c in dataQueue.tbQueue where c.idQueue == idQueue select c).First();
            bool firstTry = (queue.dataInicio == null);
            queue.andamento = true;
            queue.dataInicio = DateTime.Now;
            dataQueue.SaveChanges();
            #endregion

            string retorno = PagamentoV2.GravarPagamentoAvulso((int)queue.idRelacionado);
            bool processado = string.IsNullOrEmpty(retorno);


            if (processado)
            {
                #region finalizaQueue

                var queueFinalizar = (from c in dataQueue.tbQueue where c.idQueue == idQueue select c).First();
                queueFinalizar.concluido = true;
                queueFinalizar.dataConclusao = DateTime.Now;
                dataQueue.SaveChanges();

                #endregion
            }
            else if (firstTry)
            {
                rnEmails.EnviaEmail("", "andre@bark.com.br", "", "", "", retorno, "Erro ao gravar pagamentos " + queue.idQueue.ToString());
                #region retornaQueue
                var queueFinalizar = (from c in dataQueue.tbQueue where c.idQueue == idQueue select c).First();
                queueFinalizar.concluido = true;
                queueFinalizar.dataConclusao = DateTime.Now;
                dataQueue.SaveChanges();
                #endregion
            }
        }
        private void AtualizaPagamento(long idQueue)
        {
            #region atualizaQueue
            var dataQueue = new dbSiteEntities();
            var queue = (from c in dataQueue.tbQueue where c.idQueue == idQueue select c).First();
            bool firstTry = (queue.dataInicio == null);
            queue.andamento = true;
            queue.dataInicio = DateTime.Now;
            dataQueue.SaveChanges();
            #endregion

            string processado = PagamentoV2.AtualizaPagamento((int)queue.idRelacionado);

            if (string.IsNullOrEmpty(processado))
            {
                #region finalizaQueue

                var queueFinalizar = (from c in dataQueue.tbQueue where c.idQueue == idQueue select c).First();
                queueFinalizar.concluido = true;
                queueFinalizar.dataConclusao = DateTime.Now;
                dataQueue.SaveChanges();

                #endregion
            }
            else if (firstTry)
            {
                rnEmails.EnviaEmail("", "andre@bark.com.br", "", "", "", "Erro ao atualizar pagamentos<br>" + processado, queue.idQueue.ToString());
                #region retornaQueue
                var queueFinalizar = (from c in dataQueue.tbQueue where c.idQueue == idQueue select c).First();
                queueFinalizar.andamento = false;
                dataQueue.SaveChanges();
                #endregion
            }
        }

        private void EnviaPagamentoClearsale(long idQueue)
        {
            #region atualizaQueue
            var dataQueue = new dbSiteEntities();
            var queue = (from c in dataQueue.tbQueue where c.idQueue == idQueue select c).First();
            bool firstTry = (queue.dataInicio == null);
            queue.andamento = true;
            queue.dataInicio = DateTime.Now;
            dataQueue.SaveChanges();
            #endregion

            bool processado = true;
            int idPedidoPagamento = Convert.ToInt32(queue.mensagem);
            var pagamento = (from c in dataQueue.tbPedidoPagamento where c.idPedidoPagamento == idPedidoPagamento select c).FirstOrDefault();
            if (pagamento != null)
            {
                var analiseRiscoClearsale =
                    (from c in dataQueue.tbPedidoPagamentoRisco
                     where c.tbPedidoPagamento.pedidoId == pagamento.pedidoId
                     select c).Any();
                if (!analiseRiscoClearsale)
                {
                    var riscoRepo = new ClearSaleRepository();
                    var gatewayRepo = new MaxipagoRepository();
                    var pagamentoRepo = new PagamentoRepository(riscoRepo, gatewayRepo);
                    var riscoBus = new EnviarAnaliseRisco(riscoRepo, pagamentoRepo);
                    processado = riscoBus.Enviar(new EnviarAnaliseRiscoRequest()
                    {
                        IdPedidoPagamento = Convert.ToInt32(queue.mensagem)
                    }).Enviado;
                }
                else
                {
                    var novoQueue = new tbQueue();
                    novoQueue.agendamento = DateTime.Now.AddMinutes(1);
                    novoQueue.tipoQueue = 33;
                    novoQueue.idRelacionado = pagamento.pedidoId;
                    novoQueue.mensagem = "";
                    novoQueue.concluido = false;
                    novoQueue.andamento = false;
                    dataQueue.tbQueue.Add(novoQueue);
                    dataQueue.SaveChanges();
                }
            }
            if (processado)
            {
                #region finalizaQueue

                var queueFinalizar = (from c in dataQueue.tbQueue where c.idQueue == idQueue select c).First();
                queueFinalizar.concluido = true;
                queueFinalizar.dataConclusao = DateTime.Now;
                dataQueue.SaveChanges();

                #endregion
            }
            else if (firstTry)
            {
                rnEmails.EnviaEmail("", "andre@bark.com.br", "", "", "", "Erro ao atualizar pagamentos<br>" + processado, queue.idQueue.ToString());
                #region retornaQueue
                var queueFinalizar = (from c in dataQueue.tbQueue where c.idQueue == idQueue select c).First();
                queueFinalizar.andamento = false;
                dataQueue.SaveChanges();
                #endregion
            }
        }

        private void bwListaTransferencia_DoWork(object sender, DoWorkEventArgs e)
        {
            #region atualizaQueue
            var data = new dbSiteEntities();
            int idQueue = Convert.ToInt32(e.Argument);
            var queue = (from c in data.tbQueue where c.idQueue == idQueue select c).First();
            queue.andamento = true;
            queue.dataInicio = DateTime.Now;
            data.SaveChanges();
            #endregion atualizaQueue


            #region geraLista

            var pedidosCompletos = new List<int>();
            var pedidosDc = new dbSiteEntities();
            var itensPedidoDc = new dbSiteEntities();
            var produtosDc = new dbSiteEntities();

            lblProcessoEmAndamento3.Invoke((MethodInvoker)delegate
            {
                lblProcessoEmAndamento3.Text = "Monta lista de itens para pedido completo";
            });

            var pedidosGeral = pedidosDc.admin_itensPedidoParaPedidoCompleto().ToList();
            var pedidosSeparacao = pedidosGeral.Select(x => x.pedidoId).Distinct();
            int totalSeparacao = pedidosSeparacao.Count();
            int pedidoAtual = 0;

            foreach (var pedidoSeparacao in pedidosSeparacao)
            {
                pedidoAtual++;
                var listaProdutos = new List<produtoQuantidade>();
                var itensPedido = pedidosGeral.Where(x => x.pedidoId == pedidoSeparacao);
                int totalProdutosParcial = 0;
                int totalProdutosParcialEntregue = 0;
                int totalProdutosNaoParcial = 0;
                int totalProdutosNaoParcialEntregues = 0;
                bool totalCompleto = true;
                bool possuiItensTransferencia = false;

                var pedidoDetalhe = (from c in data.tbPedidos where c.pedidoId == pedidoSeparacao select c).First();
                var itensTransferencia =
                    (from c in pedidosDc.tbItensPedido
                     where (c.tbProdutos.requerTransferencia ?? false) == true && c.pedidoId == pedidoSeparacao && (c.cancelado ?? false) == false && (c.enviado ?? false) == false
                     && (c.statusTransferencia ?? 0) == 0
                     select c).Any();
                var itensTransferenciaCombo =
                    (from c in pedidosDc.tbItensPedidoCombo
                     where (c.tbProdutos.requerTransferencia ?? false) == true && c.tbItensPedido.pedidoId == pedidoSeparacao && (c.tbItensPedido.cancelado ?? false) == false && (c.enviado) == false
                     && (c.statusTransferencia ?? 0) == 0
                     select c).Any();

                if ((itensTransferencia | itensTransferenciaCombo) && pedidoDetalhe.separado == false)
                {
                    possuiItensTransferencia = true;
                }
                else
                {
                    totalCompleto = false;
                }

                foreach (var itemPedido in itensPedido)
                {
                    if (totalCompleto && possuiItensTransferencia)
                    {
                        bool enviado = itemPedido.enviado;
                        bool parcial = itemPedido.parcial == 1;

                        if (parcial | enviado == false)
                        {
                            if (parcial)
                            {
                                totalProdutosParcial++;
                            }
                            else
                            {
                                totalProdutosNaoParcial++;
                            }


                            int totalReservado = itemPedido.reservas;
                            int totalPedidosFornecedor = itemPedido.pedidosFornecedor;
                            int totalFaltando = itemPedido.faltandoNaoEntregues;

                            if ((totalReservado + totalPedidosFornecedor - totalFaltando) < itemPedido.itemQuantidade)
                            {
                                var segundoEnvio =
                                    (from c in pedidosDc.tbItensPedido
                                     where c.pedidoId == itemPedido.pedidoId && c.enviado == true
                                     select c).Any() | (from c in pedidosDc.tbItensPedidoCombo
                                                        where c.tbItensPedido.pedidoId == itemPedido.pedidoId && c.enviado == true
                                                        select c).Any();
                                if (!parcial)
                                {
                                    totalCompleto = false;
                                }
                                else
                                {
                                    if (segundoEnvio) totalCompleto = false;
                                }
                            }
                            else
                            {
                                if (parcial)
                                {
                                    totalProdutosParcialEntregue++;
                                }
                                else
                                {
                                    totalProdutosNaoParcialEntregues++;
                                }
                            }
                        }
                    }
                }

                if (totalCompleto)
                {
                    if (totalProdutosNaoParcial > 0)
                    {
                        if (totalProdutosNaoParcial <= totalProdutosNaoParcialEntregues)
                        {
                            pedidosCompletos.Add(pedidoSeparacao);
                        }
                    }
                    else
                    {
                        if (totalProdutosParcial <= totalProdutosParcialEntregue)
                        {
                            pedidosCompletos.Add(pedidoSeparacao);
                        }
                    }
                }
            }


            foreach (var pedidoCompleto in pedidosCompletos)
            {
                lblProcessoEmAndamento3.Invoke((MethodInvoker)delegate
                {
                    lblProcessoEmAndamento3.Text = "Marcando transferencia do pedido " + pedidoCompleto;
                });

                var dataAlterar = new dbSiteEntities();
                var itensTransferencia =
                    (from c in dataAlterar.tbItensPedido
                     where (c.tbProdutos.requerTransferencia ?? false) == true && c.pedidoId == pedidoCompleto && (c.cancelado ?? false) == false && (c.enviado ?? false) == false
                     && (c.statusTransferencia ?? 0) == 0
                     select c);
                foreach (var itemTransferencia in itensTransferencia)
                {
                    itemTransferencia.statusTransferencia = 1;
                }
                var itensTransferenciaCombo =
                    (from c in dataAlterar.tbItensPedidoCombo
                     where (c.tbProdutos.requerTransferencia ?? false) == true && c.tbItensPedido.pedidoId == pedidoCompleto && (c.tbItensPedido.cancelado ?? false) == false && (c.enviado) == false
                     && (c.statusTransferencia ?? 0) == 0
                     select c);
                foreach (var itemTransferencia in itensTransferenciaCombo)
                {
                    itemTransferencia.statusTransferencia = 1;
                }
                dataAlterar.SaveChanges();
            }
            #endregion


            #region finalizaQueue
            queue.concluido = true;
            queue.andamento = true;
            queue.dataConclusao = DateTime.Now;
            data.SaveChanges();

            var novoQueue = new tbQueue();
            novoQueue.agendamento = Convert.ToDateTime(DateTime.Now.AddDays(1).ToShortDateString() + " 06:00:00");
            novoQueue.tipoQueue = 14;
            novoQueue.mensagem = "";
            novoQueue.concluido = false;
            novoQueue.andamento = false;
            data.tbQueue.Add(novoQueue);
            data.SaveChanges();

            #endregion
        }

        private void bwListaTransferencia_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            lblProcessoEmAndamento3.Invoke((MethodInvoker)delegate
            {
                lblProcessoEmAndamento3.Text = "";
            });

        }

        private void btnChecarCompletos_Click(object sender, EventArgs e)
        {
            timerChecaCompletos.Interval = 1;
        }


        List<tbColecao> colecoes = new List<tbColecao>();
        List<tbJuncaoProdutoColecao> juncaoColecoes = new List<tbJuncaoProdutoColecao>();
        List<tbProdutoCategoria> categorias = new List<tbProdutoCategoria>();
        List<tbJuncaoProdutoCategoria> juncaoCategorias = new List<tbJuncaoProdutoCategoria>();
        List<tbProdutoFoto> fotos = new List<tbProdutoFoto>();
        List<tbProdutoVideo> videos = new List<tbProdutoVideo>();
        List<tbProdutos> listaProdutos = new List<tbProdutos>();
        List<tbProdutoFornecedor> listaFornecedores = new List<tbProdutoFornecedor>();
        List<tbInformacaoAdcional> listaInformacoes = new List<tbInformacaoAdcional>();
        DateTime lastListUpdates = Convert.ToDateTime("01/01/2016");
        bool utilizarListasProduto = false;

        private void bwAtualizarProdutoElastic_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                var dataInicio = DateTime.Now;

                #region atualizaQueue

                var data = new dbSiteEntities();
                int idQueue = Convert.ToInt32(e.Argument);
                var queue = (from c in data.tbQueue where c.idQueue == idQueue select c).First();
                queue.andamento = true;
                queue.dataInicio = dataInicio;
                data.SaveChanges();

                #endregion atualizaQueue

                lblProcessoEmAndamento3.Invoke((MethodInvoker)delegate
               {
                   lblProcessoEmAndamento3.Text = "Atualizando produto no Elastic Search ";
               });


                //rnBuscaCloudSearch.AtualizarProduto((int) queue.idRelacionado);
                var elastic = ElasticSearch.ElasticSearchProdutoRepository.Instance;
                if (utilizarListasProduto)
                {
                    elastic.AtualizarProduto((int)queue.idRelacionado, listaProdutos, listaFornecedores,
                        juncaoCategorias, categorias, juncaoColecoes, colecoes, fotos, videos, listaInformacoes);
                }
                else
                {
                    try
                    {
                        elastic.AtualizarProduto((int)queue.idRelacionado);
                    }
                    catch (Exception ex)
                    {
                        var dataNovaQueue = new dbSiteEntities();
                        var novaQueue = new tbQueue() { idRelacionado = (int)queue.idRelacionado, agendamento = DateTime.Now, andamento = false, concluido = false, mensagem = "", tipoQueue = 15 };
                        dataNovaQueue.tbQueue.Add(novaQueue);
                        dataNovaQueue.SaveChanges();
                        rnEmails.EnviaEmail("", "andre@bark.com.br", "", "", "", ex.InnerException + "<br>" + ex.ToString(), "Erro Elastic " + queue.idRelacionado);
                    }
                }

                #region finalizaQueue

                queue.concluido = true;
                queue.andamento = true;
                queue.dataConclusao = DateTime.Now;
                data.SaveChanges();

                var queuesRelacionados = (from c in data.tbQueue
                                          where
                                              c.tipoQueue == queue.tipoQueue && c.idRelacionado == queue.idRelacionado &&
                                              c.agendamento < dataInicio && c.andamento == false && c.concluido == false
                                          select c).ToList();
                foreach (var queueRelacionado in queuesRelacionados)
                {
                    queueRelacionado.dataInicio = dataInicio;
                    queueRelacionado.andamento = true;
                    queueRelacionado.concluido = true;
                    queueRelacionado.dataConclusao = queue.dataConclusao;
                }
                data.SaveChanges();

                #endregion
            }
            catch (Exception ex)
            {
                rnEmails.EnviaEmail("", "andre@bark.com.br", "", "", "", ex.InnerException.ToString(), "Erro Atualizar Produto");
            }
        }

        private void bwAtualizarProdutoElastic_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            lblProcessoEmAndamento3.Invoke((MethodInvoker)delegate
            {
                lblProcessoEmAndamento3.Text = "";
            });
        }

        private void bwAtualizarCategoriaElastic_DoWork(object sender, DoWorkEventArgs e)
        {

            #region atualizaQueue
            var data = new dbSiteEntities();
            int idQueue = Convert.ToInt32(e.Argument);
            var queue = (from c in data.tbQueue where c.idQueue == idQueue select c).First();
            queue.andamento = true;
            queue.dataInicio = DateTime.Now;
            data.SaveChanges();
            #endregion atualizaQueue

            int categoriaId = (int)queue.idRelacionado;
            var produtosCategoria =
                (from c in data.tbJuncaoProdutoCategoria where c.categoriaId == categoriaId select c.produtoId).ToList()
                    .Distinct();

            var dataNovaQueue = new dbSiteEntities();
            foreach (var produtoCategoria in produtosCategoria)
            {
                var novaQueue = new tbQueue() { idRelacionado = produtoCategoria, agendamento = DateTime.Now, andamento = false, concluido = false, mensagem = "", tipoQueue = 15 };
                dataNovaQueue.tbQueue.Add(novaQueue);
            }
            dataNovaQueue.SaveChanges();


            #region finalizaQueue
            queue.concluido = true;
            queue.andamento = true;
            queue.dataConclusao = DateTime.Now;
            data.SaveChanges();
            #endregion
        }

        private void bwAtualizarCategoriaElastic_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {

        }

        private void bwAtualizarRelevancia_DoWork(object sender, DoWorkEventArgs e)
        {
            #region atualizaQueue
            var data = new dbSiteEntities();
            int idQueue = Convert.ToInt32(e.Argument);
            var queue = (from c in data.tbQueue where c.idQueue == idQueue select c).First();
            queue.andamento = true;
            queue.dataInicio = DateTime.Now;
            data.SaveChanges();
            #endregion atualizaQueue

            #region Atualiza Relevancia
            var pedidosDc = new dbSiteEntities();
            lblRelevancia.Invoke((MethodInvoker)delegate
            {
                lblRelevancia.Text = "Carregando produtos";
            });

            var produtos = (from c in pedidosDc.tbProdutos where c.produtoAtivo.ToLower() == "true" select c).ToList();
            foreach (var produto in produtos)
            {
                lblRelevancia.Invoke((MethodInvoker)delegate
                {
                    lblRelevancia.Text = produto.produtoId + " " + produto.produtoNome;
                });
                var dataCheck = new dbSiteEntities();
                var vendas = dataCheck.quantidadeVendasProduto(DateTime.Now.AddMonths(-2).ToShortDateString(), produto.produtoId).First();
                if (produto.produtoDataDaCriacao > DateTime.Now.AddMonths(-2) && vendas > 0)
                {
                    var totalDias = (DateTime.Now - produto.produtoDataDaCriacao).TotalDays;
                    var totalDiasRelatorio = (DateTime.Now - DateTime.Now.AddMonths(-2)).TotalDays;
                    var vendasPorDia = Convert.ToDouble(vendas) / totalDias;
                    vendas = Convert.ToInt32(vendasPorDia * totalDiasRelatorio);
                }
                dataCheck.Dispose();
                lblRelevancia.Invoke((MethodInvoker)delegate
                {
                    lblRelevancia.Text = vendas + "Vendas carregadas " + produto.produtoId + " " + produto.produtoNome;
                });
                if (produto.relevancia != (int)vendas)
                {
                    lblRelevancia.Invoke((MethodInvoker)delegate
                    {
                        lblRelevancia.Text = "Alterando " + produto.produtoId + " " + produto.produtoNome;
                    });
                    var dataAlt = new dbSiteEntities();
                    var produtoAlt =
                        (from c in dataAlt.tbProdutos where c.produtoId == produto.produtoId select c).First();
                    produtoAlt.relevancia = (int)vendas;
                    dataAlt.SaveChanges();
                    dataAlt.Dispose();
                }
            }
            #endregion


            #region finalizaQueue
            queue.concluido = true;
            queue.andamento = true;
            queue.dataConclusao = DateTime.Now;
            data.SaveChanges();

            var novoQueue = new tbQueue();
            novoQueue.agendamento = Convert.ToDateTime(DateTime.Now.AddDays(1).ToShortDateString() + " 04:00:00");
            novoQueue.tipoQueue = 17;
            novoQueue.mensagem = "";
            novoQueue.concluido = false;
            novoQueue.andamento = false;
            data.tbQueue.Add(novoQueue);
            data.SaveChanges();

            #endregion
        }

        private void bwAtualizarRelevancia_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            lblRelevancia.Invoke((MethodInvoker)delegate
            {
                lblRelevancia.Text = "Aguardando Checagem Relevancia";
            });
        }

        private void bwAtualizarTodosElastic_DoWork(object sender, DoWorkEventArgs e)
        {

            lblElastic.Invoke((MethodInvoker)delegate
            {
                lblElastic.Text = "Atualizando Elastic";
            });

            #region atualizaQueue
            var data = new dbSiteEntities();
            int idQueue = Convert.ToInt32(e.Argument);
            var queue = (from c in data.tbQueue where c.idQueue == idQueue select c).First();
            queue.andamento = true;
            queue.dataInicio = DateTime.Now;
            data.SaveChanges();
            #endregion atualizaQueue

            #region Atualiza Elastic
            //var produtos = (from c in data.tbProdutos orderby c.tbProdutoFornecedor.relevanciaProdutos, c.relevancia select c.produtoId).ToList();
            int produtoInicial = listaProdutos.Select(x => x.produtoId).OrderBy(x => x).First();
            int produtoFinal = listaProdutos.Select(x => x.produtoId).OrderByDescending(x => x).First();
            int total = listaProdutos.Count;
            int atual = 0;

            lblLastListUpdate.Invoke((MethodInvoker)delegate
            {
                lblLastListUpdate.Text = "Atualizando Listas";
            });
            lastListUpdates = DateTime.Now;
            using (new TransactionScope(TransactionScopeOption.Required, new TransactionOptions
            {
                IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
            }))
            {
                using (var dataCol = new dbSiteEntities())
                {
                    colecoes = (from c in dataCol.tbColecao select c).ToList();
                    juncaoColecoes = (from c in dataCol.tbJuncaoProdutoColecao select c).ToList();
                    categorias = (from c in dataCol.tbProdutoCategoria select c).ToList();
                    juncaoCategorias =
                        (from c in dataCol.tbJuncaoProdutoCategoria select c).ToList();
                    fotos = (from c in dataCol.tbProdutoFoto select c).ToList();
                    videos = (from c in dataCol.tbProdutoVideo select c).ToList();
                    listaProdutos = (from c in dataCol.tbProdutos select c).ToList();
                    listaFornecedores = (from c in dataCol.tbProdutoFornecedor select c).ToList();
                    listaInformacoes = (from c in dataCol.tbProdutos
                                        where c.produtoAtivo.ToLower() == "true"
                                        join d in dataCol.tbInformacaoAdcional on c.produtoId equals d.produtoId
                                        select d).ToList().Distinct().ToList();
                }
            }

            lblLastListUpdate.Invoke((MethodInvoker)delegate
            {
                lblLastListUpdate.Text = "Ultimo Update Lista:" + lastListUpdates.ToString();
            });

            for (int i = produtoInicial; i <= produtoFinal; i++)
            {
                if (!listaProdutos.Select(x => x.produtoId).Any(x => x == i))
                {
                    atual++;
                    lblElastic.Invoke((MethodInvoker)delegate
                   {
                       lblElastic.Text = "Atualizando " + atual + "/" + total;
                   });
                    try
                    {
                        //rnBuscaCloudSearch.AtualizarProduto(i);
                        var elastic = ElasticSearch.ElasticSearchProdutoRepository.Instance;
                        //elastic.AtualizarProduto(i);
                        elastic.AtualizarProduto(i, listaProdutos, listaFornecedores, juncaoCategorias, categorias, juncaoColecoes, colecoes, fotos, videos, listaInformacoes);
                    }
                    catch (Exception ex)
                    {
                    }

                }
            }
            atual = 0;
            foreach (var produto in listaProdutos)
            {
                atual++;
                lblElastic.Invoke((MethodInvoker)delegate
                {
                    lblElastic.Text = "Atualizando " + atual + "/" + total;
                });
                try
                {
                    //rnBuscaCloudSearch.AtualizarProduto(produto);
                    var elastic = ElasticSearch.ElasticSearchProdutoRepository.Instance;
                    //elastic.AtualizarProduto(produto);
                    elastic.AtualizarProduto(produto.produtoId, listaProdutos, listaFornecedores, juncaoCategorias, categorias, juncaoColecoes, colecoes, fotos, videos, listaInformacoes);
                }
                catch (Exception ex) { }
                //ElasticSearch.ElasticSearchProdutoRepository.AtualizarProduto(produto, listaProdutos, listaFornecedores, juncaoCategorias, categorias, juncaoColecoes, colecoes, fotos, videos);
            }
            lastListUpdates = DateTime.Now.AddDays(-1);
            colecoes = new List<tbColecao>();
            juncaoColecoes = new List<tbJuncaoProdutoColecao>();
            categorias = new List<tbProdutoCategoria>();
            juncaoCategorias = new List<tbJuncaoProdutoCategoria>();
            fotos = new List<tbProdutoFoto>();
            videos = new List<tbProdutoVideo>();
            listaProdutos = new List<tbProdutos>();
            listaFornecedores = new List<tbProdutoFornecedor>();
            listaInformacoes = new List<tbInformacaoAdcional>();
            #endregion


            #region finalizaQueue
            queue.concluido = true;
            queue.andamento = true;
            queue.dataConclusao = DateTime.Now;
            data.SaveChanges();

            var novoQueue = new tbQueue();
            novoQueue.agendamento = Convert.ToDateTime(DateTime.Now.AddDays(1).ToShortDateString() + " 04:30:00");
            novoQueue.tipoQueue = 18;
            novoQueue.mensagem = "";
            novoQueue.concluido = false;
            novoQueue.andamento = false;
            data.tbQueue.Add(novoQueue);
            data.SaveChanges();

            #endregion
        }

        private void bwAtualizarTodosElastic_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            lblElastic.Invoke((MethodInvoker)delegate
            {
                lblElastic.Text = "Aguardando Checagem Elastic";
            });
        }

        private void bwAtualizarFornecedorElastic_DoWork(object sender, DoWorkEventArgs e)
        {
            #region atualizaQueue
            var data = new dbSiteEntities();
            int idQueue = Convert.ToInt32(e.Argument);
            var queue = (from c in data.tbQueue where c.idQueue == idQueue select c).First();
            queue.andamento = true;
            queue.dataInicio = DateTime.Now;
            data.SaveChanges();
            #endregion atualizaQueue

            int fornecedorId = (int)queue.idRelacionado;
            var produtosFornecedor =
                (from c in data.tbProdutos where c.produtoFornecedor == fornecedorId select c.produtoId).ToList()
                    .Distinct();

            foreach (var produtoCategoria in produtosFornecedor)
            {
                lblProcessoEmAndamento3.Invoke((MethodInvoker)delegate
                {
                    lblProcessoEmAndamento3.Text = "Atualizando fornecedorid elastic  " + fornecedorId;
                });

                //rnBuscaCloudSearch.AtualizarProduto(produtoCategoria);
                var elastic = ElasticSearch.ElasticSearchProdutoRepository.Instance;
                elastic.AtualizarProduto(produtoCategoria);
            }


            #region finalizaQueue
            queue.concluido = true;
            queue.andamento = true;
            queue.dataConclusao = DateTime.Now;
            data.SaveChanges();
            #endregion

            lblProcessoEmAndamento3.Invoke((MethodInvoker)delegate
            {
                lblProcessoEmAndamento3.Text = "";
            });
        }

        #region Gerar Feeds
        private void timerFeeds_Tick(object sender, EventArgs e)
        {
            timerFeeds.Enabled = false;
            if (bwFeeds.IsBusy != true)
            {
                bwFeeds.RunWorkerAsync();
            }
        }
        private void bwFeeds_DoWork(object sender, DoWorkEventArgs e)
        {
            rnFeeds.GerarFeeds();
        }

        private void bwFeeds_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            timerFeeds.Interval = 7200000;
            timerFeeds.Enabled = true;
            btnFeeds.Enabled = true;
        }
        #endregion

        private void bwCarregamentoJadLog_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;

            if ((DateTime.Now.Hour >= 07 && DateTime.Now.Hour <= 23) | jadlogAgora)
            {

                lblCarregamentoJadLog.Invoke((MethodInvoker)delegate
                {
                    lblCarregamentoJadLog.Text = "Carregando";
                });

                btnChecarEnviosJadLog.Invoke((MethodInvoker)delegate
                {
                    btnChecarEnviosJadLog.Enabled = false;
                });

                var data = new dbSiteEntities();


                var listaPacotesSemRastreio = (from ped in data.tbPedidos
                                               join pac in data.tbPedidoPacote on ped.pedidoId equals pac.idPedido
                                               join en in data.tbPedidoEnvio on ped.pedidoId equals en.idPedido
                                               where ((pac.despachado ?? false) == false | pac.rastreio == "") && (pac.formaDeEnvio == "jadlog" | pac.formaDeEnvio == "jadlogexpressa") && !(pac.rastreio.StartsWith("18")) && !(pac.rastreio.StartsWith("1008")) && pac.codJadlog != "-1" && pac.codJadlog != "-2"
                                               select new
                                               {
                                                   ped.pedidoId,
                                                   pac.codJadlog,
                                                   pac.idPedidoEnvio,
                                                   pac.idPedidoPacote
                                               }).ToList().OrderByDescending(x => x.idPedidoPacote);
                int total = listaPacotesSemRastreio.Count();
                int atual = 0;
                foreach (var listaPacote in listaPacotesSemRastreio)
                {
                    atual++;
                    var rastreio = rnFrete.consultaCodigoRastreioJadlog(listaPacote.codJadlog, urlTrackingBean);
                    lblCarregamentoJadLog.Invoke((MethodInvoker)delegate
                    {
                        lblCarregamentoJadLog.Text = total + "/" + atual + " - Rastreio JadLog: " + listaPacote.pedidoId.ToString() + " - " + rastreio;
                    });
                    if (rastreio.Trim().StartsWith("18") | rastreio.Trim().StartsWith("10"))
                    {
                        var pacotes = (from c in data.tbPedidoPacote where c.idPedidoEnvio == listaPacote.idPedidoEnvio select c).ToList();
                        foreach (var pacote in pacotes)
                        {
                            pacote.rastreio = rastreio;
                        }
                        data.SaveChanges();
                    }
                    else
                    {
                        string statusRastreio = rnFrete.consultaStatusJadlog(listaPacote.codJadlog, urlTrackingBean);
                        lblCarregamentoJadLog.Invoke((MethodInvoker)delegate
                        {
                            lblCarregamentoJadLog.Text = "Rastreio: " + statusRastreio + total + "/" + atual + " - Rastreio JadLog: " + listaPacote.pedidoId.ToString();
                        });
                        if (!string.IsNullOrEmpty(statusRastreio))
                        {
                            var pedidoPacote = (from c in data.tbPedidoPacote where c.idPedido == listaPacote.pedidoId && c.idPedidoEnvio == listaPacote.idPedidoEnvio orderby c.idPedidoEnvio descending select c).ToList();
                            foreach (var pacoteEnviado in pedidoPacote)
                            {
                                pacoteEnviado.statusAtual = statusRastreio;
                                if (statusRastreio.ToLower().Trim().Contains("custodia") | statusRastreio.ToLower().Trim().Contains("devolvido") | statusRastreio.ToLower().Trim().Contains("rota") | statusRastreio.ToLower().Trim().Contains("sinistro"))
                                {
                                    pacoteEnviado.despachado = true;
                                }
                            }
                            data.SaveChanges();
                        }
                    }

                }




                var listaDePacotes = (from ped in data.tbPedidos
                                      join pac in data.tbPedidoPacote on ped.pedidoId equals pac.idPedido
                                      join en in data.tbPedidoEnvio on ped.pedidoId equals en.idPedido
                                      where (pac.despachado ?? false) == false && (pac.formaDeEnvio == "jadlog" | pac.formaDeEnvio == "jadlogexpressa") && pac.rastreio != "" && ped.statusDoPedido != 6
                                      select new
                                      {
                                          ped.pedidoId,
                                          ped.endNomeDoDestinatario,
                                          ped.idUsuarioEmbalado,
                                          ped.dataEmbaladoFim,
                                          ped.prazoDeEntrega,
                                          pac.rastreio,
                                          en.idPedidoEnvio,
                                          pac.codJadlog
                                      }).ToList().OrderByDescending(x => x.idPedidoEnvio).ToList();

                int totalItens = listaDePacotes.Count;
                int itemAtual = 0;
                int auxPedidoId = 0;

                List<int> listaDeQueues = new List<int>();
                List<int> listaDeAtualizacaoDeInteracao = new List<int>();


                foreach (var pacote in listaDePacotes)
                {

                    itemAtual++;
                    try
                    {
                        lblCarregamentoJadLog.Invoke((MethodInvoker)delegate
                        {
                            lblCarregamentoJadLog.Text = itemAtual.ToString() + "/" + totalItens.ToString() + " - " + "Carregamento JadLog: " + pacote.pedidoId.ToString();
                        });

                        bool despachado = false;

                        lblCarregamentoJadLog.Invoke((MethodInvoker)delegate
                        {
                            lblCarregamentoJadLog.Text = itemAtual.ToString() + "/" + totalItens.ToString() + " - " + "Consultando status JadLog: " + pacote.pedidoId.ToString();
                        });

                        string statusRastreio = rnFrete.consultaStatusJadlog(pacote.rastreio, urlTrackingBean);

                        lblCarregamentoJadLog.Invoke((MethodInvoker)delegate
                        {
                            lblCarregamentoJadLog.Text = itemAtual.ToString() + "/" + totalItens.ToString() + " - " + statusRastreio + " status JadLog: " + pacote.pedidoId.ToString();
                        });

                        if (statusRastreio.ToLower().Trim() == "transferencia" | statusRastreio.ToLower().Trim() == "entrada")
                        {


                            listaDeQueues.Add(pacote.idPedidoEnvio);
                            listaDeAtualizacaoDeInteracao.Add(pacote.pedidoId);
                            despachado = true;

                            auxPedidoId = pacote.pedidoId;

                            var pedidoPacote = (from c in data.tbPedidoPacote where c.idPedido == pacote.pedidoId && c.idPedidoEnvio == pacote.idPedidoEnvio orderby c.idPedidoEnvio descending select c).ToList();

                            foreach (var pacoteEnviado in pedidoPacote)
                            {
                                pacoteEnviado.despachado = true;
                                pacoteEnviado.dataDeCarregamento = DateTime.Now;
                                pacoteEnviado.statusAtual = statusRastreio;
                                data.SaveChanges();

                                string modalidadeJadlog = "";
                                if (pedidoPacote.FirstOrDefault().formaDeEnvio == "jadlogexpressa") modalidadeJadlog = "jadlogexpressa";
                                int pesoTotal = pedidoPacote.Sum(x => x.peso);
                                var cep = Convert.ToInt64(pedidoPacote.FirstOrDefault().tbPedidos.endCep.Replace("-", "").Trim());
                                var valorTotalDoPedido = Convert.ToDecimal(pedidoPacote.FirstOrDefault().tbPedidos.valorCobrado);
                                //var clienteCPFCNPJ = (from c in data.tbClientes where c.clienteId == pedidoPacote.FirstOrDefault().tbPedidos.clienteId select c).FirstOrDefault().clienteCPFCNPJ;
                                var valoresJadlog = rnFrete.CalculaFreteWebserviceJadlog(pesoTotal, cep, modalidadeJadlog, valorTotalDoPedido);

                                if (valoresJadlog.prazo > 0)
                                {
                                    var diasUteisPrazo = rnFuncoes.retornaPrazoDiasUteis((valoresJadlog.prazo + 1), 0);
                                    var prazoFinal = DateTime.Now.AddDays(diasUteisPrazo);

                                    pacoteEnviado.prazoFinalEntrega = prazoFinal;
                                    data.SaveChanges();
                                }
                                else
                                {
                                    var diasUteisPrazo = rnFuncoes.retornaPrazoDiasUteis(Convert.ToInt32(pacote.prazoDeEntrega), 0);
                                    var prazoFinal = DateTime.Now.AddDays(diasUteisPrazo);

                                    pacoteEnviado.prazoFinalEntrega = prazoFinal;
                                }
                            }
                            data.SaveChanges();
                        }
                        else if (statusRastreio.ToLower().Trim().Contains("custodia") | statusRastreio.ToLower().Trim().Contains("devolvido") | statusRastreio.ToLower().Trim().Contains("rota") | statusRastreio.ToLower().Trim().Contains("sinistro"))
                        {
                            var pedidoPacote = (from c in data.tbPedidoPacote where c.idPedido == pacote.pedidoId && c.idPedidoEnvio == pacote.idPedidoEnvio orderby c.idPedidoEnvio descending select c).ToList();
                            foreach (var pacoteEnviado in pedidoPacote)
                            {
                                pacoteEnviado.despachado = true;
                                pacoteEnviado.statusAtual = statusRastreio;
                            }
                            data.SaveChanges();
                        }
                    }
                    catch (Exception ex)
                    {

                        lblCarregamentoJadLog.Invoke((MethodInvoker)delegate
                        {
                            lblCarregamentoJadLog.Text = "Erro Carregamento JadLog: idpedido - " + auxPedidoId + " - " + ex.Message;
                        });
                    }

                }

                try
                {
                    if (listaDeQueues.Count > 0)
                    {
                        var listaSemDuplicidade = listaDeQueues.Distinct();

                        foreach (var queuePedidoEnvio in listaSemDuplicidade)
                        {
                            var queue = new tbQueue();
                            queue.agendamento = DateTime.Now;
                            queue.idRelacionado = queuePedidoEnvio;
                            queue.tipoQueue = 2;
                            queue.concluido = false;
                            queue.andamento = false;
                            queue.mensagem = "";
                            data.tbQueue.Add(queue);
                            data.SaveChanges();

                        }

                    }

                    if (listaDeAtualizacaoDeInteracao.Count > 0)
                    {
                        var listaSemDuplicidade = listaDeAtualizacaoDeInteracao.Distinct();

                        foreach (var interacaoPedido in listaSemDuplicidade)
                        {
                            rnInteracoes.interacaoInclui(interacaoPedido, "Pedido marcado como carregado", "Sistema", "False");
                        }
                    }
                }
                catch (Exception)
                {

                }

                lblCarregamentoJadLog.Invoke((MethodInvoker)delegate
                {
                    lblCarregamentoJadLog.Text = "Concluido JadLog: " + itemAtual.ToString() + "/" + totalItens.ToString() + " - " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                });

            }

            lblCarregamentoJadLog.Invoke((MethodInvoker)delegate
            {
                lblCarregamentoJadLog.Text = "Atualizando rastreios - " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
            });
            rnFrete.atualizaTodosStatusRastreio();
            lblCarregamentoJadLog.Invoke((MethodInvoker)delegate
            {
                lblCarregamentoJadLog.Text = "Rastreios atualizados - " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
            });
        }

        private void bwCarregamentoJadLog_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            timerCarramentoJadLog.Interval = 3600000;
            timerCarramentoJadLog.Enabled = true;
            btnChecarEnviosJadLog.Enabled = true;
            jadlogAgora = false;
        }

        private void timerCarramentoJadLog_Tick(object sender, EventArgs e)
        {
            timerCarramentoJadLog.Enabled = false;
            btnChecarEnviosJadLog.Enabled = false;
            if (bwCarregamentoJadLog.IsBusy != true)
            {
                bwCarregamentoJadLog.RunWorkerAsync();
            }
        }

        private void btnChecarEnviosJadLog_Click(object sender, EventArgs e)
        {
            if (urlTrackingBean == "http://www.jadlog.com.br:8080/JadlogEdiWs/services/TrackingBean?wsdl")
            {
                urlTrackingBean = "http://www.jadlog.com:8080/JadlogEdiWs/services/TrackingBean?wsdl";
                btnChecarEnviosJadLog.Text = "Checar Envios JadLog - " + "www.jadlog.com.br";
            }
            else
            {
                urlTrackingBean = "http://www.jadlog.com.br:8080/JadlogEdiWs/services/TrackingBean?wsdl";
                btnChecarEnviosJadLog.Text = "Checar Envios JadLog - " + "www.jadlog.com";
            }

            timerCarramentoJadLog.Interval = 1;
        }



        //Fila do Estoque

        private void GerarProdutosComboAntigosThread(int idItemPedido)
        {
            var itemPedido = (from c in itensPedidoGeral where c.itemPedidoId == idItemPedido select c).First();
            var pedido = (from c in pedidosGeral where c.pedidoId == itemPedido.pedidoId select c).FirstOrDefault();
            if (pedido == null) return;

            var dataAdicionar = new dbSiteEntities();
            int loteAdicionar = 0;


            var itensCombo = itensPedidoComboGeral.Where(x => x.idItemPedido == itemPedido.itemPedidoId).ToList();
            if (itensCombo.Any())
            {
                foreach (var itemCombo in itensCombo)
                {
                    var totalDoCombo = itensPedidoComboGeral.Count(x => x.idItemPedido == itemPedido.itemPedidoId && x.produtoId == itemCombo.produtoId) * Convert.ToInt32(itemPedido.itemQuantidade);
                    var totalAtual = (from c in dataAdicionar.tbItemPedidoEstoque
                                      where c.itemPedidoId == itemPedido.itemPedidoId && c.produtoId == itemCombo.produtoId
                                      select c).Count();
                    var produtoValida = (from c in dataAdicionar.tbProdutos where c.produtoId == itemCombo.produtoId select c).Any();
                    if (totalAtual < totalDoCombo && produtoValida)
                    {
                        for (int i = totalAtual; i < totalDoCombo; i++)
                        {
                            var itemEstoque = new tbItemPedidoEstoque();
                            itemEstoque.itemPedidoId = itemCombo.idItemPedido;
                            itemEstoque.produtoId = itemCombo.produtoId;
                            itemEstoque.precoDeCusto = itemCombo.precoDeCusto;
                            itemEstoque.enviado = itemCombo.enviado;
                            itemEstoque.dataCriacao = itemPedido.dataDaCriacao;

                            if (itemCombo.enviado | pedido.statusDoPedido == 5)
                            {
                                itemEstoque.enviado = true;
                                itemEstoque.reservado = true;
                            }
                            else if (pedido.statusDoPedido == 1 | pedido.statusDoPedido == 6 | pedido.statusDoPedido == 8 | pedido.statusDoPedido == 9 | pedido.statusDoPedido == 10)
                            {
                                itemEstoque.reservado = false;
                                itemEstoque.cancelado = true;
                            }
                            else
                            {
                                itemEstoque.reservado = false;
                                itemEstoque.cancelado = false;
                            }
                            if ((itemPedido.cancelado ?? false) == true) itemEstoque.cancelado = true;
                            dataAdicionar.tbItemPedidoEstoque.Add(itemEstoque);
                            loteAdicionar++;
                            if (loteAdicionar == 100)
                            {
                                loteAdicionar = 0;
                                dataAdicionar.SaveChanges();
                                dataAdicionar.Dispose();
                                dataAdicionar = new dbSiteEntities();
                            }
                        }
                        loteAdicionar = 0;
                        dataAdicionar.SaveChanges();
                        dataAdicionar.Dispose();
                        dataAdicionar = new dbSiteEntities();
                    }
                }
            }
            else
            {
                var totalDoProduto = Convert.ToInt32(itemPedido.itemQuantidade);
                var totalAtual = (from c in dataAdicionar.tbItemPedidoEstoque
                                  where c.itemPedidoId == itemPedido.itemPedidoId && c.produtoId == itemPedido.produtoId
                                  select c).Count();
                var produtoValida = (from c in dataAdicionar.tbProdutos where c.produtoId == itemPedido.produtoId select c).Any();
                if (totalAtual < totalDoProduto && produtoValida)
                {
                    for (int i = totalAtual; i < totalDoProduto; i++)
                    {
                        var itemEstoque = new tbItemPedidoEstoque();
                        itemEstoque.itemPedidoId = itemPedido.itemPedidoId;
                        itemEstoque.produtoId = itemPedido.produtoId;
                        itemEstoque.precoDeCusto = itemPedido.valorCusto;
                        itemEstoque.enviado = (itemPedido.enviado ?? false);
                        itemEstoque.dataCriacao = itemPedido.dataDaCriacao;
                        if (itemPedido.enviado == true | pedido.statusDoPedido == 5)
                        {
                            itemEstoque.enviado = true;
                            itemEstoque.reservado = true;
                        }
                        else if (pedido.statusDoPedido == 1 | pedido.statusDoPedido == 6 | pedido.statusDoPedido == 8 | pedido.statusDoPedido == 9 | pedido.statusDoPedido == 10)
                        {
                            itemEstoque.reservado = false;
                            itemEstoque.cancelado = true;
                        }
                        else
                        {
                            itemEstoque.reservado = false;
                            itemEstoque.cancelado = false;
                        }
                        if ((itemPedido.cancelado ?? false)) itemEstoque.cancelado = true;

                        dataAdicionar.tbItemPedidoEstoque.Add(itemEstoque);
                        loteAdicionar++;
                        if (loteAdicionar == 100)
                        {
                            loteAdicionar = 0;
                            dataAdicionar.SaveChanges();
                            dataAdicionar.Dispose();
                            dataAdicionar = new dbSiteEntities();
                        }
                    }
                    loteAdicionar = 0;
                    dataAdicionar.SaveChanges();
                    dataAdicionar.Dispose();
                    dataAdicionar = new dbSiteEntities();
                }
            }

            dataAdicionar.SaveChanges();
        }

        public List<tbItensPedido> itensPedidoGeral = null;
        public List<tbPedidos> pedidosGeral = null;
        public List<tbItensPedidoCombo> itensPedidoComboGeral = null;
        //public List<tbItemPedidoEstoque> itensEstoqueGeral = null;
        public List<tbItensPedido> itemPedidoGeralFaltando = null;

        private void GerarProdutosComboAntigos(bool updateList)
        {
            var dataGeral = new dbSiteEntities();
            label4.Invoke((MethodInvoker)delegate
            {
                label4.Text = "Gerando Combos Antigos";
            });

            //var pedidosGeral = new List<tbPedidos>();
            pedidosGeral = (from c in dataGeral.tbPedidos select c).ToList();
            //itensPedidoGeral = (from c in dataGeral.tbItensPedido where c.itemPedidoId > 0 && c.itemQuantidade > 0 select c).ToList();
            itensPedidoComboGeral = (from c in dataGeral.tbItensPedidoCombo select c).ToList();
            //itensEstoqueGeral = (from c in dataGeral.tbItemPedidoEstoque select c).ToList();

            if (updateList) itensPedidoGeral = (from c in dataGeral.tbItensPedido
                                                join d in dataGeral.tbItemPedidoEstoque on c.itemPedidoId equals d.itemPedidoId into estoqueGeral
                                                where estoqueGeral.Count() == 0 && c.itemQuantidade > 0
                                                select c).ToList();
            else itensPedidoGeral = (from c in dataGeral.tbItensPedido where c.itemPedidoId > 0 && c.itemQuantidade > 0 select c).ToList();
            //if (updateList)
            //{
            //    itensPedidoGeral = itemPedidoGeralFaltando;
            //}

            int totalItens = itensPedidoGeral.Count();
            int itemAtual = 0;

            int loteAdicionar = 0;

            Parallel.ForEach(itensPedidoGeral, itemPedido =>
            {
                itemAtual++;
                label4.Invoke((MethodInvoker)delegate
                {
                    label4.Text = "Gerando Combos Antigos " + itemAtual + "/" + totalItens;
                });

                GerarProdutosComboAntigosThread(itemPedido.itemPedidoId);
            });
            //foreach(var itemPedido in itensPedidoGeral)
            //{
            //    itemAtual++;
            //    label4.Invoke((MethodInvoker) delegate
            //    {
            //        label4.Text = "Gerando Combos Antigos " + itemAtual + "/" + totalItens;
            //    });

            //    GerarProdutosComboAntigosThread(itemPedido.itemPedidoId);
            //};
            label4.Invoke((MethodInvoker)delegate
            {
                label4.Text = "Aguardando Threads";
            });
            //var itensCombo = itensPedidoComboGeral.Where(x => x.idItemPedido == itemPedido.itemPedidoId).ToList();
            //if (itensCombo.Any())
            //{
            //    foreach (var itemCombo in itensCombo)
            //    {
            //        var totalDoCombo = itensPedidoComboGeral.Count(x => x.idItemPedido == itemPedido.itemPedidoId && x.produtoId == itemCombo.produtoId) * Convert.ToInt32(itemPedido.itemQuantidade);
            //        var totalAtual = (from c in itensEstoqueGeral
            //            where c.itemPedidoId == itemPedido.itemPedidoId && c.produtoId == itemCombo.produtoId
            //            select c).Count();
            //        if (totalAtual < totalDoCombo)
            //        {
            //            for (int i = totalAtual; i < totalDoCombo; i++)
            //            {
            //                var itemEstoque = new tbItemPedidoEstoque();
            //                itemEstoque.itemPedidoId = itemCombo.idItemPedido;
            //                itemEstoque.produtoId = itemCombo.produtoId;
            //                itemEstoque.precoDeCusto = itemCombo.precoDeCusto;
            //                itemEstoque.enviado = itemCombo.enviado;
            //                itemEstoque.dataCriacao = itemPedido.dataDaCriacao;
            //                if (itemCombo.enviado | itemPedido.tbPedidos.statusDoPedido == 5)
            //                {
            //                    itemEstoque.enviado = true;
            //                    itemEstoque.reservado = true;
            //                }
            //                else if (itemPedido.tbPedidos.statusDoPedido == 1 | itemPedido.tbPedidos.statusDoPedido == 6 | itemPedido.tbPedidos.statusDoPedido == 8 | itemPedido.tbPedidos.statusDoPedido == 9 | itemPedido.tbPedidos.statusDoPedido == 10)
            //                {
            //                    itemEstoque.reservado = false;
            //                    itemEstoque.cancelado = true;
            //                }
            //                else
            //                {
            //                    itemEstoque.reservado = false;
            //                    itemEstoque.cancelado = false;
            //                }
            //                if ((itemPedido.cancelado ?? false) == true) itemEstoque.cancelado = true;
            //                dataAdicionar.tbItemPedidoEstoque.Add(itemEstoque);
            //                loteAdicionar++;
            //                if (loteAdicionar == 100)
            //                {
            //                    loteAdicionar = 0;
            //                    dataAdicionar.SaveChanges();
            //                    dataAdicionar.Dispose();
            //                    dataAdicionar = new dbSiteEntities();
            //                }
            //            }
            //        }
            //    }
            //}
            //else
            //{
            //    var totalDoProduto = Convert.ToInt32(itemPedido.itemQuantidade);
            //    var totalAtual = (from c in itensEstoqueGeral
            //                      where c.itemPedidoId == itemPedido.itemPedidoId && c.produtoId == itemPedido.produtoId
            //                      select c).Count();
            //    if (totalAtual < totalDoProduto)
            //    {
            //        for (int i = totalAtual; i < totalDoProduto; i++)
            //        {
            //            var itemEstoque = new tbItemPedidoEstoque();
            //            itemEstoque.itemPedidoId = itemPedido.itemPedidoId;
            //            itemEstoque.produtoId = itemPedido.produtoId;
            //            itemEstoque.precoDeCusto = itemPedido.valorCusto;
            //            itemEstoque.enviado = (itemPedido.enviado ?? false);
            //            itemEstoque.dataCriacao = itemPedido.dataDaCriacao;
            //            if (itemPedido.enviado == true | itemPedido.tbPedidos.statusDoPedido == 5)
            //            {
            //                itemEstoque.enviado = true;
            //                itemEstoque.reservado = true;
            //            }
            //            else if (itemPedido.tbPedidos.statusDoPedido == 1 | itemPedido.tbPedidos.statusDoPedido == 6 | itemPedido.tbPedidos.statusDoPedido == 8 | itemPedido.tbPedidos.statusDoPedido == 9 | itemPedido.tbPedidos.statusDoPedido == 10)
            //            {
            //                itemEstoque.reservado = false;
            //                itemEstoque.cancelado = true;
            //            }
            //            else
            //            {
            //                itemEstoque.reservado = false;
            //                itemEstoque.cancelado = false;
            //            }
            //            if ((itemPedido.cancelado ?? false)) itemEstoque.cancelado = true;

            //            dataAdicionar.tbItemPedidoEstoque.Add(itemEstoque);
            //            loteAdicionar++;
            //            if (loteAdicionar == 100)
            //            {
            //                loteAdicionar = 0;
            //                dataAdicionar.SaveChanges();
            //                dataAdicionar.Dispose();
            //                dataAdicionar = new dbSiteEntities();
            //            }
            //        }
            //    }
            //}

        }

        private void bwGerarFilaEstoque_DoWork(object sender, DoWorkEventArgs e)
        {
            #region atualizaQueue
            var data = new dbSiteEntities();
            int idQueue = Convert.ToInt32(e.Argument);
            var queue = (from c in data.tbQueue where c.tipoQueue == 0 && c.andamento == false select c).First();
            queue.andamento = true;
            queue.dataInicio = DateTime.Now;
            data.SaveChanges();
            #endregion atualizaQueue




            timerQueue.Enabled = false;
            forceDisableTimerQueue = true;


            label4.Invoke((MethodInvoker)delegate
            {
                label4.Text = "Cancelando Extra";
            });
            var pedidosExtra = (from c in data.tbPedidos where c.clienteId == 9437 && c.statusDoPedido != 6 select c);
            foreach (var pedidoExtra in pedidosExtra)
            {
                pedidoExtra.statusDoPedido = 6;
                var itensPedido = (from c in data.tbItemPedidoEstoque where c.tbItensPedido.pedidoId == pedidoExtra.pedidoId select c);
                foreach (var itemPedido in itensPedido)
                {
                    itemPedido.cancelado = true;
                }
                data.SaveChanges();
            }

            //GerarProdutosComboAntigos(false);
            GerarProdutosComboAntigos(true);

            label4.Invoke((MethodInvoker)delegate
            {
                label4.Text = "Cancelado extra";
            });

            var pedidos = (from c in data.tbPedidos where c.statusDoPedido == 11 | c.statusDoPedido == 3 orderby c.dataConfirmacaoPagamento select c).ToList();

            label4.Invoke((MethodInvoker)delegate
            {
                label4.Text = "Gerando Itens Pedido";
            });
            var itensPedidoTotal = (from c in data.tbItemPedidoEstoque
                                    where c.cancelado == false && c.enviado == false
                                    select new
                                    {
                                        c.tbItensPedido.pedidoId,
                                        c.itemPedidoId,
                                        c.produtoId
                                    }
                    ).ToList();

            label4.Invoke((MethodInvoker)delegate
            {
                label4.Text = "Gerando Pedidos ao Fornecedor";
            });
            var itensPedidoFornecedorGeral = (from c in data.tbPedidoFornecedorItem
                                              where
                                                  c.entregue == false
                                              orderby c.tbPedidoFornecedor.dataLimite descending
                                              select c).ToList();
            int atual = 0;
            int total = pedidos.Count();
            foreach (var pedido in pedidos)
            {
                atual++;

                label4.Invoke((MethodInvoker)delegate
                {
                    label4.Text = "Pedidos: " + atual + "/" + total;
                });
                if (pedido.pedidoId == 101620)
                {
                    var teste = "";
                }
                var itensPedido = (from c in itensPedidoTotal
                                   where c.pedidoId == pedido.pedidoId
                                   select c).ToList();

                var dataMaxima = pedido.dataConfirmacaoPagamento ?? DateTime.Now;
                foreach (var itemPedido in itensPedido)
                {
                    var itensPedidoFornecedor = (from c in itensPedidoFornecedorGeral
                                                 where
                                                     c.idItemPedido == itemPedido.itemPedidoId && c.entregue == false &&
                                                     c.idProduto == itemPedido.produtoId
                                                 orderby c.tbPedidoFornecedor.dataLimite descending
                                                 select c).FirstOrDefault();
                    if (itensPedidoFornecedor != null)
                    {
                        if (itensPedidoFornecedor.tbPedidoFornecedor.dataLimite > dataMaxima)
                        {
                            dataMaxima = itensPedidoFornecedor.tbPedidoFornecedor.dataLimite;
                        }
                    }
                }

                var dataMaximaPedido = pedido.prazoFinalPedido != null
                    ? (DateTime)pedido.prazoFinalPedido
                    : DateTime.Now;
                var dataMaximaPedidoAtualizado = pedido.prazoMaximoPostagemAtualizado != null
                    ? (DateTime)pedido.prazoMaximoPostagemAtualizado
                    : dataMaximaPedido;

                if (dataMaxima > dataMaximaPedido)
                {
                    if (dataMaximaPedido < dataMaxima && dataMaximaPedidoAtualizado <= dataMaximaPedido)
                    {
                        dataMaxima = dataMaximaPedido;
                    }
                    else
                    {
                        if (dataMaxima > dataMaximaPedidoAtualizado)
                        {
                            dataMaxima = dataMaximaPedidoAtualizado;
                        }
                    }
                }
                if (dataMaxima == (pedido.dataConfirmacaoPagamento ?? DateTime.Now))
                {
                    if (dataMaximaPedidoAtualizado <= dataMaximaPedido)
                    {
                        dataMaxima = dataMaximaPedido;
                    }
                    else
                    {
                        dataMaxima = dataMaximaPedidoAtualizado;
                    }
                }

                var dataAlterar = new dbSiteEntities();
                var produtosEstoqueAlterar =
                    (from c in dataAlterar.tbItemPedidoEstoque where c.tbItensPedido.pedidoId == pedido.pedidoId select c);
                foreach (var produtoEstoqueAlterar in produtosEstoqueAlterar)
                {
                    produtoEstoqueAlterar.dataLimite = dataMaxima;
                }
                dataAlterar.SaveChanges();
            }

            var pedidosAguardandoConfirmacao = (from c in data.tbPedidos where (c.statusDoPedido == 2 | c.statusDoPedido == 7) && c.condDePagamentoId != 25 select c).ToList();
            atual = 0;
            total = pedidosAguardandoConfirmacao.Count();
            foreach (var pedido in pedidosAguardandoConfirmacao)
            {
                atual++;
                label4.Invoke((MethodInvoker)delegate
                {
                    label4.Text = "Aguardando: " + atual + "/" + total;
                });
                var itensPedido = (from c in itensPedidoTotal
                                   where c.pedidoId == pedido.pedidoId
                                   select c).ToList();

                var dataMaxima = pedido.dataHoraDoPedido ?? DateTime.Now;

                int totalItens = 0;
                int totalReservado = 0;
                int totalPendenteFornecedor = 0;

                bool usarDataMaxima = false;
                if (pedido.tipoDePagamentoId == 11)
                {
                    var pagamentos =
                        (from c in data.tbPedidoPagamento where c.pedidoId == pedido.pedidoId && c.cancelado == false select c)
                            .ToList();
                    if (pagamentos.Any())
                    {
                        var ultimoPagamento =
                            (from c in pagamentos
                             where c.pago == false && c.cancelado == false
                             orderby c.dataVencimento descending
                             select c).FirstOrDefault();
                        if (ultimoPagamento != null)
                        {
                            usarDataMaxima = true;
                            dataMaxima = ultimoPagamento.dataVencimento;
                        }
                    }
                }
                else
                {
                    var produtosItensPedido = itensPedido.Select(x => x.produtoId).Distinct().ToList();

                    foreach (var itemPedido in produtosItensPedido)
                    {
                        totalItens += itensPedido.Count(x => x.produtoId == itemPedido);
                        var reservas = (from c in data.tbProdutoReservaEstoque
                                        where c.idPedido == pedido.pedidoId && c.idProduto == itemPedido
                                        select c).Count();
                        totalReservado += reservas;
                        var itensPedidoFornecedor = (from c in data.tbPedidoFornecedorItem
                                                     where
                                                         c.idPedido == pedido.pedidoId && c.entregue == false &&
                                                         c.idProduto == itemPedido
                                                     orderby c.tbPedidoFornecedor.dataLimite descending
                                                     select c).ToList();
                        totalPendenteFornecedor += itensPedidoFornecedor.Count();

                        if (itensPedidoFornecedor.FirstOrDefault() != null)
                        {
                            if (itensPedidoFornecedor.FirstOrDefault().tbPedidoFornecedor.dataLimite > dataMaxima)
                            {
                                dataMaxima = itensPedidoFornecedor.FirstOrDefault().tbPedidoFornecedor.dataLimite;
                            }
                        }
                    }

                    if (totalItens == totalReservado)
                    {
                        dataMaxima = pedido.dataHoraDoPedido ?? DateTime.Now;
                        usarDataMaxima = true;
                    }
                    else if (totalItens <= (totalReservado + totalPendenteFornecedor))
                    {
                        usarDataMaxima = true;
                    }
                }


                var dataAlterar = new dbSiteEntities();
                var produtosEstoqueAlterar =
                    (from c in dataAlterar.tbItemPedidoEstoque where c.tbItensPedido.pedidoId == pedido.pedidoId select c);
                foreach (var produtoEstoqueAlterar in produtosEstoqueAlterar)
                {
                    if (usarDataMaxima) produtoEstoqueAlterar.dataLimite = dataMaxima;
                }
                dataAlterar.SaveChanges();
            }



            label4.Invoke((MethodInvoker)delegate
            {
                label4.Text = "Reservando itens";
            });

            //  ReservarItens(0);


            forceDisableTimerQueue = false;
            timerQueue.Interval = 1;
            timerQueue.Enabled = true;

            return;
        }

        private void bwGerarFilaEstoque_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {

            timerQueue.Enabled = true;
            label4.Invoke((MethodInvoker)delegate
            {
                label4.Text = "Concluido";
            });
        }

        private static void ReservarItens(int produtoId)
        {
            var data = new dbSiteEntities();
            var itensEstoque =
                (from c in data.tbProdutoEstoque
                 where c.enviado == false && c.liberado == true && c.pedidoIdReserva == null && c.produtoId == produtoId
                 select c).ToList();
            var itensEstoqueEspecificos =
                (from c in data.tbProdutoEstoque
                 where c.enviado == false && c.liberado == true && c.pedidoIdReserva == null && c.produtoId == produtoId && c.tbPedidoFornecedorItem.idPedido != null
                 select c).ToList();
            if (produtoId == 0)
            {
                itensEstoque =
                (from c in data.tbProdutoEstoque
                 where c.enviado == false && c.liberado == true && c.pedidoIdReserva == null
                 select c).ToList();

            }
            foreach (var itemEstoque in itensEstoque)
            {
                using (var dataAlt = new dbSiteEntities())
                {
                    using (var dataTransaction = dataAlt.Database.BeginTransaction())
                    {
                        try
                        {

                            var itensAguardandoEstoque = (from c in dataAlt.tbItemPedidoEstoque
                                                          where c.produtoId == itemEstoque.produtoId
                                                                && c.cancelado == false && c.reservado == false && (c.dataLimite != null) && c.idCentroDistribuicao == itemEstoque.idCentroDistribuicao
                                                          orderby c.dataLimite
                                                          select c).ToList();

                            bool itemEstoquePossuiItemEspecifico = (itemEstoque.tbPedidoFornecedorItem.idPedido != null);
                            if (itemEstoquePossuiItemEspecifico)
                            {
                                var itemEspecifico = itensAguardandoEstoque.Any(x => x.tbItensPedido.pedidoId == itemEstoque.tbPedidoFornecedorItem.idPedido);
                                if (itemEspecifico) itensAguardandoEstoque = itensAguardandoEstoque.Where(x => x.tbItensPedido.pedidoId == itemEstoque.tbPedidoFornecedorItem.idPedido).ToList();
                            }
                            else
                            {
                                foreach (var itemAguardando in itensAguardandoEstoque)
                                {
                                    var possuiItemEspecifico = (from c in itensEstoqueEspecificos where c.tbPedidoFornecedorItem.idPedido == itemAguardando.tbItensPedido.pedidoId && c.tbPedidoFornecedorItem.idItemPedido == itemAguardando.itemPedidoId select c).Any();
                                    if (possuiItemEspecifico)
                                    {
                                        itensAguardandoEstoque = itensAguardandoEstoque.Where(x => x.tbItensPedido.pedidoId != itemAguardando.tbItensPedido.pedidoId).ToList();
                                        
                                    }
                                }
                            }


                            var itemAguardandoEstoque = itensAguardandoEstoque.FirstOrDefault();
                            if (itemAguardandoEstoque != null)
                            {
                                itemAguardandoEstoque.reservado = true;
                                itemAguardandoEstoque.dataReserva = DateTime.Now;
                                var itemEstoqueAlterar =
                                    (from c in dataAlt.tbProdutoEstoque
                                     where c.idProdutoEstoque == itemEstoque.idProdutoEstoque
                                     select c).First();
                                itemAguardandoEstoque.idCentroDistribuicao = itemEstoqueAlterar.idCentroDistribuicao;
                                itemEstoqueAlterar.pedidoIdReserva = itemAguardandoEstoque.tbItensPedido.pedidoId;
                                itemEstoqueAlterar.itemPedidoIdReserva = itemAguardandoEstoque.itemPedidoId;
                                itemEstoqueAlterar.idItemPedidoEstoqueReserva = itemAguardandoEstoque.idItemPedidoEstoque;
                            }
                            dataAlt.SaveChanges();
                            dataTransaction.Commit();
                            if (itemAguardandoEstoque != null)
                            {
                                AdicionaQueueChecagemPedidoCompleto(itemAguardandoEstoque.tbItensPedido.pedidoId);
                                AdicionaQueueAtualizaPrazo(itemAguardandoEstoque.tbItensPedido.pedidoId);
                            }
                        }
                        catch (Exception)
                        {

                            dataTransaction.Rollback();
                        }
                    }
                }
            }
        }

        private static void LiberarProdutosCanceladosPedido(long idQueue)
        {
            #region atualizaQueue
            var dataQueue = new dbSiteEntities();
            var queue = (from c in dataQueue.tbQueue where c.idQueue == idQueue select c).First();
            bool firstTry = (queue.dataInicio == null);
            queue.andamento = true;
            queue.dataInicio = DateTime.Now;
            dataQueue.SaveChanges();
            #endregion

            bool gravado = true;

            try
            {
                var pedidoId = Convert.ToInt32(queue.idRelacionado);
                var data = new dbSiteEntities();
                var pedido = (from c in data.tbPedidos where c.pedidoId == pedidoId select c).First();
                var itensPedido = (from c in data.tbItensPedido
                                   where c.pedidoId == pedidoId && (c.cancelado ?? false) == true
                                   select new
                                   {
                                       c.itemPedidoId,
                                       c.produtoId,
                                       c.itemQuantidade,
                                       c.tbProdutos.tbProdutoFornecedor.fornecedorPrazoDeFabricacao
                                   }
                    ).ToList();

                if (pedido.statusDoPedido == 6)
                {
                    itensPedido = (from c in data.tbItensPedido
                                   where c.pedidoId == pedidoId
                                   select new
                                   {
                                       c.itemPedidoId,
                                       c.produtoId,
                                       c.itemQuantidade,
                                       c.tbProdutos.tbProdutoFornecedor.fornecedorPrazoDeFabricacao
                                   }
                    ).ToList();
                }
                foreach (var itemPedidoGeral in itensPedido)
                {
                    var itensAguardando =
                        (from c in data.tbItemPedidoEstoque
                         where c.itemPedidoId == itemPedidoGeral.itemPedidoId
                         select c);
                    foreach (var itemAguardando in itensAguardando)
                    {
                        itemAguardando.cancelado = true;
                        itemAguardando.reservado = false;
                    }
                    data.SaveChanges();
                    var itensReservados =
                        (from c in data.tbProdutoEstoque
                         where c.itemPedidoIdReserva == itemPedidoGeral.itemPedidoId
                         select c);
                    foreach (var itensReservado in itensReservados)
                    {
                        itensReservado.pedidoIdReserva = null;
                        itensReservado.itemPedidoIdReserva = null;
                        itensReservado.idItemPedidoEstoqueReserva = null;
                        data.SaveChanges();
                        AdicionaQueueReservarProduto(itensReservado.produtoId);
                    }
                }

                AdicionaQueueAtualizaPrazo(pedidoId);
            }
            catch (Exception)
            {
                gravado = false;
            }

            if (gravado)
            {
                #region finalizaQueue

                var queueFinalizar = (from c in dataQueue.tbQueue where c.idQueue == idQueue select c).First();
                queueFinalizar.concluido = true;
                queueFinalizar.dataConclusao = DateTime.Now;
                dataQueue.SaveChanges();

                #endregion
            }
            else if (firstTry)
            {
                rnEmails.EnviaEmail("", "andre@bark.com.br", "", "", "", "Erro ao liberar estoque pedido", queue.idQueue.ToString());
                #region retornaQueue
                var queueFinalizar = (from c in dataQueue.tbQueue where c.idQueue == idQueue select c).First();
                queueFinalizar.andamento = false;
                dataQueue.SaveChanges();
                #endregion
            }
        }

        private static void ReservarEstoqueProduto(long idQueue)
        {
            #region atualizaQueue
            var dataInicio = DateTime.Now;
            var dataQueue = new dbSiteEntities();
            var queue = (from c in dataQueue.tbQueue where c.idQueue == idQueue select c).First();
            bool firstTry = (queue.dataInicio == null);
            queue.andamento = true;
            queue.dataInicio = dataInicio;
            dataQueue.SaveChanges();
            #endregion

            ReservarItens(Convert.ToInt32(queue.idRelacionado));


            #region finalizaQueue
            queue.concluido = true;
            queue.dataConclusao = DateTime.Now;
            dataQueue.SaveChanges();

            var queuesProduto = (from c in dataQueue.tbQueue where c.idRelacionado == queue.idRelacionado && c.tipoQueue == 21 && c.agendamento < dataInicio && c.andamento == false && c.concluido == false select c).ToList();
            foreach(var queueProduto in queuesProduto)
            {
                queueProduto.dataInicio = dataInicio;
                queueProduto.concluido = true;
                queueProduto.dataConclusao = DateTime.Now;
                dataQueue.SaveChanges();
            }
            if (queue.idRelacionado == 0)
            {
                var agora = DateTime.Now.AddMinutes(30);
                var novoQueue = new tbQueue()
                {
                    agendamento = agora,
                    andamento = false,
                    concluido = false,
                    idRelacionado = 0,
                    mensagem = "",
                    tipoQueue = 21
                };
                dataQueue.tbQueue.Add(novoQueue);
                dataQueue.SaveChanges();
            }
            #endregion
        }




        public static void AdicionaQueueAtualizaPrazo(int pedidoId)
        {
            var data = new dbSiteEntities();
            var queue = new tbQueue();
            queue.agendamento = DateTime.Now;
            queue.andamento = false;
            queue.concluido = false;
            queue.idRelacionado = pedidoId;
            queue.mensagem = "";
            queue.tipoQueue = 5;
            data.tbQueue.Add(queue);
            data.SaveChanges();
            data.Dispose();
        }

        public static void AdicionaQueueReservarProduto(int produtoId)
        {
            var data = new dbSiteEntities();
            var queue = new tbQueue();
            queue.agendamento = DateTime.Now;
            queue.andamento = false;
            queue.concluido = false;
            queue.idRelacionado = produtoId;
            queue.mensagem = "";
            queue.tipoQueue = 21;
            data.tbQueue.Add(queue);
            data.SaveChanges();
            data.Dispose();
        }


        public static void AdicionaQueueChecagemPedidoCompleto(int pedidoId)
        {
            var data = new dbSiteEntities();
            var queue = new tbQueue();
            queue.agendamento = DateTime.Now;
            queue.andamento = false;
            queue.concluido = false;
            queue.idRelacionado = pedidoId;
            queue.mensagem = "";
            queue.tipoQueue = 8;
            data.tbQueue.Add(queue);
            data.SaveChanges();
            data.Dispose();
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            fechar = true;
            btnFechar.Enabled = false;
        }



        private void geraLista()
        {
            FileInfo file = new FileInfo(ConfigurationManager.AppSettings["caminhoFisico"].ToString() + "relatoriopedidoss.txt");
            if (file.Exists)
            {
                file.Delete();
            }
            StreamWriter sw = file.CreateText();

            var data = new dbSiteEntities();
            lblAndamentoRelatorio.Invoke((MethodInvoker)delegate
            {
                lblAndamentoRelatorio.Text = "Carregando Pedidos";
            });
            var pedidos = (from c in data.tbPedidos where c.statusDoPedido == 11 | c.statusDoPedido == 3 select new { c.pedidoId, c.dataConfirmacaoPagamento }).ToList();
            lblAndamentoRelatorio.Invoke((MethodInvoker)delegate
            {
                lblAndamentoRelatorio.Text = "Carregando Itens Estoque";
            });
            var itensAguardandoEstoque =
                (from c in data.tbItemPedidoEstoque
                 where c.cancelado == false && c.enviado == false && c.reservado == false && c.dataLimite != null
                 select c).ToList();

            lblAndamentoRelatorio.Invoke((MethodInvoker)delegate
            {
                lblAndamentoRelatorio.Text = "Carregando Itens Fornecedor";
            });
            var itensFornecedor = (from c in data.tbPedidoFornecedorItem
                                   join d in data.tbProdutoFornecedor on c.tbPedidoFornecedor.idFornecedor equals d.fornecedorId
                                   where c.entregue == false | (c.entregue == true && (c.liberado ?? false) == false)
                                   select new { c.idProduto, c.idPedidoFornecedorItem, c.tbPedidoFornecedor.dataLimite, c.tbPedidoFornecedor.idPedidoFornecedor, c.entregue, d.fornecedorNome }).ToList();

            lblAndamentoRelatorio.Invoke((MethodInvoker)delegate
            {
                lblAndamentoRelatorio.Text = "Carregando Itens Pedido";
            });
            var itensPedido =
                (from c in data.tbItensPedido
                 where c.tbPedidos.statusDoPedido == 11 | c.tbPedidos.statusDoPedido == 3
                 select new { c.pedidoId, c.itemPedidoId }).ToList();
            foreach (var pedido in pedidos)
            {
                lblAndamentoRelatorio.Invoke((MethodInvoker)delegate
                {
                    lblAndamentoRelatorio.Text = pedido.pedidoId + " - " + pedido.dataConfirmacaoPagamento.Value.ToShortDateString();
                });
                sw.WriteLine(" ");
                sw.WriteLine("Pedido: " + pedido);
                var itens = (from c in itensPedido where c.pedidoId == pedido.pedidoId select c.itemPedidoId).ToList();
                var itensEstoque = (from c in itensAguardandoEstoque where itens.Contains(c.itemPedidoId) select new { c.tbProdutos.produtoNome, c.tbProdutos.produtoId, c.idItemPedidoEstoque }).ToList();
                var itensReservados = (from c in itensAguardandoEstoque where itens.Contains(c.itemPedidoId) select c).ToList();
                sw.WriteLine("Reservados: " + itensReservados.Count);
                foreach (var itemEstoque in itensEstoque)
                {
                    var aguardandoGeral = (from c in itensAguardandoEstoque
                                           where c.produtoId == itemEstoque.produtoId && c.reservado == false && c.cancelado == false && c.dataLimite != null
                                           orderby c.dataLimite
                                           select new
                                           {
                                               c.idItemPedidoEstoque,
                                               c.tbItensPedido.pedidoId,
                                               c.dataCriacao,
                                               dataLimiteReserva = c.dataLimite,
                                               idProduto = c.produtoId,
                                               idItemPedido = c.itemPedidoId
                                           }).ToList();
                    var pedidosFornecedorGeral = (from c in itensFornecedor
                                                  where c.idProduto == itemEstoque.produtoId
                                                  orderby c.dataLimite, c.idPedidoFornecedor
                                                  select new
                                                  {
                                                      c.idPedidoFornecedor,
                                                      c.idPedidoFornecedorItem,
                                                      dataLimiteFornecedor = c.dataLimite,
                                                      c.idProduto,
                                                      c.entregue,
                                                      c.fornecedorNome
                                                  }).ToList();
                    var listaFinal = (from c in aguardandoGeral.Select((item, index) => new { item, index })
                                      join d in pedidosFornecedorGeral.Select((item, index) => new { item, index }) on c.index equals d.index into
                                          pedidosFornecedorListaInterna
                                      from f in pedidosFornecedorListaInterna.DefaultIfEmpty()
                                      select new
                                      {
                                          c.item.idItemPedidoEstoque,
                                          c.item.pedidoId,
                                          c.item.dataCriacao,
                                          c.item.dataLimiteReserva,
                                          c.item.idItemPedido,
                                          entregue = f != null ? f.item.entregue : false,
                                          idPedidoFornecedor = f != null ? f.item.idPedidoFornecedor : 99,
                                          fornecedorNome = f != null ? f.item.fornecedorNome : "",
                                          dataLimiteFornecedor = f != null ? (DateTime?)f.item.dataLimiteFornecedor : null,
                                          idPedidoFornecedorItem = f != null ? (int?)f.item.idPedidoFornecedorItem : null,
                                      }
                        ).ToList();
                    var itemNaLista = listaFinal.FirstOrDefault(x => x.idItemPedidoEstoque == itemEstoque.idItemPedidoEstoque);
                    string linha = itemEstoque.produtoNome + " - ";
                    if (itemNaLista != null && itemNaLista.idPedidoFornecedor != 99)
                    {
                        if (itemNaLista.entregue)
                        {
                            linha += "Aguardando Endereçar - ";
                        }
                        linha +=
                            String.Format(
                                "Romaneio: {0} - Fornecedor: {1} - Data Prevista: {2} - Data limite: {3}<br>",
                                itemNaLista.idPedidoFornecedor, itemNaLista.fornecedorNome,
                                itemNaLista.dataLimiteFornecedor == null
                                    ? "-"
                                    : itemNaLista.dataLimiteFornecedor.Value.ToShortDateString(),
                                itemNaLista.dataLimiteReserva == null
                                    ? "-"
                                    : itemNaLista.dataLimiteReserva.Value.ToShortDateString());
                    }
                    else if (itemNaLista.idPedidoFornecedor == 99)
                    {
                        linha += "Fora da Fila - " + (itemNaLista.dataLimiteReserva == null
                                    ? "-"
                                    : itemNaLista.dataLimiteReserva.Value.ToShortDateString());
                    }
                    else
                    {
                        linha += "Fora da Fila";
                    }
                    sw.WriteLine(linha);
                }
            }

            lblAndamentoRelatorio.Invoke((MethodInvoker)delegate
            {
                lblAndamentoRelatorio.Text = "concluido";
            });
            sw.Close();
        }

        private void bwGerarRelatorioFila_DoWork(object sender, DoWorkEventArgs e)
        {
            btnGerarRelatorioFila.Enabled = false;
            geraListaFila2();
            btnGerarRelatorioFila.Enabled = true;
        }

        private void btnGerarRelatorioFila_Click(object sender, EventArgs e)
        {
            bwGerarRelatorioFila.RunWorkerAsync();
        }

        private void geraListaFila2()
        {
            FileInfo file = new FileInfo(ConfigurationManager.AppSettings["caminhoFisico"].ToString() + "relatoriofila.txt");
            if (file.Exists)
            {
                file.Delete();
            }
            StreamWriter sw = file.CreateText();

            List<int> idsLiberar = new List<int>();


            using (var data = new dbSiteEntities())
            {
                lblAndamentoRelatorio.Invoke((MethodInvoker)delegate
                {
                    lblAndamentoRelatorio.Text = "Carregando Pedidos";
                });

                int countGeral = 0;
                var dataLimiteReserva = DateTime.Now.AddHours(-10);
                var reservadosGeral = (from c in data.tbProdutoEstoque
                                       join d in data.tbItemPedidoEstoque on c.idItemPedidoEstoqueReserva equals d.idItemPedidoEstoque
                                       where c.enviado == false && c.idItemPedidoEstoqueReserva != null && c.pedidoId == null && d.dataReserva < dataLimiteReserva
                                       && ((d.tbItensPedido.tbPedidos.itensPendentesEnvioCd4 > 0 ? d.tbItensPedido.tbPedidos.envioLiberadoCd4 : false) == false && (d.tbItensPedido.tbPedidos.itensPendentesEnvioCd5 > 0 ? d.tbItensPedido.tbPedidos.envioLiberadoCd5 : false) == false)
                                       orderby c.idPedidoFornecedorItem
                                       select new
                                       {
                                           c.pedidoIdReserva,
                                           c.itemPedidoIdReserva,
                                           c.produtoId,
                                           c.tbProdutos.produtoNome,
                                           c.idPedidoFornecedorItem,
                                           d.dataReserva,
                                           d.dataLimite
                                       }).ToList();
                countGeral = reservadosGeral.Count;
                int idAtual = 0;
                int totalLiberar = 0;
                int totalLiberado = 0;
                foreach (var reservado in reservadosGeral)
                {
                    idAtual++;
                    lblAndamentoRelatorio.Invoke((MethodInvoker)delegate
                    {
                        lblAndamentoRelatorio.Text = "Relatorio Fila: " + idAtual.ToString() + "/" + countGeral.ToString();
                    });

                    string linha = "";
                    var itensAguardando = (from c in data.tbItemPedidoEstoque where c.dataLimite != null && c.reservado == false && c.cancelado == false && c.produtoId == reservado.produtoId && c.dataLimite < reservado.dataLimite select c).ToList();
                    if (itensAguardando.Any())
                    {
                        totalLiberar++;
                        linha += "Liberar - " + ((DateTime)itensAguardando.First().dataLimite).ToShortDateString() + " - Atual: " + ((DateTime)reservado.dataLimite).ToShortDateString() + " - " + reservado.produtoNome + " - " + reservado.pedidoIdReserva;
                    }
                    else
                    {
                        linha += "Manter - " + ((DateTime)reservado.dataLimite).ToShortDateString() + " - " + reservado.produtoNome + " - " + reservado.pedidoIdReserva;
                    }
                    if (totalLiberado <= 200 && itensAguardando.Any())
                    {
                        totalLiberado++;
                        linha += " - liberado";
                        idsLiberar.Add(reservado.idPedidoFornecedorItem);
                    }
                    if (!string.IsNullOrEmpty(linha)) sw.WriteLine(linha);
                }
                sw.WriteLine(totalLiberar.ToString() + "/" + countGeral);
            }

            foreach (var idLiberar in idsLiberar)
            {
                string linha = "";
                var data = new dbSiteEntities();

                var pedidos = (from c in data.tbProdutoEstoque where c.idPedidoFornecedorItem == idLiberar select c).First();
                var itemPedidoEstoque = (from c in data.tbItemPedidoEstoque where c.idItemPedidoEstoque == pedidos.idItemPedidoEstoqueReserva select c).First();
                itemPedidoEstoque.reservado = false;
                itemPedidoEstoque.dataReserva = null;
                pedidos.pedidoIdReserva = null;
                pedidos.idItemPedidoEstoqueReserva = null;
                pedidos.itemPedidoIdReserva = null;
                data.SaveChanges();
                var queue = new tbQueue();
                queue.agendamento = DateTime.Now;
                queue.andamento = false;
                queue.concluido = false;
                queue.idRelacionado = pedidos.produtoId;
                queue.mensagem = "";
                queue.tipoQueue = 21;
                data.tbQueue.Add(queue);
                data.SaveChanges();
            }

            lblAndamentoRelatorio.Invoke((MethodInvoker)delegate
            {
                lblAndamentoRelatorio.Text = "concluido";
            });
            sw.Close();
        }

        private void geraListaFila()
        {
            return;
            FileInfo file = new FileInfo(ConfigurationManager.AppSettings["caminhoFisico"].ToString() + "relatoriofila.txt");
            if (file.Exists)
            {
                file.Delete();
            }
            StreamWriter sw = file.CreateText();

            var data = new dbSiteEntities();
            lblAndamentoRelatorio.Invoke((MethodInvoker)delegate
            {
                lblAndamentoRelatorio.Text = "Carregando Pedidos";
            });

            int countGeral = 0;
            var reservadosGeral = (from c in data.tbProdutoEstoque
                                   where c.enviado == false && c.idItemPedidoEstoqueReserva != null && c.pedidoId == null
                                   orderby c.idPedidoFornecedorItem
                                   select new
                                   {
                                       c.pedidoIdReserva,
                                       c.itemPedidoIdReserva,
                                       c.produtoId,
                                       c.tbProdutos.produtoNome,
                                       c.idPedidoFornecedorItem
                                   }).ToList();
            countGeral = reservadosGeral.Count;

            int idAtual = 0;
            int totalAtual = 200;
            int totalAtualGeral = 0;
            int noCrash = 0;

            while (totalAtual > 0 && noCrash < 150)
            {
                try
                {
                    noCrash++;
                    var reservados = (from c in data.tbProdutoEstoque
                                      where c.enviado == false && c.idItemPedidoEstoqueReserva != null && c.pedidoId == null && c.idPedidoFornecedorItem > idAtual
                                      orderby c.idPedidoFornecedorItem
                                      select new
                                      {
                                          c.pedidoIdReserva,
                                          c.itemPedidoIdReserva,
                                          c.produtoId,
                                          c.tbProdutos.produtoNome,
                                          c.idPedidoFornecedorItem
                                      }).Take(200).ToList();
                    int total = reservados.Count;
                    int atual = 0;
                    totalAtual = total;

                    foreach (var reservado in reservados)
                    {
                        atual++;
                        totalAtualGeral++;
                        idAtual = reservado.idPedidoFornecedorItem;
                        lblAndamentoRelatorio.Text = atual.ToString() + "/" + total.ToString() + "/" + totalAtualGeral.ToString() + "/" + countGeral.ToString() + "/" + noCrash.ToString();
                        if (atual < 20000)
                        {
                            var reservadoDetalhe = (from c in data.tbProdutoEstoque where c.idPedidoFornecedorItem == reservado.idPedidoFornecedorItem && c.enviado == false && c.idItemPedidoEstoqueReserva != null && c.pedidoId == null select c).FirstOrDefault();
                            if (reservadoDetalhe != null)
                            {

                                string linha = "";
                                var itensAguardando = (from c in data.tbItemPedidoEstoque
                                                       where c.tbItensPedido.pedidoId == reservado.pedidoIdReserva && c.reservado == false && c.enviado == false && c.cancelado == false
                                                       select
                                                           c).Count();
                                if (itensAguardando > 0)
                                {
                                    var prazoAguardando = itensAguardando > 0
                                        ? (from c in data.tbItemPedidoEstoque
                                           where
                                               c.tbItensPedido.pedidoId == reservado.pedidoIdReserva && c.reservado == false &&
                                               c.enviado == false && c.cancelado == false
                                           select
                                               c).OrderByDescending(x => x.dataLimite).First().dataLimite
                                        : null;
                                    var primeiroFila = (from c in data.tbItemPedidoEstoque
                                                        where
                                                            c.reservado == false && c.cancelado == false && c.enviado == false && c.dataLimite != null &&
                                                            c.produtoId == reservado.produtoId
                                                        orderby c.dataLimite
                                                        select c).FirstOrDefault();
                                    if (primeiroFila != null)
                                    {
                                        if (primeiroFila.dataLimite < prazoAguardando)
                                        {
                                            linha = reservado.idPedidoFornecedorItem + " - ";
                                            linha = reservado.pedidoIdReserva + " - ";
                                            linha += reservado.produtoNome + " - " + reservado.produtoId;
                                            linha += " - aguardando: " + itensAguardando;
                                            linha += " - prazo: " + prazoAguardando.Value.ToShortDateString();
                                            linha += " - primeiro fila: " + primeiroFila.dataLimite.Value.ToShortDateString();
                                            linha += " - menor";
                                            /* bool temErro = false;
                                             var icTemErro = new System.Data.Entity.Core.Objects.ObjectParameter("icTemErro", typeof(int));
                                             var dsErro = new System.Data.Entity.Core.Objects.ObjectParameter("dsErro", typeof(string));
                                             data.v1_cancelaReserva(reservadoDetalhe.idPedidoFornecedorItem, icTemErro, dsErro);
                                             if (temErro)
                                             {
                                                 linha += " - erro - " + dsErro;
                                             }*/
                                        }
                                    }
                                }

                                if (!string.IsNullOrEmpty(linha)) sw.WriteLine(linha);


                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    sw.WriteLine(ex.ToString());
                }
            }

            lblAndamentoRelatorio.Invoke((MethodInvoker)delegate
            {
                lblAndamentoRelatorio.Text = "concluido";
            });
            sw.Close();
        }

        private void btnFeeds_Click(object sender, EventArgs e)
        {
            timerFeeds.Enabled = true;
            timerFeeds.Interval = 1;
            btnFeeds.Enabled = false;
        }

        private void btnJadlogAgora_Click(object sender, EventArgs e)
        {
            jadlogAgora = true;
            timerCarramentoJadLog.Interval = 1;
        }

        private void GeraPrecosBlack2()
        {
            var data = new dbSiteEntities();
            var produtos =
                (from c in data.tbProdutos
                 where c.produtoPrecoPromocional == c.produtoPrecoPromocionalAntigo && c.produtoAtivo.ToLower() == "true"
                 select c).ToList();
            foreach (var produto in produtos)
            {

                int desconto = 0;
                //if (produto.produtoFornecedor == 20) desconto = 10; 
                if (produto.produtoFornecedor == 110) desconto = 6;
                if (produto.produtoFornecedor == 41) desconto = 10;
                //if (produto.produtoFornecedor == 115) desconto = 20;
                if (produto.produtoFornecedor == 81) desconto = 12;
                //if (produto.produtoFornecedor == 145) desconto = 12;
                if (produto.produtoFornecedor == 111) desconto = 5;
                //if (produto.produtoFornecedor == 64) desconto = 6;
                //if (produto.produtoFornecedor == 87) desconto = 12;
                //if (produto.produtoFornecedor == 122) desconto = 10;
                //if (produto.produtoFornecedor == 109) desconto = 20;
                //if (produto.produtoFornecedor == 47) desconto = 30;
                //if (produto.produtoFornecedor == 121) desconto = 11;
                //if (produto.produtoFornecedor == 113) desconto = 9;
                //if (produto.produtoFornecedor == 120) desconto = 20;
                //if (produto.produtoFornecedor == 108) desconto = 10;
                //if (produto.produtoFornecedor == 48) desconto = 25;
                //if (produto.produtoFornecedor == 137) desconto = 20;
                //if (produto.produtoFornecedor == 139) desconto = 7;
                //if (produto.produtoFornecedor == 79) desconto = 7;
                //if (produto.produtoFornecedor == 63)
                //{
                //    desconto = 20;
                //    if (produto.produtoNome.ToLower().Contains("frasqueira") &&
                //        produto.produtoNome.ToLower().Contains("classic"))
                //    {
                //        desconto = 30;
                //    }
                //}

                if (desconto > 0)
                {
                    if (produto.produtoPrecoPromocional == produto.produtoPrecoPromocionalAntigo)
                    {
                        decimal precoAtivo = produto.produtoPrecoPromocional > 0 &&
                                             produto.produtoPrecoPromocional < produto.produtoPreco
                            ? (decimal)produto.produtoPrecoPromocional
                            : produto.produtoPreco;
                        decimal precoDeCusto = (decimal)produto.produtoPrecoDeCusto;
                        if (precoDeCusto > 0 && precoDeCusto < precoAtivo)
                        {
                            if (produto.produtoFornecedor == 41)
                            {
                                decimal precoDeCustoSemDesconto = (precoDeCusto * 100) / 80;
                                precoDeCusto = precoDeCustoSemDesconto;
                                desconto = 30;
                            }
                            decimal novoCusto = precoDeCusto - ((precoDeCusto / 100) * desconto);
                            decimal precoDesconto = ((decimal)produto.produtoPrecoDeCusto) - novoCusto;
                            decimal novoPreco = precoAtivo - precoDesconto;
                            if (novoPreco > precoDeCusto)
                            {
                                var dataAlt = new dbSiteEntities();
                                var produtoAlt =
                                    (from c in dataAlt.tbProdutos where c.produtoId == produto.produtoId select c).First
                                        ();
                                produtoAlt.produtoPrecoPromocional = novoPreco;
                                dataAlt.SaveChanges();
                            }
                        }
                    }
                }
            }
        }


        private static int EstoqueProduto(int produtoId)
        {
            using (var data = new dbSiteEntities())
            {
                int estoqueLivre = -1;
                var combo =
                    (from c in data.tbProdutoRelacionado where c.idProdutoPai == produtoId select new { c.idProdutoFilho })
                        .ToList();
                if (combo.Any())
                {
                    foreach (var itemCombo in combo)
                    {
                        if (estoqueLivre != 0)
                        {
                            int estoque = EstoqueLivreProduto(itemCombo.idProdutoFilho);
                            if ((estoque < estoqueLivre) | estoqueLivre == -1)
                            {
                                estoqueLivre = estoque;
                            }
                        }
                    }
                    return estoqueLivre;
                }
                return EstoqueLivreProduto(produtoId);
            }
        }


        private static int EstoqueLivreProduto(int produtoId)
        {
            using (var data = new dbSiteEntities())
            {
                var produtoIdItem = new SqlParameter("@produtoId", produtoId);
                var produtoIdEstoque = new SqlParameter("@produtoId", produtoId);
                var totalItensAguardando = data.Database.SqlQuery<int>("v1_ItensAguardandoEstoqueCount @produtoId", produtoIdItem).ToList();
                var totalEstoqueFisico = data.Database.SqlQuery<int>("v1_EstoqueFisicoCount @produtoId", produtoIdEstoque).ToList();

                int itensAguardando = 0;
                int estoqueFisico = 0;
                if (totalItensAguardando.Any()) itensAguardando = totalItensAguardando.First();
                if (totalEstoqueFisico.Any()) estoqueFisico = totalEstoqueFisico.First();
                int estoqueLivre = estoqueFisico - itensAguardando;
                if (estoqueLivre < 0) estoqueLivre = 0;
                return estoqueLivre;
            }
        }

        private void bwBlack_DoWork(object sender, DoWorkEventArgs e)
        {
            //GeraPrecosBlack2();
            //GeraPrecosBlack();
        }

        private void bwBlack_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            button1.Text = "Black OK";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!bwBlack.IsBusy)
            {
                button1.Enabled = false;
                bwBlack.RunWorkerAsync();
            }
        }

        private void timerNotas_Tick(object sender, EventArgs e)
        {
            timerNotas.Enabled = false;
            if (!bwNotas.IsBusy)
            {
                bwNotas.RunWorkerAsync();
            }
        }

        private void bwNotas_DoWork(object sender, DoWorkEventArgs e)
        {
            var emissao = new wsEmissao.EmissaoWs { Url = "http://admin.graodegente.com.br/services/emissaows.asmx" };
            emissao.EmiteNotasFiscais("glmp152029");
            lblProcessoEmAndamento1.Invoke((MethodInvoker)delegate
            {
                lblProcessoEmAndamento1.Text = "Notas Ok";
            });
        }

        private void bwNotas_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            timerNotas.Interval = 30000;
            timerNotas.Enabled = true;
        }

        private void bwAtualizarPrazo_DoWork(object sender, DoWorkEventArgs e)
        {
            rnEstoque.bwAtualizarTodoEstoqueRun();
            return;


            var data = new dbSiteEntities();
            var produtos = (from c in data.tbProdutos select new { c.prazoDeEntrega, c.produtoId, c.tbProdutoFornecedor.fornecedorPrazoDeFabricacao, c.tbProdutoFornecedor.dataFimFabricacao, c.tbProdutoFornecedor.dataInicioFabricacao }).ToList();
            int total = produtos.Count;
            int atual = 0;
            var combos = (from c in data.tbProdutoRelacionado
                          select new
                          {
                              c.idProdutoFilho,
                              c.idProdutoPai,
                              c.tbProdutos.tbProdutoFornecedor.fornecedorPrazoDeFabricacao,
                              c.tbProdutos.tbProdutoFornecedor.dataFimFabricacao,
                              c.tbProdutos.tbProdutoFornecedor.dataInicioFabricacao,
                              prazo = c.tbProdutos.tbProdutoFornecedor.fornecedorPrazoDeFabricacao ?? 1
                          }).ToList();
            foreach (var produto in produtos)
            {
                atual++;
                lblPrazos.Invoke((MethodInvoker)delegate
                {
                    lblPrazos.Text = "Prazos: " + atual + "/" + total;
                });
                int prazoAtualizado = 0;

                var combo = (from c in combos
                             where c.idProdutoPai == produto.produtoId
                             select c).ToList();
                if (combo.Any())
                {
                    foreach (var itemCombo in combo)
                    {
                        int estoque = EstoqueLivreProduto(itemCombo.idProdutoFilho);
                        if (estoque > 0)
                        {
                            combo = combo.Where(x => x.idProdutoFilho != itemCombo.idProdutoFilho).ToList();
                        }
                    }
                    if (combo.Any(
                            x =>
                                x.dataFimFabricacao != null && x.dataFimFabricacao < DateTime.Now &&
                                x.dataInicioFabricacao != null && x.dataInicioFabricacao > DateTime.Now))
                    {
                        int prazoMaximo = 0;
                        foreach (var item in combo)
                        {
                            if (item.dataFimFabricacao != null && item.dataFimFabricacao < DateTime.Now &&
                                item.dataInicioFabricacao != null && item.dataInicioFabricacao > DateTime.Now)
                            {
                                int prazo = rnFuncoes.retornaDiasUteisPeriodo(DateTime.Now, (DateTime)item.dataInicioFabricacao) +
                                            item.prazo;
                                if (prazo > prazoMaximo) prazoMaximo = prazo;
                            }
                        }
                        prazoAtualizado = prazoMaximo;
                    }
                    else
                    {
                        prazoAtualizado = combo.OrderByDescending(x => x.prazo).First().prazo;
                    }
                }
                else
                {
                    var prazosProduto = (from c in data.tbProdutos
                                         where c.produtoId == produto.produtoId
                                         select new
                                         {
                                             prazo = c.tbProdutoFornecedor.fornecedorPrazoDeFabricacao ?? 1,
                                             c.tbProdutoFornecedor.dataFimFabricacao,
                                             c.tbProdutoFornecedor.dataInicioFabricacao
                                         }).First();
                    if (prazosProduto.dataFimFabricacao != null && prazosProduto.dataFimFabricacao < DateTime.Now &&
                        prazosProduto.dataInicioFabricacao != null && prazosProduto.dataInicioFabricacao > DateTime.Now)
                    {
                        int prazo = rnFuncoes.retornaDiasUteisPeriodo(DateTime.Now, (DateTime)prazosProduto.dataInicioFabricacao) + prazosProduto.prazo;
                        prazoAtualizado = prazo;
                    }
                    else
                    {
                        prazoAtualizado = prazosProduto.prazo;
                    }
                }

                if (prazoAtualizado != produto.prazoDeEntrega)
                {
                    var dataAlterar = new dbSiteEntities();
                    var produtoAlterar =
                        (from c in dataAlterar.tbProdutos where c.produtoId == produto.produtoId select c).First();
                    produtoAlterar.prazoDeEntrega = prazoAtualizado;
                    dataAlterar.SaveChanges();
                }
            }
        }

        private void bwAtualizarPrazo_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {

            timerPrazo.Interval = 3600000;
            timerPrazo.Enabled = true;
            lblPrazos.Text = "Ultima atualização de prazos: " + DateTime.Now.ToShortDateString();
        }

        private void timerPrazo_Tick(object sender, EventArgs e)
        {
            lblPrazos.Text = "Inicio atualização de prazos: " + DateTime.Now.ToShortDateString();
            timerPrazo.Enabled = false;
            if (!bwAtualizarPrazo.IsBusy)
            {
                bwAtualizarPrazo.RunWorkerAsync();
            }
        }

        private void btnPrazos_Click(object sender, EventArgs e)
        {
            timerPrazo.Interval = 1;
        }

        private void btnDepoimentos_Click(object sender, EventArgs e)
        {
            btnDepoimentos.Enabled = false;
            if (!bwDepoimentos.IsBusy)
            {
                bwDepoimentos.RunWorkerAsync();
            }
        }



        private void bwDepoimentos_DoWork(object sender, DoWorkEventArgs e)
        {
            rnDepoimentos.GravarDepoimentosFace();
            rnDepoimentos.GravarDepoimentosTrusted();
        }

        private void timerGnre_Tick(object sender, EventArgs e)
        {
            timerGnre.Enabled = false;
            if (!bwGnre.IsBusy)
            {
                bwGnre.RunWorkerAsync();
            }
        }

        private void bwGnre_DoWork(object sender, DoWorkEventArgs e)
        {

            rnGnre.emiteGnres();
            rnGnre.consultaGnres();
        }

        private void bwGnre_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            timerGnre.Interval = 300000;
            timerGnre.Enabled = true;
        }

        private void bwLoteBanco_DoWork(object sender, DoWorkEventArgs e)
        {
            #region atualizaQueue
            var data = new dbSiteEntities();
            int idQueue = Convert.ToInt32(e.Argument);
            var queue = (from c in data.tbQueue where c.idQueue == idQueue select c).First();
            queue.andamento = true;
            queue.dataInicio = DateTime.Now;
            data.SaveChanges();
            #endregion atualizaQueue

            rnLoteBanco.GerarLote();

            #region finalizaQueue
            queue.concluido = true;
            queue.andamento = true;
            queue.dataConclusao = DateTime.Now;
            data.SaveChanges();

            var novoQueue = new tbQueue();
            if (DateTime.Now.Hour > 10 && DateTime.Now.Hour < 16)
            {
                novoQueue.agendamento = Convert.ToDateTime(DateTime.Now.ToShortDateString() + " 16:00:00");
            }
            else
            {
                novoQueue.agendamento = Convert.ToDateTime(DateTime.Now.AddDays(1).ToShortDateString() + " 10:00:00");
            }

            novoQueue.tipoQueue = 23;
            novoQueue.mensagem = "";
            novoQueue.concluido = false;
            novoQueue.andamento = false;
            data.tbQueue.Add(novoQueue);
            data.SaveChanges();

            #endregion
        }

        private void bwLoteBanco_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {

        }


        #region Rastreios TNT
        private BackgroundWorker bwRastreamentoTnt = new BackgroundWorker();

        private void bwRastreamentoTnt_DoWork(object sender, DoWorkEventArgs e)
        {
            //rnFrete.AtualizaRastreioTnt();
        }

        private void bwRastreamentoTnt_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            timerHabilitarRastreamentoTnt.Interval = 3600000;
            timerHabilitarRastreamentoTnt.Enabled = false;
        }

        private void timerHabilitarRastreamentoTnt_Tick(object sender, EventArgs e)
        {
            timerHabilitarRastreamentoTnt.Enabled = false;
            bwRastreamentoTnt.DoWork += new DoWorkEventHandler(bwRastreamentoTnt_DoWork);
            bwRastreamentoTnt.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bwRastreamentoTnt_RunWorkerCompleted);
            if (bwRastreamentoTnt.IsBusy != true)
            {
                bwRastreamentoTnt.RunWorkerAsync();
            }
        }
        #endregion


        #region Rastreios Jadlog
        private void timerRastreamentoJadlog_Tick(object sender, EventArgs e)
        {
            timerRastreamentoJadlog.Enabled = false;
            bwRastreamentoJadlog.DoWork += new DoWorkEventHandler(bwRastreamentoJadlog_DoWork);
            bwRastreamentoJadlog.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bwRastreamentoJadlog_RunWorkerCompleted);
            if (bwRastreamentoJadlog.IsBusy != true)
            {
                bwRastreamentoJadlog.RunWorkerAsync();
            }

        }
        private BackgroundWorker bwRastreamentoJadlog = new BackgroundWorker();

        private void bwRastreamentoJadlog_DoWork(object sender, DoWorkEventArgs e)
        {
            rnFrete.AtualizaRastreioJadlog();
        }

        private void bwRastreamentoJadlog_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            timerRastreamentoJadlog.Interval = 3600000;
            timerRastreamentoJadlog.Enabled = true;
        }
        #endregion

        #region Rastreios Jadlog

        private void timerRastreamentoCorreios_Tick(object sender, EventArgs e)
        {
            timerRastreamentoCorreios.Enabled = false;
            bwRastreamentoCorreios.DoWork += new DoWorkEventHandler(bwRastreamentoCorreios_DoWork);
            bwRastreamentoCorreios.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bwRastreamentoCorreios_RunWorkerCompleted);
            if (bwRastreamentoCorreios.IsBusy != true)
            {
                bwRastreamentoCorreios.RunWorkerAsync();
            }
        }
        private BackgroundWorker bwRastreamentoCorreios = new BackgroundWorker();

        private void bwRastreamentoCorreios_DoWork(object sender, DoWorkEventArgs e)
        {
            rnFrete.AtualizaRastreioCorreios();
        }

        private void bwRastreamentoCorreios_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            timerRastreamentoCorreios.Interval = 3600000;
            timerRastreamentoCorreios.Enabled = true;
        }
        #endregion

        private void timerClearsale_Tick(object sender, EventArgs e)
        {
            timerClearsale.Enabled = false;
            bwClearsale.DoWork += new DoWorkEventHandler(bwClearsale_DoWork);
            bwClearsale.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bwClearsale_RunWorkerCompleted);
            if (bwClearsale.IsBusy != true)
            {
                bwClearsale.RunWorkerAsync();
            }
        }
        private BackgroundWorker bwClearsale = new BackgroundWorker();

        private void bwClearsale_DoWork(object sender, DoWorkEventArgs e)
        {
            PagamentoV2.AtualizaClearsale();
        }

        private void bwClearsale_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            timerClearsale.Interval = 60000;
            timerClearsale.Enabled = true;
        }
        private void DesativarBanner(long idQueue)
        {
            #region atualizaQueue
            var dataQueue = new dbSiteEntities();
            var queue = (from c in dataQueue.tbQueue where c.idQueue == idQueue select c).First();
            queue.andamento = true;
            queue.dataInicio = DateTime.Now;
            dataQueue.SaveChanges();
            #endregion

            int idBanner = Convert.ToInt32(queue.idRelacionado);
            var data = new dbSiteEntities();
            var banner = (from c in data.tbBanners where c.Id == idBanner select c).FirstOrDefault();
            if (banner != null)
            {
                banner.ativo = false;
                data.SaveChanges();
            }

            #region finalizaQueue
            var queueFinalizar = (from c in dataQueue.tbQueue where c.idQueue == idQueue select c).First();
            queueFinalizar.concluido = true;
            queueFinalizar.dataConclusao = DateTime.Now;
            dataQueue.SaveChanges();
            #endregion

            var log = new rnLog { usuario = "Sistema" };
            log.descricoes.Add("Desativação do banner IdBanner: " + idBanner);
            log.tiposRelacionados.Add(new rnEnums.TipoRelacionadoIds() { idRegistroRelacionado = idQueue, tipoRelacionado = rnEnums.TipoRegistroRelacionado.Banner });
            log.tiposOperacao.Add(rnEnums.TipoOperacao.Banner);
            log.InsereLog();
        }
        private void AtivarBanner(long idQueue)
        {
            #region atualizaQueue
            var dataQueue = new dbSiteEntities();
            var queue = (from c in dataQueue.tbQueue where c.idQueue == idQueue select c).First();
            queue.andamento = true;
            queue.dataInicio = DateTime.Now;
            dataQueue.SaveChanges();
            #endregion

            int idBanner = Convert.ToInt32(queue.idRelacionado);
            var data = new dbSiteEntities();
            var banner = (from c in data.tbBanners where c.Id == idBanner select c).FirstOrDefault();
            if (banner != null)
            {
                banner.ativo = true;
                data.SaveChanges();
            }

            #region finalizaQueue
            var queueFinalizar = (from c in dataQueue.tbQueue where c.idQueue == idQueue select c).First();
            queueFinalizar.concluido = true;
            queueFinalizar.dataConclusao = DateTime.Now;
            dataQueue.SaveChanges();
            #endregion

            var log = new rnLog { usuario = "Sistema" };
            log.descricoes.Add("Ativação do banner IdBanner: " + idBanner);
            log.tiposRelacionados.Add(new rnEnums.TipoRelacionadoIds() { idRegistroRelacionado = idQueue, tipoRelacionado = rnEnums.TipoRegistroRelacionado.Banner });
            log.tiposOperacao.Add(rnEnums.TipoOperacao.Banner);
            log.InsereLog();
        }

        private void AlterarProdutoPromocao(long idQueue)
        {
            #region atualizaQueue
            var dataQueue = new dbSiteEntities();
            var queue = (from c in dataQueue.tbQueue where c.idQueue == idQueue select c).First();
            queue.andamento = true;
            queue.dataInicio = DateTime.Now;
            dataQueue.SaveChanges();
            #endregion

            var log = new rnLog();
            log.usuario = "Sistema";
            log.descricoes.Add("Atualizando Selo Produto Promoção");
            log.tiposRelacionados.Add(new rnEnums.TipoRelacionadoIds() { idRegistroRelacionado = Convert.ToInt32(queue.idRelacionado), tipoRelacionado = rnEnums.TipoRegistroRelacionado.Produto });
            log.tiposOperacao.Add(rnEnums.TipoOperacao.Produto);

            int produtoId = Convert.ToInt32(queue.idRelacionado);
            int ativar = 0;
            int.TryParse(queue.mensagem, out ativar);

            var data = new dbSiteEntities();
            var produto = (from c in data.tbProdutos where c.produtoId == produtoId select c).FirstOrDefault();
            if (produto != null)
            {
                log.descricoes.Add(produto.produtoNome);
                log.descricoes.Add("ProdutoPromocao anterior: " + produto.produtoPromocao.ToString());
                if (ativar == 1)
                {
                    produto.produtoPromocao = "True";
                    data.SaveChanges();
                }
                else
                {
                    produto.produtoPromocao = "False";
                    data.SaveChanges();
                }
                log.descricoes.Add("ProdutoPromocao novo: " + produto.produtoPromocao.ToString());
                log.InsereLog();
            }

            #region finalizaQueue
            var queueFinalizar = (from c in dataQueue.tbQueue where c.idQueue == idQueue select c).First();
            queueFinalizar.concluido = true;
            queueFinalizar.dataConclusao = DateTime.Now;
            dataQueue.SaveChanges();
            #endregion
        }

        

        private void AtivarDesativarProduto(long idQueue)
        {
            #region atualizaQueue
            var dataQueue = new dbSiteEntities();
            var queue = (from c in dataQueue.tbQueue where c.idQueue == idQueue select c).First();
            queue.andamento = true;
            queue.dataInicio = DateTime.Now;
            dataQueue.SaveChanges();
            #endregion

            var log = new rnLog();
            log.usuario = "Sistema";
            log.descricoes.Add("Alterando Produto Ativo");
            log.tiposRelacionados.Add(new rnEnums.TipoRelacionadoIds() { idRegistroRelacionado = Convert.ToInt32(queue.idRelacionado), tipoRelacionado = rnEnums.TipoRegistroRelacionado.Produto });
            log.tiposOperacao.Add(rnEnums.TipoOperacao.Produto);

            int produtoId = Convert.ToInt32(queue.idRelacionado);
            int ativar = 0;
            int.TryParse(queue.mensagem, out ativar);

            var data = new dbSiteEntities();
            var produto = (from c in data.tbProdutos where c.produtoId == produtoId select c).FirstOrDefault();
            if (produto != null)
            {
                log.descricoes.Add(produto.produtoNome);
                log.descricoes.Add("ProdutoAtivo anterior: " + produto.produtoAtivo.ToString());
                if (ativar == 1)
                {
                    produto.produtoAtivo = "True";
                    data.SaveChanges();
                }
                else
                {
                    produto.produtoAtivo = "False";
                    data.SaveChanges();
                }
                log.descricoes.Add("ProdutoAtivo novo: " + produto.produtoAtivo.ToString());
                log.InsereLog();
            }

            #region finalizaQueue
            var queueFinalizar = (from c in dataQueue.tbQueue where c.idQueue == idQueue select c).First();
            queueFinalizar.concluido = true;
            queueFinalizar.dataConclusao = DateTime.Now;
            dataQueue.SaveChanges();
            #endregion
        }



        private void CapturarPagamentoQueue(long idQueue)
        {
            #region atualizaQueue
            var dataQueue = new dbSiteEntities();
            var queue = (from c in dataQueue.tbQueue where c.idQueue == idQueue select c).First();
            queue.andamento = true;
            queue.dataInicio = DateTime.Now;
            dataQueue.SaveChanges();
            #endregion


            int idPagamento = Convert.ToInt32(queue.idRelacionado);

            var data = new dbSiteEntities();
            var detalhesPagamento = (from c in data.tbPedidoPagamento where c.idPedidoPagamento == idPagamento select c).First();
            var riscoRepo = new BarkCommerce.Core.Repositories.AnaliseRisco.ClearSaleRepository();
            var gatewayRepo = new BarkCommerce.Core.Repositories.Gateway.MaxipagoRepository();
            var pagamentoRepo = new BarkCommerce.Core.Repositories.Pagamento.PagamentoRepository(riscoRepo, gatewayRepo);
            var pagamentoBusiness = new BarkCommerce.Business.Pagamento.RetornaDetalhesPagamentoGateway(pagamentoRepo);

            pagamentoRepo.CapturaPagamento(detalhesPagamento.idPedidoPagamento);
            var queueUpdate = new tbQueue();
            queueUpdate.agendamento = DateTime.Now.AddMinutes(1);
            queueUpdate.andamento = false;
            queueUpdate.concluido = false;
            queueUpdate.idRelacionado = detalhesPagamento.pedidoId;
            queueUpdate.mensagem = "";
            queueUpdate.tipoQueue = 12;
            data.tbQueue.Add(queueUpdate);
            data.SaveChanges();



            #region finalizaQueue
            var queueFinalizar = (from c in dataQueue.tbQueue where c.idQueue == idQueue select c).First();
            queueFinalizar.concluido = true;
            queueFinalizar.dataConclusao = DateTime.Now;
            dataQueue.SaveChanges();
            #endregion
        }
        private void ConsultarClearsaleQueue(long idQueue)
        {
            #region atualizaQueue
            var dataQueue = new dbSiteEntities();
            var queue = (from c in dataQueue.tbQueue where c.idQueue == idQueue select c).First();
            queue.andamento = true;
            queue.dataInicio = DateTime.Now;
            dataQueue.SaveChanges();
            #endregion


            int pedidoId = Convert.ToInt32(queue.idRelacionado);

            PagamentoV2.AtualizaClearsale(pedidoId);



            #region finalizaQueue
            var queueFinalizar = (from c in dataQueue.tbQueue where c.idQueue == idQueue select c).First();
            queueFinalizar.concluido = true;
            queueFinalizar.dataConclusao = DateTime.Now;
            dataQueue.SaveChanges();
            #endregion
        }

        private void AlterarPrecoPromocional(long idQueue)
        {
            #region atualizaQueue
            var dataQueue = new dbSiteEntities();
            var queue = (from c in dataQueue.tbQueue where c.idQueue == idQueue select c).First();
            queue.andamento = true;
            queue.dataInicio = DateTime.Now;
            dataQueue.SaveChanges();
            #endregion

            var log = new rnLog();
            log.usuario = "Sistema";
            log.descricoes.Add("Atualizando Preço Promocional");
            log.tiposRelacionados.Add(new rnEnums.TipoRelacionadoIds() { idRegistroRelacionado = Convert.ToInt32(queue.idRelacionado), tipoRelacionado = rnEnums.TipoRegistroRelacionado.Produto });
            log.tiposOperacao.Add(rnEnums.TipoOperacao.Produto);

            int produtoId = Convert.ToInt32(queue.idRelacionado);
            decimal precoPromocional = 0;
            decimal.TryParse(queue.mensagem, out precoPromocional);

            var data = new dbSiteEntities();
            var produto = (from c in data.tbProdutos where c.produtoId == produtoId select c).FirstOrDefault();
            if (produto != null)
            {
                log.descricoes.Add(produto.produtoNome);

                log.descricoes.Add("Preço promocional anterior: " + produto.produtoPrecoPromocional.ToString());
                if (precoPromocional > 0)
                {
                    if (precoPromocional > produto.produtoPrecoDeCusto)
                    {
                        produto.produtoPrecoPromocional = precoPromocional;
                        data.SaveChanges();
                    }
                }
                else
                {
                    produto.produtoPrecoPromocional = precoPromocional;
                    data.SaveChanges();
                }

                var elastic = ElasticSearch.ElasticSearchProdutoRepository.Instance;
                try
                {
                    elastic.AtualizarProduto(produto.produtoId);
                }
                catch (Exception ex)
                {
                    rnEmails.EnviaEmail("", "andre@bark.com.br", "", "", "", ex.InnerException + "<br>" + ex.ToString(), "Erro Elastic");
                }
                log.descricoes.Add("Preço promocional novo: " + precoPromocional.ToString());
                log.InsereLog();
            }

            #region finalizaQueue
            var queueFinalizar = (from c in dataQueue.tbQueue where c.idQueue == idQueue select c).First();
            queueFinalizar.concluido = true;
            queueFinalizar.dataConclusao = DateTime.Now;
            dataQueue.SaveChanges();
            #endregion
        }

        private void bwGerarPedidoCarolinaMoveis_DoWork(object sender, DoWorkEventArgs e)
        {
            #region habilita fornecedor carolina moveis 
            var dataHabilitarFornecedor = new dbSiteEntities();
            var fornecedor = (from c in dataHabilitarFornecedor.tbProdutoFornecedor where c.fornecedorId == 72 select c).First();
            fornecedor.gerarPedido = true;
            dataHabilitarFornecedor.SaveChanges();
            #endregion habilita fornecedor carolina moveis 

            var data = new dbSiteEntities();

            var FornecedoreCarolinaMoveis = (from c in data.tbProdutoFornecedor where c.fornecedorId == 72 select c).ToList();
            alteraLabelStatusPedido("Obtendo Lista Estoque");

            alteraLabelStatusPedido("Carregando Itens aguardando Estoque");
            var itensAguardando = (from c in data.tbItemPedidoEstoque
                                   where c.reservado == false && c.cancelado == false &&
                                   c.dataLimite != null
                                   select new
                                   {
                                       c.produtoId,
                                       c.tbProdutos.produtoFornecedor,
                                       c.tbProdutos.produtoPrecoDeCusto,
                                       c.dataLimite,
                                       c.itemPedidoId,
                                       c.tbItensPedido.tbProdutos.compraComboCompleto,
                                       c.idItemPedidoEstoque,
                                       c.tbItensPedido.pedidoId,
                                       c.tbItensPedido.tbPedidos.dataHoraDoPedido
                                   }).ToList().OrderBy(x => x.dataLimite);

            int totalItensPedido = itensAguardando.Count();
            int itemPedidoAtual = 0;

            var fornecedoresEstoque = (from c in FornecedoreCarolinaMoveis where c.gerarPedido == false select c).ToList();
            var feriado = (from c in data.tbFeriados where c.data.Year == DateTime.Now.Year && c.data.Month == DateTime.Now.Month && c.data.Day == DateTime.Now.Day select c).Any();
            if (!feriado)
            {
                int diaDaSemana = (int)DateTime.Now.DayOfWeek;
                var fornecedores = (from c in FornecedoreCarolinaMoveis where c.gerarPedido select c).ToList();
                if (diaDaSemana == 0) fornecedores = fornecedores.Where(x => x.pedidoDomingo).ToList();
                if (diaDaSemana == 1) fornecedores = fornecedores.Where(x => x.pedidoSegunda).ToList();
                if (diaDaSemana == 2) fornecedores = fornecedores.Where(x => x.pedidoTerca).ToList();
                if (diaDaSemana == 3) fornecedores = fornecedores.Where(x => x.pedidoQuarta).ToList();
                if (diaDaSemana == 4) fornecedores = fornecedores.Where(x => x.pedidoQuinta).ToList();
                if (diaDaSemana == 5) fornecedores = fornecedores.Where(x => x.pedidoSexta).ToList();
                if (diaDaSemana == 6) fornecedores = fornecedores.Where(x => x.pedidoSabado).ToList();

                fornecedores = fornecedores.Where(x => x.fornecedorId == 72).ToList();

                #region Gera Pedidos 

                var pedidosFornecedorPendentes = new List<tbPedidoFornecedor>();
                foreach (var item in itensAguardando)
                {
                    itemPedidoAtual++;
                    alteraLabelStatusPedido("Gerando item: " + itemPedidoAtual + "/" + totalItensPedido);
                    var gerarPedidoFornecedor = fornecedores.Any(x => x.fornecedorId == item.produtoFornecedor);

                    var fornecedorEstoque = fornecedoresEstoque.Any(x => x.fornecedorId == item.produtoFornecedor);
                    var pedidoFornecedorPendente = pedidosFornecedorPendentes.FirstOrDefault(x => x.idFornecedor == item.produtoFornecedor);


                    if (gerarPedidoFornecedor && !fornecedorEstoque)
                    {
                        var totalItensNaoEntregues = (from c in data.tbPedidoFornecedorItem
                                                      where c.idProduto == item.produtoId && c.entregue == false
                                                      select c).Count();

                        var totalItensAguardandoLiberacao = (from c in data.tbProdutoEstoque
                                                             where c.produtoId == item.produtoId && (c.liberado ?? false) == false
                                                                   && c.enviado == false && c.pedidoId == null && c.pedidoIdReserva == null
                                                             select c).Count();

                        var totalItensAguardandoReserva = (from c in data.tbItemPedidoEstoque
                                                           where c.produtoId == item.produtoId
                                                                 && c.reservado == false && c.cancelado == false && c.dataLimite != null
                                                           select c).Count();

                        int totalNecessario = totalItensAguardandoReserva - totalItensNaoEntregues -
                                              totalItensAguardandoLiberacao;

                        if (totalNecessario > 0)
                        {
                            #region gera pedido pendente ao fornecedor 
                            if (pedidoFornecedorPendente == null)
                            {
                                pedidoFornecedorPendente = retornaPedidoFornecedorPendente(item.produtoFornecedor,
                                    FornecedoreCarolinaMoveis);
                                pedidosFornecedorPendentes.Add(pedidoFornecedorPendente);
                            }
                            #endregion

                            //for (int i = 1; i <= totalNecessario; i++) 
                            //{ 
                            bool gerarProduto = true;
                            if (item.produtoFornecedor == 82)
                            {
                                var totalItensNoRomaneio =
                                    (from c in data.tbPedidoFornecedorItem
                                     where c.idPedidoFornecedor == pedidoFornecedorPendente.idPedidoFornecedor
                                     select c).Count();
                                if (totalItensNoRomaneio >= 50000) gerarProduto = false;
                            }
                            if (gerarProduto)
                            {
                                alteraLabelStatusPedido("Adicionando a pedido fornecedor: " + itemPedidoAtual + "/" + totalItensPedido);
                                var pedidoFornecedorItem = new tbPedidoFornecedorItem();
                                pedidoFornecedorItem.custo = (decimal)item.produtoPrecoDeCusto;
                                pedidoFornecedorItem.brinde = false;
                                pedidoFornecedorItem.diferenca = 0;
                                pedidoFornecedorItem.idPedidoFornecedor = pedidoFornecedorPendente.idPedidoFornecedor;
                                pedidoFornecedorItem.idProduto = item.produtoId;
                                pedidoFornecedorItem.entregue = false;
                                //pedidoFornecedorItem.idPedido = item.pedidoId; 
                                //pedidoFornecedorItem.idItemPedido = item.itemPedidoId; 
                                pedidoFornecedorItem.quantidade = 1;
                                pedidoFornecedorItem.idProcessoFabrica = 7;

                                pedidoFornecedorItem.pedidosPendentes = totalItensAguardandoReserva;
                                pedidoFornecedorItem.produtosNaoEnderecados = totalItensAguardandoLiberacao;
                                pedidoFornecedorItem.encomendasNaoEntregues = totalItensNaoEntregues;


                                var dataAdd = new dbSiteEntities();
                                dataAdd.tbPedidoFornecedorItem.Add(pedidoFornecedorItem);
                                dataAdd.SaveChanges();

                                if (item.compraComboCompleto)
                                {
                                    var itensDoCombo = (from c in data.tbItemPedidoEstoque
                                                        where
                                                            c.itemPedidoId == item.itemPedidoId &&
                                                            c.tbProdutos.produtoFornecedor == item.produtoFornecedor && c.idItemPedidoEstoque != item.idItemPedidoEstoque
                                                        select new
                                                        {
                                                            c.produtoId,
                                                            c.tbProdutos.produtoPrecoDeCusto
                                                        }).ToList();
                                    foreach (var itemDoCombo in itensDoCombo)
                                    {
                                        var pedidoFornecedorItemCombo = new tbPedidoFornecedorItem();
                                        pedidoFornecedorItemCombo.custo = (decimal)itemDoCombo.produtoPrecoDeCusto;
                                        pedidoFornecedorItemCombo.brinde = false;
                                        pedidoFornecedorItemCombo.diferenca = 0;
                                        pedidoFornecedorItemCombo.idPedidoFornecedor = pedidoFornecedorPendente.idPedidoFornecedor;
                                        pedidoFornecedorItemCombo.idProduto = itemDoCombo.produtoId;
                                        pedidoFornecedorItemCombo.entregue = false;
                                        pedidoFornecedorItemCombo.quantidade = 1;
                                        pedidoFornecedorItemCombo.idProcessoFabrica = 7;
                                        pedidoFornecedorItemCombo.motivoAdicao = "Adicionado combo completo do produto";
                                        dataAdd.tbPedidoFornecedorItem.Add(pedidoFornecedorItemCombo);
                                        dataAdd.SaveChanges();
                                    }
                                }
                            }
                            //} 
                        }
                    }
                }

                #endregion

                int totalFornecedoresPendentes = pedidosFornecedorPendentes.Count();
                int fornecedorPendenteAtual = 0;
                foreach (var pedidoFornecedorPendentes in pedidosFornecedorPendentes)
                {
                    fornecedorPendenteAtual++;
                    alteraLabelStatusPedido("Fechando pedido ao fornecedor: " + fornecedorPendenteAtual + "/" + totalFornecedoresPendentes);
                    fechaPedidoFornecedor(pedidoFornecedorPendentes.idFornecedor);
                }

                var pedidosGeral = (from c in itensAguardando select new { c.pedidoId, c.dataHoraDoPedido }).Distinct().ToList();

                int pedidosPrazoPendentes = pedidosGeral.Count();
                int pedidosPrazoAtual = 0;
                foreach (var pedido in pedidosGeral)
                {
                    pedidosPrazoAtual++;
                    var agora = DateTime.Now;
                    alteraLabelStatusPedido("Verificando se necessita atualziar prazo: " + pedidosPrazoAtual + "/" + pedidosPrazoPendentes);
                    try
                    {
                        var itens = (from c in data.tbItensPedido
                                     where c.pedidoId == pedido.pedidoId
                                     select c
                            ).ToList();
                        var pedidoAlterado = (from c in itens
                                              where c.pedidoId == pedido.pedidoId &&
                                     ((c.cancelado ?? false) == true | c.dataDaCriacao > (c.tbPedidos.dataHoraDoPedido ?? agora).AddMinutes(2))
                                              select c
                            ).Any();
                        if (pedidoAlterado)
                        {
                            AdicionaQueueAtualizaPrazo(pedido.pedidoId);
                        }
                    }
                    catch (Exception ex)
                    {
                        //MessageBox.Show(ex.ToString()); 
                    }
                }
                alteraPedidosSeparacaoEstoque();
            }

            #region desabilita fornecedor carolina moveis 
            var dataDesabilitarFornecedor = new dbSiteEntities();
            var fornecedorDesabilitar = (from c in dataDesabilitarFornecedor.tbProdutoFornecedor where c.fornecedorId == 72 select c).First();
            fornecedorDesabilitar.gerarPedido = false;
            dataDesabilitarFornecedor.SaveChanges();
            #endregion desabilita fornecedor carolina moveis 
        }

        private void btnGerarPedidoCarolinaMoveis_Click(object sender, EventArgs e)
        {
            if (!bwGerarPedidoCarolinaMoveis.IsBusy)
            {
                btnGerarPedidoCarolinaMoveis.Enabled = false;
                bwGerarPedidoCarolinaMoveis.RunWorkerAsync();
            }
        }

        private void bwGerarPedidoCarolinaMoveis_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            btnGerarPedidoCarolinaMoveis.Enabled = true;
        }

        private BackgroundWorker bwUpdateClearPagamentos = new BackgroundWorker();
        private void timerUpdateClearPagamentos_Tick(object sender, EventArgs e)
        {
            timerUpdateClearPagamentos.Enabled = false;
            bwUpdateClearPagamentos.WorkerSupportsCancellation = true;
            bwUpdateClearPagamentos.DoWork += new DoWorkEventHandler(bwUpdateClearPagamentos_DoWork);
            bwUpdateClearPagamentos.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bwUpdateClearPagamentos_RunWorkerCompleted);

            if (bwUpdateClearPagamentos.IsBusy != true)
            {
                bwUpdateClearPagamentos.RunWorkerAsync();
            }
        }

        private void bwUpdateClearPagamentos_DoWork(object sender, DoWorkEventArgs e)
        {
            var data = new dbSiteEntities();
            var pendentesClear = (from c in data.tbPedidoPagamentoRisco
                                  where c.statusInterno == 1 && (c.tbPedidoPagamento.tbPedidos.statusDoPedido == 2 | c.tbPedidoPagamento.tbPedidos.statusDoPedido == 7)
                                  select new
                                  {
                                      c.tbPedidoPagamento.pedidoId,
                                      c.tbPedidoPagamento.tbPedidos.statusDoPedido
                                  }).ToList();
            var pendentesMundi = (from c in data.tbPedidoPagamentoGateway
                                  where c.responseMessage == "AuthorizedPendingCapture" && (c.tbPedidoPagamento.tbPedidos.statusDoPedido == 2 | c.tbPedidoPagamento.tbPedidos.statusDoPedido == 7)
                                  select new
                                  {
                                      c.tbPedidoPagamento.pedidoId,
                                      c.tbPedidoPagamento.tbPedidos.statusDoPedido
                                  }).ToList();
            var listaPendentes = pendentesClear.Union(pendentesMundi).ToList();
            string atualizados = "";

            var captureds = (from c in data.tbPedidoPagamentoGateway
                             where c.responseMessage.ToLower().Contains("captured") && c.tbPedidoPagamento.tbPedidos.statusDoPedido == 2
                             select c.tbPedidoPagamento.pedidoId).ToList();
            foreach (var captured in captureds)
            {
                int retorno = PagamentoV2.verificaPagamentoCompleto(captured);
                if (retorno != 99)
                {
                    rnEmails.EnviaEmail("", "andre@bark.com.br", "", "", "", captured.ToString(), "Erro capturado não pago");
                }
            }

            foreach (var pendente in listaPendentes)
            {
                try
                {
                    PagamentoV2.AtualizaClearsale(pendente.pedidoId);
                    atualizados += pendente.ToString() + "<br>";
                }
                catch (Exception ex)
                {
                    if (pendente.statusDoPedido == 2)
                    {
                        rnEmails.EnviaEmail("", "andre@bark.com.br", "", "", "", ex.ToString(), "Erro Clearsale Pagamentos " + pendente);
                    }
                }
            }

        }

        private void bwUpdateClearPagamentos_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            timerUpdateClearPagamentos.Interval = 18000000;
            timerUpdateClearPagamentos.Enabled = true;
        }



        private BackgroundWorker bwProceda = new BackgroundWorker();
        private void bwProceda_DoWork(object sender, DoWorkEventArgs e)
        {
            rnProceda.LerArquivo_DocCob_tnt("tnt");
            rnProceda.LerArquivo_DocCob_plimor("tnt");
            rnProceda.LerArquivo_OCOREN_tnt("tnt");
            rnProceda.LerArquivo_OCOREN_plimor("tnt");
            //rnProceda.LerArquivo_OCOREN_nowlog("tnt");


            rnProceda.LerArquivo_OCOREN("nowlog", "ocorren_");
            rnProceda.LerArquivo_OCOREN("transfolha", "ocoren_");


            timerProceda.Interval = 30000;
            timerProceda.Enabled = true;
        }

        private void timerProceda_Tick(object sender, EventArgs e)
        {
            timerProceda.Enabled = false;
            bwProceda.WorkerSupportsCancellation = true;
            bwProceda.DoWork += new DoWorkEventHandler(bwProceda_DoWork);

            if (bwProceda.IsBusy != true)
            {
                bwProceda.RunWorkerAsync();
            }
        }

        private BackgroundWorker bwLotePagamento = new BackgroundWorker();
        private void bwLotePagamento_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                rnBoleto.ProcessarRetornosSantander(@"C:\Santander\AFTData\INBOX\GE201965\", @"c:\santander_processados\GE201965\");
                rnBoleto.ProcessarRetornosSantander(@"C:\Santander\AFTData\INBOX\GE203025\", @"c:\santander_processados\GE203025\");
                rnBoleto.ProcessarRetornosSantander(@"C:\Santander\AFTData\INBOX\GE203026\", @"c:\santander_processados\GE203026\");
            }
            catch (Exception ex)
            {
                rnEmails.EnviaEmail("", "andre@bark.com.br", "", "", "", ex.ToString(), "Erro ao Processar Retornos");
                timerLoteBancario.Interval = 3600000;
                timerLoteBancario.Enabled = true;
            }
        }
        private void bwLotePagamento_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            timerLoteBancario.Interval = 1800000;
            timerLoteBancario.Enabled = true;
        }
        private void timerLoteBancario_Tick(object sender, EventArgs e)
        {
            timerLoteBancario.Enabled = false;
            bwLotePagamento.WorkerSupportsCancellation = true;
            bwLotePagamento.DoWork += new DoWorkEventHandler(bwLotePagamento_DoWork);
            bwLotePagamento.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bwLotePagamento_RunWorkerCompleted);

            if (bwLotePagamento.IsBusy != true)
            {
                bwLotePagamento.RunWorkerAsync();
            }
        }

        private void atualizarPesos()
        {
            var dataCancelar = DateTime.Now.AddDays(1);
            var dataGeral = new dbSiteEntities();
            var listaEnvios = (from c in dataGeral.tbProdutoEstoque
                               where c.dataEnvio > dataCancelar && c.dataEnvio < dataCancelar
                               group c by new { c.idPedidoEnvio } into grupo
                               select new
                               {
                                   grupo.Key.idPedidoEnvio,
                                   grupo.FirstOrDefault().produtoId,
                                   contagem = grupo.Count()
                               }).Where(x => x.contagem == 1).ToList();
            var estoqueGeral = (from c in dataGeral.tbProdutoEstoque
                                where c.dataEntrada > dataCancelar && c.dataEnvio < dataCancelar
                                select c).ToList();
            var listaIdsEnvios = listaEnvios.Select(x => x.idPedidoEnvio).ToList();
            var pacotesGeral = (from c in dataGeral.tbPedidoPacote
                                where listaIdsEnvios.Contains(c.idPedidoEnvio)
                                select c).ToList();
            var listaProdutos = (from c in listaEnvios select c.produtoId).Distinct().ToList();

            for (int i = 0; i < 200; i++)
            {
                var diasAntes = (((i + 1) * 1) * -1);
                var diasDepois = ((i * 1) * -1);
                var dataAntes = DateTime.Now.AddDays(diasAntes);
                var dataDepois = DateTime.Now.AddDays(diasDepois);
                lblAndamentoRelatorio.Invoke((MethodInvoker)delegate
                {
                    lblAndamentoRelatorio.Text = "Atualizando " + diasAntes + " até " + diasDepois;
                });

                using (new TransactionScope(TransactionScopeOption.Required, new TransactionOptions
                {
                    IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                }))
                {
                    using (var data = new dbSiteEntities())
                    {

                        listaEnvios = (from c in data.tbProdutoEstoque
                                       where c.dataEnvio > dataAntes && c.dataEnvio < dataDepois
                                       group c by new { c.idPedidoEnvio } into grupo
                                       select new
                                       {
                                           grupo.Key.idPedidoEnvio,
                                           grupo.FirstOrDefault().produtoId,
                                           contagem = grupo.Count()
                                       }).Where(x => x.contagem == 1).ToList();
                        estoqueGeral = (from c in data.tbProdutoEstoque
                                        where c.dataEntrada > dataAntes && c.dataEnvio < dataDepois
                                        select c).ToList();
                        listaIdsEnvios = listaEnvios.Select(x => x.idPedidoEnvio).ToList();
                        pacotesGeral = (from c in data.tbPedidoPacote
                                        where listaIdsEnvios.Contains(c.idPedidoEnvio)
                                        select c).ToList();
                        listaProdutos = (from c in listaEnvios select c.produtoId).Distinct().ToList();
                    }
                }
                int total = listaProdutos.Count();
                int atual = 0;
                foreach (var produto in listaProdutos)
                {
                    atual++;
                    lblAndamentoRelatorio.Invoke((MethodInvoker)delegate
                    {
                        lblAndamentoRelatorio.Text = "Atualizando pesos:" + atual + "/" + total;
                    });
                    Thread.Sleep(100);
                    try
                    {
                        var enviosUmVolume = (from c in estoqueGeral
                                              join d in estoqueGeral on c.idPedidoEnvio equals d.idPedidoEnvio into produtos
                                              join f in pacotesGeral on c.idPedidoEnvio equals f.idPedidoEnvio into pacotes
                                              where c.dataEntrada > dataAntes && c.dataEnvio < dataDepois && c.produtoId == produto && pacotes.Count() == 1 && produtos.Count() == 1
                                              select new
                                              {
                                                  produtos.FirstOrDefault().produtoId,
                                                  pacotes.FirstOrDefault().altura,
                                                  pacotes.FirstOrDefault().largura,
                                                  pacotes.FirstOrDefault().profundidade,
                                                  pacotes.FirstOrDefault().peso,
                                                  produtosEnvio = produtos.Count(),
                                                  pacotes = pacotes.Count()
                                              }).ToList().Where(x => x.produtosEnvio == 1 && x.pacotes == 1).ToList();
                        if (enviosUmVolume.Any())
                        {
                            var dataAlterar = new dbSiteEntities();
                            var produtoAlterar = (from c in dataAlterar.tbProdutos where c.produtoId == produto select c).First();
                            if (enviosUmVolume.Any(x => x.peso > 0))
                            {
                                var mediaPeso = enviosUmVolume.Where(x => x.peso > 0).Sum(x => x.peso) / enviosUmVolume.Count(x => x.peso > 0);
                                produtoAlterar.produtoPeso = mediaPeso;
                                produtoAlterar.dataConferenciaPeso = DateTime.Now;
                            }
                            if (enviosUmVolume.Any(x => x.largura > 0))
                            {
                                var mediaLargura = enviosUmVolume.Where(x => x.largura > 0).Sum(x => x.largura) / enviosUmVolume.Count(x => x.largura > 0);
                                produtoAlterar.largura = mediaLargura;
                            }
                            if (enviosUmVolume.Any(x => x.altura > 0))
                            {
                                var mediaAltura = enviosUmVolume.Where(x => x.altura > 0).Sum(x => x.altura) / enviosUmVolume.Count(x => x.altura > 0);
                                produtoAlterar.altura = mediaAltura;
                            }
                            if (enviosUmVolume.Any(x => x.profundidade > 0))
                            {
                                var mediaProfundidade = enviosUmVolume.Where(x => x.profundidade > 0).Sum(x => x.profundidade) / enviosUmVolume.Count(x => x.profundidade > 0);
                                produtoAlterar.profundidade = mediaProfundidade;
                            }
                            dataAlterar.SaveChanges();
                        }
                    }
                    catch (Exception ex) { }
                }

            }

            MessageBox.Show("Concluido");
        }
        private void button2_Click(object sender, EventArgs e)
        {
            button2.Enabled = false;
            bwAtualizarPeso.WorkerSupportsCancellation = true;
            bwAtualizarPeso.DoWork += new DoWorkEventHandler(bwAtualizarPeso_DoWork);

            if (bwAtualizarPeso.IsBusy != true)
            {
                bwAtualizarPeso.RunWorkerAsync();
            }
        }

        private BackgroundWorker bwAtualizarPeso = new BackgroundWorker();
        private void bwAtualizarPeso_DoWork(object sender, DoWorkEventArgs e)
        {
            atualizarPesos();
            button2.Enabled = true;
        }


        private void timerCache_Tick(object sender, EventArgs e)
        {
            timerCache.Enabled = false;
            timerCache.Interval = 300000;
        }





        private BackgroundWorker bwChecarCompletosQueue = new BackgroundWorker();
        public void ConsultarPedidosCompletosQueue()
        {
            if (bwChecarCompletosQueue.IsBusy != true)
            {
                bwChecarCompletosQueue.DoWork += new DoWorkEventHandler(bwChecarCompletosQueue_DoWork);
                bwChecarCompletosQueue.RunWorkerAsync();
            }
        }



        private void bwChecarCompletosQueue_DoWork(object sender, DoWorkEventArgs e)
        {
            if (rnConfiguracoes.HabilitarQueue8) //Checar se pedido está completo para embalar
            {
                var data = new dbSiteEntities();
                var queuescompleto = (from c in data.tbQueue
                                      where c.andamento == false && c.concluido == false && c.tipoQueue == 8
                                      orderby c.idQueue, c.agendamento
                                      select new
                                      {
                                          c.tipoQueue,
                                          c.idQueue
                                      }).Take(150).ToList();

                foreach (var queue in queuescompleto)
                {
                    checaPedidoCompletoParaEmbalar(queue.idQueue);
                }
            }
        }



        private BackgroundWorker bwReservarEstoqueQueue = new BackgroundWorker();
        public void ReservarEstoqueProdutoQueue()
        {
            if (bwReservarEstoqueQueue.IsBusy != true)
            {
                bwReservarEstoqueQueue.DoWork += new DoWorkEventHandler(bwReservarEstoqueQueue_DoWork);
                bwReservarEstoqueQueue.RunWorkerAsync();
            }
        }

        private void bwReservarEstoqueQueue_DoWork(object sender, DoWorkEventArgs e)
        {
            if (rnConfiguracoes.HabilitarQueue21) //Checar se pedido está completo para embalar
            {
                var data = new dbSiteEntities();
                var agora = DateTime.Now;
                var queuescompleto = (from c in data.tbQueue
                                      where c.andamento == false && c.concluido == false && c.tipoQueue == 21 && c.agendamento < agora
                                      orderby c.idQueue, c.agendamento
                                      group c by new {c.idRelacionado} into produtosQueue
                                      select new
                                      {
                                          produtosQueue.FirstOrDefault().idQueue,
                                          produtosQueue.FirstOrDefault().idRelacionado
                                      }).Take(150).ToList();

                foreach (var queue in queuescompleto)
                {
                    ReservarEstoqueProduto(queue.idQueue);
                }
            }
        }





        private BackgroundWorker bwAtualizarEstoque = new BackgroundWorker();
        public void AtualizarTodoEstoqueRealQueue(long idQueue)
        {
            if (bwAtualizarEstoque.IsBusy != true)
            {
                bwAtualizarEstoque.DoWork += new DoWorkEventHandler(bwAtualizarEstoque_DoWork);
                bwAtualizarEstoque.RunWorkerAsync(idQueue);
            }
        }

        private void bwAtualizarEstoque_DoWork(object sender, DoWorkEventArgs e)
        {
            long idQueue = Convert.ToInt64(e.Argument);
            rnEstoque.bwAtualizarTodoEstoqueRunQueue(idQueue);
        }






        private BackgroundWorker bwConsultarAtualizarEstoqueQueu = new BackgroundWorker();
        public void ConsultarAtualizarEstoqueQueue()
        {
            if (bwConsultarAtualizarEstoqueQueu.IsBusy != true)
            {
                bwConsultarAtualizarEstoqueQueu.DoWork += new DoWorkEventHandler(bwConsultarAtualizarEstoque_DoWork);
                bwConsultarAtualizarEstoqueQueu.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bwConsultarAtualizarEstoqueQueu_WorkComplete);
                bwConsultarAtualizarEstoqueQueu.RunWorkerAsync();
            }

        }
        private void bwConsultarAtualizarEstoque_DoWork(object sender, DoWorkEventArgs e)
        {
            lblUltimaAtualizacaoEstoque.Invoke((MethodInvoker)delegate
            {
                lblUltimaAtualizacaoEstoque.Text = "Iniciando checagem de atualização de estoque " + DateTime.Now;
            });

            var data = new dbSiteEntities();
            var queues = (from c in data.tbQueue
                          where c.andamento == false && c.concluido == false &&
                          c.tipoQueue == 38
                          orderby c.idQueue, c.agendamento
                          select new
                          {
                              c.tipoQueue,
                              c.idQueue
                          }).Take(20).ToList();
            foreach (var queue in queues)
            {
                rnEstoque.AtualizarEstoqueRealProdutoQueue(queue.idQueue);
            }

            lblUltimaAtualizacaoEstoque.Invoke((MethodInvoker)delegate
            {
                lblUltimaAtualizacaoEstoque.Text = "Ultima checagem de atualização de estoque " + DateTime.Now;
            });
        }
        private void bwConsultarAtualizarEstoqueQueu_WorkComplete(object sender, RunWorkerCompletedEventArgs e)
        {
            timerEstoqueAvulso.Enabled = true;
            timerEstoqueAvulso.Interval = 30000;
        }
        private void timerEstoqueAvulso_Tick(object sender, EventArgs e)
        {
            timerEstoqueAvulso.Enabled = false;
            ConsultarAtualizarEstoqueQueue();
        }


        private BackgroundWorker bwAtualizarProduto1 = new BackgroundWorker();

        private void bwAtualizarProduto1_DoWork(object sender, DoWorkEventArgs e)
        {
            int idRelacionado = Convert.ToInt32(e.Argument);
            var elastic = ElasticSearch.ElasticSearchProdutoRepository.Instance;
            try
            {
                elastic.AtualizarProduto((int)idRelacionado, listaProdutos, listaFornecedores, juncaoCategorias, categorias, juncaoColecoes, colecoes, fotos, videos, listaInformacoes);
            }
            catch (Exception ex)
            {
                var dataNovaQueue = new dbSiteEntities();
                var novaQueue = new tbQueue() { idRelacionado = (int)idRelacionado, agendamento = DateTime.Now, andamento = false, concluido = false, mensagem = "", tipoQueue = 15 };
                dataNovaQueue.tbQueue.Add(novaQueue);
                dataNovaQueue.SaveChanges();
                rnEmails.EnviaEmail("", "andre@bark.com.br", "", "", "", ex.InnerException + "<br>" + ex.ToString(), "Erro Elastic " + idRelacionado);
            }
        }

        private void bwEmailPlimor_DoWork(object sender, DoWorkEventArgs e)
        {
            var idQueue = Convert.ToInt64(e.Argument);
            try
            {

                lblStatusGerarPedido.Invoke((MethodInvoker)delegate
                {
                    lblProcessoEmAndamento1.Text = "Enviando emails Plimor";
                });

                EnviaEmailNotaPlimor(idQueue);

            }
            catch (Exception ex)
            {

            }

            lblStatusGerarPedido.Invoke((MethodInvoker)delegate
            {
                lblProcessoEmAndamento1.Text = "";
            });
        }

        private void bwEmailPlimor_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {

        }

        private void enviarSegundaViaBoleto(long idQueue)
        {
            #region atualizaQueue
            var dataQueue = new dbSiteEntities();
            var queue = (from c in dataQueue.tbQueue where c.idQueue == idQueue select c).First();
            queue.andamento = true;
            queue.dataInicio = DateTime.Now;
            dataQueue.SaveChanges();
            #endregion

            var data = new dbSiteEntities();
            var pedido = (from c in data.tbPedidos where c.pedidoId == queue.idRelacionado select c).FirstOrDefault();
            var pagamento = (from c in data.tbPedidoPagamento where c.pedidoId == queue.idRelacionado && c.cancelado == false && c.pago == false && c.idOperadorPagamento == 1 orderby c.idPedidoPagamento descending select c).FirstOrDefault();

            if (pedido != null && pagamento != null)
            {
                var cliente = (from c in data.tbClientes where c.clienteId == pedido.clienteId select c).FirstOrDefault();                
                //string linkDoBoleto = rnConfiguracoes.caminhoVirtual + "boleto.aspx?cobranca=" + pagamento.numeroCobranca + "&pedido=" + rnFuncoes.retornaIdCliente(pagamento.pedidoId);
                string linkDoBoleto = "https://apiv2.graodegente.com.br/v1/pagamentos/" + rnFuncoes.retornaIdCliente(pagamento.pedidoId) + "/boleto/" + pagamento.numeroCobranca + "?autoImpressao=undefined";
                rnEmails.enviaSegundaViaDoBoleto(cliente.clienteNome, pedido.pedidoId.ToString(), linkDoBoleto, cliente.clienteEmail);
            }
            #region finalizaQueue
            var queueFinalizar = (from c in dataQueue.tbQueue where c.idQueue == idQueue select c).First();
            queueFinalizar.concluido = true;
            queueFinalizar.dataConclusao = DateTime.Now;
            dataQueue.SaveChanges();
            #endregion
        }



        private void EmailsAndamentoPedidos()
        {
            return;
            var data = new dbSiteEntities();
            var agora = DateTime.Now;
            var dataInicio = Convert.ToDateTime("25/02/2018");
            var pedidos = (from c in data.tbPedidos
                           join d in data.tbPedidoEnvio on c.pedidoId equals d.idPedido into envios
                           where c.statusDoPedido == 11 &&  envios.Count() == 0 && (c.itensPendentesEnvioCd4 > 0 | c.itensPendentesEnvioCd5 > 0) && c.dataHoraDoPedido > dataInicio
                           select new
            {
                c.pedidoId,
                c.dataConfirmacaoPagamento,
                prazoMaximoPostagemAtualizado = (c.prazoMaximoPostagemAtualizado ?? DateTime.Now),
                c.prazoDeEntrega
            }).ToList().Where(x => (x.dataConfirmacaoPagamento ?? DateTime.Now).AddDays(3) < agora).ToList();
            //var pedidos20 = pedidos.Take(Convert.ToInt32(pedidos.Count / 5)).ToList();
            var pedidos20 = pedidos.ToList();
            foreach (var pedido in pedidos20)
            {

                int prazoFinal = rnFuncoes.retornaPrazoDiasUteis(Convert.ToInt32(pedido.prazoDeEntrega), 0, pedido.prazoMaximoPostagemAtualizado);
                var dataFinalPedido = pedido.prazoMaximoPostagemAtualizado.AddDays(prazoFinal);
                var atrasado = dataFinalPedido < agora;
                if (atrasado) {
                    var validaEmailEnviadoAtrasado = (from c in data.tbEmailEnvio where c.pedidoId == pedido.pedidoId && c.idEmailTipo == 2 select c).Any();
                    if (!validaEmailEnviadoAtrasado)
                    {
                        var validaEmailEnviadoStatus = (from c in data.tbEmailEnvio where c.pedidoId == pedido.pedidoId && c.idEmailTipo == 1 select c).ToList().Any(x => x.dataEnvio.AddDays(2) > agora);
                        if (!validaEmailEnviadoStatus)
                        {
                            //enviar email de atrasado
                            var envio = "";
                        }
                    }
                 }
                else
                {
                    var validaEmailEnviadoStatus = (from c in data.tbEmailEnvio where c.pedidoId == pedido.pedidoId && c.idEmailTipo == 1 select c).ToList().Any(x => x.dataEnvio.AddDays(3) > agora);
                    if (!validaEmailEnviadoStatus)
                    {
                        //enviar email de status
                        var envio = "";
                        rnEmails.EnviaEmailAndamentoPedido(pedido.pedidoId);
                    }

                }
            }
        }

        private void timerEmailAndamento_Tick(object sender, EventArgs e)
        {

            timerEmailAndamento.Enabled = false;
            if (bwEmailAndamento.IsBusy != true)
            {
                bwEmailAndamento.RunWorkerAsync();
            }
        }

        private void bwEmailAndamento_DoWork(object sender, DoWorkEventArgs e)
        {
            //EmailsAndamentoPedidos();
        }

        private void bwEmailAndamento_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            timerEmailAndamento.Interval = 3600000;
            timerEmailAndamento.Enabled = true;

        }

        public static void EnviaNotaFtp(long idQueue)
        {

            #region atualizaQueue 

            var data = new dbSiteEntities();
            var queue = (from c in data.tbQueue where c.idQueue == idQueue select c).First();
            queue.andamento = true;
            queue.dataInicio = DateTime.Now;
            data.SaveChanges();

            #endregion


            int numeroNota = (int)queue.idRelacionado;
            string xmlnota = queue.mensagem;

            var envio = (from c in data.tbNotaFiscal
                         join d in data.tbPedidoEnvio on c.idPedidoEnvio equals d.idPedidoEnvio
                         where c.numeroNota == numeroNota && c.idCNPJ == 6
                         select d).FirstOrDefault();
            string transportadora = "dialogo";
            if (envio != null)
            {
                transportadora = envio.formaDeEnvio.ToLower();
            }
            try
            {
                if (transportadora == "dialogo")
                {
                    #region Envia para Ftp da RichRelevance 
                    FileInfo product_full = new FileInfo(rnConfiguracoes.caminhoFisico + "notatransportadoras/" + transportadora + "_" + numeroNota + ".xml");
                    if (product_full.Exists)
                    {
                        product_full.Delete();
                    }
                    StreamWriter swProductFull = product_full.CreateText();
                    swProductFull.WriteLine(xmlnota);
                    swProductFull.Close();

                    //Cria comunicação com o servidor 
                    FileInfo product_full2 = new FileInfo(rnConfiguracoes.caminhoFisico + "notatransportadoras/" + transportadora + "_" + numeroNota + ".xml");
                    FtpWebRequest request = (FtpWebRequest)FtpWebRequest.Create("ftp://ftp.ssw.inf.br/xml_graodegente/" + numeroNota + ".xml");
                    //Define que a ação vai ser de upload 
                    request.Method = WebRequestMethods.Ftp.UploadFile;
                    //Credenciais para o login (usuario, senha) 
                    request.Credentials = new NetworkCredential("sswinfbr6", "xZrHv9xcN");
                    //modo passivo 
                    //request.UsePassive = true; 
                    //dados binarios 
                    request.UseBinary = true;
                    //setar o KeepAlive para false 
                    request.KeepAlive = false;

                    request.ContentLength = product_full2.Length;
                    //cria a stream que será usada para mandar o arquivo via FTP 
                    Stream responseStream = request.GetRequestStream();
                    byte[] buffer = new byte[2048];

                    //Lê o arquivo de origem 
                    FileStream fs = product_full2.OpenRead();
                    try
                    {
                        //Enquanto vai lendo o arquivo de origem, vai escrevendo no FTP 
                        int readCount = fs.Read(buffer, 0, buffer.Length);
                        while (readCount > 0)
                        {
                            //Esceve o arquivo 
                            responseStream.Write(buffer, 0, readCount);
                            readCount = fs.Read(buffer, 0, buffer.Length);
                        }
                    }
                    finally
                    {
                        fs.Close();
                        responseStream.Close();
                    }

                    #endregion Envia para Ftp da RichRelevance 
                }
                if (transportadora == "nowlog")
                {
                    #region Envia para Ftp da RichRelevance 
                    FileInfo product_full = new FileInfo(rnConfiguracoes.caminhoFisico + "notatransportadoras/" + transportadora + "_" + numeroNota + ".xml");
                    if (product_full.Exists)
                    {
                        product_full.Delete();
                    }
                    StreamWriter swProductFull = product_full.CreateText();
                    swProductFull.WriteLine(xmlnota);
                    swProductFull.Close();


                    FileInfo product_full2 = new FileInfo(rnConfiguracoes.caminhoFisico + "notatransportadoras/" + transportadora + "_" + numeroNota + ".xml");
                    //Cria comunicação com o servidor 
                    FtpWebRequest request = (FtpWebRequest)FtpWebRequest.Create("ftp://ftp.ssw.inf.br/graodegente/notfis/" + numeroNota + ".xml");
                    //Define que a ação vai ser de upload 
                    request.Method = WebRequestMethods.Ftp.UploadFile;
                    //Credenciais para o login (usuario, senha) 
                    request.Credentials = new NetworkCredential("sswinfbr7", "@Y$ecAKQ7k");
                    //modo passivo 
                    //request.UsePassive = true; 
                    //dados binarios 
                    request.UseBinary = true;
                    //setar o KeepAlive para false 
                    request.KeepAlive = false;

                    request.ContentLength = product_full2.Length;
                    //cria a stream que será usada para mandar o arquivo via FTP 
                    Stream responseStream = request.GetRequestStream();
                    byte[] buffer = new byte[2048];

                    //Lê o arquivo de origem 
                    FileStream fs = product_full2.OpenRead();
                    try
                    {
                        //Enquanto vai lendo o arquivo de origem, vai escrevendo no FTP 
                        int readCount = fs.Read(buffer, 0, buffer.Length);
                        while (readCount > 0)
                        {
                            //Esceve o arquivo 
                            responseStream.Write(buffer, 0, readCount);
                            readCount = fs.Read(buffer, 0, buffer.Length);
                        }
                    }
                    finally
                    {
                        fs.Close();
                        responseStream.Close();
                    }

                    #endregion Envia para Ftp da RichRelevance 
                }
                if (transportadora == "totalexpress")
                {
                    #region Envia para Ftp da RichRelevance 
                    FileInfo product_full = new FileInfo(rnConfiguracoes.caminhoFisico + "notatransportadoras/" + transportadora + "_" + numeroNota + ".xml");
                    if (product_full.Exists)
                    {
                        product_full.Delete();
                    }
                    StreamWriter swProductFull = product_full.CreateText();
                    swProductFull.WriteLine(xmlnota);
                    swProductFull.Close();


                    FileInfo product_full2 = new FileInfo(rnConfiguracoes.caminhoFisico + "notatransportadoras/" + transportadora + "_" + numeroNota + ".xml");
                    //Cria comunicação com o servidor 
                    FtpWebRequest request = (FtpWebRequest)FtpWebRequest.Create("ftp://ftp.totalexpress.com.br/envios/" + numeroNota + ".xml");
                    //Define que a ação vai ser de upload 
                    request.Method = WebRequestMethods.Ftp.UploadFile;
                    //Credenciais para o login (usuario, senha) 
                    request.Credentials = new NetworkCredential("graodegente", "zuDrwsrY");
                    //modo passivo 
                    //request.UsePassive = true; 
                    //dados binarios 
                    request.UseBinary = true;
                    //setar o KeepAlive para false 
                    request.KeepAlive = false;

                    request.ContentLength = product_full2.Length;
                    //cria a stream que será usada para mandar o arquivo via FTP 
                    Stream responseStream = request.GetRequestStream();
                    byte[] buffer = new byte[2048];

                    //Lê o arquivo de origem 
                    FileStream fs = product_full2.OpenRead();
                    try
                    {
                        //Enquanto vai lendo o arquivo de origem, vai escrevendo no FTP 
                        int readCount = fs.Read(buffer, 0, buffer.Length);
                        while (readCount > 0)
                        {
                            //Esceve o arquivo 
                            responseStream.Write(buffer, 0, readCount);
                            readCount = fs.Read(buffer, 0, buffer.Length);
                        }
                    }
                    finally
                    {
                        fs.Close();
                        responseStream.Close();
                    }

                    #endregion Envia para Ftp da RichRelevance 
                }
                if (transportadora == "transfolha")
                {
                    #region Envia para Ftp da RichRelevance 
                    FileInfo product_full = new FileInfo(rnConfiguracoes.caminhoFisico + "notatransportadoras/" + transportadora + "_" + numeroNota + ".xml");
                    if (product_full.Exists)
                    {
                        product_full.Delete();
                    }
                    StreamWriter swProductFull = product_full.CreateText();
                    swProductFull.WriteLine(xmlnota);
                    swProductFull.Close();

                    FileInfo product_full3 = new FileInfo(rnConfiguracoes.caminhoFisico + "notatransportadoras/" + transportadora + "_" + numeroNota + ".xml");
                    //Cria comunicação com o servidor 
                    FtpWebRequest request = (FtpWebRequest)FtpWebRequest.Create("ftp://ftp.transfolha.com.br/xml/" + numeroNota + ".xml");
                    //Define que a ação vai ser de upload 
                    request.Method = WebRequestMethods.Ftp.UploadFile;
                    //Credenciais para o login (usuario, senha) 
                    request.Credentials = new NetworkCredential("Graogente", "Gr40gent3");
                    //modo passivo 
                    //request.UsePassive = true; 
                    //dados binarios 
                    request.UseBinary = true;
                    //setar o KeepAlive para false 
                    request.KeepAlive = false;

                    request.ContentLength = product_full3.Length;
                    //cria a stream que será usada para mandar o arquivo via FTP 
                    Stream responseStream = request.GetRequestStream();
                    byte[] buffer = new byte[2048];

                    //Lê o arquivo de origem 
                    FileStream fs = product_full3.OpenRead();
                    try
                    {
                        //Enquanto vai lendo o arquivo de origem, vai escrevendo no FTP 
                        int readCount = fs.Read(buffer, 0, buffer.Length);
                        while (readCount > 0)
                        {
                            //Esceve o arquivo 
                            responseStream.Write(buffer, 0, readCount);
                            readCount = fs.Read(buffer, 0, buffer.Length);
                        }
                    }
                    finally
                    {
                        fs.Close();
                        responseStream.Close();
                    }

                    #endregion Envia para Ftp da RichRelevance 
                }
                //return true; 
            }
            catch (Exception)
            {
                //return false; 
            }

            #region finalizaQueue 
            queue.concluido = true;
            queue.dataConclusao = DateTime.Now;
            data.SaveChanges();
            #endregion
        }

        private void timerRegistroBoleto_Tick(object sender, EventArgs e)
        {
            timerRegistroBoleto.Enabled = false;
            if (bwRegistroBoleto.IsBusy != true)
            {
                bwRegistroBoleto.RunWorkerAsync();
            }
            else
            {
                timerRegistroBoleto.Enabled = true;
            }

        }

        private void bwRegistroBoleto_DoWork(object sender, DoWorkEventArgs e)
        {
            var data = new dbSiteEntities();
            var queues = (from c in data.tbQueue where c.tipoQueue == 45 && c.andamento == false && c.concluido == false select c).ToList();

            foreach(var queue in queues)
            {
                queue.andamento = true;
                queue.dataInicio = DateTime.Now;
                data.SaveChanges();

                var riscoRepo = new BarkCommerce.Core.Repositories.AnaliseRisco.ClearSaleRepository();
                var gatewayRepo = new BarkCommerce.Core.Repositories.Gateway.MaxipagoRepository();
                var pagamentoRepo = new BarkCommerce.Core.Repositories.Pagamento.PagamentoRepository(riscoRepo, gatewayRepo);
                var pagamentoBusiness = new BarkCommerce.Business.Pagamento.ProcessarPagamentosPedido(pagamentoRepo);
                var pagamentoEnviado = pagamentoRepo.RegistraBoleto(Convert.ToInt32(queue.idRelacionado), queue.mensagem);
                if(pagamentoEnviado == false)
                {
                    queue.andamento = false;
                    data.SaveChanges();
                }
                else
                {
                    queue.concluido = true;
                    queue.dataConclusao = DateTime.Now;
                    data.SaveChanges();
                }
            }
        }

        private void bwRegistroBoleto_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            timerRegistroBoleto.Interval = 5000;
            timerRegistroBoleto.Enabled = true;
        }


        


        private void ConsultarEnviosNotasQueue()
        {
            lblNotasTransportadora.Invoke((MethodInvoker)delegate
            {
                lblNotasTransportadora.Text = "Iniciando chamada a queue " + DateTime.Now.ToShortTimeString();
            });
            if (bwEnviarNotaTransportadora.IsBusy != true)
            {
                bwEnviarNotaTransportadora.RunWorkerAsync();
            }
        }
        private void bwEnviarNotaTransportadora_DoWork(object sender, DoWorkEventArgs e)
        {
            var data = new dbSiteEntities();
            lblNotasTransportadora.Invoke((MethodInvoker)delegate
            {
                lblNotasTransportadora.Text = "Consultando queues " + DateTime.Now.ToShortTimeString();
            });
            var queues = (from c in data.tbQueue where c.andamento == false && c.concluido == false && 
                          (c.tipoQueue == 3 | c.tipoQueue == 41 | c.tipoQueue == 44 | c.tipoQueue == 2)
                          select new { c.idQueue, c.tipoQueue }).Take(200).ToList();
            foreach(var queue in queues)
            {
                lblNotasTransportadora.Invoke((MethodInvoker)delegate
                {
                    lblNotasTransportadora.Text = "Rodando queues " + queue.idQueue;
                });
                if (queue.tipoQueue == 3 && rnConfiguracoes.HabilitarQueue3) //Carregamento -> Enviar XML tnt
                {
                    if (bwEmailTnt.IsBusy != true)
                    {
                        lblNotasTransportadora.Invoke((MethodInvoker)delegate
                        {
                            lblNotasTransportadora.Text = "Chamando queue enviar nota " + queue.idQueue;
                        });
                        bwEmailTnt.RunWorkerAsync(queue.idQueue);
                    }
                }
                if (queue.tipoQueue == 41 && rnConfiguracoes.HabilitarQueue3) //Carregamento -> Enviar XML tnt
                {
                    if (bwEmailPlimor.IsBusy != true)
                    {
                        bwEmailPlimor.RunWorkerAsync(queue.idQueue);
                    }
                }
                if (queue.tipoQueue == 44) //Enviar xml ftp 
                {
                    EnviaNotaFtp(queue.idQueue);
                }
                if (queue.tipoQueue == 2 && rnConfiguracoes.HabilitarQueue2) //Carregamento Efetuado -> Enviar email
                {
                    lblNotasTransportadora.Invoke((MethodInvoker)delegate
                    {
                        lblNotasTransportadora.Text = "Chamando queue email carregamento " + queue.idQueue;
                    });
                    if (bwEnviaEmailCarregamento.IsBusy != true)
                    {
                        bwEnviaEmailCarregamento.RunWorkerAsync(queue.idQueue);
                    }
                }
            }
            
        }

        private void bwEnviarNotaTransportadora_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            timerNotas.Interval = 5000;
            timerNotas.Enabled = true;
        }




        #region Enviar Email Confirmacao Pedido

        private void ConsultarEmailsConfirmacaoQueue()
        {
            if (bwEnviarEmailsConfirmacaoPedido.IsBusy != true)
            {
                bwEnviarEmailsConfirmacaoPedido.RunWorkerAsync();
            }
        }
        private void bwEnviarEmailsConfirmacaoPedido_DoWork(object sender, DoWorkEventArgs e)
        {
            var data = new dbSiteEntities();
            var queues = (from c in data.tbQueue
                          where c.andamento == false && c.concluido == false &&
            (c.tipoQueue == 7 | c.tipoQueue == 42)
                          select new { c.idQueue, c.tipoQueue }).ToList();
            foreach (var queue in queues)
            {
                if (queue.tipoQueue == 7 && rnConfiguracoes.HabilitarQueue7) //Pedido -> Enviar Email Pedido
                {
                    try
                    {
                        if (!rnConfiguracoes.AmbienteDebugEnviarEmailConfirmacao) EnviarEmailConfirmacaoPedidoQueue(queue.idQueue);
                    }
                    catch (Exception)
                    {

                    }
                }
                if (queue.tipoQueue == 42 && rnConfiguracoes.HabilitarQueue42) //Carregamento -> Enviar XML tnt
                {
                    enviarSegundaViaBoleto(queue.idQueue);
                }
            }
        }
        private void EnviarEmailConfirmacaoPedidoQueue(long idQueue)
        {
            #region atualizaQueue
            var dataQueue = new dbSiteEntities();
            var queue = (from c in dataQueue.tbQueue where c.idQueue == idQueue select c).First();
            bool firstTry = (queue.dataInicio == null);
            queue.andamento = true;
            queue.dataInicio = DateTime.Now;
            dataQueue.SaveChanges();
            #endregion

            bool enviado = rnEmails.enviaConfirmacaoDePedido((int)queue.idRelacionado);

            if (enviado)
            {
                #region finalizaQueue

                var queueFinalizar = (from c in dataQueue.tbQueue where c.idQueue == idQueue select c).First();
                queueFinalizar.concluido = true;
                queueFinalizar.dataConclusao = DateTime.Now;
                dataQueue.SaveChanges();

                #endregion
            }
            else if (firstTry)
            {
                rnEmails.EnviaEmail("", "andre@bark.com.br", "", "", "", "Erro ao calcular prazo maximo pedidos", queue.idQueue.ToString());
                #region retornaQueue
                var queueFinalizar = (from c in dataQueue.tbQueue where c.idQueue == idQueue select c).First();
                queueFinalizar.andamento = false;
                dataQueue.SaveChanges();
                #endregion
            }


        }



        #endregion


        private void ConsultarProcessarPagamentosPedidosQueue()
        {
            if (bwProcessarPagamentos.IsBusy != true)
            {
                bwProcessarPagamentos.RunWorkerAsync();
            }
        }
        private void bwProcessarPagamentos_DoWork(object sender, DoWorkEventArgs e)
        {
            var data = new dbSiteEntities();
            var queues = (from c in data.tbQueue
                          where c.andamento == false && c.concluido == false &&
            (c.tipoQueue == 10 | c.tipoQueue == 11 | c.tipoQueue == 12 | c.tipoQueue == 23
             | c.tipoQueue == 24 | c.tipoQueue == 32 | c.tipoQueue == 33 | c.tipoQueue == 34)
                          select new { c.idQueue, c.tipoQueue }).ToList();
            foreach (var queue in queues)
            {
                if (queue.tipoQueue == 10 && rnConfiguracoes.HabilitarQueue10) //Processar Pagamento Pedido
                {
                    ProcessaPagamentoPedido(queue.idQueue);
                }
                if (queue.tipoQueue == 11 && rnConfiguracoes.HabilitarQueue11) //Processar Pagamento Avulso
                {
                    ProcessaPagamento(queue.idQueue);
                }
                if (queue.tipoQueue == 12 && rnConfiguracoes.HabilitarQueue12) //Atualiza Pagamento
                {
                    AtualizaPagamento(queue.idQueue);
                } 
                if (queue.tipoQueue == 23 && rnConfiguracoes.HabilitarQueue23) //Lote no banco
                {
                    if (bwLoteBanco.IsBusy != true)
                    {
                        bwLoteBanco.RunWorkerAsync(queue.idQueue);
                    }
                }
                if (queue.tipoQueue == 24 && rnConfiguracoes.HabilitarQueue24) //Lote no banco
                {
                    EnviaPagamentoClearsale(queue.idQueue);
                }
                if (queue.tipoQueue == 32 && rnConfiguracoes.HabilitarQueue32) //Forçar Captura Pagamento
                {
                    CapturarPagamentoQueue(queue.idQueue);
                }
                if (queue.tipoQueue == 33 && rnConfiguracoes.HabilitarQueue33)  //Consultar status na Clearsale
                {
                    ConsultarClearsaleQueue(queue.idQueue);
                }
                if (queue.tipoQueue == 34 && rnConfiguracoes.HabilitarQueue34) //Gerar Lote Cobrança
                {
                    GerarLotesCobrancas(queue.idQueue);
                }
            }            
        }

        private void timerQueuesParalelos_Tick(object sender, EventArgs e)
        {
            timerQueuesParalelos.Enabled = false;
            if (bwQueuesParalelos.IsBusy != true)
            {
                bwQueuesParalelos.RunWorkerAsync();
            }
            else
            {
                timerQueuesParalelos.Interval = 5000;
                timerQueuesParalelos.Enabled = true;
            }
        }

        private void bwQueuesParalelos_DoWork(object sender, DoWorkEventArgs e)
        {
            ConsultarProcessarPagamentosPedidosQueue();
            ReservarEstoqueProdutoQueue();
            ConsultarPedidosCompletosQueue();
            ConsultarEnviosNotasQueue();
            ConsultarEmailsConfirmacaoQueue();
            ConsultarAtualizarEstoqueRealQueue();
        }

        private void bwQueuesParalelos_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            timerQueuesParalelos.Interval = 5000;
            timerQueuesParalelos.Enabled = true;
        }

        private void ConsultarAtualizarEstoqueRealQueue()
        {
            if (bwAtualizarEstoqueReal.IsBusy != true)
            {
                bwAtualizarEstoqueReal.RunWorkerAsync();
            }
        }
        private void bwAtualizarEstoqueReal_DoWork(object sender, DoWorkEventArgs e)
        {
            var data = new dbSiteEntities();
            var queues = (from c in data.tbQueue
                          where c.andamento == false && c.concluido == false && (c.tipoQueue == 38)
                          select new { c.idQueue, c.tipoQueue }).ToList();
            foreach (var queue in queues)
            {
                if (queue.tipoQueue == 38) //Atualizar Estoque Real
                {
                    rnEstoque.AtualizarEstoqueRealProdutoQueue(queue.idQueue);
                }
            }
        }

        private void bwGerarSimulacaoFrete_DoWork(object sender, DoWorkEventArgs e)
        {
            var data = new dbSiteEntities();
            var emissaoWs = new wsEmissao.EmissaoWs();
            emissaoWs.AtualizaEntregasPedido(720277);
        }

        private void bwLembreteBoleto_DoWork(object sender, DoWorkEventArgs e)
        {
            EnviaLembreteBoleto();
        }

        private void EnviaLembreteBoleto()
        {
            var data = new dbSiteEntities();
            var agora = DateTime.Now;
            var inicio = agora.AddDays(-5);
            var pedidos = (from c in data.tbPedidos
                           join d in data.tbPedidoPagamento on c.pedidoId equals d.pedidoId into pagamentos
                           where c.statusDoPedido == 2 && c.dataHoraDoPedido > inicio && c.tipoDePagamentoId == 1
                           && pagamentos.Count() == 1 && pagamentos.FirstOrDefault().dataVencimento > agora
                           select new
                           {
                               c.pedidoId,
                               c.clienteId,
                               dataPedido = (c.dataHoraDoPedido ?? agora)
                           }).ToList().Where(x => agora > x.dataPedido.AddDays(1)).ToList();
            //var pedidos20 = pedidos.Take(Convert.ToInt32(pedidos.Count / 5)).ToList();
            var pedidos20 = pedidos.ToList();
            foreach (var pedido in pedidos20)
            {
                var validaPushEnviado = (from c in data.tbEmailEnvio
                                         join d in data.tbPedidos on c.pedidoId equals d.pedidoId
                                         where d.clienteId == pedido.clienteId && 
                                         c.idEmailTipo == 3 select c).ToList().Any(x => x.dataEnvio.AddDays(1) > agora);
                if (!validaPushEnviado)
                {
                    enviarPushBoleto(pedido.pedidoId, pedido.clienteId ?? 0);
                }                
            }
        }
        private void enviarPushBoleto(int pedidoId, int clienteId)
        {
            var agora = DateTime.Now;            
            var enviado = enviarPush(clienteId, "Não deixe que sua compra seja cancelada", "Aproveite e pague seu boleto agora", "https://www.graodegente.com.br/pedidos?utm_source=transacional&utm_medium=webpush&utm_campaign=lembrete_boleto", "");
            if (enviado)
            {
                var data = new dbSiteEntities();
                var pushEnviado = new tbEmailEnvio()
                {
                    pedidoId = pedidoId,
                    dataEnvio = agora,
                    idEmailTipo = 3,
                    visualizado = false
                };
                data.tbEmailEnvio.Add(pushEnviado);
                data.SaveChanges();
            }
        }

        private bool enviarPush(int clienteId, string title, string content, string url, string imagem)
        {
            dynamic push = new
            {
                appId = "www.graodegente.com.br",
                clientId = clienteId.ToString(),
                title = title,
                content = content,
                url = url,
                image = imagem
            };


            var pushObject = JsonConvert.SerializeObject(push);
            var request = (HttpWebRequest)WebRequest.Create("https://konduza-server.appspot.com/api/v2/notification/send");
            request.Method = "POST";
            request.ContentType = "application/json";
            var conteudo = Encoding.UTF8.GetBytes(pushObject);
            request.GetRequestStream().Write(conteudo, 0, conteudo.Length);
            var location = String.Empty;
            var result = String.Empty;
            try
            {
                var response1 = (HttpWebResponse)request.GetResponse();
                using (var stream = response1.GetResponseStream())
                using (var reader = new StreamReader(stream))
                {
                    result = reader.ReadToEnd();
                }
                dynamic retorno = JsonConvert.DeserializeObject(result);
                return true;
            }
            catch (WebException ex)
            {
                var response1 = ex.Response;
                /*using (var stream = response1.GetResponseStream())
                using (var reader = new StreamReader(stream))
                {
                    result = reader.ReadToEnd();
                }*/
                return false;
            }
        }
        private void bwLembreteBoleto_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            timerLembreteBoleto.Interval = 3600000;
            timerLembreteBoleto.Enabled = true;

        }

        private void timerLembreteBoleto_Tick(object sender, EventArgs e)
        {
            timerLembreteBoleto.Enabled = false;
            if (bwLembreteBoleto.IsBusy != true)
            {
                bwLembreteBoleto.RunWorkerAsync();
            }
        }
        
    }

}
