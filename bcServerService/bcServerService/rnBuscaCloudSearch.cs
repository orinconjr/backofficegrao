﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
//using AmazingCloudSearch;
//using AmazingCloudSearch.Contract;

namespace bcServerService
{
    public class rnBuscaCloudSearch
    {
        //private static string apiKey = "graodegente-gktkrqswsa2wfxve4reqjoegfq.sa-east-1.cloudsearch.amazonaws.comaaa";
        //private static string apiKey = "graodegentesa-67milztjlm63efc2tg7ibtu7eu.us-east-1.cloudsearch.amazonaws.com";
        private static string apiVersion = "2013-01-01";

        public class produtoBusca //: CloudSearchDocument
        {
            public int siteid { get; set; }
            public int prazopedidos { get; set; }
            public string categorianome { get; set; }

            public string categoriaurl { get; set; }

            public List<int> colecaoids { get; set; }

            public List<string> colecaonomes { get; set; }

            public int exibirdiferencacombo { get; set; }

            public List<int> filtroids { get; set; }

            public List<string> filtronomes { get; set; }

            public string fotodestaque { get; set; }

            public double preco { get; set; }

            public int produtobrindes { get; set; }

            public int produtoexclusivo { get; set; }

            public int produtoid { get; set; }

            public int produtolancamento { get; set; }

            public string produtonome { get; set; }

            public int produtopecas { get; set; }

            public double produtopreco { get; set; }

            public double produtoprecopromocional { get; set; }

            public int produtopromocao { get; set; }

            public string produtourl { get; set; }

            public double valordiferencacombo { get; set; }

            public int relevancia { get; set; }

            public string categoriatags { get; set; }

            public string produtotags { get; set; }

            public int produtoativo { get; set; }

            public int produtoprincipal { get; set; }

            public int categoriaid { get; set; }

            public int parcelamentomaximo { get; set; }

            public double parcelamentomaximovalorparcela { get; set; }
            public string primeirapalavra { get; set; }
            public int estoquereal { get; set; }
            public int relevanciafornecedor { get; set; }
            public int relevanciamanual { get; set; }
            public double desconto { get; set; }
            public double descontototalavista { get; set; }
        }

        /*public static AmazingCloudSearch.Contract.Result.AddResult AtualizarTodos(int inicio, int quantidade)
        {
            if (rnConfiguracoes.AtualizarCloudSearch == false) return null;
            var cloudSearch = new CloudSearch<rnBuscaCloudSearch.produtoBusca>(ConfigurationManager.AppSettings["cloudSearchApiKey"], apiVersion);
            var listaProdutos = new List<rnBuscaCloudSearch.produtoBusca>();

            var data = new dbSiteEntities();
            var produtos = (from c in data.tbProdutos select c).ToList();
            int produtoInicial = produtos.OrderBy(x => x.produtoId).First().produtoId;
            int produtoFinal = produtos.OrderByDescending(x => x.produtoId).First().produtoId;
            if (quantidade > 0) produtos = produtos.Skip(inicio).Take(quantidade).ToList();

            //produtoInicial = 26081;
            //produtoFinal = 26081;

            for (int indprod = produtoFinal; indprod >= produtoInicial; indprod--)
            {
                try
                {
                    var produto = (from c in data.tbProdutos where c.produtoId == indprod select c).FirstOrDefault();
                    if (produto != null)
                    {
                        if (produto.produtoId == 23810)
                        {
                            string bp = "";
                        }
                        var produtoAdd = new rnBuscaCloudSearch.produtoBusca();
                        var categorias =
                            (from c in data.tbJuncaoProdutoCategoria where c.produtoId == produto.produtoId select c)
                                .ToList();
                        var categoriaRaiz =
                            categorias.FirstOrDefault(
                                x => x.tbProdutoCategoria.categoriaPaiId == 0 && x.tbProdutoCategoria.exibirSite == true);
                        if (categoriaRaiz != null)
                        {

                            var desconto = CalculaDesconto((decimal)produto.produtoPreco, (decimal)produto.produtoPrecoPromocional);
                            var descontoAVista = CalculaDescontoAVista((decimal)produto.produtoPreco, (decimal)produto.produtoPrecoPromocional);


                            string termoBusca = produto.produtoNome;
                            var valorExibicao = produto.produtoPrecoPromocional > 0 &&
                                                produto.produtoPrecoPromocional < produto.produtoPreco
                                ? (decimal)produto.produtoPrecoPromocional
                                : produto.produtoPreco;
                            string adicionar = "";
                            int relevancia = produto.relevancia ?? 0;
                            string primeirapalavra = "";
                            if (produto.produtoNome.Contains(" "))
                            {
                                primeirapalavra = produto.produtoNome.Split(' ')[0];
                            }
                            else
                            {
                                primeirapalavra = rnFuncoes.removeAcentos(produto.produtoNome).ToLower();
                            }
                            produtoAdd.primeirapalavra = primeirapalavra;
                            produtoAdd.Id = produto.produtoId.ToString();
                            produtoAdd.produtoid = produto.produtoId;
                            produtoAdd.fotodestaque = produto.fotoDestaque == null ? "" : produto.fotoDestaque;
                            produtoAdd.produtoexclusivo = ((produto.produtoExclusivo ?? "").ToLower() == "true" ? 1 : 0);
                            produtoAdd.produtolancamento = ((produto.produtoLancamento ?? "").ToLower() == "true" ? 1 : 0);
                            produtoAdd.produtopromocao = ((produto.produtoPromocao ?? "").ToLower() == "true" ? 1 : 0);
                            int brindes = 0;
                            int.TryParse(produto.produtoBrindes, out brindes);
                            produtoAdd.produtobrindes = brindes;

                            int pecas = 0;
                            int.TryParse(produto.produtoPecas, out pecas);
                            produtoAdd.produtopecas = pecas;

                            produtoAdd.desconto = Convert.ToDouble(desconto);
                            produtoAdd.descontototalavista = Convert.ToDouble(descontoAVista);
                            produtoAdd.prazopedidos = produto.prazoDeEntrega;

                            produtoAdd.relevancia = relevancia;
                            produtoAdd.produtonome = produto.produtoNome;
                            produtoAdd.produtourl = produto.produtoUrl.Replace("\\", "-");
                            produtoAdd.produtotags = (produto.produtoTags ?? "").Replace(",", " ");
                            produtoAdd.categorianome = categoriaRaiz.tbProdutoCategoria.categoriaNomeExibicao.Replace("\\",
                                "-");
                            produtoAdd.categoriaurl = categoriaRaiz.tbProdutoCategoria.categoriaUrl.Replace("\\", "-");
                            produtoAdd.categoriatags = (categoriaRaiz.tbProdutoCategoria.categoriaTags ?? "").Replace(",",
                                " ");
                            produtoAdd.preco = Convert.ToDouble(valorExibicao);
                            produtoAdd.produtopreco = Convert.ToDouble(produto.produtoPreco);
                            produtoAdd.produtoprecopromocional = Convert.ToDouble(produto.produtoPrecoPromocional);
                            produtoAdd.exibirdiferencacombo = (produto.exibirDiferencaCombo ?? "").ToLower() == "true"
                                ? 1
                                : 0;
                            produtoAdd.valordiferencacombo = Convert.ToDouble(produto.valorDiferencaCombo);
                            produtoAdd.produtoativo = ((produto.produtoAtivo ?? "").ToLower() == "true" ? 1 : 0);
                            produtoAdd.produtoprincipal = ((produto.produtoPrincipal ?? "").ToLower() == "true" ? 1 : 0);
                            produtoAdd.categoriaid = categoriaRaiz.categoriaId;
                            produtoAdd.parcelamentomaximo = produto.parcelamentoMaximo;
                            produtoAdd.parcelamentomaximovalorparcela =
                                Convert.ToDouble(produto.parcelamentoMaximoValorParcela);

                            var listaCategoriaNomes = new List<string>();
                            var listaCategoriaIds = new List<int>();

                            int categoriasCount = categorias.Count();
                            for (int i = 0; i < categoriasCount; i++)
                            {
                                listaCategoriaNomes.Add(categorias[i].tbProdutoCategoria.categoriaNomeExibicao);
                                listaCategoriaIds.Add(categorias[i].tbProdutoCategoria.categoriaId);
                                termoBusca += " " + categorias[i].tbProdutoCategoria.categoriaNomeExibicao;
                            }
                            produtoAdd.filtroids = listaCategoriaIds;
                            produtoAdd.filtronomes = listaCategoriaNomes;

                            var listaColecaoNomes = new List<string>();
                            var listaColecaoIds = new List<int>();
                            var colecoes =
                                (from c in data.tbJuncaoProdutoColecao where c.produtoId == produto.produtoId select c)
                                    .ToList();
                            int colecoesCount = colecoes.Count();
                            if (colecoesCount > 0)
                            {
                                for (int i = 0; i < colecoesCount; i++)
                                {
                                    termoBusca += " " + colecoes[i].tbColecao.colecaoNome;
                                    listaColecaoNomes.Add(colecoes[i].tbColecao.colecaoNome);
                                    listaColecaoIds.Add(colecoes[i].tbColecao.colecaoId);
                                }
                            }
                            produtoAdd.colecaoids = listaColecaoIds;
                            produtoAdd.colecaonomes = listaColecaoNomes;
                            //produtoAdd.termoBusca = termoBusca;
                            produtoAdd.estoquereal = (produto.estoqueReal ?? 0);
                            produtoAdd.relevanciamanual = produto.relevanciaManual;
                            produtoAdd.relevanciafornecedor = produto.tbProdutoFornecedor.relevanciaProdutos;
                            produtoAdd.siteid = produto.siteId;
                            if (string.IsNullOrEmpty(produtoAdd.fotodestaque)) produtoAdd.fotodestaque = "semfoto";
                            listaProdutos.Add(produtoAdd);
                        }
                        else
                        {
                            ExcluirProduto(indprod);
                        }
                    }
                    else
                    {
                        ExcluirProduto(indprod);
                    }
                }
                catch (Exception ex)
                {
                    //rnEmails.EnviaEmail("", "andre@bark.com.br", "", "", "", ex.InnerException.ToString(), "Erro busca");
                }
            }

            return cloudSearch.Add(listaProdutos);
        }
        public static AmazingCloudSearch.Contract.Result.AddResult AtualizarProduto(int produtoId)
        {
            if (rnConfiguracoes.AtualizarCloudSearch == false) return null;

            try
            {
                var cloudSearch = new CloudSearch<rnBuscaCloudSearch.produtoBusca>(ConfigurationManager.AppSettings["cloudSearchApiKey"], apiVersion);

                var data = new dbSiteEntities();
                var produto = (from c in data.tbProdutos select c).Where(x => x.produtoId == produtoId).FirstOrDefault();

                var produtoAdd = new rnBuscaCloudSearch.produtoBusca();
                if (produto != null)
                {
                    try
                    {

                        var precoExibicao = produto.produtoPrecoPromocional > 0 && produto.produtoPrecoPromocional < produto.produtoPreco
                            ? produto.produtoPrecoPromocional
                            : produto.produtoPreco;
                        var parcelamento = calculaParcelamento((decimal)precoExibicao);
                        var desconto = CalculaDesconto((decimal)produto.produtoPreco, (decimal)produto.produtoPrecoPromocional);
                        var descontoAVista = CalculaDescontoAVista((decimal)produto.produtoPreco, (decimal)produto.produtoPrecoPromocional);
                        //int prazoProduto = CalculaPrazoMaximoProdutoFornecedor(produto.produtoId);

                        bool alterado = false;
                        if (produto.parcelamentoMaximo != parcelamento.parcelas)
                        {
                            produto.parcelamentoMaximo = parcelamento.parcelas;
                            alterado = true;
                        }
                        if (produto.parcelamentoMaximoValorParcela.ToString("C") != parcelamento.valor.ToString("C"))
                        {
                            alterado = true;
                            produto.parcelamentoMaximoValorParcela = parcelamento.valor;
                        }
                        //if (prazoProduto != produto.prazoDeEntrega)
                        //{
                        //    alterado = true;
                        //    produto.prazoDeEntrega = prazoProduto;
                        //}
                        if (alterado) data.SaveChanges();


                        var categorias = (from c in data.tbJuncaoProdutoCategoria where c.produtoId == produto.produtoId select c).ToList();
                        var categoriaRaiz = categorias.FirstOrDefault(x => x.tbProdutoCategoria.categoriaPaiId == 0 && x.tbProdutoCategoria.exibirSite == true);
                        if (categoriaRaiz == null) categoriaRaiz = categorias.FirstOrDefault(x => x.tbProdutoCategoria.categoriaPaiId == 0);
                        if (categoriaRaiz != null)
                        {
                            string termoBusca = produto.produtoNome;
                            var valorExibicao = produto.produtoPrecoPromocional > 0 &&
                                                produto.produtoPrecoPromocional < produto.produtoPreco
                                ? (decimal)produto.produtoPrecoPromocional
                                : produto.produtoPreco;
                            string adicionar = "";
                            int relevancia = produto.relevancia ?? 0;
                            string primeirapalavra = "";
                            if (produto.produtoNome.Contains(" "))
                            {
                                primeirapalavra = produto.produtoNome.Split(' ')[0];
                            }
                            else
                            {
                                primeirapalavra = rnFuncoes.removeAcentos(produto.produtoNome).ToLower();
                            }


                            produtoAdd.desconto = Convert.ToDouble(desconto);
                            produtoAdd.descontototalavista = Convert.ToDouble(descontoAVista);
                            produtoAdd.prazopedidos = produto.prazoDeEntrega;
                            produtoAdd.primeirapalavra = primeirapalavra;
                            produtoAdd.Id = produto.produtoId.ToString();
                            produtoAdd.produtoid = produto.produtoId;
                            produtoAdd.fotodestaque = produto.fotoDestaque == null ? "" : produto.fotoDestaque;
                            produtoAdd.produtoexclusivo = ((produto.produtoExclusivo ?? "").ToLower() == "true" ? 1 : 0);
                            produtoAdd.produtolancamento = ((produto.produtoLancamento ?? "").ToLower() == "true" ? 1 : 0);
                            produtoAdd.produtopromocao = ((produto.produtoPromocao ?? "").ToLower() == "true" ? 1 : 0);
                            int brindes = 0;
                            int.TryParse(produto.produtoBrindes, out brindes);
                            produtoAdd.produtobrindes = brindes;

                            int pecas = 0;
                            int.TryParse(produto.produtoPecas, out pecas);
                            produtoAdd.produtopecas = pecas;


                            produtoAdd.relevancia = relevancia;
                            produtoAdd.produtonome = produto.produtoNome;
                            produtoAdd.produtourl = produto.produtoUrl.Replace("\\", "-");
                            produtoAdd.produtotags = (produto.produtoTags ?? "").Replace(",", " ");
                            produtoAdd.categorianome = categoriaRaiz.tbProdutoCategoria.categoriaNomeExibicao.Replace("\\",
                                "-");
                            produtoAdd.categoriaurl = categoriaRaiz.tbProdutoCategoria.categoriaUrl.Replace("\\", "-");
                            produtoAdd.categoriatags = (categoriaRaiz.tbProdutoCategoria.categoriaTags ?? "").Replace(",",
                                " ");
                            produtoAdd.preco = Convert.ToDouble(valorExibicao);
                            produtoAdd.produtopreco = Convert.ToDouble(produto.produtoPreco);
                            produtoAdd.produtoprecopromocional = Convert.ToDouble(produto.produtoPrecoPromocional);
                            produtoAdd.exibirdiferencacombo = (produto.exibirDiferencaCombo ?? "").ToLower() == "true"
                                ? 1
                                : 0;
                            produtoAdd.valordiferencacombo = Convert.ToDouble(produto.valorDiferencaCombo);
                            produtoAdd.produtoativo = ((produto.produtoAtivo ?? "").ToLower() == "true" ? 1 : 0);
                            produtoAdd.produtoprincipal = ((produto.produtoPrincipal ?? "").ToLower() == "true" ? 1 : 0);
                            produtoAdd.categoriaid = categoriaRaiz.categoriaId;
                            produtoAdd.parcelamentomaximo = produto.parcelamentoMaximo;
                            produtoAdd.parcelamentomaximovalorparcela =
                                Convert.ToDouble(produto.parcelamentoMaximoValorParcela);

                            var listaCategoriaNomes = new List<string>();
                            var listaCategoriaIds = new List<int>();

                            int categoriasCount = categorias.Count();
                            for (int i = 0; i < categoriasCount; i++)
                            {
                                listaCategoriaNomes.Add(categorias[i].tbProdutoCategoria.categoriaNomeExibicao);
                                listaCategoriaIds.Add(categorias[i].tbProdutoCategoria.categoriaId);
                                termoBusca += " " + categorias[i].tbProdutoCategoria.categoriaNomeExibicao;
                            }
                            produtoAdd.filtroids = listaCategoriaIds;
                            produtoAdd.filtronomes = listaCategoriaNomes;

                            var listaColecaoNomes = new List<string>();
                            var listaColecaoIds = new List<int>();
                            var colecoes =
                                (from c in data.tbJuncaoProdutoColecao where c.produtoId == produto.produtoId select c)
                                    .ToList();
                            int colecoesCount = colecoes.Count();
                            if (colecoesCount > 0)
                            {
                                for (int i = 0; i < colecoesCount; i++)
                                {
                                    termoBusca += " " + colecoes[i].tbColecao.colecaoNome;
                                    listaColecaoNomes.Add(colecoes[i].tbColecao.colecaoNome);
                                    listaColecaoIds.Add(colecoes[i].tbColecao.colecaoId);
                                }
                            }
                            produtoAdd.colecaoids = listaColecaoIds;
                            produtoAdd.colecaonomes = listaColecaoNomes;
                            //produtoAdd.termoBusca = termoBusca;
                            produtoAdd.estoquereal = (produto.estoqueReal ?? 0);
                            produtoAdd.relevanciamanual = produto.relevanciaManual;
                            produtoAdd.relevanciafornecedor = produto.tbProdutoFornecedor.relevanciaProdutos;
                            produtoAdd.siteid = produto.siteId;
                            if (string.IsNullOrEmpty(produtoAdd.fotodestaque)) produtoAdd.fotodestaque = "semfoto";

                        }
                        else
                        {
                            ExcluirProduto(produtoId);
                        }

                    }
                    catch (Exception ex)
                    {
                        rnEmails.EnviaEmail("", "andre@bark.com.br", "", "", "", ex.ToString(), "Erro Cloudsearch");
                    }

                    //var combos = (from c in data.tbProdutoRelacionado where c.idProdutoFilho == produtoId select c.idProdutoPai).Distinct().ToList();
                    //foreach (var combo in combos)
                    //{
                    //    AtualizarProduto(combo);
                    //}
                }
                else
                {
                    ExcluirProduto(produtoId);
                }
                //var update = cloudSearch.Update(produtoAdd);
                try
                {
                    var retorno = cloudSearch.Add(produtoAdd);
                    return retorno;
                }
                catch (Exception ex)
                {
                    //rnEmails.EnviaEmail("", "andre@bark.com.br", "", "", "", ex.ToString(), "Erro Cloudsearch");
                }
                //rnEmails.EnviaEmail("", "andre@bark.com.br", "", "", "", retorno.errors.First().message, "Erro busca");
            }
            catch (Exception ex)
            {
                //rnEmails.EnviaEmail("", "andre@bark.com.br", "", "", "", ex.InnerException.ToString(), "Erro Cloudsearch");
            }
            return null;
        }
        public static AmazingCloudSearch.Contract.Result.DeleteResult ExcluirProduto(int produtoId)
        {
            if (rnConfiguracoes.AtualizarCloudSearch == false) return null;
            var cloudSearch = new CloudSearch<rnBuscaCloudSearch.produtoBusca>(ConfigurationManager.AppSettings["cloudSearchApiKey"], apiVersion);
            var produtoAdd = new rnBuscaCloudSearch.produtoBusca();
            produtoAdd.Id = produtoId.ToString();
            return cloudSearch.Delete(produtoAdd);
            return null;
        }
        public static AmazingCloudSearch.Contract.Result.DeleteResult RemoveProduto(int produtoId)
        {
            if (rnConfiguracoes.AtualizarCloudSearch == false) return null;
            var cloudSearch = new CloudSearch<rnBuscaCloudSearch.produtoBusca>(ConfigurationManager.AppSettings["cloudSearchApiKey"], apiVersion);

            var produtoRemove = new rnBuscaCloudSearch.produtoBusca();
            produtoRemove.Id = produtoId.ToString();

            return cloudSearch.Delete(produtoRemove);
        }



        public class retornoParcelamento
        {
            public int parcelas { get; set; }
            public decimal valor { get; set; }

        }

        public static retornoParcelamento calculaParcelamento(decimal valor)
        {
            //var data = new dbSiteEntities();
            //var dvCondicoesDePagamento = (from c in data.tbCondicoesDePagamento where c.ativo.ToLower() == "true" select c).ToList();

            //string mensagemDesconto = "";

            //var condicao = dvCondicoesDePagamento.First(x => x.destaque.ToLower() == "true");
            int parcelasSemJuros = 12;
            //double taxaDeJuros = condicao.taxaDeJuros / 100;
            decimal valorMinimoDaParcela = 5;
            //decimal porcentagemDeDesconto = Convert.ToDecimal(condicao.porcentagemDeDesconto);
            int numeroMaximoDeParcelas = 12;
            decimal valorDaParcela = 0;

            decimal produtoPreco = valor;


            var retornoParcelamento = new retornoParcelamento();
            retornoParcelamento.parcelas = 0;
            retornoParcelamento.valor = 0;

            for (int i = numeroMaximoDeParcelas; i <= numeroMaximoDeParcelas && i > 0; i--)
            {
                valorDaParcela = produtoPreco / i;

                if (valorDaParcela >= valorMinimoDaParcela)
                {
                    if (i > 1)
                    {
                        if (i <= parcelasSemJuros)
                        {
                            retornoParcelamento.parcelas = i;
                            retornoParcelamento.valor = decimal.Parse(valorDaParcela.ToString());
                        }
                        break;
                    }
                }
            }

            return retornoParcelamento;
        }
        //public static retornoParcelamento calculaParcelamento(decimal valor)
        //{
        //    var data = new dbSiteEntities();
        //    var dvCondicoesDePagamento = (from c in data.tbCondicoesDePagamento where c.ativo.ToLower() == "true" select c).ToList();

        //    string mensagemDesconto = "";

        //    var condicao = dvCondicoesDePagamento.First(x => x.destaque.ToLower() == "true");
        //    int parcelasSemJuros = condicao.parcelasSemJuros;
        //    double taxaDeJuros = condicao.taxaDeJuros / 100;
        //    decimal valorMinimoDaParcela = condicao.valorMinimoDaParcela;
        //    decimal porcentagemDeDesconto = Convert.ToDecimal(condicao.porcentagemDeDesconto);
        //    int numeroMaximoDeParcelas = condicao.numeroMaximoDeParcelas;
        //    decimal valorDaParcela = 0;

        //    decimal produtoPreco = valor;


        //    var retornoParcelamento = new retornoParcelamento();
        //    retornoParcelamento.parcelas = 0;
        //    retornoParcelamento.valor = 0;

        //    for (int i = numeroMaximoDeParcelas; i <= numeroMaximoDeParcelas && i > 0; i--)
        //    {
        //        valorDaParcela = produtoPreco / i;

        //        if (valorDaParcela >= valorMinimoDaParcela)
        //        {
        //            if (i > 1)
        //            {
        //                if (i <= parcelasSemJuros)
        //                {
        //                    retornoParcelamento.parcelas = i;
        //                    retornoParcelamento.valor = decimal.Parse(valorDaParcela.ToString());
        //                }
        //                break;
        //            }
        //        }
        //    }

        //    return retornoParcelamento;
        //}

        private static decimal CalculaDesconto(decimal produtoPreco, decimal produtoPrecoPromocional)
        {
            if (produtoPreco == 0) return 0;
            decimal precoAtivo = produtoPrecoPromocional == 0 ? produtoPreco : produtoPrecoPromocional;
            decimal desconto = 100 - ((precoAtivo * 100) / produtoPreco);
            return desconto;
        }
        private static decimal CalculaDescontoAVista(decimal produtoPreco, decimal produtoPrecoPromocional)
        {
            if (produtoPreco == 0) return 0;
            decimal precoAtivo = produtoPrecoPromocional == 0 ? produtoPreco : produtoPrecoPromocional;
            decimal precoComDesconto = precoAtivo - ((precoAtivo / 100) * 15);
            decimal desconto = 100 - ((precoComDesconto * 100) / produtoPreco);
            return desconto;
        }


        public static int CalculaPrazoMaximoProdutoFornecedor(int produtoId)
        {
            var data = new dbSiteEntities();
            var combo = (from c in data.tbProdutoRelacionado
                         where c.idProdutoPai == produtoId
                         select new
                         {
                             prazo = c.tbProdutos.tbProdutoFornecedor.fornecedorPrazoDeFabricacao ?? 1,
                             c.tbProdutos.tbProdutoFornecedor.dataFimFabricacao,
                             c.tbProdutos.tbProdutoFornecedor.dataInicioFabricacao
                         }).ToList();
            if (combo.Any())
            {
                if (combo.Any(
                        x =>
                            x.dataFimFabricacao != null && x.dataFimFabricacao < DateTime.Now &&
                            x.dataInicioFabricacao != null && x.dataInicioFabricacao > DateTime.Now))
                {
                    int prazoMaximo = 0;
                    foreach (var item in combo)
                    {
                        if (item.dataFimFabricacao != null && item.dataFimFabricacao < DateTime.Now &&
                            item.dataInicioFabricacao != null && item.dataInicioFabricacao > DateTime.Now)
                        {
                            int prazo = rnFuncoes.retornaDiasUteisPeriodo(DateTime.Now, (DateTime)item.dataInicioFabricacao) +
                                        item.prazo;
                            if (prazo > prazoMaximo) prazoMaximo = prazo;
                        }
                    }
                    return prazoMaximo;
                }
                else
                {
                    return combo.OrderByDescending(x => x.prazo).First().prazo;
                }
            }

            var prazosProduto = (from c in data.tbProdutos
                                 where c.produtoId == produtoId
                                 select new
                                 {
                                     prazo = c.tbProdutoFornecedor.fornecedorPrazoDeFabricacao ?? 1,
                                     c.tbProdutoFornecedor.dataFimFabricacao,
                                     c.tbProdutoFornecedor.dataInicioFabricacao
                                 }).First();
            if (prazosProduto.dataFimFabricacao != null && prazosProduto.dataFimFabricacao < DateTime.Now &&
                prazosProduto.dataInicioFabricacao != null && prazosProduto.dataInicioFabricacao > DateTime.Now)
            {
                int prazo = rnFuncoes.retornaDiasUteisPeriodo(DateTime.Now, (DateTime)prazosProduto.dataInicioFabricacao) + prazosProduto.prazo;
                return prazo;
            }
            return prazosProduto.prazo;
        }*/
    }
}
