//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace bcServerService
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbProdutoMateriaPrima
    {
        public int idProdutoMateriaPrima { get; set; }
        public int produtoId { get; set; }
        public int idComprasProduto { get; set; }
        public decimal consumo { get; set; }
        public Nullable<int> largura { get; set; }
        public Nullable<int> comprimento { get; set; }
        public string observacao { get; set; }
    
        public virtual tbComprasProduto tbComprasProduto { get; set; }
        public virtual tbProdutos tbProdutos { get; set; }
    }
}
