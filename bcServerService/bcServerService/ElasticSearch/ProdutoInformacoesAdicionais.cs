﻿using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bcServerService.ElasticSearch
{
    public class ProdutoInformacoesAdicionais
    {
        public int informacaoAdicionalId { get; set; }
        [Keyword]
        public string nome { get; set; }
        [Keyword]
        public string conteudo { get; set; }
    }
}
