﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nest;

namespace bcServerService.ElasticSearch
{
    public class ProdutoAtributos
    {
        [Keyword]
        public string chave { get; set; }
        [Keyword]
        public string valor { get; set; }
    }
}
