﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bcServerService.ElasticSearch
{
    public class EspecificacaoItem
    {
        public string nome { get; set; }
        public string estilo { get; set; }
        public string produtoUrl { get; set; }
        public string foto { get; set; }
        public int produtoId { get; set; }
        public int ordem { get; set; }
    }
}
