﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nest;
using System.Transactions;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;
using System.ComponentModel;

namespace bcServerService.ElasticSearch
{
    public sealed class ElasticSearchBannerRepository
    {


        private static readonly ElasticSearchBannerRepository instance = new ElasticSearchBannerRepository();


        ElasticClient client;
        ElasticClient clientAmazon;
        ElasticClient clientAmazon2;
        string connectionString = ConfigurationManager.AppSettings["elasticSearchUri"];
        string connectionStringAmazon = ConfigurationManager.AppSettings["elasticSearchUriAmazon"];
        string connectionStringAmazon2 = ConfigurationManager.AppSettings["elasticSearchUriAmazon2"];

        private ElasticSearchBannerRepository()
        {
            var node = new Uri(connectionString);
            var nodeAmazon = new Uri(connectionStringAmazon);
            var nodeAmazon2 = new Uri(connectionStringAmazon2);
            var settings = new ConnectionSettings(node).RequestTimeout(new TimeSpan(0, 1, 0));
            var settingsAmazon = new ConnectionSettings(nodeAmazon).RequestTimeout(new TimeSpan(0, 1, 0));
            var settingsAmazon2 = new ConnectionSettings(nodeAmazon2).RequestTimeout(new TimeSpan(0, 1, 0));
            settings.BasicAuthentication(ConfigurationManager.AppSettings["elasticSearchUser"], ConfigurationManager.AppSettings["elasticSearchPass"]);
            client = new ElasticClient(settings);
            clientAmazon = new ElasticClient(settingsAmazon);
            clientAmazon2 = new ElasticClient(settingsAmazon2);
        }

        static ElasticSearchBannerRepository()
        {
        }

        public static ElasticSearchBannerRepository Instance
        {
            get
            {
                return instance;
            }
        }
        public void AtualizarBannerElasticSearch(int idBanner)
        {
            try
            {                
                using (var data = new dbSiteEntities())
                {
                    var banner = (from c in data.tbBanners where c.Id == idBanner && c.ativo == true select c).FirstOrDefault();
                    if (banner != null)
                    {
                        var filtros = (from c in data.tbBannersFiltro where c.bannerId == idBanner select (c.filtro ?? 0)).Where(x => x > 0).ToList();
                        var url = new Banner();
                        url.id = idBanner;
                        url.LinkDoProduto = banner.linkProduto;
                        url.IdDoProduto = banner.idProduto;
                        url.Foto = banner.foto;
                        url.Local = (banner.local ?? 1);
                        url.Posicao = (banner.posicao ?? 1);
                        url.Relevancia = (banner.relevancia ?? 1) * (-1);
                        url.Mobile = (banner.mobile ?? false);
                        url.InicioDoTimer = banner.inicioContador;
                        url.FimDoTimer = banner.fimContador;
                        url.filtros = filtros;

                        int idEstilo = 0;
                        if(banner.estilo != null)
                        {
                            idEstilo = Convert.ToInt32(banner.estilo);
                            var estilo = (from c in data.tbEstilos where c.id == idEstilo select c).FirstOrDefault();
                            if(estilo != null)
                            {
                                url.EstiloDoTimer = estilo.estilo;
                            }
                        }

                        var responseAmazon = clientAmazon.Index(url, idx => idx.Index("banner"));
                        var responseAmazon2 = clientAmazon2.Index(url, idx => idx.Index("banner"));
                        //var response = client.Index(url, idx => idx.Index("banner"));


                        var awsCloudFrontService = new CloudFront.AWSCloudFrontService();
                        var paths = new List<string>();
                        if(url.Local == 4)
                        {
                            var categorias = (from c in data.tbProdutoCategoria where c.categoriaPaiId == 0 && c.exibirSite == true select c).ToList();
                            foreach(var categoria in categorias)
                            {
                                paths.Add("/" + categoria.categoriaUrl);
                            }
                        }
                        else
                        {
                            paths.Add("/");
                        }
                        var queue = new tbQueue();
                        queue.tipoQueue = 40;
                        queue.agendamento = DateTime.Now;
                        queue.mensagem = "";
                        queue.andamento = false;
                        queue.concluido = false;
                        data.tbQueue.Add(queue);
                        data.SaveChanges();

                        //awsCloudFrontService.CreateInvalidation(paths);
                    }
                    else
                    {
                        ExcluirBanner(idBanner);
                    }
                }                
            }
            catch (Exception ex)
            {
                rnEmails.EnviaEmail("", "andre@bark.com.br", "", "", "", ex.ToString(), "Exception atualizacao elastic banner " + idBanner.ToString());
            }
        }


        public void ExcluirBanner(int idBanner)
        {
            if (rnConfiguracoes.AtualizarElasticSearch == false) return;
            //var response = client.Delete<Banner>(idBanner, d => d.Index("banner"));
            var responsee = clientAmazon.Delete<Banner>(idBanner, d => d.Index("banner"));
            var responsee2 = clientAmazon2.Delete<Banner>(idBanner, d => d.Index("banner"));
        }

        public void GerarIndice()
        {
            var descriptor = new CreateIndexDescriptor("banner").Mappings(x => x.Map<Banner>(m => m.AutoMap()));
            //var res1 = client.DeleteIndex("banner");
            var res = clientAmazon2.CreateIndex(descriptor);
        }

        public void AtualizarBannerQueue(long? idQueuee)
        {
            var dataInicio = DateTime.Now;

            #region atualizaQueue
            var data = new dbSiteEntities();
            int idQueue = Convert.ToInt32(idQueuee);
            var queue = (from c in data.tbQueue where c.idQueue == idQueue select c).First();
            queue.andamento = true;
            queue.dataInicio = DateTime.Now;
            data.SaveChanges();
            #endregion atualizaQueue

            int idBanner = (int)queue.idRelacionado;


            var elastic = ElasticSearch.ElasticSearchBannerRepository.Instance;
            elastic.AtualizarBannerElasticSearch(idBanner);

            
            queue.concluido = true;
            queue.andamento = true;
            queue.dataConclusao = DateTime.Now;
            data.SaveChanges();


            var queuesRelacionados = (from c in data.tbQueue
                                      where
                                          c.tipoQueue == queue.tipoQueue && c.idRelacionado == queue.idRelacionado &&
                                          c.agendamento < dataInicio && c.andamento == false && c.concluido == false
                                      select c).ToList();
            foreach (var queueRelacionado in queuesRelacionados)
            {
                queueRelacionado.dataInicio = dataInicio;
                queueRelacionado.andamento = true;
                queueRelacionado.concluido = true;
                queueRelacionado.dataConclusao = queue.dataConclusao;
            }
            data.SaveChanges();
        }

        private BackgroundWorker bwAtualizarBanner = new BackgroundWorker();
        private void bwAtualizarBanner_DoWork(object sender, DoWorkEventArgs e)
        {
            var dataInicio = DateTime.Now;

            #region atualizaQueue
            var data = new dbSiteEntities();
            int idQueue = Convert.ToInt32(e.Argument);
            var queue = (from c in data.tbQueue where c.idQueue == idQueue select c).First();
            queue.andamento = true;
            queue.dataInicio = DateTime.Now;
            data.SaveChanges();
            #endregion atualizaQueue

            int idBanner = (int)queue.idRelacionado;


            var elastic = ElasticSearch.ElasticSearchBannerRepository.Instance;
            elastic.AtualizarBannerElasticSearch(idBanner);


            #region finalizaQueue
            queue.concluido = true;
            queue.andamento = true;
            queue.dataConclusao = DateTime.Now;
            data.SaveChanges();


            var queuesRelacionados = (from c in data.tbQueue
                                      where
                                          c.tipoQueue == queue.tipoQueue && c.idRelacionado == queue.idRelacionado &&
                                          c.agendamento < dataInicio && c.andamento == false && c.concluido == false
                                      select c).ToList();
            foreach (var queueRelacionado in queuesRelacionados)
            {
                queueRelacionado.dataInicio = dataInicio;
                queueRelacionado.andamento = true;
                queueRelacionado.concluido = true;
                queueRelacionado.dataConclusao = queue.dataConclusao;
            }
            data.SaveChanges();
            #endregion

        }

    }
}
