﻿using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bcServerService.ElasticSearch
{
    [ElasticsearchType(Name = "produto", IdProperty = "id")]
    public class Produto
    {
        public int id { get; set; }
        [Text(Analyzer = "brazilian")]
        public string categoria { get; set; }

        [Keyword]
        public string categoriaurl { get; set; }

        public List<int> colecaoids { get; set; }

        public List<string> colecaonomes { get; set; }

        public List<int> filtroids { get; set; }

        public List<string> filtronomes { get; set; }
        public string fotodestaque { get; set; }
        public double preco { get; set; }

        public int brindes { get; set; }

        public bool exclusivo { get; set; }

        public bool lancamento { get; set; }

        [Text(Analyzer = "brazilian")]
        public string nome { get; set; }

        public int pecas { get; set; }

        public double produtopreco { get; set; }

        public double precopromocional { get; set; }
        public double precoDeCusto { get; set; }

        public bool promocao { get; set; }

        [Keyword]
        public string produtourl { get; set; }

        public int relevancia { get; set; }

        [Text(Analyzer = "brazilian")]
        public string categoriatags { get; set; }

        [Text(Analyzer = "brazilian")]
        public string tags { get; set; }

        public bool ativo { get; set; }
        public int parcelamentomaximo { get; set; }
        public double parcelamentomaximovalorparcela { get; set; }
        public int relevanciafornecedor { get; set; }
        public int relevanciamanual { get; set; }
        public int prazopedidos { get; set; }
        public double desconto { get; set; }
        public double descontototalavista { get; set; }
        public int estoquevirtual { get; set; }
        public int estoquereal { get; set; }
        public int estoqueminimo { get; set; }
        public List<ProdutoFoto> fotos { get; set; }
        public string produtoDescricao { get; set; }
        public string colecaoNome { get; set; }
        public bool freteGratis { get; set; }
        public bool prontaEntrega { get; set; }
        public bool foraDeLinha { get; set; }
        public List<ProdutoInformacoesAdicionais> informacoesAdicionais { get; set; } 
        public string produtoIdDaEmpresa { get; set; }
        public double produtoPeso { get; set; }
        public List<ProdutoVideo> videos { get; set; }
        public bool descontoProgressivoFrete { get; set; }
        public int descontoProgressivoFretePorcentagem { get; set; }
        public List<ProdutoAtributos> atributos { get; set; }

        [Keyword]
        public string nomeraw { get; set; }
        public List<CompreJunto> compreJunto { get; set; }
        public string nometitle { get; set; }
        public string foto360 { get; set; }
        public string foto360Mini { get; set; }
        public List<Especificacao> Especificacoes { get; set; }
        public bool ocultarLista { get; set; }
        public bool ocultarBusca { get; set; }

        public string term1 { get; set; }
        public string term2 { get; set; }
        public string term3 { get; set; }
        public string term4 { get; set; }
        public string term5 { get; set; }
    }
}
