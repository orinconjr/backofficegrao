﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nest;
using System.Transactions;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;
using System.ComponentModel;

namespace bcServerService.ElasticSearch
{
    public sealed class ElasticSearchCondicaoDePagamentoRepository
    {
        private static readonly ElasticSearchCondicaoDePagamentoRepository instance = new ElasticSearchCondicaoDePagamentoRepository();


        ElasticClient client;
        ElasticClient clientAmazon;
        ElasticClient clientAmazon2;
        string connectionString = ConfigurationManager.AppSettings["elasticSearchUri"];
        string connectionStringAmazon = ConfigurationManager.AppSettings["elasticSearchUriAmazon"];
        string connectionStringAmazon2 = ConfigurationManager.AppSettings["elasticSearchUriAmazon2"];

        private ElasticSearchCondicaoDePagamentoRepository()
        {
            var node = new Uri(connectionString);
            var nodeAmazon = new Uri(connectionStringAmazon);
            var nodeAmazon2 = new Uri(connectionStringAmazon2);
            var settings = new ConnectionSettings(node).RequestTimeout(new TimeSpan(0, 1, 0));
            var settingsAmazon = new ConnectionSettings(nodeAmazon).RequestTimeout(new TimeSpan(0, 1, 0));
            var settingsAmazon2 = new ConnectionSettings(nodeAmazon2).RequestTimeout(new TimeSpan(0, 1, 0));
            settings.BasicAuthentication(ConfigurationManager.AppSettings["elasticSearchUser"], ConfigurationManager.AppSettings["elasticSearchPass"]);
            client = new ElasticClient(settings);
            clientAmazon = new ElasticClient(settingsAmazon);
            clientAmazon2 = new ElasticClient(settingsAmazon2);
        }

        static ElasticSearchCondicaoDePagamentoRepository()
        {
        }

        public static ElasticSearchCondicaoDePagamentoRepository Instance
        {
            get
            {
                return instance;
            }
        }
        public void AtualizarCondicaoDePagamentoElasticSearch(int idCondicaoDePagamento)
        {
            try
            {
                using (new TransactionScope(TransactionScopeOption.Required, new TransactionOptions
                {
                    IsolationLevel = IsolationLevel.ReadUncommitted
                }))
                {
                    using (var data = new dbSiteEntities())
                    {
                        var condicaoDePagamento = (from c in data.tbCondicoesDePagamento where c.condicaoId == idCondicaoDePagamento select c).FirstOrDefault();
                        if (condicaoDePagamento != null)
                        {
                            var condicao = new CondicaoDePagamentoElastic();
                            condicao.id = idCondicaoDePagamento;
                            condicao.tipoDePagamentoId = condicaoDePagamento.tipoDePagamentoId;
                            condicao.nome = condicaoDePagamento.condicaoNome;
                            condicao.numeroMaximoDeParcelas = condicaoDePagamento.numeroMaximoDeParcelas;
                            condicao.parcelasSemJuros = condicaoDePagamento.parcelasSemJuros;
                            condicao.taxaDeJuros = condicaoDePagamento.taxaDeJuros;
                            condicao.valorMinimoDaParcela = condicaoDePagamento.valorMinimoDaParcela;
                            condicao.porcentagemDeDesconto = condicaoDePagamento.porcentagemDeDesconto;
                            condicao.imagem = condicaoDePagamento.imagemSite;
                            condicao.imagemMini = condicaoDePagamento.imagemMini;
                            condicao.enviarParaOGateway = condicaoDePagamento.enviarGateway;
                            condicao.enviarParaAnaliseDeRisco = condicaoDePagamento.enviarAnaliseRisco;
                            condicao.valorMinimoAnaliseRisco = condicaoDePagamento.valorMinimoAnaliseRisco;
                            condicao.idGatewayContrato = condicaoDePagamento.idGatewayContrato;
                            condicao.idCodigoAdquirenteGateway = condicaoDePagamento.idCodigoAdquirenteGateway;
                            condicao.destaque = (condicaoDePagamento.destaque ?? "").ToLower() == "true";
                            condicao.ativo = (condicaoDePagamento.ativo ?? "").ToLower() == "true";
                            condicao.idContaPagamento = condicaoDePagamento.idContaPagamento;
                            condicao.ordem = condicaoDePagamento.ordem;
                            
                            var response = client.Index(condicao, idx => idx.Index("condicaodepagamento"));
                            var responseAmazon = clientAmazon.Index(condicao, idx => idx.Index("condicaodepagamento"));
                            var responseAmazon2 = clientAmazon2.Index(condicao, idx => idx.Index("condicaodepagamento"));
                        }
                        else
                        {
                            ExcluirCondicao(idCondicaoDePagamento);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                rnEmails.EnviaEmail("", "andre@bark.com.br", "", "", "", ex.ToString(), "Exception atualizacao elastic Condicao de Pagamento " + idCondicaoDePagamento.ToString());
            }
        }


        public void ExcluirCondicao(int idCondicaoDePagamento)
        {
            if (rnConfiguracoes.AtualizarElasticSearch == false) return;
            var response = client.Delete<CondicaoDePagamentoElastic>(idCondicaoDePagamento, d => d.Index("condicaodepagamento"));
            var responseAmazon = clientAmazon.Delete<CondicaoDePagamentoElastic>(idCondicaoDePagamento, d => d.Index("condicaodepagamento"));
            var responseAmazon2 = clientAmazon2.Delete<CondicaoDePagamentoElastic>(idCondicaoDePagamento, d => d.Index("condicaodepagamento"));
        }

        public void GerarIndice()
        {
            var descriptor = new CreateIndexDescriptor("condicaodepagamento").Mappings(x => x.Map<CondicaoDePagamentoElastic>(m => m.AutoMap()));
            //var res1 = clientAmazon.DeleteIndex("condicaodepagamento");
            var res = clientAmazon2.CreateIndex(descriptor);
        }

        public void AtualizarCondicaoDePagamentoQueue(long? idQueue)
        {
            if (bwAtualizarCondicaoDePagamento.IsBusy != true)
            {
                bwAtualizarCondicaoDePagamento.DoWork += new DoWorkEventHandler(bwAtualizarCondicaoDePagamento_DoWork);
                bwAtualizarCondicaoDePagamento.RunWorkerAsync(idQueue);
            }
        }

        private BackgroundWorker bwAtualizarCondicaoDePagamento = new BackgroundWorker();
        private void bwAtualizarCondicaoDePagamento_DoWork(object sender, DoWorkEventArgs e)
        {
            var dataInicio = DateTime.Now;

            #region atualizaQueue
            var data = new dbSiteEntities();
            int idQueue = Convert.ToInt32(e.Argument);
            var queue = (from c in data.tbQueue where c.idQueue == idQueue select c).First();
            queue.andamento = true;
            queue.dataInicio = DateTime.Now;
            data.SaveChanges();
            #endregion atualizaQueue

            int idCondicaoDePagamento = (int)queue.idRelacionado;


            var elastic = ElasticSearch.ElasticSearchCondicaoDePagamentoRepository.Instance;
            elastic.AtualizarCondicaoDePagamentoElasticSearch(idCondicaoDePagamento);


            #region finalizaQueue
            queue.concluido = true;
            queue.andamento = true;
            queue.dataConclusao = DateTime.Now;
            data.SaveChanges();


            var queuesRelacionados = (from c in data.tbQueue
                                      where
                                          c.tipoQueue == queue.tipoQueue && c.idRelacionado == queue.idRelacionado &&
                                          c.agendamento < dataInicio && c.andamento == false && c.concluido == false
                                      select c).ToList();
            foreach (var queueRelacionado in queuesRelacionados)
            {
                queueRelacionado.dataInicio = dataInicio;
                queueRelacionado.andamento = true;
                queueRelacionado.concluido = true;
                queueRelacionado.dataConclusao = queue.dataConclusao;
            }
            data.SaveChanges();
            #endregion

        }

    }
}
