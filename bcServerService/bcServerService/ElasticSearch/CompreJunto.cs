﻿using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bcServerService.ElasticSearch
{
    public class CompreJunto
    {
        public int ID { get; set; }
        [Keyword]
        public string FrasePersonalizada { get; set; }
        public List<ProdutoDaCampanha> ProdutosDaCampanha { get; set; }
        public DateTime InicioDaVigencia { get; set; }
        public DateTime FimDaVigencia { get; set; }
        public DateTime? InicioDoTimer { get; set; }
        public DateTime? FimDoTimer { get; set; }
        public int Prioridade { get; set; }
    }
}
