﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nest;
using System.Transactions;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;
using System.ComponentModel;

namespace bcServerService.ElasticSearch
{
    public sealed class ElasticSearchColecaoRepository
    {


        private static readonly ElasticSearchColecaoRepository instance = new ElasticSearchColecaoRepository();


        ElasticClient client;
        ElasticClient clientAmazon;
        ElasticClient clientAmazon2;
        string connectionString = ConfigurationManager.AppSettings["elasticSearchUri"];
        string connectionStringAmazon = ConfigurationManager.AppSettings["elasticSearchUriAmazon"];
        string connectionStringAmazon2 = ConfigurationManager.AppSettings["elasticSearchUriAmazon2"];

        private ElasticSearchColecaoRepository()
        {
            var node = new Uri(connectionString);
            var nodeAmazon = new Uri(connectionStringAmazon);
            var nodeAmazon2 = new Uri(connectionStringAmazon2);
            var settings = new ConnectionSettings(node).RequestTimeout(new TimeSpan(0, 1, 0));
            var settingsAmazon = new ConnectionSettings(nodeAmazon).RequestTimeout(new TimeSpan(0, 1, 0));
            var settingsAmazon2 = new ConnectionSettings(nodeAmazon2).RequestTimeout(new TimeSpan(0, 1, 0));
            settings.BasicAuthentication(ConfigurationManager.AppSettings["elasticSearchUser"], ConfigurationManager.AppSettings["elasticSearchPass"]);
            client = new ElasticClient(settings);
            clientAmazon = new ElasticClient(settingsAmazon);
            clientAmazon2 = new ElasticClient(settingsAmazon2);
        }

        static ElasticSearchColecaoRepository()
        {
        }

        public static ElasticSearchColecaoRepository Instance
        {
            get
            {
                return instance;
            }
        }
        public void AtualizarColecaoElasticSearch(int idColecao)
        {
            try
            {
                using (var data = new dbSiteEntities())
                {
                    var colecao = (from c in data.tbColecao where c.colecaoId == idColecao && !string.IsNullOrEmpty(c.colecaoNomeSite) select c).FirstOrDefault();
                    if (colecao != null)
                    {
                        var colecaoCadastro = new Colecao();
                        colecaoCadastro.id = idColecao;
                        colecaoCadastro.url = colecao.colecaoUrl;
                        colecaoCadastro.nome = colecao.colecaoNomeSite;

                        var responseAmazon = clientAmazon.Index(colecaoCadastro, idx => idx.Index("colecao"));
                        var responseAmazon2 = clientAmazon2.Index(colecaoCadastro, idx => idx.Index("colecao"));
                        //awsCloudFrontService.CreateInvalidation(paths);
                    }
                    else
                    {
                        ExcluirColecao(idColecao);
                    }
                }
            }
            catch (Exception ex)
            {
                rnEmails.EnviaEmail("", "andre@bark.com.br", "", "", "", ex.ToString(), "Exception atualizacao elastic colecao " + idColecao.ToString());
            }
        }


        public void ExcluirColecao(int idColecao)
        {
            if (rnConfiguracoes.AtualizarElasticSearch == false) return;
            //var response = client.Delete<Colecao>(idColecao, d => d.Index("colecao"));
            var responsee = clientAmazon.Delete<Colecao>(idColecao, d => d.Index("colecao"));
            var responsee2 = clientAmazon2.Delete<Colecao>(idColecao, d => d.Index("colecao"));
        }

        public void GerarIndice()
        {
            var descriptor = new CreateIndexDescriptor("colecao").Mappings(x => x.Map<Colecao>(m => m.AutoMap()));
            //var res1 = client.DeleteIndex("colecao");
            //var res = clientAmazon.CreateIndex(descriptor);
            var res2 = clientAmazon2.CreateIndex(descriptor);
        }

        public void AtualizarColecaoQueue(long? idQueuee)
        {
            var dataInicio = DateTime.Now;

            #region atualizaQueue
            var data = new dbSiteEntities();
            int idQueue = Convert.ToInt32(idQueuee);
            var queue = (from c in data.tbQueue where c.idQueue == idQueue select c).First();
            queue.andamento = true;
            queue.dataInicio = DateTime.Now;
            data.SaveChanges();
            #endregion atualizaQueue

            int idColecao = (int)queue.idRelacionado;


            var elastic = ElasticSearch.ElasticSearchColecaoRepository.Instance;
            elastic.AtualizarColecaoElasticSearch(idColecao);


            queue.concluido = true;
            queue.andamento = true;
            queue.dataConclusao = DateTime.Now;
            data.SaveChanges();


            var queuesRelacionados = (from c in data.tbQueue
                                      where
                                          c.tipoQueue == queue.tipoQueue && c.idRelacionado == queue.idRelacionado &&
                                          c.agendamento < dataInicio && c.andamento == false && c.concluido == false
                                      select c).ToList();
            foreach (var queueRelacionado in queuesRelacionados)
            {
                queueRelacionado.dataInicio = dataInicio;
                queueRelacionado.andamento = true;
                queueRelacionado.concluido = true;
                queueRelacionado.dataConclusao = queue.dataConclusao;
            }
            data.SaveChanges();
        }

        private BackgroundWorker bwAtualizarColecao = new BackgroundWorker();
        private void bwAtualizarColecao_DoWork(object sender, DoWorkEventArgs e)
        {
            var dataInicio = DateTime.Now;

            #region atualizaQueue
            var data = new dbSiteEntities();
            int idQueue = Convert.ToInt32(e.Argument);
            var queue = (from c in data.tbQueue where c.idQueue == idQueue select c).First();
            queue.andamento = true;
            queue.dataInicio = DateTime.Now;
            data.SaveChanges();
            #endregion atualizaQueue

            int idColecao = (int)queue.idRelacionado;


            var elastic = ElasticSearch.ElasticSearchColecaoRepository.Instance;
            elastic.AtualizarColecaoElasticSearch(idColecao);


            #region finalizaQueue
            queue.concluido = true;
            queue.andamento = true;
            queue.dataConclusao = DateTime.Now;
            data.SaveChanges();


            var queuesRelacionados = (from c in data.tbQueue
                                      where
                                          c.tipoQueue == queue.tipoQueue && c.idRelacionado == queue.idRelacionado &&
                                          c.agendamento < dataInicio && c.andamento == false && c.concluido == false
                                      select c).ToList();
            foreach (var queueRelacionado in queuesRelacionados)
            {
                queueRelacionado.dataInicio = dataInicio;
                queueRelacionado.andamento = true;
                queueRelacionado.concluido = true;
                queueRelacionado.dataConclusao = queue.dataConclusao;
            }
            data.SaveChanges();
            #endregion

        }

    }
}
