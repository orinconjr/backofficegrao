﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nest;
using System.Transactions;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;
using System.ComponentModel;

namespace bcServerService.ElasticSearch
{
    public sealed class ElasticSearchSeoUrlRepository
    {



        private static readonly ElasticSearchSeoUrlRepository instance = new ElasticSearchSeoUrlRepository();


        ElasticClient client;
        ElasticClient clientAmazon;
        ElasticClient clientAmazon2;
        string connectionString = ConfigurationManager.AppSettings["elasticSearchUri"];
        string connectionStringAmazon = ConfigurationManager.AppSettings["elasticSearchUriAmazon"];
        string connectionStringAmazon2 = ConfigurationManager.AppSettings["elasticSearchUriAmazon2"];

        private ElasticSearchSeoUrlRepository()
        {
            var node = new Uri(connectionString);
            var nodeAmazon = new Uri(connectionStringAmazon);
            var nodeAmazon2 = new Uri(connectionStringAmazon2);
            var settings = new ConnectionSettings(node).RequestTimeout(new TimeSpan(0, 1, 0));
            var settingsAmazon = new ConnectionSettings(nodeAmazon).RequestTimeout(new TimeSpan(0, 1, 0));
            var settingsAmazon2 = new ConnectionSettings(nodeAmazon2).RequestTimeout(new TimeSpan(0, 1, 0));
            settings.BasicAuthentication(ConfigurationManager.AppSettings["elasticSearchUser"], ConfigurationManager.AppSettings["elasticSearchPass"]);
            client = new ElasticClient(settings);
            clientAmazon = new ElasticClient(settingsAmazon);
            clientAmazon2 = new ElasticClient(settingsAmazon2);
        }

        static ElasticSearchSeoUrlRepository()
        {
        }

        public static ElasticSearchSeoUrlRepository Instance
        {
            get
            {
                return instance;
            }
        }
        private void AtualizarSeoUrlElasticSearch(int idSeoUrl)
        {
            try
            {
                using (new TransactionScope(TransactionScopeOption.Required, new TransactionOptions
                {
                    IsolationLevel = IsolationLevel.ReadUncommitted
                }))
                {
                    using (var data = new dbSiteEntities())
                    {
                        var seoUrl = (from c in data.tbSeoUrl where c.idSeoUrl == idSeoUrl && c.redirect == true select c).FirstOrDefault();
                        if (seoUrl != null)
                        {
                            var url = new UrlDeRedirecionamento();
                            url.id = idSeoUrl;
                            url.Tipo = seoUrl.idSeoUrlTipo;
                            url.Url = seoUrl.url;
                            url.RedirectTo = seoUrl.urlRedirect;

                            var response = client.Index(url, idx => idx.Index("urlsderedirecionamento"));
                            var responseAmazon = clientAmazon.Index(url, idx => idx.Index("urlsderedirecionamento"));
                            var responseAmazon2 = clientAmazon2.Index(url, idx => idx.Index("urlsderedirecionamento"));
                        }
                        else
                        {
                            ExcluirSeoUrl(idSeoUrl);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                rnEmails.EnviaEmail("", "andre@bark.com.br", "", "", "", ex.ToString(), "Exception atualizacao elastic seourl " + idSeoUrl.ToString());
            }
        }


        public void ExcluirSeoUrl(int idSeoUrl)
        {
            if (rnConfiguracoes.AtualizarElasticSearch == false) return;
            var response = client.Delete<UrlDeRedirecionamento>(idSeoUrl, d => d.Index("urlsderedirecionamento"));
            var responseAmazon = clientAmazon.Delete<UrlDeRedirecionamento>(idSeoUrl, d => d.Index("urlsderedirecionamento"));
            var responseAmazon2 = clientAmazon2.Delete<UrlDeRedirecionamento>(idSeoUrl, d => d.Index("urlsderedirecionamento"));
        }   
        
        public void GerarIndice()
        {
            var descriptor = new CreateIndexDescriptor("urlsderedirecionamento").Mappings(x => x.Map<UrlDeRedirecionamento>(m => m.AutoMap()));
            //var res1 = client.DeleteIndex("seourl");
            var res = clientAmazon2.CreateIndex(descriptor);
        }

        public void AtualizarSeoUrlQueue(long? idQueueee)
        {
            #region atualizaQueue
            var data = new dbSiteEntities();
            int idQueue = Convert.ToInt32(idQueueee);
            var queue = (from c in data.tbQueue where c.idQueue == idQueue select c).First();
            queue.andamento = true;
            queue.dataInicio = DateTime.Now;
            data.SaveChanges();
            #endregion atualizaQueue

            int idSeoUrl = (int)queue.idRelacionado;


            var elastic = ElasticSearch.ElasticSearchSeoUrlRepository.Instance;
            elastic.AtualizarSeoUrlElasticSearch(idSeoUrl);


            #region finalizaQueue
            queue.concluido = true;
            queue.andamento = true;
            queue.dataConclusao = DateTime.Now;
            data.SaveChanges();
            #endregion
        }

        private BackgroundWorker bwAtualizarSeoUrl = new BackgroundWorker();
        private void bwAtualizarSeoUrl_DoWork(object sender, DoWorkEventArgs e)
        {

            #region atualizaQueue
            var data = new dbSiteEntities();
            int idQueue = Convert.ToInt32(e.Argument);
            var queue = (from c in data.tbQueue where c.idQueue == idQueue select c).First();
            queue.andamento = true;
            queue.dataInicio = DateTime.Now;
            data.SaveChanges();
            #endregion atualizaQueue

            int idSeoUrl = (int)queue.idRelacionado;


            var elastic = ElasticSearch.ElasticSearchSeoUrlRepository.Instance;
            elastic.AtualizarSeoUrlElasticSearch(idSeoUrl);


            #region finalizaQueue
            queue.concluido = true;
            queue.andamento = true;
            queue.dataConclusao = DateTime.Now;
            data.SaveChanges();
            #endregion

        }

    }
}
