﻿using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bcServerService.ElasticSearch
{
    [ElasticsearchType(Name = "colecao", IdProperty = "id")]
    class Colecao
    {
        public int id { get; set; }
        [Keyword]
        public string url { get; set; }
        public string nome { get; set; }
    }
}
