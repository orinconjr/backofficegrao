﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nest;
using System.Transactions;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;
using System.ComponentModel;

namespace bcServerService.ElasticSearch
{
    public sealed class ElasticSearchCategoriaRepository
    {


        private static readonly ElasticSearchCategoriaRepository instance = new ElasticSearchCategoriaRepository();


        ElasticClient client;
        ElasticClient clientAmazon;
        ElasticClient clientAmazon2;
        string connectionString = ConfigurationManager.AppSettings["elasticSearchUri"];
        string connectionStringAmazon = ConfigurationManager.AppSettings["elasticSearchUriAmazon"];
        string connectionStringAmazon2 = ConfigurationManager.AppSettings["elasticSearchUriAmazon2"];

        private ElasticSearchCategoriaRepository()
        {
            var node = new Uri(connectionString);
            var nodeAmazon = new Uri(connectionStringAmazon);
            var nodeAmazon2 = new Uri(connectionStringAmazon2);
            var settings = new ConnectionSettings(node).RequestTimeout(new TimeSpan(0, 1, 0));
            var settingsAmazon = new ConnectionSettings(nodeAmazon).RequestTimeout(new TimeSpan(0, 1, 0));
            var settingsAmazon2 = new ConnectionSettings(nodeAmazon2).RequestTimeout(new TimeSpan(0, 1, 0));
            settings.BasicAuthentication(ConfigurationManager.AppSettings["elasticSearchUser"], ConfigurationManager.AppSettings["elasticSearchPass"]);
            client = new ElasticClient(settings);
            clientAmazon = new ElasticClient(settingsAmazon);
            clientAmazon2 = new ElasticClient(settingsAmazon2);
        }

        static ElasticSearchCategoriaRepository()
        {
        }

        public static ElasticSearchCategoriaRepository Instance
        {
            get
            {
                return instance;
            }
        }
        public void AtualizarCategoriaElasticSearch(int idCategoria)
        {
            try
            {
                using (new TransactionScope(TransactionScopeOption.Required, new TransactionOptions
                {
                    IsolationLevel = IsolationLevel.ReadUncommitted
                }))
                {
                    using (var data = new dbSiteEntities())
                    {
                        var Categoria = (from c in data.tbProdutoCategoria where c.categoriaId == idCategoria select c).FirstOrDefault();
                        if (Categoria != null)
                        {
                            var categ = new Categoria();
                            categ.Id = idCategoria;
                            categ.CategoriaPaiId = Categoria.categoriaPaiId;
                            categ.Nome = Categoria.categoriaNome;
                            categ.Url = Categoria.categoriaUrl;
                            categ.Descricao = Categoria.categoriaDescricao;
                            categ.Imagem1 = Categoria.imagem1;
                            categ.Imagem2 = Categoria.imagem2;
                            categ.Imagem3 = Categoria.imagem3;
                            categ.NomeDeExibicao = Categoria.categoriaNomeExibicao;
                            categ.ExibirNoSite = (Categoria.exibirSite ?? false);
                            categ.ExibirFiltro = (Categoria.exibirNoFiltro ?? false);
                            categ.ExibirFiltroComValorUnico = (Categoria.exibirMesmoCom1Filtro ?? false);
                            categ.Ordem = Categoria.CategoriaOrdem;     
                            //var response = client.Index(categ, idx => idx.Index("categoria"));
                            var responseAmazon = clientAmazon.Index(categ, idx => idx.Index("categoria"));
                            var responseAmazon2 = clientAmazon2.Index(categ, idx => idx.Index("categoria"));
                        }
                        else
                        {
                            ExcluirCategoria(idCategoria);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                rnEmails.EnviaEmail("", "andre@bark.com.br", "", "", "", ex.ToString(), "Exception atualizacao elastic Categoria " + idCategoria.ToString());
            }
        }


        public void ExcluirCategoria(int idCategoria)
        {
            if (rnConfiguracoes.AtualizarElasticSearch == false) return;
            //var response = client.Delete<Categoria>(idCategoria, d => d.Index("categoria"));
            var responseAmazon = clientAmazon.Delete<Categoria>(idCategoria, d => d.Index("categoria"));
            var responseAmazon2 = clientAmazon2.Delete<Categoria>(idCategoria, d => d.Index("categoria"));
        }

        public void GerarIndice()
        {
            var descriptor = new CreateIndexDescriptor("categoria").Mappings(x => x.Map<Categoria>(m => m.AutoMap()));
            //var res1 = client.DeleteIndex("Categoria");
            var res = clientAmazon2.CreateIndex(descriptor);
        }

        public void AtualizarCategoriaQueue(long? idQueuee)
        {
            var dataInicio = DateTime.Now;

            #region atualizaQueue
            var data = new dbSiteEntities();
            int idQueue = Convert.ToInt32(idQueuee);
            var queue = (from c in data.tbQueue where c.idQueue == idQueue select c).First();
            queue.andamento = true;
            queue.dataInicio = DateTime.Now;
            data.SaveChanges();
            #endregion atualizaQueue

            int idCategoria = (int)queue.idRelacionado;


            var elastic = ElasticSearch.ElasticSearchCategoriaRepository.Instance;
            elastic.AtualizarCategoriaElasticSearch(idCategoria);


            #region finalizaQueue
            queue.concluido = true;
            queue.andamento = true;
            queue.dataConclusao = DateTime.Now;
            data.SaveChanges();


            var queuesRelacionados = (from c in data.tbQueue
                                      where
                                          c.tipoQueue == queue.tipoQueue && c.idRelacionado == queue.idRelacionado &&
                                          c.agendamento < dataInicio && c.andamento == false && c.concluido == false
                                      select c).ToList();
            foreach (var queueRelacionado in queuesRelacionados)
            {
                queueRelacionado.dataInicio = dataInicio;
                queueRelacionado.andamento = true;
                queueRelacionado.concluido = true;
                queueRelacionado.dataConclusao = queue.dataConclusao;
            }
            data.SaveChanges();
            #endregion
        }

        private BackgroundWorker bwAtualizarCategoria = new BackgroundWorker();
        private void bwAtualizarCategoria_DoWork(object sender, DoWorkEventArgs e)
        {
            var dataInicio = DateTime.Now;

            #region atualizaQueue
            var data = new dbSiteEntities();
            int idQueue = Convert.ToInt32(e.Argument);
            var queue = (from c in data.tbQueue where c.idQueue == idQueue select c).First();
            queue.andamento = true;
            queue.dataInicio = DateTime.Now;
            data.SaveChanges();
            #endregion atualizaQueue

            int idCategoria = (int)queue.idRelacionado;


            var elastic = ElasticSearch.ElasticSearchCategoriaRepository.Instance;
            elastic.AtualizarCategoriaElasticSearch(idCategoria);


            #region finalizaQueue
            queue.concluido = true;
            queue.andamento = true;
            queue.dataConclusao = DateTime.Now;
            data.SaveChanges();


            var queuesRelacionados = (from c in data.tbQueue
                                      where
                                          c.tipoQueue == queue.tipoQueue && c.idRelacionado == queue.idRelacionado &&
                                          c.agendamento < dataInicio && c.andamento == false && c.concluido == false
                                      select c).ToList();
            foreach (var queueRelacionado in queuesRelacionados)
            {
                queueRelacionado.dataInicio = dataInicio;
                queueRelacionado.andamento = true;
                queueRelacionado.concluido = true;
                queueRelacionado.dataConclusao = queue.dataConclusao;
            }
            data.SaveChanges();
            #endregion

        }

    }
}
