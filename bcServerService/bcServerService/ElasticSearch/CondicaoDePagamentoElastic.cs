﻿using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bcServerService.ElasticSearch
{
    [ElasticsearchType(Name = "condicaodepagamentoelastic", IdProperty = "id")]
    public class CondicaoDePagamentoElastic
    {
        public int id { get; set; }
        public  int tipoDePagamentoId { get; set; }
        public string nome { get; set; }
        public int numeroMaximoDeParcelas { get; set; }
        public int parcelasSemJuros { get; set; }
        public double taxaDeJuros { get; set; }
        public decimal valorMinimoDaParcela { get; set; }
        public double porcentagemDeDesconto { get; set; }
        public string imagem { get; set; }
        public string imagemMini { get; set; }
        public bool? enviarParaOGateway { get; set; }
        public bool? enviarParaAnaliseDeRisco { get; set; }
        public decimal? valorMinimoAnaliseRisco { get; set; }
        public int? idGatewayContrato { get; set; }
        public int? idCodigoAdquirenteGateway { get; set; }
        public int? idBandeiraCartaoGateway { get; set; }
        public bool destaque { get; set; }
        public bool ativo { get; set; }
        public int? ordem { get; set; }
        public int? idContaPagamento { get; set; }
    }
}
