﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bcServerService.ElasticSearch
{
    public class Especificacao
    {
        public string Nome { get; set; }
        public bool exibirFoto { get; set; }
        public List<EspecificacaoItem> especificacaoItens { get; set; }
    }
}
