﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nest;
using System.Transactions;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;
using System.ComponentModel;

namespace bcServerService.ElasticSearch
{
    [ElasticsearchType(Name = "urlsderedirecionamento", IdProperty = "id")]
    public class UrlDeRedirecionamento
    {
        #region Properties

        public int id { get; set; }

        public int Tipo { get; set; }

        [Keyword]
        public string Url { get; set; }

        [Keyword]
        public string RedirectTo { get; set; }

        #endregion
        

    }
}
