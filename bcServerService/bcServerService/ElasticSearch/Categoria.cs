﻿using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bcServerService.ElasticSearch
{
    [ElasticsearchType(Name = "categoria", IdProperty = "id")]
    public class Categoria
    {
        public int Id { get; set; }
        public int? CategoriaPaiId { get; set; }
        [Keyword]
        public string Nome { get; set; }
        [Keyword]
        public string Url { get; set; }
        [Keyword]
        public string Descricao { get; set; }
        [Keyword]
        public string Imagem1 { get; set; }
        [Keyword]
        public string Imagem2 { get; set; }
        [Keyword]
        public string Imagem3 { get; set; }
        [Keyword]
        public string NomeDeExibicao { get; set; }
        public bool ExibirNoSite { get; set; }
        public bool ExibirFiltro { get; set; }
        public bool ExibirFiltroComValorUnico { get; set; }
        public int Ordem { get; set; }
    }
}
