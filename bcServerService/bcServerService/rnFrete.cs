﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Policy;
using System.Text;
using System.Xml;
using HtmlAgilityPack;

namespace bcServerService
{
    public class RetornoCalculoFrete
    {
        public decimal valor { get; set; }
        public int prazo { get; set; }
    }

    class rnFrete
    {

        public static bool GravaPedidoWebserviceJadlog(int idPedidoEnvio)
        {
            var pedidoDc = new dbSiteEntities();
            var pedido = (from c in pedidoDc.tbPedidoEnvio where c.idPedidoEnvio == idPedidoEnvio select c.tbPedidos).First();
            var pedidoEnvio = (from c in pedidoDc.tbPedidoEnvio where c.idPedidoEnvio == idPedidoEnvio select c).First();
            var nota =
                (from c in pedidoDc.tbNotaFiscal where c.idPedidoEnvio == idPedidoEnvio select c).FirstOrDefault();
            if (nota != null)
            {
                var clientesDc = new dbSiteEntities();
                var cliente = (from c in clientesDc.tbClientes where c.clienteId == pedido.clienteId select c).First();
                var volumes =
                    (from c in pedidoDc.tbPedidoPacote
                        where c.idPedidoEnvio == idPedidoEnvio && (c.concluido ?? true) == true
                        select c).ToList();

                string notaXml = "";
                string chave = "";
                if(nota.xmlBase64 != "")
                {
                    notaXml = rnFuncoes.retornaXmlNotaFromBase64(nota.xmlBase64);
                    XmlDocument xml = new XmlDocument();
                    xml.LoadXml(notaXml);
                    var ideNota = xml.DocumentElement.FirstChild.FirstChild.Attributes[1].Value;
                    chave = ideNota.ToLower().Replace("nfe", "");
                }
                if (chave.Length != 44) chave = "";
                if (chave == "") chave = nota.nfeKey + rnNotaFiscal.GerarDigitoVerificadorNFe(nota.nfeKey);

                int idPedidoCliente = rnFuncoes.retornaIdCliente(pedido.pedidoId);

                string CodCliente = "40745";
                string Password = "L2f0M1e6";
                string Remetente = nota.tbEmpresa.nomeEmitente.ToUpper();
                string RemetenteCNPJ = nota.tbEmpresa.cnpj;
                string RemetenteIE = nota.tbEmpresa.ieEmitente;
                string RemetenteEndereco = nota.tbEmpresa.logradouroEmitente;
                string RemetenteBairro = nota.tbEmpresa.bairroEmitente;
                string RemetenteCEP = nota.tbEmpresa.cepEmitente;
                string RemetenteTelefone = "01135228379";
                string Destino = "";
                string Destinatario = "";
                string DestinatarioCNPJ = "";
                string DestinatarioIE = "";
                string DestinatarioEndereco = "";
                string DestinatarioBairro = "";
                string DestinatarioCEP = "";
                string DestinatarioTelefone = "";
                string fone = string.IsNullOrEmpty(cliente.clienteFoneCelular)
                    ? cliente.clienteFoneResidencial
                    : cliente.clienteFoneCelular;

                if (!string.IsNullOrEmpty(pedido.endNomeDoDestinatario))
                {
                    string enderecoCompleto = pedido.endRua + " - " + pedido.endNumero + " - " + pedido.endComplemento;
                    Destino =
                        rnFuncoes.removeCaracteresEspeciaisDeixaEspaco(rnFuncoes.limpaStringDeixaEspaco(pedido.endCidade))
                            .ToUpper();
                    Destinatario = idPedidoCliente + " " +
                                   rnFuncoes.removeCaracteresEspeciaisDeixaEspaco(
                                       rnFuncoes.limpaStringDeixaEspaco(pedido.endNomeDoDestinatario)).ToUpper();
                    DestinatarioCNPJ = cliente.clienteCPFCNPJ.Replace(".", "").Replace("-", "").Replace("/", "");
                    DestinatarioEndereco =
                        rnFuncoes.removeCaracteresEspeciaisDeixaEspaco(rnFuncoes.limpaStringDeixaEspaco(enderecoCompleto))
                            .ToUpper();
                    DestinatarioBairro =
                        rnFuncoes.removeCaracteresEspeciaisDeixaEspaco(rnFuncoes.limpaStringDeixaEspaco(pedido.endBairro))
                            .ToUpper();
                    DestinatarioCEP = pedido.endCep.Replace("-", "").Replace(" ", "");
                    DestinatarioTelefone = fone.Replace(" ", "").Replace("-", "");
                }
                else
                {
                    string enderecoCompleto = cliente.clienteRua + " - " + cliente.clienteNumero + " - " +
                                              cliente.clienteComplemento;
                    Destino =
                        rnFuncoes.removeCaracteresEspeciaisDeixaEspaco(
                            rnFuncoes.limpaStringDeixaEspaco(cliente.clienteCidade)).ToUpper();
                    Destinatario = idPedidoCliente + " " +
                                   rnFuncoes.removeCaracteresEspeciaisDeixaEspaco(
                                       rnFuncoes.limpaStringDeixaEspaco(cliente.clienteNome)).ToUpper();
                    DestinatarioCNPJ = cliente.clienteCPFCNPJ.Replace(".", "").Replace("-", "").Replace("/", "");
                    DestinatarioEndereco =
                        rnFuncoes.removeCaracteresEspeciaisDeixaEspaco(rnFuncoes.limpaStringDeixaEspaco(enderecoCompleto))
                            .ToUpper();
                    DestinatarioBairro =
                        rnFuncoes.removeCaracteresEspeciaisDeixaEspaco(
                            rnFuncoes.limpaStringDeixaEspaco(cliente.clienteBairro));
                    DestinatarioCEP = cliente.clienteCep.Replace("-", "").Replace(" ", "");
                    DestinatarioTelefone = fone.Replace(" ", "").Replace("-", "");
                }

                //Destinatario = "TESTE DE PEDIDO - NÃO EMITIR";


                string ColetaResponsavel = "";
                string Volumes = volumes.Count().ToString();
                decimal pesoTotal = 0;
                
                foreach(var volume in volumes)
                {
                    var cubagem = (Convert.ToDecimal(volume.altura * volume.largura * volume.profundidade) / 6000);
                    if(cubagem > 10 && cubagem > (Convert.ToDecimal(volume.peso) / 1000))
                    {
                        pesoTotal += cubagem;
                    }
                    else
                    {
                        pesoTotal += (Convert.ToDecimal(volume.peso) / 1000);
                    }
                }
                string PesoReal = pesoTotal.ToString("0.00");
                string Especie = "CONFECCOES";
                string Conteudo = "CONFECCOES";
                string Nr_Pedido = rnFuncoes.retornaIdCliente(pedido.pedidoId).ToString();
                string Nr_NF = "";
                string Danfe = "";
                string Serie_Nf = "";
                string ValorDeclarado = "";

                Nr_NF = nota.numeroNota.ToString();
                Danfe = chave;
                Serie_Nf = "01";
                ValorDeclarado = Convert.ToDecimal(pedidoEnvio.nfeValor).ToString("0.00");

                if (pedidoEnvio.nfeNumero > 0)
                {
                }
                else
                {
                    //Nr_NF = "DECLARACAO";
                    ////ValorDeclarado = "100,00";
                    //decimal valorPedido = pedido.valorCobrado ?? 0;
                    //if (valorPedido > 1000) valorPedido = 1000;
                    //ValorDeclarado = valorPedido.ToString("0.00");
                }
                string Observacoes = "";
                string Modalidade = "3";
                string wCentroCusto = "";
                string wContaCorrente = nota.tbEmpresa.contaCorrenteJadlog;
                string wTipo = "K";
                //string CodUnidade = "1192";
                string CodUnidade = "1368";
                if (pedidoEnvio.idCentroDeDistribuicao == 3) CodUnidade = "1368";
                if (pedidoEnvio.idCentroDeDistribuicao == 4) CodUnidade = "1368";
                if (pedidoEnvio.idCentroDeDistribuicao == 5) CodUnidade = "1368";
                bool erroService = false;

                if (pedidoEnvio.formaDeEnvio.ToLower() == "jadlogexpressa")
                {
                    Modalidade = "0";
                }
                if (pedidoEnvio.formaDeEnvio.ToLower() == "jadlogcom")
                {
                    Modalidade = "9";
                }
                if (pedidoEnvio.formaDeEnvio.ToLower() == "jadlogrodo")
                {
                    Modalidade = "4";
                }

                string codigo = "";
                try
                {
                    List<string> listUrisNotfis = new List<string>();
                    listUrisNotfis.Add("http://www.jadlog.com.br:8080/JadlogEdiWs/services/NotfisBean?wsdl");
                    //listUrisNotfis.Add("http://www.jadlog.com:8080/JadlogEdiWs/services/NotfisBean?wsdl");
                    //listUrisNotfis.Add("http://br.jadlog.com:8080/JadlogEdiWs/services/NotfisBean?wsdl");

                    Random r = new Random();
                    int index = r.Next(listUrisNotfis.Count);
                    string randomUri = listUrisNotfis[index];


                    if (pedidoEnvio.idCentroDeDistribuicao == 3)
                    {
                        //Nr_NF = Nr_NF + "a";
                    }

                    var serviceJad = new serviceJadEnvio.NotfisBeanService() { Url = randomUri }; 
                    var retornoServiceJad = serviceJad.inserir(CodCliente, Password, Remetente, RemetenteCNPJ,
                        RemetenteIE, RemetenteEndereco, RemetenteBairro, RemetenteCEP,
                        RemetenteTelefone, Destino, Destinatario, DestinatarioCNPJ, DestinatarioIE, DestinatarioEndereco,
                        DestinatarioBairro, DestinatarioCEP, DestinatarioTelefone,
                        ColetaResponsavel, Volumes, PesoReal, Especie, Conteudo, Nr_Pedido, Nr_NF, Danfe, Serie_Nf,
                        ValorDeclarado, Observacoes, Modalidade, wCentroCusto, wContaCorrente,
                        wTipo, CodUnidade);

                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.LoadXml(retornoServiceJad);
                    XmlNodeList parentNode = xmlDoc.GetElementsByTagName("Jadlog_Pedido_eletronico_Inserir");
                    foreach (XmlNode childrenNode in parentNode)
                    {
                        string retorno = childrenNode.ChildNodes[1].InnerText;
                        if (!String.IsNullOrEmpty(retorno))
                        {
                            codigo = retorno;
                            if (pedidoEnvio.idCentroDeDistribuicao == 3)
                            {
                                //var retorno1 = serviceJad.cancelar(Convert.ToInt32(CodCliente), Password, codigo, "Nota fiscal com valor incorreto");
                            }
                        }

                    }
                    if (codigo == "-1" | codigo == "-2")
                    {
                        rnEmails.EnviaEmail("", "andre@bark.com.br", "", "", "", retornoServiceJad.ToString(), "Falha Gravar Jadlog");
                        //codigo = "1";
                    }
                }
                catch (WebException ex)
                {
                    var response = ex.Response;
                    var result = String.Empty;
                    using (var stream = response.GetResponseStream())
                    using (var reader = new StreamReader(stream))
                    {
                        result = reader.ReadToEnd();
                    }

                    //codigo = "1";

                    rnEmails.EnviaEmail("", "andre@bark.com.br", "","","", result, "Falha Gravar Jadlog");
                }
                catch (Exception ex)
                {
                    rnEmails.EnviaEmail("", "andre@bark.com.br", "", "", "", ex.ToString(), "Falha Gravar Jadlog");
                    return false;
                }

                //if (codigo == "-1") codigo = "";
                foreach (var pacote in volumes)
                {
                    var pacoteAlterarDc = new dbSiteEntities();
                    var detalhePacote =
                        (from c in pacoteAlterarDc.tbPedidoPacote
                            where c.idPedidoPacote == pacote.idPedidoPacote
                            select c).First();
                    if(string.IsNullOrEmpty(detalhePacote.codJadlog)) detalhePacote.codJadlog = codigo;
                    if (detalhePacote.codJadlog == Nr_Pedido) detalhePacote.codJadlog = Nr_NF;
                    pacoteAlterarDc.SaveChanges();
                }
            }
            return true;
        }

        public static RetornoCalculoFrete CalculaFreteWebserviceJadlog(int peso, long cep, string modalidade, decimal valorPedido)
        {
            string valorNota = Convert.ToDecimal(valorPedido).ToString("0.00");
            var retornoCalculo = new RetornoCalculoFrete();
            bool erroServiceJad = false;
            decimal valorJad = 0;
            int prazoJad = 0;
            decimal pesoEmKg = Convert.ToDecimal(peso) / 1000;

            try
            {
                int modalidadeJad = 3;
                if (modalidade == "jadlogexpressa") modalidadeJad = 0;
                var serviceJad = new serviceJadValorFrete.ValorFreteBeanClient();
                var retornoServiceJad = serviceJad.valorar(modalidadeJad, "L2F0M0E1", "N", valorNota, "0,00", "14910000", cep.ToString().Replace("-", "").Replace(" ", ""), pesoEmKg.ToString("0.00"), "N", "D", "10924051000163");
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(retornoServiceJad);
                XmlNodeList parentNode = xmlDoc.GetElementsByTagName("Jadlog_Valor_Frete");
                foreach (XmlNode childrenNode in parentNode)
                {
                    string retorno = childrenNode.ChildNodes[1].InnerText;
                    valorJad = Convert.ToDecimal(retorno);
                }
            }
            catch (Exception)
            {
                erroServiceJad = true;
            }

            var faixasDc = new dbSiteEntities();
            var faixas = faixasDc.admin_TipoDeEntregaPorFaixaDeCep(cep).Where(x => x.tipodeEntregaId == 9).FirstOrDefault();
            if (faixas != null)
            {
                prazoJad = faixas.prazo + 1;
            }

            retornoCalculo.valor = valorJad;
            retornoCalculo.prazo = prazoJad;
            if (erroServiceJad)
            {
                retornoCalculo.valor = 0;
                retornoCalculo.prazo = 0;
            }
            return retornoCalculo;
        }


        public static string consultaCodigoRastreioJadlog(string codPedidoJadlog, string urlTrackingBean)
        {
            try
            {
                //Cod Lindsay
                string CodCliente = "10924051000163";
                //string CodCliente = "40745";


                string Password = "L2f0M1e6";



                List<string> listUrisNotfis = new List<string>();
                listUrisNotfis.Add("http://www.jadlog.com.br:8080/JadlogEdiWs/services/TrackingBean?wsdl");
                //listUrisNotfis.Add("http://www.jadlog.com:8080/JadlogEdiWs/services/TrackingBean?wsdl");
                //listUrisNotfis.Add("http://br.jadlog.com:8080/JadlogEdiWs/services/TrackingBean?wsdl");

                Random r = new Random();
                int index = r.Next(listUrisNotfis.Count);
                string randomUri = listUrisNotfis[index];

                var serviceJad = new serviceJadTracking.TrackingBeanService { Url = randomUri };
                var retornoServiceJad = serviceJad.consultar(CodCliente, Password, codPedidoJadlog);
                //Response.Write(retornoServiceJad + "<br>");
                string numeroTracking = "";

                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(retornoServiceJad);


                XmlNodeList parentNodeStatus = xmlDoc.GetElementsByTagName("Numero");
                string codigoRastreio = "";
                try
                {
                    codigoRastreio = parentNodeStatus[0].InnerText.Trim();
                }
                catch (Exception)
                {

                }
                return codigoRastreio;                
            }
            catch (Exception ex)
            {
                return "Erro";
            }
        }


        public static string consultaStatusJadlog(string codPedidoJadlog, string urlTrackingBean)
        {
            try
            {
                //Cod Lindsay
                string CodCliente = "10924051000163";
                string Password = "L2f0M1e6";


                List<string> listUrisNotfis = new List<string>();
                listUrisNotfis.Add("http://www.jadlog.com.br:8080/JadlogEdiWs/services/TrackingBean?wsdl");
                //listUrisNotfis.Add("http://www.jadlog.com:8080/JadlogEdiWs/services/TrackingBean?wsdl");
                //listUrisNotfis.Add("http://br.jadlog.com:8080/JadlogEdiWs/services/TrackingBean?wsdl");

                Random r = new Random();
                int index = r.Next(listUrisNotfis.Count);
                string randomUri = listUrisNotfis[index];


                var serviceJad = new serviceJadTracking.TrackingBeanService {Url = randomUri };
                var retornoServiceJad = serviceJad.consultar(CodCliente, Password, codPedidoJadlog);
                //Response.Write(retornoServiceJad + "<br>");
                string numeroTracking = "";

                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(retornoServiceJad);


                XmlNodeList parentNodeStatus = xmlDoc.GetElementsByTagName("Status");
                string statusAtual = "";
                try
                {
                    statusAtual = parentNodeStatus[0].InnerText.Trim();
                }
                catch (Exception)
                {

                }

                if (statusAtual.ToLower() == "entregue") return statusAtual;

                XmlNodeList parentNode = xmlDoc.GetElementsByTagName("Evento");

                foreach (XmlNode childrenNode in parentNode)
                {
                    try
                    {
                        string retorno = childrenNode.ChildNodes[2].InnerText;
                        numeroTracking = retorno;
                        if (!string.IsNullOrEmpty(statusAtual) && statusAtual.Trim().ToLower() != numeroTracking.Trim().ToLower())
                        {
                            numeroTracking = statusAtual + " - " + numeroTracking;
                        }
                    }
                    catch (Exception)
                    {

                    }
                }
                return numeroTracking;
            }
            catch (Exception)
            {
                return "Erro";
            }
        }




        public static void atualizaTodosStatusRastreio()
        {
            //checaPedidosPendentesTnt();
            var data = new dbSiteEntities();
            var pacotes = (from c in data.tbPedidoPacote where (c.rastreamentoConcluido ?? false) == false && c.rastreio != "" && c.formaDeEnvio != "" && c.formaDeEnvio != null && c.formaDeEnvio != "tnt" orderby c.idPedidoEnvio descending select c).Take(5000).ToList();
            foreach (var pacote in pacotes)
            {
                if (pacote.rastreio != "" && pacote.rastreio != null)
                {
                    if (pacote.formaDeEnvio.ToLower().Contains("jadlog"))
                    {
                        string status = consultaStatusJadlog(pacote.rastreio, "http://www.jadlog.com.br:8080/JadlogEdiWs/services/TrackingBean?wsdl").Trim();
                        pacote.statusAtual = status;
                        if (status.ToLower() == "entregue")
                        {
                            pacote.rastreamentoConcluido = true;
                        }
                        data.SaveChanges();
                    }
                    else if (pacote.formaDeEnvio.ToLower() == "pac" | pacote.formaDeEnvio.ToLower() == "sedex")
                    {
                        string status = consultaStatusCorreios(pacote.rastreio).Trim();
                        pacote.statusAtual = status;
                        if (status.ToLower() == "entrega efetuada")
                        {
                            pacote.rastreamentoConcluido = true;
                        }
                        data.SaveChanges();
                    }
                    //else if (pacote.formaDeEnvio.ToLower() == "tnt")
                    //{
                    //    var envio = pacote.tbPedidoEnvio;
                    //    if (envio != null)
                    //    {
                    //        string status = consultaStatusTnt((int)envio.nfeNumero).Trim();
                    //        pacote.statusAtual = status;
                    //        if (status.ToLower() == "entrega realizada")
                    //        {
                    //            pacote.rastreamentoConcluido = true;
                    //        }
                    //        data.SubmitChanges();
                    //    }
                    //}
                }
            }
        }


        public static void AtualizaRastreioTnt()
        {
            return;
            var pedidosDc = new dbSiteEntities();
            var pedidosTnt = (from c in pedidosDc.tbPedidoEnvio
                              join d in pedidosDc.tbPedidoPacote on c.idPedidoEnvio equals d.idPedidoEnvio 
                              join e in pedidosDc.tbNotaFiscal on c.idPedidoEnvio equals e.idPedidoEnvio 
                              where (d.rastreamentoConcluido ?? false) == false && d.despachado == true && c.formaDeEnvio == "tnt"
                              orderby c.idPedidoEnvio descending
                              select new
                              {
                                  c.idPedidoEnvio,
                                  e.numeroNota,
                                  e.tbEmpresa.cnpj,
                                  d.rastreio,
                                  d.statusAtual
                              }).ToList().Distinct().ToList();
            foreach (var pedido in pedidosTnt)
            {
                var data = new dbSiteEntities();
                var parametros = new serviceTntLocalizacao.LocalizacaoIn();
                parametros.usuario = "atendimento@bark.com.br";
                parametros.nf = pedido.numeroNota;
                parametros.nfSpecified = true;
                parametros.nfSerie = "1";
                parametros.cnpj = pedido.cnpj;

                try
                {
                    var consulta = new serviceTntLocalizacao.Localizacao();
                    var retorno = consulta.localizaMercadoria(parametros);
                    if (!string.IsNullOrEmpty(retorno.conhecimento))
                    {
                        string statusAtual = retorno.localizacao.Trim();
                        if (statusAtual.ToLower() != pedido.statusAtual.ToLower())
                        {
                            var itensCodigo = (from c in data.tbPedidoPacote where c.idPedidoEnvio == pedido.idPedidoEnvio select c);
                            foreach (var pacote in itensCodigo)
                            {
                                pacote.statusAtual = statusAtual;
                                pacote.dataStatusAtual = DateTime.Now;
                                pacote.dataStatusAtualTransportadora = DateTime.Now;
                                if (string.IsNullOrEmpty(pacote.rastreio))
                                    pacote.rastreio = retorno.conhecimento.Trim();
                                if (pacote.statusAtual.ToLower().Trim() == "entrega realizada")
                                {
                                    pacote.rastreamentoConcluido = true;
                                    pacote.dataEntrega = DateTime.Now;
                                    pacote.dataEntregaTransportadora = Convert.ToDateTime(retorno.dataEntrega);

                                    if (pacote.dataEntrega.Value.Date.AddDays(-5) > pacote.dataEntregaTransportadora)
                                        pacote.dataEntrega = pacote.dataEntregaTransportadora;
                                }
                                if (pacote.prazoFinalEntregaTransportadora == null)
                                    pacote.prazoFinalEntregaTransportadora = Convert.ToDateTime(retorno.previsaoEntrega);
                            }

                            var status = new tbPedidoEnvioStatusTransporte();
                            status.idPedidoEnvio = pedido.idPedidoEnvio;
                            status.dataStatus = DateTime.Now;
                            status.dataStatusTransportadora = DateTime.Now;
                            status.status = statusAtual;
                            status.observacao = retorno.motivoNaoEntrega.Trim();
                            data.tbPedidoEnvioStatusTransporte.Add(status);
                            data.SaveChanges();
                        }
                    }
                }
                catch (Exception)
                {

                }
            }
        }
        public static void AtualizaRastreioJadlog()
        {
            var pedidosDc = new dbSiteEntities();
            var pedidosTnt = (from c in pedidosDc.tbPedidoEnvio
                              join d in pedidosDc.tbPedidoPacote on c.idPedidoEnvio equals d.idPedidoEnvio 
                              where d.rastreamentoConcluido == false && c.formaDeEnvio.ToLower().Contains("jadlog") && d.rastreio != "" && d.despachado == true && d.rastreio.StartsWith("181")
                              orderby c.idPedidoEnvio
                              select new
                              {
                                  c.idPedidoEnvio,
                                  d.rastreio,
                                  d.statusAtual
                              }).ToList().Distinct().ToList();
            foreach (var pedido in pedidosTnt)
            {
                var data = new dbSiteEntities();
                try
                {
                    //Cod Lindsay
                    string CodCliente = "10924051000163";
                    string Password = "L2f0M1e6";

                    var serviceJad = new serviceJadTracking.TrackingBeanService { Url = "http://www.jadlog.com:8080/JadlogEdiWs/services/TrackingBean" };
                    var retornoServiceJad = serviceJad.consultar(CodCliente, Password, pedido.rastreio);
                    string numeroTracking = "";

                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.LoadXml(retornoServiceJad);


                    XmlNodeList parentNodeStatus = xmlDoc.GetElementsByTagName("Status");
                    string statusAtual = "";
                    try
                    {
                        statusAtual = parentNodeStatus[0].InnerText.Trim();
                    }
                    catch (Exception)
                    {

                    }

                    if (statusAtual.ToLower() != pedido.statusAtual.ToLower())
                    {
                        var lastEvent = DateTime.Now.AddYears(-5);
                        string descricao = "";
                        string observacao = "";

                        XmlNodeList parentNode = xmlDoc.GetElementsByTagName("Evento");
                        foreach (XmlNode childrenNode in parentNode)
                        {
                            try
                            {
                                string dataHora = childrenNode.ChildNodes[1].InnerText;
                                var dataHoraEvento = Convert.ToDateTime(dataHora);
                                if(dataHoraEvento > lastEvent)
                                {
                                    lastEvent = dataHoraEvento;
                                    descricao = childrenNode.ChildNodes[2].InnerText;
                                    observacao = childrenNode.ChildNodes[3].InnerText;
                                }
                            }
                            catch (Exception)
                            {

                            }
                        }



                        var itensCodigo = (from c in data.tbPedidoPacote where c.idPedidoEnvio == pedido.idPedidoEnvio select c);
                        foreach (var pacote in itensCodigo)
                        {
                            pacote.statusAtual = statusAtual;
                            pacote.dataStatusAtual = DateTime.Now;
                            pacote.dataStatusAtualTransportadora = DateTime.Now;
                            if (pacote.statusAtual.ToLower().Trim() == "entregue")
                            {
                                pacote.rastreamentoConcluido = true;
                                pacote.dataEntrega = DateTime.Now;
                                pacote.dataEntregaTransportadora = lastEvent;

                                if (pacote.dataEntrega.Value.Date.AddDays(-5) > pacote.dataEntregaTransportadora)
                                    pacote.dataEntrega = pacote.dataEntregaTransportadora;
                            }

                            //if (pacote.prazoFinalEntregaTransportadora == null) pacote.prazoFinalEntregaTransportadora = Convert.ToDateTime(retorno.previsaoEntrega);
                        }

                        var status = new tbPedidoEnvioStatusTransporte();
                        status.idPedidoEnvio = pedido.idPedidoEnvio;
                        status.dataStatus = DateTime.Now;
                        status.dataStatusTransportadora = lastEvent;
                        status.status = statusAtual + " - " + descricao + " - " + observacao;
                        status.observacao = "";
                        data.tbPedidoEnvioStatusTransporte.Add(status);
                        data.SaveChanges();
                    }
                }
                catch (Exception)
                {

                }
            }
        }
        public static void AtualizaRastreioCorreios()
        {
            var pedidosDc = new dbSiteEntities();
            var pedidosTnt = (from c in pedidosDc.tbPedidoEnvio
                              join d in pedidosDc.tbPedidoPacote on c.idPedidoEnvio equals d.idPedidoEnvio 
                              where d.rastreamentoConcluido == false && (c.formaDeEnvio.ToLower() == "pac" | c.formaDeEnvio.ToLower() == "sedex") && d.rastreio != ""
                              orderby c.idPedidoEnvio
                              select new
                              {
                                  c.idPedidoEnvio,
                                  d.rastreio,
                                  d.statusAtual,
                                  d.idPedidoPacote
                              }).ToList().Distinct().ToList();
            foreach (var pedido in pedidosTnt)
            {
                var data = new dbSiteEntities();

                string sUrlHtml = @"http://websro.correios.com.br/sro_bin/txect01$.QueryList?P_LINGUA=001&P_TIPO=001&P_COD_UNI=" + pedido.rastreio;
                string retorno = "";
                try
                {

                    var lastEvent = DateTime.Now.AddYears(-5);
                    string descricao = "";
                    string observacao = "";
                    string evento = "";

                    HtmlWeb oHtmlWeb = new HtmlAgilityPack.HtmlWeb();
                    HtmlDocument oHtmlDocument = oHtmlWeb.Load(sUrlHtml);
                    HtmlNode oRootNode = oHtmlDocument.DocumentNode;
                    int linhaAtual = 0;
                    var tabelas = from table in oRootNode.SelectNodes("//table").Cast<HtmlNode>()
                                  select table;
                    foreach (var tabela in tabelas)
                    {
                        var linhas = from row in tabela.SelectNodes("//tr").Cast<HtmlNode>() select row;
                        foreach (HtmlNode linha in linhas)
                        {
                            linhaAtual++;
                            if (linhaAtual == 2)
                            {
                                var colunas = from column in linha.SelectNodes("//td").Cast<HtmlNode>() select column;
                                int colunaAtual = 0;
                                foreach (var coluna in colunas)
                                {
                                    colunaAtual++;
                                    if(colunaAtual == 4)
                                    {
                                        lastEvent = Convert.ToDateTime(coluna.InnerText);
                                    }
                                    if(colunaAtual == 5)
                                    {
                                        descricao = coluna.InnerText;
                                    }
                                    if (colunaAtual == 6)
                                    {
                                        evento = coluna.InnerText;
                                    }
                                    if (colunaAtual == 7)
                                    {
                                        observacao = coluna.InnerText;
                                    }
                                }
                            }
                        }
                    }


                    if (evento.ToLower() != pedido.statusAtual.ToLower())
                    {
                        var itensCodigo = (from c in data.tbPedidoPacote where c.idPedidoPacote == pedido.idPedidoPacote select c);
                        foreach (var pacote in itensCodigo)
                        {
                            pacote.statusAtual = evento;
                            pacote.dataStatusAtual = DateTime.Now;
                            pacote.dataStatusAtualTransportadora = DateTime.Now;
                            if (pacote.statusAtual.ToLower().Trim() == "entrega efetuada")
                            {
                                pacote.rastreamentoConcluido = true;
                                pacote.dataEntrega = DateTime.Now;
                                pacote.dataEntregaTransportadora = lastEvent;

                                if (pacote.dataEntrega.Value.Date.AddDays(-5) > pacote.dataEntregaTransportadora)
                                    pacote.dataEntrega = pacote.dataEntregaTransportadora;
                            }

                            //if (pacote.prazoFinalEntregaTransportadora == null) pacote.prazoFinalEntregaTransportadora = Convert.ToDateTime(retorno.previsaoEntrega);
                        }

                        var status = new tbPedidoEnvioStatusTransporte();
                        status.idPedidoEnvio = pedido.idPedidoEnvio;
                        status.dataStatus = DateTime.Now;
                        status.dataStatusTransportadora = lastEvent;
                        status.status = evento + " - " + descricao + " - " + observacao;
                        status.observacao = "";
                        data.tbPedidoEnvioStatusTransporte.Add(status);
                        data.SaveChanges();
                    }
                }
                catch (Exception)
                {
                }


            }
        }


        private static string checaRastreioTnt(int idPedidoEnvio)
        {
            var data = new dbSiteEntities();
            var nota = (from c in data.tbNotaFiscal where c.idPedidoEnvio == idPedidoEnvio select c).First();
            string numeroTracking = "";
            var parametros = new serviceTntLocalizacao.LocalizacaoIn();
            parametros.usuario = "atendimento@bark.com.br";
            parametros.nf = nota.numeroNota;
            parametros.nfSpecified = true;
            parametros.nfSerie = "1";
            parametros.cnpj = nota.tbEmpresa.cnpj;

            try
            {
                var consulta = new serviceTntLocalizacao.Localizacao();
                var retorno = consulta.localizaMercadoria(parametros);
                numeroTracking = retorno.conhecimento;

                if (!string.IsNullOrEmpty(numeroTracking))
                {
                    var itensCodigo = (from c in data.tbPedidoPacote where c.idPedidoEnvio == idPedidoEnvio select c);
                    foreach (var pacote in itensCodigo)
                    {
                        pacote.statusAtual = retorno.localizacao;
                        if (pacote.statusAtual.ToLower().Trim() == "entrega realizada")
                        {
                            pacote.rastreamentoConcluido = true;
                        }
                    }
                    data.SaveChanges();
                }
            }
            catch (Exception)
            {

            }
            //retorno.localizacao;
            //retorno.conhecimento;
            //http://app.tntbrasil.com.br/LocalizacaoSimplificada/resultado_localizacao.asp?cnpj=01404654135&nota=7858

            string teste = "";
            return numeroTracking;
        }

        public static string consultaStatusCorreios(string rastreio)
        {
            try
            {
                string usuarioCorreios = "epsadmin1664";
                string senhaCorreios = "654321";

                string contratoCorreios = "9912304790";
                string cartaoCorreios = "0064356663";
                string codAdministrativoCorreios = "0012322504";
                string perfilImportacaoCorreios = "1476";


                var listarRastreio = new serviceVip1.ListarRastreio();
                listarRastreio.NrEtiquetaPostagem = rastreio;
                listarRastreio.PerfilVipp = new serviceVip1.PerfilVipp()
                {
                    Usuario = usuarioCorreios,
                    Token = senhaCorreios,
                    IdPerfil = perfilImportacaoCorreios
                };


                var postar = new serviceVip1.PostagemVipp();
                var resultadoPostagem = postar.ListarRastreioObjeto(listarRastreio);

                return resultadoPostagem[0].NomeGrupoStatusEvento.ToString();
            }
            catch(Exception ex)
            {
                return "Erro";
            }


            string sUrlHtml = @"http://websro.correios.com.br/sro_bin/txect01$.QueryList?P_LINGUA=001&P_TIPO=001&P_COD_UNI=" + rastreio;
            string retorno = "";
            try
            {
                HtmlWeb oHtmlWeb = new HtmlAgilityPack.HtmlWeb();
                HtmlDocument oHtmlDocument = oHtmlWeb.Load(sUrlHtml);
                HtmlNode oRootNode = oHtmlDocument.DocumentNode;
                int linhaAtual = 0;
                var tabelas = from table in oRootNode.SelectNodes("//table").Cast<HtmlNode>()
                              select table;
                foreach (var tabela in tabelas)
                {
                    var linhas = from row in tabela.SelectNodes("//tr").Cast<HtmlNode>() select row;
                    foreach (HtmlNode linha in linhas)
                    {
                        linhaAtual++;
                        if (linhaAtual == 2)
                        {
                            var colunas = from column in linha.SelectNodes("//td").Cast<HtmlNode>() select column;
                            int colunaAtual = 0;
                            foreach (var coluna in colunas)
                            {
                                colunaAtual++;
                                if (colunaAtual == 6)
                                {
                                    return coluna.InnerText;
                                }
                            }
                        }
                    }

                }
            }
            catch (Exception)
            {
            }
            return "ERRO";
        }

    }

}
