﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Xml;

namespace bcServerService
{
    class rnPedidos
    {

        public static bool verificaReservasPedidoFornecedorCompleto(int pedidoId)
        {
            var produtosDc = new dbSiteEntities();
            var pedidosDc = new dbSiteEntities();
            var itensDc = new dbSiteEntities();
            var itensPedido = (from c in itensDc.tbItensPedido where c.pedidoId == pedidoId select c);
            bool completo = true;

            foreach (var item in itensPedido)
            {
                bool cancelado = item.cancelado ?? false;
                if (!cancelado)
                {
                    var relacionados = (from c in produtosDc.tbItensPedidoCombo where c.idItemPedido == item.itemPedidoId select c);
                    if (relacionados.Any())
                    {
                        foreach (var relacionado in relacionados)
                        {
                            var reservasAtuais = (from c in produtosDc.tbProdutoReservaEstoque where c.idPedido == pedidoId && c.idItemPedido == item.itemPedidoId && c.idProduto == relacionado.produtoId select c).Count();
                            var pedidosAtuais = (from c in pedidosDc.tbPedidoFornecedorItem where c.idPedido == pedidoId && c.idItemPedido == item.itemPedidoId && c.idProduto == relacionado.produtoId select c).Count();
                            if (reservasAtuais + pedidosAtuais < Convert.ToInt32(item.itemQuantidade))
                            {
                                completo = false;
                            }
                        }
                    }
                    else
                    {
                        var reservasAtuais = (from c in produtosDc.tbProdutoReservaEstoque where c.idPedido == pedidoId && c.idItemPedido == item.itemPedidoId && c.idProduto == item.produtoId select c).Count();
                        var pedidosAtuais = (from c in pedidosDc.tbPedidoFornecedorItem where c.idPedido == pedidoId && c.idItemPedido == item.itemPedidoId && c.idProduto == item.produtoId select c).Count();
                        if (reservasAtuais + pedidosAtuais < Convert.ToInt32(item.itemQuantidade))
                        {
                            completo = false;
                        }
                    }

                }
            }

            return completo;
        }
        public static bool pedidoAlteraStatus(int statusDoPedido, int pedidoId)
        {
            var data = new dbSiteEntities();
            var pedido = (from c in data.tbPedidos where c.pedidoId == pedidoId select c).First();
            pedido.statusDoPedido = statusDoPedido;
            data.SaveChanges();
            return true;
        }
        public static string checaRastreioJad(string codPedidoJadlog)
        {
            string CodCliente = "10924051000163";
            string Password = "L2F0M0E1";

            var serviceJad = new serviceJadTracking.TrackingBeanService();
            var retornoServiceJad = serviceJad.consultar(CodCliente, Password, codPedidoJadlog);
            //Response.Write(retornoServiceJad + "<br>");
            string numeroTracking = "";

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(retornoServiceJad);
            XmlNodeList parentNode = xmlDoc.GetElementsByTagName("ND");

            foreach (XmlNode childrenNode in parentNode)
            {
                try
                {
                    string retorno = childrenNode.ChildNodes[0].InnerText;
                    numeroTracking = retorno;
                }
                catch (Exception)
                {

                }
            }
            return numeroTracking;
        }

        public static bool CancelarPedido(int pedidoId, string usuario, int motivoCancelamento)
        {
            RetiraVinculoPedidoFornecedor(pedidoId);
            var data = new dbSiteEntities();
            var pedido = (from c in data.tbPedidos where c.pedidoId == pedidoId select c).First();
            if (pedido.statusDoPedido == 6) return false;
            pedido.statusDoPedido = 6;
            pedido.motivoCancelamento = motivoCancelamento.ToString();
            data.SaveChanges();

            rnInteracoes.interacaoInclui(pedidoId, "Pedido Cancelado", usuario, "True");

            rnEmails.enviaEmailPedidoCancelado(pedidoId);


            #region Manda para tabela de Queue para liberar Reservas
            var queue = new tbQueue();
            queue.agendamento = DateTime.Now;
            queue.andamento = false;
            queue.concluido = false;
            queue.idRelacionado = pedidoId;
            queue.tipoQueue = 20; // Cancela reserva de itens de pedido
            queue.mensagem = "";
            data.tbQueue.Add(queue);
            data.SaveChanges();
            #endregion

            return true;
        }

        public static void AdicionarChamado(DateTime dataAgendamento, int? idPedido, string titulo, string textoChamado, int idUsuario)
        {
            using (var data = new dbSiteEntities())
            {
                var chamado = new tbChamado();
                chamado.dataLimite = dataAgendamento;
                chamado.titulo = titulo;
                chamado.dataCadastro = DateTime.Now;
                chamado.descricao = textoChamado;
                chamado.idUsuario = idUsuario;
                chamado.idPedido = idPedido;
                data.tbChamado.Add(chamado);
                data.SaveChanges();
            }
        }

        public static bool RetiraVinculoPedidoFornecedor(int pedidoId)
        {
            var pedidosDc = new dbSiteEntities();
            var itensPedido = (from c in pedidosDc.tbPedidoFornecedorItem where c.idPedido == pedidoId && c.entregue == false select c);
            foreach (var item in itensPedido)
            {
                item.idItemPedido = null;
                item.idPedido = null;
            }
            var itensPedidoSeparado = (from c in pedidosDc.tbProdutoEstoque where c.pedidoId == pedidoId && c.enviado == false select c);
            foreach (var itemPedidoSeparado in itensPedidoSeparado)
            {
                itemPedidoSeparado.itemPedidoId = null;
                itemPedidoSeparado.idPedidoEnvio = null;
                itemPedidoSeparado.pedidoId = null;
            }
            pedidosDc.SaveChanges();
            return true;
        }





        
    }
}
