﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxGridView;

public partial class admin_sendoEmbaladosAtual : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["cancelar"] != null)
        {
            int pedidoId = Convert.ToInt32(Request.QueryString["cancelar"]);

            var pedidoDc = new dbCommerceDataContext();
            var pedido = (from c in pedidoDc.tbPedidos where c.pedidoId == pedidoId select c).First();
            pedido.idUsuarioEmbalado = null;
            pedido.dataSendoEmbalado = null;
            pedido.dataEmbaladoFim = null;
            pedido.volumesEmbalados = null;
            pedido.idUsuarioSeparacao = null;
            pedido.separado = false;
            pedido.dataInicioSeparacao = null;
            pedido.statusDoPedido = 11;
            pedido.envioLiberado = false;
            pedidoDc.SubmitChanges();

            var envio = (from c in pedidoDc.tbPedidoEnvios where c.idPedido == pedidoId && c.dataInicioEmbalagem == null orderby c.idPedidoEnvio descending select c).FirstOrDefault();
            if (envio != null)
            {
                var pacotes = (from c in pedidoDc.tbPedidoPacotes where c.idPedidoEnvio == envio.idPedidoEnvio select c);
                foreach (var pacote in pacotes)
                {
                    pedidoDc.tbPedidoPacotes.DeleteOnSubmit(pacote);
                }
                pedidoDc.SubmitChanges();

                var itensSeparados = (from c in pedidoDc.tbProdutoEstoques where c.idPedidoEnvio == envio.idPedidoEnvio select c).ToList();
                foreach (var itensSeparado in itensSeparados)
                {
                    itensSeparado.idPedidoEnvio = null;
                    itensSeparado.pedidoId = null;
                    itensSeparado.itemPedidoId = null;
                    pedidoDc.SubmitChanges();
                }
                pedidoDc.tbPedidoEnvios.DeleteOnSubmit(envio);
                pedidoDc.SubmitChanges();
            }
        }

        fillGrid();

    }

    private void fillGrid()
    {
        var data = new dbCommerceDataContext();
        int idPedido = 0;
        int.TryParse(Request.QueryString["pedidoId"], out idPedido);

        var pedido = (from c in data.tbPedidos
            where c.statusDoPedido == 4 | (c.statusDoPedido == 11 && c.dataInicioSeparacao != null && c.idUsuarioSeparacao != null && c.separado == false) | c.pedidoId == idPedido orderby c.dataSendoEmbalado descending
            select new
            {
                c.pedidoId,
                c.dataSendoEmbalado,
                c.idUsuarioEmbalado
            });
        grd.DataSource = pedido;
        if(!Page.IsPostBack && !Page.IsCallback) grd.DataBind();
    }
    protected void gridClientes_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType != GridViewRowType.Data)
        {
            return;
        }

        int pedidoId = (int)grd.GetRowValues(e.VisibleIndex, new string[] { grd.KeyFieldName });
    }



    protected void btPdf_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WritePdfToResponse();
    }
    protected void btXsl_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteXlsToResponse();
    }
    protected void btRtf_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteRtfToResponse();
    }
    protected void btCsv_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteCsvToResponse();
    }
    protected void grd_OnCustomUnboundColumnData(object sender, ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "pedidoIdCliente")
        {
            int pedidoId = Convert.ToInt32(e.GetListSourceFieldValue("pedidoId"));
            e.Value = rnFuncoes.retornaIdCliente(pedidoId);
        }
        if (e.Column.FieldName == "nome")
        {
            int idUsuarioEmbalado = Convert.ToInt32(e.GetListSourceFieldValue("idUsuarioEmbalado"));
            var usuarioDc = new dbCommerceDataContext();
            var usuario = (from c in usuarioDc.tbUsuarioExpedicaos where c.idUsuarioExpedicao == idUsuarioEmbalado select c).FirstOrDefault();
            if (usuario != null)
            {
                e.Value = usuario.nome;
            }
        }
    }

    protected void btnCancelar_OnCommand(object sender, CommandEventArgs e)
    {
        pcCancelarEmbalagem.ShowOnPageLoad = true;
        hdfPedidoId.Value = e.CommandArgument.ToString();
        int pedidoId = Convert.ToInt32(hdfPedidoId.Value);
        var itensPedidoDc = new dbCommerceDataContext();
        var itensPedido = (from c in itensPedidoDc.tbItensPedidos where c.pedidoId == pedidoId select c);
        lstItensPedido.DataSource = itensPedido;
        lstItensPedido.DataBind();
    }
    protected void lstItensPedido_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        var produtosDc = new dbCommerceDataContext();
        HiddenField hdfProdutoId = (HiddenField)e.Item.FindControl("hdfProdutoId");
        HiddenField hdfQuantidade = (HiddenField)e.Item.FindControl("hdfQuantidade");
        HiddenField hdfItemPedidoId = (HiddenField)e.Item.FindControl("hdfItemPedidoId");
        Literal litNome = (Literal)e.Item.FindControl("litNome");
        ListView lstItensPedidoCombo = (ListView)e.Item.FindControl("lstItensPedidoCombo");
        CheckBox chkCancelar = (CheckBox)e.Item.FindControl("chkCancelar");
        DropDownList ddlQuantidade = (DropDownList)e.Item.FindControl("ddlQuantidade");

        int itemPedidoId = Convert.ToInt32(hdfItemPedidoId.Value);
        int produtoId = Convert.ToInt32(hdfProdutoId.Value);
        var produto = (from c in produtosDc.tbProdutos where c.produtoId == produtoId select c).First();
        litNome.Text = produto.produtoNome;

        int quantidade = Convert.ToInt32(hdfQuantidade.Value);

        var produtosRelacionados = (from c in produtosDc.tbItensPedidoCombos where c.idItemPedido == itemPedidoId select new
        {
            produtoId = c.produtoId,
            c.tbProduto.produtoNome,
            itemQuantidade = quantidade
        });
        lstItensPedidoCombo.DataSource = produtosRelacionados;
        lstItensPedidoCombo.DataBind();

        if (produtosRelacionados.Any())
        {
            chkCancelar.Visible = false;
            ddlQuantidade.Visible = false;
        }
        else
        {
            for (int i = 1; i <= quantidade; i++)
            {
                ddlQuantidade.Items.Add(new ListItem(i.ToString(), i.ToString()));
            }
            ddlQuantidade.SelectedValue = quantidade.ToString();
        }
    }

    protected void lstItensPedidoCombo_OnItemDataBound(object sender, ListViewItemEventArgs e)
    {
        HiddenField hdfQuantidadeCombo = (HiddenField)e.Item.FindControl("hdfQuantidadeCombo");
        DropDownList ddlQuantidadeCombo = (DropDownList)e.Item.FindControl("ddlQuantidadeCombo");

        int quantidade = Convert.ToInt32(hdfQuantidadeCombo.Value);

        for (int i = 1; i <= quantidade; i++)
        {
            ddlQuantidadeCombo.Items.Add(new ListItem(i.ToString(), i.ToString()));
        }
        ddlQuantidadeCombo.SelectedValue = quantidade.ToString();
    }

    protected void btnCancelarEmbalagem_OnClick(object sender, EventArgs e)
    {
        if(string.IsNullOrEmpty(txtMotivo.Text))
        {
            AlertShow("Favor preencher o motivo para cancelamento da embalagem");
            txtMotivo.Focus();
            return;
        }

        var interacao = new StringBuilder();
        var produtosDc = new dbCommerceDataContext();
        var pedidoDc = new dbCommerceDataContext();

        int pedidoID = Convert.ToInt32(hdfPedidoId.Value);


        interacao.AppendFormat("<b>Pedido sendo embalado retornado ao status de Separação de Estoque.</b><br>");
        interacao.AppendFormat("Motivo: " + txtMotivo.Text);
        foreach (var itemPedido in lstItensPedido.Items)
        {
            HiddenField hdfProdutoId = (HiddenField)itemPedido.FindControl("hdfProdutoId");
            HiddenField hdfItemPedidoId = (HiddenField)itemPedido.FindControl("hdfItemPedidoId");
            Literal litNome = (Literal)itemPedido.FindControl("litNome");
            CheckBox chkCancelar = (CheckBox)itemPedido.FindControl("chkCancelar");

            if (chkCancelar.Visible == false)
            {
                ListView lstItensPedidoCombo = (ListView)itemPedido.FindControl("lstItensPedidoCombo");
                foreach (var itemPedidoCombo in lstItensPedidoCombo.Items)
                {
                    CheckBox chkCancelarCombo = (CheckBox)itemPedidoCombo.FindControl("chkCancelarCombo");
                    if (chkCancelarCombo.Checked)
                    {
                        DropDownList ddlQuantidade = (DropDownList)itemPedidoCombo.FindControl("ddlQuantidadeCombo");
                        int quantidade = Convert.ToInt32(ddlQuantidade.SelectedValue);

                        for (int i = 1; i <= quantidade; i++)
                        {
                            HiddenField hdfProdutoIdFilho = (HiddenField) itemPedidoCombo.FindControl("hdfProdutoIdFilho");
                            int produtoId = Convert.ToInt32(hdfProdutoIdFilho.Value);
                            int itemPedidoId = Convert.ToInt32(hdfItemPedidoId.Value);
                            var produto = (from c in produtosDc.tbProdutos where c.produtoId == produtoId select c).First();
                            var logEstoque = new tbProdutoLogEstoque();
                            logEstoque.idProduto = produtoId;
                            logEstoque.idOperacao = pedidoID;
                            logEstoque.estoqueAnterior = Convert.ToInt32(produto.estoqueReal);
                            produto.estoqueReal -= 1;
                            logEstoque.estoqueAlterado = Convert.ToInt32(produto.estoqueReal);
                            logEstoque.data = DateTime.Now;
                            logEstoque.descricao = "Produto Faltando " + pedidoID;
                            produtosDc.tbProdutoLogEstoques.InsertOnSubmit(logEstoque);
                            produtosDc.SubmitChanges();

                            interacao.AppendFormat("<br>Produto Faltando: " + produto.produtoNome);

                            var pedidoProduto = rnPedidos.reservaProdutoOuPedeFornecedor(pedidoID, produtoId, 1, itemPedidoId);
                            var produtoFaltando = new tbPedidoProdutoFaltando();
                            produtoFaltando.pedidoId = pedidoID;
                            produtoFaltando.produtoId = produtoId;
                            produtoFaltando.entregue = pedidoProduto == 1 ? true : false;
                            produtoFaltando.itemPedidoId = itemPedidoId;
                            produtosDc.tbPedidoProdutoFaltandos.InsertOnSubmit(produtoFaltando);
                            produtosDc.SubmitChanges();
                        }
                    }
                }
            }
            else
            {
                if (chkCancelar.Checked)
                {
                    DropDownList ddlQuantidade = (DropDownList)itemPedido.FindControl("ddlQuantidade");
                    int quantidade = Convert.ToInt32(ddlQuantidade.SelectedValue);

                    for (int i = 1; i <= quantidade; i++)
                    {
                        int produtoId = Convert.ToInt32(hdfProdutoId.Value);
                        int itemPedidoId = Convert.ToInt32(hdfItemPedidoId.Value);

                        var pedidoProduto = rnPedidos.reservaProdutoOuPedeFornecedor(pedidoID, produtoId, 1, itemPedidoId);

                        var produtoFaltando = new tbPedidoProdutoFaltando();
                        produtoFaltando.pedidoId = pedidoID;
                        produtoFaltando.produtoId = produtoId;
                        produtoFaltando.entregue = pedidoProduto == 1 ? true : false;
                        produtoFaltando.itemPedidoId = itemPedidoId;
                        produtosDc.tbPedidoProdutoFaltandos.InsertOnSubmit(produtoFaltando);

                        var produto = (from c in produtosDc.tbProdutos where c.produtoId == produtoId select c).First();
                        var logEstoque = new tbProdutoLogEstoque();
                        logEstoque.idProduto = produtoId;
                        logEstoque.idOperacao = pedidoID;
                        logEstoque.estoqueAnterior = Convert.ToInt32(produto.estoqueReal);
                        produto.estoqueReal -= 1;
                        logEstoque.estoqueAlterado = Convert.ToInt32(produto.estoqueReal);
                        logEstoque.data = DateTime.Now;
                        logEstoque.descricao = "Produto Faltando " + pedidoID;
                        produtosDc.tbProdutoLogEstoques.InsertOnSubmit(logEstoque);
                        produtosDc.SubmitChanges();


                        interacao.AppendFormat("<br>Produto Faltando: " + produto.produtoNome);

                    }
                }
            }
        }

        var pedido = (from c in pedidoDc.tbPedidos where c.pedidoId == pedidoID select c).First();
        pedido.idUsuarioEmbalado = null;
        pedido.dataSendoEmbalado = null;
        pedido.dataEmbaladoFim = null;
        pedido.volumesEmbalados = null;
        pedido.idUsuarioSeparacao = null;
        pedido.separado = false;
        pedido.dataInicioSeparacao = null;
        pedido.statusDoPedido = 11;
        pedido.envioLiberado = false;
        pedidoDc.SubmitChanges();

        var envio = (from c in pedidoDc.tbPedidoEnvios where c.idPedido == pedidoID orderby c.idPedidoEnvio descending select c).FirstOrDefault();
        if (envio != null)
        {
            var pacotes = (from c in pedidoDc.tbPedidoPacotes where c.idPedidoEnvio == envio.idPedidoEnvio select c);
            foreach (var pacote in pacotes)
            {
                pedidoDc.tbPedidoPacotes.DeleteOnSubmit(pacote);
            }
            pedidoDc.SubmitChanges();

            var itensSeparados = (from c in pedidoDc.tbProdutoEstoques where c.idPedidoEnvio == envio.idPedidoEnvio select c).ToList();
            int idTransferenciaEndereco = 0;
            if (itensSeparados.Count > 0)
            {
                var transferencia = new tbTransferenciaEndereco();
                transferencia.idEnderecamentoTransferencia = 2;
                transferencia.usuarioEntrada = rnUsuarios.retornaNomeUsuarioLogado();
                transferencia.idUsuarioExpedicao = envio.idUsuarioSeparacao;
                transferencia.dataCadastro = DateTime.Now;
                transferencia.dataInicio = DateTime.Now;
                pedidoDc.tbTransferenciaEnderecos.InsertOnSubmit(transferencia);
                pedidoDc.SubmitChanges();
                idTransferenciaEndereco = transferencia.idTransferenciaEndereco;
            }
            foreach (var itensSeparado in itensSeparados)
            {
                var itemTransferenciaEndereco = new tbTransferenciaEnderecoProduto();
                itemTransferenciaEndereco.idTransferenciaEndereco = idTransferenciaEndereco;
                itemTransferenciaEndereco.idPedidoFornecedorItem = itensSeparado.idPedidoFornecedorItem;
                pedidoDc.tbTransferenciaEnderecoProdutos.InsertOnSubmit(itemTransferenciaEndereco);
                itensSeparado.idEnderecamentoAndar = null;
                itensSeparado.idEnderecamentoApartamento = null;
                itensSeparado.idEnderecamentoArea = null;
                itensSeparado.idEnderecamentoPredio = null;
                itensSeparado.idEnderecamentoRua = null;
                itensSeparado.idPedidoEnvio = null;
                itensSeparado.pedidoId = null;
                itensSeparado.itemPedidoId = null;
                pedidoDc.SubmitChanges();
            }
            pedidoDc.tbPedidoEnvios.DeleteOnSubmit(envio);
            pedidoDc.SubmitChanges();
        }

        txtMotivo.Text = "";
        adicionaInteracaoPedido(interacao.ToString(), "False", pedidoID);

        var data = new dbCommerceDataContext();
        var queue = new tbQueue();
        queue.agendamento = DateTime.Now;
        queue.andamento = false;
        queue.concluido = false;
        queue.idRelacionado = pedidoID;
        queue.mensagem = "";
        queue.tipoQueue = 8;
        data.tbQueues.InsertOnSubmit(queue);
        data.SubmitChanges();

        rnEmails.EnviaEmail("", "comercial@graodegente.com.br", "", "", "", interacao.ToString(), "Pedido: " + pedidoID);
        Response.Redirect("sendoEmbaladosAtual.aspx");
    }


    public void AlertShow(string message)
    {
        base.ClientScript.RegisterStartupScript(base.GetType(), "myalert", "alert('" + message + "'); ", true);
    }

    public void adicionaInteracaoPedido(string interacao, string alteracaoDeEstatus, int pedidoId)
    {
        HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
        usuarioLogadoId = HttpContext.Current.Request.Cookies["usuarioLogadoId"];
        if (usuarioLogadoId != null)
        {
            rnInteracoes.interacaoInclui(pedidoId, interacao, rnUsuarios.usuarioSeleciona_PorUsuarioId(int.Parse(usuarioLogadoId.Value.ToString())).Tables[0].Rows[0]["usuarioNome"].ToString(), alteracaoDeEstatus);
        }
    }

}