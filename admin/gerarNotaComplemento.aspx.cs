﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class gerarNotaComplemento : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        var nota = rnNotaFiscal.gerarNotaFiscalComplemento(18807, 1, "5103858");
        rnNotaFiscal.autorizarNota(nota.xml, 0, nota.idNotaFiscal);

    }
}