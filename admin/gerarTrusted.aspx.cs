﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HtmlAgilityPack;

public partial class gerarTrusted : System.Web.UI.Page
{
    public class Review
    {
        public string opiniao { get; set; }
        public string nome { get; set; }
        public string titulo { get; set; }
        public string nota { get; set; }
        public string link { get; set; }
        public DateTime data { get; set; }

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        List<Review> reviewlist = new List<Review>();
        using (WebClient client = new WebClient())
        {
            client.Encoding = System.Text.Encoding.UTF8;

            string htmlCode =
                  client.DownloadString("http://trustedcompany.com/br/graodegente.com.br-opini%C3%B5es?rating=5");
            HtmlDocument document = new HtmlDocument();
            document.LoadHtml(htmlCode);

            var reviewobjects = (from d in document.DocumentNode.Descendants()
                                 where d.Name == "div" && d.Attributes["class"].Value == "review  "
                                 select d).Take(30);
            foreach (var item in reviewobjects)
            {
                Review reviewobject = new Review();
                HtmlNode titleNode = (from o in item.Descendants()
                                    where o.Name == "h5" && o.Attributes["class"].Value == "title"
                                    select o).First();

                HtmlNode titleLinkNode = (from o in titleNode.Descendants()
                                    where o.Name == "a"
                                    select o).First();
                var link = titleLinkNode.Attributes.First().Value.ToString();
                var titulo = titleLinkNode.InnerText.ToString();
                reviewobject.link = link;
                reviewobject.titulo = titulo;



                HtmlNode ratingNode = (from o in item.Descendants()
                                       where o.Name == "div" && o.Attributes["class"].Value == "rating"
                                       select o).First();


                HtmlNode dateNode = (from o in ratingNode.Descendants()
                                     where o.Name == "div" && o.Attributes["class"].Value == "created"
                                     select o).First();
                var dateNotFormated = dateNode.InnerText.ToString();
                var datePart = dateNotFormated.Split(',')[1];
                datePart = datePart.Replace("-", "").Replace("  ", " ");
                var dataCompleta = Convert.ToDateTime(datePart);
                reviewobject.data = dataCompleta;

                var rating = (from o in ratingNode.Descendants()
                                            where o.Name == "meta" && o.Attributes["itemprop"].Value == "ratingValue"
                                       select o).First().Attributes["content"].Value.ToString();
                reviewobject.nota = rating;

                HtmlNode opiniao = (from o in item.Descendants()
                                    where o.Name == "p" && o.Attributes["class"].Value == "body"
                                    select o).First();
                reviewobject.opiniao = opiniao.InnerText;
                HtmlNode userNode = (from u in item.Descendants()
                                     where u.Name == "div" && u.Attributes["class"].Value == "user"
                                     select u).First();
                



                try
                {
                    HtmlNode nome = (from n in userNode.Descendants()
                                     where n.Name == "div" && n.Attributes["class"].Value == "user__name"
                                     select n).First().Descendants().First();

                    reviewobject.nome = nome.InnerHtml;
                }
                catch (Exception) { }

                reviewlist.Add(reviewobject);
            }

        }
    }
}