﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class pedidoembalar : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        var data = new dbCommerceDataContext();
        var pedidosCd1e2 = (from c in data.tbPedidoPacotes
                            where c.idCentroDistribuicao == 2 && c.etiquetaImpressa == false && (c.concluido ?? false) == true
                            select c.tbPedido).ToList();
        var pedidosCd2 = (from c in data.tbPedidos
                          join d in data.tbItemPedidoEstoques on c.pedidoId equals d.tbItensPedido.pedidoId into itensEstoque
                          join g in data.tbNotaFiscals on c.pedidoId equals g.idPedido into notas
                          join f in data.tbProdutoEstoques on c.pedidoId equals f.pedidoId into itensSeparados
                          where c.statusDoPedido == 11 &&
                          itensEstoque.Count(x => x.cancelado == false && x.enviado == false && x.reservado == false && (x.reservado == false && x.autorizarParcial == true) == false) == 0
                          && itensEstoque.Count(x => x.cancelado == false && x.enviado == false) > 0 &&
                          itensEstoque.Count(x => x.cancelado == false && x.enviado == false && x.reservado == true) == itensSeparados.Count(x => x.enviado == false) &&
                          (itensEstoque.Count(x => x.cancelado == false && x.enviado == false && x.reservado == true && x.idCentroDistribuicao == 1) == 0 && itensEstoque.Count(x => x.cancelado == false && x.enviado == false && x.reservado == true && x.idCentroDistribuicao == 2) <= itensSeparados.Count(x => x.idCentroDistribuicao == 2 && x.enviado == false))
                          orderby (c.entregaEnviarPor == "JadlogExpressa" ? 0 : c.prioridadeEnvio == null ? 9999 : c.prioridadeEnvio),
                          (
                          notas.OrderByDescending(x => x.idNotaFiscal).FirstOrDefault() != null ?
                          (notas.OrderByDescending(x => x.idNotaFiscal).FirstOrDefault().dataHora < c.prazoFinalPedido ?
                          notas.OrderByDescending(x => x.idNotaFiscal).FirstOrDefault().dataHora :
                          c.prazoMaximoPostagemAtualizado != null ?
                          c.prazoMaximoPostagemAtualizado : c.prazoFinalPedido) :
                          c.prazoMaximoPostagemAtualizado != null ?
                          c.prazoMaximoPostagemAtualizado :
                          c.prazoFinalPedido == null ? DateTime.Now : c.prazoFinalPedido),
                          c.dataHoraDoPedido
                          select c).ToList();

        var pedidosTotal = pedidosCd1e2.Union(pedidosCd2);

        var pedidoSelecionado = pedidosTotal.FirstOrDefault();
        Response.Write(pedidoSelecionado.pedidoId);
    }
}