﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace carrinho
{
	[Serializable]
    public class Carrinho
    {
        private List<CarrinhoItem> _itens = new List<CarrinhoItem>();

        public List<CarrinhoItem> Itens
        {
            get { return _itens; }
        }
    }

    [Serializable]
    public class CarrinhoItem
    {
        private int _produtoId;
        private string _produtoIdDaEmpresa;
        private string _produtoCodDeBarras;
        private int _garantiaId;
        private string _garantiaNome;
        private int _quantidade;
        private string _produtoNome;
        private string _produtoNomeNoCarrinho;
        private decimal _preco;
        private decimal _precoDeCusto;
        private decimal _precoPromocional;
        private decimal _precoAtacado;
        private decimal _precoNoCarrinho;
        private int _quantidadeMinimaParaAtacado;
        private double _peso;
        private bool _freteGratis;
        private bool _promocao;
        private bool _itemPresente;
        private string _itemMensagemDoCartao;
        private decimal _valorDoEmbrulhoECartao;
        private decimal _valorTotal;

        public int produtoId
        {
            get { return _produtoId; }
            set { _produtoId = value; }
        }
        public string produtoIdDaEmpresa
        {
            get { return _produtoIdDaEmpresa; }
            set { _produtoIdDaEmpresa = value; }
        }
        public string produtoCodDeBarras
        {
            get { return _produtoCodDeBarras; }
            set { _produtoCodDeBarras = value; }
        }
        public int garantiaId
        {
            get { return _garantiaId; }
            set { _garantiaId = value; }
        }
        public string garantiaNome
        {
            get { return _garantiaNome; }
            set { _garantiaNome = value; }
        }
        public int quantidade
        {
            get { return _quantidade; }
            set { _quantidade = value; }
        }
        public string produtoNome
        {
            get { return _produtoNome; }
            set { _produtoNome = value; }
        }
        public string produtoNomeNoCarrinho
        {
            get { return _produtoNomeNoCarrinho; }
            set { _produtoNomeNoCarrinho = value; }
        }
        public decimal preco
        {
            get { return _preco; }
            set { _preco = value; }
        }
        public decimal precoDeCusto
        {
            get { return _precoDeCusto; }
            set { _precoDeCusto = value; }
        }
        public decimal precoPromocional
        {
            get { return _precoPromocional; }
            set { _precoPromocional = value; }
        }
        public decimal precoAtacado
        {
            get { return _precoAtacado; }
            set { _precoAtacado = value; }
        }
        public decimal precoNoCarrinho
        {
            get { return _precoNoCarrinho; }
            set { _precoNoCarrinho = value; }
        }
        public int quantidadeMinimaParaAtacado
        {
            get { return _quantidadeMinimaParaAtacado; }
            set { _quantidadeMinimaParaAtacado = value; }
        }
        public double peso
        {
            get { return _peso; }
            set { _peso = value; }
        }
        public bool freteGratis
        {
            get { return _freteGratis; }
            set { _freteGratis = value; }
        }
        public bool promocao
        {
            get { return _promocao; }
            set { _promocao = value; }
        }
        public bool itemPresente
        {
            get { return _itemPresente; }
            set { _itemPresente = value; }
        }
        public string itemMensagemDoCartao
        {
            get { return _itemMensagemDoCartao; }
            set { _itemMensagemDoCartao = value; }
        }
        public decimal valorDoEmbrulhoECartao
        {
            get { return _valorDoEmbrulhoECartao; }
            set { _valorDoEmbrulhoECartao = value; }
        }
        public decimal valorTotal
        {
            get { return _valorTotal; }
            set { _valorTotal = value; }
        }

        public CarrinhoItem() { }

        public CarrinhoItem(int produtoId, string produtoIdDaEmpresa, string produtoCodDeBarras, int garantiaId, string garantiaNome, int quantidade, string produtoNome, string produtoNomeNoCarrinho, decimal preco, decimal precoDeCusto, decimal precoPromocional, decimal precoAtacado, decimal precoNoCarrinho, int quantidadeMinimaParaAtacado, double peso, bool freteGratis, bool promocao, bool itemPresente, string itemMensagemDoCartao, decimal valorDoEmbrulhoECartao, decimal valorTotal)
        {
            _produtoId = produtoId;
            _produtoIdDaEmpresa = produtoIdDaEmpresa;
            _produtoCodDeBarras = produtoCodDeBarras;
            _garantiaId = garantiaId;
            _garantiaNome = garantiaNome;
            _quantidade = quantidade;
            _produtoNome = produtoNome;
            _produtoNomeNoCarrinho = produtoNomeNoCarrinho;
            _preco = preco;
            _precoDeCusto = precoDeCusto;
            _precoPromocional = precoPromocional;
            _precoAtacado = precoAtacado;
            _precoNoCarrinho = precoNoCarrinho;
            _quantidadeMinimaParaAtacado = quantidadeMinimaParaAtacado;
            _peso = peso;
            _freteGratis = freteGratis;
            _promocao = promocao;
            _itemPresente = itemPresente;
            _itemMensagemDoCartao = itemMensagemDoCartao;
            _valorDoEmbrulhoECartao = valorDoEmbrulhoECartao;
            _valorTotal = valorTotal;
        }
    }
}