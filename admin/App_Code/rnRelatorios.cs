﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Web.Configuration;
using System.Collections;

/// <summary>
/// Summary description for rnRelatorios
/// </summary>
public class rnRelatorios
{
    public static DataSet vendasPorPeriodo(string dataInicial, string dataFinal)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("vendasPorPeriodo");

        db.AddInParameter(dbCommand, "dataInicial", DbType.String, dataInicial);
        db.AddInParameter(dbCommand, "dataFinal", DbType.String, dataFinal);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }


    public static DataSet quebraPorData(string data)
    {
        string dataRelatorio = data;
        if (string.IsNullOrEmpty(data)) dataRelatorio = DateTime.Now.ToShortDateString();

        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("relatorioQuebraPorDia");

        db.AddInParameter(dbCommand, "data", DbType.String, Convert.ToDateTime(dataRelatorio).Year + "-" + Convert.ToDateTime(dataRelatorio).Month + "-" + Convert.ToDateTime(dataRelatorio).Day);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet vendasDeProdutoPorPeriodo(string dataInicial, string dataFinal)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("vendasDeProdutoPorPeriodo");

        db.AddInParameter(dbCommand, "dataInicial", DbType.String, dataInicial);
        db.AddInParameter(dbCommand, "dataFinal", DbType.String, dataFinal);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }
    public static DataSet vendasDeProdutoPorPeriodoCombo(string dataInicial, string dataFinal)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("vendasDeProdutoPorPeriodoCombo");

        db.AddInParameter(dbCommand, "dataInicial", DbType.String, dataInicial);
        db.AddInParameter(dbCommand, "dataFinal", DbType.String, dataFinal);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet vendasDeClientesPorPeriodo(string dataInicial, string dataFinal)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("vendasDeClientesPorPeriodo");

        db.AddInParameter(dbCommand, "dataInicial", DbType.String, dataInicial);
        db.AddInParameter(dbCommand, "dataFinal", DbType.String, dataFinal);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet relatorioGeral(string dataInicial, string dataFinal)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("relatorioGeral");

        db.AddInParameter(dbCommand, "dataInicial", DbType.String, dataInicial);
        db.AddInParameter(dbCommand, "dataFinal", DbType.String, dataFinal);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet relatorioEntregasPorPeriodo(string dataInicial, string dataFinal)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("entregasPorPeriodo");

        db.AddInParameter(dbCommand, "dataInicial", DbType.String, dataInicial);
        db.AddInParameter(dbCommand, "dataFinal", DbType.String, dataFinal);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet entradaDeProdutoPorPeriodo(string dataInicial, string dataFinal, string fornecedorId)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("entradaDeProdutoPorPeriodo");

        db.AddInParameter(dbCommand, "dataInicial", DbType.String, dataInicial);
        db.AddInParameter(dbCommand, "dataFinal", DbType.String, dataFinal);
        db.AddInParameter(dbCommand, "fornecedorId", DbType.Int32, fornecedorId);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet pedidoDeProdutoPorPeriodo(string dataInicial, string dataFinal, string fornecedorId)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("pedidoDeProdutoPorPeriodo");

        db.AddInParameter(dbCommand, "dataInicial", DbType.String, dataInicial);
        db.AddInParameter(dbCommand, "dataFinal", DbType.String, dataFinal);
        db.AddInParameter(dbCommand, "fornecedorId", DbType.Int32, fornecedorId);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet entradaDeProdutoPorData(string data, string fornecedorId)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("entradaDeProdutoPorData");

        db.AddInParameter(dbCommand, "data", DbType.String, Convert.ToDateTime(data).ToString("yyyy-MM-dd"));
        db.AddInParameter(dbCommand, "fornecedorId", DbType.Int32, fornecedorId);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

}
