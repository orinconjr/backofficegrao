﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

/// <summary>
/// Summary description for Funcoes
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class Funcoes : System.Web.Services.WebService {

    public Funcoes () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public string HelloWorld() {
        return "Hello World";
    }

    [WebMethod]
    public static int retornaPrazoDiasUteis(int prazo, int prazoInicioFabricacao, DateTime dataInicio)
    {
        return rnFuncoes.retornaPrazoDiasUteis(prazo, prazoInicioFabricacao, dataInicio);
    }

}
