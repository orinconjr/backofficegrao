﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Services;
using rakutenPedidos;

/// <summary>
/// Summary description for EmbalagemWs
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class EmbalagemWs : System.Web.Services.WebService
{

    public class pesosPacotesGravar
    {
        public int volume { get; set; }
        public int peso { get; set; }
        public int altura { get; set; }
        public int largura { get; set; }
        public int profundidade { get; set; }
    }
    public class rastreiosGravar
    {
        public string rastreio { get; set; }
    }
    public class embalarRetornoLogar
    {
        public int idUsuarioExpedicao { get; set; }
        public int idPedido { get; set; }
        public string funcao { get; set; }
    }
    public class embalarRetornoContar
    {
        public int totalPedidos { get; set; }
        public int totalEmbalados { get; set; }
    }
    public class embalarRetornoSelecionarPedido
    {
        public int pedidoId { get; set; }
        public bool sucesso { get; set; }
    }
    public class retornoEtiqueta
    {
        public string telResidencial { get; set; }
        public string telComercial { get; set; }
        public string telCelular { get; set; }
        public string nome { get; set; }
        public string rua { get; set; }
        public string numero { get; set; }
        public string complemento { get; set; }
        public string bairro { get; set; }
        public string cidade { get; set; }
        public string estado { get; set; }
        public string cep { get; set; }
        public string referencia { get; set; }
        public string cnpjEmissao { get; set; }
        public string notaEmissao { get; set; }
        public string totalVolumes { get; set; }
    }

    public class retornoFinalizaPedido
    {
        public string formaEnvio { get; set; }
        public retornoEtiqueta etiqueta { get; set; }
        public string cnpj { get; set; }
        public string nfeNumero { get; set; }
        public string totalVolumes { get; set; }
        public string totalVolumesCd1 { get; set; }
        public string totalVolumesCd2 { get; set; }
        public List<string> produtosCd2 { get; set; }
        public int pedidoId { get; set; }
    }
    public EmbalagemWs()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public embalarRetornoLogar Logar(string senha)
    {
        var usuarioDc = new dbCommerceDataContext();
        var usuarioExpedicao = (from c in usuarioDc.tbUsuarioExpedicaos where c.senha == senha && c.ativo == true select c).FirstOrDefault();
        var retorno = new embalarRetornoLogar();
        if (usuarioExpedicao != null)
        {
            var pedidosDc = new dbCommerceDataContext();
            retorno.idUsuarioExpedicao = usuarioExpedicao.idUsuarioExpedicao;
            var pedidosEnvio = (from c in pedidosDc.tbPedidoEnvios
                                where c.idUsuarioEmbalagem == usuarioExpedicao.idUsuarioExpedicao && c.dataFimEmbalagem == null
                                orderby c.dataInicioEmbalagem
                                select c).FirstOrDefault();
            if (pedidosEnvio == null)
            {
                retorno.funcao = "selecionarpedido";
            }
            else
            {
                retorno.idPedido = pedidosEnvio.idPedido;
                if (pedidosEnvio.volumesEmbalados > 0)
                {
                    var pacotes = (from c in pedidosDc.tbPedidoPacotes where c.idPedido == pedidosEnvio.idPedido && (c.concluido ?? true) == false select c).Count();
                    if (pacotes < pedidosEnvio.volumesEmbalados)
                    {
                        retorno.funcao = "conferirpacotes";
                    }
                    else
                    {
                        retorno.funcao = "conferirpacotes";
                    }
                }
                else
                {
                    using (
                        var txn =
                            new System.Transactions.TransactionScope(
                                System.Transactions.TransactionScopeOption.Required,
                                new System.Transactions.TransactionOptions
                                {
                                    IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                                }))

                    {
                        var produtosAguardandoConferencia = (from c in pedidosDc.tbProdutoEstoques
                                                             where
                                                                 c.idPedidoEnvio == pedidosEnvio.idPedidoEnvio &&
                                                                 (c.conferidoEmbalagem ?? false) == false
                                                             select c).Any();
                        if (produtosAguardandoConferencia)
                        {
                            retorno.funcao = "conferirprodutos";
                        }
                        else
                        {
                            retorno.funcao = "quantidadepacotes";
                        }
                    }
                }
            }
        }
        else
        {
            retorno.idUsuarioExpedicao = 0;
        }
        return retorno;
    }

    [WebMethod]
    public embalarRetornoLogar LogarCds(string senha, int idCentroDistribuicao)
    {
        var usuarioDc = new dbCommerceDataContext();
        var usuarioExpedicao = (from c in usuarioDc.tbUsuarioExpedicaos where c.senha == senha && 
                                c.idCentroDistribuicao == idCentroDistribuicao && c.ativo == true
                                select c).FirstOrDefault();
        var retorno = new embalarRetornoLogar();
        if (usuarioExpedicao != null)
        {
            var pedidosDc = new dbCommerceDataContext();
            retorno.idUsuarioExpedicao = usuarioExpedicao.idUsuarioExpedicao;
            var pedidosEnvio = (from c in pedidosDc.tbPedidoEnvios
                                where c.idUsuarioEmbalagem == usuarioExpedicao.idUsuarioExpedicao && c.dataFimEmbalagem == null
                                orderby c.dataInicioEmbalagem
                                select c).FirstOrDefault();
            if (pedidosEnvio == null)
            {
                retorno.funcao = "selecionarpedido";
            }
            else
            {
                retorno.idPedido = pedidosEnvio.idPedido;
                if (pedidosEnvio.volumesEmbalados > 0)
                {
                    var pacotes = (from c in pedidosDc.tbPedidoPacotes where c.idPedido == pedidosEnvio.idPedido && (c.concluido ?? true) == false select c).Count();
                    if (pacotes < pedidosEnvio.volumesEmbalados)
                    {
                        retorno.funcao = "conferirpacotes";
                    }
                    else
                    {
                        retorno.funcao = "conferirpacotes";
                    }
                }
                else
                {
                    using (
                        var txn =
                            new System.Transactions.TransactionScope(
                                System.Transactions.TransactionScopeOption.Required,
                                new System.Transactions.TransactionOptions
                                {
                                    IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                                }))

                    {
                        var produtosAguardandoConferencia = (from c in pedidosDc.tbProdutoEstoques
                                                             where
                                                                 c.idPedidoEnvio == pedidosEnvio.idPedidoEnvio &&
                                                                 (c.conferidoEmbalagem ?? false) == false
                                                             select c).Any();
                        if (produtosAguardandoConferencia)
                        {
                            retorno.funcao = "conferirprodutos";
                        }
                        else
                        {
                            retorno.funcao = "quantidadepacotes";
                        }
                    }
                }
            }
        }
        else
        {
            retorno.idUsuarioExpedicao = 0;
        }
        return retorno;
    }

    [WebMethod]
    public embalarRetornoContar ContarPedidos(int idUsuarioExpedicao)
    {
        var retorno = new embalarRetornoContar();
        var pedidosDc = new dbCommerceDataContext();
        var sendoEmbalados = rnPedidos.verificaPedidosCompletosSeparadosCd1e2().Where(x => x.separado).ToList();
        //var sendoEmbalados = rnPedidos.verificaPedidosCompletos().Where(x => x.separado).ToList();
        retorno.totalPedidos = sendoEmbalados.Count();

        //var hoje = DateTime.Now;
        //var pedidosHoje =
        //    (from c in pedidosDc.tbPedidoEnvios
        //     where c.dataFimEmbalagem != null && c.idUsuarioEmbalagem == idUsuarioExpedicao
        //     select c);
        //pedidosHoje = pedidosHoje.Where(x => x.dataFimEmbalagem.Value.Date == hoje.Date);
        //retorno.totalEmbalados = pedidosHoje.Count();
        retorno.totalEmbalados = 0;
        return retorno;
    }


    [WebMethod]
    public embalarRetornoContar ContarPedidosCd4(int idUsuarioExpedicao)
    {
        var retorno = new embalarRetornoContar();
        var pedidosDc = new dbCommerceDataContext();
        var sendoEmbalados = rnPedidos.verificaIdsPedidosCompletos(4, true, null, new List<int>());
        //var sendoEmbalados = rnPedidos.verificaPedidosCompletos().Where(x => x.separado).ToList();
        retorno.totalPedidos = sendoEmbalados.Count();

        //var hoje = DateTime.Now;
        //var pedidosHoje =
        //    (from c in pedidosDc.tbPedidoEnvios
        //     where c.dataFimEmbalagem != null && c.idUsuarioEmbalagem == idUsuarioExpedicao
        //     select c);
        //pedidosHoje = pedidosHoje.Where(x => x.dataFimEmbalagem.Value.Date == hoje.Date);
        //retorno.totalEmbalados = pedidosHoje.Count();
        retorno.totalEmbalados = 0;
        return retorno;
    }

    [WebMethod]
    public embalarRetornoContar ContarPedidosCd5(int idUsuarioExpedicao)
    {
        var retorno = new embalarRetornoContar();
        var data = new dbCommerceDataContext();
        var pedidosCd4e5 = (from c in data.tbPedidoPacotes
                            where c.idCentroDistribuicao == 5 && c.etiquetaImpressa == false && (c.concluido ?? false) == true
                            select new
                            {
                                c.tbPedido.pedidoId,
                                c.idPedidoEnvio
                            }).ToList();
        var pedidosCd5 = (from c in data.tbPedidos
                          join d in data.tbItemPedidoEstoques on c.pedidoId equals d.tbItensPedido.pedidoId into itensEstoque
                          join e in data.tbNotaFiscals on c.pedidoId equals e.idPedido into notas
                          join f in data.tbProdutoEstoques on c.pedidoId equals f.pedidoId into itensSeparados
                          where c.statusDoPedido == 11 && c.itensPendentesEnvioCd4 == 0 && c.envioLiberadoCd5 == true && c.itensPendentesEnvioCd5 > 0 && c.separadoCd5 == true
                          orderby (c.entregaEnviarPor == "JadlogExpressa" ? 0 : c.prioridadeEnvio == null ? 9999 : c.prioridadeEnvio),
                          (
                          notas.OrderByDescending(x => x.idNotaFiscal).FirstOrDefault() != null ?
                          (notas.OrderByDescending(x => x.idNotaFiscal).FirstOrDefault().dataHora < c.prazoFinalPedido ?
                          notas.OrderByDescending(x => x.idNotaFiscal).FirstOrDefault().dataHora :
                          c.prazoMaximoPostagemAtualizado != null ?
                          c.prazoMaximoPostagemAtualizado : c.prazoFinalPedido) :
                          c.prazoMaximoPostagemAtualizado != null ?
                          c.prazoMaximoPostagemAtualizado :
                          c.prazoFinalPedido == null ? DateTime.Now : c.prazoFinalPedido),
                          c.dataHoraDoPedido
                          select new
                          {
                              c.pedidoId,
                              itensSeparados.FirstOrDefault(x => x.enviado == false && x.idCentroDistribuicao == 5).idPedidoEnvio
                          }).ToList();

        var pedidosTotal = pedidosCd4e5.Union(pedidosCd5);
        //var sendoEmbalados = rnPedidos.verificaPedidosCompletos().Where(x => x.separado).ToList();
        retorno.totalPedidos = pedidosTotal.Count();

        //var hoje = DateTime.Now;
        //var pedidosHoje =
        //    (from c in pedidosDc.tbPedidoEnvios
        //     where c.dataFimEmbalagem != null && c.idUsuarioEmbalagem == idUsuarioExpedicao
        //     select c);
        //pedidosHoje = pedidosHoje.Where(x => x.dataFimEmbalagem.Value.Date == hoje.Date);
        //retorno.totalEmbalados = pedidosHoje.Count();
        retorno.totalEmbalados = 0;
        return retorno;
    }

    [WebMethod]
    public embalarRetornoContar ContarPedidosCd2(int idUsuarioExpedicao)
    {
        var retorno = new embalarRetornoContar();
        var data = new dbCommerceDataContext();
        var pedidosCd1e2 = (from c in data.tbPedidoPacotes
                            where c.idCentroDistribuicao == 2 && c.etiquetaImpressa == false && (c.concluido ?? false) == true
                            select new
                            {
                                c.tbPedido.pedidoId,
                                c.idPedidoEnvio
                            }).ToList();
        var pedidosCd2 = (from c in data.tbPedidos
                          join d in data.tbItemPedidoEstoques on c.pedidoId equals d.tbItensPedido.pedidoId into itensEstoque
                          join e in data.tbNotaFiscals on c.pedidoId equals e.idPedido into notas
                          join f in data.tbProdutoEstoques on c.pedidoId equals f.pedidoId into itensSeparados
                          where c.statusDoPedido == 11 &&
                          c.itensPendetenEnvioCd2 > 0 && c.separadoCd2 == true
                          orderby (c.entregaEnviarPor == "JadlogExpressa" ? 0 : c.prioridadeEnvio == null ? 9999 : c.prioridadeEnvio),
                          (
                          notas.OrderByDescending(x => x.idNotaFiscal).FirstOrDefault() != null ?
                          (notas.OrderByDescending(x => x.idNotaFiscal).FirstOrDefault().dataHora < c.prazoFinalPedido ?
                          notas.OrderByDescending(x => x.idNotaFiscal).FirstOrDefault().dataHora :
                          c.prazoMaximoPostagemAtualizado != null ?
                          c.prazoMaximoPostagemAtualizado : c.prazoFinalPedido) :
                          c.prazoMaximoPostagemAtualizado != null ?
                          c.prazoMaximoPostagemAtualizado :
                          c.prazoFinalPedido == null ? DateTime.Now : c.prazoFinalPedido),
                          c.dataHoraDoPedido
                          select new
                          {
                              c.pedidoId,
                              itensSeparados.FirstOrDefault(x => x.enviado == false && x.idCentroDistribuicao == 2).idPedidoEnvio
                          }).ToList();

        var pedidosTotal = pedidosCd1e2.Union(pedidosCd2);

        retorno.totalPedidos = pedidosTotal.Count();
        retorno.totalEmbalados = 0;
        return retorno;
    }

    [WebMethod]
    public embalarRetornoContar ContarPedidosCd3(int idUsuarioExpedicao)
    {
        var retorno = new embalarRetornoContar();
        var data = new dbCommerceDataContext();
        var pedidosCd3 = (from c in data.tbPedidos
                          join d in data.tbItemPedidoEstoques on c.pedidoId equals d.tbItensPedido.pedidoId into itensEstoque
                          join f in data.tbProdutoEstoques on c.pedidoId equals f.pedidoId into itensSeparados
                          where c.statusDoPedido == 11 && itensEstoque.Count(x => x.cancelado == false && x.enviado == false) > 0 &&
                          (itensEstoque.Count(x => x.cancelado == false && x.enviado == false && x.reservado == true && x.idCentroDistribuicao == 3) <= itensSeparados.Count(x => x.idCentroDistribuicao == 3 && x.enviado == false)) &&
                          itensEstoque.Count(x => x.cancelado == false && x.enviado == false && x.reservado == true && x.idCentroDistribuicao == 3) > 0
                          orderby (c.entregaEnviarPor == "JadlogExpressa" ? 0 : c.prioridadeEnvio == null ? 9999 : c.prioridadeEnvio),
                                (c.prazoMaximoPostagemAtualizado != null ?
                                c.prazoMaximoPostagemAtualizado :
                                c.prazoFinalPedido == null ? DateTime.Now : c.prazoFinalPedido),
                                c.dataHoraDoPedido
                          select c).ToList();

        retorno.totalPedidos = pedidosCd3.Count();
        retorno.totalEmbalados = 0;
        return retorno;
    }

    [WebMethod]
    public embalarRetornoSelecionarPedido SelecionarPedido(int idUsuarioExpedicao)
    {
        var retorno = new embalarRetornoSelecionarPedido();
        var pedidosDc = new dbCommerceDataContext();
        var pedidosEnvio = (from c in pedidosDc.tbPedidoEnvios
                            where c.idUsuarioEmbalagem == idUsuarioExpedicao && c.dataFimEmbalagem == null
                            orderby c.dataInicioEmbalagem
                            select c).FirstOrDefault();
        if (pedidosEnvio != null)
        {
            retorno.pedidoId = pedidosEnvio.idPedido;
            retorno.sucesso = true;
            return retorno;
        }


        //var sendoEmbalados = rnPedidos.verificaPedidosCompletos().Where(x => x.separado).ToList();

        var sendoEmbaladosTotal = rnPedidos.verificaPedidosCompletosSeparadosCd1e2().Where(x => x.separado).ToList();
        var sendoEmbalados = sendoEmbaladosTotal.ToList().FirstOrDefault();
        if (sendoEmbalados != null)
        {
            int tentativas = 0;
            int tentativasMaximas = 200;
            int tempo = 0;
            bool sucesso = false;
            while (tentativas <= tentativasMaximas && sucesso == false)
            {
                try
                {
                    var pedido = (from c in pedidosDc.tbPedidos where c.pedidoId == sendoEmbalados.pedidoId select c).FirstOrDefault();
                    var envio = (from c in pedidosDc.tbPedidoEnvios
                                 where c.idPedido == sendoEmbalados.pedidoId && c.idUsuarioEmbalagem == null && c.idCentroDeDistribuicao < 3
                                 orderby c.idPedidoEnvio descending
                                 select c).FirstOrDefault();
                    if (envio != null)
                    {
                        try
                        {
                            pedido.idUsuarioEmbalado = idUsuarioExpedicao;
                            pedido.dataSendoEmbalado = DateTime.Now;
                            pedido.dataEmbaladoFim = null;
                            pedido.volumesEmbalados = null;
                            pedido.compactarPacote = false;
                            pedido.statusDoPedido = 4;
                            pedidosDc.SubmitChanges();
                        }
                        catch (System.Data.Linq.ChangeConflictException ex)
                        {
                            foreach (System.Data.Linq.ObjectChangeConflict objConflict in pedidosDc.ChangeConflicts)
                            {
                                foreach (System.Data.Linq.MemberChangeConflict memberConflict in objConflict.MemberConflicts)
                                {
                                    memberConflict.Resolve(System.Data.Linq.RefreshMode.KeepCurrentValues);
                                }
                            }
                            pedidosDc.SubmitChanges(System.Data.Linq.ConflictMode.ContinueOnConflict);
                        }

                        try
                        {
                            envio.idUsuarioEmbalagem = idUsuarioExpedicao;
                            envio.dataInicioEmbalagem = DateTime.Now;
                            pedidosDc.SubmitChanges();
                        }
                        catch (System.Data.Linq.ChangeConflictException ex)
                        {
                            foreach (System.Data.Linq.ObjectChangeConflict objConflict in pedidosDc.ChangeConflicts)
                            {
                                foreach (System.Data.Linq.MemberChangeConflict memberConflict in objConflict.MemberConflicts)
                                {
                                    memberConflict.Resolve(System.Data.Linq.RefreshMode.KeepCurrentValues);
                                }
                            }
                            pedidosDc.SubmitChanges(System.Data.Linq.ConflictMode.ContinueOnConflict);
                        }

                        rnPedidos.pedidoAlteraStatus(4, pedido.pedidoId);
                        var usuarioDc = new dbCommerceDataContext();
                        var usuarioExpedicao = (from c in usuarioDc.tbUsuarioExpedicaos where c.idUsuarioExpedicao == idUsuarioExpedicao select c).FirstOrDefault();
                        rnInteracoes.interacaoInclui(pedido.pedidoId, "Seu pedido está sendo embalado", usuarioExpedicao.nome, "True");
                        sucesso = true;
                    }
                    else
                    {
                        var data = new dbCommerceDataContext();
                        var agora = DateTime.Now;
                        var queue = new tbQueue();
                        queue.agendamento = agora;
                        queue.tipoQueue = 25;
                        queue.mensagem = "Erro selecionar pedido " + sendoEmbalados.pedidoId;
                        queue.concluido = false;
                        queue.andamento = false;
                        data.tbQueues.InsertOnSubmit(queue);
                        data.SubmitChanges();
                        tentativas++;
                        sendoEmbalados = sendoEmbaladosTotal.Skip(tentativas).FirstOrDefault();
                    }
                }
                catch (Exception ex)
                {
                    tentativas++;
                    Thread.Sleep(new TimeSpan(0, 0, tempo));
                    tempo++;
                }
            }

            if (sucesso)
            {
                retorno.pedidoId = sendoEmbalados.pedidoId;
                retorno.sucesso = true;
            }
            else
            {
                retorno.sucesso = false;
            }
        }
        else
        {
            retorno.sucesso = false;
        }
        return retorno;
    }


    [WebMethod]
    public embalarRetornoSelecionarPedido SelecionarPedidoCd4(int idUsuarioExpedicao)
    {
        return SelecionarPedidoCd4Maquina(idUsuarioExpedicao, 0);
    }


    [WebMethod]
    public embalarRetornoSelecionarPedido SelecionarPedidoCd4Maquina(int idUsuarioExpedicao, int idComputadorExpedicao)
    {
        var retorno = new embalarRetornoSelecionarPedido();
        var pedidosDc = new dbCommerceDataContext();
        var pedidosEnvio = (from c in pedidosDc.tbPedidoEnvios
                            where c.idUsuarioEmbalagem == idUsuarioExpedicao && c.dataFimEmbalagem == null
                            orderby c.dataInicioEmbalagem
                            select c).FirstOrDefault();
        if (pedidosEnvio != null)
        {
            retorno.pedidoId = pedidosEnvio.idPedido;
            retorno.sucesso = true;
            return retorno;
        }

        var usuarioExpedicao = (from c in pedidosDc.tbUsuarioExpedicaos where c.idUsuarioExpedicao == idUsuarioExpedicao select c).FirstOrDefault();
        var usuarioExpedicaoEnderecosSeparacao = (from c in pedidosDc.tbUsuarioExpedicaoEnderecoSeparacaos where c.idUsuarioExpedicao == idUsuarioExpedicao select c.idEnderecoSeparacao).ToList();
        //var sendoEmbalados = rnPedidos.verificaPedidosCompletos().Where(x => x.separado).ToList();

        //var sendoEmbaladosTotal = rnPedidos.verificaIdsPedidosCompletosSeparadosCd4e5(4, true, null, usuarioExpedicaoEnderecosSeparacao, idComputadorExpedicao).Where(x => x > 0).ToList();
        var sendoEmbaladosTotal = rnPedidos.verificaIdsPedidosCompletosSeparadosCd4e5(4, true, null, new List<int>(), idComputadorExpedicao).Where(x => x > 0).ToList();

        var sendoEmbalados = sendoEmbaladosTotal.ToList().FirstOrDefault();
        if (sendoEmbalados != null)
        {
            int tentativas = 0;
            int tentativasMaximas = 50;
            int tempo = 0;
            bool sucesso = false;
            while (tentativas <= tentativasMaximas && sucesso == false)
            {
                try
                {
                    var pedido = (from c in pedidosDc.tbPedidos where c.pedidoId == sendoEmbalados select c).FirstOrDefault();
                    var envio = (from c in pedidosDc.tbPedidoEnvios
                                 where c.idPedido == sendoEmbalados && c.idUsuarioEmbalagem == null && c.idCentroDeDistribuicao == 4
                                 orderby c.idPedidoEnvio descending
                                 select c).FirstOrDefault();
                    if (envio != null)
                    {
                        try
                        {
                            pedido.idUsuarioEmbalado = idUsuarioExpedicao;
                            pedido.dataSendoEmbalado = DateTime.Now;
                            pedido.dataEmbaladoFim = null;
                            pedido.volumesEmbalados = null;
                            pedido.compactarPacote = false;
                            pedido.statusDoPedido = 4;
                            pedidosDc.SubmitChanges();
                        }
                        catch (System.Data.Linq.ChangeConflictException ex)
                        {
                            foreach (System.Data.Linq.ObjectChangeConflict objConflict in pedidosDc.ChangeConflicts)
                            {
                                foreach (System.Data.Linq.MemberChangeConflict memberConflict in objConflict.MemberConflicts)
                                {
                                    memberConflict.Resolve(System.Data.Linq.RefreshMode.KeepCurrentValues);
                                }
                            }
                            pedidosDc.SubmitChanges(System.Data.Linq.ConflictMode.ContinueOnConflict);
                        }

                        try
                        {
                            envio.idUsuarioEmbalagem = idUsuarioExpedicao;
                            envio.dataInicioEmbalagem = DateTime.Now;
                            pedidosDc.SubmitChanges();
                        }
                        catch (System.Data.Linq.ChangeConflictException ex)
                        {
                            foreach (System.Data.Linq.ObjectChangeConflict objConflict in pedidosDc.ChangeConflicts)
                            {
                                foreach (System.Data.Linq.MemberChangeConflict memberConflict in objConflict.MemberConflicts)
                                {
                                    memberConflict.Resolve(System.Data.Linq.RefreshMode.KeepCurrentValues);
                                }
                            }
                            pedidosDc.SubmitChanges(System.Data.Linq.ConflictMode.ContinueOnConflict);
                        }

                        rnPedidos.pedidoAlteraStatus(4, pedido.pedidoId);
                        rnInteracoes.interacaoInclui(pedido.pedidoId, "Seu pedido está sendo embalado", usuarioExpedicao.nome, "True");
                        sucesso = true;
                    }
                    else
                    {
                        sendoEmbaladosTotal = rnPedidos.verificaIdsPedidosCompletos(4, true, null, new List<int>()).ToList();
                        //rnEmails.EnviaEmail("posvendas@graodegente.com.br", "andre@graodegente.com.br", "", "", "", sendoEmbalados.ToString(), "Erro selecionar pedido " + sendoEmbalados);
                        var data = new dbCommerceDataContext();
                        var agora = DateTime.Now;
                        var queue = new tbQueue();
                        queue.agendamento = agora;
                        queue.tipoQueue = 25;
                        queue.mensagem = "Erro selecionar pedido " + sendoEmbalados;
                        queue.concluido = false;
                        queue.andamento = false;
                        data.tbQueues.InsertOnSubmit(queue);
                        data.SubmitChanges();
                        tentativas++;
                        sendoEmbalados = sendoEmbaladosTotal.Skip(tentativas).FirstOrDefault();
                        //pedido.separado = false;
                        //pedido.idUsuarioSeparacao = null;
                        //pedido.dataInicioSeparacao = null;
                        //pedido.dataFimSeparacao = null;
                        //pedidosDc.SubmitChanges();
                    }
                }
                catch (Exception ex)
                {
                    tentativas++;
                    sendoEmbalados = sendoEmbaladosTotal.Skip(tentativas).FirstOrDefault();
                    rnEmails.EnviaEmail("", "andre@graodegente.com.br", "", "", "", sendoEmbalados.ToString(), "Erro selecionar pedido " + sendoEmbalados);
                }
            }

            if (sucesso)
            {
                retorno.pedidoId = sendoEmbalados;
                retorno.sucesso = true;
            }
            else
            {
                retorno.sucesso = false;
            }
        }
        else
        {
            retorno.sucesso = false;
        }
        return retorno;
    }

    [WebMethod]
    public retornoFinalizaPedido SelecionarPedidoCd2(int idUsuarioExpedicao)
    {
        var retorno = new retornoFinalizaPedido();
        var data = new dbCommerceDataContext();
        var pedidosCd1e2 = (from c in data.tbPedidoPacotes
                            where c.idCentroDistribuicao == 2 && c.etiquetaImpressa == false && (c.concluido ?? false) == true
                            select new
                            {
                                c.tbPedido.pedidoId,
                                c.idPedidoEnvio
                            }).ToList();
        var pedidosCd2 = (from c in data.tbPedidos
                          join d in data.tbItemPedidoEstoques on c.pedidoId equals d.tbItensPedido.pedidoId into itensEstoque
                          join e in data.tbNotaFiscals on c.pedidoId equals e.idPedido into notas
                          join f in data.tbProdutoEstoques on c.pedidoId equals f.pedidoId into itensSeparados
                          where c.statusDoPedido == 11 && c.envioLiberadoCd2 == true && c.itensPendetenEnvioCd2 > 0 && c.separadoCd2 == true
                          orderby (c.entregaEnviarPor == "JadlogExpressa" ? 0 : c.prioridadeEnvio == null ? 9999 : c.prioridadeEnvio),
                          (
                          notas.OrderByDescending(x => x.idNotaFiscal).FirstOrDefault() != null ?
                          (notas.OrderByDescending(x => x.idNotaFiscal).FirstOrDefault().dataHora < c.prazoFinalPedido ?
                          notas.OrderByDescending(x => x.idNotaFiscal).FirstOrDefault().dataHora :
                          c.prazoMaximoPostagemAtualizado != null ?
                          c.prazoMaximoPostagemAtualizado : c.prazoFinalPedido) :
                          c.prazoMaximoPostagemAtualizado != null ?
                          c.prazoMaximoPostagemAtualizado :
                          c.prazoFinalPedido == null ? DateTime.Now : c.prazoFinalPedido),
                          c.dataHoraDoPedido
                          select new
                          {
                              c.pedidoId,
                              itensSeparados.FirstOrDefault(x => x.enviado == false && x.idCentroDistribuicao == 2).idPedidoEnvio
                          }).ToList();

        var pedidosTotal = pedidosCd1e2.Union(pedidosCd2);

        var pedidoSelecionado = pedidosTotal.FirstOrDefault();

        if (pedidoSelecionado != null)
        {
            var envio = (from c in data.tbPedidoEnvios where c.idPedido == pedidoSelecionado.pedidoId && c.idPedidoEnvio == pedidoSelecionado.idPedidoEnvio orderby c.idPedidoEnvio descending select c).First();
            if (envio.dataFimEmbalagem == null)
            {
                retorno = GravaPacotesSemPesoFinalizaPedido(idUsuarioExpedicao, pedidoSelecionado.pedidoId, 0);
            }
            else
            {
                retorno = RetornaDetalhesFinalizacaoPedidoEnvio(idUsuarioExpedicao, pedidoSelecionado.pedidoId, (pedidoSelecionado.idPedidoEnvio ?? 0));
            }
        }

        return retorno;
    }


    [WebMethod]
    public retornoFinalizaPedido SelecionarPedidoCd3(int idUsuarioExpedicao)
    {
        var retorno = new retornoFinalizaPedido();
        var data = new dbCommerceDataContext();
        var pedidosCd3 = (from c in data.tbPedidos
                          join d in data.tbItemPedidoEstoques on c.pedidoId equals d.tbItensPedido.pedidoId into itensEstoque
                          join f in data.tbProdutoEstoques on c.pedidoId equals f.pedidoId into itensSeparados
                          where c.statusDoPedido == 11 && itensEstoque.Count(x => x.cancelado == false && x.enviado == false) > 0 &&
                          (itensEstoque.Count(x => x.cancelado == false && x.enviado == false && x.reservado == true && x.idCentroDistribuicao == 3) <= itensSeparados.Count(x => x.idCentroDistribuicao == 3 && x.enviado == false)) &&
                          itensEstoque.Count(x => x.cancelado == false && x.enviado == false && x.reservado == true && x.idCentroDistribuicao == 3) > 0
                          orderby (c.entregaEnviarPor == "JadlogExpressa" ? 0 : c.prioridadeEnvio == null ? 9999 : c.prioridadeEnvio),
                               (c.prazoMaximoPostagemAtualizado != null ?
                               c.prazoMaximoPostagemAtualizado :
                               c.prazoFinalPedido == null ? DateTime.Now : c.prazoFinalPedido),
                               c.dataHoraDoPedido
                          select c).ToList();

        var pedidoSelecionado = pedidosCd3.FirstOrDefault();

        if (pedidoSelecionado != null)
        {
            var envio = (from c in data.tbPedidoEnvios where c.idPedido == pedidoSelecionado.pedidoId && c.idCentroDeDistribuicao == 3 orderby c.idPedidoEnvio descending select c).First();
            if (envio.dataFimEmbalagem == null)
            {
                retorno = GravaPacotesSemPesoFinalizaPedido(idUsuarioExpedicao, pedidoSelecionado.pedidoId, 0, true, 3, envio.idPedidoEnvio);
            }
            else
            {
                retorno = RetornaDetalhesFinalizacaoPedido(idUsuarioExpedicao, pedidoSelecionado.pedidoId);
            }
        }

        return retorno;
    }

    [WebMethod]
    public retornoFinalizaPedido SelecionarPedidoCd5(int idUsuarioExpedicao)
    {
        var retorno = new retornoFinalizaPedido();
        var data = new dbCommerceDataContext();
        var pedidosCd5 = (from c in data.tbPedidos
                          join d in data.tbItemPedidoEstoques on c.pedidoId equals d.tbItensPedido.pedidoId into itensEstoque
                          join e in data.tbNotaFiscals on c.pedidoId equals e.idPedido into notas
                          join f in data.tbProdutoEstoques on c.pedidoId equals f.pedidoId into itensSeparados
                          where c.statusDoPedido == 11 && c.itensPendentesEnvioCd4 == 0 && c.envioLiberadoCd5 == true && c.itensPendentesEnvioCd5 > 0 && c.separadoCd5 == true
                          orderby (c.entregaEnviarPor == "JadlogExpressa" ? 0 : c.prioridadeEnvio == null ? 9999 : c.prioridadeEnvio),
                          (
                          notas.OrderByDescending(x => x.idNotaFiscal).FirstOrDefault() != null ?
                          (notas.OrderByDescending(x => x.idNotaFiscal).FirstOrDefault().dataHora < c.prazoFinalPedido ?
                          notas.OrderByDescending(x => x.idNotaFiscal).FirstOrDefault().dataHora :
                          c.prazoMaximoPostagemAtualizado != null ?
                          c.prazoMaximoPostagemAtualizado : c.prazoFinalPedido) :
                          c.prazoMaximoPostagemAtualizado != null ?
                          c.prazoMaximoPostagemAtualizado :
                          c.prazoFinalPedido == null ? DateTime.Now : c.prazoFinalPedido),
                          c.dataHoraDoPedido
                          select new
                          {
                              c.pedidoId,
                              itensSeparados.FirstOrDefault(x => x.enviado == false && x.idCentroDistribuicao == 5).idPedidoEnvio
                          }).ToList();
        var pedidosCd4e5 = (from c in data.tbPedidoPacotes
                            where c.idCentroDistribuicao == 5 && c.etiquetaImpressa == false && (c.concluido ?? false) == true
                            select new
                            {
                                c.tbPedido.pedidoId,
                                c.idPedidoEnvio
                            }).ToList();

        var pedidosTotal = pedidosCd4e5.Union(pedidosCd5);
        //var pedidosTotal = pedidosCd5;

        var pedidoSelecionado = pedidosTotal.FirstOrDefault();

        bool erro = true;
        int noLoop = 0;
        int skip = 1;
        while (erro && noLoop < 30)
        {
            noLoop++;
            try
            {
                if (pedidoSelecionado != null)
                {
                    var envio = (from c in data.tbPedidoEnvios where c.idPedido == pedidoSelecionado.pedidoId && c.idPedidoEnvio == pedidoSelecionado.idPedidoEnvio orderby c.idPedidoEnvio descending select c).First();
                    if (envio.dataFimEmbalagem == null)
                    {
                        retorno = GravaPacotesSemPesoFinalizaPedido(idUsuarioExpedicao, pedidoSelecionado.pedidoId, 0);
                    }
                    else
                    {
                        retorno = RetornaDetalhesFinalizacaoPedidoEnvio(idUsuarioExpedicao, pedidoSelecionado.pedidoId, (pedidoSelecionado.idPedidoEnvio ?? 0));
                    }
                }
                erro = false;
            }
            catch(Exception ex)
            {
                rnEmails.EnviaEmail("", "andre@bark.com.br", "", "", "", "Erro Selecioanr Pedido CD5 " + pedidoSelecionado.pedidoId, "Erro Selecioanr Pedido CD5 " + pedidoSelecionado.pedidoId);
                pedidoSelecionado = pedidosTotal.Skip(skip).FirstOrDefault();
                skip++;
            }
        }
        return retorno;
    }


    [WebMethod]
    public retornoFinalizaPedido EmbalarPedidoCd5(int idPedidoEnvio, int idUsuarioExpedicao)
    {
        var retorno = new retornoFinalizaPedido();
        var data = new dbCommerceDataContext();
        try
        {
            var envio = (from c in data.tbPedidoEnvios where c.idPedidoEnvio == idPedidoEnvio orderby c.idPedidoEnvio descending select c).First();
            if (envio.dataFimEmbalagem == null)
            {
                retorno = GravaPacotesSemPesoFinalizaPedido(idUsuarioExpedicao, envio.idPedido, 0);
            }
            else
            {
                retorno = RetornaDetalhesFinalizacaoPedidoEnvio(idUsuarioExpedicao, envio.idPedido, idPedidoEnvio);
            }
        }
        catch (Exception ex)
        {
            rnEmails.EnviaEmail("", "andre@bark.com.br", "", "", "", "Erro Selecioanr Pedido CD5 envio" + idPedidoEnvio, "Erro Selecioanr Pedido CD5 envio" + idPedidoEnvio);
        }
        
        return retorno;
    }

    [WebMethod]
    public bool GravaQuantidadePacotes(int idUsuarioExpedicao, int idPedido, int quantidadePacotes)
    {
        var pedidosDc = new dbCommerceDataContext();
        try
        {
            var pedidosEnvio = (from c in pedidosDc.tbPedidoEnvios
                                where c.idUsuarioEmbalagem == idUsuarioExpedicao && c.idPedido == idPedido
                                orderby c.dataInicioEmbalagem descending
                                select c).FirstOrDefault();
            pedidosEnvio.volumesEmbalados = quantidadePacotes;
            pedidosDc.SubmitChanges();
            return true;
        }
        catch (Exception)
        {

        }
        return false;
    }

    [WebMethod]
    public retornoEtiqueta RetornaEtiqueta(int idUsuarioExpedicao, int idPedido)
    {
        var retorno = new retornoEtiqueta();
        var pedidosDc = new dbCommerceDataContext();
        var clientesDc = new dbCommerceDataContext();
        var pedidos = (from c in pedidosDc.tbPedidoEnvios
                       where c.idPedido == idPedido
                       select c.tbPedido).FirstOrDefault();
        var cliente = (from c in clientesDc.tbClientes where c.clienteId == pedidos.clienteId select c).FirstOrDefault();
        retorno.telResidencial = cliente.clienteFoneResidencial;
        retorno.telComercial = cliente.clienteFoneComercial;
        retorno.telCelular = cliente.clienteFoneCelular;
        if (String.IsNullOrEmpty(pedidos.endNomeDoDestinatario))
        {
            if (String.IsNullOrEmpty(cliente.clienteNome)) retorno.nome = cliente.clienteNomeDaEmpresa;
            else retorno.nome = cliente.clienteNome;
        }
        else
        {
            retorno.nome = pedidos.endNomeDoDestinatario;
        }
        retorno.rua = pedidos.endRua;
        retorno.numero = pedidos.endNumero;
        retorno.complemento = pedidos.endComplemento;
        retorno.bairro = pedidos.endBairro;
        retorno.cidade = pedidos.endCidade;
        retorno.estado = pedidos.endEstado;
        retorno.cep = pedidos.endCep;
        retorno.referencia = pedidos.endReferenciaParaEntrega;
        return retorno;
    }

    [WebMethod]
    public int RetornaQuantidadePacotes(int idUsuarioExpedicao, int idPedido)
    {
        try
        {
            var pedidosDc = new dbCommerceDataContext();
            var pedidosEnvio = (from c in pedidosDc.tbPedidoEnvios
                                where c.idUsuarioEmbalagem == idUsuarioExpedicao && c.idPedido == idPedido
                                orderby c.idPedidoEnvio descending
                                select c).FirstOrDefault();
            return Convert.ToInt32(pedidosEnvio.volumesEmbalados);

        }
        catch (Exception)
        {
            return 0;
        }
    }

    [WebMethod]
    public string GravaPesos(int idUsuarioExpedicao, int idPedido, List<pesosPacotesGravar> pesosGravar)
    {
        var pedidosDc = new dbCommerceDataContext();
        var pedidosEnvio = (from c in pedidosDc.tbPedidoEnvios
                            where c.idUsuarioEmbalagem == idUsuarioExpedicao && c.dataFimEmbalagem == null
                            select c).FirstOrDefault();

        foreach (var pesosPacotesGravar in pesosGravar)
        {
            bool pesoCheck = false;
            try
            {
                pesoCheck = (from c in pedidosDc.tbPedidoPacotes
                             where c.numeroVolume == pesosPacotesGravar.volume && c.idPedidoEnvio == pedidosEnvio.idPedidoEnvio
                             select c).Any();
            }
            catch (Exception)
            {

            }
            if (!pesoCheck)
            {
                var pedidoPacotes = new tbPedidoPacote();
                pedidoPacotes.numeroVolume = pesosPacotesGravar.volume;
                pedidoPacotes.idPedido = idPedido;
                pedidoPacotes.peso = pesosPacotesGravar.peso;
                int largura = pesosPacotesGravar.largura;
                int altura = pesosPacotesGravar.altura;
                int profundidade = pesosPacotesGravar.profundidade;
                pedidoPacotes.largura = largura;
                pedidoPacotes.altura = altura;
                pedidoPacotes.profundidade = profundidade;
                pedidoPacotes.rastreio = "";
                pedidoPacotes.concluido = false;
                pedidoPacotes.idPedidoEnvio = pedidosEnvio.idPedidoEnvio;
                pedidoPacotes.idEnderecoPacote = 1;
                pedidosDc.tbPedidoPacotes.InsertOnSubmit(pedidoPacotes);
                pedidosDc.SubmitChanges();
            }
        }

        var entrega = rnPedidos.DefineEntregaPedido(idPedido, pedidosEnvio.idPedidoEnvio);
        if (entrega != "pac" && entrega != "sedex") finalizarPedido(idPedido, idUsuarioExpedicao);
        return entrega;
    }

    [WebMethod]
    public retornoFinalizaPedido GravaPesosFinalizaPedido(int idUsuarioExpedicao, int idPedido, List<pesosPacotesGravar> pesosGravar)
    {
        var data = new dbCommerceDataContext();
        var pedidosEnvio = (from c in data.tbPedidoEnvios
                            where c.idUsuarioEmbalagem == idUsuarioExpedicao && c.dataFimEmbalagem == null
                            select c).FirstOrDefault();
        pedidosEnvio.volumesEmbalados = pesosGravar.Count;
        data.SubmitChanges();

        foreach (var pesosPacotesGravar in pesosGravar)
        {
            bool pesoCheck = false;
            try
            {
                pesoCheck = (from c in data.tbPedidoPacotes
                             where c.numeroVolume == pesosPacotesGravar.volume && c.idPedidoEnvio == pedidosEnvio.idPedidoEnvio
                             select c).Any();
            }
            catch (Exception)
            {

            }
            if (!pesoCheck)
            {
                var pedidoPacotes = new tbPedidoPacote();
                pedidoPacotes.numeroVolume = pesosPacotesGravar.volume;
                pedidoPacotes.idPedido = idPedido;
                pedidoPacotes.peso = pesosPacotesGravar.peso;
                int largura = pesosPacotesGravar.largura;
                int altura = pesosPacotesGravar.altura;
                int profundidade = pesosPacotesGravar.profundidade;
                pedidoPacotes.largura = largura;
                pedidoPacotes.altura = altura;
                pedidoPacotes.profundidade = profundidade;
                pedidoPacotes.rastreio = "";
                pedidoPacotes.concluido = false;
                pedidoPacotes.idPedidoEnvio = pedidosEnvio.idPedidoEnvio;
                pedidoPacotes.idEnderecoPacote = 1;
                data.tbPedidoPacotes.InsertOnSubmit(pedidoPacotes);
                data.SubmitChanges();
            }
        }
        var retorno = new retornoFinalizaPedido();
        var entrega = rnPedidos.DefineEntregaPedido(idPedido, pedidosEnvio.idPedidoEnvio);

        retorno.formaEnvio = entrega;
        retorno.etiqueta = RetornaEtiqueta(idUsuarioExpedicao, idPedido);
        //if (entrega != "pac" && entrega != "sedex") finalizarPedido(idPedido, idUsuarioExpedicao);
        finalizarPedido(idPedido, idUsuarioExpedicao);
        if (entrega.ToLower() == "tnt")
        {


            var nota = rnNotaFiscal.gerarNumeroNota(idPedido, pedidosEnvio.idPedidoEnvio, false, pedidosEnvio.tbPedido.endEstado.ToLower());
            var detalhesNota = (from c in data.tbNotaFiscals where c.idNotaFiscal == nota.idNotaFiscal select c).First();
            retorno.cnpj = detalhesNota.tbEmpresa.cnpj;
            retorno.nfeNumero = detalhesNota.numeroNota.ToString();
        }
        retorno.totalVolumes = pesosGravar.Count().ToString();
        return retorno;
    }

    [WebMethod]
    public retornoFinalizaPedido ReimprimirEtiquetaPedidoCds(int idPedido, int idCentroDistribuicao)
    {
        var data = new dbCommerceDataContext();
        var pedidosEnvio = (from c in data.tbPedidoEnvios
                            orderby c.idPedidoEnvio descending
                            where c.idPedido == idPedido && c.idCentroDeDistribuicao == idCentroDistribuicao
                            select c).FirstOrDefault();
        var retorno = new retornoFinalizaPedido();

        if (pedidosEnvio != null)
        {
            retorno.formaEnvio = pedidosEnvio.formaDeEnvio;
            retorno.etiqueta = RetornaEtiqueta(pedidosEnvio.idUsuarioEmbalagem ?? 0, idPedido);
            var nota = rnNotaFiscal.gerarNumeroNota(idPedido, pedidosEnvio.idPedidoEnvio, false, pedidosEnvio.tbPedido.endEstado.ToLower());
            var detalhesNota =
                (from c in data.tbNotaFiscals where c.idNotaFiscal == nota.idNotaFiscal select c).First();
            retorno.cnpj = detalhesNota.tbEmpresa.cnpj;
            retorno.nfeNumero = detalhesNota.numeroNota.ToString();

            retorno.totalVolumes = pedidosEnvio.volumesEmbalados.ToString();
        }
        return retorno;
    }

    [WebMethod]
    public retornoFinalizaPedido ReimprimirEtiquetaPedido(int idPedido)
    {
        var data = new dbCommerceDataContext();
        var pedidosEnvio = (from c in data.tbPedidoEnvios
                            orderby c.idPedidoEnvio descending
                            where c.idPedido == idPedido
                            select c).FirstOrDefault();
        var retorno = new retornoFinalizaPedido();

        if (pedidosEnvio != null)
        {
            retorno.formaEnvio = pedidosEnvio.formaDeEnvio;
            retorno.etiqueta = RetornaEtiqueta(pedidosEnvio.idUsuarioEmbalagem ?? 0, idPedido);
            var nota = rnNotaFiscal.gerarNumeroNota(idPedido, pedidosEnvio.idPedidoEnvio, false, pedidosEnvio.tbPedido.endEstado.ToLower());
            var detalhesNota =
                (from c in data.tbNotaFiscals where c.idNotaFiscal == nota.idNotaFiscal select c).First();
            retorno.cnpj = detalhesNota.tbEmpresa.cnpj;
            retorno.nfeNumero = detalhesNota.numeroNota.ToString();

            retorno.totalVolumes = pedidosEnvio.volumesEmbalados.ToString();
        }
        return retorno;
    }

    [WebMethod]
    public string GravaRastreios(int idUsuarioExpedicao, int idPedido, List<rastreiosGravar> rastreios)
    {
        var pedidosDc = new dbCommerceDataContext();
        var pedidosEnvio = (from c in pedidosDc.tbPedidoEnvios
                            where c.idUsuarioEmbalagem == idUsuarioExpedicao && c.idPedido == idPedido
                            orderby c.idPedidoEnvio descending
                            select c).FirstOrDefault();

        int i = 1;
        foreach (var rastreio in rastreios)
        {
            var pacote = (from c in pedidosDc.tbPedidoPacotes
                          where c.idPedido == idPedido && c.idPedidoEnvio == pedidosEnvio.idPedidoEnvio && c.numeroVolume == i
                          select c).FirstOrDefault();
            if (pacote != null) pacote.rastreio = rastreio.rastreio;
            pedidosDc.SubmitChanges();
            i++;
        }
        finalizarPedido(idPedido, idUsuarioExpedicao);
        rnEmails.enviaCodigoDoTrakingCorreios("", idPedido.ToString(), "");
        return "ok";
    }

    private void finalizarPedido(int idPedido, int idUsuarioExpedicao)
    {
        var pedidosDc = new dbCommerceDataContext();
        var pedido = (from c in pedidosDc.tbPedidos where c.pedidoId == idPedido select c).First();
        var pedidosEnvio = (from c in pedidosDc.tbPedidoEnvios where c.idUsuarioEmbalagem == idUsuarioExpedicao && c.dataFimEmbalagem == null select c).FirstOrDefault();
        var listaProdutos = rnPedidos.retornaProdutosEtiqueta(idPedido);
        var itensPedidoDc = new dbCommerceDataContext();

        foreach (var listaProduto in listaProdutos)
        {
            var itensFilho = (from c in pedidosDc.tbItensPedidoCombos where c.idItemPedido == listaProduto.itemPedidoId select c).ToList();
            try
            {
                if (itensFilho.Any())
                {
                    var itemPedido = (from c in itensPedidoDc.tbItensPedidoCombos where c.idItemPedido == listaProduto.itemPedidoId && c.produtoId == listaProduto.produtoId select c).First();
                    itemPedido.enviado = true;
                    itemPedido.idPedidoEnvio = pedidosEnvio.idPedidoEnvio;
                    itensPedidoDc.SubmitChanges();
                    try
                    {
                        pedidosDc.SubmitChanges();
                    }
                    catch (System.Data.Linq.ChangeConflictException ex)
                    {
                        foreach (System.Data.Linq.ObjectChangeConflict objConflict in pedidosDc.ChangeConflicts)
                        {
                            foreach (System.Data.Linq.MemberChangeConflict memberConflict in objConflict.MemberConflicts)
                            {
                                memberConflict.Resolve(System.Data.Linq.RefreshMode.KeepCurrentValues);
                            }
                        }
                        pedidosDc.SubmitChanges(System.Data.Linq.ConflictMode.ContinueOnConflict);
                    }
                }
                else
                {
                    var itemPedido = (from c in itensPedidoDc.tbItensPedidos where c.itemPedidoId == listaProduto.itemPedidoId select c).First();
                    itemPedido.enviado = true;
                    itemPedido.idPedidoEnvio = pedidosEnvio.idPedidoEnvio;
                    itensPedidoDc.SubmitChanges();
                    try
                    {
                        pedidosDc.SubmitChanges();
                    }
                    catch (System.Data.Linq.ChangeConflictException ex)
                    {
                        foreach (System.Data.Linq.ObjectChangeConflict objConflict in pedidosDc.ChangeConflicts)
                        {
                            foreach (System.Data.Linq.MemberChangeConflict memberConflict in objConflict.MemberConflicts)
                            {
                                memberConflict.Resolve(System.Data.Linq.RefreshMode.KeepCurrentValues);
                            }
                        }
                        pedidosDc.SubmitChanges(System.Data.Linq.ConflictMode.ContinueOnConflict);
                    }
                }

                var itemPedidoEstoque = (from c in pedidosDc.tbItemPedidoEstoques
                                         where c.itemPedidoId == listaProduto.itemPedidoId && c.enviado == false && c.produtoId == listaProduto.produtoId && c.cancelado == false
                                         select c).FirstOrDefault();
                if (itemPedidoEstoque != null)
                {
                    itemPedidoEstoque.enviado = true;
                    try
                    {
                        pedidosDc.SubmitChanges();
                    }
                    catch (System.Data.Linq.ChangeConflictException ex)
                    {
                        foreach (System.Data.Linq.ObjectChangeConflict objConflict in pedidosDc.ChangeConflicts)
                        {
                            foreach (System.Data.Linq.MemberChangeConflict memberConflict in objConflict.MemberConflicts)
                            {
                                memberConflict.Resolve(System.Data.Linq.RefreshMode.KeepCurrentValues);
                            }
                        }
                        pedidosDc.SubmitChanges(System.Data.Linq.ConflictMode.ContinueOnConflict);
                    }
                }
            }
            catch (Exception)
            {

            }
        }

        var parciaisPendentes = (from c in pedidosDc.tbItemPedidoEstoques
                                 where
                                 c.tbItensPedido.pedidoId == idPedido && c.enviado == false
                                 && c.cancelado == false
                                 select c).ToList();

        //var parciaisPendentes = (from c in pedidosDc.tbItensPedidoEnvioParcials
        //                         join d in pedidosDc.tbItensPedidos on c.idItemPedido equals d.itemPedidoId
        //                         where c.idPedido == idPedido && c.enviado == false && (d.cancelado ?? false) == false select c).Any();

        var clientesDc = new dbCommerceDataContext();
        var cliente = (from c in clientesDc.tbClientes where c.clienteId == pedido.clienteId select c).First();



        var usuarioDc = new dbCommerceDataContext();
        var usuarioExpedicao = (from c in usuarioDc.tbUsuarioExpedicaos where c.idUsuarioExpedicao == idUsuarioExpedicao select c).FirstOrDefault();
        if (!parciaisPendentes.Any())
        {
            rnPedidos.pedidoAlteraStatus(5, idPedido);
            rnInteracoes.interacaoInclui(idPedido, "Pedido Enviado", usuarioExpedicao.nome, "True");
            //rnEmails.enviaAlteracaoDeStatus(cliente.clienteNome, pedido.pedidoId.ToString(), "Pedido Enviado", cliente.clienteEmail);
            //rnEmails.enviaAlteracaoDeStatus(cliente.clienteNome, pedido.pedidoId.ToString(), "Pedido Enviado", "andre@bark.com.br");
        }
        else
        {
            foreach (var parcial in parciaisPendentes)
            {
                parcial.autorizarParcial = false;
                try
                {
                    pedidosDc.SubmitChanges();
                }
                catch (System.Data.Linq.ChangeConflictException ex)
                {
                    foreach (System.Data.Linq.ObjectChangeConflict objConflict in pedidosDc.ChangeConflicts)
                    {
                        foreach (System.Data.Linq.MemberChangeConflict memberConflict in objConflict.MemberConflicts)
                        {
                            memberConflict.Resolve(System.Data.Linq.RefreshMode.KeepCurrentValues);
                        }
                    }
                    pedidosDc.SubmitChanges(System.Data.Linq.ConflictMode.ContinueOnConflict);
                }
            }
            rnPedidos.pedidoAlteraStatus(11, pedido.pedidoId);
            rnInteracoes.interacaoInclui(idPedido, "Pedido Enviado Parcialmente", usuarioExpedicao.nome, "True");
            pedido.separado = false;
            pedido.dataInicioSeparacao = null;
            pedido.dataFimSeparacao = null;
            pedido.idUsuarioSeparacao = null;
            pedido.statusDoPedido = 11;
            pedidosDc.SubmitChanges();
        }

        pedidosEnvio.dataFimEmbalagem = DateTime.Now;
        try
        {
            pedidosDc.SubmitChanges();
        }
        catch (System.Data.Linq.ChangeConflictException ex)
        {
            foreach (System.Data.Linq.ObjectChangeConflict objConflict in pedidosDc.ChangeConflicts)
            {
                foreach (System.Data.Linq.MemberChangeConflict memberConflict in objConflict.MemberConflicts)
                {
                    memberConflict.Resolve(System.Data.Linq.RefreshMode.KeepCurrentValues);
                }
            }
            pedidosDc.SubmitChanges(System.Data.Linq.ConflictMode.ContinueOnConflict);
        }

        int prazo = pedidosEnvio.prazoSelecionado ?? 1;
        int prazoDiasUteis = rnFuncoes.retornaPrazoDiasUteis(prazo, 0);
        var pacotesAlterar = (from c in pedidosDc.tbPedidoPacotes where c.idPedido == pedido.pedidoId && (c.concluido ?? true) == false select c);
        foreach (var pacote in pacotesAlterar)
        {
            pacote.concluido = true;
            pacote.formaDeEnvio = pedidosEnvio.formaDeEnvio;
            pacote.statusAtual = "Emissão";
            pacote.rastreamentoConcluido = false;
            pacote.prazoFinalEntrega = DateTime.Now.AddDays(prazoDiasUteis);
        }

        using (var txn = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required,
            new System.Transactions.TransactionOptions
            {
                IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
            }))

        {

            var produtosEstoque = (from c in pedidosDc.tbProdutoEstoques
                                   where
                                       c.pedidoId == pedido.pedidoId && c.enviado == false && c.idPedidoEnvio == pedidosEnvio.idPedidoEnvio
                                   select c);
            foreach (var produtoEstoque in produtosEstoque)
            {
                produtoEstoque.enviado = true;
                produtoEstoque.dataEnvio = DateTime.Now;

                try
                {
                    pedidosDc.SubmitChanges();
                }
                catch (System.Data.Linq.ChangeConflictException ex)
                {
                    foreach (System.Data.Linq.ObjectChangeConflict objConflict in pedidosDc.ChangeConflicts)
                    {
                        foreach (System.Data.Linq.MemberChangeConflict memberConflict in objConflict.MemberConflicts)
                        {
                            memberConflict.Resolve(System.Data.Linq.RefreshMode.KeepCurrentValues);
                        }
                    }
                    pedidosDc.SubmitChanges(System.Data.Linq.ConflictMode.ContinueOnConflict);
                }
            }
        }


        var itensPedido = (from c in pedidosDc.tbItensPedidos where c.pedidoId == pedido.pedidoId select c);
        foreach (var itemPedido in itensPedido)
        {
            var itensCombo =
                (from c in pedidosDc.tbItensPedidoCombos where c.idItemPedido == itemPedido.itemPedidoId select c)
                    .ToList();
            if (itensCombo.Any())
            {
                bool itensFaltando = itensCombo.Where(x => x.enviado == false).Any();
                if (!itensFaltando)
                {
                    itemPedido.enviado = true;
                    try
                    {
                        pedidosDc.SubmitChanges();
                    }
                    catch (System.Data.Linq.ChangeConflictException ex)
                    {
                        foreach (System.Data.Linq.ObjectChangeConflict objConflict in pedidosDc.ChangeConflicts)
                        {
                            foreach (System.Data.Linq.MemberChangeConflict memberConflict in objConflict.MemberConflicts)
                            {
                                memberConflict.Resolve(System.Data.Linq.RefreshMode.KeepCurrentValues);
                            }
                        }
                        pedidosDc.SubmitChanges(System.Data.Linq.ConflictMode.ContinueOnConflict);
                    }
                }
            }
        }
    }

    public class ListaAguardandoConferenciaItem
    {
        public int etiqueta { get; set; }
        public string produtoNome { get; set; }
    }

    [WebMethod]
    public List<ListaAguardandoConferenciaItem> RetornaProdutosAguardandoConferencia(int idUsuarioExpedicao, int idPedido)
    {
        var retorno = new List<ListaAguardandoConferenciaItem>();
        var pedidosDc = new dbCommerceDataContext();
        var pedidosEnvio = (from c in pedidosDc.tbPedidoEnvios
                            where c.idUsuarioEmbalagem == idUsuarioExpedicao && c.idPedido == idPedido && c.dataFimEmbalagem == null
                            orderby c.idPedidoEnvio descending
                            select c).FirstOrDefault();
        using (var txn = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required,
            new System.Transactions.TransactionOptions
            {
                IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
            }))

        {
            var itensAguardando = (from c in pedidosDc.tbProdutoEstoques
                                   where c.idPedidoEnvio == pedidosEnvio.idPedidoEnvio && (c.conferidoEmbalagem ?? false) == false
                                   select c);

            foreach (var itemAguardando in itensAguardando)
            {
                var itemRetorno = new ListaAguardandoConferenciaItem();
                itemRetorno.etiqueta = itemAguardando.idPedidoFornecedorItem;
                itemRetorno.produtoNome = itemAguardando.tbProduto.produtoNome;
                retorno.Add(itemRetorno);
            }
        }
        return retorno;
    }

    [WebMethod]
    public string RegistraProdutoAguardandoConferencia(int idUsuarioExpedicao, int idPedido, int etiqueta)
    {
        var pedidosDc = new dbCommerceDataContext();
        var pedidosEnvio = (from c in pedidosDc.tbPedidoEnvios
                            where c.idPedido == idPedido && c.idUsuarioEmbalagem == idUsuarioExpedicao && c.dataFimEmbalagem == null

                            orderby c.idPedidoEnvio descending
                            select c).FirstOrDefault();
        //using (var txn = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required,
        //    new System.Transactions.TransactionOptions
        //    {
        //        IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
        //    }))
        //{
        var usuarioExpedicao = (from c in pedidosDc.tbUsuarioExpedicaos where c.idUsuarioExpedicao == idUsuarioExpedicao select c).First();
        var itemAguardando = (from c in pedidosDc.tbProdutoEstoques
                              where c.idPedidoEnvio == pedidosEnvio.idPedidoEnvio && c.idPedidoFornecedorItem == etiqueta
                              select c).FirstOrDefault();
        if (itemAguardando != null)
        {
            if ((itemAguardando.conferidoEmbalagem ?? false) == false)
            {
                rnLog.InsereLogEtiqueta(usuarioExpedicao.nome, etiqueta, idPedido, "Conferida etiqueta na embalagem");
                itemAguardando.conferidoEmbalagem = true;
                pedidosDc.SubmitChanges();
                return "conferido";
            }
            rnLog.InsereLogEtiqueta(usuarioExpedicao.nome, etiqueta, idPedido, "Conferida etiqueta na embalagem já conferida anteriormente");
            return "registrado";
        }
        //}

        rnLog.InsereLogEtiqueta(usuarioExpedicao.nome, etiqueta, idPedido, "Etiqueta que não pertence ao pedido lida na embalagem");
        return "naofazparte";
    }


    [WebMethod]
    public int RetornaVolumesCd1(int idUsuarioExpedicao, int idPedido)
    {
        var data = new dbCommerceDataContext();
        var pedidosEnvio = (from c in data.tbPedidoEnvios
                            where c.idPedido == idPedido && c.dataFimEmbalagem == null && c.idUsuarioEmbalagem == idUsuarioExpedicao
                            select c).FirstOrDefault();

        int volumesCd1 = (pedidosEnvio.volumesEmbalados ?? 0);

        if (pedidosEnvio.idCentroDeDistribuicao == 1 | pedidosEnvio.idCentroDeDistribuicao == 2)
        {
            var produtosCd2 = (from c in data.tbProdutoEstoques where c.idPedidoEnvio == pedidosEnvio.idPedidoEnvio && c.idCentroDistribuicao == 2 select c).Count();
            volumesCd1 = volumesCd1 - produtosCd2;
        }
        if (pedidosEnvio.idCentroDeDistribuicao == 4 | pedidosEnvio.idCentroDeDistribuicao == 5)
        {
            var produtosCd2 = (from c in data.tbProdutoEstoques where c.idPedidoEnvio == pedidosEnvio.idPedidoEnvio && c.idCentroDistribuicao == 5 select c).Count();
            volumesCd1 = volumesCd1 - produtosCd2;
        }
        return volumesCd1;
    }

    [WebMethod]
    public retornoFinalizaPedido GravaPacotesSemPesoAguardaConferenciaPacote(int idUsuarioExpedicao, int idPedido, int quantidadePacotes, bool finalizaEnvio)
    {
        var data = new dbCommerceDataContext();
        var usuario = (from c in data.tbUsuarioExpedicaos where c.idUsuarioExpedicao == idUsuarioExpedicao select c).First();
        return GravaPacotesSemPesoFinalizaPedido(idUsuarioExpedicao, idPedido, quantidadePacotes, finalizaEnvio, usuario.idCentroDistribuicao);
    }

    [WebMethod]
    public bool FinalizaEnvio(int idUsuarioExpedicao, int idPedido)
    {
        return FinalizarEnvio(idUsuarioExpedicao, idPedido, 0);
    }

    public bool FinalizarEnvio(int idUsuarioExpedicao, int idPedido, int idPedidoEnvio)
    {
        var data = new dbCommerceDataContext();
        var pedidosEnvios = (from c in data.tbPedidoEnvios
                             where c.idPedido == idPedido && c.dataFimEmbalagem == null
                             select c).ToList();
        if (pedidosEnvios.Where(x => x.idUsuarioEmbalagem == idUsuarioExpedicao).Any()) pedidosEnvios = pedidosEnvios.Where(x => x.idUsuarioEmbalagem == idUsuarioExpedicao).ToList();
        if (idPedidoEnvio > 0) pedidosEnvios = (from c in data.tbPedidoEnvios
                                                where c.idPedidoEnvio == idPedidoEnvio
                                                select c).ToList();
        var pedidosEnvio = pedidosEnvios.FirstOrDefault();
        if (pedidosEnvio != null)
        {
            #region finalizaPedido
            var pedido = (from c in data.tbPedidos where c.pedidoId == idPedido select c).First();

            var produtosEstoque = (from c in data.tbProdutoEstoques where c.idPedidoEnvio == pedidosEnvio.idPedidoEnvio && c.pedidoId == idPedido select c).ToList();
            foreach (var produtoEstoque in produtosEstoque)
            {
                bool erro = true;
                int retries = 0;
                ;
                while (erro == true && retries < 4)
                {
                    try
                    {
                        retries++;
                        var itemPedidoEstoque = (from c in data.tbItemPedidoEstoques where c.idItemPedidoEstoque == produtoEstoque.idItemPedidoEstoqueReserva select c).FirstOrDefault();
                        if (itemPedidoEstoque != null)
                        {
                            itemPedidoEstoque.enviado = true;
                            data.SubmitChanges();
                        }

                        produtoEstoque.enviado = true;
                        produtoEstoque.dataEnvio = DateTime.Now;
                        data.SubmitChanges();
                        erro = false;
                    }
                    catch (Exception ex)
                    {
                        erro = true;
                    }
                }

            }

            var parciaisPendentes = (from c in data.tbItemPedidoEstoques
                                     where
                                     c.tbItensPedido.pedidoId == idPedido && c.enviado == false
                                     && c.cancelado == false
                                     select c).ToList();
            var usuarioExpedicao = (from c in data.tbUsuarioExpedicaos where c.idUsuarioExpedicao == idUsuarioExpedicao select c).FirstOrDefault();
            if (!parciaisPendentes.Any())
            {
                rnPedidos.pedidoAlteraStatus(5, idPedido);
                rnInteracoes.interacaoInclui(idPedido, "Pedido Enviado", usuarioExpedicao.nome, "True");
            }
            else
            {
                foreach (var parcial in parciaisPendentes)
                {
                    parcial.autorizarParcial = false;
                    try
                    {
                        data.SubmitChanges();
                    }
                    catch (System.Data.Linq.ChangeConflictException ex)
                    {
                        foreach (System.Data.Linq.ObjectChangeConflict objConflict in data.ChangeConflicts)
                        {
                            foreach (System.Data.Linq.MemberChangeConflict memberConflict in objConflict.MemberConflicts)
                            {
                                memberConflict.Resolve(System.Data.Linq.RefreshMode.KeepCurrentValues);
                            }
                        }
                        data.SubmitChanges(System.Data.Linq.ConflictMode.ContinueOnConflict);
                    }
                }
                rnPedidos.pedidoAlteraStatus(11, pedido.pedidoId);
                rnInteracoes.interacaoInclui(idPedido, "Pedido Enviado Parcialmente", usuarioExpedicao.nome, "True");
                pedido.separado = false;
                pedido.dataInicioSeparacao = null;
                pedido.dataFimSeparacao = null;
                pedido.idUsuarioSeparacao = null;
                pedido.statusDoPedido = 11;

                try
                {
                    data.SubmitChanges();
                }
                catch (System.Data.Linq.ChangeConflictException ex)
                {
                    foreach (System.Data.Linq.ObjectChangeConflict objConflict in data.ChangeConflicts)
                    {
                        foreach (System.Data.Linq.MemberChangeConflict memberConflict in objConflict.MemberConflicts)
                        {
                            memberConflict.Resolve(System.Data.Linq.RefreshMode.KeepCurrentValues);
                        }
                    }
                    data.SubmitChanges(System.Data.Linq.ConflictMode.ContinueOnConflict);
                }
                rnPedidos.AdicionaQueueChecagemCompleto(pedido.pedidoId);
            }

            pedidosEnvio.dataFimEmbalagem = DateTime.Now; //Não finaliza a embalagem até conferir os pacotes no CD1
            if (pedidosEnvio.dataInicioEmbalagem == null) pedidosEnvio.dataInicioEmbalagem = DateTime.Now;
            if (pedidosEnvio.idUsuarioEmbalagem == null) pedidosEnvio.idUsuarioEmbalagem = idUsuarioExpedicao;


            #endregion

            pedidosEnvio.dataFimEmbalagem = DateTime.Now;

            var pacotes = (from c in data.tbPedidoPacotes where c.idPedidoEnvio == pedidosEnvio.idPedidoEnvio select c);
            foreach(var pacote in pacotes)
            {
                pacote.idEnderecoPacote = 2;
            }
            data.SubmitChanges();
            return true;
        }
        return false;
    }

    [WebMethod]
    public retornoFinalizaPedido GravaPacotesSemPesoFinalizaPedido(int idUsuarioExpedicao, int idPedido, int quantidadePacotes)
    {
        var data = new dbCommerceDataContext();
        var usuario = (from c in data.tbUsuarioExpedicaos where c.idUsuarioExpedicao == idUsuarioExpedicao select c).First();
        return GravaPacotesSemPesoFinalizaPedido(idUsuarioExpedicao, idPedido, quantidadePacotes, true, usuario.idCentroDistribuicao);
    }


    public retornoFinalizaPedido GravaPacotesSemPesoFinalizaPedido(int idUsuarioExpedicao, int idPedido, int quantidadePacotes, bool finalizaEnvio, int idCentroDistribuicao)
    {
        return GravaPacotesSemPesoFinalizaPedido(idUsuarioExpedicao, idPedido, quantidadePacotes, finalizaEnvio, idCentroDistribuicao, 0);
    }

    [WebMethod]
    public retornoFinalizaPedido AlterarQuantidadeDePacotesPedido(int idUsuarioExpedicao, string cnpj, string numeroNota, int totalPacotes)
    {
        var retorno = new retornoFinalizaPedido();
        int numeronota = Convert.ToInt32(numeroNota);
        var data = new dbCommerceDataContext();
        var empresa = (from c in data.tbEmpresas where c.cnpj == cnpj select c).First();
        var nota = (from c in data.tbNotaFiscals where c.idCNPJ == empresa.idEmpresa && c.numeroNota == numeronota select c).First();
        var envio = (from c in data.tbPedidoEnvios where c.idPedidoEnvio == nota.idPedidoEnvio select c).FirstOrDefault();
        if (!string.IsNullOrEmpty(envio.formaDeEnvio))
        {
            var pacotes = (from c in data.tbPedidoPacotes where c.idPedidoEnvio == envio.idPedidoEnvio select c).ToList();
            retorno.formaEnvio = envio.formaDeEnvio;
            retorno.etiqueta = RetornaEtiqueta(idUsuarioExpedicao, envio.idPedido);
            retorno.cnpj = nota.tbEmpresa.cnpj;
            retorno.nfeNumero = nota.numeroNota.ToString();

            retorno.totalVolumes = envio.volumesEmbalados.ToString();
            retorno.totalVolumesCd1 = (envio.volumesEmbalados - pacotes.Count(x => x.idCentroDistribuicao == 2)).ToString();
            retorno.totalVolumesCd2 = (pacotes.Count(x => x.idCentroDistribuicao == 2)).ToString();


            retorno.produtosCd2 = new List<string>();
            var etiquetasCd2 = (from c in data.tbProdutoEstoques where c.idPedidoEnvio == envio.idPedidoEnvio && c.idCentroDistribuicao == 2 select c).ToList();
            foreach (var etiquetaCd2 in etiquetasCd2)
            {
                retorno.produtosCd2.Add(etiquetaCd2.idPedidoFornecedorItem + " - " + etiquetaCd2.tbProduto.produtoNome);
            }
            retorno.pedidoId = envio.idPedido;
        }
        else if (envio.idCentroDeDistribuicao == 3)
        {
            var pacotes = (from c in data.tbPedidoPacotes where c.idPedidoEnvio == envio.idPedidoEnvio select c).ToList();
            retorno.formaEnvio = envio.formaDeEnvio;
            retorno.etiqueta = RetornaEtiqueta(idUsuarioExpedicao, envio.idPedido);
            retorno.cnpj = nota.tbEmpresa.cnpj;
            retorno.nfeNumero = nota.numeroNota.ToString();

            retorno.totalVolumes = envio.volumesEmbalados.ToString();
            retorno.totalVolumesCd1 = (envio.volumesEmbalados - pacotes.Count(x => x.idCentroDistribuicao == 2)).ToString();
            retorno.totalVolumesCd2 = (pacotes.Count(x => x.idCentroDistribuicao == 2)).ToString();


            retorno.produtosCd2 = new List<string>();
            var etiquetasCd2 = (from c in data.tbProdutoEstoques where c.idPedidoEnvio == envio.idPedidoEnvio && c.idCentroDistribuicao == 2 select c).ToList();
            foreach (var etiquetaCd2 in etiquetasCd2)
            {
                retorno.produtosCd2.Add(etiquetaCd2.idPedidoFornecedorItem + " - " + etiquetaCd2.tbProduto.produtoNome);
            }
            retorno.pedidoId = envio.idPedido;
        }
        else
        {

            var pacotesDel = (from c in data.tbPedidoPacotes
                              where c.idPedidoEnvio == envio.idPedidoEnvio && c.idCentroDistribuicao > 3
                              select c);
            foreach (var pacoteDel in pacotesDel)
            {
                data.tbPedidoPacotes.DeleteOnSubmit(pacoteDel);
                data.SubmitChanges();
            }
            int numeroVolumeAdicionar = 0;
            var etiquetasCd2 = (from c in data.tbProdutoEstoques where c.idPedidoEnvio == envio.idPedidoEnvio && c.idCentroDistribuicao == 5 select c).ToList();
            
            for (int i = 1; i <= (totalPacotes + etiquetasCd2.Count()); i++)
            {
                numeroVolumeAdicionar++;
                bool pacoteValido = false;
                while (pacoteValido == false)
                {
                    var validaPacote = (from c in data.tbPedidoPacotes where c.idPedidoEnvio == envio.idPedidoEnvio && c.numeroVolume == numeroVolumeAdicionar select c).Any();
                    if (validaPacote == false)
                    {
                        pacoteValido = true;
                    }
                    else
                    {
                        numeroVolumeAdicionar++;
                    }
                }
                
                var pedidoPacotes = new tbPedidoPacote();
                pedidoPacotes.numeroVolume = numeroVolumeAdicionar;
                pedidoPacotes.idPedido = envio.idPedido;
                pedidoPacotes.rastreio = "";
                pedidoPacotes.concluido = false;
                pedidoPacotes.formaDeEnvio = "";
                pedidoPacotes.idPedidoEnvio = envio.idPedidoEnvio;
                pedidoPacotes.idCentroDistribuicao = i <= totalPacotes ? envio.idCentroDeDistribuicao : envio.idCentroDeDistribuicao + 1;
                
                if(i <= totalPacotes)
                {
                    pedidoPacotes.etiquetaImpressa = true;
                }
                else
                {
                    pedidoPacotes.etiquetaImpressa = false;
                }
                pedidoPacotes.idEnderecoPacote = 1;
                data.tbPedidoPacotes.InsertOnSubmit(pedidoPacotes);
                data.SubmitChanges();
            }


            envio.volumesEmbalados = (totalPacotes + etiquetasCd2.Count());
            data.SubmitChanges();
            retorno.formaEnvio = "";
            retorno.etiqueta = RetornaEtiqueta(idUsuarioExpedicao, envio.idPedido);
            retorno.cnpj = nota.tbEmpresa.cnpj;
            retorno.nfeNumero = nota.numeroNota.ToString();
            

            retorno.produtosCd2 = new List<string>();

            foreach (var etiquetaCd2 in etiquetasCd2)
            {
                retorno.produtosCd2.Add(etiquetaCd2.idPedidoFornecedorItem + " - " + etiquetaCd2.tbProduto.produtoNome);
            }

            retorno.totalVolumes = (totalPacotes + etiquetasCd2.Count).ToString();
            retorno.totalVolumesCd1 = totalPacotes.ToString();
            retorno.totalVolumesCd2 = (etiquetasCd2.Count).ToString();
            retorno.pedidoId = envio.idPedido;


            var usuarioExpedicao = (from c in data.tbUsuarioExpedicaos where c.idUsuarioExpedicao == idUsuarioExpedicao select c).FirstOrDefault();
            rnInteracoes.interacaoInclui(envio.idPedido, "Quantidade de pacotes alterados para " + totalPacotes.ToString(), usuarioExpedicao.nome, "True");
        }


        //var usuario = (from c in data.tbUsuarioExpedicaos where c.idUsuarioExpedicao == idUsuarioExpedicao select c).First();
        return retorno;
    }


    public retornoFinalizaPedido GravaPacotesSemPesoFinalizaPedido(int idUsuarioExpedicao, int idPedido, int quantidadePacotes, bool finalizaEnvio, int idCentroDistribuicao, int idPedidoenvio)
    {

        var data = new dbCommerceDataContext();
        var pedidosEnvios = (from c in data.tbPedidoEnvios
                             where c.idPedido == idPedido && c.dataFimEmbalagem == null && c.idCentroDeDistribuicao == idCentroDistribuicao
                             orderby c.idUsuarioEmbalagem != idUsuarioExpedicao
                             select c).ToList();
        if (idCentroDistribuicao == 2) pedidosEnvios = (from c in data.tbPedidoEnvios
                                                        where c.idPedido == idPedido && c.dataFimEmbalagem == null && (c.idCentroDeDistribuicao == 0 | c.idCentroDeDistribuicao == 1 | c.idCentroDeDistribuicao == 2)
                                                        orderby c.idUsuarioEmbalagem != idUsuarioExpedicao
                                                        select c).ToList();
        if (idPedidoenvio > 0 && pedidosEnvios.Any(x => x.idPedidoEnvio == idPedidoenvio)) pedidosEnvios = pedidosEnvios.Where(x => x.idPedidoEnvio == idPedidoenvio).ToList();
        if (pedidosEnvios.Any(x => x.idUsuarioEmbalagem == idUsuarioExpedicao)) pedidosEnvios = pedidosEnvios.Where(x => x.idUsuarioEmbalagem == idUsuarioExpedicao).ToList();
        var pedidosEnvio = (from c in pedidosEnvios
                            select c).FirstOrDefault();
        if (idCentroDistribuicao == 3 | idCentroDistribuicao == 4) pedidosEnvio = (from c in pedidosEnvios
                                                      where c.idCentroDeDistribuicao == idCentroDistribuicao
                                                      select c).FirstOrDefault();
        if (idCentroDistribuicao == 5) pedidosEnvio = (from c in pedidosEnvios
                                                                       where c.idCentroDeDistribuicao == 4 | c.idCentroDeDistribuicao == 5
                                                                       select c).FirstOrDefault();
        var listaProdutos = rnPedidos.retornaProdutosEtiqueta(idPedido, 1);
        var listaProdutosCd2 = rnPedidos.retornaProdutosEtiqueta(idPedido, 0);
        if (idCentroDistribuicao > 2) listaProdutos = rnPedidos.retornaProdutosEtiqueta(idPedido, idCentroDistribuicao);
        if (idCentroDistribuicao == 1 | idCentroDistribuicao == 2) listaProdutosCd2 = rnPedidos.retornaProdutosEtiqueta(idPedido, 2);
        if (idCentroDistribuicao == 4 | idCentroDistribuicao == 5) listaProdutosCd2 = rnPedidos.retornaProdutosEtiqueta(idPedido, 5);
        if (idCentroDistribuicao == 3) listaProdutosCd2 = rnPedidos.retornaProdutosEtiqueta(idPedido, 3);
        pedidosEnvio.volumesEmbalados = quantidadePacotes + listaProdutosCd2.Count;
        int volumesCubagem = listaProdutosCd2.Count;
        //pedidosEnvio.volumesUnicosCubagem = volumesCubagem;
        data.SubmitChanges();


        var pacotesDel = (from c in data.tbPedidoPacotes
                          where c.idPedidoEnvio == pedidosEnvio.idPedidoEnvio
                          select c);
        foreach (var pacoteDel in pacotesDel)
        {
            data.tbPedidoPacotes.DeleteOnSubmit(pacoteDel);
            data.SubmitChanges();
        }
        for (int i = 1; i <= (quantidadePacotes + listaProdutosCd2.Count); i++)
        {
            bool pesoCheck = false;
            try
            {
                pesoCheck = (from c in data.tbPedidoPacotes
                             where c.numeroVolume == i && c.idPedidoEnvio == pedidosEnvio.idPedidoEnvio
                             select c).Any();
            }
            catch (Exception)
            {

            }
            if (!pesoCheck)
            {
                var pedidoPacotes = new tbPedidoPacote();
                pedidoPacotes.numeroVolume = i;
                pedidoPacotes.idPedido = idPedido;
                pedidoPacotes.rastreio = "";
                pedidoPacotes.concluido = false;
                pedidoPacotes.formaDeEnvio = "";
                pedidoPacotes.idPedidoEnvio = pedidosEnvio.idPedidoEnvio;

                if (i <= quantidadePacotes)
                {
                    pedidoPacotes.idCentroDistribuicao = pedidosEnvio.idCentroDeDistribuicao;
                    pedidoPacotes.etiquetaImpressa = true;
                }
                else
                {
                    pedidoPacotes.idCentroDistribuicao = idCentroDistribuicao;
                    if (idCentroDistribuicao == 1) pedidoPacotes.idCentroDistribuicao = 2;
                    if (idCentroDistribuicao == 4) pedidoPacotes.idCentroDistribuicao = 5;
                    pedidoPacotes.etiquetaImpressa = false;
                }
                pedidoPacotes.idEnderecoPacote = 1;
                data.tbPedidoPacotes.InsertOnSubmit(pedidoPacotes);
                data.SubmitChanges();
            }
        }


        var retorno = new retornoFinalizaPedido();
        retorno.formaEnvio = "";
        retorno.etiqueta = RetornaEtiqueta(idUsuarioExpedicao, idPedido);
        var nota = rnNotaFiscal.gerarNumeroNota(idPedido, pedidosEnvio.idPedidoEnvio, false, pedidosEnvio.tbPedido.endEstado.ToLower());
        var detalhesNota = (from c in data.tbNotaFiscals where c.idNotaFiscal == nota.idNotaFiscal select c).First();
        retorno.cnpj = detalhesNota.tbEmpresa.cnpj;
        retorno.nfeNumero = detalhesNota.numeroNota.ToString();

        if (idCentroDistribuicao == 2 | idCentroDistribuicao == 3 | idCentroDistribuicao == 5) FinalizarEnvio(idUsuarioExpedicao, idPedido, pedidosEnvio.idPedidoEnvio);

        retorno.produtosCd2 = new List<string>();
        var etiquetasCd2 = (from c in data.tbProdutoEstoques where c.idPedidoEnvio == pedidosEnvio.idPedidoEnvio && c.idCentroDistribuicao == idCentroDistribuicao select c).ToList();
        foreach (var etiquetaCd2 in etiquetasCd2)
        {
            retorno.produtosCd2.Add(etiquetaCd2.idPedidoFornecedorItem + " - " + etiquetaCd2.tbPedidoFornecedorItem.codigoProduto + " - " + etiquetaCd2.tbProduto.produtoNome);
        }
        retorno.totalVolumes = (quantidadePacotes + listaProdutosCd2.Count).ToString();
        retorno.totalVolumesCd1 = quantidadePacotes.ToString();
        retorno.totalVolumesCd2 = (listaProdutosCd2.Count).ToString();
        retorno.pedidoId = idPedido;

        if (quantidadePacotes == 0 && listaProdutosCd2.Count > 0)
        {
            var cubagens = new List<EmissaoWs.Cubagem>();
            int volAtual = 0;
            var volumesPesar = (from c in data.tbProdutoEstoques
                                where c.idPedidoEnvio == pedidosEnvio.idPedidoEnvio
                                select new
                                {
                                    altura = (c.tbProduto.altura ?? 0),
                                    comprimento = (c.tbProduto.profundidade ?? 0),
                                    etiqueta = c.idPedidoFornecedorItem,
                                    largura = (c.tbProduto.largura ?? 0),
                                    peso = Convert.ToInt32(c.tbProduto.produtoPeso),
                                    produtoId = c.produtoId,
                                    idCentroDistribuicao = c.idCentroDistribuicao
                                }).ToList();
            foreach (var produto in volumesPesar.Where(x => x.idCentroDistribuicao == idCentroDistribuicao))
            {
                volAtual++;
                var cubagem = new EmissaoWs.Cubagem();
                cubagem.numeroVolume = volAtual;
                cubagem.altura = Convert.ToInt32(produto.altura);
                cubagem.largura = Convert.ToInt32(produto.largura);
                cubagem.comprimento = Convert.ToInt32(produto.comprimento);
                cubagem.peso = Convert.ToInt32(produto.peso);
                cubagem.cubado = true;
                cubagens.RemoveAll(x => x.numeroVolume == volAtual);
                cubagens.Add(cubagem);
            }

            var emissaoWs = new EmissaoWs();
            var retornoVolumesPesar = emissaoWs.GravarPesosFinalizarPedido("glmp152029", idPedido, pedidosEnvio.idPedidoEnvio, cubagens, idUsuarioExpedicao);
            retorno.formaEnvio = retornoVolumesPesar;
            rnQueue.AdicionaQueueChecagemPedidoCompleto(idPedido);
        }

        return retorno;
    }

    [WebMethod]
    public retornoFinalizaPedido RetornaDetalhesFinalizacaoPedido(int idUsuarioExpedicao, int idPedido)
    {
        return RetornaDetalhesFinalizacaoPedidoEnvio(idUsuarioExpedicao, idPedido, 0);
    }

    public retornoFinalizaPedido RetornaDetalhesFinalizacaoPedidoEnvio(int idUsuarioExpedicao, int idPedido, int idPedidoEnvio)
    {
        var data = new dbCommerceDataContext();
        var pedidosEnvios = (from c in data.tbPedidoEnvios
                             where c.idPedido == idPedido
                             orderby c.idPedidoEnvio descending
                             select c);
        var pedidosEnvio = pedidosEnvios.FirstOrDefault();
        if (idPedidoEnvio > 0)
        {
            pedidosEnvio = pedidosEnvios.FirstOrDefault(x => x.idPedidoEnvio == idPedidoEnvio);
        }

        var pacotes = (from c in data.tbPedidoPacotes where c.idPedidoEnvio == pedidosEnvio.idPedidoEnvio select c).ToList();
        if (pacotes.Any(x => x.etiquetaImpressa == false))
        {
            foreach (var pacote in pacotes)
            {
                pacote.etiquetaImpressa = true;
            }
            data.SubmitChanges();
        }
        var retorno = new retornoFinalizaPedido();
        retorno.formaEnvio = pedidosEnvio.formaDeEnvio;
        retorno.etiqueta = RetornaEtiqueta(idUsuarioExpedicao, idPedido);
        var nota = rnNotaFiscal.gerarNumeroNota(idPedido, pedidosEnvio.idPedidoEnvio, false, pedidosEnvio.tbPedido.endEstado.ToLower());
        var detalhesNota = (from c in data.tbNotaFiscals where c.idNotaFiscal == nota.idNotaFiscal select c).First();
        retorno.cnpj = detalhesNota.tbEmpresa.cnpj;
        retorno.nfeNumero = detalhesNota.numeroNota.ToString();

        retorno.totalVolumes = pedidosEnvio.volumesEmbalados.ToString();
        retorno.totalVolumesCd1 = (pedidosEnvio.volumesEmbalados - pacotes.Count(x => x.idCentroDistribuicao == 2 | x.idCentroDistribuicao == 5)).ToString();
        retorno.totalVolumesCd2 = (pacotes.Count(x => x.idCentroDistribuicao == 2 | x.idCentroDistribuicao == 5)).ToString();


        retorno.produtosCd2 = new List<string>();
        var etiquetasCd2 = (from c in data.tbProdutoEstoques where c.idPedidoEnvio == pedidosEnvio.idPedidoEnvio && (c.idCentroDistribuicao == 2 | c.idCentroDistribuicao == 5) select c).ToList();
        foreach (var etiquetaCd2 in etiquetasCd2)
        {
            retorno.produtosCd2.Add(etiquetaCd2.idPedidoFornecedorItem + " - " + etiquetaCd2.tbProduto.produtoNome);
        }
        retorno.pedidoId = idPedido;
        return retorno;
    }

    [WebMethod]
    public bool FinalizaImpressaoCd2(int idPedido)
    {
        var data = new dbCommerceDataContext();
        var pedidosEnvio = (from c in data.tbPedidoEnvios
                            where c.idPedido == idPedido
                            orderby c.idPedidoEnvio descending
                            select c).FirstOrDefault();
        var pacotes = (from c in data.tbPedidoPacotes where c.idPedidoEnvio == pedidosEnvio.idPedidoEnvio select c).ToList();
        foreach (var pacote in pacotes)
        {
            pacote.etiquetaImpressa = true;
        }
        data.SubmitChanges();
        return true;
    }


    [WebMethod]
    public string ConferirEnderecoPacote(string etiqueta, string idusuarioexpedicao)
    {
        int idUsuario = Convert.ToInt32(idusuarioexpedicao);
        

        var data = new dbCommerceDataContext();
        var usuario = (from c in data.tbUsuarioExpedicaos where c.idUsuarioExpedicao == idUsuario select c).First();


        var cnpj = string.Empty;
        var numeroNota = string.Empty;
        var volAtual = string.Empty;
        var volTotal = string.Empty;

        if (etiqueta.Length == 31)
        {
            cnpj = etiqueta.Substring(0, 14);
            numeroNota = etiqueta.Substring(14, 9);
            volAtual = etiqueta.Substring(23, 4);
            volTotal = etiqueta.Substring(27, 4);
            var empresa = (from c in data.tbEmpresas where c.cnpj == cnpj select c).First();
            var nota = (from c in data.tbNotaFiscals
                        join d in data.tbPedidoEnvios on c.idPedidoEnvio equals d.idPedidoEnvio
                        where c.idCNPJ == empresa.idEmpresa && c.numeroNota == int.Parse(numeroNota)
                        select new
                        {
                            c.idPedidoEnvio,
                            c.idPedido,
                            d.volumesEmbalados
                        }).First();
            var pacote =
                (from c in data.tbPedidoPacotes
                 where c.idPedidoEnvio == nota.idPedidoEnvio && c.numeroVolume == int.Parse(volAtual)
                 select c).FirstOrDefault();

            bool isError = false;
            if (pacote != null)
            {
                string retorno = "";
                pacote.idEnderecoPacote = 5;
                string mensagem = "";
                if(pacote.tbPedidoEnvio.dataFimEmbalagem == null)
                {
                    isError = true;
                    mensagem = "Pedido não foi finalizado na embalagem";
                }

                if(pacote.peso == 0 && pacote.concluido == false)
                {
                    isError = true;
                    mensagem = "Pacote não foi pesado";
                }

                if (pacote.rastreio != "" | pacote.tbPedidoEnvio.nfeImpressa == true)
                {
                    isError = true;
                    mensagem = "Pedido já foi emitido pela transportadora";
                }
                InserirHistoricoEtiqueta(pacote.idPedidoPacote, 5, idUsuario, usuario.nome, mensagem);

                data.SubmitChanges();
                if (isError) {
                    return "0|" + mensagem;
                }
                else {
                    retorno = "1|";
                    var pacotesEnvios = (from c in data.tbPedidoPacotes where c.idPedidoEnvio == pacote.idPedidoEnvio && c.idPedidoPacote != pacote.idPedidoPacote select c);
                    foreach(var pacoteEnvio in pacotesEnvios)
                    {
                        if(pacoteEnvio.idEnderecoPacote != null)
                        {
                            retorno += "Volume " + pacoteEnvio.numeroVolume + ": " + pacoteEnvio.tbEnderecoPacote.enderecoPacote + "<br>";
                        }
                    }
                    return retorno;
                }
            }
        }
        return "0|Etiqueta Inválida";
    }


    [WebMethod]
    public string ConferirEnderecoProdutoRegistrar(string endereco, string etiqueta, string idusuarioexpedicao)
    {
        int idUsuario = Convert.ToInt32(idusuarioexpedicao);


        var data = new dbCommerceDataContext();
        var usuario = (from c in data.tbUsuarioExpedicaos where c.idUsuarioExpedicao == idUsuario select c).First();
        var enderecoPedido = endereco.Split('-');
        if(enderecoPedido.Count() < 2) return "0|Etiqueta Inválida";
        if (enderecoPedido[1] != "pac") return "0|Etiqueta Inválida";
        int enderecoFinal = Convert.ToInt32(enderecoPedido[0]);
        var enderecoPacote = (from c in data.tbEnderecoPacotes where c.idEnderecoPacote == enderecoFinal select c).FirstOrDefault();
        if(enderecoPacote == null)
        {
            return "0|Etiqueta Inválida";
        }
        var cnpj = string.Empty;
        var numeroNota = string.Empty;
        var volAtual = string.Empty;
        var volTotal = string.Empty;

        if (etiqueta.Length == 31)
        {
            cnpj = etiqueta.Substring(0, 14);
            numeroNota = etiqueta.Substring(14, 9);
            volAtual = etiqueta.Substring(23, 4);
            volTotal = etiqueta.Substring(27, 4);
            var empresa = (from c in data.tbEmpresas where c.cnpj == cnpj select c).First();
            var nota = (from c in data.tbNotaFiscals
                        join d in data.tbPedidoEnvios on c.idPedidoEnvio equals d.idPedidoEnvio
                        where c.idCNPJ == empresa.idEmpresa && c.numeroNota == int.Parse(numeroNota)
                        select new
                        {
                            c.idPedidoEnvio,
                            c.idPedido,
                            d.volumesEmbalados
                        }).First();
            var pacote =
                (from c in data.tbPedidoPacotes
                 where c.idPedidoEnvio == nota.idPedidoEnvio && c.numeroVolume == int.Parse(volAtual)
                 select c).FirstOrDefault();

            bool isError = false;
            if (pacote != null)
            {
                string retorno = "";
                pacote.idEnderecoPacote = enderecoFinal;
                string mensagem = "";
                if (pacote.tbPedidoEnvio.dataFimEmbalagem == null)
                {
                    isError = true;
                    mensagem = "Pedido não foi finalizado na embalagem";
                }

                if (pacote.peso == 0 && pacote.concluido == false)
                {
                    isError = true;
                    mensagem = "Pacote não foi pesado";
                }

                if (pacote.rastreio != "" | pacote.tbPedidoEnvio.nfeImpressa == true)
                {
                    isError = true;
                    mensagem = "Pedido já foi emitido pela transportadora";
                }
                InserirHistoricoEtiqueta(pacote.idPedidoPacote, enderecoFinal, idUsuario, usuario.nome, mensagem);

                data.SubmitChanges();
                if (isError)
                {
                    return "0|" + mensagem;
                }
                else
                {
                    retorno = "1|";
                    var pacotesEnvios = (from c in data.tbPedidoPacotes where c.idPedidoEnvio == pacote.idPedidoEnvio && c.idPedidoPacote != pacote.idPedidoPacote select c);
                    foreach (var pacoteEnvio in pacotesEnvios)
                    {
                        if (pacoteEnvio.idEnderecoPacote != null)
                        {
                            retorno += "Volume " + pacoteEnvio.numeroVolume + ": " + pacoteEnvio.tbEnderecoPacote.enderecoPacote + "<br>";
                        }
                    }
                    return retorno;
                }
            }
        }
        return "0|Etiqueta Inválida";
    }


    private void InserirHistoricoEtiqueta(int idPedidoPacote, int idPedidoPacoteEndereco, int idUsuarioExpedicao, string nomeUsuario, string mensagem)
    {
        var agora = DateTime.Now;
        var data = new dbCommerceDataContext();
        var historico = new tbPedidoPacoteEnderecoHistorico()
        {
            dataHora = agora,
            idPedidoPacote = idPedidoPacote,
            idPedidoPacoteEndereco = idPedidoPacoteEndereco,
            nomeUsuario = nomeUsuario,
            mensagem = mensagem
        };
        if (idUsuarioExpedicao > 0) historico.idUsuarioExpedicao = idUsuarioExpedicao;
        data.tbPedidoPacoteEnderecoHistoricos.InsertOnSubmit(historico);
        data.SubmitChanges();
    }
}
