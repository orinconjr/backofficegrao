﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Web.Configuration;
using System.Collections;

/// <summary>
/// Summary description for rnClientes
/// </summary>
public class rnClientes
{
    public static Page page()
    {
        return (Page)HttpContext.Current.Handler;
    }

    public static DataSet clienteSeleciona()
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("clienteSeleciona");

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }
    
    public static DataSet clienteSeleciona_PorClienteId()
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("clienteSeleciona_PorClienteId");

        db.AddInParameter(dbCommand, "clienteId", DbType.Int32, page().Request.Cookies["cliente"]["clienteId"].ToString());

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet clienteSelecionaAdmin_PorClienteId(int clienteId)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("clienteSeleciona_PorClienteId");

        db.AddInParameter(dbCommand, "clienteId", DbType.Int32, clienteId);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet clienteSeleciona_PorClienteEmail(string clienteEmail)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("clienteSeleciona_PorClienteEmail");

        db.AddInParameter(dbCommand, "clienteEmail", DbType.String, clienteEmail);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet clienteSeleciona_PorClienteEmailOuClienteCPFCNPJ(string clienteEmail, string clienteCPFCNPJ)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("clienteSeleciona_PorClienteEmailOuClienteCPFCNPJ");

        db.AddInParameter(dbCommand, "clienteEmail", DbType.String, clienteEmail);
        db.AddInParameter(dbCommand, "clienteCPFCNPJ", DbType.String, clienteCPFCNPJ);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet clienteSeleciona_PorClienteEmailOuClienteCPFCNPJClienteIdDiferente(string clienteEmail, string clienteCPFCNPJ, int clienteId)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("clienteSeleciona_PorClienteEmailOuClienteCPFCNPJClienteIdDiferente");

        db.AddInParameter(dbCommand, "clienteEmail", DbType.String, clienteEmail);
        db.AddInParameter(dbCommand, "clienteCPFCNPJ", DbType.String, clienteCPFCNPJ);
        db.AddInParameter(dbCommand, "clienteId", DbType.String, clienteId);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet clienteSelecionaCidade()
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("clienteSelecionaCidade");

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet clienteSelecionaEstado()
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("clienteSelecionaEstado");

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet clienteSeleciona_PorClienteEmailClienteSenha(string clienteEmail, string clienteSenha)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("clienteSeleciona_PorClienteEmailClienteSenha");

        db.AddInParameter(dbCommand, "clienteEmail", DbType.String, clienteEmail);
        db.AddInParameter(dbCommand, "clienteSenha", DbType.String, clienteSenha);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet clienteSelecionaUltimoId()
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("clienteSelecionaUltimoId");

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static bool clienteInclui(string clienteNome, string clienteNomeDaEmpresa, string clienteEmail, string clienteCPFCNPJ, string clienteRGIE, string clienteSenha, string ondeConheceuALoja, string clienteSexo, string clienteDataNascimento, 
                      string clienteEstadoCivil, string clienteFoneResidencial, string clienteFoneComercial, string clienteFoneCelular, string clienteCep, string clienteRua, string clienteBairro, string clienteNumero, string clienteComplemento,
                      string clienteCidade, string clienteEstado, string clientePais, string clienteReferenciaParaEntrega, string clienteRecebeInformativo, string melhorHorarioContato, string telRecado, string nomeRecado)
    {
        bool result = false;

        Database db = DatabaseFactory.CreateDatabase();

        DbCommand dbCommand = db.GetStoredProcCommand("clienteInclui");

        db.AddInParameter(dbCommand, "clienteNome", DbType.String, clienteNome);
        db.AddInParameter(dbCommand, "clienteNomeDaEmpresa", DbType.String, clienteNomeDaEmpresa);
        db.AddInParameter(dbCommand, "clienteEmail", DbType.String, clienteEmail);
        db.AddInParameter(dbCommand, "clienteCPFCNPJ", DbType.String, clienteCPFCNPJ);
        db.AddInParameter(dbCommand, "clienteRGIE", DbType.String, clienteRGIE);
        db.AddInParameter(dbCommand, "clienteSenha", DbType.String, clienteSenha);
        db.AddInParameter(dbCommand, "ondeConheceuALoja", DbType.String, ondeConheceuALoja);
        db.AddInParameter(dbCommand, "clienteSexo", DbType.String, clienteSexo);
        db.AddInParameter(dbCommand, "clienteDataNascimento", DbType.String, clienteDataNascimento);
        db.AddInParameter(dbCommand, "clienteEstadoCivil", DbType.String, clienteEstadoCivil);
        db.AddInParameter(dbCommand, "clienteFoneResidencial", DbType.String, clienteFoneResidencial);
        db.AddInParameter(dbCommand, "clienteFoneComercial", DbType.String, clienteFoneComercial);
        db.AddInParameter(dbCommand, "clienteFoneCelular", DbType.String, clienteFoneCelular);
        db.AddInParameter(dbCommand, "clienteCep", DbType.String, clienteCep);
        db.AddInParameter(dbCommand, "clienteRua", DbType.String, clienteRua);
        db.AddInParameter(dbCommand, "clienteBairro", DbType.String, clienteBairro);
        db.AddInParameter(dbCommand, "clienteNumero", DbType.String, clienteNumero);
        db.AddInParameter(dbCommand, "clienteComplemento", DbType.String, clienteComplemento);
        db.AddInParameter(dbCommand, "clienteCidade", DbType.String, clienteCidade);
        db.AddInParameter(dbCommand, "clienteEstado", DbType.String, clienteEstado);
        db.AddInParameter(dbCommand, "clientePais", DbType.String, clientePais);
        db.AddInParameter(dbCommand, "clienteReferenciaParaEntrega", DbType.String, clienteReferenciaParaEntrega);
        db.AddInParameter(dbCommand, "clienteRecebeInformativo", DbType.String, clienteRecebeInformativo);
        db.AddInParameter(dbCommand, "melhorHorarioContato", DbType.String, melhorHorarioContato);
        db.AddInParameter(dbCommand, "telRecado", DbType.String, telRecado);
        db.AddInParameter(dbCommand, "nomeRecado", DbType.String, nomeRecado);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
            DbTransaction transaction = connection.BeginTransaction();

            try
            {
                db.ExecuteNonQuery(dbCommand, transaction);

                transaction.Commit();

                result = true;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
                
            }
            connection.Close();

            return result;
        }
    }

    public static bool clienteAlterar(string clienteNome, string clienteNomeDaEmpresa, string clienteEmail, string clienteCPFCNPJ, string clienteRGIE, string clienteSenha, string ondeConheceuALoja, string clienteSexo, string clienteDataNascimento,
                  string clienteEstadoCivil, string clienteFoneResidencial, string clienteFoneComercial, string clienteFoneCelular, string clienteCep, string clienteRua, string clienteBairro, string clienteNumero, string clienteComplemento,
                  string clienteCidade, string clienteEstado, string clientePais, string clienteReferenciaParaEntrega, string clienteRecebeInformativo, int clienteId, string melhorHorarioContato, string telRecado, string nomeRecado)
    {
        bool result = false;

        Database db = DatabaseFactory.CreateDatabase();

        DbCommand dbCommand = db.GetStoredProcCommand("clienteAlterar");

        db.AddInParameter(dbCommand, "clienteNome", DbType.String, clienteNome);
        db.AddInParameter(dbCommand, "clienteNomeDaEmpresa", DbType.String, clienteNomeDaEmpresa);
        db.AddInParameter(dbCommand, "clienteEmail", DbType.String, clienteEmail);
        db.AddInParameter(dbCommand, "clienteCPFCNPJ", DbType.String, clienteCPFCNPJ);
        db.AddInParameter(dbCommand, "clienteRGIE", DbType.String, clienteRGIE);
        db.AddInParameter(dbCommand, "clienteSenha", DbType.String, clienteSenha);
        db.AddInParameter(dbCommand, "ondeConheceuALoja", DbType.String, ondeConheceuALoja);
        db.AddInParameter(dbCommand, "clienteSexo", DbType.String, clienteSexo);
        db.AddInParameter(dbCommand, "clienteDataNascimento", DbType.String, clienteDataNascimento);
        db.AddInParameter(dbCommand, "clienteEstadoCivil", DbType.String, clienteEstadoCivil);
        db.AddInParameter(dbCommand, "clienteFoneResidencial", DbType.String, clienteFoneResidencial);
        db.AddInParameter(dbCommand, "clienteFoneComercial", DbType.String, clienteFoneComercial);
        db.AddInParameter(dbCommand, "clienteFoneCelular", DbType.String, clienteFoneCelular);
        db.AddInParameter(dbCommand, "clienteCep", DbType.String, clienteCep);
        db.AddInParameter(dbCommand, "clienteRua", DbType.String, clienteRua);
        db.AddInParameter(dbCommand, "clienteBairro", DbType.String, clienteBairro);
        db.AddInParameter(dbCommand, "clienteNumero", DbType.String, clienteNumero);
        db.AddInParameter(dbCommand, "clienteComplemento", DbType.String, clienteComplemento);
        db.AddInParameter(dbCommand, "clienteCidade", DbType.String, clienteCidade);
        db.AddInParameter(dbCommand, "clienteEstado", DbType.String, clienteEstado);
        db.AddInParameter(dbCommand, "clientePais", DbType.String, clientePais);
        db.AddInParameter(dbCommand, "clienteReferenciaParaEntrega", DbType.String, clienteReferenciaParaEntrega);
        db.AddInParameter(dbCommand, "clienteRecebeInformativo", DbType.String, clienteRecebeInformativo);
        db.AddInParameter(dbCommand, "clienteId", DbType.Int32, clienteId);
        db.AddInParameter(dbCommand, "melhorHorarioContato", DbType.String, melhorHorarioContato);
        db.AddInParameter(dbCommand, "telRecado", DbType.String, telRecado);
        db.AddInParameter(dbCommand, "nomeRecado", DbType.String, nomeRecado);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
            DbTransaction transaction = connection.BeginTransaction();

            try
            {
                db.ExecuteNonQuery(dbCommand, transaction);

                transaction.Commit();

                result = true;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
                
            }
            connection.Close();

            return result;
        }
    }
}
