﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mime;
using System.Threading.Tasks;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Web.Configuration;
using System.Net.Mail;
using System.Text;
using AmazonSES;
using PostmarkDotNet;
using Attachment = System.Net.Mail.Attachment;

/// <summary>
/// Summary description for rnEmails
/// </summary>
public class rnEmails
{

    public static bool EnviaEmail(string de, string para, string cc, string cco, string responder, string mensagem, string assunto)
    {
        return EnviaEmailAutenticado(de, para, cc, cco, responder, mensagem, assunto);

        string remetente = "";
        if (de != "")
        {
            remetente = de;
        }
        else
        {
            remetente = ConfigurationManager.AppSettings["fromAddress"];
        }


        var destinatarios = new List<string>();
        if (para == "")
        {
            destinatarios.Add(ConfigurationManager.AppSettings["fromAddress"]);
        }
        else
        {
            var emailsPara = para.Split(',');
            foreach (var emailPara in emailsPara)
            {
                destinatarios.Add(emailPara);
            }
        }


        var copias = new List<string>();
        if (cc != "")
        {
            var emailsCC = cc.Split(',');
            foreach (var emailCC in emailsCC)
            {
                copias.Add(emailCC);
            }
        }


        var copiasocultas = new List<string>();
        if (cco != "")
        {
            var emailsCCO = cco.Split(',');
            foreach (var emailCCO in emailsCCO)
            {
                copiasocultas.Add(emailCCO);
            }
        }

        string replyTo = responder == "" ? ConfigurationManager.AppSettings["fromAddress"] : responder;

        var amazonMail = new AmazonSESWrapper("AKIAIP7IE6WVNK2K6TIQ", "9UkGi7cYKpBGFG60a/J0SWmziuu2rYNWLnoqVWi/");
        var retorno = amazonMail.SendEmail(destinatarios, copias, copiasocultas, remetente, replyTo, assunto, mensagem);
        return retorno.HasError;
    }

    public static bool EnviaEmailAutenticado(string de, string para, string cc, string cco, string responder, string mensagem, string assunto)
    {
        MailMessage mailMessage = new MailMessage();
        if (de != "")
        {
            mailMessage.From = new MailAddress(ConfigurationManager.AppSettings["fromAddress"], de);
        }
        else
        {
            mailMessage.From = new MailAddress(ConfigurationManager.AppSettings["fromAddress"], ConfigurationManager.AppSettings["nomeDoSite"]);
        }

        if (para == "")
        {
            mailMessage.To.Add(ConfigurationManager.AppSettings["fromAddress"]);
        }
        else
        {
            var emailsPara = para.Split(',');
            foreach (var emailPara in emailsPara)
            {
                mailMessage.To.Add(new MailAddress(emailPara));
            }
        }

        if (cc != "")
        {
            var emailsCC = cc.Split(',');
            foreach (var emailCC in emailsCC)
            {
                mailMessage.CC.Add(new MailAddress(emailCC));
            }
        }

        if (cco != "")
        {
            var emailsCCO = cco.Split(',');
            foreach (var emailCCO in emailsCCO)
            {
                mailMessage.Bcc.Add(new MailAddress(emailCCO));
            }
        }

        if (responder != "")
        {
            try
            {
                mailMessage.ReplyTo = new MailAddress(responder, de);
            }
            catch
            {
            }
        }

        mailMessage.Subject = assunto;
        mailMessage.Body = mensagem;
        mailMessage.IsBodyHtml = true;
        mailMessage.Priority = MailPriority.Normal;

        var smtpClient = new SmtpClient(ConfigurationManager.AppSettings["emailSmtp"]);
        smtpClient.EnableSsl = true;
        smtpClient.Port = 587;
        smtpClient.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["emailLogin"], ConfigurationManager.AppSettings["emailSenha"]);

        // Send Mail via SmtpClient
        smtpClient.Send(mailMessage);

        return false;
    }

    public static bool EnviaEmailComAnexo(string de, string para, string responder, string mensagem, string assunto, string anexo, string nomeAnexo, string contentTypeName)
    {

        MailMessage mailMessage = new MailMessage();
        mailMessage.From = new MailAddress(de);
        var emailsPara = para.Split(',');
        foreach (var emailPara in emailsPara)
        {
            mailMessage.To.Add(new MailAddress(emailPara));
        }
        mailMessage.ReplyTo = new MailAddress(responder);
        mailMessage.Subject = assunto;
        mailMessage.Body = mensagem;
        mailMessage.IsBodyHtml = true;
        mailMessage.Priority = MailPriority.Normal;

        var smtpClient = new SmtpClient(ConfigurationManager.AppSettings["emailSmtp"]);
        smtpClient.EnableSsl = true;
        smtpClient.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["emailLogin"], ConfigurationManager.AppSettings["emailSenha"]);

        using (MemoryStream memoryStream = new MemoryStream())
        {
            byte[] bom = { 0xEF, 0xBB, 0xBF };
            memoryStream.Write(bom, 0, bom.Length);

            byte[] contentAsBytes = Encoding.UTF8.GetBytes(anexo);
            memoryStream.Write(contentAsBytes, 0, contentAsBytes.Length);

            // Set the position to the beginning of the stream.
            memoryStream.Seek(0, SeekOrigin.Begin);

            // Create attachment
            ContentType contentType = new ContentType();
            contentType.MediaType = contentTypeName;
            contentType.Name = nomeAnexo;
            contentType.CharSet = "UTF-8";
            Attachment attachment = new Attachment(memoryStream, contentType);

            // Add the attachment
            mailMessage.Attachments.Add(attachment);

            // Send Mail via SmtpClient
            smtpClient.Send(mailMessage);
        }
        return true;
    }

    public static bool EnviaNotaTnt(int numeroNota)
    {
        //string xmlNota = rnNotaFiscal.retornaXmlNota(numeroNota);
        string xmlNota = "";

        MailMessage mailMessage = new MailMessage();
        mailMessage.From = new MailAddress("atendimento@graodegente.com.br");
        mailMessage.To.Add(new MailAddress("andre@bark.com.br"));
        mailMessage.To.Add(new MailAddress("edi@tntbrasil.com.br"));
        mailMessage.To.Add(new MailAddress("notaseletronicas@tntbrasil.com.br"));
        mailMessage.To.Add(new MailAddress("operacoes.bau@tntbrasil.com.br"));
        mailMessage.To.Add(new MailAddress("agendamento.nau@tntbrasil.com.br"));
        mailMessage.To.Add(new MailAddress("expedicao.bau@tntbrasil.com.br"));
        mailMessage.Subject = "NFe " + numeroNota;
        mailMessage.Body = "";
        mailMessage.IsBodyHtml = true;
        mailMessage.Priority = MailPriority.Normal;

        var smtpClient = new SmtpClient(ConfigurationManager.AppSettings["emailSmtp"]);
        smtpClient.EnableSsl = true;
        smtpClient.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["emailLogin"], ConfigurationManager.AppSettings["emailSenha"]);

        using (MemoryStream memoryStream = new MemoryStream())
        {
            byte[] bom = { 0xEF, 0xBB, 0xBF };
            memoryStream.Write(bom, 0, bom.Length);

            byte[] contentAsBytes = Encoding.UTF8.GetBytes(xmlNota);
            memoryStream.Write(contentAsBytes, 0, contentAsBytes.Length);

            // Set the position to the beginning of the stream.
            memoryStream.Seek(0, SeekOrigin.Begin);

            // Create attachment
            ContentType contentType = new ContentType();
            contentType.MediaType = "text/xml";
            contentType.Name = numeroNota + ".xml";
            contentType.CharSet = "UTF-8";
            Attachment attachment = new Attachment(memoryStream, contentType);

            // Add the attachment
            mailMessage.Attachments.Add(attachment);

            // Send Mail via SmtpClient
            smtpClient.Send(mailMessage);
        }
        return true;
    }

    public static bool enviaAvisoReposicaoDeEstoque(DataTable dtt)
    {
        try
        {
            StringBuilder conteudo = new StringBuilder();

            foreach (DataRow dr in dtt.Rows)
            {
                conteudo.Append("<table cellpadding='0' cellspacing='0' style='width: 650px; height:601px; background-image: url(" + ConfigurationManager.AppSettings["caminhoVirtual"] + "images/bkEmailAviseMe.jpg); background-repeat: no-repeat;'><tr><td valign='top' style='font-family:Arial; color: #000000; padding-left:50px;'><div style='font-size: 14px; padding-top:127px'><b>AVISE-ME</b></div><div style='font-size: 11px; padding-top:17px'>Prezado (a) Sr.(a)<br /><b>" + dr["clienteNome"].ToString() + "</b>, <br />você cadastrou seu e-mail à espera de que o produto:<br /><b>" + dr["produtoNome"].ToString() + "</b><br />estivesse novamente em estoque em nossa loja, o que acaba de ocorrer.</div><div style='font-size: 11px; padding-top:21px'><b>Clique <a style='font-family:Arial; font-size:11px; color:#000000' href=" + ConfigurationManager.AppSettings["caminhoVirtual"].ToString() + "produto/0/" + dr["produtoId"].ToString() + "/produto> <b>aqui</b></a> para visualizar o produto:</b></div><div style='font-size: 20px; padding-top:40px'><b>" + dr["produtoNome"].ToString() + "</b></div><div style='font-size: 11px; padding-top:35px'><br />Anteciosamente,<br /><b>Equipe " + ConfigurationManager.AppSettings["nomeDoSite"].ToString() + "</b></div><div style='font-size: 11px; padding-top:120px; padding-right:50px' align='center'><a style='font-family:Arial; font-size:11px; color:#000000' href=" + ConfigurationManager.AppSettings["caminhoVirtual"].ToString() + ">" + ConfigurationManager.AppSettings["caminhoVirtual"].ToString() + "</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a style='font-family:Arial; font-size:11px; color:#000000' href=mailto:" + ConfigurationManager.AppSettings["toAddress"].ToString() + ">" + ConfigurationManager.AppSettings["toAddress"].ToString() + "</a></div></td></tr></table>");

                /*MailMessage msg = new MailMessage(ConfigurationManager.AppSettings["fromAddress"], dr["clienteEmail"].ToString());
                msg.Subject = "Aviso de reposição de estoque " + ConfigurationManager.AppSettings["nomeDoSite"];
                msg.SubjectEncoding = Encoding.Default;
                msg.Body = conteudo.ToString();
                msg.BodyEncoding = Encoding.Default;
                msg.IsBodyHtml = true;*/

                rnEmails.EnviaEmail("", dr["clienteEmail"].ToString(), "", "", "", conteudo.ToString(), "Aviso de reposição de estoque " + ConfigurationManager.AppSettings["nomeDoSite"]);
                /*var smtpClient = new SmtpClient(ConfigurationManager.AppSettings["emailSmtp"]);
				smtpClient.EnableSsl = true;
				smtpClient.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["emailLogin"], ConfigurationManager.AppSettings["emailSenha"]);
				smtpClient.Send(msg);

                conteudo.Remove(0, conteudo.Length);*/
            }
        }
        catch (System.Net.Mail.SmtpException)
        {
            throw;
        }
        catch (Exception)
        {
            throw;
        }
        return true;
    }

    public static bool enviaIndicacaoDeProduto(string nome, string email, string nomeDoAmigo, string emailDoAmigo, int produtoId)
    {
        try
        {
            string produtoNome = rnProdutos.produtoSeleciona_PorProdutoId(produtoId).Tables[0].Rows[0]["produtoNome"].ToString();
            string produtoDescricao = rnProdutos.produtoSeleciona_PorProdutoId(produtoId).Tables[0].Rows[0]["produtoDescricao"].ToString();

            StringBuilder conteudo = new StringBuilder();

            conteudo.Append("<table cellpadding='0' cellspacing='0' style='width: 650px; height:601px; background-image: url(" + ConfigurationManager.AppSettings["caminhoVirtual"] + "images/bkEmailIndiqueOSite.jpg); background-repeat: no-repeat;'><tr><td valign='top' style='font-family:Arial; color: #000000; padding-left:50px;'><div style='font-size: 14px; padding-top:127px'><b>INDICAÇÃO</b></div><div style='font-size: 11px; padding-top:15px'>Prezado (a) Sr.(a)<br /><b>" + nomeDoAmigo + ",</b><br />Seu amigo(a) <b>" + nome + "</b> lhe indicou este produto do nosso site</div><div style='font-size: 20px; padding-top:25px'><b>" + produtoNome + "</b></div><div style='font-size: 11px; padding-top:15px; padding-right:60px'>" + produtoDescricao + "</div><br><div><b><a style='font-size: 14px; padding-top:21px; color: #000000;' href=" + ConfigurationManager.AppSettings["caminhoVirtual"].ToString() + "detalhes.aspx?produtoId=" + produtoId + ">VEJA O PRODUTO</a></b></div><br><br><br><br><div style='font-size: 11px; padding-top:120px; padding-right:50px' align='center'><a style='font-family:Arial; font-size:11px; color:#000000' href=" + ConfigurationManager.AppSettings["caminhoVirtual"].ToString() + ">" + ConfigurationManager.AppSettings["caminhoVirtual"].ToString() + "</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a style='font-family:Arial; font-size:11px; color:#000000' href=mailto:" + ConfigurationManager.AppSettings["toAddress"].ToString() + ">" + ConfigurationManager.AppSettings["toAddress"].ToString() + "</a></div></td></tr></table>");

            MailMessage msg = new MailMessage(email, emailDoAmigo);
            msg.Bcc.Add(new MailAddress(ConfigurationManager.AppSettings["toAddress"]));
            msg.Subject = "Indicação de produto do site " + ConfigurationManager.AppSettings["nomeDoSite"];
            msg.SubjectEncoding = Encoding.Default;
            msg.Body = conteudo.ToString();
            msg.BodyEncoding = Encoding.Default;
            msg.IsBodyHtml = true;

            var smtpClient = new SmtpClient(ConfigurationManager.AppSettings["emailSmtp"]);
            smtpClient.EnableSsl = true;
            smtpClient.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["emailLogin"], ConfigurationManager.AppSettings["emailSenha"]);
            smtpClient.Send(msg);
        }
        catch (System.Net.Mail.SmtpException)
        {
            throw;
        }
        catch (Exception)
        {
            throw;
        }
        return true;
    }

    public static bool enviaIndicacaoSite(string nome, string email, string nomeDoAmigo, string emailDoAmigo)
    {
        try
        {

            StringBuilder conteudo = new StringBuilder();

            conteudo.Append("<table cellpadding='0' cellspacing='0' style='width: 650px; height:601px; background-image: url(" + ConfigurationManager.AppSettings["caminhoVirtual"] + "images/bkEmailIndiqueOSite.jpg); background-repeat: no-repeat;'><tr><td valign='top' style='font-family:Arial; color: #000000; padding-left:50px;'><div style='font-size: 14px; padding-top:127px'><b>INDICAÇÃO</b></div><div style='font-size: 11px; padding-top:15px'>Prezado (a) Sr.(a)<br /><b>" + nomeDoAmigo + ",</b><br />Seu amigo(a) <b>" + nome + "</b> lhe indicou este site</div><div style='font-size: 20px; padding-top:25px'><b>" + ConfigurationManager.AppSettings["nomeDoSite"] + "</b></div><br><div><b><a style='font-size: 14px; padding-top:21px; color: #000000;' href=" + ConfigurationManager.AppSettings["caminhoVirtual"].ToString() + ">VEJA O SITE</a></b></div><br><br><br><br><div style='font-size: 11px; padding-top:120px; padding-right:50px' align='center'><a style='font-family:Arial; font-size:11px; color:#000000' href=" + ConfigurationManager.AppSettings["caminhoVirtual"].ToString() + ">" + ConfigurationManager.AppSettings["caminhoVirtual"].ToString() + "</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a style='font-family:Arial; font-size:11px; color:#000000' href=mailto:" + ConfigurationManager.AppSettings["toAddress"].ToString() + ">" + ConfigurationManager.AppSettings["toAddress"].ToString() + "</a></div></td></tr></table>");

            MailMessage msg = new MailMessage(email, emailDoAmigo);
            msg.Bcc.Add(new MailAddress(ConfigurationManager.AppSettings["toAddress"]));
            msg.Subject = "Indicação do site " + ConfigurationManager.AppSettings["nomeDoSite"];
            msg.SubjectEncoding = Encoding.Default;
            msg.Body = conteudo.ToString();
            msg.BodyEncoding = Encoding.Default;
            msg.IsBodyHtml = true;

            var smtpClient = new SmtpClient(ConfigurationManager.AppSettings["emailSmtp"]);
            smtpClient.EnableSsl = true;
            smtpClient.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["emailLogin"], ConfigurationManager.AppSettings["emailSenha"]);
            smtpClient.Send(msg);
        }
        catch (System.Net.Mail.SmtpException)
        {
            throw;
        }
        catch (Exception)
        {
            throw;
        }
        return true;
    }

    public static bool enviaFaleConosco(string nome, string email, string telefone, string assunto, string mensagem)
    {
        try
        {

            StringBuilder conteudo = new StringBuilder();

            conteudo.Append("<font size='2' face='Tahoma'>");

            conteudo.Append("O Srº(ª) " + "<b>" + nome + "</b>" + " entrou em contato pelo site www.yeshop.com.br no dia " + DateTime.Now.ToShortDateString() + " às " + DateTime.Now.ToShortTimeString() + "hrs" + "<br><br>");
            conteudo.Append("<b>" + "Nome do contato: " + "</b>" + nome + "<br>");
            conteudo.Append("<b>" + "Email: " + "</b>" + email + "<br>");
            conteudo.Append("<b>" + "Telefone: " + "</b>" + telefone + "<br>");
            conteudo.Append("<b>" + "Mensagem: " + "</b><br>" + mensagem);

            conteudo.Append("</font>");

            MailMessage msg = new MailMessage(email, ConfigurationManager.AppSettings["toAddress"]);
            msg.Subject = "Formulário fale conosco " + ConfigurationManager.AppSettings["nomeDoSite"] + "";
            msg.SubjectEncoding = Encoding.Default;
            msg.Body = conteudo.ToString();
            msg.BodyEncoding = Encoding.Default;
            msg.IsBodyHtml = true;

            var smtpClient = new SmtpClient(ConfigurationManager.AppSettings["emailSmtp"]);
            smtpClient.EnableSsl = true;
            smtpClient.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["emailLogin"], ConfigurationManager.AppSettings["emailSenha"]);
            smtpClient.Send(msg);
        }
        catch (System.Net.Mail.SmtpException)
        {
            throw;
        }
        catch (Exception)
        {
            throw;
        }
        return true;
    }

    public static bool enviaRecuperacaoDeSenha(int clienteId)
    {
        try
        {

            string clienteNome = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteNome"].ToString();
            string clienteEmail = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteEmail"].ToString();
            string clienteSenha = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteSenha"].ToString();

            StringBuilder conteudo = new StringBuilder();

            conteudo.Append("<table cellpadding='0' cellspacing='0' style='width: 650px; height:690px; background-image: url(" + ConfigurationManager.AppSettings["caminhoVirtual"] + "images/bkEmailRecuperacaoDeSenha.jpg); background-repeat: no-repeat;'><tr><td valign='top' style='font-family:Arial; color: #000000; padding-left:50px; '><div style='font-size: 14px; padding-top:127px'><b>RECUPERAÇÃO DE SENHA</b></div><div style='font-size: 11px; padding-top:25px'>Prezado (a) Sr.(a)<br /><b>" + clienteNome + ",</b><br />Atendendo seu pedido estamos lhe enviando sua <b>senha de acesso</b></div><div style='font-size: 11px; padding-top:21px'><b>DADOS PARA O LOGIN NO SITE<br /></b><br /><b>USUÁRIO</b><br /><br />" + clienteEmail + "<br /><br /><b>SENHA:<br /><br />" + clienteSenha + "</b></div><div style='font-size: 14px; padding-top: 37px'><b>IMPORTANTE</b></div><div style='font-size: 11px; padding-top: 5px'>Lembre-se de armazenar a sua senha com segurança.<br />Anteciosamente,<br /><b>Equipe " + ConfigurationManager.AppSettings["nomeDoSite"].ToString() + "</b></div><div style='font-size: 11px; padding-top:160px; padding-right:50px' align='center'><a style='font-family:Arial; font-size:11px; color:#000000' href=" + ConfigurationManager.AppSettings["caminhoVirtual"].ToString() + ">" + ConfigurationManager.AppSettings["caminhoVirtual"].ToString() + "</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a style='font-family:Arial; font-size:11px; color:#000000' href=mailto:" + ConfigurationManager.AppSettings["toAddress"].ToString() + ">" + ConfigurationManager.AppSettings["toAddress"].ToString() + "</a></div></td></tr></table>");

            MailMessage msg = new MailMessage(ConfigurationManager.AppSettings["toAddress"], clienteEmail);
            msg.Subject = "Recuperação de senha " + ConfigurationManager.AppSettings["nomeDoSite"] + "";
            msg.SubjectEncoding = Encoding.Default;
            msg.Body = conteudo.ToString();
            msg.BodyEncoding = Encoding.Default;
            msg.IsBodyHtml = true;

            var smtpClient = new SmtpClient(ConfigurationManager.AppSettings["emailSmtp"]);
            smtpClient.EnableSsl = true;
            smtpClient.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["emailLogin"], ConfigurationManager.AppSettings["emailSenha"]);
            smtpClient.Send(msg);
        }
        catch (System.Net.Mail.SmtpException)
        {
            throw;
        }
        catch (Exception)
        {
            throw;
        }
        return true;
    }

    public static bool enviaConfirmacaoDecadastro(string clienteNome, string clienteEmail, string clienteSenha)
    {
        try
        {
            StringBuilder conteudo = new StringBuilder();

            conteudo.Append("<table cellpadding='0' cellspacing='0' style='width: 650px; height:690px; background-image: url(" + ConfigurationManager.AppSettings["caminhoVirtual"] + "images/bkEmailRecuperacaoDeSenha.jpg); background-repeat: no-repeat;'><tr><td valign='top' style='font-family:Arial; color: #000000; padding-left:50px; '><div style='font-size: 14px; padding-top:127px'><b>CONFIRMAÇÃO DE CADASTRO</b></div><div style='font-size: 11px; padding-top:25px'>Prezado (a) Sr.(a)<br /><b>" + clienteNome + ",</b><br />Seu Cadastro foi realizado com sucesso no site <b>" + ConfigurationManager.AppSettings["nomeDoSite"].ToString() + ".</b></div><div style='font-size: 11px; padding-top:21px'><b>DADOS PARA O LOGIN NO SITE<br /></b><br /><b>USUÁRIO</b><br /><br />" + clienteEmail + "<br /><br /><b>SENHA:<br /><br />" + clienteSenha + "</b></div><div style='font-size: 14px; padding-top: 37px'><b>IMPORTANTE</b></div><div style='font-size: 11px; padding-top: 5px'>Lembre-se de armazenar a sua senha com segurança.<br />Anteciosamente,<br /><b>Equipe " + ConfigurationManager.AppSettings["nomeDoSite"].ToString() + "</b></div><div style='font-size: 11px; padding-top:160px; padding-right:50px' align='center'><a style='font-family:Arial; font-size:11px; color:#000000' href=" + ConfigurationManager.AppSettings["caminhoVirtual"].ToString() + ">" + ConfigurationManager.AppSettings["caminhoVirtual"].ToString() + "</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a style='font-family:Arial; font-size:11px; color:#000000' href=mailto:" + ConfigurationManager.AppSettings["toAddress"].ToString() + ">" + ConfigurationManager.AppSettings["toAddress"].ToString() + "</a></div></td></tr></table>");

            MailMessage msg = new MailMessage(ConfigurationManager.AppSettings["toAddress"], clienteEmail);
            msg.Bcc.Add(new MailAddress(ConfigurationManager.AppSettings["toAddress"]));
            msg.Subject = "Confirmação de cadastro " + ConfigurationManager.AppSettings["nomeDoSite"] + "";
            msg.SubjectEncoding = Encoding.Default;
            msg.Body = conteudo.ToString();
            msg.BodyEncoding = Encoding.Default;
            msg.IsBodyHtml = true;

            var smtpClient = new SmtpClient(ConfigurationManager.AppSettings["emailSmtp"]);
            smtpClient.EnableSsl = true;
            smtpClient.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["emailLogin"], ConfigurationManager.AppSettings["emailSenha"]);
            smtpClient.Send(msg);
        }
        catch (System.Net.Mail.SmtpException)
        {
            throw;
        }
        catch (Exception)
        {
            throw;
        }
        return true;
    }

    public static bool enviaConfirmacaoDePedido(string clienteNome, string clienteEmail, string pedidoId, string produtos, string nomeDoDestinatario, string rua, string numero, string complemento, string bairro, string cep, string cidade, string estado, string pais, string referenciaParaEntrega, string subTotal, string valorDoFrete, string valorDescontoPagamento, string cupomDeDesconto, string embalagens, string total)
    {
        try
        {
            StringBuilder conteudo = new StringBuilder();

            conteudo.Append("<table cellpadding='0' cellspacing='0' style='width: 650px'><tr><td style='background-image: url(" + ConfigurationManager.AppSettings["caminhoVirtual"] + "images/bkTopoEmailConfirmacao.jpg); background-repeat: no-repeat; font-family:Arial; color: #000000; padding-left:50px; padding-bottom: 35px' valign='top'><div style='font-size: 14px; padding-top:125px'><b>CONFIRMAÇÃO DO PEDIDO</b></div><div style='font-size: 11px; padding-top:30px'>Olá Sr. (a) <b>" + clienteNome + ",</b><br />Agradecemos sua referencia pela <b>" + ConfigurationManager.AppSettings["nomeDoSite"] + ".</b><br /><br />Confirmamos a finalização de seu pedido em nosso site.<br /><br />Por favor, confira os dados abaixo e lembre-se de que os mesmo devem estar corretos e completos para que a entrega seja efetuada com sucesso.<br /><br /><b>Número do Pedido:</b></div><div style='font-size: 20px;'>" + rnFuncoes.retornaIdCliente(Convert.ToInt32(pedidoId)) + "</div><div style='font-size: 14px; padding-top:15px;'><b>DETALHES DO PEDIDO</b></div></td></tr><tr><td align='center'>" + produtos + "</td></tr><tr><td style='background-image: url(" + ConfigurationManager.AppSettings["caminhoVirtual"] + "images/bkRodapeEmailConfirmacao.jpg); background-repeat: no-repeat; font-family:Arial; color: #000000; padding-left:50px; padding-bottom: 60px; padding-right: 50px;'><table cellpadding='0' cellspacing='0' style='width: 540px'><tr><td valign='top' width='315'><div style='font-size: 14px; padding-top:18px; padding-left:5px;'><b>ENDEREÇOS</b></div><div style='font-size: 14px; padding-top:15px'><b>NOME DO DESTINATÁRIO</b></div><div style='font-size: 11px;'>" + nomeDoDestinatario + "</div><div style='font-size: 14px; padding-top:15px'><b>ENDEREÇO DE ENTREGA</b></div><div style='font-size: 11px; padding-right:25px;'>" + rua + ", " + numero + " " + complemento + "<br>" + bairro + "<br>Cep: " + cep + " - " + cidade + " - " + estado + " - " + pais + "</div><div style='font-size: 14px; padding-top:15px'><b>REFERÊNCIA PARA ENTREGA</b></div><div style='font-size: 11px;'>" + referenciaParaEntrega + "</b></div></td><td valign='top' align='right' width='100'><div style='font-size: 14px; padding-top:18px;' align='left'><b>VALORES</b></div><div style='font-size: 11px; padding-top:18px;'><b>Sub Total</b></div><div style='font-size: 11px; padding-top:25px;'><b>Valor do Frete</b></div><div style='font-size: 11px; padding-top:15px;'><b>Valor Desconto</b><br />(Forma de Pagamento)</div><div style='font-size: 11px; padding-top:5px;'><b>Cupom de Desconto</b></div><div style='font-size: 11px; padding-top:5px;'><b>Embalagens para Presente</b></div><div style='font-size: 11px; padding-top:15px;'><b>Total</b></div></td><td align='right' valign='top'><div style='font-size: 11px; padding-top:53px;'><b>" + subTotal + "</b></div><div style='font-size: 11px; padding-top:25px;'><b>" + valorDoFrete + "</b></div><div style='font-size: 11px; padding-top:30px;'><b>" + valorDescontoPagamento + "</b></div><div style='font-size: 11px; padding-top:25px;'><b>" + cupomDeDesconto + "</b></div><div style='font-size: 11px; padding-top:20px;'><b>" + embalagens + "</b></div><div style='font-size: 11px; padding-top:20px;'><b>" + total + "</b></div></td></tr></table><div style='font-size: 11px; padding-top:150px;' align='center'><a style='font-family:Arial; font-size:11px; color:#000000' href=" + ConfigurationManager.AppSettings["caminhoVirtual"].ToString() + ">" + ConfigurationManager.AppSettings["caminhoVirtual"].ToString() + "</a> - <a style='font-family:Arial; font-size:11px; color:#000000' href=mailto:" + ConfigurationManager.AppSettings["toAddress"].ToString() + ">" + ConfigurationManager.AppSettings["toAddress"].ToString() + "</a></div></td></tr></table>");

            MailMessage msg = new MailMessage(ConfigurationManager.AppSettings["fromAddress"], clienteEmail);
            msg.Bcc.Add(new MailAddress(ConfigurationManager.AppSettings["toAddress"]));
            msg.Subject = "Confirmação de pedido " + ConfigurationManager.AppSettings["nomeDoSite"] + "";
            msg.SubjectEncoding = Encoding.Default;
            msg.Body = conteudo.ToString();
            msg.BodyEncoding = Encoding.Default;
            msg.IsBodyHtml = true;

            var smtpClient = new SmtpClient(ConfigurationManager.AppSettings["emailSmtp"]);
            smtpClient.EnableSsl = true;
            smtpClient.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["emailLogin"], ConfigurationManager.AppSettings["emailSenha"]);
            smtpClient.Send(msg);
        }
        catch (System.Net.Mail.SmtpException)
        {
            throw;
        }
        catch (Exception)
        {
            throw;
        }
        return true;
    }

    public static bool enviaAlteracaoDeStatus(string clienteNome, string pedidoId, string status, string clienteEmail)
    {
        int idPedido = Convert.ToInt32(pedidoId);
        var pedidoDc = new dbCommerceDataContext();
        var pedido = (from c in pedidoDc.tbPedidos where c.pedidoId == idPedido select c).First();
        if (pedido.statusDoPedido == 3)
        {
            string frasePrazoPrevisto = "";
            /*if (pedido.prazoFinalPedido != null)
            {
                frasePrazoPrevisto = "A data prevista de postagem do seu produto é: " +
                                     pedido.prazoFinalPedido.Value.ToShortDateString() + "<br>";
            }*/
            StringBuilder conteudo = new StringBuilder();
            conteudo.Append("<table cellpadding='0' cellspacing='0' style='width: 650px; height:601px; background-image: url(" + ConfigurationManager.AppSettings["caminhoVirtual"] + "images/bkEmailAlteracaoDoPedido.jpg); background-repeat: no-repeat;'><tr><td valign='top' style='font-family:Arial; color: #000000; padding-left:50px; '><div style='font-size: 14px; padding-top:127px'><b>ALTERAÇÃO DA SITUAÇÃO DO PEDIDO</b></div><div style='font-size: 11px; padding-top:25px'>Prezado (a) Sr.(a)<br /><b>" + clienteNome + ",</b><br />Informamos que a situação do seu pedido <b>nº " + rnFuncoes.retornaIdCliente(Convert.ToInt32(pedidoId)) + "</b> foi alterada para:</div><div style='font-size: 20px; padding-top:21px'><b>&quot;" + status + "&quot;</b></div><div style='font-size: 11px; padding-top:10px'><b>" + frasePrazoPrevisto + "Para acompanhá-lo, Clique no link abaixo</b></div><div'><a  style='font-size: 14px; padding-top:21px; color: #000000;' href=" + ConfigurationManager.AppSettings["caminhoVirtual"].ToString() + "/pedidos.aspx" + ">MEUS PEDIDOS</a></div><br /><div style='font-size: 11px; padding-top:35px'><b>Anteciosamente,<br />Equipe " + ConfigurationManager.AppSettings["nomeDoSite"].ToString() + "</b></div><div style='font-size: 11px; padding-top:120px; padding-right:50px' align='center'><a style='font-family:Arial; font-size:11px; color:#000000' href=" + ConfigurationManager.AppSettings["caminhoVirtual"].ToString() + ">" + ConfigurationManager.AppSettings["caminhoVirtual"].ToString() + "</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a style='font-family:Arial; font-size:11px; color:#000000' href=mailto:" + ConfigurationManager.AppSettings["toAddress"].ToString() + ">" + ConfigurationManager.AppSettings["toAddress"].ToString() + "</a></div></td></tr></table>");
            return rnEmails.EnviaEmail("", clienteEmail, "", "", "", conteudo.ToString(), "Alteração de status " + ConfigurationManager.AppSettings["nomeDoSite"] + "");
        }
        else
        {
            StringBuilder conteudo = new StringBuilder();
            conteudo.Append("<table cellpadding='0' cellspacing='0' style='width: 650px; height:601px; background-image: url(" + ConfigurationManager.AppSettings["caminhoVirtual"] + "images/bkEmailAlteracaoDoPedido.jpg); background-repeat: no-repeat;'><tr><td valign='top' style='font-family:Arial; color: #000000; padding-left:50px; '><div style='font-size: 14px; padding-top:127px'><b>ALTERAÇÃO DA SITUAÇÃO DO PEDIDO</b></div><div style='font-size: 11px; padding-top:25px'>Prezado (a) Sr.(a)<br /><b>" + clienteNome + ",</b><br />Informamos que a situação do seu pedido <b>nº " + rnFuncoes.retornaIdCliente(Convert.ToInt32(pedidoId)) + "</b> foi alterada para:</div><div style='font-size: 20px; padding-top:21px'><b>&quot;" + status + "&quot;</b></div><div style='font-size: 11px; padding-top:10px'><b>Para acompanhá-lo, Clique no link abaixo</b></div><div'><a  style='font-size: 14px; padding-top:21px; color: #000000;' href=" + ConfigurationManager.AppSettings["caminhoVirtual"].ToString() + "/pedidos.aspx" + ">MEUS PEDIDOS</a></div><br /><div style='font-size: 11px; padding-top:35px'><b>Anteciosamente,<br />Equipe " + ConfigurationManager.AppSettings["nomeDoSite"].ToString() + "</b></div><div style='font-size: 11px; padding-top:120px; padding-right:50px' align='center'><a style='font-family:Arial; font-size:11px; color:#000000' href=" + ConfigurationManager.AppSettings["caminhoVirtual"].ToString() + ">" + ConfigurationManager.AppSettings["caminhoVirtual"].ToString() + "</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a style='font-family:Arial; font-size:11px; color:#000000' href=mailto:" + ConfigurationManager.AppSettings["toAddress"].ToString() + ">" + ConfigurationManager.AppSettings["toAddress"].ToString() + "</a></div></td></tr></table>");
            return rnEmails.EnviaEmail(ConfigurationManager.AppSettings["emailPosVendas"], clienteEmail, "", "", "", conteudo.ToString(), "Alteração de status " + ConfigurationManager.AppSettings["nomeDoSite"] + "");
        }
        //Pagamento Confirmado - ID 3

        //Pedido sendo embalado - ID 4

        //Pedido enviado - ID 5

        //Pedido cancelado - ID 6


        //Pagamento não autorizado - ID 7


        //Separação de Estoque - ID 11



    }

    public static bool enviaCodigoDoTraking(string clienteNome, string pedidoId, string tracking, string tracking2, string tracking3, string tracking4, string clienteEmail)
    {
        try
        {
            StringBuilder conteudo = new StringBuilder();

            int remessas = 0;
            if (tracking != "") remessas = remessas + 1;
            if (tracking2 != "") remessas = remessas + 1;
            if (tracking3 != "") remessas = remessas + 1;
            if (tracking4 != "") remessas = 1;

            string codigoTracking = "";
            if (tracking != "") codigoTracking = tracking;
            if (tracking2 != "") codigoTracking = codigoTracking + ", " + tracking2;
            if (tracking3 != "") codigoTracking = codigoTracking + ", " + tracking3;
            if (tracking4 != "") codigoTracking = tracking4;

            string urlTracking = "http://websro.correios.com.br/sro_bin/txect01$.Inexistente?P_LINGUA=001&P_TIPO=002&P_COD_LIS=";
            if (tracking != "") urlTracking = urlTracking + tracking;
            if (tracking2 != "") urlTracking = urlTracking + "%2C+" + tracking2;
            if (tracking3 != "") urlTracking = urlTracking + "%2C+" + tracking3;

            //if (tracking4 != "") codigoTracking = tracking;

            conteudo.Append("<table cellpadding='0' cellspacing='0' style='width: 650px; height:601px; background-image: url(" + ConfigurationManager.AppSettings["caminhoVirtual"] + "images/bkEmailAlteracaoDoPedido.jpg); background-repeat: no-repeat;'><tr><td valign='top' style='font-family:Arial; color: #000000; padding-left:50px; '><div style='font-size: 14px; padding-top:127px'><b>ENVIO DO CÓDIGO DE RASTREAMENTO</b></div>");
            conteudo.Append("<div style='font-size: 11px; padding-top:25px'>Prezado (a) Sr.(a)<br /><b>" + clienteNome + ",</b><br />");
            if (remessas == 1) conteudo.Append("Seu pedido <b>nº " + rnFuncoes.retornaIdCliente(Convert.ToInt32(pedidoId)) + "</b> foi enviado em " + remessas + " remessa. Para rastreá-lo acesse os códigos abaixo:</div>");
            else if (remessas > 1) conteudo.Append("Seu pedido <b>nº " + rnFuncoes.retornaIdCliente(Convert.ToInt32(pedidoId)) + "</b> foi enviado em " + remessas + " remessas. Para rastreá-lo acesse os códigos abaixo:</div>");
            if (tracking != "" | tracking2 != "" | tracking3 != "") conteudo.Append("<div style='font-size: 11px; padding-top:30px'><b>Para acompanhá-lo, <a  style='font-size: 11px; padding-top:21px; color: #000000;' href=" + urlTracking + ">Clique Aqui</a></b></div>");
            else if (tracking4 != "") conteudo.Append("<div style='font-size: 11px; padding-top:30px'><b>Para acompanhá-lo, <a  style='font-size: 11px; padding-top:21px; color: #000000;' href=http://www.jadlog.com.br/tracking.jsp>Clique Aqui</a></b></div>");
            conteudo.Append("<div style='font-size: 11px; padding-top:10px'><b>Código de rastreamento:</b> " + codigoTracking + "</div>");
            conteudo.Append("<div style='padding-top:35px'><br /><div style='font-size: 11px; padding-top:35px'><b>Atenciosamente,<br />Equipe " + ConfigurationManager.AppSettings["nomeDoSite"].ToString() + "</b></div><div style='font-size: 11px; padding-top:120px; padding-right:50px' align='center'><a style='font-family:Arial; font-size:11px; color:#000000' href=" + ConfigurationManager.AppSettings["caminhoVirtual"].ToString() + ">" + ConfigurationManager.AppSettings["caminhoVirtual"].ToString() + "</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a style='font-family:Arial; font-size:11px; color:#000000' href=mailto:" + ConfigurationManager.AppSettings["toAddress"].ToString() + ">" + ConfigurationManager.AppSettings["toAddress"].ToString() + "</a></div></td></tr></table>");

            return rnEmails.EnviaEmail(ConfigurationManager.AppSettings["emailPosVendas"], clienteEmail, "", "", "", conteudo.ToString(), "Código do rastreamento");

            /*MailMessage msg = new MailMessage(ConfigurationManager.AppSettings["toAddress"], clienteEmail);
            msg.Subject = "Código do rastreamento";
            msg.SubjectEncoding = Encoding.Default;
            msg.Body = conteudo.ToString();
            msg.BodyEncoding = Encoding.Default;
            msg.IsBodyHtml = true;
			
            var smtpClient = new SmtpClient(ConfigurationManager.AppSettings["emailSmtp"]);
            smtpClient.EnableSsl = true;
            smtpClient.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["emailLogin"], ConfigurationManager.AppSettings["emailSenha"]);
            smtpClient.Send(msg);*/
        }
        catch (System.Net.Mail.SmtpException)
        {
            throw;
        }
        catch (Exception)
        {
            throw;
        }
        return true;
    }

    public static bool enviaCodigoDoTrakingJadlog(int idPedido)
    {
        return enviaCodigoDoTrakingJadlog(idPedido, "", "");
    }
    public static bool enviaCodigoDoTrakingJadlogAsync(int idPedido, string numeroTracking)
    {
        return enviaCodigoDoTrakingJadlog(idPedido, numeroTracking);
    }
    public static bool enviaCodigoDoTrakingJadlog(int idPedido, string numeroTracking)
    {
        return enviaCodigoDoTrakingJadlog(idPedido, numeroTracking, "");
    }

    /*public static bool enviaCodigoDoTrakingJadlog(int idPedido, string numeroTracking, string email)
    {
        try
        {
            StringBuilder conteudo = new StringBuilder();

            var pedidoDc = new dbCommerceDataContext();
            var pedido = (from c in pedidoDc.tbPedidos where c.pedidoId == idPedido select c).First();
            var clienteDc = new dbCommerceDataContext();
            var cliente = (from c in clienteDc.tbClientes where c.clienteId == pedido.clienteId select c).First();

            string clienteNome = cliente.clienteNome;
            string clienteEmail = cliente.clienteEmail;

            if (!string.IsNullOrEmpty(email)) clienteEmail = email;

            string codigoTracking = numeroTracking;
            string urlTracking = "";
            if (!string.IsNullOrEmpty(numeroTracking))
            {
                urlTracking = "http://jadlog.com.br/tracking.jsp?cte=" + numeroTracking;
            }
            else
            {
                urlTracking = "http://jadlog.com.br/tracking.jsp?cte=" + pedido.numeroDoTracking4;
                codigoTracking = pedido.numeroDoTracking4;
            }
            conteudo.Append("<table cellpadding='0' cellspacing='0' style='width: 650px; height:601px;'><tr><td valign='top' style='font-family:Arial; color: #000000; padding-left:50px; '><div style='font-size: 14px; padding-top:15px'><b>ACOMPANHE A ENTREGA DO SEU PEDIDO</b></div>");
            conteudo.Append("<div style='font-size: 11px; padding-top:25px'>Prezado (a) Sr.(a)<br /><b>" + clienteNome + ",</b><br />");
            conteudo.Append("Seu pedido <b>nº " + rnFuncoes.retornaIdCliente(Convert.ToInt32(idPedido)) + "</b> foi coletado pela transportadora Jadlog.<br />Você receberá seu pedido em até <b>" + pedido.prazoDeEntrega + " dias úteis.</b><br />Para rastrear o andamento da entrega acesse o link abaixo:<br /></div>");
            conteudo.Append("<div style='font-size: 11px; padding-top:30px'><b><a  style='font-size: 11px; padding-top:21px; color: #000000;' href=" + urlTracking + ">" + urlTracking + "</a></b></div>");
            conteudo.Append("<div style='font-size: 11px; padding-top:10px'><b>Código de rastreamento:</b> " + codigoTracking + "</div>");
            conteudo.Append("<div style='font-size: 11px; padding-top:10px'><b>O link acima poderá levar até 24 horas para exibir as informações atualizadas, devido ao tempo de processamento da transportadora.</b></div>");
            conteudo.Append("<div style='padding-top:10px'><br /><div style='font-size: 11px; padding-top:10px'><b>Atenciosamente,<br />Equipe Grão de Gente<br />SAC: (11) 3522-8379<br />posvendas@graodegente.com.br<br />http://www.graodegente.com.br</b></div></td></tr></table>");

            return rnEmails.EnviaEmail(ConfigurationManager.AppSettings["emailPosVendas"], clienteEmail, "", "", "", conteudo.ToString(), "Rastreamento de Entrega");
			
            /*MailMessage msg = new MailMessage(ConfigurationManager.AppSettings["toAddress"], clienteEmail);
            msg.Subject = "Código do rastreamento";
            msg.SubjectEncoding = Encoding.Default;
            msg.Body = conteudo.ToString();
            msg.BodyEncoding = Encoding.Default;
            msg.IsBodyHtml = true;
			
            var smtpClient = new SmtpClient(ConfigurationManager.AppSettings["emailSmtp"]);
			smtpClient.EnableSsl = true;
            smtpClient.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["emailLogin"], ConfigurationManager.AppSettings["emailSenha"]);
            smtpClient.Send(msg);
        }
        catch (System.Net.Mail.SmtpException)
        {
            throw;
        }
        catch (Exception)
        {
            throw;
        }
        return true;
    }
*/

    public static bool enviaCodigoDoTrakingTnt(int idPedidoEnvio)
    {
        return rnEmails.enviaCodigoDoTrakingTnt(idPedidoEnvio, "");
    }

    /*public static bool enviaCodigoDoTrakingTnt(int idPedidoEnvio, string email)
    {
        try
        {
            StringBuilder conteudo = new StringBuilder();

            var pedidoDc = new dbCommerceDataContext();
            var envio = (from c in pedidoDc.tbPedidoEnvios where c.idPedidoEnvio == idPedidoEnvio select c).First();
            var pedido = (from c in pedidoDc.tbPedidoEnvios where c.idPedidoEnvio == idPedidoEnvio select c.tbPedido).First();
            var clienteDc = new dbCommerceDataContext();
            var cliente = (from c in clienteDc.tbClientes where c.clienteId == pedido.clienteId select c).First();

            string clienteNome = cliente.clienteNome;
            string clienteEmail = cliente.clienteEmail;

            if (!string.IsNullOrEmpty(email)) clienteEmail = email;

            string urlTracking = "http://app.tntbrasil.com.br/LocalizacaoSimplificada/resultado_localizacao.asp?cnpj=" + cliente.clienteCPFCNPJ + "&amp;nota=" + envio.nfeNumero;
            


            conteudo.Append("<table cellpadding='0' cellspacing='0' style='width: 650px; height:601px;'><tr><td valign='top' style='font-family:Arial; color: #000000; padding-left:50px; '><div style='font-size: 14px; padding-top:15px'><b>ACOMPANHE A ENTREGA DO SEU PEDIDO</b></div>");
            conteudo.Append("<div style='font-size: 11px; padding-top:25px'>Prezado (a) Sr.(a)<br /><br /><b>" + clienteNome + ",</b><br />");
            conteudo.Append("Seu pedido <b>nº " + rnFuncoes.retornaIdCliente(Convert.ToInt32(pedido.pedidoId)) + "</b> foi coletado pela transportadora TNT Mercúrio.<br />Você receberá seu pedido em até <b>" + pedido.prazoDeEntrega + " dias úteis.</b><br />Para rastrear o andamento da entrega acesse o link abaixo:<br /></div>");
            conteudo.Append("<div style='font-size: 11px; padding-top:30px'><b><a href='" + urlTracking + "'>" + urlTracking + "</a></b></div>");
            conteudo.Append("<div style='font-size: 11px; padding-top:10px'><b>O link acima poderá levar até 24 horas para exibir as informações atualizadas, devido ao tempo de processamento da transportadora.</b></div>");
            conteudo.Append("<div style='padding-top:10px'><br /><div style='font-size: 11px; padding-top:10px'><b>Atenciosamente,<br />Equipe Grão de Gente<br />SAC: (11) 3522-8379<br />posvendas@graodegente.com.br<br />http://www.graodegente.com.br</b></div></td></tr></table>");

            return rnEmails.EnviaEmail(ConfigurationManager.AppSettings["emailPosVendas"], clienteEmail, "", "", "", conteudo.ToString(), "Rastreamento de Entrega");
			
            /*MailMessage msg = new MailMessage(ConfigurationManager.AppSettings["toAddress"], clienteEmail);
            msg.Subject = "Código do rastreamento";
            msg.SubjectEncoding = Encoding.Default;
            msg.Body = conteudo.ToString();
            msg.BodyEncoding = Encoding.Default;
            msg.IsBodyHtml = true;
			
            var smtpClient = new SmtpClient(ConfigurationManager.AppSettings["emailSmtp"]);
			smtpClient.EnableSsl = true;
            smtpClient.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["emailLogin"], ConfigurationManager.AppSettings["emailSenha"]);
            smtpClient.Send(msg);
        }
        catch (System.Net.Mail.SmtpException)
        {
            throw;
        }
        catch (Exception)
        {
            throw;
        }
        return true;
    }*/

    /*public static bool enviaCodigoDoTrakingCorreios(string clienteNome, string pedidoId, string clienteEmail)
    {
        try
        {
            StringBuilder conteudo = new StringBuilder();

            int idPedido = Convert.ToInt32(pedidoId);
            var pedidosDc = new dbCommerceDataContext();
            var volumesPedido = (from c in pedidosDc.tbPedidoPacotes where c.idPedido == idPedido select c);
            var pedido = (from c in pedidosDc.tbPedidos where c.pedidoId == idPedido select c).First();
            int volumeAtual = 0;
            string codigoTracking = "";
            string urlTracking = "http://websro.correios.com.br/sro_bin/txect01$.Inexistente?P_LINGUA=001&P_TIPO=002&P_COD_LIS=";


            var cliente = (from c in pedidosDc.tbClientes where c.clienteId == pedido.clienteId select c).First();

            string nome = cliente.clienteNome;
            string email = cliente.clienteEmail;
            if (!string.IsNullOrEmpty(clienteEmail)) email = clienteEmail;


            foreach (var pacote in volumesPedido)
            {
                if (volumeAtual == 0)
                {
                    codigoTracking = pacote.rastreio;
                    urlTracking = urlTracking + pacote.rastreio;
                }
                else
                {
                    codigoTracking = codigoTracking + ", " + pacote.rastreio;
                    urlTracking = urlTracking + "%2C+" + pacote.rastreio;
                }
                volumeAtual++;
            }


            conteudo.Append("<table cellpadding='0' cellspacing='0' style='width: 650px; height:601px;'><tr><td valign='top' style='font-family:Arial; color: #000000; padding-left:50px; '><div style='font-size: 14px; padding-top:15px'><b>ACOMPANHE A ENTREGA DO SEU PEDIDO</b></div>");
            conteudo.Append("<div style='font-size: 11px; padding-top:25px'>Prezado (a) Sr.(a)<br /><br /><b>" + nome + ",</b><br />");
            if (volumeAtual == 1) conteudo.Append("Seu pedido <b>nº " + rnFuncoes.retornaIdCliente(Convert.ToInt32(pedido.pedidoId)) + "</b> foi enviado.<br />Você receberá seu pedido em até <b>" + pedido.prazoDeEntrega + " dias úteis.</b><br />Para rastrear o andamento da entrega acesse o link abaixo:<br /></div>");
            else if (volumeAtual > 1) conteudo.Append("Seu pedido <b>nº " + rnFuncoes.retornaIdCliente(Convert.ToInt32(pedido.pedidoId)) + "</b> foi enviado em " + volumeAtual + " remessas.<br />Você receberá seu pedido em até <b>" + pedido.prazoDeEntrega + " dias úteis.</b><br />Para rastrear o andamento da entrega acesse o link abaixo:<br /></div>");
            conteudo.Append("<div style='font-size: 11px; padding-top:30px'><b><a  style='font-size: 11px; padding-top:21px; color: #000000;' href=" + urlTracking + ">" + urlTracking + "</a></b></div>");
            conteudo.Append("<div style='font-size: 11px; padding-top:10px'><b>Código de rastreamento:</b> " + codigoTracking + "</div>");
            conteudo.Append("<div style='font-size: 11px; padding-top:10px'><b>O link acima poderá levar até 24 horas para exibir as informações atualizadas, devido ao tempo de processamento da transportadora.</b></div>");
            conteudo.Append("<div style='padding-top:10px'><br /><div style='font-size: 11px; padding-top:10px'><b>Atenciosamente,<br />Equipe Grão de Gente<br />SAC: (11) 3522-8379<br />posvendas@graodegente.com.br<br />http://www.graodegente.com.br</b></div></td></tr></table>");

            return rnEmails.EnviaEmail(ConfigurationManager.AppSettings["emailPosVendas"], email, "", "", "", conteudo.ToString(), "Rastreamento de Entrega");
			
            /*MailMessage msg = new MailMessage(ConfigurationManager.AppSettings["toAddress"], clienteEmail);
            msg.Subject = "Código do rastreamento";
            msg.SubjectEncoding = Encoding.Default;
            msg.Body = conteudo.ToString();
            msg.BodyEncoding = Encoding.Default;
            msg.IsBodyHtml = true;
			
            var smtpClient = new SmtpClient(ConfigurationManager.AppSettings["emailSmtp"]);
			smtpClient.EnableSsl = true;
            smtpClient.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["emailLogin"], ConfigurationManager.AppSettings["emailSenha"]);
            smtpClient.Send(msg);
        }
        catch (System.Net.Mail.SmtpException)
        {
            throw;
        }
        catch (Exception)
        {
            throw;
        }
        return true;
    }
*/

    public static bool enviaCupomDeDesconto(string clienteNome, string codigoDoCupom, string porcentagemDoDesconto, string clienteEmail)
    {
        try
        {
            StringBuilder conteudo = new StringBuilder();

            conteudo.Append("<table cellpadding='0' cellspacing='0' style='width: 650px; height:601px; background-image: url(" + ConfigurationManager.AppSettings["caminhoVirtual"] + "images/bkEmailAlteracaoDoPedido.jpg); background-repeat: no-repeat;'><tr><td valign='top' style='font-family:Arial; color: #000000; padding-left:50px;'><div style='font-size: 14px; padding-top:127px'><b>CUPOM DE DESCONTO</b></div><div style='font-size: 11px; padding-top:25px'>Prezado (a) Sr.(a)<br /><b>" + clienteNome + ",</b><br />Você acaba de ganhar <b>" + porcentagemDoDesconto + "%</b> de desconto na sua próxima compra. </div><div style='font-size: 11px; padding-top:10px'><b>Para usar seu desconto digite o código abaixo no carrinho de compras na sua próxima compra.</b></div><div'>Código do desconto: <b>" + codigoDoCupom + "</b></div><div style='font-size: 11px; padding-top:35px'><br />Anteciosamente,<br /><b>Equipe " + ConfigurationManager.AppSettings["nomeDoSite"].ToString() + "</b></div><div style='font-size: 11px; padding-top:120px; padding-right:50px' align='center'><br><br><br><br><a style='font-family:Arial; font-size:11px; color:#000000' href=" + ConfigurationManager.AppSettings["caminhoVirtual"].ToString() + ">" + ConfigurationManager.AppSettings["caminhoVirtual"].ToString() + "</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a style='font-family:Arial; font-size:11px; color:#000000' href=mailto:" + ConfigurationManager.AppSettings["toAddress"].ToString() + ">" + ConfigurationManager.AppSettings["toAddress"].ToString() + "</a></div></td></tr></table>");

            /*MailMessage msg = new MailMessage(ConfigurationManager.AppSettings["toAddress"], clienteEmail);
            msg.Subject = "Você ganhou " + porcentagemDoDesconto + " % de desconto na sua próxima compra no site " + ConfigurationManager.AppSettings["nomeDoSite"] + "! ";
            msg.SubjectEncoding = Encoding.Default;
            msg.Body = conteudo.ToString();
            msg.BodyEncoding = Encoding.Default;
            msg.IsBodyHtml = true;

            var smtpClient = new SmtpClient(ConfigurationManager.AppSettings["emailSmtp"]);
			smtpClient.EnableSsl = true;
            smtpClient.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["emailLogin"], ConfigurationManager.AppSettings["emailSenha"]);
            smtpClient.Send(msg);*/


            return rnEmails.EnviaEmail("", clienteEmail, "", "", "", conteudo.ToString(), "Você ganhou " + porcentagemDoDesconto + " % de desconto na sua próxima compra no site " + ConfigurationManager.AppSettings["nomeDoSite"] + "! ");
        }
        catch (System.Net.Mail.SmtpException)
        {
            throw;
        }
        catch (Exception)
        {
            throw;
        }
        return true;
    }

    public static bool enviaInterecao(string interacao, string clienteEmail)
    {
        try
        {
            StringBuilder conteudo = new StringBuilder();

            conteudo.Append(interacao);

            /* MailMessage msg = new MailMessage(ConfigurationManager.AppSettings["toAddress"], clienteEmail);
             msg.Subject = "Atendimento " + ConfigurationManager.AppSettings["nomeDoSite"] + "";
             msg.SubjectEncoding = Encoding.Default;
             msg.Body = conteudo.ToString();
             msg.BodyEncoding = Encoding.Default;
             msg.IsBodyHtml = true;

             var smtpClient = new SmtpClient(ConfigurationManager.AppSettings["emailSmtp"]);
             smtpClient.EnableSsl = true;
             smtpClient.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["emailLogin"], ConfigurationManager.AppSettings["emailSenha"]);
             smtpClient.Send(msg);*/

            return rnEmails.EnviaEmail("", clienteEmail, "", "", "", conteudo.ToString(), "Atendimento " + ConfigurationManager.AppSettings["nomeDoSite"] + "");

        }
        catch (System.Net.Mail.SmtpException)
        {
            throw;
        }
        catch (Exception)
        {
            throw;
        }
        return true;
    }

    public static bool enviaEmailPagamentoConfirmado(int pedidoId)
    {
        string corpo = carregarTemplateArquivo(System.Configuration.ConfigurationManager.AppSettings["caminhoFisico"] + "\\templateemails\\pagamentoconfirmado\\" + "pagamento.html");
        string templateProdutos = carregarTemplateArquivo(System.Configuration.ConfigurationManager.AppSettings["caminhoFisico"] + "\\templateemails\\pedidos\\" + "produtos.html");
        string templateparcelas = carregarTemplateArquivo(System.Configuration.ConfigurationManager.AppSettings["caminhoFisico"] + "\\templateemails\\pedidos\\" + "parcelas.html");
        string templateboleto = carregarTemplateArquivo(System.Configuration.ConfigurationManager.AppSettings["caminhoFisico"] + "\\templateemails\\pedidos\\" + "boleto.html");

        var data = new dbCommerceDataContext();
        int idPedido = Convert.ToInt32(pedidoId);
        var pedido = (from c in data.tbPedidos where c.pedidoId == idPedido select c).First();
        var cliente = (from c in data.tbClientes where c.clienteId == pedido.clienteId select c).FirstOrDefault();
        var produtos = (from c in data.tbItensPedidos where c.pedidoId == idPedido select c).ToList();

        string listaProdutos = "";
        foreach (var produto in produtos)
        {
            if ((produto.cancelado ?? false) == false)
            {
                string linhaProduto = templateProdutos;
                linhaProduto = linhaProduto.Replace("{{imagemproduto}}", System.Configuration.ConfigurationManager.AppSettings["caminhoCDN"] + "fotos/" + produto.produtoId + "/pequena_" + produto.tbProduto.fotoDestaque + ".jpg");
                linhaProduto = linhaProduto.Replace("{{produtonome}}", produto.tbProduto.produtoNome);
                linhaProduto = linhaProduto.Replace("{{quantidadeproduto}}", produto.itemQuantidade.ToString());
                linhaProduto = linhaProduto.Replace("{{valorproduto}}", produto.itemValor.ToString("C"));
                listaProdutos += linhaProduto;
            }
        }
        corpo = corpo.Replace("{{numeropedido}}", rnFuncoes.retornaIdCliente(Convert.ToInt32(pedidoId)).ToString());
        corpo = corpo.Replace("{{nomecliente}}", cliente.clienteNome);
        corpo = corpo.Replace("{{produtos}}", listaProdutos);




        if (pedido.tipoDePagamentoId == 11)
        {
            var cobrancas =
                (from c in data.tbPedidoPagamentos
                 where c.pedidoId == pedidoId && c.cancelado == false
                 orderby c.dataVencimento
                 select c).ToList();


            corpo = corpo.Replace("{{prazo}}", cobrancas.OrderByDescending(x => x.dataVencimento)
                .First()
                .dataVencimento.AddDays(3)
                .AddDays(Convert.ToInt32(3))
                .ToShortDateString());
            corpo = corpo.Replace("{{processamento}}", "3");
            corpo = corpo.Replace("{{entrega}}", pedido.prazoDeEntrega.ToString());
        }
        else
        {
            int prazoFinal = rnFuncoes.retornaPrazoDiasUteis(Convert.ToInt32(pedido.prazoDeEntrega), 0, Convert.ToDateTime(pedido.prazoFinalPedido));
            var dataEntrega = pedido.prazoFinalPedido.Value.AddDays(prazoFinal).ToShortDateString();
            corpo = corpo.Replace("{{prazo}}", dataEntrega);
            corpo = corpo.Replace("{{processamento}}", ((pedido.prazoMaximoPedidos ?? 0) + (pedido.prazoInicioFabricao ?? 0)).ToString());
            corpo = corpo.Replace("{{entrega}}", pedido.prazoDeEntrega.ToString());
        }


        string urlBoleto = "";
        int tipoDePagamentoId = Convert.ToInt32(pedido.tipoDePagamentoId);
        int condDePagamentoId = Convert.ToInt32(pedido.condDePagamentoId);
        string imgCondicao = pedido.tbCondicoesDePagamento.imagemMini;
        string imgCondicao2 = "";
        string condicaoNome = pedido.tbCondicoesDePagamento.condicaoNome;
        string condicaoNome2 = "";

        string clienteCPFCNPJ = cliente.clienteCPFCNPJ;
        string clienteCidade = cliente.clienteCidade;
        string clienteRua = cliente.clienteRua;
        string clienteBairro = cliente.clienteBairro;
        string clienteCep = cliente.clienteCep;
        string clienteEstado = cliente.clienteEstado;


        if (tipoDePagamentoId == 8)
        {
            string templateformas =
                carregarTemplateArquivo(System.Configuration.ConfigurationManager.AppSettings["caminhoFisico"] +
                                        "\\templateemails\\pedidos\\" + "duasformas.html");
            int condDePagamentoId1 = Convert.ToInt32(pedido.condDePagamentoId1);
            int condDePagamentoId2 = Convert.ToInt32(pedido.condDePagamentoId2);
            var condicao1 =
                (from c in data.tbCondicoesDePagamentos where c.condicaoId == condDePagamentoId1 select c).FirstOrDefault
                    ();
            var condicao2 =
                (from c in data.tbCondicoesDePagamentos where c.condicaoId == condDePagamentoId2 select c).FirstOrDefault
                    ();


            templateformas = templateformas.Replace("{{imagempagamento1}}", condicao1.imagemMini);
            templateformas = templateformas.Replace("{{imagempagamento2}}", condicao2.imagemMini);
            templateformas = templateformas.Replace("{{nomepagamento1}}", condicao1.condicaoNome);
            templateformas = templateformas.Replace("{{nomepagamento2}}", condicao2.condicaoNome);
            corpo = corpo.Replace("{{formapagamento}}", templateformas);

            string parcelas2Formas = templateparcelas;
            parcelas2Formas = parcelas2Formas.Replace("{{quantidadeparcelas}}", pedido.parcelasPagamento1.ToString());
            parcelas2Formas = parcelas2Formas.Replace("{{valorparcelas}}", Convert.ToDecimal(pedido.valorParcelaPagamento1).ToString("C"));
            parcelas2Formas += templateparcelas;
            parcelas2Formas = parcelas2Formas.Replace("{{quantidadeparcelas}}", pedido.parcelasPagamento2.ToString());
            parcelas2Formas = parcelas2Formas.Replace("{{valorparcelas}}",
                Convert.ToDecimal(pedido.valorParcelaPagamento2).ToString("C"));
            corpo = corpo.Replace("{{parcelas}}", parcelas2Formas);

            if (pedido.tipoDePagamentoId1 == 1 | pedido.tipoDePagamentoId2 == 1)
            {
                var valorBoleto = Convert.ToInt32(pedido.tipoDePagamentoId1) == 1
                    ? Convert.ToDecimal(pedido.valorPagamento1)
                    : Convert.ToDecimal(pedido.valorPagamento2);
                urlBoleto = ConfigurationManager.AppSettings["caminhoVirtual"] + "boleto.aspx?sacado=" + cliente.clienteNome.Replace(" ", "+") +
                            "&CPFCNPJ=" + clienteCPFCNPJ + "&endereco=" + clienteRua.Replace(" ", "+") + "&bairro=" +
                            clienteBairro.Replace(" ", "+") + "&cidade=" + clienteCidade.Replace(" ", "+") + "&cep=" +
                            clienteCep + "&uf=" + clienteEstado + "&numeroDocumento=" + pedidoId + "&nossoNumero=" +
                            pedidoId + "&valor=" + valorBoleto + "";
                corpo = corpo.Replace("{{linkboleto}}", templateboleto.Replace("{{linkboleto}}", urlBoleto));
            }
            else
            {
                corpo = corpo.Replace("{{linkboleto}}", "");
            }
        }
        else
        {
            string templateformas =
                carregarTemplateArquivo(System.Configuration.ConfigurationManager.AppSettings["caminhoFisico"] +
                                        "\\templateemails\\pedidos\\" + "umaforma.html");

            templateformas = templateformas.Replace("{{imagempagamento1}}", pedido.tbCondicoesDePagamento.imagemMini);
            templateformas = templateformas.Replace("{{nomepagamento1}}", pedido.tbCondicoesDePagamento.condicaoNome);
            corpo = corpo.Replace("{{formapagamento}}", templateformas);


            var pagamentosPedido = (from c in data.tbPedidoPagamentos
                                    where
                                        c.pedidoId == pedido.pedidoId && c.cancelado == false && c.pago == true &&
                                        c.pagamentoNaoAutorizado == false
                                    select c).ToList();

            //if (pagamentosPedido.Count > 1)
            //{
            string parcelas2Formas = "";
            foreach (var pagamentoPedido in pagamentosPedido)
            {
                var tempParcela = templateparcelas;
                tempParcela = tempParcela.Replace("{{quantidadeparcelas}}", pagamentoPedido.parcelas.ToString());
                tempParcela = tempParcela.Replace("{{valorparcelas}}", Convert.ToDecimal(pagamentoPedido.valorParcela).ToString("C"));
                parcelas2Formas += tempParcela;
            }
            corpo = corpo.Replace("{{parcelas}}", parcelas2Formas);
            //}
            //else
            //{
            //    string parcelasUmaForma = templateparcelas;
            //    parcelasUmaForma = parcelasUmaForma.Replace("{{quantidadeparcelas}}", pedido.numeroDeParcelas.ToString());
            //    parcelasUmaForma = parcelasUmaForma.Replace("{{valorparcelas}}", Convert.ToDecimal(pedido.valorDaParcela).ToString("C"));
            //    corpo = corpo.Replace("{{parcelas}}", parcelasUmaForma);
            //}

            if (tipoDePagamentoId == 1)
            {
                urlBoleto = ConfigurationManager.AppSettings["caminhoVirtual"] + "boleto.aspx?sacado=" + cliente.clienteNome.Replace(" ", "+") + "&CPFCNPJ=" + clienteCPFCNPJ + "&endereco=" + clienteRua.Replace(" ", "+") + "&bairro=" + clienteBairro.Replace(" ", "+") + "&cidade=" + clienteCidade.Replace(" ", "+") + "&cep=" + clienteCep + "&uf=" + clienteEstado + "&numeroDocumento=" + pedidoId + "&nossoNumero=" + pedidoId + "&valor=" + pedido.valorCobrado + "";
                corpo = corpo.Replace("{{linkboleto}}", templateboleto.Replace("{{linkboleto}}", urlBoleto));
            }
            else
            {
                corpo = corpo.Replace("{{linkboleto}}", "");
            }
        }

        corpo = corpo.Replace("{{subtotal}}", Convert.ToDecimal(pedido.valorTotalGeral).ToString("C"));
        corpo = corpo.Replace("{{frete}}", Convert.ToDecimal(pedido.valorDoFrete).ToString("C"));
        corpo = corpo.Replace("{{desconto}}", Convert.ToDecimal(pedido.valorDoDescontoDoPagamento).ToString("C"));
        corpo = corpo.Replace("{{total}}", Convert.ToDecimal(pedido.valorCobrado).ToString("C"));
        corpo = corpo.Replace("{{enderecocliente}}", pedido.endRua + ", " + pedido.endNumero + " " + pedido.endComplemento + "<br>" + pedido.endBairro + "<br>Cep: " + pedido.endCep + " - " + pedido.endCidade + " - " + pedido.endEstado);

        return EnviaEmail("", cliente.clienteEmail, "", "", "", corpo, "Pagamento Confirmado");

    }
    public static bool enviaEmailPagamentoConfirmadoPlano(int pedidoId)
    {
        string corpo = carregarTemplateArquivo(System.Configuration.ConfigurationManager.AppSettings["caminhoFisico"] + "\\templateemails\\pagamentoconfirmadoplano\\" + "pagamento.html");
        string templateProdutos = carregarTemplateArquivo(System.Configuration.ConfigurationManager.AppSettings["caminhoFisico"] + "\\templateemails\\pedidos\\" + "produtos.html");
        string templateparcelas = carregarTemplateArquivo(System.Configuration.ConfigurationManager.AppSettings["caminhoFisico"] + "\\templateemails\\pedidos\\" + "parcelas.html");
        string templateboleto = carregarTemplateArquivo(System.Configuration.ConfigurationManager.AppSettings["caminhoFisico"] + "\\templateemails\\pedidos\\" + "boleto.html");
        string templateboletoplano = carregarTemplateArquivo(System.Configuration.ConfigurationManager.AppSettings["caminhoFisico"] + "\\templateemails\\pagamentoconfirmadoplano\\" + "boletoplano.html");
        string templateplanoparcelas = carregarTemplateArquivo(System.Configuration.ConfigurationManager.AppSettings["caminhoFisico"] + "\\templateemails\\pagamentoconfirmadoplano\\" + "planoparcelas.html");
        string templateplanoparcela = carregarTemplateArquivo(System.Configuration.ConfigurationManager.AppSettings["caminhoFisico"] + "\\templateemails\\pagamentoconfirmadoplano\\" + "planoparcela.html");
        string templateplanoparcelapaga = carregarTemplateArquivo(System.Configuration.ConfigurationManager.AppSettings["caminhoFisico"] + "\\templateemails\\pagamentoconfirmadoplano\\" + "planoparcelapaga.html");

        var data = new dbCommerceDataContext();
        int idPedido = Convert.ToInt32(pedidoId);
        var pedido = (from c in data.tbPedidos where c.pedidoId == idPedido select c).First();
        var cliente = (from c in data.tbClientes where c.clienteId == pedido.clienteId select c).FirstOrDefault();
        var produtos = (from c in data.tbItensPedidos where c.pedidoId == idPedido select c).ToList();

        string listaProdutos = "";
        foreach (var produto in produtos)
        {
            if ((produto.cancelado ?? false) == false)
            {
                string linhaProduto = templateProdutos;
                linhaProduto = linhaProduto.Replace("{{imagemproduto}}", System.Configuration.ConfigurationManager.AppSettings["caminhoCDN"] + "fotos/" + produto.produtoId + "/pequena_" + produto.tbProduto.fotoDestaque + ".jpg");
                linhaProduto = linhaProduto.Replace("{{produtonome}}", produto.tbProduto.produtoNome);
                linhaProduto = linhaProduto.Replace("{{quantidadeproduto}}", produto.itemQuantidade.ToString());
                linhaProduto = linhaProduto.Replace("{{valorproduto}}", produto.itemValor.ToString("C"));
                listaProdutos += linhaProduto;
            }
        }
        corpo = corpo.Replace("{{numeropedido}}", rnFuncoes.retornaIdCliente(Convert.ToInt32(pedidoId)).ToString());
        corpo = corpo.Replace("{{nomecliente}}", cliente.clienteNome);
        corpo = corpo.Replace("{{produtos}}", listaProdutos);



        var cobrancas =
                        (from c in data.tbPedidoPagamentos
                         where c.pedidoId == pedidoId && c.cancelado == false
                         orderby c.dataVencimento
                         select c).ToList();


        corpo = corpo.Replace("{{prazo}}", cobrancas.OrderByDescending(x => x.dataVencimento)
                .First()
                .dataVencimento.AddDays(3)
                .AddDays(Convert.ToInt32(3))
                .ToShortDateString());
        corpo = corpo.Replace("{{processamento}}", "3");
        corpo = corpo.Replace("{{entrega}}", pedido.prazoDeEntrega.ToString());


        string urlBoleto = "";
        int tipoDePagamentoId = Convert.ToInt32(pedido.tipoDePagamentoId);
        int condDePagamentoId = Convert.ToInt32(pedido.condDePagamentoId);
        string imgCondicao = pedido.tbCondicoesDePagamento.imagemMini;
        string imgCondicao2 = "";
        string condicaoNome = pedido.tbCondicoesDePagamento.condicaoNome;
        string condicaoNome2 = "";

        string clienteCPFCNPJ = cliente.clienteCPFCNPJ;
        string clienteCidade = cliente.clienteCidade;
        string clienteRua = cliente.clienteRua;
        string clienteBairro = cliente.clienteBairro;
        string clienteCep = cliente.clienteCep;
        string clienteEstado = cliente.clienteEstado;


        if (tipoDePagamentoId == 8)
        {
            string templateformas =
                carregarTemplateArquivo(System.Configuration.ConfigurationManager.AppSettings["caminhoFisico"] +
                                        "\\templateemails\\pedidos\\" + "duasformas.html");
            int condDePagamentoId1 = Convert.ToInt32(pedido.condDePagamentoId1);
            int condDePagamentoId2 = Convert.ToInt32(pedido.condDePagamentoId2);
            var condicao1 =
                (from c in data.tbCondicoesDePagamentos where c.condicaoId == condDePagamentoId1 select c).FirstOrDefault
                    ();
            var condicao2 =
                (from c in data.tbCondicoesDePagamentos where c.condicaoId == condDePagamentoId2 select c).FirstOrDefault
                    ();


            templateformas = templateformas.Replace("{{imagempagamento1}}", condicao1.imagemMini);
            templateformas = templateformas.Replace("{{imagempagamento2}}", condicao2.imagemMini);
            templateformas = templateformas.Replace("{{nomepagamento1}}", condicao1.condicaoNome);
            templateformas = templateformas.Replace("{{nomepagamento2}}", condicao2.condicaoNome);
            corpo = corpo.Replace("{{formapagamento}}", templateformas);

            string parcelas2Formas = templateparcelas;
            parcelas2Formas = parcelas2Formas.Replace("{{quantidadeparcelas}}", pedido.parcelasPagamento1.ToString());
            parcelas2Formas = parcelas2Formas.Replace("{{valorparcelas}}", Convert.ToDecimal(pedido.valorParcelaPagamento1).ToString("C"));
            parcelas2Formas += templateparcelas;
            parcelas2Formas = parcelas2Formas.Replace("{{quantidadeparcelas}}", pedido.parcelasPagamento2.ToString());
            parcelas2Formas = parcelas2Formas.Replace("{{valorparcelas}}",
                Convert.ToDecimal(pedido.valorParcelaPagamento2).ToString("C"));
            corpo = corpo.Replace("{{parcelas}}", parcelas2Formas);

            var pagamentosBoleto =
            (from c in data.tbPedidoPagamentos
             where c.pedidoId == idPedido && c.idOperadorPagamento == 1
             select c).FirstOrDefault();
            if (pagamentosBoleto != null)
            {
                urlBoleto = ConfigurationManager.AppSettings["caminhoVirtual"] + "boleto.aspx?cobranca=" + pagamentosBoleto.numeroCobranca + "&pedido=" + rnFuncoes.retornaIdCliente(idPedido);
                corpo = corpo.Replace("{{linkboleto}}", templateboleto.Replace("{{linkboleto}}", urlBoleto));
                corpo = corpo.Replace("{{styleplano}}", "display: none;");
            }
            else
            {
                corpo = corpo.Replace("{{linkboleto}}", "");
                corpo = corpo.Replace("{{styleplano}}", "display: none;");
            }
        }
        else
        {
            string templateformas =
                carregarTemplateArquivo(System.Configuration.ConfigurationManager.AppSettings["caminhoFisico"] +
                                        "\\templateemails\\pedidos\\" + "umaforma.html");

            templateformas = templateformas.Replace("{{imagempagamento1}}", pedido.tbCondicoesDePagamento.imagemMini);
            templateformas = templateformas.Replace("{{nomepagamento1}}", pedido.tbCondicoesDePagamento.condicaoNome);
            corpo = corpo.Replace("{{formapagamento}}", templateformas);

            string parcelasUmaForma = templateparcelas;
            parcelasUmaForma = parcelasUmaForma.Replace("{{quantidadeparcelas}}", pedido.numeroDeParcelas.ToString());
            parcelasUmaForma = parcelasUmaForma.Replace("{{valorparcelas}}", Convert.ToDecimal(pedido.valorDaParcela).ToString("C"));
            corpo = corpo.Replace("{{parcelas}}", parcelasUmaForma);

            var pagamentosBoleto =
            (from c in data.tbPedidoPagamentos
             where c.pedidoId == idPedido && c.idOperadorPagamento == 1
             select c).FirstOrDefault();

            if (pagamentosBoleto != null && pedido.tipoDePagamentoId != 11)
            {
                urlBoleto = ConfigurationManager.AppSettings["caminhoVirtual"] + "boleto.aspx?cobranca=" + pagamentosBoleto.numeroCobranca + "&pedido=" + rnFuncoes.retornaIdCliente(idPedido);
                corpo = corpo.Replace("{{linkboleto}}", templateboleto.Replace("{{linkboleto}}", urlBoleto));
                corpo = corpo.Replace("{{styleplano}}", "display: none;");
            }
            else if (pagamentosBoleto != null && pedido.tipoDePagamentoId == 11)
            {
                urlBoleto = ConfigurationManager.AppSettings["caminhoVirtual"] + "boleto.aspx?pedido=" + rnFuncoes.retornaIdCliente(idPedido);
                corpo = corpo.Replace("{{linkboleto}}", templateboletoplano.Replace("{{linkboleto}}", urlBoleto));

                string pagamentosplano = "";
                var pagamentosPlanoBoleto =
                (from c in data.tbPedidoPagamentos
                 where c.pedidoId == idPedido && c.idOperadorPagamento == 1
                 select c).ToList();
                foreach (var pagamentoPlano in pagamentosPlanoBoleto)
                {
                    if (pagamentoPlano.pago)
                    {
                        var linhaPagamento = templateplanoparcelapaga;
                        linhaPagamento = linhaPagamento.Replace("{{datavencimento}}", pagamentoPlano.dataVencimento.ToShortDateString());
                        pagamentosplano += linhaPagamento;
                    }
                    else
                    {
                        string urlBoletoPlano = ConfigurationManager.AppSettings["caminhoVirtual"] +
                                                "boleto.aspx?pedido=" + rnFuncoes.retornaIdCliente(idPedido) +
                                                "&cobranca=" + pagamentoPlano.numeroCobranca;
                        var linhaPagamento = templateplanoparcela;
                        linhaPagamento = linhaPagamento.Replace("{{linkboleto}}", urlBoletoPlano);
                        linhaPagamento = linhaPagamento.Replace("{{datavencimento}}",
                            pagamentoPlano.dataVencimento.ToShortDateString());
                        pagamentosplano += linhaPagamento;
                    }
                }
                templateplanoparcelas = templateplanoparcelas.Replace("{{planoparcela}}", pagamentosplano);
                corpo = corpo.Replace("{{planoparcelas}}", templateplanoparcelas);

            }
            else
            {
                corpo = corpo.Replace("{{linkboleto}}", "");
                corpo = corpo.Replace("{{styleplano}}", "display: none;");
            }
        }

        corpo = corpo.Replace("{{subtotal}}", Convert.ToDecimal(pedido.valorTotalGeral).ToString("C"));
        corpo = corpo.Replace("{{frete}}", Convert.ToDecimal(pedido.valorDoFrete).ToString("C"));
        corpo = corpo.Replace("{{desconto}}", Convert.ToDecimal(pedido.valorDoDescontoDoPagamento).ToString("C"));
        corpo = corpo.Replace("{{total}}", Convert.ToDecimal(pedido.valorCobrado).ToString("C"));
        corpo = corpo.Replace("{{enderecocliente}}", pedido.endRua + ", " + pedido.endNumero + " " + pedido.endComplemento + "<br>" + pedido.endBairro + "<br>Cep: " + pedido.endCep + " - " + pedido.endCidade + " - " + pedido.endEstado);

        return EnviaEmail("", cliente.clienteEmail, "", "", "", corpo, "Pagamento Confirmado");

    }


    public static bool enviaEmailPedidoProcessado(int pedidoId)
    {
        string corpo = carregarTemplateArquivo(System.Configuration.ConfigurationManager.AppSettings["caminhoFisico"] + "\\templateemails\\processado\\" + "processado.html");
        string templateProdutos = carregarTemplateArquivo(System.Configuration.ConfigurationManager.AppSettings["caminhoFisico"] + "\\templateemails\\pedidos\\" + "produtos.html");
        string templateparcelas = carregarTemplateArquivo(System.Configuration.ConfigurationManager.AppSettings["caminhoFisico"] + "\\templateemails\\pedidos\\" + "parcelas.html");
        string templateboleto = carregarTemplateArquivo(System.Configuration.ConfigurationManager.AppSettings["caminhoFisico"] + "\\templateemails\\pedidos\\" + "boleto.html");

        var data = new dbCommerceDataContext();
        int idPedido = Convert.ToInt32(pedidoId);
        var pedido = (from c in data.tbPedidos where c.pedidoId == idPedido select c).First();
        var cliente = (from c in data.tbClientes where c.clienteId == pedido.clienteId select c).FirstOrDefault();
        var produtos = (from c in data.tbItensPedidos where c.pedidoId == idPedido select c).ToList();

        string listaProdutos = "";
        foreach (var produto in produtos)
        {
            if ((produto.cancelado ?? false) == false)
            {
                string linhaProduto = templateProdutos;
                linhaProduto = linhaProduto.Replace("{{imagemproduto}}", System.Configuration.ConfigurationManager.AppSettings["caminhoCDN"] + "fotos/" + produto.produtoId + "/pequena_" + produto.tbProduto.fotoDestaque + ".jpg");
                linhaProduto = linhaProduto.Replace("{{produtonome}}", produto.tbProduto.produtoNome);
                linhaProduto = linhaProduto.Replace("{{quantidadeproduto}}", produto.itemQuantidade.ToString());
                linhaProduto = linhaProduto.Replace("{{valorproduto}}", produto.itemValor.ToString("C"));
                listaProdutos += linhaProduto;
            }
        }
        corpo = corpo.Replace("{{numeropedido}}", rnFuncoes.retornaIdCliente(Convert.ToInt32(pedidoId)).ToString());
        corpo = corpo.Replace("{{nomecliente}}", cliente.clienteNome);
        corpo = corpo.Replace("{{produtos}}", listaProdutos);


        if (pedido.tipoDePagamentoId == 11)
        {
            var cobrancas =
                (from c in data.tbPedidoPagamentos
                 where c.pedidoId == pedidoId && c.cancelado == false
                 orderby c.dataVencimento
                 select c).ToList();


            corpo = corpo.Replace("{{prazo}}", cobrancas.OrderByDescending(x => x.dataVencimento)
                .First()
                .dataVencimento.AddDays(3)
                .AddDays(Convert.ToInt32(3))
                .ToShortDateString());
            corpo = corpo.Replace("{{processamento}}", "3");
            corpo = corpo.Replace("{{entrega}}", pedido.prazoDeEntrega.ToString());
        }
        else
        {
            int prazoFinal = rnFuncoes.retornaPrazoDiasUteis(Convert.ToInt32(pedido.prazoDeEntrega), 0, (DateTime)pedido.prazoFinalPedido);
            var dataEntrega = pedido.prazoFinalPedido.Value.AddDays(prazoFinal).ToShortDateString();
            corpo = corpo.Replace("{{prazo}}", dataEntrega);
            corpo = corpo.Replace("{{processamento}}", ((pedido.prazoMaximoPedidos ?? 0) + (pedido.prazoInicioFabricao ?? 0)).ToString());
            corpo = corpo.Replace("{{entrega}}", pedido.prazoDeEntrega.ToString());
        }


        string urlBoleto = "";
        int tipoDePagamentoId = Convert.ToInt32(pedido.tipoDePagamentoId);
        int condDePagamentoId = Convert.ToInt32(pedido.condDePagamentoId);
        string imgCondicao = pedido.tbCondicoesDePagamento.imagemMini;
        string imgCondicao2 = "";
        string condicaoNome = pedido.tbCondicoesDePagamento.condicaoNome;
        string condicaoNome2 = "";

        string clienteCPFCNPJ = cliente.clienteCPFCNPJ;
        string clienteCidade = cliente.clienteCidade;
        string clienteRua = cliente.clienteRua;
        string clienteBairro = cliente.clienteBairro;
        string clienteCep = cliente.clienteCep;
        string clienteEstado = cliente.clienteEstado;


        if (tipoDePagamentoId == 8)
        {
            string templateformas =
                carregarTemplateArquivo(System.Configuration.ConfigurationManager.AppSettings["caminhoFisico"] +
                                        "\\templateemails\\pedidos\\" + "duasformas.html");
            int condDePagamentoId1 = Convert.ToInt32(pedido.condDePagamentoId1);
            int condDePagamentoId2 = Convert.ToInt32(pedido.condDePagamentoId2);
            var condicao1 =
                (from c in data.tbCondicoesDePagamentos where c.condicaoId == condDePagamentoId1 select c).FirstOrDefault
                    ();
            var condicao2 =
                (from c in data.tbCondicoesDePagamentos where c.condicaoId == condDePagamentoId2 select c).FirstOrDefault
                    ();


            templateformas = templateformas.Replace("{{imagempagamento1}}", condicao1.imagemMini);
            templateformas = templateformas.Replace("{{imagempagamento2}}", condicao2.imagemMini);
            templateformas = templateformas.Replace("{{nomepagamento1}}", condicao1.condicaoNome);
            templateformas = templateformas.Replace("{{nomepagamento2}}", condicao2.condicaoNome);
            corpo = corpo.Replace("{{formapagamento}}", templateformas);

            string parcelas2Formas = templateparcelas;
            parcelas2Formas = parcelas2Formas.Replace("{{quantidadeparcelas}}", pedido.parcelasPagamento1.ToString());
            parcelas2Formas = parcelas2Formas.Replace("{{valorparcelas}}", Convert.ToDecimal(pedido.valorParcelaPagamento1).ToString("C"));
            parcelas2Formas += templateparcelas;
            parcelas2Formas = parcelas2Formas.Replace("{{quantidadeparcelas}}", pedido.parcelasPagamento2.ToString());
            parcelas2Formas = parcelas2Formas.Replace("{{valorparcelas}}",
                Convert.ToDecimal(pedido.valorParcelaPagamento2).ToString("C"));
            corpo = corpo.Replace("{{parcelas}}", parcelas2Formas);

            if (pedido.tipoDePagamentoId1 == 1 | pedido.tipoDePagamentoId2 == 1)
            {
                var valorBoleto = Convert.ToInt32(pedido.tipoDePagamentoId1) == 1
                    ? Convert.ToDecimal(pedido.valorPagamento1)
                    : Convert.ToDecimal(pedido.valorPagamento2);
                urlBoleto = ConfigurationManager.AppSettings["caminhoVirtual"] + "boleto.aspx?sacado=" + cliente.clienteNome.Replace(" ", "+") +
                            "&CPFCNPJ=" + clienteCPFCNPJ + "&endereco=" + clienteRua.Replace(" ", "+") + "&bairro=" +
                            clienteBairro.Replace(" ", "+") + "&cidade=" + clienteCidade.Replace(" ", "+") + "&cep=" +
                            clienteCep + "&uf=" + clienteEstado + "&numeroDocumento=" + pedidoId + "&nossoNumero=" +
                            pedidoId + "&valor=" + valorBoleto + "";
                corpo = corpo.Replace("{{linkboleto}}", templateboleto.Replace("{{linkboleto}}", urlBoleto));
            }
            else
            {
                corpo = corpo.Replace("{{linkboleto}}", "");
            }
        }
        else
        {
            string templateformas =
                carregarTemplateArquivo(System.Configuration.ConfigurationManager.AppSettings["caminhoFisico"] +
                                        "\\templateemails\\pedidos\\" + "umaforma.html");

            templateformas = templateformas.Replace("{{imagempagamento1}}", pedido.tbCondicoesDePagamento.imagemMini);
            templateformas = templateformas.Replace("{{nomepagamento1}}", pedido.tbCondicoesDePagamento.condicaoNome);
            corpo = corpo.Replace("{{formapagamento}}", templateformas);

            string parcelasUmaForma = templateparcelas;
            parcelasUmaForma = parcelasUmaForma.Replace("{{quantidadeparcelas}}", pedido.numeroDeParcelas.ToString());
            parcelasUmaForma = parcelasUmaForma.Replace("{{valorparcelas}}", Convert.ToDecimal(pedido.valorDaParcela).ToString("C"));
            corpo = corpo.Replace("{{parcelas}}", parcelasUmaForma);

            if (tipoDePagamentoId == 1)
            {
                urlBoleto = ConfigurationManager.AppSettings["caminhoVirtual"] + "boleto.aspx?sacado=" + cliente.clienteNome.Replace(" ", "+") + "&CPFCNPJ=" + clienteCPFCNPJ + "&endereco=" + clienteRua.Replace(" ", "+") + "&bairro=" + clienteBairro.Replace(" ", "+") + "&cidade=" + clienteCidade.Replace(" ", "+") + "&cep=" + clienteCep + "&uf=" + clienteEstado + "&numeroDocumento=" + pedidoId + "&nossoNumero=" + pedidoId + "&valor=" + pedido.valorCobrado + "";
                corpo = corpo.Replace("{{linkboleto}}", templateboleto.Replace("{{linkboleto}}", urlBoleto));
            }
            else
            {
                corpo = corpo.Replace("{{linkboleto}}", "");
            }
        }

        corpo = corpo.Replace("{{subtotal}}", Convert.ToDecimal(pedido.valorTotalGeral).ToString("C"));
        corpo = corpo.Replace("{{frete}}", Convert.ToDecimal(pedido.valorDoFrete).ToString("C"));
        corpo = corpo.Replace("{{desconto}}", Convert.ToDecimal(pedido.valorDoDescontoDoPagamento).ToString("C"));
        corpo = corpo.Replace("{{total}}", Convert.ToDecimal(pedido.valorCobrado).ToString("C"));
        corpo = corpo.Replace("{{enderecocliente}}", pedido.endRua + ", " + pedido.endNumero + " " + pedido.endComplemento + "<br>" + pedido.endBairro + "<br>Cep: " + pedido.endCep + " - " + pedido.endCidade + " - " + pedido.endEstado);

        return EnviaEmail("", cliente.clienteEmail, "", "", "", corpo, "Pedido Processado");

    }

    public static bool enviaEmailPagamentoRecusado(int pedidoId)
    {
        string corpo = carregarTemplateArquivo(System.Configuration.ConfigurationManager.AppSettings["caminhoFisico"] + "\\templateemails\\naoprocessado\\" + "naoprocessado.html");

        var data = new dbCommerceDataContext();
        int idPedido = Convert.ToInt32(pedidoId);
        var pedido = (from c in data.tbPedidos where c.pedidoId == idPedido select c).First();
        var cliente = (from c in data.tbClientes where c.clienteId == pedido.clienteId select c).FirstOrDefault();

        corpo = corpo.Replace("{{nomecliente}}", cliente.clienteNome);

        return EnviaEmail("", cliente.clienteEmail, "", "", "", corpo, "Pagamento Não Processado");

    }

    public static bool enviaEmailPedidoCancelado(int pedidoId)
    {
        string corpo = carregarTemplateArquivo(System.Configuration.ConfigurationManager.AppSettings["caminhoFisico"] + "\\templateemails\\cancelado\\" + "cancelado.html");

        var data = new dbCommerceDataContext();
        int idPedido = Convert.ToInt32(pedidoId);
        var pedido = (from c in data.tbPedidos where c.pedidoId == idPedido select c).First();
        var cliente = (from c in data.tbClientes where c.clienteId == pedido.clienteId select c).FirstOrDefault();

        corpo = corpo.Replace("{{nomecliente}}", cliente.clienteNome);
        corpo = corpo.Replace("{{numeropedido}}", rnFuncoes.retornaIdCliente(Convert.ToInt32(pedidoId)).ToString());

        return EnviaEmail("", cliente.clienteEmail, "", "", "", corpo, "Pedido Cancelado");

    }

    //public static bool enviaSegundaViaDoBoleto(string clienteNome, string pedidoId, string linkDoBoleto, string clienteEmail)
    //{
    //    try
    //    {
    //        StringBuilder conteudo = new StringBuilder();

    //        conteudo.Append("<table cellpadding='0' cellspacing='0' style='width: 650px; height:601px; background-image: url(" + ConfigurationManager.AppSettings["caminhoVirtual"] + "images/bkEmailAlteracaoDoPedido.jpg); background-repeat: no-repeat;'><tr><td valign='top' style='font-family:Arial; color: #000000; padding-left:50px; '><div style='font-size: 14px; padding-top:127px'><b>SEGUNDA VIA DO BOLETO</b></div><div style='font-size: 11px; padding-top:25px'>Prezado (a) Sr.(a)<br /><b>" + clienteNome + ",</b><br />Estamos lhe enviando a segunda via do boleto referente ao pedido <b>nº " + rnFuncoes.retornaIdCliente(Convert.ToInt32(pedidoId)) + "</b></div><div style='font-size: 11px; padding-top:10px'><b>Para imprimir seu boleto, clique no link abaixo</b></div><div'><b><a  style='font-size: 14px; padding-top:21px; color: #000000;' href=" + linkDoBoleto.Trim() + ">CLIQUE AQUI PARA IMPRIMIR SEU BOLETO</a></b></div><br><br><br><br><div style='font-size: 11px; padding-top:35px'><br />Anteciosamente,<br /><b>Equipe " + ConfigurationManager.AppSettings["nomeDoSite"].ToString() + "</b></div><div style='font-size: 11px; padding-top:120px; padding-right:50px' align='center'><a style='font-family:Arial; font-size:11px; color:#000000' href=" + ConfigurationManager.AppSettings["caminhoVirtual"].ToString() + ">" + ConfigurationManager.AppSettings["caminhoVirtual"].ToString() + "</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a style='font-family:Arial; font-size:11px; color:#000000' href=mailto:" + ConfigurationManager.AppSettings["toAddress"].ToString() + ">" + ConfigurationManager.AppSettings["toAddress"].ToString() + "</a></div></td></tr></table>");

    //        return rnEmails.EnviaEmail("", clienteEmail, "", "", "", conteudo.ToString(), "Segunda via do boleto " + ConfigurationManager.AppSettings["nomeDoSite"] + "");
    //    }
    //    catch (System.Net.Mail.SmtpException)
    //    {
    //        throw;
    //    }
    //    catch (Exception)
    //    {
    //        throw;
    //    }
    //    return true;
    //}

    public static bool enviaSegundaViaDoBoleto(string clienteNome, string pedidoId, string linkDoBoleto, string clienteEmail)
    {
        string corpo = carregarTemplateArquivo(System.Configuration.ConfigurationManager.AppSettings["caminhoFisico"] + "\\templateemails\\segundavia\\" + "segundavia.html");
        corpo = corpo.Replace("{{nomecliente}}", clienteNome);
        corpo = corpo.Replace("{{numeropedido}}", rnFuncoes.retornaIdCliente(Convert.ToInt32(pedidoId)).ToString());
        corpo = corpo.Replace("{{linkboleto}}", linkDoBoleto);
        return EnviaEmail("", clienteEmail, "", "", "", corpo, "Segunda via de Boleto");

    }

    public static bool enviaCodigoDoTrakingCorreios(string clienteNome, string pedidoId, string clienteEmail)
    {
        try
        {
            int idPedido = Convert.ToInt32(pedidoId);
            var pedidosDc = new dbCommerceDataContext();
            var volumesPedido = (from c in pedidosDc.tbPedidoPacotes where c.idPedido == idPedido select c);
            var pedido = (from c in pedidosDc.tbPedidos where c.pedidoId == idPedido select c).First();
            var pacotes = (from c in pedidosDc.tbPedidoPacotes where c.idPedidoPacote == idPedido select c).FirstOrDefault();
            int volumeAtual = 0;
            string codigoTracking = "";
            string urlTracking = "http://websro.correios.com.br/sro_bin/txect01$.Inexistente?P_LINGUA=001&P_TIPO=002&P_COD_LIS=";

            var prazoData = DateTime.Now;

            if (pacotes.dataDeCarregamento != null)
            {
                prazoData = (DateTime)pacotes.dataDeCarregamento;
            }
            else if (pacotes.prazoFinalEntrega != null)
            {
                prazoData = (DateTime)pacotes.prazoFinalEntrega;
            }

            string corpo = carregarTemplateArquivo(System.Configuration.ConfigurationManager.AppSettings["caminhoFisico"] + "\\templateemails\\enviado\\" + "enviado.html");
            string templateRastreio = carregarTemplateArquivo(System.Configuration.ConfigurationManager.AppSettings["caminhoFisico"] + "\\templateemails\\enviado\\" + "codigorastreio.htm");

            var data = new dbCommerceDataContext();
            var cliente = (from c in data.tbClientes where c.clienteId == pedido.clienteId select c).FirstOrDefault();


            int prazoFinal = rnFuncoes.retornaPrazoDiasUteis(Convert.ToInt32(pedido.prazoDeEntrega), 0);
            //var dataEntrega = DateTime.Now.AddDays(prazoFinal).ToShortDateString();
            var dataEntrega = prazoData.AddDays(prazoFinal).ToShortDateString();
            corpo = corpo.Replace("{{prazo}}", dataEntrega);
            corpo = corpo.Replace("{{processamento}}", ((pedido.prazoMaximoPedidos ?? 0) + (pedido.prazoInicioFabricao ?? 0)).ToString());
            corpo = corpo.Replace("{{entrega}}", pedido.prazoDeEntrega.ToString());
            corpo = corpo.Replace("{{nomecliente}}", cliente.clienteNome);
            corpo = corpo.Replace("{{numeropedido}}", rnFuncoes.retornaIdCliente(Convert.ToInt32(pedidoId)).ToString());

            string urlBoleto = "";
            int tipoDePagamentoId = Convert.ToInt32(pedido.tipoDePagamentoId);
            int condDePagamentoId = Convert.ToInt32(pedido.condDePagamentoId);
            string imgCondicao = pedido.tbCondicoesDePagamento.imagemMini;
            string imgCondicao2 = "";
            string condicaoNome = pedido.tbCondicoesDePagamento.condicaoNome;
            string condicaoNome2 = "";

            string clienteCPFCNPJ = cliente.clienteCPFCNPJ;
            string clienteCidade = cliente.clienteCidade;
            string clienteRua = cliente.clienteRua;
            string clienteBairro = cliente.clienteBairro;
            string clienteCep = cliente.clienteCep;
            string clienteEstado = cliente.clienteEstado;


            if (tipoDePagamentoId == 8)


                corpo = corpo.Replace("{{subtotal}}", Convert.ToDecimal(pedido.valorTotalGeral).ToString("C"));
            corpo = corpo.Replace("{{frete}}", Convert.ToDecimal(pedido.valorDoFrete).ToString("C"));
            corpo = corpo.Replace("{{desconto}}", Convert.ToDecimal(pedido.valorDoDescontoDoPagamento).ToString("C"));
            corpo = corpo.Replace("{{total}}", Convert.ToDecimal(pedido.valorCobrado).ToString("C"));
            corpo = corpo.Replace("{{enderecocliente}}", pedido.endRua + ", " + pedido.endNumero + " " + pedido.endComplemento + "<br>" + pedido.endBairro + "<br>Cep: " + pedido.endCep + " - " + pedido.endCidade + " - " + pedido.endEstado);



            string nome = cliente.clienteNome;
            string email = cliente.clienteEmail;
            if (!string.IsNullOrEmpty(clienteEmail)) email = clienteEmail;


            foreach (var pacote in volumesPedido)
            {
                if (volumeAtual == 0)
                {
                    codigoTracking = pacote.rastreio;
                    urlTracking = urlTracking + pacote.rastreio;
                }
                else
                {
                    codigoTracking = codigoTracking + ", " + pacote.rastreio;
                    urlTracking = urlTracking + "%2C+" + pacote.rastreio;
                }
                volumeAtual++;
            }

            string mensagem = "foi enviado em " + volumeAtual;
            if (volumeAtual == 1) mensagem += " remessa";
            else mensagem += " remessas";

            corpo = corpo.Replace("{{fraseenviado}}", mensagem);
            corpo = corpo.Replace("{{diasentrega}}", pedido.prazoDeEntrega);

            templateRastreio = templateRastreio.Replace("{{codigorastreio}}", codigoTracking);
            corpo = corpo.Replace("{{codigorastreio}}", templateRastreio);


            corpo = corpo.Replace("{{linkrastreio}}", urlTracking);

            return rnEmails.EnviaEmail(ConfigurationManager.AppSettings["emailPosVendas"], clienteEmail, "", "", "", corpo, "Pedido Enviado");

        }
        catch (System.Net.Mail.SmtpException)
        {
            throw;
        }
        catch (Exception)
        {
            throw;
        }
        return true;
    }

    public static bool enviaCodigoDoTrakingJadlog(int idPedido, string numeroTracking, string email)
    {
        try
        {

            var pedidoDc = new dbCommerceDataContext();
            var pedido = (from c in pedidoDc.tbPedidoPacotes
                          where c.idPedidoPacote == idPedido
                          select new
                          {
                              c.idPedido,
                              c.tbPedido.clienteId,
                              c.tbPedido.numeroDoTracking4,
                              c.tbPedido.prazoDeEntrega,
                              c.tbPedido.prazoMaximoPedidos,
                              c.tbPedido.tipoDePagamentoId,
                              c.tbPedido.valorTotalGeral,
                              c.tbPedido.valorDoFrete,
                              c.tbPedido.valorDoDescontoDoPagamento,
                              c.tbPedido.valorCobrado,
                              c.tbPedido.endRua,
                              c.tbPedido.endNumero,
                              c.tbPedido.endComplemento,
                              c.tbPedido.endBairro,
                              c.tbPedido.endCep,
                              c.tbPedido.endCidade,
                              c.tbPedido.endEstado,
                              c.tbPedido.prazoFinalPedido,
                              c.tbPedido.prazoInicioFabricao
                          }).First();
            var pacote = (from c in pedidoDc.tbPedidoPacotes where c.idPedidoPacote == idPedido select c).FirstOrDefault();
            //var pacote = (from c in pedidoDc.tbPedidoPacotes where c.idPedido == idPedido select c).FirstOrDefault();
            var clienteDc = new dbCommerceDataContext();
            var cliente = (from c in clienteDc.tbClientes where c.clienteId == pedido.clienteId select c).First();

            var prazoData = DateTime.Now;

            if (pacote.dataDeCarregamento != null)
            {
                prazoData = (DateTime)pacote.dataDeCarregamento;
            }
            else if (pacote.prazoFinalEntrega != null)
            {
                prazoData = (DateTime)pacote.prazoFinalEntrega;
            }

            string clienteNome = cliente.clienteNome;
            string clienteEmail = cliente.clienteEmail;

            if (!string.IsNullOrEmpty(email)) clienteEmail = email;

            string codigoTracking = numeroTracking;
            string urlTracking = "";
            if (!string.IsNullOrEmpty(numeroTracking))
            {
                urlTracking = "http://jadlog.com.br/tracking.jsp?cte=" + numeroTracking;
            }
            else
            {
                urlTracking = "http://jadlog.com.br/tracking.jsp?cte=" + pedido.numeroDoTracking4;
                codigoTracking = pedido.numeroDoTracking4;
            }


            string corpo = carregarTemplateArquivo(System.Configuration.ConfigurationManager.AppSettings["caminhoFisico"] + "\\templateemails\\enviado\\" + "enviado.html");
            string templateRastreio = carregarTemplateArquivo(System.Configuration.ConfigurationManager.AppSettings["caminhoFisico"] + "\\templateemails\\enviado\\" + "codigorastreio.htm");

            var data = new dbCommerceDataContext();

            int prazoFinal = rnFuncoes.retornaPrazoDiasUteis(Convert.ToInt32(pedido.prazoDeEntrega), 0, (DateTime)pedido.prazoFinalPedido);
            //var dataEntrega = DateTime.Now.AddDays(prazoFinal).ToShortDateString();
            var dataEntrega = pedido.prazoFinalPedido.Value.AddDays(prazoFinal).ToShortDateString(); //prazoData.AddDays(prazoFinal).ToShortDateString();
            corpo = corpo.Replace("{{prazo}}", dataEntrega);
            corpo = corpo.Replace("{{processamento}}", ((pedido.prazoMaximoPedidos ?? 0) + (pedido.prazoInicioFabricao ?? 0)).ToString());
            corpo = corpo.Replace("{{entrega}}", pedido.prazoDeEntrega.ToString());
            corpo = corpo.Replace("{{nomecliente}}", cliente.clienteNome);
            corpo = corpo.Replace("{{numeropedido}}", rnFuncoes.retornaIdCliente(pacote.idPedido).ToString());

            int tipoDePagamentoId = Convert.ToInt32(pedido.tipoDePagamentoId);


            if (tipoDePagamentoId == 8)


                corpo = corpo.Replace("{{subtotal}}", Convert.ToDecimal(pedido.valorTotalGeral).ToString("C"));
            corpo = corpo.Replace("{{frete}}", Convert.ToDecimal(pedido.valorDoFrete).ToString("C"));
            corpo = corpo.Replace("{{desconto}}", Convert.ToDecimal(pedido.valorDoDescontoDoPagamento).ToString("C"));
            corpo = corpo.Replace("{{total}}", Convert.ToDecimal(pedido.valorCobrado).ToString("C"));
            corpo = corpo.Replace("{{enderecocliente}}", pedido.endRua + ", " + pedido.endNumero + " " + pedido.endComplemento + "<br>" + pedido.endBairro + "<br>Cep: " + pedido.endCep + " - " + pedido.endCidade + " - " + pedido.endEstado);

            string mensagem = "foi coletado pela transportadora Jadlog";

            corpo = corpo.Replace("{{fraseenviado}}", mensagem);
            corpo = corpo.Replace("{{diasentrega}}", pedido.prazoDeEntrega);

            templateRastreio = templateRastreio.Replace("{{codigorastreio}}", codigoTracking);
            corpo = corpo.Replace("{{codigorastreio}}", templateRastreio);


            corpo = corpo.Replace("{{linkrastreio}}", urlTracking);

            //clienteEmail = "renato@bark.com.br";

            return rnEmails.EnviaEmail(ConfigurationManager.AppSettings["emailPosVendas"], clienteEmail, "", "", "", corpo, "Pedido Enviado");

        }
        catch (System.Net.Mail.SmtpException)
        {
            throw;
        }
        catch (Exception ex)
        {
            string msg = ex.Message;
            return false;
        }
        return true;
    }

    public static bool enviaCodigoDoTrakingTntAsync(int idPedidoEnvio, string email)
    {
        return enviaCodigoDoTrakingTnt(idPedidoEnvio, email);
    }
    public static bool enviaCodigoDoTrakingTnt(int idPedidoEnvio, string email)
    {
        try
        {
            StringBuilder conteudo = new StringBuilder();

            var pedidoDc = new dbCommerceDataContext();
            var envio = (from c in pedidoDc.tbPedidoEnvios where c.idPedidoEnvio == idPedidoEnvio select c).First();
            var pedido = (from c in pedidoDc.tbPedidoEnvios where c.idPedidoEnvio == idPedidoEnvio select c.tbPedido).First();
            var pacote = (from c in pedidoDc.tbPedidoEnvios where c.idPedidoEnvio == idPedidoEnvio select c.tbPedidoPacotes).FirstOrDefault();
            var clienteDc = new dbCommerceDataContext();
            var cliente = (from c in clienteDc.tbClientes where c.clienteId == pedido.clienteId select c).First();

            var prazoData = DateTime.Now;

            if (pacote.Select(x => x).FirstOrDefault().dataDeCarregamento != null)
            {
                prazoData = (DateTime)pacote.Select(x => x).FirstOrDefault().dataDeCarregamento;
            }
            else if (pacote.Select(x => x).FirstOrDefault().prazoFinalEntrega != null)
            {
                prazoData = (DateTime)pacote.Select(x => x).FirstOrDefault().prazoFinalEntrega;
            }

            string clienteNome = cliente.clienteNome;
            string clienteEmail = cliente.clienteEmail;

            if (!string.IsNullOrEmpty(email)) clienteEmail = email;

            string urlTracking = "http://app.tntbrasil.com.br/LocalizacaoSimplificada/resultado_localizacao.asp?cnpj=" + cliente.clienteCPFCNPJ + "&amp;nota=" + envio.nfeNumero;

            string corpo = carregarTemplateArquivo(System.Configuration.ConfigurationManager.AppSettings["caminhoFisico"] + "\\templateemails\\enviado\\" + "enviado.html");

            int prazoFinal = rnFuncoes.retornaPrazoDiasUteis(Convert.ToInt32(pedido.prazoDeEntrega), 0);
            //var dataEntrega = DateTime.Now.AddDays(prazoFinal).ToShortDateString();
            var dataEntrega = prazoData.AddDays(prazoFinal).ToShortDateString();
            corpo = corpo.Replace("{{prazo}}", dataEntrega);
            corpo = corpo.Replace("{{processamento}}", ((pedido.prazoMaximoPedidos ?? 0) + (pedido.prazoInicioFabricao ?? 0)).ToString());
            corpo = corpo.Replace("{{entrega}}", pedido.prazoDeEntrega.ToString());
            corpo = corpo.Replace("{{nomecliente}}", cliente.clienteNome);
            corpo = corpo.Replace("{{numeropedido}}", rnFuncoes.retornaIdCliente(pedido.pedidoId).ToString());

            corpo = corpo.Replace("{{subtotal}}", Convert.ToDecimal(pedido.valorTotalGeral).ToString("C"));
            corpo = corpo.Replace("{{frete}}", Convert.ToDecimal(pedido.valorDoFrete).ToString("C"));
            corpo = corpo.Replace("{{desconto}}", Convert.ToDecimal(pedido.valorDoDescontoDoPagamento).ToString("C"));
            corpo = corpo.Replace("{{total}}", Convert.ToDecimal(pedido.valorCobrado).ToString("C"));
            corpo = corpo.Replace("{{enderecocliente}}", pedido.endRua + ", " + pedido.endNumero + " " + pedido.endComplemento + "<br>" + pedido.endBairro + "<br>Cep: " + pedido.endCep + " - " + pedido.endCidade + " - " + pedido.endEstado);

            string mensagem = "foi coletado pela transportadora TNT";

            corpo = corpo.Replace("{{fraseenviado}}", mensagem);
            corpo = corpo.Replace("{{diasentrega}}", pedido.prazoDeEntrega);

            corpo = corpo.Replace("{{codigorastreio}}", "");


            corpo = corpo.Replace("{{linkrastreio}}", urlTracking);

            return rnEmails.EnviaEmail(ConfigurationManager.AppSettings["emailPosVendas"], clienteEmail, "", "", "", corpo, "Pedido Enviado");

        }
        catch (System.Net.Mail.SmtpException)
        {
            throw;
        }
        catch (Exception)
        {
            throw;
        }
        return true;
    }

    public static string carregarTemplateArquivo(string pathArquivo)
    {
        try
        {
            if (!File.Exists(pathArquivo))
                throw new Exception(String.Format("Arquivo '{0}' não encontrado!", pathArquivo));

            return File.ReadAllText(pathArquivo, Encoding.GetEncoding("ISO-8859-1"));
        }
        catch (Exception ex)
        {
            return "";
        }
    }
}
