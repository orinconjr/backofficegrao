﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Web.Configuration;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Collections.Generic;

/// <summary>
/// Summary description for rnUsuarios
/// </summary>
public class rnUsuarios
{
    public static DataSet usuarioSeleciona_PorUsuarioNomeUsuarioSenha(string usuarioNome, string usuarioSenha)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("usuarioSeleciona_PorUsuarioNomeUsuarioSenha");

        db.AddInParameter(dbCommand, "usuarioNome", DbType.String, usuarioNome);
        db.AddInParameter(dbCommand, "usuarioSenha", DbType.String, usuarioSenha);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet usuarioSeleciona_PorUsuarioId(int usuarioId)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("usuarioSeleciona_PorUsuarioId");

        db.AddInParameter(dbCommand, "usuarioId", DbType.Int32, usuarioId);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet usuarioAcessoSeleciona_PorUsuarioId(int usuarioId)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("usuarioAcessoSeleciona_PorUsuarioId");

        db.AddInParameter(dbCommand, "usuarioId", DbType.Int32, usuarioId);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet usuarioPaginasPermitidaSeleciona_PorUsuarioId(int usuarioId)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("usuarioPaginasPermitidaSeleciona_PorUsuarioId");

        db.AddInParameter(dbCommand, "usuarioId", DbType.Int32, usuarioId);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet usuarioDepartamentoSeleciona_PorUsuarioIdDepartamentoId(int usuarioId, int idDepartamento)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("usuarioDepartamentoPermitidoSeleciona_PorUsuarioIdDepartamento");

        db.AddInParameter(dbCommand, "usuarioId", DbType.Int32, usuarioId);
        db.AddInParameter(dbCommand, "idDepartamento", DbType.Int32, idDepartamento);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }


    public static DataSet usuarioPaginasPermitidaSeleciona_PorUsuarioIdPaginaPermitidaNome(int usuarioId, string paginaPermitidaNome)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("usuarioPaginasPermitidaSeleciona_PorUsuarioIdPaginaPermitidaNome");

        db.AddInParameter(dbCommand, "usuarioId", DbType.Int32, usuarioId);
        db.AddInParameter(dbCommand, "paginaPermitidaNome", DbType.String, paginaPermitidaNome);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet usuarioSelecionaUltimoId()
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("usuarioSelecionaUltimoId");

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static bool usuarioAcessoInclui(int usuarioId)
    {
        bool result = false;

        Database db = DatabaseFactory.CreateDatabase();

        DbCommand dbCommand = db.GetStoredProcCommand("usuarioAcessoInclui");

        db.AddInParameter(dbCommand, "usuarioId", DbType.Int32, usuarioId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
            DbTransaction transaction = connection.BeginTransaction();

            try
            {
                db.ExecuteNonQuery(dbCommand, transaction);

                transaction.Commit();

                result = true;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
                
            }
            connection.Close();

            return result;
        }
    }

    public static bool usuarioPaginasPermitidaInclui(int usuarioId, string paginaPermitidaNome)
    {
        bool result = false;

        Database db = DatabaseFactory.CreateDatabase();

        DbCommand dbCommand = db.GetStoredProcCommand("usuarioPaginasPermitidaInclui");

        db.AddInParameter(dbCommand, "usuarioId", DbType.Int32, usuarioId);
        db.AddInParameter(dbCommand, "paginaPermitidaNome", DbType.String, paginaPermitidaNome);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
            DbTransaction transaction = connection.BeginTransaction();

            try
            {
                db.ExecuteNonQuery(dbCommand, transaction);

                transaction.Commit();

                result = true;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
                
            }
            connection.Close();

            return result;
        }
    }

    public static bool usuarioDepartamentoInclui(int usuarioId, int idDepartamento)
    {
        bool result = false;

        Database db = DatabaseFactory.CreateDatabase();

        DbCommand dbCommand = db.GetStoredProcCommand("usuarioDepartamentoInclui");

        db.AddInParameter(dbCommand, "usuarioId", DbType.Int32, usuarioId);
        db.AddInParameter(dbCommand, "idDepartamento", DbType.Int32, idDepartamento);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
            DbTransaction transaction = connection.BeginTransaction();

            try
            {
                db.ExecuteNonQuery(dbCommand, transaction);

                transaction.Commit();

                result = true;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);

            }
            connection.Close();

            return result;
        }
    }

    public static bool usuarioDepartamentoExclui(int usuarioId, int idDepartamento)
    {
        bool result = false;

        Database db = DatabaseFactory.CreateDatabase();

        DbCommand dbCommand = db.GetStoredProcCommand("usuarioDepartamentoExclui");

        db.AddInParameter(dbCommand, "usuarioId", DbType.Int32, usuarioId);
        db.AddInParameter(dbCommand, "idDepartamento", DbType.Int32, idDepartamento);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
            DbTransaction transaction = connection.BeginTransaction();

            try
            {
                db.ExecuteNonQuery(dbCommand, transaction);

                transaction.Commit();

                result = true;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);

            }
            connection.Close();

            return result;
        }
    }

    public static bool usuarioPaginasPermitidaExclui(int usuarioId)
    {
        bool result = false;

        Database db = DatabaseFactory.CreateDatabase();

        DbCommand dbCommand = db.GetStoredProcCommand("usuarioPaginasPermitidaExclui");

        db.AddInParameter(dbCommand, "usuarioId", DbType.Int32, usuarioId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
            DbTransaction transaction = connection.BeginTransaction();

            try
            {
                db.ExecuteNonQuery(dbCommand, transaction);

                transaction.Commit();

                result = true;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
                
            }
            connection.Close();

            return result;
        }
    }

    public static string retornaNomeUsuarioLogado()
    {
        HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
        usuarioLogadoId = HttpContext.Current.Request.Cookies["usuarioLogadoId"];
        return rnUsuarios.usuarioSeleciona_PorUsuarioId(int.Parse(usuarioLogadoId.Value.ToString())).Tables[0].Rows[0]["usuarioNome"].ToString();
    }

    public static retornoLogin logar(string login, string senha, string ip)
    {
        var retorno = new retornoLogin();
        retorno.valido = false;
        using (var data = new dbCommerceDataContext())
        {
            var usuario = (from c in data.tbUsuarios where c.usuarioNome.ToLower() == login.ToLower() && c.usuarioSenha.ToLower() == senha.ToLower() select c).FirstOrDefault();
            if(usuario != null)
            {
                var toke_expire = DateTime.Now.AddHours(2);
                usuario.token = Guid.NewGuid().ToString();
                usuario.token_expire = toke_expire;
                retorno.token = usuario.token;
                retorno.usuarioId = usuario.usuarioId;
                retorno.usuario = usuario.usuarioNome;
                retorno.valido = true;

                var agora = DateTime.Now;
                var acesso = new tbUsuarioAcesso();
                acesso.acessoData = agora;
                acesso.ip = ip;
                acesso.usuarioId = usuario.usuarioId;
                data.tbUsuarioAcessos.InsertOnSubmit(acesso);
                data.SubmitChanges();
            }
            else
            {
                var log = new rnLog();
                log.usuario = login.ToLower();
                log.descricoes = new List<string>();
                log.tiposRelacionados = new List<rnEnums.TipoRelacionadoIds>();
                log.tiposOperacao = new List<rnEnums.TipoOperacao>();
                log.descricoes.Add("Tentativa de acesso com dados inválidos");
                log.descricoes.Add(ip);
                log.descricoes.Add(login + " - " + senha);
                log.InsereLog();
            }
        }
        return retorno;
    }

    public static retornoLogin validaToken(int usuarioId, string token)
    {
        var retorno = new retornoLogin();
        retorno.valido = false;
        using (var data = new dbCommerceDataContext())
        {
            var agora = DateTime.Now;
            var usuario = (from c in data.tbUsuarios where c.usuarioId == usuarioId && c.token == token && c.token_expire > agora select c).FirstOrDefault();
            if (usuario != null)
            {
                var toke_expire = DateTime.Now.AddHours(2);
                usuario.token_expire = toke_expire;
                data.SubmitChanges();
                retorno.token = usuario.token;
                retorno.usuarioId = usuario.usuarioId;
                retorno.usuario = usuario.usuarioNome;
                retorno.valido = true;
            }
        }
        return retorno;
    }

    public class retornoLogin
    {
        public bool valido { get; set; }
        public int usuarioId { get; set; }
        public string token { get; set; }
        public string usuario { get; set; }
    }
}
