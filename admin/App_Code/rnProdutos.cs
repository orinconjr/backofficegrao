﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Web.Configuration;
using System.Collections;
using System.Diagnostics.Eventing.Reader;

/// <summary>
/// Summary description for rnProdutos
/// </summary>
public class rnProdutos
{
    public static Page page()
    {
        return (Page)HttpContext.Current.Handler;
    }

    #region monta variaveis para o parcelamento
    public static int numeroMaximoDeParcelas()
    {
        if (rnCondicoesDePagamento.condicaoDePagamentoSelecionaDestaque().Tables[0].Rows.Count > 0)
            return int.Parse(rnCondicoesDePagamento.condicaoDePagamentoSelecionaDestaque().Tables[0].Rows[0]["numeroMaximoDeParcelas"].ToString());
        else
            return 0;
    }
    public static int semJurosAte = 0;

    public static double taxaDeJuros;
    public static decimal valorMinimoDaParcela;
    public static decimal porcentagemDeDesconto;
    public static decimal valorDaParcela;
    public static decimal valorDoDesconto;
    #endregion

    public static void dtlDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            int produtoId = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "produtoId"));

            HyperLink fotoDestaque = (HyperLink)e.Item.FindControl("fotoDestaque");
            HyperLink hplColecao = (HyperLink)e.Item.FindControl("hplColecao");
            HtmlTableRow trFreteGratis = (HtmlTableRow)e.Item.FindControl("trFreteGratis");
            HyperLink imgFreteGratis = (HyperLink)e.Item.FindControl("imgFreteGratis");
            HtmlTableRow trPromocao = (HtmlTableRow)e.Item.FindControl("trPromocao");
            HyperLink imgPromocao = (HyperLink)e.Item.FindControl("imgPromocao");
            HtmlTableRow trLancamento = (HtmlTableRow)e.Item.FindControl("trLancamento");
            HyperLink imgLancamento = (HyperLink)e.Item.FindControl("imgLancamento");
            HtmlTableRow trEsgotado = (HtmlTableRow)e.Item.FindControl("trEsgotado");
            HyperLink imgEsgotado = (HyperLink)e.Item.FindControl("HyperLink");
            HtmlTableRow trPrecoPromocional = (HtmlTableRow)e.Item.FindControl("trPrecoPromocional");
            HyperLink hplPrecoPromocional = (HyperLink)e.Item.FindControl("hplPrecoPromocional");
            HyperLink hplPreco = (HyperLink)e.Item.FindControl("hplPreco");
            HyperLink hplParcelamento = (HyperLink)e.Item.FindControl("hplParcelamento");
            HtmlTableRow trLegendaAtacado = (HtmlTableRow)e.Item.FindControl("trLegendaAtacado");
            HtmlTableRow trPrecoAtacado = (HtmlTableRow)e.Item.FindControl("trPrecoAtacado");
            HtmlTableRow trAviseMe = (HtmlTableRow)e.Item.FindControl("trAviseMe");
            HtmlTableRow trComprar = (HtmlTableRow)e.Item.FindControl("trComprar");
            HtmlTableRow trIncluido = (HtmlTableRow)e.Item.FindControl("trIncluido");

            if (rnProdutos.produtoSeleciona_PorProdutoId(int.Parse(DataBinder.Eval(e.Item.DataItem, "produtoId").ToString())).Tables[0].Rows[0]["produtoPaiId"].ToString() != "0" && (HyperLink)e.Item.FindControl("hplColecao") != null)
                hplColecao.Visible = true;

            if (rnFotos.produtoFotoDestaqueSeleciona_PorProdutoId(produtoId).Tables[0].Rows.Count > 0)
                fotoDestaque.ImageUrl = "fotos/" + produtoId + "/pequena_" + rnFotos.produtoFotoDestaqueSeleciona_PorProdutoId(produtoId).Tables[0].Rows[0]["produtoFoto"].ToString() + ".jpg";
            else
                fotoDestaque.ImageUrl = "fotos/naoExiste/pequena.jpg";


            int produtoEstoqueAtual = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "produtoEstoqueAtual"));
            int produtoEstoqueMinimo = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "produtoEstoqueMinimo"));
            //se tiver o produto no estoque
            if (produtoEstoqueAtual > produtoEstoqueMinimo)
            {
                if (DataBinder.Eval(e.Item.DataItem, "produtoFreteGratis").ToString() == "True")
                    trFreteGratis.Visible = true;

                if (DataBinder.Eval(e.Item.DataItem, "produtoPromocao").ToString() == "True")
                    trPromocao.Visible = true;

                if (DataBinder.Eval(e.Item.DataItem, "produtoLancamento").ToString() == "True")
                    trLancamento.Visible = true;

                #region monta preco
                decimal produtoPreco;
                if (DataBinder.Eval(e.Item.DataItem, "produtoPrecoPromocional").ToString() != "0,0000")
                {
                    trPrecoPromocional.Visible = true;
                    hplPrecoPromocional.Text = "De: " + String.Format("{0:c}", DataBinder.Eval(e.Item.DataItem, "produtoPreco")).ToString().Trim();
                    hplPreco.Text = "Por: " + String.Format("{0:c}", DataBinder.Eval(e.Item.DataItem, "produtoPrecoPromocional")).ToString().Trim();
                    produtoPreco = decimal.Parse(DataBinder.Eval(e.Item.DataItem, "produtoPrecoPromocional").ToString());
                }
                else
                {
                    hplPreco.Text = String.Format("{0:c}", DataBinder.Eval(e.Item.DataItem, "produtoPreco")).ToString().Trim();
                    produtoPreco = decimal.Parse(DataBinder.Eval(e.Item.DataItem, "produtoPreco").ToString());
                }
                #endregion

                using (var data = new dbCommerceDataContext())
                {
                    var CondicoesDePagamento = (from c in data.tbCondicoesDePagamentos where c.ativo.ToLower() == "true" && c.destaque.ToLower() == "true" select c).FirstOrDefault();
                    valorMinimoDaParcela = CondicoesDePagamento == null ? 0 : CondicoesDePagamento.valorMinimoDaParcela;
                }

                #region monta parcelamento
                for (int i = numeroMaximoDeParcelas(); i <= numeroMaximoDeParcelas() && i > 0; i--)
                {
                    valorDaParcela = produtoPreco / i;

                    if (valorDaParcela >= valorMinimoDaParcela)
                    {
                        if (i > 1)
                        {
                            if (i <= semJurosAte)
                            {
                                hplParcelamento.Text = "ou " + i.ToString() + "x de " + String.Format("{0:c}", decimal.Parse(valorDaParcela.ToString())) + " no cartão";
                            }
                            else
                            {
                                using (var data = new dbCommerceDataContext())
                                {
                                    var CondicoesDePagamento = (from c in data.tbCondicoesDePagamentos where c.ativo.ToLower() == "true" && c.destaque.ToLower() == "true" select c).FirstOrDefault();
                                    taxaDeJuros = (CondicoesDePagamento == null ? 0 : CondicoesDePagamento.taxaDeJuros) / 100;
                                }

                                hplParcelamento.Text = "ou " + i.ToString() + "x de " + String.Format("{0:c}", (double)valorDaParcela * (Math.Pow((1 + taxaDeJuros), i))) + " no cartão";
                            }
                            break;
                        }
                    }
                }
                #endregion

                if (DataBinder.Eval(e.Item.DataItem, "produtoPrecoAtacado").ToString() != "0,0000")
                {
                    trLegendaAtacado.Visible = true;
                    trPrecoAtacado.Visible = true;
                }

                trComprar.Visible = true;

                if (page().Request.Cookies["cliente"] != null)
                {
                    if (page().Request.Cookies["cliente"]["pedidoId"] != null)
                    {
                        if (rnPedidos.itensPedidoSeleciona_PorPedidoIdEProdutoId(int.Parse(page().Request.Cookies["cliente"]["pedidoId"].ToString()), produtoId).Tables[0].Rows.Count > 0)
                        {
                            trIncluido.Visible = true;
                        }
                    }
                }
            }
            else
            {
                trEsgotado.Visible = true;
                trAviseMe.Visible = true;
            }
        }
    }

    public static void dtlCommand(object source, DataListCommandEventArgs e)
    {
        int produtoPaiId = int.Parse(rnProdutos.produtoSeleciona_PorProdutoId(int.Parse(e.CommandArgument.ToString())).Tables[0].Rows[0]["produtoPaiId"].ToString());

        if (rnProdutos.produtoSeleciona_PorProdutoPaiId(produtoPaiId).Tables[0].Rows.Count == 1)
        {
            if (rnProdutos.produtoSeleciona_PorProdutoId(int.Parse(e.CommandArgument.ToString())).Tables[0].Rows[0]["produtoPrecoPromocional"].ToString() != "0,0000")
            {
                rnPedidos.pedidoEItensInclui(int.Parse(e.CommandArgument.ToString()), 1, decimal.Parse(rnProdutos.produtoSeleciona_PorProdutoId(int.Parse(e.CommandArgument.ToString())).Tables[0].Rows[0]["produtoPrecoPromocional"].ToString()), 0, 0);
                //redireciona para que seja atualizada a página e o carrinho das páginas
                page().Response.Redirect(page().Request.UrlReferrer.ToString());
            }
            else
            {
                rnPedidos.pedidoEItensInclui(int.Parse(e.CommandArgument.ToString()), 1, decimal.Parse(rnProdutos.produtoSeleciona_PorProdutoId(int.Parse(e.CommandArgument.ToString())).Tables[0].Rows[0]["produtoPreco"].ToString()), 0, 0);
                //redireciona para que seja atualizada a página e o carrinho das páginas
                page().Response.Redirect(page().Request.UrlReferrer.ToString());
            }
        }
        else
            page().Response.Write("<script>window.location=('" + ConfigurationManager.AppSettings["caminhoVirtual"].ToString() + rnProdutos.produtoSeleciona_PorProdutoId(int.Parse(e.CommandArgument.ToString())).Tables[0].Rows[0]["produtoUrl"].ToString() + "/produto/" + e.CommandArgument.ToString() + ".aspx');</script>");
    }

    public static DataSet produtoSelecionaLancamentos()
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("produtoSelecionaLancamentos");

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet produtoSelecionaPromocoes()
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("produtoSelecionaPromocoes");

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet produtoAdminSeleciona(string categoriaId)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("produtoAdminSeleciona");

        db.AddInParameter(dbCommand, "categoriaId", DbType.String, categoriaId);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static List<viewProdutosAdmin> produtosAdminListaView(string ativo)
    {

        using (var data = new dbCommerceDataContext())
        {
            var listaDeProdutos = ativo != "Todos" ? data.viewProdutosAdmins.Where(x => x.produtoAtivo == ativo).ToList() : data.viewProdutosAdmins.ToList();

            return listaDeProdutos;
        }

    }

    public static DataSet produtosAdminLista(string categoriaId, string ativo)
    {

        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("produtosAdminLista");

        db.AddInParameter(dbCommand, "categoriaId", DbType.String, categoriaId);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        if (ativo != "Todos")
        {
            DataTable filtroAtivos = ds.Tables[0].Select("produtoAtivo = " + ativo).CopyToDataTable();
            DataSet ds2 = new DataSet();
            ds2.Tables.Add(filtroAtivos);

            return ds2;
        }


        return ds;

    }

    public static DataSet produtoAdminSelecionaProdutosTroca(string categoriaId)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("produtoAdminSelecionaProdutosTroca");

        db.AddInParameter(dbCommand, "categoriaId", DbType.String, categoriaId);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet produtoAdminSelecionaAtivos(string categoriaId, string idSite)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("produtoAdminSelecionaAtivos");

        db.AddInParameter(dbCommand, "categoriaId", DbType.String, categoriaId);
        db.AddInParameter(dbCommand, "idSite", DbType.String, idSite);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }
    public static DataSet produtoAdminSelecionaSemCategoria(string categoriaId)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("produtoAdminSelecionaSemCategoria");

        db.AddInParameter(dbCommand, "categoriaId", DbType.String, categoriaId);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet produtoSeleciona_PorMarcaId(int marcaId)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("produtoSeleciona_PorMarcaId");

        db.AddInParameter(dbCommand, "produtoMarca", DbType.String, marcaId);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet produtoBusca(int categoriaId, string produtoNome)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("produtoBusca");

        db.AddInParameter(dbCommand, "categoriaId", DbType.Int32, categoriaId);
        db.AddInParameter(dbCommand, "produtoNome", DbType.String, produtoNome);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }


    public static DataSet produtoSelecionaProdutosPai()
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("produtoSelecionaProdutosPai");

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet selecionaParaGerarXML()
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("selecionaParaGerarXML");

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet produtoSelecionaProdutosFilho()
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("produtoSelecionaProdutosFilho");

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet produtoSelecionaDestaque1()
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("produtoSelecionaDestaque1");

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet produtoSelecionaDestaque2()
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("produtoSelecionaDestaque2");

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet produtoSelecionaMaisVendidos()
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("produtoSelecionaMaisVendidos");

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet produtoSelecionaIdDoProdutoPrincipal_PorProdutoPaiId(int produtoPaiId)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("produtoSelecionaIdDoProdutoPrincipal_PorProdutoPaiId");

        db.AddInParameter(dbCommand, "produtoPaiId", DbType.Int32, produtoPaiId);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet produtosSelecionaGruposDeCompreJunto_PorProdutoId(int produtoId)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("produtosSelecionaGruposDeCompreJunto_PorProdutoId");

        db.AddInParameter(dbCommand, "produtoId", DbType.Int32, produtoId);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet produtosSelecionaCompreJuntoItens_CompreJuntoGrupo(int compreJuntoGrupo)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("produtosSelecionaCompreJuntoItens_CompreJuntoGrupo");

        db.AddInParameter(dbCommand, "compreJuntoGrupo", DbType.Int32, compreJuntoGrupo);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet produtosCompreJuntoSeleciona_PorProdutoId(int produtoId)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("produtosCompreJuntoSeleciona_PorProdutoId");

        db.AddInParameter(dbCommand, "produtoId", DbType.Int32, produtoId);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet produtoSeleciona_PorCategoriasId(int categoriaId)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("produtoSeleciona_PorCategoriasId");

        db.AddInParameter(dbCommand, "categoriaId", DbType.Int32, categoriaId);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet produtoSelecionaIdDoProdutoPrincipal_PorProdutoIdDaEmpresa(string produtoIdDaEmpresa)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("produtoSelecionaIdDoProdutoPrincipal_PorProdutoIdDaEmpresa");

        db.AddInParameter(dbCommand, "produtoIdDaEmpresa", DbType.String, produtoIdDaEmpresa);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet produtoSeleciona_PorProdutoEspecificacao(string sql)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetSqlStringCommand(sql);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet produtoSeleciona_PorProdutoId(int produtoId)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("produtoSeleciona_PorProdutoId");

        db.AddInParameter(dbCommand, "produtoId", DbType.Int32, produtoId);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet produtoSeleciona_PorProdutoPaiId(int produtoPaiId)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("produtoSeleciona_PorProdutoPaiId");

        db.AddInParameter(dbCommand, "produtoPaiId", DbType.Int32, produtoPaiId);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet produtoItemEspecificacaoSeleciona_PorProdutoId(int produtoId)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("produtoItemEspecificacaoSeleciona_PorProdutoId");

        db.AddInParameter(dbCommand, "produtoId", DbType.Int32, produtoId);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet produtoItemEspecificacaoSeleciona_PorProdutoPaiId(int produtoPaiId, int especificacaoId)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("produtoItemEspecificacaoSeleciona_PorProdutoPaiId");

        db.AddInParameter(dbCommand, "produtoPaiId", DbType.Int32, produtoPaiId);
        db.AddInParameter(dbCommand, "especificacaoId", DbType.Int32, especificacaoId);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet produtoItemEspecificacaoSeleciona_PorProdutoIdEProdutoEspecificacaoId(int produtoId, int especificacaoId)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("produtoItemEspecificacaoSeleciona_PorProdutoIdEProdutoEspecificacaoId");

        db.AddInParameter(dbCommand, "produtoId", DbType.Int32, produtoId);
        db.AddInParameter(dbCommand, "especificacaoId", DbType.Int32, especificacaoId);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }
    public static DataSet produtoSelecionaCombo_porProdutoId(int produtoId)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("produtoSelecionaCombo_porProdutoId");

        db.AddInParameter(dbCommand, "produtoId", DbType.Int32, produtoId);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }
    public static DataSet produtoSelecionaComboPedido_porItemPedidoId(int itemPedidoId)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("produtoSelecionaComboPedido_porItemPedidoId");

        db.AddInParameter(dbCommand, "itemPedidoId", DbType.Int32, itemPedidoId);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet produtoSelecionaUltimoId()
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("produtoSelecionaUltimoId");

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static int produtoInclui(int produtoPaiId, string produtoPrincipal, string produtoIdDaEmpresa, string produtoCodDeBarras, string produtoNome,
                                     string produtoUrl, string produtoDescricao, int produtoFornecedor, int produtoMarca, decimal produtoPreco,
                                     decimal produtoPrecoDeCusto, decimal produtoPrecoPromocional, decimal produtoPrecoAtacado,
                                     int produtoQuantidadeMininaParaAtacado, string produtoLegendaAtacado, int produtoEstoqueAtual,
                                     int produtoEstoqueMinimo, decimal produtoPeso, string produtoAtivo, string produtoFreteGratis,
                                     string produtoLancamento, string produtoPromocao, string produtoMetaDescription, string produtoMetakeywords,
                                     string destaque1, string destaque2, string destaque3, string disponibilidadeEmEstoque, string preVenda, string produtoComposicao, string produtoFios,
                                     string produtoPecas, string produtoBrindes, string produtoTags, string ncm, string marketplaceCadastrar, string marketplaceEnviarMarca,
                                     string produtoExclusivo, int altura, int largura, int profundidade, string complementoIdDaEmpresa, string descontoProgressivoFrete, string cubagem, string nomeNovo,
                                     string exibirDiferencaCombo, int siteId, int produtoAlternativoId, bool bloqueado, int relevanciaManual, bool prontaEntrega, bool foraDeLinha,
                                     int icms, string produtoDeTroca, string produtoNomeTitle, string produtoNomeShopping, string foto360,
                                     bool limitarVendaEstoqueReal = false, bool limitarVendaEstoqueVirtual = false, int idCentroDistribuicao = 1, bool criacaoPropria = false, 
                                     bool ocultarLista = false, bool ocultarBusca = false, bool ativoMercadoLivre = false, bool ativoB2w = false, bool ativoCnova = false, bool ativoWalmart = false, 
                                     bool ativoMagazineLuiza = false, DateTime? dataAtivacao = null)
    {
        //bool result = false;
        int result = 0;


        var precoExibicao = produtoPrecoPromocional > 0 && produtoPrecoPromocional < produtoPreco
            ? produtoPrecoPromocional
            : produtoPreco;
        var parcelamento = calculaParcelamento(precoExibicao);
        var margem = produtoPrecoDeCusto == 0 ? 100 : (((precoExibicao * 100) / produtoPrecoDeCusto) - 100);
        
        //Database db = DatabaseFactory.CreateDatabase();

        //DbCommand dbCommand = db.GetStoredProcCommand("produtoInclui");

        //db.AddInParameter(dbCommand, "produtoPaiId", DbType.Int32, produtoPaiId);
        //db.AddInParameter(dbCommand, "produtoPrincipal", DbType.String, produtoPrincipal);
        //db.AddInParameter(dbCommand, "produtoIdDaEmpresa", DbType.String, produtoIdDaEmpresa);
        //db.AddInParameter(dbCommand, "produtoCodDeBarras", DbType.String, produtoCodDeBarras);
        //db.AddInParameter(dbCommand, "produtoNome", DbType.String, produtoNome.TrimStart().Replace("  ", " "));
        //db.AddInParameter(dbCommand, "produtoUrl", DbType.String, produtoUrl);
        //db.AddInParameter(dbCommand, "produtoDescricao", DbType.String, produtoDescricao);
        //db.AddInParameter(dbCommand, "produtoFornecedor", DbType.Int32, produtoFornecedor);
        //db.AddInParameter(dbCommand, "produtoMarca", DbType.Int32, produtoMarca);
        //db.AddInParameter(dbCommand, "produtoPreco", DbType.Decimal, produtoPreco);
        //db.AddInParameter(dbCommand, "produtoPrecoDeCusto", DbType.Decimal, produtoPrecoDeCusto);
        //db.AddInParameter(dbCommand, "produtoPrecoPromocional", DbType.Decimal, produtoPrecoPromocional);
        //db.AddInParameter(dbCommand, "produtoPrecoAtacado", DbType.Decimal, produtoPrecoAtacado);
        //db.AddInParameter(dbCommand, "produtoQuantidadeMininaParaAtacado", DbType.Int32, produtoQuantidadeMininaParaAtacado);
        //db.AddInParameter(dbCommand, "produtoLegendaAtacado", DbType.String, produtoLegendaAtacado);
        //db.AddInParameter(dbCommand, "produtoEstoqueAtual", DbType.Int32, produtoEstoqueAtual);
        //db.AddInParameter(dbCommand, "produtoEstoqueMinimo", DbType.Int32, produtoEstoqueMinimo);
        //db.AddInParameter(dbCommand, "produtoPeso", DbType.Decimal, produtoPeso);
        //db.AddInParameter(dbCommand, "produtoAtivo", DbType.String, produtoAtivo);
        //db.AddInParameter(dbCommand, "produtoFreteGratis", DbType.String, produtoFreteGratis);
        //db.AddInParameter(dbCommand, "produtoLancamento", DbType.String, produtoLancamento);
        //db.AddInParameter(dbCommand, "produtoPromocao", DbType.String, produtoPromocao);
        //db.AddInParameter(dbCommand, "produtoMetaDescription", DbType.String, produtoMetaDescription);
        //db.AddInParameter(dbCommand, "produtoMetakeywords", DbType.String, produtoMetakeywords);
        //db.AddInParameter(dbCommand, "destaque1", DbType.String, destaque1);
        //db.AddInParameter(dbCommand, "destaque2", DbType.String, destaque2);
        //db.AddInParameter(dbCommand, "destaque3", DbType.String, destaque3);
        //db.AddInParameter(dbCommand, "disponibilidadeEmEstoque", DbType.String, disponibilidadeEmEstoque);
        //db.AddInParameter(dbCommand, "preVenda", DbType.String, preVenda);
        //db.AddInParameter(dbCommand, "produtoComposicao", DbType.String, produtoComposicao);
        //db.AddInParameter(dbCommand, "produtoFios", DbType.String, produtoFios);
        //db.AddInParameter(dbCommand, "produtoPecas", DbType.String, produtoPecas);
        //db.AddInParameter(dbCommand, "produtoBrindes", DbType.String, produtoBrindes);
        //db.AddInParameter(dbCommand, "produtoTags", DbType.String, produtoTags);
        //db.AddInParameter(dbCommand, "ncm", DbType.String, ncm);
        //db.AddInParameter(dbCommand, "marketplaceCadastrar", DbType.String, marketplaceCadastrar);
        //db.AddInParameter(dbCommand, "marketplaceEnviarMarca", DbType.String, marketplaceEnviarMarca);
        //db.AddInParameter(dbCommand, "produtoExclusivo", DbType.String, produtoExclusivo);
        //db.AddInParameter(dbCommand, "largura", DbType.Int32, largura);
        //db.AddInParameter(dbCommand, "altura", DbType.Int32, altura);
        //db.AddInParameter(dbCommand, "profundidade", DbType.Int32, profundidade);
        //db.AddInParameter(dbCommand, "complementoIdDaEmpresa", DbType.String, complementoIdDaEmpresa);
        //db.AddInParameter(dbCommand, "descontoProgressivoFrete", DbType.String, descontoProgressivoFrete);
        //db.AddInParameter(dbCommand, "estoqueReal", DbType.Int32, 0);
        //db.AddInParameter(dbCommand, "cubagem", DbType.String, cubagem);
        //db.AddInParameter(dbCommand, "produtoNomeNovo", DbType.String, nomeNovo);
        //db.AddInParameter(dbCommand, "exibirDiferencaCombo", DbType.String, exibirDiferencaCombo);
        //db.AddInParameter(dbCommand, "parcelamentoMaximo", DbType.Int32, Convert.ToDecimal(Convert.ToInt32(parcelamento.parcelas)));
        //db.AddInParameter(dbCommand, "parcelamentoMaximoValorParcela", DbType.Decimal, parcelamento.valor);
        //db.AddInParameter(dbCommand, "siteId", DbType.Int32, siteId);
        //db.AddInParameter(dbCommand, "produtoAlternativoId", DbType.Int32, produtoAlternativoId);
        //db.AddInParameter(dbCommand, "bloqueado", DbType.Boolean, bloqueado);
        //db.AddInParameter(dbCommand, "relevanciaManual", DbType.Int32, relevanciaManual);
        //db.AddInParameter(dbCommand, "prontaEntrega", DbType.Boolean, prontaEntrega);
        //db.AddInParameter(dbCommand, "foraDeLinha", DbType.Boolean, foraDeLinha);
        //db.AddInParameter(dbCommand, "margemDeLucro", DbType.Decimal, margem);
        //db.AddInParameter(dbCommand, "icms", DbType.Int32, icms);

        var produtoNew = new tbProduto();
        produtoNew.produtoPaiId = produtoPaiId;
        produtoNew.produtoPrincipal = produtoPrincipal;
        produtoNew.produtoIdDaEmpresa = produtoIdDaEmpresa;
        produtoNew.produtoCodDeBarras = produtoCodDeBarras;
        produtoNew.produtoNome = produtoNome.TrimStart().Replace("  ", " ");
        produtoNew.produtoNomeTitle = produtoNomeTitle.TrimStart().Replace("  ", " ");
        produtoNew.produtoNomeShopping = produtoNomeShopping.TrimStart().Replace("  ", " ");
        produtoNew.foto360 = foto360;
        produtoNew.produtoUrl = produtoUrl;
        produtoNew.produtoDescricao = produtoDescricao;
        produtoNew.produtoFornecedor = produtoFornecedor;
        produtoNew.produtoMarca = produtoMarca;
        produtoNew.produtoPreco = produtoPreco;
        produtoNew.produtoPrecoDeCusto = produtoPrecoDeCusto;
        produtoNew.produtoPrecoPromocional = produtoPrecoPromocional;
        produtoNew.produtoPrecoAtacado = produtoPrecoAtacado;
        produtoNew.produtoQuantidadeMininaParaAtacado = produtoQuantidadeMininaParaAtacado;
        produtoNew.produtoLegendaAtacado = produtoLegendaAtacado;
        produtoNew.produtoEstoqueAtual = produtoEstoqueAtual;
        produtoNew.produtoEstoqueMinimo = produtoEstoqueMinimo;
        produtoNew.produtoPeso = (float)produtoPeso;
        produtoNew.produtoAtivo = produtoAtivo;
        produtoNew.produtoFreteGratis = produtoFreteGratis;
        produtoNew.produtoLancamento = produtoLancamento;
        produtoNew.produtoPromocao = produtoPromocao;
        produtoNew.produtoMetaDescription = produtoMetaDescription;
        produtoNew.produtoMetakeywords = produtoMetakeywords;
        produtoNew.destaque1 = destaque1;
        produtoNew.destaque2 = destaque2;
        produtoNew.destaque3 = destaque3;
        produtoNew.disponibilidadeEmEstoque = disponibilidadeEmEstoque;
        produtoNew.preVenda = preVenda;
        produtoNew.produtoComposicao = produtoComposicao;
        produtoNew.produtoFios = produtoFios;
        produtoNew.produtoPecas = produtoPecas;
        produtoNew.produtoBrindes = produtoBrindes;
        produtoNew.produtoTags = produtoTags;
        produtoNew.ncm = ncm;
        produtoNew.marketplaceCadastrar = marketplaceCadastrar;
        produtoNew.marketplaceEnviarMarca = marketplaceEnviarMarca;
        produtoNew.produtoExclusivo = produtoExclusivo;
        produtoNew.largura = largura;
        produtoNew.altura = altura;
        produtoNew.profundidade = profundidade;
        produtoNew.complementoIdDaEmpresa = complementoIdDaEmpresa;
        produtoNew.descontoProgressivoFrete = descontoProgressivoFrete;
        produtoNew.estoqueReal = 0;
        produtoNew.cubagem = cubagem;
        produtoNew.produtoNomeNovo = nomeNovo;
        produtoNew.exibirDiferencaCombo = exibirDiferencaCombo;
        produtoNew.parcelamentoMaximo = parcelamento.parcelas;
        produtoNew.parcelamentoMaximoValorParcela = parcelamento.valor;
        produtoNew.siteId = siteId;
        produtoNew.produtoAlternativoId = produtoAlternativoId;
        produtoNew.bloqueado = bloqueado;
        produtoNew.relevanciaManual = relevanciaManual;
        produtoNew.prontaEntrega = prontaEntrega;
        produtoNew.foraDeLinha = foraDeLinha;
        produtoNew.margemDeLucro = margem;
        produtoNew.icms = icms;
        produtoNew.dataDaCriacao = DateTime.Now;
        produtoNew.produtoDataDaCriacao = DateTime.Now;
        produtoNew.limitarVendaEstoqueReal = limitarVendaEstoqueReal;
        produtoNew.limitarVendaEstoqueVirtual = limitarVendaEstoqueVirtual;
        produtoNew.idCentroDistribuicao = idCentroDistribuicao;
        produtoNew.criacaoPropria = criacaoPropria;
        produtoNew.ocultarLista = ocultarLista;
        produtoNew.ocultarBusca = ocultarBusca;
        produtoNew.ativoMercadoLivre = ativoMercadoLivre;
        produtoNew.ativoB2w = ativoB2w;
        produtoNew.ativoCnova = ativoCnova;
        produtoNew.ativoWalmart = ativoWalmart;
        produtoNew.ativoMagazineLuiza = ativoMagazineLuiza;
        produtoNew.dataAtivacao = dataAtivacao;

        try
        {
            using (var data = new dbCommerceDataContext())
            {
                data.tbProdutos.InsertOnSubmit(produtoNew);
                data.SubmitChanges();


                var prazoDeEntrega = PrazoProduto(produtoNew.produtoId);
                if (prazoDeEntrega != produtoNew.prazoDeEntrega)
                {
                    produtoNew.prazoDeEntrega = prazoDeEntrega;
                    data.SubmitChanges();
                }

                var log = new rnLog();
                log.usuario = rnUsuarios.retornaNomeUsuarioLogado();
                log.descricoes.Add("Produto cadastrado");
                var tipoRelacionado = new rnEnums.TipoRelacionadoIds() { idRegistroRelacionado = produtoNew.produtoId, tipoRelacionado = rnEnums.TipoRegistroRelacionado.Produto };
                log.tiposRelacionados.Add(tipoRelacionado);
                log.tiposOperacao.Add(rnEnums.TipoOperacao.Produto);
                log.InsereLog();

                rnIntegracoes.atualizaProdutoExtra(produtoNew.produtoId);

                result = produtoNew.produtoId;
            }
        }
        catch (Exception e)
        {

            throw new Exception(e.Message);
        }

        using(var data = new dbCommerceDataContext())
        {
            var queue = new tbQueue()
            {
                agendamento = DateTime.Now,
                andamento = false,
                concluido = false,
                idRelacionado = result,
                mensagem = "1",
                tipoQueue = 15
            };
            data.tbQueues.InsertOnSubmit(queue);

            var produtosGrupo = (from c in data.tbProdutos where c.produtoPaiId == produtoNew.produtoPaiId && c.produtoPaiId > 0 select c.produtoId).ToList();
            foreach(var produtoGrupo in produtosGrupo)
            {
                var queueFilhos = new tbQueue()
                {
                    agendamento = DateTime.Now,
                    andamento = false,
                    concluido = false,
                    idRelacionado = produtoGrupo,
                    mensagem = "1",
                    tipoQueue = 15
                };
                data.tbQueues.InsertOnSubmit(queueFilhos);
            }
            data.SubmitChanges();
        }
        return result;

        //using (DbConnection connection = db.CreateConnection())
        //{
        //    connection.Open();
        //    DbTransaction transaction = connection.BeginTransaction();

        //    try
        //    {
        //        db.ExecuteNonQuery(dbCommand, transaction);

        //        transaction.Commit();

        //        result = true;




        //    }
        //    catch (Exception e)
        //    {
        //        throw new Exception(e.Message);

        //    }
        //    connection.Close();

        //    int produtoId = int.Parse(rnProdutos.produtoSelecionaUltimoId().Tables[0].Rows[0]["produtoId"].ToString());


        //    var log = new rnLog();
        //    log.usuario = rnUsuarios.retornaNomeUsuarioLogado();
        //    log.descricoes.Add("Produto cadastrado");
        //    var tipoRelacionado = new rnEnums.TipoRelacionadoIds() { idRegistroRelacionado = produtoId, tipoRelacionado = rnEnums.TipoRegistroRelacionado.Produto };
        //    log.tiposRelacionados.Add(tipoRelacionado);
        //    log.tiposOperacao.Add(rnEnums.TipoOperacao.Produto);
        //    log.InsereLog();


        //    try
        //    {
        //        //rnBuscaCloudSearch.AtualizarProduto(produtoId);
        //        rnIntegracoes.atualizaProdutoExtra(produtoId);

        //        //rnIntegracoes.cadastraAtualizaProdutoRakuten(produtoId);
        //        //mlIntegracao.cadastraAtualizaProdutos(produtoId);
        //    }
        //    catch (Exception) { }

        //    return result;
        //}
    }

    public static bool produtoAlterar(int produtoPaiId, string produtoIdDaEmpresa, string produtoCodDeBarras, string produtoNome, string produtoUrl,
                                     string produtoDescricao, int produtoFornecedor, int produtoMarca, decimal produtoPreco,
                                     decimal produtoPrecoDeCusto, decimal produtoPrecoPromocional, decimal produtoPrecoAtacado,
                                     int produtoQuantidadeMininaParaAtacado, string produtoLegendaAtacado, int produtoEstoqueAtual,
                                     int produtoEstoqueMinimo, decimal produtoPeso, string produtoAtivo, string produtoFreteGratis,
                                     string produtoLancamento, string produtoPromocao, string produtoMetaDescription, string produtoMetakeywords,
                                     string destaque1, string destaque2, string destaque3, int produtoId, string disponibilidadeEmEstoque, string preVenda, string produtoComposicao,
                                     string produtoFios, string produtoPecas, string produtoBrindes, string produtoTags, string ncm, string marketplaceCadastrar, string marketplaceEnviarMarca,
                                     string produtoExclusivo, int altura, int largura, int profundidade, string complementoIdDaEmpresa, string descontoProgressivoFrete, string cubagem, string nomeNovo,
                                     string exibirDiferencaCombo, decimal valorDiferencaCombo, int siteId, int produtoAlternativoId, bool bloqueado, int relevanciaManual, bool prontaEntrega,
                                     bool foraDeLinha, int icms, string produtoNomeTitle, string produtoNomeShopping, string foto360,
                                     bool limitarVendaEstoqueReal = false, bool limitarVendaEstoqueVirtual = false, int idCentroDistribuicao = 1, bool criacaoPropria = false,
                                        bool ocultarLista = false, bool ocultarBusca = false, bool ativoMercadoLivre = false, bool ativoB2w = false, bool ativoCnova = false, bool ativoWalmart = false,
                                     bool ativoMagazineLuiza = false)
    {
        bool result = false;

        var dataOriginal = new dbCommerceDataContext();
        var produtoOriginal = (from c in dataOriginal.tbProdutos where c.produtoId == produtoId select c).First();

        string produtoUrlAtual = produtoOriginal.produtoUrl;
        var precoExibicao = produtoPrecoPromocional > 0 && produtoPrecoPromocional < produtoPreco
            ? produtoPrecoPromocional
            : produtoPreco;
        var parcelamento = calculaParcelamento(precoExibicao);

        var margem = produtoPrecoDeCusto == 0 ? 100 : (((precoExibicao * 100) / produtoPrecoDeCusto) - 100);

        Database db = DatabaseFactory.CreateDatabase();

        DbCommand dbCommand = db.GetStoredProcCommand("produtoAlterar");
        db.AddInParameter(dbCommand, "produtoPaiId", DbType.Int32, produtoPaiId);
        db.AddInParameter(dbCommand, "produtoIdDaEmpresa", DbType.String, produtoIdDaEmpresa);
        db.AddInParameter(dbCommand, "produtoCodDeBarras", DbType.String, produtoCodDeBarras);
        db.AddInParameter(dbCommand, "produtoNome", DbType.String, produtoNome.TrimStart().Replace("  ", " "));
        db.AddInParameter(dbCommand, "foto360", DbType.String, foto360.TrimStart().Replace("  ", " "));
        db.AddInParameter(dbCommand, "produtoNomeTitle", DbType.String, produtoNomeTitle.TrimStart().Replace("  ", " "));
        db.AddInParameter(dbCommand, "produtoNomeShopping", DbType.String, produtoNomeShopping.TrimStart().Replace("  ", " "));
        db.AddInParameter(dbCommand, "produtoUrl", DbType.String, produtoUrl);
        db.AddInParameter(dbCommand, "produtoDescricao", DbType.String, produtoDescricao);
        db.AddInParameter(dbCommand, "produtoFornecedor", DbType.Int32, produtoFornecedor);
        db.AddInParameter(dbCommand, "produtoMarca", DbType.Int32, produtoMarca);
        db.AddInParameter(dbCommand, "produtoPreco", DbType.Decimal, produtoPreco);
        db.AddInParameter(dbCommand, "produtoPrecoDeCusto", DbType.Decimal, produtoPrecoDeCusto);
        db.AddInParameter(dbCommand, "produtoPrecoPromocional", DbType.Decimal, produtoPrecoPromocional);
        db.AddInParameter(dbCommand, "produtoPrecoAtacado", DbType.Decimal, produtoPrecoAtacado);
        db.AddInParameter(dbCommand, "produtoQuantidadeMininaParaAtacado", DbType.Int32, produtoQuantidadeMininaParaAtacado);
        db.AddInParameter(dbCommand, "produtoLegendaAtacado", DbType.String, produtoLegendaAtacado);
        db.AddInParameter(dbCommand, "produtoEstoqueAtual", DbType.Int32, produtoEstoqueAtual);
        db.AddInParameter(dbCommand, "produtoEstoqueMinimo", DbType.Int32, produtoEstoqueMinimo);
        db.AddInParameter(dbCommand, "produtoPeso", DbType.Decimal, produtoPeso);
        db.AddInParameter(dbCommand, "produtoAtivo", DbType.String, produtoAtivo);
        db.AddInParameter(dbCommand, "produtoFreteGratis", DbType.String, produtoFreteGratis);
        db.AddInParameter(dbCommand, "produtoLancamento", DbType.String, produtoLancamento);
        db.AddInParameter(dbCommand, "produtoPromocao", DbType.String, produtoPromocao);
        db.AddInParameter(dbCommand, "produtoMetaDescription", DbType.String, produtoMetaDescription);
        db.AddInParameter(dbCommand, "produtoMetakeywords", DbType.String, produtoMetakeywords);
        db.AddInParameter(dbCommand, "destaque1", DbType.String, destaque1);
        db.AddInParameter(dbCommand, "destaque2", DbType.String, destaque2);
        db.AddInParameter(dbCommand, "destaque3", DbType.String, destaque3);
        db.AddInParameter(dbCommand, "produtoId", DbType.Int32, produtoId);
        db.AddInParameter(dbCommand, "disponibilidadeEmEstoque", DbType.String, disponibilidadeEmEstoque);
        db.AddInParameter(dbCommand, "preVenda", DbType.String, preVenda);
        db.AddInParameter(dbCommand, "produtoComposicao", DbType.String, produtoComposicao);
        db.AddInParameter(dbCommand, "produtoFios", DbType.String, produtoFios);
        db.AddInParameter(dbCommand, "produtoPecas", DbType.String, produtoPecas);
        db.AddInParameter(dbCommand, "produtoBrindes", DbType.String, produtoBrindes);
        db.AddInParameter(dbCommand, "produtoTags", DbType.String, produtoTags);
        db.AddInParameter(dbCommand, "ncm", DbType.String, ncm);
        db.AddInParameter(dbCommand, "marketplaceCadastrar", DbType.String, marketplaceCadastrar);
        db.AddInParameter(dbCommand, "marketplaceEnviarMarca", DbType.String, marketplaceEnviarMarca);
        db.AddInParameter(dbCommand, "produtoExclusivo", DbType.String, produtoExclusivo);
        db.AddInParameter(dbCommand, "largura", DbType.Int32, largura);
        db.AddInParameter(dbCommand, "altura", DbType.Int32, altura);
        db.AddInParameter(dbCommand, "profundidade", DbType.Int32, profundidade);
        db.AddInParameter(dbCommand, "complementoIdDaEmpresa", DbType.String, complementoIdDaEmpresa);
        db.AddInParameter(dbCommand, "descontoProgressivoFrete", DbType.String, descontoProgressivoFrete);
        db.AddInParameter(dbCommand, "cubagem", DbType.String, cubagem);
        db.AddInParameter(dbCommand, "produtoNomeNovo", DbType.String, nomeNovo);
        db.AddInParameter(dbCommand, "exibirDiferencaCombo", DbType.String, exibirDiferencaCombo);
        db.AddInParameter(dbCommand, "valorDiferencaCombo", DbType.Decimal, Convert.ToDecimal(Convert.ToInt32(valorDiferencaCombo)));
        db.AddInParameter(dbCommand, "parcelamentoMaximo", DbType.Int32, Convert.ToDecimal(Convert.ToInt32(parcelamento.parcelas)));
        db.AddInParameter(dbCommand, "parcelamentoMaximoValorParcela", DbType.Decimal, parcelamento.valor);
        db.AddInParameter(dbCommand, "siteId", DbType.Int32, siteId);
        db.AddInParameter(dbCommand, "produtoAlternativoId", DbType.Int32, produtoAlternativoId);
        db.AddInParameter(dbCommand, "bloqueado", DbType.Boolean, bloqueado);
        db.AddInParameter(dbCommand, "relevanciaManual", DbType.Int32, relevanciaManual);
        db.AddInParameter(dbCommand, "prontaEntrega", DbType.Boolean, prontaEntrega);
        db.AddInParameter(dbCommand, "foraDeLinha", DbType.Boolean, foraDeLinha);
        db.AddInParameter(dbCommand, "margemDeLucro", DbType.Decimal, margem);
        db.AddInParameter(dbCommand, "icms", DbType.Int32, icms);
        db.AddInParameter(dbCommand, "limitarVendaEstoqueReal", DbType.Boolean, limitarVendaEstoqueReal);
        db.AddInParameter(dbCommand, "limitarVendaEstoqueVirtual", DbType.Boolean, limitarVendaEstoqueVirtual);
        db.AddInParameter(dbCommand, "idCentroDistribuicao", DbType.Int32, idCentroDistribuicao);
        db.AddInParameter(dbCommand, "criacaoPropria", DbType.Boolean, criacaoPropria);
        db.AddInParameter(dbCommand, "ocultarLista", DbType.Boolean, ocultarLista);
        db.AddInParameter(dbCommand, "ocultarBusca", DbType.Boolean, ocultarBusca);
        db.AddInParameter(dbCommand, "ativoMercadoLivre", DbType.Boolean, ativoMercadoLivre);
        db.AddInParameter(dbCommand, "ativoB2w", DbType.Boolean, ativoB2w);
        db.AddInParameter(dbCommand, "ativoCnova", DbType.Boolean, ativoCnova);
        db.AddInParameter(dbCommand, "ativoWalmart", DbType.Boolean, ativoWalmart);
        db.AddInParameter(dbCommand, "ativoMagazineLuiza", DbType.Boolean, ativoMagazineLuiza);
        //db.AddInParameter(dbCommand, "dataAtivacao", DbType.DateTime, dataAtivacao);

        /* var pedidoDc = new dbCommerceDataContext();
         //var itensPedido = (from c in pedidoDc.tbPedidoFornecedorItems where c.idProduto == produtoId && c.brinde != true && c.entregue != true select c);
         //foreach (var item in itensPedido)
         //{
         //    item.custo = produtoPrecoDeCusto;
         //}
         pedidoDc.SubmitChanges();*/


        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
            DbTransaction transaction = connection.BeginTransaction();

            try
            {
                db.ExecuteNonQuery(dbCommand, transaction);

                transaction.Commit();

                result = true;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);

            }
            connection.Close();


            var log = new rnLog();
            log.usuario = rnUsuarios.retornaNomeUsuarioLogado();
            var dataAlterado = new dbCommerceDataContext();
            var produtoAlterado = (from c in dataAlterado.tbProdutos where c.produtoId == produtoId select c).First();



            PropertyInfo[] propriedadesOriginal = produtoOriginal.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);
            PropertyInfo[] propriedadesAlterado = produtoAlterado.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (var propriedadeOriginal in propriedadesOriginal)
            {
                if (propriedadeOriginal.PropertyType.BaseType != null)
                {
                    var propriedadeAlterado = propriedadesAlterado.Where(x => x.Name == propriedadeOriginal.Name).FirstOrDefault();
                    if (propriedadeAlterado != null)
                    {
                        try
                        {
                            string valorPropriedadeOriginal = propriedadeOriginal.GetValue(produtoOriginal, null).ToString();
                            string valorPropriedadeAlterado = propriedadeAlterado.GetValue(produtoAlterado, null).ToString();
                            if (valorPropriedadeOriginal != valorPropriedadeAlterado)
                            {
                                log.descricoes.Add(propriedadeOriginal.Name + " alterado de " + propriedadeOriginal.GetValue(produtoOriginal, null) + " para " + propriedadeAlterado.GetValue(produtoAlterado, null));
                            }
                        }
                        catch (Exception)
                        {

                        }
                    }
                }
            }


            if (produtoAlterado.produtoPrecoDeCusto != produtoOriginal.produtoPrecoDeCusto)
            {
                var dataCombo = new dbCommerceDataContext();
                var relacionados = (from c in dataCombo.tbProdutoRelacionados where c.idProdutoFilho == produtoId select c).ToList();
                foreach (var relacionado in relacionados)
                {
                    if (relacionado.idProdutoPai != produtoId)
                    {
                        var filhos = (from c in dataCombo.tbProdutoRelacionados
                                      join d in dataCombo.tbProdutos on c.idProdutoFilho equals d.produtoId
                                      where c.idProdutoPai == relacionado.idProdutoPai
                                      select new { d.produtoId, produtoPrecoDeCusto = (d.produtoPrecoDeCusto ?? 0) }).ToList();
                        var custoTotalCombo = filhos.Sum(x => x.produtoPrecoDeCusto);
                        var produtoRelacionadoAlterar = (from c in dataCombo.tbProdutos where c.produtoId == relacionado.idProdutoPai select c).First();
                        log.descricoes.Add("Custo total do combo alterado de " + produtoRelacionadoAlterar.produtoPrecoDeCusto.ToString() + " para " + custoTotalCombo.ToString());
                        log.tiposRelacionados.Add(new rnEnums.TipoRelacionadoIds() { idRegistroRelacionado = relacionado.idProdutoPai, tipoRelacionado = rnEnums.TipoRegistroRelacionado.Produto });
                        produtoRelacionadoAlterar.produtoPrecoDeCusto = custoTotalCombo;
                        dataCombo.SubmitChanges();
                    }
                }
            }

            if(produtoAlterado.produtoAtivo != produtoOriginal.produtoAtivo && produtoAlterado.produtoAtivo == "True" && produtoAlterado.dataAtivacao == null)
            {
                produtoAlterado.dataAtivacao = DateTime.Now;
                dataAlterado.SubmitChanges();
            }
            var prazoDeEntrega = PrazoProduto(produtoId);
            if(prazoDeEntrega != produtoAlterado.prazoDeEntrega)
            {
                log.descricoes.Add("Prazo de entrega alterado de " + produtoAlterado.prazoDeEntrega.ToString() + " para " + prazoDeEntrega.ToString());
                produtoAlterado.prazoDeEntrega = prazoDeEntrega;
                dataAlterado.SubmitChanges();
            }



            var tipoRelacionado = new rnEnums.TipoRelacionadoIds() { idRegistroRelacionado = produtoId, tipoRelacionado = rnEnums.TipoRegistroRelacionado.Produto };
            log.tiposRelacionados.Add(tipoRelacionado);
            log.tiposOperacao.Add(rnEnums.TipoOperacao.Produto);
            log.InsereLog();

            GravarPermalinkRedirect(produtoId, 2, produtoUrlAtual, produtoUrl);
            try
            {
                //var resultado = rnBuscaCloudSearch.AtualizarProduto(produtoId);
                //rnIntegracoes.atualizaProdutoExtra(produtoId);
                //rnIntegracoes.cadastraAtualizaProdutoRakuten(produtoId);
                //mlIntegracao.cadastraAtualizaProdutos(produtoId);
            }
            catch (Exception) { }

            using (var data = new dbCommerceDataContext())
            {
                var queue = new tbQueue()
                {
                    agendamento = DateTime.Now,
                    andamento = false,
                    concluido = false,
                    idRelacionado = produtoId,
                    mensagem = "1",
                    tipoQueue = 15
                };
                data.tbQueues.InsertOnSubmit(queue);

                var produtosGrupo = (from c in data.tbProdutos where c.produtoPaiId == produtoAlterado.produtoPaiId && c.produtoPaiId > 0 select c.produtoId).ToList();
                foreach (var produtoGrupo in produtosGrupo)
                {
                    var queueFilhos = new tbQueue()
                    {
                        agendamento = DateTime.Now,
                        andamento = false,
                        concluido = false,
                        idRelacionado = produtoGrupo,
                        mensagem = "1",
                        tipoQueue = 15
                    };
                    data.tbQueues.InsertOnSubmit(queueFilhos);
                }

                data.SubmitChanges();
            }

            return result;
        }

    }
    

    private static int PrazoProduto(int produtoId)
    {
        var data = new dbCommerceDataContext();
        var produtosComboGeral = (from c in data.tbProdutoRelacionados
                                  where c.idProdutoPai == produtoId
                                  select new
                                  {
                                      c.idProdutoPai,
                                      c.idProdutoFilho,
                                      prazo = c.tbProduto.tbProdutoFornecedor.fornecedorPrazoDeFabricacao ?? 1,
                                      c.tbProduto.tbProdutoFornecedor.dataFimFabricacao,
                                      c.tbProduto.tbProdutoFornecedor.dataInicioFabricacao
                                  }).ToList();
        int prazoAtualizado = 0;
        if (produtosComboGeral.Any())
        {
            foreach (var itemCombo in produtosComboGeral)
            {
                if (itemCombo.dataFimFabricacao != null && itemCombo.dataFimFabricacao < DateTime.Now.AddDays(2) &&
                    itemCombo.dataInicioFabricacao != null && itemCombo.dataInicioFabricacao > DateTime.Now)
                {
                    int prazo = rnFuncoes.retornaDiasUteisPeriodo(DateTime.Now, (DateTime)itemCombo.dataInicioFabricacao) + itemCombo.prazo;
                    if (prazo > prazoAtualizado) prazoAtualizado = prazo;
                }
            }
            if (produtosComboGeral.OrderByDescending(x => x.prazo).First().prazo > prazoAtualizado) prazoAtualizado = produtosComboGeral.OrderByDescending(x => x.prazo).First().prazo;

        }
        else
        {
            var prazosProduto = (from c in data.tbProdutos
                                 where c.produtoId == produtoId
                                 select new
                                 {
                                     prazo = c.tbProdutoFornecedor.fornecedorPrazoDeFabricacao ?? 1,
                                     c.tbProdutoFornecedor.dataFimFabricacao,
                                     c.tbProdutoFornecedor.dataInicioFabricacao
                                 }).First();
            if (prazosProduto.dataFimFabricacao != null && prazosProduto.dataFimFabricacao < DateTime.Now.AddDays(2) &&
                prazosProduto.dataInicioFabricacao != null && prazosProduto.dataInicioFabricacao > DateTime.Now)
            {
                int prazo = rnFuncoes.retornaDiasUteisPeriodo(DateTime.Now, (DateTime)prazosProduto.dataInicioFabricacao) + prazosProduto.prazo;
                prazoAtualizado = prazo;
            }
            else
            {
                prazoAtualizado = prazosProduto.prazo;
            }
        }

        return prazoAtualizado;
    }


    public static void AtualizaValorParcelamento(int produtoId, decimal produtoPrecoPromocional, decimal produtoPreco)
    {
        var precoExibicao = produtoPrecoPromocional > 0 && produtoPrecoPromocional < produtoPreco
            ? produtoPrecoPromocional
            : produtoPreco;
        var parcelamento = calculaParcelamento(precoExibicao);
        var data = new dbCommerceDataContext();
        var produto = (from c in data.tbProdutos where c.produtoId == produtoId select c).First();
        produto.parcelamentoMaximo = Convert.ToInt32(parcelamento.parcelas);
        produto.parcelamentoMaximoValorParcela = Convert.ToInt32(parcelamento.valor);
        data.SubmitChanges();
    }

    public class retornoParcelamento
    {
        public int parcelas { get; set; }
        public decimal valor { get; set; }

    }

    public static retornoParcelamento calculaParcelamento(decimal valor)
    {
        var data = new dbCommerceDataContext();
        var dvCondicoesDePagamento = (from c in data.tbCondicoesDePagamentos where c.ativo.ToLower() == "true" select c).ToList();

        string mensagemDesconto = "";

        var condicao = dvCondicoesDePagamento.First(x => x.destaque.ToLower() == "true");
        int parcelasSemJuros = condicao.parcelasSemJuros;
        double taxaDeJuros = condicao.taxaDeJuros / 100;
        decimal valorMinimoDaParcela = condicao.valorMinimoDaParcela;
        decimal porcentagemDeDesconto = Convert.ToDecimal(condicao.porcentagemDeDesconto);
        int numeroMaximoDeParcelas = condicao.numeroMaximoDeParcelas;
        decimal valorDaParcela = 0;

        decimal produtoPreco = valor;


        var retornoParcelamento = new retornoParcelamento();
        retornoParcelamento.parcelas = 0;
        retornoParcelamento.valor = 0;

        for (int i = numeroMaximoDeParcelas; i <= numeroMaximoDeParcelas && i > 0; i--)
        {
            valorDaParcela = produtoPreco / i;

            if (valorDaParcela >= valorMinimoDaParcela)
            {
                if (i > 1)
                {
                    if (i <= parcelasSemJuros)
                    {
                        retornoParcelamento.parcelas = i;
                        retornoParcelamento.valor = decimal.Parse(valorDaParcela.ToString());
                    }
                    break;
                }
            }
        }

        return retornoParcelamento;
    }
    public static bool produtoAlterarEmLote(int produtoPaiId, string produtoIdDaEmpresa, string produtoNome, string produtoUrl, decimal produtoPreco, int produtoEstoqueAtual,
                                          decimal produtoPeso, string produtoAtivo, string produtoCodDeBarras, int produtoEstoqueMinimo,
                                          decimal produtoPrecoDeCusto, decimal produtoPrecoPromocional, decimal produtoPrecoAtacado,
                                          string produtoFreteGratis, string produtoLancamento, string produtoPromocao,
                                          string destaque1, string destaque2, string destaque3, int produtoId, string complementoIdDaEmpresa, string produtoDescricao,
                                          string descontoProgressivoFrete, bool foraDeLinha)
    {
        bool result = false;



        var precoExibicao = produtoPrecoPromocional > 0 && produtoPrecoPromocional < produtoPreco
            ? produtoPrecoPromocional
            : produtoPreco;
        var parcelamento = calculaParcelamento(precoExibicao);


        Database db = DatabaseFactory.CreateDatabase();

        DbCommand dbCommand = db.GetStoredProcCommand("produtoAlterarEmLote");

        db.AddInParameter(dbCommand, "produtoPaiId", DbType.String, produtoPaiId);
        db.AddInParameter(dbCommand, "produtoIdDaEmpresa", DbType.String, produtoIdDaEmpresa);
        db.AddInParameter(dbCommand, "produtoNome", DbType.String, produtoNome);
        db.AddInParameter(dbCommand, "produtoUrl", DbType.String, produtoUrl);
        db.AddInParameter(dbCommand, "produtoPreco", DbType.Decimal, produtoPreco);
        db.AddInParameter(dbCommand, "produtoEstoqueAtual", DbType.Int32, produtoEstoqueAtual);
        db.AddInParameter(dbCommand, "produtoPeso", DbType.Decimal, produtoPeso);
        db.AddInParameter(dbCommand, "produtoAtivo", DbType.String, produtoAtivo);
        db.AddInParameter(dbCommand, "produtoCodDeBarras", DbType.String, produtoCodDeBarras);
        db.AddInParameter(dbCommand, "produtoEstoqueMinimo", DbType.Int32, produtoEstoqueMinimo);
        db.AddInParameter(dbCommand, "produtoPrecoDeCusto", DbType.Decimal, produtoPrecoDeCusto);
        db.AddInParameter(dbCommand, "produtoPrecoPromocional", DbType.Decimal, produtoPrecoPromocional);
        db.AddInParameter(dbCommand, "produtoPrecoAtacado", DbType.Decimal, produtoPrecoAtacado);
        db.AddInParameter(dbCommand, "produtoFreteGratis", DbType.String, produtoFreteGratis);
        db.AddInParameter(dbCommand, "produtoLancamento", DbType.String, produtoLancamento);
        db.AddInParameter(dbCommand, "produtoPromocao", DbType.String, produtoPromocao);
        db.AddInParameter(dbCommand, "destaque1", DbType.String, destaque1);
        db.AddInParameter(dbCommand, "destaque2", DbType.String, destaque2);
        db.AddInParameter(dbCommand, "destaque3", DbType.String, destaque3);
        db.AddInParameter(dbCommand, "produtoId", DbType.Int32, produtoId);
        db.AddInParameter(dbCommand, "complementoIdDaEmpresa", DbType.String, complementoIdDaEmpresa);
        db.AddInParameter(dbCommand, "produtoDescricao", DbType.String, produtoDescricao.Trim());
        db.AddInParameter(dbCommand, "descontoProgressivoFrete", DbType.String, descontoProgressivoFrete);
        db.AddInParameter(dbCommand, "parcelamentoMaximo", DbType.Int32, Convert.ToDecimal(Convert.ToInt32(parcelamento.parcelas)));
        db.AddInParameter(dbCommand, "parcelamentoMaximoValorParcela", DbType.Decimal, parcelamento.valor);
        db.AddInParameter(dbCommand, "foraDeLinha", DbType.Boolean, foraDeLinha);


        var pedidoDc = new dbCommerceDataContext();
        //var itensPedido = (from c in pedidoDc.tbPedidoFornecedorItems where c.idProduto == produtoId && c.brinde != true && c.entregue != true select c);
        //foreach (var item in itensPedido)
        //{
        //    item.custo = produtoPrecoDeCusto;
        //}
        pedidoDc.SubmitChanges();

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
            DbTransaction transaction = connection.BeginTransaction();

            try
            {
                db.ExecuteNonQuery(dbCommand, transaction);

                transaction.Commit();

                result = true;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);

            }
            connection.Close();


            try
            {
                //rnBuscaCloudSearch.AtualizarProduto(produtoId);
                rnIntegracoes.atualizaProdutoExtra(produtoId);
                //rnIntegracoes.cadastraAtualizaProdutoRakuten(produtoId);
                //mlIntegracao.cadastraAtualizaProdutos(produtoId);
            }
            catch (Exception) { }

            return result;
        }
    }

    public static bool produtoPaiIdAltera(int produtoPaiId, int produtoId)
    {
        bool result = false;

        Database db = DatabaseFactory.CreateDatabase();

        DbCommand dbCommand = db.GetStoredProcCommand("produtoPaiIdAltera");

        db.AddInParameter(dbCommand, "produtoPaiId", DbType.Int32, produtoPaiId);
        db.AddInParameter(dbCommand, "produtoId", DbType.String, produtoId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
            DbTransaction transaction = connection.BeginTransaction();

            try
            {
                db.ExecuteNonQuery(dbCommand, transaction);

                transaction.Commit();

                result = true;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);

            }
            connection.Close();

            return result;
        }
    }

    public static bool produtoAlteraEstoque(int produtoEstoqueAtual, int produtoId)
    {
        bool result = false;

        Database db = DatabaseFactory.CreateDatabase();

        DbCommand dbCommand = db.GetStoredProcCommand("produtoAlteraEstoque");

        db.AddInParameter(dbCommand, "produtoEstoqueAtual", DbType.Int32, produtoEstoqueAtual);
        db.AddInParameter(dbCommand, "produtoId", DbType.String, produtoId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
            DbTransaction transaction = connection.BeginTransaction();

            try
            {
                db.ExecuteNonQuery(dbCommand, transaction);

                transaction.Commit();

                result = true;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);

            }
            connection.Close();

            return result;
        }
    }

    public static DataSet alteraProdutoPrincipal(int produtoPaiId, int produtoId)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("alteraProdutoPrincipal");

        db.AddInParameter(dbCommand, "produtoPaiId", DbType.Int32, produtoPaiId);
        db.AddInParameter(dbCommand, "produtoId", DbType.Int32, produtoId);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static bool produtoExclui(int produtoId)
    {
        bool result = false;

        Database db = DatabaseFactory.CreateDatabase();

        DbCommand dbCommand = db.GetStoredProcCommand("produtoExclui");

        db.AddInParameter(dbCommand, "produtoId", DbType.String, produtoId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
            DbTransaction transaction = connection.BeginTransaction();

            try
            {
                db.ExecuteNonQuery(dbCommand, transaction);

                transaction.Commit();

                result = true;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);

            }
            connection.Close();

            var sku = rnIntegracoes.atualizaSkuExtra(produtoId);
            rnIntegracoes.excluiProdutoExtra(sku);
            return result;
        }
    }
    public static DataSet produtoEstoqueReal()
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("admin_produtosEmEstoque");

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }
    public static DataSet produtoEstoqueRealNegativo()
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("admin_produtosEmEstoqueNegativo");

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }
    public static DataSet produtoEstoqueRealNegativoCarolinaMoveis()
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("admin_produtosEmEstoqueNegativoCarolinaMoveis");

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }
    public static DataSet dataUltimoPedidoDoProduto(int produtoId)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("admin_dataUltimoPedidoDoProduto");
        db.AddInParameter(dbCommand, "produtoId", DbType.String, produtoId);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }
    public static DataSet dataUltimaCompraDoProduto(int produtoId)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("admin_dataUltimaCompraDoProduto");
        db.AddInParameter(dbCommand, "produtoId", DbType.String, produtoId);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }
    public static DataSet produtosReposicaoEstoque()
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("admin_produtosReposicaoEstoque");

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }


    public static string GerarPermalinkProduto(string produtoNome)
    {
        return GerarPermalinkProduto(0, produtoNome);
    }

    public static string GerarPermalinkProduto(int produtoId, string produtoNome)
    {
        var data = new dbCommerceDataContext();
        string nomeComCores = produtoNome;
        var cores = (from c in data.tbJuncaoProdutoCategorias
                     where c.tbProdutoCategoria.categoriaPaiId == 723 && c.produtoId == produtoId
                     select new { c.tbProdutoCategoria.categoriaNome }).Distinct().ToList();
        foreach (var cor in cores)
        {
            if (!produtoNome.ToLower().Contains(cor.categoriaNome.ToLower()))
            {
                nomeComCores += " " + cor.categoriaNome;
            }
        }
        string nomeLimpo = rnFuncoes.limpaString(nomeComCores.Trim()).ToLower();
        string nomeLimpoAjustado = nomeLimpo;
        bool jaExiste = true;
        int contagem = 1;
        while (jaExiste)
        {
            bool possuiProduto = (from c in data.tbProdutos where c.produtoUrl == nomeLimpoAjustado && c.produtoId != produtoId select c).Any();
            bool possuiProdutoAnterior = (from c in data.tbSeoUrls where c.idSeoUrlTipo == 2 && c.idRelacionado != produtoId && c.redirect == true && c.url == nomeLimpoAjustado select c).Any();
            if (possuiProduto | possuiProdutoAnterior)
            {
                nomeLimpoAjustado = nomeLimpo + "-" + contagem;
                contagem++;
            }
            else
            {
                jaExiste = false;
            }
        }
        return nomeLimpoAjustado;
    }

    public static void GravarPermalinkRedirect(int idRelacionado, int tipo, string permalinkAnterior, string permalinkNovo)
    {
        try
        {
            if (permalinkAnterior != permalinkNovo)
            {
                var data = new dbCommerceDataContext();
                var redirectsAnteriores = (from c in data.tbSeoUrls where c.idRelacionado == idRelacionado && c.idSeoUrlTipo == tipo && c.redirect == true && c.url != permalinkNovo select c).ToList();
                foreach (var redirectAnterior in redirectsAnteriores)
                {
                    redirectAnterior.redirect = false;
                    var newRedirect = new tbSeoUrl();
                    newRedirect.idRelacionado = idRelacionado;
                    newRedirect.idSeoUrlTipo = tipo;
                    newRedirect.url = redirectAnterior.url;
                    newRedirect.urlRedirect = permalinkNovo;
                    newRedirect.redirect = true;
                    newRedirect.data = DateTime.Now;
                    data.tbSeoUrls.InsertOnSubmit(newRedirect);
                }

                if (permalinkAnterior != permalinkNovo)
                {
                    var redirect = new tbSeoUrl();
                    redirect.idRelacionado = idRelacionado;
                    redirect.idSeoUrlTipo = tipo;
                    redirect.url = permalinkAnterior;
                    redirect.urlRedirect = permalinkNovo;
                    redirect.redirect = true;
                    redirect.data = DateTime.Now;
                    data.tbSeoUrls.InsertOnSubmit(redirect);
                }
                var permalinkNovoCheck = (from c in data.tbSeoUrls where c.idRelacionado == idRelacionado && c.idSeoUrlTipo == tipo && c.redirect == true && c.url == permalinkNovo select c).FirstOrDefault();
                if (permalinkNovoCheck != null)
                {
                    permalinkNovoCheck.redirect = false;
                }
                data.SubmitChanges();
            }
        }
        catch(Exception ex) {

        }

    }

    public static void AtualizaDescontoCombo(int produtoId)
    {
        return;
        var data = new dbCommerceDataContext();
        var produto = (from c in data.tbProdutos where c.produtoId == produtoId select c).First();

        if ((produto.exibirDiferencaCombo ?? "").ToLower() == "true")
        {
            decimal valorDiferencaCombo = 0;
            decimal valorTotalCombo = 0;
            var relacionados = (from c in data.tbProdutoRelacionados
                                join d in data.tbProdutos on c.idProdutoFilho equals d.produtoId
                                where c.idProdutoPai == produtoId
                                select
                                    new
                                    {
                                        c.tbProduto.produtoPreco
                                    });
            if (relacionados.Any()) valorTotalCombo = relacionados.Sum(x => x.produtoPreco);
            valorDiferencaCombo = valorTotalCombo - (produto.produtoPrecoPromocional > 0
                ? (decimal)produto.produtoPrecoPromocional
                : produto.produtoPreco);
            valorDiferencaCombo = Convert.ToInt32(Math.Floor(valorDiferencaCombo));
            produto.valorDiferencaCombo = valorDiferencaCombo;
            data.SubmitChanges();
        }
        else
        {
            produto.valorDiferencaCombo = 0;
            data.SubmitChanges();

            var combos = (from c in data.tbProdutoRelacionados
                          join d in data.tbProdutos on c.idProdutoPai equals d.produtoId
                          where c.idProdutoFilho == produtoId && c.idProdutoPai != produtoId
                          select c.idProdutoPai).Distinct().ToList();
            foreach (var combo in combos)
            {
                AtualizaDescontoCombo(combo);
            }
        }
    }

    public static void AtualizaOutlet(int produtoId)
    {
        return;
        /*var listaIds = new List<int>() { 12197, 12296, 12438, 12954, 12957, 12960, 12961, 12971, 12975, 13075, 13096, 13194, 13214, 13322, 13342, 13350, 13485, 13496, 13771, 13823, 13824, 13935, 14034, 14044, 14301, 14460, 14509, 14531, 14555, 14640, 14652, 14743, 14838, 14982, 15521, 15564, 15574, 16141, 16233, 16234, 16396, 16525, 16526, 16537, 16561, 16605, 16608, 16666, 16762, 16778, 16824, 16839, 16848, 16849, 16850, 16853, 16854, 16855, 16866, 16891, 16903, 16907, 16962, 17056, 17059, 17103, 17104, 17106, 17148, 17163, 17172, 17176, 17179, 17182, 17197, 17201, 17202, 17232, 17279, 17298, 17341, 17400, 17497, 17507, 17508, 17715, 17933, 18059, 18094, 18118, 18163, 18282, 18293, 18294, 18295, 18296, 18317, 18420, 18532, 18599, 18606, 18607, 18614, 18673, 18680, 18693, 18697, 18700, 18719, 18727, 18729, 18745, 18758, 18763, 18794, 18798, 18803, 18810, 18815, 18841, 18850, 18900, 19189, 19643, 19746, 19829, 19974, 20158, 20295, 20406, 20709, 20751, 20762, 20816, 20861, 20904, 20920, 20932, 20936, 20947, 20948, 20949, 20954, 20967, 20973, 20980, 21211, 21217, 21218, 21219, 21220, 21221, 21222, 21223, 21224, 21225, 21226, 21227, 21250, 21252, 21253, 21255, 21277, 21278, 21297, 21329, 21421, 21584, 21651, 21652, 21653, 21654, 21655, 21656, 21657, 21658, 21659, 21660, 21661, 21662, 21719, 21720, 21721, 21722, 21723, 21724, 21725, 21790, 21791, 21923, 22058, 22115, 22223, 22349, 22400, 22462, 22564, 22924, 23161, 23171, 23207, 23208, 23210, 23211, 23212, 23217, 23218, 23220, 23221, 23223, 23226, 23502, 23504, 23509, 23510, 23544, 23696, 23801, 23841, 23842, 23852, 23855, 23856, 23887, 23889, 23948, 24250, 24257, 24586, 24589, 24624, 24660, 24954, 24956, 24966, 25233, 25425, 25452, 25576, 25999, 26153, 26523, 26538, 26571, 26840, 27024, 27116, 27172, 27174, 27455, 28170, 28372, 28378, 28445, 28604, 28706, 28714, 28937, 28951, 29260, 29369, 29462, 29639, 29688, 29789, 30011 };
        if (produtoId > 0)
        {
            listaIds = listaIds.Where(x => x == produtoId).ToList();
        }
        var data = new dbCommerceDataContext();
        foreach (var id in listaIds)
        {
            var produto = (from c in data.tbProdutos where c.produtoId == id select c).FirstOrDefault();
            if (produto != null)
            {
                int quantidadeOutlet = (from c in data.tbPedidoFornecedorItems
                                        where c.idPedidoFornecedor == 8801 && c.idProduto == id && c.idPedido == null
                                        select c).Count();
                if (produto.foraDeLinha)
                {
                    produto.produtoEstoqueAtual = quantidadeOutlet;
                    if (quantidadeOutlet >= 3) //Desconto de 50% - Fora de linha
                    {
                        decimal precoDescontoBoleto = produto.produtoPreco / 2;
                        decimal precoPromocional = (precoDescontoBoleto * 100) / 85;
                        produto.produtoPrecoPromocional = precoPromocional;
                        produto.produtoAtivo = "True";
                    }
                    else if (quantidadeOutlet < 3 && quantidadeOutlet > 0) // Desconto de 40% fora de linha
                    {
                        decimal precoDescontoBoleto = produto.produtoPreco - ((produto.produtoPreco / 100) * 40);
                        decimal precoPromocional = (precoDescontoBoleto * 100) / 85;
                        produto.produtoPrecoPromocional = precoPromocional;
                        produto.produtoAtivo = "True";
                    }
                    else
                    {
                        produto.produtoPrecoPromocional = 0;
                        produto.produtoAtivo = "False";
                    }
                    if (id == 18810 | id == 21297 | id == 18803 | id == 20967) // Charme
                    {
                        produto.produtoAtivo = "False";
                    }
                    var listaOutros = new List<int>() { 21227, 25576, 27923, 21725, 21724, 21723, 21722, 21721, 21720, 21719, 21662, 21661, 21660, 21659, 21658, 21657, 21656, 21655, 21654, 21653, 21652, 21651, 21584, 21421, 21329, 21226, 21225, 21224, 21223, 21222, 21221, 21220, 21219, 21218, 21217, 21211, 20949, 20947, 20816, 20762, 20709, 19974, 19189, 18296, 18295, 18294, 18163, 18094, 17933, 17341, 17298, 17232, 17179, 17176, 17106, 17104, 17103, 17059, 17056, 16962, 16907, 16903, 16866, 16853, 16849, 16848, 16824, 16778, 15574, 15564, 15521, 14652, 14555, 14531, 14034, 13096, 12960 };
                    if (listaOutros.Contains(id)) // Outros
                    {
                        produto.produtoAtivo = "False";
                    }
                    produto.produtoAtivo = "False";
                    data.SubmitChanges();
                    //rnBuscaCloudSearch.AtualizarProduto(id);
                }
                else
                {
                    if (quantidadeOutlet >= 5) //Desconto de 40% - Em linha
                    {
                        decimal precoDescontoBoleto = produto.produtoPreco - ((produto.produtoPreco / 100) * 40);
                        decimal precoPromocional = (precoDescontoBoleto * 100) / 85;
                        produto.produtoPrecoPromocional = precoPromocional;
                    }
                    else if (quantidadeOutlet < 5 && quantidadeOutlet > 0) // Desconto de 30% fora de linha
                    {
                        decimal precoDescontoBoleto = produto.produtoPreco - ((produto.produtoPreco / 100) * 30);
                        decimal precoPromocional = (precoDescontoBoleto * 100) / 85;
                        produto.produtoPrecoPromocional = precoPromocional;
                    }
                    else
                    {
                        produto.produtoPrecoPromocional = 0;
                        var juncao =
                            (from c in data.tbJuncaoProdutoCategorias
                             where c.categoriaId == 936 && c.produtoId == id
                             select c).FirstOrDefault();
                        if (juncao != null)
                        {
                            data.tbJuncaoProdutoCategorias.DeleteOnSubmit(juncao);
                        }
                    }
                    produto.produtoPrecoPromocional = 0;
                    data.SubmitChanges();
                    //rnBuscaCloudSearch.AtualizarProduto(id);
                }
            }
        }
        if (produtoId == 0)
        {
            var outrosCategoriaPromo =
                (from c in data.tbJuncaoProdutoCategorias where c.categoriaId == 936 select c).ToList();
            foreach (var outroCategoriaPromo in outrosCategoriaPromo)
            {
                bool outlet = listaIds.Any(x => x == outroCategoriaPromo.produtoId);
                if (!outlet)
                {
                    data.tbJuncaoProdutoCategorias.DeleteOnSubmit(outroCategoriaPromo);
                    data.SubmitChanges();
                    //rnBuscaCloudSearch.AtualizarProduto(outroCategoriaPromo.produtoId);
                }
            }
        }*/
    }

    public static List<tbProdutoFornecedor> GetFornecedores()
    {
        List<tbProdutoFornecedor> fornecedores;

        using (var data = new dbCommerceDataContext())
        {
            fornecedores = (from c in data.tbProdutoFornecedors orderby c.fornecedorNome select c).ToList();
        }

        return fornecedores;
    }

    public static List<tbMarca> GetMarcas()
    {
        List<tbMarca> marcas;

        using (var data = new dbCommerceDataContext())
        {
            marcas = (from c in data.tbMarcas orderby c.marcaNome select c).ToList();
        }

        return marcas;
    }
}
