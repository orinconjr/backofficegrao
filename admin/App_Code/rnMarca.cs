﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Web.Configuration;
using System.Collections;
using System.IO;


/// <summary>
/// Summary description for rnMarca
/// </summary>
public class rnMarca
{
    public static DataSet marcaSeleciona_PorMarcaId(int marcaId)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("marcaSeleciona_PorMarcaId");

        db.AddInParameter(dbCommand, "marcaId", DbType.Int32, marcaId);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet marcaSelecionaDestaque()
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("marcaSelecionaDestaque");

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static int retornaUltimoId()
    {
        var db = new dbCommerceDataContext();

        var verifica = (from marca in db.tbMarcas
                        select new { marca.marcaId }).Count();

        if (verifica > 0)
        {
            var consulta = (from marca in db.tbMarcas
                            orderby marca.marcaId descending
                            select new { marca.marcaId }).Take(1).First().marcaId.ToString();

            return Convert.ToInt32(consulta);
        }
        else
            return 1;
    }

    public static bool excluiFoto(int marcaId, string imagem)
    {
        if (File.Exists(ConfigurationManager.AppSettings["caminhoFisico"] + "marcas\\" + marcaId + "\\" + imagem))
        {
            var db = new dbCommerceDataContext();
            var consulta = (from marca in db.tbMarcas
                            where marca.marcaId == marcaId
                            select marca).Single();

            if (imagem == "imagem1.jpg")
                consulta.imagem1 = "";

            if (imagem == "imagem2.jpg")
                consulta.imagem2 = "";

            if (imagem == "imagem3.jpg")
                consulta.imagem3 = "";

            db.SubmitChanges();

            File.Delete(ConfigurationManager.AppSettings["caminhoFisico"] + "marcas\\" + marcaId + "\\" + imagem);

            return true;
        }
        else
            return false;
    }
}
