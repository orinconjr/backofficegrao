﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.OleDb;
using System.Data.Common;
using System.Web.Configuration;

/// <summary>
/// Summary description for rnCep
/// </summary>
public class rnCep
{
    public static DataTable selecionaEnderecoPorCep(string cep)
    {
        OleDbConnection conn = new OleDbConnection(ConfigurationManager.ConnectionStrings["connectionStringCep"].ConnectionString);

        try
        {
            conn.Open();

            OleDbCommand cmd = new OleDbCommand("selecionaEnderecoPorCep", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add(new OleDbParameter("@cep", cep));

            OleDbDataAdapter dta = new OleDbDataAdapter(cmd);

            DataTable dtt;
            dtt = new DataTable();
            dta.Fill(dtt);

            return dtt;

        }
        finally
        {
            conn.Close();
        }
    }
}
