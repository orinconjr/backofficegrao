﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Web.Configuration;

/// <summary>
/// Summary description for rnCondicoesDePagamento
/// </summary>
public class rnCondicoesDePagamento
{
    public static DataSet condicaoDePagamentoSelecionaDestaque()
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("condicaoDePagamentoSelecionaDestaque");

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet condicaoDePagamentoSeleciona()
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("condicaoDePagamentoSeleciona");

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet condicaoDePagamentoSelecionaAdmin()
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("condicaoDePagamentoSelecionaAdmin");

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet condicaoDePagamentoSeleciona_PorCondicaoId(int condicaoId)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("condicaoDePagamentoSeleciona_PorCondicaoId");

        db.AddInParameter(dbCommand, "condicaoId", DbType.String, condicaoId);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet condicaoDePagamentoSelecionaTodas()
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("condicaoDePagamentoSelecionaTodas");

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static bool condicaoDePagamentoAltera(int numeroMaximoDeParcelas, int parcelasSemJuros, double taxaDeJuros, double valorMinimoDaParcela, double porcentagemDeDesconto, string ativo, string condicaoNome, string destaque, int condicaoId)
    {
        bool result = false;

        Database db = DatabaseFactory.CreateDatabase();

        DbCommand dbCommand = db.GetStoredProcCommand("condicaoDePagamentoAltera");

        db.AddInParameter(dbCommand, "numeroMaximoDeParcelas", DbType.Int32, numeroMaximoDeParcelas);
        db.AddInParameter(dbCommand, "parcelasSemJuros", DbType.Int32, parcelasSemJuros);
        db.AddInParameter(dbCommand, "taxaDeJuros", DbType.Double, taxaDeJuros);
        db.AddInParameter(dbCommand, "valorMinimoDaParcela", DbType.Double, valorMinimoDaParcela);
        db.AddInParameter(dbCommand, "porcentagemDeDesconto", DbType.Double, porcentagemDeDesconto);
        db.AddInParameter(dbCommand, "ativo", DbType.String, ativo);
        db.AddInParameter(dbCommand, "condicaoNome", DbType.String, condicaoNome);
        db.AddInParameter(dbCommand, "destaque", DbType.String, destaque);
        db.AddInParameter(dbCommand, "condicaoId", DbType.Int32, condicaoId);


        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
            DbTransaction transaction = connection.BeginTransaction();

            try
            {
                db.ExecuteNonQuery(dbCommand, transaction);

                transaction.Commit();

                result = true;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
                
            }
            connection.Close();

            return result;
        }
    }
}
