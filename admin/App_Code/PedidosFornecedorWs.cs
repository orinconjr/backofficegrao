﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using Amazon.SimpleDB.Model;

/// <summary>
/// Summary description for PedidosFornecedorWs
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class PedidosFornecedorWs : System.Web.Services.WebService {
    public class DetalhesItemRomaneioWs
    {
        public int idPedidoFornecedorItem { get; set; }
        public int idPedidoFornecedor { get; set; }
        public int numeroItem { get; set; }
        public int totalItens { get; set; }
        public int produtoId { get; set; }
        public string produtoNome { get; set; }
        public string produtoIdDaEmpresa { get; set; }
        public string complementoIdDaEmpresa { get; set; }
        public string produtoFoto { get; set; }
        public string fornecedorNome { get; set; }
        public string codigoEtiqueta { get; set; }
    }

    public class ListaAguardandoLiberacao
    {
        public int idPedidoFornecedor { get; set; }
        public string fornecedorNome { get; set; }
    }

    public class EntradaRomaneioWs
    {
        public int totalProdutos { get; set; }
        public int produtosEntregues { get; set; }
        public bool adicionado { get; set; }
        public int idPedidoFornecedor { get; set; }
    }
    public class EntradaRomaneioEnderecadoWs
    {
        public int totalProdutos { get; set; }
        public int produtosEntregues { get; set; }
        public bool adicionado { get; set; }
        public int idPedidoFornecedor { get; set; }
        public string respostaEndereco { get; set; }
    }
    public PedidosFornecedorWs () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public List<admin_pedidosFornecedorConfirmadosResult> ListaPedidosFornecedor(string key)
    {
        if(key == "glmp152029")
        {
            var data = new dbCommerceDataContext();
            var pedidos = data.admin_pedidosFornecedorConfirmados(0).ToList();
            return pedidos;
        }
        return null;
    }

    [WebMethod]
    public DetalhesItemRomaneioWs RetornaDetalhesItemUsuario(int idPedidoFornecedorItem, string chave, bool atualizarCodigo, int idUsuario)
    {
        var data = new dbCommerceDataContext();
        var usuario = (from c in data.tbUsuarios where c.usuarioId == idUsuario select c).FirstOrDefault();
        string nomeUsuario = "";
        if(usuario != null)
        {
            nomeUsuario = usuario.usuarioNome;
        }
        return RetornaDetalhesItemInterno(idPedidoFornecedorItem, chave, atualizarCodigo, nomeUsuario);

    }

    [WebMethod]
    public DetalhesItemRomaneioWs RetornaDetalhesItem(int idPedidoFornecedorItem, string chave, bool atualizarCodigo)
    {
        return RetornaDetalhesItemInterno(idPedidoFornecedorItem, chave, atualizarCodigo, "");
    }

    [WebMethod]
    public void InsereLogEtiqueta(int idPedidoFornecedorItem, int idUsuario, string descricao)
    {
        var data = new dbCommerceDataContext();
        var usuario = (from c in data.tbUsuarios where c.usuarioId == idUsuario select c).FirstOrDefault();
        rnLog.InsereLogEtiqueta(usuario.usuarioNome, idPedidoFornecedorItem, descricao);
    }

    [WebMethod]
    public void InsereLogEtiquetaPedido(int idPedidoFornecedorItem, int idUsuario, int idPedido, string descricao)
    {
        var data = new dbCommerceDataContext();
        var usuario = (from c in data.tbUsuarios where c.usuarioId == idUsuario select c).FirstOrDefault();
        rnLog.InsereLogEtiqueta(usuario.usuarioNome, idPedidoFornecedorItem, idPedido, descricao);
    }


    [WebMethod]
    public void InsereLogEtiquetaPedidoNome(int idPedidoFornecedorItem, string nome, int idPedido, string descricao)
    {
        rnLog.InsereLogEtiqueta(nome, idPedidoFornecedorItem, idPedido, descricao);
    }

    public DetalhesItemRomaneioWs RetornaDetalhesItemInterno(int idPedidoFornecedorItem, string chave, bool atualizarCodigo, string usuario)
    {
        if (chave != "glmp152029")
        {
            return null;
        }

        rnLog.InsereLogEtiqueta(usuario, idPedidoFornecedorItem, "Etiqueta lida para conferir foto e dar entrada");
        bool atualizar = atualizarCodigo;
        var pedidosDc = new dbCommerceDataContext();
        var pedidoFornecedorItemCheck =
            (from c in pedidosDc.tbPedidoFornecedorItems
             where c.idPedidoFornecedorItem == idPedidoFornecedorItem
             select c).FirstOrDefault();
        if (String.IsNullOrEmpty(pedidoFornecedorItemCheck.codigoProduto)) atualizar = true;
        AtualizaCodigoEtiquetas(pedidoFornecedorItemCheck.idPedidoFornecedor, idPedidoFornecedorItem);
        var pedidosDc2 = new dbCommerceDataContext();
        var pedidoFornecedorItem =
            (from c in pedidosDc2.tbPedidoFornecedorItems
             join d in pedidosDc2.tbPedidoFornecedors on c.idPedidoFornecedor equals d.idPedidoFornecedor
             join e in pedidosDc2.tbProdutoFornecedors on d.idFornecedor equals e.fornecedorId
             where c.idPedidoFornecedorItem == idPedidoFornecedorItem
             select new
             {
                 c.idPedidoFornecedor,
                 c.tbProduto.complementoIdDaEmpresa,
                 e.fornecedorNome,
                 numeroItem = 0,
                 c.tbProduto.fotoDestaque,
                 c.idProduto,
                 c.tbProduto.produtoIdDaEmpresa,
                 c.tbProduto.produtoNome,
                 c.codigoProduto
             }).FirstOrDefault();
        if (pedidoFornecedorItem != null)
        {
            int totalItensRomaneio = (from c in pedidosDc2.tbPedidoFornecedorItems
                where c.idPedidoFornecedor == pedidoFornecedorItem.idPedidoFornecedor
                select c).Count();
            //var detalhes = pedidosDc.admin_pedidoFornecedorDetalhesPorId(pedidoFornecedorItem.idPedidoFornecedor).ToList();
            //var detalheItem = detalhes.First(x => x.idPedidoFornecedorItem == idPedidoFornecedorItem);
            var retorno = new DetalhesItemRomaneioWs();
            retorno.complementoIdDaEmpresa = pedidoFornecedorItem.complementoIdDaEmpresa;
            retorno.fornecedorNome = pedidoFornecedorItem.fornecedorNome;
            retorno.idPedidoFornecedor = pedidoFornecedorItem.idPedidoFornecedor;
            retorno.idPedidoFornecedorItem = idPedidoFornecedorItem;
            retorno.numeroItem = Convert.ToInt32(pedidoFornecedorItem.numeroItem);
            retorno.produtoFoto = pedidoFornecedorItem.fotoDestaque;
            retorno.produtoId = pedidoFornecedorItem.idProduto;
            retorno.produtoIdDaEmpresa = pedidoFornecedorItem.produtoIdDaEmpresa;
            retorno.produtoNome = pedidoFornecedorItem.produtoNome;
            retorno.totalItens = totalItensRomaneio;
            retorno.codigoEtiqueta = pedidoFornecedorItem.codigoProduto;
            return retorno;
        }
        return null;
    }

    [WebMethod]
    public bool ChecaRomaneioImpresso(int idPedidoFornecedor)
    {
        var pedidosDc = new dbCommerceDataContext();
        var pedidoFornecedor =
            (from c in pedidosDc.tbPedidoFornecedors where c.idPedidoFornecedor == idPedidoFornecedor select c)
                .FirstOrDefault();
        if (pedidoFornecedor != null)
        {
            if (pedidoFornecedor.impresso) return true;
        }
        return false;
    }

    [WebMethod]
    public List<DetalhesItemRomaneioWs> ListaDetalhesItemRomaneioUsuario(int idPedidoFornecedor, int inicio, int fim, string chave, bool atualizarCodigo, int idUsuario)
    {
        var data = new dbCommerceDataContext();
        var usuario = (from c in data.tbUsuarios where c.usuarioId == idUsuario select c).FirstOrDefault();
        return ListaDetalhesItemRomaneioInterno(idPedidoFornecedor, inicio, fim, chave, atualizarCodigo, usuario.usuarioNome);
    }

    [WebMethod]
    public List<DetalhesItemRomaneioWs> ListaDetalhesItemRomaneio(int idPedidoFornecedor, int inicio, int fim, string chave, bool atualizarCodigo)
    {
        return ListaDetalhesItemRomaneioInterno(idPedidoFornecedor, inicio, fim, chave, atualizarCodigo, "");
    }

    public List<DetalhesItemRomaneioWs> ListaDetalhesItemRomaneioInterno(int idPedidoFornecedor, int inicio, int fim, string chave, bool atualizarCodigo, string usuario)
    {
        if (chave != "glmp152029")
        {
            return null;
        }
        var detalhesRomaneio = new List<DetalhesItemRomaneioWs>();
        var pedidosDc = new dbCommerceDataContext();
        var pedidoFornecedor =
            (from c in pedidosDc.tbPedidoFornecedors where c.idPedidoFornecedor == idPedidoFornecedor select c)
                .FirstOrDefault();
        if (pedidoFornecedor != null)
        {
            pedidoFornecedor.impresso = true;
            pedidosDc.SubmitChanges();
        }
        if(atualizarCodigo) AtualizaCodigoEtiquetas(idPedidoFornecedor, 0);
        var pedidoFornecedorItens =
            (from c in pedidosDc.tbPedidoFornecedorItems
             where c.idPedidoFornecedor == idPedidoFornecedor && c.entregue == false
             select c).ToList();

        var log = new rnLog();
        log.usuario = usuario;
        log.tiposRelacionados = new List<rnEnums.TipoRelacionadoIds>();
        log.descricoes = new List<string>();
        log.descricoes.Add("Imprimindo etiqueta do pedido via Aplicativo");

        var detalhesGeral = pedidosDc.admin_pedidoFornecedorDetalhesPorId(idPedidoFornecedor).ToList();
        foreach (var tbPedidoFornecedorItem in pedidoFornecedorItens)
        {
            log.tiposRelacionados.Add(new rnEnums.TipoRelacionadoIds() { tipoRelacionado = rnEnums.TipoRegistroRelacionado.Etiqueta, idRegistroRelacionado = tbPedidoFornecedorItem.idPedidoFornecedorItem });
            var detalheItem = detalhesGeral.First(x => x.idPedidoFornecedorItem == tbPedidoFornecedorItem.idPedidoFornecedorItem);
            var retorno = new DetalhesItemRomaneioWs();
            retorno.complementoIdDaEmpresa = detalheItem.complementoIdDaEmpresa;
            retorno.fornecedorNome = detalheItem.fornecedorNome;
            retorno.idPedidoFornecedor = tbPedidoFornecedorItem.idPedidoFornecedor;
            retorno.idPedidoFornecedorItem = tbPedidoFornecedorItem.idPedidoFornecedorItem;
            retorno.numeroItem = Convert.ToInt32(detalheItem.numeroItem);
            retorno.produtoFoto = detalheItem.fotoDestaque;
            retorno.produtoId = detalheItem.idProduto;
            retorno.produtoIdDaEmpresa = detalheItem.produtoIdDaEmpresa;
            retorno.produtoNome = detalheItem.produtoNome;
            retorno.totalItens = Convert.ToInt32(detalhesGeral.OrderByDescending(x => x.numeroItem).First().numeroItem);
            retorno.codigoEtiqueta = tbPedidoFornecedorItem.codigoProduto;

            if (inicio > 0 | fim > 0)
            {
                if (inicio > 0 && fim > 0)
                {
                    if (detalheItem.numeroItem >= inicio && detalheItem.numeroItem <= fim) detalhesRomaneio.Add(retorno);
                }
                else if (inicio > 0 && fim == 0)
                {
                    if (detalheItem.numeroItem >= inicio) detalhesRomaneio.Add(retorno);
                }
                else if (inicio == 0 && fim > 0)
                {
                    if (detalheItem.numeroItem <= fim) detalhesRomaneio.Add(retorno);
                }
            }
            else
            {
                detalhesRomaneio.Add(retorno);
            } 
        }

        log.InsereLog();
        detalhesRomaneio = detalhesRomaneio.OrderByDescending(x => x.numeroItem).ToList();
        return detalhesRomaneio;
    }

    private static void AtualizaCodigoEtiquetas(int idPedidoFornecedor, int idPedidoFornecedorItem)
    {
        var pedidosDc = new dbCommerceDataContext();
        var itensSemCodigo =
            (from c in pedidosDc.tbPedidoFornecedorItems
             where c.idPedidoFornecedor == idPedidoFornecedor && (c.codigoProduto ?? "") == ""
             select c).ToList();
        if (idPedidoFornecedorItem > 0)
        {
            itensSemCodigo = itensSemCodigo.Where(x => x.idPedidoFornecedorItem == idPedidoFornecedorItem).ToList();
        }
        foreach (var itemSemCodigo in itensSemCodigo)
        {
            itemSemCodigo.codigoProduto = itemSemCodigo.idProduto.ToString("x").ToUpper();

            //var fornecedorSigla = itemSemCodigo.tbProduto.tbProdutoFornecedor.codigoEtiqueta;
            //fornecedorSigla = fornecedorSigla.ToUpper();
            //var categoriaSigla = "";
            //var categoriaPrincipal = (from c in pedidosDc.tbJuncaoProdutoCategorias
            //                          where
            //                              c.tbProdutoCategoria.categoriaPaiId == 0 && c.tbProdutoCategoria.exibirSite == true &&
            //                              c.produtoId == itemSemCodigo.idProduto
            //                          select c).FirstOrDefault();
            //if (categoriaPrincipal != null)
            //{
            //    categoriaSigla = categoriaPrincipal.tbProdutoCategoria.codigoEtiqueta;
            //}
            //categoriaSigla = categoriaSigla.ToUpper();
            //var codigoLetra = categoriaSigla + "-" + fornecedorSigla;
            //var codigoAtual = (from c in pedidosDc.tbPedidoFornecedorItems
            //                   where c.idProduto == itemSemCodigo.idProduto && c.codigoProduto.StartsWith(codigoLetra)
            //                   select c).FirstOrDefault();
            //if (codigoAtual != null)
            //{
            //    itemSemCodigo.codigoProduto = codigoAtual.codigoProduto;
            //}
            //else
            //{
            //    var ultimoCodigo = (from c in pedidosDc.tbPedidoFornecedorItems
            //                        where c.codigoProduto.StartsWith(codigoLetra)
            //                        orderby c.codigoProduto descending
            //                        select c).FirstOrDefault();
            //    if (ultimoCodigo != null)
            //    {
            //        var codigoNumerico = Convert.ToInt32(ultimoCodigo.codigoProduto.Replace(codigoLetra, "").Replace("-", ""));
            //        var produtoCodigo = codigoNumerico++;
            //        string codigoValidacao = codigoLetra + "-" + produtoCodigo.ToString();
            //        var validacaoCodigo = (from c in pedidosDc.tbPedidoFornecedorItems
            //                               where c.codigoProduto == codigoValidacao
            //                               orderby c.codigoProduto descending
            //                               select c).FirstOrDefault();
            //        while (validacaoCodigo != null)
            //        {
            //            produtoCodigo = codigoNumerico++;
            //            codigoValidacao = codigoLetra + "-" + produtoCodigo.ToString();
            //            validacaoCodigo = (from c in pedidosDc.tbPedidoFornecedorItems
            //                               where c.codigoProduto == codigoValidacao
            //                               orderby c.codigoProduto descending
            //                               select c).FirstOrDefault();
            //        }
            //        itemSemCodigo.codigoProduto = codigoValidacao;
            //    }
            //    else
            //    {
            //        itemSemCodigo.codigoProduto = codigoLetra + "-" + "1";
            //    }
            //}
            pedidosDc.SubmitChanges();
        }
    }

    [WebMethod]
    public List<admin_pedidoFornecedorDetalhesPorIdResult> ListaProdutosPendentes(int idPedidoFornecedor, string chave)
    {
        if (chave != "glmp152029")
        {
            return null;
        }
        var pedidosDc = new dbCommerceDataContext();
        var detalhes = pedidosDc.admin_pedidoFornecedorDetalhesPorId(idPedidoFornecedor).ToList().Where(x => x.entregue == false).ToList();
        return detalhes;
    }

    [WebMethod]
    public EntradaRomaneioWs RegistrarEntradaRomaneio(int idPedidoFornecedorItem, int idUsuario, string chave)
    {
        if (chave != "glmp152029") return null;

        var pedidosDc = new dbCommerceDataContext();
        var itemPedido =
            (from c in pedidosDc.tbPedidoFornecedorItems
                where c.idPedidoFornecedorItem == idPedidoFornecedorItem
                select c).First();
        if (itemPedido.entregue != true)
        {
            itemPedido.entregue = true;
            itemPedido.dataEntrega = DateTime.Now;
            pedidosDc.SubmitChanges();
            if (itemPedido.idPedido != null)
            {
                rnQueue.AdicionaQueueChecagemPedidoCompleto((int)itemPedido.idPedido);
            }
        }


        var estoqueCheck =
            (from c in pedidosDc.tbProdutoEstoques where c.idPedidoFornecedorItem == idPedidoFornecedorItem select c)
                .Any();

        var retorno = new EntradaRomaneioWs();
        retorno.adicionado = false;
        if (!estoqueCheck)
        {
            var estoque = new tbProdutoEstoque();
            estoque.idPedidoFornecedorItem = idPedidoFornecedorItem;
            estoque.produtoId = itemPedido.idProduto;
            estoque.dataEntrada = DateTime.Now;
            estoque.enviado = false;
            pedidosDc.tbProdutoEstoques.InsertOnSubmit(estoque);
            pedidosDc.SubmitChanges();

            var produtosDc = new dbCommerceDataContext();
            var produto = (from c in produtosDc.tbProdutos where c.produtoId == itemPedido.idProduto select new
            {
                c.produtoNome,
                c.produtoIdDaEmpresa,
                c.complementoIdDaEmpresa
            }).First();
            var interacaoObj = new tbPedidoFornecedorInteracao();
            interacaoObj.data = DateTime.Now;
            interacaoObj.idPedidoFornecedor = itemPedido.idPedidoFornecedor;
            string interacao = "Produto entregue" + "<br>";
            interacao += "ID: " + idPedidoFornecedorItem + "<br>";
            interacao += produto.produtoNome + "<br>";
            interacao += produto.produtoIdDaEmpresa + " - " + produto.complementoIdDaEmpresa;
            interacaoObj.interacao = interacao;

            interacaoObj.usuario = rnUsuarios.usuarioSeleciona_PorUsuarioId(idUsuario).Tables[0].Rows[0]["usuarioNome"].ToString();
            pedidosDc.tbPedidoFornecedorInteracaos.InsertOnSubmit(interacaoObj);
            pedidosDc.SubmitChanges();
            retorno.adicionado = true;
        }
        retorno.totalProdutos =
            (from c in pedidosDc.tbPedidoFornecedorItems
             where c.idPedidoFornecedor == itemPedido.idPedidoFornecedor
             select c).Count();
        retorno.produtosEntregues =
            (from c in pedidosDc.tbPedidoFornecedorItems
             where c.idPedidoFornecedor == itemPedido.idPedidoFornecedor && c.entregue == true
             select c).Count();
        retorno.idPedidoFornecedor = itemPedido.idPedidoFornecedor;
        return retorno;
    }


    [WebMethod]
    public EntradaRomaneioEnderecadoWs RegistrarEntradaRomaneioComEndereco(int idPedidoFornecedorItem, int idUsuario, int idEnderecamentoTransferencia, string chave)
    {
        if (chave != "glmp152029") return null;

        var pedidosDc = new dbCommerceDataContext();

        var itemEstoque =
            (from c in pedidosDc.tbProdutoEstoques where c.idPedidoFornecedorItem == idPedidoFornecedorItem select c)
                .FirstOrDefault();
        var itemPedido =
            (from c in pedidosDc.tbPedidoFornecedorItems
             where c.idPedidoFornecedorItem == idPedidoFornecedorItem
             select c).FirstOrDefault();
        var usuario = (from c in pedidosDc.tbUsuarios where c.usuarioId == idUsuario select c).FirstOrDefault();

        if (itemPedido == null)
        {
            var retornoTransferencia = new EntradaRomaneioEnderecadoWs();
            retornoTransferencia.adicionado = false;
            retornoTransferencia.idPedidoFornecedor = 0;
            retornoTransferencia.produtosEntregues = 0;
            retornoTransferencia.respostaEndereco = "Produto não localizado.";
            return retornoTransferencia;
        }
        if (itemEstoque != null)
        {
            if (itemEstoque.enviado == true)
            {
                if (itemPedido.motivo == "Remocao para transferencia a jacutinga")
                {
                    rnLog.InsereLogEtiqueta(usuario.usuarioNome, idPedidoFornecedorItem, "Entrada de estoque transferido de Jacutinga");
                    itemEstoque.enviado = false;
                    itemEstoque.liberado = false;
                    itemEstoque.pedidoId = null;
                    itemEstoque.idPedidoEnvio = null;
                    itemEstoque.itemPedidoId = null;
                    itemEstoque.idEnderecamentoArea = null;
                    itemEstoque.idEnderecamentoRua = null;
                    itemEstoque.idEnderecamentoPredio = null;
                    itemEstoque.pedidoIdReserva = null;
                    itemEstoque.itemPedidoIdReserva = null;
                    itemEstoque.conferidoEmbalagem = null;
                    itemEstoque.idItemPedidoEstoque = null;
                    itemEstoque.idItemPedidoEstoqueReserva = null;
                    itemEstoque.idCentroDistribuicao = (itemEstoque.tbProduto.tbProdutoFornecedor.idCentroDistribuicao ?? 4);
                    itemPedido.liberado = false;
                    pedidosDc.SubmitChanges();


                    var interacaoObj = new tbPedidoFornecedorInteracao();
                    interacaoObj.data = DateTime.Now;
                    interacaoObj.idPedidoFornecedor = itemPedido.idPedidoFornecedor;
                    string interacao = "Produto entregue" + "<br>";
                    interacao += "ID: " + idPedidoFornecedorItem + "<br>";
                    interacao += itemEstoque.tbProduto.produtoNome + "<br>";
                    interacao += itemEstoque.tbProduto.produtoIdDaEmpresa + " - " + itemEstoque.tbProduto.complementoIdDaEmpresa;
                    interacaoObj.interacao = interacao;

                    interacaoObj.usuario = rnUsuarios.usuarioSeleciona_PorUsuarioId(idUsuario).Tables[0].Rows[0]["usuarioNome"].ToString();
                    pedidosDc.tbPedidoFornecedorInteracaos.InsertOnSubmit(interacaoObj);
                    pedidosDc.SubmitChanges();

                }
                else
                {
                    rnLog.InsereLogEtiqueta(usuario.usuarioNome, idPedidoFornecedorItem, "Tentativa de dar entrada em produto enviado");
                    var retornoTransferencia = new EntradaRomaneioEnderecadoWs();
                    retornoTransferencia.adicionado = false;
                    retornoTransferencia.idPedidoFornecedor = 0;
                    retornoTransferencia.produtosEntregues = 0;
                    retornoTransferencia.respostaEndereco = "Este produto consta como enviado.";
                    return retornoTransferencia;
                }
            }
        }


        /*int idCentroDistribuicao = itemPedido.tbProduto.idCentroDistribuicao;
        if (idCentroDistribuicao == 0) idCentroDistribuicao = 1;
        var centroDistribuicao = (from c in pedidosDc.tbCentroDistribuicaos where c.idCentroDistribuicao == idCentroDistribuicao select c).First();*/

        


        var detalhesCarrinho = (from c in pedidosDc.tbEnderecamentoTransferencias where c.idEnderecamentoTransferencia == idEnderecamentoTransferencia select c).First();
        var carrinhoTranserecia =
            (from c in pedidosDc.tbTransferenciaEnderecos
                where c.idUsuarioExpedicao == null && c.dataFim == null && c.idEnderecamentoTransferencia == idEnderecamentoTransferencia
                select c).FirstOrDefault();

        if (carrinhoTranserecia != null)
        {
            var carrinhoTransferenciaCheck = (from c in pedidosDc.tbTransferenciaEnderecoProdutos
                                                where c.idPedidoFornecedorItem == idPedidoFornecedorItem && c.dataEnderecamento == null
                                                select c).FirstOrDefault();
            if (carrinhoTransferenciaCheck == null | (carrinhoTransferenciaCheck != null && itemPedido.entregue == false))
            {
                if(carrinhoTransferenciaCheck != null)
                {
                    carrinhoTransferenciaCheck.dataEnderecamento = DateTime.Now;
                    pedidosDc.SubmitChanges();
                }
                rnLog.InsereLogEtiqueta(usuario.usuarioNome, idPedidoFornecedorItem, "Entrada de produto no carrinho " + carrinhoTranserecia.tbEnderecamentoTransferencia.enderecamentoTransferencia);
                var enderecoCarrinhoProduto = new tbTransferenciaEnderecoProduto();
                enderecoCarrinhoProduto.idTransferenciaEndereco = carrinhoTranserecia.idTransferenciaEndereco;
                enderecoCarrinhoProduto.idPedidoFornecedorItem = idPedidoFornecedorItem;
                pedidosDc.tbTransferenciaEnderecoProdutos.InsertOnSubmit(enderecoCarrinhoProduto);
                pedidosDc.SubmitChanges();
            }
            else
            {
                rnLog.InsereLogEtiqueta(usuario.usuarioNome, idPedidoFornecedorItem, "Tentativa de entrada de produto que já consta no carrinho " + carrinhoTransferenciaCheck.tbTransferenciaEndereco.tbEnderecamentoTransferencia.enderecamentoTransferencia);
                var retornoTransferencia = new EntradaRomaneioEnderecadoWs();
                retornoTransferencia.adicionado = false;
                retornoTransferencia.idPedidoFornecedor = 0;
                retornoTransferencia.produtosEntregues = 0;
                retornoTransferencia.respostaEndereco = "Este produto está no carrinho " + carrinhoTransferenciaCheck.tbTransferenciaEndereco.tbEnderecamentoTransferencia.enderecamentoTransferencia;
                return retornoTransferencia;
            }
        }
        else
        {

            var carrinhoTransereciaPendente =
                (from c in pedidosDc.tbTransferenciaEnderecos
                    where
                    c.idUsuarioExpedicao != null && c.dataFim == null &&
                    c.idEnderecamentoTransferencia == idEnderecamentoTransferencia
                    select c).FirstOrDefault();
            
            if (carrinhoTransereciaPendente != null)
            {
                var itensPendentes = (from c in pedidosDc.tbTransferenciaEnderecoProdutos where c.idTransferenciaEndereco == carrinhoTransereciaPendente.idTransferenciaEndereco && c.dataEnderecamento == null select c).Any();
                if (itensPendentes)
                {
                    rnLog.InsereLogEtiqueta(usuario.usuarioNome, idPedidoFornecedorItem, "Tentativa de entrada de produto em carrinho " + carrinhoTransereciaPendente.tbEnderecamentoTransferencia.enderecamentoTransferencia + " que está sendo endereçado");
                    var retornoTransferencia = new EntradaRomaneioEnderecadoWs();
                    retornoTransferencia.adicionado = false;
                    retornoTransferencia.idPedidoFornecedor = 0;
                    retornoTransferencia.produtosEntregues = 0;
                    retornoTransferencia.respostaEndereco = "Este local de armazenamento está sendo endereçado por " +
                                                            carrinhoTransereciaPendente.tbUsuarioExpedicao.nome +
                                                            ". Favor selecionar outro local e dar entrada novamente.";
                    return retornoTransferencia;
                }
                else
                {
                    carrinhoTransereciaPendente.dataFim = DateTime.Now;
                    pedidosDc.SubmitChanges();
                }
            }

            var transferenciaPendente = new tbTransferenciaEndereco();
            transferenciaPendente.dataCadastro = DateTime.Now;
            transferenciaPendente.idEnderecamentoTransferencia = idEnderecamentoTransferencia;
            transferenciaPendente.usuarioEntrada =
                rnUsuarios.usuarioSeleciona_PorUsuarioId(idUsuario).Tables[0].Rows[0]["usuarioNome"].ToString();
            pedidosDc.tbTransferenciaEnderecos.InsertOnSubmit(transferenciaPendente);
            pedidosDc.SubmitChanges();

            var enderecoCarrinhoProduto = new tbTransferenciaEnderecoProduto();
            enderecoCarrinhoProduto.idTransferenciaEndereco = transferenciaPendente.idTransferenciaEndereco;
            enderecoCarrinhoProduto.idPedidoFornecedorItem = idPedidoFornecedorItem;
            pedidosDc.tbTransferenciaEnderecoProdutos.InsertOnSubmit(enderecoCarrinhoProduto);
            pedidosDc.SubmitChanges();

            rnLog.InsereLogEtiqueta(usuario.usuarioNome, idPedidoFornecedorItem, "Entrada de produto no carrinho " + transferenciaPendente.tbEnderecamentoTransferencia.enderecamentoTransferencia);
            

        }
        


        if (itemPedido.entregue != true)
        {
            itemPedido.entregue = true;
            itemPedido.dataEntrega = DateTime.Now;
            /*if (centroDistribuicao.enderecamentoAutomatico)
            {
                itemPedido.liberado = true;
            }*/
            pedidosDc.SubmitChanges();
        }
        
        var estoqueCheck =
            (from c in pedidosDc.tbProdutoEstoques where c.idPedidoFornecedorItem == idPedidoFornecedorItem select c)
                .Any();

        var retorno = new EntradaRomaneioEnderecadoWs();
        retorno.adicionado = false;
        if (!estoqueCheck)
        {
            /*var produto = (from c in produtoDc.tbProdutos
                           where c.produtoId == itemPedido.idProduto
                           select new
                           {
                               c.produtoNome,
                               c.produtoIdDaEmpresa,
                               c.complementoIdDaEmpresa,
                               c.tbProdutoFornecedor.idCentroDistribuicao
                           }).First();*/

            var estoque = new tbProdutoEstoque();
            estoque.idPedidoFornecedorItem = idPedidoFornecedorItem;
            estoque.produtoId = itemPedido.idProduto;
            estoque.dataEntrada = DateTime.Now;
            estoque.enviado = false;
            estoque.idCentroDistribuicao = 4;

            /*if (centroDistribuicao.enderecamentoAutomatico)
            {
                estoque.idEnderecamentoArea = centroDistribuicao.idEnderecamentoArea;
                estoque.idEnderecamentoAndar = centroDistribuicao.idEnderecamentoAndar;
                //estoque.idEnderecamentoApartamento = centroDistribuicao.idEnderecamentoApartamento;
                estoque.idEnderecamentoPredio = centroDistribuicao.idEnderecamentoPredio;
                estoque.idEnderecamentoRua = centroDistribuicao.idEnderecamentoRua;
                estoque.liberado = true;
                estoque.dataConferencia = DateTime.Now;
            }*/


            pedidosDc.tbProdutoEstoques.InsertOnSubmit(estoque);
            pedidosDc.SubmitChanges();
            
            
            /*var interacaoObj = new tbPedidoFornecedorInteracao();
            interacaoObj.data = DateTime.Now;
            interacaoObj.idPedidoFornecedor = itemPedido.idPedidoFornecedor;
            string interacao = "Produto entregue" + "<br>";
            interacao += "ID: " + idPedidoFornecedorItem + "<br>";
            interacao += produto.produtoNome + "<br>";
            interacao += produto.produtoIdDaEmpresa + " - " + produto.complementoIdDaEmpresa;
            interacaoObj.interacao = interacao;

            interacaoObj.usuario = rnUsuarios.usuarioSeleciona_PorUsuarioId(idUsuario).Tables[0].Rows[0]["usuarioNome"].ToString();
            pedidosDc.tbPedidoFornecedorInteracaos.InsertOnSubmit(interacaoObj);
            pedidosDc.SubmitChanges();*/
            retorno.adicionado = true;
        }
        if (itemPedido.motivo == "Remocao para transferencia a jacutinga")
        {
            retorno.adicionado = true;
        }


        retorno.totalProdutos =
            (from c in pedidosDc.tbPedidoFornecedorItems
             where c.idPedidoFornecedor == itemPedido.idPedidoFornecedor
             select c).Count();
        retorno.produtosEntregues =
            (from c in pedidosDc.tbPedidoFornecedorItems
             where c.idPedidoFornecedor == itemPedido.idPedidoFornecedor && c.entregue == true
             select c).Count();
        retorno.idPedidoFornecedor = itemPedido.idPedidoFornecedor;
        
        rnQueue.AdicionaQueueReservaEstoque((int)itemPedido.idProduto); 
        return retorno;
    }



    [WebMethod]
    public DetalhesItemRomaneioWs RetornaDetalhesItemChecagemEstoque(int idPedidoFornecedorItem, string chave)
    {
        if (chave != "glmp152029")
        {
            return null;
        }
        var pedidosDc = new dbCommerceDataContext();
        var pedidoFornecedorItem =
            (from c in pedidosDc.tbPedidoFornecedorItems
             where c.idPedidoFornecedorItem == idPedidoFornecedorItem
             select c).FirstOrDefault();
        if (pedidoFornecedorItem != null)
        {
            var produto =
                (from c in pedidosDc.tbProdutos
                    where c.produtoId == pedidoFornecedorItem.idProduto
                 select new
                 {
                     c.produtoNome,
                     c.produtoIdDaEmpresa,
                     c.complementoIdDaEmpresa,
                     c.fotoDestaque,
                     c.produtoId
                 }).First();
            var estoqueItem =
                (from c in pedidosDc.tbProdutoEstoques where c.idPedidoFornecedorItem == pedidoFornecedorItem.idPedidoFornecedorItem select c)
                    .FirstOrDefault();

            var retorno = new DetalhesItemRomaneioWs();
            retorno.complementoIdDaEmpresa = produto.complementoIdDaEmpresa;
            retorno.fornecedorNome = "";
            retorno.idPedidoFornecedor = pedidoFornecedorItem.idPedidoFornecedor;
            retorno.idPedidoFornecedorItem = idPedidoFornecedorItem;
            retorno.numeroItem = 0;
            retorno.produtoFoto = produto.fotoDestaque;
            retorno.produtoId = produto.produtoId;
            retorno.produtoIdDaEmpresa = produto.produtoIdDaEmpresa;
            retorno.produtoNome = produto.produtoNome;
            retorno.totalItens = 0;

            if (estoqueItem == null)
            {
                retorno.complementoIdDaEmpresa = "problema";
            }
            else if (estoqueItem.enviado | estoqueItem.pedidoId != null)
            {
                retorno.complementoIdDaEmpresa = "problema";
            }
            else
            {
                var estoque = pedidosDc.admin_produtoEmEstoque(pedidoFornecedorItem.idProduto).FirstOrDefault();
                if (estoque != null)
                {
                    retorno.complementoIdDaEmpresa = ((int)estoque.estoqueLivre).ToString();
                }
                else
                {
                    retorno.complementoIdDaEmpresa = "0";
                }
            }
            return retorno;
        }
        return null;
    }
    [WebMethod]
    public string ConfirmarChecagemEstoque(int idPedidoFornecedorItem, string chave)
    {
        if (chave != "glmp152029")
        {
            return "";
        }
        var pedidosDc = new dbCommerceDataContext();
        var pedidoFornecedorItem =
            (from c in pedidosDc.tbPedidoFornecedorItems
             where c.idPedidoFornecedorItem == idPedidoFornecedorItem
             select c).FirstOrDefault();
        if (pedidoFornecedorItem != null)
        {

            var data = new dbCommerceDataContext();
            var etiqueta = (from c in data.tbProdutoEstoques where c.idPedidoFornecedorItem == pedidoFornecedorItem.idPedidoFornecedorItem select c).FirstOrDefault();
            etiqueta.enviado = true;
            etiqueta.dataEnvio = DateTime.Now;
            etiqueta.tbPedidoFornecedorItem.motivo = "Produto retirado do estoque para contagem de estoque (sistema).";

            var pedidoItem = new tbPedidoFornecedorItem();
            pedidoItem.idPedidoFornecedor = 8801;
            pedidoItem.idProduto = pedidoFornecedorItem.idProduto;
            pedidoItem.custo = 0;
            pedidoItem.entregue = false;
            pedidoItem.diferenca = 0;
            pedidoItem.brinde = false;
            pedidoItem.quantidade = 1;
            
            data.tbPedidoFornecedorItems.InsertOnSubmit(pedidoItem);
            data.SubmitChanges();

            return "ok";

        }
        return "";
    }


    [WebMethod]
    public List<ListaAguardandoLiberacao> ListaRomaneiosPendentes(string chave)
    {
        if (chave != "glmp152029")
        {
            return null;
        }
        return null;
        var listaRetorno = new List<ListaAguardandoLiberacao>();
        var pedidosDc = new dbCommerceDataContext();
        var detalhes = (from c in pedidosDc.tbPedidoFornecedorItems where (c.liberado ?? false) == false && c.entregue == true select c.idPedidoFornecedor).Distinct().ToList().Take(10);
        var pedidosFornecedor =
            (from c in pedidosDc.tbPedidoFornecedors where detalhes.Contains(c.idPedidoFornecedor) select c).ToList();
        foreach (var pedidoFornecedor in pedidosFornecedor)
        {
            var fornecedor =
                (from c in pedidosDc.tbProdutoFornecedors where c.fornecedorId == pedidoFornecedor.idFornecedor select c)
                    .FirstOrDefault();
            string fornecedorNome = fornecedor == null ? "Grão de Gente" : fornecedor.fornecedorNome;

            var itemRetorno = new ListaAguardandoLiberacao();
            itemRetorno.fornecedorNome = fornecedorNome;
            itemRetorno.idPedidoFornecedor = pedidoFornecedor.idPedidoFornecedor;
            listaRetorno.Add(itemRetorno);
        }
        return listaRetorno;
    }

    [WebMethod]
    public bool LiberarRomaneio(int idPedidoFornecedor, string chave)
    {
        if (chave != "glmp152029")
        {
            return false;
        }
        var pedidosDc = new dbCommerceDataContext();
        var itensAguardandoLiberacao = (from c in pedidosDc.tbPedidoFornecedorItems
                                        where (c.liberado ?? false) == false && c.entregue == true && c.idPedidoFornecedor == idPedidoFornecedor
                                        select c);
        foreach (var pedidoFornecedorItem in itensAguardandoLiberacao)
        {
            pedidoFornecedorItem.liberado = true;
        }
        pedidosDc.SubmitChanges();
        return true;
    }


    [WebMethod]
    public string RetornaNomeEnderecoTransferencia(int idEnderecamentoTransferencia, string chave)
    {
        if (chave != "glmp152029")
        {
            return "";
        }
        var data = new dbCommerceDataContext();
        var endereco = (from c in data.tbEnderecamentoTransferencias
            where c.idEnderecamentoTransferencia == idEnderecamentoTransferencia
            select c).FirstOrDefault();
        if (endereco == null) return "";

        return endereco.enderecamentoTransferencia;
    }

}
