﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;
using Microsoft.Practices.EnterpriseLibrary.Data;

/// <summary>
/// Summary description for rnColecoes
/// </summary>
public class rnColecoes
{
    public static DataSet retornaUltimoId()
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("colecaoSelecionaUltimoId");

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }
}