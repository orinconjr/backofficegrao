﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for rnQueue
/// </summary>
public class rnQueue
{
    public static void AdicionaQueueChecagemPedidoCompleto(int pedidoId)
    {
        var data = new dbCommerceDataContext();
        var queue = new tbQueue();
        queue.agendamento = DateTime.Now;
        queue.andamento = false;
        queue.concluido = false;
        queue.idRelacionado = pedidoId;
        queue.mensagem = "";
        queue.tipoQueue = 8;
        data.tbQueues.InsertOnSubmit(queue);
        data.SubmitChanges();
        data.Dispose();
    }

    public static void AdicionaQueueReservaEstoque(int produtoId)
    {
        var data = new dbCommerceDataContext();
        var queue = new tbQueue();
        queue.agendamento = DateTime.Now;
        queue.andamento = false;
        queue.concluido = false;
        queue.idRelacionado = produtoId;
        queue.mensagem = "";
        queue.tipoQueue = 21;
        data.tbQueues.InsertOnSubmit(queue);
        data.SubmitChanges();
        data.Dispose();
    }
}