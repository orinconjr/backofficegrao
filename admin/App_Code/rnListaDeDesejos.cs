﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Web.Configuration;
using System.Collections;

/// <summary>
/// Summary description for rnListaDeDesejos
/// </summary>
public class rnListaDeDesejos
{
    public static Page page()
    {
        return (Page)HttpContext.Current.Handler;
    }

    public static DataSet listaDeDesejosSeleciona()
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("listaDeDesejosSeleciona");

        db.AddInParameter(dbCommand, "clienteId", DbType.Int32, page().Request.Cookies["cliente"]["clienteId"].ToString());

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static bool listaDeDesejosInclui(int produtoId, int clienteId, int itemQuantidade)
    {
        bool result = false;

        Database db = DatabaseFactory.CreateDatabase();

        DbCommand dbCommand = db.GetStoredProcCommand("listaDeDesejosInclui");

        db.AddInParameter(dbCommand, "produtoId", DbType.Int32, produtoId);
        db.AddInParameter(dbCommand, "clienteId", DbType.Int32, clienteId);
        db.AddInParameter(dbCommand, "itemQuantidade", DbType.Int32, itemQuantidade);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
            DbTransaction transaction = connection.BeginTransaction();

            try
            {
                db.ExecuteNonQuery(dbCommand, transaction);

                transaction.Commit();

                result = true;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
                
            }
            connection.Close();

            return result;
        }
    }

    public static bool listaDeDesejosExclui(int produtoId)
    {
        bool result = false;

        Database db = DatabaseFactory.CreateDatabase();

        DbCommand dbCommand = db.GetStoredProcCommand("listaDeDesejosExclui");

        db.AddInParameter(dbCommand, "produtoId", DbType.Int32, produtoId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
            DbTransaction transaction = connection.BeginTransaction();

            try
            {
                db.ExecuteNonQuery(dbCommand, transaction);

                transaction.Commit();

                result = true;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
                
            }
            connection.Close();

            return result;
        }
    }
}
