﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for rnPagamento
/// </summary>
public class rnPagamento
{
    public static int confirmarPagamento(int idPedidoPagamento, int idOperadorPagamento, string numeroCobranca, string observacao, bool consolidado)
    {
        string interacao = "<b>Pagamento Efetuado</b><br>";
        var data = new dbCommerceDataContext();
        var pagamento = (from c in data.tbPedidoPagamentos where c.idPedidoPagamento == idPedidoPagamento select c).First();

        interacao += "<b>Vencimento:</b> " + pagamento.dataVencimento.ToShortDateString() + "<br>";
        interacao += "<b>Valor:</b> " + pagamento.valor.ToString("C") + "<br>";
        if (pagamento.idOperadorPagamento != idOperadorPagamento)
        {
            var operador = (from c in data.tbOperadorPagamentos where c.idOperadorPagamento == idOperadorPagamento select c).First();
            interacao += "<b>Operador de pagamento alterado de </b> " + pagamento.tbOperadorPagamento.nome + " <b>para</b> " + operador.nome + "<br>";
        }
        interacao += "<b>Observação:</b> " + observacao + "<br>";
        if (consolidado)
        {
            interacao += "<b>Consolidado pelo Sistema</b> " + "<br>";
        }
        if (!string.IsNullOrEmpty(numeroCobranca)) pagamento.numeroCobranca = numeroCobranca;
        if (pagamento.idOperadorPagamento != idOperadorPagamento) pagamento.idOperadorPagamento = idOperadorPagamento;
        pagamento.dataPagamento = DateTime.Now;
        if (!string.IsNullOrEmpty(numeroCobranca) && pagamento.idOperadorPagamento != 1) pagamento.numeroCobranca = numeroCobranca;
        pagamento.consolidado = consolidado;
        pagamento.pago = true;
        pagamento.cancelado = false;
        data.SubmitChanges();

        rnInteracoes.interacaoInclui(pagamento.pedidoId, interacao, rnUsuarios.retornaNomeUsuarioLogado(), "False");
        if (pagamento.recebimentoParcelado)
        {
            for (int i = 1; i <= pagamento.parcelas; i++)
            {
                var pagamentoSecundario = new tbPedidoPagamento();
                pagamentoSecundario.pedidoId = pagamento.pedidoId;
                pagamentoSecundario.pagamentoPrincipal = false;
                pagamentoSecundario.dataVencimento = retornaPrazoPagamentoParcelado(i, (DateTime)pagamento.dataPagamento, pagamento.idOperadorPagamento);
                pagamentoSecundario.valor = pagamento.valorParcela == null ? 0 : (decimal)pagamento.valorParcela;
                pagamentoSecundario.pago = false;
                pagamentoSecundario.numeroCobranca = pagamento.numeroCobranca;
                pagamentoSecundario.recebimentoParcelado = false;
                pagamentoSecundario.idPedidoPagamentoPai = pagamento.idPedidoPagamento;
                pagamentoSecundario.observacao = "";
                pagamentoSecundario.idOperadorPagamento = pagamento.idOperadorPagamento;
                pagamentoSecundario.consolidado = consolidado;
                pagamentoSecundario.parcelas = 1;
                pagamentoSecundario.cancelado = false;
                pagamentoSecundario.condicaoDePagamentoId = pagamento.condicaoDePagamentoId;
                pagamentoSecundario.valorParcela = pagamento.valorParcela;
                pagamentoSecundario.pagamentoNaoAutorizado = false;
                pagamentoSecundario.valorTotal = pagamento.valorParcela;
                pagamentoSecundario.valorDesconto = 0;
                pagamentoSecundario.nomeCartao = "";
                pagamentoSecundario.numeroCartao = "";
                pagamentoSecundario.validadeCartao = "";
                pagamentoSecundario.codSegurancaCartao = "";
                pagamentoSecundario.idContaPagamento = pagamento.idContaPagamento;
                data.tbPedidoPagamentos.InsertOnSubmit(pagamentoSecundario);
                data.SubmitChanges();
            }
        }
        if (pagamento.tbPedido.tipoDePagamentoId == 11)
        {
            rnEmails.enviaEmailPagamentoConfirmadoPlano(pagamento.tbPedido.pedidoId);
        }
        return verificaPagamentoCompleto(pagamento.pedidoId);

    }

    public static bool reativarPagamentoNaoAutorizado(int idPedidoPagamento,  string observacao)
    {
        bool retorno = true;
        try
        {
            string interacao = "<b>Pagamento Reativado</b><br>";

            var data = new dbCommerceDataContext();
            var pagamento = (from c in data.tbPedidoPagamentos where c.idPedidoPagamento == idPedidoPagamento select c).First();

            pagamento.pagamentoNaoAutorizado = false;
            pagamento.cancelado = false;

            tbPedido pedido = (from c in data.tbPedidos where c.pedidoId == pagamento.pedidoId select c).FirstOrDefault();
            if(pedido.statusDoPedido == 7)
            {
                pedido.statusDoPedido = 2;
            }
            interacao += "O pagamento:<b>" + idPedidoPagamento + "</b> que estava com status de não autorizado ";
            interacao += " foi reativado por: <b>" + rnUsuarios.retornaNomeUsuarioLogado() + "</b></br>";
            interacao += "<b>Observação:</b> " + observacao + "<br>";

            data.SubmitChanges();

            rnInteracoes.interacaoInclui(pagamento.pedidoId, interacao, rnUsuarios.retornaNomeUsuarioLogado(), "False");
        }
        catch(Exception ex)
        {
            retorno = false;
        }
        
        return retorno;
    }

    public static void atualizaDescontoMaximoPedido(int pedidoId)
    {
        var data = new dbCommerceDataContext();
        var pedido = (from c in data.tbPedidos where c.pedidoId == pedidoId select c).FirstOrDefault();
        var condicoesPedido = new List<int>();
        condicoesPedido.Add((int)pedido.condDePagamentoId);
        var condicoesPagamentos =
            (from c in data.tbPedidoPagamentos
             where c.pedidoId == pedidoId && c.cancelado == false
             select c.condicaoDePagamentoId);
        condicoesPedido.AddRange(condicoesPagamentos);
        var descontoMaximo = (from c in data.tbCondicoesDePagamentos
                              where condicoesPedido.Contains(c.condicaoId)
                              orderby c.porcentagemDeDesconto descending
                              select c).FirstOrDefault();
        if (descontoMaximo != null)
        {
            var desconto = Convert.ToInt32(descontoMaximo.porcentagemDeDesconto);
            var diaMudancaDescontoBoleto = Convert.ToDateTime("20/01/2017");
            if (pedido.tipoDePagamentoId != 11)
            {
                if ((pedido.dataHoraDoPedido.Value.Date < diaMudancaDescontoBoleto.Date) &&
                    (desconto == 10 | desconto == 15))
                {
                    desconto = 15;
                }
            }

            var valorDescontoMaximo = (pedido.valorDosItens / 100) * desconto;
            if (pedido.valorDoDescontoDoPagamento > valorDescontoMaximo)
            {
                var valorDiferencaDesconto = pedido.valorDoDescontoDoPagamento - valorDescontoMaximo;
                pedido.valorCobrado += valorDiferencaDesconto;
                pedido.valorDoDescontoDoPagamento = valorDescontoMaximo;
                data.SubmitChanges();
            }
        }
    }

    public static int verificaPagamentoCompleto(int pedidoId)
    {
        var data = new dbCommerceDataContext();
        var pedido = (from c in data.tbPedidos where c.pedidoId == pedidoId select c).First();
        var pagamentos = (from c in data.tbPedidoPagamentos where c.pagamentoPrincipal && c.pago && c.cancelado == false && c.pedidoId == pedidoId select c).ToList();

        if (pagamentos.Any() && pedido.statusDoPedido != 5 && pedido.statusDoPedido != 6)// se o pedido já tem pagamento e seu status é diferente de enviado ou cancelado faz a verificação
        {
            var totalPago = pagamentos.Sum(x => x.valor);
            if (pedido.valorCobrado <= (totalPago + Convert.ToDecimal("0,05")))
            {
                return rnPedidos.confirmarPagamentoPedido(pedidoId, rnUsuarios.retornaNomeUsuarioLogado());
            }
            else
            {
                if (pedido.statusDoPedido != 2)
                {
                    pedido.statusDoPedido = 2;
                    data.SubmitChanges();
                    rnInteracoes.interacaoInclui(pedidoId, "Status alterado para Aguardando Confirmação de Pagamento", rnUsuarios.retornaNomeUsuarioLogado(), "False");
                }
            }
        }
        else
        {
            if (pedido.statusDoPedido != 2)
            {
                pedido.statusDoPedido = 2;
                data.SubmitChanges();
                rnInteracoes.interacaoInclui(pedidoId, "Status alterado para Aguardando Confirmação de Pagamento", rnUsuarios.retornaNomeUsuarioLogado(), "False");
            }
        }
        return 99; // não confirmado
    }
    private static DateTime retornaPrazoPagamentoParcelado(int parcela, DateTime dataPagamento, int idOperadorPagamento)
    {
        if (idOperadorPagamento == 2)
        {
            return dataPagamento.AddDays(14);
        }
        else if (idOperadorPagamento == 3)
        {
            return dataPagamento.AddMonths(parcela);
        }
        else if (idOperadorPagamento == 4)
        {
            return dataPagamento.AddMonths(parcela);
        }
        else if (idOperadorPagamento == 5)
        {
            return dataPagamento.AddDays(14);
        }
        else if (idOperadorPagamento == 6)
        {
            return dataPagamento.AddMonths(1);
        }
        else if (idOperadorPagamento == 7)
        {
            return dataPagamento.AddMonths(1);
        }
        else if (idOperadorPagamento == 8)
        {
            return dataPagamento.AddDays(14);
        }
        else
        {
            return dataPagamento;
        }
    }
    public static int defineOperadorPagamento(int condicaoDePagamento)
    {
        if (condicaoDePagamento == 3)
        {
            return 4; // Elavon
        }
        else if (condicaoDePagamento == 4)
        {
            return 1; // Boleto HSBC
        }
        else if (condicaoDePagamento == 5)
        {
            return 4; // Boleto HSBC
        }
        else if (condicaoDePagamento == 6)
        {
            return 3; // Cielo
        }
        else if (condicaoDePagamento == 7)
        {
            return 3; // Cielo
        }
        else if (condicaoDePagamento == 10)
        {
            return 3; // Cielo
        }
        else if (condicaoDePagamento == 11)
        {
            return 3; // Cielo
        }
        else if (condicaoDePagamento == 14)
        {
            return 9; // Rede
        }
        else if (condicaoDePagamento == 17)
        {
            return 2; // Moip
        }
        else if (condicaoDePagamento == 18)
        {
            return 2; // Moip
        }
        else if (condicaoDePagamento == 19)
        {
            return 2; // Moip
        }
        else if (condicaoDePagamento == 20)
        {
            return 2; // Moip
        }
        else if (condicaoDePagamento == 22)
        {
            return 9; // Rede
        }
        else if (condicaoDePagamento == 23)
        {
            return 3; // Cielo
        }
        else if (condicaoDePagamento == 24)
        {
            return 6; // Rakuten
        }
        else if (condicaoDePagamento == 25)
        {
            return 7; // Extra
        }
        else if (condicaoDePagamento == 26)
        {
            return 8; // Mercado Livre
        }
        else if (condicaoDePagamento == 27)
        {
            return 11; // Boleto
        }
        else
        {
            return 11; // Boleto
        }
    }
}