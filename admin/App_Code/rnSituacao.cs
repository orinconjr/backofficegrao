﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Web.Configuration;
using System.Collections;

/// <summary>
/// Summary description for rnSituacao
/// </summary>
public class rnSituacao
{
    public static DataSet situacaoSeleciona()
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("situacaoSeleciona");

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }
    
    public static DataSet situacaoInternaSeleciona()
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("situacaoInternaSeleciona");

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet situacaoSeleciona_PorSituacaoId(int situacaoId)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("situacaoSeleciona_PorSituacaoId");

        db.AddInParameter(dbCommand, "situacaoId", DbType.Int32, situacaoId);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }
}
