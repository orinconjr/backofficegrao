﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Web.Configuration;
using System.Collections;

/// <summary>
/// Summary description for rnFornecedor
/// </summary>
public class rnFornecedor
{
    public static DataSet fornecedorSeleciona_PorFornecedorId(int fornecedorId)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("fornecedorSeleciona_PorFornecedorId");

        db.AddInParameter(dbCommand, "fornecedorId", DbType.Int32, fornecedorId);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }
}
