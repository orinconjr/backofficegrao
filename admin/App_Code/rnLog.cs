﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for rnLog
/// </summary>
public class rnLog
{
    public rnLog()
    {
        descricoes = new List<string>();
        tiposRelacionados = new List<rnEnums.TipoRelacionadoIds>();
        tiposOperacao = new List<rnEnums.TipoOperacao>();
    }

    public string usuario { get; set; }
    public List<string> descricoes { get; set; }
    public List<rnEnums.TipoRelacionadoIds> tiposRelacionados { get; set; }
    public List<rnEnums.TipoOperacao> tiposOperacao { get; set; }
    public void InsereLog()
    {
        try
        {
            string usuarioParam = "";
            string descricaoParam = "";
            string idTiposRelacionadosParam = "";
            string idTiposLogsOperacaoParam = "";

            using (var db = new dbCommerceDataContext())
            {
                //var log = new tbLog();
                //log.data = DateTime.Now;
                //log.usuario = usuario;
                //db.tbLogs.InsertOnSubmit(log);
                //db.SubmitChanges();
                usuarioParam = usuario;

                foreach (var descricao in descricoes)
                {
                    //var logDescricao = new tbLogDescricao();
                    //logDescricao.idLog = log.idLog;
                    //logDescricao.descricao = descricao;
                    //db.tbLogDescricaos.InsertOnSubmit(logDescricao);
                    descricaoParam += descricao + ",";
                }

                foreach (var tipoRelacionado in tiposRelacionados)
                {
                    //var logRegistroRelacionado = new tbLogRegistroRelacionado();
                    //logRegistroRelacionado.idLog = log.idLog;
                    //logRegistroRelacionado.idRegistroRelacionado = tipoRelacionado.idRegistroRelacionado;
                    //logRegistroRelacionado.tipoRegistroRelacionado = (int)tipoRelacionado.tipoRelacionado;
                    //db.tbLogRegistroRelacionados.InsertOnSubmit(logRegistroRelacionado);
                    idTiposRelacionadosParam += tipoRelacionado.idRegistroRelacionado + "," + (int)tipoRelacionado.tipoRelacionado + "|";
                }

                foreach (var tipoOperacao in tiposOperacao)
                {
                    //var logTipo = new tbLogTipo();
                    //logTipo.idLog = log.idLog;
                    //logTipo.tipo = (int)tipoOperacao;
                    //db.tbLogTipos.InsertOnSubmit(logTipo);
                    idTiposLogsOperacaoParam += (int)tipoOperacao + ",";
                }

                InsereLogNovo(usuarioParam, descricaoParam, idTiposRelacionadosParam, idTiposLogsOperacaoParam);
                //db.SubmitChanges();
            }

        }
        catch (Exception ex)
        {
            //TODO Logar erros ao gravar log de erro? Hamn ???
            var err = ex.ToString();
        }
    }

    public static void InsereLogEtiqueta(string usuario, int etiqueta, string descricao)
    {
        InsereLogEtiqueta(usuario, etiqueta, 0, descricao);
    }
    public static void InsereLogEtiqueta(string usuario, int etiqueta, int pedidoId, string descricao)
    {
        var log = new rnLog();
        log.usuario = usuario;
        log.tiposRelacionados = new List<rnEnums.TipoRelacionadoIds>();
        log.tiposRelacionados.Add(new rnEnums.TipoRelacionadoIds() { tipoRelacionado = rnEnums.TipoRegistroRelacionado.Etiqueta, idRegistroRelacionado = etiqueta });
        if(pedidoId > 0) log.tiposRelacionados.Add(new rnEnums.TipoRelacionadoIds() { tipoRelacionado = rnEnums.TipoRegistroRelacionado.Pedido, idRegistroRelacionado = pedidoId });
        log.descricoes = new List<string>();
        log.descricoes.Add(descricao);
        log.InsereLog();
    }

    public void InsereLogNovo(string usuario, string descricao, string idTiposRelacionados, string idTiposLogsOperacao)
    {
        try
        {
            Database db = DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetStoredProcCommand("admin_inserelog");

            var desc = descricao.Substring(0, descricao.Length - 1);
            var tipoRel = idTiposRelacionados.Substring(0, idTiposRelacionados.Length - 1);
            var tipoLog = idTiposLogsOperacao.Substring(0, idTiposLogsOperacao.Length - 1);

            db.AddInParameter(dbCommand, "usuario", System.Data.DbType.String, usuario);
            db.AddInParameter(dbCommand, "descricao", System.Data.DbType.String, desc);
            db.AddInParameter(dbCommand, "idTiposRelacionados", System.Data.DbType.String, tipoRel);
            db.AddInParameter(dbCommand, "idTiposLogsOperacao", System.Data.DbType.String, tipoLog);

            db.ExecuteNonQuery(dbCommand);
            dbCommand.Dispose();
        }
        catch (Exception ex)
        {
            //TODO Logar erros ao gravar log de erro
            var err = ex.ToString();
        }

    }

}