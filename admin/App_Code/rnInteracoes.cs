﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Web.Configuration;

/// <summary>
/// Summary description for rnInteracoes
/// </summary>
public class rnInteracoes
{
    public static DataSet interacoSeleciona_PorPedidoId(int pedidoId)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("interacoSeleciona_PorPedidoId");

        db.AddInParameter(dbCommand, "pedidoId", DbType.Int32, pedidoId);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet interacoSelecionaMeusPedidos_PorPedidoId(int pedidoId)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("interacoSelecionaMeusPedidos_PorPedidoId");

        db.AddInParameter(dbCommand, "pedidoId", DbType.Int32, pedidoId);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static bool interacaoInclui(int pedidoId, string interacao, string usuarioQueInteragiu, string alteracaoDeEstatus)
    {
        bool result = false;

        Database db = DatabaseFactory.CreateDatabase();

        DbCommand dbCommand = db.GetStoredProcCommand("interacaoInclui");

        db.AddInParameter(dbCommand, "pedidoId", DbType.Int32, pedidoId);
        db.AddInParameter(dbCommand, "interacao", DbType.String, interacao);
        db.AddInParameter(dbCommand, "usuarioQueInteragiu", DbType.String, usuarioQueInteragiu);
        db.AddInParameter(dbCommand, "alteracaoDeEstatus", DbType.String, alteracaoDeEstatus);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
            DbTransaction transaction = connection.BeginTransaction();

            try
            {
                db.ExecuteNonQuery(dbCommand, transaction);

                transaction.Commit();

                result = true;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
                
            }
            connection.Close();

            return result;
        }
    }
}
