﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for RegistrodeGuia
/// </summary>

public class RegistrodeGuia
{
    public string SequencialDaGuia { get; set; }
    public string SituacaoDaGuia { get; set; }
    public string UFFavorecida { get; set; }
    public string CodigoDaReceita { get; set; }
    public string TipoDocEmitente { get; set; }
    public string DocumentoEmitente { get; set; }
    public string RazaoSocialEmitente { get; set; }
    public string EnderecoEmitente { get; set; }
    public string MunicipioEmitente { get; set; }
    public string UFEmitente { get; set; }
    public string CEPEmitente { get; set; }
    public string TelefoneEmitente { get; set; }
    public string TipoDocDestinatario { get; set; }
    public string DocumentoDestinatario { get; set; }
    public string MunicipioDestinatario { get; set; }
    public string Produto { get; set; }
    public string NumeroDoDocumentoDeOrigem { get; set; }
    public string Convenio { get; set; }
    public string InformacoesComplementares { get; set; }
    private string DataDeVencimentoString { get; set; }

    public DateTime DataDeVencimento
    {
        get
        {
            return
                Convert.ToDateTime(DataDeVencimentoString.Substring(0, 2) + "/" +
                                   DataDeVencimentoString.Substring(2, 2) + "/" +
                                   DataDeVencimentoString.Substring(4, 4));
        }
    }

    public string DataLimiteDePagamentoString { get; set; }
    public DateTime DataLimiteDePagamento
    {
        get
        {
            if (DataLimiteDePagamentoString != "00000000")
            {
                return
                    Convert.ToDateTime(DataLimiteDePagamentoString.Substring(0, 2) + "/" +
                                       DataLimiteDePagamentoString.Substring(2, 2) + "/" +
                                       DataLimiteDePagamentoString.Substring(4, 4));
            }
            return
                Convert.ToDateTime(DataDeVencimentoString.Substring(0, 2) + "/" +
                                   DataDeVencimentoString.Substring(2, 2) + "/" +
                                   DataDeVencimentoString.Substring(4, 4));
        }
    }
    public string PeriodoDeReferencia { get; set; }
    public string MesAnoDeReferencia { get; set; }
    public string Parcela { get; set; }
    private string ValorPrincipalString { get; set; }

    public decimal ValorPrincipal
    {
        get
        {
            string inteiro = ValorPrincipalString.Substring(0, ValorPrincipalString.Length - 2);
            string pdecimal = ValorPrincipalString.Substring(ValorPrincipalString.Length - 2, 2);
            decimal valor =
                Convert.ToDecimal(inteiro + "," + pdecimal);
            return valor;
        }
    }

    public string AtualizacaoMonetaria { get; set; }
    public string Juros { get; set; }
    public string Multa { get; set; }
    public string RepresentacaooNumerica { get; set; }
    public string CodigoDeBarras { get; set; }
    public string QtdDeVias { get; set; }

    public decimal ValorJuros
    {
        get
        {
            string inteiro = Juros.Substring(0, Juros.Length - 2);
            string pdecimal = Juros.Substring(Juros.Length - 2, 2);
            decimal valor =
                Convert.ToDecimal(inteiro + "," + pdecimal);
            return valor;
        }
    }

    public decimal ValorMulta
    {
        get
        {
            string inteiro = Multa.Substring(0, Multa.Length - 2);
            string pdecimal = Multa.Substring(Multa.Length - 2, 2);
            decimal valor =
                Convert.ToDecimal(inteiro + "," + pdecimal);
            return valor;
        }
    }

    public string NumeroControle
    {
        get
        {
            string controle = CodigoDeBarras.Substring(26, 16);
            return controle;
        }
    }

    public RegistrodeGuia(string guia)
    {
        if (guia.StartsWith("1"))
        {
            SequencialDaGuia = guia.Substring(1, 4);
            SituacaoDaGuia = guia.Substring(5, 1);
            UFFavorecida = guia.Substring(6, 2);
            CodigoDaReceita = guia.Substring(8, 6);
            TipoDocEmitente = guia.Substring(14, 1);
            DocumentoEmitente = guia.Substring(15, 16);
            RazaoSocialEmitente = guia.Substring(31, 60);
            EnderecoEmitente = guia.Substring(91, 60);
            MunicipioEmitente = guia.Substring(151, 50);
            UFEmitente = guia.Substring(201, 2);
            CEPEmitente = guia.Substring(203, 8);
            TelefoneEmitente = guia.Substring(211, 11);
            TipoDocDestinatario = guia.Substring(221, 1);
            DocumentoDestinatario = guia.Substring(223, 16);
            MunicipioDestinatario = guia.Substring(239, 50);
            Produto = guia.Substring(289, 255);
            NumeroDoDocumentoDeOrigem = guia.Substring(544, 18);
            Convenio = guia.Substring(562, 30);
            InformacoesComplementares = guia.Substring(592, 300);
            DataDeVencimentoString = guia.Substring(892, 8);
            DataLimiteDePagamentoString = guia.Substring(900, 8);
            PeriodoDeReferencia = guia.Substring(908, 1);
            MesAnoDeReferencia = guia.Substring(909, 6);
            Parcela = guia.Substring(915, 3);
            ValorPrincipalString = guia.Substring(918, 15);
            AtualizacaoMonetaria = guia.Substring(933, 15);
            Juros = guia.Substring(948, 15);
            Multa = guia.Substring(963, 15);
            RepresentacaooNumerica = guia.Substring(978, 48);
            CodigoDeBarras = guia.Substring(1026, 44);
            QtdDeVias = guia.Substring(1070, 1);
        }
    }
}