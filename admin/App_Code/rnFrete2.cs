﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for rnFrete2
/// </summary>
public sealed class rnFrete2
{

    private static readonly rnFrete2 instance = new rnFrete2();
    public List<tbTipoDeEntrega> tiposDeEntrega = new List<tbTipoDeEntrega>();
    public List<faixasDeCepFormatadas> faixasDeCep = new List<faixasDeCepFormatadas>();
    public List<tbTipoDeEntregaTabelaPreco> tabelasDePreco = new List<tbTipoDeEntregaTabelaPreco>();
    public List<tbTipoDeEntregaTabelaPrecoValor> tabelasDePrecoValor = new List<tbTipoDeEntregaTabelaPrecoValor>();
    public List<tbTipoDeEntregaTarifa> tarifas = new List<tbTipoDeEntregaTarifa>();
    public List<tbFaixaDeCepTarifa> faixaTarifas = new List<tbFaixaDeCepTarifa>();
    




    public class faixasDeCepFormatadas
    {
        public int idFaixaDeCep { get; set; }
        public int idTipoDeEntrega { get; set; }
        public long faixaInicial { get; set; }
        public long faixaFinal { get; set; }
        public int idTipoDeEntregaTabelaPreco { get; set; }
        public int prazo { get; set; }
        public bool interior { get; set; }
    }
    private rnFrete2()
    {
        using (var data = new dbCommerceDataContext())
        {
            tiposDeEntrega = (from c in data.tbTipoDeEntregas select c).ToList();
            tabelasDePreco = (from c in data.tbTipoDeEntregaTabelaPrecos select c).ToList();
            tabelasDePrecoValor = (from c in data.tbTipoDeEntregaTabelaPrecoValors select c).ToList();
            tarifas = (from c in data.tbTipoDeEntregaTarifas select c).ToList();
            faixaTarifas = (from c in data.tbFaixaDeCepTarifas select c).ToList();
            faixasDeCep = (from c in data.tbFaixaDeCeps
                               join d in data.tbTipoDeEntregaTabelaPrecos on c.idTipoDeEntregaTabelaPreco equals d.idTipoDeEntregaTabelaPreco
                               where (c.ativo ?? true) == true
                               select new faixasDeCepFormatadas
                               {
                                   faixaFinal = Convert.ToInt64(c.faixaFinal.Replace("-", "")),
                                   faixaInicial = Convert.ToInt64(c.faixaInicial.Replace("-", "")),
                                   idFaixaDeCep = c.faixaDeCepId,
                                   idTipoDeEntregaTabelaPreco = c.idTipoDeEntregaTabelaPreco ?? 0,
                                   idTipoDeEntrega = c.tipodeEntregaId,
                                   prazo = c.prazo,
                                   interior = d.nome.ToLower().Contains("interior")
                           }).ToList();
            
        }
    }

    static rnFrete2()
    {

    }

    public static rnFrete2 Instance
    {
        get
        {
            return instance;
        }
    }


    public RetornoCalculoFrete CalculaFrete(int idTipoDeEntrega, int peso, string cep, decimal valorPedido, decimal largura, decimal altura, decimal profundidade)
    {
        var pacotes = new List<Pacotes>();
        var pacote = new Pacotes()
        {
            altura = altura,
            comprimento = profundidade,
            largura = largura,
            peso = peso
        };
        pacotes.Add(pacote);

        return CalculaFrete(idTipoDeEntrega, cep, valorPedido, pacotes);

    }

    public RetornoCalculoFrete CalculaFrete(int idTipoDeEntrega, string cep, decimal valorPedido, List<Pacotes> pacotes)
    {
        return CalculaFrete(idTipoDeEntrega, cep, valorPedido, pacotes, new List<Pacotes>());
    }

    public RetornoCalculoFrete CalculaFrete(int idTipoDeEntrega, string cep, decimal valorPedido, List<Pacotes> pacotes, List<Pacotes> produtos)
    {
        List<string> detalhesValor = new List<string>();

        long cepFormatado = 0;
        Int64.TryParse(cep.Replace("-", "").Trim(), out cepFormatado);
        var retorno = new RetornoCalculoFrete() { valor = 0, prazo = 0 };
        //var data = new dbCommerceDataContext();
        var tipoDeEntrega = (from c in tiposDeEntrega where c.tipoDeEntregaId == idTipoDeEntrega select c).FirstOrDefault();
        if (tipoDeEntrega == null)
        {
            return retorno;
        }

        if (tipoDeEntrega.limiteMaiorDimensao > 0)
        {
            if (pacotes.Any(x => x.altura > tipoDeEntrega.limiteMaiorDimensao | x.largura > tipoDeEntrega.limiteMaiorDimensao | x.comprimento > tipoDeEntrega.limiteMaiorDimensao))
            {
                return retorno;
            }
            if (produtos.Any(x => x.altura > tipoDeEntrega.limiteMaiorDimensao | x.largura > tipoDeEntrega.limiteMaiorDimensao | x.comprimento > tipoDeEntrega.limiteMaiorDimensao))
            {
                return retorno;
            }
        }

        if (tipoDeEntrega.limiteSomaDimensoes > 0)
        {
            if (pacotes.Any(x => (x.altura + x.largura + x.comprimento) > tipoDeEntrega.limiteSomaDimensoes))
            {
                return retorno;
            }
            if (produtos.Any(x => (x.altura + x.largura + x.comprimento) > tipoDeEntrega.limiteSomaDimensoes))
            {
                return retorno;
            }
        }

        int pesoTotal = pacotes.Sum(x => x.peso);

        if (tipoDeEntrega.idTipoEntregaTipoTabela == 2)
        {
            var faixa = retornaFaixaDeCep(cepFormatado, idTipoDeEntrega);
            if (faixa == null)
            {
                var faixasTotal1 = (from c in faixasDeCep
                                    where c.idTipoDeEntrega == tipoDeEntrega.tipoDeEntregaId && c.interior
                                    select new
                                    {
                                        faixaInicial = c.faixaInicial,
                                        faixaFinal = c.faixaFinal
                                    }).ToList();


                var faixasTotalInicial = faixasTotal1.Select(x => Convert.ToInt64(x.faixaInicial)).ToList();
                var faixasTotalFinal = faixasTotal1.Select(x => Convert.ToInt64(x.faixaFinal)).ToList();
                var proximaFaixa = (from c in faixasTotalInicial where c > cepFormatado orderby c select c).FirstOrDefault();
                var faixaAnterior = (from c in faixasTotalFinal where c < cepFormatado orderby c descending select c).FirstOrDefault();

                if (proximaFaixa > 0 && faixaAnterior > 0)
                {
                    var compararProximaFaixa = proximaFaixa - cepFormatado;
                    var compararFaixaAnterior = cepFormatado - faixaAnterior;
                    if (tipoDeEntrega.cepAproximado == true)
                    {
                        if (compararProximaFaixa < compararFaixaAnterior)
                            faixa = retornaFaixaDeCep(proximaFaixa, idTipoDeEntrega);
                        else
                            faixa = retornaFaixaDeCep(faixaAnterior, idTipoDeEntrega);

                        try
                        {
                            // resolveu com próxima faixa, mas o cep pesquisado não existe
                            //rnEmails.EnviaEmail("atendimento@graodegente.com.br", "renato@bark.com.br", "andre@bark.com.br,danny@bark.com.br", "", "", "O cep: " + cepFormatado + "</br>" + "Não foi encontrado no banco de dados, favor incluí-lo para melhor precisão nos calculos de prazos e valores de frete.</br>Tipo de entrega Id: " + idTipoDeEntrega, "CEP não encontrado no banco de dados");
                        }
                        catch (Exception) { }
                    }
                }
                else
                {
                    try
                    {
                        //rnEmails.EnviaEmail("atendimento@graodegente.com.br", "renato@bark.com.br", "andre@bark.com.br,danny@bark.com.br", "", "", "O cep: " + cepFormatado + "</br>" + "Não foi encontrado no banco de dados, favor incluí-lo para melhor precisão nos calculos de prazos e valores de frete.</br>Tipo de entrega Id: " + idTipoDeEntrega, "CEP não encontrado no banco de dados");
                    }
                    catch (Exception) { }

                    //if (proximaFaixa != null) faixa = data.site_CepFaixaPorTipoDeEntrega(proximaFaixa, idTipoDeEntrega).FirstOrDefault();
                }
            }
            if (faixa != null)
            {
                if (faixa.idTipoDeEntregaTabelaPreco != null)
                {
                    decimal valorCalculado = 0;
                    var tabela = (from c in tabelasDePreco
                                  where c.idTipoDeEntregaTabelaPreco == faixa.idTipoDeEntregaTabelaPreco
                                  select c).FirstOrDefault();
                    if (tabela != null)
                    {
                        int pesoFinal = pesoTotal;
                        if (tipoDeEntrega.cubagem)
                        {
                            int fatorCubagem = tipoDeEntrega.divisaoCubagem;
                            if (tabela.fatorCubagem > 0)
                            {
                                fatorCubagem = (int)tabela.fatorCubagem;
                            }

                            decimal pesoTotalCubagem = 0;
                            foreach (var volume in pacotes)
                            {
                                var cubagem = (Convert.ToDecimal(volume.altura * volume.largura * volume.comprimento) / fatorCubagem);
                                if (cubagem > 10 && cubagem > (Convert.ToDecimal(volume.peso) / 1000))
                                {
                                    pesoTotalCubagem += cubagem;
                                }
                                else
                                {
                                    pesoTotalCubagem += (Convert.ToDecimal(volume.peso) / 1000);
                                }
                            }
                            pesoFinal = Convert.ToInt32(pesoTotalCubagem * 1000);
                        }

                        retorno.pesoAferido = pesoFinal;
                        var tabelaPrecos = (from c in tabelasDePrecoValor
                                            where c.idTipoDeEntregaTabelaPreco == faixa.idTipoDeEntregaTabelaPreco
                                            select c).ToList();
                        var valorFaixa = (from c in tabelaPrecos where pesoFinal > c.pesoInicial && pesoFinal <= c.pesoFinal select c).FirstOrDefault();
                        if (valorFaixa != null)
                        {
                            valorCalculado = valorFaixa.valor;
                            detalhesValor.Add("Valor da faixa: " + valorCalculado.ToString("C"));
                        }
                        else
                        {
                            if (tipoDeEntrega.pesoAdicionalSomaPesoMaximo)
                            {
                                var pesoMaximo = tabelaPrecos.OrderByDescending(x => x.pesoFinal).FirstOrDefault();
                                if (pesoMaximo != null)
                                {
                                    valorCalculado = pesoMaximo.valor;
                                    var diferencaPeso = Convert.ToInt32(Convert.ToDecimal(pesoFinal - pesoMaximo.pesoFinal) / 1000);
                                    valorCalculado += diferencaPeso * tabela.valorKgAdicional;
                                }
                                else
                                {
                                    valorCalculado = Convert.ToInt32(Convert.ToDecimal(pesoFinal) / 1000) * tabela.valorKgAdicional;
                                }
                            }
                            else
                            {
                                valorCalculado = Convert.ToInt32(Convert.ToDecimal(pesoFinal) / 1000) * tabela.valorKgAdicional;
                                detalhesValor.Add("Valor da faixa com peso adicional: " + valorCalculado.ToString("C"));
                            }
                        }

                        if (tabela.valorSeguro > 0 && valorPedido > 0)
                        {
                            valorCalculado += (valorPedido / 100) * tabela.valorSeguro;
                            detalhesValor.Add("Valor com seguro: " + valorCalculado.ToString("C"));
                            detalhesValor.Add("Valor do seguro: " + ((valorPedido / 100) * tabela.valorSeguro).ToString("C"));
                        }


                        var tarifasObrigatorias = (from c in tarifas where c.idTipoDeEntrega == idTipoDeEntrega && c.taxaObrigatoria == true && c.valorSobreFrete == false select c);
                        foreach (var tarifa in tarifasObrigatorias)
                        {
                            var valorTarifa = (valorPedido / 100) * tarifa.percentual;
                            if (valorTarifa < tarifa.valor) valorTarifa = tarifa.valor;
                            valorCalculado += valorTarifa;
                            detalhesValor.Add("Valor com tarifa " + tarifa.nome + ": " + valorCalculado.ToString("C"));
                            detalhesValor.Add("Valor da tarifa " + tarifa.nome + ": " + valorTarifa.ToString("C"));
                        }


                        var tarifasFaixaNota = (from c in faixaTarifas
                                                join d in tarifas on c.idTipoDeEntregaTarifa equals d.idTipoDeEntregaTarifa
                                                where c.idFaixaDeCep == faixa.idFaixaDeCep && d.valorSobreFrete == false select c);
                        foreach (var tarifa in tarifasFaixaNota)
                        {
                            var valorTarifa = (valorPedido / 100) * tarifa.tbTipoDeEntregaTarifa.percentual;
                            if (valorTarifa < tarifa.tbTipoDeEntregaTarifa.valor) valorTarifa = tarifa.tbTipoDeEntregaTarifa.valor;
                            valorCalculado += valorTarifa;
                            detalhesValor.Add("Valor com tarifa " + tarifa.tbTipoDeEntregaTarifa.nome + ": " + valorCalculado.ToString("C"));
                            detalhesValor.Add("Valor da tarifa " + tarifa.tbTipoDeEntregaTarifa.nome + ": " + valorTarifa.ToString("C"));
                        }


                        var tarifasSobreFrete = (from c in faixaTarifas
                                                 join d in tarifas on c.idTipoDeEntregaTarifa equals d.idTipoDeEntregaTarifa
                                                 where c.idFaixaDeCep == faixa.idFaixaDeCep && d.valorSobreFrete == true select c);
                        foreach (var tarifa in tarifasSobreFrete)
                        {
                            if (tarifa.tbTipoDeEntregaTarifa.percentual > 0)
                            {
                                if (tarifa.tbTipoDeEntregaTarifa.valorSobreFreteDivisao)
                                {
                                    var valorTarifa = valorCalculado / tarifa.tbTipoDeEntregaTarifa.percentual;
                                    if (valorTarifa > valorCalculado) valorCalculado = valorTarifa;
                                    detalhesValor.Add("Valor com tarifa " + tarifa.tbTipoDeEntregaTarifa.nome + ": " + valorCalculado.ToString("C"));
                                    detalhesValor.Add("Valor da tarifa " + tarifa.tbTipoDeEntregaTarifa.nome + ": " + valorTarifa.ToString("C"));
                                }
                                else
                                {
                                    var valorTarifa = (valorCalculado / 100) * tarifa.tbTipoDeEntregaTarifa.percentual;
                                    if (valorTarifa < tarifa.tbTipoDeEntregaTarifa.valor) valorTarifa = tarifa.tbTipoDeEntregaTarifa.valor;
                                    valorCalculado += valorTarifa;
                                    detalhesValor.Add("Valor com tarifa " + tarifa.tbTipoDeEntregaTarifa.nome + ": " + valorCalculado.ToString("C"));
                                    detalhesValor.Add("Valor da tarifa " + tarifa.tbTipoDeEntregaTarifa.nome + ": " + valorTarifa.ToString("C"));
                                }
                            }
                        }


                        var tarifasObrigatoriasSobreFrete = (from c in tarifas where c.idTipoDeEntrega == idTipoDeEntrega && c.taxaObrigatoria == true && c.valorSobreFrete == true select c);
                        foreach (var tarifa in tarifasObrigatoriasSobreFrete)
                        {
                            if (tarifa.valorSobreFreteDivisao)
                            {
                                var valorTarifa = valorCalculado / tarifa.percentual;
                                if (valorTarifa > valorCalculado) valorCalculado = valorTarifa;
                                detalhesValor.Add("Valor com tarifa " + tarifa.nome + ": " + valorCalculado.ToString("C"));
                                detalhesValor.Add("Valor da tarifa " + tarifa.nome + ": " + valorTarifa.ToString("C"));
                            }
                            else
                            {
                                var valorTarifa = (valorCalculado / 100) * tarifa.percentual;
                                if (valorTarifa < tarifa.valor) valorTarifa = tarifa.valor;
                                valorCalculado += valorTarifa;
                                detalhesValor.Add("Valor com tarifa " + tarifa.nome + ": " + valorCalculado.ToString("C"));
                                detalhesValor.Add("Valor da tarifa " + tarifa.nome + ": " + valorTarifa.ToString("C"));
                            }
                        }

                        retorno.valor = valorCalculado;
                        retorno.prazo = faixa.prazo;
                        retorno.detalhamento = detalhesValor;
                    }
                }
            }
        }
        return retorno;
    }

    private faixasDeCepFormatadas retornaFaixaDeCep(decimal cep, int tipoDeEntrega)
    {
        return (from c in faixasDeCep where (cep >= c.faixaInicial && cep  <= c.faixaFinal) && c.idTipoDeEntrega == tipoDeEntrega select c).FirstOrDefault();
    }
    public class RetornoCalculoFrete
    {
        public decimal valor { get; set; }
        public int prazo { get; set; }
        public List<string> detalhamento { get; set; }
        public int pesoAferido { get; set; }
    }

    public class Pacotes
    {
        public int peso { get; set; }
        public decimal largura { get; set; }
        public decimal altura { get; set; }
        public decimal comprimento { get; set; }
    }

}