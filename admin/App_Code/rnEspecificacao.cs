﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Web.Configuration;

/// <summary>
/// Summary description for rnEspecificacao
/// </summary>
public class rnEspecificacao
{
    public static DataSet especificacaoSeleciona()
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("especificacaoSeleciona");

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet especificacaoSelecionaPorEspecificacaoId(int especificacaoId)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("especificacaoSelecionaPorEspecificacaoId");

        db.AddInParameter(dbCommand, "especificacaoId", DbType.Int32, especificacaoId);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet produtoEspecificacoesItensSeleciona(int produtoPaiId, int especificacaoId)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("produtoEspecificacoesItensSeleciona");

        db.AddInParameter(dbCommand, "produtoPaiId", DbType.Int32, produtoPaiId);
        db.AddInParameter(dbCommand, "especificacaoId", DbType.Int32, especificacaoId);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet produtoItemEspecificacaoSeleciona_PorProdutoIdEspecificacaoId(int produtoId, int especificacaoId)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("produtoItemEspecificacaoSeleciona_PorProdutoIdEspecificacaoId");

        db.AddInParameter(dbCommand, "produtoId", DbType.Int32, produtoId);
        db.AddInParameter(dbCommand, "especificacaoId", DbType.Int32, especificacaoId);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet especificacaoItemSeleciona_PorProdutoId(int produtoId)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("especificacaoItemSeleciona_PorProdutoId");

        db.AddInParameter(dbCommand, "produtoId", DbType.Int32, produtoId);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }
    
    public static DataSet juncaoProdutoEspecificacaoSeleciona_PorProdutoId(int produtoId)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("juncaoProdutoEspecificacaoSeleciona_PorProdutoId");

        db.AddInParameter(dbCommand, "produtoId", DbType.Int32, produtoId);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet especificacaoItemSeleciona_PorEspecificacaoId(int especificacaoId)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("especificacaoItemSeleciona_PorEspecificacaoId");

        db.AddInParameter(dbCommand, "especificacaoId", DbType.Int32, especificacaoId);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet produtoEspecificacaoItemSeleciona_PorProdutoPaiId_E_ProdutoId_E_EspecificacaoId(int produtoId, int produtoPaiId, int especificacaoId)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("produtoEspecificacaoItemSeleciona_PorProdutoPaiId_E_ProdutoId_E_EspecificacaoId");

        db.AddInParameter(dbCommand, "produtoId", DbType.Int32, produtoId);
        db.AddInParameter(dbCommand, "produtoPaiId", DbType.Int32, produtoPaiId);
        db.AddInParameter(dbCommand, "especificacaoId", DbType.Int32, especificacaoId);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static bool juncaoProdutoEspecificacaoInclui(int produtoId, int especificacaoId)
    {
        bool result = false;

        Database db = DatabaseFactory.CreateDatabase();

        DbCommand dbCommand = db.GetStoredProcCommand("juncaoProdutoEspecificacaoInclui");

        db.AddInParameter(dbCommand, "produtoId", DbType.Int32, produtoId);
        db.AddInParameter(dbCommand, "especificacaoId", DbType.Int32, especificacaoId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
            DbTransaction transaction = connection.BeginTransaction();

            try
            {
                db.ExecuteNonQuery(dbCommand, transaction);

                transaction.Commit();

                result = true;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
                
            }
            connection.Close();

            return result;
        }
    }

    public static bool produtoItensEspecificacaoInclui(int produtoId, int produtoPaiId, int especificacaoId, int produtoItemEspecificacaoId)
    {
        bool result = false;

        Database db = DatabaseFactory.CreateDatabase();

        DbCommand dbCommand = db.GetStoredProcCommand("produtoItensEspecificacaoInclui");

        db.AddInParameter(dbCommand, "produtoId", DbType.Int32, produtoId);
        db.AddInParameter(dbCommand, "produtoPaiId", DbType.Int32, produtoPaiId);
        db.AddInParameter(dbCommand, "especificacaoId", DbType.Int32, especificacaoId);
        db.AddInParameter(dbCommand, "produtoItemEspecificacaoId", DbType.Int32, produtoItemEspecificacaoId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
            DbTransaction transaction = connection.BeginTransaction();

            try
            {
                db.ExecuteNonQuery(dbCommand, transaction);

                transaction.Commit();

                result = true;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
                
            }
            connection.Close();

            return result;
        }
    }

    public static bool produtoItensEspecificacaoExclui(int produtoId)
    {
        bool result = false;

        Database db = DatabaseFactory.CreateDatabase();

        DbCommand dbCommand = db.GetStoredProcCommand("produtoItensEspecificacaoExclui");

        db.AddInParameter(dbCommand, "produtoId", DbType.Int32, produtoId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
            DbTransaction transaction = connection.BeginTransaction();

            try
            {
                db.ExecuteNonQuery(dbCommand, transaction);

                transaction.Commit();

                result = true;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
                
            }
            connection.Close();

            return result;
        }
    }
}
