﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Web.Configuration;
using System.Collections;

/// <summary>
/// Summary description for rnCidades
/// </summary>
public class rnCidades
{
    public static DataSet cidadeSeleciona_PorEstadoId(int estadoId)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("cidadeSeleciona_PorEstadoId");

        db.AddInParameter(dbCommand, "estadoId", DbType.Int32, estadoId);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }
}
