﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Web.Configuration;

/// <summary>
/// Summary description for rnComentarios
/// </summary>
public class rnComentarios
{
    public static DataSet comentarioSeleciona_PorProdutoId(int produtoId)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("comentarioSeleciona_PorProdutoId");

        db.AddInParameter(dbCommand, "@produtoId", DbType.Int32, produtoId);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet comentarioSelecionaPorComentarioId(int comentarioId)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("comentarioSelecionaPorComentarioId");

        db.AddInParameter(dbCommand, "@produtoId", DbType.Int32, comentarioId);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet comentarioSeleciona()
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("comentarioSeleciona");


        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static bool comentarioInclui(int produtoId, string comentaristaNome, string comentaristaEmail, string comentaristaCidade, string comentario)
    {
        bool result = false;

        Database db = DatabaseFactory.CreateDatabase();

        DbCommand dbCommand = db.GetStoredProcCommand("comentarioInclui");

        db.AddInParameter(dbCommand, "produtoId", DbType.Int32, produtoId);
        db.AddInParameter(dbCommand, "comentaristaNome", DbType.String, comentaristaNome);
        db.AddInParameter(dbCommand, "comentaristaEmail", DbType.String, comentaristaEmail);
        db.AddInParameter(dbCommand, "comentaristaCidade", DbType.String, comentaristaCidade);
        db.AddInParameter(dbCommand, "comentario", DbType.String, comentario);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
            DbTransaction transaction = connection.BeginTransaction();

            try
            {
                db.ExecuteNonQuery(dbCommand, transaction);

                transaction.Commit();

                result = true;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
                
            }
            connection.Close();

            return result;
        }
    }
}
