﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Web.Configuration;
using System.Collections;
using System.IO;

/// <summary>
/// Summary description for rnCategorias
/// </summary>
public class rnCategorias
{
    public static DataSet categoriasSelecionaNivelPai()
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("categoriasSelecionaNivelPai");

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }
    public static DataSet categoriasSelecionaNivelPaiPorSiteId(int idSite)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("categoriasSelecionaNivelPaiPorSiteId");
        db.AddInParameter(dbCommand, "idSite", DbType.Int32, idSite);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet categoriasSelecionaNivelPaiOcultos()
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("categoriasSelecionaNivelPaiOcultos");

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet categoriasSeleciona()
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("categoriasSeleciona");

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet categoriasSelecionaNivelFilho(int categoriaPaiId)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("categoriasSelecionaNivelFilho");

        db.AddInParameter(dbCommand, "categoriaPaiId", DbType.Int32, categoriaPaiId);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet produtoCategoriaSeleciona_PorCategoriaId(int categoriaId)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("produtoCategoriaSeleciona_PorCategoriaId");

        db.AddInParameter(dbCommand, "categoriaId", DbType.Int32, categoriaId);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet categoriasGeraXML()
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("categoriasGeraXML");

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }
    
    public static DataSet juncaoProdutoCategoriaSeleciona_PorProdutoId(int produtoId)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("juncaoProdutoCategoriaSeleciona_PorProdutoId");

        db.AddInParameter(dbCommand, "produtoId", DbType.Int32, produtoId);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet juncaoProdutoCategoriaSeleciona_PorCategoriaId(int categoriaId, int produtoId)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("juncaoProdutoCategoriaSeleciona_PorCategoriaId");

        db.AddInParameter(dbCommand, "categoriaId", DbType.Int32, categoriaId);
        db.AddInParameter(dbCommand, "produtoId", DbType.Int32, produtoId);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet retornaUltimoId()
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("categoriaSelecionaUltimoId");

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet contaCategoriasFilho(int categoriaPaiId)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("contaCategoriasFilho");

        db.AddInParameter(dbCommand, "categoriaPaiId", DbType.Int32, categoriaPaiId);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static bool excluiFoto(int categoriaId, string imagem)
    {
        if (File.Exists(ConfigurationManager.AppSettings["caminhoFisico"] + "categorias\\" + categoriaId + "\\" + imagem))
        {
            var db = new dbCommerceDataContext();
            var consulta = (from categoria in db.tbProdutoCategorias
                            where categoria.categoriaId == categoriaId
                            select categoria).Single();

            if (imagem == "imagem1.jpg")
                consulta.imagem1 = "";

            if (imagem == "imagem2.jpg")
                consulta.imagem2 = "";

            if (imagem == "imagem3.jpg")
                consulta.imagem3 = "";

            db.SubmitChanges();

            File.Delete(ConfigurationManager.AppSettings["caminhoFisico"] + "categorias\\" + categoriaId + "\\" + imagem);

            return true;
        }
        else
            return false;
    }

    public static bool juncaoProdutoCategoriaInclui(int produtoId, int categoriaId, int produtoPaiId)
    {
        bool result = false;

        Database db = DatabaseFactory.CreateDatabase();

        DbCommand dbCommand = db.GetStoredProcCommand("juncaoProdutoCategoriaInclui");

        db.AddInParameter(dbCommand, "produtoId", DbType.Int32, produtoId);
        db.AddInParameter(dbCommand, "categoriaId", DbType.Int32, categoriaId);
        db.AddInParameter(dbCommand, "produtoPaiId", DbType.Int32, produtoPaiId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
            DbTransaction transaction = connection.BeginTransaction();

            try
            {
                db.ExecuteNonQuery(dbCommand, transaction);

                transaction.Commit();

                result = true;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
                
            }
            connection.Close();

            return result;
        }
    }

    public static bool juncaoProdutoCategoriaExclui_PorProdutoId(int produtoId)
    {
        bool result = false;

        Database db = DatabaseFactory.CreateDatabase();

        DbCommand dbCommand = db.GetStoredProcCommand("juncaoProdutoCategoriaExclui_PorProdutoId");

        db.AddInParameter(dbCommand, "produtoId", DbType.Int32, produtoId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
            DbTransaction transaction = connection.BeginTransaction();

            try
            {
                db.ExecuteNonQuery(dbCommand, transaction);

                transaction.Commit();

                result = true;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
                
            }
            connection.Close();

            return result;
        }
    }

    public static bool juncaoProdutoCategoriaExclui_PorProdutoIdCategoriaId(int produtoId, int categoriaId)
    {
        bool result = false;

        Database db = DatabaseFactory.CreateDatabase();

        DbCommand dbCommand = db.GetStoredProcCommand("juncaoProdutoCategoriaExclui_PorProdutoIdCategoriaId");

        db.AddInParameter(dbCommand, "produtoId", DbType.Int32, produtoId);
        db.AddInParameter(dbCommand, "categoriaId", DbType.Int32, categoriaId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
            DbTransaction transaction = connection.BeginTransaction();

            try
            {
                db.ExecuteNonQuery(dbCommand, transaction);

                transaction.Commit();

                result = true;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
                
            }
            connection.Close();

            return result;
        }
    }
    
    public static bool juncaoProdutoCategoriaExclui_PorProdutoPaiId(int produtoPaiId)
    {
        bool result = false;

        Database db = DatabaseFactory.CreateDatabase();

        DbCommand dbCommand = db.GetStoredProcCommand("juncaoProdutoCategoriaExclui_PorProdutoPaiId");

        db.AddInParameter(dbCommand, "produtoPaiId", DbType.Int32, produtoPaiId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
            DbTransaction transaction = connection.BeginTransaction();

            try
            {
                db.ExecuteNonQuery(dbCommand, transaction);

                transaction.Commit();

                result = true;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
                
            }
            connection.Close();

            return result;
        }
    }

    public static string GerarPermalinkCategoria(string categoriaNome)
    {
        return GerarPermalinkCategoria(null, categoriaNome);
    }

    public static string GerarPermalinkCategoria(int? categoriaId, string categoriaNome)
    {
        var data = new dbCommerceDataContext();
        var categorias = (from c in data.tbProdutoCategorias select c).ToList();
        //var categorias = ListaCategoriasFromCache(true);
        if (categoriaId != null)
        {
            categorias = categorias.Where(x => x.categoriaId != (int)categoriaId).ToList();
        }
        string nomeLimpo = rnFuncoes.limpaString(categoriaNome.Trim()).ToLower();
        string nomeLimpoAjustado = nomeLimpo;
        bool jaExiste = true;
        int contagem = 1;
        while (jaExiste)
        {
            bool possuiCategoria = (from c in categorias where c.categoriaUrl.ToLower() == nomeLimpoAjustado select c).Any();
            bool possuiCategoriaAnterior = (from c in data.tbSeoUrls where c.idSeoUrlTipo == 1 && c.idRelacionado == categoriaId && c.redirect == true && c.url == nomeLimpoAjustado select c).Any();
            if (possuiCategoria)
            {
                nomeLimpoAjustado = nomeLimpo + "-" + contagem;
                contagem++;
            }
            else
            {
                jaExiste = false;
            }
        }
        return nomeLimpoAjustado;
    }
}
