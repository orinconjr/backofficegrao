﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Web.Configuration;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;

/// <summary>
/// Summary description for rnCompreJunto
/// </summary>
public class rnCompreJunto
{
    public static DataSet juncaoProdutoCompreJuntoSeleciona_PorProdutoId(int produtoId)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("juncaoProdutoCompreJuntoSeleciona_PorProdutoId");

        db.AddInParameter(dbCommand, "produtoId", DbType.Int32, produtoId);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet compreJuntoSeleciona_PorIdDoGrupo(int idDoGrupo)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("compreJuntoSeleciona_PorIdDoGrupo");

        db.AddInParameter(dbCommand, "idDoGrupo", DbType.Int32, idDoGrupo);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }
    
    public static DataSet juncaoProdutoCompreJuntoSeleciona_PorProdutoIdCompreJuntoGrupo(int produtoId, int compreJuntoGrupo)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("juncaoProdutoCompreJuntoSeleciona_PorProdutoIdCompreJuntoGrupo");

        db.AddInParameter(dbCommand, "produtoId", DbType.Int32, produtoId);
        db.AddInParameter(dbCommand, "compreJuntoGrupo", DbType.Int32, compreJuntoGrupo);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static bool juncaoProdutoCompreJuntoInclui(int compreJuntoGrupo, int produtoId)
    {
        bool result = false;

        Database db = DatabaseFactory.CreateDatabase();

        DbCommand dbCommand = db.GetStoredProcCommand("juncaoProdutoCompreJuntoInclui");

        db.AddInParameter(dbCommand, "compreJuntoGrupo", DbType.Int32, compreJuntoGrupo);
        db.AddInParameter(dbCommand, "produtoId", DbType.Int32, produtoId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
            DbTransaction transaction = connection.BeginTransaction();

            try
            {
                db.ExecuteNonQuery(dbCommand, transaction);

                transaction.Commit();

                result = true;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
                
            }
            connection.Close();

            return result;
        }
    }

    public static bool juncaoProdutoCompreJuntoExclui(int compreJuntoGrupo, int produtoId)
    {
        bool result = false;

        Database db = DatabaseFactory.CreateDatabase();

        DbCommand dbCommand = db.GetStoredProcCommand("juncaoProdutoCompreJuntoExclui");

        db.AddInParameter(dbCommand, "compreJuntoGrupo", DbType.Int32, compreJuntoGrupo);
        db.AddInParameter(dbCommand, "produtoId", DbType.Int32, produtoId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
            DbTransaction transaction = connection.BeginTransaction();

            try
            {
                db.ExecuteNonQuery(dbCommand, transaction);

                transaction.Commit();

                result = true;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
                
            }
            connection.Close();

            return result;
        }
    }
}
