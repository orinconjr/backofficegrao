﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Web.Configuration;
using System.Collections;

/// <summary>
/// Descrição resumida para rnDestaques
/// </summary>
public class rnDestaques
{
    public static DataSet destaqueSeleciona()
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("destaqueSeleciona");

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static bool destaqueIncluir(string nomeDaPagina, int categoriaId, string destaque, int produtoDestaqueId)
    {
        bool result = false;

        Database db = DatabaseFactory.CreateDatabase();

        DbCommand dbCommand = db.GetStoredProcCommand("destaqueIncluir");

        db.AddInParameter(dbCommand, "nomeDaPagina", DbType.String, nomeDaPagina);
        db.AddInParameter(dbCommand, "categoriaId", DbType.Int32, categoriaId);
        db.AddInParameter(dbCommand, "destaque", DbType.String, destaque);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
            DbTransaction transaction = connection.BeginTransaction();

            try
            {
                db.ExecuteNonQuery(dbCommand, transaction);

                transaction.Commit();

                result = true;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);

            }
            connection.Close();

            return result;
        }
    }

    public static bool destaqueAlterar(string nomeDaPagina, int categoriaId, string destaque, int produtoDestaqueId)
    {
        bool result = false;

        Database db = DatabaseFactory.CreateDatabase();

        DbCommand dbCommand = db.GetStoredProcCommand("destaqueAlterar");

        db.AddInParameter(dbCommand, "nomeDaPagina", DbType.String, nomeDaPagina);
        db.AddInParameter(dbCommand, "categoriaId", DbType.Int32, categoriaId);
        db.AddInParameter(dbCommand, "destaque", DbType.String, destaque);
        db.AddInParameter(dbCommand, "produtoDestaqueId", DbType.Int32, produtoDestaqueId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
            DbTransaction transaction = connection.BeginTransaction();

            try
            {
                db.ExecuteNonQuery(dbCommand, transaction);

                transaction.Commit();

                result = true;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);

            }
            connection.Close();

            return result;
        }
    }

    public static bool destaqueExcluir(int produtoDestaqueId)
    {
        bool result = false;

        Database db = DatabaseFactory.CreateDatabase();

        DbCommand dbCommand = db.GetStoredProcCommand("destaqueExcluir");

        db.AddInParameter(dbCommand, "produtoDestaqueId", DbType.Int32, produtoDestaqueId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
            DbTransaction transaction = connection.BeginTransaction();

            try
            {
                db.ExecuteNonQuery(dbCommand, transaction);

                transaction.Commit();

                result = true;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);

            }
            connection.Close();

            return result;
        }
    }
}