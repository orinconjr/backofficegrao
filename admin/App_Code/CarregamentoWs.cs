﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Threading;
using System.Web;
using System.Web.Services;
using System.Xml;

/// <summary>
/// Summary description for CarregamentoWs
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class CarregamentoWs : System.Web.Services.WebService
{

    public CarregamentoWs()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }


    public class CarregamentosEmAberto
    {
        public DateTime? dataAbertura { get; set; }
        public string nomeMotorista { get; set; }
        public string placaCaminhao { get; set; }
        public int idCarregamentoCaminhaoTransportadora { get; set; }
        public int idCarregamentoCaminhao { get; set; }
        public string transportadora { get; set; }
    }
    

    [WebMethod]
    public List<CarregamentosEmAberto> CarregamentosTransportadoraEmAberto(int idUsuario)
    {
        var data = new dbCommerceDataContext();
        var permissaoTransportadoras = (from c in data.tbUsuarioExpedicaoPermissaos where c.idUsuarioExpedicao == idUsuario && c.idExpedicaoTipoPermissao == 4 select c.Id_Relacionado).ToList();
        var carregamentos = (from c in data.tbCarregamentoCaminhaoTransportadoras
                             where (c.dataAbertura == null | c.dataFechamento == null) && c.tbCarregamentoCaminhao.dataFechamento == null && permissaoTransportadoras.Contains(c.idTransportadora)
                             select new CarregamentosEmAberto
                             {
                                 dataAbertura = c.dataAbertura,
                                 nomeMotorista = c.tbCarregamentoCaminhao.nomeMotorista,
                                 placaCaminhao = c.tbCarregamentoCaminhao.placaCaminhao,
                                 idCarregamentoCaminhaoTransportadora = c.idCarregamentoCaminhaoTransportadora,
                                 idCarregamentoCaminhao = c.idCarregamentoCaminhao,
                                 transportadora = c.tbTransportadora.transportadora
                             });
        return carregamentos.ToList();
    }


    public class CarregamentoPacotes
    {
        public int idPedidoPacote { get; set; }
        public int idPedido { get; set; }
        public int totalPacotes { get; set; }
        public int totalCarregado { get; set; }
    }

    public class RetornoCarregamentoPacotes
    {
        public int totalPedidosCarregadosTotal { get; set; }
        public int totalPacotesCarregadosTotal { get; set; }
        public int totalPedidosCarregadosparcial { get; set; }
        public List<CarregamentoPacotes> carregamentosParciais { get; set; }
    }

    [WebMethod]
    public RetornoCarregamentoPacotes CarregamentoListaPacotes(int idCarregamentoCaminhaoTransportadora)
    {
        var data = new dbCommerceDataContext();

        var carregamentoCaminhaoPacote = (from c in data.tbCarregamentoCaminhaoPacote
                                           where c.idCarregamentoCaminhaoTransportadora == idCarregamentoCaminhaoTransportadora
                                           select new
                                           {
                                               c.tbPedidoPacote.idPedidoEnvio,
                                               c.tbPedidoPacote.idPedido,
                                               c.idPedidoPacote
                                           }).ToList();
        var envios = carregamentoCaminhaoPacote.Select(x => x.idPedidoEnvio).Distinct();
        var pacotesTotal = (from c in data.tbPedidoPacotes where envios.Contains(c.idPedidoEnvio) select new { c.idPedidoEnvio, c.idPedidoPacote }).ToList();

        var carregamentos = (from c in carregamentoCaminhaoPacote
                             join d in pacotesTotal on c.idPedidoEnvio equals d.idPedidoEnvio into pacotesTotais
                             join f in carregamentoCaminhaoPacote on c.idPedidoEnvio equals f.idPedidoEnvio into pacotesCarregado
                             select new CarregamentoPacotes
                             {
                                 idPedidoPacote = c.idPedidoPacote,
                                 idPedido = c.idPedido,
                                 totalPacotes = pacotesTotais.Count(),
                                 totalCarregado = pacotesCarregado.Count()
                             });

        /*var carregamentos = (from c in data.tbCarregamentoCaminhaoPacote
                             join d in data.tbPedidoPacotes on c.tbPedidoPacote.idPedidoEnvio equals d.idPedidoEnvio into pacotesTotais
                             join f in data.tbCarregamentoCaminhaoPacote on c.tbPedidoPacote.idPedidoEnvio equals f.tbPedidoPacote.idPedidoEnvio into pacotesCarregado
                             where c.idCarregamentoCaminhaoTransportadora == idCarregamentoCaminhaoTransportadora
                             select new CarregamentoPacotes
                             {
                                 idPedidoPacote = c.idPedidoPacote,
                                 idPedido = c.tbPedidoPacote.idPedido,
                                 totalPacotes = pacotesTotais.Count(),
                                 totalCarregado = pacotesCarregado.Count()
                             });*/
        var retorno = new RetornoCarregamentoPacotes();

        retorno.totalPedidosCarregadosTotal = carregamentos.Where(x => x.totalPacotes == x.totalCarregado).Select(x => x.idPedido).Distinct().Count();
        retorno.totalPacotesCarregadosTotal = carregamentos.Where(x => x.totalPacotes == x.totalCarregado).Select(x => x.idPedidoPacote).Distinct().Count();
        retorno.totalPedidosCarregadosparcial = carregamentos.Where(x => x.totalPacotes != x.totalCarregado).Select(x => x.idPedido).Distinct().Count();
        retorno.carregamentosParciais = carregamentos.Where(x => x.totalPacotes != x.totalCarregado).ToList();
        return retorno;
    }

    public class RetornoCarregarPacote
    {
        public bool carregado { get; set; }
        public string mensagem { get; set; }
    }
    [WebMethod]
    public RetornoCarregarPacote CarregarPacote(int idCarregamentoCaminhaoTransportadora, int idUsuario, string etiqueta)
    {

        var data = new dbCommerceDataContext();
        var usuario = (from c in data.tbUsuarioExpedicaos where c.idUsuarioExpedicao == idUsuario select c).First();
        var carregamentoCaminhao = (from c in data.tbCarregamentoCaminhaoTransportadoras where c.idCarregamentoCaminhaoTransportadora == idCarregamentoCaminhaoTransportadora select c).First();
        var cnpj = string.Empty;
        var numeroNota = string.Empty;
        var volAtual = string.Empty;
        var volTotal = string.Empty;

        var retorno = new RetornoCarregarPacote();
        if (etiqueta.Length == 31)
        {
            cnpj = etiqueta.Substring(0, 14);
            numeroNota = etiqueta.Substring(14, 9);
            volAtual = etiqueta.Substring(23, 4);
            volTotal = etiqueta.Substring(27, 4);
            var empresa = (from c in data.tbEmpresas where c.cnpj == cnpj select c).First();
            var nota = (from c in data.tbNotaFiscals
                        join d in data.tbPedidoEnvios on c.idPedidoEnvio equals d.idPedidoEnvio
                        where c.idCNPJ == empresa.idEmpresa && c.numeroNota == int.Parse(numeroNota)
                        select new
                        {
                            c.idPedidoEnvio,
                            c.idPedido,
                            d.volumesEmbalados
                        }).First();
            var pacote =
                (from c in data.tbPedidoPacotes
                 where c.idPedidoEnvio == nota.idPedidoEnvio && c.numeroVolume == int.Parse(volAtual)
                 select c).FirstOrDefault();

            if (pacote != null)
            {
                string transportadoraString = pacote.tbPedidoEnvio.formaDeEnvio;
                var formaDeEnvio = (from c in data.tbTipoDeEntregas where c.idTransportadoraString.ToLower() == transportadoraString.ToLower() select c.tbTransportadora).FirstOrDefault();
                if(formaDeEnvio == null)
                {
                    retorno.carregado = false;
                    retorno.mensagem = "TRANSPORTADORA INCORRETA. Pacote não faz parte do carregamento.";
                    rnInteracoes.interacaoInclui(pacote.idPedido, "Tentativa de carregar pedido no caminhão da " + carregamentoCaminhao.tbTransportadora.transportadora, usuario.nome, "False");
                    return retorno;
                }
                else
                {
                    bool emitido = pacote.tbPedidoEnvio.nfeImpressa == true | !string.IsNullOrEmpty(pacote.rastreio);
                    var validaPacoteCarregado = (from c in data.tbCarregamentoCaminhaoPacote where c.idPedidoPacote == pacote.idPedidoPacote select c).Any();
                    if(formaDeEnvio.idTransportadora != carregamentoCaminhao.idTransportadora)
                    {
                        retorno.mensagem = "TRANSPORTADORA INCORRETA. Pacote não faz parte do carregamento.";
                        rnInteracoes.interacaoInclui(pacote.idPedido, "Tentativa de carregar pedido no caminhão da " + carregamentoCaminhao.tbTransportadora.transportadora, usuario.nome, "False");

                    }
                    else if (!emitido)
                    {
                        retorno.mensagem = "PEDIDO SEM EMISSÃO. Faça a emissão antes de carregar o pacote.";
                        rnInteracoes.interacaoInclui(pacote.idPedido, "Tentativa de carregar pedido no caminhão sem realizar emissão", usuario.nome, "False");

                    }
                    else if(validaPacoteCarregado)
                    {
                        retorno.mensagem = "Pacote já foi carregado anteriormente.";
                        rnInteracoes.interacaoInclui(pacote.idPedido, "Tentativa de carregar pedido no caminhão da " + carregamentoCaminhao.tbTransportadora.transportadora + " já carregado anteriormente", usuario.nome, "False");
                    }
                    else
                    {
                        rnInteracoes.interacaoInclui(pacote.idPedido, "Volume " + pacote.numeroVolume + " carregado", usuario.nome, "False");

                        var agora = DateTime.Now;
                        var pacoteCarregamento = new tbCarregamentoCaminhaoPacote();
                        pacoteCarregamento.idPedidoPacote = pacote.idPedidoPacote;
                        pacoteCarregamento.idCarregamentoCaminhaoTransportadora = idCarregamentoCaminhaoTransportadora;
                        pacoteCarregamento.idUsuarioExpedicao = idUsuario;
                        pacoteCarregamento.nomeUsuarioExpedicao = usuario.nome;
                        pacoteCarregamento.dataCarregamento = agora;
                        pacote.carregadoCaminhao = true;
                        pacote.dataCarregamentoCaminhao = agora;

                        data.tbCarregamentoCaminhaoPacote.InsertOnSubmit(pacoteCarregamento);
                        data.SubmitChanges();
                        retorno.carregado = true;
                    }
                }
            }
            else
            {
                retorno.carregado = false;
                retorno.mensagem = "Pacote não localizado";
                return retorno;
            }
        }


        return retorno;
    }


    [WebMethod]
    public bool AbreCarregamentoCaminhaoTransportadora(int idCarregamentoCaminhaoTransportadora, int idUsuario)
    {
        try
        {
            var data = new dbCommerceDataContext();
            var carregamento = (from c in data.tbCarregamentoCaminhaoTransportadoras where c.idCarregamentoCaminhaoTransportadora == idCarregamentoCaminhaoTransportadora select c).First();
            carregamento.idUsuarioAbertura = idUsuario;
            carregamento.dataAbertura = DateTime.Now;
            data.SubmitChanges();
            return true;
        }
        catch(Exception ex)
        {
            return false;
        }
    }

    [WebMethod]
    public RetornoCarregarPacote FinalizarCarregamento(int idCarregamentoCaminhaoTransportadora, int idUsuarioFinalizacao, int idUsuarioFinalizacaoAutorizacaoParcial)
    {
        var retorno = new RetornoCarregarPacote();
        var data = new dbCommerceDataContext();
        var usuario = (from c in data.tbUsuarioExpedicaos where c.idUsuarioExpedicao == idUsuarioFinalizacao select c).First();
        var carregamentos = (from c in data.tbCarregamentoCaminhaoPacote
                             join d in data.tbPedidoPacotes on c.tbPedidoPacote.idPedidoEnvio equals d.idPedidoEnvio into pacotesTotais
                             join f in data.tbCarregamentoCaminhaoPacote on c.tbPedidoPacote.idPedidoEnvio equals f.tbPedidoPacote.idPedidoEnvio into pacotesCarregado
                             where c.idCarregamentoCaminhaoTransportadora == idCarregamentoCaminhaoTransportadora
                             select new CarregamentoPacotes
                             {
                                 idPedidoPacote = c.idPedidoPacote,
                                 idPedido = c.tbPedidoPacote.idPedido,
                                 totalPacotes = pacotesTotais.Count(),
                                 totalCarregado = pacotesCarregado.Count()
                             }).Where(x => x.totalPacotes > x.totalCarregado);
        
        var usuarioAutorizacaoParcial = (from c in data.tbUsuarioExpedicaos where c.idUsuarioExpedicao == idUsuarioFinalizacaoAutorizacaoParcial select c).First();

        if (carregamentos.Any())
        {
            var pedidosParciais = carregamentos.Select(x => x.idPedido).Distinct().ToList();
            foreach (var pedidoParcial in pedidosParciais)
            {
                //Abrir chamado
                rnInteracoes.interacaoInclui(pedidoParcial, "Carregamento finalizado com pedido parcialmente carregado", usuarioAutorizacaoParcial.nome, "False");
            }
        }
        var carregamentoCaminhaoTransportadora = (from c in data.tbCarregamentoCaminhaoTransportadoras where c.idCarregamentoCaminhaoTransportadora == idCarregamentoCaminhaoTransportadora select c).First();
        carregamentoCaminhaoTransportadora.idUsuarioFechamento = idUsuarioFinalizacao;
        carregamentoCaminhaoTransportadora.dataFechamento = DateTime.Now;
        data.SubmitChanges();
        retorno.carregado = true;
        return retorno;
    }


}
