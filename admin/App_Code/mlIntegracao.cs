﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using DevExpress.Utils.OAuth;
using DevExpress.Utils.OAuth.Provider;
using MercadoLibre.SDK;
using Newtonsoft.Json;
using RestSharp;

/// <summary>
/// Summary description for mlIntegracao
/// </summary>
public class mlIntegracao
{
    // Credenciais de Produção
    public static long mlUserId = 8919138202281911;
    public static string mlPass = "isDUkGURbdOcwI1Z45bl9ABBIeuOdqPt";
    public static string mlSellerId = "241861815";
    // Credenciais de Teste
    //public static long mlUserId = 4331620370331490;
    //public static string mlPass = "NQ7oyKxbLyNZLFnb4G9Hk4uoREtRrki8";
    //public static string mlSellerId = "258051953";
    public static bool mlHomologacao = false;
    public static string urlBase = "https://admin.graodegente.com.br/admin/";
    //public static string urlBase = "http://localhost:17380/admin/";
    public static int mlCondicaoDePagamentoId = 26;
    public static int mlOrigemClienteId = 4;
    public static int mlStatusAguardandoPagamento = 2;
    public static int mlOfficialStoreId = 1029;
    public static int mlEntregaNormal = 8;
    public static int mlEntregaExpressa = 5;


    public static Meli verificaAutenticacao(string pagina, string returnCode)
    {
        
        var integracoesDc = new dbCommerceDataContext();
        var tokiens = (from c in integracoesDc.tbMlTokens orderby c.idMlToken descending select c).FirstOrDefault();
        if (tokiens != null && returnCode == null)
        {
            Meli m = new Meli(mlUserId, mlPass, tokiens.auth_token, tokiens.refresh_token);

            //var p = new RestSharp.Parameter();
            //p.Name = "access_token";
            //p.Value = m.AccessToken;
            //var ps = new List<RestSharp.Parameter>();
            //ps.Add(p);

            //var sellerId = new RestSharp.Parameter();
            //sellerId.Name = "seller";
            //sellerId.Value = mlSellerId;
            //ps.Add(sellerId);

            //try
            //{

            //}
            //catch (Exception)
            //{
                
            //    throw;
            //}

            //var sort = new RestSharp.Parameter();
            //sort.Name = "sort";
            //sort.Value = "date_desc";
            //ps.Add(sort);

            //IRestResponse r = m.Post("/orders/search", ps, "");

            atualizaTokien(m);

            return m;
        }
        else
        {
            return criaNovoTokien(pagina, returnCode);
        }
    }

    private static Meli criaNovoTokien(string pagina, string returnCode)
    {
        var integracoesDc = new dbCommerceDataContext();
        Meli m = new Meli(mlUserId, mlPass);

        if (string.IsNullOrEmpty(returnCode))
        {
            string redirectUrl = m.GetAuthUrl(urlBase + pagina);
            HttpContext.Current.Response.Redirect(redirectUrl);
        }
        else if (!string.IsNullOrEmpty(returnCode))
        {
            string code = HttpContext.Current.Request.QueryString["code"];
            m.Authorize(code, urlBase + pagina);
            var tokien = new tbMlToken();
            tokien.auth_token = m.AccessToken;
            tokien.refresh_token = m.RefreshToken;
            integracoesDc.tbMlTokens.InsertOnSubmit(tokien);
            integracoesDc.SubmitChanges();
            HttpContext.Current.Response.Redirect(urlBase + pagina);
        }
        return m;
    }

    public static string retornaAccessToken()
    {
        var integracoesDc = new dbCommerceDataContext();
        return (from c in integracoesDc.tbMlTokens orderby c.idMlToken descending select c).FirstOrDefault().auth_token;
        //return HttpContext.Current.Session["mlAccessToken"].ToString();
    }

    public static mlShipping retornaShippingPadrao(int? largura, int? altura, int? profundidade, int peso)
    {
        int l = largura == null ? 5 : (int)largura;
        int a = altura == null ? 5 : (int)altura;
        int p = profundidade == null ? 5 : (int)profundidade;

        if (l < 20) l = 20;
        if (a < 20) a = 20;
        if (p < 20) p = 20;

        var shipping = new mlShipping();
        shipping.local_pick_up = false;
        shipping.mode = "me1";

        //var shippingMethods = new List<int>();
        //shippingMethods.Add(100009);
        //shipping.methods = shippingMethods;

        /*var shippingMethod = new mlShippingMethod();
        shippingMethod.id = 100009;
        var shippingMethods = new List<mlShippingMethod>();
        shippingMethods.Add(shippingMethod);
        shipping.methods = shippingMethods;*/

        shipping.dimensions = l + "x" + a + "x" + p + "," + peso;
        return shipping;
    }

    public static mlShippingFree retornaShippingGratis(int? largura, int? altura, int? profundidade, int peso)
    {
        int l = largura == null ? 5 : (int)largura;
        int a = altura == null ? 5 : (int)altura;
        int p = profundidade == null ? 5 : (int)profundidade;

        if (l < 20) l = 20;
        if (a < 20) a = 20;
        if (p < 20) p = 20;

        var shipping = new mlShippingFree();
        shipping.local_pick_up = false;
        shipping.dimensions = l + "x" + a + "x" + p + "," + peso;
        var shippingMethod = new mlShippingMethodFree();
        shippingMethod.id = 100009;
        shippingMethod.free = "country";
        var shippingMethods = new List<mlShippingMethodFree>();
        shippingMethods.Add(shippingMethod);
        shipping.methods = shippingMethods;
        return shipping;
    }

    public static string montaHtmlDescricao(string disponibilidade, string descricao)
    {
        string corpo = mlCarregarArquivo(System.Configuration.ConfigurationManager.AppSettings["caminhoFisico"] + "\\admin\\templates\\" + "mercadolivre.html");

        corpo = corpo.Replace("[#disponibilidade#]", disponibilidade);
        corpo = corpo.Replace("[#descricao#]", descricao);
        return corpo;
    }

    public static string mlCarregarArquivo(string pathArquivo)
    {
        try
        {
            if (!File.Exists(pathArquivo))
                throw new Exception(String.Format("Arquivo '{0}' não encontrado!", pathArquivo));

            return File.ReadAllText(pathArquivo, Encoding.GetEncoding("ISO-8859-1"));
        }
        catch (Exception ex)
        {
            return "";
        }
    }

    public static void checaPedidosML()
    {
        /*var pedidoDc = new dbCommerceDataContext();
        var checagem = (from c in pedidoDc.tbMarketplaceChecagems orderby c.dataHora descending select c).FirstOrDefault();
        var ultimaChecagem = checagem == null ? DateTime.Now.AddHours(-2) : checagem.dataHora;
        if (ultimaChecagem < DateTime.Now.AddMinutes(-5))
        {*/
            var meli = mlIntegracao.verificaAutenticacao("pedidos.aspx", HttpContext.Current.Request.QueryString["code"]);
            var p = new RestSharp.Parameter();
            p.Name = "access_token";
            p.Value = meli.AccessToken;
            var ps = new List<RestSharp.Parameter>();
            ps.Add(p);

            var sellerId = new RestSharp.Parameter();
            sellerId.Name = "seller";
            sellerId.Value = mlSellerId;
            ps.Add(sellerId);

            var sort = new RestSharp.Parameter();
            sort.Name = "sort";
            sort.Value = "date_desc";
            ps.Add(sort);

            //IRestResponse r = meli.Post("/orders/search", ps, "");
            IRestResponse r = meli.Get("/orders/search", ps);
            HttpContext.Current.Response.Write(r.Content);
            //atualizaTokien(meli);
            var pedidos = JsonConvert.DeserializeObject<mlPedidos>(r.Content);
            checaPedidosML(pedidos, meli);
        //}

    }

    public static void atualizaTokien(Meli meli)
    {
        var integracoesDc = new dbCommerceDataContext();
        var tokiens = (from c in integracoesDc.tbMlTokens orderby c.idMlToken descending select c).FirstOrDefault();

        if (tokiens != null)
        {
            if (tokiens.auth_token != meli.AccessToken | tokiens.refresh_token != meli.RefreshToken)
            {
                var tokien = new tbMlToken();
                tokien.auth_token = meli.AccessToken;
                tokien.refresh_token = meli.RefreshToken;
                integracoesDc.tbMlTokens.InsertOnSubmit(tokien);
                integracoesDc.SubmitChanges();
            }
        }
    }
    public static void checaPedidosML(mlPedidos pedidos, Meli meli)
    {

        var pedidoDc = new dbCommerceDataContext();

        if (pedidos.results != null)
        {
            foreach (var pedidoEx in pedidos.results)
            {
                var p2 = new RestSharp.Parameter();
                p2.Name = "access_token";
                p2.Value = meli.AccessToken;
                var ps2 = new List<RestSharp.Parameter>();
                ps2.Add(p2);
                IRestResponse r2 = meli.Get("/orders/" + pedidoEx.id, ps2);
                atualizaTokien(meli);
                var pedidoMl = JsonConvert.DeserializeObject<mlSingleOrder>(r2.Content);

                var pedidoCheck = (from c in pedidoDc.tbPedidos
                    where c.marketplaceId == pedidoMl.id.ToString() && c.condDePagamentoId == mlCondicaoDePagamentoId
                    select c).FirstOrDefault();
                if (pedidoCheck == null)
                {
                    var clienteDc = new dbCommerceDataContext();
                    int clienteId = 0;
                    var clienteCheck = (from c in clienteDc.tbClientes
                        where
                            c.clienteCPFCNPJ == pedidoMl.buyer.billing_info.doc_number &&
                            c.idOrigemCliente == mlOrigemClienteId
                        select c).FirstOrDefault();
                    if (clienteCheck != null)
                    {
                        clienteId = clienteCheck.clienteId;
                    }
                    else
                    {
                        var cliente = new tbCliente();
                        cliente.clienteNome = pedidoMl.buyer.first_name + " " + pedidoMl.buyer.last_name;
                        cliente.clienteCPFCNPJ = pedidoMl.buyer.billing_info == null | pedidoMl.buyer.billing_info.doc_number == null ? "11111111111" : pedidoMl.buyer.billing_info.doc_number.ToString();
                        cliente.clienteRGIE = "";
                        cliente.clienteSexo = "M";
                        cliente.clienteRua = pedidoMl.shipping == null | pedidoMl.shipping.receiver_address == null ? "" : pedidoMl.shipping.receiver_address.street_name;
                        cliente.clienteNumero = pedidoMl.shipping == null | pedidoMl.shipping.receiver_address == null ? "" : pedidoMl.shipping.receiver_address.street_number;
                        cliente.clienteComplemento = pedidoMl.shipping == null | pedidoMl.shipping.receiver_address == null ? "" : pedidoMl.shipping.receiver_address.comment;
                        cliente.clienteBairro = pedidoMl.shipping == null | pedidoMl.shipping.receiver_address == null ? "" : pedidoMl.shipping.receiver_address.neighborhood.name;
                        cliente.clienteCidade = pedidoMl.shipping == null | pedidoMl.shipping.receiver_address == null ? "" : pedidoMl.shipping.receiver_address.city.name;
                        cliente.clienteEstado = pedidoMl.shipping == null | pedidoMl.shipping.receiver_address == null ? "" : pedidoMl.shipping.receiver_address.state.id.Replace("BR-", "");
                        cliente.clientePais = "Brasil";
                        cliente.clienteCep = pedidoMl.shipping == null | pedidoMl.shipping.receiver_address == null ? "" : pedidoMl.shipping.receiver_address.zip_code ?? "";
                        cliente.clienteEmail = pedidoMl.buyer.email;
                        cliente.clienteSenha = "marketplace";
                        cliente.clienteDataNascimento = "01/01/1900";
                        cliente.clienteFoneResidencial = "";
                        cliente.clienteFoneComercial = "";
                        cliente.clienteFoneCelular = "";
                        cliente.clienteRecebeInformativo = "False";
                        cliente.idOrigemCliente = mlOrigemClienteId;
                        cliente.dataDaCriacao = DateTime.Now;

                        clienteDc.tbClientes.InsertOnSubmit(cliente);
                        clienteDc.SubmitChanges();
                        clienteId = cliente.clienteId;
                    }

                    var pedido = new tbPedido();
                    pedido.pedidoKey = "";
                    pedido.clienteId = clienteId;
                    pedido.ipDoCliente = "";
                    pedido.tipoDePagamentoId = rnIntegracoes.marketplaceFormaDePagamentoId;
                    pedido.condDePagamentoId = mlCondicaoDePagamentoId;
                    int tipoDeEntrega = 8;
                    if (pedidoMl.shipping.service_id != null)
                    {
                        if (pedidoMl.shipping.service_id.ToString() == "22")
                        {
                            tipoDeEntrega = mlEntregaExpressa;
                        }
                        else
                        {
                            tipoDeEntrega = mlEntregaNormal;
                        }
                    }

                    if (pedidoMl.shipping != null && pedidoMl.shipping.service_id != null)
                    {
                        HttpContext.Current.Response.Write(pedidoMl.shipping.service_id);
                    }

                    pedido.tipoDeEntregaId = tipoDeEntrega;

                    pedido.valorDoFrete = Convert.ToDecimal(pedidoMl.shipping.cost);
                    pedido.prazoDeEntrega = "0";
                    pedido.valorDosItens = Convert.ToDecimal(pedidoMl.total_amount);
                    pedido.valorTotalGeral = Convert.ToDecimal(pedidoMl.total_amount + pedidoMl.shipping.cost);
                    pedido.valorCobrado = Convert.ToDecimal(pedidoMl.total_amount + pedidoMl.shipping.cost);
                    pedido.valorDosJuros = 0;
                    pedido.valorDoEmbrulhoECartao = 0;
                    pedido.porcentagemDoJuros = 0;
                    pedido.numeroDeParcelas = 1;
                    pedido.valorDaParcela = Convert.ToDecimal(pedidoMl.total_amount + pedidoMl.shipping.cost);
                    pedido.pesoDoPedido = 0;
                    pedido.dataHoraDoPedido = DateTime.Now;
                    int status = mlStatusAguardandoPagamento;
                    if (pedidoMl.status == "confirmed") status = mlStatusAguardandoPagamento;
                    if (pedidoMl.status == "paid")
                    {
                        status = rnIntegracoes.marketplaceStatusPagamentoConfirmado;
                        pedido.dataConfirmacaoPagamento = DateTime.Now;
                    }

                    pedido.statusDoPedido = status;
                    pedido.endNomeDoDestinatario = pedidoMl.buyer.first_name + " " + pedidoMl.buyer.last_name;
                    pedido.endRua = pedidoMl.shipping == null | pedidoMl.shipping.receiver_address == null ? "" : pedidoMl.shipping.receiver_address.street_name;
                    pedido.endNumero = pedidoMl.shipping == null | pedidoMl.shipping.receiver_address == null ? "" : pedidoMl.shipping.receiver_address.street_number;
                    pedido.endComplemento = pedidoMl.shipping == null | pedidoMl.shipping.receiver_address == null ? "" : pedidoMl.shipping.receiver_address.comment;
                    pedido.endBairro = pedidoMl.shipping == null | pedidoMl.shipping.receiver_address == null ? "" : pedidoMl.shipping.receiver_address.neighborhood.name;
                    pedido.endCidade = pedidoMl.shipping == null | pedidoMl.shipping.receiver_address == null ? "" : pedidoMl.shipping.receiver_address.city.name;
                    pedido.endEstado = pedidoMl.shipping == null | pedidoMl.shipping.receiver_address == null ? "" : pedidoMl.shipping.receiver_address.state.id.Replace("BR-", "");
                    pedido.endPais = "Brasil";
                    pedido.endCep = pedidoMl.shipping == null | pedidoMl.shipping.receiver_address == null ? "" : pedidoMl.shipping.receiver_address.zip_code;

                    pedido.marketplace = true;
                    pedido.marketplaceId = pedidoMl.id.ToString();
                    pedido.dataHoraDoPedido = Convert.ToDateTime(pedidoMl.date_created);;
                    pedidoDc.tbPedidos.InsertOnSubmit(pedido);
                    pedidoDc.SubmitChanges();

                    if (pedidoMl.status == "paid")
                    {
                        rnInteracoes.interacaoInclui(pedido.pedidoId, "Pagamento Confirmado", "Mercado Livre", "True");
                        inserirQueue(pedido.pedidoId);
                    }

                    var itensPedidoDc = new dbCommerceDataContext();
                    var extraProdutoDc = new dbCommerceDataContext();

                    foreach (var itemPedidoEx in pedidoMl.order_items)
                    {
                        var produtoIdCheck = (from c in extraProdutoDc.tbMlProdutos
                            where c.id == itemPedidoEx.item.id
                            select c).FirstOrDefault();
                        int produtoId = 0;
                        if (produtoIdCheck != null)
                        {
                            produtoId = produtoIdCheck.idProduto;
                        }

                        var produtoDc = new dbCommerceDataContext();
                        var produto =
                            (from c in produtoDc.tbProdutos where c.produtoId == produtoId select c).FirstOrDefault();
                        if (produto != null)
                        {
                            var itemPedido = new tbItensPedido();
                            itemPedido.pedidoId = pedido.pedidoId;
                            itemPedido.produtoId = produtoId;
                            itemPedido.produtoIdDaEmpresa = produto.produtoIdDaEmpresa;
                            itemPedido.itemQuantidade = itemPedidoEx.quantity;
                            itemPedido.itemValor = Convert.ToDecimal(itemPedidoEx.unit_price);
                            itemPedido.dataDaCriacao = DateTime.Now;
                            itemPedido.garantiaId = 0;
                            itemPedido.itemPresente = "False";
                            itemPedido.valorDoEmbrulhoECartao = 0;

                            itensPedidoDc.tbItensPedidos.InsertOnSubmit(itemPedido);
                        }
                    }
                    itensPedidoDc.SubmitChanges();
                    enviaEmailMercadoLivre(pedido.pedidoId, pedidoMl.buyer.email, pedidoMl.buyer.first_name + " " + pedidoMl.buyer.last_name);
                    //enviaEmailMercadoLivre(pedido.pedidoId, "andre@bark.com.br", pedidoMl.buyer.first_name + " " + pedidoMl.buyer.last_name);

                }
                else
                {
                    if (pedidoMl.shipping != null && pedidoMl.shipping.service_id != null)
                    {
                        if (pedidoMl.shipping.service_id.ToString() == "22")
                        {
                           pedidoCheck.tipoDeEntregaId = mlEntregaExpressa;
                        }
                        else
                        {
                            pedidoCheck.tipoDeEntregaId = mlEntregaNormal;
                        }
                    }
                    pedidoCheck.dataHoraDoPedido = Convert.ToDateTime(pedidoMl.date_created);
                    pedidoCheck.valorDoFrete = Convert.ToDecimal(pedidoMl.shipping.cost);
                    pedidoCheck.valorTotalGeral = Convert.ToDecimal(pedidoMl.total_amount + pedidoMl.shipping.cost);
                    pedidoCheck.valorCobrado = Convert.ToDecimal(pedidoMl.total_amount + pedidoMl.shipping.cost);
                    pedidoCheck.valorDaParcela = Convert.ToDecimal(pedidoMl.total_amount + pedidoMl.shipping.cost);
                    int status = mlStatusAguardandoPagamento;
                    if (pedidoMl.status == "confirmed") status = mlStatusAguardandoPagamento;
                    if (pedidoMl.status == "paid")
                    {
                        status = rnIntegracoes.marketplaceStatusPagamentoConfirmado;
                        if (pedidoCheck.dataConfirmacaoPagamento == null)
                        {
                            pedidoCheck.dataConfirmacaoPagamento = DateTime.Now;
                            rnInteracoes.interacaoInclui(pedidoCheck.pedidoId, "Pagamento Confirmado", "Mercado Livre", "True");
                            pedidoCheck.statusDoPedido = status;
                            inserirQueue(pedidoCheck.pedidoId);
                        }
                    }

                    pedidoCheck.endNomeDoDestinatario = pedidoMl.buyer.first_name + " " + pedidoMl.buyer.last_name;
                    pedidoCheck.endRua = pedidoMl.shipping == null | pedidoMl.shipping.receiver_address == null ? "" : pedidoMl.shipping.receiver_address.street_name;
                    pedidoCheck.endNumero = pedidoMl.shipping == null | pedidoMl.shipping.receiver_address == null ? "" : pedidoMl.shipping.receiver_address.street_number;
                    pedidoCheck.endComplemento = pedidoMl.shipping == null | pedidoMl.shipping.receiver_address == null ? "" : pedidoMl.shipping.receiver_address.comment;
                    pedidoCheck.endBairro = pedidoMl.shipping == null | pedidoMl.shipping.receiver_address == null ? "" : pedidoMl.shipping.receiver_address.neighborhood.name;
                    pedidoCheck.endCidade = pedidoMl.shipping == null | pedidoMl.shipping.receiver_address == null ? "" : pedidoMl.shipping.receiver_address.city.name;
                    pedidoCheck.endEstado = pedidoMl.shipping == null | pedidoMl.shipping.receiver_address == null ? "" : pedidoMl.shipping.receiver_address.state.id.Replace("BR-", "");
                    pedidoCheck.endPais = "Brasil";
                    pedidoCheck.endCep = pedidoMl.shipping == null | pedidoMl.shipping.receiver_address == null ? "" : pedidoMl.shipping.receiver_address.zip_code;

                    pedidoDc.SubmitChanges();
                }

            }
        }
    }

    private static void inserirQueue(int pedidoId)
    {
        using (var data = new dbCommerceDataContext())
        {
            var tiposQueue = new List<int>();
            tiposQueue.Add(4); // Itens
            tiposQueue.Add(5); // Reserva de estoque
            tiposQueue.Add(6); // Calcular prazo

            foreach (var item in tiposQueue)
            {
                var queue = new tbQueue();
                queue.agendamento = DateTime.Now.AddMinutes(1);
                queue.andamento = false;
                queue.concluido = false;
                queue.idRelacionado = pedidoId;
                queue.mensagem = "";
                queue.tipoQueue = item;
                data.tbQueues.InsertOnSubmit(queue);
                data.SubmitChanges();
            }
        }
    }

    public static void atualizaStatusPedidoMl(int pedidoId, int situacaoId)
    {
        var pedidoDc = new dbCommerceDataContext();
        var pedido = (from c in pedidoDc.tbPedidos where c.pedidoId == pedidoId select c).First();


        var meli = mlIntegracao.verificaAutenticacao("pedidos.aspx", HttpContext.Current.Request.QueryString["code"]);
        var p2 = new RestSharp.Parameter();
        p2.Name = "access_token";
        p2.Value = meli.AccessToken;
        var ps2 = new List<RestSharp.Parameter>();
        ps2.Add(p2);

        IRestResponse r2 = meli.Get("/orders/" + pedido.marketplaceId, ps2);
        var pedidoMl = JsonConvert.DeserializeObject<mlSingleOrder>(r2.Content);
        long shippingId = pedidoMl.shipping.id;


        var pTracking = new RestSharp.Parameter();
        pTracking.Name = "tracking_number";

        var pService = new RestSharp.Parameter();
        pService.Name = "service";

        var meli2 = mlIntegracao.verificaAutenticacao("pedidos.aspx", HttpContext.Current.Request.QueryString["code"]);
        var p1 = new RestSharp.Parameter();
        p1.Name = "access_token";
        p1.Value = meli2.AccessToken;
        var ps1 = new List<RestSharp.Parameter>();
        ps1.Add(p1);

        if (pedido.numeroDoTracking != "")
        {
            if (pedido.tipoDeEntregaId == 5)
            {
                var objReq = new { tracking_number = pedido.numeroDoTracking, service_id = 22 };
                IRestResponse r3 = meli2.Put("/shipments/" + shippingId, ps2, objReq);
            }
            else
            {
                var objReq = new { tracking_number = pedido.numeroDoTracking, service_id = 21 };
                IRestResponse r3 = meli2.Put("/shipments/" + shippingId, ps2, objReq);
            }
        }
        if (pedido.numeroDoTracking4 != "")
        {
            var objReq = new { tracking_number = pedido.numeroDoTracking4, service_id = 11 };
            IRestResponse r3 = meli2.Put("/shipments/" + shippingId, ps2, objReq);
        }
    }

    public static void enviaEmailMercadoLivre(int pedidoId, string email, string nome)
    {
        var pedidoDc = new dbCommerceDataContext();
        var pedido = (from c in pedidoDc.tbPedidos where c.pedidoId == pedidoId select c).First();
        var itensDc = new dbCommerceDataContext();
        var itens = (from c in itensDc.tbItensPedidos where c.pedidoId == pedidoId select c);
        
       StringBuilder produtoHtml = new StringBuilder();

        string disponibilidade = "";

        foreach (var item in itens)
        {
            var produtosDc = new dbCommerceDataContext();
            var produto = (from c in produtosDc.tbProdutos where c.produtoId == item.produtoId select c).FirstOrDefault();
            produtoHtml.Append("<table cellpadding='0' cellspacing='0' style='width: 550' align='center'><tr><td><table cellpadding='0' cellspacing='0' style='width: 100%'><tr><td style='width: 30px'><img alt='' src=" + System.Configuration.ConfigurationManager.AppSettings["caminhoVirtual"] + produto.fotoDestaque + " border='0' width='30' heigth='30' /></a></td><td style='font-family: Arial; font-size: 12px; color: #434242;padding: 8px;padding-left: 10px; font-size: 12px;'>" + produto.produtoNome + "</td></tr></table></td><td align='center' style='width: 70px'>" + string.Empty + "</td><td align='right' style='width: 60px; font-family: Arial; font-size: 12px; color: #434242;padding: 8px;'>" + item.itemQuantidade + "</td><td align='right' style='width: 80px; font-family: Arial; font-size: 12px; color: #434242;padding: 8px;'>" + Convert.ToDecimal(item.itemValor).ToString("C") + "</td><td align='right' style='width: 85px; font-family: Arial; font-size: 12px; color: #434242;padding: 8px;'><b>" + (item.itemQuantidade * item.itemValor).ToString("C") + "</b></td></tr><tr><td colspan='5'><hr color='#809DB9' size='1' style='height: -15px' /></td></tr></table>");
            disponibilidade =
                produto.disponibilidadeEmEstoque.Replace("mais prazo de entrega exposto no carrinho de compras",
                    "mais prazo de entrega");
        }

        string endereco = pedido.endRua + ", " + pedido.endNumero + " " + pedido.endComplemento + "<br>" + pedido.endBairro + "<br>Cep: " + pedido.endCep + " - " + pedido.endCidade + " - " + pedido.endEstado;

        string corpo = mlCarregarArquivo(System.Configuration.ConfigurationManager.AppSettings["caminhoFisico"] + "\\admin\\templates\\" + "compraMercadoLivre.html");
        corpo = corpo.Replace("#pedidoid#", rnFuncoes.retornaIdCliente(pedidoId).ToString());
        corpo = corpo.Replace("#nomecliente#", nome);
        corpo = corpo.Replace("#prazo#", disponibilidade);
        corpo = corpo.Replace("#produtos#", produtoHtml.ToString());
        corpo = corpo.Replace("#endereco#", endereco);

        rnEmails.EnviaEmail("", email, "atendimento@graodegente.com.br", "", "", corpo.ToString(), "Recebemos seu Pedido");
    }


    public static void cadastraAtualizaProdutos(int produtoId)
    {
        HttpContext.Current.Server.ScriptTimeout = 60000;

        var meli = mlIntegracao.verificaAutenticacao("pedidos.aspx", HttpContext.Current.Request.QueryString["code"]);
        var p = new RestSharp.Parameter();
        p.Name = "access_token";
        p.Value = meli.AccessToken;
        var ps = new List<RestSharp.Parameter>();
        ps.Add(p);

        var produto = new StringBuilder();

        var dataProdutos = new dbCommerceDataContext();
        var integracoesDc = new dbCommerceDataContext();

        dataProdutos.CommandTimeout = 0;
        var produtos = (from c in dataProdutos.tbProdutos
                        where c.produtoId == produtoId
                        select new
                        {
                            c.produtoId,
                            c.produtoPaiId,
                            c.produtoEstoqueAtual,
                            c.produtoEstoqueMinimo,
                            c.produtoUrl,
                            c.produtoNome,
                            c.produtoNomeNovo,
                            c.produtoPrecoPromocional,
                            c.produtoLegendaAtacado,
                            c.produtoPrecoAtacado,
                            c.produtoFreteGratis,
                            c.produtoPromocao,
                            c.produtoLancamento,
                            c.produtoMarca,
                            c.produtoDataDaCriacao,
                            c.produtoPreco,
                            c.produtoIdDaEmpresa,
                            c.produtoComposicao,
                            c.produtoFios,
                            c.produtoPecas,
                            c.produtoBrindes,
                            c.produtoDescricao,
                            c.produtoPeso,
                            c.fotoDestaque,
                            c.produtoPrincipal,
                            c.produtoFornecedor,
                            c.produtoAtivo,
                            c.marketplaceEnviarMarca,
                            c.marketplaceCadastrar,
                            c.largura,
                            c.altura,
                            c.profundidade,
                            c.disponibilidadeEmEstoque
                        });

        int totalProdutos = produtos.Count();
        int atual = 1;

        foreach (var prod in produtos)
        {
            var descricaoCompleta = new StringBuilder();
            var dataInformacao = new dbCommerceDataContext();
            var informacoesAdicional = (from c in dataInformacao.tbInformacaoAdcionals where c.produtoId == prod.produtoId orderby c.informacaoAdcionalNome descending select c).ToList();
            foreach (var informacaoAdcional in informacoesAdicional)
            {
                descricaoCompleta.AppendFormat("<div style='font-family: Arial; font-size: 26px; font-weight: bold; color: #686A39; border-bottom: 1px solid #E6E7C4; padding-bottom: 10px; margin-top: 20px; margin-bottom: 10px;'>{0}</div>", informacaoAdcional.informacaoAdcionalNome);
                descricaoCompleta.AppendFormat("<div>{0}</div>", informacaoAdcional.informacaoAdcionalConteudo);
            }
            descricaoCompleta = descricaoCompleta.Replace("class=\"templateDetalheTabela\"", "style='color: #333333;font-family: Arial;font-size: 13px;margin: 0;padding: 0;width:100%;font-weight:bold;'");
            descricaoCompleta = descricaoCompleta.Replace("class=\"templateDetalheTituloGeralBloco\"", "style='color: #7D8043;font-size: 14px;font-weight: bold;padding-bottom: 3px;padding-left: 7px;'");
            descricaoCompleta = descricaoCompleta.Replace("class=\"templateDetalheTituloBloco\"", "style='border-bottom: 1px dotted #BEC165;font-size: 16px;font-weight: bold;padding-bottom: 3px;padding-left: 7px;'");
            descricaoCompleta = descricaoCompleta.Replace("class=\"templateDetalheTexto\"", "style='height: 24px;'");
            descricaoCompleta = descricaoCompleta.Replace("class=\"templateDetalheNome\"", "style='padding-bottom: 10px;padding-left: 7px;padding-top: 10px;width: 156px;'");
            descricaoCompleta = descricaoCompleta.Replace("class=\"templateDetalheQuantidade\"", "style='border-left: 1px solid #CCCCCC;border-right: 1px solid #CCCCCC;text-align: center;width: 40px;'");
            descricaoCompleta = descricaoCompleta.Replace("class=\"templateDetalheDescricao\"", "style='padding-left: 7px;'");
            descricaoCompleta = descricaoCompleta.Replace("class=\"templateDetalheTitulo\"", "style='background: none repeat scroll 0 0 #E6E7C4;font-weight: bold;height: 24px;'");
            descricaoCompleta = descricaoCompleta.Replace("class=\"templateDetalheNomeDetalhe\"", "style='padding-bottom: 10px;padding-left: 7px;padding-top: 10px;width: 156px;'");
            descricaoCompleta = descricaoCompleta.Replace("class=\"templateDetalheQuantidadeDetalhe\"", "style='border-left: 1px solid #CCCCCC;border-right: 1px solid #CCCCCC;padding-bottom: 10px;padding-top: 10px;text-align: center;width: 40px;'");
            descricaoCompleta = descricaoCompleta.Replace("class=\"templateDetalheDescricaoDetalhe\"", "style='padding-bottom: 10px;padding-left: 30px;padding-top: 10px;'");

            var produtoImportado = (from c in integracoesDc.tbMlProdutos where c.idProduto == prod.produtoId && c.stop_time > DateTime.Now select c).FirstOrDefault();
            if (prod.produtoAtivo.ToLower() == "true" && prod.marketplaceCadastrar.ToLower() == "true")
            {
                if (produtoImportado == null)
                {
                    var dataProd = new dbCommerceDataContext();
                    var categoria = (from c in dataProd.tbJuncaoProdutoCategorias
                                     where
                                         c.produtoId == prod.produtoId && c.tbProdutoCategoria.exibirSite == true &&
                                         c.tbProdutoCategoria.categoriaPaiId == 0 && c.tbProdutoCategoria.categoriaMl != "" &&
                                         c.tbProdutoCategoria.categoriaMl != null && c.categoriaId != 610
                                     select c).FirstOrDefault();
                    if (categoria != null)
                    {
                        var produtoMl = new mlProduto();

                        string produtoDescricao = descricaoCompleta.ToString();
                        if (string.IsNullOrEmpty(produtoDescricao))
                        {
                            produtoDescricao = prod.produtoDescricao.Replace(Environment.NewLine, "<br />");
                            if (string.IsNullOrEmpty(produtoDescricao)) produtoDescricao = prod.produtoNome;
                        }
                        produtoDescricao = produtoDescricao.Replace(Environment.NewLine, "");
                        produtoDescricao = rnIntegracoes.removeLinks(produtoDescricao);
                        if (string.IsNullOrEmpty(produtoDescricao)) produtoDescricao = prod.produtoNome;
                        produtoDescricao = mlIntegracao.montaHtmlDescricao(prod.disponibilidadeEmEstoque.Replace("mais prazo de entrega exposto no carrinho de compras", "mais prazo de entrega"),
                            produtoDescricao);
                        decimal precoPromocional = prod.produtoPreco;
                        if (prod.produtoPrecoPromocional > 0 && prod.produtoPrecoPromocional < prod.produtoPreco)
                            precoPromocional = (decimal)prod.produtoPrecoPromocional;

                        string produtoNome = string.IsNullOrEmpty(prod.produtoNomeNovo)
                            ? prod.produtoNome
                            : prod.produtoNomeNovo;
                        produtoMl.title = produtoNome;
                        if (mlIntegracao.mlHomologacao) produtoMl.title = "TESTE " + produtoNome;
                        //produtoMl.subtitle = "A maior loja de Decoração de Quarto de Bebê do Brasil";
                        produtoMl.category_id = categoria.tbProdutoCategoria.categoriaMl;
                        produtoMl.price = Convert.ToDecimal(precoPromocional.ToString("0.00"));
                        produtoMl.currency_id = "BRL";
                        int estoque = prod.produtoEstoqueAtual > 999 ? 999 : prod.produtoEstoqueAtual;
                        produtoMl.available_quantity = estoque;
                        produtoMl.buying_mode = "buy_it_now";
                        produtoMl.listing_type_id = "bronze";
                        produtoMl.condition = "new";
                        produtoMl.description = produtoDescricao;
                        produtoMl.video_id = "";
                        produtoMl.warranty = "";

                        /*produtoExtra.Weight = Convert.ToDouble(Convert.ToDecimal(prod.produtoPeso) / 100);
                        produtoExtra.Length = Convert.ToDouble(Convert.ToDecimal(prod.profundidade) / 100);
                        produtoExtra.Width = Convert.ToDouble(Convert.ToDecimal(prod.largura) / 100);
                        produtoExtra.Height = Convert.ToDouble(Convert.ToDecimal(prod.altura) / 100);
                        int prazoAdicional = 0;
                        if (fornecedor.fornecedorPrazoDeFabricacao != null)
                        {
                            prazoAdicional = (int)fornecedor.fornecedorPrazoDeFabricacao;
                        }
                        produtoExtra.handlingTime = prazoAdicional;*/


                        var fotos = new List<mlPicture>();
                        var fotoPrincipal = new mlPicture();
                        fotoPrincipal.source =
                            System.Configuration.ConfigurationManager.AppSettings["caminhoVirtual"].ToString() +
                            "fotos\\" + prod.produtoId + "\\" + prod.fotoDestaque + ".jpg";
                        if (!string.IsNullOrEmpty(fotoPrincipal.source)) fotos.Add(fotoPrincipal);
                        var fotosDc = new dbCommerceDataContext();
                        var produtoFotos =
                            (from c in fotosDc.tbProdutoFotos
                             where c.produtoId == prod.produtoId && c.produtoFoto != prod.fotoDestaque
                             select c);
                        int totalFotosPermitidas = 11;
                        if (prod.produtoBrindes != "")
                        {
                            totalFotosPermitidas = 10;
                        }
                        foreach (var produtoFoto in produtoFotos)
                        {
                            if (fotos.Count < totalFotosPermitidas)
                            {
                                var foto = new mlPicture();
                                foto.source =
                                    System.Configuration.ConfigurationManager.AppSettings["caminhoVirtual"].ToString() +
                                    "fotos/" + prod.produtoId + "/" + produtoFoto.produtoFoto + ".jpg";
                                if (!string.IsNullOrEmpty(foto.source)) fotos.Add(foto);

                            }
                        }
                        if (prod.produtoBrindes != "")
                        {
                            var foto = new mlPicture();
                            foto.source = System.Configuration.ConfigurationManager.AppSettings["caminhoVirtual"].ToString() + "mercadoLivre/brindes" + prod.produtoBrindes + ".jpg";
                            if (!string.IsNullOrEmpty(foto.source)) fotos.Add(foto);

                        }
                        var fotoLogo = new mlPicture();
                        fotoLogo.source = System.Configuration.ConfigurationManager.AppSettings["caminhoVirtual"].ToString() + "imagens/mercadoLivre/logo.jpg";
                        fotos.Add(fotoLogo);

                        produtoMl.pictures = fotos;
                        if (prod.produtoBrindes == "4")
                        {
                            produtoNome = produtoNome + " - 4 brindes";
                        }

                        /*if (produtoMl.price > 499)
                        {
                            produtoMl.shipping = mlIntegracao.retornaShippingGratis(prod.largura, prod.altura,
                                prod.profundidade, Convert.ToInt32(prod.produtoPeso));
                            produtoNome = produtoNome + " - Frete Grátis! Grão de Gente";
                            if (produtoNome.Length > 200) produtoNome.Replace(" - Frete Grátis! Grão de Gente", "");
                        }
                        else
                        {*/
                        produtoMl.shipping = mlIntegracao.retornaShippingPadrao(prod.largura, prod.altura, prod.profundidade, Convert.ToInt32(prod.produtoPeso));
                        produtoNome = produtoNome + "- Grão de Gente";
                        if (produtoNome.Length > 200) produtoNome.Replace(" - Grão de Gente", "");
                       /* }*/

                        produtoMl.title = produtoNome;
                        RestSharp.IRestResponse r = meli.Post("/items", ps, produtoMl);
                        /*Response.Write(r.Content);
                        return;*/
                        HttpContext.Current.Response.Write(r.ToString());

                        dynamic retorno = JsonConvert.DeserializeObject(r.Content);
                        var produtoMlGravar = new tbMlProduto();
                        produtoMlGravar.id = retorno.id;
                        produtoMlGravar.site_id = retorno.site_id;
                        produtoMlGravar.title = retorno.title;
                        produtoMlGravar.sold_quantity = 0;
                        produtoMlGravar.permalink = retorno.permalink;
                        produtoMlGravar.idProduto = prod.produtoId;
                        produtoMlGravar.start_time = Convert.ToDateTime(retorno.start_time);
                        produtoMlGravar.stop_time = Convert.ToDateTime(retorno.stop_time);
                        integracoesDc.tbMlProdutos.InsertOnSubmit(produtoMlGravar);
                        integracoesDc.SubmitChanges();

                    }
                }
                else
                {
                    var dataProd = new dbCommerceDataContext();
                    var categoria = (from c in dataProd.tbJuncaoProdutoCategorias
                                     where
                                         c.produtoId == prod.produtoId && c.tbProdutoCategoria.exibirSite == true &&
                                         c.tbProdutoCategoria.categoriaPaiId == 0 && c.tbProdutoCategoria.categoriaMl != "" &&
                                         c.tbProdutoCategoria.categoriaMl != null && c.categoriaId != 610
                                     select c).FirstOrDefault();
                    if (categoria != null)
                    {
                        var produtoMl = new mlProduto();

                        decimal precoPromocional = prod.produtoPreco;
                        if (prod.produtoPrecoPromocional > 0 && prod.produtoPrecoPromocional < prod.produtoPreco) precoPromocional = (decimal)prod.produtoPrecoPromocional;
                        string produtoNome = string.IsNullOrEmpty(prod.produtoNomeNovo) ? prod.produtoNome : prod.produtoNomeNovo;
                        if (prod.produtoBrindes == "4")
                        {
                            produtoNome = produtoNome + " - 4 brindes";
                        }
                        if (produtoMl.price > 499)
                        {
                            produtoNome = produtoNome + " - Frete Grátis! Grão de Gente";
                        }
                        else
                        {
                            produtoNome = produtoNome + " - Grão de Gente";
                        }

                        produtoMl.title = produtoNome;
                        //produtoMl.subtitle = "A maior loja de Decoração de Quarto de Bebê do Brasil";
                        produtoMl.price = Convert.ToDecimal(precoPromocional.ToString("0.00"));
                        int estoque = prod.produtoEstoqueAtual > 999 ? 999 : prod.produtoEstoqueAtual;
                        produtoMl.available_quantity = estoque;

                        produtoMl.shipping = mlIntegracao.retornaShippingPadrao(prod.largura, prod.altura, prod.profundidade, Convert.ToInt32(prod.produtoPeso));
                        //produtoNome = produtoNome + "- Grão de Gente";
                       // if (produtoNome.Length > 200) produtoNome.Replace(" - Grão de Gente", "");

                        var obj =
                            new
                            {
                                price = Convert.ToDecimal(precoPromocional.ToString("0.00")),
                                available_quantity = estoque,
                                title = produtoNome,
                                category_id = categoria.tbProdutoCategoria.categoriaMl
                            };
                        RestSharp.IRestResponse r = meli.Put("/items/" + produtoImportado.id, ps, obj);
                        HttpContext.Current.Response.Write(r.ToString());
                    }
                }
            }
            else
            {
                if (produtoImportado != null)
                {
                    //Cancelar anuncio
                }
            }
            atual++;
        }
    }

}

public class mlShippingMethodFree
{
    public int id { get; set; }
    public string free { get; set; }
}

public class mlShippingMethod
{
    public int id { get; set; }
    public string free { get; set; }
}

public class mlShippingFree
{
    public bool local_pick_up { get; set; }
    public List<mlShippingMethodFree> methods { get; set; }
    public string dimensions { get; set; }
}

public class mlShipping
{
    public bool local_pick_up { get; set; }
    public string dimensions { get; set; }
    public string mode { get; set; }
    public List<int> methods { get; set; }
}

public class mlPicture
{
    public string source { get; set; }
}

public class mlProduto
{
    public string title { get; set; }
    //public string subtitle { get; set; }
    public string category_id { get; set; }
    public int official_store_id { get; set; } // Item obrigatorio para lojas oficiais
    public decimal price { get; set; }
    public string currency_id { get; set; }
    public int available_quantity { get; set; }
    public string buying_mode { get; set; }
    public string listing_type_id { get; set; }
    public string condition { get; set; }
    public string description { get; set; }
    public string video_id { get; set; }
    public string warranty { get; set; }
    public List<mlPicture> pictures { get; set; }
    public object shipping { get; set; }
}

public class mlOrderPaging
{
    public int total { get; set; }
    public int offset { get; set; }
    public int limit { get; set; }
}

public class mlOrderItem
{
    public string id { get; set; }
    public string title { get; set; }
    public object variation_id { get; set; }
    public List<object> variation_attributes { get; set; }
}

public class mlOrderOrderItem
{
    public mlOrderItem item { get; set; }
    public int quantity { get; set; }
    public double unit_price { get; set; }
    public string currency_id { get; set; }
}

public class mlOrderPhone
{
    public string area_code { get; set; }
    public string number { get; set; }
    public string extension { get; set; }
}

public class mlOrderBillingInfo
{
    public object doc_type { get; set; }
    public object doc_number { get; set; }
}

public class mlOrderBuyer
{
    public int id { get; set; }
    public string nickname { get; set; }
    public string first_name { get; set; }
    public string last_name { get; set; }
    public string email { get; set; }
    public mlOrderPhone phone { get; set; }
    public mlOrderBillingInfo billing_info { get; set; }
}

public class mlOrderPhone2
{
    public string area_code { get; set; }
    public string number { get; set; }
    public string extension { get; set; }
}

public class Seller
{
    public int id { get; set; }
    public string nickname { get; set; }
    public string first_name { get; set; }
    public string last_name { get; set; }
    public string email { get; set; }
    public mlOrderPhone2 phone { get; set; }
}

public class mlOrderFeedback
{
    public object sale { get; set; }
    public object purchase { get; set; }
}

public class mlOrderShipping
{
    public string status { get; set; }
}

public class mlResult
{
    public int id { get; set; }
    public string status { get; set; }
    public object status_detail { get; set; }
    public string date_created { get; set; }
    public string date_closed { get; set; }
    public List<mlOrderOrderItem> order_items { get; set; }
    public double total_amount { get; set; }
    public string currency_id { get; set; }
    public mlOrderBuyer buyer { get; set; }
    public Seller seller { get; set; }
    public List<object> payments { get; set; }
    public mlOrderFeedback feedback { get; set; }
    public mlOrderShipping shipping { get; set; }
    public List<string> tags { get; set; }
}

public class mlSort
{
    public string id { get; set; }
    public string name { get; set; }
}

public class mlAvailableSort
{
    public string id { get; set; }
    public string name { get; set; }
}

public class mlValue
{
    public object id { get; set; }
    public string name { get; set; }
    public int results { get; set; }
}

public class mlAvailableFilter
{
    public string id { get; set; }
    public string name { get; set; }
    public string type { get; set; }
    public List<mlValue> values { get; set; }
}

public class mlPedidos
{
    public string query { get; set; }
    public string display { get; set; }
    public mlOrderPaging paging { get; set; }
    public List<mlResult> results { get; set; }
    public mlSort sort { get; set; }
    public List<mlAvailableSort> available_sorts { get; set; }
    public List<object> filters { get; set; }
    public List<mlAvailableFilter> available_filters { get; set; }
}

public class mlSingleOrderItem
{
    public string id { get; set; }
    public string title { get; set; }
    public object variation_id { get; set; }
    public List<object> variation_attributes { get; set; }
}

public class mlSingleOrderOrderItem
{
    public mlSingleOrderItem item { get; set; }
    public int quantity { get; set; }
    public double unit_price { get; set; }
    public string currency_id { get; set; }
}

public class mlSingleOrderPhone
{
    public string area_code { get; set; }
    public string number { get; set; }
    public object extension { get; set; }
}

public class mlSingleOrderBillingInfo
{
    public string doc_type { get; set; }
    public string doc_number { get; set; }
}

public class mlSingleOrderBuyer
{
    public int id { get; set; }
    public string nickname { get; set; }
    public string email { get; set; }
    public mlSingleOrderPhone phone { get; set; }
    public string first_name { get; set; }
    public string last_name { get; set; }
    public mlSingleOrderBillingInfo billing_info { get; set; }
}

public class mlSingleOrderPhone2
{
    public string area_code { get; set; }
    public string number { get; set; }
    public object extension { get; set; }
}

public class mlSingleOrderSeller
{
    public int id { get; set; }
    public string nickname { get; set; }
    public string email { get; set; }
    public mlSingleOrderPhone2 phone { get; set; }
    public string first_name { get; set; }
    public string last_name { get; set; }
}

public class mlSingleOrderFeedback
{
    public object sale { get; set; }
    public object purchase { get; set; }
}

public class mlSingleOrderState
{
    public string id { get; set; }
    public string name { get; set; }
}

public class mlSingleOrderCity
{
    public string id { get; set; }
    public string name { get; set; }
}

public class mlSingleOrderCountry
{
    public string id { get; set; }
    public string name { get; set; }
}

public class mlSingleOrderNeighborhood
{
    public string id { get; set; }
    public string name { get; set; }
}

public class mlSingleOrderReceiverAddress
{
    public int id { get; set; }
    public string address_line { get; set; }
    public string street_name { get; set; }
    public string street_number { get; set; }
    public string comment { get; set; }
    public string zip_code { get; set; }
    public mlSingleOrderState state { get; set; }
    public object longitude { get; set; }
    public object latitude { get; set; }
    public mlSingleOrderCity city { get; set; }
    public mlSingleOrderCountry country { get; set; }
    public mlSingleOrderNeighborhood neighborhood { get; set; }
}

public class mlSingleOrderShipping
{
    public long id { get; set; }
    public object date_first_printed { get; set; }
    public object shipping_mode { get; set; }
    public object service_id { get; set; }
    public string shipment_type { get; set; }
    public string status { get; set; }
    public string currency_id { get; set; }
    public string date_created { get; set; }
    public mlSingleOrderReceiverAddress receiver_address { get; set; }
    public double cost { get; set; }
}

public class mlSingleOrder
{
    public int id { get; set; }
    public string status { get; set; }
    public object status_detail { get; set; }
    public string date_created { get; set; }
    public string date_closed { get; set; }
    public List<mlSingleOrderOrderItem> order_items { get; set; }
    public double total_amount { get; set; }
    public string currency_id { get; set; }
    public mlSingleOrderBuyer buyer { get; set; }
    public mlSingleOrderSeller seller { get; set; }
    public List<object> payments { get; set; }
    public mlSingleOrderFeedback feedback { get; set; }
    public mlSingleOrderShipping shipping { get; set; }
    public List<string> tags { get; set; }
}