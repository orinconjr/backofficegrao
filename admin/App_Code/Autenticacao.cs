﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

/// <summary>
/// Summary description for Autenticacao
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class Autenticacao : System.Web.Services.WebService
{

    public Autenticacao()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public int ValidaLoginAdmin(string login, string senha, string paginaAutorizacao)
    {
        if (rnUsuarios.usuarioSeleciona_PorUsuarioNomeUsuarioSenha(login, senha).Tables[0].Rows.Count > 0)
        {
            string usuarioId = rnUsuarios.usuarioSeleciona_PorUsuarioNomeUsuarioSenha(login, senha).Tables[0].Rows[0]["usuarioId"].ToString();
            if (!string.IsNullOrEmpty(paginaAutorizacao))
            {
                if (rnUsuarios.usuarioPaginasPermitidaSeleciona_PorUsuarioIdPaginaPermitidaNome(int.Parse(usuarioId), paginaAutorizacao).Tables[0].Rows.Count > 0)
                {
                    return Convert.ToInt32(usuarioId);
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(usuarioId))
                {
                    return Convert.ToInt32(usuarioId);
                }

            }
        }

        return 0;
    }

    [WebMethod]
    public List<int> ValidaLoginAppFornecedor(string login, string senha)
    {
        tbFornecedorUsuario usuarioFornecedor;

        using (var data = new dbCommerceDataContext())
        {
            usuarioFornecedor =
                (from c in data.tbFornecedorUsuarios where c.nome == login && c.senha == senha && c.imprimir select c).FirstOrDefault();
        }

        List<int> dadosUsuario = new List<int>();

        if (usuarioFornecedor != null)
        {
            dadosUsuario.Add(usuarioFornecedor.idFornecedorUsuario);
            dadosUsuario.Add(usuarioFornecedor.idFornecedor);
        }

        return dadosUsuario;
    }

    [WebMethod]
    public int ValidaRomaneioAppFornecedor(int idFornecedor, int romaneio)
    {
        tbPedidoFornecedor romaneioFornecedor;

        using (var data = new dbCommerceDataContext())
        {
            romaneioFornecedor =
                (from c in data.tbPedidoFornecedors where c.idFornecedor == idFornecedor && c.idPedidoFornecedor == romaneio select c).FirstOrDefault();
        }

        return romaneioFornecedor != null ? 1 : 0;
    }

    [WebMethod]
    public int ValidaEtiquetaAppFornecedor(int idFornecedor, int etiqueta)
    {
        tbPedidoFornecedorItem etiquetaFornecedor;

        using (var data = new dbCommerceDataContext())
        {
            etiquetaFornecedor =
                (from c in data.tbPedidoFornecedorItems
                 where c.tbPedidoFornecedor.idFornecedor == idFornecedor && c.idPedidoFornecedorItem == etiqueta
                 select c).FirstOrDefault();
        }

        return etiquetaFornecedor != null ? 1 : 0;
    }
}
