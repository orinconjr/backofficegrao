﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Xml;
using Telerik.Web.UI;


/// <summary>
/// Summary description for rnNotaFiscal
/// </summary>
/// 

public class listaPedidosIbgs
{
    public int pedidoId { get; set; }
    public string codigoIbge { get; set; }
}

public class rnNotaFiscal
{
    #region CNPJ Mayara
    //private static string cUF = "35";
    //private static string cMunFG = "3552700";
    //public static string tpAmb = "1";
    //public static string cnpjEmitente = "20907518000110";
    //private static string nomeEmitente = "MAYARA CAROLINA FERRO ME";
    //private static string nomeFantasiaEmitente = "GRAO DE GENTE";
    //private static string logradouroEmitente = "RUA RODRIGUES ALVES";
    //private static string numeroEmitente = "27";
    //private static string bairroEmitente = "CENTRO";
    //private static string codigoMunicipioEmitente = "3552700";
    //private static string municipioEmitente = "TABATINGA";
    //private static string ufEmitente = "SP";
    //private static string cepEmitente = "14910000";
    //private static string foneEmitente = "1135228379";
    //private static string ieEmitente = "674017393110";
    //public static string chaveDeAcesso = "9c5gKp3l+8aUBnL8QzwRkKaEXKqFxO3r";
    //public static string EmpPK = "6rRJmEhQmdMKCIcVwD22vw==";
    #endregion

    #region CNPJ Lindsay
    //private static string cUF = "35";
    //private static string cMunFG = "3548500";
    public static string tpAmb = "1";
    //public static string cnpjEmitente = "10924051000163";
    //private static string nomeEmitente = "LINDSAY FERRANDO ME";
    //private static string nomeFantasiaEmitente = "GRAO DE GENTE";
    //private static string logradouroEmitente = "AV ANA COSTA";
    //private static string numeroEmitente = "416";
    //private static string bairroEmitente = "GONZAGA";
    //private static string codigoMunicipioEmitente = "3552700";
    //private static string municipioEmitente = "SANTOS";
    //private static string ufEmitente = "SP";
    //private static string cepEmitente = "11060002";
    //private static string foneEmitente = "1135228379";
    //private static string ieEmitente = "633459940118";

    //public static string chaveDeAcesso = "VfIVYjA/xTjRVKoNFshvHSrWZqanJ3bg";
    //public static string chaveDeAcesso = "bkXnxecdoeQNOHCQhKIk85sRPXzbVNNK";
    //public static string EmpPK = "6rRJmEhQmdMKCIcVwD22vw==";
    #endregion

    public static string transportadora { get; set; }
    public static DateTime? dataSaida { get; set; }

    public static bool gerarNotaFiscalEmLote(List<listaPedidosIbgs> listaPedidos)
    {
        foreach (var listaPedido in listaPedidos)
        {
            //gerarNotaFiscalEmLote(listaPedido.pedidoId, listaPedido.codigoIbge);
        }

        return true;
    }

    #region gerarNotaFiscal
    /************************************gerarNotaFiscal Inicio********************************************************/
    public static notaGerada gerarNotaFiscal(int idPedido, string codigoIbge, List<int> itensMarcados, bool gerarNovaNota)
    {
        return gerarNotaFiscal(idPedido, codigoIbge, itensMarcados, gerarNovaNota, 0);
    }

    public static notaGerada gerarNotaFiscal(int idPedido, string codigoIbge, List<int> itensMarcados, bool gerarNovaNota, int idPedidoEnvio)
    {
        bool totalBrinde = true;
        bool totalReposicao = true;

        string cfop = "";
        var pedidoDc = new dbCommerceDataContext();
        var pedido = (from c in pedidoDc.tbPedidos where c.pedidoId == idPedido select c).FirstOrDefault();
        //var envio = (from c in pedidoDc.tbPedidoEnvios where c.idPedidoEnvio == idPedidoEnvio select c).FirstOrDefault();

        if (pedido == null) return new notaGerada();
        int pedidoId = pedido.pedidoId;

        var clienteDc = new dbCommerceDataContext();
        var cliente = (from c in clienteDc.tbClientes where c.clienteId == pedido.clienteId select c).FirstOrDefault();

        var gerarNota = new tbNotaFiscal();
        if (idPedidoEnvio > 0) gerarNota = gerarNumeroNota(pedido.pedidoId, idPedidoEnvio, gerarNovaNota, pedido.endEstado.ToLower());
        else gerarNota = gerarNumeroNota(pedido.pedidoId, 0, gerarNovaNota, pedido.endEstado.ToLower());


        if ((gerarNota.complemento ?? false) == true)
        {
            return gerarNotaFiscalComplemento((int)gerarNota.numeroNotaComplemento, (int)gerarNota.idCNPJ, codigoIbge);
        }
        var empresaNota = (from c in pedidoDc.tbEmpresas where c.idEmpresa == gerarNota.idCNPJ select c).First();

        string numeroNota = gerarNota.numeroNota.ToString();
        string chave = gerarNota.nfeKey;
        string cDV = GerarDigitoVerificadorNFe(chave).ToString();
        chave += cDV;

        string idDest = "";

        bool gerarGnre = false;
        bool pagarGnre = false;
        var configuracaoGnre = (from c in pedidoDc.tbGnreConfiguracaos where c.uf == pedido.endEstado.ToLower() select c).FirstOrDefault();
        if (configuracaoGnre != null)
        {
            gerarGnre = (configuracaoGnre.gerarGnre ?? false);
            pagarGnre = (configuracaoGnre.pagamentoGnre ?? false);
        }

        List<string> ufICMSInterestadualValidar = new List<string> { "am", "ba", "ce", "go", "mg", "ms", "mt", "pe", "rn", "se", "sp","pa" };

        #region validações para o endereço
        string cpfCnpj = Regex.Replace(cliente.clienteCPFCNPJ, "[^0-9]+", "").Trim().TrimEnd();

        var validaCodigoIbge = (from c in clienteDc.tbCepCidades where c.cod_ibge == codigoIbge select c).FirstOrDefault();

        string ufCodigoIbge = pedido.endEstado.Trim().TrimEnd();

        if (validaCodigoIbge != null)
            ufCodigoIbge = validaCodigoIbge.uf;

        string clienteEstado = string.IsNullOrEmpty(pedido.endEstado.Trim().TrimEnd()) ? ufCodigoIbge : pedido.endEstado.Trim().TrimEnd();

        if (clienteEstado.ToLower() != ufCodigoIbge.ToLower())
            clienteEstado = ufCodigoIbge;

        string clienteBairro = Regex.Replace(pedido.endBairro, "[^0-9 a-zA-Z]+", "").TrimEnd().TrimStart();
        if (clienteBairro.Length > 60)
            clienteBairro = clienteBairro.Substring(0, 58).TrimEnd();

        if (clienteBairro.Length < 5)
            clienteBairro = "Bairro: " + clienteBairro;

        #endregion validações para o endereço

        if (empresaNota.idEmpresa == 2)
        {
            if (cpfCnpj.Length == 11 && clienteEstado.ToLower() == "sp")
            {
                cfop = "5101";
                idDest = "1";
            }
            else if (cpfCnpj.Length > 11 && clienteEstado.ToLower() == "sp")
            {
                cfop = "5101";
                idDest = "1";
            }
            else if (cpfCnpj.Length == 11 && clienteEstado.ToLower() != "sp")
            {
                //cfop = "6101"; alterado cfop a pedido do financeiro - 13/06/2016
                cfop = "6107";
                idDest = "2";
            }
            else
            {
                cfop = "6101";
                idDest = "2";
            }
        }
        else if (empresaNota.idEmpresa == 6)
        {
            if (cpfCnpj.Length == 11 && clienteEstado.ToLower() == "mg")
            {
                //cfop = "5403";
                //cfop = "5405"; // alterado a pedido ca contabilidade
                cfop = "5102"; // alterado a pedido ca contabilidade - 02/01/2016

                idDest = "1";
            }
            else if (cpfCnpj.Length > 11 && clienteEstado.ToLower() == "mg")
            {
                cfop = "5102";
                idDest = "1";
            }
            else if (cpfCnpj.Length == 11 && clienteEstado.ToLower() != "mg")
            {
                cfop = "6108";
                idDest = "2";
            }
            else
            {
                cfop = "6102";
                idDest = "2";
            }
        }
        else
        {
            if (cpfCnpj.Length == 11 && clienteEstado.ToLower() == "sp")
            {
                //cfop = "5403";
                //cfop = "5405"; // alterado a pedido ca contabilidade
                cfop = "5102"; // alterado a pedido ca contabilidade - 02/01/2016

                idDest = "1";
            }
            else if (cpfCnpj.Length > 11 && clienteEstado.ToLower() == "sp")
            {
                cfop = "5102";
                idDest = "1";
            }
            else if (cpfCnpj.Length == 11 && clienteEstado.ToLower() != "sp")
            {
                cfop = "6108";
                idDest = "2";
            }
            else
            {
                cfop = "6102";
                idDest = "2";
            }
        }



        var nota = new StringBuilder();


        #region Identificadores da NF-e
        //nota.Append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        //nota.Append("<NFe xmlns=\"http://www.portalfiscal.inf.br/nfe\">");
        //nota.AppendFormat("<infNFe versao=\"2.00\" Id=\"NFe{0}\">", chave);
        nota.Append("<ide>");
        nota.AppendFormat("<cUF>{0}</cUF>", empresaNota.codigoUfEmitente);
        nota.AppendFormat("<cNF>{0}</cNF>", pedidoId.ToString().PadLeft(8, '0'));

        if (empresaNota.idEmpresa == 2)
        {
            if ((cliente.clienteCPFCNPJ.Length == 11 | cliente.clienteCPFCNPJ.Length > 11) && pedido.endEstado.ToLower() == "sp")
            {
                nota.Append("<natOp>Venda de Prod. do Estab. Independente substituicao</natOp>");
            }
            else if (pedido.endEstado.ToLower() != "sp")
            {
                //nota.Append("<natOp>Venda de Produção de Estabelecimento Com Produtos Sujeitos ao Regime de Substituição Tributária</natOp>");
                nota.Append("<natOp>Venda Producao do Estabelecimento Destinada nao Contribuinte</natOp>");
            }
            else
            {
                //nota.Append("<natOp>Venda de Produção de Estabelecimento Com Produtos Sujeitos ao Regime de Substituição Tributária</natOp>");
                nota.Append("<natOp>Venda Producao do Estabelecimento Destinada nao Contribuinte</natOp>");
            }
        }
        else
        {
            nota.Append("<natOp>VENDA DE MERCADORIA</natOp>");
        }

        //nota.Append("<natOp>VENDA DE MERCADORIA</natOp>");
        nota.Append("<indPag>0</indPag>");
        nota.Append("<mod>55</mod>");
        nota.Append("<serie>1</serie>");
        nota.AppendFormat("<nNF>{0}</nNF>", numeroNota);
        nota.AppendFormat("<dhEmi>{0}T{1}</dhEmi>", DateTime.Now.ToString("yyyy-MM-dd"), DateTime.Now.ToString("HH:mm:ss"));
        nota.Append("<fusoHorario>-02:00</fusoHorario>");
        nota.Append("<tpNf>1</tpNf>");
        nota.AppendFormat("<idDest>{0}</idDest>", idDest);
        nota.AppendFormat("<indFinal>1</indFinal>");
        nota.AppendFormat("<indPres>2</indPres>");
        nota.AppendFormat("<cMunFg>{0}</cMunFg>", empresaNota.codigoMunicipioEmitente);
        nota.Append("<tpImp>1</tpImp>");
        nota.Append("<tpEmis>1</tpEmis>");
        nota.AppendFormat("<tpAmb>{0}</tpAmb>", tpAmb);
        nota.Append("<finNFe>1</finNFe>");
        nota.Append("</ide>");
        #endregion

        #region Emitente

        nota.Append("<emit>");
        nota.AppendFormat("<CNPJ_emit>{0}</CNPJ_emit>", empresaNota.cnpj);
        nota.AppendFormat("<xNome>{0}</xNome>", empresaNota.nomeEmitente);
        nota.AppendFormat("<xFant>{0}</xFant>", empresaNota.nomeFantasiaEmitente);
        nota.Append("<enderEmit>");
        nota.AppendFormat("<xLgr>{0}</xLgr>", empresaNota.logradouroEmitente);
        nota.AppendFormat("<nro>{0}</nro>", empresaNota.numeroEmitente);
        nota.AppendFormat("<xBairro>{0}</xBairro>", empresaNota.bairroEmitente);
        nota.AppendFormat("<cMun>{0}</cMun>", empresaNota.codigoMunicipioEmitente);
        nota.AppendFormat("<xMun>{0}</xMun>", empresaNota.municipioEmitente);
        nota.AppendFormat("<UF>{0}</UF>", empresaNota.ufEmitente);
        nota.AppendFormat("<CEP>{0}</CEP>", empresaNota.cepEmitente);
        nota.Append("<cPais>1058</cPais>");
        nota.Append("<xPais>BRASIL</xPais>");
        nota.AppendFormat("<fone>{0}</fone>", empresaNota.foneEmitente);
        nota.Append("</enderEmit>");
        nota.AppendFormat("<IE>{0}</IE>", empresaNota.ieEmitente);
        if (!string.IsNullOrEmpty(empresaNota.ieES) && pedido.endEstado.Trim().ToLower() == "es")
        {
            nota.AppendFormat("<IEST>{0}</IEST>", empresaNota.ieES);
        }
        else if (!string.IsNullOrEmpty(empresaNota.ieSP) && pedido.endEstado.Trim().ToLower() == "sp")
        {
            nota.AppendFormat("<IEST>{0}</IEST>", empresaNota.ieSP);
        }
        else if (!string.IsNullOrEmpty(empresaNota.ieMA) && pedido.endEstado.Trim().ToLower() == "ma")
        {
            nota.AppendFormat("<IEST>{0}</IEST>", empresaNota.ieMA);
        }
        else if (!string.IsNullOrEmpty(empresaNota.iePR) && pedido.endEstado.Trim().ToLower() == "pr")
        {
            nota.AppendFormat("<IEST>{0}</IEST>", empresaNota.iePR);
        }
        else if (!string.IsNullOrEmpty(empresaNota.ieDF) && pedido.endEstado.Trim().ToLower() == "df")
        {
            nota.AppendFormat("<IEST>{0}</IEST>", empresaNota.ieDF);
        }
        else if (!string.IsNullOrEmpty(empresaNota.ieSE) && pedido.endEstado.Trim().ToLower() == "se")
        {
            nota.AppendFormat("<IEST>{0}</IEST>", empresaNota.ieSE);
        }
        else if (!string.IsNullOrEmpty(empresaNota.ieTO) && pedido.endEstado.Trim().ToLower() == "to")
        {
            nota.AppendFormat("<IEST>{0}</IEST>", empresaNota.ieTO);
        }
        else if (!string.IsNullOrEmpty(empresaNota.ieRJ) && pedido.endEstado.Trim().ToLower() == "rj")
        {
            nota.AppendFormat("<IEST>{0}</IEST>", empresaNota.ieRJ);
        }
        else if (!string.IsNullOrEmpty(empresaNota.iePB) && pedido.endEstado.Trim().ToLower() == "pb")
        {
            nota.AppendFormat("<IEST>{0}</IEST>", empresaNota.iePB);
        }
        else if (!string.IsNullOrEmpty(empresaNota.ieMT) && pedido.endEstado.Trim().ToLower() == "mt")
        {
            nota.AppendFormat("<IEST>{0}</IEST>", empresaNota.ieMT);
        }
        else if (!string.IsNullOrEmpty(empresaNota.ieRS) && pedido.endEstado.Trim().ToLower() == "rs")
        {
            nota.AppendFormat("<IEST>{0}</IEST>", empresaNota.ieRS);
        }
        else if (!string.IsNullOrEmpty(empresaNota.ieAP) && pedido.endEstado.Trim().ToLower() == "ap")
        {
            nota.AppendFormat("<IEST>{0}</IEST>", empresaNota.ieAP);
        }
        else if (!string.IsNullOrEmpty(empresaNota.ieSC) && pedido.endEstado.Trim().ToLower() == "sc")
        {
            nota.AppendFormat("<IEST>{0}</IEST>", empresaNota.ieSC);
        }
        else if (!string.IsNullOrEmpty(empresaNota.ieCE) && pedido.endEstado.Trim().ToLower() == "ce")
        {
            nota.AppendFormat("<IEST>{0}</IEST>", empresaNota.ieCE);
        }
        else if (!string.IsNullOrEmpty(empresaNota.ieBA) && pedido.endEstado.Trim().ToLower() == "ba")
        {
            nota.AppendFormat("<IEST>{0}</IEST>", empresaNota.ieBA);
        }
        else if (!string.IsNullOrEmpty(empresaNota.ieMG) && pedido.endEstado.Trim().ToLower() == "mg")
        {
            nota.AppendFormat("<IEST>{0}</IEST>", empresaNota.ieMG);
        }
        else if (!string.IsNullOrEmpty(empresaNota.ieMS) && pedido.endEstado.Trim().ToLower() == "ms")
        {
            nota.AppendFormat("<IEST>{0}</IEST>", empresaNota.ieMS);
        }
        else if (!string.IsNullOrEmpty(empresaNota.iePE) && pedido.endEstado.Trim().ToLower() == "pe")
        {
            nota.AppendFormat("<IEST>{0}</IEST>", empresaNota.iePE);
        }
        else if (!string.IsNullOrEmpty(empresaNota.ieAM) && pedido.endEstado.Trim().ToLower() == "am")
        {
            nota.AppendFormat("<IEST>{0}</IEST>", empresaNota.ieAM);
        }
        else if (!string.IsNullOrEmpty(empresaNota.ieRN) && pedido.endEstado.Trim().ToLower() == "rn")
        {
            nota.AppendFormat("<IEST>{0}</IEST>", empresaNota.ieRN);
        }
        else if (!string.IsNullOrEmpty(empresaNota.ieAC) && pedido.endEstado.Trim().ToLower() == "ac")
        {
            nota.AppendFormat("<IEST>{0}</IEST>", empresaNota.ieAC);
        }
        else if (!string.IsNullOrEmpty(empresaNota.iePA) && pedido.endEstado.Trim().ToLower() == "pa")
        {
            nota.AppendFormat("<IEST>{0}</IEST>", empresaNota.iePA);
        }
        else if (!string.IsNullOrEmpty(empresaNota.ieGO) && pedido.endEstado.Trim().ToLower() == "go")
        {
            nota.AppendFormat("<IEST>{0}</IEST>", empresaNota.ieGO);
        }
        else if (!string.IsNullOrEmpty(empresaNota.iePI) && pedido.endEstado.Trim().ToLower() == "pi")
        {
            nota.AppendFormat("<IEST>{0}</IEST>", empresaNota.iePI);
        }
        else if (!string.IsNullOrEmpty(empresaNota.ieRO) && pedido.endEstado.Trim().ToLower() == "ro")
        {
            nota.AppendFormat("<IEST>{0}</IEST>", empresaNota.ieRO);
        }
        else if (!string.IsNullOrEmpty(empresaNota.ieAL) && pedido.endEstado.Trim().ToLower() == "al")
        {
            nota.AppendFormat("<IEST>{0}</IEST>", empresaNota.ieAL);
        }
        else if (!string.IsNullOrEmpty(empresaNota.ieRR) && pedido.endEstado.Trim().ToLower() == "rr")
        {
            nota.AppendFormat("<IEST>{0}</IEST>", empresaNota.ieRR);
        }
        if (empresaNota.simples) nota.Append("<CRT>1</CRT>");
        else nota.Append("<CRT>3</CRT>");
        nota.Append("</emit>");

        #endregion


        #region Destinatario

        nota.Append("<dest>");

        if (cpfCnpj.Length == 11)
        {
            nota.AppendFormat("<CPF_dest>{0}</CPF_dest>", cpfCnpj);
            if (tpAmb == "2")
            {
                nota.Append("<xNome_dest>NF-E EMITIDA EM AMBIENTE DE HOMOLOGACAO - SEM VALOR FISCAL</xNome_dest>");
            }
            else
            {
                nota.AppendFormat("<xNome_dest>{0}</xNome_dest>", Regex.Replace(cliente.clienteNome, "[^0-9 a-zA-ZÀ-ú]+", "").TrimEnd().TrimStart());
            }
        }
        else
        {
            nota.AppendFormat("<CNPJ_dest>{0}</CNPJ_dest>", cpfCnpj);
            if (tpAmb == "2")
            {
                nota.Append("<xNome_dest>NF-E EMITIDA EM AMBIENTE DE HOMOLOGACAO - SEM VALOR FISCAL</xNome_dest>");
            }
            else
            {
                nota.AppendFormat("<xNome_dest>{0}</xNome_dest>", string.IsNullOrEmpty(cliente.clienteNomeDaEmpresa) ? Regex.Replace(cliente.clienteNome, "[^0-9 a-zA-ZÀ-ú]+", "").TrimEnd().TrimStart() : Regex.Replace(cliente.clienteNomeDaEmpresa, "[^0-9 a-zA-Z]+", "").TrimEnd().TrimStart());
            }
        }


        string foneNfe = "";
        try
        {
            if (!string.IsNullOrEmpty(cliente.clienteFoneComercial) && cliente.clienteFoneComercial.Length > 9) foneNfe = cliente.clienteFoneComercial.TrimEnd();
            if (!string.IsNullOrEmpty(cliente.clienteFoneResidencial) && cliente.clienteFoneResidencial.Length > 9) foneNfe = cliente.clienteFoneResidencial.TrimEnd();
            if (!string.IsNullOrEmpty(cliente.clienteFoneCelular) && cliente.clienteFoneCelular.Length > 9) foneNfe = cliente.clienteFoneCelular.TrimEnd();
            if (string.IsNullOrEmpty(foneNfe)) foneNfe = "9999-9999";
        }
        catch (Exception)
        {
            if (string.IsNullOrEmpty(foneNfe)) foneNfe = "9999-9999";
        }

        string pedidoEnderecoTratado = Regex.Replace(rnFuncoes.removeCaracteresEspeciaisDeixaEspaco(rnFuncoes.limpaStringDeixaEspaco(pedido.endRua)), "[^0-9 a-zA-Z]+", "").ToUpper().Trim().TrimEnd().TrimStart();
        string pedidoComplementoTratado = Regex.Replace(rnFuncoes.removeCaracteresEspeciaisDeixaEspaco(rnFuncoes.limpaStringDeixaEspaco(pedido.endComplemento ?? "")), "[^0-9 a-zA-Z]+", "").ToUpper().Trim().TrimEnd().TrimStart();

        nota.Append("<enderDest>");
        nota.AppendFormat("<xLgr_dest>{0}</xLgr_dest>", pedidoEnderecoTratado.Length < 5 ? "Rua " + pedidoEnderecoTratado.Trim().TrimEnd() : pedidoEnderecoTratado.Length > 59 ? pedidoEnderecoTratado.Substring(0, 58).TrimEnd() : pedidoEnderecoTratado);
        nota.AppendFormat("<nro_dest>{0}</nro_dest>", pedido.endNumero.Trim());
        if (!string.IsNullOrEmpty(pedido.endComplemento)) nota.AppendFormat("<xCpl_dest>{0}</xCpl_dest>", pedidoComplementoTratado.Length > 59 ? pedidoComplementoTratado.Substring(0, 58).TrimEnd() : pedidoComplementoTratado);
        nota.AppendFormat("<xBairro_dest>{0}</xBairro_dest>", clienteBairro);
        nota.AppendFormat("<cMun_dest>{0}</cMun_dest>", codigoIbge);
        nota.AppendFormat("<xMun_dest>{0}</xMun_dest>", pedido.endCidade.Trim().TrimEnd());
        nota.AppendFormat("<UF_dest>{0}</UF_dest>", clienteEstado);
        nota.AppendFormat("<CEP_dest>{0}</CEP_dest>", Regex.Replace(pedido.endCep, "[^0-9]+", "").Trim().TrimEnd());
        nota.Append("<cPais_dest>1058</cPais_dest>");
        nota.Append("<xPais_dest>BRASIL</xPais_dest>");
        if (!string.IsNullOrEmpty(foneNfe)) nota.AppendFormat("<fone_dest>{0}</fone_dest>", Regex.Replace(foneNfe, "[^0-9]+", "").Replace(" ", "").Trim().TrimEnd());
        nota.Append("</enderDest>");

        if (cpfCnpj.Length == 11)
        {
            //nota.Append("<IE_dest>ISENTO</IE_dest>");
            if (pedido.endEstado.ToLower() != "sp")
            {
                nota.Append("<indIEDest>9</indIEDest>");
            }
            else if (pedido.endEstado.ToLower() == "sp")
            {
                nota.Append("<indIEDest>9</indIEDest>");
            }
            else
            {
                nota.Append("<indIEDest>2</indIEDest>");
            }
        }
        else
        {
            if (!string.IsNullOrEmpty(cliente.clienteRGIE))
            {
                nota.Append("<indIEDest>1</indIEDest>");
                nota.AppendFormat("<IE_dest>{0}</IE_dest>", cliente.clienteRGIE.Trim());
            }
            else
            {

                if (ufICMSInterestadualValidar.Contains(pedido.endEstado.ToLower()))
                {
                    nota.Append("<indIEDest>9</indIEDest>");
                }
                else if (pedido.endEstado.ToLower() == "sp")
                {
                    nota.Append("<indIEDest>9</indIEDest>");
                }
                else
                {
                    nota.Append("<IE_dest></IE_dest>");
                    nota.Append("<indIEDest>2</indIEDest>");
                }


            }
        }
        nota.AppendFormat("<Email_dest>{0}</Email_dest>", cliente.clienteEmail.Trim());
        nota.Append("</dest>");
        #endregion

        #region Autorizacao Emissao
        nota.Append("<autXML>");
        nota.Append("<autXMLItem>");
        nota.AppendFormat("<CNPJ_aut>{0}</CNPJ_aut>", empresaNota.cnpj);
        nota.Append("</autXMLItem>");
        nota.Append("</autXML>");
        #endregion

        #region Detalhamento dos Itens

        nota.Append("<det>");

        List<tbNotaFiscalItem> notaFiscalItens = new List<tbNotaFiscalItem>();
        int nItem = 1;
        var itensDc = new dbCommerceDataContext();
        decimal valorDoDescontoDoCupom = pedido.valorDoDescontoDoCupom > 0 ? (decimal)pedido.valorDoDescontoDoCupom : 0;
        decimal valorDoDescontoDoPagamento = pedido.valorDoDescontoDoPagamento > 0 ? (decimal)pedido.valorDoDescontoDoPagamento : 0;
        decimal totalDesconto = valorDoDescontoDoCupom + valorDoDescontoDoPagamento;
        decimal valorDoDesconto = 0;
        decimal totalDoPedidoUnitario = 0;
        decimal totalDoPedido = 0;
        decimal totalDescontoCalculado = 0;
        decimal totalIcmsOrigem = 0;
        decimal totalIcmsDestino = 0;
        decimal totalIcms = 0;
        decimal totalPis = 0;
        decimal totalCofins = 0;

        decimal totalDoFrete = pedido.valorDoFrete > 0 ? (decimal)pedido.valorDoFrete : 0;
        decimal valorDoFrete = 0;
        decimal totalDoFreteCalculado = 0;

        var produtosNaoEnviados = (from c in pedidoDc.tbItemPedidoEstoques
                                   join d in pedidoDc.tbItensPedidos on c.itemPedidoId equals d.itemPedidoId
                                   where d.pedidoId == pedidoId && c.cancelado == false && c.reservado == true
                                   select new { itemPedidoId = (int)d.itemPedidoId, c.produtoId }).ToList();

        decimal valorPagoPedido = (from c in pedidoDc.tbItemPedidoEstoques
                                   join d in pedidoDc.tbItensPedidos on c.itemPedidoId equals d.itemPedidoId
                                   where d.pedidoId == pedidoId && c.cancelado == false && c.reservado == true
                                   select new { d.produtoId, itemValor = (d.itemValor * d.itemQuantidade) }).Sum(x => x.itemValor);

        using (var txn = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required,
            new System.Transactions.TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted }))
        {


            if (!produtosNaoEnviados.Any())//reenvio de nota rejeitada pelo invoicy
            {
                produtosNaoEnviados = (from c in pedidoDc.tbItemPedidoEstoques
                                       join d in pedidoDc.tbItensPedidos on c.itemPedidoId equals d.itemPedidoId
                                       where d.pedidoId == pedidoId && c.cancelado == false && c.reservado == true
                                       select new { itemPedidoId = (int)d.itemPedidoId, c.produtoId }).ToList();
            }

            if (itensMarcados.Any())
            {
                produtosNaoEnviados = (from c in pedidoDc.tbItemPedidoEstoques
                                       join d in pedidoDc.tbItensPedidos on c.itemPedidoId equals d.itemPedidoId
                                       where d.pedidoId == pedidoId && itensMarcados.Contains(c.produtoId)
                                       select new { itemPedidoId = (int)d.itemPedidoId, c.produtoId }).ToList();
            }



            if (idPedidoEnvio > 0 | (gerarNota.idPedidoEnvio ?? 0) > 0)
            {
                int idEnvio = idPedidoEnvio;
                if ((gerarNota.idPedidoEnvio ?? 0) > 0) idEnvio = (int)gerarNota.idPedidoEnvio;
                produtosNaoEnviados = (from c in pedidoDc.tbItemPedidoEstoques
                                       join d in pedidoDc.tbProdutoEstoques on c.idItemPedidoEstoque equals d.idItemPedidoEstoque
                                       where d.idPedidoEnvio == idEnvio
                                       select new { itemPedidoId = (int)c.itemPedidoId, c.produtoId }).ToList();

                if (!produtosNaoEnviados.Any()) //reenvio de nota rejeitada pelo invoicy
                {
                    produtosNaoEnviados = (from c in pedidoDc.tbItemPedidoEstoques
                                           join d in pedidoDc.tbItensPedidos on c.itemPedidoId equals d.itemPedidoId
                                           where d.pedidoId == pedidoId && c.cancelado == false && c.reservado == true
                                           select new { itemPedidoId = (int)d.itemPedidoId, c.produtoId }).ToList();

                    if (itensMarcados.Any())
                    {
                        produtosNaoEnviados = (from c in pedidoDc.tbItemPedidoEstoques
                                               join d in pedidoDc.tbItensPedidos on c.itemPedidoId equals d.itemPedidoId
                                               where d.pedidoId == pedidoId && itensMarcados.Contains(c.produtoId)
                                               select new { itemPedidoId = (int)d.itemPedidoId, c.produtoId }).ToList();
                    }
                }


            }

        }

        var produtosGeral = (from c in pedidoDc.tbItemPedidoEstoques
                             join d in pedidoDc.tbItensPedidos on c.itemPedidoId equals d.itemPedidoId
                             where d.pedidoId == pedidoId
                             select new { itemPedidoId = (int)d.itemPedidoId, c.produtoId }).ToList();

        var itensPedidoGeral = (from c in itensDc.tbItensPedidos where c.pedidoId == pedidoId select c).ToList();

        List<tbPedidoPacote> pacotes;

        using (var txn = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required, new System.Transactions.TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted }))
        {

            pacotes = (from c in pedidoDc.tbPedidoPacotes where c.idPedido == idPedido select c).ToList();
            if (idPedidoEnvio != 0)
            {
                pacotes = (from c in pedidoDc.tbPedidoPacotes where c.idPedidoEnvio == idPedidoEnvio select c).ToList();
            }

        }

        decimal valorFrete = 0;

        decimal pesoTotal = 0;
        if (pacotes.Any())
        {
            pesoTotal = pacotes.Sum(x => x.peso);
        }
        else
        {
            foreach (var item in itensPedidoGeral)
            {
                var produtoDc = new dbCommerceDataContext();
                var produto = (from c in produtoDc.tbProdutos where c.produtoId == item.produtoId select c).First();
                pesoTotal += Convert.ToDecimal(produto.produtoPeso);
            }
        }

        int indiceProduto = 0;

        int qtdTotalProdutos = produtosNaoEnviados.Count;
        int qtdTotalBrindes = 0;
        int qtdTotalReposicao = 0;
        string estado = pedido.endEstado.Trim().ToLower();
        decimal vBCAcumulador = 0;
        foreach (var produtoEnviado in produtosNaoEnviados)
        {
            var produtoDc = new dbCommerceDataContext();
            var produto = (from c in produtoDc.tbProdutos where c.produtoId == produtoEnviado.produtoId select c).First();
            var itemPedido = itensPedidoGeral.FirstOrDefault(x => x.itemPedidoId == produtoEnviado.itemPedidoId);

            if (itemPedido == null)
                continue;

            bool cancelado = itemPedido.cancelado == null ? false : (bool)itemPedido.cancelado;
            bool brinde = itemPedido.brinde == null ? false : (bool)itemPedido.brinde;
            bool reposicao = false;
            if (itemPedido.tbItemPedidoTipoAdicao != null)
            {
                if (itemPedido.tbItemPedidoTipoAdicao.brinde) brinde = true;
                if (itemPedido.tbItemPedidoTipoAdicao.reposicao) reposicao = true;
            }
            decimal valorDoProduto = 0;
            string cfopProduto = cfop;
            decimal freteDoProduto = 0;
            decimal valorDesconto = 0;

            decimal valorFrete_ = 0;
            if (pedido.valorDoFrete > 0 && indiceProduto == 0)
            {
                freteDoProduto = Convert.ToDecimal(pedido.valorDoFrete);
                valorFrete = freteDoProduto;
            }

            if (brinde)
            {
                if (pedido.endEstado.ToLower() == "sp" && empresaNota.idEmpresa != 6)
                {
                    cfopProduto = "5910";
                }
                else if (pedido.endEstado.ToLower() == "mg" && empresaNota.idEmpresa == 6)
                {
                    cfopProduto = "5910";
                }
                else
                {
                    cfopProduto = "6910";
                }
                qtdTotalBrindes++;
            }
            else
            {
                totalBrinde = false;
            }

            if (reposicao)
            {
                if (pedido.endEstado.ToLower() == "sp" && empresaNota.idEmpresa != 6)
                {
                    cfopProduto = "5949";
                }
                else if (pedido.endEstado.ToLower() == "mg" && empresaNota.idEmpresa == 6)
                {
                    cfopProduto = "5949";
                }
                else
                {
                    cfopProduto = "6949";
                }
                qtdTotalReposicao++;
            }
            else
            {
                totalReposicao = false;
            }

            if (!cancelado)
            {
                var produtosDc = new dbCommerceDataContext();
                var combo = (from c in produtosGeral where c.itemPedidoId == itemPedido.itemPedidoId select c).Count() > 1;
                if (combo)
                {
                    if (produtoEnviado.produtoId == 42820)
                    {
                        string teste = "";
                    }
                    decimal totalCombo = 0;
                    var produtosComboItem = produtosGeral.Where(x => x.itemPedidoId == produtoEnviado.itemPedidoId).ToList();
                    foreach (var produtoRelacionado in produtosComboItem)
                    {
                        var produtoRelacionadoItem =
                            (from c in produtosDc.tbProdutos where c.produtoId == produtoRelacionado.produtoId select c)
                                .First();
                        var valorDoProdutoCombo = produtoRelacionadoItem.produtoPrecoPromocional > 0
                            ? produtoRelacionadoItem.produtoPrecoPromocional
                            : produtoRelacionadoItem.produtoPreco;

                        if (itemPedido.produtoId == produtoRelacionado.produtoId && valorDoProdutoCombo < itemPedido.itemValor)
                            //if (valorDoProdutoCombo < itemPedido.itemValor)
                            valorDoProdutoCombo = itemPedido.itemValor;

                        totalCombo += Convert.ToDecimal(valorDoProdutoCombo);
                    }


                    if ((itemPedido.itemValor * itemPedido.itemQuantidade) < totalCombo)
                    {

                        var produtoFilho =
                       (from c in produtosDc.tbProdutos where c.produtoId == produtoEnviado.produtoId select c).First();
                        decimal valorAtualDoProduto = produtoFilho.produtoPrecoPromocional > 0
                             ? Convert.ToDecimal(produtoFilho.produtoPrecoPromocional)
                             : produtoFilho.produtoPreco;

                        if (itemPedido.produtoId == produtoEnviado.produtoId && valorAtualDoProduto < itemPedido.itemValor)
                            valorAtualDoProduto = itemPedido.itemValor;

                        decimal valorProporcionalDoProduto = ((itemPedido.itemValor * itemPedido.itemQuantidade) * ((valorAtualDoProduto * 100) / totalCombo)) / 100;

                        valorDoProduto = valorProporcionalDoProduto;

                        if (totalDesconto > 0)
                        {
                            valorDesconto = Convert.ToDecimal((Convert.ToDecimal((valorDoProduto * totalDesconto) / pedido.valorDosItens)).ToString("0.00"));
                            if ((valorDoDesconto + valorDesconto) > totalDesconto) valorDesconto = totalDesconto - valorDoDesconto;
                            if (valorDesconto < 0) valorDesconto = 0;
                            valorDoDesconto += valorDesconto;
                            totalDescontoCalculado += valorDesconto;

                        }

                        if (totalDoFrete > 0)
                        {
                            valorFrete_ = Convert.ToDecimal((Convert.ToDecimal((valorDoProduto * totalDoFrete) / pedido.valorDosItens)).ToString("0.00"));
                            if ((valorDoFrete + valorFrete_) > totalDoFrete) valorFrete_ = totalDoFrete - valorDoFrete;
                            if (valorFrete_ < 0) valorFrete_ = 0;
                            valorDoFrete += valorFrete_;
                            totalDoFreteCalculado += valorFrete_;

                        }
                    }
                    else
                    {
                        if (totalCombo > (itemPedido.itemValor * itemPedido.itemQuantidade))
                        {
                            totalDesconto = totalCombo - itemPedido.itemValor;
                            totalCombo = itemPedido.itemValor;
                        }

                        var produtoFilho =
                            (from c in produtosDc.tbProdutos where c.produtoId == produtoEnviado.produtoId select c).First();
                        valorDoProduto = produtoFilho.produtoPrecoPromocional > 0
                            ? Convert.ToDecimal(produtoFilho.produtoPrecoPromocional)
                            : produtoFilho.produtoPreco;

                        if (itemPedido.produtoId == produtoEnviado.produtoId && valorDoProduto != itemPedido.itemValor)
                            valorDoProduto = itemPedido.itemValor;

                        if (totalCombo > produtoFilho.produtoPreco)
                        {
                            valorDoProduto = (valorDoProduto * (itemPedido.itemValor * itemPedido.itemQuantidade)) / totalCombo;
                        }
                        valorDoProduto = Convert.ToDecimal(valorDoProduto.ToString("0.00"));
                        if (totalDesconto > 0)
                        {
                            valorDesconto = Convert.ToDecimal((Convert.ToDecimal((valorDoProduto * totalDesconto) / pedido.valorDosItens)).ToString("0.00"));
                            if ((valorDoDesconto + valorDesconto) > totalDesconto) valorDesconto = totalDesconto - valorDoDesconto;
                            if (valorDesconto < 0) valorDesconto = 0;
                            valorDoDesconto += valorDesconto;
                            totalDescontoCalculado += valorDesconto;
                        }

                        if (totalDoFrete > 0)
                        {
                            valorFrete_ = Convert.ToDecimal((Convert.ToDecimal((valorDoProduto * totalDoFrete) / pedido.valorDosItens)).ToString("0.00"));
                            if ((valorDoFrete + valorFrete_) > totalDoFrete) valorFrete_ = totalDoFrete - valorDoFrete;
                            if (valorFrete_ < 0) valorFrete_ = 0;
                            valorDoFrete += valorFrete_;
                            totalDoFreteCalculado += valorFrete_;

                        }
                    }
                } // se não for combo
                else
                {

                    if (totalDesconto > 0)
                    {
                        valorDesconto = Convert.ToDecimal((Convert.ToDecimal((itemPedido.itemValor * totalDesconto) / pedido.valorDosItens)).ToString("0.00"));
                        valorDoDesconto += valorDesconto;
                        totalDescontoCalculado += valorDesconto;
                    }
                    else
                    {
                        totalBrinde = false;
                    }

                    if (totalDoFrete > 0)
                    {
                        valorFrete_ = Convert.ToDecimal((Convert.ToDecimal((itemPedido.itemValor * totalDoFrete) / pedido.valorDosItens)).ToString("0.00"));
                        //valorDoDesconto += valorDesconto;
                        //totalDescontoCalculado += valorDesconto;

                        //valorFrete_ = Convert.ToDecimal((Convert.ToDecimal((valorDoProduto * totalDoFrete) / pedido.valorDosItens)).ToString("0.00"));
                        //if ((valorDoFrete + valorFrete_) > totalDoFrete) valorFrete_ = totalDoFrete - valorDoFrete;
                        //if (valorFrete_ < 0) valorFrete_ = 0;
                        valorDoFrete += valorFrete_;
                        totalDoFreteCalculado += valorFrete_;

                    }
                    valorDoProduto = itemPedido.itemValor == 0 ? itemPedido.tbProduto.produtoPreco : itemPedido.itemValor;
                }


                decimal valorDoProdutoComDesconto = valorDoProduto - valorDesconto + valorFrete_;
                totalDoPedidoUnitario += decimal.Round(Convert.ToDecimal(valorDoProduto), 2);
                totalDoPedido += decimal.Round(Convert.ToDecimal(valorDoProduto), 2);

                indiceProduto++;


                if (brinde == true && reposicao == false)
                {
                    if (pedido.endEstado.ToLower() == "sp" && empresaNota.idEmpresa != 6)
                    {
                        cfopProduto = "5910";
                    }
                    else if (pedido.endEstado.ToLower() == "mg" && empresaNota.idEmpresa == 6)
                    {
                        cfopProduto = "5910";
                    }
                    else
                    {
                        cfopProduto = "6910";
                    }
                }
                if (reposicao == true)
                {
                    if (pedido.endEstado.ToLower() == "sp" && empresaNota.idEmpresa != 6)
                    {
                        cfopProduto = "5949";
                    }
                    else if (pedido.endEstado.ToLower() == "mg" && empresaNota.idEmpresa == 6)
                    {
                        cfopProduto = "5949";
                    }
                    else
                    {
                        cfopProduto = "6949";
                    }
                }

                var aliquota = (produto.icms ?? 0) == 0 || clienteEstado.ToLower() == "sp" ? 18 : (int)produto.icms;
                if (empresaNota.idEmpresa == 6) aliquota = 18;
                var aliquotaInterestadual = CalculaDiferencialAliquota(clienteEstado.ToLower(), (Convert.ToDecimal(valorDoProdutoComDesconto)), DateTime.Now);
                if (clienteEstado.ToLower() != "sp" && empresaNota.idEmpresa != 6)
                {
                    aliquota = Convert.ToInt32(aliquotaInterestadual.icmsOrigem);
                    totalIcmsOrigem += aliquotaInterestadual.diferencialOrigem;
                    totalIcmsDestino += aliquotaInterestadual.diferencialDestino;
                }
                else if (clienteEstado.ToLower() != "mg" && empresaNota.idEmpresa == 6)
                {
                    aliquotaInterestadual = CalculaDiferencialAliquota("mg", clienteEstado.ToLower(), (Convert.ToDecimal(valorDoProdutoComDesconto)), DateTime.Now);
                    aliquota = Convert.ToInt32(aliquotaInterestadual.icmsOrigem);
                    totalIcmsOrigem += aliquotaInterestadual.diferencialOrigem;
                    totalIcmsDestino += aliquotaInterestadual.diferencialDestino;
                }
                else
                {
                    //List<int> moveisColchoes = new List<int> { 773, 990, 954, 957, 1005 };
                    List<int> moveisColchoes = new List<int> { 66, 72, 112, 119, 126, 149, 184, 185, 189, 192, 193 };
                    bool movelcolchao = moveisColchoes.Contains(produto.produtoFornecedor);
                    //bool movelcolchao = (from jpc in produtoDc.tbJuncaoProdutoCategorias
                    //                     where moveisColchoes.Contains(jpc.tbProdutoCategoria.categoriaId)
                    //                     && jpc.produtoId == produto.produtoId
                    //                     select jpc).Any();
                    //if (movelcolchao) aliquota = 12;
                }
                var valorIcms = Convert.ToDecimal((((Convert.ToDecimal(valorDoProdutoComDesconto)) / 100) * aliquota).ToString("0.00"));

                totalIcms += valorIcms;
                //nota.AppendFormat("<det nItem=\"{0}\">", nItem);
                nota.AppendFormat("<detItem>", nItem);
                nota.Append("<prod>");
                nota.AppendFormat("<cProd>{0}</cProd>", produto.produtoId);
                //nota.Append("<cEAN/>");
                string produtoNomeCompactado = limitaTexto(rnFuncoes.removeCaracteresEspeciaisDeixaEspaco(rnFuncoes.limpaStringDeixaEspaco(produto.produtoNome)).ToUpper().Replace("-", " ").Trim(), 119).Trim();
                nota.AppendFormat("<xProd>{0}</xProd>", produtoNomeCompactado);
                nota.AppendFormat("<NCM>{0}</NCM>", produto.ncm.Trim());
                nota.AppendFormat("<CFOP>{0}</CFOP>", cfopProduto);
                nota.AppendFormat("<uCOM>{0}</uCOM>", "UN");
                nota.AppendFormat("<qCOM>{0}</qCOM>", Convert.ToInt32(1).ToString("0.0000").Replace(",", "."));
                nota.AppendFormat("<vUnCom>{0}</vUnCom>", (Decimal.Round(valorDoProduto, 2)).ToString("0.00").Replace(",", "."));
                nota.AppendFormat("<vProd>{0}</vProd>", (Decimal.Round(valorDoProduto, 2)).ToString("0.00").Replace(",", "."));
                nota.Append("<cEANTrib/>");
                nota.AppendFormat("<uTrib>{0}</uTrib>", "UN");
                nota.AppendFormat("<qTrib>{0}</qTrib>", Convert.ToInt32(1).ToString("0.00").Replace(",", "."));
                nota.AppendFormat("<vUnTrib>{0}</vUnTrib>", valorDoProduto.ToString("0.00").Replace(",", "."));
                // if (freteDoProduto > 0 && indiceProduto == 1) nota.AppendFormat("<vFrete>{0}</vFrete>", freteDoProduto.ToString("0.00").Replace(",", "."));
                //if (freteDoProduto > 0 && indiceProduto == 1) nota.AppendFormat("<vFrete>{0}</vFrete>", valorFrete_.ToString("0.00").Replace(",", "."));
                if (valorFrete_ > 0) nota.AppendFormat("<vFrete>{0}</vFrete>", valorFrete_.ToString("0.00").Replace(",", "."));
                if (valorDesconto > 0) nota.AppendFormat("<vDesc>{0}</vDesc>", valorDesconto.ToString("0.00").Replace(",", "."));
                nota.AppendFormat("<indTot>{0}</indTot>", "1");
                nota.AppendFormat("<nTipoItem>{0}</nTipoItem>", "0");
                nota.Append("</prod>");
                string CST_pis = "";
                string CST_cofins = "";
                decimal valorTributoProduto = 0;
                decimal valorTributoProdutoPisCofins = 0;
                tbNotaFiscalItem notaFiscalItem = new tbNotaFiscalItem();
                bool partilhaICMSInterestadual = false;
                int? orig_icms = 0;
                int? CST_Icms = 0;
                int? modBC_Icms = null;

                if (empresaNota.simples)
                {
                    nota.Append("<imposto>");
                    nota.Append("<ICMS>");
                    orig_icms = 0;
                    nota.AppendFormat("<orig>{0}</orig>", orig_icms.ToString());
                    //nota.Append("<CST>102</CST>"); alterado a pedido da contabilidade - 06/04/2016
                    CST_Icms = 500;
                    nota.AppendFormat("<CST>{0}</CST>", CST_Icms.ToString());
                    nota.Append("</ICMS>");
                    nota.Append("<PIS>");
                    CST_pis = (cfopProduto == "5910" || cfopProduto == "6910" ? "49" : "07");
                    nota.AppendFormat("<CST_pis>{0}</CST_pis>", CST_pis);
                    nota.Append("</PIS>");
                    nota.Append("<COFINS>");
                    CST_cofins = (cfopProduto == "5910" || cfopProduto == "6910" ? "49" : "07");
                    nota.AppendFormat("<CST_cofins>{0}</CST_cofins>", CST_cofins);
                    nota.Append("</COFINS>");
                    if (estado != "sp" && empresaNota.idEmpresa != 6 && (gerarGnre | empresaNota.simples == false) && cpfCnpj.Length == 11)
                    {
                        nota.Append("<ICMSUFDest>");
                        nota.AppendFormat("<vBCUFDest>{0}</vBCUFDest>", (Convert.ToDecimal(valorDoProdutoComDesconto)).ToString("0.00").Replace(",", "."));
                        nota.Append("<pFCPUFDest>0.00</pFCPUFDest>");
                        nota.AppendFormat("<pICMSUFDest>{0}</pICMSUFDest>", aliquotaInterestadual.icmsDestino.ToString("0.00").Replace(",", "."));
                        nota.AppendFormat("<pICMSInter>{0}</pICMSInter>", aliquotaInterestadual.icmsOrigem.ToString("0.00").Replace(",", "."));
                        nota.AppendFormat("<pICMSInterPart>{0}</pICMSInterPart>", aliquotaInterestadual.partilhaDestino.ToString("0.00").Replace(",", "."));
                        nota.AppendFormat("<vFCPUFDest>{0}</vFCPUFDest>", (0).ToString("0.00").Replace(",", "."));
                        nota.AppendFormat("<vICMSUFDest>{0}</vICMSUFDest>", (aliquotaInterestadual.diferencialDestino).ToString("0.00").Replace(",", "."));
                        aliquotaInterestadual.diferencialOrigem = (decimal)0;
                        nota.AppendFormat("<vICMSUFRemet>{0}</vICMSUFRemet>", (0).ToString("0.00").Replace(",", "."));
                        nota.Append("</ICMSUFDest>");

                        partilhaICMSInterestadual = true;
                    }
                    else if (empresaNota.idEmpresa == 2 && estado != "sp" && cpfCnpj.Length == 11)
                    {
                        nota.Append("<ICMSUFDest>");
                        nota.AppendFormat("<vBCUFDest>{0}</vBCUFDest>", (Convert.ToDecimal(valorDoProdutoComDesconto)).ToString("0.00").Replace(",", "."));
                        nota.Append("<pFCPUFDest>0.00</pFCPUFDest>");
                        nota.AppendFormat("<pICMSUFDest>{0}</pICMSUFDest>", aliquotaInterestadual.icmsDestino.ToString("0.00").Replace(",", "."));
                        nota.AppendFormat("<pICMSInter>{0}</pICMSInter>", aliquotaInterestadual.icmsOrigem.ToString("0.00").Replace(",", "."));
                        nota.AppendFormat("<pICMSInterPart>{0}</pICMSInterPart>", aliquotaInterestadual.partilhaDestino.ToString("0.00").Replace(",", "."));
                        nota.AppendFormat("<vFCPUFDest>{0}</vFCPUFDest>", (0).ToString("0.00").Replace(",", "."));
                        nota.AppendFormat("<vICMSUFDest>{0}</vICMSUFDest>", (aliquotaInterestadual.diferencialDestino).ToString("0.00").Replace(",", "."));
                        nota.AppendFormat("<vICMSUFRemet>{0}</vICMSUFRemet>", (aliquotaInterestadual.diferencialOrigem).ToString("0.00").Replace(",", "."));
                        nota.Append("</ICMSUFDest>");

                        partilhaICMSInterestadual = true;
                    }
                    nota.Append("</imposto>");
                }
                else
                {
                    valorTributoProduto = (valorDoProdutoComDesconto + freteDoProduto);
                    valorTributoProdutoPisCofins = valorDoProdutoComDesconto;

                    totalPis += decimal.Round((Convert.ToDecimal((valorTributoProdutoPisCofins / 100) * (Decimal).65)), 2);
                    totalCofins += decimal.Round((Convert.ToDecimal((valorTributoProdutoPisCofins / 100) * 3)), 2);

                    nota.Append("<imposto>");

                    nota.Append("<ICMS>");
                    nota.Append("<orig>0</orig>");
                    nota.Append("<CST>00</CST>");
                    modBC_Icms = 3;
                    nota.AppendFormat("<modBC>{0}</modBC>", modBC_Icms.ToString());
                    nota.AppendFormat("<vBC>{0}</vBC>", (Convert.ToDecimal(valorDoProdutoComDesconto)).ToString("0.00").Replace(",", "."));
                    vBCAcumulador += Convert.ToDecimal(valorDoProdutoComDesconto.ToString("0.00"));
                    nota.AppendFormat("<pICMS>{0}</pICMS>", (aliquota).ToString("0.00").Replace(",", "."));
                    nota.AppendFormat("<vICMS_icms>{0}</vICMS_icms>", (valorIcms).ToString("0.00").Replace(",", "."));
                    nota.Append("</ICMS>");

                    nota.Append("<PIS>");
                    CST_pis = (cfopProduto == "5910" || cfopProduto == "6910" ? "49" : "01");
                    nota.AppendFormat("<CST_pis>{0}</CST_pis>", CST_pis);
                    nota.AppendFormat("<vBC_pis>{0}</vBC_pis>", (Convert.ToDecimal(valorTributoProdutoPisCofins)).ToString("0.00").Replace(".", "").Replace(",", "."));
                    nota.Append("<pPIS>0.65</pPIS>");
                    nota.AppendFormat("<vPIS>{0}</vPIS>", decimal.Round((Convert.ToDecimal((valorTributoProdutoPisCofins / 100) * (Decimal).65)), 2).ToString("0.00").Replace(".", "").Replace(",", "."));
                    nota.Append("</PIS>");

                    nota.Append("<COFINS>");
                    CST_cofins = (cfopProduto == "5910" || cfopProduto == "6910" ? "49" : "01");
                    nota.AppendFormat("<CST_cofins>{0}</CST_cofins>", CST_cofins);
                    nota.AppendFormat("<vBC_cofins>{0}</vBC_cofins>", (Convert.ToDecimal(valorTributoProdutoPisCofins)).ToString("0.00").Replace(".", "").Replace(",", "."));
                    nota.Append("<pCOFINS>3.00</pCOFINS>");
                    nota.AppendFormat("<vCOFINS>{0}</vCOFINS>", decimal.Round((Convert.ToDecimal((valorTributoProdutoPisCofins / 100) * 3)), 2).ToString("0.00").Replace(".", "").Replace(",", "."));
                    nota.Append("</COFINS>");

                    if (estado != "sp" && empresaNota.idEmpresa != 6 && (gerarGnre | empresaNota.simples == false) && cpfCnpj.Length == 11 || (estado != "sp" && empresaNota.idEmpresa != 6 && ufICMSInterestadualValidar.Contains(pedido.endEstado.ToLower()) && cpfCnpj.Length > 11 && string.IsNullOrEmpty(cliente.clienteRGIE)))
                    {
                        nota.Append("<ICMSUFDest>");
                        nota.AppendFormat("<vBCUFDest>{0}</vBCUFDest>", (Convert.ToDecimal(valorDoProdutoComDesconto)).ToString("0.00").Replace(",", "."));
                        nota.Append("<pFCPUFDest>0.00</pFCPUFDest>");
                        nota.AppendFormat("<pICMSUFDest>{0}</pICMSUFDest>", aliquotaInterestadual.icmsDestino.ToString("0.00").Replace(",", "."));
                        nota.AppendFormat("<pICMSInter>{0}</pICMSInter>", aliquotaInterestadual.icmsOrigem.ToString("0.00").Replace(",", "."));
                        nota.AppendFormat("<pICMSInterPart>{0}</pICMSInterPart>", aliquotaInterestadual.partilhaDestino.ToString("0.00").Replace(",", "."));
                        nota.AppendFormat("<vFCPUFDest>{0}</vFCPUFDest>", (0).ToString("0.00").Replace(",", "."));
                        nota.AppendFormat("<vICMSUFDest>{0}</vICMSUFDest>", (aliquotaInterestadual.diferencialDestino).ToString("0.00").Replace(",", "."));
                        nota.AppendFormat("<vICMSUFRemet>{0}</vICMSUFRemet>", (aliquotaInterestadual.diferencialOrigem).ToString("0.00").Replace(",", "."));
                        nota.Append("</ICMSUFDest>");

                        partilhaICMSInterestadual = true;
                    }
                    if (estado != "mg" && empresaNota.idEmpresa == 6 && (gerarGnre | empresaNota.simples == false) && cpfCnpj.Length == 11 || (estado != "mg" && empresaNota.idEmpresa == 6 && ufICMSInterestadualValidar.Contains(pedido.endEstado.ToLower()) && cpfCnpj.Length > 11 && string.IsNullOrEmpty(cliente.clienteRGIE)))
                    {
                        nota.Append("<ICMSUFDest>");
                        nota.AppendFormat("<vBCUFDest>{0}</vBCUFDest>", (Convert.ToDecimal(valorDoProdutoComDesconto)).ToString("0.00").Replace(",", "."));
                        nota.Append("<pFCPUFDest>0.00</pFCPUFDest>");
                        nota.AppendFormat("<pICMSUFDest>{0}</pICMSUFDest>", aliquotaInterestadual.icmsDestino.ToString("0.00").Replace(",", "."));
                        nota.AppendFormat("<pICMSInter>{0}</pICMSInter>", aliquotaInterestadual.icmsOrigem.ToString("0.00").Replace(",", "."));
                        nota.AppendFormat("<pICMSInterPart>{0}</pICMSInterPart>", aliquotaInterestadual.partilhaDestino.ToString("0.00").Replace(",", "."));
                        nota.AppendFormat("<vFCPUFDest>{0}</vFCPUFDest>", (0).ToString("0.00").Replace(",", "."));
                        nota.AppendFormat("<vICMSUFDest>{0}</vICMSUFDest>", (aliquotaInterestadual.diferencialDestino).ToString("0.00").Replace(",", "."));
                        nota.AppendFormat("<vICMSUFRemet>{0}</vICMSUFRemet>", (aliquotaInterestadual.diferencialOrigem).ToString("0.00").Replace(",", "."));
                        nota.Append("</ICMSUFDest>");

                        partilhaICMSInterestadual = true;
                    }
                    nota.Append("</imposto>");
                }
                nota.Append("</detItem>");
                nItem++;
                //}

                notaFiscalItem.cfop = cfopProduto;
                notaFiscalItem.ncm = produto.ncm.Trim();
                notaFiscalItem.produtoId = produto.produtoId;
                notaFiscalItem.produtoNome = produtoNomeCompactado;
                notaFiscalItem.qCOM = 1;
                notaFiscalItem.vUnCom = Decimal.Round(valorDoProduto, 2) > 0 ? Decimal.Round(valorDoProduto, 2) : Convert.ToDecimal(0.01);
                notaFiscalItem.vProd = Decimal.Round(valorDoProduto, 2) > 0 ? Decimal.Round(valorDoProduto, 2) : Convert.ToDecimal(0.01);
                notaFiscalItem.qTrib = 1;
                notaFiscalItem.vUnTrib = Decimal.Round(valorDoProduto, 2) > 0 ? Decimal.Round(valorDoProduto, 2) : Convert.ToDecimal(0.01);
                notaFiscalItem.vFrete = Decimal.Round(valorFrete_, 2);
                notaFiscalItem.CST_pis = CST_pis;
                notaFiscalItem.CST_cofins = CST_cofins;
                notaFiscalItem.aliquotaIcms = aliquota;

                //Novos campos 20/06/2017 - Adilson:
                notaFiscalItem.vDesc = valorDesconto;
                notaFiscalItem.vBC_icms = valorDoProdutoComDesconto;
                notaFiscalItem.pICMS = aliquota;
                notaFiscalItem.vICMS_icms = valorIcms;
                notaFiscalItem.vBC_pis = valorTributoProdutoPisCofins;
                notaFiscalItem.pPIS = (decimal)0.65;
                notaFiscalItem.vPIS = decimal.Round((Convert.ToDecimal((valorTributoProdutoPisCofins / 100) * (Decimal).65)), 2);
                notaFiscalItem.vBC_cofins = valorTributoProdutoPisCofins;
                notaFiscalItem.pCOFINS = (decimal)3;
                notaFiscalItem.vCOFINS = decimal.Round((Convert.ToDecimal((valorTributoProdutoPisCofins / 100) * 3)), 2);

                if (partilhaICMSInterestadual)
                {
                    notaFiscalItem.vBCUFDest = Convert.ToDecimal(valorDoProdutoComDesconto);
                    notaFiscalItem.pFCPUFDest = (decimal)0;
                    notaFiscalItem.pICMSUFDest = aliquotaInterestadual.icmsDestino;
                    notaFiscalItem.pICMSInter = aliquotaInterestadual.icmsOrigem;
                    notaFiscalItem.pICMSInterPart = aliquotaInterestadual.partilhaDestino;
                    notaFiscalItem.vFCPUFDest = (decimal)0;
                    notaFiscalItem.vICMSUFDest = aliquotaInterestadual.diferencialDestino;
                    notaFiscalItem.vICMSUFRemet = aliquotaInterestadual.diferencialOrigem;
                }
                notaFiscalItem.orig_icms = orig_icms;
                notaFiscalItem.CST_Icms = CST_Icms;
                notaFiscalItem.modBC_Icms = modBC_Icms;

                notaFiscalItens.Add(notaFiscalItem);
            }
        }

        nota.Append("</det>");
        #endregion

        #region Totais
        nota.Append("<total>");
        nota.Append("<ICMStot>");
        if (empresaNota.simples || (empresaNota.idEmpresa == 2 && estado != "sp" && cpfCnpj.Length == 11))
        {
            nota.Append("<vBC_ttlnfe>0.00</vBC_ttlnfe>");
            nota.Append("<vICMS_ttlnfe>0.00</vICMS_ttlnfe>");
        }
        else
        {
            //nota.AppendFormat("<vBC_ttlnfe>{0}</vBC_ttlnfe>", Convert.ToDecimal(totalDoPedido - totalDescontoCalculado + totalDoFreteCalculado).ToString("0.00").Replace(",", "."));
            nota.AppendFormat("<vBC_ttlnfe>{0}</vBC_ttlnfe>", vBCAcumulador.ToString("0.00").Replace(",", "."));
            nota.AppendFormat("<vICMS_ttlnfe>{0}</vICMS_ttlnfe>", Convert.ToDecimal(totalIcms).ToString("0.00").Replace(",", "."));

            if ((totalDoPedido - totalDescontoCalculado) > valorPagoPedido && (qtdTotalProdutos != qtdTotalBrindes))
            {
                totalDescontoCalculado += ((totalDoPedido - totalDescontoCalculado + totalDoFreteCalculado) - valorPagoPedido);
            }
        }
        nota.Append("<vBCST_ttlnfe>0.00</vBCST_ttlnfe>");
        nota.Append("<vST_ttlnfe>0.00</vST_ttlnfe>");



        nota.AppendFormat("<vProd_ttlnfe>{0}</vProd_ttlnfe>", Convert.ToDecimal(totalDoPedido).ToString("0.00").Replace(",", "."));
        //if (valorFrete > 0) nota.AppendFormat("<vFrete_ttlnfe>{0}</vFrete_ttlnfe>", valorFrete.ToString("0.00").Replace(",", "."));
        if (valorFrete > 0) nota.AppendFormat("<vFrete_ttlnfe>{0}</vFrete_ttlnfe>", totalDoFreteCalculado.ToString("0.00").Replace(",", "."));
        else nota.AppendFormat("<vFrete_ttlnfe>{0}</vFrete_ttlnfe>", (0).ToString("0.00").Replace(",", "."));
        nota.Append("<vSeg_ttlnfe>0.00</vSeg_ttlnfe>");

        nota.AppendFormat("<vDesc_ttlnfe>{0}</vDesc_ttlnfe>", totalDescontoCalculado.ToString("0.00").Replace(",", "."));
        //nota.Append("<vII>0.00</vII>");
        nota.Append("<vIPI_ttlnfe>0.00</vIPI_ttlnfe>");

        if (empresaNota.simples && empresaNota.idEmpresa != 2)
            nota.Append("<vPIS_ttlnfe>0.00</vPIS_ttlnfe>");
        else if (empresaNota.idEmpresa == 2 && estado != "sp" && cpfCnpj.Length == 11)
            nota.AppendFormat("<vPIS_ttlnfe>{0}</vPIS_ttlnfe>", totalPis.ToString("0.00").Replace(",", "."));
        else
            nota.AppendFormat("<vPIS_ttlnfe>{0}</vPIS_ttlnfe>", totalPis.ToString("0.00").Replace(",", "."));

        if (empresaNota.simples && empresaNota.idEmpresa != 2)
            nota.Append("<vCOFINS_ttlnfe>0.00</vCOFINS_ttlnfe>");
        else if (empresaNota.idEmpresa == 2 && estado != "sp" && cpfCnpj.Length == 11)
            nota.AppendFormat("<vCOFINS_ttlnfe>{0}</vCOFINS_ttlnfe>", totalCofins.ToString("0.00").Replace(",", "."));
        else
            nota.AppendFormat("<vCOFINS_ttlnfe>{0}</vCOFINS_ttlnfe>", totalCofins.ToString("0.00").Replace(",", "."));

        nota.Append("<vOutro>0.00</vOutro>");
        nota.Append("<vICMSDeson_ttlnfe>0.00</vICMSDeson_ttlnfe>");
        //nota.AppendFormat("<vNF>{0}</vNF>", Convert.ToDecimal(totalDoPedido + valorFrete - valorDoDesconto).ToString("0.00").Replace(",", "."));
        nota.AppendFormat("<vNF>{0}</vNF>", Convert.ToDecimal(totalDoPedido + totalDoFreteCalculado - totalDescontoCalculado).ToString("0.00").Replace(",", "."));
        nota.Append("<vTotTrib_ttlnfe>0.00</vTotTrib_ttlnfe>");

        if (estado != "sp" && empresaNota.idEmpresa != 6 && (gerarGnre | empresaNota.simples == false) && cpfCnpj.Length == 11 || (ufICMSInterestadualValidar.Contains(pedido.endEstado.ToLower()) && cpfCnpj.Length > 11 && string.IsNullOrEmpty(cliente.clienteRGIE)))
        {
            nota.Append("<vFCPUFDest_ttlnfe>0.00</vFCPUFDest_ttlnfe>");

            if (empresaNota.idEmpresa == 2 && cpfCnpj.Length > 11)
                nota.AppendFormat("<vICMSUFDest_ttlnfe>0.00</vICMSUFDest_ttlnfe>");
            else
                nota.AppendFormat("<vICMSUFDest_ttlnfe>{0}</vICMSUFDest_ttlnfe>",
                    totalIcmsDestino.ToString("0.00").Replace(",", "."));
            if (empresaNota.simples)
            {
                nota.Append("<vICMSUFRemet_ttlnfe>0.00</vICMSUFRemet_ttlnfe>");
            }
            else
            {
                nota.AppendFormat("<vICMSUFRemet_ttlnfe>{0}</vICMSUFRemet_ttlnfe>",
                    totalIcmsOrigem.ToString("0.00").Replace(",", "."));
            }
        }
        if (estado != "mg" && empresaNota.idEmpresa == 6 && (gerarGnre | empresaNota.simples == false) && cpfCnpj.Length == 11 || (ufICMSInterestadualValidar.Contains(pedido.endEstado.ToLower()) && cpfCnpj.Length > 11 && string.IsNullOrEmpty(cliente.clienteRGIE)))
        {
            nota.Append("<vFCPUFDest_ttlnfe>0.00</vFCPUFDest_ttlnfe>");

            if (empresaNota.idEmpresa == 2 && cpfCnpj.Length > 11)
                nota.AppendFormat("<vICMSUFDest_ttlnfe>0.00</vICMSUFDest_ttlnfe>");
            else
                nota.AppendFormat("<vICMSUFDest_ttlnfe>{0}</vICMSUFDest_ttlnfe>",
                    totalIcmsDestino.ToString("0.00").Replace(",", "."));
            if (empresaNota.simples)
            {
                nota.Append("<vICMSUFRemet_ttlnfe>0.00</vICMSUFRemet_ttlnfe>");
            }
            else
            {
                nota.AppendFormat("<vICMSUFRemet_ttlnfe>{0}</vICMSUFRemet_ttlnfe>",
                    totalIcmsOrigem.ToString("0.00").Replace(",", "."));
            }
        }
        else if (empresaNota.idEmpresa == 2 && estado != "sp" && cpfCnpj.Length == 11)
        {
            nota.Append("<vFCPUFDest_ttlnfe>0.00</vFCPUFDest_ttlnfe>");
            nota.AppendFormat("<vICMSUFDest_ttlnfe>{0}</vICMSUFDest_ttlnfe>",
                totalIcmsDestino.ToString("0.00").Replace(",", "."));

            nota.AppendFormat("<vICMSUFRemet_ttlnfe>{0}</vICMSUFRemet_ttlnfe>",
                    totalIcmsOrigem.ToString("0.00").Replace(",", "."));
        }

        nota.Append("</ICMStot>");
        nota.Append("</total>");
        #endregion

        #region transportadora
        nota.Append("<transp>");
        nota.Append("<modFrete>0</modFrete>");

        var formaDeEnvio = "";
        var envioPacote = pacotes.FirstOrDefault(x => x.formaDeEnvio != "" && x.formaDeEnvio != null);
        int tipoDeEntregaId = 0;
        if (envioPacote != null)
        {
            formaDeEnvio = envioPacote.formaDeEnvio;
            if (envioPacote.tbPedidoEnvio.tipoDeEntregaId != null)
            {
                tipoDeEntregaId = envioPacote.tbPedidoEnvio.tipoDeEntregaId ?? 0;
            }
        }

        var data = new dbCommerceDataContext();

        if (tipoDeEntregaId > 0)
        {
            var tipoDeEntrega = (from c in data.tbTipoDeEntregas where c.tipoDeEntregaId == tipoDeEntregaId select c).FirstOrDefault();
            if (tipoDeEntrega != null)
            {
                nota.Append("<transporta>");
                nota.AppendFormat("<CNPJ_transp>{0}</CNPJ_transp>", tipoDeEntrega.cnpj);
                nota.AppendFormat("<xNome_transp>{0}</xNome_transp>", tipoDeEntrega.razaoSocial);
                nota.AppendFormat("<IE_transp>{0}</IE_transp>", tipoDeEntrega.ie);
                nota.AppendFormat("<xEnder>{0}</xEnder>", tipoDeEntrega.endereco);
                nota.AppendFormat("<xMun_transp>{0}</xMun_transp>", tipoDeEntrega.municipio);
                nota.AppendFormat("<UF_transp>{0}</UF_transp>", tipoDeEntrega.uf);
                nota.Append("</transporta>");
            }
        }
        else
        {
            List<string> modalidadeJadlog = new List<string> { "jadlog", "jadlogexpressa", "jadlogcom", "jadlogrodo" };
            if (modalidadeJadlog.Contains(formaDeEnvio))
            {
                nota.Append("<transporta>");
                nota.Append("<CNPJ_transp>14135995000102</CNPJ_transp>");
                nota.Append("<xNome_transp>CALLILI EXPRESS LTDA ME</xNome_transp>");
                nota.Append("<IE_transp>344063890114</IE_transp>");
                nota.Append("<xEnder>RUA DANIEL DE FREITAS , 928 - CENTRO</xEnder>");
                nota.Append("<xMun_transp>Ibitinga</xMun_transp>");
                nota.Append("<UF_transp>SP</UF_transp>");
                nota.Append("</transporta>");
            }
            else if (formaDeEnvio == "plimor")
            {
                nota.Append("<transporta>");
                nota.Append("<CNPJ_transp>88085485006226</CNPJ_transp>");
                nota.Append("<xNome_transp>TRANSPORTADORA PLIMOR LTDA</xNome_transp>");
                nota.Append("<IE_transp>209364638111</IE_transp>");
                nota.Append("<xEnder>Marechal Rondon - km 334 - SN</xEnder>");
                nota.Append("<xMun_transp>Bauru</xMun_transp>");
                nota.Append("<UF_transp>SP</UF_transp>");
                nota.Append("</transporta>");
            }
            else if (formaDeEnvio == "lbr")
            {
                nota.Append("<transporta>");
                nota.Append("<CNPJ_transp>16660697000248</CNPJ_transp>");
                nota.Append("<xNome_transp>LBR EXPRESS TRANSPORTES E LOGÍSTICA LTDA</xNome_transp>");
                nota.Append("<IE_transp>0028344510058</IE_transp>");
                nota.Append("<xEnder>Rua Quintino Bocaiúva</xEnder>");
                nota.Append("<xMun_transp>Contagem</xMun_transp>");
                nota.Append("<UF_transp>MG</UF_transp>");
                nota.Append("</transporta>");
            }
            else if (formaDeEnvio == "jamef")
            {
                nota.Append("<transporta>");
                nota.Append("<CNPJ_transp>20147617002276</CNPJ_transp>");
                nota.Append("<xNome_transp>Jamef Transporte LTDA</xNome_transp>");
                nota.Append("<IE_transp>114387171114</IE_transp>");
                nota.Append("<xEnder>Rua Miguel Mentem 500</xEnder>");
                nota.Append("<xMun_transp>Vila Guilherme</xMun_transp>");
                nota.Append("<UF_transp>SP</UF_transp>");
                nota.Append("</transporta>");
            }
            else if (formaDeEnvio == "nowlog")
            {
                nota.Append("<transporta>");
                nota.Append("<CNPJ_transp>20712076000238</CNPJ_transp>");
                nota.Append("<xNome_transp>Nowlog Logistica Inteligente LTDA</xNome_transp>");
                nota.Append("<IE_transp>0026412980032</IE_transp>");
                nota.Append("<xEnder>Avenida Mestra Fininha, 1726, Letra A, Sala 1</xEnder>");
                nota.Append("<xMun_transp>Montes Claros</xMun_transp>");
                nota.Append("<UF_transp>MG</UF_transp>");
                nota.Append("</transporta>");
            }
            else if (formaDeEnvio == "dialogo")
            {
                nota.Append("<transporta>");
                nota.Append("<CNPJ_transp>21930065000297</CNPJ_transp>");
                nota.Append("<xNome_transp>Dialogo Logistica Inteligente LTDA</xNome_transp>");
                nota.Append("<IE_transp>144872110110</IE_transp>");
                nota.Append("<xEnder>Rodovia Dom Gabriel Paulino Bueno KM 71 M06 G2, Medeiros</xEnder>");
                nota.Append("<xMun_transp>Jundiai</xMun_transp>");
                nota.Append("<UF_transp>SP</UF_transp>");
                nota.Append("</transporta>");
            }
            else if (formaDeEnvio == "totalexpress")
            {
                nota.Append("<transporta>");
                nota.Append("<CNPJ_transp>73939449000193</CNPJ_transp>");
                nota.Append("<xNome_transp>TEX COURIER S.A.</xNome_transp>");
                nota.Append("<IE_transp>206214714111</IE_transp>");
                nota.Append("<xEnder>AVENIDA PIRACEMA 155 GALPAO 1</xEnder>");
                nota.Append("<xMun_transp>Barueri</xMun_transp>");
                nota.Append("<UF_transp>SP</UF_transp>");
                nota.Append("</transporta>");
            }
            else if (formaDeEnvio == "tnt")
            {
                if (envioPacote.idCentroDistribuicao == 3)
                {
                    nota.Append("<transporta>");
                    nota.Append("<CNPJ_transp>95591723003800</CNPJ_transp>");
                    nota.Append("<xNome_transp>TNT Mercurio Cargas e Encomendas Expressas SA</xNome_transp>");
                    nota.Append("<IE_transp>108254280116</IE_transp>");
                    nota.Append("<xEnder>Avenida Marginal Direita do Tiete N 2500</xEnder>");
                    nota.Append("<xMun_transp>Sao Paulo</xMun_transp>");
                    nota.Append("<UF_transp>SP</UF_transp>");
                    nota.Append("</transporta>");
                }
                else
                {
                    nota.Append("<transporta>");
                    nota.Append("<CNPJ_transp>95591723008284</CNPJ_transp>");
                    nota.Append("<xNome_transp>TNT Mercurio Cargas e Encomendas Expressas SA</xNome_transp>");
                    nota.Append("<IE_transp>209199664118</IE_transp>");
                    nota.Append("<xEnder>RUA JOAQUIM PELEGRINA LOPES,1-80 - Dist. Ind. III</xEnder>");
                    nota.Append("<xMun_transp>Bauru</xMun_transp>");
                    nota.Append("<UF_transp>SP</UF_transp>");
                    nota.Append("</transporta>");
                }
            }
        }
        int vol = 1;
        nota.Append("<vol>");
        if (pacotes.Count == 0)
        {
            nota.Append("<volItem>");
            nota.AppendFormat("<qVol>{0}</qVol>", 1);
            nota.Append("<esp>VOLUME</esp>");
            nota.AppendFormat("<nVol>{0}</nVol>", vol);
            nota.AppendFormat("<pesoB_transp>{0}</pesoB_transp>", (Convert.ToDecimal(pesoTotal) / 1000).ToString("0.000").Replace(",", "."));
            nota.Append("</volItem>");
        }
        else
        {
            bool nfeVolumeTotal = false;
            if (tipoDeEntregaId > 0)
            {
                var entrega = (from c in data.tbTipoDeEntregas where c.tipoDeEntregaId == tipoDeEntregaId select c).FirstOrDefault();
                if (entrega != null)
                {
                    nfeVolumeTotal = (entrega.nfeVolumeTotal ?? false);
                }
            }
            if (nfeVolumeTotal)
            {
                nota.Append("<volItem>");
                nota.AppendFormat("<qVol>{0}</qVol>", pacotes.Count());
                nota.Append("<esp>VOLUMES</esp>");
                nota.AppendFormat("<nVol>{0}</nVol>", vol);
                nota.AppendFormat("<pesoB_transp>{0}</pesoB_transp>", (Convert.ToDecimal(pacotes.Sum(x => x.peso)) / 1000).ToString("0.000").Replace(",", "."));
                nota.Append("</volItem>");
                vol++;
            }
            else
            {
                foreach (var pacote in pacotes.OrderBy(x => x.numeroVolume))
                {
                    nota.Append("<volItem>");
                    nota.AppendFormat("<qVol>{0}</qVol>", 1);
                    nota.Append("<esp>VOLUME</esp>");
                    nota.AppendFormat("<nVol>{0}</nVol>", vol);
                    nota.AppendFormat("<pesoB_transp>{0}</pesoB_transp>", (Convert.ToDecimal(pacote.peso) / 1000).ToString("0.000").Replace(",", "."));
                    nota.Append("</volItem>");
                    vol++;
                }
            }
        }
        nota.Append("</vol>");
        nota.Append("</transp>");

        string informacoesAdicionais = "";
        informacoesAdicionais += "PEDIDO " + rnFuncoes.retornaIdCliente(pedido.pedidoId);
        if (!string.IsNullOrEmpty(pedido.endReferenciaParaEntrega))
        {
            informacoesAdicionais += " - Ref.: " + rnFuncoes.removeAcentos(pedido.endReferenciaParaEntrega.Replace("&", "").Replace("(", "").Replace(")", "").Replace("-", "").Replace("\n", " ").Replace("\r", " ").Replace("’", "'").Replace("•", " ").TrimEnd());
            informacoesAdicionais.Replace("“", "'");
            informacoesAdicionais.Replace("”", "'");
            informacoesAdicionais.Replace("–", "-");
        }
        string telefone = "";
        if (!string.IsNullOrEmpty(cliente.clienteFoneResidencial)) telefone += "Res.: " + cliente.clienteFoneResidencial + " - ";
        if (!string.IsNullOrEmpty(cliente.clienteFoneComercial)) telefone += "Com.: " + cliente.clienteFoneComercial + " - ";
        if (!string.IsNullOrEmpty(cliente.clienteFoneCelular)) telefone += "Cel.: " + cliente.clienteFoneCelular;
        if (!string.IsNullOrEmpty(telefone)) informacoesAdicionais += " - " + telefone;

        //if (estado != "sp" && cliente.clienteCPFCNPJ.Length == 11)
        if (estado != "sp" && empresaNota.idEmpresa != 6)
        {
            informacoesAdicionais += " - Convenio 93/15 - ";
            informacoesAdicionais += "Total do ICMS de partilha para a UF do destinatario R$" + totalIcmsDestino.ToString("0.00") + " - ";
            informacoesAdicionais += "Total do ICMS interestadual para a UF do remetente R$" + totalIcmsOrigem.ToString("0.00") + " - ";
        }
        if (estado != "mg" && empresaNota.idEmpresa == 6)
        {
            informacoesAdicionais += " - Convenio 93/15 - ";
            informacoesAdicionais += "Total do ICMS de partilha para a UF do destinatario R$" + totalIcmsDestino.ToString("0.00") + " - ";
            informacoesAdicionais += "Total do ICMS interestadual para a UF do remetente R$" + totalIcmsOrigem.ToString("0.00") + " - ";
        }
        if (empresaNota.simples) informacoesAdicionais += " - I DOCUMENTO EMITIDO POR EMPRESA ME OU EPP OPTANTE PELO SIMPLES NACIONAL II- NAO GERA CREDITO FISCAL DE IPI E ISS.";
        if (empresaNota.idEmpresa == 3 || empresaNota.idEmpresa == 1) informacoesAdicionais += " - Lei 12.741 de 12/2012 Carga Tributária 29,33% ";
        if (empresaNota.idEmpresa == 6) informacoesAdicionais += " - Contribuinte detentor do Regime Especial e-PTA 45.000013531-61, nos termos da Lei nº 1.641 de 28 de dezembro de 2005, e no Decreto nº 42.533 de 23 de dezembro de 2015 ";
        informacoesAdicionais = informacoesAdicionais.Replace(Environment.NewLine, " ");
        nota.Append("<infAdic>");
        nota.AppendFormat("<infCpl>{0}</infCpl>", informacoesAdicionais);
        nota.Append("</infAdic>");
        #endregion

        //nota.Append("</infNFe>");
        //nota.Append("</NFe>");


        pedidoDc.SubmitChanges();

        var dataAlt = new dbCommerceDataContext();
        var notaAlt = (from c in dataAlt.tbNotaFiscals where c.idNotaFiscal == gerarNota.idNotaFiscal select c).First();
        notaAlt.valor = Convert.ToDecimal(totalDoPedido + valorFrete - valorDoDesconto);
        if (notaAlt.statusNfe != 2)
        {
            notaAlt.statusNfe = 1;
            var notaItens = (from c in dataAlt.tbNotaFiscalItems where c.idNotaFiscal == notaAlt.idNotaFiscal select c).ToList();
            foreach (var notaItem in notaItens)
            {
                dataAlt.tbNotaFiscalItems.DeleteOnSubmit(notaItem);
            }
            dataAlt.SubmitChanges();
        }
        notaAlt.valorPartilha = totalIcmsDestino;
        if (cliente.clienteCPFCNPJ.Length == 11)
        {
            notaAlt.gerarGnre = DefineGerarGnre(pedido.pedidoId, pedido.endEstado.ToLower());
        }
        else
        {
            notaAlt.gerarGnre = false;
        }

        notaAlt.municioDestinatario = codigoIbge;
        dataAlt.SubmitChanges();



        if (idPedidoEnvio > 0)
        {
            //var detalheEnvio = (from c in dataAlt.tbPedidoEnvios where c.idPedidoEnvio == idPedidoEnvio select c).First();
            var detalheEnvio = dataAlt.tbPedidoEnvios.First(x => x.idPedidoEnvio == idPedidoEnvio);
            detalheEnvio.nfeNumero = Convert.ToInt32(numeroNota);
            detalheEnvio.nfeKey = chave;
            detalheEnvio.nfeValor = Convert.ToDecimal(totalDoPedido);

            try
            {
                dataAlt.SubmitChanges();
            }
            catch (System.Data.Linq.ChangeConflictException ex)
            {

                foreach (System.Data.Linq.ObjectChangeConflict objConflict in dataAlt.ChangeConflicts)
                {
                    foreach (System.Data.Linq.MemberChangeConflict memberConflict in objConflict.MemberConflicts)
                    {
                        memberConflict.Resolve(System.Data.Linq.RefreshMode.KeepCurrentValues);
                    }
                }
                dataAlt.SubmitChanges(System.Data.Linq.ConflictMode.ContinueOnConflict);
            }
        }


        try
        {
            System.IO.File.WriteAllText(System.Configuration.ConfigurationManager.AppSettings["caminhoFisico"] + "\\admin\\notas\\" + pedidoId + "_" + numeroNota + ".xml", nota.ToString());
        }
        catch (Exception ex)
        { }
        string notaFormatado = nota.ToString();
        if (totalBrinde || (qtdTotalProdutos == qtdTotalBrindes))
            notaFormatado = notaFormatado.Replace("<natOp>VENDA DE MERCADORIA</natOp>", "<natOp>REMESSA EM BONIFICAÇÃO/BRINDE</natOp>")
                .Replace("<natOp>Venda de Prod. do Estab. Independente substituicao</natOp>", "<natOp>REMESSA EM BONIFICAÇÃO/BRINDE</natOp>")
                .Replace("<natOp>Venda Producao do Estabelecimento Destinada nao Contribuinte</natOp>", "<natOp>REMESSA EM BONIFICAÇÃO/BRINDE</natOp>");


        if (totalReposicao || (qtdTotalProdutos == qtdTotalReposicao))
            notaFormatado = notaFormatado.Replace("<natOp>VENDA DE MERCADORIA</natOp>", "<natOp>REMESSA PARA REPOSIÇÃO</natOp>")
                .Replace("<natOp>Venda de Prod. do Estab. Independente substituicao</natOp>", "<natOp>REMESSA PARA REPOSIÇÃO</natOp>")
                .Replace("<natOp>Venda Producao do Estabelecimento Destinada nao Contribuinte</natOp>", "<natOp>REMESSA PARA REPOSIÇÃO</natOp>");

        var notaRetorno = new notaGerada();
        notaRetorno.xml = notaFormatado;
        notaRetorno.idNotaFiscal = gerarNota.idNotaFiscal;

        if (notaAlt.statusNfe != 2)
        {
            foreach (var item in notaFiscalItens)
            {
                item.idNotaFiscal = notaRetorno.idNotaFiscal;

                data.tbNotaFiscalItems.InsertOnSubmit(item);
                try
                {
                    data.SubmitChanges();
                }
                catch (Exception ex)
                {
                    string erro = ex.Message;
                }

            }
        }
        return notaRetorno;
    }



    /************************************gerarNotaFiscal Fim***********************************************************/
    #endregion
    public static notaGerada gerarNotaFiscalDevolucao1(int idPedido, int idNotaFiscal, string codigoIbge, int idEmpresa,
                                                  string informacoesAdicionaisExtra = "", List<int> idsItemPedidoEstoque = null,
                                                  int idNotaOrigem = 0)
    {

        string cfop = "";
        var pedidoDc = new dbCommerceDataContext();
        var pedido = (from c in pedidoDc.tbPedidos where c.pedidoId == idPedido select c).FirstOrDefault();

        if (pedido == null) return new notaGerada();
        int pedidoId = pedido.pedidoId;

        var clienteDc = new dbCommerceDataContext();
        var cliente = (from c in clienteDc.tbClientes where c.clienteId == pedido.clienteId select c).FirstOrDefault();

        bool gerarNovaNota = true;
        if (idNotaFiscal != null)
        {
            gerarNovaNota = false;
        }
        var gerarNota = gerarNumeroNotaEmpresa(pedido.pedidoId, 0, gerarNovaNota, idEmpresa);
        var empresaNota = (from c in pedidoDc.tbEmpresas where c.idEmpresa == gerarNota.idCNPJ select c).First();
        var notaOrigem = (from c in pedidoDc.tbNotaFiscals where c.idNotaFiscal == idNotaOrigem select c).FirstOrDefault();
        if (notaOrigem == null)
        {
            return new notaGerada();
        }

        string numeroNota = gerarNota.numeroNota.ToString();
        string chave = gerarNota.nfeKey;
        string cDV = GerarDigitoVerificadorNFe(chave).ToString();
        chave += cDV;

        string idDest = "";

        if (empresaNota.idEmpresa == 2)
        {
            if (cliente.clienteCPFCNPJ.Length == 11 && pedido.endEstado.ToLower() == "sp")
            {
                cfop = "1201";
                idDest = "1";
            }
            else if (cliente.clienteCPFCNPJ.Length > 11 && pedido.endEstado.ToLower() == "sp")
            {
                cfop = "1201";
                idDest = "1";
            }
            else if (cliente.clienteCPFCNPJ.Length == 11 && pedido.endEstado.ToLower() != "sp")
            {
                cfop = "2201";
                idDest = "2";
            }
            else
            {
                cfop = "2201";
                idDest = "2";
            }
        }
        else if (empresaNota.idEmpresa == 6)
        {
            if (cliente.clienteCPFCNPJ.Length == 11 && pedido.endEstado.ToLower() == "mg")
            {
                cfop = "1202";
                idDest = "1";
            }
            else if (cliente.clienteCPFCNPJ.Length > 11 && pedido.endEstado.ToLower() == "mg")
            {
                cfop = "1202";
                idDest = "1";
            }
            else if (cliente.clienteCPFCNPJ.Length == 11 && pedido.endEstado.ToLower() != "mg")
            {
                cfop = "2202";
                idDest = "2";
            }
            else
            {
                cfop = "2202";
                idDest = "2";
            }
        }
        else
        {
            if (cliente.clienteCPFCNPJ.Length == 11 && pedido.endEstado.ToLower() == "sp")
            {
                cfop = "1202";
                idDest = "1";
            }
            else if (cliente.clienteCPFCNPJ.Length > 11 && pedido.endEstado.ToLower() == "sp")
            {
                cfop = "1202";
                idDest = "1";
            }
            else if (cliente.clienteCPFCNPJ.Length == 11 && pedido.endEstado.ToLower() != "sp")
            {
                cfop = "2202";
                idDest = "2";
            }
            else
            {
                cfop = "2202";
                idDest = "2";
            }
        }



        var nota = new StringBuilder();

        #region Identificadores da NF-e
        nota.Append("<ide>");
        nota.AppendFormat("<cUF>{0}</cUF>", empresaNota.codigoUfEmitente);
        nota.AppendFormat("<cNF>{0}</cNF>", pedidoId.ToString().PadLeft(8, '0'));

        string natOp = "Devolucao Mercadoria Adquirida ou Recebida de Terceiros";
        if (empresaNota.idEmpresa == 2)
        {
            if ((cliente.clienteCPFCNPJ.Length == 11 | cliente.clienteCPFCNPJ.Length > 11) && pedido.endEstado.ToLower() == "sp")
            {
                natOp = "Devolucao de Venda de Producao";
            }
            else if (pedido.endEstado.ToLower() != "sp")
            {
                natOp = "Devolucao de Venda de Producao";
            }
            else
            {
                natOp = "Devolucao de Venda de Producao";
            }
        }
        else
        {
            natOp = "Devolucao Mercadoria Adquirida ou Recebida de Terceiros";
        }
        nota.Append("natOp");

        List<string> ufValidacao = new List<string> { "AM", "BA", "CE", "GO", "MG", "MS", "MT", "PE", "RN", "SE", "SP", "PA", "RJ", "ES", "PR" };

        nota.Append("<indPag>0</indPag>");
        nota.Append("<mod>4</mod>");
        nota.Append("<serie>1</serie>");
        nota.AppendFormat("<nNF>{0}</nNF>", numeroNota);
        nota.AppendFormat("<dhEmi>{0}T{1}</dhEmi>", DateTime.Now.ToString("yyyy-MM-dd"), DateTime.Now.ToString("HH:mm:ss"));

        if (dataSaida != null)
        {
            nota.AppendFormat("<dhSaiEnt>{0}T{1}</dhSaiEnt>", Convert.ToDateTime(dataSaida).ToString("yyyy-MM-dd"), Convert.ToDateTime(dataSaida).ToString("HH:mm:ss"));
        }


        nota.Append("<fusoHorario>-02:00</fusoHorario>");
        nota.Append("<tpNf>0</tpNf>");
        nota.AppendFormat("<idDest>{0}</idDest>", idDest);

        if (ufValidacao.Contains(pedido.endEstado.ToUpper()) && cliente.clienteCPFCNPJ.Length == 11)
            nota.AppendFormat("<indFinal>1</indFinal>");
        else
            nota.AppendFormat("<indFinal>0</indFinal>");

        nota.AppendFormat("<indPres>2</indPres>");
        nota.AppendFormat("<cMunFg>{0}</cMunFg>", empresaNota.codigoMunicipioEmitente);

        nota.Append("<NFRef>");
        nota.Append("<NFRefItem>");
        nota.AppendFormat("<refNFe>{0}</refNFe>", notaOrigem.nfeKey + GerarDigitoVerificadorNFe(notaOrigem.nfeKey));
        nota.Append("</NFRefItem>");
        nota.Append("</NFRef>");

        nota.Append("<tpImp>1</tpImp>");
        nota.Append("<tpEmis>1</tpEmis>");
        nota.AppendFormat("<tpAmb>{0}</tpAmb>", tpAmb);
        nota.Append("finNFe");
        //nota.Append("<finNFe>4</finNFe>");
        nota.Append("</ide>");
        #endregion

        int finNFe = 4;
        #region Emitente

        nota.Append("<emit>");
        nota.AppendFormat("<CNPJ_emit>{0}</CNPJ_emit>", empresaNota.cnpj);
        nota.AppendFormat("<xNome>{0}</xNome>", empresaNota.nomeEmitente);
        nota.AppendFormat("<xFant>{0}</xFant>", empresaNota.nomeFantasiaEmitente);
        nota.Append("<enderEmit>");
        nota.AppendFormat("<xLgr>{0}</xLgr>", empresaNota.logradouroEmitente);
        nota.AppendFormat("<nro>{0}</nro>", empresaNota.numeroEmitente);
        nota.AppendFormat("<xBairro>{0}</xBairro>", empresaNota.bairroEmitente);
        nota.AppendFormat("<cMun>{0}</cMun>", empresaNota.codigoMunicipioEmitente);
        nota.AppendFormat("<xMun>{0}</xMun>", empresaNota.municipioEmitente);
        nota.AppendFormat("<UF>{0}</UF>", empresaNota.ufEmitente);
        nota.AppendFormat("<CEP>{0}</CEP>", empresaNota.cepEmitente);
        nota.Append("<cPais>1058</cPais>");
        nota.Append("<xPais>BRASIL</xPais>");
        nota.AppendFormat("<fone>{0}</fone>", empresaNota.foneEmitente);
        nota.Append("</enderEmit>");
        nota.AppendFormat("<IE>{0}</IE>", empresaNota.ieEmitente);
        if (empresaNota.simples) nota.Append("<CRT>1</CRT>");
        else nota.Append("<CRT>3</CRT>");
        nota.Append("</emit>");

        #endregion

        #region Destinatario

        nota.Append("<dest>");

        if (cliente.clienteCPFCNPJ.Length == 11)
        {
            nota.AppendFormat("<CPF_dest>{0}</CPF_dest>", cliente.clienteCPFCNPJ);
            if (tpAmb == "2")
            {
                nota.Append("<xNome_dest>NF-E EMITIDA EM AMBIENTE DE HOMOLOGACAO - SEM VALOR FISCAL</xNome_dest>");
            }
            else
            {
                nota.AppendFormat("<xNome_dest>{0}</xNome_dest>", cliente.clienteNome.Trim());
            }
        }
        else
        {
            nota.AppendFormat("<CNPJ_dest>{0}</CNPJ_dest>", cliente.clienteCPFCNPJ.Trim());
            if (tpAmb == "2")
            {
                nota.Append("<xNome_dest>NF-E EMITIDA EM AMBIENTE DE HOMOLOGACAO - SEM VALOR FISCAL</xNome_dest>");
            }
            else
            {
                nota.AppendFormat("<xNome_dest>{0}</xNome_dest>", cliente.clienteNomeDaEmpresa.Trim());
            }
        }


        string foneNfe = "";
        if (!string.IsNullOrEmpty(cliente.clienteFoneComercial)) foneNfe = cliente.clienteFoneComercial;
        if (!string.IsNullOrEmpty(cliente.clienteFoneResidencial)) foneNfe = cliente.clienteFoneResidencial;
        if (!string.IsNullOrEmpty(cliente.clienteFoneCelular)) foneNfe = cliente.clienteFoneCelular;


        nota.Append("<enderDest>");
        nota.AppendFormat("<xLgr_dest>{0}</xLgr_dest>", pedido.endRua.Trim().Length < 5 ? "Rua " + pedido.endRua.Trim() : pedido.endRua.Trim().Trim().Length > 42 ? rnFuncoes.removeCaracteresEspeciaisDeixaEspaco(rnFuncoes.limpaStringDeixaEspaco(pedido.endRua)).ToUpper().Replace("-", " ").Trim().Trim().Substring(0, 41) : rnFuncoes.removeCaracteresEspeciaisDeixaEspaco(rnFuncoes.limpaStringDeixaEspaco(pedido.endRua)).ToUpper().Replace("-", " ").Trim().Trim());
        nota.AppendFormat("<nro_dest>{0}</nro_dest>", pedido.endNumero.Trim());
        if (!string.IsNullOrEmpty(pedido.endComplemento)) nota.AppendFormat("<xCpl_dest>{0}</xCpl_dest>", rnFuncoes.removeCaracteresEspeciaisDeixaEspaco(rnFuncoes.limpaStringDeixaEspaco(pedido.endComplemento ?? "")).ToUpper().Replace("-", " ").Trim().Trim());
        nota.AppendFormat("<xBairro_dest>{0}</xBairro_dest>", pedido.endBairro.Trim());
        nota.AppendFormat("<cMun_dest>{0}</cMun_dest>", codigoIbge.Trim());
        nota.AppendFormat("<xMun_dest>{0}</xMun_dest>", pedido.endCidade.Trim());
        nota.AppendFormat("<UF_dest>{0}</UF_dest>", pedido.endEstado.Trim());
        nota.AppendFormat("<CEP_dest>{0}</CEP_dest>", pedido.endCep.Replace("-", "").Replace(" ", "").Replace(".", "").Trim());
        nota.Append("<cPais_dest>1058</cPais_dest>");
        nota.Append("<xPais_dest>BRASIL</xPais_dest>");
        if (!string.IsNullOrEmpty(cliente.clienteFoneResidencial)) nota.AppendFormat("<fone_dest>{0}</fone_dest>", foneNfe.Replace("(", "").Replace(")", "").Replace(" ", "").Replace("-", "").Trim());
        nota.Append("</enderDest>");

        //{
        //    nota.Append("<indIEDest>9</indIEDest>");
        //}
        //else if (cliente.clienteCPFCNPJ.Length == 11)
        //{
        //    nota.Append("<indIEDest>2</indIEDest>");
        //}
        // Em conversa entre Adilson e Fabiana da Contabilidade Sposito em 23/01/2017. Fabiana informou que o cód. 9 ( não contribuinte ) 
        // pode ser aplicado para todos os clientes PF indepente da UF do cliente 
        // if (cliente.clienteCPFCNPJ.Length == 11)
        if (ufValidacao.Contains(pedido.endEstado.ToUpper()) && cliente.clienteCPFCNPJ.Length == 11)
        {
            nota.Append("<indIEDest>9</indIEDest>");
        }
        else
        {
            if (!string.IsNullOrEmpty(cliente.clienteRGIE))
            {
                nota.Append("<indIEDest>1</indIEDest>");
                nota.AppendFormat("<IE_dest>{0}</IE_dest>", cliente.clienteRGIE.Trim());
            }
            else
            {
                nota.Append("<indIEDest>2</indIEDest>");
            }
        }
        nota.AppendFormat("<Email_dest>{0}</Email_dest>", cliente.clienteEmail.Trim());
        nota.Append("</dest>");
        #endregion

        #region Autorizacao Emissao
        nota.Append("<autXML>");
        nota.Append("<autXMLItem>");
        nota.AppendFormat("<CNPJ_aut>{0}</CNPJ_aut>", empresaNota.cnpj);
        nota.Append("</autXMLItem>");
        nota.Append("</autXML>");
        #endregion

        #region Detalhamento dos Itens

        nota.Append("<det>");
        List<tbNotaFiscalItem> notaFiscalItens = new List<tbNotaFiscalItem>();
        int nItem = 1;
        var itensDc = new dbCommerceDataContext();
        decimal valorDoDescontoDoCupom = pedido.valorDoDescontoDoCupom > 0 ? (decimal)pedido.valorDoDescontoDoCupom : 0;
        decimal valorDoDescontoDoPagamento = pedido.valorDoDescontoDoPagamento > 0 ? (decimal)pedido.valorDoDescontoDoPagamento : 0;
        decimal totalDesconto = valorDoDescontoDoCupom + valorDoDescontoDoPagamento;
        decimal valorDoDesconto = 0;
        decimal totalDoPedidoUnitario = 0;
        decimal totalDoPedido = 0;
        decimal totalDescontoCalculado = 0;
        decimal totalIcmsOrigem = 0;
        decimal totalIcmsDestino = 0;
        decimal totalIcms = 0;
        decimal totalPis = 0;
        decimal totalCofins = 0;
        decimal totalFrete = 0;

        var data = new dbCommerceDataContext();
        //var produtosCombo = (from c in pedidoDc.tbItensPedidoCombos
        //                     join d in pedidoDc.tbItensPedidos on c.idItemPedido equals d.itemPedidoId
        //                     where d.pedidoId == pedidoId && c.produtoId != 28735 && itensMarcados.Contains(c.produtoId)
        //                     select new { c.idPedidoEnvio, itemPedidoId = d.itemPedidoId, c.produtoId }).ToList();
        //var listaIdsCombo = produtosCombo.Select(x => x.itemPedidoId).ToList();
        //var produtosNaoCombo = (from c in itensDc.tbItensPedidos where c.pedidoId == pedidoId && c.produtoId != 28735 && !listaIdsCombo.Contains(c.itemPedidoId) && itensMarcados.Contains(c.produtoId) select new { c.idPedidoEnvio, c.itemPedidoId, c.produtoId }).ToList();

        var itensNotaOrigem = (from c in data.tbNotaFiscalItems where c.idNotaFiscal == idNotaOrigem select c).ToList();

        //var produtos = produtosCombo.Union(produtosNaoCombo).ToList();
        var produtos = (from c in pedidoDc.tbItensPedidos
                        join p in pedidoDc.tbItemPedidoEstoques on c.itemPedidoId equals p.itemPedidoId
                        where c.pedidoId == pedidoId
                        select new { c.idPedidoEnvio, c.itemPedidoId, p.produtoId, p.tbProduto.produtoPeso }).ToList();

        if (idsItemPedidoEstoque != null && idsItemPedidoEstoque.Count > 0)
        {
            produtos = (from c in pedidoDc.tbItensPedidos
                        join p in pedidoDc.tbItemPedidoEstoques on c.itemPedidoId equals p.itemPedidoId
                        where c.pedidoId == pedidoId && idsItemPedidoEstoque.Contains(p.idItemPedidoEstoque)
                        select new { c.idPedidoEnvio, c.itemPedidoId, p.produtoId, p.tbProduto.produtoPeso }).ToList();
        }

        var pacotes = (from c in pedidoDc.tbPedidoPacotes where c.idPedido == idPedido select c);
        //decimal valorFrete = 0;

        decimal pesoTotal = 0;
        if (pacotes.Any())
        {
            pesoTotal = pacotes.Sum(x => x.peso);
        }
        else
        {
            foreach (var item in produtos)
            {
                var produtoDc = new dbCommerceDataContext();
                pesoTotal += Convert.ToDecimal(item.produtoPeso);
            }
        }

        bool totalBrinde = true;
        bool totalAssistencia = true;

        int indiceProduto = 0;
        int qtdProdutosDevolvidos = produtos.Count;
        int qtdTotalBrindes = 0;
        string estado = pedido.endEstado.Trim().ToLower();
        decimal totalBcIcms = 0;

        foreach (var produtoEnviado in produtos)
        {
            var itemEnviado = (from c in itensNotaOrigem where c.produtoId == produtoEnviado.produtoId select c).First();


            totalDoPedidoUnitario += Convert.ToDecimal((itemEnviado.vUnCom ?? 0));
            totalDoPedido += Convert.ToDecimal((itemEnviado.vUnCom ?? 0));
            totalDescontoCalculado += (itemEnviado.vDesc ?? 0);
            totalPis += itemEnviado.vPIS ?? 0;
            totalCofins += itemEnviado.vCOFINS ?? 0;

            indiceProduto++;

            var aliquota = itemEnviado.aliquotaIcms ?? 18;
            if ((itemEnviado.vICMSUFDest ?? 0) > 0)
            {
                aliquota = Convert.ToInt32((itemEnviado.pICMSUFDest ?? 0));
                totalIcmsOrigem += (itemEnviado.vICMSUFRemet ?? 0);
                totalIcmsDestino += (itemEnviado.vICMSUFDest ?? 0);
            }
            //var valorIcms = ((Convert.ToDecimal(valorDoProduto)) / 100) * aliquota;
            var valorIcms = Convert.ToDecimal((itemEnviado.vICMS_icms ?? 0).ToString("0.00"));

            if (itemEnviado.cfop == "5949")
            {
                finNFe = 1;
                cfop = "1949";
                totalBrinde = false;
            }
            else if (itemEnviado.cfop == "6949")
            {
                finNFe = 1;
                cfop = "2949";
                totalBrinde = false;
            }
            else if (itemEnviado.cfop == "5910")
            {
                finNFe = 1;
                cfop = "1949";
                totalAssistencia = false;
            }
            else if (itemEnviado.cfop == "6910")
            {
                finNFe = 1;
                cfop = "2949";
                totalAssistencia = false;
            }
            else
            {
                totalAssistencia = false;
                totalBrinde = false;
            }

            totalIcms += valorIcms;
            totalFrete += itemEnviado.vFrete ?? 0;
            totalBcIcms += (itemEnviado.vBC_icms ?? 0);
            nota.AppendFormat("<detItem>", nItem);
            nota.Append("<prod>");
            nota.AppendFormat("<cProd>{0}</cProd>", itemEnviado.produtoId);
            string produtoNomeCompactado = limitaTexto(rnFuncoes.removeCaracteresEspeciaisDeixaEspaco(rnFuncoes.limpaStringDeixaEspaco(itemEnviado.produtoNome)).ToUpper().Replace("-", " ").Trim(), 119).Trim();
            nota.AppendFormat("<xProd>{0}</xProd>", produtoNomeCompactado);
            nota.AppendFormat("<NCM>{0}</NCM>", itemEnviado.ncm.Trim());
            nota.AppendFormat("<CFOP>{0}</CFOP>", cfop);
            nota.AppendFormat("<uCOM>{0}</uCOM>", "UN");
            nota.AppendFormat("<qCOM>{0}</qCOM>", Convert.ToInt32(1).ToString("0.0000").Replace(",", "."));
            nota.AppendFormat("<vUnCom>{0}</vUnCom>", (itemEnviado.vUnCom ?? 0).ToString("0.00").Replace(",", "."));
            nota.AppendFormat("<vProd>{0}</vProd>", (Decimal.Round((itemEnviado.vProd ?? 0), 2)).ToString("0.00").Replace(",", "."));
            nota.Append("<cEANTrib/>");
            nota.AppendFormat("<uTrib>{0}</uTrib>", "UN");
            nota.AppendFormat("<qTrib>{0}</qTrib>", Convert.ToInt32(1).ToString("0.00").Replace(",", "."));
            nota.AppendFormat("<vUnTrib>{0}</vUnTrib>", (itemEnviado.vUnTrib ?? 0).ToString("0.00").Replace(",", "."));
            if ((itemEnviado.vFrete ?? 0) > 0) nota.AppendFormat("<vFrete>{0}</vFrete>", (itemEnviado.vFrete ?? 0).ToString("0.00").Replace(",", "."));
            if ((itemEnviado.vDesc ?? 0) > 0) nota.AppendFormat("<vDesc>{0}</vDesc>", (itemEnviado.vDesc ?? 0).ToString("0.00").Replace(",", "."));
            nota.AppendFormat("<indTot>{0}</indTot>", "1");
            nota.AppendFormat("<nTipoItem>{0}</nTipoItem>", "0");
            nota.Append("</prod>");

            string CST_pis = "";
            string CST_cofins = "";
            if (empresaNota.simples)
            {
                nota.Append("<imposto>");
                nota.Append("<ICMS>");
                nota.Append("<orig>0</orig>");
                nota.Append("<CST>102</CST>");
                nota.Append("</ICMS>");
                nota.Append("<PIS>");
                CST_pis = (itemEnviado.cfop == "5910" || itemEnviado.cfop == "6910" ? "49" : "07");
                nota.AppendFormat("<CST_pis>{0}</CST_pis>", CST_pis);
                nota.Append("</PIS>");
                nota.Append("<COFINS>");
                CST_cofins = (itemEnviado.cfop == "5910" || itemEnviado.cfop == "6910" ? "49" : "07");
                nota.AppendFormat("<CST_cofins>{0}</CST_cofins>", CST_cofins);
                nota.Append("</COFINS>");
                if ((itemEnviado.vICMSUFDest ?? 0) > 0)
                {
                    nota.Append("<ICMSUFDest>");
                    nota.AppendFormat("<vBCUFDest>{0}</vBCUFDest>", (itemEnviado.vBCUFDest ?? 0).ToString("0.00").Replace(",", "."));
                    nota.Append("<pFCPUFDest>0.00</pFCPUFDest>");
                    nota.AppendFormat("<pICMSUFDest>{0}</pICMSUFDest>", (itemEnviado.pICMSUFDest ?? 0).ToString("0.00").Replace(",", "."));
                    nota.AppendFormat("<pICMSInter>{0}</pICMSInter>", (itemEnviado.pICMSInter ?? 0).ToString("0.00").Replace(",", "."));
                    nota.AppendFormat("<pICMSInterPart>{0}</pICMSInterPart>", (itemEnviado.pICMSInterPart ?? 0).ToString("0.00").Replace(",", "."));
                    nota.AppendFormat("<vFCPUFDest>{0}</vFCPUFDest>", (0).ToString("0.00").Replace(",", "."));
                    nota.AppendFormat("<vICMSUFDest>{0}</vICMSUFDest>", ((itemEnviado.vICMSUFDest ?? 0)).ToString("0.00").Replace(",", "."));
                    nota.AppendFormat("<vICMSUFRemet>{0}</vICMSUFRemet>", ((itemEnviado.vICMSUFRemet ?? 0)).ToString("0.00").Replace(",", "."));
                    nota.Append("</ICMSUFDest>");
                }
                nota.Append("</imposto>");
            }
            else
            {
                nota.Append("<imposto>");
                nota.Append("<ICMS>");
                nota.Append("<orig>0</orig>");
                nota.Append("<CST>00</CST>");
                nota.Append("<modBC>3</modBC>");
                nota.AppendFormat("<vBC>{0}</vBC>", ((itemEnviado.vBC_icms ?? 0)).ToString("0.00").Replace(",", "."));
                nota.AppendFormat("<pICMS>{0}</pICMS>", ((itemEnviado.pICMS ?? 0)).ToString("0.00").Replace(",", "."));
                nota.AppendFormat("<vICMS_icms>{0}</vICMS_icms>", ((itemEnviado.vICMS_icms ?? 0)).ToString("0.00").Replace(",", "."));
                nota.Append("</ICMS>");
                nota.Append("<PIS>");
                //nota.AppendFormat("<CST_pis>{0}</CST_pis>", (cfopProduto == "5910" || cfopProduto == "6910" ? "49" : "01"));
                CST_pis = (itemEnviado.cfop == "5910" || itemEnviado.cfop == "6910" ? "49" : "98");
                nota.AppendFormat("<CST_pis>{0}</CST_pis>", CST_pis);//alterado a pedido da contabiliade 27/12/2016
                nota.AppendFormat("<vBC_pis>{0}</vBC_pis>", ((itemEnviado.vBC_pis ?? 0)).ToString("0.00").Replace(".", "").Replace(",", "."));
                nota.Append("<pPIS>0.65</pPIS>");
                nota.AppendFormat("<vPIS>{0}</vPIS>", ((itemEnviado.vPIS ?? 0)).ToString("0.00").Replace(".", "").Replace(",", "."));
                nota.Append("</PIS>");
                nota.Append("<COFINS>");
                //nota.AppendFormat("<CST_cofins>{0}</CST_cofins>", (cfopProduto == "5910" || cfopProduto == "6910" ? "49" : "01"));
                CST_cofins = (itemEnviado.cfop == "5910" || itemEnviado.cfop == "6910" ? "49" : "98");
                nota.AppendFormat("<CST_cofins>{0}</CST_cofins>", CST_cofins);//alterado a pedido da contabiliade 27/12/2016
                nota.AppendFormat("<vBC_cofins>{0}</vBC_cofins>", ((itemEnviado.vBC_cofins ?? 0)).ToString("0.00").Replace(".", "").Replace(",", "."));
                nota.Append("<pCOFINS>3.00</pCOFINS>");
                nota.AppendFormat("<vCOFINS>{0}</vCOFINS>", ((itemEnviado.vCOFINS ?? 0)).ToString("0.00").Replace(".", "").Replace(",", "."));

                nota.Append("</COFINS>");

                nota.Append("</imposto>");
            }
            nota.Append("</detItem>");
            nItem++; tbNotaFiscalItem notaFiscalItem = new tbNotaFiscalItem();
            notaFiscalItem.cfop = itemEnviado.cfop;
            notaFiscalItem.ncm = itemEnviado.ncm.Trim();
            notaFiscalItem.produtoId = itemEnviado.produtoId;
            notaFiscalItem.produtoNome = produtoNomeCompactado;
            notaFiscalItem.qCOM = 1;
            notaFiscalItem.vUnCom = (itemEnviado.vUnCom ?? 0);
            notaFiscalItem.vProd = (itemEnviado.vProd ?? 0);
            notaFiscalItem.qTrib = 1;
            notaFiscalItem.vUnTrib = (itemEnviado.vUnTrib ?? 0);
            notaFiscalItem.vFrete = (itemEnviado.vFrete ?? 0);
            notaFiscalItem.CST_pis = CST_pis;
            notaFiscalItem.CST_cofins = CST_cofins;
            notaFiscalItem.aliquotaIcms = aliquota;

            notaFiscalItens.Add(notaFiscalItem);

        }

        nota.Append("</det>");
        #endregion

        #region Totais
        nota.Append("<total>");
        nota.Append("<ICMStot>");
        if (empresaNota.simples || (empresaNota.idEmpresa == 2 && estado != "sp" && cliente.clienteCPFCNPJ.Length == 11))
        {
            nota.Append("<vBC_ttlnfe>0.00</vBC_ttlnfe>");
            nota.Append("<vICMS_ttlnfe>0.00</vICMS_ttlnfe>");
        }
        else
        {
            nota.AppendFormat("<vBC_ttlnfe>{0}</vBC_ttlnfe>", Convert.ToDecimal(totalBcIcms).ToString("0.00").Replace(",", "."));
            nota.AppendFormat("<vICMS_ttlnfe>{0}</vICMS_ttlnfe>", Convert.ToDecimal(totalIcms).ToString("0.00").Replace(",", "."));
        }


        nota.Append("<vBCST_ttlnfe>0.00</vBCST_ttlnfe>");
        nota.Append("<vST_ttlnfe>0.00</vST_ttlnfe>");
        nota.AppendFormat("<vProd_ttlnfe>{0}</vProd_ttlnfe>", Convert.ToDecimal(totalDoPedido).ToString("0.00").Replace(",", "."));
        if (totalFrete > 0) nota.AppendFormat("<vFrete_ttlnfe>{0}</vFrete_ttlnfe>", totalFrete.ToString("0.00").Replace(",", "."));
        else nota.AppendFormat("<vFrete_ttlnfe>{0}</vFrete_ttlnfe>", (totalFrete).ToString("0.00").Replace(",", "."));
        nota.Append("<vSeg_ttlnfe>0.00</vSeg_ttlnfe>");
        nota.AppendFormat("<vDesc_ttlnfe>{0}</vDesc_ttlnfe>", totalDescontoCalculado.ToString("0.00").Replace(",", "."));
        //nota.Append("<vII>0.00</vII>");
        nota.Append("<vIPI_ttlnfe>0.00</vIPI_ttlnfe>");

        if (empresaNota.simples && empresaNota.idEmpresa != 2)
            nota.Append("<vPIS_ttlnfe>0.00</vPIS_ttlnfe>");
        else if (empresaNota.idEmpresa == 2 && estado != "sp" && cliente.clienteCPFCNPJ.Length == 11)
            nota.AppendFormat("<vPIS_ttlnfe>{0}</vPIS_ttlnfe>", totalPis.ToString("0.00").Replace(",", "."));
        else
            nota.AppendFormat("<vPIS_ttlnfe>{0}</vPIS_ttlnfe>", totalPis.ToString("0.00").Replace(",", "."));

        if (empresaNota.simples && empresaNota.idEmpresa != 2)
            nota.Append("<vCOFINS_ttlnfe>0.00</vCOFINS_ttlnfe>");
        else if (empresaNota.idEmpresa == 2 && estado != "sp" && cliente.clienteCPFCNPJ.Length == 11)
            nota.AppendFormat("<vCOFINS_ttlnfe>{0}</vCOFINS_ttlnfe>", totalCofins.ToString("0.00").Replace(",", "."));
        else
            nota.AppendFormat("<vCOFINS_ttlnfe>{0}</vCOFINS_ttlnfe>", totalCofins.ToString("0.00").Replace(",", "."));

        nota.Append("<vOutro>0.00</vOutro>");
        nota.Append("<vICMSDeson_ttlnfe>0.00</vICMSDeson_ttlnfe>");
        nota.AppendFormat("<vNF>{0}</vNF>", Convert.ToDecimal(totalDoPedido + totalFrete - totalDescontoCalculado).ToString("0.00").Replace(",", "."));
        nota.Append("<vTotTrib_ttlnfe>0.00</vTotTrib_ttlnfe>");
        nota.Append("</ICMStot>");
        nota.Append("</total>");
        #endregion

        #region transportadora
        nota.Append("<transp>");
        nota.Append("<modFrete>0</modFrete>");


        var formaDeEnvio = "";
        if (!String.IsNullOrEmpty(transportadora))
            formaDeEnvio = transportadora;
        // Seleciona transportadora manualmente pelo ddlTransportadora em admin/pedido.aspx , popup : pcNotaDevolucao

        var envioPacote = pacotes.FirstOrDefault(x => x.formaDeEnvio != "" && x.formaDeEnvio != null);
        int tipoDeEntregaId = 0;
        if (envioPacote != null)
        {
            formaDeEnvio = envioPacote.formaDeEnvio;
            if (envioPacote.tbPedidoEnvio.tipoDeEntregaId != null)
            {
                tipoDeEntregaId = envioPacote.tbPedidoEnvio.tipoDeEntregaId ?? 0;
            }
        }


        if (tipoDeEntregaId > 0)
        {
            var tipoDeEntrega = (from c in data.tbTipoDeEntregas where c.tipoDeEntregaId == tipoDeEntregaId select c).FirstOrDefault();
            if (tipoDeEntrega != null)
            {
                nota.Append("<transporta>");
                nota.AppendFormat("<CNPJ_transp>{0}</CNPJ_transp>", tipoDeEntrega.cnpj);
                nota.AppendFormat("<xNome_transp>{0}</xNome_transp>", tipoDeEntrega.razaoSocial);
                nota.AppendFormat("<IE_transp>{0}</IE_transp>", tipoDeEntrega.ie);
                nota.AppendFormat("<xEnder>{0}</xEnder>", tipoDeEntrega.endereco);
                nota.AppendFormat("<xMun_transp>{0}</xMun_transp>", tipoDeEntrega.municipio);
                nota.AppendFormat("<UF_transp>{0}</UF_transp>", tipoDeEntrega.uf);
                nota.Append("</transporta>");
            }
        }
        else
        {
            List<string> modalidadeJadlog = new List<string> { "jadlog", "jadlogexpressa", "jadlogcom", "jadlogrodo" };
            if (modalidadeJadlog.Contains(formaDeEnvio))
            {
                nota.Append("<transporta>");
                nota.Append("<CNPJ_transp>14135995000102</CNPJ_transp>");
                nota.Append("<xNome_transp>CALLILI EXPRESS LTDA ME</xNome_transp>");
                nota.Append("<IE_transp>344063890114</IE_transp>");
                nota.Append("<xEnder>RUA DANIEL DE FREITAS , 928 - CENTRO</xEnder>");
                nota.Append("<xMun_transp>Ibitinga</xMun_transp>");
                nota.Append("<UF_transp>SP</UF_transp>");
                nota.Append("</transporta>");
            }
            else if (formaDeEnvio == "plimor")
            {
                nota.Append("<transporta>");
                nota.Append("<CNPJ_transp>88085485006226</CNPJ_transp>");
                nota.Append("<xNome_transp>TRANSPORTADORA PLIMOR LTDA</xNome_transp>");
                nota.Append("<IE_transp>209364638111</IE_transp>");
                nota.Append("<xEnder>Marechal Rondon - km 334 - SN</xEnder>");
                nota.Append("<xMun_transp>Bauru</xMun_transp>");
                nota.Append("<UF_transp>SP</UF_transp>");
                nota.Append("</transporta>");
            }
            else if (formaDeEnvio == "lbr")
            {
                nota.Append("<transporta>");
                nota.Append("<CNPJ_transp>16660697000248</CNPJ_transp>");
                nota.Append("<xNome_transp>LBR EXPRESS TRANSPORTES E LOGÍSTICA LTDA</xNome_transp>");
                nota.Append("<IE_transp>0028344510058</IE_transp>");
                nota.Append("<xEnder>Rua Quintino Bocaiúva</xEnder>");
                nota.Append("<xMun_transp>Contagem</xMun_transp>");
                nota.Append("<UF_transp>MG</UF_transp>");
                nota.Append("</transporta>");
            }
            else if (formaDeEnvio == "jamef")
            {
                nota.Append("<transporta>");
                nota.Append("<CNPJ_transp>20147617002276</CNPJ_transp>");
                nota.Append("<xNome_transp>Jamef Transporte LTDA</xNome_transp>");
                nota.Append("<IE_transp>114387171114</IE_transp>");
                nota.Append("<xEnder>Rua Miguel Mentem 500</xEnder>");
                nota.Append("<xMun_transp>Vila Guilherme</xMun_transp>");
                nota.Append("<UF_transp>SP</UF_transp>");
                nota.Append("</transporta>");
            }
            else if (formaDeEnvio == "nowlog")
            {
                nota.Append("<transporta>");
                nota.Append("<CNPJ_transp>20712076000238</CNPJ_transp>");
                nota.Append("<xNome_transp>Nowlog Logistica Inteligente LTDA</xNome_transp>");
                nota.Append("<IE_transp>0026412980032</IE_transp>");
                nota.Append("<xEnder>Avenida Mestra Fininha, 1726, Letra A, Sala 1</xEnder>");
                nota.Append("<xMun_transp>Montes Claros</xMun_transp>");
                nota.Append("<UF_transp>MG</UF_transp>");
                nota.Append("</transporta>");
            }
            else if (formaDeEnvio == "dialogo")
            {
                nota.Append("<transporta>");
                nota.Append("<CNPJ_transp>21930065000297</CNPJ_transp>");
                nota.Append("<xNome_transp>Dialogo Logistica Inteligente LTDA</xNome_transp>");
                nota.Append("<IE_transp>144872110110</IE_transp>");
                nota.Append("<xEnder>Rodovia Dom Gabriel Paulino Bueno KM 71 M06 G2, Medeiros</xEnder>");
                nota.Append("<xMun_transp>Jundiai</xMun_transp>");
                nota.Append("<UF_transp>SP</UF_transp>");
                nota.Append("</transporta>");
            }
            else if (formaDeEnvio == "totalexpress")
            {
                nota.Append("<transporta>");
                nota.Append("<CNPJ_transp>73939449000193</CNPJ_transp>");
                nota.Append("<xNome_transp>TEX COURIER S.A.</xNome_transp>");
                nota.Append("<IE_transp>206214714111</IE_transp>");
                nota.Append("<xEnder>AVENIDA PIRACEMA 155 GALPAO 1</xEnder>");
                nota.Append("<xMun_transp>Barueri</xMun_transp>");
                nota.Append("<UF_transp>SP</UF_transp>");
                nota.Append("</transporta>");
            }
            else if (formaDeEnvio == "tnt")
            {
                if (envioPacote.idCentroDistribuicao == 3)
                {
                    nota.Append("<transporta>");
                    nota.Append("<CNPJ_transp>95591723003800</CNPJ_transp>");
                    nota.Append("<xNome_transp>TNT Mercurio Cargas e Encomendas Expressas SA</xNome_transp>");
                    nota.Append("<IE_transp>108254280116</IE_transp>");
                    nota.Append("<xEnder>Avenida Marginal Direita do Tiete N 2500</xEnder>");
                    nota.Append("<xMun_transp>Sao Paulo</xMun_transp>");
                    nota.Append("<UF_transp>SP</UF_transp>");
                    nota.Append("</transporta>");
                }
                else
                {
                    nota.Append("<transporta>");
                    nota.Append("<CNPJ_transp>95591723008284</CNPJ_transp>");
                    nota.Append("<xNome_transp>TNT Mercurio Cargas e Encomendas Expressas SA</xNome_transp>");
                    nota.Append("<IE_transp>209199664118</IE_transp>");
                    nota.Append("<xEnder>RUA JOAQUIM PELEGRINA LOPES,1-80 - Dist. Ind. III</xEnder>");
                    nota.Append("<xMun_transp>Bauru</xMun_transp>");
                    nota.Append("<UF_transp>SP</UF_transp>");
                    nota.Append("</transporta>");
                }
            }
        }
        int vol = 1;
        nota.Append("<vol>");
        bool nfeVolumeTotal = false;
        if (tipoDeEntregaId > 0)
        {
            var entrega = (from c in data.tbTipoDeEntregas where c.tipoDeEntregaId == tipoDeEntregaId select c).FirstOrDefault();
            if (entrega != null)
            {
                nfeVolumeTotal = (entrega.nfeVolumeTotal ?? false);
            }
        }
        if (nfeVolumeTotal)
        {
            nota.Append("<volItem>");
            nota.AppendFormat("<qVol>{0}</qVol>", pacotes.Count());
            nota.Append("<esp>VOLUMES</esp>");
            nota.AppendFormat("<nVol>{0}</nVol>", vol);
            nota.AppendFormat("<pesoB_transp>{0}</pesoB_transp>", (Convert.ToDecimal(pacotes.Sum(x => x.peso)) / 1000).ToString("0.000").Replace(",", "."));
            nota.Append("</volItem>");
            vol++;
        }
        else
        {
            foreach (var pacote in pacotes.OrderBy(x => x.numeroVolume))
            {
                nota.Append("<volItem>");
                nota.AppendFormat("<qVol>{0}</qVol>", 1);
                nota.Append("<esp>VOLUME</esp>");
                nota.AppendFormat("<nVol>{0}</nVol>", vol);
                nota.AppendFormat("<pesoB_transp>{0}</pesoB_transp>", (Convert.ToDecimal(pacote.peso) / 1000).ToString("0.000").Replace(",", "."));
                nota.Append("</volItem>");
                vol++;
            }
        }

        nota.Append("</vol>");
        nota.Append("</transp>");

        string informacoesAdicionais = "PEDIDO " + rnFuncoes.retornaIdCliente(pedido.pedidoId);
        if (!string.IsNullOrEmpty(pedido.endReferenciaParaEntrega)) informacoesAdicionais += " - Ref.: " + rnFuncoes.removeAcentos(pedido.endReferenciaParaEntrega.Replace("&", "").Replace("(", "").Replace(")", "").Replace("-", "").Replace("\n", " ").Replace("\r", " ").Replace("’", "'").Replace("•", " ").TrimEnd());
        string telefone = "";
        if (!string.IsNullOrEmpty(cliente.clienteFoneResidencial)) telefone += "Res.: " + cliente.clienteFoneResidencial + " - ";
        if (!string.IsNullOrEmpty(cliente.clienteFoneComercial)) telefone += "Com.: " + cliente.clienteFoneComercial + " - ";
        if (!string.IsNullOrEmpty(cliente.clienteFoneCelular)) telefone += "Cel.: " + cliente.clienteFoneCelular;
        if (!string.IsNullOrEmpty(telefone)) informacoesAdicionais += " - " + telefone;

        if (estado != "sp" && cliente.clienteCPFCNPJ.Length == 11)
        {
            informacoesAdicionais += " - Convenio 93/15 - ";
            informacoesAdicionais += "Total do ICMS de partilha para a UF do destinatario R$" + totalIcmsDestino.ToString("0.00") + " - ";
            informacoesAdicionais += "Total do ICMS interestadual para a UF do remetente R$" + totalIcmsOrigem.ToString("0.00") + " - ";
        }

        if (empresaNota.simples) informacoesAdicionais += " - I DOCUMENTO EMITIDO POR EMPRESA ME OU EPP OPTANTE PELO SIMPLES NACIONAL II- NAO GERA CREDITO FISCAL DE IPI E ISS.";
        if (empresaNota.idEmpresa == 3 || empresaNota.idEmpresa == 1) informacoesAdicionais += " - Lei 12.741 de 12/2012 Carga Tributária 29,33% ";

        informacoesAdicionais += (notaFiscalItens.Count() > idsItemPedidoEstoque.Count) ? " Devolução parcial referente a NF-e " + notaOrigem.nfeKey + "." : " Devolução referente a NF-e " + notaOrigem.nfeKey + ".";

        informacoesAdicionais += informacoesAdicionaisExtra;

        informacoesAdicionais = informacoesAdicionais.Replace(Environment.NewLine, " ");
        nota.Append("<infAdic>");
        nota.AppendFormat("<infCpl>{0}</infCpl>", informacoesAdicionais);
        nota.Append("</infAdic>");
        #endregion

        pedidoDc.SubmitChanges();

        var dataAlt = new dbCommerceDataContext();
        var notaAlt = (from c in dataAlt.tbNotaFiscals where c.idNotaFiscal == gerarNota.idNotaFiscal select c).First();
        notaAlt.valor = Convert.ToDecimal(totalDoPedido + totalFrete - valorDoDesconto);
        dataAlt.SubmitChanges();
        try
        {
            System.IO.File.WriteAllText(System.Configuration.ConfigurationManager.AppSettings["caminhoFisico"] + "\\admin\\notas\\" + pedidoId + "_" + numeroNota + ".xml", nota.ToString());
        }
        catch (Exception ex)
        { }
        string notaFormatado = nota.ToString();




        notaFormatado = notaFormatado.Replace("finNFe", "<finNFe>" + finNFe + "</finNFe>");
        if (totalBrinde) natOp = "Retorno de Brinde";
        if (totalAssistencia) natOp = "Retorno para Troca";

        notaFormatado = notaFormatado.Replace("natOp", "<natOp>" + natOp + "</natOp>");

        var notaRetorno = new notaGerada();
        notaRetorno.xml = notaFormatado;
        notaRetorno.idNotaFiscal = gerarNota.idNotaFiscal;
        if (gerarNota.statusNfe != 2)
        {
            var notasItensAtuais = (from c in data.tbNotaFiscalItems where c.idNotaFiscal == gerarNota.idNotaFiscal select c).ToList();
            foreach (var notaItemAtual in notasItensAtuais)
            {
                data.tbNotaFiscalItems.DeleteOnSubmit(notaItemAtual);
            }
            data.SubmitChanges();
            foreach (var item in notaFiscalItens)
            {
                item.idNotaFiscal = notaRetorno.idNotaFiscal;

                data.tbNotaFiscalItems.InsertOnSubmit(item);
                try
                {
                    data.SubmitChanges();
                    //var notaItemOrigem = (from c in data.tbNotaFiscalItems where c.idNotaFiscal == idNotaOrigem && c.idNotaFistalItemDevolucao == null && c.produtoId == item.produtoId select c).FirstOrDefault();
                    var notaItemOrigem = (from c in data.tbNotaFiscalItems where c.idNotaFiscal == idNotaOrigem && c.produtoId == item.produtoId select c).FirstOrDefault();
                    if (notaItemOrigem != null)
                    {
                        notaItemOrigem.idNotaFistalItemDevolucao = item.idNotaFiscalItem;
                    }
                    data.SubmitChanges();
                }
                catch (Exception ex)
                {
                    string erro = ex.Message;
                }

            }
        }
        return notaRetorno;
    }

    public static notaGerada gerarNotaFiscalDevolucao(int idPedido, string codigoIbge, List<int> itensMarcados, bool gerarNovaNota, int idEmpresa, string keyNotaFiscalReferencia,
                                                      string informacoesAdicionaisExtra = "", List<int> idsItemPedidoEstoque = null, string numeroNfeReferencia = "", bool devolucaoParcial = false)
    {
        bool totalBrinde = true;

        string cfop = "";
        var pedidoDc = new dbCommerceDataContext();
        var pedido = (from c in pedidoDc.tbPedidos where c.pedidoId == idPedido select c).FirstOrDefault();

        int idNotaOrigem = Convert.ToInt32(numeroNfeReferencia);
        if (pedido == null) return new notaGerada();
        int pedidoId = pedido.pedidoId;

        var clienteDc = new dbCommerceDataContext();
        var cliente = (from c in clienteDc.tbClientes where c.clienteId == pedido.clienteId select c).FirstOrDefault();

        var gerarNota = gerarNumeroNotaEmpresa(pedido.pedidoId, 0, gerarNovaNota, idEmpresa);

        if ((gerarNota.complemento ?? false) == true)
        {
            return gerarNotaFiscalComplemento((int)gerarNota.numeroNotaComplemento, (int)gerarNota.idCNPJ, codigoIbge);
        }
        var empresaNota = (from c in pedidoDc.tbEmpresas where c.idEmpresa == gerarNota.idCNPJ select c).First();

        string numeroNota = gerarNota.numeroNota.ToString();
        string chave = gerarNota.nfeKey;
        string cDV = GerarDigitoVerificadorNFe(chave).ToString();
        chave += cDV;

        string idDest = "";

        bool gerarGnre = false;
        bool pagarGnre = false;
        var configuracaoGnre = (from c in pedidoDc.tbGnreConfiguracaos where c.uf == pedido.endEstado.ToLower() select c).FirstOrDefault();
        if (configuracaoGnre != null)
        {
            gerarGnre = (configuracaoGnre.gerarGnre ?? false);
            pagarGnre = (configuracaoGnre.pagamentoGnre ?? false);
        }
        if (empresaNota.idEmpresa == 2)
        {
            if (cliente.clienteCPFCNPJ.Length == 11 && pedido.endEstado.ToLower() == "sp")
            {
                cfop = "1201";
                idDest = "1";
            }
            else if (cliente.clienteCPFCNPJ.Length > 11 && pedido.endEstado.ToLower() == "sp")
            {
                cfop = "1201";
                idDest = "1";
            }
            else if (cliente.clienteCPFCNPJ.Length == 11 && pedido.endEstado.ToLower() != "sp")
            {
                cfop = "2201";
                idDest = "2";
            }
            else
            {
                cfop = "2201";
                idDest = "2";
            }
        }
        else if (empresaNota.idEmpresa == 6)
        {
            if (cliente.clienteCPFCNPJ.Length == 11 && pedido.endEstado.ToLower() == "mg")
            {
                cfop = "1202";
                idDest = "1";
            }
            else if (cliente.clienteCPFCNPJ.Length > 11 && pedido.endEstado.ToLower() == "mg")
            {
                cfop = "1202";
                idDest = "1";
            }
            else if (cliente.clienteCPFCNPJ.Length == 11 && pedido.endEstado.ToLower() != "mg")
            {
                cfop = "2202";
                idDest = "2";
            }
            else
            {
                cfop = "2202";
                idDest = "2";
            }
        }
        else
        {
            if (cliente.clienteCPFCNPJ.Length == 11 && pedido.endEstado.ToLower() == "sp")
            {
                cfop = "1202";
                idDest = "1";
            }
            else if (cliente.clienteCPFCNPJ.Length > 11 && pedido.endEstado.ToLower() == "sp")
            {
                cfop = "1202";
                idDest = "1";
            }
            else if (cliente.clienteCPFCNPJ.Length == 11 && pedido.endEstado.ToLower() != "sp")
            {
                cfop = "2202";
                idDest = "2";
            }
            else
            {
                cfop = "2202";
                idDest = "2";
            }
        }



        var nota = new StringBuilder();

        #region Identificadores da NF-e
        nota.Append("<ide>");
        nota.AppendFormat("<cUF>{0}</cUF>", empresaNota.codigoUfEmitente);
        nota.AppendFormat("<cNF>{0}</cNF>", pedidoId.ToString().PadLeft(8, '0'));

        if (empresaNota.idEmpresa == 2)
        {
            if ((cliente.clienteCPFCNPJ.Length == 11 | cliente.clienteCPFCNPJ.Length > 11) && pedido.endEstado.ToLower() == "sp")
            {
                nota.Append("<natOp>Devolucao de Venda de Producao</natOp>");
            }
            else if (pedido.endEstado.ToLower() != "sp")
            {
                nota.Append("<natOp>Devolucao de Venda de Producao</natOp>");
            }
            else
            {
                nota.Append("<natOp>Devolucao de Venda de Producao</natOp>");
            }
        }
        else
        {
            nota.Append("<natOp>Devolucao Mercadoria Adquirida ou Recebida de Terceiros</natOp>");
        }

        List<string> ufValidacao = new List<string> { "AM", "BA", "CE", "GO", "MG", "MS", "MT", "PE", "RN", "SE", "SP", "PA", "RJ", "ES", "PR" };

        nota.Append("<indPag>0</indPag>");
        nota.Append("<mod>4</mod>");
        nota.Append("<serie>1</serie>");
        nota.AppendFormat("<nNF>{0}</nNF>", numeroNota);
        nota.AppendFormat("<dhEmi>{0}T{1}</dhEmi>", DateTime.Now.ToString("yyyy-MM-dd"), DateTime.Now.ToString("HH:mm:ss"));

        if (dataSaida != null)
        {
            nota.AppendFormat("<dhSaiEnt>{0}T{1}</dhSaiEnt>", Convert.ToDateTime(dataSaida).ToString("yyyy-MM-dd"), Convert.ToDateTime(dataSaida).ToString("HH:mm:ss"));
        }


        nota.Append("<fusoHorario>-02:00</fusoHorario>");
        nota.Append("<tpNf>0</tpNf>");
        nota.AppendFormat("<idDest>{0}</idDest>", idDest);

        if (ufValidacao.Contains(pedido.endEstado.ToUpper()) && cliente.clienteCPFCNPJ.Length == 11)
            nota.AppendFormat("<indFinal>1</indFinal>");
        else
            nota.AppendFormat("<indFinal>0</indFinal>");

        nota.AppendFormat("<indPres>2</indPres>");
        nota.AppendFormat("<cMunFg>{0}</cMunFg>", empresaNota.codigoMunicipioEmitente);

        nota.Append("<NFRef>");
        nota.Append("<NFRefItem>");
        nota.AppendFormat("<refNFe>{0}</refNFe>", keyNotaFiscalReferencia + GerarDigitoVerificadorNFe(keyNotaFiscalReferencia));
        nota.Append("</NFRefItem>");
        nota.Append("</NFRef>");

        nota.Append("<tpImp>1</tpImp>");
        nota.Append("<tpEmis>1</tpEmis>");
        nota.AppendFormat("<tpAmb>{0}</tpAmb>", tpAmb);
        nota.Append("<finNFe>4</finNFe>");
        nota.Append("</ide>");
        #endregion

        #region Emitente

        nota.Append("<emit>");
        nota.AppendFormat("<CNPJ_emit>{0}</CNPJ_emit>", empresaNota.cnpj);
        nota.AppendFormat("<xNome>{0}</xNome>", empresaNota.nomeEmitente);
        nota.AppendFormat("<xFant>{0}</xFant>", empresaNota.nomeFantasiaEmitente);
        nota.Append("<enderEmit>");
        nota.AppendFormat("<xLgr>{0}</xLgr>", empresaNota.logradouroEmitente);
        nota.AppendFormat("<nro>{0}</nro>", empresaNota.numeroEmitente);
        nota.AppendFormat("<xBairro>{0}</xBairro>", empresaNota.bairroEmitente);
        nota.AppendFormat("<cMun>{0}</cMun>", empresaNota.codigoMunicipioEmitente);
        nota.AppendFormat("<xMun>{0}</xMun>", empresaNota.municipioEmitente);
        nota.AppendFormat("<UF>{0}</UF>", empresaNota.ufEmitente);
        nota.AppendFormat("<CEP>{0}</CEP>", empresaNota.cepEmitente);
        nota.Append("<cPais>1058</cPais>");
        nota.Append("<xPais>BRASIL</xPais>");
        nota.AppendFormat("<fone>{0}</fone>", empresaNota.foneEmitente);
        nota.Append("</enderEmit>");
        nota.AppendFormat("<IE>{0}</IE>", empresaNota.ieEmitente);
        if (empresaNota.simples) nota.Append("<CRT>1</CRT>");
        else nota.Append("<CRT>3</CRT>");
        nota.Append("</emit>");

        #endregion

        #region Destinatario

        nota.Append("<dest>");

        if (cliente.clienteCPFCNPJ.Length == 11)
        {
            nota.AppendFormat("<CPF_dest>{0}</CPF_dest>", cliente.clienteCPFCNPJ);
            if (tpAmb == "2")
            {
                nota.Append("<xNome_dest>NF-E EMITIDA EM AMBIENTE DE HOMOLOGACAO - SEM VALOR FISCAL</xNome_dest>");
            }
            else
            {
                nota.AppendFormat("<xNome_dest>{0}</xNome_dest>", cliente.clienteNome.Trim());
            }
        }
        else
        {
            nota.AppendFormat("<CNPJ_dest>{0}</CNPJ_dest>", cliente.clienteCPFCNPJ.Trim());
            if (tpAmb == "2")
            {
                nota.Append("<xNome_dest>NF-E EMITIDA EM AMBIENTE DE HOMOLOGACAO - SEM VALOR FISCAL</xNome_dest>");
            }
            else
            {
                nota.AppendFormat("<xNome_dest>{0}</xNome_dest>", cliente.clienteNomeDaEmpresa.Trim());
            }
        }


        string foneNfe = "";
        if (!string.IsNullOrEmpty(cliente.clienteFoneComercial)) foneNfe = cliente.clienteFoneComercial;
        if (!string.IsNullOrEmpty(cliente.clienteFoneResidencial)) foneNfe = cliente.clienteFoneResidencial;
        if (!string.IsNullOrEmpty(cliente.clienteFoneCelular)) foneNfe = cliente.clienteFoneCelular;


        nota.Append("<enderDest>");
        nota.AppendFormat("<xLgr_dest>{0}</xLgr_dest>", pedido.endRua.Trim().Length < 5 ? "Rua " + pedido.endRua.Trim() : pedido.endRua.Trim().Trim().Length > 42 ? rnFuncoes.removeCaracteresEspeciaisDeixaEspaco(rnFuncoes.limpaStringDeixaEspaco(pedido.endRua)).ToUpper().Replace("-", " ").Trim().Trim().Substring(0, 41) : rnFuncoes.removeCaracteresEspeciaisDeixaEspaco(rnFuncoes.limpaStringDeixaEspaco(pedido.endRua)).ToUpper().Replace("-", " ").Trim().Trim());
        nota.AppendFormat("<nro_dest>{0}</nro_dest>", pedido.endNumero.Trim());
        if (!string.IsNullOrEmpty(pedido.endComplemento)) nota.AppendFormat("<xCpl_dest>{0}</xCpl_dest>", rnFuncoes.removeCaracteresEspeciaisDeixaEspaco(rnFuncoes.limpaStringDeixaEspaco(pedido.endComplemento ?? "")).ToUpper().Replace("-", " ").Trim().Trim());
        nota.AppendFormat("<xBairro_dest>{0}</xBairro_dest>", pedido.endBairro.Trim());
        nota.AppendFormat("<cMun_dest>{0}</cMun_dest>", codigoIbge.Trim());
        nota.AppendFormat("<xMun_dest>{0}</xMun_dest>", pedido.endCidade.Trim());
        nota.AppendFormat("<UF_dest>{0}</UF_dest>", pedido.endEstado.Trim());
        nota.AppendFormat("<CEP_dest>{0}</CEP_dest>", pedido.endCep.Replace("-", "").Replace(" ", "").Replace(".", "").Trim());
        nota.Append("<cPais_dest>1058</cPais_dest>");
        nota.Append("<xPais_dest>BRASIL</xPais_dest>");
        if (!string.IsNullOrEmpty(cliente.clienteFoneResidencial)) nota.AppendFormat("<fone_dest>{0}</fone_dest>", foneNfe.Replace("(", "").Replace(")", "").Replace(" ", "").Replace("-", "").Trim());
        nota.Append("</enderDest>");

        //{
        //    nota.Append("<indIEDest>9</indIEDest>");
        //}
        //else if (cliente.clienteCPFCNPJ.Length == 11)
        //{
        //    nota.Append("<indIEDest>2</indIEDest>");
        //}
        // Em conversa entre Adilson e Fabiana da Contabilidade Sposito em 23/01/2017. Fabiana informou que o cód. 9 ( não contribuinte ) 
        // pode ser aplicado para todos os clientes PF indepente da UF do cliente 
        // if (cliente.clienteCPFCNPJ.Length == 11)
        if (ufValidacao.Contains(pedido.endEstado.ToUpper()) && cliente.clienteCPFCNPJ.Length == 11)
        {
            nota.Append("<indIEDest>9</indIEDest>");
        }
        else
        {
            if (!string.IsNullOrEmpty(cliente.clienteRGIE))
            {
                nota.Append("<indIEDest>1</indIEDest>");
                nota.AppendFormat("<IE_dest>{0}</IE_dest>", cliente.clienteRGIE.Trim());
            }
            else
            {
                nota.Append("<indIEDest>2</indIEDest>");
            }
        }
        nota.AppendFormat("<Email_dest>{0}</Email_dest>", cliente.clienteEmail.Trim());
        nota.Append("</dest>");
        #endregion

        #region Autorizacao Emissao
        nota.Append("<autXML>");
        nota.Append("<autXMLItem>");
        nota.AppendFormat("<CNPJ_aut>{0}</CNPJ_aut>", empresaNota.cnpj);
        nota.Append("</autXMLItem>");
        nota.Append("</autXML>");
        #endregion

        #region Detalhamento dos Itens

        nota.Append("<det>");
        List<tbNotaFiscalItem> notaFiscalItens = new List<tbNotaFiscalItem>();
        int nItem = 1;
        var itensDc = new dbCommerceDataContext();
        decimal valorDoDescontoDoCupom = pedido.valorDoDescontoDoCupom > 0 ? (decimal)pedido.valorDoDescontoDoCupom : 0;
        decimal valorDoDescontoDoPagamento = pedido.valorDoDescontoDoPagamento > 0 ? (decimal)pedido.valorDoDescontoDoPagamento : 0;
        decimal totalDesconto = valorDoDescontoDoCupom + valorDoDescontoDoPagamento;
        decimal valorDoDesconto = 0;
        decimal totalDoPedidoUnitario = 0;
        decimal totalDoPedido = 0;
        decimal totalDescontoCalculado = 0;
        decimal totalIcmsOrigem = 0;
        decimal totalIcmsDestino = 0;
        decimal totalIcms = 0;
        decimal totalPis = 0;
        decimal totalCofins = 0;

        //var produtosCombo = (from c in pedidoDc.tbItensPedidoCombos
        //                     join d in pedidoDc.tbItensPedidos on c.idItemPedido equals d.itemPedidoId
        //                     where d.pedidoId == pedidoId && c.produtoId != 28735 && itensMarcados.Contains(c.produtoId)
        //                     select new { c.idPedidoEnvio, itemPedidoId = d.itemPedidoId, c.produtoId }).ToList();
        //var listaIdsCombo = produtosCombo.Select(x => x.itemPedidoId).ToList();
        //var produtosNaoCombo = (from c in itensDc.tbItensPedidos where c.pedidoId == pedidoId && c.produtoId != 28735 && !listaIdsCombo.Contains(c.itemPedidoId) && itensMarcados.Contains(c.produtoId) select new { c.idPedidoEnvio, c.itemPedidoId, c.produtoId }).ToList();

        var itensPedidoGeral = (from c in itensDc.tbItensPedidos where c.pedidoId == pedidoId && c.produtoId != 28735 select c).ToList();

        //var produtos = produtosCombo.Union(produtosNaoCombo).ToList();
        var produtos = (from c in pedidoDc.tbItensPedidos
                        join p in pedidoDc.tbItemPedidoEstoques on c.itemPedidoId equals p.itemPedidoId
                        where c.pedidoId == pedidoId
                        select new { c.idPedidoEnvio, c.itemPedidoId, p.produtoId }).ToList();

        int qtdProdutosNoPedido = produtos.Count;

        produtos = produtos.Where(x => itensMarcados.Contains(x.produtoId)).ToList();

        if (idsItemPedidoEstoque != null && idsItemPedidoEstoque.Count > 0)
        {
            produtos = (from c in pedidoDc.tbItensPedidos
                        join p in pedidoDc.tbItemPedidoEstoques on c.itemPedidoId equals p.itemPedidoId
                        where c.pedidoId == pedidoId && idsItemPedidoEstoque.Contains(p.idItemPedidoEstoque)
                        select new { c.idPedidoEnvio, c.itemPedidoId, p.produtoId }).ToList();
        }

        var pacotes = (from c in pedidoDc.tbPedidoPacotes where c.idPedido == idPedido select c);
        decimal valorFrete = 0;

        decimal pesoTotal = 0;
        if (pacotes.Any())
        {
            pesoTotal = pacotes.Sum(x => x.peso);
        }
        else
        {
            foreach (var item in itensPedidoGeral)
            {
                var produtoDc = new dbCommerceDataContext();
                var produto = (from c in produtoDc.tbProdutos where c.produtoId == item.produtoId select c).First();
                pesoTotal += Convert.ToDecimal(produto.produtoPeso);
            }
        }

        int indiceProduto = 0;

        int qtdProdutosDevolvidos = produtos.Count;
        int qtdTotalBrindes = 0;
        string estado = pedido.endEstado.Trim().ToLower();
        foreach (var produtoEnviado in produtos)
        {
            var produtoDc = new dbCommerceDataContext();
            var produto = (from c in produtoDc.tbProdutos where c.produtoId == produtoEnviado.produtoId select c).First();
            var itemPedido = itensPedidoGeral.First(x => x.itemPedidoId == produtoEnviado.itemPedidoId);
            decimal valorDoProduto = 0;
            string cfopProduto = cfop;
            decimal freteDoProduto = 0;
            decimal valorDesconto = 0;

            if (pedido.valorDoFrete > 0 && indiceProduto == 0)
            {
                freteDoProduto = Convert.ToDecimal(pedido.valorDoFrete);
                valorFrete = freteDoProduto;
            }

            var produtosDc = new dbCommerceDataContext();
            var combo = (from c in produtos where c.itemPedidoId == itemPedido.itemPedidoId select c).Count() > 1;
            if (combo)
            {
                if (produtoEnviado.produtoId == 42820)
                {
                    string teste = "";
                }
                decimal totalCombo = 0;
                var produtosComboItem = produtos.Where(x => x.itemPedidoId == produtoEnviado.itemPedidoId).ToList();
                foreach (var produtoRelacionado in produtosComboItem)
                {
                    var produtoRelacionadoItem =
                        (from c in produtosDc.tbProdutos where c.produtoId == produtoRelacionado.produtoId select c)
                            .First();
                    var valorDoProdutoCombo = produtoRelacionadoItem.produtoPrecoPromocional > 0
                        ? produtoRelacionadoItem.produtoPrecoPromocional
                        : produtoRelacionadoItem.produtoPreco;

                    if (itemPedido.produtoId == produtoRelacionado.produtoId && valorDoProdutoCombo < itemPedido.itemValor)
                        //if (valorDoProdutoCombo < itemPedido.itemValor)
                        valorDoProdutoCombo = itemPedido.itemValor;

                    totalCombo += Convert.ToDecimal(valorDoProdutoCombo);
                }


                if ((itemPedido.itemValor * itemPedido.itemQuantidade) < totalCombo)
                {

                    var produtoFilho =
                   (from c in produtosDc.tbProdutos where c.produtoId == produtoEnviado.produtoId select c).First();
                    decimal valorAtualDoProduto = produtoFilho.produtoPrecoPromocional > 0
                         ? Convert.ToDecimal(produtoFilho.produtoPrecoPromocional)
                         : produtoFilho.produtoPreco;

                    if (itemPedido.produtoId == produtoEnviado.produtoId && valorAtualDoProduto < itemPedido.itemValor)
                        valorAtualDoProduto = itemPedido.itemValor;

                    decimal valorProporcionalDoProduto = (itemPedido.itemValor * ((valorAtualDoProduto * 100) / totalCombo)) / 100;

                    valorDoProduto = valorProporcionalDoProduto;

                    if (totalDesconto > 0)
                    {
                        valorDesconto = Convert.ToDecimal((Convert.ToDecimal((valorDoProduto * totalDesconto) / pedido.valorDosItens)).ToString("0.00"));
                        if ((valorDoDesconto + valorDesconto) > totalDesconto) valorDesconto = totalDesconto - valorDoDesconto;
                        if (valorDesconto < 0) valorDesconto = 0;
                        valorDoDesconto += valorDesconto;
                        totalDescontoCalculado += valorDesconto;
                    }
                }
                else
                {
                    if (totalCombo > (itemPedido.itemValor * itemPedido.itemQuantidade))
                    {
                        totalDesconto = totalCombo - itemPedido.itemValor;
                        totalCombo = itemPedido.itemValor;
                    }

                    var produtoFilho =
                        (from c in produtosDc.tbProdutos where c.produtoId == produtoEnviado.produtoId select c).First();
                    valorDoProduto = produtoFilho.produtoPrecoPromocional > 0
                        ? Convert.ToDecimal(produtoFilho.produtoPrecoPromocional)
                        : produtoFilho.produtoPreco;

                    if (itemPedido.produtoId == produtoEnviado.produtoId && valorDoProduto != itemPedido.itemValor)
                        valorDoProduto = itemPedido.itemValor;

                    if (totalCombo > produtoFilho.produtoPreco)
                    {
                        valorDoProduto = (valorDoProduto * (itemPedido.itemValor * itemPedido.itemQuantidade)) / totalCombo;
                    }
                    valorDoProduto = Convert.ToDecimal(valorDoProduto.ToString("0.00"));
                    if (totalDesconto > 0)
                    {
                        valorDesconto = Convert.ToDecimal((Convert.ToDecimal((valorDoProduto * totalDesconto) / pedido.valorDosItens)).ToString("0.00"));
                        if ((valorDoDesconto + valorDesconto) > totalDesconto) valorDesconto = totalDesconto - valorDoDesconto;
                        if (valorDesconto < 0) valorDesconto = 0;
                        valorDoDesconto += valorDesconto;
                        totalDescontoCalculado += valorDesconto;
                    }
                }


            }
            else
            {

                if (totalDesconto > 0)
                {
                    valorDesconto = Convert.ToDecimal((Convert.ToDecimal((itemPedido.itemValor * totalDesconto) / pedido.valorDosItens)).ToString("0.00"));
                    valorDoDesconto += valorDesconto;
                    totalDescontoCalculado += valorDesconto;
                }
                else
                {
                    totalBrinde = false;
                }
                valorDoProduto = itemPedido.itemValor == 0 ? itemPedido.tbProduto.produtoPreco : itemPedido.itemValor;
            }

            decimal valorDoProdutoComDesconto = valorDoProduto - valorDesconto;
            totalDoPedidoUnitario += Convert.ToDecimal(valorDoProduto);
            totalDoPedido += Convert.ToDecimal(valorDoProduto);

            indiceProduto++;

            var aliquota = ((produto.icms ?? 0) == 0 || estado == "sp") ? 18 : (int)produto.icms;
            var aliquotaInterestadual = CalculaDiferencialAliquota(estado, (Convert.ToDecimal(valorDoProdutoComDesconto)), DateTime.Now);
            if (estado != "sp")
            {
                aliquota = Convert.ToInt32(aliquotaInterestadual.icmsOrigem);
                totalIcmsOrigem += aliquotaInterestadual.diferencialOrigem;
                totalIcmsDestino += aliquotaInterestadual.diferencialDestino;
            }
            //var valorIcms = ((Convert.ToDecimal(valorDoProduto)) / 100) * aliquota;
            var valorIcms = Convert.ToDecimal((((Convert.ToDecimal(valorDoProdutoComDesconto)) / 100) * aliquota).ToString("0.00"));


            totalIcms += valorIcms;
            nota.AppendFormat("<detItem>", nItem);
            nota.Append("<prod>");
            nota.AppendFormat("<cProd>{0}</cProd>", produto.produtoId);
            string produtoNomeCompactado = limitaTexto(rnFuncoes.removeCaracteresEspeciaisDeixaEspaco(rnFuncoes.limpaStringDeixaEspaco(produto.produtoNome)).ToUpper().Replace("-", " ").Trim(), 119).Trim();
            nota.AppendFormat("<xProd>{0}</xProd>", produtoNomeCompactado);
            nota.AppendFormat("<NCM>{0}</NCM>", produto.ncm.Trim());
            nota.AppendFormat("<CFOP>{0}</CFOP>", cfopProduto);
            nota.AppendFormat("<uCOM>{0}</uCOM>", "UN");
            nota.AppendFormat("<qCOM>{0}</qCOM>", Convert.ToInt32(1).ToString("0.0000").Replace(",", "."));
            nota.AppendFormat("<vUnCom>{0}</vUnCom>", valorDoProduto.ToString("0.00").Replace(",", "."));
            nota.AppendFormat("<vProd>{0}</vProd>", (Decimal.Round(valorDoProduto, 2)).ToString("0.00").Replace(",", "."));
            nota.Append("<cEANTrib/>");
            nota.AppendFormat("<uTrib>{0}</uTrib>", "UN");
            nota.AppendFormat("<qTrib>{0}</qTrib>", Convert.ToInt32(1).ToString("0.00").Replace(",", "."));
            nota.AppendFormat("<vUnTrib>{0}</vUnTrib>", valorDoProduto.ToString("0.00").Replace(",", "."));
            if (freteDoProduto > 0 && indiceProduto == 1) nota.AppendFormat("<vFrete>{0}</vFrete>", freteDoProduto.ToString("0.00").Replace(",", "."));
            if (valorDesconto > 0) nota.AppendFormat("<vDesc>{0}</vDesc>", valorDesconto.ToString("0.00").Replace(",", "."));
            nota.AppendFormat("<indTot>{0}</indTot>", "1");
            nota.AppendFormat("<nTipoItem>{0}</nTipoItem>", "0");
            nota.Append("</prod>");
            //if (empresaNota.simples)
            //{
            //    nota.Append("<imposto>");
            //    nota.Append("<ICMS>");
            //    nota.Append("<orig>0</orig>");
            //    nota.Append("<CST>102</CST>");
            //    nota.Append("</ICMS>");
            //    nota.Append("<PIS>");
            //    nota.AppendFormat("<CST_pis>{0}</CST_pis>", (cfopProduto == "5910" || cfopProduto == "6910" ? "49" : "07"));
            //    nota.Append("</PIS>");
            //    nota.Append("<COFINS>");
            //    nota.AppendFormat("<CST_cofins>{0}</CST_cofins>", (cfopProduto == "5910" || cfopProduto == "6910" ? "49" : "07"));
            //    nota.Append("</COFINS>");
            //    if (estado != "sp" && gerarGnre && cliente.clienteCPFCNPJ.Length == 11)
            //    {
            //        nota.Append("<ICMSUFDest>");
            //        nota.AppendFormat("<vBCUFDest>{0}</vBCUFDest>", (Convert.ToDecimal(valorDoProduto)).ToString("0.00").Replace(",", "."));
            //        nota.Append("<pFCPUFDest>0.00</pFCPUFDest>");
            //        nota.AppendFormat("<pICMSUFDest>{0}</pICMSUFDest>", aliquotaInterestadual.icmsDestino.ToString("0.00").Replace(",", "."));
            //        nota.AppendFormat("<pICMSInter>{0}</pICMSInter>", aliquotaInterestadual.icmsOrigem.ToString("0.00").Replace(",", "."));
            //        nota.AppendFormat("<pICMSInterPart>{0}</pICMSInterPart>", aliquotaInterestadual.partilhaDestino.ToString("0.00").Replace(",", "."));
            //        nota.AppendFormat("<vFCPUFDest>{0}</vFCPUFDest>", (0).ToString("0.00").Replace(",", "."));
            //        nota.AppendFormat("<vICMSUFDest>{0}</vICMSUFDest>", (aliquotaInterestadual.diferencialDestino).ToString("0.00").Replace(",", "."));
            //        nota.AppendFormat("<vICMSUFRemet>{0}</vICMSUFRemet>", (0).ToString("0.00").Replace(",", "."));
            //        nota.Append("</ICMSUFDest>");
            //    }
            //    nota.Append("</imposto>");
            //}
            //else
            //{
            //    decimal valorTributoProduto = (valorDoProdutoComDesconto + freteDoProduto);
            //    totalPis += (Convert.ToDecimal((valorTributoProduto / 100) * (Decimal).65));
            //    totalCofins += (Convert.ToDecimal((valorTributoProduto / 100) * 3));

            //    nota.Append("<imposto>");
            //    nota.Append("<ICMS>");
            //    nota.Append("<orig>0</orig>");
            //    nota.Append("<CST>00</CST>");
            //    nota.Append("<modBC>3</modBC>");
            //    nota.AppendFormat("<vBC>{0}</vBC>", (Convert.ToDecimal(valorDoProduto)).ToString("0.00").Replace(",", "."));
            //    nota.AppendFormat("<pICMS>{0}</pICMS>", (aliquota).ToString("0.00").Replace(",", "."));
            //    nota.AppendFormat("<vICMS_icms>{0}</vICMS_icms>", (valorIcms).ToString("0.00").Replace(",", "."));
            //    nota.Append("</ICMS>");
            //    nota.Append("<PIS>");
            //    nota.AppendFormat("<CST_pis>{0}</CST_pis>", (cfopProduto == "5910" || cfopProduto == "6910" ? "49" : "01"));
            //    nota.Append("</PIS>");
            //    nota.Append("<COFINS>");
            //    nota.AppendFormat("<CST_cofins>{0}</CST_cofins>", (cfopProduto == "5910" || cfopProduto == "6910" ? "49" : "01"));
            //    nota.Append("</COFINS>");
            //    if (estado != "sp" && gerarGnre && cliente.clienteCPFCNPJ.Length == 11)
            //    {
            //        nota.Append("<ICMSUFDest>");
            //        nota.AppendFormat("<vBCUFDest>{0}</vBCUFDest>", (Convert.ToDecimal(valorDoProduto)).ToString("0.00").Replace(",", "."));
            //        nota.Append("<pFCPUFDest>0.00</pFCPUFDest>");
            //        nota.AppendFormat("<pICMSUFDest>{0}</pICMSUFDest>", aliquotaInterestadual.icmsDestino.ToString("0.00").Replace(",", "."));
            //        nota.AppendFormat("<pICMSInter>{0}</pICMSInter>", aliquotaInterestadual.icmsOrigem.ToString("0.00").Replace(",", "."));
            //        nota.AppendFormat("<pICMSInterPart>{0}</pICMSInterPart>", aliquotaInterestadual.partilhaDestino.ToString("0.00").Replace(",", "."));
            //        nota.AppendFormat("<vFCPUFDest>{0}</vFCPUFDest>", (0).ToString("0.00").Replace(",", "."));
            //        nota.AppendFormat("<vICMSUFDest>{0}</vICMSUFDest>", (aliquotaInterestadual.diferencialDestino).ToString("0.00").Replace(",", "."));
            //        nota.AppendFormat("<vICMSUFRemet>{0}</vICMSUFRemet>", (aliquotaInterestadual.diferencialOrigem).ToString("0.00").Replace(",", "."));
            //        nota.Append("</ICMSUFDest>");
            //    }
            //    nota.Append("</imposto>");
            //}

            string CST_pis = "";
            string CST_cofins = "";
            if (empresaNota.simples)
            {
                nota.Append("<imposto>");
                nota.Append("<ICMS>");
                nota.Append("<orig>0</orig>");
                nota.Append("<CST>102</CST>");
                nota.Append("</ICMS>");
                nota.Append("<PIS>");
                CST_pis = (cfopProduto == "5910" || cfopProduto == "6910" ? "49" : "07");
                nota.AppendFormat("<CST_pis>{0}</CST_pis>", CST_pis);
                nota.Append("</PIS>");
                nota.Append("<COFINS>");
                CST_cofins = (cfopProduto == "5910" || cfopProduto == "6910" ? "49" : "07");
                nota.AppendFormat("<CST_cofins>{0}</CST_cofins>", CST_cofins);
                nota.Append("</COFINS>");
                if (estado != "sp" && (gerarGnre | empresaNota.simples == false) && cliente.clienteCPFCNPJ.Length == 11)
                {
                    nota.Append("<ICMSUFDest>");
                    nota.AppendFormat("<vBCUFDest>{0}</vBCUFDest>", (Convert.ToDecimal(valorDoProdutoComDesconto)).ToString("0.00").Replace(",", "."));
                    nota.Append("<pFCPUFDest>0.00</pFCPUFDest>");
                    nota.AppendFormat("<pICMSUFDest>{0}</pICMSUFDest>", aliquotaInterestadual.icmsDestino.ToString("0.00").Replace(",", "."));
                    nota.AppendFormat("<pICMSInter>{0}</pICMSInter>", aliquotaInterestadual.icmsOrigem.ToString("0.00").Replace(",", "."));
                    nota.AppendFormat("<pICMSInterPart>{0}</pICMSInterPart>", aliquotaInterestadual.partilhaDestino.ToString("0.00").Replace(",", "."));
                    nota.AppendFormat("<vFCPUFDest>{0}</vFCPUFDest>", (0).ToString("0.00").Replace(",", "."));
                    nota.AppendFormat("<vICMSUFDest>{0}</vICMSUFDest>", (aliquotaInterestadual.diferencialDestino).ToString("0.00").Replace(",", "."));
                    nota.AppendFormat("<vICMSUFRemet>{0}</vICMSUFRemet>", (0).ToString("0.00").Replace(",", "."));
                    nota.Append("</ICMSUFDest>");
                }
                //else if (empresaNota.idEmpresa == 2 && estado != "sp" && cliente.clienteCPFCNPJ.Length == 11)
                //{
                //    nota.Append("<ICMSUFDest>");
                //    nota.AppendFormat("<vBCUFDest>{0}</vBCUFDest>", (Convert.ToDecimal(valorDoProdutoComDesconto)).ToString("0.00").Replace(",", "."));
                //    nota.Append("<pFCPUFDest>0.00</pFCPUFDest>");
                //    nota.AppendFormat("<pICMSUFDest>{0}</pICMSUFDest>", aliquotaInterestadual.icmsDestino.ToString("0.00").Replace(",", "."));
                //    nota.AppendFormat("<pICMSInter>{0}</pICMSInter>", aliquotaInterestadual.icmsOrigem.ToString("0.00").Replace(",", "."));
                //    nota.AppendFormat("<pICMSInterPart>{0}</pICMSInterPart>", aliquotaInterestadual.partilhaDestino.ToString("0.00").Replace(",", "."));
                //    nota.AppendFormat("<vFCPUFDest>{0}</vFCPUFDest>", (0).ToString("0.00").Replace(",", "."));
                //    nota.AppendFormat("<vICMSUFDest>{0}</vICMSUFDest>", (aliquotaInterestadual.diferencialDestino).ToString("0.00").Replace(",", "."));
                //    nota.AppendFormat("<vICMSUFRemet>{0}</vICMSUFRemet>", (aliquotaInterestadual.diferencialOrigem).ToString("0.00").Replace(",", "."));
                //    nota.Append("</ICMSUFDest>");
                //}
                nota.Append("</imposto>");
            }
            else
            {
                decimal valorTributoProduto = (valorDoProdutoComDesconto + freteDoProduto);
                totalPis += (Convert.ToDecimal((valorTributoProduto / 100) * (Decimal).65));
                totalCofins += (Convert.ToDecimal((valorTributoProduto / 100) * 3));

                nota.Append("<imposto>");
                nota.Append("<ICMS>");
                nota.Append("<orig>0</orig>");
                nota.Append("<CST>00</CST>");
                nota.Append("<modBC>3</modBC>");
                nota.AppendFormat("<vBC>{0}</vBC>", (Convert.ToDecimal(valorDoProdutoComDesconto)).ToString("0.00").Replace(",", "."));
                nota.AppendFormat("<pICMS>{0}</pICMS>", (aliquota).ToString("0.00").Replace(",", "."));
                nota.AppendFormat("<vICMS_icms>{0}</vICMS_icms>", (valorIcms).ToString("0.00").Replace(",", "."));
                nota.Append("</ICMS>");
                nota.Append("<PIS>");
                //nota.AppendFormat("<CST_pis>{0}</CST_pis>", (cfopProduto == "5910" || cfopProduto == "6910" ? "49" : "01"));
                CST_pis = (cfopProduto == "5910" || cfopProduto == "6910" ? "49" : "98");
                nota.AppendFormat("<CST_pis>{0}</CST_pis>", CST_pis);//alterado a pedido da contabiliade 27/12/2016
                nota.AppendFormat("<vBC_pis>{0}</vBC_pis>", (Convert.ToDecimal(valorTributoProduto)).ToString("0.00").Replace(".", "").Replace(",", "."));
                nota.Append("<pPIS>0.65</pPIS>");
                nota.AppendFormat("<vPIS>{0}</vPIS>", (Convert.ToDecimal((valorTributoProduto / 100) * (Decimal).65)).ToString("0.00").Replace(".", "").Replace(",", "."));
                nota.Append("</PIS>");
                nota.Append("<COFINS>");
                //nota.AppendFormat("<CST_cofins>{0}</CST_cofins>", (cfopProduto == "5910" || cfopProduto == "6910" ? "49" : "01"));
                CST_cofins = (cfopProduto == "5910" || cfopProduto == "6910" ? "49" : "98");
                nota.AppendFormat("<CST_cofins>{0}</CST_cofins>", CST_cofins);//alterado a pedido da contabiliade 27/12/2016
                nota.AppendFormat("<vBC_cofins>{0}</vBC_cofins>", (Convert.ToDecimal(valorTributoProduto)).ToString("0.00").Replace(".", "").Replace(",", "."));
                nota.Append("<pCOFINS>3.00</pCOFINS>");
                nota.AppendFormat("<vCOFINS>{0}</vCOFINS>", (Convert.ToDecimal((valorTributoProduto / 100) * 3)).ToString("0.00").Replace(".", "").Replace(",", "."));

                nota.Append("</COFINS>");

                nota.Append("</imposto>");
            }
            nota.Append("</detItem>");
            nItem++; tbNotaFiscalItem notaFiscalItem = new tbNotaFiscalItem();
            notaFiscalItem.cfop = cfopProduto;
            notaFiscalItem.ncm = produto.ncm.Trim();
            notaFiscalItem.produtoId = produto.produtoId;
            notaFiscalItem.produtoNome = produtoNomeCompactado;
            notaFiscalItem.qCOM = 1;
            notaFiscalItem.vUnCom = Decimal.Round(valorDoProduto, 2) > 0 ? Decimal.Round(valorDoProduto, 2) : Convert.ToDecimal(0.01);
            notaFiscalItem.vProd = Decimal.Round(valorDoProduto, 2) > 0 ? Decimal.Round(valorDoProduto, 2) : Convert.ToDecimal(0.01);
            notaFiscalItem.qTrib = 1;
            notaFiscalItem.vUnTrib = Decimal.Round(valorDoProduto, 2) > 0 ? Decimal.Round(valorDoProduto, 2) : Convert.ToDecimal(0.01);
            notaFiscalItem.vFrete = Decimal.Round(freteDoProduto, 2);
            notaFiscalItem.CST_pis = CST_pis;
            notaFiscalItem.CST_cofins = CST_cofins;
            notaFiscalItem.aliquotaIcms = aliquota;

            notaFiscalItens.Add(notaFiscalItem);
            // notaFiscalItem.idNotaFiscal = 
        }

        nota.Append("</det>");
        #endregion

        #region Totais
        nota.Append("<total>");
        nota.Append("<ICMStot>");
        //if (empresaNota.simples)
        //{
        //    nota.Append("<vBC_ttlnfe>0.00</vBC_ttlnfe>");
        //    nota.Append("<vICMS_ttlnfe>0.00</vICMS_ttlnfe>");
        //}
        //else
        //{
        //    nota.AppendFormat("<vBC_ttlnfe>{0}</vBC_ttlnfe>", Convert.ToDecimal(totalDoPedido).ToString("0.00").Replace(",", "."));//totalDescontoCalculado
        //    nota.AppendFormat("<vICMS_ttlnfe>{0}</vICMS_ttlnfe>", Convert.ToDecimal(totalIcms).ToString("0.00").Replace(",", "."));
        //}

        if (empresaNota.simples || (empresaNota.idEmpresa == 2 && estado != "sp" && cliente.clienteCPFCNPJ.Length == 11))
        {
            nota.Append("<vBC_ttlnfe>0.00</vBC_ttlnfe>");
            nota.Append("<vICMS_ttlnfe>0.00</vICMS_ttlnfe>");
        }
        else
        {
            nota.AppendFormat("<vBC_ttlnfe>{0}</vBC_ttlnfe>", Convert.ToDecimal(totalDoPedido - totalDescontoCalculado).ToString("0.00").Replace(",", "."));
            nota.AppendFormat("<vICMS_ttlnfe>{0}</vICMS_ttlnfe>", Convert.ToDecimal(totalIcms).ToString("0.00").Replace(",", "."));
        }


        nota.Append("<vBCST_ttlnfe>0.00</vBCST_ttlnfe>");
        nota.Append("<vST_ttlnfe>0.00</vST_ttlnfe>");
        nota.AppendFormat("<vProd_ttlnfe>{0}</vProd_ttlnfe>", Convert.ToDecimal(totalDoPedido).ToString("0.00").Replace(",", "."));
        if (valorFrete > 0) nota.AppendFormat("<vFrete_ttlnfe>{0}</vFrete_ttlnfe>", valorFrete.ToString("0.00").Replace(",", "."));
        else nota.AppendFormat("<vFrete_ttlnfe>{0}</vFrete_ttlnfe>", (0).ToString("0.00").Replace(",", "."));
        nota.Append("<vSeg_ttlnfe>0.00</vSeg_ttlnfe>");
        nota.AppendFormat("<vDesc_ttlnfe>{0}</vDesc_ttlnfe>", totalDescontoCalculado.ToString("0.00").Replace(",", "."));
        //nota.Append("<vII>0.00</vII>");
        nota.Append("<vIPI_ttlnfe>0.00</vIPI_ttlnfe>");

        if (empresaNota.simples && empresaNota.idEmpresa != 2)
            nota.Append("<vPIS_ttlnfe>0.00</vPIS_ttlnfe>");
        else if (empresaNota.idEmpresa == 2 && estado != "sp" && cliente.clienteCPFCNPJ.Length == 11)
            nota.AppendFormat("<vPIS_ttlnfe>{0}</vPIS_ttlnfe>", totalPis.ToString("0.00").Replace(",", "."));
        else
            nota.AppendFormat("<vPIS_ttlnfe>{0}</vPIS_ttlnfe>", totalPis.ToString("0.00").Replace(",", "."));

        if (empresaNota.simples && empresaNota.idEmpresa != 2)
            nota.Append("<vCOFINS_ttlnfe>0.00</vCOFINS_ttlnfe>");
        else if (empresaNota.idEmpresa == 2 && estado != "sp" && cliente.clienteCPFCNPJ.Length == 11)
            nota.AppendFormat("<vCOFINS_ttlnfe>{0}</vCOFINS_ttlnfe>", totalCofins.ToString("0.00").Replace(",", "."));
        else
            nota.AppendFormat("<vCOFINS_ttlnfe>{0}</vCOFINS_ttlnfe>", totalCofins.ToString("0.00").Replace(",", "."));

        nota.Append("<vOutro>0.00</vOutro>");
        nota.Append("<vICMSDeson_ttlnfe>0.00</vICMSDeson_ttlnfe>");
        nota.AppendFormat("<vNF>{0}</vNF>", Convert.ToDecimal(totalDoPedido + valorFrete - valorDoDesconto).ToString("0.00").Replace(",", "."));
        nota.Append("<vTotTrib_ttlnfe>0.00</vTotTrib_ttlnfe>");
        nota.Append("</ICMStot>");
        nota.Append("</total>");
        #endregion

        #region transportadora
        nota.Append("<transp>");
        nota.Append("<modFrete>0</modFrete>");

        var formaDeEnvio = "";
        if (!String.IsNullOrEmpty(transportadora))
            formaDeEnvio = transportadora;
        var envioPacote = pacotes.FirstOrDefault(x => x.formaDeEnvio != "" && x.formaDeEnvio != null);
        int tipoDeEntregaId = 0;
        if (envioPacote != null)
        {
            formaDeEnvio = envioPacote.formaDeEnvio;
            if (envioPacote.tbPedidoEnvio.tipoDeEntregaId != null)
            {
                tipoDeEntregaId = envioPacote.tbPedidoEnvio.tipoDeEntregaId ?? 0;
            }
        }

        var data = new dbCommerceDataContext();

        if (tipoDeEntregaId > 0)
        {
            var tipoDeEntrega = (from c in data.tbTipoDeEntregas where c.tipoDeEntregaId == tipoDeEntregaId select c).FirstOrDefault();
            if (tipoDeEntrega != null)
            {
                nota.Append("<transporta>");
                nota.AppendFormat("<CNPJ_transp>{0}</CNPJ_transp>", tipoDeEntrega.cnpj);
                nota.AppendFormat("<xNome_transp>{0}</xNome_transp>", tipoDeEntrega.razaoSocial);
                nota.AppendFormat("<IE_transp>{0}</IE_transp>", tipoDeEntrega.ie);
                nota.AppendFormat("<xEnder>{0}</xEnder>", tipoDeEntrega.endereco);
                nota.AppendFormat("<xMun_transp>{0}</xMun_transp>", tipoDeEntrega.municipio);
                nota.AppendFormat("<UF_transp>{0}</UF_transp>", tipoDeEntrega.uf);
                nota.Append("</transporta>");
            }
        }
        else
        {
            List<string> modalidadeJadlog = new List<string> { "jadlog", "jadlogexpressa", "jadlogcom", "jadlogrodo" };
            if (modalidadeJadlog.Contains(formaDeEnvio))
            {
                nota.Append("<transporta>");
                nota.Append("<CNPJ_transp>14135995000102</CNPJ_transp>");
                nota.Append("<xNome_transp>CALLILI EXPRESS LTDA ME</xNome_transp>");
                nota.Append("<IE_transp>344063890114</IE_transp>");
                nota.Append("<xEnder>RUA DANIEL DE FREITAS , 928 - CENTRO</xEnder>");
                nota.Append("<xMun_transp>Ibitinga</xMun_transp>");
                nota.Append("<UF_transp>SP</UF_transp>");
                nota.Append("</transporta>");
            }
            else if (formaDeEnvio == "plimor")
            {
                nota.Append("<transporta>");
                nota.Append("<CNPJ_transp>88085485006226</CNPJ_transp>");
                nota.Append("<xNome_transp>TRANSPORTADORA PLIMOR LTDA</xNome_transp>");
                nota.Append("<IE_transp>209364638111</IE_transp>");
                nota.Append("<xEnder>Marechal Rondon - km 334 - SN</xEnder>");
                nota.Append("<xMun_transp>Bauru</xMun_transp>");
                nota.Append("<UF_transp>SP</UF_transp>");
                nota.Append("</transporta>");
            }
            else if (formaDeEnvio == "lbr")
            {
                nota.Append("<transporta>");
                nota.Append("<CNPJ_transp>16660697000248</CNPJ_transp>");
                nota.Append("<xNome_transp>LBR EXPRESS TRANSPORTES E LOGÍSTICA LTDA</xNome_transp>");
                nota.Append("<IE_transp>0028344510058</IE_transp>");
                nota.Append("<xEnder>Rua Quintino Bocaiúva</xEnder>");
                nota.Append("<xMun_transp>Contagem</xMun_transp>");
                nota.Append("<UF_transp>MG</UF_transp>");
                nota.Append("</transporta>");
            }
            else if (formaDeEnvio == "jamef")
            {
                nota.Append("<transporta>");
                nota.Append("<CNPJ_transp>20147617002276</CNPJ_transp>");
                nota.Append("<xNome_transp>Jamef Transporte LTDA</xNome_transp>");
                nota.Append("<IE_transp>114387171114</IE_transp>");
                nota.Append("<xEnder>Rua Miguel Mentem 500</xEnder>");
                nota.Append("<xMun_transp>Vila Guilherme</xMun_transp>");
                nota.Append("<UF_transp>SP</UF_transp>");
                nota.Append("</transporta>");
            }
            else if (formaDeEnvio == "nowlog")
            {
                nota.Append("<transporta>");
                nota.Append("<CNPJ_transp>20712076000238</CNPJ_transp>");
                nota.Append("<xNome_transp>Nowlog Logistica Inteligente LTDA</xNome_transp>");
                nota.Append("<IE_transp>0026412980032</IE_transp>");
                nota.Append("<xEnder>Avenida Mestra Fininha, 1726, Letra A, Sala 1</xEnder>");
                nota.Append("<xMun_transp>Montes Claros</xMun_transp>");
                nota.Append("<UF_transp>MG</UF_transp>");
                nota.Append("</transporta>");
            }
            else if (formaDeEnvio == "dialogo")
            {
                nota.Append("<transporta>");
                nota.Append("<CNPJ_transp>21930065000297</CNPJ_transp>");
                nota.Append("<xNome_transp>Dialogo Logistica Inteligente LTDA</xNome_transp>");
                nota.Append("<IE_transp>144872110110</IE_transp>");
                nota.Append("<xEnder>Rodovia Dom Gabriel Paulino Bueno KM 71 M06 G2, Medeiros</xEnder>");
                nota.Append("<xMun_transp>Jundiai</xMun_transp>");
                nota.Append("<UF_transp>SP</UF_transp>");
                nota.Append("</transporta>");
            }
            else if (formaDeEnvio == "totalexpress")
            {
                nota.Append("<transporta>");
                nota.Append("<CNPJ_transp>73939449000193</CNPJ_transp>");
                nota.Append("<xNome_transp>TEX COURIER S.A.</xNome_transp>");
                nota.Append("<IE_transp>206214714111</IE_transp>");
                nota.Append("<xEnder>AVENIDA PIRACEMA 155 GALPAO 1</xEnder>");
                nota.Append("<xMun_transp>Barueri</xMun_transp>");
                nota.Append("<UF_transp>SP</UF_transp>");
                nota.Append("</transporta>");
            }
            else if (formaDeEnvio == "tnt")
            {
                if (envioPacote.idCentroDistribuicao == 3)
                {
                    nota.Append("<transporta>");
                    nota.Append("<CNPJ_transp>95591723003800</CNPJ_transp>");
                    nota.Append("<xNome_transp>TNT Mercurio Cargas e Encomendas Expressas SA</xNome_transp>");
                    nota.Append("<IE_transp>108254280116</IE_transp>");
                    nota.Append("<xEnder>Avenida Marginal Direita do Tiete N 2500</xEnder>");
                    nota.Append("<xMun_transp>Sao Paulo</xMun_transp>");
                    nota.Append("<UF_transp>SP</UF_transp>");
                    nota.Append("</transporta>");
                }
                else
                {
                    nota.Append("<transporta>");
                    nota.Append("<CNPJ_transp>95591723008284</CNPJ_transp>");
                    nota.Append("<xNome_transp>TNT Mercurio Cargas e Encomendas Expressas SA</xNome_transp>");
                    nota.Append("<IE_transp>209199664118</IE_transp>");
                    nota.Append("<xEnder>RUA JOAQUIM PELEGRINA LOPES,1-80 - Dist. Ind. III</xEnder>");
                    nota.Append("<xMun_transp>Bauru</xMun_transp>");
                    nota.Append("<UF_transp>SP</UF_transp>");
                    nota.Append("</transporta>");
                }
            }
        }
        int vol = 1;
        nota.Append("<vol>");
        bool nfeVolumeTotal = false;
        if (tipoDeEntregaId > 0)
        {
            var entrega = (from c in data.tbTipoDeEntregas where c.tipoDeEntregaId == tipoDeEntregaId select c).FirstOrDefault();
            if (entrega != null)
            {
                nfeVolumeTotal = (entrega.nfeVolumeTotal ?? false);
            }
        }
        if (nfeVolumeTotal)
        {
            nota.Append("<volItem>");
            nota.AppendFormat("<qVol>{0}</qVol>", pacotes.Count());
            nota.Append("<esp>VOLUMES</esp>");
            nota.AppendFormat("<nVol>{0}</nVol>", vol);
            nota.AppendFormat("<pesoB_transp>{0}</pesoB_transp>", (Convert.ToDecimal(pacotes.Sum(x => x.peso)) / 1000).ToString("0.000").Replace(",", "."));
            nota.Append("</volItem>");
            vol++;
        }
        else
        {
            foreach (var pacote in pacotes.OrderBy(x => x.numeroVolume))
            {
                nota.Append("<volItem>");
                nota.AppendFormat("<qVol>{0}</qVol>", 1);
                nota.Append("<esp>VOLUME</esp>");
                nota.AppendFormat("<nVol>{0}</nVol>", vol);
                nota.AppendFormat("<pesoB_transp>{0}</pesoB_transp>", (Convert.ToDecimal(pacote.peso) / 1000).ToString("0.000").Replace(",", "."));
                nota.Append("</volItem>");
                vol++;
            }
        }

        nota.Append("</vol>");
        nota.Append("</transp>");

        string informacoesAdicionais = "PEDIDO " + rnFuncoes.retornaIdCliente(pedido.pedidoId);
        if (!string.IsNullOrEmpty(pedido.endReferenciaParaEntrega)) informacoesAdicionais += " - Ref.: " + rnFuncoes.removeAcentos(pedido.endReferenciaParaEntrega);
        string telefone = "";
        if (!string.IsNullOrEmpty(cliente.clienteFoneResidencial)) telefone += "Res.: " + cliente.clienteFoneResidencial + " - ";
        if (!string.IsNullOrEmpty(cliente.clienteFoneComercial)) telefone += "Com.: " + cliente.clienteFoneComercial + " - ";
        if (!string.IsNullOrEmpty(cliente.clienteFoneCelular)) telefone += "Cel.: " + cliente.clienteFoneCelular;
        if (!string.IsNullOrEmpty(telefone)) informacoesAdicionais += " - " + telefone;

        if (estado != "sp" && cliente.clienteCPFCNPJ.Length == 11)
        {
            informacoesAdicionais += " - Convenio 93/15 - ";
            informacoesAdicionais += "Total do ICMS de partilha para a UF do destinatario R$" + totalIcmsDestino.ToString("0.00") + " - ";
            informacoesAdicionais += "Total do ICMS interestadual para a UF do remetente R$" + totalIcmsOrigem.ToString("0.00") + " - ";
        }

        if (empresaNota.simples) informacoesAdicionais += " - I DOCUMENTO EMITIDO POR EMPRESA ME OU EPP OPTANTE PELO SIMPLES NACIONAL II- NAO GERA CREDITO FISCAL DE IPI E ISS.";
        if (empresaNota.idEmpresa == 3 || empresaNota.idEmpresa == 1) informacoesAdicionais += " - Lei 12.741 de 12/2012 Carga Tributária 29,33% ";

        informacoesAdicionais += devolucaoParcial ? " Devolução parcial referente a NF-e " + numeroNfeReferencia + "." : " Devolução referente a NF-e " + numeroNfeReferencia + ".";

        informacoesAdicionais += informacoesAdicionaisExtra;

        informacoesAdicionais = informacoesAdicionais.Replace(Environment.NewLine, " ");
        nota.Append("<infAdic>");
        nota.AppendFormat("<infCpl>{0}</infCpl>", informacoesAdicionais);
        nota.Append("</infAdic>");
        #endregion

        pedidoDc.SubmitChanges();

        var dataAlt = new dbCommerceDataContext();
        var notaAlt = (from c in dataAlt.tbNotaFiscals where c.idNotaFiscal == gerarNota.idNotaFiscal select c).First();
        notaAlt.valor = Convert.ToDecimal(totalDoPedido + valorFrete - valorDoDesconto);
        dataAlt.SubmitChanges();
        try
        {
            System.IO.File.WriteAllText(System.Configuration.ConfigurationManager.AppSettings["caminhoFisico"] + "\\admin\\notas\\" + pedidoId + "_" + numeroNota + ".xml", nota.ToString());
        }
        catch (Exception ex)
        { }
        string notaFormatado = nota.ToString();

        var notaRetorno = new notaGerada();
        notaRetorno.xml = nota.ToString();
        notaRetorno.idNotaFiscal = gerarNota.idNotaFiscal;
        if (gerarNota.statusNfe != 2)
        {
            var notasItensAtuais = (from c in data.tbNotaFiscalItems where c.idNotaFiscal == gerarNota.idNotaFiscal select c).ToList();
            foreach (var notaItemAtual in notasItensAtuais)
            {
                data.tbNotaFiscalItems.DeleteOnSubmit(notaItemAtual);
            }
            data.SubmitChanges();
            foreach (var item in notaFiscalItens)
            {
                item.idNotaFiscal = notaRetorno.idNotaFiscal;

                data.tbNotaFiscalItems.InsertOnSubmit(item);
                try
                {
                    data.SubmitChanges();
                    //  var notaItemOrigem = (from c in data.tbNotaFiscalItems where c.idNotaFiscal == idNotaOrigem && c.idNotaFistalItemDevolucao == null && c.produtoId == item.produtoId select c).FirstOrDefault();
                    var notaItemOrigem = (from c in data.tbNotaFiscalItems where c.idNotaFiscal == idNotaOrigem && c.produtoId == item.produtoId select c).FirstOrDefault();
                    if (notaItemOrigem != null)
                    {
                        notaItemOrigem.idNotaFistalItemDevolucao = item.idNotaFiscalItem;
                    }
                    data.SubmitChanges();
                }
                catch (Exception ex)
                {
                    string erro = ex.Message;
                }

            }
        }
        return notaRetorno;
    }

    public static notaGerada gerarNotaFiscalAvulsa()
    {

        string chave = "";
        chave += "35";
        chave += DateTime.Now.Year.ToString().Substring(2, 2);
        chave += DateTime.Now.Month.ToString().PadLeft(2, '0');
        chave += "10924051000163";
        chave += "55";
        chave += "001";
        chave += "25583".PadLeft(9, '0');
        chave += "1";
        chave += 1.ToString().PadLeft(8, '0');
        string cDV = GerarDigitoVerificadorNFe(chave).ToString();
        chave += cDV;

        var nota = new StringBuilder();

        var pedidoDc = new dbCommerceDataContext();

        #region Identificadores da NF-e

        nota.Append("<ide>");
        nota.AppendFormat("<cUF>{0}</cUF>", "35");
        nota.AppendFormat("<cNF>{0}</cNF>", "05289980".PadLeft(8, '0'));
        nota.Append("<natOp>simples remessa</natOp>");
        nota.Append("<indPag>1</indPag>");
        nota.Append("<mod>55</mod>");
        nota.Append("<serie>1</serie>");
        nota.AppendFormat("<nNF>{0}</nNF>", "25583");
        nota.AppendFormat("<dhEmi>{0}T{1}</dhEmi>", DateTime.Now.ToString("yyyy-MM-dd"), DateTime.Now.ToString("HH:mm:ss"));
        nota.Append("<fusoHorario>-02:00</fusoHorario>");
        nota.Append("<tpNf>1</tpNf>");
        nota.AppendFormat("<idDest>{0}</idDest>", "1");
        nota.AppendFormat("<indFinal>1</indFinal>");
        nota.AppendFormat("<indPres>2</indPres>");
        nota.AppendFormat("<cMunFg>{0}</cMunFg>", "3552700");
        nota.Append("<tpImp>1</tpImp>");
        nota.Append("<tpEmis>1</tpEmis>");
        nota.AppendFormat("<tpAmb>{0}</tpAmb>", "1");
        nota.Append("<finNFe>1</finNFe>");
        nota.Append("</ide>");
        #endregion

        #region Emitente

        nota.Append("<emit>");
        nota.AppendFormat("<CNPJ_emit>{0}</CNPJ_emit>", "10924051000163");
        nota.AppendFormat("<xNome>{0}</xNome>", "LINDSAY FERRANDO - ME");
        nota.AppendFormat("<xFant>{0}</xFant>", "GRAO DE GENTE");
        nota.Append("<enderEmit>");
        nota.AppendFormat("<xLgr>{0}</xLgr>", "AVENIDA ANA COSTA,");
        nota.AppendFormat("<nro>{0}</nro>", "416");
        nota.AppendFormat("<xBairro>{0}</xBairro>", "GONZAGA,");
        nota.AppendFormat("<cMun>{0}</cMun>", "3548500");
        nota.AppendFormat("<xMun>{0}</xMun>", "Santos");
        nota.AppendFormat("<UF>{0}</UF>", "SP");
        nota.AppendFormat("<CEP>{0}</CEP>", "11060002");
        nota.Append("<cPais>1058</cPais>");
        nota.Append("<xPais>BRASIL</xPais>");
        nota.AppendFormat("<fone>{0}</fone>", "1135226155");
        nota.Append("</enderEmit>");
        nota.AppendFormat("<IE>{0}</IE>", "633459940118");
        nota.Append("<CRT>3</CRT>");
        nota.Append("</emit>");

        #endregion

        #region Destinatario

        nota.Append("<dest>");


        //nota.AppendFormat("<CNPJ_dest>{0}</CNPJ_dest>", "00595061000149");

        //nota.AppendFormat("<xNome_dest>{0}</xNome_dest>", "DISK MAQPECAS IMPORTACAO E EXPORTACAO LTDA - DISK MAQPECAS");

        //nota.Append("<enderDest>");
        //nota.AppendFormat("<xLgr_dest>{0}</xLgr_dest>", "RUA PEDRO ALVARES CABRAL");
        //nota.AppendFormat("<nro_dest>{0}</nro_dest>", "63");
        //nota.AppendFormat("<xBairro_dest>{0}</xBairro_dest>", "LUZ");
        //nota.AppendFormat("<cMun_dest>{0}</cMun_dest>", "3550308");
        //nota.AppendFormat("<xMun_dest>{0}</xMun_dest>", "Sao Paulo");
        //nota.AppendFormat("<UF_dest>{0}</UF_dest>", "SP");
        //nota.AppendFormat("<CEP_dest>{0}</CEP_dest>", "01105050");
        //nota.Append("<cPais_dest>1058</cPais_dest>");
        //nota.Append("<xPais_dest>BRASIL</xPais_dest>");
        //nota.Append("</enderDest>");

        //nota.Append("<indIEDest>1</indIEDest>");
        //nota.AppendFormat("<IE_dest>{0}</IE_dest>", "114349172111");

        nota.AppendFormat("<CNPJ_dest>{0}</CNPJ_dest>", "07312099000106");

        nota.AppendFormat("<xNome_dest>{0}</xNome_dest>", "DATALOGIC ADC BRASIL COMERCIO EQUIPAMENTOS ATUTOMACAO LTDA");

        nota.Append("<enderDest>");
        nota.AppendFormat("<xLgr_dest>{0}</xLgr_dest>", "RUA OLIVIO RONCOLETTA,");
        nota.AppendFormat("<nro_dest>{0}</nro_dest>", "465");
        nota.AppendFormat("<xBairro_dest>{0}</xBairro_dest>", "VILA HORTOLÂNDIA");
        nota.AppendFormat("<cMun_dest>{0}</cMun_dest>", "3525904");
        nota.AppendFormat("<xMun_dest>{0}</xMun_dest>", "JUNDIAÍ");
        nota.AppendFormat("<UF_dest>{0}</UF_dest>", "SP");
        nota.AppendFormat("<CEP_dest>{0}</CEP_dest>", "13214306");
        nota.Append("<cPais_dest>1058</cPais_dest>");
        nota.Append("<xPais_dest>BRASIL</xPais_dest>");
        nota.Append("</enderDest>");

        nota.Append("<indIEDest>1</indIEDest>");
        nota.AppendFormat("<IE_dest>{0}</IE_dest>", "407346963119");

        nota.AppendFormat("<Email_dest>{0}</Email_dest>", "");
        nota.Append("</dest>");
        #endregion

        #region Autorizacao Emissao
        nota.Append("<autXML>");
        nota.Append("<autXMLItem>");
        nota.AppendFormat("<CNPJ_aut>{0}</CNPJ_aut>", "10924051000163");
        nota.Append("</autXMLItem>");
        nota.Append("</autXML>");
        #endregion

        #region Detalhamento dos Itens

        nota.Append("<det>");

        int nItem = 1;
        decimal valorDoDescontoDoCupom = 0;
        decimal valorDoDescontoDoPagamento = 0;
        decimal totalDesconto = valorDoDescontoDoCupom + valorDoDescontoDoPagamento;
        decimal valorDoDesconto = 0;
        decimal totalDoPedidoUnitario = 0;
        decimal totalDoPedido = 0;
        decimal totalDescontoCalculado = 0;
        decimal totalIcms = 0;
        decimal valorFrete = 0;
        int indiceProduto = 0;
        decimal freteDoProduto = 0;
        decimal valorDesconto = 0;

        freteDoProduto = 0;
        valorFrete = freteDoProduto;

        if (totalDesconto > 0)
        {
            valorDesconto = 0;
            valorDoDesconto += valorDesconto;
            totalDescontoCalculado += valorDesconto;
        }

        totalDoPedidoUnitario += (decimal)766.15;
        totalDoPedido += (decimal)766.15;

        indiceProduto++;

        var aliquota = 18;

        var valorIcms = (((decimal)766.15 * 1) / 100) * aliquota;

        totalIcms += valorIcms;

        nota.AppendFormat("<detItem>", nItem);
        nota.Append("<prod>");
        nota.AppendFormat("<cProd>{0}</cProd>", "QD2430-BKK10-C591");
        nota.AppendFormat("<xProd>{0}</xProd>", "DATALOGIC LEITOR CODIGO DE BARRAS QD2430 NACIONALPRETO USB O.C.");
        nota.AppendFormat("<NCM>{0}</NCM>", "84719012");
        nota.AppendFormat("<CFOP>{0}</CFOP>", "5949");
        nota.AppendFormat("<uCOM>{0}</uCOM>", "UN");
        nota.AppendFormat("<qCOM>{0}</qCOM>", 1.ToString("0.0000").Replace(",", "."));
        nota.AppendFormat("<vUnCom>{0}</vUnCom>", 766.15.ToString("0.00").Replace(",", "."));
        nota.AppendFormat("<vProd>{0}</vProd>", 766.15.ToString("0.00").Replace(",", "."));
        nota.Append("<cEANTrib/>");
        nota.AppendFormat("<uTrib>{0}</uTrib>", "UN");
        nota.AppendFormat("<qTrib>{0}</qTrib>", 1.ToString("0.00").Replace(",", "."));
        nota.AppendFormat("<vUnTrib>{0}</vUnTrib>", 766.15.ToString("0.00").Replace(",", "."));
        nota.AppendFormat("<vFrete>{0}</vFrete>", 0.ToString("0.00").Replace(",", "."));
        nota.AppendFormat("<vDesc>{0}</vDesc>", 0.ToString("0.00").Replace(",", "."));
        nota.AppendFormat("<indTot>{0}</indTot>", "1");
        nota.AppendFormat("<nTipoItem>{0}</nTipoItem>", "0");
        nota.Append("</prod>");

        nota.Append("<imposto>");
        nota.Append("<ICMS>");
        nota.Append("<orig>0</orig>");
        nota.Append("<CST>00</CST>");
        nota.Append("<modBC>3</modBC>");
        nota.AppendFormat("<vBC>{0}</vBC>", 766.15.ToString("0.00").Replace(",", "."));
        nota.AppendFormat("<pICMS>{0}</pICMS>", (aliquota).ToString("0.00").Replace(",", "."));
        nota.AppendFormat("<vICMS_icms>{0}</vICMS_icms>", (valorIcms).ToString("0.00").Replace(",", "."));
        nota.Append("</ICMS>");
        nota.Append("<PIS>");
        nota.Append("<CST_pis>01</CST_pis>");
        nota.Append("</PIS>");
        nota.Append("<COFINS>");
        nota.Append("<CST_cofins>01</CST_cofins>");
        nota.Append("</COFINS>");
        nota.Append("</imposto>");

        nota.Append("</detItem>");
        nItem++;

        nota.Append("</det>");
        #endregion

        #region Totais
        nota.Append("<total>");
        nota.Append("<ICMStot>");

        nota.AppendFormat("<vBC_ttlnfe>{0}</vBC_ttlnfe>", 766.15.ToString("0.00").Replace(",", "."));
        nota.AppendFormat("<vICMS_ttlnfe>{0}</vICMS_ttlnfe>", Convert.ToDecimal(totalIcms).ToString("0.00").Replace(",", "."));

        nota.Append("<vBCST_ttlnfe>0.00</vBCST_ttlnfe>");
        nota.Append("<vST_ttlnfe>0.00</vST_ttlnfe>");
        nota.AppendFormat("<vProd_ttlnfe>{0}</vProd_ttlnfe>", 766.15.ToString("0.00").Replace(",", "."));
        if (valorFrete > 0) nota.AppendFormat("<vFrete_ttlnfe>{0}</vFrete_ttlnfe>", 0.ToString("0.00").Replace(",", "."));
        else nota.AppendFormat("<vFrete_ttlnfe>{0}</vFrete_ttlnfe>", (0).ToString("0.00").Replace(",", "."));
        nota.Append("<vSeg_ttlnfe>0.00</vSeg_ttlnfe>");
        nota.AppendFormat("<vDesc_ttlnfe>{0}</vDesc_ttlnfe>", totalDescontoCalculado.ToString("0.00").Replace(",", "."));
        nota.Append("<vIPI_ttlnfe>0.00</vIPI_ttlnfe>");
        nota.Append("<vPIS_ttlnfe>0.00</vPIS_ttlnfe>");
        nota.Append("<vCOFINS_ttlnfe>0.00</vCOFINS_ttlnfe>");
        nota.Append("<vOutro>0.00</vOutro>");
        nota.Append("<vICMSDeson_ttlnfe>0.00</vICMSDeson_ttlnfe>");
        nota.AppendFormat("<vNF>{0}</vNF>", Convert.ToDecimal(totalDoPedido + valorFrete - valorDoDesconto).ToString("0.00").Replace(",", "."));
        nota.Append("<vTotTrib_ttlnfe>0.00</vTotTrib_ttlnfe>");
        nota.Append("</ICMStot>");
        nota.Append("</total>");
        #endregion

        #region transportadora
        nota.Append("<transp>");
        nota.Append("<modFrete>1</modFrete>");


        //nota.Append("<transporta>");
        //nota.Append("<CNPJ_transp>14135995000102</CNPJ_transp>");
        //nota.Append("<xNome_transp>CALLILI EXPRESS LTDA ME</xNome_transp>");
        //nota.Append("<IE_transp>344063890114</IE_transp>");
        //nota.Append("<xEnder>RUA DANIEL DE FREITAS , 928 - CENTRO</xEnder>");
        //nota.Append("<xMun_transp>Ibitinga</xMun_transp>");
        //nota.Append("<UF_transp>SP</UF_transp>");
        //nota.Append("</transporta>");

        nota.Append("<vol>");

        nota.Append("<volItem>");
        nota.AppendFormat("<qVol>{0}</qVol>", "1");
        nota.Append("<esp>VOLUMES</esp>");
        nota.AppendFormat("<nVol>{0}</nVol>", "1");
        nota.AppendFormat("<pesoB_transp>{0}</pesoB_transp>", 0, 145.ToString("0.000").Replace(",", "."));
        nota.Append("</volItem>");


        nota.Append("</vol>");
        nota.Append("</transp>");
        nota.Append("<infAdic>");
        nota.Append("<infCpl>simples remessa</infCpl>");
        nota.Append("</infAdic>");
        #endregion

        pedidoDc.SubmitChanges();

        //var dataAlt = new dbCommerceDataContext();
        //var notaAlt = (from c in dataAlt.tbNotaFiscals where c.idNotaFiscal == 34164 select c).First();
        //notaAlt.valor = Convert.ToDecimal(totalDoPedido + valorFrete - valorDoDesconto);
        //dataAlt.SubmitChanges();

        string notaFormatado = nota.ToString();

        var notaRetorno = new notaGerada();
        notaRetorno.xml = nota.ToString();
        notaRetorno.idNotaFiscal = 34830;
        return notaRetorno;
    }

    public static int GerarDigitoVerificadorNFe2(string chave)
    {
        int soma = 0; // Vai guardar a Soma
        int mod = -1; // Vai guardar o Resto da divisão
        int dv = -1;  // Vai guardar o DigitoVerificador
        int pesso = 2; // vai guardar o pesso de multiplicacao

        //percorrendo cada caracter da chave da direita para esquerda para fazer os calculos com o pesso
        for (int i = chave.Length - 1; i != -1; i--)
        {
            int ch = Convert.ToInt32(chave[i].ToString());
            soma += ch * pesso;
            //sempre que for 9 voltamos o pesso a 2
            if (pesso < 9)
                pesso += 1;
            else
                pesso = 2;
        }

        //Agora que tenho a soma vamos pegar o resto da divisão por 11
        mod = soma % 11;
        //Aqui temos uma regrinha, se o resto da divisão for 0 ou 1 então o dv vai ser 0
        if (mod == 0 || mod == 1)
            dv = 0;
        else
            dv = 11 - mod;

        return dv;
    }

    public static notaGerada gerarNotaFiscalComplemento(int numeroNotaComplementar, int idCnpjNota, string codigoIbge)
    {
        bool totalBrinde = true;

        string cfop = "";
        var pedidoDc = new dbCommerceDataContext();
        var notaComplementar =
            (from c in pedidoDc.tbNotaFiscals
             where c.numeroNota == numeroNotaComplementar && c.idCNPJ == idCnpjNota
             select c).FirstOrDefault();
        if (notaComplementar != null)
        {
            var pedido = (from c in pedidoDc.tbPedidos where c.pedidoId == notaComplementar.idPedido select c).FirstOrDefault();
            if (pedido == null) return new notaGerada();
            int pedidoId = pedido.pedidoId;

            var clienteDc = new dbCommerceDataContext();
            var cliente = (from c in clienteDc.tbClientes where c.clienteId == pedido.clienteId select c).FirstOrDefault();

            var gerarNota = gerarNumeroNotaComplementar(numeroNotaComplementar, idCnpjNota);
            var empresaNota = (from c in pedidoDc.tbEmpresas where c.idEmpresa == gerarNota.idCNPJ select c).First();

            string numeroNota = gerarNota.numeroNota.ToString();
            string chave = gerarNota.nfeKey;
            string cDV = GerarDigitoVerificadorNFe(chave).ToString();
            chave += cDV;

            string idDest = "";
            if (cliente.clienteCPFCNPJ.Length == 11 && pedido.endEstado.ToLower() == "sp")
            {
                cfop = "5403";
                idDest = "1";
            }
            else if (cliente.clienteCPFCNPJ.Length > 11 && pedido.endEstado.ToLower() == "sp")
            {
                cfop = "5102";
                idDest = "1";
            }
            else if (cliente.clienteCPFCNPJ.Length == 11 && pedido.endEstado.ToLower() != "sp")
            {
                cfop = "6108";
                idDest = "2";
            }
            else
            {
                cfop = "6102";
                idDest = "2";
            }



            var nota = new StringBuilder();

            #region Identificadores da NF-e
            //nota.Append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
            //nota.Append("<NFe xmlns=\"http://www.portalfiscal.inf.br/nfe\">");
            //nota.AppendFormat("<infNFe versao=\"2.00\" Id=\"NFe{0}\">", chave);
            nota.Append("<ide>");
            nota.AppendFormat("<cUF>{0}</cUF>", empresaNota.codigoUfEmitente);
            nota.AppendFormat("<cNF>{0}</cNF>", pedidoId.ToString().PadLeft(8, '0'));
            nota.Append("<natOp>COMPLEMENTO DE ICMS</natOp>");
            nota.Append("<indPag>0</indPag>");
            nota.Append("<mod>55</mod>");
            nota.Append("<serie>1</serie>");
            nota.AppendFormat("<nNF>{0}</nNF>", numeroNota);
            nota.AppendFormat("<dhEmi>{0}T{1}</dhEmi>", DateTime.Now.ToString("yyyy-MM-dd"), DateTime.Now.ToString("HH:mm:ss"));
            nota.Append("<fusoHorario>-02:00</fusoHorario>");
            nota.Append("<tpNf>1</tpNf>");
            nota.AppendFormat("<idDest>{0}</idDest>", idDest);
            nota.AppendFormat("<indFinal>1</indFinal>");
            nota.AppendFormat("<indPres>2</indPres>");
            nota.AppendFormat("<cMunFg>{0}</cMunFg>", empresaNota.codigoMunicipioEmitente);

            nota.Append("<NFRef>");
            nota.Append("<NFRefItem>");
            nota.AppendFormat("<refNFe>{0}</refNFe>", notaComplementar.nfeKey + GerarDigitoVerificadorNFe(notaComplementar.nfeKey).ToString());
            nota.Append("</NFRefItem>");
            nota.Append("</NFRef>");

            nota.Append("<tpImp>1</tpImp>");
            nota.Append("<tpEmis>1</tpEmis>");
            nota.AppendFormat("<tpAmb>{0}</tpAmb>", tpAmb);
            nota.Append("<finNFe>2</finNFe>");
            nota.Append("</ide>");
            #endregion

            #region Emitente

            nota.Append("<emit>");
            nota.AppendFormat("<CNPJ_emit>{0}</CNPJ_emit>", empresaNota.cnpj);
            nota.AppendFormat("<xNome>{0}</xNome>", empresaNota.nomeEmitente);
            nota.AppendFormat("<xFant>{0}</xFant>", empresaNota.nomeFantasiaEmitente);
            nota.Append("<enderEmit>");
            nota.AppendFormat("<xLgr>{0}</xLgr>", empresaNota.logradouroEmitente);
            nota.AppendFormat("<nro>{0}</nro>", empresaNota.numeroEmitente);
            nota.AppendFormat("<xBairro>{0}</xBairro>", empresaNota.bairroEmitente);
            nota.AppendFormat("<cMun>{0}</cMun>", empresaNota.codigoMunicipioEmitente);
            nota.AppendFormat("<xMun>{0}</xMun>", empresaNota.municipioEmitente);
            nota.AppendFormat("<UF>{0}</UF>", empresaNota.ufEmitente);
            nota.AppendFormat("<CEP>{0}</CEP>", empresaNota.cepEmitente);
            nota.Append("<cPais>1058</cPais>");
            nota.Append("<xPais>BRASIL</xPais>");
            nota.AppendFormat("<fone>{0}</fone>", empresaNota.foneEmitente);
            nota.Append("</enderEmit>");
            nota.AppendFormat("<IE>{0}</IE>", empresaNota.ieEmitente);
            nota.Append("<CRT>3</CRT>");
            nota.Append("</emit>");

            #endregion

            #region Destinatario

            nota.Append("<dest>");

            if (cliente.clienteCPFCNPJ.Length == 11)
            {
                nota.AppendFormat("<CPF_dest>{0}</CPF_dest>", cliente.clienteCPFCNPJ);
                if (tpAmb == "2")
                {
                    nota.Append("<xNome_dest>NF-E EMITIDA EM AMBIENTE DE HOMOLOGACAO - SEM VALOR FISCAL</xNome_dest>");
                }
                else
                {
                    nota.AppendFormat("<xNome_dest>{0}</xNome_dest>", cliente.clienteNome.Trim());
                }
            }
            else
            {
                nota.AppendFormat("<CNPJ_dest>{0}</CNPJ_dest>", cliente.clienteCPFCNPJ.Trim());
                if (tpAmb == "2")
                {
                    nota.Append("<xNome_dest>NF-E EMITIDA EM AMBIENTE DE HOMOLOGACAO - SEM VALOR FISCAL</xNome_dest>");
                }
                else
                {
                    nota.AppendFormat("<xNome_dest>{0}</xNome_dest>", cliente.clienteNomeDaEmpresa.Trim());
                }
            }


            string foneNfe = "";
            if (!string.IsNullOrEmpty(cliente.clienteFoneComercial)) foneNfe = cliente.clienteFoneComercial;
            if (!string.IsNullOrEmpty(cliente.clienteFoneResidencial)) foneNfe = cliente.clienteFoneResidencial;
            if (!string.IsNullOrEmpty(cliente.clienteFoneCelular)) foneNfe = cliente.clienteFoneCelular;

            nota.Append("<enderDest>");
            nota.AppendFormat("<xLgr_dest>{0}</xLgr_dest>", pedido.endRua.Trim());
            nota.AppendFormat("<nro_dest>{0}</nro_dest>", pedido.endNumero.Trim());
            if (!string.IsNullOrEmpty(pedido.endComplemento)) nota.AppendFormat("<xCpl_dest>{0}</xCpl_dest>", pedido.endComplemento.Trim());
            nota.AppendFormat("<xBairro_dest>{0}</xBairro_dest>", pedido.endBairro.Trim());
            nota.AppendFormat("<cMun_dest>{0}</cMun_dest>", codigoIbge.Trim());
            nota.AppendFormat("<xMun_dest>{0}</xMun_dest>", pedido.endCidade.Trim());
            nota.AppendFormat("<UF_dest>{0}</UF_dest>", pedido.endEstado.Trim());
            nota.AppendFormat("<CEP_dest>{0}</CEP_dest>", pedido.endCep.Replace("-", "").Trim());
            nota.Append("<cPais_dest>1058</cPais_dest>");
            nota.Append("<xPais_dest>BRASIL</xPais_dest>");
            if (!string.IsNullOrEmpty(foneNfe)) nota.AppendFormat("<fone_dest>{0}</fone_dest>", foneNfe.Replace("(", "").Replace(")", "").Replace(" ", "").Replace("-", "").Trim());
            nota.Append("</enderDest>");
            if (cliente.clienteCPFCNPJ.Length == 11)
            {
                //nota.Append("<IE_dest>ISENTO</IE_dest>");
                nota.Append("<indIEDest>2</indIEDest>");
            }
            else
            {
                if (!string.IsNullOrEmpty(cliente.clienteRGIE))
                {
                    nota.Append("<indIEDest>1</indIEDest>");
                    nota.AppendFormat("<IE_dest>{0}</IE_dest>", cliente.clienteRGIE.Trim());
                }
                else
                {
                    nota.Append("<indIEDest>2</indIEDest>");
                }
            }
            nota.AppendFormat("<Email_dest>{0}</Email_dest>", cliente.clienteEmail.Trim());
            nota.Append("</dest>");
            #endregion

            #region Autorizacao Emissao
            nota.Append("<autXML>");
            nota.Append("<autXMLItem>");
            nota.AppendFormat("<CNPJ_aut>{0}</CNPJ_aut>", empresaNota.cnpj);
            nota.Append("</autXMLItem>");
            nota.Append("</autXML>");
            #endregion

            #region Detalhamento dos Itens

            nota.Append("<det>");

            int nItem = 1;
            var itensDc = new dbCommerceDataContext();
            decimal valorDoDescontoDoCupom = pedido.valorDoDescontoDoCupom > 0 ? (decimal)pedido.valorDoDescontoDoCupom : 0;
            decimal valorDoDescontoDoPagamento = pedido.valorDoDescontoDoPagamento > 0 ? (decimal)pedido.valorDoDescontoDoPagamento : 0;
            decimal totalDesconto = valorDoDescontoDoCupom + valorDoDescontoDoPagamento;
            decimal valorDoDesconto = 0;
            decimal totalDoPedidoUnitario = 0;
            decimal totalDoPedido = 0;
            decimal totalDescontoCalculado = 0;
            decimal totalIcms = 0;

            var produtosCombo = (from c in pedidoDc.tbItensPedidoCombos
                                 join d in pedidoDc.tbItensPedidos on c.idItemPedido equals d.itemPedidoId
                                 where d.pedidoId == pedidoId && c.produtoId != 28735
                                 select new { c.idPedidoEnvio, itemPedidoId = (int)d.itemPedidoId, c.produtoId }).ToList();
            var listaIdsCombo = produtosCombo.Select(x => x.itemPedidoId).ToList();
            var produtosNaoCombo = (from c in itensDc.tbItensPedidos where c.pedidoId == pedidoId && c.produtoId != 28735 && !listaIdsCombo.Contains(c.itemPedidoId) select new { c.idPedidoEnvio, c.itemPedidoId, c.produtoId }).ToList();

            var itensPedidoGeral = (from c in itensDc.tbItensPedidos where c.pedidoId == pedidoId && c.produtoId != 28735 select c).ToList();


            var produtos = produtosCombo.Union(produtosNaoCombo).ToList();


            int indiceProduto = 0;

            foreach (var produtoEnviado in produtos)
            {
                var produtoDc = new dbCommerceDataContext();
                var produto = (from c in produtoDc.tbProdutos where c.produtoId == produtoEnviado.produtoId select c).First();
                var itemPedido = itensPedidoGeral.First(x => x.itemPedidoId == produtoEnviado.itemPedidoId);
                bool cancelado = itemPedido.cancelado == null ? false : (bool)itemPedido.cancelado;
                bool brinde = itemPedido.brinde == null ? false : (bool)itemPedido.brinde;
                decimal valorDoProduto = 0;
                string cfopProduto = cfop;
                decimal freteDoProduto = 0;
                decimal valorDesconto = 0;
                if (brinde)
                {
                    if (cliente.clienteEstado.ToLower() == "sp")
                    {
                        cfopProduto = "5910";
                    }
                    else
                    {
                        cfopProduto = "6910";
                    }
                }
                else
                {
                    totalBrinde = false;
                }

                if (!cancelado)
                {
                    var produtosDc = new dbCommerceDataContext();
                    var combo = (from c in produtos where c.itemPedidoId == itemPedido.itemPedidoId select c).Count() > 1;
                    if (combo)
                    {
                        decimal totalCombo = 0;
                        var produtosComboItem = produtos.Where(x => x.itemPedidoId == produtoEnviado.itemPedidoId).ToList();
                        foreach (var produtoRelacionado in produtosComboItem)
                        {
                            var produtoRelacionadoItem =
                                (from c in produtosDc.tbProdutos where c.produtoId == produtoRelacionado.produtoId select c)
                                    .First();
                            var valorDoProdutoCombo = produtoRelacionadoItem.produtoPrecoPromocional > 0
                                ? produtoRelacionadoItem.produtoPrecoPromocional
                                : produtoRelacionadoItem.produtoPreco;
                            totalCombo += Convert.ToDecimal(valorDoProdutoCombo);
                        }

                        var produtoFilho =
                            (from c in produtosDc.tbProdutos where c.produtoId == produtoEnviado.produtoId select c).First();
                        valorDoProduto = produtoFilho.produtoPrecoPromocional > 0
                            ? Convert.ToDecimal(produtoFilho.produtoPrecoPromocional)
                            : produtoFilho.produtoPreco;

                        if (totalDesconto > 0)
                        {
                            valorDesconto = (Convert.ToDecimal((valorDoProduto * totalDesconto) / pedido.valorDosItens));
                            if ((valorDoDesconto + valorDesconto) > totalDesconto) valorDesconto = totalDesconto - valorDoDesconto;
                            valorDoDesconto += valorDesconto;
                            totalDescontoCalculado += valorDesconto;
                        }


                        if (totalCombo > produtoFilho.produtoPreco)
                        {
                            valorDoProduto = (valorDoProduto * itemPedido.itemValor) / totalCombo;
                        }
                    }
                    else
                    {
                        if (totalDesconto > 0)
                        {
                            valorDesconto = (Convert.ToDecimal((itemPedido.itemValor * totalDesconto) / pedido.valorDosItens));
                            valorDoDesconto += valorDesconto;
                            totalDescontoCalculado += valorDesconto;
                        }
                        else
                        {
                            totalBrinde = false;
                        }
                        valorDoProduto = itemPedido.itemValor == 0 ? itemPedido.tbProduto.produtoPreco : itemPedido.itemValor;
                    }



                    totalDoPedidoUnitario += Convert.ToDecimal(valorDoProduto);
                    totalDoPedido += Convert.ToDecimal(valorDoProduto);

                    indiceProduto++;


                    if ((itemPedido.brinde ?? false) == true)
                    {
                        if (cliente.clienteEstado.ToLower() == "sp")
                        {
                            cfopProduto = "5910";
                        }
                        else
                        {
                            cfopProduto = "6910";
                        }
                    }

                    var aliquota = (produto.icms ?? 0) == 0 ? 18 : (int)produto.icms;
                    var valorIcms = ((Convert.ToDecimal(valorDoProduto)) / 100) *
                                    aliquota;

                    totalIcms += valorIcms;

                    //nota.AppendFormat("<det nItem=\"{0}\">", nItem);
                    nota.AppendFormat("<detItem>", nItem);
                    nota.Append("<prod>");
                    nota.AppendFormat("<cProd>{0}</cProd>", produto.produtoId);
                    //nota.Append("<cEAN/>");
                    nota.AppendFormat("<xProd>{0}</xProd>", limitaTexto(rnFuncoes.removeCaracteresEspeciaisDeixaEspaco(rnFuncoes.limpaStringDeixaEspaco(produto.produtoNome)).ToUpper().Replace("-", " ").Trim(), 119).Trim());
                    nota.AppendFormat("<NCM>{0}</NCM>", produto.ncm.Trim());
                    nota.AppendFormat("<CFOP>{0}</CFOP>", cfopProduto);
                    nota.AppendFormat("<uCOM>{0}</uCOM>", "UN");
                    nota.AppendFormat("<qCOM>{0}</qCOM>", Convert.ToInt32(0).ToString("0.0000").Replace(",", "."));
                    nota.AppendFormat("<vUnCom>{0}</vUnCom>", (0).ToString("0.00").Replace(",", "."));
                    nota.AppendFormat("<vProd>{0}</vProd>", (0).ToString("0.00").Replace(",", "."));
                    nota.Append("<cEANTrib/>");
                    nota.AppendFormat("<uTrib>{0}</uTrib>", "UN");
                    nota.AppendFormat("<qTrib>{0}</qTrib>", Convert.ToInt32(0).ToString("0.00").Replace(",", "."));
                    nota.AppendFormat("<vUnTrib>{0}</vUnTrib>", (0).ToString("0.00").Replace(",", "."));
                    //if (freteDoProduto > 0 && indiceProduto == 1) nota.AppendFormat("<vFrete>{0}</vFrete>", freteDoProduto.ToString("0.00").Replace(",", "."));
                    //if (freteDoProduto > 0 && indiceProduto == 1) nota.AppendFormat("<vFrete>{0}</vFrete>", freteDoProduto.ToString("0.00").Replace(",", "."));
                    // if (valorDesconto > 0) nota.AppendFormat("<vDesc>{0}</vDesc>", valorDesconto.ToString("0.00").Replace(",", "."));
                    nota.AppendFormat("<indTot>{0}</indTot>", "1");
                    nota.AppendFormat("<nTipoItem>{0}</nTipoItem>", "0");
                    nota.Append("</prod>");
                    nota.Append("<imposto>");
                    nota.Append("<ICMS>");
                    //nota.Append("<ICMSSN102>");
                    nota.Append("<orig>0</orig>");
                    nota.Append("<CST>00</CST>");
                    nota.Append("<modBC>3</modBC>");
                    nota.AppendFormat("<vBC>{0}</vBC>", (Convert.ToDecimal(valorDoProduto)).ToString("0.00").Replace(",", "."));
                    nota.AppendFormat("<pICMS>{0}</pICMS>", (aliquota).ToString("0.00").Replace(",", "."));
                    nota.AppendFormat("<vICMS_icms>{0}</vICMS_icms>", (valorIcms).ToString("0.00").Replace(",", "."));

                    //nota.Append("<vBC>0.00</vBC>");
                    //nota.Append("<CSOSN>102</CSOSN>");
                    //nota.Append("</ICMSSN102>");
                    nota.Append("</ICMS>");
                    /*nota.Append("<IPI>");
                    nota.Append("<cEnq>999</cEnq>");
                    nota.Append("<IPINT>");
                    nota.Append("<CST>53</CST>");
                    nota.Append("</IPINT>");
                    nota.Append("</IPI>");*/
                    nota.Append("<PIS>");
                    nota.Append("<CST_pis>01</CST_pis>");
                    nota.Append("</PIS>");
                    nota.Append("<COFINS>");
                    nota.Append("<CST_cofins>01</CST_cofins>");
                    nota.Append("</COFINS>");
                    nota.Append("</imposto>");
                    nota.Append("</detItem>");
                    nItem++;
                    //}
                }
            }

            nota.Append("</det>");
            #endregion

            #region Totais

            nota.Append("<total>");
            nota.Append("<ICMStot>");
            nota.AppendFormat("<vBC_ttlnfe>{0}</vBC_ttlnfe>", Convert.ToDecimal(totalDoPedido).ToString("0.00").Replace(",", "."));
            nota.AppendFormat("<vICMS_ttlnfe>{0}</vICMS_ttlnfe>", Convert.ToDecimal(totalIcms).ToString("0.00").Replace(",", "."));
            nota.Append("<vBCST_ttlnfe>0.00</vBCST_ttlnfe>");
            nota.Append("<vST_ttlnfe>0.00</vST_ttlnfe>");
            nota.AppendFormat("<vProd_ttlnfe>{0}</vProd_ttlnfe>", Convert.ToDecimal(0).ToString("0.00").Replace(",", "."));
            //if (valorFrete > 0) nota.AppendFormat("<vFrete_ttlnfe>{0}</vFrete_ttlnfe>", valorFrete.ToString("0.00").Replace(",", "."));
            nota.AppendFormat("<vFrete_ttlnfe>{0}</vFrete_ttlnfe>", (0).ToString("0.00").Replace(",", "."));
            nota.Append("<vSeg_ttlnfe>0.00</vSeg_ttlnfe>");
            nota.AppendFormat("<vDesc_ttlnfe>{0}</vDesc_ttlnfe>", 0.ToString("0.00").Replace(",", "."));
            //nota.Append("<vII>0.00</vII>");
            nota.Append("<vIPI_ttlnfe>0.00</vIPI_ttlnfe>");
            nota.Append("<vPIS_ttlnfe>0.00</vPIS_ttlnfe>");
            nota.Append("<vCOFINS_ttlnfe>0.00</vCOFINS_ttlnfe>");
            nota.Append("<vOutro>0.00</vOutro>");
            nota.Append("<vICMSDeson_ttlnfe>0.00</vICMSDeson_ttlnfe>");
            nota.AppendFormat("<vNF>{0}</vNF>", Convert.ToDecimal(0).ToString("0.00").Replace(",", "."));
            nota.Append("<vTotTrib_ttlnfe>0.00</vTotTrib_ttlnfe>");
            nota.Append("</ICMStot>");
            nota.Append("</total>");
            #endregion

            #region transportadora
            nota.Append("<transp>");
            nota.Append("<modFrete>0</modFrete>");
            nota.Append("</transp>");
            nota.Append("<infAdic>");
            nota.AppendFormat("<infCpl>NF-e complementar referente a NF-e {0};</infCpl>", notaComplementar.nfeKey);
            nota.Append("</infAdic>");
            #endregion

            //nota.Append("</infNFe>");
            //nota.Append("</NFe>");


            pedidoDc.SubmitChanges();

            try
            {
                System.IO.File.WriteAllText(System.Configuration.ConfigurationManager.AppSettings["caminhoFisico"] + "\\admin\\notas\\" + pedidoId + "_" + numeroNota + ".xml", nota.ToString());
            }
            catch (Exception ex)
            { }
            string notaFormatado = nota.ToString();
            //if (totalBrinde) notaFormatado.Replace("<natOp>VENDA DE MERCADORIA</natOp>", "<natOp>REMESSA EM BONIFICAÇÃO/BRINDE</natOp>");

            var notaRetorno = new notaGerada();
            notaRetorno.xml = nota.ToString();
            notaRetorno.idNotaFiscal = gerarNota.idNotaFiscal;
            return notaRetorno;
        }
        return null;
    }

    public static notaGerada gerarNotaFiscalEmLote(int idPedido, string codigoIbge)
    {
        bool totalBrinde = true;

        string cfop = "";
        var pedidoDc = new dbCommerceDataContext();
        var pedido = (from c in pedidoDc.tbPedidos where c.pedidoId == idPedido select c).FirstOrDefault();
        //var envio = (from c in pedidoDc.tbPedidoEnvios where c.idPedidoEnvio == idPedidoEnvio select c).FirstOrDefault();

        if (pedido == null) return new notaGerada();
        int pedidoId = pedido.pedidoId;

        var clienteDc = new dbCommerceDataContext();
        var cliente = (from c in clienteDc.tbClientes where c.clienteId == pedido.clienteId select c).FirstOrDefault();

        var gerarNota = gerarNumeroNota(pedido.pedidoId, 0, false, pedido.endEstado.ToLower());
        var empresaNota = (from c in pedidoDc.tbEmpresas where c.idEmpresa == gerarNota.idCNPJ select c).First();

        string numeroNota = gerarNota.numeroNota.ToString();
        string chave = gerarNota.nfeKey;
        string cDV = GerarDigitoVerificadorNFe(chave).ToString();
        chave += cDV;

        string idDest = "";
        if (cliente.clienteCPFCNPJ.Length == 11 && pedido.endEstado.ToLower() == "sp")
        {
            cfop = "5403";
            idDest = "1";
        }
        else if (cliente.clienteCPFCNPJ.Length > 11 && pedido.endEstado.ToLower() == "sp")
        {
            cfop = "5102";
            idDest = "1";
        }
        else if (cliente.clienteCPFCNPJ.Length == 11 && pedido.endEstado.ToLower() != "sp")
        {
            cfop = "6108";
            idDest = "2";
        }
        else
        {
            cfop = "6102";
            idDest = "2";
        }



        var nota = new StringBuilder();

        #region Identificadores da NF-e
        //nota.Append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        //nota.Append("<NFe xmlns=\"http://www.portalfiscal.inf.br/nfe\">");
        //nota.AppendFormat("<infNFe versao=\"2.00\" Id=\"NFe{0}\">", chave);
        nota.Append("<ide>");
        nota.AppendFormat("<cUF>{0}</cUF>", empresaNota.codigoUfEmitente);
        nota.AppendFormat("<cNF>{0}</cNF>", pedidoId.ToString().PadLeft(8, '0'));
        nota.Append("<natOp>VENDA DE MERCADORIA</natOp>");
        nota.Append("<indPag>0</indPag>");
        nota.Append("<mod>55</mod>");
        nota.Append("<serie>1</serie>");
        nota.AppendFormat("<nNF>{0}</nNF>", numeroNota);
        nota.AppendFormat("<dhEmi>{0}T{1}</dhEmi>", DateTime.Now.ToString("yyyy-MM-dd"), DateTime.Now.ToString("HH:mm:ss"));
        nota.Append("<fusoHorario>-02:00</fusoHorario>");
        nota.Append("<tpNf>1</tpNf>");
        nota.AppendFormat("<idDest>{0}</idDest>", idDest);
        nota.AppendFormat("<indFinal>1</indFinal>");
        nota.AppendFormat("<indPres>2</indPres>");
        nota.AppendFormat("<cMunFg>{0}</cMunFg>", empresaNota.codigoMunicipioEmitente);
        nota.Append("<tpImp>1</tpImp>");
        nota.Append("<tpEmis>1</tpEmis>");
        nota.AppendFormat("<tpAmb>{0}</tpAmb>", tpAmb);
        nota.Append("<finNFe>1</finNFe>");
        nota.Append("</ide>");
        #endregion

        #region Emitente

        nota.Append("<emit>");
        nota.AppendFormat("<CNPJ_emit>{0}</CNPJ_emit>", empresaNota.cnpj);
        nota.AppendFormat("<xNome>{0}</xNome>", empresaNota.nomeEmitente);
        nota.AppendFormat("<xFant>{0}</xFant>", empresaNota.nomeFantasiaEmitente);
        nota.Append("<enderEmit>");
        nota.AppendFormat("<xLgr>{0}</xLgr>", empresaNota.logradouroEmitente);
        nota.AppendFormat("<nro>{0}</nro>", empresaNota.numeroEmitente);
        nota.AppendFormat("<xBairro>{0}</xBairro>", empresaNota.bairroEmitente);
        nota.AppendFormat("<cMun>{0}</cMun>", empresaNota.codigoMunicipioEmitente);
        nota.AppendFormat("<xMun>{0}</xMun>", empresaNota.municipioEmitente);
        nota.AppendFormat("<UF>{0}</UF>", empresaNota.ufEmitente);
        nota.AppendFormat("<CEP>{0}</CEP>", empresaNota.cepEmitente);
        nota.Append("<cPais>1058</cPais>");
        nota.Append("<xPais>BRASIL</xPais>");
        nota.AppendFormat("<fone>{0}</fone>", empresaNota.foneEmitente);
        nota.Append("</enderEmit>");
        nota.AppendFormat("<IE>{0}</IE>", empresaNota.ieEmitente);
        nota.Append("<CRT>1</CRT>");
        nota.Append("</emit>");

        #endregion

        #region Destinatario

        nota.Append("<dest>");

        if (cliente.clienteCPFCNPJ.Length == 11)
        {
            nota.AppendFormat("<CPF_dest>{0}</CPF_dest>", cliente.clienteCPFCNPJ);
            if (tpAmb == "2")
            {
                nota.Append("<xNome_dest>NF-E EMITIDA EM AMBIENTE DE HOMOLOGACAO - SEM VALOR FISCAL</xNome_dest>");
            }
            else
            {
                nota.AppendFormat("<xNome_dest>{0}</xNome_dest>", cliente.clienteNome.Trim());
            }
        }
        else
        {
            nota.AppendFormat("<CNPJ_dest>{0}</CNPJ_dest>", cliente.clienteCPFCNPJ.Trim());
            if (tpAmb == "2")
            {
                nota.Append("<xNome_dest>NF-E EMITIDA EM AMBIENTE DE HOMOLOGACAO - SEM VALOR FISCAL</xNome_dest>");
            }
            else
            {
                nota.AppendFormat("<xNome_dest>{0}</xNome_dest>", cliente.clienteNomeDaEmpresa.Trim());
            }
        }


        string foneNfe = "";
        if (!string.IsNullOrEmpty(cliente.clienteFoneComercial)) foneNfe = cliente.clienteFoneComercial;
        if (!string.IsNullOrEmpty(cliente.clienteFoneResidencial)) foneNfe = cliente.clienteFoneResidencial;
        if (!string.IsNullOrEmpty(cliente.clienteFoneCelular)) foneNfe = cliente.clienteFoneCelular;

        nota.Append("<enderDest>");
        nota.AppendFormat("<xLgr_dest>{0}</xLgr_dest>", pedido.endRua.Trim());
        nota.AppendFormat("<nro_dest>{0}</nro_dest>", pedido.endNumero.Trim());
        if (!string.IsNullOrEmpty(pedido.endComplemento)) nota.AppendFormat("<xCpl_dest>{0}</xCpl_dest>", pedido.endComplemento.Trim());
        nota.AppendFormat("<xBairro_dest>{0}</xBairro_dest>", pedido.endBairro.Trim());
        nota.AppendFormat("<cMun_dest>{0}</cMun_dest>", codigoIbge.Trim());
        nota.AppendFormat("<xMun_dest>{0}</xMun_dest>", pedido.endCidade.Trim());
        nota.AppendFormat("<UF_dest>{0}</UF_dest>", pedido.endEstado.Trim());
        nota.AppendFormat("<CEP_dest>{0}</CEP_dest>", pedido.endCep.Replace("-", "").Trim());
        nota.Append("<cPais_dest>1058</cPais_dest>");
        nota.Append("<xPais_dest>BRASIL</xPais_dest>");
        if (!string.IsNullOrEmpty(cliente.clienteFoneResidencial)) nota.AppendFormat("<fone_dest>{0}</fone_dest>", foneNfe.Replace("(", "").Replace(")", "").Replace(" ", "").Replace("-", "").Trim());
        nota.Append("</enderDest>");
        if (cliente.clienteCPFCNPJ.Length == 11)
        {
            //nota.Append("<IE_dest>ISENTO</IE_dest>");
            nota.Append("<indIEDest>2</indIEDest>");
        }
        else
        {
            if (!string.IsNullOrEmpty(cliente.clienteRGIE))
            {
                nota.Append("<indIEDest>1</indIEDest>");
                nota.AppendFormat("<IE_dest>{0}</IE_dest>", cliente.clienteRGIE.Trim());
            }
            else
            {
                nota.Append("<indIEDest>2</indIEDest>");
            }
        }
        nota.AppendFormat("<Email_dest>{0}</Email_dest>", cliente.clienteEmail.Trim());
        nota.Append("</dest>");
        #endregion

        #region Autorizacao Emissao
        nota.Append("<autXML>");
        nota.Append("<autXMLItem>");
        nota.AppendFormat("<CNPJ_aut>{0}</CNPJ_aut>", empresaNota.cnpj);
        nota.Append("</autXMLItem>");
        nota.Append("</autXML>");
        #endregion

        #region Detalhamento dos Itens

        nota.Append("<det>");

        int nItem = 1;
        var itensDc = new dbCommerceDataContext();
        decimal valorDoDescontoDoCupom = pedido.valorDoDescontoDoCupom > 0 ? (decimal)pedido.valorDoDescontoDoCupom : 0;
        decimal valorDoDescontoDoPagamento = pedido.valorDoDescontoDoPagamento > 0 ? (decimal)pedido.valorDoDescontoDoPagamento : 0;
        decimal totalDesconto = valorDoDescontoDoCupom + valorDoDescontoDoPagamento;
        decimal valorDoDesconto = 0;
        decimal totalDoPedidoUnitario = 0;
        decimal totalDoPedido = 0;
        decimal totalDescontoCalculado = 0;

        var produtos = (from c in pedidoDc.tbItensPedidos where c.pedidoId == pedidoId && c.produtoId != 28735 select new { c.idPedidoEnvio, itemPedidoId = (int)c.itemPedidoId, c.produtoId }).ToList();
        var itensPedido = (from c in itensDc.tbItensPedidos where c.pedidoId == pedidoId && c.produtoId != 28735 && produtos.Select(x => x.itemPedidoId).Contains(c.itemPedidoId) select c).ToList();
        //var itensPedido = (from c in pedidoDc.tbProdutoEstoques where c.idPedidoEnvio == idPedidoEnvio select c);
        var pacotes = (from c in pedidoDc.tbPedidoPacotes where c.idPedido == idPedido select c);
        decimal valorFrete = 0;

        decimal pesoTotal = 0;
        if (pacotes.Any())
        {
            pesoTotal = pacotes.Sum(x => x.peso);
        }
        else
        {
            foreach (var item in itensPedido)
            {
                var produtoDc = new dbCommerceDataContext();
                var produto = (from c in produtoDc.tbProdutos where c.produtoId == item.produtoId select c).First();
                pesoTotal += Convert.ToDecimal(produto.produtoPeso);
            }
        }

        int indiceProduto = 0;

        foreach (var produtoEnviado in produtos)
        {
            var produtoDc = new dbCommerceDataContext();
            var produto = (from c in produtoDc.tbProdutos where c.produtoId == produtoEnviado.produtoId select c).First();
            var itemPedido = itensPedido.First(x => x.itemPedidoId == produtoEnviado.itemPedidoId);
            bool cancelado = itemPedido.cancelado == null ? false : (bool)itemPedido.cancelado;
            bool brinde = itemPedido.brinde == null ? false : (bool)itemPedido.brinde;
            decimal valorDoProduto = 0;
            string cfopProduto = cfop;
            decimal freteDoProduto = 0;
            decimal valorDesconto = 0;

            if (pedido.valorDoFrete > 0 && indiceProduto == 0)
            {
                freteDoProduto = Convert.ToDecimal(pedido.valorDoFrete);
                valorFrete = freteDoProduto;
            }

            if (brinde)
            {
                if (cliente.clienteEstado.ToLower() == "sp")
                {
                    cfopProduto = "5910";
                }
                else
                {
                    cfopProduto = "6910";
                }
            }
            else
            {
                totalBrinde = false;
            }

            if (!cancelado)
            {
                var produtosDc = new dbCommerceDataContext();
                var produtosFilho = (from c in produtosDc.tbProdutoRelacionados where c.idProdutoPai == itemPedido.produtoId select c);
                if (produtosFilho.Any())
                {
                    //var produtoFilho = produtosFilho.First(x => x.idProdutoFilho == produto.produtoId);
                    foreach (var produtoFilho in produtosFilho)
                    {

                        decimal totalCombo = 0;
                        indiceProduto++;
                        foreach (var produtoRelacionado in produtosFilho)
                        {
                            var valorDoProdutoCombo = produtoRelacionado.tbProduto.produtoPrecoPromocional > 0
                                ? produtoRelacionado.tbProduto.produtoPrecoPromocional
                                : produtoRelacionado.tbProduto.produtoPreco;
                            totalCombo += Convert.ToDecimal(valorDoProdutoCombo);
                        }

                        valorDoProduto = produtoFilho.tbProduto.produtoPrecoPromocional > 0
                            ? Convert.ToDecimal(produtoFilho.tbProduto.produtoPrecoPromocional)
                            : produtoFilho.tbProduto.produtoPreco;

                        if (totalDesconto > 0)
                        {
                            valorDesconto = (Convert.ToDecimal((valorDoProduto * totalDesconto) / pedido.valorDosItens));
                            if ((valorDoDesconto + valorDesconto) > totalDesconto) valorDesconto = totalDesconto - valorDoDesconto;
                            valorDoDesconto += valorDesconto;
                            totalDescontoCalculado += valorDesconto;
                        }

                        if (totalCombo > produto.produtoPreco)
                        {
                            valorDoProduto = (valorDoProduto * itemPedido.itemValor) / totalCombo;
                        }

                        totalDoPedidoUnitario += Convert.ToDecimal(valorDoProduto);
                        totalDoPedido += Convert.ToDecimal(valorDoProduto);

                        if (produtoFilho.desconto > 0)
                        {
                            if (cliente.clienteEstado.ToLower() == "sp")
                            {
                                cfopProduto = "5910";
                            }
                            else
                            {
                                cfopProduto = "6910";
                            }
                        }
                        else
                        {
                            totalBrinde = false;
                        }

                        var produtoFilhoObj =
                            (from c in produtoDc.tbProdutos where c.produtoId == produtoFilho.idProdutoFilho select c)
                                .First();
                        //nota.AppendFormat("<det nItem=\"{0}\">", nItem);
                        nota.AppendFormat("<detItem>", nItem);
                        nota.Append("<prod>");
                        nota.AppendFormat("<cProd>{0}</cProd>", produto.produtoId);
                        //nota.Append("<cEAN/>");
                        nota.AppendFormat("<xProd>{0}</xProd>", limitaTexto(rnFuncoes.removeCaracteresEspeciaisDeixaEspaco(rnFuncoes.limpaStringDeixaEspaco(produtoFilhoObj.produtoNome)).ToUpper().Replace("-", " ").Trim(), 119).Trim());
                        nota.AppendFormat("<NCM>{0}</NCM>", produto.ncm.Trim());
                        nota.AppendFormat("<CFOP>{0}</CFOP>", cfopProduto);
                        nota.AppendFormat("<uCOM>{0}</uCOM>", "UN");
                        nota.AppendFormat("<qCOM>{0}</qCOM>", Convert.ToInt32(itemPedido.itemQuantidade).ToString("0.0000").Replace(",", "."));
                        nota.AppendFormat("<vUnCom>{0}</vUnCom>", valorDoProduto.ToString("0.00").Replace(",", "."));
                        nota.AppendFormat("<vProd>{0}</vProd>", (valorDoProduto * Convert.ToInt32(itemPedido.itemQuantidade)).ToString("0.00").Replace(",", "."));
                        nota.Append("<cEANTrib/>");
                        nota.AppendFormat("<uTrib>{0}</uTrib>", "UN");
                        nota.AppendFormat("<qTrib>{0}</qTrib>", Convert.ToInt32(itemPedido.itemQuantidade).ToString("0.00").Replace(",", "."));
                        nota.AppendFormat("<vUnTrib>{0}</vUnTrib>", valorDoProduto.ToString("0.00").Replace(",", "."));
                        if (freteDoProduto > 0 && indiceProduto == 1) nota.AppendFormat("<vFrete>{0}</vFrete>", freteDoProduto.ToString("0.00").Replace(",", "."));
                        if (valorDesconto > 0) nota.AppendFormat("<vDesc>{0}</vDesc>", valorDesconto.ToString("0.00").Replace(",", "."));
                        nota.AppendFormat("<indTot>{0}</indTot>", "1");
                        nota.AppendFormat("<nTipoItem>{0}</nTipoItem>", "0");
                        nota.Append("</prod>");
                        nota.Append("<imposto>");
                        nota.Append("<ICMS>");
                        //nota.Append("<ICMSSN102>");
                        nota.Append("<orig>0</orig>");
                        nota.Append("<CST>102</CST>");
                        //nota.Append("<modBC>3</modBC>");
                        //nota.Append("<vBC>0.00</vBC>");
                        //nota.Append("<vBC>0.00</vBC>");
                        //nota.Append("<CSOSN>102</CSOSN>");
                        //nota.Append("</ICMSSN102>");
                        nota.Append("</ICMS>");
                        /*nota.Append("<IPI>");
                        nota.Append("<cEnq>999</cEnq>");
                        nota.Append("<IPINT>");
                        nota.Append("<CST>53</CST>");
                        nota.Append("</IPINT>");
                        nota.Append("</IPI>");*/
                        nota.Append("<PIS>");
                        nota.Append("<CST_pis>07</CST_pis>");
                        nota.Append("</PIS>");
                        nota.Append("<COFINS>");
                        nota.Append("<CST_cofins>07</CST_cofins>");
                        nota.Append("</COFINS>");
                        nota.Append("</imposto>");
                        nota.Append("</detItem>");
                        nItem++;
                    }

                }
                else
                {
                    valorDoProduto = itemPedido.itemValor == 0 ? itemPedido.tbProduto.produtoPreco : itemPedido.itemValor;
                    if (totalDesconto > 0)
                    {
                        valorDesconto = (Convert.ToDecimal((itemPedido.itemValor * totalDesconto) / pedido.valorDosItens));
                        valorDoDesconto += valorDesconto;
                        totalDescontoCalculado += valorDesconto;
                    }
                    else
                    {
                        totalBrinde = false;
                    }

                    totalDoPedidoUnitario += Convert.ToDecimal(itemPedido.itemValor) * Convert.ToInt32(itemPedido.itemQuantidade);
                    totalDoPedido += Convert.ToDecimal(itemPedido.itemValor) * Convert.ToInt32(itemPedido.itemQuantidade);

                    indiceProduto++;


                    if ((itemPedido.brinde ?? false) == true)
                    {
                        if (cliente.clienteEstado.ToLower() == "sp")
                        {
                            cfopProduto = "5910";
                        }
                        else
                        {
                            cfopProduto = "6910";
                        }
                    }

                    //nota.AppendFormat("<det nItem=\"{0}\">", nItem);
                    nota.AppendFormat("<detItem>", nItem);
                    nota.Append("<prod>");
                    nota.AppendFormat("<cProd>{0}</cProd>", produto.produtoId);
                    //nota.Append("<cEAN/>");
                    nota.AppendFormat("<xProd>{0}</xProd>", limitaTexto(rnFuncoes.removeCaracteresEspeciaisDeixaEspaco(rnFuncoes.limpaStringDeixaEspaco(produto.produtoNome)).ToUpper().Replace("-", " ").Trim(), 119).Trim());
                    nota.AppendFormat("<NCM>{0}</NCM>", produto.ncm.Trim());
                    nota.AppendFormat("<CFOP>{0}</CFOP>", cfopProduto);
                    nota.AppendFormat("<uCOM>{0}</uCOM>", "UN");
                    nota.AppendFormat("<qCOM>{0}</qCOM>", Convert.ToInt32(1).ToString("0.0000").Replace(",", "."));
                    nota.AppendFormat("<vUnCom>{0}</vUnCom>", valorDoProduto.ToString("0.00").Replace(",", "."));
                    nota.AppendFormat("<vProd>{0}</vProd>", (valorDoProduto).ToString("0.00").Replace(",", "."));
                    nota.Append("<cEANTrib/>");
                    nota.AppendFormat("<uTrib>{0}</uTrib>", "UN");
                    nota.AppendFormat("<qTrib>{0}</qTrib>", Convert.ToInt32(1).ToString("0.00").Replace(",", "."));
                    nota.AppendFormat("<vUnTrib>{0}</vUnTrib>", valorDoProduto.ToString("0.00").Replace(",", "."));
                    if (freteDoProduto > 0 && indiceProduto == 1) nota.AppendFormat("<vFrete>{0}</vFrete>", freteDoProduto.ToString("0.00").Replace(",", "."));
                    //if (freteDoProduto > 0 && indiceProduto == 1) nota.AppendFormat("<vFrete>{0}</vFrete>", freteDoProduto.ToString("0.00").Replace(",", "."));
                    if (valorDesconto > 0) nota.AppendFormat("<vDesc>{0}</vDesc>", valorDesconto.ToString("0.00").Replace(",", "."));
                    nota.AppendFormat("<indTot>{0}</indTot>", "1");
                    nota.AppendFormat("<nTipoItem>{0}</nTipoItem>", "0");
                    nota.Append("</prod>");
                    nota.Append("<imposto>");
                    nota.Append("<ICMS>");
                    //nota.Append("<ICMSSN102>");
                    nota.Append("<orig>0</orig>");
                    nota.Append("<CST>102</CST>");
                    //nota.Append("<modBC>3</modBC>");
                    //nota.Append("<vBC>0.00</vBC>");
                    //nota.Append("<vBC>0.00</vBC>");
                    //nota.Append("<CSOSN>102</CSOSN>");
                    //nota.Append("</ICMSSN102>");
                    nota.Append("</ICMS>");
                    /*nota.Append("<IPI>");
                    nota.Append("<cEnq>999</cEnq>");
                    nota.Append("<IPINT>");
                    nota.Append("<CST>53</CST>");
                    nota.Append("</IPINT>");
                    nota.Append("</IPI>");*/
                    nota.Append("<PIS>");
                    nota.Append("<CST_pis>07</CST_pis>");
                    nota.Append("</PIS>");
                    nota.Append("<COFINS>");
                    nota.Append("<CST_cofins>07</CST_cofins>");
                    nota.Append("</COFINS>");
                    nota.Append("</imposto>");
                    nota.Append("</detItem>");
                    nItem++;
                }
            }
        }

        nota.Append("</det>");
        #endregion

        #region Totais
        nota.Append("<total>");
        nota.Append("<ICMStot>");
        nota.Append("<vBC_ttlnfe>0.00</vBC_ttlnfe>");
        nota.Append("<vICMS_ttlnfe>0.00</vICMS_ttlnfe>");
        nota.Append("<vBCST_ttlnfe>0.00</vBCST_ttlnfe>");
        nota.Append("<vST_ttlnfe>0.00</vST_ttlnfe>");
        nota.AppendFormat("<vProd_ttlnfe>{0}</vProd_ttlnfe>", Convert.ToDecimal(totalDoPedido).ToString("0.00").Replace(",", "."));
        if (valorFrete > 0) nota.AppendFormat("<vFrete_ttlnfe>{0}</vFrete_ttlnfe>", valorFrete.ToString("0.00").Replace(",", "."));
        else nota.AppendFormat("<vFrete_ttlnfe>{0}</vFrete_ttlnfe>", (0).ToString("0.00").Replace(",", "."));
        nota.Append("<vSeg_ttlnfe>0.00</vSeg_ttlnfe>");
        nota.AppendFormat("<vDesc_ttlnfe>{0}</vDesc_ttlnfe>", totalDescontoCalculado.ToString("0.00").Replace(",", "."));
        //nota.Append("<vII>0.00</vII>");
        nota.Append("<vIPI_ttlnfe>0.00</vIPI_ttlnfe>");
        nota.Append("<vPIS_ttlnfe>0.00</vPIS_ttlnfe>");
        nota.Append("<vCOFINS_ttlnfe>0.00</vCOFINS_ttlnfe>");
        nota.Append("<vOutro>0.00</vOutro>");
        nota.Append("<vICMSDeson_ttlnfe>0.00</vICMSDeson_ttlnfe>");
        nota.AppendFormat("<vNF>{0}</vNF>", Convert.ToDecimal(totalDoPedido + valorFrete - valorDoDesconto).ToString("0.00").Replace(",", "."));
        nota.Append("<vTotTrib_ttlnfe>0.00</vTotTrib_ttlnfe>");
        nota.Append("</ICMStot>");
        nota.Append("</total>");
        #endregion

        #region transportadora
        nota.Append("<transp>");
        nota.Append("<modFrete>0</modFrete>");

        var formaDeEnvio = "";
        var envioPacote = pacotes.FirstOrDefault(x => x.formaDeEnvio != "" && x.formaDeEnvio != null);
        int tipoDeEntregaId = 0;
        if (envioPacote != null)
        {
            formaDeEnvio = envioPacote.formaDeEnvio;
            if (envioPacote.tbPedidoEnvio.tipoDeEntregaId != null)
            {
                tipoDeEntregaId = envioPacote.tbPedidoEnvio.tipoDeEntregaId ?? 0;
            }
        }

        var data = new dbCommerceDataContext();

        if (tipoDeEntregaId > 0)
        {
            var tipoDeEntrega = (from c in data.tbTipoDeEntregas where c.tipoDeEntregaId == tipoDeEntregaId select c).FirstOrDefault();
            if (tipoDeEntrega != null)
            {
                nota.Append("<transporta>");
                nota.AppendFormat("<CNPJ_transp>{0}</CNPJ_transp>", tipoDeEntrega.cnpj);
                nota.AppendFormat("<xNome_transp>{0}</xNome_transp>", tipoDeEntrega.razaoSocial);
                nota.AppendFormat("<IE_transp>{0}</IE_transp>", tipoDeEntrega.ie);
                nota.AppendFormat("<xEnder>{0}</xEnder>", tipoDeEntrega.endereco);
                nota.AppendFormat("<xMun_transp>{0}</xMun_transp>", tipoDeEntrega.municipio);
                nota.AppendFormat("<UF_transp>{0}</UF_transp>", tipoDeEntrega.uf);
                nota.Append("</transporta>");
            }
        }
        else
        {
            List<string> modalidadeJadlog = new List<string> { "jadlog", "jadlogexpressa", "jadlogcom", "jadlogrodo" };
            if (modalidadeJadlog.Contains(formaDeEnvio))
            {
                nota.Append("<transporta>");
                nota.Append("<CNPJ_transp>14135995000102</CNPJ_transp>");
                nota.Append("<xNome_transp>CALLILI EXPRESS LTDA ME</xNome_transp>");
                nota.Append("<IE_transp>344063890114</IE_transp>");
                nota.Append("<xEnder>RUA DANIEL DE FREITAS , 928 - CENTRO</xEnder>");
                nota.Append("<xMun_transp>Ibitinga</xMun_transp>");
                nota.Append("<UF_transp>SP</UF_transp>");
                nota.Append("</transporta>");
            }
            else if (formaDeEnvio == "plimor")
            {
                nota.Append("<transporta>");
                nota.Append("<CNPJ_transp>88085485006226</CNPJ_transp>");
                nota.Append("<xNome_transp>TRANSPORTADORA PLIMOR LTDA</xNome_transp>");
                nota.Append("<IE_transp>209364638111</IE_transp>");
                nota.Append("<xEnder>Marechal Rondon - km 334 - SN</xEnder>");
                nota.Append("<xMun_transp>Bauru</xMun_transp>");
                nota.Append("<UF_transp>SP</UF_transp>");
                nota.Append("</transporta>");
            }
            else if (formaDeEnvio == "lbr")
            {
                nota.Append("<transporta>");
                nota.Append("<CNPJ_transp>16660697000248</CNPJ_transp>");
                nota.Append("<xNome_transp>LBR EXPRESS TRANSPORTES E LOGÍSTICA LTDA</xNome_transp>");
                nota.Append("<IE_transp>0028344510058</IE_transp>");
                nota.Append("<xEnder>Rua Quintino Bocaiúva</xEnder>");
                nota.Append("<xMun_transp>Contagem</xMun_transp>");
                nota.Append("<UF_transp>MG</UF_transp>");
                nota.Append("</transporta>");
            }
            else if (formaDeEnvio == "jamef")
            {
                nota.Append("<transporta>");
                nota.Append("<CNPJ_transp>20147617002276</CNPJ_transp>");
                nota.Append("<xNome_transp>Jamef Transporte LTDA</xNome_transp>");
                nota.Append("<IE_transp>114387171114</IE_transp>");
                nota.Append("<xEnder>Rua Miguel Mentem 500</xEnder>");
                nota.Append("<xMun_transp>Vila Guilherme</xMun_transp>");
                nota.Append("<UF_transp>SP</UF_transp>");
                nota.Append("</transporta>");
            }
            else if (formaDeEnvio == "nowlog")
            {
                nota.Append("<transporta>");
                nota.Append("<CNPJ_transp>20712076000238</CNPJ_transp>");
                nota.Append("<xNome_transp>Nowlog Logistica Inteligente LTDA</xNome_transp>");
                nota.Append("<IE_transp>0026412980032</IE_transp>");
                nota.Append("<xEnder>Avenida Mestra Fininha, 1726, Letra A, Sala 1</xEnder>");
                nota.Append("<xMun_transp>Montes Claros</xMun_transp>");
                nota.Append("<UF_transp>MG</UF_transp>");
                nota.Append("</transporta>");
            }
            else if (formaDeEnvio == "dialogo")
            {
                nota.Append("<transporta>");
                nota.Append("<CNPJ_transp>21930065000297</CNPJ_transp>");
                nota.Append("<xNome_transp>Dialogo Logistica Inteligente LTDA</xNome_transp>");
                nota.Append("<IE_transp>144872110110</IE_transp>");
                nota.Append("<xEnder>Rodovia Dom Gabriel Paulino Bueno KM 71 M06 G2, Medeiros</xEnder>");
                nota.Append("<xMun_transp>Jundiai</xMun_transp>");
                nota.Append("<UF_transp>SP</UF_transp>");
                nota.Append("</transporta>");
            }
            else if (formaDeEnvio == "totalexpress")
            {
                nota.Append("<transporta>");
                nota.Append("<CNPJ_transp>73939449000193</CNPJ_transp>");
                nota.Append("<xNome_transp>TEX COURIER S.A.</xNome_transp>");
                nota.Append("<IE_transp>206214714111</IE_transp>");
                nota.Append("<xEnder>AVENIDA PIRACEMA 155 GALPAO 1</xEnder>");
                nota.Append("<xMun_transp>Barueri</xMun_transp>");
                nota.Append("<UF_transp>SP</UF_transp>");
                nota.Append("</transporta>");
            }
            else if (formaDeEnvio == "tnt")
            {
                if (envioPacote.idCentroDistribuicao == 3)
                {
                    nota.Append("<transporta>");
                    nota.Append("<CNPJ_transp>95591723003800</CNPJ_transp>");
                    nota.Append("<xNome_transp>TNT Mercurio Cargas e Encomendas Expressas SA</xNome_transp>");
                    nota.Append("<IE_transp>108254280116</IE_transp>");
                    nota.Append("<xEnder>Avenida Marginal Direita do Tiete N 2500</xEnder>");
                    nota.Append("<xMun_transp>Sao Paulo</xMun_transp>");
                    nota.Append("<UF_transp>SP</UF_transp>");
                    nota.Append("</transporta>");
                }
                else
                {
                    nota.Append("<transporta>");
                    nota.Append("<CNPJ_transp>95591723008284</CNPJ_transp>");
                    nota.Append("<xNome_transp>TNT Mercurio Cargas e Encomendas Expressas SA</xNome_transp>");
                    nota.Append("<IE_transp>209199664118</IE_transp>");
                    nota.Append("<xEnder>RUA JOAQUIM PELEGRINA LOPES,1-80 - Dist. Ind. III</xEnder>");
                    nota.Append("<xMun_transp>Bauru</xMun_transp>");
                    nota.Append("<UF_transp>SP</UF_transp>");
                    nota.Append("</transporta>");
                }
            }
        }
        /*else
        {
            nota.Append("<transporta>");
            nota.Append("<CNPJ>14135995000102</CNPJ>");
            nota.Append("<xNome>CALLILI EXPRESS LTDA-ME</xNome>");
            nota.Append("<IE>344063890114</IE>");
            nota.Append("<xEnder>RUA DANIEL DE FREITAS , 928 - CENTRO</xEnder>");
            nota.Append("<xMun>Ibitinga</xMun>");
            nota.Append("<UF>SP</UF>");
            nota.Append("</transporta>");
        }*/
        int vol = 1;
        nota.Append("<vol>");
        bool nfeVolumeTotal = false;
        if (tipoDeEntregaId > 0)
        {
            var entrega = (from c in data.tbTipoDeEntregas where c.tipoDeEntregaId == tipoDeEntregaId select c).FirstOrDefault();
            if (entrega != null)
            {
                nfeVolumeTotal = (entrega.nfeVolumeTotal ?? false);
            }
        }
        if (nfeVolumeTotal)
        {
            nota.Append("<volItem>");
            nota.AppendFormat("<qVol>{0}</qVol>", pacotes.Count());
            nota.Append("<esp>VOLUMES</esp>");
            nota.AppendFormat("<nVol>{0}</nVol>", vol);
            nota.AppendFormat("<pesoB_transp>{0}</pesoB_transp>", (Convert.ToDecimal(pacotes.Sum(x => x.peso)) / 1000).ToString("0.000").Replace(",", "."));
            nota.Append("</volItem>");
            vol++;
        }
        else
        {
            foreach (var pacote in pacotes.OrderBy(x => x.numeroVolume))
            {
                nota.Append("<volItem>");
                nota.AppendFormat("<qVol>{0}</qVol>", 1);
                nota.Append("<esp>VOLUME</esp>");
                nota.AppendFormat("<nVol>{0}</nVol>", vol);
                nota.AppendFormat("<pesoB_transp>{0}</pesoB_transp>", (Convert.ToDecimal(pacote.peso) / 1000).ToString("0.000").Replace(",", "."));
                nota.Append("</volItem>");
                vol++;
            }
        }

        nota.Append("</vol>");
        nota.Append("</transp>");
        nota.Append("<infAdic>");
        nota.AppendFormat("<infCpl>PEDIDO {0} - I DOCUMENTO EMITIDO POR EMPRESA ME OU EPP OPTANTE PELO SIMPLES NACIONAL II- NAO GERA CREDITO FISCAL DE IPI E ISS.;</infCpl>", rnFuncoes.retornaIdCliente(pedido.pedidoId));
        nota.Append("</infAdic>");
        #endregion

        //nota.Append("</infNFe>");
        //nota.Append("</NFe>");


        pedidoDc.SubmitChanges();

        try
        {
            System.IO.File.WriteAllText(System.Configuration.ConfigurationManager.AppSettings["caminhoFisico"] + "\\admin\\notas\\" + pedidoId + "_" + numeroNota + ".xml", nota.ToString());
        }
        catch (Exception ex)
        { }
        string notaFormatado = nota.ToString();
        if (totalBrinde) notaFormatado.Replace("<natOp>VENDA DE MERCADORIA</natOp>", "<natOp>REMESSA EM BONIFICAÇÃO/BRINDE</natOp>");

        var notaRetorno = new notaGerada();
        notaRetorno.xml = nota.ToString();
        notaRetorno.idNotaFiscal = gerarNota.idNotaFiscal;
        return notaRetorno;
    }

    public static tbNotaFiscal gerarNumeroNota(int pedidoId, int idPedidoEnvio, bool gerarNovaNota, string uf)
    {
        var notaDc = new dbCommerceDataContext();
        var notas = (from c in notaDc.tbNotaFiscals where c.idPedido == pedidoId orderby (c.statusNfe ?? 0) select c).ToList();
        if (idPedidoEnvio > 0)
        {
            notas = notas.Where(x => x.idPedidoEnvio == idPedidoEnvio).ToList();
        }
        var nota = notas.FirstOrDefault();
        if (nota != null && !gerarNovaNota)
        {
            nota.ultimaChecagem = DateTime.Now.AddSeconds(-270);
            notaDc.SubmitChanges();
            return nota;
        }
        else
        {
            #region Define CNPJ de Emissão

            //var empresaSimples = (from c in notaDc.tbEmpresas
            //                      join d in notaDc.tbNotaFiscals on c.idEmpresa equals d.idCNPJ into totalNotas
            //                      where (c.simples | !c.simples) && c.ativo == true
            //                      select new
            //                      {
            //                          c.idEmpresa,
            //                          total =
            //                              (totalNotas.Any(
            //                                  x => x.dataHora.Month == DateTime.Now.Month && x.dataHora.Year == DateTime.Now.Year) == true
            //                                  ? (decimal)
            //                                      totalNotas.Where(
            //                                          x => x.dataHora.Month == DateTime.Now.Month && x.dataHora.Year == DateTime.Now.Year)
            //                                          .Sum(x => x.valor)
            //                                  : (decimal)0),
            //                          c.limite
            //                      }
            //    ).Where(x => x.total < x.limite).OrderBy(x => x.total).FirstOrDefault();
            var empresaSimples = (from c in notaDc.tbEmpresas
                                  join d in notaDc.tbNotaFiscals on c.idEmpresa equals d.idCNPJ into totalNotas
                                  where (c.simples | !c.simples) && c.ativo
                                  select new
                                  {
                                      c.idEmpresa,
                                      total = totalNotas.Where(
                                                      x => x.dataHora.Month == DateTime.Now.Month && x.dataHora.Year == DateTime.Now.Year)
                                                      .Sum(x => x.valor) ?? 0,
                                      c.limite
                                  }
                ).Where(x => x.total < x.limite).OrderBy(x => x.total);
            var empresaNaoSimples =
                (from c in notaDc.tbEmpresas where c.simples == false && c.ativo == true select c).First();
            int idCnpj = 0;

            //List<string> ufRestrita = new List<string> { "es", "df", "rs", "ce", "go" };
            List<string> ufRestrita = new List<string> { "pi", "ro", "rr", "mt", "al" };
            //if (ufRestrita.Contains(uf.ToLower()))//orientação do financeiro - 12/08/2016 //liberado para empresa mayara 28/10/2016 - a pedido do administrativo
            //{
            //    idCnpj = 2;
            //    //idCnpj = empresaSimples.idEmpresa;
            //    if (!empresaSimples.Any(x => x.idEmpresa == 2))
            //        idCnpj = 3;
            //}
            //else if (uf.ToLower() != "sp")// a pedido do financeiro notas emitidas para outros estados devem sair pelo cnpj do Luiz Roberto - 10/02/2016
            //{
            //    idCnpj = 5;
            //} comentado 16/06/2016 - rerga não é mais aplicada
            //else
            //{
            idCnpj = empresaSimples.FirstOrDefault().idEmpresa;
            //}
            //if (empresaSimples != null)
            //{
            //idCnpj = empresaSimples.idEmpresa;
            //}
            //else
            //{
            //    idCnpj = empresaNaoSimples.idEmpresa;
            //}
            if (idCnpj == 1 && uf.ToLower() != "sp")//&& ufRestrita.Contains(uf.ToLower())
            {
                idCnpj = 2;

                if (!empresaSimples.Any(x => x.idEmpresa == 2))
                    idCnpj = 3;
            }
            if (idCnpj == 1 && uf.ToLower() != "sp")//&& !ufRestrita.Contains(uf.ToLower())
            {
                idCnpj = 3;
            }

            if (idCnpj == 2 && !ufRestrita.Contains(uf.ToLower()))
            {
                idCnpj = 3;
            }

            if (idPedidoEnvio > 0)
            {
                //var itensEnvioCd1 = (from c in notaDc.tbProdutoEstoques where c.idPedidoEnvio == idPedidoEnvio
                //                     && c.idCentroDistribuicao == 1 select c).Count();
                //if(itensEnvioCd1 > 0)
                //{
                //    var totalNotasMes = (from x in notaDc.tbNotaFiscals where x.idCNPJ == 2 && x.dataHora.Month == DateTime.Now.Month && x.dataHora.Year == DateTime.Now.Year select x).Sum(x => x.valor);
                //    if(totalNotasMes < Convert.ToDecimal("640000,00"))
                //    {
                //        idCnpj = 2;
                //    } 
                //    else
                //    {
                //        idCnpj = 1;
                //    }               
                //}
                //else
                //{
                //    idCnpj = 3;
                //}
                var itensEnvioCd2 = (from c in notaDc.tbProdutoEstoques
                                     where c.idPedidoEnvio == idPedidoEnvio && c.idCentroDistribuicao > 1
                                     select c).Count();
                if (itensEnvioCd2 > 0)
                {
                    idCnpj = 3;
                }

                var envio = (from c in notaDc.tbPedidoEnvios where c.idPedidoEnvio == idPedidoEnvio select c).FirstOrDefault();
                if (envio != null)
                {
                    if (envio.idCentroDeDistribuicao > 3)
                    {
                        idCnpj = 6;
                    }
                }
            }
            var empresaSelecionada = (from c in notaDc.tbEmpresas where c.idEmpresa == idCnpj select c).First();
            #endregion

            int numero = 1;
            var numeroNota = (from c in notaDc.tbNotaFiscals where c.idCNPJ == idCnpj orderby c.numeroNota descending select c).FirstOrDefault();
            if (numeroNota != null)
            {
                numero = (numeroNota.numeroNota + 1);
            }
            var notaNova = new tbNotaFiscal
            {
                idPedido = pedidoId,
                numeroNota = numero,
                dataHora = DateTime.Now,
                idPedidoEnvio = idPedidoEnvio,
                ultimaChecagem = DateTime.Now.AddMinutes(-270),
                ultimoStatus = "Aguardando Processamento",
                idCNPJ = idCnpj
            };

            string chave = "";
            try
            {
                chave += empresaSelecionada.codigoUfEmitente;
                chave += DateTime.Now.Year.ToString().Substring(2, 2);
                chave += DateTime.Now.Month.ToString().PadLeft(2, '0');
                chave += empresaSelecionada.cnpj;
                chave += "55";
                chave += "001";
                chave += numero.ToString().PadLeft(9, '0');
                chave += "1";
                chave += pedidoId.ToString().PadLeft(8, '0');

                notaNova.nfeKey = chave;

                notaDc.tbNotaFiscals.InsertOnSubmit(notaNova);
                notaDc.SubmitChanges();
            }
            catch (Exception ioEx)
            {
                if (ioEx.Message.IndexOf("nota-duplicada") > -1)
                {
                    #region Validar Nota  

                    bool notaValida = false;
                    while (!notaValida)
                    {

                        using (var notaDcDuplicada = new dbCommerceDataContext())
                        {
                            numeroNota = (from c in notaDcDuplicada.tbNotaFiscals
                                          where c.idCNPJ == idCnpj
                                          orderby c.numeroNota descending
                                          select c).FirstOrDefault();
                            if (numeroNota != null)
                            {
                                numero = (numeroNota.numeroNota + 1);
                            }

                            var notaNovaValida = new tbNotaFiscal
                            {
                                idPedido = pedidoId,
                                numeroNota = numero,
                                dataHora = DateTime.Now,
                                idPedidoEnvio = idPedidoEnvio,
                                ultimaChecagem = DateTime.Now.AddMinutes(-270),
                                ultimoStatus = "Aguardando Processamento",
                                idCNPJ = idCnpj
                            };

                            chave = "";
                            chave += empresaSelecionada.codigoUfEmitente;
                            chave += DateTime.Now.Year.ToString().Substring(2, 2);
                            chave += DateTime.Now.Month.ToString().PadLeft(2, '0');
                            chave += empresaSelecionada.cnpj;
                            chave += "55";
                            chave += "001";
                            chave += numero.ToString().PadLeft(9, '0');
                            chave += "1";
                            chave += pedidoId.ToString().PadLeft(8, '0');

                            notaNovaValida.nfeKey = chave;

                            try
                            {
                                notaDcDuplicada.tbNotaFiscals.InsertOnSubmit(notaNovaValida);
                                notaDcDuplicada.SubmitChanges();
                                notaValida = true;
                                notaNova = notaNovaValida;
                            }
                            catch (Exception)
                            {
                                notaValida = false;
                            }
                        }
                    }

                    #endregion validar  
                }
            }
            return notaNova;
        }
    }

    public static tbNotaFiscal gerarNumeroNotaEmpresa(int pedidoId, int idPedidoEnvio, bool gerarNovaNota, int idEmpresa)
    {
        var notaDc = new dbCommerceDataContext();
        var notas = (from c in notaDc.tbNotaFiscals where c.idPedido == pedidoId && c.idPedido > 0 && c.linkDanfe == null select c);
        if (idPedidoEnvio > 0)
        {
            notas = notas.Where(x => x.idPedidoEnvio == idPedidoEnvio);
        }
        var nota = notas.FirstOrDefault();
        if (nota != null && !gerarNovaNota)
        {
            nota.ultimaChecagem = DateTime.Now.AddSeconds(-270);
            notaDc.SubmitChanges();
            return nota;
        }
        else
        {
            #region Define CNPJ de Emissão

            var empresaSelecionada = (from c in notaDc.tbEmpresas where c.idEmpresa == idEmpresa select c).First();
            #endregion

            int numero = 1;
            var numeroNota = (from c in notaDc.tbNotaFiscals where c.idCNPJ == idEmpresa orderby c.numeroNota descending select c).FirstOrDefault();
            if (numeroNota != null)
            {
                numero = (numeroNota.numeroNota + 1);
            }
            var notaNova = new tbNotaFiscal
            {
                idPedido = pedidoId,
                numeroNota = numero,
                dataHora = DateTime.Now,
                idPedidoEnvio = idPedidoEnvio,
                ultimaChecagem = DateTime.Now.AddMinutes(-270),
                ultimoStatus = "Aguardando Processamento",
                idCNPJ = idEmpresa
            };

            string chave = "";

            try
            {
                chave += empresaSelecionada.codigoUfEmitente;
                chave += DateTime.Now.Year.ToString().Substring(2, 2);
                chave += DateTime.Now.Month.ToString().PadLeft(2, '0');
                chave += empresaSelecionada.cnpj;
                chave += "55";
                chave += "001";
                chave += numero.ToString().PadLeft(9, '0');
                chave += "1";
                chave += pedidoId.ToString().PadLeft(8, '0');

                notaNova.nfeKey = chave;

                notaDc.tbNotaFiscals.InsertOnSubmit(notaNova);
                notaDc.SubmitChanges();
            }
            catch (Exception ioEx)
            {
                if (ioEx.Message.IndexOf("nota-duplicada") > -1)
                {
                    #region Validar Nota  

                    bool notaValida = false;
                    while (!notaValida)
                    {

                        using (var notaDcDuplicada = new dbCommerceDataContext())
                        {
                            numeroNota = (from c in notaDcDuplicada.tbNotaFiscals
                                          where c.idCNPJ == idEmpresa
                                          orderby c.numeroNota descending
                                          select c).FirstOrDefault();
                            if (numeroNota != null)
                            {
                                numero = (numeroNota.numeroNota + 1);
                            }

                            var notaNovaValida = new tbNotaFiscal
                            {
                                idPedido = pedidoId,
                                numeroNota = numero,
                                dataHora = DateTime.Now,
                                idPedidoEnvio = idPedidoEnvio,
                                ultimaChecagem = DateTime.Now.AddMinutes(-270),
                                ultimoStatus = "Aguardando Processamento",
                                idCNPJ = idEmpresa
                            };

                            chave = "";
                            chave += empresaSelecionada.codigoUfEmitente;
                            chave += DateTime.Now.Year.ToString().Substring(2, 2);
                            chave += DateTime.Now.Month.ToString().PadLeft(2, '0');
                            chave += empresaSelecionada.cnpj;
                            chave += "55";
                            chave += "001";
                            chave += numero.ToString().PadLeft(9, '0');
                            chave += "1";
                            chave += pedidoId.ToString().PadLeft(8, '0');

                            notaNovaValida.nfeKey = chave;

                            try
                            {
                                notaDcDuplicada.tbNotaFiscals.InsertOnSubmit(notaNovaValida);
                                notaDcDuplicada.SubmitChanges();
                                notaValida = true;
                                notaNova = notaNovaValida;
                            }
                            catch (Exception)
                            {
                                notaValida = false;
                            }
                        }
                    }

                    #endregion validar  
                }
            }


            return notaNova;
        }
    }

    private static tbNotaFiscal gerarNumeroNotaComplementar(int numeroNotaComplementar, int idCnpj)
    {
        var notaDc = new dbCommerceDataContext();
        var notaComplementar = (from c in notaDc.tbNotaFiscals where c.numeroNotaComplemento == numeroNotaComplementar select c).FirstOrDefault();
        if (notaComplementar != null)
        {
            notaComplementar.ultimaChecagem = DateTime.Now.AddMinutes(-270);
            notaDc.SubmitChanges();
            return notaComplementar;
        }
        else
        {
            var notaOrigem =
                (from c in notaDc.tbNotaFiscals
                 where c.numeroNota == numeroNotaComplementar && c.idCNPJ == idCnpj
                 select c).FirstOrDefault();
            if (notaOrigem != null)
            {
                var empresaSelecionada = (from c in notaDc.tbEmpresas where c.idEmpresa == idCnpj select c).First();
                int numero = 1;
                var numeroNota = (from c in notaDc.tbNotaFiscals where c.idCNPJ == idCnpj orderby c.numeroNota descending select c).FirstOrDefault();
                if (numeroNota != null)
                {
                    numero = (numeroNota.numeroNota + 1);
                }
                var notaNova = new tbNotaFiscal();
                notaNova.idPedido = notaOrigem.idPedido;
                notaNova.numeroNota = numero;
                notaNova.dataHora = DateTime.Now;
                notaNova.idPedidoEnvio = notaOrigem.idPedidoEnvio;
                notaNova.ultimaChecagem = DateTime.Now.AddMinutes(-270);
                notaNova.ultimoStatus = "Aguardando Processamento";
                notaNova.idCNPJ = idCnpj;
                notaNova.complemento = true;
                notaNova.numeroNotaComplemento = numeroNotaComplementar;

                string chave = "";
                chave += empresaSelecionada.codigoUfEmitente;
                chave += DateTime.Now.Year.ToString().Substring(2, 2);
                chave += DateTime.Now.Month.ToString().PadLeft(2, '0');
                chave += empresaSelecionada.cnpj;
                chave += "55";
                chave += "001";
                chave += numero.ToString().PadLeft(9, '0');
                chave += "1";
                chave += notaOrigem.idPedido.ToString().PadLeft(8, '0');

                notaNova.nfeKey = chave;

                notaDc.tbNotaFiscals.InsertOnSubmit(notaNova);
                notaDc.SubmitChanges();
                return notaNova;
            }
        }
        return null;
    }

    private static int GerarDigitoVerificadorNFe(string chave)
    {
        int soma = 0; // Vai guardar a Soma
        int mod = -1; // Vai guardar o Resto da divisão
        int dv = -1;  // Vai guardar o DigitoVerificador
        int pesso = 2; // vai guardar o pesso de multiplicacao

        //percorrendo cada caracter da chave da direita para esquerda para fazer os calculos com o pesso
        for (int i = chave.Length - 1; i != -1; i--)
        {
            int ch = Convert.ToInt32(chave[i].ToString());
            soma += ch * pesso;
            //sempre que for 9 voltamos o pesso a 2
            if (pesso < 9)
                pesso += 1;
            else
                pesso = 2;
        }

        //Agora que tenho a soma vamos pegar o resto da divisão por 11
        mod = soma % 11;
        //Aqui temos uma regrinha, se o resto da divisão for 0 ou 1 então o dv vai ser 0
        if (mod == 0 || mod == 1)
            dv = 0;
        else
            dv = 11 - mod;

        return dv;
    }

    private static string limitaTexto(string texto, int tamanho)
    {
        string textoLimpo = Regex.Replace(texto, "<(.|\\n)*?>", string.Empty);
        bool bl = textoLimpo.Length <= tamanho;
        string textoFormatado = textoLimpo;
        if (!bl)
        {
            textoFormatado = textoLimpo.Substring(0, tamanho);
        }
        return textoFormatado;
    }

    private static void CopyStream(Stream source, Stream target)
    {
        const int bufSize = 0x1000;
        byte[] buf = new byte[bufSize];
        int bytesRead = 0;
        while ((bytesRead = source.Read(buf, 0, bufSize)) > 0)
            target.Write(buf, 0, bytesRead);
    }


    public class notaGerada
    {
        public string xml { get; set; }
        public int idNotaFiscal { get; set; }
    }



    public static notaGerada gerarNotaFiscalEnvio(int idPedidoEnvio, string codigoIbge)
    {
        var pedidoDc = new dbCommerceDataContext();
        var envio = (from c in pedidoDc.tbPedidoEnvios where c.idPedidoEnvio == idPedidoEnvio select new { c.idPedido }).FirstOrDefault();
        return gerarNotaFiscal(envio.idPedido, codigoIbge, new List<int>(), false, idPedidoEnvio);

        //bool totalBrinde = true;

        //string cfop = "";

        //var pedidoDc = new dbCommerceDataContext();
        //var pedido = (from c in pedidoDc.tbPedidoEnvios where c.idPedidoEnvio == idPedidoEnvio select c.tbPedido).FirstOrDefault();
        //var envio = (from c in pedidoDc.tbPedidoEnvios where c.idPedidoEnvio == idPedidoEnvio select c).FirstOrDefault();

        //if (pedido == null) return new notaGerada();
        //int pedidoId = pedido.pedidoId;

        //var clienteDc = new dbCommerceDataContext();
        //var cliente = (from c in clienteDc.tbClientes where c.clienteId == pedido.clienteId select c).FirstOrDefault();

        //var gerarNota = gerarNumeroNota(pedido.pedidoId, idPedidoEnvio, false);
        //if ((gerarNota.complemento ?? false) == true)
        //{
        //    return gerarNotaFiscalComplemento((int)gerarNota.numeroNotaComplemento, (int)gerarNota.idCNPJ, codigoIbge);
        //}
        //var empresaNota = (from c in pedidoDc.tbEmpresas where c.idEmpresa == gerarNota.idCNPJ select c).First();

        //string numeroNota = gerarNota.numeroNota.ToString();
        //string chave = gerarNota.nfeKey;
        //string cDV = GerarDigitoVerificadorNFe(chave).ToString();
        //chave += cDV;

        //string idDest = "";

        //if (empresaNota.idEmpresa == 2)
        //{
        //    if (cliente.clienteCPFCNPJ.Length == 11 && pedido.endEstado.ToLower() == "sp")
        //    {
        //        cfop = "5101";
        //        idDest = "1";
        //    }
        //    else if (cliente.clienteCPFCNPJ.Length > 11 && pedido.endEstado.ToLower() == "sp")
        //    {
        //        cfop = "5101";
        //        idDest = "1";
        //    }
        //    else if (cliente.clienteCPFCNPJ.Length == 11 && pedido.endEstado.ToLower() != "sp")
        //    {
        //        cfop = "6101";
        //        idDest = "2";
        //    }
        //    else
        //    {
        //        cfop = "6101";
        //        idDest = "2";
        //    }
        //}
        //else
        //{
        //    if (cliente.clienteCPFCNPJ.Length == 11 && pedido.endEstado.ToLower() == "sp")
        //    {
        //        cfop = "5403";
        //        idDest = "1";
        //    }
        //    else if (cliente.clienteCPFCNPJ.Length > 11 && pedido.endEstado.ToLower() == "sp")
        //    {
        //        cfop = "5102";
        //        idDest = "1";
        //    }
        //    else if (cliente.clienteCPFCNPJ.Length == 11 && pedido.endEstado.ToLower() != "sp")
        //    {
        //        cfop = "6108";
        //        idDest = "2";
        //    }
        //    else
        //    {
        //        cfop = "6102";
        //        idDest = "2";
        //    }
        //}





        //var nota = new StringBuilder();

        //#region Identificadores da NF-e
        ////nota.Append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        ////nota.Append("<NFe xmlns=\"http://www.portalfiscal.inf.br/nfe\">");
        ////nota.AppendFormat("<infNFe versao=\"2.00\" Id=\"NFe{0}\">", chave);
        //nota.Append("<ide>");
        //nota.AppendFormat("<cUF>{0}</cUF>", empresaNota.codigoUfEmitente);
        //nota.AppendFormat("<cNF>{0}</cNF>", pedidoId.ToString().PadLeft(8, '0'));

        //if (empresaNota.idEmpresa == 2)
        //{
        //    if (cliente.clienteCPFCNPJ.Length == 11 && pedido.endEstado.ToLower() == "sp")
        //    {
        //        nota.Append("<natOp>Venda de Produção de Estabelecimento</natOp>");
        //    }
        //    else if (cliente.clienteCPFCNPJ.Length > 11 && pedido.endEstado.ToLower() == "sp")
        //    {
        //        nota.Append("<natOp>Venda de Produção de Estabelecimento</natOp>");
        //    }
        //    else if (cliente.clienteCPFCNPJ.Length == 11 && pedido.endEstado.ToLower() != "sp")
        //    {
        //        //nota.Append("<natOp>Venda de Produção de Estabelecimento Com Produtos Sujeitos ao Regime de Substituição Tributária</natOp>");
        //        nota.Append("<natOp>Venda Produtos Sujeitos Regime de Substituição Tributária</natOp>");
        //    }
        //    else
        //    {
        //        //nota.Append("<natOp>Venda de Produção de Estabelecimento Com Produtos Sujeitos ao Regime de Substituição Tributária</natOp>");
        //        nota.Append("<natOp>Venda Produtos Sujeitos Regime de Substituição Tributária</natOp>");
        //    }
        //}
        //else
        //{
        //    nota.Append("<natOp>VENDA DE MERCADORIA</natOp>");
        //}


        //nota.Append("<indPag>0</indPag>");
        //nota.Append("<mod>55</mod>");
        //nota.Append("<serie>1</serie>");
        //nota.AppendFormat("<nNF>{0}</nNF>", numeroNota);
        //nota.AppendFormat("<dhEmi>{0}T{1}</dhEmi>", DateTime.Now.ToString("yyyy-MM-dd"), DateTime.Now.ToString("HH:mm:ss"));
        //nota.Append("<fusoHorario>-02:00</fusoHorario>");
        //nota.Append("<tpNf>1</tpNf>");
        //nota.AppendFormat("<idDest>{0}</idDest>", idDest);
        //nota.AppendFormat("<indFinal>1</indFinal>");
        //nota.AppendFormat("<indPres>2</indPres>");
        //nota.AppendFormat("<cMunFg>{0}</cMunFg>", empresaNota.codigoMunicipioEmitente);
        //nota.Append("<tpImp>1</tpImp>");
        //nota.Append("<tpEmis>1</tpEmis>");
        //nota.AppendFormat("<tpAmb>{0}</tpAmb>", tpAmb);
        //nota.Append("<finNFe>1</finNFe>");
        //nota.Append("</ide>");
        //#endregion

        //#region Emitente

        //nota.Append("<emit>");
        //nota.AppendFormat("<CNPJ_emit>{0}</CNPJ_emit>", empresaNota.cnpj);
        //nota.AppendFormat("<xNome>{0}</xNome>", empresaNota.nomeEmitente);
        //nota.AppendFormat("<xFant>{0}</xFant>", empresaNota.nomeFantasiaEmitente);
        //nota.Append("<enderEmit>");
        //nota.AppendFormat("<xLgr>{0}</xLgr>", empresaNota.logradouroEmitente);
        //nota.AppendFormat("<nro>{0}</nro>", empresaNota.numeroEmitente);
        //nota.AppendFormat("<xBairro>{0}</xBairro>", empresaNota.bairroEmitente);
        //nota.AppendFormat("<cMun>{0}</cMun>", empresaNota.codigoMunicipioEmitente);
        //nota.AppendFormat("<xMun>{0}</xMun>", empresaNota.municipioEmitente);
        //nota.AppendFormat("<UF>{0}</UF>", empresaNota.ufEmitente);
        //nota.AppendFormat("<CEP>{0}</CEP>", empresaNota.cepEmitente);
        //nota.Append("<cPais>1058</cPais>");
        //nota.Append("<xPais>BRASIL</xPais>");
        //nota.AppendFormat("<fone>{0}</fone>", empresaNota.foneEmitente);
        //nota.Append("</enderEmit>");
        //nota.AppendFormat("<IE>{0}</IE>", empresaNota.ieEmitente);
        //if (empresaNota.simples) nota.Append("<CRT>1</CRT>");
        //else nota.Append("<CRT>3</CRT>");
        //nota.Append("</emit>");

        //#endregion

        //#region Destinatario

        //nota.Append("<dest>");

        //if (cliente.clienteCPFCNPJ.Length == 11)
        //{
        //    nota.AppendFormat("<CPF_dest>{0}</CPF_dest>", cliente.clienteCPFCNPJ);
        //    if (tpAmb == "2")
        //    {
        //        nota.Append("<xNome_dest>NF-E EMITIDA EM AMBIENTE DE HOMOLOGACAO - SEM VALOR FISCAL</xNome_dest>");
        //    }
        //    else
        //    {
        //        nota.AppendFormat("<xNome_dest>{0}</xNome_dest>", cliente.clienteNome.Trim());
        //    }
        //    nota.Append("<indIEDest>2</indIEDest>");
        //}
        //else
        //{
        //    nota.AppendFormat("<CNPJ_dest>{0}</CNPJ_dest>", cliente.clienteCPFCNPJ.Trim());
        //    nota.AppendFormat("<xNome_dest>{0}</xNome_dest>", cliente.clienteNomeDaEmpresa.Trim());
        //    if (tpAmb == "2")
        //    {
        //        nota.Append("<xNome_dest>NF-E EMITIDA EM AMBIENTE DE HOMOLOGACAO - SEM VALOR FISCAL</xNome_dest>");
        //    }
        //    else
        //    {
        //        if (!string.IsNullOrEmpty(cliente.clienteRGIE))
        //        {
        //            nota.Append("<indIEDest>1</indIEDest>");
        //            nota.AppendFormat("<IE_dest>{0}</IE_dest>", cliente.clienteRGIE.Trim());
        //        }
        //        else
        //        {
        //            nota.Append("<indIEDest>2</indIEDest>");
        //        }
        //    }
        //}


        //string foneNfe = "";
        //if (!string.IsNullOrEmpty(cliente.clienteFoneComercial)) foneNfe = cliente.clienteFoneComercial;
        //if (!string.IsNullOrEmpty(cliente.clienteFoneResidencial)) foneNfe = cliente.clienteFoneResidencial;
        //if (!string.IsNullOrEmpty(cliente.clienteFoneCelular)) foneNfe = cliente.clienteFoneCelular;

        //nota.Append("<enderDest>");
        //nota.AppendFormat("<xLgr_dest>{0}</xLgr_dest>", rnFuncoes.removeCaracteresEspeciaisDeixaEspaco(rnFuncoes.limpaStringDeixaEspaco(pedido.endRua.Length > 58 ? pedido.endRua.Substring(0, 58) : pedido.endRua)).ToUpper().Replace("-", " ").Trim().Trim());
        //nota.AppendFormat("<nro_dest>{0}</nro_dest>", pedido.endNumero.Trim());
        //if (!string.IsNullOrEmpty(pedido.endComplemento)) nota.AppendFormat("<xCpl_dest>{0}</xCpl_dest>", rnFuncoes.removeCaracteresEspeciaisDeixaEspaco(rnFuncoes.limpaStringDeixaEspaco(pedido.endComplemento.Length > 58 ? pedido.endComplemento.Substring(0, 58) : pedido.endComplemento)).ToUpper().Replace("-", " ").Trim().Trim());
        //nota.AppendFormat("<xBairro_dest>{0}</xBairro_dest>", pedido.endBairro.Trim());
        //nota.AppendFormat("<cMun_dest>{0}</cMun_dest>", codigoIbge.Trim());
        //nota.AppendFormat("<xMun_dest>{0}</xMun_dest>", pedido.endCidade.Trim());
        //nota.AppendFormat("<UF_dest>{0}</UF_dest>", pedido.endEstado.Trim());
        //nota.AppendFormat("<CEP_dest>{0}</CEP_dest>", pedido.endCep.Replace("-", "").Trim());
        //nota.Append("<cPais_dest>1058</cPais_dest>");
        //nota.Append("<xPais_dest>BRASIL</xPais_dest>");
        //if (!string.IsNullOrEmpty(cliente.clienteFoneResidencial)) nota.AppendFormat("<fone_dest>{0}</fone_dest>", foneNfe.Replace("(", "").Replace(")", "").Replace(" ", "").Replace("-", "").Trim());
        //nota.Append("</enderDest>");
        //if (cliente.clienteCPFCNPJ.Length == 11)
        //{
        //    //nota.Append("<IE_dest>ISENTO</IE_dest>");
        //}
        //else
        //{
        //    //nota.AppendFormat("<IE_dest>{0}</IE_dest>", cliente.clienteRGIE);
        //}
        //nota.AppendFormat("<Email_dest>{0}</Email_dest>", cliente.clienteEmail.Trim());
        //nota.Append("</dest>");
        //#endregion

        //#region Autorizacao Emissao
        //nota.Append("<autXML>");
        //nota.Append("<autXMLItem>");
        //nota.AppendFormat("<CNPJ_aut>{0}</CNPJ_aut>", empresaNota.cnpj);
        //nota.Append("</autXMLItem>");
        //nota.Append("</autXML>");
        //#endregion

        //#region Detalhamento dos Itens

        //nota.Append("<det>");

        //int nItem = 1;
        //var itensDc = new dbCommerceDataContext();
        //decimal valorDoDescontoDoCupom = pedido.valorDoDescontoDoCupom > 0 ? (decimal)pedido.valorDoDescontoDoCupom : 0;
        //decimal valorDoDescontoDoPagamento = pedido.valorDoDescontoDoPagamento > 0 ? (decimal)pedido.valorDoDescontoDoPagamento : 0;
        //decimal totalDesconto = valorDoDescontoDoCupom + valorDoDescontoDoPagamento;
        //decimal valorDoDesconto = 0;
        //decimal totalDoPedidoUnitario = 0;
        //decimal totalDoPedido = 0;
        //decimal totalIcms = 0;
        //decimal totalDescontoCalculado = 0;

        //var produtos = (from c in pedidoDc.tbProdutoEstoques where c.pedidoId == pedidoId && c.produtoId != 28735 && c.idPedidoEnvio == idPedidoEnvio select new { c.idPedidoEnvio, itemPedidoId = (c.itemPedidoId ?? 0), c.produtoId }).ToList();
        //var itensPedido = (from c in itensDc.tbItensPedidos where c.pedidoId == pedidoId && c.produtoId != 28735 && produtos.Select(x => x.itemPedidoId).Contains(c.itemPedidoId) && (c.cancelado ?? false) == false select c).ToList();
        ////var itensPedido = (from c in pedidoDc.tbProdutoEstoques where c.idPedidoEnvio == idPedidoEnvio select c);
        //var pacotes = (from c in pedidoDc.tbPedidoPacotes where c.idPedidoEnvio == idPedidoEnvio select c);
        //decimal valorFrete = 0;
        //decimal valorFreteProduto = 0;
        //int totalItens = produtos.Count;
        //if (Convert.ToDecimal(pedido.valorDoFrete) > 0)
        //{
        //    valorFreteProduto = Convert.ToDecimal(pedido.valorDoFrete) / (totalItens == 0 ? 1 : totalItens);
        //}

        //decimal pesoTotal = 0;
        //if (pacotes.Any())
        //{
        //    pesoTotal = pacotes.Sum(x => x.peso);
        //}
        //else
        //{
        //    foreach (var item in itensPedido)
        //    {
        //        var produtoDc = new dbCommerceDataContext();
        //        var produto = (from c in produtoDc.tbProdutos where c.produtoId == item.produtoId select c).First();
        //        pesoTotal += Convert.ToDecimal(produto.produtoPeso);
        //    }
        //}

        //int indiceProduto = 0;
        //foreach (var produtoEnviado in produtos)
        //{
        //    var produtoDc = new dbCommerceDataContext();
        //    var produto = (from c in produtoDc.tbProdutos where c.produtoId == produtoEnviado.produtoId select c).First();
        //    var itemPedido = itensPedido.FirstOrDefault(x => x.itemPedidoId == produtoEnviado.itemPedidoId);
        //    if (itemPedido != null)
        //    {
        //        var itensPedidoEstoque = (from c in produtoDc.tbItemPedidoEstoques where c.itemPedidoId == itemPedido.itemPedidoId select c).ToList();
        //        bool cancelado = itemPedido.cancelado == null ? false : (bool)itemPedido.cancelado;
        //        bool brinde = itemPedido.brinde == null ? false : (bool)itemPedido.brinde;
        //        decimal valorDoProduto = itemPedido.itemValor;
        //        string cfopProduto = cfop;
        //        decimal freteDoProduto = 0;
        //        decimal valorDesconto = 0;
        //        bool adicionarFrete = false;
        //        if (pedido.valorDoFrete > 0)
        //        {
        //            freteDoProduto = valorFreteProduto;
        //            valorFrete += valorFreteProduto;
        //        }

        //        if (brinde)
        //        {

        //            if (cliente.clienteEstado.ToLower() == "sp")
        //            {
        //                cfopProduto = "5910";
        //            }
        //            else
        //            {
        //                cfopProduto = "6910";
        //            }

        //        }
        //        else
        //        {
        //            totalBrinde = false;
        //        }

        //        if (!cancelado)
        //        {
        //            var produtosDc = new dbCommerceDataContext();

        //            //var combo = (from c in produtos where c.itemPedidoId == itemPedido.itemPedidoId select c).Count() > 1;
        //            var combo =
        //                (from c in itensPedidoEstoque
        //                 where c.itemPedidoId == itemPedido.itemPedidoId
        //                 select c).Any();
        //            if (combo)
        //            {
        //                decimal totalCombo = 0;
        //                foreach (var produtoRelacionado in itensPedidoEstoque)
        //                {
        //                    var produtoRelacionadoItem =
        //                        (from c in produtosDc.tbProdutos where c.produtoId == produtoRelacionado.produtoId select c)
        //                            .First();
        //                    var valorDoProdutoCombo = produtoRelacionadoItem.produtoPrecoPromocional > 0
        //                        ? produtoRelacionadoItem.produtoPrecoPromocional
        //                        : produtoRelacionadoItem.produtoPreco;
        //                    totalCombo += Convert.ToDecimal(valorDoProdutoCombo);
        //                }

        //                var produtoFilho =
        //                    (from c in produtosDc.tbProdutos where c.produtoId == produtoEnviado.produtoId select c).First();
        //                valorDoProduto = produtoFilho.produtoPrecoPromocional > 0
        //                    ? Convert.ToDecimal(produtoFilho.produtoPrecoPromocional)
        //                    : produtoFilho.produtoPreco;

        //                if (totalDesconto > 0)
        //                {
        //                    valorDesconto = (Convert.ToDecimal((valorDoProduto * totalDesconto) / pedido.valorDosItens));
        //                    if ((valorDoDesconto + valorDesconto) > totalDesconto) valorDesconto = totalDesconto - valorDoDesconto;
        //                    valorDoDesconto += valorDesconto;
        //                    totalDescontoCalculado += valorDesconto;
        //                }


        //                if (totalCombo > produtoFilho.produtoPreco)
        //                {
        //                    valorDoProduto = (valorDoProduto * itemPedido.itemValor) / totalCombo;
        //                }
        //            }
        //            else
        //            {
        //                indiceProduto++;
        //                valorDoProduto = itemPedido.itemValor == 0 ? itemPedido.tbProduto.produtoPreco : itemPedido.itemValor;
        //                if (totalDesconto > 0 && brinde == false)
        //                {
        //                    valorDesconto = (Convert.ToDecimal((itemPedido.itemValor * totalDesconto) / pedido.valorDosItens));
        //                    valorDoDesconto += valorDesconto;
        //                    totalDescontoCalculado += valorDesconto;
        //                }
        //                else
        //                {
        //                    totalBrinde = false;
        //                }
        //            }


        //            totalDoPedidoUnitario += valorDoProduto;
        //            totalDoPedido += valorDoProduto;

        //            var aliquota = (produto.icms ?? 0) == 0 ? 18 : (int)produto.icms;
        //            var valorIcms = ((Convert.ToDecimal(valorDoProduto) * Convert.ToInt32(itemPedido.itemQuantidade)) / 100) *
        //                            aliquota;

        //            totalIcms += valorIcms;

        //            nota.AppendFormat("<detItem>", nItem);
        //            nota.Append("<prod>");
        //            nota.AppendFormat("<cProd>{0}</cProd>", produto.produtoId);
        //            nota.AppendFormat("<xProd>{0}</xProd>",
        //                limitaTexto(
        //                    rnFuncoes.removeCaracteresEspeciaisDeixaEspaco(
        //                        rnFuncoes.limpaStringDeixaEspaco(produto.produtoNome))
        //                        .ToUpper()
        //                        .Replace("-", " ")
        //                        .Trim(), 119).Trim());
        //            nota.AppendFormat("<NCM>{0}</NCM>", produto.ncm.Trim());
        //            nota.AppendFormat("<CFOP>{0}</CFOP>", cfopProduto);
        //            nota.AppendFormat("<uCOM>{0}</uCOM>", "UN");
        //            nota.AppendFormat("<qCOM>{0}</qCOM>", "1.0000");
        //            nota.AppendFormat("<vUnCom>{0}</vUnCom>", valorDoProduto.ToString("0.00").Replace(",", "."));
        //            nota.AppendFormat("<vProd>{0}</vProd>", (valorDoProduto).ToString("0.00").Replace(",", "."));
        //            nota.Append("<cEANTrib/>");
        //            nota.AppendFormat("<uTrib>{0}</uTrib>", "UN");
        //            nota.AppendFormat("<qTrib>{0}</qTrib>", "1.00");
        //            nota.AppendFormat("<vUnTrib>{0}</vUnTrib>", valorDoProduto.ToString("0.00").Replace(",", "."));
        //            if (freteDoProduto > 0)
        //                nota.AppendFormat("<vFrete>{0}</vFrete>", freteDoProduto.ToString("0.00").Replace(",", "."));
        //            if (valorDesconto > 0)
        //                nota.AppendFormat("<vDesc>{0}</vDesc>", valorDesconto.ToString("0.00").Replace(",", "."));
        //            nota.AppendFormat("<indTot>{0}</indTot>", "1");
        //            nota.AppendFormat("<nTipoItem>{0}</nTipoItem>", "0");
        //            nota.Append("</prod>");
        //            if (empresaNota.simples)
        //            {
        //                nota.Append("<imposto>");
        //                nota.Append("<ICMS>");
        //                //nota.Append("<ICMSSN102>");
        //                nota.Append("<orig>0</orig>");
        //                nota.Append("<CST>102</CST>");
        //                //nota.Append("<modBC>3</modBC>");
        //                //nota.Append("<vBC>0.00</vBC>");
        //                //nota.Append("<vBC>0.00</vBC>");
        //                //nota.Append("<CSOSN>102</CSOSN>");
        //                //nota.Append("</ICMSSN102>");
        //                nota.Append("</ICMS>");
        //                /*nota.Append("<IPI>");
        //                nota.Append("<cEnq>999</cEnq>");
        //                nota.Append("<IPINT>");
        //                nota.Append("<CST>53</CST>");
        //                nota.Append("</IPINT>");
        //                nota.Append("</IPI>");*/
        //                nota.Append("<PIS>");
        //                nota.Append("<CST_pis>07</CST_pis>");
        //                nota.Append("</PIS>");
        //                nota.Append("<COFINS>");
        //                nota.Append("<CST_cofins>07</CST_cofins>");
        //                nota.Append("</COFINS>");
        //                nota.Append("</imposto>");
        //            }
        //            else
        //            {
        //                nota.Append("<imposto>");
        //                nota.Append("<ICMS>");
        //                nota.Append("<orig>0</orig>");
        //                nota.Append("<CST>00</CST>");
        //                nota.Append("<modBC>3</modBC>");
        //                nota.AppendFormat("<vBC>{0}</vBC>", (Convert.ToDecimal(valorDoProduto) * Convert.ToInt32(itemPedido.itemQuantidade)).ToString("0.00").Replace(",", "."));
        //                nota.AppendFormat("<pICMS>{0}</pICMS>", (aliquota).ToString("0.00").Replace(",", "."));
        //                nota.AppendFormat("<vICMS_icms>{0}</vICMS_icms>", (valorIcms).ToString("0.00").Replace(",", "."));
        //                nota.Append("</ICMS>");
        //                nota.Append("<PIS>");
        //                nota.Append("<CST_pis>01</CST_pis>");
        //                nota.Append("</PIS>");
        //                nota.Append("<COFINS>");
        //                nota.Append("<CST_cofins>01</CST_cofins>");
        //                nota.Append("</COFINS>");
        //                nota.Append("</imposto>");
        //            }
        //            nota.Append("</detItem>");
        //            nItem++;
        //        }
        //    }
        //}

        //nota.Append("</det>");
        //#endregion

        //#region Totais
        //nota.Append("<total>");
        //nota.Append("<ICMStot>");
        //if (empresaNota.simples)
        //{
        //    nota.Append("<vBC_ttlnfe>0.00</vBC_ttlnfe>");
        //    nota.Append("<vICMS_ttlnfe>0.00</vICMS_ttlnfe>");
        //}
        //else
        //{
        //    nota.AppendFormat("<vBC_ttlnfe>{0}</vBC_ttlnfe>", Convert.ToDecimal(totalDoPedido).ToString("0.00").Replace(",", "."));
        //    nota.AppendFormat("<vICMS_ttlnfe>{0}</vICMS_ttlnfe>", Convert.ToDecimal(totalIcms).ToString("0.00").Replace(",", "."));
        //}
        //nota.Append("<vBCST_ttlnfe>0.00</vBCST_ttlnfe>");
        //nota.Append("<vST_ttlnfe>0.00</vST_ttlnfe>");
        //nota.AppendFormat("<vProd_ttlnfe>{0}</vProd_ttlnfe>", totalDoPedido.ToString("0.00").Replace(",", "."));
        //nota.AppendFormat("<vFrete_ttlnfe>{0}</vFrete_ttlnfe>", valorFrete.ToString("0.00").Replace(",", "."));
        //nota.Append("<vSeg_ttlnfe>0.00</vSeg_ttlnfe>");
        //nota.AppendFormat("<vDesc_ttlnfe>{0}</vDesc_ttlnfe>", totalDescontoCalculado.ToString("0.00").Replace(",", "."));
        ////nota.Append("<vII>0.00</vII>");
        //nota.Append("<vIPI_ttlnfe>0.00</vIPI_ttlnfe>");
        //nota.Append("<vPIS_ttlnfe>0.00</vPIS_ttlnfe>");
        //nota.Append("<vCOFINS_ttlnfe>0.00</vCOFINS_ttlnfe>");
        //nota.Append("<vOutro>0.00</vOutro>");
        //nota.Append("<vICMSDeson_ttlnfe>0.00</vICMSDeson_ttlnfe>");
        //nota.AppendFormat("<vNF>{0}</vNF>", Convert.ToDecimal(totalDoPedido + valorFrete - valorDoDesconto).ToString("0.00").Replace(",", "."));
        //nota.Append("<vTotTrib_ttlnfe>0.00</vTotTrib_ttlnfe>");
        //nota.Append("</ICMStot>");
        //nota.Append("</total>");
        //#endregion

        //#region transportadora
        //nota.Append("<transp>");
        //nota.Append("<modFrete>0</modFrete>");

        //if (envio.formaDeEnvio == "jadlog" | envio.formaDeEnvio == "jadlogexpressa")
        //{
        //    nota.Append("<transporta>");
        //    nota.Append("<CNPJ_transp>14135995000102</CNPJ_transp>");
        //    nota.Append("<xNome_transp>CALLILI EXPRESS LTDA ME</xNome_transp>");
        //    nota.Append("<IE_transp>344063890114</IE_transp>");
        //    nota.Append("<xEnder>RUA DANIEL DE FREITAS , 928 - CENTRO</xEnder>");
        //    nota.Append("<xMun_transp>Ibitinga</xMun_transp>");
        //    nota.Append("<UF_transp>SP</UF_transp>");
        //    nota.Append("</transporta>");
        //}
        //else if (envio.formaDeEnvio == "tnt")
        //{
        //    nota.Append("<transporta>");
        //    nota.Append("<CNPJ_transp>95591723008284</CNPJ_transp>");
        //    nota.Append("<xNome_transp>TNT Mercurio Cargas e Encomendas Expressas SA</xNome_transp>");
        //    nota.Append("<IE_transp>209199664118</IE_transp>");
        //    nota.Append("<xEnder>RUA JOAQUIM PELEGRINA LOPES,1-80 - Dist. Ind. III</xEnder>");
        //    nota.Append("<xMun_transp>Bauru</xMun_transp>");
        //    nota.Append("<UF_transp>SP</UF_transp>");
        //    nota.Append("</transporta>");
        //}
        ///*else
        //{
        //    nota.Append("<transporta>");
        //    nota.Append("<CNPJ>14135995000102</CNPJ>");
        //    nota.Append("<xNome>CALLILI EXPRESS LTDA-ME</xNome>");
        //    nota.Append("<IE>344063890114</IE>");
        //    nota.Append("<xEnder>RUA DANIEL DE FREITAS , 928 - CENTRO</xEnder>");
        //    nota.Append("<xMun>Ibitinga</xMun>");
        //    nota.Append("<UF>SP</UF>");
        //    nota.Append("</transporta>");
        //}*/
        //int vol = 1;
        //nota.Append("<vol>");
        //foreach (var pacote in pacotes.OrderBy(x => x.numeroVolume))
        //{
        //    nota.Append("<volItem>");
        //    nota.AppendFormat("<qVol>{0}</qVol>", 1);
        //    nota.Append("<esp>VOLUME</esp>");
        //    nota.AppendFormat("<nVol>{0}</nVol>", vol);
        //    nota.AppendFormat("<pesoB_transp>{0}</pesoB_transp>", (Convert.ToDecimal(pacote.peso) / 1000).ToString("0.000").Replace(",", "."));
        //    nota.Append("</volItem>");
        //    vol++;
        //}
        //nota.Append("</vol>");
        //nota.Append("</transp>");

        //string informacoesAdicionais = "PEDIDO " + rnFuncoes.retornaIdCliente(pedido.pedidoId);
        //if (!string.IsNullOrEmpty(pedido.endReferenciaParaEntrega)) informacoesAdicionais += " - Ref.: " + rnFuncoes.removeAcentos(pedido.endReferenciaParaEntrega);
        //string telefone = "";
        //if (!string.IsNullOrEmpty(cliente.clienteFoneResidencial)) telefone += "Res.: " + cliente.clienteFoneResidencial + " - ";
        //if (!string.IsNullOrEmpty(cliente.clienteFoneComercial)) telefone += "Com.: " + cliente.clienteFoneComercial + " - ";
        //if (!string.IsNullOrEmpty(cliente.clienteFoneCelular)) telefone += "Cel.: " + cliente.clienteFoneCelular;
        //if (!string.IsNullOrEmpty(telefone)) informacoesAdicionais += " - " + telefone;
        //informacoesAdicionais += " - I DOCUMENTO EMITIDO POR EMPRESA ME OU EPP OPTANTE PELO SIMPLES NACIONAL II- NAO GERA CREDITO FISCAL DE IPI E ISS.";
        //informacoesAdicionais = informacoesAdicionais.Replace(Environment.NewLine, " ");
        //nota.Append("<infAdic>");
        //nota.AppendFormat("<infCpl>{0}</infCpl>", informacoesAdicionais);
        //nota.Append("</infAdic>");

        //#endregion

        ////nota.Append("</infNFe>");
        ////nota.Append("</NFe>");

        //envio.nfeNumero = Convert.ToInt32(numeroNota);
        //envio.nfeKey = chave;
        //envio.nfeValor = Convert.ToDecimal(totalDoPedido);

        //pedidoDc.SubmitChanges();

        //var dataAlt = new dbCommerceDataContext();
        //var notaAlt = (from c in dataAlt.tbNotaFiscals where c.idNotaFiscal == gerarNota.idNotaFiscal select c).First();
        //notaAlt.valor = Convert.ToDecimal(totalDoPedido + valorFrete - valorDoDesconto);
        //dataAlt.SubmitChanges();


        //try
        //{
        //    System.IO.File.WriteAllText(System.Configuration.ConfigurationManager.AppSettings["caminhoFisico"] + "\\admin\\notas\\" + pedidoId + "_" + idPedidoEnvio + ".xml", nota.ToString());
        //}
        //catch (Exception ex)
        //{ }
        //string notaFormatado = nota.ToString();
        //if (totalBrinde) notaFormatado.Replace("<natOp>VENDA DE MERCADORIA</natOp>", "<natOp>REMESSA EM BONIFICAÇÃO/BRINDE</natOp>");

        //var notaRetorno = new notaGerada();
        //notaRetorno.xml = nota.ToString();
        //notaRetorno.idNotaFiscal = gerarNota.idNotaFiscal;
        //return notaRetorno;
    }


    public static string retornaDanfeNotaPorNumeroNota(string numeroNota, int idNotaFiscal)
    {
        bool retornado = false;
        int nfeNumero = 0;
        int.TryParse(numeroNota, out nfeNumero);

        var data = new dbCommerceDataContext();
        var notaDetalhe = (from c in data.tbNotaFiscals where c.numeroNota == nfeNumero && c.idNotaFiscal == idNotaFiscal select c).First();
        if (notaDetalhe.ultimaChecagem < DateTime.Now.AddMinutes(-5))
        {
            notaDetalhe.ultimaChecagem = DateTime.Now;
            data.SubmitChanges();
            var empresaNota = (from c in data.tbEmpresas where c.idEmpresa == notaDetalhe.idCNPJ select c).First();

            var notas = new List<serviceNfe.InvoiCyRecepcaoDadosItem>();
            var serviceRecepcao = new serviceNfe.InvoiCy();
            var cabecalho = new serviceNfe.InvoiCyRecepcaoCabecalho();

            cabecalho.EmpPK = empresaNota.EmpPK;
            serviceRecepcao.Cabecalho = cabecalho;

            var nota = new serviceNfe.InvoiCyRecepcaoDadosItem();
            string documentoRequisicao =
                "<Consulta><ModeloDocumento>NFe</ModeloDocumento><Versao>3.10</Versao><tpAmb>1</tpAmb><CnpjEmissor>" +
                empresaNota.cnpj + "</CnpjEmissor><NumeroInicial>" + numeroNota + "</NumeroInicial><NumeroFinal>" +
                numeroNota +
                "</NumeroFinal><Serie>1</Serie><ChaveAcesso/><DataEmissaoInicial/><DataEmissaoFinal/><ParametrosConsulta><Situacao>S</Situacao><XMLCompleto>S</XMLCompleto><XMLLink>S</XMLLink><PDFBase64>S</PDFBase64><PDFLink>S</PDFLink><Eventos>S</Eventos></ParametrosConsulta></Consulta>";
            string parametrosRequisicao =
                "<ParametrosConsulta><Situacao>S</Situacao><XMLCompleto>S</XMLCompleto><XMLLink>S</XMLLink><PDFBase64>S</PDFBase64><PDFLink>S</PDFLink><Eventos>S</Eventos></ParametrosConsulta>";

            nota.Documento = documentoRequisicao;
            nota.Parametros = parametrosRequisicao;
            string valorParaHash = empresaNota.chaveDeAcesso + documentoRequisicao;
            cabecalho.EmpCK = rnFuncoes.gerarMd5(valorParaHash);
            notas.Add(nota);
            serviceRecepcao.Dados = notas.ToArray();
            var nfe = new serviceNfe.recepcao();
            var retornoNota = nfe.Execute(serviceRecepcao);
            try
            {
                XmlDocument xml = new XmlDocument();
                xml.LoadXml(retornoNota.Mensagem[0].Documentos[0].Documento);
                XmlNodeList nodes = xml.DocumentElement.SelectNodes("/Documento/DocSitDescricao");
                XmlNodeList nodesPdf = xml.DocumentElement.SelectNodes("/Documento/DocPDFLink");
                XmlNodeList nodesPdfBase64 = xml.DocumentElement.SelectNodes("/Documento/DocPDF");
                XmlNodeList nodesXmls = xml.DocumentElement.SelectNodes("/Documento/DocXML");
                foreach (XmlNode node in nodes)
                {
                    int numeroNfe = Convert.ToInt32(numeroNota);
                    var notaDc = new dbCommerceDataContext();
                    var notaAlterar = (from c in notaDc.tbNotaFiscals where c.numeroNota == numeroNfe && c.idNotaFiscal == idNotaFiscal select c).First();
                    string linkPdf = "";
                    string pdfBase64 = "";
                    string xmlCompleto = "";
                    foreach (XmlNode pdfLink in nodesPdf)
                    {
                        if (!string.IsNullOrEmpty(pdfLink.InnerText))
                        {
                            linkPdf = pdfLink.InnerText;
                        }
                    }
                    foreach (XmlNode pdfBase in nodesPdfBase64)
                    {
                        if (!string.IsNullOrEmpty(pdfBase.InnerText))
                        {
                            pdfBase64 = pdfBase.InnerText;
                        }
                    }
                    foreach (XmlNode nodeXml in nodesXmls)
                    {
                        if (!string.IsNullOrEmpty(nodeXml.InnerText))
                        {
                            xmlCompleto = nodeXml.InnerText;
                        }
                    }

                    if (!string.IsNullOrEmpty(linkPdf))
                    {
                        if (notaAlterar.idPedidoEnvio != null)
                        {
                            var pedidoEnvio =
                                (from c in notaDc.tbPedidoEnvios
                                 where c.idPedidoEnvio == notaAlterar.idPedidoEnvio
                                 select c).FirstOrDefault();
                            if (pedidoEnvio != null)
                            {
                                pedidoEnvio.nfeStatus = 2;
                                notaDc.SubmitChanges();
                            }
                        }
                        notaAlterar.linkDanfe = linkPdf;
                        notaAlterar.danfeBase64 = pdfBase64;
                        notaAlterar.xmlBase64 = xmlCompleto;
                        notaAlterar.statusNfe = 2;
                        notaDc.SubmitChanges();
                        retornado = true;
                        return linkPdf;
                    }
                    if (string.IsNullOrEmpty(linkPdf) && !string.IsNullOrEmpty(pdfBase64))
                    {
                        gravaDanfeBase64(notaAlterar.numeroNota, (notaAlterar.idCNPJ ?? 0), true);
                        return "";
                    }
                }
            }
            catch (Exception)
            {

            }
        }
        //retornaDanfeBase64NotaPorNumeroNota(numeroNota, idNotaFiscal);
        return "";
    }
    public static bool gravaDanfeBase64(int numeroNota, int idCnpj, bool updateDanfeLink)
    {
        var data = new dbCommerceDataContext();
        var notaDetalhe = (from c in data.tbNotaFiscals where c.numeroNota == numeroNota && c.idCNPJ == idCnpj select c).FirstOrDefault();
        if (notaDetalhe != null)
        {
            return gravaDanfeBase64(notaDetalhe.numeroNota.ToString(), notaDetalhe.idNotaFiscal, updateDanfeLink);
        }
        return false;
    }

    public static bool gravaDanfeBase64(string numeroNota, int idNotaFiscal, bool updateDanfeLink)
    {
        string danfe = retornaDanfeBase64NotaPorNumeroNota(numeroNota, idNotaFiscal);
        if (!string.IsNullOrEmpty(danfe))
        {
            int nfeNumero = 0;
            int.TryParse(numeroNota, out nfeNumero);
            var data = new dbCommerceDataContext();
            var notaDetalhe = (from c in data.tbNotaFiscals where c.numeroNota == nfeNumero && c.idNotaFiscal == idNotaFiscal select c).First();
            byte[] binaryData;
            binaryData = Convert.FromBase64String(danfe);
            System.IO.FileStream outFile = new FileStream("C:\\inetpub\\wwwroot\\admin\\notas\\danfes\\" + notaDetalhe.idCNPJ.ToString() + "_" + notaDetalhe.numeroNota + ".pdf", System.IO.FileMode.Create, System.IO.FileAccess.Write);
            outFile.Write(binaryData, 0, binaryData.Length);
            outFile.Close();

            if (updateDanfeLink)
            {
                string url = System.Configuration.ConfigurationManager.AppSettings["caminhoVirtualAdmin"] +
                             "notas/danfes/" + notaDetalhe.idCNPJ.ToString() + "_" + notaDetalhe.numeroNota + ".pdf";
                notaDetalhe.linkDanfe = url;
                notaDetalhe.statusNfe = 2;
                data.SubmitChanges();
                if (notaDetalhe.idPedidoEnvio != null)
                {
                    //var envio = (from c in data.tbPedidoEnvios where c.idPedidoEnvio == notaDetalhe.idPedidoEnvio select c).FirstOrDefault();
                    var envio = data.tbPedidoEnvios.FirstOrDefault(c => c.idPedidoEnvio == notaDetalhe.idPedidoEnvio);
                    if (envio != null)
                    {
                        envio.nfeStatus = 2;
                        data.SubmitChanges();
                    }
                }
            }
            return true;
        }
        return false;
    }
    public static string retornaDanfeBase64NotaPorNumeroNota(string numeroNota, int idNotaFiscal)
    {
        int nfeNumero = 0;
        int.TryParse(numeroNota, out nfeNumero);

        var data = new dbCommerceDataContext();
        var notaDetalhe = (from c in data.tbNotaFiscals where c.numeroNota == nfeNumero && c.idNotaFiscal == idNotaFiscal select c).First();

        notaDetalhe.ultimaChecagem = DateTime.Now;
        data.SubmitChanges();
        var empresaNota = (from c in data.tbEmpresas where c.idEmpresa == notaDetalhe.idCNPJ select c).First();

        var notas = new List<serviceNfe.InvoiCyRecepcaoDadosItem>();
        var serviceRecepcao = new serviceNfe.InvoiCy();
        var cabecalho = new serviceNfe.InvoiCyRecepcaoCabecalho();

        cabecalho.EmpPK = empresaNota.EmpPK;
        serviceRecepcao.Cabecalho = cabecalho;

        var nota = new serviceNfe.InvoiCyRecepcaoDadosItem();
        string documentoRequisicao =
            "<Consulta><ModeloDocumento>NFe</ModeloDocumento><Versao>3.10</Versao><tpAmb>1</tpAmb><CnpjEmissor>" +
            empresaNota.cnpj + "</CnpjEmissor><NumeroInicial>" + numeroNota + "</NumeroInicial><NumeroFinal>" +
            numeroNota +
            "</NumeroFinal><Serie>1</Serie><ChaveAcesso/><DataEmissaoInicial/><DataEmissaoFinal/><ParametrosConsulta><Situacao>S</Situacao><XMLCompleto>S</XMLCompleto><XMLLink>S</XMLLink><PDFBase64>S</PDFBase64><PDFLink>S</PDFLink><Eventos>S</Eventos></ParametrosConsulta></Consulta>";
        string parametrosRequisicao =
            "<ParametrosConsulta><Situacao>S</Situacao><XMLCompleto>N</XMLCompleto><XMLLink>S</XMLLink><PDFBase64>S</PDFBase64><PDFLink>S</PDFLink><Eventos>S</Eventos></ParametrosConsulta>";

        nota.Documento = documentoRequisicao;
        nota.Parametros = parametrosRequisicao;
        string valorParaHash = empresaNota.chaveDeAcesso + documentoRequisicao;
        cabecalho.EmpCK = rnFuncoes.gerarMd5(valorParaHash);
        notas.Add(nota);
        serviceRecepcao.Dados = notas.ToArray();
        var nfe = new serviceNfe.recepcao();
        var retornoNota = nfe.Execute(serviceRecepcao);
        try
        {
            XmlDocument xml = new XmlDocument();
            xml.LoadXml(retornoNota.Mensagem[0].Documentos[0].Documento);
            XmlNodeList nodes = xml.DocumentElement.SelectNodes("/Documento/DocSitDescricao");
            XmlNodeList nodesPdf = xml.DocumentElement.SelectNodes("/Documento/DocPDF");
            foreach (XmlNode pdfLink in nodesPdf)
            {
                if (!string.IsNullOrEmpty(pdfLink.InnerText))
                {
                    try
                    {
                        //    byte[] binaryData;
                        //    binaryData = Convert.FromBase64String(pdfLink.InnerText);
                        //    System.IO.FileStream outFile = new FileStream("C:\\inetpub\\wwwroot\\admin\\notas\\danfes\\" + notaDetalhe.idCNPJ.ToString() + "_" + notaDetalhe.numeroNota + ".pdf", System.IO.FileMode.Create, System.IO.FileAccess.Write);
                        //    outFile.Write(binaryData, 0, binaryData.Length);
                        //    outFile.Close();
                    }
                    catch (Exception)
                    {

                    }
                    notaDetalhe.danfeBase64 = pdfLink.InnerText;
                    data.SubmitChanges();
                    return pdfLink.InnerText;
                    /*int numeroNfe = Convert.ToInt32(numeroNota);
                    var notaDc = new dbCommerceDataContext();
                    var notaAlterar =
                        (from c in notaDc.tbNotaFiscals where c.numeroNota == numeroNfe select c).First();
                    notaAlterar.linkDanfe = pdfLink.InnerText;
                    notaDc.SubmitChanges();
                    return pdfLink.InnerText;*/
                }
            }
        }
        catch (Exception)
        {

        }

        return "";
    }
    public static string retornaInformacaoNotaPorNumeroNota(string numeroNota, int idNotaFiscal)
    {
        int nfeNumero = 0;
        int.TryParse(numeroNota, out nfeNumero);

        var data = new dbCommerceDataContext();
        var notaDetalhe = (from c in data.tbNotaFiscals where c.numeroNota == nfeNumero && c.idNotaFiscal == idNotaFiscal select c).First();
        var empresaNota = (from c in data.tbEmpresas where c.idEmpresa == notaDetalhe.idCNPJ select c).First();


        var notas = new List<serviceNfe.InvoiCyRecepcaoDadosItem>();
        var serviceRecepcao = new serviceNfe.InvoiCy();
        var cabecalho = new serviceNfe.InvoiCyRecepcaoCabecalho();

        cabecalho.EmpPK = empresaNota.EmpPK;
        serviceRecepcao.Cabecalho = cabecalho;

        var nota = new serviceNfe.InvoiCyRecepcaoDadosItem();
        string documentoRequisicao = "<Consulta><ModeloDocumento>NFe</ModeloDocumento><Versao>3.10</Versao><tpAmb>1</tpAmb><CnpjEmissor>" + empresaNota.cnpj + "</CnpjEmissor><NumeroInicial>" + numeroNota + "</NumeroInicial><NumeroFinal>" + numeroNota + "</NumeroFinal><Serie>1</Serie><ChaveAcesso/><DataEmissaoInicial/><DataEmissaoFinal/><ParametrosConsulta><Situacao>S</Situacao><XMLCompleto>S</XMLCompleto><XMLLink>S</XMLLink><PDFBase64>S</PDFBase64><PDFLink>S</PDFLink><Eventos>S</Eventos></ParametrosConsulta></Consulta>";
        string parametrosRequisicao = "<ParametrosConsulta><Situacao>S</Situacao><XMLCompleto>N</XMLCompleto><XMLLink>S</XMLLink><PDFBase64>N</PDFBase64><PDFLink>S</PDFLink><Eventos>S</Eventos></ParametrosConsulta>";

        nota.Documento = documentoRequisicao;
        nota.Parametros = parametrosRequisicao;
        string valorParaHash = empresaNota.chaveDeAcesso + documentoRequisicao;
        cabecalho.EmpCK = rnFuncoes.gerarMd5(valorParaHash);
        notas.Add(nota);
        serviceRecepcao.Dados = notas.ToArray();
        var nfe = new serviceNfe.recepcao();
        var retornoNota = nfe.Execute(serviceRecepcao);
        try
        {
            XmlDocument xml = new XmlDocument();
            xml.LoadXml(retornoNota.Mensagem[0].Documentos[0].Documento);
            XmlNodeList nodes = xml.DocumentElement.SelectNodes("/Documento/DocSitDescricao");
            XmlNodeList nodesPdf = xml.DocumentElement.SelectNodes("/Documento/DocPDFLink");
            foreach (XmlNode node in nodes)
            {
                return node.InnerText;
            }
        }
        catch (Exception)
        {

        }
        return "";
    }

    public static void atualizaDataReceitaFederal(int nfeNumero, int idNotaFiscal, int idCNPJ)
    {
        System.Threading.Thread.Sleep(1000);

        var data = new dbCommerceDataContext();
        tbNotaFiscal notaDetalhe = new tbNotaFiscal();
        if (idCNPJ == 0)
        {
            notaDetalhe = (from c in data.tbNotaFiscals where c.numeroNota == nfeNumero && c.idNotaFiscal == idNotaFiscal select c).First();
        }
        else
        {
            notaDetalhe = (from c in data.tbNotaFiscals where c.numeroNota == nfeNumero && c.idCNPJ == idCNPJ select c).First();
        }
        var empresaNota = (from c in data.tbEmpresas where c.idEmpresa == notaDetalhe.idCNPJ select c).First();


        var notas = new List<serviceNfe.InvoiCyRecepcaoDadosItem>();
        var serviceRecepcao = new serviceNfe.InvoiCy();
        var cabecalho = new serviceNfe.InvoiCyRecepcaoCabecalho();

        cabecalho.EmpPK = empresaNota.EmpPK;
        serviceRecepcao.Cabecalho = cabecalho;

        var nota = new serviceNfe.InvoiCyRecepcaoDadosItem();
        string documentoRequisicao = "<Consulta><ModeloDocumento>NFe</ModeloDocumento><Versao>3.10</Versao><tpAmb>1</tpAmb><CnpjEmissor>" + empresaNota.cnpj + "</CnpjEmissor><NumeroInicial>" + nfeNumero + "</NumeroInicial><NumeroFinal>" + nfeNumero + "</NumeroFinal><Serie>1</Serie><ChaveAcesso/><DataEmissaoInicial/><DataEmissaoFinal/><ParametrosConsulta><Situacao>S</Situacao><XMLCompleto>S</XMLCompleto><XMLLink>S</XMLLink><PDFBase64>S</PDFBase64><PDFLink>S</PDFLink><Eventos>S</Eventos></ParametrosConsulta></Consulta>";
        string parametrosRequisicao = "<ParametrosConsulta><Situacao>S</Situacao><XMLCompleto>N</XMLCompleto><XMLLink>S</XMLLink><PDFBase64>N</PDFBase64><PDFLink>S</PDFLink><Eventos>S</Eventos></ParametrosConsulta>";

        nota.Documento = documentoRequisicao;
        nota.Parametros = parametrosRequisicao;
        string valorParaHash = empresaNota.chaveDeAcesso + documentoRequisicao;
        cabecalho.EmpCK = rnFuncoes.gerarMd5(valorParaHash);
        notas.Add(nota);
        serviceRecepcao.Dados = notas.ToArray();
        var nfe = new serviceNfe.recepcao();
        var retornoNota = nfe.Execute(serviceRecepcao);
        try
        {
            XmlDocument xml = new XmlDocument();
            xml.LoadXml(retornoNota.Mensagem[0].Documentos[0].Documento);
            XmlNodeList nodesSituacao = xml.DocumentElement.SelectNodes("/Documento/DocSitDescricao");
            XmlNodeList nodesDataEmissao = xml.DocumentElement.SelectNodes("/Documento/Resumo/DocDataEmissao");
            XmlNodeList nodesPDFLink = xml.DocumentElement.SelectNodes("/Documento/DocPDFLink");
            foreach (XmlNode node in nodesDataEmissao)
            {
                notaDetalhe.dataReceitaFederal = Convert.ToDateTime(node.InnerText);
            }

            foreach (XmlNode node in nodesSituacao)
            {
                notaDetalhe.ultimoStatus = node.InnerText;
            }
            foreach (XmlNode node in nodesPDFLink)
            {
                notaDetalhe.linkDanfe = node.InnerText;
            }
            if (notaDetalhe != null)
            {
                data.SubmitChanges();
            }
        }
        catch (Exception)
        {

        }

    }
    public static string retornaDanfeNotaPorKey(string accessKey, int idCnpj)
    {
        var data = new dbCommerceDataContext();
        var notaDetalhe = (from c in data.tbNotaFiscals where c.nfeKey == accessKey && c.idCNPJ == idCnpj select c).FirstOrDefault();
        if (notaDetalhe != null)
        {
            var empresaNota = (from c in data.tbEmpresas where c.idEmpresa == notaDetalhe.idCNPJ select c).First();

            var notas = new List<serviceNfe.InvoiCyRecepcaoDadosItem>();
            var serviceRecepcao = new serviceNfe.InvoiCy();
            var cabecalho = new serviceNfe.InvoiCyRecepcaoCabecalho();

            cabecalho.EmpPK = empresaNota.EmpPK;
            serviceRecepcao.Cabecalho = cabecalho;

            var nota = new serviceNfe.InvoiCyRecepcaoDadosItem();
            string documentoRequisicao =
                "<Consulta><ModeloDocumento>NFe</ModeloDocumento><Versao>3.10</Versao><tpAmb>1</tpAmb><CnpjEmissor>" +
                empresaNota.cnpj + "</CnpjEmissor><ChaveAcesso>" + accessKey +
                "</ChaveAcesso><ChaveAcesso/><DataEmissaoInicial/><DataEmissaoFinal/><ParametrosConsulta><Situacao>S</Situacao><XMLCompleto>S</XMLCompleto><XMLLink>S</XMLLink><PDFBase64>S</PDFBase64><PDFLink>S</PDFLink><Eventos>S</Eventos></ParametrosConsulta></Consulta>";
            string parametrosRequisicao =
                "<ParametrosConsulta><Situacao>S</Situacao><XMLCompleto>N</XMLCompleto><XMLLink>S</XMLLink><PDFBase64>N</PDFBase64><PDFLink>S</PDFLink><Eventos>S</Eventos></ParametrosConsulta>";

            nota.Documento = documentoRequisicao;
            nota.Parametros = parametrosRequisicao;
            string valorParaHash = empresaNota.chaveDeAcesso + documentoRequisicao;
            cabecalho.EmpCK = rnFuncoes.gerarMd5(valorParaHash);
            notas.Add(nota);
            serviceRecepcao.Dados = notas.ToArray();
            var nfe = new serviceNfe.recepcao();
            var retornoNota = nfe.Execute(serviceRecepcao);
            try
            {
                XmlDocument xml = new XmlDocument();
                xml.LoadXml(retornoNota.Mensagem[0].Documentos[0].Documento);
                XmlNodeList nodes = xml.DocumentElement.SelectNodes("/Documento/DocSitDescricao");
                XmlNodeList nodesPdf = xml.DocumentElement.SelectNodes("/Documento/DocPDFLink");
                foreach (XmlNode node in nodes)
                {
                    //if (node.InnerText == "Autorizado o uso da NF-e")
                    //{
                    foreach (XmlNode pdfLink in nodesPdf)
                    {
                        if (!string.IsNullOrEmpty(pdfLink.InnerText))
                        {
                            return pdfLink.InnerText;
                        }
                    }
                    //}
                }
            }
            catch (Exception)
            {

            }
            return "";
        }
        return "";
    }


    public static bool checaNotasPendentes()
    {
        return checaNotasPendentes(0);
    }


    public static bool checaNotasPendentes(int idPedidoEnvio)
    {
        bool notaAlterada = false;


        var pedidosDc = new dbCommerceDataContext();
        var notasUnion = new List<tbNotaFiscal>();
        if (idPedidoEnvio > 0)
        {
            notasUnion = (from c in pedidosDc.tbNotaFiscals where c.idPedidoEnvio == idPedidoEnvio select c).ToList();
        }
        else
        {
            var pedidosNotaProcessamentoEnvios =
            (from c in pedidosDc.tbPedidoEnvios
             join d in pedidosDc.tbNotaFiscals on c.idPedidoEnvio equals d.idPedidoEnvio
             where (c.nfeStatus == 1)
             select d).ToList();
            var notasProcessamentoGeral = (from c in pedidosDc.tbNotaFiscals where c.statusNfe == 1 select c).ToList();
            notasUnion = pedidosNotaProcessamentoEnvios.Union(notasProcessamentoGeral).ToList();
        }

        foreach (var pedido in notasUnion)
        {
            var notaCheck = (from c in pedidosDc.tbNotaFiscals where c.idNotaFiscal == pedido.idNotaFiscal select c).FirstOrDefault();
            bool checar = true;
            if (notaCheck == null)
            {
                checar = false;
            }
            else
            {
                var agora = DateTime.Now.AddMinutes(-5);
                if (!(notaCheck.ultimaChecagem == null | notaCheck.ultimaChecagem < agora))
                {
                    checar = false;
                }
            }

            if (idPedidoEnvio > 0) checar = true;
            if (checar)
            {
                var empresaNota = (from c in pedidosDc.tbEmpresas where c.idEmpresa == notaCheck.idCNPJ select c).First();
                notaCheck.ultimaChecagem = DateTime.Now;
                pedidosDc.SubmitChanges();

                var notas = new List<serviceNfe.InvoiCyRecepcaoDadosItem>();
                var serviceRecepcao = new serviceNfe.InvoiCy();
                var cabecalho = new serviceNfe.InvoiCyRecepcaoCabecalho();

                cabecalho.EmpPK = empresaNota.EmpPK;
                serviceRecepcao.Cabecalho = cabecalho;

                var nota = new serviceNfe.InvoiCyRecepcaoDadosItem();
                //string documentoRequisicao = "<Consulta><ModeloDocumento>NFe</ModeloDocumento><Versao>3.10</Versao><tpAmb>1</tpAmb><CnpjEmissor>" + rnNotaFiscal.cnpjEmitente + "</CnpjEmissor><ChaveAcesso>" + pedido.nfeKey + "</ChaveAcesso><ChaveAcesso/><DataEmissaoInicial/><DataEmissaoFinal/><ParametrosConsulta><Situacao>S</Situacao><XMLCompleto>S</XMLCompleto><XMLLink>S</XMLLink><PDFBase64>S</PDFBase64><PDFLink>S</PDFLink><Eventos>S</Eventos></ParametrosConsulta></Consulta>";

                string documentoRequisicao = "<Consulta><ModeloDocumento>NFe</ModeloDocumento><Versao>3.10</Versao><tpAmb>1</tpAmb><CnpjEmissor>" + empresaNota.cnpj + "</CnpjEmissor><NumeroInicial>" + pedido.numeroNota + "</NumeroInicial><NumeroFinal>" + pedido.numeroNota + "</NumeroFinal><Serie>1</Serie><ChaveAcesso/><DataEmissaoInicial/><DataEmissaoFinal/><ParametrosConsulta><Situacao>S</Situacao><XMLCompleto>S</XMLCompleto><XMLLink>S</XMLLink><PDFBase64>S</PDFBase64><PDFLink>S</PDFLink><Eventos>S</Eventos></ParametrosConsulta></Consulta>";
                string parametrosRequisicao = "<ParametrosConsulta><Situacao>S</Situacao><XMLCompleto>S</XMLCompleto><XMLLink>S</XMLLink><PDFBase64>S</PDFBase64><PDFLink>S</PDFLink><Eventos>S</Eventos></ParametrosConsulta>";

                nota.Documento = documentoRequisicao;
                nota.Parametros = parametrosRequisicao;
                string valorParaHash = empresaNota.chaveDeAcesso + documentoRequisicao;
                cabecalho.EmpCK = rnFuncoes.gerarMd5(valorParaHash);
                notas.Add(nota);
                serviceRecepcao.Dados = notas.ToArray();
                var nfe = new serviceNfe.recepcao();
                var retornoNota = nfe.Execute(serviceRecepcao);
                try
                {
                    XmlDocument xml = new XmlDocument();
                    xml.LoadXml(retornoNota.Mensagem[0].Documentos[0].Documento);
                    XmlNodeList nodes = xml.DocumentElement.SelectNodes("/Documento/DocSitDescricao");
                    XmlNodeList nodesPdf = xml.DocumentElement.SelectNodes("/Documento/DocPDFLink");
                    XmlNodeList nodesPdfBase64 = xml.DocumentElement.SelectNodes("/Documento/DocPDF");
                    XmlNodeList nodesXmls = xml.DocumentElement.SelectNodes("/Documento/DocXML");
                    foreach (XmlNode node in nodes)
                    {
                        int numeroNfe = Convert.ToInt32(notaCheck.numeroNota);
                        var notaDc = new dbCommerceDataContext();
                        var notaAlterar = (from c in notaDc.tbNotaFiscals where c.numeroNota == numeroNfe && c.idNotaFiscal == notaCheck.idNotaFiscal select c).First();
                        string linkPdf = "";
                        string pdfBase64 = "";
                        string xmlCompleto = "";
                        foreach (XmlNode pdfLink in nodesPdf)
                        {
                            if (!string.IsNullOrEmpty(pdfLink.InnerText))
                            {
                                linkPdf = pdfLink.InnerText;
                            }
                        }
                        foreach (XmlNode pdfBase in nodesPdfBase64)
                        {
                            if (!string.IsNullOrEmpty(pdfBase.InnerText))
                            {
                                pdfBase64 = pdfBase.InnerText;
                            }
                        }
                        foreach (XmlNode nodeXml in nodesXmls)
                        {
                            if (!string.IsNullOrEmpty(nodeXml.InnerText))
                            {
                                xmlCompleto = nodeXml.InnerText;
                            }
                        }

                        if (!string.IsNullOrEmpty(linkPdf))
                        {
                            if (notaAlterar.idPedidoEnvio != null)
                            {
                                var pedidoEnvio =
                                    (from c in notaDc.tbPedidoEnvios
                                     where c.idPedidoEnvio == notaAlterar.idPedidoEnvio
                                     select c).FirstOrDefault();
                                if (pedidoEnvio != null)
                                {
                                    pedidoEnvio.nfeStatus = 2;
                                    notaDc.SubmitChanges();
                                }
                            }
                            notaAlterar.linkDanfe = linkPdf;
                            notaAlterar.danfeBase64 = pdfBase64;
                            notaAlterar.xmlBase64 = xmlCompleto;
                            notaAlterar.statusNfe = 2;
                            notaDc.SubmitChanges();
                        }
                        if (string.IsNullOrEmpty(linkPdf) && !string.IsNullOrEmpty(pdfBase64))
                        {
                            gravaDanfeBase64(notaAlterar.numeroNota, (notaAlterar.idCNPJ ?? 0), true);
                        }
                    }
                }
                catch (Exception)
                {

                }
            }
            notaAlterada = true;
        }
        return notaAlterada;
    }

    public static string retornaXmlNota(int nfeNumero, int idNotaFiscal)
    {
        var data = new dbCommerceDataContext();
        var notaDetalhe = (from c in data.tbNotaFiscals where c.numeroNota == nfeNumero && c.idNotaFiscal == idNotaFiscal select c).First();
        var empresaNota = (from c in data.tbEmpresas where c.idEmpresa == notaDetalhe.idCNPJ select c).First();


        var notas = new List<serviceNfe.InvoiCyRecepcaoDadosItem>();
        var serviceRecepcao = new serviceNfe.InvoiCy();
        var cabecalho = new serviceNfe.InvoiCyRecepcaoCabecalho();

        cabecalho.EmpPK = empresaNota.EmpPK;
        serviceRecepcao.Cabecalho = cabecalho;

        var nota = new serviceNfe.InvoiCyRecepcaoDadosItem();
        //string documentoRequisicao = "<Consulta><ModeloDocumento>NFe</ModeloDocumento><Versao>3.10</Versao><tpAmb>1</tpAmb><CnpjEmissor>" + rnNotaFiscal.cnpjEmitente + "</CnpjEmissor><ChaveAcesso>" + pedido.nfeKey + "</ChaveAcesso><ChaveAcesso/><DataEmissaoInicial/><DataEmissaoFinal/><ParametrosConsulta><Situacao>S</Situacao><XMLCompleto>S</XMLCompleto><XMLLink>S</XMLLink><PDFBase64>S</PDFBase64><PDFLink>S</PDFLink><Eventos>S</Eventos></ParametrosConsulta></Consulta>";

        string documentoRequisicao = "<Consulta><ModeloDocumento>NFe</ModeloDocumento><Versao>3.10</Versao><tpAmb>1</tpAmb><CnpjEmissor>" + empresaNota.cnpj + "</CnpjEmissor><NumeroInicial>" + nfeNumero + "</NumeroInicial><NumeroFinal>" + nfeNumero + "</NumeroFinal><Serie>1</Serie><ChaveAcesso/><DataEmissaoInicial/><DataEmissaoFinal/><ParametrosConsulta><Situacao>S</Situacao><XMLCompleto>S</XMLCompleto><XMLLink>S</XMLLink><PDFBase64>S</PDFBase64><PDFLink>S</PDFLink><Eventos>S</Eventos></ParametrosConsulta></Consulta>";
        string parametrosRequisicao = "<ParametrosConsulta><Situacao>S</Situacao><XMLCompleto>S</XMLCompleto><XMLLink>S</XMLLink><PDFBase64>S</PDFBase64><PDFLink>S</PDFLink><Eventos>S</Eventos></ParametrosConsulta>";

        nota.Documento = documentoRequisicao;
        nota.Parametros = parametrosRequisicao;
        string valorParaHash = empresaNota.chaveDeAcesso + documentoRequisicao;
        cabecalho.EmpCK = rnFuncoes.gerarMd5(valorParaHash);
        notas.Add(nota);
        serviceRecepcao.Dados = notas.ToArray();
        var nfe = new serviceNfe.recepcao();
        var retornoNota = nfe.Execute(serviceRecepcao);
        try
        {
            XmlDocument xml = new XmlDocument();
            xml.LoadXml(retornoNota.Mensagem[0].Documentos[0].Documento);
            XmlNodeList nodes = xml.DocumentElement.SelectNodes("/Documento/DocSitDescricao");
            XmlNodeList nodesPdf = xml.DocumentElement.SelectNodes("/Documento/DocXML");
            foreach (XmlNode node in nodes)
            {
                //if (node.InnerText == "Autorizado o uso da NF-e")
                //{
                foreach (XmlNode pdfLink in nodesPdf)
                {
                    if (!string.IsNullOrEmpty(pdfLink.InnerText))
                    {
                        var xmlNota = base64Decode(pdfLink.InnerText);
                        Encoding w1252 = Encoding.GetEncoding(1252);
                        Encoding utf8 = Encoding.UTF8;
                        string msg = utf8.GetString(w1252.GetBytes(xmlNota));
                        return xmlNota;
                    }
                }
                //}
            }
        }
        catch (Exception)
        {

        }
        return "";
    }

    public static string retornaXmlNotaFromBase64(string xmlBase64)
    {
        var xmlNota = base64Decode(xmlBase64);
        Encoding w1252 = Encoding.GetEncoding(1252);
        Encoding utf8 = Encoding.UTF8;
        string msg = utf8.GetString(w1252.GetBytes(xmlNota));
        return xmlNota;
    }

    public static void autorizarNota(string xmlNotaFiscal, int idPedidoEnvio, int idNotaFiscal)
    {
        var data = new dbCommerceDataContext();
        var notaDetalhe = (from c in data.tbNotaFiscals where c.idNotaFiscal == idNotaFiscal select c).First();
        var empresaNota = (from c in data.tbEmpresas where c.idEmpresa == notaDetalhe.idCNPJ select c).First();

        var notas = new List<serviceNfe.InvoiCyRecepcaoDadosItem>();
        var serviceRecepcao = new serviceNfe.InvoiCy();
        var cabecalho = new serviceNfe.InvoiCyRecepcaoCabecalho();

        cabecalho.EmpPK = empresaNota.EmpPK;
        serviceRecepcao.Cabecalho = cabecalho;

        var nota = new serviceNfe.InvoiCyRecepcaoDadosItem();
        string documentoRequisicao = "<Envio><ModeloDocumento>NFe</ModeloDocumento><Versao>3.10</Versao>" + xmlNotaFiscal + "</Envio>";
        nota.Documento = documentoRequisicao;
        //rnEmails.EnviaEmail("", "andre@bark.com.br", "", "", "", documentoRequisicao, "nota");
        //HttpContext.Current.Response.Write(documentoRequisicao);
        string valorParaHash = empresaNota.chaveDeAcesso + documentoRequisicao;
        cabecalho.EmpCK = rnFuncoes.gerarMd5(valorParaHash);


        notas.Add(nota);
        //gerarNotaFiscalEnvio
        serviceRecepcao.Dados = notas.ToArray();

        var nfe = new serviceNfe.recepcao();
        var retornoNota = nfe.Execute(serviceRecepcao);
        System.IO.File.WriteAllText(System.Configuration.ConfigurationManager.AppSettings["caminhoFisico"] + "\\admin\\autorizacaonotas\\retorno_autorizacao_" + notaDetalhe.idPedido + "_" + idNotaFiscal + ".xml", retornoNota.Mensagem[0].Documentos[0].Documento.ToString());
        //HttpContext.Current.Response.Write(retornoNota.Mensagem[0].Descricao);
        try
        {
            var pedidosDc = new dbCommerceDataContext();
            var pedidoEnvio = (from c in pedidosDc.tbPedidoEnvios where c.idPedidoEnvio == idPedidoEnvio select c).First();
            pedidoEnvio.nfeStatus = 1;
            pedidosDc.SubmitChanges();
        }
        catch (Exception ex)
        {
            string erro = ex.Message;
        }

        try
        {
            XmlDocument xml = new XmlDocument();
            xml.LoadXml(retornoNota.Mensagem[0].Documentos[0].Documento);
            XmlNodeList nodes = xml.DocumentElement.SelectNodes("/Documento/Situacao/SitDescricao");
            XmlNodeList nodesPdfBase64 = xml.DocumentElement.SelectNodes("/Documento/DocPDFBase64");
            XmlNodeList nodesXmls = xml.DocumentElement.SelectNodes("/Documento/DocXMLBase64");
            foreach (XmlNode node in nodes)
            {
                int numeroNfe = Convert.ToInt32(notaDetalhe.numeroNota);
                var notaDc = new dbCommerceDataContext();
                var notaAlterar = (from c in notaDc.tbNotaFiscals where c.numeroNota == numeroNfe && c.idNotaFiscal == notaDetalhe.idNotaFiscal select c).First();
                notaAlterar.ultimoStatus = node.InnerText;
                notaDc.SubmitChanges();
                string pdfBase64 = "";
                string xmlCompleto = "";
                foreach (XmlNode pdfBase in nodesPdfBase64)
                {
                    if (!string.IsNullOrEmpty(pdfBase.InnerText))
                    {
                        pdfBase64 = pdfBase.InnerText;
                    }
                }
                foreach (XmlNode nodeXml in nodesXmls)
                {
                    if (!string.IsNullOrEmpty(nodeXml.InnerText))
                    {
                        xmlCompleto = nodeXml.InnerText;
                    }
                }

                if (!string.IsNullOrEmpty(pdfBase64))
                {
                    byte[] binaryData;
                    binaryData = Convert.FromBase64String(pdfBase64);
                    System.IO.FileStream outFile = new FileStream("C:\\inetpub\\wwwroot\\admin\\notas\\danfes\\" + notaDetalhe.idCNPJ.ToString() + "_" + notaDetalhe.numeroNota + ".pdf", System.IO.FileMode.Create, System.IO.FileAccess.Write);
                    outFile.Write(binaryData, 0, binaryData.Length);
                    outFile.Close();

                    string url = System.Configuration.ConfigurationManager.AppSettings["caminhoVirtualAdmin"] + "notas/danfes/" + notaDetalhe.idCNPJ.ToString() + "_" + notaDetalhe.numeroNota + ".pdf";
                    notaAlterar.linkDanfe = url;
                    notaAlterar.statusNfe = 2;
                    notaAlterar.danfeBase64 = pdfBase64;
                    notaAlterar.xmlBase64 = xmlCompleto;
                    notaDc.SubmitChanges();
                    if ((notaAlterar.idPedidoEnvio ?? 0) != 0)
                    {
                        var pedidosDc = new dbCommerceDataContext();
                        var pedidoEnvio = (from c in pedidosDc.tbPedidoEnvios where c.idPedidoEnvio == idPedidoEnvio select c).First();
                        pedidoEnvio.nfeStatus = 2;

                        if (pedidoEnvio.formaDeEnvio == "plimor")
                        {
                            var queueNota = new tbQueue();
                            queueNota.agendamento = DateTime.Now;
                            queueNota.idRelacionado = notaAlterar.numeroNota;
                            queueNota.tipoQueue = 41;
                            queueNota.concluido = false;
                            queueNota.andamento = false;
                            queueNota.mensagem = xmlCompleto;
                            data.tbQueues.InsertOnSubmit(queueNota);
                            data.SubmitChanges();
                        }
                        pedidosDc.SubmitChanges();
                    }
                }
                atualizaDataReceitaFederal(numeroNfe, idNotaFiscal, 0);
            }
        }
        catch (Exception ex)
        {
            string erro = ex.Message;
        }
    }
    public static string enviarCartaCorrecao(int idNotaFiscal, string cartaDeCorrecao)
    {
        var db = new dbCommerceDataContext();
        var notaEmitida = (from c in db.tbNotaFiscals where c.idNotaFiscal == idNotaFiscal select c).First();
        var empresaNota = (from c in db.tbEmpresas where c.idEmpresa == notaEmitida.idCNPJ select c).First();

        var notas = new List<serviceNfe.InvoiCyRecepcaoDadosItem>();
        var serviceRecepcao = new serviceNfe.InvoiCy();
        var cabecalho = new serviceNfe.InvoiCyRecepcaoCabecalho();

        cabecalho.EmpPK = empresaNota.EmpPK;
        serviceRecepcao.Cabecalho = cabecalho;

        string dataEvent = string.Format("{0}T{1}", DateTime.Now.ToString("yyyy-MM-dd"), DateTime.Now.ToString("HH:mm:ss"));

        string chaveCompleta = notaEmitida.nfeKey + GerarDigitoVerificadorNFe(notaEmitida.nfeKey);

        var nota = new serviceNfe.InvoiCyRecepcaoDadosItem();
        int sequenciaCartaCorrecao = retornaSequenciaCartaCorrecao(notaEmitida.numeroNota, empresaNota.cnpj, empresaNota.EmpPK, empresaNota.chaveDeAcesso);
        string documentoRequisicao = "<EnvioEvento><ModeloDocumento>NFe</ModeloDocumento><Versao>1.0</Versao><Evento><NtfCnpjEmissor>" + empresaNota.cnpj + "</NtfCnpjEmissor><NtfNumero>" + notaEmitida.numeroNota + "</NtfNumero><NtfSerie>1</NtfSerie><tpAmb>1</tpAmb><ChaAcesso>" + notaEmitida.nfeKey + GerarDigitoVerificadorNFe(notaEmitida.nfeKey) + "</ChaAcesso><EveInf><EveDh>" + dataEvent + "</EveDh><EveFusoHorario>-03:00</EveFusoHorario><EveTp>110110</EveTp><EvenSeq>" + sequenciaCartaCorrecao + "</EvenSeq><Evedet><EveDesc>Carta de Correcao</EveDesc><EveCorrecao>" + cartaDeCorrecao.Trim().Replace("|", "-") + "</EveCorrecao><EveCondUso>A Carta de Correcao e disciplinada pelo paragrafo 1o-A do art. 7o do Convenio S/N, de 15 de dezembro de 1970 e pode ser utilizada para regularizacao de erro ocorrido na emissao de documento fiscal, desde que o erro nao esteja relacionado com: I - as variaveis que determinam o valor do imposto tais como: base de calculo, aliquota, diferenca de preco, quantidade, valor da operacao ou da prestacao; II - a correcao de dados cadastrais que implique mudanca do remetente ou do destinatario; III - a data de emissao ou de saida.</EveCondUso></Evedet></EveInf></Evento></EnvioEvento>";
        nota.Documento = documentoRequisicao;

        string valorParaHash = empresaNota.chaveDeAcesso + documentoRequisicao;
        cabecalho.EmpCK = rnFuncoes.gerarMd5(valorParaHash);


        notas.Add(nota);
        //gerarNotaFiscalEnvio
        serviceRecepcao.Dados = notas.ToArray();

        var nfe = new serviceNfe.recepcao();
        var retornoNota = nfe.Execute(serviceRecepcao);
        return retornoNota.Mensagem[0].Descricao;
    }

    public static int retornaSequenciaCartaCorrecao(int numeroNota, string cnpj, string EmpPK, string chaveDeAcesso)
    {
        int sequencia = 1;
        var notas = new List<serviceNfe.InvoiCyRecepcaoDadosItem>();
        var serviceRecepcao = new serviceNfe.InvoiCy();
        var cabecalho = new serviceNfe.InvoiCyRecepcaoCabecalho();

        cabecalho.EmpPK = EmpPK;
        serviceRecepcao.Cabecalho = cabecalho;

        var nota = new serviceNfe.InvoiCyRecepcaoDadosItem();
        string documentoRequisicao = "<Consulta><ModeloDocumento>NFe</ModeloDocumento><Versao>3.10</Versao><tpAmb>1</tpAmb><CnpjEmissor>" + cnpj + "</CnpjEmissor><NumeroInicial>" + numeroNota + "</NumeroInicial><NumeroFinal>" + numeroNota + "</NumeroFinal><Serie>1</Serie><ChaveAcesso/><DataEmissaoInicial/><DataEmissaoFinal/><ParametrosConsulta><Situacao>S</Situacao><XMLCompleto>S</XMLCompleto><XMLLink>S</XMLLink><PDFBase64>S</PDFBase64><PDFLink>S</PDFLink><Eventos>S</Eventos></ParametrosConsulta></Consulta>";
        string parametrosRequisicao = "<ParametrosConsulta><Eventos>S</Eventos></ParametrosConsulta>";

        nota.Documento = documentoRequisicao;
        nota.Parametros = parametrosRequisicao;
        string valorParaHash = chaveDeAcesso + documentoRequisicao;
        cabecalho.EmpCK = rnFuncoes.gerarMd5(valorParaHash);
        notas.Add(nota);
        serviceRecepcao.Dados = notas.ToArray();
        var nfe = new serviceNfe.recepcao();
        var retornoNota = nfe.Execute(serviceRecepcao);

        try
        {
            XmlDocument xml = new XmlDocument();
            xml.LoadXml(retornoNota.Mensagem[0].Documentos[0].Documento);
            XmlNodeList nodesEventos = xml.DocumentElement.SelectNodes("/Documento/Eventos/EventosItem");
            foreach (XmlNode evento in nodesEventos)
            {
                XmlNode eventoTipo = evento.SelectSingleNode("DocEveTp");
                string tipo = eventoTipo.InnerText;
                if (tipo == "110110")
                {
                    sequencia += Convert.ToInt32(evento.SelectSingleNode("DocEvenSeq").InnerText);

                }
            }
        }
        catch (Exception ex)
        {
            string erro = ex.Message;
        }

        return sequencia;
    }
    public static string inutilizarNotaMayara(int nfeNumero)
    {
        return inutilizarNotaMayara(nfeNumero, nfeNumero);
    }

    public static string inutilizarNotaMayara(int nfeNumeroInicial, int nfeNumeroFinal)
    {
        string cnpjEmitenteMayara = "20907518000110";
        string chaveDeAcessoMayara = "9c5gKp3l+8aUBnL8QzwRkKaEXKqFxO3r";
        string EmpPKMayara = "6rRJmEhQmdMKCIcVwD22vw==";

        var db = new dbCommerceDataContext();
        var notas = new List<serviceNfe.InvoiCyRecepcaoDadosItem>();
        var serviceRecepcao = new serviceNfe.InvoiCy();
        var cabecalho = new serviceNfe.InvoiCyRecepcaoCabecalho();

        cabecalho.EmpPK = EmpPKMayara;
        serviceRecepcao.Cabecalho = cabecalho;

        string dataEvent = string.Format("{0}T{1}", DateTime.Now.ToString("yyyy-MM-dd"), DateTime.Now.ToString("HH:mm:ss"));


        var nota = new serviceNfe.InvoiCyRecepcaoDadosItem();
        string documentoRequisicao = "<Inutilizacao>" +
                                     "<ModeloDocumento>NFe</ModeloDocumento>" +
                                     "<Versao>3.1</Versao>" +
                                     "<CnpjEmissor>" + cnpjEmitenteMayara + "</CnpjEmissor>" +
                                     "<tpAmb>1</tpAmb>" +
                                     "<NumeroInicial>" + nfeNumeroInicial + "</NumeroInicial>" +
                                     "<NumeroFinal>" + nfeNumeroFinal + "</NumeroFinal>" +
                                     "<Serie>1</Serie>" +
                                     "<Justificativa>Inutilizacao por motivos de pulo de numeracao</Justificativa>" +
                                     "</Inutilizacao>";
        nota.Documento = documentoRequisicao;

        string valorParaHash = chaveDeAcessoMayara + documentoRequisicao;
        cabecalho.EmpCK = rnFuncoes.gerarMd5(valorParaHash);


        notas.Add(nota);
        serviceRecepcao.Dados = notas.ToArray();

        var nfe = new serviceNfe.recepcao();
        var retornoNota = nfe.Execute(serviceRecepcao);
        return retornoNota.Mensagem[0].Descricao;
    }

    public static string inutilizarNota(int nfeNumeroInicial, int nfeNumeroFinal, string cnpjEmitente)
    {

        var db = new dbCommerceDataContext();
        var empresaNota = (from c in db.tbEmpresas where c.cnpj == cnpjEmitente select c).First();
        string chaveDeAcesso = empresaNota.chaveDeAcesso;
        string EmpPk = empresaNota.EmpPK;


        var notas = new List<serviceNfe.InvoiCyRecepcaoDadosItem>();
        var serviceRecepcao = new serviceNfe.InvoiCy();
        var cabecalho = new serviceNfe.InvoiCyRecepcaoCabecalho();

        cabecalho.EmpPK = EmpPk;
        serviceRecepcao.Cabecalho = cabecalho;

        string dataEvent = string.Format("{0}T{1}", DateTime.Now.ToString("yyyy-MM-dd"), DateTime.Now.ToString("HH:mm:ss"));


        var nota = new serviceNfe.InvoiCyRecepcaoDadosItem();
        string documentoRequisicao = "<Inutilizacao>" +
                                     "<ModeloDocumento>NFe</ModeloDocumento>" +
                                     "<Versao>3.1</Versao>" +
                                     "<CnpjEmissor>" + cnpjEmitente + "</CnpjEmissor>" +
                                     "<tpAmb>1</tpAmb>" +
                                     "<NumeroInicial>" + nfeNumeroInicial + "</NumeroInicial>" +
                                     "<NumeroFinal>" + nfeNumeroFinal + "</NumeroFinal>" +
                                     "<Serie>1</Serie>" +
                                     "<Justificativa>Inutilizacao por motivos de pulo de numeracao</Justificativa>" +
                                     "</Inutilizacao>";
        nota.Documento = documentoRequisicao;

        string valorParaHash = chaveDeAcesso + documentoRequisicao;
        cabecalho.EmpCK = rnFuncoes.gerarMd5(valorParaHash);


        notas.Add(nota);
        serviceRecepcao.Dados = notas.ToArray();

        var nfe = new serviceNfe.recepcao();
        var retornoNota = nfe.Execute(serviceRecepcao);


        for (int i = nfeNumeroInicial; i <= nfeNumeroFinal; i++)
        {
            atualizaDataReceitaFederal(i, 0, empresaNota.idEmpresa);
        }
        return retornoNota.Mensagem[0].Descricao;
        // return "";
    }
    public static string cancelarNotaLindsay(int nfeNumero, string sequencia, string motivo, int idNotaFiscal)
    {
        var db = new dbCommerceDataContext();
        var notaEmitida = (from c in db.tbNotaFiscals where c.numeroNota == nfeNumero && c.idNotaFiscal == idNotaFiscal select c).First();
        var empresaNota = (from c in db.tbEmpresas where c.idEmpresa == notaEmitida.idCNPJ select c).First();

        var notas = new List<serviceNfe.InvoiCyRecepcaoDadosItem>();
        var serviceRecepcao = new serviceNfe.InvoiCy();
        var cabecalho = new serviceNfe.InvoiCyRecepcaoCabecalho();

        cabecalho.EmpPK = empresaNota.EmpPK;
        serviceRecepcao.Cabecalho = cabecalho;

        string dataEvent = string.Format("{0}T{1}", DateTime.Now.ToString("yyyy-MM-dd"), DateTime.Now.ToString("HH:mm:ss"));

        string chaveCompleta = notaEmitida.nfeKey + GerarDigitoVerificadorNFe(notaEmitida.nfeKey);

        var nota = new serviceNfe.InvoiCyRecepcaoDadosItem();
        string documentoRequisicao = "<EnvioEvento>" +
                                     "<ModeloDocumento>NFe</ModeloDocumento>" +
                                     "<Versao>1.0</Versao><Evento>" +
                                     "<NtfCnpjEmissor>" + empresaNota.cnpj + "</NtfCnpjEmissor><NtfNumero>" + nfeNumero + "</NtfNumero>" +
                                     "<NtfSerie>1</NtfSerie><tpAmb>1</tpAmb>" +
                                     "<ChaAcesso>" + notaEmitida.nfeKey + GerarDigitoVerificadorNFe(notaEmitida.nfeKey) + "</ChaAcesso>" +
                                     "<EveInf><EveDh>" + dataEvent + "</EveDh><EveFusoHorario>-03:00</EveFusoHorario>" +
                                     "<EveTp>110111</EveTp>" +
                                     "<EvenSeq>1</EvenSeq>" +
                                     "<Evedet><EveDesc>Cancelamento</EveDesc>" +
                                     "<EvexJust>" + sequencia + "</EvexJust>" +
                                     "</Evedet></EveInf></Evento></EnvioEvento>";
        nota.Documento = documentoRequisicao;

        //return documentoRequisicao;
        string valorParaHash = empresaNota.chaveDeAcesso + documentoRequisicao;
        cabecalho.EmpCK = rnFuncoes.gerarMd5(valorParaHash);


        notas.Add(nota);
        //gerarNotaFiscalEnvio
        serviceRecepcao.Dados = notas.ToArray();

        var nfe = new serviceNfe.recepcao();
        var retornoNota = nfe.Execute(serviceRecepcao);
        return retornoNota.Mensagem[0].Descricao;
    }
    public static string retornaChaveComDigito(int nfeNumero, string cartaDeCorrecao)
    {
        var db = new dbCommerceDataContext();
        var notaEmitida = (from c in db.tbNotaFiscals where c.numeroNota == nfeNumero select c).First();

        var notas = new List<serviceNfe.InvoiCyRecepcaoDadosItem>();
        var serviceRecepcao = new serviceNfe.InvoiCy();
        var cabecalho = new serviceNfe.InvoiCyRecepcaoCabecalho();

        return notaEmitida.nfeKey + GerarDigitoVerificadorNFe(notaEmitida.nfeKey);
    }

    public static void emiteNotasCompletas()
    {
        emiteNotasCompletas(0);
    }

    public static void emiteNotasCompletas(int idPedidoEnvio)
    {
        var data = new dbCommerceDataContext();
        var pedidosNota =
            (from c in data.tbPedidoEnvios
             where (c.nfeObrigatoria ?? false) == true && (c.nfeStatus == null) && (c.formaDeEnvio ?? "") != ""
             select c);
        if (idPedidoEnvio > 0) pedidosNota = pedidosNota.Where(x => x.idPedidoEnvio == idPedidoEnvio);

        foreach (var pedidoNota in pedidosNota)
        {
            var pacotesNaoCompletos = (from c in data.tbPedidoPacotes
                                       where c.idPedidoEnvio == pedidoNota.idPedidoEnvio && ((c.concluido ?? false) == false | c.formaDeEnvio == "")
                                       select c).Any();
            if (!pacotesNaoCompletos)
            {
                string codIbge = "";
                bool ncm = true;

                List<int> produtosIds;

                using (var txn = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required, new System.Transactions.TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted }))
                {
                    produtosIds =
                       (from c in data.tbProdutoEstoques
                        where c.idPedidoEnvio == pedidoNota.idPedidoEnvio
                        select c.produtoId).ToList();
                }

                var produtos = (from c in data.tbProdutos where produtosIds.Contains(c.produtoId) select c);
                foreach (var produto in produtos)
                {
                    var produtosFilho =
                        (from c in data.tbProdutoRelacionados where c.idProdutoPai == produto.produtoId select c);
                    if (produtosFilho.Any())
                    {
                        foreach (var produtoRelacionado in produtosFilho)
                        {
                            if (string.IsNullOrEmpty(produtoRelacionado.tbProduto.ncm)) ncm = false;
                        }
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(produto.ncm)) ncm = false;
                    }
                }

                var cepsDc = new dbCommerceDataContext();
                try
                {
                    string cepOut = pedidoNota.tbPedido.endCep.Replace("-", "");
                    var endereco =
                        (from c in cepsDc.tbCepEnderecos where c.cep == cepOut.Replace("-", "") select c).FirstOrDefault
                            ();
                    if (endereco != null)
                    {
                        var cidade =
                            (from c in cepsDc.tbCepCidades where c.id_cidade == endereco.id_cidade select c)
                                .FirstOrDefault();
                        if (cidade != null)
                        {
                            codIbge = cidade.cod_ibge;
                        }
                    }

                    if (codIbge == "0") codIbge = "";
                    if (string.IsNullOrEmpty(codIbge))
                    {
                        string nomeCidade =
                            pedidoNota.tbPedido.endCidade.Replace("´", "'").Replace("`", "'").ToLower().Trim();
                        var cidade = (from c in cepsDc.tbCepCidades
                                      where
                                          c.cidade.ToLower().Trim() == nomeCidade &&
                                          c.uf.ToLower() == pedidoNota.tbPedido.endEstado.ToLower()
                                      select c).FirstOrDefault();
                        if (cidade != null)
                        {
                            codIbge = cidade.cod_ibge;
                        }
                    }

                    if (codIbge == "0") codIbge = "";
                    if (string.IsNullOrEmpty(codIbge))
                    {
                        string nomeCidade =
                            pedidoNota.tbPedido.endCidade.Replace("´", "'").Replace("`", "'").ToLower().Trim();
                        var nomeCidadeSplit = nomeCidade.Split('(');
                        if (nomeCidadeSplit.Count() > 1)
                        {
                            string nomeParte2 = nomeCidadeSplit[1];
                            var nomeCidadeSplit2 = nomeParte2.Split(')')[0];
                            var cidade = (from c in cepsDc.tbCepCidades
                                          where
                                              c.cidade.ToLower().Trim() == nomeCidadeSplit2 &&
                                              c.uf.ToLower() == pedidoNota.tbPedido.endEstado.ToLower()
                                          select c).FirstOrDefault();
                            if (cidade != null)
                            {
                                codIbge = cidade.cod_ibge;
                            }
                        }
                    }
                }
                catch (Exception)
                {

                }

                if (ncm == true && !string.IsNullOrEmpty(codIbge))
                {
                    var xmlNotaFiscal = gerarNotaFiscalEnvio(pedidoNota.idPedidoEnvio, codIbge);
                    autorizarNota(xmlNotaFiscal.xml, pedidoNota.idPedidoEnvio, xmlNotaFiscal.idNotaFiscal);
                }
                else
                {
                    pedidoNota.nfeStatus = 0;
                    data.SubmitChanges();
                }
            }
        }
    }
    public static void emiteNotasCompletasGnre()
    {
        var data = new dbCommerceDataContext();
        var pedidosNota =
            (from c in data.tbNotaFiscals
             where c.statusNfe == 0
             select c);
        foreach (var pedidoNota in pedidosNota)
        {
            var pedidoDetalhe =
                (from c in data.tbPedidos where c.pedidoId == pedidoNota.idPedido select c).FirstOrDefault();
            if (pedidoDetalhe != null)
            {
                string codIbge = "";
                bool ncm = true;
                var produtosIds =
                    (from c in data.tbItemPedidoEstoques
                     where c.tbItensPedido.pedidoId == pedidoNota.idPedido
                     select c.produtoId).ToList();
                var produtos = (from c in data.tbProdutos where produtosIds.Contains(c.produtoId) select c);
                foreach (var produto in produtos)
                {
                    var produtosFilho =
                        (from c in data.tbProdutoRelacionados where c.idProdutoPai == produto.produtoId select c);
                    if (produtosFilho.Any())
                    {
                        foreach (var produtoRelacionado in produtosFilho)
                        {
                            if (string.IsNullOrEmpty(produtoRelacionado.tbProduto.ncm)) ncm = false;
                        }
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(produto.ncm)) ncm = false;
                    }
                }

                var cepsDc = new dbCommerceDataContext();
                try
                {
                    string cepOut = pedidoDetalhe.endCep.Replace("-", "");
                    var endereco =
                        (from c in cepsDc.tbCepEnderecos where c.cep == cepOut.Replace("-", "") select c).FirstOrDefault
                            ();
                    if (endereco != null)
                    {
                        var cidade =
                            (from c in cepsDc.tbCepCidades where c.id_cidade == endereco.id_cidade select c)
                                .FirstOrDefault();
                        if (cidade != null)
                        {
                            codIbge = cidade.cod_ibge;
                        }
                    }

                    if (codIbge == "0") codIbge = "";
                    if (string.IsNullOrEmpty(codIbge))
                    {
                        string nomeCidade =
                            pedidoDetalhe.endCidade.Replace("´", "'").Replace("`", "'").ToLower().Trim();
                        var cidade = (from c in cepsDc.tbCepCidades
                                      where
                                          c.cidade.ToLower().Trim() == nomeCidade &&
                                          c.uf.ToLower() == pedidoDetalhe.endEstado.ToLower()
                                      select c).FirstOrDefault();
                        if (cidade != null)
                        {
                            codIbge = cidade.cod_ibge;
                        }
                    }

                    if (codIbge == "0") codIbge = "";
                    if (string.IsNullOrEmpty(codIbge))
                    {
                        string nomeCidade =
                            pedidoDetalhe.endCidade.Replace("´", "'").Replace("`", "'").ToLower().Trim();
                        var nomeCidadeSplit = nomeCidade.Split('(');
                        if (nomeCidadeSplit.Count() > 1)
                        {
                            string nomeParte2 = nomeCidadeSplit[1];
                            var nomeCidadeSplit2 = nomeParte2.Split(')')[0];
                            var cidade = (from c in cepsDc.tbCepCidades
                                          where
                                              c.cidade.ToLower().Trim() == nomeCidadeSplit2 &&
                                              c.uf.ToLower() == pedidoDetalhe.endEstado.ToLower()
                                          select c).FirstOrDefault();
                            if (cidade != null)
                            {
                                codIbge = cidade.cod_ibge;
                            }
                        }
                    }
                }
                catch (Exception)
                {

                }

                if (ncm == true && !string.IsNullOrEmpty(codIbge))
                {
                    var xmlNotaFiscal = gerarNotaFiscal(pedidoDetalhe.pedidoId, codIbge, new List<int>(), false);
                    autorizarNota(xmlNotaFiscal.xml, 0, xmlNotaFiscal.idNotaFiscal);
                }
                else
                {
                    pedidoNota.statusNfe = -1;
                    data.SubmitChanges();
                }
            }
        }
    }

    public static string base64Decode(string data)
    {
        try
        {
            System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();
            System.Text.Decoder utf8Decode = encoder.GetDecoder();

            byte[] todecode_byte = Convert.FromBase64String(data);
            int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
            char[] decoded_char = new char[charCount];
            utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
            string result = new String(decoded_char);
            return result;
        }
        catch (Exception e)
        {
            throw new Exception("Error in base64Decode" + e.Message);
        }
    }

    public static DiferencialAliquota CalculaDiferencialAliquota(string estadoDestino, decimal valor, DateTime dataEmissao)
    {
        return CalculaDiferencialAliquota("sp", estadoDestino, valor, dataEmissao);
    }
    public static DiferencialAliquota CalculaDiferencialAliquota(string estadoOrigem, string estadoDestino, decimal valor, DateTime dataEmissao)
    {
        decimal aliquotaInterestadual = 7;
        decimal aliquotaInterna = 17;
        if (estadoOrigem == "sp")
        {
            switch (estadoDestino.ToLower())
            {
                case "ac":
                    aliquotaInterna = 17;
                    break;
                case "al":
                    aliquotaInterna = 17;
                    break;
                case "am":
                    aliquotaInterna = 18;
                    break;
                case "ap":
                    aliquotaInterna = 18;
                    break;
                case "ba":
                    aliquotaInterna = 18;
                    break;
                case "ce":
                    aliquotaInterna = 17;
                    break;
                case "df":
                    aliquotaInterna = 18;
                    break;
                case "es":
                    aliquotaInterna = 17;
                    aliquotaInterestadual = 7;
                    break;
                case "go":
                    aliquotaInterna = 17;
                    break;
                case "ma":
                    aliquotaInterna = 18;
                    break;
                case "mt":
                    aliquotaInterna = 17;
                    break;
                case "ms":
                    aliquotaInterna = 17;
                    break;
                case "mg":
                    aliquotaInterna = 18;
                    aliquotaInterestadual = 12;
                    break;
                case "pa":
                    aliquotaInterna = 17;
                    break;
                case "pb":
                    aliquotaInterna = 18;
                    break;
                case "pr":
                    aliquotaInterna = 18;
                    aliquotaInterestadual = 12;
                    break;
                case "pe":
                    aliquotaInterna = 18;
                    break;
                case "pi":
                    aliquotaInterna = 17;
                    break;
                case "rn":
                    aliquotaInterna = 18;
                    break;
                case "rs":
                    aliquotaInterna = 18;
                    aliquotaInterestadual = 12;
                    break;
                case "rj":
                    aliquotaInterna = 18;
                    aliquotaInterestadual = 12;
                    break;
                case "ro":
                    aliquotaInterna = 17;
                    break;
                case "rr":
                    aliquotaInterna = 17;
                    break;
                case "sc":
                    aliquotaInterna = 17;
                    aliquotaInterestadual = 12;
                    break;
                case "sp":
                    aliquotaInterna = 18;
                    aliquotaInterestadual = 12;
                    break;
                case "se":
                    aliquotaInterna = 18;
                    break;
                case "to":
                    aliquotaInterna = 18;
                    break;
            }
        }
        if (estadoOrigem == "mg")
        {
            switch (estadoDestino.ToLower())
            {
                case "ac":
                    aliquotaInterna = 17;
                    break;
                case "al":
                    aliquotaInterna = 18;
                    break;
                case "am":
                    aliquotaInterna = 18;
                    break;
                case "ap":
                    aliquotaInterna = 18;
                    break;
                case "ba":
                    aliquotaInterna = 18;
                    break;
                case "ce":
                    aliquotaInterna = 18;
                    break;
                case "df":
                    aliquotaInterna = 18;
                    break;
                case "es":
                    aliquotaInterna = 17;
                    aliquotaInterestadual = 7;
                    break;
                case "go":
                    aliquotaInterna = 17;
                    break;
                case "ma":
                    aliquotaInterna = 18;
                    break;
                case "mt":
                    aliquotaInterna = 17;
                    break;
                case "ms":
                    aliquotaInterna = 17;
                    break;
                case "mg":
                    aliquotaInterna = 18;
                    aliquotaInterestadual = 12;
                    break;
                case "pa":
                    aliquotaInterna = 17;
                    break;
                case "pb":
                    aliquotaInterna = 18;
                    break;
                case "pr":
                    aliquotaInterna = 18;
                    aliquotaInterestadual = 12;
                    break;
                case "pe":
                    aliquotaInterna = 18;
                    break;
                case "pi":
                    aliquotaInterna = 18;
                    break;
                case "rn":
                    aliquotaInterna = 18;
                    break;
                case "rs":
                    aliquotaInterna = 18;
                    aliquotaInterestadual = 12;
                    break;
                case "rj":
                    aliquotaInterna = 20;
                    aliquotaInterestadual = 12;
                    break;
                case "ro":
                    aliquotaInterna = Convert.ToDecimal("17,5");
                    break;
                case "rr":
                    aliquotaInterna = 17;
                    break;
                case "sc":
                    aliquotaInterna = 17;
                    aliquotaInterestadual = 12;
                    break;
                case "sp":
                    aliquotaInterna = 18;
                    aliquotaInterestadual = 12;
                    break;
                case "se":
                    aliquotaInterna = 18;
                    break;
                case "to":
                    aliquotaInterna = 18;
                    break;
            }
        }

        decimal partilhaDestino = 40;
        if (dataEmissao.Year == 2017) partilhaDestino = 60;
        if (dataEmissao.Year == 2018) partilhaDestino = 80;

        decimal diferencialAliquota = aliquotaInterna - aliquotaInterestadual;
        decimal valorDiferencial = (valor / 100) * diferencialAliquota;

        var retorno = new DiferencialAliquota();
        retorno.totalDiferencial = valorDiferencial;
        retorno.diferencialOrigem = Convert.ToDecimal(((valorDiferencial / 100) * (100 - partilhaDestino)).ToString("0.00"));
        retorno.diferencialDestino = Convert.ToDecimal(((valorDiferencial / 100) * (partilhaDestino)).ToString("0.00"));

        retorno.icmsOrigem = aliquotaInterestadual;
        retorno.icmsDestino = aliquotaInterna;
        retorno.partilhaDestino = partilhaDestino;

        return retorno;
    }

    public class DiferencialAliquota
    {
        public decimal icmsOrigem { get; set; }
        public decimal icmsDestino { get; set; }
        public decimal totalDiferencial { get; set; }
        public decimal diferencialOrigem { get; set; }
        public decimal diferencialDestino { get; set; }
        public decimal partilhaDestino { get; set; }
    }


    public static bool DefineGerarGnre(int pedidoId, string estado)
    {
        var data = new dbCommerceDataContext();
        var configuracaoEstado =
            (from c in data.tbGnreConfiguracaos where c.uf == estado.ToLower() select c).FirstOrDefault();
        if (configuracaoEstado == null) return false;

        if (configuracaoEstado.gerarGnre == true)
        {
            return true;
        }
        return false;
    }






    public static tbNotaFiscal gerarNumeroNotaRomaneioEmpresa(int idPedidoFornecedor, int idEmpresa)
    {
        var notaDc = new dbCommerceDataContext();
        var notas = (from c in notaDc.tbNotaFiscals where c.idPedidoFornecedor == idPedidoFornecedor && c.statusNfe != 2 && c.idPedido == 0 select c);
        var nota = notas.FirstOrDefault();
        if (nota != null)
        {
            nota.ultimaChecagem = DateTime.Now.AddSeconds(-270);
            notaDc.SubmitChanges();
            return nota;
        }
        else
        {
            #region Define CNPJ de Emissão

            var empresaSelecionada = (from c in notaDc.tbEmpresas where c.idEmpresa == idEmpresa select c).First();
            #endregion

            int numero = 1;
            var numeroNota = (from c in notaDc.tbNotaFiscals where c.idCNPJ == idEmpresa orderby c.numeroNota descending select c).FirstOrDefault();
            if (numeroNota != null)
            {
                numero = (numeroNota.numeroNota + 1);
            }
            var notaNova = new tbNotaFiscal
            {
                idPedido = 0,
                numeroNota = numero,
                dataHora = DateTime.Now,
                idPedidoFornecedor = idPedidoFornecedor,
                ultimaChecagem = DateTime.Now.AddMinutes(-270),
                ultimoStatus = "Aguardando Processamento",
                idCNPJ = idEmpresa
            };

            string chave = "";

            try
            {
                chave += empresaSelecionada.codigoUfEmitente;
                chave += DateTime.Now.Year.ToString().Substring(2, 2);
                chave += DateTime.Now.Month.ToString().PadLeft(2, '0');
                chave += empresaSelecionada.cnpj;
                chave += "55";
                chave += "001";
                chave += numero.ToString().PadLeft(9, '0');
                chave += "1";
                chave += idPedidoFornecedor.ToString().PadLeft(8, '0');

                notaNova.nfeKey = chave;

                notaDc.tbNotaFiscals.InsertOnSubmit(notaNova);
                notaDc.SubmitChanges();
            }
            catch (Exception ioEx)
            {
                if (ioEx.Message.IndexOf("nota-duplicada") > -1)
                {
                    #region Validar Nota  

                    bool notaValida = false;
                    while (!notaValida)
                    {

                        using (var notaDcDuplicada = new dbCommerceDataContext())
                        {
                            numeroNota = (from c in notaDcDuplicada.tbNotaFiscals
                                          where c.idCNPJ == idEmpresa
                                          orderby c.numeroNota descending
                                          select c).FirstOrDefault();
                            if (numeroNota != null)
                            {
                                numero = (numeroNota.numeroNota + 1);
                            }

                            var notaNovaValida = new tbNotaFiscal
                            {
                                idPedido = 0,
                                numeroNota = numero,
                                dataHora = DateTime.Now,
                                idPedidoFornecedor = idPedidoFornecedor,
                                ultimaChecagem = DateTime.Now.AddMinutes(-270),
                                ultimoStatus = "Aguardando Processamento",
                                idCNPJ = idEmpresa
                            };

                            chave = "";
                            chave += empresaSelecionada.codigoUfEmitente;
                            chave += DateTime.Now.Year.ToString().Substring(2, 2);
                            chave += DateTime.Now.Month.ToString().PadLeft(2, '0');
                            chave += empresaSelecionada.cnpj;
                            chave += "55";
                            chave += "001";
                            chave += numero.ToString().PadLeft(9, '0');
                            chave += "1";
                            chave += idPedidoFornecedor.ToString().PadLeft(8, '0');

                            notaNovaValida.nfeKey = chave;

                            try
                            {
                                notaDcDuplicada.tbNotaFiscals.InsertOnSubmit(notaNovaValida);
                                notaDcDuplicada.SubmitChanges();
                                notaValida = true;
                                notaNova = notaNovaValida;
                            }
                            catch (Exception)
                            {
                                notaValida = false;
                            }
                        }
                    }

                    #endregion validar  
                }
            }


            return notaNova;
        }
    }
    public static notaGerada GerarNotaFiscalRomaneioFabrica(int idPedidoFornecedor)
    {

        string cfop = "";
        var data = new dbCommerceDataContext();
        var jacutinga = (from c in data.tbPedidoFornecedorItems
                         join d in data.tbTransferenciaEnderecoProdutos on c.idPedidoFornecedorItem equals d.idPedidoFornecedorItem
                         where c.idPedidoFornecedor == idPedidoFornecedor && d.tbTransferenciaEndereco.tbEnderecamentoTransferencia.idCentroDistribuicao == 4
                         orderby c.dataEntrega descending
                         select c).Count() > 0;
        //if (romaneioItem == null) return new notaGerada();

        var notaGerada = gerarNumeroNotaRomaneioEmpresa(idPedidoFornecedor, 2);
        var empresaNota = (from c in data.tbEmpresas where c.idEmpresa == notaGerada.idCNPJ select c).First();
        var empresaDestino = (from c in data.tbEmpresas where c.idEmpresa == 3 select c).First();
        var totalNotasGeradas = (from c in data.tbNotaFiscals where c.idCNPJ == 2 && c.dataHora.Month == DateTime.Now.Month && c.dataHora.Year == DateTime.Now.Year select c).Sum(x => x.valor) ?? 0;
        var limiteValorDia = empresaNota.limite / DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month);
        var totalLimiteAtual = limiteValorDia * DateTime.Now.Day;
        var limiteNotaAtual = totalLimiteAtual - totalNotasGeradas;
        if (limiteNotaAtual < 0) limiteNotaAtual = 0;

        if (jacutinga)
        {
            limiteNotaAtual = 99999999;
            empresaDestino = (from c in data.tbEmpresas where c.idEmpresa == 6 select c).First();
        }

        string numeroNota = notaGerada.numeroNota.ToString();
        string chave = notaGerada.nfeKey;
        string cDV = GerarDigitoVerificadorNFe(chave).ToString();
        chave += cDV;

        string idDest = "";



        #region validações para o endereço
        string cpfCnpj = empresaDestino.cnpj;

        var validaCodigoIbge = empresaDestino.codigoMunicipioEmitente;

        string uf = empresaDestino.ufEmitente;
        string bairro = empresaDestino.bairroEmitente;

        cfop = "5101";
        idDest = "1";
        if (jacutinga)
        {
            cfop = "6101";
            idDest = "2";
        }
        #endregion validações para o endereço



        string naturezaOperacao = "Venda de Mercadoria";

        var notaAlterar = (from c in data.tbNotaFiscals where c.idNotaFiscal == notaGerada.idNotaFiscal select c).First();
        notaAlterar.naturezaOperacao = naturezaOperacao;

        var nota = new StringBuilder();

        #region Identificadores da NF-e
        nota.Append("<ide>");
        nota.AppendFormat("<cUF>{0}</cUF>", empresaNota.codigoUfEmitente);
        nota.AppendFormat("<cNF>{0}</cNF>", idPedidoFornecedor.ToString().PadLeft(8, '0'));
        nota.AppendFormat("<natOp>{0}</natOp>", naturezaOperacao);


        //nota.Append("<natOp>VENDA DE MERCADORIA</natOp>");
        nota.Append("<indPag>0</indPag>");
        nota.Append("<mod>55</mod>");
        nota.Append("<serie>1</serie>");
        nota.AppendFormat("<nNF>{0}</nNF>", numeroNota);
        nota.AppendFormat("<dhEmi>{0}T{1}</dhEmi>", DateTime.Now.ToString("yyyy-MM-dd"), DateTime.Now.ToString("HH:mm:ss"));
        nota.Append("<fusoHorario>-02:00</fusoHorario>");
        nota.Append("<tpNf>1</tpNf>");
        nota.AppendFormat("<idDest>{0}</idDest>", idDest);
        nota.AppendFormat("<indFinal>1</indFinal>");
        nota.AppendFormat("<indPres>2</indPres>");
        nota.AppendFormat("<cMunFg>{0}</cMunFg>", empresaNota.codigoMunicipioEmitente);
        nota.Append("<tpImp>1</tpImp>");
        nota.Append("<tpEmis>1</tpEmis>");
        nota.AppendFormat("<tpAmb>{0}</tpAmb>", tpAmb);
        nota.Append("<finNFe>1</finNFe>");
        nota.Append("</ide>");
        #endregion

        #region Emitente

        nota.Append("<emit>");
        nota.AppendFormat("<CNPJ_emit>{0}</CNPJ_emit>", empresaNota.cnpj);
        nota.AppendFormat("<xNome>{0}</xNome>", empresaNota.nomeEmitente);
        nota.AppendFormat("<xFant>{0}</xFant>", empresaNota.nomeFantasiaEmitente);
        nota.Append("<enderEmit>");
        nota.AppendFormat("<xLgr>{0}</xLgr>", empresaNota.logradouroEmitente);
        nota.AppendFormat("<nro>{0}</nro>", empresaNota.numeroEmitente);
        nota.AppendFormat("<xBairro>{0}</xBairro>", empresaNota.bairroEmitente);
        nota.AppendFormat("<cMun>{0}</cMun>", empresaNota.codigoMunicipioEmitente);
        nota.AppendFormat("<xMun>{0}</xMun>", empresaNota.municipioEmitente);
        nota.AppendFormat("<UF>{0}</UF>", empresaNota.ufEmitente);
        nota.AppendFormat("<CEP>{0}</CEP>", empresaNota.cepEmitente);
        nota.Append("<cPais>1058</cPais>");
        nota.Append("<xPais>BRASIL</xPais>");
        nota.AppendFormat("<fone>{0}</fone>", empresaNota.foneEmitente);
        nota.Append("</enderEmit>");
        nota.AppendFormat("<IE>{0}</IE>", empresaNota.ieEmitente);
        if (empresaNota.simples) nota.Append("<CRT>1</CRT>");
        else nota.Append("<CRT>3</CRT>");
        nota.Append("</emit>");

        #endregion


        #region Destinatario

        nota.Append("<dest>");
        nota.AppendFormat("<xNome_dest>{0}</xNome_dest>", Regex.Replace(empresaDestino.nome, "[^0-9 a-zA-ZÀ-ú]+", "").TrimEnd().TrimStart());
        nota.AppendFormat("<CNPJ_dest>{0}</CNPJ_dest>", cpfCnpj);
        //nota.AppendFormat("<idEstrangeiro>{0}</idEstrangeiro>", cpfCnpj);

        string foneNfe = empresaDestino.foneEmitente;

        nota.Append("<enderDest>");
        nota.AppendFormat("<xLgr_dest>{0}</xLgr_dest>", empresaDestino.logradouroEmitente);
        nota.AppendFormat("<nro_dest>{0}</nro_dest>", empresaDestino.numeroEmitente);
        nota.AppendFormat("<xBairro_dest>{0}</xBairro_dest>", empresaDestino.bairroEmitente);
        nota.AppendFormat("<cMun_dest>{0}</cMun_dest>", empresaDestino.codigoMunicipioEmitente);
        nota.AppendFormat("<xMun_dest>{0}</xMun_dest>", empresaDestino.municipioEmitente.Trim().TrimEnd());
        nota.AppendFormat("<UF_dest>{0}</UF_dest>", empresaDestino.ufEmitente);
        nota.AppendFormat("<CEP_dest>{0}</CEP_dest>", Regex.Replace(empresaDestino.cepEmitente, "[^0-9]+", "").Trim().TrimEnd());
        nota.Append("<cPais_dest>1058</cPais_dest>");
        nota.Append("<xPais_dest>BRASIL</xPais_dest>");
        if (!string.IsNullOrEmpty(foneNfe)) nota.AppendFormat("<fone_dest>{0}</fone_dest>", Regex.Replace(foneNfe, "[^0-9]+", "").Replace(" ", "").Trim().TrimEnd());
        nota.Append("</enderDest>");
        nota.Append("<indIEDest>1</indIEDest>");
        nota.AppendFormat("<IE_dest>{0}</IE_dest>", empresaDestino.ieEmitente.Trim());

        nota.AppendFormat("<Email_dest>{0}</Email_dest>", "webmaster@graodengente.com.br");
        nota.Append("</dest>");
        #endregion

        #region Autorizacao Emissao
        nota.Append("<autXML>");
        nota.Append("<autXMLItem>");
        nota.AppendFormat("<CNPJ_aut>{0}</CNPJ_aut>", empresaNota.cnpj);
        nota.Append("</autXMLItem>");
        nota.Append("</autXML>");
        #endregion



        #region Detalhamento dos Itens

        nota.Append("<det>");

        int nItem = 1;
        var itensDc = new dbCommerceDataContext();
        decimal valorDoDesconto = 0;
        decimal totalDoPedidoUnitario = 0;
        decimal totalDoPedido = 0;
        decimal totalDescontoCalculado = 0;
        decimal totalIcmsOrigem = 0;
        decimal totalIcmsDestino = 0;
        decimal totalIcms = 0;
        decimal totalPis = 0;
        decimal totalCofins = 0;
        decimal pesoTotal = 0;


        var itensNota = (from c in data.tbPedidoFornecedorItems
                         where c.idPedidoFornecedor == idPedidoFornecedor && c.custo > 0 && c.entregue == true
                         join d in data.tbNotaFiscalItems on c.idPedidoFornecedorItem equals d.idPedidoFornecedorItem
                         select d).ToList();

        var itensPedidoFornecedor = (from c in data.tbPedidoFornecedorItems
                                     where c.idPedidoFornecedor == idPedidoFornecedor && c.custo > 0 && c.entregue == true
                                     select new
                                     {
                                         c.custo,
                                         c.idProduto,
                                         c.tbProduto.produtoNome,
                                         c.tbProduto.icms,
                                         c.tbProduto.ncm,
                                         c.idPedidoFornecedorItem
                                     }).ToList();

        var itensPedido = (from c in itensPedidoFornecedor
                           join d in itensNota on c.idPedidoFornecedorItem equals d.idPedidoFornecedorItem into notas
                           where notas.Count() == 0
                           select new
                           {
                               c.custo,
                               c.idProduto,
                               c.produtoNome,
                               c.icms,
                               c.ncm,
                               c.idPedidoFornecedorItem
                           }).ToList().OrderBy(x => Guid.NewGuid()).Take(500);


        int indiceProduto = 0;
        decimal totalNotaFiscal = 0;

        var notaFiscalItensCheck = (from c in data.tbNotaFiscalItems where c.idNotaFiscal == notaGerada.idNotaFiscal select c).Any();
        if (!notaFiscalItensCheck)
        {
            foreach (var itemPedido in itensPedido)
            {
                if (totalNotaFiscal < limiteNotaAtual)
                {
                    decimal freteDoProduto = 0;
                    var aliquota = (itemPedido.icms ?? 0) == 0 ? 18 : (int)itemPedido.icms;
                    var itemNota = new tbNotaFiscalItem()
                    {
                        cfop = cfop,
                        idNotaFiscal = notaGerada.idNotaFiscal,
                        ncm = itemPedido.ncm,
                        produtoId = itemPedido.idProduto,
                        produtoNome = limitaTexto(rnFuncoes.removeCaracteresEspeciaisDeixaEspaco(rnFuncoes.limpaStringDeixaEspaco(itemPedido.produtoNome)).ToUpper().Replace("-", " ").Trim(), 119).Trim(),
                        qCOM = Convert.ToInt32(1),
                        vUnCom = (Decimal.Round(itemPedido.custo, 2)),
                        vProd = (Decimal.Round(itemPedido.custo, 2)),
                        qTrib = Convert.ToInt32(1),
                        vUnTrib = itemPedido.custo,
                        vFrete = freteDoProduto,
                        CST_pis = (cfop == "5910" || cfop == "6910" ? "49" : "07"),
                        CST_cofins = (cfop == "5910" || cfop == "6910" ? "49" : "07"),
                        aliquotaIcms = aliquota,
                        idPedidoFornecedorItem = itemPedido.idPedidoFornecedorItem
                    };
                    data.tbNotaFiscalItems.InsertOnSubmit(itemNota);
                    data.SubmitChanges();
                    totalNotaFiscal += itemPedido.custo;
                }
            }
        }

        int totalItensRomaneio = (from c in data.tbPedidoFornecedorItems where c.idPedidoFornecedor == idPedidoFornecedor select c).Count();
        var itensNotaFiscal = (from c in data.tbNotaFiscalItems where c.idNotaFiscal == notaGerada.idNotaFiscal select c).ToList();
        foreach (var itemNota in itensNotaFiscal)
        {
            totalDoPedido += itemNota.vUnTrib ?? 0;
            var valorIcms = Convert.ToDecimal((((Convert.ToDecimal(itemNota.vUnTrib)) / 100) * (itemNota.aliquotaIcms ?? 18)).ToString("0.00"));
            totalIcms += valorIcms;
            nota.AppendFormat("<detItem>", nItem);
            nota.Append("<prod>");
            nota.AppendFormat("<cProd>{0}</cProd>", itemNota.produtoId);
            nota.AppendFormat("<xProd>{0}</xProd>", itemNota.produtoNome);
            nota.AppendFormat("<NCM>{0}</NCM>", itemNota.ncm.Trim());
            nota.AppendFormat("<CFOP>{0}</CFOP>", itemNota.cfop);
            nota.AppendFormat("<uCOM>{0}</uCOM>", "UN");
            nota.AppendFormat("<qCOM>{0}</qCOM>", (itemNota.qCOM ?? 0).ToString("0.0000").Replace(",", "."));
            nota.AppendFormat("<vUnCom>{0}</vUnCom>", (itemNota.vUnCom ?? 0).ToString("0.00").Replace(",", "."));
            nota.AppendFormat("<vProd>{0}</vProd>", (itemNota.vProd ?? 0).ToString("0.00").Replace(",", "."));
            nota.Append("<cEANTrib/>");
            nota.AppendFormat("<uTrib>{0}</uTrib>", "UN");
            nota.AppendFormat("<qTrib>{0}</qTrib>", (itemNota.qTrib ?? 0).ToString("0.00").Replace(",", "."));
            nota.AppendFormat("<vUnTrib>{0}</vUnTrib>", (itemNota.vUnTrib ?? 0).ToString("0.00").Replace(",", "."));
            if ((itemNota.vFrete ?? 0) > 0 && indiceProduto == 1) nota.AppendFormat("<vFrete>{0}</vFrete>", (itemNota.vFrete ?? 0).ToString("0.00").Replace(",", "."));
            nota.AppendFormat("<indTot>{0}</indTot>", "1");
            nota.AppendFormat("<nTipoItem>{0}</nTipoItem>", "0");
            nota.Append("</prod>");
            if (empresaNota.simples)
            {
                nota.Append("<imposto>");
                nota.Append("<ICMS>");
                nota.Append("<orig>0</orig>");
                nota.Append("<CST>500</CST>");
                nota.Append("</ICMS>");
                nota.Append("<PIS>");
                nota.AppendFormat("<CST_pis>{0}</CST_pis>", itemNota.CST_pis);
                nota.Append("</PIS>");
                nota.Append("<COFINS>");
                nota.AppendFormat("<CST_cofins>{0}</CST_cofins>", itemNota.CST_cofins);
                nota.Append("</COFINS>");
                nota.Append("</imposto>");
            }
            nota.Append("</detItem>");
            nItem++;
        }





        nota.Append("</det>");
        #endregion

        #region Totais
        nota.Append("<total>");
        nota.Append("<ICMStot>");
        if (empresaNota.simples)
        {
            nota.Append("<vBC_ttlnfe>0.00</vBC_ttlnfe>");
            nota.Append("<vICMS_ttlnfe>0.00</vICMS_ttlnfe>");
        }
        nota.Append("<vBCST_ttlnfe>0.00</vBCST_ttlnfe>");
        nota.Append("<vST_ttlnfe>0.00</vST_ttlnfe>");



        nota.AppendFormat("<vProd_ttlnfe>{0}</vProd_ttlnfe>", Convert.ToDecimal(totalDoPedido).ToString("0.00").Replace(",", "."));
        nota.AppendFormat("<vFrete_ttlnfe>{0}</vFrete_ttlnfe>", (0).ToString("0.00").Replace(",", "."));
        nota.Append("<vSeg_ttlnfe>0.00</vSeg_ttlnfe>");

        nota.AppendFormat("<vDesc_ttlnfe>{0}</vDesc_ttlnfe>", totalDescontoCalculado.ToString("0.00").Replace(",", "."));
        nota.Append("<vIPI_ttlnfe>0.00</vIPI_ttlnfe>");

        nota.AppendFormat("<vPIS_ttlnfe>{0}</vPIS_ttlnfe>", totalPis.ToString("0.00").Replace(",", "."));

        nota.AppendFormat("<vCOFINS_ttlnfe>{0}</vCOFINS_ttlnfe>", totalCofins.ToString("0.00").Replace(",", "."));

        nota.Append("<vOutro>0.00</vOutro>");
        nota.Append("<vICMSDeson_ttlnfe>0.00</vICMSDeson_ttlnfe>");
        nota.AppendFormat("<vNF>{0}</vNF>", Convert.ToDecimal(totalDoPedido).ToString("0.00").Replace(",", "."));
        nota.Append("<vTotTrib_ttlnfe>0.00</vTotTrib_ttlnfe>");

        nota.Append("</ICMStot>");
        nota.Append("</total>");
        #endregion

        #region transportadora

        string informacoesAdicionais = "";
        informacoesAdicionais += "Romaneio " + idPedidoFornecedor + " (" + itensNotaFiscal.Count + "/" + totalItensRomaneio + ")";
        if (empresaNota.simples) informacoesAdicionais += " - I DOCUMENTO EMITIDO POR EMPRESA ME OU EPP OPTANTE PELO SIMPLES NACIONAL II- NAO GERA CREDITO FISCAL DE IPI E ISS.";
        informacoesAdicionais = informacoesAdicionais.Replace(Environment.NewLine, " ");
        nota.Append("<infAdic>");
        nota.AppendFormat("<infCpl>{0}</infCpl>", informacoesAdicionais);
        nota.Append("</infAdic>");
        #endregion

        notaAlterar.valor = Convert.ToDecimal(totalDoPedido);
        if (notaAlterar.statusNfe != 2) notaAlterar.statusNfe = 1;
        notaAlterar.valorPartilha = totalIcmsDestino;
        notaAlterar.gerarGnre = false;

        notaAlterar.municioDestinatario = empresaDestino.codigoMunicipioEmitente;
        data.SubmitChanges();

        try
        {
            System.IO.File.WriteAllText(System.Configuration.ConfigurationManager.AppSettings["caminhoFisico"] + "\\admin\\notas\\r_" + idPedidoFornecedor + "_" + numeroNota + ".xml", nota.ToString());
        }
        catch (Exception ex)
        { }


        string notaFormatado = nota.ToString();

        var notaRetorno = new notaGerada();
        notaRetorno.xml = notaFormatado;
        notaRetorno.idNotaFiscal = notaAlterar.idNotaFiscal;
        return notaRetorno;
    }
}