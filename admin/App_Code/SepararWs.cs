﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.InteropServices;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;

/// <summary>
/// Summary description for SepararWs
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class SepararWs : System.Web.Services.WebService
{

    public SepararWs()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public int VerifcarPermissaoUsuarioExpedicao(string senha)
    {
        var data = new dbCommerceDataContext();
        var usuario = (from c in data.tbUsuarioExpedicaos
                       join d in data.tbUsuarioExpedicaoPermissaos on c.idUsuarioExpedicao equals d.idUsuarioExpedicao
                       where c.separacao == true && c.senha == senha && c.ativo == true
                       select c).FirstOrDefault();

        if (usuario == null) return 0;

        return usuario.idUsuarioExpedicao;
    }


    public class permissoesUsuarioExpedicao
    {
        public int idExpedicaoTipoPermissao { get; set; }
        public int idRelacionado { get; set; }
        public string filtro { get; set; }
    }
    public class UsuarioExpedicaoDetalhes
    {
        public bool valido { get; set; }
        public string nome { get; set; }
        public int idUsuarioExpedicao { get; set; }
        public string senha { get; set; }
        public List<permissoesUsuarioExpedicao> permissoes { get; set; }
        public int idCentroDistribuicao { get; set; }
        public string retornoServico { get; set; }
        public int retornoServicoFiltro { get; set; }
    }

    [WebMethod]
    public UsuarioExpedicaoDetalhes LogarExpedicaoApp(string senha)
    {
        var retorno = new UsuarioExpedicaoDetalhes();
        var data = new dbCommerceDataContext();
        var usuario = (from c in data.tbUsuarioExpedicaos
                       where c.separacao == true && c.senha == senha && c.ativo == true
                       select c).FirstOrDefault();
        if (usuario == null)
        {
            retorno.valido = false;
            return retorno;
        }

        retorno.valido = true;
        retorno.nome = usuario.nome;
        retorno.idUsuarioExpedicao = usuario.idUsuarioExpedicao;
        retorno.senha = senha;
        retorno.idCentroDistribuicao = usuario.idCentroDistribuicao;
        retorno.permissoes = (from c in data.tbUsuarioExpedicaoPermissaos
                              where c.idUsuarioExpedicao == usuario.idUsuarioExpedicao
                              select new permissoesUsuarioExpedicao
                              {
                                  filtro = c.filtro,
                                  idExpedicaoTipoPermissao = c.idExpedicaoTipoPermissao,
                                  idRelacionado = (c.Id_Relacionado ?? 0)
                              }).ToList();       
        return retorno;
    }

    [WebMethod]
    public UsuarioExpedicaoDetalhes LogarExpedicao(string senha, int servico)
    {
        var retorno = new UsuarioExpedicaoDetalhes();
        var data = new dbCommerceDataContext();
        var usuario = (from c in data.tbUsuarioExpedicaos
                       where c.separacao == true && c.senha == senha && c.ativo == true
                       select c).FirstOrDefault();
        if (usuario == null)
        {
            retorno.valido = false;
            return retorno;
        }

        retorno.valido = true;
        retorno.nome = usuario.nome;
        retorno.idUsuarioExpedicao = usuario.idUsuarioExpedicao;
        retorno.senha = senha;
        retorno.idCentroDistribuicao = usuario.idCentroDistribuicao;
        retorno.permissoes = (from c in data.tbUsuarioExpedicaoPermissaos
                          where c.idUsuarioExpedicao == usuario.idUsuarioExpedicao
                          select new permissoesUsuarioExpedicao
                          {
                              filtro = c.filtro,
                              idExpedicaoTipoPermissao = c.idExpedicaoTipoPermissao,
                              idRelacionado = (c.Id_Relacionado ?? 0)
                          }).ToList();

        if(servico == 1) // separar pedido
        {
            var enderecamentoPendente =
                        (from c in data.tbTransferenciaEnderecos
                         where c.idUsuarioExpedicao == usuario.idUsuarioExpedicao && c.dataFim == null
                         select c).FirstOrDefault();
            if (enderecamentoPendente != null)
            {
                retorno.retornoServico = "enderecar";
                return retorno;
            }

            var pedidosSeparacao = (from c in data.tbPedidoEnvios
                                    where
                                        c.tbPedido.statusDoPedido == 11 &&
                                        ((c.dataInicioSeparacao != null && c.dataFimSeparacao == null && c.idUsuarioSeparacao == usuario.idUsuarioExpedicao))
                                        && (c.idCentroDeDistribuicao == usuario.idCentroDistribuicao)
                                    select c).ToList();
            if (usuario.idCentroDistribuicao == 5)
            {
                pedidosSeparacao = (from c in data.tbPedidoEnvios
                                    where
                                        c.tbPedido.statusDoPedido == 11 &&
                                        ((c.idUsuarioSeparacaoCd2 == usuario.idUsuarioExpedicao && c.dataFimSeparacaoCd2 == null))
                                        && (c.idCentroDeDistribuicao == 4 | c.idCentroDeDistribuicao == 5)
                                    select c).ToList();
            }
            if (pedidosSeparacao.Count > 0)
            {
                retorno.retornoServico = "separando";
                if(pedidosSeparacao.Count > 1)
                {
                    retorno.retornoServico = "separandolote";
                }
                retorno.retornoServicoFiltro = pedidosSeparacao.First().idPedido;
            }
            return retorno;
        }
        return retorno;
    }

    [WebMethod]
    public string LogarSeparacao(string senha)
    {
        var data = new dbCommerceDataContext();
        var usuario = (from c in data.tbUsuarioExpedicaos where c.separacao == true && 
                       c.senha == senha && c.ativo == true
                       select c).FirstOrDefault();
        if (usuario == null) return "0";

        var enderecamentoPendente =
            (from c in data.tbTransferenciaEnderecos
             where c.idUsuarioExpedicao == usuario.idUsuarioExpedicao && c.dataFim == null
             select c).FirstOrDefault();
        if (enderecamentoPendente != null)
        {
            return usuario.idUsuarioExpedicao + "_enderecar";
        }

        var pedidosSeparacao = (from c in data.tbPedidoEnvios
                                where
                                    c.idUsuarioSeparacao == usuario.idUsuarioExpedicao && c.tbPedido.statusDoPedido == 11 &&
                                    c.dataInicioSeparacao != null && c.dataFimSeparacao == null && c.tbPedido.separado == false
                                    && (c.idCentroDeDistribuicao == 1 | c.idCentroDeDistribuicao == 2 | c.idCentroDeDistribuicao == 0)
                                select c).FirstOrDefault();
        if (pedidosSeparacao != null)
        {
            return usuario.idUsuarioExpedicao + "_" + pedidosSeparacao.idPedido.ToString();
        }
        return usuario.idUsuarioExpedicao + "_" + "1";
    }

    [WebMethod]
    public string LogarSeparacaoCds(string senha)
    {
        var data = new dbCommerceDataContext();
        var usuario = (from c in data.tbUsuarioExpedicaos where c.separacao == true && 
                       c.senha == senha && c.ativo == true
                       select c).FirstOrDefault();
        if (usuario == null) return "0";

        var enderecamentoPendente =
            (from c in data.tbTransferenciaEnderecos
             where c.idUsuarioExpedicao == usuario.idUsuarioExpedicao && c.dataFim == null
             select c).FirstOrDefault();
        if (enderecamentoPendente != null)
        {
            return usuario.idUsuarioExpedicao + "_enderecar";
        }

        var pedidosSeparacao = (from c in data.tbPedidoEnvios
                                where
                                    c.tbPedido.statusDoPedido == 11 &&
                                    ((c.dataInicioSeparacao != null && c.dataFimSeparacao == null && c.idUsuarioSeparacao == usuario.idUsuarioExpedicao))
                                    && (c.idCentroDeDistribuicao == usuario.idCentroDistribuicao)
                                select c).FirstOrDefault();
        if(usuario.idCentroDistribuicao == 5)
        {
            pedidosSeparacao = (from c in data.tbPedidoEnvios
                                where
                                    c.tbPedido.statusDoPedido == 11 &&
                                    ((c.idUsuarioSeparacaoCd2 == usuario.idUsuarioExpedicao && c.dataFimSeparacaoCd2 == null))
                                    && (c.idCentroDeDistribuicao == 4 | c.idCentroDeDistribuicao == 5)
                                select c).FirstOrDefault();
        }
        if (pedidosSeparacao != null)
        {
            return usuario.idUsuarioExpedicao + "_" + pedidosSeparacao.idPedido.ToString();
        }
        return usuario.idUsuarioExpedicao + "_" + "1";
    }

    [WebMethod]
    public string LogarSeparacaoCd2(string senha)
    {
        var data = new dbCommerceDataContext();
        var usuario = (from c in data.tbUsuarioExpedicaos where c.separacao == true && 
                       c.senha == senha && c.ativo == true
                       select c).FirstOrDefault();
        if (usuario == null) return "0";

        var enderecamentoPendente =
            (from c in data.tbTransferenciaEnderecos
             where c.idUsuarioExpedicao == usuario.idUsuarioExpedicao && c.dataFim == null
             select c).FirstOrDefault();
        if (enderecamentoPendente != null)
        {
            return usuario.idUsuarioExpedicao + "_enderecar";
        }

        var pedidosSeparacao = (from c in data.tbPedidoEnvios
                                where
                                    c.idUsuarioSeparacaoCd2 == usuario.idUsuarioExpedicao && c.tbPedido.statusDoPedido == 11 &&
                                    c.dataInicioSeparacaoCd2 != null && c.dataFimSeparacaoCd2 == null
                                    && (c.idCentroDeDistribuicao == 1 | c.idCentroDeDistribuicao == 2 | c.idCentroDeDistribuicao == 0)
                                select c).FirstOrDefault();
        if (pedidosSeparacao != null)
        {
            return usuario.idUsuarioExpedicao + "_" + pedidosSeparacao.idPedido.ToString();
        }
        return usuario.idUsuarioExpedicao + "_" + "1";
    }

    [WebMethod]
    public string LogarSeparacaoCd3(string senha)
    {
        var data = new dbCommerceDataContext();
        var usuario = (from c in data.tbUsuarioExpedicaos where c.separacao == true && c.senha == senha 
                       && c.idCentroDistribuicao == 3 && c.ativo == true
                       select c).FirstOrDefault();
        if (usuario == null) return "0";

        var enderecamentoPendente =
            (from c in data.tbTransferenciaEnderecos
             where c.idUsuarioExpedicao == usuario.idUsuarioExpedicao && c.dataFim == null
             select c).FirstOrDefault();
        if (enderecamentoPendente != null)
        {
            return usuario.idUsuarioExpedicao + "_enderecar";
        }

        var pedidosSeparacao = (from c in data.tbPedidoEnvios
                                where
                                    c.idUsuarioSeparacaoCd2 == usuario.idUsuarioExpedicao && c.tbPedido.statusDoPedido == 11 &&
                                    c.dataInicioSeparacaoCd2 != null && c.dataFimSeparacaoCd2 == null
                                    && (c.idCentroDeDistribuicao == 3)
                                select c).FirstOrDefault();
        if (pedidosSeparacao != null)
        {
            return usuario.idUsuarioExpedicao + "_" + pedidosSeparacao.idPedido.ToString();
        }
        return usuario.idUsuarioExpedicao + "_" + "1";
    }

    [WebMethod]
    public string ContaEmbalar()
    {
        return rnPedidos.contaPedidosCompletos(false, null).ToString();
    }

    [WebMethod]
    public string ContaEmbalarCds(string idUsuario, string senha)
    {
        var data = new dbCommerceDataContext();
        int idUsuarioSeparacao = Convert.ToInt32(idUsuario);
        var usuario = (from c in data.tbUsuarioExpedicaos where c.idUsuarioExpedicao == idUsuarioSeparacao select c).FirstOrDefault();
        if (usuario != null)
        {
            if (usuario.idCentroDistribuicao == 1) return rnPedidos.contaPedidosCompletos(false, null).ToString();
            if (usuario.idCentroDistribuicao == 2) return rnPedidos.verificaIdsPedidosCompletosCd2SeparadosCd1().Count().ToString();
            if (usuario.idCentroDistribuicao == 3) return rnPedidos.verificaIdsPedidosCompletosCd3().Count().ToString();
            if (usuario.idCentroDistribuicao == 4) return rnPedidos.contaPedidosCompletos(4, false, null).ToString();
            if (usuario.idCentroDistribuicao == 5) return rnPedidos.contaPedidosCompletos(5, false, null).ToString();
        }
        return "0";
    }

    [WebMethod]
    public string ContaEmbalarCd2()
    {
        return rnPedidos.verificaIdsPedidosCompletosCd2SeparadosCd1().Count().ToString();
    }

    [WebMethod]
    public string ContaEmbalarCd3()
    {
        return rnPedidos.verificaIdsPedidosCompletosCd3().Count().ToString();
    }

    [WebMethod]
    public string SepararPedido(string idUsuario, string senha)
    {
        var data = new dbCommerceDataContext();
        int idUsuarioSeparacao = Convert.ToInt32(idUsuario);
        var usuario = (from c in data.tbUsuarioExpedicaos where c.idUsuarioExpedicao == idUsuarioSeparacao select c).FirstOrDefault();
        if(usuario != null)
        {
            if (usuario.idCentroDistribuicao == 1) return SepararPedidoCd1(idUsuario, senha);
            if (usuario.idCentroDistribuicao == 2) return SepararPedidoCd2(idUsuario, senha);
            if (usuario.idCentroDistribuicao == 3) return SepararPedidoCd3(idUsuario, senha);
            if (usuario.idCentroDistribuicao == 4) return SepararPedidoCd4(idUsuario, senha);
            if (usuario.idCentroDistribuicao == 5) return SepararPedidoCd5(idUsuario, senha);
        }
        return "0";
    }

    public string SepararPedidoCd1(string idUsuario, string senha)
    {
        int idUsuarioSeparacao = Convert.ToInt32(idUsuario);
        int totalPedidos = rnPedidos.contaPedidosCompletos(false, false);
        if (totalPedidos == 0) return "0";

        var pedidosCompletos = rnPedidos.verificaIdsPedidosCompletos(false, false);

        int pedidoAtual = 0;
        var pedidoSeparar = pedidosCompletos[pedidoAtual];
        int possuiItens = 0;
        if (pedidoSeparar != null) possuiItens = rnPedidos.retornaProdutosEtiqueta(pedidoSeparar).Count;
        while (pedidoAtual < totalPedidos && possuiItens == 0)
        {
            pedidoAtual++;
            pedidoSeparar = pedidosCompletos[pedidoAtual];
            if (pedidoSeparar != null) possuiItens = rnPedidos.retornaProdutosEtiqueta(pedidoSeparar).Count;
        }
        if (pedidoSeparar != null)
        {
            try
            {
                using (var data = new dbCommerceDataContext())
                {
                    var result = data.iniciarSeparacao(pedidoSeparar, idUsuarioSeparacao, 1).First();

                    return Convert.ToBoolean(result.result) ? pedidoSeparar.ToString() : result.value;
                }
            }
            catch (Exception)
            {
                return "0";
            }
        }
        return "0";
    }

    [WebMethod]
    public string SepararPedidoCd2(string idUsuario, string senha)
    {
        int idUsuarioSeparacao = Convert.ToInt32(idUsuario);
        var pedidosCompletos = rnPedidos.verificaIdsPedidosCompletosCd2SeparadosCd1();
        int totalPedidos = pedidosCompletos.Count();
        if (totalPedidos == 0) return "0";


        int pedidoAtual = 0;
        var pedidoSeparar = pedidosCompletos[pedidoAtual];
        int possuiItens = 0;
        if (pedidoSeparar != null) possuiItens = rnPedidos.retornaProdutosEtiqueta(pedidoSeparar, 2).Count;
        while (pedidoAtual < totalPedidos && possuiItens == 0)
        {
            pedidoAtual++;
            pedidoSeparar = pedidosCompletos[pedidoAtual];
            if (pedidoSeparar != null) possuiItens = rnPedidos.retornaProdutosEtiqueta(pedidoSeparar, 2).Count;
        }
        if (pedidoSeparar != null)
        {
            var pedidosDc = new dbCommerceDataContext();
            var pedidoSepararCheck =
                (from c in pedidosDc.tbPedidoEnvios
                 where c.idPedido == pedidoSeparar && c.dataFimEmbalagem == null && c.idCentroDeDistribuicao < 3
                 select c).FirstOrDefault();
            if (pedidoSepararCheck == null)
            {
                var pedido = (from c in pedidosDc.tbPedidos where c.pedidoId == pedidoSeparar select c).First();
                pedido.idUsuarioSeparacao = idUsuarioSeparacao;
                pedido.dataInicioSeparacao = DateTime.Now;

                var pedidoEnvio = new tbPedidoEnvio();
                pedidoEnvio.idUsuarioSeparacao = idUsuarioSeparacao;
                pedidoEnvio.dataInicioSeparacao = DateTime.Now;
                pedidoEnvio.idPedido = pedidoSeparar;
                pedidoEnvio.idUsuarioSeparacaoCd2 = idUsuarioSeparacao;
                pedidoEnvio.dataInicioSeparacaoCd2 = DateTime.Now;
                pedidosDc.tbPedidoEnvios.InsertOnSubmit(pedidoEnvio);
                pedidosDc.SubmitChanges();


                var usuarioSeparacao = (from c in pedidosDc.tbUsuarioExpedicaos where c.idUsuarioExpedicao == idUsuarioSeparacao select c).FirstOrDefault();
                if (usuarioSeparacao != null)
                {
                    rnInteracoes.interacaoInclui(pedidoSeparar, "Pedido sendo separado no CD2", usuarioSeparacao.nome, "False");
                }

                return pedido.pedidoId.ToString();
            }
            else
            {
                if (pedidoSepararCheck.idUsuarioSeparacaoCd2 == null)
                {
                    pedidoSepararCheck.idUsuarioSeparacaoCd2 = idUsuarioSeparacao;
                    pedidoSepararCheck.dataInicioSeparacaoCd2 = DateTime.Now;
                    pedidosDc.SubmitChanges();

                    var usuarioSeparacao = (from c in pedidosDc.tbUsuarioExpedicaos where c.idUsuarioExpedicao == idUsuarioSeparacao select c).FirstOrDefault();
                    if (usuarioSeparacao != null)
                    {
                        rnInteracoes.interacaoInclui(pedidoSeparar, "Pedido sendo separado no CD2", usuarioSeparacao.nome, "False");
                    }
                }
                return pedidoSepararCheck.idPedido.ToString();
            }
        }
        return "0";
    }

    [WebMethod]
    public string SepararPedidoCd3(string idUsuario, string senha)
    {
        int idUsuarioSeparacao = Convert.ToInt32(idUsuario);
        var pedidosCompletos = rnPedidos.verificaIdsPedidosCompletosCd3();
        int totalPedidos = pedidosCompletos.Count();
        if (totalPedidos == 0) return "0";


        int pedidoAtual = 0;
        var pedidoSeparar = pedidosCompletos[pedidoAtual];
        int possuiItens = 0;
        if (pedidoSeparar != null) possuiItens = rnPedidos.retornaProdutosEtiqueta(pedidoSeparar, 3).Count;
        while (pedidoAtual < totalPedidos && possuiItens == 0)
        {
            pedidoAtual++;
            pedidoSeparar = pedidosCompletos[pedidoAtual];
            if (pedidoSeparar != null) possuiItens = rnPedidos.retornaProdutosEtiqueta(pedidoSeparar, 3).Count;
        }
        if (pedidoSeparar != null)
        {
            var pedidosDc = new dbCommerceDataContext();
            var pedidoSepararCheck =
                (from c in pedidosDc.tbPedidoEnvios
                 where c.idPedido == pedidoSeparar && c.dataFimEmbalagem == null && c.idCentroDeDistribuicao == 3
                 select c).FirstOrDefault();
            if (pedidoSepararCheck == null)
            {
                var pedido = (from c in pedidosDc.tbPedidos where c.pedidoId == pedidoSeparar select c).First();
                pedido.idUsuarioSeparacao = idUsuarioSeparacao;
                pedido.dataInicioSeparacao = DateTime.Now;

                var pedidoEnvio = new tbPedidoEnvio();
                pedidoEnvio.idUsuarioSeparacao = idUsuarioSeparacao;
                pedidoEnvio.dataInicioSeparacao = DateTime.Now;
                pedidoEnvio.idPedido = pedidoSeparar;
                pedidoEnvio.idUsuarioSeparacaoCd2 = idUsuarioSeparacao;
                pedidoEnvio.dataInicioSeparacaoCd2 = DateTime.Now;
                pedidoEnvio.idCentroDeDistribuicao = 3;
                pedidosDc.tbPedidoEnvios.InsertOnSubmit(pedidoEnvio);
                pedidosDc.SubmitChanges();


                var usuarioSeparacao = (from c in pedidosDc.tbUsuarioExpedicaos where c.idUsuarioExpedicao == idUsuarioSeparacao select c).FirstOrDefault();
                if (usuarioSeparacao != null)
                {
                    rnInteracoes.interacaoInclui(pedidoSeparar, "Pedido sendo separado no CD3", usuarioSeparacao.nome, "False");
                }

                return pedido.pedidoId.ToString();
            }
            else
            {
                if (pedidoSepararCheck.idUsuarioSeparacaoCd2 == null)
                {
                    pedidoSepararCheck.idUsuarioSeparacaoCd2 = idUsuarioSeparacao;
                    pedidoSepararCheck.dataInicioSeparacaoCd2 = DateTime.Now;
                    pedidosDc.SubmitChanges();

                    var usuarioSeparacao = (from c in pedidosDc.tbUsuarioExpedicaos where c.idUsuarioExpedicao == idUsuarioSeparacao select c).FirstOrDefault();
                    if (usuarioSeparacao != null)
                    {
                        rnInteracoes.interacaoInclui(pedidoSeparar, "Pedido sendo separado no CD3", usuarioSeparacao.nome, "False");
                    }
                }
                return pedidoSepararCheck.idPedido.ToString();
            }
        }
        return "0";
    }

    public string SepararPedidoCd4(string idUsuario, string senha)
    {
        int idUsuarioSeparacao = Convert.ToInt32(idUsuario);
        int totalPedidos = rnPedidos.contaPedidosCompletos(4, false, false);
        if (totalPedidos == 0) return "0";

        var pedidosCompletos = rnPedidos.verificaIdsPedidosCompletos(4, false, false, new List<int>());

        int pedidoAtual = 0;
        var pedidoSeparar = pedidosCompletos[pedidoAtual];
        int possuiItens = 0;
       /* if (pedidoSeparar != null) possuiItens = rnPedidos.retornaProdutosEtiqueta(pedidoSeparar, 4).Count;
        while (pedidoAtual < totalPedidos && possuiItens == 0)
        {
            pedidoAtual++;
            pedidoSeparar = pedidosCompletos[pedidoAtual];
            if (pedidoSeparar != null) possuiItens = rnPedidos.retornaProdutosEtiqueta(pedidoSeparar, 4).Count;
        }*/
        if (pedidoSeparar != null)
        {
            bool falha = true;
            int skipErros = 0;
            while (falha == true && skipErros < 50)
            {
                try
                {
                    using (var data = new dbCommerceDataContext())
                    {
                        var result = data.iniciarSeparacao(pedidoSeparar, idUsuarioSeparacao, 4).First();
                        if (result.value.Contains("duplicada"))
                        {
                            rnEmails.EnviaEmail("", "andre@graodegente.com.br", "", "", "", "Emissao duplicada " + pedidoSeparar, "Emissao duplicada " + pedidoSeparar);
                            skipErros++;
                            pedidoSeparar = pedidosCompletos[pedidoAtual + skipErros];
                        }
                        else
                        {
                            falha = false;
                            return Convert.ToBoolean(result.result) ? pedidoSeparar.ToString() : result.value;
                        }
                    }
                }
                catch (Exception)
                {
                    skipErros++;
                    pedidoSeparar = pedidosCompletos[pedidoAtual + skipErros];
                    return "0";
                }
            }
        }
        return "0";
    }

    public string SepararPedidoCd5(string idUsuario, string senha)
    {
        var pedidosDc = new dbCommerceDataContext();
        int idUsuarioSeparacao = Convert.ToInt32(idUsuario);
        int totalPedidos = rnPedidos.contaPedidosCompletos(5, false, false);
        if (totalPedidos == 0) return "0";

        var pedidosCompletos = rnPedidos.verificaIdsPedidosCompletos(5, false, false, new List<int>());

        if (idUsuarioSeparacao == 322 | idUsuarioSeparacao == 323 | idUsuarioSeparacao == 324 | idUsuarioSeparacao == 340)
        {
            var pedidosCd45 = (from c in pedidosDc.tbPedidos where c.statusDoPedido == 11 && c.envioLiberadoCd5 == true && c.itensPendentesEnvioCd5 > 0 && c.itensPendentesEnvioCd4 > 0 select c.pedidoId).ToList();
            if (pedidosCd45.Any()) pedidosCompletos = (from c in pedidosCompletos
                                                       join d in pedidosCd45 on c equals d
                                                       select c).ToList();
        }
        else if (idUsuarioSeparacao == 358)
        {
            var pedidos1Volume = (from c in pedidosDc.tbPedidos where c.statusDoPedido == 11 && c.envioLiberadoCd5 == true && c.itensPendentesEnvioCd5 == 1 && c.itensPendentesEnvioCd4 == 0 select c.pedidoId).ToList();
            if (pedidos1Volume.Any()) pedidosCompletos = (from c in pedidosCompletos
                                                          join d in pedidos1Volume on c equals d
                                                          select c).ToList();
        }
        else
        {
            var pedidosCd5 = (from c in pedidosDc.tbPedidos where c.statusDoPedido == 11 && c.envioLiberadoCd5 == true && c.itensPendentesEnvioCd5 > 0 && c.itensPendentesEnvioCd4 == 0 select c.pedidoId).ToList();
            pedidosCompletos = (from c in pedidosCompletos
                                join d in pedidosCd5 on c equals d
                                select c).ToList();
        }

        int pedidoAtual = 0;
        var pedidoSeparar = pedidosCompletos[pedidoAtual];
        int possuiItens = 0;
        int itensSeparados = 0;
        if (pedidoSeparar != null)
        {
            possuiItens = rnPedidos.retornaProdutosEtiqueta(pedidoSeparar, 5).Count;
            var produtosEmbalados =
               (from c in pedidosDc.tbProdutoEstoques where c.pedidoId == pedidoSeparar && c.dataEnvio == null && c.idCentroDistribuicao == 5 select c)
                   .Count();
            if(possuiItens == produtosEmbalados)
            {
                possuiItens = 0;
                var queue = new tbQueue();
                queue.agendamento = DateTime.Now;
                queue.andamento = false;
                queue.concluido = false;
                queue.idRelacionado = pedidoSeparar;
                queue.mensagem = "";
                queue.tipoQueue = 8;
                pedidosDc.tbQueues.InsertOnSubmit(queue);
                pedidosDc.SubmitChanges();
            }
        }
        while (pedidoAtual < totalPedidos && possuiItens == 0)
        {
            pedidoAtual++;
            pedidoSeparar = pedidosCompletos[pedidoAtual];
            if (pedidoSeparar != null)
            {
                possuiItens = rnPedidos.retornaProdutosEtiqueta(pedidoSeparar, 5).Count;
                var produtosEmbalados =
               (from c in pedidosDc.tbProdutoEstoques where c.pedidoId == pedidoSeparar && c.dataEnvio == null && c.idCentroDistribuicao == 5 select c)
                   .Count();
                if (possuiItens == produtosEmbalados)
                {
                    possuiItens = 0;
                    var queue = new tbQueue();
                    queue.agendamento = DateTime.Now;
                    queue.andamento = false;
                    queue.concluido = false;
                    queue.idRelacionado = pedidoSeparar;
                    queue.mensagem = "";
                    queue.tipoQueue = 8;
                    pedidosDc.tbQueues.InsertOnSubmit(queue);
                    pedidosDc.SubmitChanges();
                }
            }
        }
        if (pedidoSeparar != null)
        {
            var pedidoSepararCheck =
                (from c in pedidosDc.tbPedidoEnvios
                 where c.idPedido == pedidoSeparar && c.dataFimEmbalagem == null && c.idCentroDeDistribuicao == 4
                 select c).FirstOrDefault();
            if (pedidoSepararCheck == null)
            {
                var pedido = (from c in pedidosDc.tbPedidos where c.pedidoId == pedidoSeparar select c).First();
                pedido.idUsuarioSeparacao = idUsuarioSeparacao;
                pedido.dataInicioSeparacao = DateTime.Now;

                var pedidoEnvio = new tbPedidoEnvio();
                pedidoEnvio.idUsuarioSeparacao = idUsuarioSeparacao;
                pedidoEnvio.dataInicioSeparacao = DateTime.Now;
                pedidoEnvio.idPedido = pedidoSeparar;
                pedidoEnvio.idCentroDeDistribuicao = 5;
                pedidoEnvio.idUsuarioSeparacaoCd2 = idUsuarioSeparacao;
                pedidoEnvio.dataInicioSeparacaoCd2 = DateTime.Now;
                pedidosDc.tbPedidoEnvios.InsertOnSubmit(pedidoEnvio);
                pedidosDc.SubmitChanges();


                var usuarioSeparacao = (from c in pedidosDc.tbUsuarioExpedicaos where c.idUsuarioExpedicao == idUsuarioSeparacao select c).FirstOrDefault();
                if (usuarioSeparacao != null)
                {
                    rnInteracoes.interacaoInclui(pedidoSeparar, "Pedido sendo separado no CD Jacutinga Moveis", usuarioSeparacao.nome, "False");
                }

                return pedido.pedidoId.ToString();
            }
            else
            {
                if (pedidoSepararCheck.idUsuarioSeparacaoCd2 == null)
                {
                    pedidoSepararCheck.idUsuarioSeparacaoCd2 = idUsuarioSeparacao;
                    pedidoSepararCheck.dataInicioSeparacaoCd2 = DateTime.Now;
                    pedidosDc.SubmitChanges();

                    var usuarioSeparacao = (from c in pedidosDc.tbUsuarioExpedicaos where c.idUsuarioExpedicao == idUsuarioSeparacao select c).FirstOrDefault();
                    if (usuarioSeparacao != null)
                    {
                        rnInteracoes.interacaoInclui(pedidoSeparar, "Pedido sendo separado no CD Jacutinga Moveis", usuarioSeparacao.nome, "False");
                    }
                }
                return pedidoSepararCheck.idPedido.ToString();
            }
        }
        return "0";
    }


    [WebMethod]
    public List<int> SepararPedidoCd5Lote(string idUsuario, string senha, int quantidadeLote)
    {
        var data = new dbCommerceDataContext();
        int idUsuarioSeparacao = Convert.ToInt32(idUsuario);
        var usuarioSeparacao = (from c in data.tbUsuarioExpedicaos where c.idUsuarioExpedicao == idUsuarioSeparacao select c).FirstOrDefault();
        var permissaoLote = (from c in data.tbUsuarioExpedicaoPermissaos where c.idUsuarioExpedicao == idUsuarioSeparacao && c.idExpedicaoTipoPermissao == 7 select c).FirstOrDefault();

        int quantidadePedidosSimultaneo = 1;
        int quantidadeVolumesPedido = 1;

        if(permissaoLote != null)
        {
            quantidadePedidosSimultaneo = permissaoLote.Id_Relacionado ?? 1;
            int.TryParse(permissaoLote.filtro, out quantidadeVolumesPedido);
        }

        var pedidosCompletos = rnPedidos.verificaIdsPedidosCompletos(5, false, false, new List<int>());
        bool possuiPedidosIguais = false;
        var pedidosLimiteVolume = (from c in data.tbPedidos where c.statusDoPedido == 11 && c.envioLiberadoCd5 == true 
                                   && c.itensPendentesEnvioCd5 <= quantidadeVolumesPedido && c.itensPendentesEnvioCd4 == 0 select c.pedidoId).ToList();
        if (pedidosLimiteVolume.Any()) pedidosCompletos = (from c in pedidosCompletos
                                                    join d in pedidosLimiteVolume on c equals d
                                                    select c).ToList();
        var pedidoSeparar = pedidosCompletos.First();
        var volumesPrimeiroPedido = (from c in data.tbPedidos where c.pedidoId == pedidoSeparar select c.itensPendentesEnvioCd5).First();

        var pedidosTotalVolumes = (from c in data.tbPedidos where c.statusDoPedido == 11 && c.envioLiberadoCd5 == true 
                                   && c.itensPendentesEnvioCd5 == volumesPrimeiroPedido
                                   && c.itensPendentesEnvioCd4 == 0 select c.pedidoId).ToList();
        if (pedidosTotalVolumes.Any()) pedidosCompletos = (from c in pedidosCompletos
                                                           join d in pedidosTotalVolumes on c equals d
                                                           select c).ToList();

        var idsProdutoSeparar = (from c in data.tbProdutoEstoques
                                where c.pedidoIdReserva == pedidoSeparar && c.enviado == false && c.pedidoId == null && c.idCentroDistribuicao == 5
                                select c.produtoId).ToList();
        var pedidosMesmoVolume = pedidosCompletos.ToList();
        if(idsProdutoSeparar.Count() != idsProdutoSeparar.Distinct().Count())
        {
            pedidosCompletos = pedidosCompletos.Take(1).ToList();
        }
        var itensPedidoGeral = (from c in data.tbProdutoEstoques
                                where pedidosMesmoVolume.Contains(c.pedidoIdReserva ?? -1) && c.enviado == false && c.idCentroDistribuicao == 5 && c.pedidoId == null
                                select new
                                {
                                    c.produtoId,
                                    pedidoIdReserva = (c.pedidoIdReserva ?? 0)
                                }).ToList();
        foreach (var idProdutoSeparar in idsProdutoSeparar)
        {
            var pedidosComVolume = (from c in itensPedidoGeral where c.produtoId == idProdutoSeparar select c.pedidoIdReserva).ToList();
            pedidosMesmoVolume = (from c in pedidosMesmoVolume
                                  join d in pedidosComVolume on c equals d
                                select c).ToList();
        }
        
        if (pedidosMesmoVolume.Any() && pedidosCompletos.Count() > 1)
        {
            pedidosCompletos = (from c in pedidosCompletos
                                join d in pedidosMesmoVolume on c equals d
                                select c).ToList().Take(quantidadePedidosSimultaneo).ToList();
        }
        else
        {
            pedidosCompletos = pedidosCompletos.Take(1).ToList();
        }

        List<int> pedidosRetorno = new List<int>();
        foreach(var pedidoCompleto in pedidosCompletos)
        {
            var pedidoSepararCheck =
                (from c in data.tbPedidoEnvios
                 where c.idPedido == pedidoCompleto && c.dataFimEmbalagem == null && c.idCentroDeDistribuicao > 3
                 select c).FirstOrDefault();
            if (pedidoSepararCheck == null)
            {
                var pedidoEnvio = new tbPedidoEnvio();
                pedidoEnvio.idUsuarioSeparacao = idUsuarioSeparacao;
                pedidoEnvio.dataInicioSeparacao = DateTime.Now;
                pedidoEnvio.idPedido = pedidoCompleto;
                pedidoEnvio.idCentroDeDistribuicao = 5;
                pedidoEnvio.idUsuarioSeparacaoCd2 = idUsuarioSeparacao;
                pedidoEnvio.dataInicioSeparacaoCd2 = DateTime.Now;
                data.tbPedidoEnvios.InsertOnSubmit(pedidoEnvio);
                data.SubmitChanges();


                if (usuarioSeparacao != null)
                {
                    rnInteracoes.interacaoInclui(pedidoSeparar, "Pedido sendo separado no CD Jacutinga Moveis", usuarioSeparacao.nome, "False");
                }

                pedidosRetorno.Add(pedidoCompleto);
            }
            else
            {
                if (pedidoSepararCheck.idUsuarioSeparacaoCd2 == null)
                {
                    pedidoSepararCheck.idUsuarioSeparacaoCd2 = idUsuarioSeparacao;
                    pedidoSepararCheck.dataInicioSeparacaoCd2 = DateTime.Now;
                    data.SubmitChanges();                    
                    if (usuarioSeparacao != null)
                    {
                        rnInteracoes.interacaoInclui(pedidoSeparar, "Pedido sendo separado no CD Jacutinga Moveis", usuarioSeparacao.nome, "False");
                    }
                }
                pedidosRetorno.Add(pedidoCompleto);
            }
        }

        if (!pedidosRetorno.Any())
        {
            pedidosRetorno.Add(0);
        }

        return pedidosRetorno;
    }

    [WebMethod]
    public string Registrar(string idPedido)
    {
        int pedidoId = Convert.ToInt32(idPedido);
        var produtosParaEmbalar = rnPedidos.retornaProdutosEtiqueta(pedidoId);
        var pedidosDc = new dbCommerceDataContext();
        var produtosDc = new dbCommerceDataContext();

        List<tbProdutoEstoque> produtosEmbalados;

        using (var txn = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required,
            new System.Transactions.TransactionOptions
            {
                IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
            }))

        {
            produtosEmbalados =
               (from c in pedidosDc.tbProdutoEstoques where c.pedidoId == pedidoId && c.dataEnvio == null select c)
                   .ToList();
        }
        var retorno = new List<ListaProdutosSeparar>();
        foreach (var produtoParaEmbalar in produtosParaEmbalar)
        {
            int totalSeparado = produtosEmbalados.Count(x => x.produtoId == produtoParaEmbalar.produtoId | x.produtoId == produtoParaEmbalar.produtoAlternativoId);
            int quantidadeTotal = Convert.ToInt32(produtosParaEmbalar.Where(x => x.produtoId == produtoParaEmbalar.produtoId).Sum(x => x.itemQuantidade));
            var produtoCheck = retorno.Any(x => x.produtoId == produtoParaEmbalar.produtoId | x.produtoId == produtoParaEmbalar.produtoAlternativoId);
            if (!produtoCheck)
            {
                var etiquetaCheck = (from c in pedidosDc.tbProdutoEstoques
                                     where c.enviado == false && (c.pedidoId == null | c.pedidoId == pedidoId) &&
                                     c.produtoId == produtoParaEmbalar.produtoId && c.idEnderecamentoArea != null && c.liberado == true
                                     orderby c.pedidoIdReserva != pedidoId, c.pedidoId != null
                                     select new
                                     {
                                         codigoProduto = c.tbPedidoFornecedorItem.codigoProduto,
                                         area = c.idEnderecamentoArea == null ? "" : c.tbEnderecamentoArea.area,
                                         rua = c.idEnderecamentoRua == null ? "" : c.tbEnderecamentoRua.rua,
                                         predio = c.idEnderecamentoPredio == null ? "" : c.tbEnderecamentoPredio.predio,
                                         lado = c.idEnderecamentoPredio == null ? "" : c.tbEnderecamentoPredio.tbEnderecamentoLado.lado,
                                         andar = c.idEnderecamentoAndar == null ? "" : c.tbEnderecamentoAndar.andar,
                                         apartamento = c.idEnderecamentoApartamento == null ? "" : c.tbEnderecamentoApartamento.apartamento,
                                         idPedidoEtiqueta = c.tbPedidoFornecedorItem.idPedido,
                                         c.idPedidoFornecedorItem
                                     }).FirstOrDefault();

                var categoriaCheck = (from c in pedidosDc.tbJuncaoProdutoCategorias
                                      where c.produtoId == produtoParaEmbalar.produtoId && c.tbProdutoCategoria.categoriaPaiId == 0 &&
                                            c.tbProdutoCategoria.exibirSite == true
                                      select c).FirstOrDefault();

                var produtoDb = (from c in produtosDc.tbProdutos where c.produtoId == produtoParaEmbalar.produtoId select c).First();
                var produto = new ListaProdutosSeparar();
                produto.pedidoId = rnFuncoes.retornaIdCliente(pedidoId);
                produto.quantidade = quantidadeTotal;
                produto.separados = totalSeparado;
                produto.fornecedorNome = produtoParaEmbalar.fornecedorNome;
                produto.produtoId = produtoParaEmbalar.produtoId;
                produto.produtoNome = produtoParaEmbalar.produtoNome;
                produto.produtoFoto = produtoDb.fotoDestaque;
                produto.produtoIdDaEmpresa = produtoParaEmbalar.produtoIdDaEmpresa;
                produto.complementoIdDaEmpresa = produtoDb.complementoIdDaEmpresa;
                if (totalSeparado == quantidadeTotal) produto.separado = true;
                produto.codigoProduto = etiquetaCheck == null ? "" : etiquetaCheck.codigoProduto;
                produto.area = etiquetaCheck == null ? "" : etiquetaCheck.area;
                produto.rua = etiquetaCheck == null ? "" : etiquetaCheck.rua;
                produto.predio = etiquetaCheck == null ? "" : etiquetaCheck.predio;
                produto.lado = etiquetaCheck == null ? "" : etiquetaCheck.lado;
                produto.andar = etiquetaCheck == null ? "" : etiquetaCheck.andar;
                produto.apartamento = etiquetaCheck == null ? "" : etiquetaCheck.apartamento;
                if (categoriaCheck != null) produto.categoria = categoriaCheck.tbProdutoCategoria.categoriaNomeExibicao;
                produto.etiqueta = "";
                if(etiquetaCheck != null) if(etiquetaCheck.idPedidoEtiqueta == pedidoId) produto.etiqueta = etiquetaCheck.idPedidoFornecedorItem.ToString();
                retorno.Add(produto);
            }
        }

        if (retorno.All(x => x.separado != false))
        {
            return "enderecar";
        }

        JavaScriptSerializer js = new JavaScriptSerializer();
        var produtosJson = js.Serialize(retorno.OrderBy(x => x.rua));
        return produtosJson;
    }




    public class ListaProdutosSeparar
    {
        public int pedidoId { get; set; }
        public int quantidade { get; set; }
        public int separados { get; set; }
        public string fornecedorNome { get; set; }
        public int produtoId { get; set; }
        public string produtoNome { get; set; }
        public string produtoFoto { get; set; }
        public string produtoIdDaEmpresa { get; set; }
        public string complementoIdDaEmpresa { get; set; }
        public bool separado { get; set; }
        public string codigoProduto { get; set; }
        public string categoria { get; set; }
        public string area { get; set; }
        public string rua { get; set; }
        public string lado { get; set; }
        public string predio { get; set; }
        public string andar { get; set; }
        public string apartamento { get; set; }
        public string etiqueta { get; set; }
    }

    public class ListaProdutosColetor
    {
        public int pedidoId { get; set; }
        public int quantidade { get; set; }
        public int separados { get; set; }
        public int produtoId { get; set; }
        public string produtoNome { get; set; }
        public string codigoProduto { get; set; }
        public List<string> enderecos { get; set; }
        public string etiqueta { get; set; }
    }
    public class RetornoSepararListaColetor
    {
        public int funcao { get; set; }
        public List<ListaProdutosColetor> produtosSeparar { get; set; }
    }

    public class RetornoSepararLista
    {
        public int funcao { get; set; }
        public List<ListaProdutosSeparar> produtosSeparar {get;set;}
    }

    [WebMethod]
    public RetornoSepararListaColetor ListaProdutosSeparacao(string idPedido, string idUsuario, int produtoId)
    {
        var data = new dbCommerceDataContext();
        int idUsuarioSeparacao = Convert.ToInt32(idUsuario);
        int pedidoId = Convert.ToInt32(idPedido);
        var usuario = (from c in data.tbUsuarioExpedicaos where c.separacao == true && c.idUsuarioExpedicao == idUsuarioSeparacao select c).FirstOrDefault();
        var envio = (from c in data.tbPedidoEnvios where (c.idUsuarioSeparacao == idUsuarioSeparacao | c.idUsuarioSeparacaoCd2 == idUsuarioSeparacao) && (c.dataFimSeparacao == null | c.dataFimSeparacaoCd2 == null) && c.idPedido == pedidoId select c).FirstOrDefault();

        var retorno = new RetornoSepararListaColetor();
        retorno.produtosSeparar = new List<ListaProdutosColetor>();
        retorno.funcao = 0;
        int idCentroDistribuicao = usuario.idCentroDistribuicao;
        if (envio == null) return retorno;

        var produtosParaEmbalar = rnPedidos.retornaProdutosEtiqueta(pedidoId, idCentroDistribuicao);

        List<tbProdutoEstoque> produtosEmbalados;

        using (var txn = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required,
            new System.Transactions.TransactionOptions
            {
                IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
            }))

        {
            produtosEmbalados =
               (from c in data.tbProdutoEstoques where c.pedidoId == pedidoId && c.dataEnvio == null && c.idCentroDistribuicao == idCentroDistribuicao select c)
                   .ToList();
        }
        foreach (var produtoParaEmbalar in produtosParaEmbalar)
        {
            int totalSeparado = produtosEmbalados.Count(x => x.produtoId == produtoParaEmbalar.produtoId | x.produtoId == produtoParaEmbalar.produtoAlternativoId);
            int quantidadeTotal = Convert.ToInt32(produtosParaEmbalar.Where(x => x.produtoId == produtoParaEmbalar.produtoId).Sum(x => x.itemQuantidade));
            var produtoCheck = retorno.produtosSeparar.Any(x => x.produtoId == produtoParaEmbalar.produtoId | x.produtoId == produtoParaEmbalar.produtoAlternativoId);
            if (!produtoCheck)
            {
                var etiquetas = (from c in data.tbProdutoEstoques
                                     where c.enviado == false && (c.pedidoId == null | c.pedidoId == pedidoId) &&
                                     c.produtoId == produtoParaEmbalar.produtoId && c.idEnderecamentoArea != null && c.liberado == true && c.idCentroDistribuicao == idCentroDistribuicao
                                     orderby c.pedidoIdReserva != pedidoId, c.pedidoId != null
                                     select new
                                     {
                                         codigoProduto = c.tbPedidoFornecedorItem.codigoProduto,
                                         area = c.idEnderecamentoArea == null ? "" : c.tbEnderecamentoArea.area,
                                         rua = c.idEnderecamentoRua == null ? "" : c.tbEnderecamentoRua.rua,
                                         predio = c.idEnderecamentoPredio == null ? "" : c.tbEnderecamentoPredio.predio,
                                         lado = c.idEnderecamentoPredio == null ? "" : c.tbEnderecamentoPredio.tbEnderecamentoLado.lado,
                                         andar = c.idEnderecamentoAndar == null ? "" : c.tbEnderecamentoAndar.andar,
                                         apartamento = c.idEnderecamentoApartamento == null ? "" : c.tbEnderecamentoApartamento.apartamento,
                                         idPedidoEtiqueta = c.tbPedidoFornecedorItem.idPedido,
                                         c.idPedidoFornecedorItem
                                     }).Take(20).ToList();
                
                var produtoDb = (from c in data.tbProdutos where c.produtoId == produtoParaEmbalar.produtoId select c).First();
                var produto = new ListaProdutosColetor();
                produto.pedidoId = rnFuncoes.retornaIdCliente(pedidoId);
                produto.quantidade = quantidadeTotal;
                produto.separados = totalSeparado;
                produto.produtoId = produtoParaEmbalar.produtoId;
                produto.produtoNome = produtoParaEmbalar.produtoNome;
                //if (etiquetas.Count() > 0) if (etiquetas.Any(x => x.idPedidoEtiqueta == pedidoId)) produto.etiqueta = etiquetas.First(x => x.idPedidoEtiqueta == pedidoId).ToString();
                if (produtoId == 0)
                {
                    etiquetas = etiquetas.Take(5).ToList();
                }
                produto.enderecos = new List<string>();
                foreach (var etiqueta in etiquetas)
                {
                    string endereco = (etiqueta == null ? " " : etiqueta.codigoProduto) + "|" + (etiqueta == null ? " " : etiqueta.rua)
                        + "|" + (etiqueta == null ? " " : etiqueta.lado)
                        + "|" + (etiqueta == null ? " " : etiqueta.predio)
                        + "|" + (etiqueta == null ? " " : etiqueta.andar);
                    produto.enderecos.Add(endereco);
                }
                retorno.produtosSeparar.Add(produto);
            }
        }

        if (retorno.produtosSeparar.All(x => x.separados == x.quantidade))
        {
            envio.dataFimSeparacaoCd2 = DateTime.Now;
            if (envio.dataFimSeparacao == null) envio.dataFimSeparacao = DateTime.Now;
            data.SubmitChanges();
            rnPedidos.AdicionaQueueChecagemPedidoCompleto(envio.idPedido);
            retorno.funcao = 1;
            return retorno;
        }

        retorno.funcao = 2;
        return retorno;
    }



    [WebMethod]
    public RetornoSepararListaColetor ListaProdutosSeparacaoLote(string idUsuario, int produtoId)
    {
        var data = new dbCommerceDataContext();
        int idUsuarioSeparacao = Convert.ToInt32(idUsuario);
        var usuario = (from c in data.tbUsuarioExpedicaos where c.separacao == true && c.idUsuarioExpedicao == idUsuarioSeparacao select c).FirstOrDefault();
        var envios = (from c in data.tbPedidoEnvios where (c.idUsuarioSeparacao == idUsuarioSeparacao | c.idUsuarioSeparacaoCd2 == idUsuarioSeparacao) && (c.dataFimSeparacao == null | c.dataFimSeparacaoCd2 == null) select c).ToList();

        var pedidos = envios.Select(x => x.idPedido).ToList();

        var retorno = new RetornoSepararListaColetor();
        retorno.produtosSeparar = new List<ListaProdutosColetor>();
        retorno.funcao = 0;
        int idCentroDistribuicao = usuario.idCentroDistribuicao;
        if (envios.Count == 0) return retorno;
        

        var produtosParaEmbalar = rnPedidos.retornaProdutosEtiquetaLote(envios.Select(x => x.idPedido).ToList(), idCentroDistribuicao);

        List<tbProdutoEstoque> produtosEmbalados;

        using (var txn = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required,
            new System.Transactions.TransactionOptions
            {
                IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
            }))

        {
            produtosEmbalados =
               (from c in data.tbProdutoEstoques where pedidos.Contains(c.pedidoId ?? 0) && c.dataEnvio == null && c.idCentroDistribuicao == idCentroDistribuicao select c)
                   .ToList();
        }
        foreach (var produtoParaEmbalar in produtosParaEmbalar)
        {
            int totalSeparado = produtosEmbalados.Count(x => x.produtoId == produtoParaEmbalar.produtoId | x.produtoId == produtoParaEmbalar.produtoAlternativoId);
            int quantidadeTotal = Convert.ToInt32(produtosParaEmbalar.Where(x => x.produtoId == produtoParaEmbalar.produtoId).Sum(x => x.itemQuantidade));
            var produtoCheck = retorno.produtosSeparar.Any(x => x.produtoId == produtoParaEmbalar.produtoId | x.produtoId == produtoParaEmbalar.produtoAlternativoId);
            if (!produtoCheck)
            {
                var etiquetas = (from c in data.tbProdutoEstoques
                                 where c.enviado == false && (c.pedidoId == null | pedidos.Contains(c.pedidoId ?? 0)) &&
                                 c.produtoId == produtoParaEmbalar.produtoId && c.idEnderecamentoArea != null && c.liberado == true && c.idCentroDistribuicao == idCentroDistribuicao
                                 orderby c.pedidoId != null
                                 select new
                                 {
                                     codigoProduto = c.tbPedidoFornecedorItem.codigoProduto,
                                     area = c.idEnderecamentoArea == null ? "" : c.tbEnderecamentoArea.area,
                                     rua = c.idEnderecamentoRua == null ? "" : c.tbEnderecamentoRua.rua,
                                     predio = c.idEnderecamentoPredio == null ? "" : c.tbEnderecamentoPredio.predio,
                                     lado = c.idEnderecamentoPredio == null ? "" : c.tbEnderecamentoPredio.tbEnderecamentoLado.lado,
                                     andar = c.idEnderecamentoAndar == null ? "" : c.tbEnderecamentoAndar.andar,
                                     apartamento = c.idEnderecamentoApartamento == null ? "" : c.tbEnderecamentoApartamento.apartamento,
                                     idPedidoEtiqueta = c.tbPedidoFornecedorItem.idPedido,
                                     c.idPedidoFornecedorItem
                                 }).Take(20).ToList();

                var produtoDb = (from c in data.tbProdutos where c.produtoId == produtoParaEmbalar.produtoId select c).First();
                var produto = new ListaProdutosColetor();
                produto.pedidoId = rnFuncoes.retornaIdCliente(pedidos.First());
                produto.quantidade = quantidadeTotal;
                produto.separados = totalSeparado;
                produto.produtoId = produtoParaEmbalar.produtoId;
                produto.produtoNome = produtoParaEmbalar.produtoNome;
                //if (etiquetas.Count() > 0) if (etiquetas.Any(x => x.idPedidoEtiqueta == pedidoId)) produto.etiqueta = etiquetas.First(x => x.idPedidoEtiqueta == pedidoId).ToString();
                if (produtoId == 0)
                {
                    etiquetas = etiquetas.Take(5).ToList();
                }
                produto.enderecos = new List<string>();
                foreach (var etiqueta in etiquetas)
                {
                    string endereco = (etiqueta == null ? " " : etiqueta.codigoProduto) + "|" + (etiqueta == null ? " " : etiqueta.rua)
                        + "|" + (etiqueta == null ? " " : etiqueta.lado)
                        + "|" + (etiqueta == null ? " " : etiqueta.predio)
                        + "|" + (etiqueta == null ? " " : etiqueta.andar);
                    produto.enderecos.Add(endereco);
                }
                retorno.produtosSeparar.Add(produto);
            }
        }

        if (retorno.produtosSeparar.All(x => x.separados == x.quantidade))
        {
            retorno.funcao = 1;
            return retorno;
        }

        retorno.funcao = 2;
        return retorno;
    }


    [WebMethod]
    public RetornoSepararLista RegistrarCdsColetor(string idPedido, string idUsuario)
    {
        var data = new dbCommerceDataContext();
        int idUsuarioSeparacao = Convert.ToInt32(idUsuario);
        int pedidoId = Convert.ToInt32(idPedido);
        var usuario = (from c in data.tbUsuarioExpedicaos where c.separacao == true && c.idUsuarioExpedicao == idUsuarioSeparacao select c).FirstOrDefault();
        var envio = (from c in data.tbPedidoEnvios where (c.idUsuarioSeparacao == idUsuarioSeparacao | c.idUsuarioSeparacaoCd2 == idUsuarioSeparacao) && (c.dataFimSeparacao == null | c.dataFimSeparacaoCd2 == null) && c.idPedido == pedidoId select c).FirstOrDefault();

        var retorno = new RetornoSepararLista();
        retorno.produtosSeparar = new List<ListaProdutosSeparar>();
        retorno.funcao = 0;
        int idCentroDistribuicao = usuario.idCentroDistribuicao;
        if (envio == null) return retorno;

        var produtosParaEmbalar = rnPedidos.retornaProdutosEtiqueta(pedidoId, idCentroDistribuicao);

        List<tbProdutoEstoque> produtosEmbalados;

        using (var txn = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required,
            new System.Transactions.TransactionOptions
            {
                IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
            }))

        {
            produtosEmbalados =
               (from c in data.tbProdutoEstoques where c.pedidoId == pedidoId && c.dataEnvio == null && c.idCentroDistribuicao == idCentroDistribuicao select c)
                   .ToList();
        }
        foreach (var produtoParaEmbalar in produtosParaEmbalar)
        {
            int totalSeparado = produtosEmbalados.Count(x => x.produtoId == produtoParaEmbalar.produtoId | x.produtoId == produtoParaEmbalar.produtoAlternativoId);
            int quantidadeTotal = Convert.ToInt32(produtosParaEmbalar.Where(x => x.produtoId == produtoParaEmbalar.produtoId).Sum(x => x.itemQuantidade));
            var produtoCheck = retorno.produtosSeparar.Any(x => x.produtoId == produtoParaEmbalar.produtoId | x.produtoId == produtoParaEmbalar.produtoAlternativoId);
            if (!produtoCheck)
            {
                var etiquetaCheck = (from c in data.tbProdutoEstoques
                                     where c.enviado == false && (c.pedidoId == null | c.pedidoId == pedidoId) &&
                                     c.produtoId == produtoParaEmbalar.produtoId && c.idEnderecamentoArea != null && c.liberado == true && c.idCentroDistribuicao == idCentroDistribuicao
                                     orderby c.pedidoIdReserva != pedidoId, c.pedidoId != null
                                     select new
                                     {
                                         codigoProduto = c.tbPedidoFornecedorItem.codigoProduto,
                                         area = c.idEnderecamentoArea == null ? "" : c.tbEnderecamentoArea.area,
                                         rua = c.idEnderecamentoRua == null ? "" : c.tbEnderecamentoRua.rua,
                                         predio = c.idEnderecamentoPredio == null ? "" : c.tbEnderecamentoPredio.predio,
                                         lado = c.idEnderecamentoPredio == null ? "" : c.tbEnderecamentoPredio.tbEnderecamentoLado.lado,
                                         andar = c.idEnderecamentoAndar == null ? "" : c.tbEnderecamentoAndar.andar,
                                         apartamento = c.idEnderecamentoApartamento == null ? "" : c.tbEnderecamentoApartamento.apartamento,
                                         idPedidoEtiqueta = c.tbPedidoFornecedorItem.idPedido,
                                         c.idPedidoFornecedorItem
                                     }).FirstOrDefault();

                var categoriaCheck = (from c in data.tbJuncaoProdutoCategorias
                                      where c.produtoId == produtoParaEmbalar.produtoId && c.tbProdutoCategoria.categoriaPaiId == 0 &&
                                            c.tbProdutoCategoria.exibirSite == true
                                      select c).FirstOrDefault();

                var produtoDb = (from c in data.tbProdutos where c.produtoId == produtoParaEmbalar.produtoId select c).First();
                var produto = new ListaProdutosSeparar();
                produto.pedidoId = rnFuncoes.retornaIdCliente(pedidoId);
                produto.quantidade = quantidadeTotal;
                produto.separados = totalSeparado;
                produto.fornecedorNome = produtoParaEmbalar.fornecedorNome;
                produto.produtoId = produtoParaEmbalar.produtoId;
                produto.produtoNome = produtoParaEmbalar.produtoNome;
                produto.produtoFoto = produtoDb.fotoDestaque;
                produto.produtoIdDaEmpresa = produtoParaEmbalar.produtoIdDaEmpresa;
                produto.complementoIdDaEmpresa = produtoDb.complementoIdDaEmpresa;
                if (totalSeparado == quantidadeTotal) produto.separado = true;
                produto.codigoProduto = etiquetaCheck == null ? "" : etiquetaCheck.codigoProduto;
                produto.area = etiquetaCheck == null ? "" : etiquetaCheck.area;
                produto.rua = etiquetaCheck == null ? "" : etiquetaCheck.rua;
                produto.predio = etiquetaCheck == null ? "" : etiquetaCheck.predio;
                produto.lado = etiquetaCheck == null ? "" : etiquetaCheck.lado;
                produto.andar = etiquetaCheck == null ? "" : etiquetaCheck.andar;
                produto.apartamento = etiquetaCheck == null ? "" : etiquetaCheck.apartamento;
                if (categoriaCheck != null) produto.categoria = categoriaCheck.tbProdutoCategoria.categoriaNomeExibicao;
                produto.etiqueta = "";
                if (etiquetaCheck != null) if (etiquetaCheck.idPedidoEtiqueta == pedidoId) produto.etiqueta = etiquetaCheck.idPedidoFornecedorItem.ToString();
                retorno.produtosSeparar.Add(produto);
            }
        }

        if (retorno.produtosSeparar.All(x => x.separado != false))
        {
            retorno.funcao = 1;
            return retorno;
        }

        retorno.funcao = 2;
        return retorno;
    }


    [WebMethod]
    public string RegistrarCds(string idPedido, string idUsuario)
    {
        var data = new dbCommerceDataContext();
        int idUsuarioSeparacao = Convert.ToInt32(idUsuario);
        int pedidoId = Convert.ToInt32(idPedido);
        var usuario = (from c in data.tbUsuarioExpedicaos where c.separacao == true && c.idUsuarioExpedicao == idUsuarioSeparacao select c).FirstOrDefault();
        var envio = (from c in data.tbPedidoEnvios where (c.idUsuarioSeparacao == idUsuarioSeparacao | c.idUsuarioSeparacaoCd2 == idUsuarioSeparacao) && (c.dataFimSeparacao == null | c.dataFimSeparacaoCd2 == null) && c.idPedido == pedidoId select c).FirstOrDefault();

        int idCentroDistribuicao = usuario.idCentroDistribuicao;
        if (envio == null) return "";

        var produtosParaEmbalar = rnPedidos.retornaProdutosEtiqueta(pedidoId, idCentroDistribuicao);

        List<tbProdutoEstoque> produtosEmbalados;

        using (var txn = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required,
            new System.Transactions.TransactionOptions
            {
                IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
            }))

        {
            produtosEmbalados =
               (from c in data.tbProdutoEstoques where c.pedidoId == pedidoId && c.dataEnvio == null && c.idCentroDistribuicao == idCentroDistribuicao select c)
                   .ToList();
        }
        var retorno = new List<ListaProdutosSeparar>();
        foreach (var produtoParaEmbalar in produtosParaEmbalar)
        {
            int totalSeparado = produtosEmbalados.Count(x => x.produtoId == produtoParaEmbalar.produtoId | x.produtoId == produtoParaEmbalar.produtoAlternativoId);
            int quantidadeTotal = Convert.ToInt32(produtosParaEmbalar.Where(x => x.produtoId == produtoParaEmbalar.produtoId).Sum(x => x.itemQuantidade));
            var produtoCheck = retorno.Any(x => x.produtoId == produtoParaEmbalar.produtoId | x.produtoId == produtoParaEmbalar.produtoAlternativoId);
            if (!produtoCheck)
            {
                var etiquetaCheck = (from c in data.tbProdutoEstoques
                                     where c.enviado == false && (c.pedidoId == null | c.pedidoId == pedidoId) &&
                                     c.produtoId == produtoParaEmbalar.produtoId && c.idEnderecamentoArea != null && c.liberado == true && c.idCentroDistribuicao == idCentroDistribuicao
                                     orderby c.pedidoIdReserva != pedidoId, c.pedidoId != null
                                     select new
                                     {
                                         codigoProduto = c.tbPedidoFornecedorItem.codigoProduto,
                                         area = c.idEnderecamentoArea == null ? "" : c.tbEnderecamentoArea.area,
                                         rua = c.idEnderecamentoRua == null ? "" : c.tbEnderecamentoRua.rua,
                                         predio = c.idEnderecamentoPredio == null ? "" : c.tbEnderecamentoPredio.predio,
                                         lado = c.idEnderecamentoPredio == null ? "" : c.tbEnderecamentoPredio.tbEnderecamentoLado.lado,
                                         andar = c.idEnderecamentoAndar == null ? "" : c.tbEnderecamentoAndar.andar,
                                         apartamento = c.idEnderecamentoApartamento == null ? "" : c.tbEnderecamentoApartamento.apartamento,
                                         idPedidoEtiqueta = c.tbPedidoFornecedorItem.idPedido,
                                         c.idPedidoFornecedorItem
                                     }).FirstOrDefault();

                var categoriaCheck = (from c in data.tbJuncaoProdutoCategorias
                                      where c.produtoId == produtoParaEmbalar.produtoId && c.tbProdutoCategoria.categoriaPaiId == 0 &&
                                            c.tbProdutoCategoria.exibirSite == true
                                      select c).FirstOrDefault();

                var produtoDb = (from c in data.tbProdutos where c.produtoId == produtoParaEmbalar.produtoId select c).First();
                var produto = new ListaProdutosSeparar();
                produto.pedidoId = rnFuncoes.retornaIdCliente(pedidoId);
                produto.quantidade = quantidadeTotal;
                produto.separados = totalSeparado;
                produto.fornecedorNome = produtoParaEmbalar.fornecedorNome;
                produto.produtoId = produtoParaEmbalar.produtoId;
                produto.produtoNome = produtoParaEmbalar.produtoNome;
                produto.produtoFoto = produtoDb.fotoDestaque;
                produto.produtoIdDaEmpresa = produtoParaEmbalar.produtoIdDaEmpresa;
                produto.complementoIdDaEmpresa = produtoDb.complementoIdDaEmpresa;
                if (totalSeparado == quantidadeTotal) produto.separado = true;
                produto.codigoProduto = etiquetaCheck == null ? "" : etiquetaCheck.codigoProduto;
                produto.area = etiquetaCheck == null ? "" : etiquetaCheck.area;
                produto.rua = etiquetaCheck == null ? "" : etiquetaCheck.rua;
                produto.predio = etiquetaCheck == null ? "" : etiquetaCheck.predio;
                produto.lado = etiquetaCheck == null ? "" : etiquetaCheck.lado;
                produto.andar = etiquetaCheck == null ? "" : etiquetaCheck.andar;
                produto.apartamento = etiquetaCheck == null ? "" : etiquetaCheck.apartamento;
                if (categoriaCheck != null) produto.categoria = categoriaCheck.tbProdutoCategoria.categoriaNomeExibicao;
                produto.etiqueta = "";
                if(etiquetaCheck != null) if(etiquetaCheck.idPedidoEtiqueta == pedidoId) produto.etiqueta = etiquetaCheck.idPedidoFornecedorItem.ToString();
                retorno.Add(produto);
            }
        }

        if (retorno.All(x => x.separado != false))
        {
            return "enderecar";
        }

        JavaScriptSerializer js = new JavaScriptSerializer();
        var produtosJson = js.Serialize(retorno.OrderBy(x => x.rua));
        return produtosJson;
    }

    [WebMethod]
    public string RegistrarCd2(string idPedido)
    {
        int pedidoId = Convert.ToInt32(idPedido);
        var produtosParaEmbalar = rnPedidos.retornaProdutosEtiqueta(pedidoId, 2);
        var pedidosDc = new dbCommerceDataContext();
        var produtosDc = new dbCommerceDataContext();

        List<tbProdutoEstoque> produtosEmbalados;

        using (var txn = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required,
            new System.Transactions.TransactionOptions
            {
                IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
            }))

        {
            produtosEmbalados =
               (from c in pedidosDc.tbProdutoEstoques where c.pedidoId == pedidoId && c.dataEnvio == null && c.idCentroDistribuicao == 2 select c)
                   .ToList();
        }
        var retorno = new List<ListaProdutosSeparar>();
        foreach (var produtoParaEmbalar in produtosParaEmbalar)
        {
            int totalSeparado = produtosEmbalados.Count(x => x.produtoId == produtoParaEmbalar.produtoId | x.produtoId == produtoParaEmbalar.produtoAlternativoId);
            int quantidadeTotal = Convert.ToInt32(produtosParaEmbalar.Where(x => x.produtoId == produtoParaEmbalar.produtoId).Sum(x => x.itemQuantidade));
            var produtoCheck = retorno.Any(x => x.produtoId == produtoParaEmbalar.produtoId | x.produtoId == produtoParaEmbalar.produtoAlternativoId);
            if (!produtoCheck)
            {
                var etiquetaCheck = (from c in pedidosDc.tbProdutoEstoques
                                     where c.enviado == false && (c.pedidoId == null | c.pedidoId == pedidoId) && c.produtoId == produtoParaEmbalar.produtoId && c.idEnderecamentoArea != null
                                     orderby c.pedidoIdReserva != pedidoId, c.pedidoId != null
                                     select new
                                     {
                                         codigoProduto = c.tbPedidoFornecedorItem.codigoProduto,
                                         area = c.idEnderecamentoArea == null ? "" : c.tbEnderecamentoArea.area,
                                         rua = c.idEnderecamentoRua == null ? "" : c.tbEnderecamentoRua.rua,
                                         predio = c.idEnderecamentoPredio == null ? "" : c.tbEnderecamentoPredio.predio,
                                         lado = c.idEnderecamentoPredio == null ? "" : c.tbEnderecamentoPredio.tbEnderecamentoLado.lado,
                                         andar = c.idEnderecamentoAndar == null ? "" : c.tbEnderecamentoAndar.andar,
                                         apartamento = c.idEnderecamentoApartamento == null ? "" : c.tbEnderecamentoApartamento.apartamento,
                                         idPedidoEtiqueta = c.tbPedidoFornecedorItem.idPedido,
                                         c.idPedidoFornecedorItem
                                     }).FirstOrDefault();

                var categoriaCheck = (from c in pedidosDc.tbJuncaoProdutoCategorias
                                      where c.produtoId == produtoParaEmbalar.produtoId && c.tbProdutoCategoria.categoriaPaiId == 0 &&
                                            c.tbProdutoCategoria.exibirSite == true
                                      select c).FirstOrDefault();

                var produtoDb = (from c in produtosDc.tbProdutos where c.produtoId == produtoParaEmbalar.produtoId select c).First();
                var produto = new ListaProdutosSeparar();
                produto.pedidoId = rnFuncoes.retornaIdCliente(pedidoId);
                produto.quantidade = quantidadeTotal;
                produto.separados = totalSeparado;
                produto.fornecedorNome = produtoParaEmbalar.fornecedorNome;
                produto.produtoId = produtoParaEmbalar.produtoId;
                produto.produtoNome = produtoParaEmbalar.produtoNome;
                produto.produtoFoto = produtoDb.fotoDestaque;
                produto.produtoIdDaEmpresa = produtoParaEmbalar.produtoIdDaEmpresa;
                produto.complementoIdDaEmpresa = produtoDb.complementoIdDaEmpresa;
                if (totalSeparado == quantidadeTotal) produto.separado = true;
                produto.codigoProduto = etiquetaCheck == null ? "" : etiquetaCheck.codigoProduto;
                produto.area = etiquetaCheck == null ? "" : etiquetaCheck.area;
                produto.rua = etiquetaCheck == null ? "" : etiquetaCheck.rua;
                produto.predio = etiquetaCheck == null ? "" : etiquetaCheck.predio;
                produto.lado = etiquetaCheck == null ? "" : etiquetaCheck.lado;
                produto.andar = etiquetaCheck == null ? "" : etiquetaCheck.andar;
                produto.apartamento = etiquetaCheck == null ? "" : etiquetaCheck.apartamento;
                if (categoriaCheck != null) produto.categoria = categoriaCheck.tbProdutoCategoria.categoriaNomeExibicao;
                produto.etiqueta = "";
                if (etiquetaCheck.idPedidoEtiqueta == pedidoId) produto.etiqueta = etiquetaCheck.idPedidoFornecedorItem.ToString();
                retorno.Add(produto);
            }
        }

        if (retorno.All(x => x.separado != false))
        {
            return "enderecar";
        }

        JavaScriptSerializer js = new JavaScriptSerializer();
        var produtosJson = js.Serialize(retorno.OrderBy(x => x.rua));
        return produtosJson;
    }

    [WebMethod]
    public string RegistrarCd5(string idPedido)
    {
        int pedidoId = Convert.ToInt32(idPedido);
        var produtosParaEmbalar = rnPedidos.retornaProdutosEtiqueta(pedidoId, 5);
        var pedidosDc = new dbCommerceDataContext();
        var produtosDc = new dbCommerceDataContext();

        List<tbProdutoEstoque> produtosEmbalados;

        using (var txn = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required,
            new System.Transactions.TransactionOptions
            {
                IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
            }))

        {
            produtosEmbalados =
               (from c in pedidosDc.tbProdutoEstoques where c.pedidoId == pedidoId && c.dataEnvio == null && c.idCentroDistribuicao == 5 select c)
                   .ToList();
        }
        var retorno = new List<ListaProdutosSeparar>();
        foreach (var produtoParaEmbalar in produtosParaEmbalar)
        {
            int totalSeparado = produtosEmbalados.Count(x => x.produtoId == produtoParaEmbalar.produtoId | x.produtoId == produtoParaEmbalar.produtoAlternativoId);
            int quantidadeTotal = Convert.ToInt32(produtosParaEmbalar.Where(x => x.produtoId == produtoParaEmbalar.produtoId).Sum(x => x.itemQuantidade));
            var produtoCheck = retorno.Any(x => x.produtoId == produtoParaEmbalar.produtoId | x.produtoId == produtoParaEmbalar.produtoAlternativoId);
            if (!produtoCheck)
            {
                var etiquetaCheck = (from c in pedidosDc.tbProdutoEstoques
                                     where c.enviado == false && (c.pedidoId == null | c.pedidoId == pedidoId) && c.produtoId == produtoParaEmbalar.produtoId && c.idEnderecamentoArea != null
                                     orderby c.pedidoIdReserva != pedidoId, c.pedidoId != null
                                     select new
                                     {
                                         codigoProduto = c.tbPedidoFornecedorItem.codigoProduto,
                                         area = c.idEnderecamentoArea == null ? "" : c.tbEnderecamentoArea.area,
                                         rua = c.idEnderecamentoRua == null ? "" : c.tbEnderecamentoRua.rua,
                                         predio = c.idEnderecamentoPredio == null ? "" : c.tbEnderecamentoPredio.predio,
                                         lado = c.idEnderecamentoPredio == null ? "" : c.tbEnderecamentoPredio.tbEnderecamentoLado.lado,
                                         andar = c.idEnderecamentoAndar == null ? "" : c.tbEnderecamentoAndar.andar,
                                         apartamento = c.idEnderecamentoApartamento == null ? "" : c.tbEnderecamentoApartamento.apartamento,
                                         idPedidoEtiqueta = c.tbPedidoFornecedorItem.idPedido,
                                         c.idPedidoFornecedorItem
                                     }).FirstOrDefault();

                var categoriaCheck = (from c in pedidosDc.tbJuncaoProdutoCategorias
                                      where c.produtoId == produtoParaEmbalar.produtoId && c.tbProdutoCategoria.categoriaPaiId == 0 &&
                                            c.tbProdutoCategoria.exibirSite == true
                                      select c).FirstOrDefault();

                var produtoDb = (from c in produtosDc.tbProdutos where c.produtoId == produtoParaEmbalar.produtoId select c).First();
                var produto = new ListaProdutosSeparar();
                produto.pedidoId = rnFuncoes.retornaIdCliente(pedidoId);
                produto.quantidade = quantidadeTotal;
                produto.separados = totalSeparado;
                produto.fornecedorNome = produtoParaEmbalar.fornecedorNome;
                produto.produtoId = produtoParaEmbalar.produtoId;
                produto.produtoNome = produtoParaEmbalar.produtoNome;
                produto.produtoFoto = produtoDb.fotoDestaque;
                produto.produtoIdDaEmpresa = produtoParaEmbalar.produtoIdDaEmpresa;
                produto.complementoIdDaEmpresa = produtoDb.complementoIdDaEmpresa;
                if (totalSeparado == quantidadeTotal) produto.separado = true;
                produto.codigoProduto = etiquetaCheck == null ? "" : etiquetaCheck.codigoProduto;
                produto.area = etiquetaCheck == null ? "" : etiquetaCheck.area;
                produto.rua = etiquetaCheck == null ? "" : etiquetaCheck.rua;
                produto.predio = etiquetaCheck == null ? "" : etiquetaCheck.predio;
                produto.lado = etiquetaCheck == null ? "" : etiquetaCheck.lado;
                produto.andar = etiquetaCheck == null ? "" : etiquetaCheck.andar;
                produto.apartamento = etiquetaCheck == null ? "" : etiquetaCheck.apartamento;
                if (categoriaCheck != null) produto.categoria = categoriaCheck.tbProdutoCategoria.categoriaNomeExibicao;
                produto.etiqueta = "";
                if (etiquetaCheck.idPedidoEtiqueta == pedidoId) produto.etiqueta = etiquetaCheck.idPedidoFornecedorItem.ToString();
                retorno.Add(produto);
            }
        }

        if (retorno.All(x => x.separado != false))
        {
            return "enderecar";
        }

        JavaScriptSerializer js = new JavaScriptSerializer();
        var produtosJson = js.Serialize(retorno.OrderBy(x => x.rua));
        return produtosJson;
    }

    [WebMethod]
    public string RegistrarCd3(string idPedido)
    {
        int pedidoId = Convert.ToInt32(idPedido);
        var produtosParaEmbalar = rnPedidos.retornaProdutosEtiqueta(pedidoId, 3);
        var pedidosDc = new dbCommerceDataContext();
        var produtosDc = new dbCommerceDataContext();

        List<tbProdutoEstoque> produtosEmbalados;

        using (var txn = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required,
            new System.Transactions.TransactionOptions
            {
                IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
            }))

        {
            produtosEmbalados =
               (from c in pedidosDc.tbProdutoEstoques where c.pedidoId == pedidoId && c.dataEnvio == null && c.idCentroDistribuicao == 3 select c)
                   .ToList();
        }
        var retorno = new List<ListaProdutosSeparar>();
        foreach (var produtoParaEmbalar in produtosParaEmbalar)
        {
            int totalSeparado = produtosEmbalados.Count(x => x.produtoId == produtoParaEmbalar.produtoId | x.produtoId == produtoParaEmbalar.produtoAlternativoId);
            int quantidadeTotal = Convert.ToInt32(produtosParaEmbalar.Where(x => x.produtoId == produtoParaEmbalar.produtoId).Sum(x => x.itemQuantidade));
            var produtoCheck = retorno.Any(x => x.produtoId == produtoParaEmbalar.produtoId | x.produtoId == produtoParaEmbalar.produtoAlternativoId);
            if (!produtoCheck)
            {
                var etiquetaCheck = (from c in pedidosDc.tbProdutoEstoques
                                     where c.enviado == false && (c.pedidoId == null | c.pedidoId == pedidoId) && c.produtoId == produtoParaEmbalar.produtoId && c.idEnderecamentoArea != null
                                     orderby c.pedidoIdReserva != pedidoId, c.pedidoId != null
                                     select new
                                     {
                                         codigoProduto = c.tbPedidoFornecedorItem.codigoProduto,
                                         area = c.idEnderecamentoArea == null ? "" : c.tbEnderecamentoArea.area,
                                         rua = c.idEnderecamentoRua == null ? "" : c.tbEnderecamentoRua.rua,
                                         predio = c.idEnderecamentoPredio == null ? "" : c.tbEnderecamentoPredio.predio,
                                         lado = c.idEnderecamentoPredio == null ? "" : c.tbEnderecamentoPredio.tbEnderecamentoLado.lado,
                                         andar = c.idEnderecamentoAndar == null ? "" : c.tbEnderecamentoAndar.andar,
                                         apartamento = c.idEnderecamentoApartamento == null ? "" : c.tbEnderecamentoApartamento.apartamento,
                                         idPedidoEtiqueta = c.tbPedidoFornecedorItem.idPedido,
                                         c.idPedidoFornecedorItem
                                     }).FirstOrDefault();

                var categoriaCheck = (from c in pedidosDc.tbJuncaoProdutoCategorias
                                      where c.produtoId == produtoParaEmbalar.produtoId && c.tbProdutoCategoria.categoriaPaiId == 0 &&
                                            c.tbProdutoCategoria.exibirSite == true
                                      select c).FirstOrDefault();

                var produtoDb = (from c in produtosDc.tbProdutos where c.produtoId == produtoParaEmbalar.produtoId select c).First();
                var produto = new ListaProdutosSeparar();
                produto.pedidoId = rnFuncoes.retornaIdCliente(pedidoId);
                produto.quantidade = quantidadeTotal;
                produto.separados = totalSeparado;
                produto.fornecedorNome = produtoParaEmbalar.fornecedorNome;
                produto.produtoId = produtoParaEmbalar.produtoId;
                produto.produtoNome = produtoParaEmbalar.produtoNome;
                produto.produtoFoto = produtoDb.fotoDestaque;
                produto.produtoIdDaEmpresa = produtoParaEmbalar.produtoIdDaEmpresa;
                produto.complementoIdDaEmpresa = produtoDb.complementoIdDaEmpresa;
                if (totalSeparado == quantidadeTotal) produto.separado = true;
                produto.codigoProduto = etiquetaCheck == null ? "" : etiquetaCheck.codigoProduto;
                produto.area = etiquetaCheck == null ? "" : etiquetaCheck.area;
                produto.rua = etiquetaCheck == null ? "" : etiquetaCheck.rua;
                produto.predio = etiquetaCheck == null ? "" : etiquetaCheck.predio;
                produto.lado = etiquetaCheck == null ? "" : etiquetaCheck.lado;
                produto.andar = etiquetaCheck == null ? "" : etiquetaCheck.andar;
                produto.apartamento = etiquetaCheck == null ? "" : etiquetaCheck.apartamento;
                if (categoriaCheck != null) produto.categoria = categoriaCheck.tbProdutoCategoria.categoriaNomeExibicao;
                produto.etiqueta = "";
                if (etiquetaCheck.idPedidoEtiqueta == pedidoId) produto.etiqueta = etiquetaCheck.idPedidoFornecedorItem.ToString();
                retorno.Add(produto);
            }
        }

        if (retorno.All(x => x.separado != false))
        {
            return "enderecar";
        }

        JavaScriptSerializer js = new JavaScriptSerializer();
        var produtosJson = js.Serialize(retorno.OrderBy(x => x.rua));
        return produtosJson;
    }


    public class RetornoListaEnderecoProvisorio
    {
        public string idPedidoFornecedorItem { get; set; }
        public string produtoNome { get; set; }
        public string codigoProduto { get; set; }
    }


    [WebMethod]
    public string CarregaListaEnderecoProvisorio(string idUsuario)
    {
        int usuario = Convert.ToInt32(idUsuario);
        var pedidosDc = new dbCommerceDataContext();
        var listaProdutos = (from c in pedidosDc.tbTransferenciaEnderecoProdutos
                             where c.tbTransferenciaEndereco.idUsuarioExpedicao == usuario && c.tbTransferenciaEndereco.dataFim == null && c.dataEnderecamento == null
                             select new RetornoListaEnderecoProvisorio
                             {
                                 idPedidoFornecedorItem = c.idPedidoFornecedorItem.ToString(),
                                 produtoNome = c.tbPedidoFornecedorItem.tbProduto.produtoNome,
                                 codigoProduto = c.tbPedidoFornecedorItem.codigoProduto
                             }).ToList();
        if (listaProdutos.Count > 1000) listaProdutos = listaProdutos.Take(1000).ToList();
        if (listaProdutos.Count == 0)
        {
            var enderecamentoPendente = (from c in pedidosDc.tbTransferenciaEnderecos
                                         where c.idUsuarioExpedicao == usuario && c.dataFim == null
                                         select c).FirstOrDefault();
            if (enderecamentoPendente != null)
            {
                enderecamentoPendente.dataFim = DateTime.Now;
                pedidosDc.SubmitChanges();
            }
        }
        foreach(var listaProduto in listaProdutos)
        {
            string etiquetaOculta = listaProduto.idPedidoFornecedorItem.ToString().Substring(0, 1);
            for (int i = 1; i < listaProduto.idPedidoFornecedorItem.ToString().Length - 1; i++)
            {
                etiquetaOculta += "x";
            }
            etiquetaOculta += listaProduto.idPedidoFornecedorItem.ToString().Substring(listaProduto.idPedidoFornecedorItem.ToString().Length - 1, 1);
            listaProduto.idPedidoFornecedorItem = etiquetaOculta;

        }
        JavaScriptSerializer js = new JavaScriptSerializer();
        var produtosJson = js.Serialize(listaProdutos);
        return produtosJson;
    }

    private bool TotalmenteSeparado(int pedidoId)
    {

        var produtosParaEmbalar = rnPedidos.retornaProdutosEtiqueta(pedidoId);
        var pedidosDc = new dbCommerceDataContext();
        var pedidoEnvio = (from c in pedidosDc.tbPedidoEnvios
                           where c.idPedido == pedidoId && c.dataFimSeparacao == null
                           orderby c.idPedidoEnvio descending
                           select c).FirstOrDefault();

        if (pedidoEnvio == null) return true;


        var totalProdutos = (from c in pedidosDc.tbProdutoEstoques where c.pedidoId == pedidoId && c.idPedidoEnvio == pedidoEnvio.idPedidoEnvio select c).Count();
        if (Convert.ToInt32(produtosParaEmbalar.Sum(x => x.itemQuantidade)) <= totalProdutos)
        {
            var pedido = (from c in pedidosDc.tbPedidos where c.pedidoId == pedidoId select c).First();
            pedido.separado = true;
            pedido.dataFimSeparacao = DateTime.Now;
            pedidoEnvio.dataFimSeparacao = DateTime.Now;
            pedidosDc.SubmitChanges();


            var queue = new tbQueue();
            queue.agendamento = DateTime.Now;
            queue.andamento = false;
            queue.concluido = false;
            queue.idRelacionado = pedidoId;
            queue.mensagem = "";
            queue.tipoQueue = 8;
            pedidosDc.tbQueues.InsertOnSubmit(queue);
            pedidosDc.SubmitChanges();
            return true;
        }

        return false;
    }

    [WebMethod]
    public string VerificaProduto(string idNumeroPedido, string idPedido)
    {
        int itemPedidoId = Convert.ToInt32(idNumeroPedido);
        int pedidoId = Convert.ToInt32(idPedido);

        var produtosParaEmbalar = rnPedidos.retornaProdutosEtiqueta(pedidoId);
        var pedidosDc = new dbCommerceDataContext();
        var produtoRomaneio = (from c in pedidosDc.tbProdutoEstoques where c.idPedidoFornecedorItem == itemPedidoId && c.pedidoId == null && c.enviado == false select c).FirstOrDefault();
        if (produtoRomaneio == null)
        {
            var produtoRomaneioSeparado = (from c in pedidosDc.tbProdutoEstoques
                                           where c.idPedidoFornecedorItem == itemPedidoId && c.pedidoId == pedidoId && c.enviado == false && c.idCentroDistribuicao < 4
                                           select c).FirstOrDefault();
            if (produtoRomaneioSeparado != null)
            {
                int idProdutoNoRomaneio = produtoRomaneioSeparado.produtoId;
                var produtosEmbalados =
                    (from c in pedidosDc.tbProdutoEstoques where c.pedidoId == pedidoId && c.dataEnvio == null select c)
                        .ToList();
                int totalSeparado =
                    produtosEmbalados.Count(x => x.produtoId == idProdutoNoRomaneio | x.produtoId == idProdutoNoRomaneio);
                int quantidadeTotal =
                    Convert.ToInt32(
                        produtosParaEmbalar.Where(
                            x => x.produtoId == idProdutoNoRomaneio | x.produtoAlternativoId == idProdutoNoRomaneio)
                            .Sum(x => x.itemQuantidade));

                if (totalSeparado == quantidadeTotal) return "1"; // Produto Separado ok
                if (totalSeparado > 0 && totalSeparado < quantidadeTotal) return "3"; // Produto separado, falta quantidade
            }
            else
            {
                var produtoRomaneioSeparadoOutro = (from c in pedidosDc.tbProdutoEstoques
                                                    where c.idPedidoFornecedorItem == itemPedidoId
                                                    select c).FirstOrDefault();
                if (produtoRomaneioSeparadoOutro == null)
                {
                    return "6"; // Produto não encontrado
                }
                else
                {
                    if (produtoRomaneioSeparadoOutro.enviado == false)
                    {
                        return "4"; // Produto separado para outro pedido
                    }
                    else
                    {
                        return "5"; // Produto Enviado
                    }
                }
            }
        }
        else
        {
            int idProdutoNoRomaneio = produtoRomaneio.produtoId;
            if (!produtosParaEmbalar.Any(x => x.produtoId == idProdutoNoRomaneio | x.produtoAlternativoId == idProdutoNoRomaneio)) return "0"; // Produto não faz parte do pedido

            var produtosEmbalados =
                (from c in pedidosDc.tbProdutoEstoques where c.pedidoId == pedidoId && c.dataEnvio == null && c.idCentroDistribuicao < 4 select c)
                    .ToList();
            int totalSeparado =
                produtosEmbalados.Count(x => x.produtoId == idProdutoNoRomaneio | x.produtoId == idProdutoNoRomaneio);
            int quantidadeTotal =
                Convert.ToInt32(
                    produtosParaEmbalar.Where(
                        x => x.produtoId == idProdutoNoRomaneio | x.produtoAlternativoId == idProdutoNoRomaneio)
                        .Sum(x => x.itemQuantidade));

            if (totalSeparado == quantidadeTotal) return "1"; // Produto Separado ok

            if ((produtoRomaneio.liberado ?? false) == false) return "6"; // Produto não está endereçado

            //if (produtosEmbalados.Count(x => (x.produtoId == idProdutoNoRomaneio | x.produtoId == idProdutoNoRomaneio) && x.idEnderecamentoArea == null) > 0)
            //{
            //    var produtosSemEnderecamento = produtosEmbalados.Where(x => (x.produtoId == idProdutoNoRomaneio | x.produtoId == idProdutoNoRomaneio) && x.idEnderecamentoArea == null);

            //    foreach (var produto in produtosSemEnderecamento)
            //    {
            //        produto.idEnderecamentoArea = 0;
            //        produto.idEnderecamentoRua = 0;
            //        produto.idEnderecamentoPredio = 0;
            //        produto.idEnderecamentoAndar = 0;
            //    }
            //pedidosDc.SubmitChanges();
            //    return "6"; // Produto não está endereçado
            //}


        }

        return "2"; // Produto OK, separar
    }

    
    [WebMethod]
    public string VerificaProdutoV2(string idNumeroPedido, string idPedido)
    {
        int itemPedidoId = Convert.ToInt32(idNumeroPedido);
        int itemPedidoFornecedorId = Convert.ToInt32(idNumeroPedido);

        int pedidoId = Convert.ToInt32(idPedido);
        var data = new dbCommerceDataContext();
        var produtosParaEmbalar = rnPedidos.retornaProdutosEtiqueta(pedidoId);

        var produtosEstoqueGeral = (from c in data.tbProdutoEstoques
                                   where c.idPedidoFornecedorItem == 0 && c.idCentroDistribuicao < 4
                                    select new
                                   {
                                       c.idPedidoFornecedorItem,
                                       c.pedidoId,
                                       c.enviado,
                                       c.produtoId,
                                       c.dataEnvio,
                                       c.liberado,
                                       pedidoEtiqueta = c.tbPedidoFornecedorItem.idPedido
                                   }).ToList();

        using (var txn = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required,
            new System.Transactions.TransactionOptions
            {
                IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
            }))

        {

            produtosEstoqueGeral =
               (from c in data.tbProdutoEstoques
                where (c.idPedidoFornecedorItem == itemPedidoId | c.pedidoId == pedidoId) && c.idCentroDistribuicao < 4
                select new {
                    c.idPedidoFornecedorItem,
                    c.pedidoId,
                    c.enviado,
                    c.produtoId,
                    c.dataEnvio,
                    c.liberado,
                    pedidoEtiqueta = c.tbPedidoFornecedorItem.idPedido
                }).ToList();
        }
        var produtoRomaneio = (from c in produtosEstoqueGeral where c.idPedidoFornecedorItem == itemPedidoId && c.pedidoId == null && c.enviado == false select c).FirstOrDefault();
        if (produtoRomaneio == null)
        {
            var produtoRomaneioSeparado = (from c in produtosEstoqueGeral
                                           where c.idPedidoFornecedorItem == itemPedidoId && c.pedidoId == pedidoId && c.enviado == false
                                           select c).FirstOrDefault();
            if (produtoRomaneioSeparado != null)
            {
                int idProdutoNoRomaneio = produtoRomaneioSeparado.produtoId;
                var produtosEmbalados =
                    (from c in produtosEstoqueGeral where c.pedidoId == pedidoId && c.dataEnvio == null select c)
                        .ToList();
                int totalSeparado =
                    produtosEmbalados.Count(x => x.produtoId == idProdutoNoRomaneio | x.produtoId == idProdutoNoRomaneio);
                int quantidadeTotal =
                    Convert.ToInt32(
                        produtosParaEmbalar.Where(
                            x => x.produtoId == idProdutoNoRomaneio | x.produtoAlternativoId == idProdutoNoRomaneio)
                            .Sum(x => x.itemQuantidade));

                if (totalSeparado == quantidadeTotal) return "1"; // Produto Separado ok
                if (totalSeparado > 0 && totalSeparado < quantidadeTotal) return "3"; // Produto separado, falta quantidade
            }
            else
            {
                var produtoRomaneioSeparadoOutro = (from c in produtosEstoqueGeral
                                                    where c.idPedidoFornecedorItem == itemPedidoId
                                                    select c).FirstOrDefault();
                if (produtoRomaneioSeparadoOutro == null)
                {
                    return "6"; // Produto não encontrado
                }
                else
                {
                    if (produtoRomaneioSeparadoOutro.enviado == false)
                    {
                        return "4"; // Produto separado para outro pedido
                    }
                    else
                    {
                        return "5"; // Produto Enviado
                    }
                }
            }
        }
        else
        {
            int idProdutoNoRomaneio = produtoRomaneio.produtoId;
            if (!produtosParaEmbalar.Any(x => x.produtoId == idProdutoNoRomaneio | x.produtoAlternativoId == idProdutoNoRomaneio)) return "0"; // Produto não faz parte do pedido

            var produtosEmbalados =
                (from c in produtosEstoqueGeral where c.pedidoId == pedidoId && c.dataEnvio == null select c)
                    .ToList();
            int totalSeparado =
                produtosEmbalados.Count(x => x.produtoId == idProdutoNoRomaneio | x.produtoId == idProdutoNoRomaneio);
            int quantidadeTotal =
                Convert.ToInt32(
                    produtosParaEmbalar.Where(
                        x => x.produtoId == idProdutoNoRomaneio | x.produtoAlternativoId == idProdutoNoRomaneio)
                        .Sum(x => x.itemQuantidade));

            if (totalSeparado == quantidadeTotal) return "1"; // Produto Separado ok

            if ((produtoRomaneio.liberado ?? false) == false) return "6"; // Produto não está endereçado

            if(produtoRomaneio.pedidoId != null)
            {
                if(produtoRomaneio.pedidoId != pedidoId)
                {
                    return "4";
                }
            }

        }

        //return "2"; // Produto OK, separar



        var pedidoEnvio = (from c in data.tbPedidoEnvios
                           where c.idPedido == pedidoId && c.dataFimEmbalagem == null && c.idCentroDeDistribuicao < 4
                           orderby c.idPedidoEnvio descending
                           select c).FirstOrDefault();
        if (pedidoEnvio == null) return "7";

        var produto =
            (from c in produtosEstoqueGeral
             where c.idPedidoFornecedorItem == itemPedidoFornecedorId
             select c)
                .First();
        var produtosSeparados =
            (from c in produtosEstoqueGeral
             where c.pedidoId == pedidoId && c.produtoId == produto.produtoId
             select c).ToList();

        var produtosParaSeparar =
            (from c in data.tbProdutoEstoques
             where c.pedidoId == null && c.produtoId == produto.produtoId && c.pedidoIdReserva == pedidoId && c.idCentroDistribuicao < 4
             select c).ToList();
        var itensProdutoPedido = (from c in produtosParaEmbalar where c.produtoId == produto.produtoId select c);
        bool itemPedidoIdSeparado = false;

        var itemParaSeparar = produtosParaSeparar.FirstOrDefault();
        if (itemParaSeparar != null)
        {
            if(itemParaSeparar.tbPedidoFornecedorItem.idPedido != null)
            {
                if(itemParaSeparar.tbPedidoFornecedorItem.idPedido != produtoRomaneio.pedidoEtiqueta) return "7";
            }
            bool? isErro = false;
            string erroMessage = "";
            var separar = data.v1_separaProduto(itemPedidoFornecedorId, pedidoId, itemParaSeparar.itemPedidoIdReserva, itemParaSeparar.idItemPedidoEstoqueReserva, ref isErro, ref erroMessage);
            itemPedidoIdSeparado = true;
        }

        var pedidosDcAltera = new dbCommerceDataContext();
        var produtoAltera =
            (from c in pedidosDcAltera.tbProdutoEstoques
             where c.idPedidoFornecedorItem == itemPedidoFornecedorId
             select c)
                .First();
        produtoAltera.idPedidoEnvio = pedidoEnvio.idPedidoEnvio;
        if (produtoAltera.idCentroDistribuicao == 2) produtoAltera.conferidoEmbalagem = true;
        pedidosDcAltera.SubmitChanges();

        var totalProdutos = (from c in data.tbProdutoEstoques where c.pedidoId == pedidoId && c.idPedidoEnvio == pedidoEnvio.idPedidoEnvio select c).Count();
        if (Convert.ToInt32(produtosParaEmbalar.Sum(x => x.itemQuantidade)) <= totalProdutos)
        {
            return "2_2";
        }

        return "2_1";

    }

    [WebMethod]
    public string VerificaProdutoV2cds(string idNumeroPedido, string idPedido, string idUsuario)
    {

        int idUsuarioSeparacao = Convert.ToInt32(idUsuario);
        var data = new dbCommerceDataContext();
        var usuario = (from c in data.tbUsuarioExpedicaos where c.idUsuarioExpedicao == idUsuarioSeparacao select c).FirstOrDefault();
        int idCentroDistribuicao = usuario.idCentroDistribuicao;

        int itemPedidoId = Convert.ToInt32(idNumeroPedido);
        int itemPedidoFornecedorId = Convert.ToInt32(idNumeroPedido);

        int pedidoId = Convert.ToInt32(idPedido);
        var produtosParaEmbalar = rnPedidos.retornaProdutosEtiqueta(pedidoId, idCentroDistribuicao);

        var produtosEstoqueGeral = (from c in data.tbProdutoEstoques
                                    where c.idPedidoFornecedorItem == 0
                                    select new
                                    {
                                        c.idPedidoFornecedorItem,
                                        c.pedidoId,
                                        c.enviado,
                                        c.produtoId,
                                        c.dataEnvio,
                                        c.liberado,
                                        pedidoEtiqueta = c.tbPedidoFornecedorItem.idPedido
                                    }).ToList();

        using (var txn = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required,
            new System.Transactions.TransactionOptions
            {
                IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
            }))

        {

            produtosEstoqueGeral =
               (from c in data.tbProdutoEstoques
                where c.idPedidoFornecedorItem == itemPedidoId | c.pedidoId == pedidoId
                select new
                {
                    c.idPedidoFornecedorItem,
                    c.pedidoId,
                    c.enviado,
                    c.produtoId,
                    c.dataEnvio,
                    c.liberado,
                    pedidoEtiqueta = c.tbPedidoFornecedorItem.idPedido
                }).ToList();
        }
        var produtoRomaneio = (from c in produtosEstoqueGeral where c.idPedidoFornecedorItem == itemPedidoId && c.pedidoId == null && c.enviado == false select c).FirstOrDefault();
        if (produtoRomaneio == null)
        {
            var produtoRomaneioSeparado = (from c in produtosEstoqueGeral
                                           where c.idPedidoFornecedorItem == itemPedidoId && c.pedidoId == pedidoId && c.enviado == false
                                           select c).FirstOrDefault();
            if (produtoRomaneioSeparado != null)
            {
                int idProdutoNoRomaneio = produtoRomaneioSeparado.produtoId;
                var produtosEmbalados =
                    (from c in produtosEstoqueGeral where c.pedidoId == pedidoId && c.dataEnvio == null select c)
                        .ToList();
                int totalSeparado =
                    produtosEmbalados.Count(x => x.produtoId == idProdutoNoRomaneio | x.produtoId == idProdutoNoRomaneio);
                int quantidadeTotal =
                    Convert.ToInt32(
                        produtosParaEmbalar.Where(
                            x => x.produtoId == idProdutoNoRomaneio | x.produtoAlternativoId == idProdutoNoRomaneio)
                            .Sum(x => x.itemQuantidade));


                if (totalSeparado == quantidadeTotal)
                {
                    rnLog.InsereLogEtiqueta(usuario.nome, itemPedidoId, pedidoId, "Produto separado para pedido");
                    return "1"; // Produto Separado ok
                }
                if (totalSeparado > 0 && totalSeparado < quantidadeTotal)
                {
                    rnLog.InsereLogEtiqueta(usuario.nome, itemPedidoId, pedidoId, "Produto separado para pedido");
                    return "3"; // Produto separado, falta quantidade
                }
            }
            else
            {
                var produtoRomaneioSeparadoOutro = (from c in produtosEstoqueGeral
                                                    where c.idPedidoFornecedorItem == itemPedidoId
                                                    select c).FirstOrDefault();
                if (produtoRomaneioSeparadoOutro == null)
                {
                    rnLog.InsereLogEtiqueta(usuario.nome, itemPedidoId, pedidoId, "Produto não encontrado ao separar para pedido");
                    return "6"; // Produto não encontrado
                }
                else
                {
                    if (produtoRomaneioSeparadoOutro.enviado == false)
                    {
                        rnLog.InsereLogEtiqueta(usuario.nome, itemPedidoId, pedidoId, "Tentativa de separar produto já separado para outro pedido");
                        return "4"; // Produto separado para outro pedido
                    }
                    else
                    {
                        rnLog.InsereLogEtiqueta(usuario.nome, itemPedidoId, pedidoId, "Tentativa de separar produto enviado");
                        return "5"; // Produto Enviado
                    }
                }
            }
        }
        else
        {
            int idProdutoNoRomaneio = produtoRomaneio.produtoId;
            if (!produtosParaEmbalar.Any(x => x.produtoId == idProdutoNoRomaneio | x.produtoAlternativoId == idProdutoNoRomaneio))
            {
                rnLog.InsereLogEtiqueta(usuario.nome, itemPedidoId, pedidoId, "Tentativa de separar produto que não faz parte do pedido");
                return "0"; // Produto não faz parte do pedido
            }

            var produtosEmbalados =
                (from c in produtosEstoqueGeral where c.pedidoId == pedidoId && c.dataEnvio == null select c)
                    .ToList();
            int totalSeparado =
                produtosEmbalados.Count(x => x.produtoId == idProdutoNoRomaneio | x.produtoId == idProdutoNoRomaneio);
            int quantidadeTotal =
                Convert.ToInt32(
                    produtosParaEmbalar.Where(
                        x => x.produtoId == idProdutoNoRomaneio | x.produtoAlternativoId == idProdutoNoRomaneio)
                        .Sum(x => x.itemQuantidade));

            if (totalSeparado == quantidadeTotal)
            {
                rnLog.InsereLogEtiqueta(usuario.nome, itemPedidoId, pedidoId, "Produto separado para pedido");
                return "1"; // Produto Separado ok
            }

            if ((produtoRomaneio.liberado ?? false) == false)
            {
                rnLog.InsereLogEtiqueta(usuario.nome, itemPedidoId, pedidoId, "Tentativa de separar produto que não está endereçado");
                return "6"; // Produto não está endereçado
            }

            if (produtoRomaneio.pedidoId != null)
            {
                if (produtoRomaneio.pedidoId != pedidoId)
                {
                    rnLog.InsereLogEtiqueta(usuario.nome, itemPedidoId, pedidoId, "Tentativa de separar produto já separado para outro pedido");
                    return "4";
                }
            }

        }

        //return "2"; // Produto OK, separar



        var pedidoEnvio = (from c in data.tbPedidoEnvios
                           where c.idPedido == pedidoId && c.dataFimEmbalagem == null
                           orderby c.idPedidoEnvio descending
                           select c).FirstOrDefault();
        if (pedidoEnvio == null) {

            rnLog.InsereLogEtiqueta(usuario.nome, itemPedidoId, pedidoId, "Tentativa de separar produto específico para outro pedido");
            return "7";
        }

        var produto =
            (from c in produtosEstoqueGeral
             where c.idPedidoFornecedorItem == itemPedidoFornecedorId
             select c)
                .First();
        var produtosSeparados =
            (from c in produtosEstoqueGeral
             where c.pedidoId == pedidoId && c.produtoId == produto.produtoId
             select c).ToList();

        var produtosParaSeparar =
            (from c in data.tbProdutoEstoques
             where c.pedidoId == null && c.produtoId == produto.produtoId && c.pedidoIdReserva == pedidoId
             select c).ToList();
        var itensProdutoPedido = (from c in produtosParaEmbalar where c.produtoId == produto.produtoId select c);
        bool itemPedidoIdSeparado = false;

        var itemParaSeparar = produtosParaSeparar.FirstOrDefault();
        if (itemParaSeparar != null)
        {
            if (itemParaSeparar.tbPedidoFornecedorItem.idPedido != null)
            {
                if (itemParaSeparar.tbPedidoFornecedorItem.idPedido != produtoRomaneio.pedidoEtiqueta) return "7";
            }
            bool? isErro = false;
            string erroMessage = "";

            var separar = data.v1_separaProduto(itemPedidoFornecedorId, pedidoId, itemParaSeparar.itemPedidoIdReserva, itemParaSeparar.idItemPedidoEstoqueReserva, ref isErro, ref erroMessage);
            itemPedidoIdSeparado = true;

            if (isErro == false)
            {
                rnLog.InsereLogEtiqueta(usuario.nome, itemPedidoId, pedidoId, "Produto separado para pedido");
                var pedidosDcAltera = new dbCommerceDataContext();
                var produtoAltera =
                    (from c in pedidosDcAltera.tbProdutoEstoques
                     where c.idPedidoFornecedorItem == itemPedidoFornecedorId
                     select c)
                        .First();
                produtoAltera.idPedidoEnvio = pedidoEnvio.idPedidoEnvio;
                if (produtoAltera.idCentroDistribuicao == 2 | produtoAltera.idCentroDistribuicao == 5) produtoAltera.conferidoEmbalagem = true;
                pedidosDcAltera.SubmitChanges();
            }
            else
            {
                rnLog.InsereLogEtiqueta(usuario.nome, itemPedidoId, pedidoId, "Falha ao separar produto para pedido");
            }

        }


        var totalProdutos = (from c in data.tbProdutoEstoques where c.pedidoId == pedidoId && c.idPedidoEnvio == pedidoEnvio.idPedidoEnvio && c.idCentroDistribuicao == idCentroDistribuicao select c).Count();
        if (Convert.ToInt32(produtosParaEmbalar.Sum(x => x.itemQuantidade)) <= totalProdutos)
        {
            if (usuario.idCentroDistribuicao == 5)
            {
                var pedido = (from c in data.tbPedidos where c.pedidoId == pedidoEnvio.idPedido select c).First();
                pedido.separadoCd5 = true;
                pedidoEnvio.dataFimSeparacaoCd2 = DateTime.Now;
                if(pedidoEnvio.idEnderecoSeparacao == null)
                {
                    pedidoEnvio.idEnderecoSeparacao = 124;
                }
                if (pedidoEnvio.dataFimSeparacao == null)
                {
                    pedidoEnvio.dataFimSeparacao = DateTime.Now;
                }
                data.SubmitChanges();
            }
            
            return "2_2";
        }

        return "2_1";

    }




    [WebMethod]
    public string VerificaProdutoLote(string idPedidoFornecedorItem, string idUsuario)
    {

        int idUsuarioSeparacao = Convert.ToInt32(idUsuario);
        var data = new dbCommerceDataContext();
        var usuario = (from c in data.tbUsuarioExpedicaos where c.idUsuarioExpedicao == idUsuarioSeparacao select c).FirstOrDefault();
        int idCentroDistribuicao = usuario.idCentroDistribuicao;
        int etiqueta = Convert.ToInt32(idPedidoFornecedorItem);

        var envios = (from c in data.tbPedidoEnvios where (c.idUsuarioSeparacao == idUsuarioSeparacao | c.idUsuarioSeparacaoCd2 == idUsuarioSeparacao) && (c.dataFimSeparacao == null | c.dataFimSeparacaoCd2 == null) select c).ToList();
        var pedidos = envios.Select(x => x.idPedido).ToList();

        var produtosParaEmbalar = rnPedidos.retornaProdutosEtiquetaLote(pedidos, idCentroDistribuicao);

        var produtosEstoqueGeral = (from c in data.tbProdutoEstoques
                                    where c.idPedidoFornecedorItem == 0
                                    select new
                                    {
                                        c.idPedidoFornecedorItem,
                                        c.pedidoId,
                                        c.enviado,
                                        c.produtoId,
                                        c.dataEnvio,
                                        c.liberado,
                                        pedidoEtiqueta = c.tbPedidoFornecedorItem.idPedido
                                    }).ToList();

        using (var txn = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required,
            new System.Transactions.TransactionOptions
            {
                IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
            }))

        {

            produtosEstoqueGeral =
               (from c in data.tbProdutoEstoques
                where c.idPedidoFornecedorItem == etiqueta | pedidos.Contains(c.pedidoId ?? -1)
                select new
                {
                    c.idPedidoFornecedorItem,
                    c.pedidoId,
                    c.enviado,
                    c.produtoId,
                    c.dataEnvio,
                    c.liberado,
                    pedidoEtiqueta = c.tbPedidoFornecedorItem.idPedido
                }).ToList();
        }
        var produtoRomaneio = (from c in produtosEstoqueGeral where c.idPedidoFornecedorItem == etiqueta && c.pedidoId == null && c.enviado == false select c).FirstOrDefault();
        if (produtoRomaneio == null)
        {
            var produtoRomaneioSeparado = (from c in produtosEstoqueGeral
                                           where c.idPedidoFornecedorItem == etiqueta && pedidos.Contains(c.pedidoId ?? -1) && c.enviado == false
                                           select c).FirstOrDefault();
            if (produtoRomaneioSeparado != null)
            {
                int idProdutoNoRomaneio = produtoRomaneioSeparado.produtoId;
                var produtosEmbalados =
                    (from c in produtosEstoqueGeral where pedidos.Contains(c.pedidoId ?? -1) && c.dataEnvio == null select c)
                        .ToList();
                int totalSeparado =
                    produtosEmbalados.Count(x => x.produtoId == idProdutoNoRomaneio | x.produtoId == idProdutoNoRomaneio);
                int quantidadeTotal =
                    Convert.ToInt32(
                        produtosParaEmbalar.Where(
                            x => x.produtoId == idProdutoNoRomaneio | x.produtoAlternativoId == idProdutoNoRomaneio)
                            .Sum(x => x.itemQuantidade));


                if (totalSeparado == quantidadeTotal)
                {
                    rnLog.InsereLogEtiqueta(usuario.nome, etiqueta, "Produto separado para pedido");
                    return "1"; // Produto Separado ok
                }
                if (totalSeparado > 0 && totalSeparado < quantidadeTotal)
                {
                    rnLog.InsereLogEtiqueta(usuario.nome, etiqueta, "Produto separado para pedido");
                    return "3"; // Produto separado, falta quantidade
                }
            }
            else
            {
                var produtoRomaneioSeparadoOutro = (from c in produtosEstoqueGeral
                                                    where c.idPedidoFornecedorItem == etiqueta
                                                    select c).FirstOrDefault();
                if (produtoRomaneioSeparadoOutro == null)
                {
                    rnLog.InsereLogEtiqueta(usuario.nome, etiqueta, "Produto não encontrado ao separar para pedido");
                    return "6"; // Produto não encontrado
                }
                else
                {
                    if (produtoRomaneioSeparadoOutro.enviado == false)
                    {
                        rnLog.InsereLogEtiqueta(usuario.nome, etiqueta, "Tentativa de separar produto já separado para outro pedido");
                        return "4"; // Produto separado para outro pedido
                    }
                    else
                    {
                        rnLog.InsereLogEtiqueta(usuario.nome, etiqueta, "Tentativa de separar produto enviado");
                        return "5"; // Produto Enviado
                    }
                }
            }
        }
        else
        {
            int idProdutoNoRomaneio = produtoRomaneio.produtoId;
            if (!produtosParaEmbalar.Any(x => x.produtoId == idProdutoNoRomaneio | x.produtoAlternativoId == idProdutoNoRomaneio))
            {
                rnLog.InsereLogEtiqueta(usuario.nome, etiqueta, "Tentativa de separar produto que não faz parte do pedido");
                return "0"; // Produto não faz parte do pedido
            }

            var produtosEmbalados =
                (from c in produtosEstoqueGeral where pedidos.Contains(c.pedidoId ?? -1) && c.dataEnvio == null select c)
                    .ToList();
            int totalSeparado =
                produtosEmbalados.Count(x => x.produtoId == idProdutoNoRomaneio | x.produtoId == idProdutoNoRomaneio);
            int quantidadeTotal =
                Convert.ToInt32(
                    produtosParaEmbalar.Where(
                        x => x.produtoId == idProdutoNoRomaneio | x.produtoAlternativoId == idProdutoNoRomaneio)
                        .Sum(x => x.itemQuantidade));

            if (totalSeparado == quantidadeTotal)
            {
                rnLog.InsereLogEtiqueta(usuario.nome, etiqueta, "Produto separado para pedido");
                return "1"; // Produto Separado ok
            }

            if ((produtoRomaneio.liberado ?? false) == false)
            {
                rnLog.InsereLogEtiqueta(usuario.nome, etiqueta, "Tentativa de separar produto que não está endereçado");
                return "6"; // Produto não está endereçado
            }

            if (produtoRomaneio.pedidoId != null)
            {
                if (!pedidos.Contains(produtoRomaneio.pedidoId ?? -1))
                {
                    rnLog.InsereLogEtiqueta(usuario.nome, etiqueta, "Tentativa de separar produto já separado para outro pedido");
                    return "4";
                }
            }

        }

        //return "2"; // Produto OK, separar



        /*var pedidoEnvio = (from c in data.tbPedidoEnvios
                           where c.idPedido == pedidoId && c.dataFimEmbalagem == null
                           orderby c.idPedidoEnvio descending
                           select c).FirstOrDefault();
        if (pedidoEnvio == null)
        {

            rnLog.InsereLogEtiqueta(usuario.nome, itemPedidoId, pedidoId, "Tentativa de separar produto específico para outro pedido");
            return "7";
        }*/

        var produto =
            (from c in produtosEstoqueGeral
             where c.idPedidoFornecedorItem == etiqueta
             select c)
                .First();
        var produtosSeparados =
            (from c in produtosEstoqueGeral
             where pedidos.Contains(c.pedidoId ?? -1) && c.produtoId == produto.produtoId
             select c).ToList();

        var produtosParaSeparar =
            (from c in data.tbProdutoEstoques
             where c.pedidoId == null && c.produtoId == produto.produtoId && pedidos.Contains(c.pedidoIdReserva ?? -1)
             select c).ToList();
        var itensProdutoPedido = (from c in produtosParaEmbalar where c.produtoId == produto.produtoId select c);
        bool itemPedidoIdSeparado = false;

        var itemParaSeparar = produtosParaSeparar.FirstOrDefault();
        if (itemParaSeparar != null)
        {
            if (itemParaSeparar.tbPedidoFornecedorItem.idPedido != null)
            {
                if (itemParaSeparar.tbPedidoFornecedorItem.idPedido != produtoRomaneio.pedidoEtiqueta) return "7";
            }
            bool? isErro = false;
            string erroMessage = "";

            var separar = data.v1_separaProduto(etiqueta, itemParaSeparar.pedidoIdReserva, itemParaSeparar.itemPedidoIdReserva, itemParaSeparar.idItemPedidoEstoqueReserva, ref isErro, ref erroMessage);
            itemPedidoIdSeparado = true;

            if (isErro == false)
            {
                var envio = envios.Where(x => x.idPedido == itemParaSeparar.pedidoIdReserva).First();
                rnLog.InsereLogEtiqueta(usuario.nome, etiqueta, itemParaSeparar.pedidoIdReserva ?? 0, "Produto separado para pedido");
                var pedidosDcAltera = new dbCommerceDataContext();
                var produtoAltera =
                    (from c in pedidosDcAltera.tbProdutoEstoques
                     where c.idPedidoFornecedorItem == etiqueta
                     select c)
                        .First();
                produtoAltera.idPedidoEnvio = envio.idPedidoEnvio;
                if (produtoAltera.idCentroDistribuicao == 2 | produtoAltera.idCentroDistribuicao == 5) produtoAltera.conferidoEmbalagem = true;
                pedidosDcAltera.SubmitChanges();
            }
            else
            {
                rnLog.InsereLogEtiqueta(usuario.nome, etiqueta, itemParaSeparar.pedidoIdReserva ?? 0, "Falha ao separar produto para pedido");
            }

        }

        var idsEnvios = envios.Select(x => x.idPedidoEnvio);
        var totalProdutos = (from c in data.tbProdutoEstoques where pedidos.Contains(c.pedidoId ?? -1) && idsEnvios.Contains(c.idPedidoEnvio ?? -1) && c.idCentroDistribuicao == idCentroDistribuicao select c).Count();
        if (Convert.ToInt32(produtosParaEmbalar.Sum(x => x.itemQuantidade)) <= totalProdutos)
        {
            if (usuario.idCentroDistribuicao == 5)
            {
                foreach (var envio in envios)
                {
                    var pedido = (from c in data.tbPedidos where c.pedidoId == envio.idPedido select c).First();
                    pedido.separadoCd5 = true;
                    envio.dataFimSeparacaoCd2 = DateTime.Now;
                    if (envio.idEnderecoSeparacao == null)
                    {
                        envio.idEnderecoSeparacao = 124;
                    }
                    if (envio.dataFimSeparacao == null)
                    {
                        envio.dataFimSeparacao = DateTime.Now;
                    }
                    data.SubmitChanges();
                }
            }

            return "2_2";
        }

        return "2_1";

    }



    [WebMethod]
    public string VerificaProdutoV2Cd2(string idNumeroPedido, string idPedido)
    {
        int itemPedidoId = Convert.ToInt32(idNumeroPedido);
        int itemPedidoFornecedorId = Convert.ToInt32(idNumeroPedido);

        int pedidoId = Convert.ToInt32(idPedido);
        var data = new dbCommerceDataContext();
        var produtosParaEmbalar = rnPedidos.retornaProdutosEtiqueta(pedidoId, 2);

        List<tbProdutoEstoque> produtosEstoqueGeral;

        using (var txn = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required,
            new System.Transactions.TransactionOptions
            {
                IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
            }))

        {

            produtosEstoqueGeral =
               (from c in data.tbProdutoEstoques
                where (c.idPedidoFornecedorItem == itemPedidoId | c.pedidoId == pedidoId) && c.idCentroDistribuicao == 2
                select c).ToList();
        }
        var produtoRomaneio = (from c in produtosEstoqueGeral where c.idPedidoFornecedorItem == itemPedidoId && c.pedidoId == null && c.enviado == false select c).FirstOrDefault();
        if (produtoRomaneio == null)
        {
            var produtoRomaneioSeparado = (from c in produtosEstoqueGeral
                                           where c.idPedidoFornecedorItem == itemPedidoId && c.pedidoId == pedidoId && c.enviado == false
                                           select c).FirstOrDefault();
            if (produtoRomaneioSeparado != null)
            {
                int idProdutoNoRomaneio = produtoRomaneioSeparado.produtoId;
                var produtosEmbalados =
                    (from c in produtosEstoqueGeral where c.pedidoId == pedidoId && c.dataEnvio == null select c)
                        .ToList();
                int totalSeparado =
                    produtosEmbalados.Count(x => x.produtoId == idProdutoNoRomaneio | x.produtoId == idProdutoNoRomaneio);
                int quantidadeTotal =
                    Convert.ToInt32(
                        produtosParaEmbalar.Where(
                            x => x.produtoId == idProdutoNoRomaneio | x.produtoAlternativoId == idProdutoNoRomaneio)
                            .Sum(x => x.itemQuantidade));

                if (totalSeparado == quantidadeTotal) return "1"; // Produto Separado ok
                if (totalSeparado > 0 && totalSeparado < quantidadeTotal) return "3"; // Produto separado, falta quantidade
            }
            else
            {
                var produtoRomaneioSeparadoOutro = (from c in produtosEstoqueGeral
                                                    where c.idPedidoFornecedorItem == itemPedidoId
                                                    select c).FirstOrDefault();
                if (produtoRomaneioSeparadoOutro == null)
                {
                    return "6"; // Produto não encontrado
                }
                else
                {
                    if (produtoRomaneioSeparadoOutro.enviado == false)
                    {
                        return "4"; // Produto separado para outro pedido
                    }
                    else
                    {
                        return "5"; // Produto Enviado
                    }
                }
            }
        }
        else
        {
            int idProdutoNoRomaneio = produtoRomaneio.produtoId;
            if (!produtosParaEmbalar.Any(x => x.produtoId == idProdutoNoRomaneio | x.produtoAlternativoId == idProdutoNoRomaneio)) return "0"; // Produto não faz parte do pedido

            var produtosEmbalados =
                (from c in produtosEstoqueGeral where c.pedidoId == pedidoId && c.dataEnvio == null select c)
                    .ToList();
            int totalSeparado =
                produtosEmbalados.Count(x => x.produtoId == idProdutoNoRomaneio | x.produtoId == idProdutoNoRomaneio);
            int quantidadeTotal =
                Convert.ToInt32(
                    produtosParaEmbalar.Where(
                        x => x.produtoId == idProdutoNoRomaneio | x.produtoAlternativoId == idProdutoNoRomaneio)
                        .Sum(x => x.itemQuantidade));

            if (totalSeparado == quantidadeTotal) return "1"; // Produto Separado ok

            if ((produtoRomaneio.liberado ?? false) == false) return "6"; // Produto não está endereçado
        }

        //return "2"; // Produto OK, separar



        var pedidoEnvio = (from c in data.tbPedidoEnvios
                           where c.idPedido == pedidoId && c.dataFimEmbalagem == null
                           orderby c.idPedidoEnvio descending
                           select c).FirstOrDefault();
        if (pedidoEnvio == null) return "0";

        var produto =
            (from c in produtosEstoqueGeral
             where c.idPedidoFornecedorItem == itemPedidoFornecedorId
             select c)
                .First();
        var produtosSeparados =
            (from c in produtosEstoqueGeral
             where c.pedidoId == pedidoId && c.produtoId == produto.produtoId
             select c).ToList();

        var produtosParaSeparar =
            (from c in data.tbProdutoEstoques
             where c.pedidoId == null && c.produtoId == produto.produtoId && c.pedidoIdReserva == pedidoId && c.idCentroDistribuicao == 2
             select c).ToList();
        var itensProdutoPedido = (from c in produtosParaEmbalar where c.produtoId == produto.produtoId select c);
        bool itemPedidoIdSeparado = false;

        var itemParaSeparar = produtosParaSeparar.FirstOrDefault();
        if (itemParaSeparar != null)
        {
            bool? isErro = false;
            string erroMessage = "";
            var separar = data.v1_separaProduto(itemPedidoFornecedorId, pedidoId, itemParaSeparar.itemPedidoIdReserva, itemParaSeparar.idItemPedidoEstoqueReserva, ref isErro, ref erroMessage);
            itemPedidoIdSeparado = true;
        }

        var pedidosDcAltera = new dbCommerceDataContext();
        var produtoAltera =
            (from c in pedidosDcAltera.tbProdutoEstoques
             where c.idPedidoFornecedorItem == itemPedidoFornecedorId
             select c)
                .First();
        produtoAltera.idPedidoEnvio = pedidoEnvio.idPedidoEnvio;
        if (produtoAltera.idCentroDistribuicao == 2) produtoAltera.conferidoEmbalagem = true;
        pedidosDcAltera.SubmitChanges();

        var totalProdutos = (from c in data.tbProdutoEstoques where c.pedidoId == pedidoId && c.idPedidoEnvio == pedidoEnvio.idPedidoEnvio && c.idCentroDistribuicao == 2 select c).Count();
        if (Convert.ToInt32(produtosParaEmbalar.Sum(x => x.itemQuantidade)) <= totalProdutos)
        {
            pedidoEnvio.dataFimSeparacaoCd2 = DateTime.Now;
            if (produtoAltera.idCentroDistribuicao == 2)
            {
                if (pedidoEnvio.dataFimSeparacao == null)
                {
                    pedidoEnvio.dataFimSeparacao = DateTime.Now;
                }
            }
            data.SubmitChanges();

            var queue = new tbQueue();
            queue.agendamento = DateTime.Now;
            queue.andamento = false;
            queue.concluido = false;
            queue.idRelacionado = pedidoId;
            queue.mensagem = "";
            queue.tipoQueue = 8;
            data.tbQueues.InsertOnSubmit(queue);
            data.SubmitChanges();

            return "2_2";
        }

        return "2_1";

    }



    [WebMethod]
    public string VerificaProdutoV2Cd3(string idNumeroPedido, string idPedido)
    {
        int itemPedidoId = Convert.ToInt32(idNumeroPedido);
        int itemPedidoFornecedorId = Convert.ToInt32(idNumeroPedido);

        int pedidoId = Convert.ToInt32(idPedido);
        var data = new dbCommerceDataContext();
        var produtosParaEmbalar = rnPedidos.retornaProdutosEtiqueta(pedidoId, 3);

        List<tbProdutoEstoque> produtosEstoqueGeral;

        using (var txn = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required,
            new System.Transactions.TransactionOptions
            {
                IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
            }))

        {

            produtosEstoqueGeral =
               (from c in data.tbProdutoEstoques
                where (c.idPedidoFornecedorItem == itemPedidoId | c.pedidoId == pedidoId) && c.idCentroDistribuicao == 3
                select c).ToList();
        }
        var produtoRomaneio = (from c in produtosEstoqueGeral where c.idPedidoFornecedorItem == itemPedidoId && c.pedidoId == null && c.enviado == false select c).FirstOrDefault();
        if (produtoRomaneio == null)
        {
            var produtoRomaneioSeparado = (from c in produtosEstoqueGeral
                                           where c.idPedidoFornecedorItem == itemPedidoId && c.pedidoId == pedidoId && c.enviado == false
                                           select c).FirstOrDefault();
            if (produtoRomaneioSeparado != null)
            {
                int idProdutoNoRomaneio = produtoRomaneioSeparado.produtoId;
                var produtosEmbalados =
                    (from c in produtosEstoqueGeral where c.pedidoId == pedidoId && c.dataEnvio == null select c)
                        .ToList();
                int totalSeparado =
                    produtosEmbalados.Count(x => x.produtoId == idProdutoNoRomaneio | x.produtoId == idProdutoNoRomaneio);
                int quantidadeTotal =
                    Convert.ToInt32(
                        produtosParaEmbalar.Where(
                            x => x.produtoId == idProdutoNoRomaneio | x.produtoAlternativoId == idProdutoNoRomaneio)
                            .Sum(x => x.itemQuantidade));

                if (totalSeparado == quantidadeTotal) return "1"; // Produto Separado ok
                if (totalSeparado > 0 && totalSeparado < quantidadeTotal) return "3"; // Produto separado, falta quantidade
            }
            else
            {
                var produtoRomaneioSeparadoOutro = (from c in produtosEstoqueGeral
                                                    where c.idPedidoFornecedorItem == itemPedidoId
                                                    select c).FirstOrDefault();
                if (produtoRomaneioSeparadoOutro == null)
                {
                    return "6"; // Produto não encontrado
                }
                else
                {
                    if (produtoRomaneioSeparadoOutro.enviado == false)
                    {
                        return "4"; // Produto separado para outro pedido
                    }
                    else
                    {
                        return "5"; // Produto Enviado
                    }
                }
            }
        }
        else
        {
            int idProdutoNoRomaneio = produtoRomaneio.produtoId;
            if (!produtosParaEmbalar.Any(x => x.produtoId == idProdutoNoRomaneio | x.produtoAlternativoId == idProdutoNoRomaneio)) return "0"; // Produto não faz parte do pedido

            var produtosEmbalados =
                (from c in produtosEstoqueGeral where c.pedidoId == pedidoId && c.dataEnvio == null select c)
                    .ToList();
            int totalSeparado =
                produtosEmbalados.Count(x => x.produtoId == idProdutoNoRomaneio | x.produtoId == idProdutoNoRomaneio);
            int quantidadeTotal =
                Convert.ToInt32(
                    produtosParaEmbalar.Where(
                        x => x.produtoId == idProdutoNoRomaneio | x.produtoAlternativoId == idProdutoNoRomaneio)
                        .Sum(x => x.itemQuantidade));

            if (totalSeparado == quantidadeTotal) return "1"; // Produto Separado ok

            if ((produtoRomaneio.liberado ?? false) == false) return "6"; // Produto não está endereçado
        }

        //return "2"; // Produto OK, separar



        var pedidoEnvio = (from c in data.tbPedidoEnvios
                           where c.idPedido == pedidoId && c.dataFimEmbalagem == null && c.idCentroDeDistribuicao == 3
                           orderby c.idPedidoEnvio descending
                           select c).FirstOrDefault();
        if (pedidoEnvio == null) return "0";

        var produto =
            (from c in produtosEstoqueGeral
             where c.idPedidoFornecedorItem == itemPedidoFornecedorId
             select c)
                .First();
        var produtosSeparados =
            (from c in produtosEstoqueGeral
             where c.pedidoId == pedidoId && c.produtoId == produto.produtoId
             select c).ToList();

        var produtosParaSeparar =
            (from c in data.tbProdutoEstoques
             where c.pedidoId == null && c.produtoId == produto.produtoId && c.pedidoIdReserva == pedidoId && c.idCentroDistribuicao == 3
             select c).ToList();
        var itensProdutoPedido = (from c in produtosParaEmbalar where c.produtoId == produto.produtoId select c);
        bool itemPedidoIdSeparado = false;

        var itemParaSeparar = produtosParaSeparar.FirstOrDefault();
        if (itemParaSeparar != null)
        {
            bool? isErro = false;
            string erroMessage = "";
            var separar = data.v1_separaProduto(itemPedidoFornecedorId, pedidoId, itemParaSeparar.itemPedidoIdReserva, itemParaSeparar.idItemPedidoEstoqueReserva, ref isErro, ref erroMessage);
            itemPedidoIdSeparado = true;
        }

        var pedidosDcAltera = new dbCommerceDataContext();
        var produtoAltera =
            (from c in pedidosDcAltera.tbProdutoEstoques
             where c.idPedidoFornecedorItem == itemPedidoFornecedorId
             select c)
                .First();
        produtoAltera.idPedidoEnvio = pedidoEnvio.idPedidoEnvio;
        if (produtoAltera.idCentroDistribuicao == 3) produtoAltera.conferidoEmbalagem = true;
        pedidosDcAltera.SubmitChanges();

        var totalProdutos = (from c in data.tbProdutoEstoques where c.pedidoId == pedidoId && c.idPedidoEnvio == pedidoEnvio.idPedidoEnvio && c.idCentroDistribuicao == 3 select c).Count();
        if (Convert.ToInt32(produtosParaEmbalar.Sum(x => x.itemQuantidade)) <= totalProdutos)
        {
            pedidoEnvio.dataFimSeparacaoCd2 = DateTime.Now;

            var pedido = (from c in data.tbPedidos where c.pedidoId == pedidoId select c).First();
            pedido.separadoCd3 = true;
            if (produtoAltera.idCentroDistribuicao == 3)
            {
                if (pedidoEnvio.dataFimSeparacao == null)
                {
                    pedidoEnvio.dataFimSeparacao = DateTime.Now;
                }
            }
            data.SubmitChanges();

            var queue = new tbQueue();
            queue.agendamento = DateTime.Now;
            queue.andamento = false;
            queue.concluido = false;
            queue.idRelacionado = pedidoId;
            queue.mensagem = "";
            queue.tipoQueue = 8;
            data.tbQueues.InsertOnSubmit(queue);
            data.SubmitChanges();

            return "2_2";
        }

        return "2_1";

    }


    [WebMethod]
    public string AdicionaProduto(string idNumeroPedido, string idPedido)
    {
        int itemPedidoFornecedorId = Convert.ToInt32(idNumeroPedido);
        int pedidoId = Convert.ToInt32(idPedido);
        var produtosParaEmbalar = rnPedidos.retornaProdutosEtiqueta(pedidoId);

        var pedidosDc = new dbCommerceDataContext();

        var pedidoEnvio = (from c in pedidosDc.tbPedidoEnvios
                           where c.idPedido == pedidoId && c.dataFimSeparacao == null
                           orderby c.idPedidoEnvio descending
                           select c).FirstOrDefault();
        var produtoRomaneio =
            (from c in pedidosDc.tbPedidoFornecedorItems
             where c.idPedidoFornecedorItem == itemPedidoFornecedorId
             select c).FirstOrDefault();
        if (produtoRomaneio == null) return "0";
        if (pedidoEnvio == null) return "0";

        var produto =
            (from c in pedidosDc.tbProdutoEstoques
             where c.idPedidoFornecedorItem == itemPedidoFornecedorId
             select c)
                .First();
        var produtosSeparados =
            (from c in pedidosDc.tbProdutoEstoques
             where c.pedidoId == pedidoId && c.produtoId == produto.produtoId
             select c).ToList();
        var produtosParaSeparar =
            (from c in pedidosDc.tbProdutoEstoques
             where c.pedidoId == null && c.produtoId == produto.produtoId && c.pedidoIdReserva == pedidoId
             select c).ToList();
        var itensProdutoPedido = (from c in produtosParaEmbalar where c.produtoId == produto.produtoId select c);
        bool itemPedidoIdSeparado = false;

        var itemParaSeparar = produtosParaSeparar.FirstOrDefault();
        if (itemParaSeparar != null)
        {
            bool? isErro = false;
            string erroMessage = "";
            var data = new dbCommerceDataContext();
            var separar = data.v1_separaProduto(itemPedidoFornecedorId, pedidoId, itemParaSeparar.itemPedidoIdReserva, itemParaSeparar.idItemPedidoEstoqueReserva, ref isErro, ref erroMessage);
            itemPedidoIdSeparado = true;
        }

        var pedidosDcAltera = new dbCommerceDataContext();
        var produtoAltera =
            (from c in pedidosDcAltera.tbProdutoEstoques
             where c.idPedidoFornecedorItem == itemPedidoFornecedorId
             select c)
                .First();
        produtoAltera.idPedidoEnvio = pedidoEnvio.idPedidoEnvio;
        pedidosDcAltera.SubmitChanges();

        var totalProdutos = (from c in pedidosDc.tbProdutoEstoques where c.pedidoId == pedidoId && c.idPedidoEnvio == pedidoEnvio.idPedidoEnvio select c).Count();
        if (Convert.ToInt32(produtosParaEmbalar.Sum(x => x.itemQuantidade)) <= totalProdutos)
        {
            //var pedido = (from c in pedidosDc.tbPedidos where c.pedidoId == pedidoId select c).First();
            //pedido.separado = true;
            //pedido.dataFimSeparacao = DateTime.Now;
            //pedidoEnvio.dataFimSeparacao = DateTime.Now;
            //pedidosDc.SubmitChanges();
            return "2";
        }

        return "1";
    }


    [WebMethod]
    public string AdicionaProdutoCds(string idNumeroPedido, string idPedido, string idUsuario)
    {
        var data = new dbCommerceDataContext();
        int idUsuarioSeparacao = Convert.ToInt32(idUsuario);
        var usuario = (from c in data.tbUsuarioExpedicaos where c.idUsuarioExpedicao == idUsuarioSeparacao select c).FirstOrDefault();
        int itemPedidoFornecedorId = Convert.ToInt32(idNumeroPedido);
        int pedidoId = Convert.ToInt32(idPedido);
        var produtosParaEmbalar = rnPedidos.retornaProdutosEtiqueta(pedidoId, usuario.idCentroDistribuicao);

        var pedidosDc = new dbCommerceDataContext();

        var pedidoEnvio = (from c in pedidosDc.tbPedidoEnvios
                           where c.idPedido == pedidoId && c.dataFimSeparacao == null && c.idCentroDeDistribuicao == usuario.idCentroDistribuicao
                           orderby c.idPedidoEnvio descending
                           select c).FirstOrDefault();
        var produtoRomaneio =
            (from c in pedidosDc.tbPedidoFornecedorItems
             where c.idPedidoFornecedorItem == itemPedidoFornecedorId
             select c).FirstOrDefault();
        if (produtoRomaneio == null) return "0";
        if (pedidoEnvio == null) return "0";

        var produto =
            (from c in pedidosDc.tbProdutoEstoques
             where c.idPedidoFornecedorItem == itemPedidoFornecedorId
             select c)
                .First();
        var produtosSeparados =
            (from c in pedidosDc.tbProdutoEstoques
             where c.pedidoId == pedidoId && c.produtoId == produto.produtoId
             select c).ToList();
        var produtosParaSeparar =
            (from c in pedidosDc.tbProdutoEstoques
             where c.pedidoId == null && c.produtoId == produto.produtoId && c.pedidoIdReserva == pedidoId && c.idCentroDistribuicao == usuario.idCentroDistribuicao
             select c).ToList();
        var itensProdutoPedido = (from c in produtosParaEmbalar where c.produtoId == produto.produtoId select c);
        bool itemPedidoIdSeparado = false;

        var itemParaSeparar = produtosParaSeparar.FirstOrDefault();
        if (itemParaSeparar != null)
        {
            bool? isErro = false;
            string erroMessage = "";
            var separar = data.v1_separaProduto(itemPedidoFornecedorId, pedidoId, itemParaSeparar.itemPedidoIdReserva, itemParaSeparar.idItemPedidoEstoqueReserva, ref isErro, ref erroMessage);
            itemPedidoIdSeparado = true;
        }

        var pedidosDcAltera = new dbCommerceDataContext();
        var produtoAltera =
            (from c in pedidosDcAltera.tbProdutoEstoques
             where c.idPedidoFornecedorItem == itemPedidoFornecedorId
             select c)
                .First();
        produtoAltera.idPedidoEnvio = pedidoEnvio.idPedidoEnvio;
        pedidosDcAltera.SubmitChanges();

        var totalProdutos = (from c in pedidosDc.tbProdutoEstoques where c.pedidoId == pedidoId && c.idPedidoEnvio == pedidoEnvio.idPedidoEnvio && c.idCentroDistribuicao == usuario.idCentroDistribuicao select c).Count();
        if (Convert.ToInt32(produtosParaEmbalar.Sum(x => x.itemQuantidade)) <= totalProdutos)
        {
            //var pedido = (from c in pedidosDc.tbPedidos where c.pedidoId == pedidoId select c).First();
            //pedido.separado = true;
            //pedido.dataFimSeparacao = DateTime.Now;
            //pedidoEnvio.dataFimSeparacao = DateTime.Now;
            //pedidosDc.SubmitChanges();
            
            return "2";
        }

        return "1";
    }

    [WebMethod]
    public string AdicionaProdutoCd2(string idNumeroPedido, string idPedido)
    {
        int itemPedidoFornecedorId = Convert.ToInt32(idNumeroPedido);
        int pedidoId = Convert.ToInt32(idPedido);
        var produtosParaEmbalar = rnPedidos.retornaProdutosEtiqueta(pedidoId, 2);

        var pedidosDc = new dbCommerceDataContext();

        var pedidoEnvio = (from c in pedidosDc.tbPedidoEnvios
                           where c.idPedido == pedidoId && c.dataFimEmbalagem == null
                           orderby c.idPedidoEnvio descending
                           select c).FirstOrDefault();
        var produtoRomaneio =
            (from c in pedidosDc.tbPedidoFornecedorItems
             where c.idPedidoFornecedorItem == itemPedidoFornecedorId
             select c).FirstOrDefault();
        if (produtoRomaneio == null) return "0";
        if (pedidoEnvio == null) return "0";

        var produto =
            (from c in pedidosDc.tbProdutoEstoques
             where c.idPedidoFornecedorItem == itemPedidoFornecedorId
             select c)
                .First();
        var produtosSeparados =
            (from c in pedidosDc.tbProdutoEstoques
             where c.pedidoId == pedidoId && c.produtoId == produto.produtoId
             select c).ToList();
        var produtosParaSeparar =
            (from c in pedidosDc.tbProdutoEstoques
             where c.pedidoId == null && c.produtoId == produto.produtoId && c.pedidoIdReserva == pedidoId
             select c).ToList();
        var itensProdutoPedido = (from c in produtosParaEmbalar where c.produtoId == produto.produtoId select c);
        bool itemPedidoIdSeparado = false;

        var itemParaSeparar = produtosParaSeparar.FirstOrDefault();
        if (itemParaSeparar != null)
        {
            bool? isErro = false;
            string erroMessage = "";
            var data = new dbCommerceDataContext();
            var separar = data.v1_separaProduto(itemPedidoFornecedorId, pedidoId, itemParaSeparar.itemPedidoIdReserva, itemParaSeparar.idItemPedidoEstoqueReserva, ref isErro, ref erroMessage);
            itemPedidoIdSeparado = true;
        }

        var pedidosDcAltera = new dbCommerceDataContext();
        var produtoAltera =
            (from c in pedidosDcAltera.tbProdutoEstoques
             where c.idPedidoFornecedorItem == itemPedidoFornecedorId
             select c)
                .First();
        produtoAltera.idPedidoEnvio = pedidoEnvio.idPedidoEnvio;
        pedidosDcAltera.SubmitChanges();

        var totalProdutos = (from c in pedidosDc.tbProdutoEstoques where c.pedidoId == pedidoId && c.idPedidoEnvio == pedidoEnvio.idPedidoEnvio select c).Count();
        if (Convert.ToInt32(produtosParaEmbalar.Sum(x => x.itemQuantidade)) <= totalProdutos)
        {
            //var pedido = (from c in pedidosDc.tbPedidos where c.pedidoId == pedidoId select c).First();
            //pedido.separado = true;
            //pedido.dataFimSeparacao = DateTime.Now;
            //pedidoEnvio.dataFimSeparacao = DateTime.Now;
            //pedidosDc.SubmitChanges();
            return "2";
        }

        return "1";
    }

    [WebMethod]
    public string AdicionaProdutoCd3(string idNumeroPedido, string idPedido)
    {
        int itemPedidoFornecedorId = Convert.ToInt32(idNumeroPedido);
        int pedidoId = Convert.ToInt32(idPedido);
        var produtosParaEmbalar = rnPedidos.retornaProdutosEtiqueta(pedidoId, 3);

        var pedidosDc = new dbCommerceDataContext();

        var pedidoEnvio = (from c in pedidosDc.tbPedidoEnvios
                           where c.idPedido == pedidoId && c.dataFimEmbalagem == null && c.idCentroDeDistribuicao == 3
                           orderby c.idPedidoEnvio descending
                           select c).FirstOrDefault();
        var produtoRomaneio =
            (from c in pedidosDc.tbPedidoFornecedorItems
             where c.idPedidoFornecedorItem == itemPedidoFornecedorId
             select c).FirstOrDefault();
        if (produtoRomaneio == null) return "0";
        if (pedidoEnvio == null) return "0";

        var produto =
            (from c in pedidosDc.tbProdutoEstoques
             where c.idPedidoFornecedorItem == itemPedidoFornecedorId
             select c)
                .First();
        var produtosSeparados =
            (from c in pedidosDc.tbProdutoEstoques
             where c.pedidoId == pedidoId && c.produtoId == produto.produtoId
             select c).ToList();
        var produtosParaSeparar =
            (from c in pedidosDc.tbProdutoEstoques
             where c.pedidoId == null && c.produtoId == produto.produtoId && c.pedidoIdReserva == pedidoId
             select c).ToList();
        var itensProdutoPedido = (from c in produtosParaEmbalar where c.produtoId == produto.produtoId select c);
        bool itemPedidoIdSeparado = false;

        var itemParaSeparar = produtosParaSeparar.FirstOrDefault();
        if (itemParaSeparar != null)
        {
            bool? isErro = false;
            string erroMessage = "";
            var data = new dbCommerceDataContext();
            var separar = data.v1_separaProduto(itemPedidoFornecedorId, pedidoId, itemParaSeparar.itemPedidoIdReserva, itemParaSeparar.idItemPedidoEstoqueReserva, ref isErro, ref erroMessage);
            itemPedidoIdSeparado = true;
        }

        var pedidosDcAltera = new dbCommerceDataContext();
        var produtoAltera =
            (from c in pedidosDcAltera.tbProdutoEstoques
             where c.idPedidoFornecedorItem == itemPedidoFornecedorId
             select c)
                .First();
        produtoAltera.idPedidoEnvio = pedidoEnvio.idPedidoEnvio;
        pedidosDcAltera.SubmitChanges();

        var totalProdutos = (from c in pedidosDc.tbProdutoEstoques where c.pedidoId == pedidoId && c.idPedidoEnvio == pedidoEnvio.idPedidoEnvio select c).Count();
        if (Convert.ToInt32(produtosParaEmbalar.Sum(x => x.itemQuantidade)) <= totalProdutos)
        {
            //var pedido = (from c in pedidosDc.tbPedidos where c.pedidoId == pedidoId select c).First();
            //pedido.separado = true;
            //pedido.dataFimSeparacao = DateTime.Now;
            //pedidoEnvio.dataFimSeparacao = DateTime.Now;
            //pedidosDc.SubmitChanges();
            return "2";
        }

        return "1";
    }
    

    [WebMethod]
    public string VerificaEnderecoPendente(string idUsuarioExpedicao)
    {
        int idUsuario = Convert.ToInt32(idUsuarioExpedicao);
        var data = new dbCommerceDataContext();
        var enderecamentoPendente =
            (from c in data.tbTransferenciaEnderecos
             where c.idUsuarioExpedicao == idUsuario && c.dataFim == null
             select c).FirstOrDefault();
        if (enderecamentoPendente != null)
        {
            var validaItens = (from c in data.tbTransferenciaEnderecoProdutos
                               where
                                   c.idTransferenciaEndereco == enderecamentoPendente.idTransferenciaEndereco &&
                                   c.dataEnderecamento == null
                               select c).Any();
            if (!validaItens)
            {
                enderecamentoPendente.dataFim = DateTime.Now;
                data.SubmitChanges();
                return "0";
            }
            else
            {
                return enderecamentoPendente.idTransferenciaEndereco.ToString();
            }
        }
        return "0";
    }

    [WebMethod]
    public string AtribuiCarrinhoUsuario(string idEndereco, string idUsuarioExpedicao)
    {
        var enderecoSplit = idEndereco.Split('-');
        if (enderecoSplit.Count() != 2) return "0";


        if (enderecoSplit[1] != "edt") return "0";


        int idUsuario = Convert.ToInt32(idUsuarioExpedicao);
        int idEnderecoTransferencia = Convert.ToInt32(enderecoSplit[0]);
        var data = new dbCommerceDataContext();
        var enderecamentoPendente =
            (from c in data.tbTransferenciaEnderecos
             where c.idUsuarioExpedicao == null && c.dataFim == null && c.idEnderecamentoTransferencia == idEnderecoTransferencia
             select c).FirstOrDefault();
        if (enderecamentoPendente != null)
        {
            enderecamentoPendente.idUsuarioExpedicao = idUsuario;
            enderecamentoPendente.dataInicio = DateTime.Now;
            data.SubmitChanges();
            return enderecamentoPendente.idTransferenciaEndereco.ToString();
        }
        return "0";
    }


    public class RetornoAtribuirCarrinho
    {
        public bool valido { get; set; }
        public string mensagem {get;set;}
        public int idTransferenciaEndereco { get; set; }
        public DateTime dataInicio { get; set; }
        public string carrinhoNome { get; set; }
        public int totalPendente { get; set; }
    }
    [WebMethod]
    public RetornoAtribuirCarrinho AtribuiRetornaCarrinhoPendente(string idEndereco, int idUsuarioExpedicao)
    {
        var enderecoSplit = idEndereco.Split('-');

        var retorno = new RetornoAtribuirCarrinho();

        if (enderecoSplit.Count() != 2)
        {
            retorno.valido = false;
            retorno.mensagem = "Carrinho Inválido!";
            return retorno;
        }


        if (enderecoSplit[1] != "edt")
        {
            retorno.mensagem = "Carrinho Inválido!";
            return retorno;
        }

       
        int idEnderecoTransferencia = Convert.ToInt32(enderecoSplit[0]);
        var data = new dbCommerceDataContext();
        var enderecamentoPendente =
            (from c in data.tbTransferenciaEnderecos
             where c.dataFim == null && c.idEnderecamentoTransferencia == idEnderecoTransferencia
             select c).FirstOrDefault();
        if (enderecamentoPendente != null)
        {
            if (enderecamentoPendente.idUsuarioExpedicao == null)
            {
                enderecamentoPendente.idUsuarioExpedicao = idUsuarioExpedicao;
                enderecamentoPendente.dataInicio = DateTime.Now;
                data.SubmitChanges();
            }
            var produtosAguardandoEnderecar = (from c in data.tbTransferenciaEnderecoProdutos
                                               where c.idTransferenciaEndereco == enderecamentoPendente.idTransferenciaEndereco && 
                                               c.dataEnderecamento == null select c).Any();
            if (!produtosAguardandoEnderecar)
            {
                retorno.mensagem = "Carrinho não possui itens para endereçar";
                enderecamentoPendente.dataFim = DateTime.Now;
                data.SubmitChanges();
                return retorno;
            }
            retorno.valido = true;
            retorno.dataInicio = enderecamentoPendente.dataInicio ?? DateTime.Now;
            retorno.carrinhoNome = enderecamentoPendente.tbEnderecamentoTransferencia.enderecamentoTransferencia;
            retorno.idTransferenciaEndereco = enderecamentoPendente.idTransferenciaEndereco;
            retorno.totalPendente = (from c in data.tbTransferenciaEnderecoProdutos where c.idTransferenciaEndereco == enderecamentoPendente.idTransferenciaEndereco && c.dataEnderecamento == null select c).Count();
            return retorno;
        }
        return retorno;
    }

    [WebMethod]
    public string EnderecarPedido(string endereco, string idPedido)
    {
        var enderecoSplit = endereco.Split('-');
        if (enderecoSplit.Count() != 2)
        {
            return "1";
        }
        if (enderecoSplit[1] != "esp")
        {
            return "1";
        }
        int idEnderecoSeparacao = 0;
        int.TryParse(enderecoSplit[0], out idEnderecoSeparacao);
        if (idEnderecoSeparacao == 0)
        {
            return "1";
        }


        var pedidosDc = new dbCommerceDataContext();

        var objEndereco =
            (from c in pedidosDc.tbEnderecoSeparacaos where c.idEnderecamentoSeparacao == idEnderecoSeparacao select c)
                .FirstOrDefault();
        if (objEndereco == null)
        {
            return "1";
        }

        int pedidoId = Convert.ToInt32(idPedido);
        var pedidoEnvio = (from c in pedidosDc.tbPedidoEnvios
                           where c.idPedido == pedidoId && c.dataFimSeparacao == null
                           orderby c.idPedidoEnvio descending
                           select c).FirstOrDefault();
        var pedido = (from c in pedidosDc.tbPedidos where c.pedidoId == pedidoId select c).First();
        pedido.separado = true;
        pedido.separadoCd1 = true;

        pedido.dataFimSeparacao = DateTime.Now;

        pedidoEnvio.dataFimSeparacao = DateTime.Now;
        pedidoEnvio.idEnderecoSeparacao = idEnderecoSeparacao;
        pedidosDc.SubmitChanges();


        var queue = new tbQueue();
        queue.agendamento = DateTime.Now;
        queue.andamento = false;
        queue.concluido = false;
        queue.idRelacionado = pedidoId;
        queue.mensagem = "";
        queue.tipoQueue = 8;
        pedidosDc.tbQueues.InsertOnSubmit(queue);
        pedidosDc.SubmitChanges();


        return "0";

    }


    [WebMethod]
    public string EnderecarPedidoCds(string endereco, string idPedido, string idUsuario)
    {
        int idUsuarioSeparacao = Convert.ToInt32(idUsuario);

        var data = new dbCommerceDataContext();
        var usuario = (from c in data.tbUsuarioExpedicaos where c.idUsuarioExpedicao == idUsuarioSeparacao select c).FirstOrDefault();
        int idCentroDistribuicao = usuario.idCentroDistribuicao;

        var enderecoSplit = endereco.Split('-');
        if (enderecoSplit.Count() != 2)
        {
            return "1";
        }
        if (enderecoSplit[1] != "esp")
        {
            return "1";
        }
        int idEnderecoSeparacao = 0;
        int.TryParse(enderecoSplit[0], out idEnderecoSeparacao);
        if (idEnderecoSeparacao == 0)
        {
            return "1";
        }


        var pedidosDc = new dbCommerceDataContext();

        var objEndereco =
            (from c in pedidosDc.tbEnderecoSeparacaos where c.idEnderecamentoSeparacao == idEnderecoSeparacao select c)
                .FirstOrDefault();
        if (objEndereco == null)
        {
            return "1";
        }

        int pedidoId = Convert.ToInt32(idPedido);
        var pedidoEnvio = (from c in pedidosDc.tbPedidoEnvios
                           where c.idPedido == pedidoId && c.dataFimSeparacao == null && c.idCentroDeDistribuicao == idCentroDistribuicao
                           orderby c.idPedidoEnvio descending
                           select c).FirstOrDefault();
        var pedido = (from c in pedidosDc.tbPedidos where c.pedidoId == pedidoId select c).First();

        if (idCentroDistribuicao == 1)
        {
            pedido.separado = true;
            pedido.separadoCd1 = true;

            pedido.dataFimSeparacao = DateTime.Now;
        }
        if(idCentroDistribuicao == 4)
        {
            pedido.separadoCd4 = true;
            pedido.dataFimSeparacao = DateTime.Now;
        }
        pedidoEnvio.dataFimSeparacao = DateTime.Now;
        pedidoEnvio.idEnderecoSeparacao = idEnderecoSeparacao;
        pedidosDc.SubmitChanges();

        var queue = new tbQueue();
        queue.agendamento = DateTime.Now.AddSeconds(10);
        queue.andamento = false;
        queue.concluido = false;
        queue.idRelacionado = pedidoId;
        queue.mensagem = "";
        queue.tipoQueue = 8;
        pedidosDc.tbQueues.InsertOnSubmit(queue);
        pedidosDc.SubmitChanges();


        return "0";

    }


    [WebMethod]
    public string VerificaProdutoEnderecamento(string etiqueta)
    {
        int idPedidoFornecedorItem = Convert.ToInt32(etiqueta);
        var data = new dbCommerceDataContext();
        var produtoEstoque =
            (from c in data.tbProdutoEstoques where c.idPedidoFornecedorItem == idPedidoFornecedorItem select c)
                .FirstOrDefault();
        if (produtoEstoque == null)
        {
            var itemPedido =
                (from c in data.tbPedidoFornecedorItems
                 where c.idPedidoFornecedorItem == idPedidoFornecedorItem
                 select c).FirstOrDefault();
            if (itemPedido != null)
            {
                if (itemPedido.entregue != true)
                {
                    itemPedido.entregue = true;
                    itemPedido.dataEntrega = DateTime.Now;
                    data.SubmitChanges();
                }

                var produtoDc = new dbCommerceDataContext();
                for (int i = 1; i <= itemPedido.quantidade; i++)
                {
                    var checaProdutoFaltando = (from c in produtoDc.tbPedidoProdutoFaltandos
                                                where
                                                    c.pedidoId == itemPedido.idPedido && c.produtoId == itemPedido.idProduto &&
                                                    c.entregue == false
                                                select c).FirstOrDefault();
                    if (checaProdutoFaltando != null)
                    {
                        checaProdutoFaltando.entregue = true;
                        produtoDc.SubmitChanges();
                    }
                }

                var estoqueCheck =
                    (from c in data.tbProdutoEstoques where c.idPedidoFornecedorItem == idPedidoFornecedorItem select c)
                        .Any();
                if (!estoqueCheck)
                {
                    var estoque = new tbProdutoEstoque();
                    estoque.idPedidoFornecedorItem = idPedidoFornecedorItem;
                    estoque.produtoId = itemPedido.idProduto;
                    estoque.dataEntrada = DateTime.Now;
                    estoque.enviado = false;
                    data.tbProdutoEstoques.InsertOnSubmit(estoque);
                    data.SubmitChanges();

                    var produtosDc = new dbCommerceDataContext();
                    var produto = (from c in produtosDc.tbProdutos where c.produtoId == itemPedido.idProduto select c).First();
                    var interacaoObj = new tbPedidoFornecedorInteracao();
                    interacaoObj.data = DateTime.Now;
                    interacaoObj.idPedidoFornecedor = itemPedido.idPedidoFornecedor;
                    string interacao = "Produto entregue" + "<br>";
                    interacao += "ID: " + idPedidoFornecedorItem + "<br>";
                    interacao += produto.produtoNome + "<br>";
                    interacao += produto.produtoIdDaEmpresa + " - " + produto.complementoIdDaEmpresa;
                    interacaoObj.interacao = interacao;

                    interacaoObj.usuario = "Endereçamento";
                    data.tbPedidoFornecedorInteracaos.InsertOnSubmit(interacaoObj);
                    data.SubmitChanges();
                }

                return "3";
            }
            else
            {
                return "0";
            }
        }

        if (produtoEstoque.enviado == true)
        {
            return "1";
        }

        if (produtoEstoque.idEnderecamentoArea != null)
        {
            return "2";
        }

        return "3";
    }

    public class RetornoVerificarProdutoEnderecamento
    {
        public bool valido { get; set; }
        public string mensagem { get; set; }
        public int idPedido { get; set; }
    }

    [WebMethod]
    public RetornoVerificarProdutoEnderecamento VerificaProdutoEnderecamentoColetor(int idPedidoFornecedorItem, int idTransferenciaEndereco, int idUsuarioExpedicao)
    {

        var data = new dbCommerceDataContext();
        var retorno = new RetornoVerificarProdutoEnderecamento();
        retorno.valido = false;
        retorno.idPedido = 0;
        var enderecamento = (from c in data.tbTransferenciaEnderecos where c.idTransferenciaEndereco == idTransferenciaEndereco select c).First();

        var usuarioExpedicao = (from c in data.tbUsuarioExpedicaos where c.idUsuarioExpedicao == idUsuarioExpedicao select c).First();

        var validaEtiqueta = (from c in data.tbTransferenciaEnderecoProdutos
                              where c.idTransferenciaEndereco == idTransferenciaEndereco && c.idPedidoFornecedorItem == idPedidoFornecedorItem
                              select c).FirstOrDefault();

        if (validaEtiqueta == null)
        {
            retorno.valido = false;
            retorno.mensagem = "Produto não faz parte do seu carrinho";
            rnLog.InsereLogEtiqueta(usuarioExpedicao.nome, idPedidoFornecedorItem, "Tentativa de endereçar produto que não faz parte do carrinho");
            return retorno;
        }

        var produtoEstoque =
            (from c in data.tbProdutoEstoques where c.idPedidoFornecedorItem == idPedidoFornecedorItem select c)
                .FirstOrDefault();
        if (produtoEstoque != null)
        {
            var itemPedido =
                (from c in data.tbPedidoFornecedorItems
                 where c.idPedidoFornecedorItem == idPedidoFornecedorItem
                 select c).FirstOrDefault();
            if (itemPedido != null)
            {
                if (itemPedido.entregue != true)
                {
                    itemPedido.entregue = true;
                    itemPedido.dataEntrega = DateTime.Now;
                    data.SubmitChanges();
                }

                var produtoDc = new dbCommerceDataContext();
                var estoqueCheck =
                    (from c in data.tbProdutoEstoques where c.idPedidoFornecedorItem == idPedidoFornecedorItem select c)
                        .Any();
                if (!estoqueCheck)
                {
                    var estoque = new tbProdutoEstoque();
                    estoque.idPedidoFornecedorItem = idPedidoFornecedorItem;
                    estoque.produtoId = itemPedido.idProduto;
                    estoque.dataEntrada = DateTime.Now;
                    estoque.enviado = false;
                    data.tbProdutoEstoques.InsertOnSubmit(estoque);
                    data.SubmitChanges();
                }

                retorno.valido = true;
                retorno.mensagem = "";
                rnLog.InsereLogEtiqueta(enderecamento.tbUsuarioExpedicao.nome, idPedidoFornecedorItem, "Etiqueta lida para ser endereçada");
                return retorno;
            }
            else
            {
                retorno.valido = false;
                retorno.mensagem = "Produto não localizado";
                return retorno;
            }
        }
        else
        {
            retorno.valido = false;
            retorno.mensagem = "Produto não foi dado entrada. Favor verificar";
                return retorno;
        }
    }

    [WebMethod]
    public string VerificaProdutoEnderecamentoTransferencia(string etiqueta, string idTransferenciaEndereco)
    {
        int idPedidoFornecedorItem = 0;
        int.TryParse(etiqueta, out idPedidoFornecedorItem);
        int idTransferencia = Convert.ToInt32(idTransferenciaEndereco);


        var data = new dbCommerceDataContext();
        var enderecamento = (from c in data.tbTransferenciaEnderecos where c.idTransferenciaEndereco == idTransferencia select c).First();

        int idUsuarioExpedicao = (int)enderecamento.idUsuarioExpedicao;
        var usuarioExpedicao = (from c in data.tbUsuarioExpedicaos where c.idUsuarioExpedicao == idUsuarioExpedicao select c).First();

        var validaEtiqueta = (from c in data.tbTransferenciaEnderecoProdutos
                              where c.idTransferenciaEndereco == idTransferencia && c.idPedidoFornecedorItem == idPedidoFornecedorItem
                              select c).FirstOrDefault();
        if (validaEtiqueta == null)
        {
            rnLog.InsereLogEtiqueta(usuarioExpedicao.nome, idPedidoFornecedorItem, "Tentativa de endereçar produto que não faz parte do carrinho");
            return "4";
        }

        var produtoEstoque =
            (from c in data.tbProdutoEstoques where c.idPedidoFornecedorItem == idPedidoFornecedorItem select c)
                .FirstOrDefault();
        if (produtoEstoque == null)
        {
            var itemPedido =
                (from c in data.tbPedidoFornecedorItems
                 where c.idPedidoFornecedorItem == idPedidoFornecedorItem
                 select c).FirstOrDefault();
            if (itemPedido != null)
            {
                if (itemPedido.entregue != true)
                {
                    itemPedido.entregue = true;
                    itemPedido.dataEntrega = DateTime.Now;
                    data.SubmitChanges();
                }

                var produtoDc = new dbCommerceDataContext();
                for (int i = 1; i <= itemPedido.quantidade; i++)
                {
                    var checaProdutoFaltando = (from c in produtoDc.tbPedidoProdutoFaltandos
                                                where
                                                    c.pedidoId == itemPedido.idPedido && c.produtoId == itemPedido.idProduto &&
                                                    c.entregue == false
                                                select c).FirstOrDefault();
                    if (checaProdutoFaltando != null)
                    {
                        checaProdutoFaltando.entregue = true;
                        produtoDc.SubmitChanges();
                    }
                }

                var estoqueCheck =
                    (from c in data.tbProdutoEstoques where c.idPedidoFornecedorItem == idPedidoFornecedorItem select c)
                        .Any();
                if (!estoqueCheck)
                {
                    var estoque = new tbProdutoEstoque();
                    estoque.idPedidoFornecedorItem = idPedidoFornecedorItem;
                    estoque.produtoId = itemPedido.idProduto;
                    estoque.dataEntrada = DateTime.Now;
                    estoque.enviado = false;
                    data.tbProdutoEstoques.InsertOnSubmit(estoque);
                    data.SubmitChanges();

                    var produtosDc = new dbCommerceDataContext();
                    var produto = (from c in produtosDc.tbProdutos where c.produtoId == itemPedido.idProduto select c).First();
                    var interacaoObj = new tbPedidoFornecedorInteracao();
                    interacaoObj.data = DateTime.Now;
                    interacaoObj.idPedidoFornecedor = itemPedido.idPedidoFornecedor;
                    string interacao = "Produto entregue" + "<br>";
                    interacao += "ID: " + idPedidoFornecedorItem + "<br>";
                    interacao += produto.produtoNome + "<br>";
                    interacao += produto.produtoIdDaEmpresa + " - " + produto.complementoIdDaEmpresa;
                    interacaoObj.interacao = interacao;

                    interacaoObj.usuario = "Endereçamento";
                    data.tbPedidoFornecedorInteracaos.InsertOnSubmit(interacaoObj);
                    data.SubmitChanges();
                }

                rnLog.InsereLogEtiqueta(enderecamento.tbUsuarioExpedicao.nome, idPedidoFornecedorItem, "Etiqueta lida para ser endereçada");
                return "3";
            }
            else
            {
                return "0";
            }
        }

        if (produtoEstoque.enviado == true)
        {
            rnLog.InsereLogEtiqueta(enderecamento.usuarioEntrada, idPedidoFornecedorItem, "Tentativa de endereçar produto enviado");
            return "1";
        }

        if (produtoEstoque.idEnderecamentoArea != null && produtoEstoque.idCentroDistribuicao != 2)
        {
            if (produtoEstoque.pedidoIdReserva != null)
            {
                rnLog.InsereLogEtiqueta(enderecamento.tbUsuarioExpedicao.nome, idPedidoFornecedorItem, "Etiqueta lida para ser endereçada");
                return "3";
            }
        }

        //if (produtoEstoque.pedidoIdReserva != null)
        //{
        //    int pedidoIdReservado = Convert.ToInt32(produtoEstoque.pedidoIdReserva);
        //    var itensAguardandoEnvio =
        //        (from c in data.tbItemPedidoEstoques
        //         where c.tbItensPedido.pedidoId == pedidoIdReservado && c.enviado == false
        //         select c).Count();
        //    if (itensAguardandoEnvio == 1)
        //    {
        //        return "2_" + pedidoIdReservado;
        //    }
        //}


        //ENDEREÇAMENTO NA PRATEIRA, CANCELADO MOMENTANEAMENTE
        /*if ((produtoEstoque.liberado ?? false) == false && produtoEstoque.pedidoIdReserva == null && produtoEstoque.idCentroDistribuicao != 2 && produtoEstoque.idCentroDistribuicao != 3 && produtoEstoque.tbPedidoFornecedorItem.idPedido == null)
        {
            var proximaFila = (from c in data.tbItemPedidoEstoques
                               where c.produtoId == produtoEstoque.produtoId && c.tbItensPedido.tbPedido.statusDoPedido == 11
                                     && c.cancelado == false && c.reservado == false && c.dataLimite != null
                               orderby c.dataLimite
                               select c).FirstOrDefault();
            if (proximaFila != null)
            {
                var pedidoIdProximaReserva = proximaFila.tbItensPedido.pedidoId;
                var itensAguardandoEnvio =
                    (from c in data.tbItemPedidoEstoques
                     where c.tbItensPedido.pedidoId == pedidoIdProximaReserva && c.enviado == false
                     select c).Count();
                if (itensAguardandoEnvio == 1)
                {
                    return "2_" + pedidoIdProximaReserva;
                }
            }
        }*/


        rnLog.InsereLogEtiqueta(enderecamento.usuarioEntrada, idPedidoFornecedorItem, "Etiqueta lida para ser endereçada");
        return "3";
    }

    public class RetornoEnderecarProduto
    {
        public bool valido { get; set; }
        public string mensagem { get; set; }
        public int totalPendente { get; set; }
    }

    private int totalItensPendentesCarrinho(int idTransferenciaEndereco)
    {
        var data = new dbCommerceDataContext();
        return (from c in data.tbTransferenciaEnderecoProdutos where c.idTransferenciaEndereco == idTransferenciaEndereco && c.dataEnderecamento == null select c).Count();
    }

    [WebMethod]
    public List<string> ListaProdutosPendentesEnderecamento(int idTransferenciaEndereco)
    {
        var data = new dbCommerceDataContext();
        var produtos = (from c in data.tbTransferenciaEnderecoProdutos where c.idTransferenciaEndereco == idTransferenciaEndereco && c.dataEnderecamento == null select
                        new {
                            item = (c.idPedidoFornecedorItem.ToString() + " - " + c.tbPedidoFornecedorItem.tbProduto.produtoNome)
                        }).ToList();
        return produtos.Select(x => x.item).ToList();
    }


    [WebMethod]
    public RetornoEnderecarProduto EnderecarProdutoTransferenciaColetor(int idPedidoFornecedorItem, string endereco, int idTransferenciaEndereco, int idUsuarioExpedicao)
    {
        var retorno = new RetornoEnderecarProduto();

        var lista = endereco.Split('-');
        if (lista.Count() != 2)
        {
            retorno.valido = false;
            retorno.mensagem = "Local de endereçamento incorreto"; ;
            rnLog.InsereLogEtiqueta("", idPedidoFornecedorItem, "Local de endereçamento incorreto");
            return retorno;
        }

        int idEndereco = 0;
        int tipo = 0;
        try
        {
            idEndereco = Convert.ToInt32(lista[0]);
            tipo = Convert.ToInt32(lista[1]);
        }
        catch (Exception)
        {
            retorno.valido = false;
            retorno.mensagem = "Local de endereçamento incorreto";
            throw;
        }
        var data = new dbCommerceDataContext();

        var validaEtiqueta = (from c in data.tbTransferenciaEnderecoProdutos
                              where c.idTransferenciaEndereco == idTransferenciaEndereco && c.idPedidoFornecedorItem == idPedidoFornecedorItem
                              select c).FirstOrDefault();
        validaEtiqueta.dataEnderecamento = DateTime.Now;

        var validaEtiquetas = (from c in data.tbTransferenciaEnderecoProdutos
                               where c.idPedidoFornecedorItem == idPedidoFornecedorItem && c.dataEnderecamento == null
                               select c);
        foreach (var valida in validaEtiquetas)
        {
            valida.dataEnderecamento = DateTime.Now;
            valida.idUsuarioExpedicao = idUsuarioExpedicao;
        }
        
        var usuarioExpedicao = (from c in data.tbUsuarioExpedicaos where c.idUsuarioExpedicao == idUsuarioExpedicao select c).First();

        var produto = (from c in data.tbProdutoEstoques where c.idPedidoFornecedorItem == idPedidoFornecedorItem select c).First();
        var pedidoItem =
            (from c in data.tbPedidoFornecedorItems
             where c.idPedidoFornecedorItem == idPedidoFornecedorItem
             select c).First();
        int centroDistribuicaoProduto = pedidoItem.tbProduto.tbProdutoFornecedor.idCentroDistribuicao ?? 4;
        if (tipo == 1)
        {
            var area = (from c in data.tbEnderecamentoAreas where c.idEnderecamentoArea == idEndereco select c).FirstOrDefault();
            if (area == null)
            {
                retorno.valido = false;
                retorno.mensagem = "Local de endereçamento não encontrado neste CD";
                return retorno;
            }

            produto.idCentroDistribuicao = (area.idCentroDistribuicao ?? 1);
            if (produto.idCentroDistribuicao != centroDistribuicaoProduto)
            {
                retorno.valido = false;
                retorno.mensagem = "Este produto deve ser endereçado no CD " + centroDistribuicaoProduto;
                return retorno;
            }

            produto.idEnderecamentoArea = idEndereco;
            rnLog.InsereLogEtiqueta(usuarioExpedicao.nome, idPedidoFornecedorItem, "Etiqueta endereçada em " + area.area);
        }
        else if (tipo == 2)
        {
            var rua = (from c in data.tbEnderecamentoRuas where c.idEnderecamentoRua == idEndereco select c).FirstOrDefault();
            if (rua == null)
            {
                retorno.valido = false;
                retorno.mensagem = "Local de endereçamento não encontrado neste CD";
                return retorno;
            }

            produto.idCentroDistribuicao = (rua.tbEnderecamentoArea.idCentroDistribuicao ?? 1);
            if (produto.idCentroDistribuicao != centroDistribuicaoProduto)
            {
                retorno.valido = false;
                retorno.mensagem = "Este produto deve ser endereçado no CD " + centroDistribuicaoProduto;
                return retorno;
            }

            produto.idEnderecamentoArea = rua.idEnderecamentoArea;
            produto.idEnderecamentoRua = rua.idEnderecamentoRua;
            produto.liberado = true;
            
            rnLog.InsereLogEtiqueta(usuarioExpedicao.nome, idPedidoFornecedorItem, "Etiqueta endereçada em A:" + rua.tbEnderecamentoArea.area + " - R: " + rua.rua);
        }
        else if (tipo == 3)
        {
            var predio =
                (from c in data.tbEnderecamentoPredios where c.idEnderecamentoPredio == idEndereco select c).FirstOrDefault();
            if (predio == null)
            {
                retorno.valido = false;
                retorno.mensagem = "Local de endereçamento não encontrado neste CD";
                return retorno;
            }

            produto.idCentroDistribuicao = (predio.tbEnderecamentoRua.tbEnderecamentoArea.idCentroDistribuicao ?? 1);
            if (produto.idCentroDistribuicao != centroDistribuicaoProduto)
            {
                retorno.valido = false;
                retorno.mensagem = "Este produto deve ser endereçado no CD " + centroDistribuicaoProduto;
                return retorno;
            }

            produto.idEnderecamentoArea = predio.tbEnderecamentoRua.idEnderecamentoArea;
            produto.idEnderecamentoRua = predio.idEnderecamentoRua;
            produto.idEnderecamentoPredio = predio.idEnderecamentoPredio;
            rnLog.InsereLogEtiqueta(usuarioExpedicao.nome, idPedidoFornecedorItem, "Etiqueta endereçada em A:" + predio.tbEnderecamentoRua.tbEnderecamentoArea.area + " - R: " + predio.tbEnderecamentoRua.rua + " - P: " + predio.predio);
        }
        else if (tipo == 4)
        {
            var andar =
                (from c in data.tbEnderecamentoAndars where c.idEnderecamentoAndar == idEndereco select c).FirstOrDefault();
            if (andar == null)
            {
                retorno.valido = false;
                retorno.mensagem = "Local de endereçamento não encontrado neste CD";
                return retorno;
            }

            produto.idCentroDistribuicao = (andar.tbEnderecamentoPredio.tbEnderecamentoRua.tbEnderecamentoArea.idCentroDistribuicao ?? 1);
            if (produto.idCentroDistribuicao != centroDistribuicaoProduto)
            {
                retorno.valido = false;
                retorno.mensagem = "Este produto deve ser endereçado no CD " + centroDistribuicaoProduto;
                return retorno;
            }
            produto.idEnderecamentoArea = andar.tbEnderecamentoPredio.tbEnderecamentoRua.idEnderecamentoArea;
            produto.idEnderecamentoRua = andar.tbEnderecamentoPredio.idEnderecamentoRua;
            produto.idEnderecamentoPredio = andar.idEnderecamentoPredio;
            produto.idEnderecamentoAndar = andar.idEnderecamentoAndar;
            rnLog.InsereLogEtiqueta(usuarioExpedicao.nome, idPedidoFornecedorItem, "Etiqueta endereçada em A:" + andar.tbEnderecamentoPredio.tbEnderecamentoRua.tbEnderecamentoArea.area + " - R: " + andar.tbEnderecamentoPredio.tbEnderecamentoRua.rua + " - P: " + andar.tbEnderecamentoPredio.predio + " - L: " + andar.tbEnderecamentoPredio.tbEnderecamentoLado.lado + " - A: " + andar.andar);
        }
        else if (tipo == 5)
        {
            var apartamento =
                (from c in data.tbEnderecamentoApartamentos where c.idEnderecamentoApartamento == idEndereco select c).FirstOrDefault();
            if (apartamento == null)
            {
                retorno.valido = false;
                retorno.mensagem = "Local de endereçamento não encontrado neste CD";
                return retorno;
            }

            produto.idCentroDistribuicao = (apartamento.tbEnderecamentoAndar.tbEnderecamentoPredio.tbEnderecamentoRua.tbEnderecamentoArea.idCentroDistribuicao ?? 1);
            if (produto.idCentroDistribuicao != centroDistribuicaoProduto)
            {
                retorno.valido = false;
                retorno.mensagem = "Este produto deve ser endereçado no CD " + centroDistribuicaoProduto;
                return retorno;
            }
            produto.idEnderecamentoArea = apartamento.tbEnderecamentoAndar.tbEnderecamentoPredio.tbEnderecamentoRua.idEnderecamentoArea;
            produto.idEnderecamentoRua = apartamento.tbEnderecamentoAndar.tbEnderecamentoPredio.idEnderecamentoRua;
            produto.idEnderecamentoPredio = apartamento.tbEnderecamentoAndar.idEnderecamentoPredio;
            produto.idEnderecamentoAndar = apartamento.idEnderecamentoAndar;
            produto.idEnderecamentoApartamento = apartamento.idEnderecamentoApartamento;
        }
        else
        {
            retorno.valido = false;
            retorno.mensagem = "Este produto deve ser endereçado no CD " + centroDistribuicaoProduto;
            return retorno;
        }

        produto.liberado = true;
        
        pedidoItem.liberado = true;
        data.SubmitChanges();
        AdicionaQueueReservaEstoque(produto.produtoId);
        retorno.valido = true;
        retorno.totalPendente = (from c in data.tbTransferenciaEnderecoProdutos where c.idTransferenciaEndereco == idTransferenciaEndereco && c.dataEnderecamento == null select c).Count();

        if(retorno.totalPendente == 0)
        {
            var carrinhoFinalizar = (from c in data.tbTransferenciaEnderecos where c.idTransferenciaEndereco == idTransferenciaEndereco select c).First();
            carrinhoFinalizar.dataFim = DateTime.Now;
            data.SubmitChanges();
        }
        return retorno;
    }
    

    [WebMethod]
    public string EnderecarProdutoTransferencia(string etiqueta, string endereco, string idTransferenciaEndereco)
    {
        int idPedidoFornecedorItem = 0;
        int idTransferencia = Convert.ToInt32(idTransferenciaEndereco);

        try
        {
            idPedidoFornecedorItem = Convert.ToInt32(etiqueta);
        }
        catch (Exception)
        {
            return "0";
        }
        var lista = endereco.Split('-');
        if (lista.Count() != 2)
        {
            rnLog.InsereLogEtiqueta("", idPedidoFornecedorItem, "Local de endereçamento incorreto");
            return "0";
        }

        int idEndereco = 0;
        int tipo = 0;
        try
        {
            idEndereco = Convert.ToInt32(lista[0]);
            tipo = Convert.ToInt32(lista[1]);
        }
        catch (Exception)
        {
            return "0";
            throw;
        }
        var data = new dbCommerceDataContext();

        var validaEtiqueta = (from c in data.tbTransferenciaEnderecoProdutos
                              where c.idTransferenciaEndereco == idTransferencia && c.idPedidoFornecedorItem == idPedidoFornecedorItem
                              select c).FirstOrDefault();
        validaEtiqueta.dataEnderecamento = DateTime.Now;

        var validaEtiquetas = (from c in data.tbTransferenciaEnderecoProdutos
                               where c.idPedidoFornecedorItem == idPedidoFornecedorItem && c.dataEnderecamento == null
                               select c);
        foreach (var valida in validaEtiquetas)
        {
            valida.dataEnderecamento = DateTime.Now;
        }

        int idUsuarioExpedicao = (int)validaEtiqueta.tbTransferenciaEndereco.idUsuarioExpedicao;
        var usuarioExpedicao = (from c in data.tbUsuarioExpedicaos where c.idUsuarioExpedicao == idUsuarioExpedicao select c).First();

        if (tipo == 1)
        {
            var area =
                (from c in data.tbEnderecamentoAreas where c.idEnderecamentoArea == idEndereco select c).FirstOrDefault();
            if (area == null) return "0";

            var produto =
                (from c in data.tbProdutoEstoques where c.idPedidoFornecedorItem == idPedidoFornecedorItem select c)
                    .First();
            produto.idEnderecamentoArea = idEndereco;
            produto.liberado = true;
            produto.idCentroDistribuicao = (area.idCentroDistribuicao ?? 1);
            AdicionaQueueReservaEstoque(produto.produtoId);

            var pedidoItem =
                (from c in data.tbPedidoFornecedorItems
                 where c.idPedidoFornecedorItem == idPedidoFornecedorItem
                 select c).First();
            pedidoItem.liberado = true;
            data.SubmitChanges();
            if (pedidoItem.idPedido != null)
            {
                AdicionaQueueChecagemPedidoCompleto((int)pedidoItem.idPedido);
            }
            
            rnLog.InsereLogEtiqueta(usuarioExpedicao.nome, idPedidoFornecedorItem, "Etiqueta endereçada em " + area.area);
            return "1";
        }
        if (tipo == 2)
        {
            var rua =
                (from c in data.tbEnderecamentoRuas where c.idEnderecamentoRua == idEndereco select c).FirstOrDefault();
            if (rua == null) return "0";

            var produto =
                (from c in data.tbProdutoEstoques where c.idPedidoFornecedorItem == idPedidoFornecedorItem select c)
                    .First();
            produto.idEnderecamentoArea = rua.idEnderecamentoArea;
            produto.idEnderecamentoRua = rua.idEnderecamentoRua;
            produto.liberado = true;
            produto.idCentroDistribuicao = (rua.tbEnderecamentoArea.idCentroDistribuicao ?? 1);
            AdicionaQueueReservaEstoque(produto.produtoId);

            var pedidoItem =
                (from c in data.tbPedidoFornecedorItems
                 where c.idPedidoFornecedorItem == idPedidoFornecedorItem
                 select c).First();
            pedidoItem.liberado = true;

            data.SubmitChanges();
            if (pedidoItem.idPedido != null)
            {
                AdicionaQueueChecagemPedidoCompleto((int)pedidoItem.idPedido);
            }
            rnLog.InsereLogEtiqueta(usuarioExpedicao.nome, idPedidoFornecedorItem, "Etiqueta endereçada em A:" + rua.tbEnderecamentoArea.area + " - R: "+ rua.rua);
            return "1";
        }
        if (tipo == 3)
        {
            var predio =
                (from c in data.tbEnderecamentoPredios where c.idEnderecamentoPredio == idEndereco select c).FirstOrDefault();
            if (predio == null) return "0";

            var produto =
                (from c in data.tbProdutoEstoques where c.idPedidoFornecedorItem == idPedidoFornecedorItem select c)
                    .First();
            produto.idEnderecamentoArea = predio.tbEnderecamentoRua.idEnderecamentoArea;
            produto.idEnderecamentoRua = predio.idEnderecamentoRua;
            produto.idEnderecamentoPredio = predio.idEnderecamentoPredio;
            produto.liberado = true;
            produto.idCentroDistribuicao = (predio.tbEnderecamentoRua.tbEnderecamentoArea.idCentroDistribuicao ?? 1);
            AdicionaQueueReservaEstoque(produto.produtoId);

            var pedidoItem =
                (from c in data.tbPedidoFornecedorItems
                 where c.idPedidoFornecedorItem == idPedidoFornecedorItem
                 select c).First();
            pedidoItem.liberado = true;

            data.SubmitChanges();
            if (pedidoItem.idPedido != null)
            {
                AdicionaQueueChecagemPedidoCompleto((int)pedidoItem.idPedido);
            }
            rnLog.InsereLogEtiqueta(usuarioExpedicao.nome, idPedidoFornecedorItem, "Etiqueta endereçada em A:" + predio.tbEnderecamentoRua.tbEnderecamentoArea.area + " - R: " + predio.tbEnderecamentoRua.rua + " - P: " + predio.predio);
            return "1";
        }
        if (tipo == 4)
        {
            var andar =
                (from c in data.tbEnderecamentoAndars where c.idEnderecamentoAndar == idEndereco select c).FirstOrDefault();
            if (andar == null) return "0";

            var produto =
                (from c in data.tbProdutoEstoques where c.idPedidoFornecedorItem == idPedidoFornecedorItem select c)
                    .First();
            produto.idEnderecamentoArea = andar.tbEnderecamentoPredio.tbEnderecamentoRua.idEnderecamentoArea;
            produto.idEnderecamentoRua = andar.tbEnderecamentoPredio.idEnderecamentoRua;
            produto.idEnderecamentoPredio = andar.idEnderecamentoPredio;
            produto.idEnderecamentoAndar = andar.idEnderecamentoAndar;
            produto.liberado = true;
            produto.idCentroDistribuicao = (andar.tbEnderecamentoPredio.tbEnderecamentoRua.tbEnderecamentoArea.idCentroDistribuicao ?? 1);
            AdicionaQueueReservaEstoque(produto.produtoId);

            var pedidoItem =
                (from c in data.tbPedidoFornecedorItems
                 where c.idPedidoFornecedorItem == idPedidoFornecedorItem
                 select c).First();
            pedidoItem.liberado = true;

            data.SubmitChanges();
            if (pedidoItem.idPedido != null)
            {
                AdicionaQueueChecagemPedidoCompleto((int)pedidoItem.idPedido);
            }
            rnLog.InsereLogEtiqueta(usuarioExpedicao.nome, idPedidoFornecedorItem, "Etiqueta endereçada em A:" + andar.tbEnderecamentoPredio.tbEnderecamentoRua.tbEnderecamentoArea.area + " - R: " + andar.tbEnderecamentoPredio.tbEnderecamentoRua.rua + " - P: " + andar.tbEnderecamentoPredio.predio + " - L: " + andar.tbEnderecamentoPredio.tbEnderecamentoLado.lado + " - A: " + andar.andar);
            return "1";
        }
        if (tipo == 5)
        {
            var apartamento =
                (from c in data.tbEnderecamentoApartamentos where c.idEnderecamentoApartamento == idEndereco select c).FirstOrDefault();
            if (apartamento == null) return "0";

            var produto =
                (from c in data.tbProdutoEstoques where c.idPedidoFornecedorItem == idPedidoFornecedorItem select c)
                    .First();
            produto.idEnderecamentoArea = apartamento.tbEnderecamentoAndar.tbEnderecamentoPredio.tbEnderecamentoRua.idEnderecamentoArea;
            produto.idEnderecamentoRua = apartamento.tbEnderecamentoAndar.tbEnderecamentoPredio.idEnderecamentoRua;
            produto.idEnderecamentoPredio = apartamento.tbEnderecamentoAndar.idEnderecamentoPredio;
            produto.idEnderecamentoAndar = apartamento.idEnderecamentoAndar;
            produto.idEnderecamentoApartamento = apartamento.idEnderecamentoApartamento;
            produto.liberado = true;
            produto.idCentroDistribuicao = (apartamento.tbEnderecamentoAndar.tbEnderecamentoPredio.tbEnderecamentoRua.tbEnderecamentoArea.idCentroDistribuicao ?? 1);
            AdicionaQueueReservaEstoque(produto.produtoId);

            var pedidoItem =
                (from c in data.tbPedidoFornecedorItems
                 where c.idPedidoFornecedorItem == idPedidoFornecedorItem
                 select c).First();
            pedidoItem.liberado = true;

            data.SubmitChanges();
            if (pedidoItem.idPedido != null)
            {
                AdicionaQueueChecagemPedidoCompleto((int)pedidoItem.idPedido);
            }
            return "1";
        }

        return "0";
    }

    [WebMethod]
    public string EnderecarProdutoTransferenciaPedido(string etiqueta, string endereco, string idTransferenciaEndereco, string pedido)
    {
        int idPedidoFornecedorItem = 0;
        int idTransferencia = Convert.ToInt32(idTransferenciaEndereco);
        int pedidoId = Convert.ToInt32(pedido);

        try
        {
            idPedidoFornecedorItem = Convert.ToInt32(etiqueta);
        }
        catch (Exception)
        {
            return "0";
        }
        var lista = endereco.Split('-');
        if (lista.Count() != 2)
        {
            return "0";
        }

        int idEndereco = 0;
        string tipo = "";
        try
        {
            idEndereco = Convert.ToInt32(lista[0]);
            tipo = lista[1];
        }
        catch (Exception)
        {
            return "0";
            throw;
        }

        if (tipo != "esp")
        {
            return "0_1";
        }


        var data = new dbCommerceDataContext();

        var validaEtiqueta = (from c in data.tbTransferenciaEnderecoProdutos
                              where c.idTransferenciaEndereco == idTransferencia && c.idPedidoFornecedorItem == idPedidoFornecedorItem
                              select c).FirstOrDefault();
        validaEtiqueta.dataEnderecamento = DateTime.Now;

        var validaEtiquetas = (from c in data.tbTransferenciaEnderecoProdutos
                               where c.idPedidoFornecedorItem == idPedidoFornecedorItem && c.dataEnderecamento == null
                               select c);
        foreach (var valida in validaEtiquetas)
        {
            valida.dataEnderecamento = DateTime.Now;
        }

        int idUsuarioExpedicao = (int)validaEtiqueta.tbTransferenciaEndereco.idUsuarioExpedicao;
        var usuarioExpedicao = (from c in data.tbUsuarioExpedicaos where c.idUsuarioExpedicao == idUsuarioExpedicao select c).First();



        var produto = (from c in data.tbProdutoEstoques where c.idPedidoFornecedorItem == idPedidoFornecedorItem select c).First();
        if (produto.liberado == true | produto.pedidoIdReserva != null | produto.itemPedidoIdReserva != null) return "0";

        produto.liberado = true;
        produto.idCentroDistribuicao = usuarioExpedicao.idCentroDistribuicao;

        var itemAguardandoEnvio =
            (from c in data.tbItemPedidoEstoques
             where c.tbItensPedido.pedidoId == pedidoId && c.tbItensPedido.tbPedido.statusDoPedido == 11 && c.enviado == false && c.produtoId == produto.produtoId && c.reservado == false && c.cancelado == false && c.dataLimite != null
             select c).FirstOrDefault();
        if (itemAguardandoEnvio == null) return "0";


        var objEndereco = (from c in data.tbEnderecoSeparacaos where c.idEnderecamentoSeparacao == idEndereco select c).FirstOrDefault();
        if (objEndereco == null)
        {
            return "0";
        }

        var pedidoDetalhe = (from c in data.tbPedidos where c.pedidoId == pedidoId select c).First();
        pedidoDetalhe.envioLiberado = true;
        pedidoDetalhe.idUsuarioSeparacao = idUsuarioExpedicao;
        pedidoDetalhe.dataInicioSeparacao = DateTime.Now;
        pedidoDetalhe.separado = true;
        pedidoDetalhe.dataFimSeparacao = DateTime.Now;


        var queue = new tbQueue();
        queue.agendamento = DateTime.Now;
        queue.andamento = false;
        queue.concluido = false;
        queue.idRelacionado = pedidoId;
        queue.mensagem = "";
        queue.tipoQueue = 8;
        data.tbQueues.InsertOnSubmit(queue);

        int idEnvio = 0;
        var pedidoSepararCheck =
            (from c in data.tbPedidoEnvios
             where c.idPedido == pedidoId && c.dataFimEmbalagem == null && (c.idCentroDeDistribuicao == 0 | c.idCentroDeDistribuicao == 1)
             select c).FirstOrDefault();
        if (pedidoSepararCheck == null)
        {
            var dataAdicionarEnvio = new dbCommerceDataContext();
            var pedidoEnvio = new tbPedidoEnvio();
            pedidoEnvio.idUsuarioSeparacao = idUsuarioExpedicao;
            pedidoEnvio.dataInicioSeparacao = DateTime.Now;
            pedidoEnvio.dataFimSeparacao = DateTime.Now;
            pedidoEnvio.idEnderecoSeparacao = idEndereco;
            pedidoEnvio.idPedido = pedidoId;
            pedidoEnvio.idCentroDeDistribuicao = produto.idCentroDistribuicao;
            dataAdicionarEnvio.tbPedidoEnvios.InsertOnSubmit(pedidoEnvio);
            dataAdicionarEnvio.SubmitChanges();
            idEnvio = pedidoEnvio.idPedidoEnvio;

            var usuarioSeparacao = (from c in data.tbUsuarioExpedicaos where c.idUsuarioExpedicao == idUsuarioExpedicao select c).FirstOrDefault();
            if (usuarioSeparacao != null)
            {
                rnInteracoes.interacaoInclui(pedidoId, "Pedido separado", usuarioSeparacao.nome, "False");
            }
        }
        else
        {
            pedidoSepararCheck.dataFimSeparacao = DateTime.Now;
            pedidoSepararCheck.idEnderecoSeparacao = idEndereco;
            idEnvio = pedidoSepararCheck.idPedidoEnvio;
        }


        itemAguardandoEnvio.reservado = true;
        itemAguardandoEnvio.dataReserva = DateTime.Now;
        produto.pedidoIdReserva = itemAguardandoEnvio.tbItensPedido.pedidoId;
        produto.itemPedidoIdReserva = itemAguardandoEnvio.itemPedidoId;
        produto.idItemPedidoEstoqueReserva = itemAguardandoEnvio.idItemPedidoEstoque;
        produto.pedidoId = itemAguardandoEnvio.tbItensPedido.pedidoId;
        produto.itemPedidoId = itemAguardandoEnvio.itemPedidoId;
        produto.idItemPedidoEstoque = itemAguardandoEnvio.idItemPedidoEstoque;
        produto.idPedidoEnvio = idEnvio;

        var pedidoItem =
            (from c in data.tbPedidoFornecedorItems
             where c.idPedidoFornecedorItem == idPedidoFornecedorItem
             select c).First();
        pedidoItem.liberado = true;
        data.SubmitChanges();
        return "1";
    }


    [WebMethod]
    public string EnderecarProduto(string etiqueta, string endereco)
    {
        int idPedidoFornecedorItem = 0;
        try
        {
            idPedidoFornecedorItem = Convert.ToInt32(etiqueta);
        }
        catch (Exception)
        {
            return "0";
        }
        var lista = endereco.Split('-');
        if (lista.Count() != 2)
        {
            return "0";
        }

        int idEndereco = 0;
        int tipo = 0;
        try
        {
            idEndereco = Convert.ToInt32(lista[0]);
            tipo = Convert.ToInt32(lista[1]);
        }
        catch (Exception)
        {
            return "0";
            throw;
        }
        var data = new dbCommerceDataContext();

        if (tipo == 1)
        {
            var area =
                (from c in data.tbEnderecamentoAreas where c.idEnderecamentoArea == idEndereco select c).FirstOrDefault();
            if (area == null) return "0";

            var produto =
                (from c in data.tbProdutoEstoques where c.idPedidoFornecedorItem == idPedidoFornecedorItem select c)
                    .First();
            produto.idEnderecamentoArea = idEndereco;

            var pedidoItem =
                (from c in data.tbPedidoFornecedorItems
                 where c.idPedidoFornecedorItem == idPedidoFornecedorItem
                 select c).First();
            pedidoItem.liberado = true;

            data.SubmitChanges();
            if (pedidoItem.idPedido != null)
            {
                AdicionaQueueChecagemPedidoCompleto((int)pedidoItem.idPedido);
            }
            return "1";
        }
        if (tipo == 2)
        {
            var rua =
                (from c in data.tbEnderecamentoRuas where c.idEnderecamentoRua == idEndereco select c).FirstOrDefault();
            if (rua == null) return "0";

            var produto =
                (from c in data.tbProdutoEstoques where c.idPedidoFornecedorItem == idPedidoFornecedorItem select c)
                    .First();
            produto.idEnderecamentoArea = rua.idEnderecamentoArea;
            produto.idEnderecamentoRua = rua.idEnderecamentoRua;

            var pedidoItem =
                (from c in data.tbPedidoFornecedorItems
                 where c.idPedidoFornecedorItem == idPedidoFornecedorItem
                 select c).First();
            pedidoItem.liberado = true;

            data.SubmitChanges();
            if (pedidoItem.idPedido != null)
            {
                AdicionaQueueChecagemPedidoCompleto((int)pedidoItem.idPedido);
            }
            return "1";
        }
        if (tipo == 3)
        {
            var predio =
                (from c in data.tbEnderecamentoPredios where c.idEnderecamentoPredio == idEndereco select c).FirstOrDefault();
            if (predio == null) return "0";

            var produto =
                (from c in data.tbProdutoEstoques where c.idPedidoFornecedorItem == idPedidoFornecedorItem select c)
                    .First();
            produto.idEnderecamentoArea = predio.tbEnderecamentoRua.idEnderecamentoArea;
            produto.idEnderecamentoRua = predio.idEnderecamentoRua;
            produto.idEnderecamentoPredio = predio.idEnderecamentoPredio;

            var pedidoItem =
                (from c in data.tbPedidoFornecedorItems
                 where c.idPedidoFornecedorItem == idPedidoFornecedorItem
                 select c).First();
            pedidoItem.liberado = true;

            data.SubmitChanges();
            if (pedidoItem.idPedido != null)
            {
                AdicionaQueueChecagemPedidoCompleto((int)pedidoItem.idPedido);
            }
            return "1";
        }
        if (tipo == 4)
        {
            var andar =
                (from c in data.tbEnderecamentoAndars where c.idEnderecamentoAndar == idEndereco select c).FirstOrDefault();
            if (andar == null) return "0";

            var produto =
                (from c in data.tbProdutoEstoques where c.idPedidoFornecedorItem == idPedidoFornecedorItem select c)
                    .First();
            produto.idEnderecamentoArea = andar.tbEnderecamentoPredio.tbEnderecamentoRua.idEnderecamentoArea;
            produto.idEnderecamentoRua = andar.tbEnderecamentoPredio.idEnderecamentoRua;
            produto.idEnderecamentoPredio = andar.idEnderecamentoPredio;
            produto.idEnderecamentoAndar = andar.idEnderecamentoAndar;

            var pedidoItem =
                (from c in data.tbPedidoFornecedorItems
                 where c.idPedidoFornecedorItem == idPedidoFornecedorItem
                 select c).First();
            pedidoItem.liberado = true;

            data.SubmitChanges();
            if (pedidoItem.idPedido != null)
            {
                AdicionaQueueChecagemPedidoCompleto((int)pedidoItem.idPedido);
            }
            return "1";
        }
        if (tipo == 5)
        {
            var apartamento =
                (from c in data.tbEnderecamentoApartamentos where c.idEnderecamentoApartamento == idEndereco select c).FirstOrDefault();
            if (apartamento == null) return "0";

            var produto =
                (from c in data.tbProdutoEstoques where c.idPedidoFornecedorItem == idPedidoFornecedorItem select c)
                    .First();
            produto.idEnderecamentoArea = apartamento.tbEnderecamentoAndar.tbEnderecamentoPredio.tbEnderecamentoRua.idEnderecamentoArea;
            produto.idEnderecamentoRua = apartamento.tbEnderecamentoAndar.tbEnderecamentoPredio.idEnderecamentoRua;
            produto.idEnderecamentoPredio = apartamento.tbEnderecamentoAndar.idEnderecamentoPredio;
            produto.idEnderecamentoAndar = apartamento.idEnderecamentoAndar;
            produto.idEnderecamentoApartamento = apartamento.idEnderecamentoApartamento;

            var pedidoItem =
                (from c in data.tbPedidoFornecedorItems
                 where c.idPedidoFornecedorItem == idPedidoFornecedorItem
                 select c).First();
            pedidoItem.liberado = true;

            data.SubmitChanges();
            if (pedidoItem.idPedido != null)
            {
                AdicionaQueueChecagemPedidoCompleto((int)pedidoItem.idPedido);
            }
            return "1";
        }

        return "0";
    }


    [WebMethod]
    public string ConferirEndereco(string endereco)
    {
        var lista = endereco.Split('-');
        if (lista.Count() != 2)
        {
            return "0";
        }

        int idEndereco = 0;
        string tipo = lista[1];
        try
        {
            idEndereco = Convert.ToInt32(lista[0]);
        }
        catch (Exception)
        {
            return "0";
            throw;
        }
        var data = new dbCommerceDataContext();

        if (tipo == "1")
        {
            var area =
                (from c in data.tbEnderecamentoAreas where c.idEnderecamentoArea == idEndereco select c).FirstOrDefault();
            if (area == null) return "0";
            return area.area;
        }
        if (tipo == "2")
        {
            var rua =
                (from c in data.tbEnderecamentoRuas where c.idEnderecamentoRua == idEndereco select c).FirstOrDefault();
            if (rua == null) return "0";

            return "A:" + rua.tbEnderecamentoArea.area + " R:" + rua.rua;
        }
        if (tipo == "3")
        {
            var predio =
                (from c in data.tbEnderecamentoPredios where c.idEnderecamentoPredio == idEndereco select c).FirstOrDefault();
            if (predio == null) return "0";

            return "A:" + predio.tbEnderecamentoRua.tbEnderecamentoArea.area + " R:" + predio.tbEnderecamentoRua.rua + "L:" + predio.tbEnderecamentoLado.lado + " P:" + predio.predio;
        }
        if (tipo == "4")
        {
            var andar =
                (from c in data.tbEnderecamentoAndars where c.idEnderecamentoAndar == idEndereco select c).FirstOrDefault();
            if (andar == null) return "0";


            return "A:" + andar.tbEnderecamentoPredio.tbEnderecamentoRua.tbEnderecamentoArea.area + " R:" + andar.tbEnderecamentoPredio.tbEnderecamentoRua.rua + " L:" + andar.tbEnderecamentoPredio.tbEnderecamentoLado.lado + " P:" + andar.tbEnderecamentoPredio.predio + " A:" + andar.andar;
        }
        if (tipo == "5")
        {
            var apartamento =
                (from c in data.tbEnderecamentoApartamentos where c.idEnderecamentoApartamento == idEndereco select c).FirstOrDefault();
            if (apartamento == null) return "0";

            return "A:" + apartamento.tbEnderecamentoAndar.tbEnderecamentoPredio.tbEnderecamentoRua.tbEnderecamentoArea.area + " R:" + apartamento.tbEnderecamentoAndar.tbEnderecamentoPredio.tbEnderecamentoRua.rua + " L:" + apartamento.tbEnderecamentoAndar.tbEnderecamentoPredio.tbEnderecamentoLado.lado + " P:" + apartamento.tbEnderecamentoAndar.tbEnderecamentoPredio.predio + " A:" + apartamento.tbEnderecamentoAndar.andar + " AP:" + apartamento.apartamento;
        }
        if (tipo == "esp")
        {
            var enderecamentoSeparacao =
                (from c in data.tbEnderecoSeparacaos where c.idEnderecamentoSeparacao == idEndereco select c)
                    .FirstOrDefault();
            if (enderecamentoSeparacao == null) return "0";

            return enderecamentoSeparacao.enderecoSeparacao;
        }

        return "0";
    }
    


    [WebMethod]
    public string ConferirEnderecoProduto(string etiqueta, string idusuarioexpedicao)
    {
        int idPedidoFornecedorItem = 0;
        int.TryParse(etiqueta, out idPedidoFornecedorItem);
        int idUsuario = Convert.ToInt32(idusuarioexpedicao);
        
        if (idPedidoFornecedorItem == 0)
        {
            return "0|Etiqueta Incorreta";
        }

        var data = new dbCommerceDataContext();
        var usuario = (from c in data.tbUsuarioExpedicaos where c.idUsuarioExpedicao == idUsuario select c).First();
        var produtoEstoque =
            (from c in data.tbProdutoEstoques where c.idPedidoFornecedorItem == idPedidoFornecedorItem select c)
                .FirstOrDefault();
        if (produtoEstoque == null)
        {
            rnLog.InsereLogEtiqueta(usuario.nome, idPedidoFornecedorItem, "Tentativa de reendereçar etiqueta que não consta em estoque");
            return "0|" + "Produto não consta em estoque";
        }



        if (produtoEstoque.enviado)
        {
            rnLog.InsereLogEtiqueta(usuario.nome, idPedidoFornecedorItem, "Tentativa de reendereçar produto que consta como enviado");
            string mensagem = produtoEstoque.tbProduto.produtoNome + " consta como enviado";
            registrarRetornoReenderecar(mensagem, idPedidoFornecedorItem, Convert.ToInt32(idusuarioexpedicao));
            return "0|" + mensagem;
        }
        
        if (produtoEstoque.pedidoIdReserva == null)
        {
            bool possuiProdutoAtivo = false;
            var produtoPrincipal = (from c in data.tbProdutos where c.produtoId == produtoEstoque.produtoId && c.produtoAtivo.ToLower() == "true" select c).Any();
            if (produtoPrincipal) possuiProdutoAtivo = true;

            var produtosFilhos = (from c in data.tbProdutoRelacionados where c.idProdutoFilho == produtoEstoque.produtoId select c).ToList();
            foreach(var produtoFilho in produtosFilhos)
            {
                var detalheProdutoFilho = (from c in data.tbProdutos where c.produtoId == produtoFilho.idProdutoPai && c.produtoAtivo.ToLower() == "true" select c).Any();
                if (detalheProdutoFilho) possuiProdutoAtivo = true;
            }

            var produtosPersonalizados = (from c in data.tbProdutoPersonalizacaos where c.idProdutoFilhoPersonalizacao == produtoEstoque.produtoId select c).Any();
            if (produtosPersonalizados) possuiProdutoAtivo = true;

            var produtosAguardandoReserva = (from c in data.tbItemPedidoEstoques where c.cancelado == false && c.enviado == false && c.reservado == false && c.produtoId == produtoEstoque.produtoId select c).Any();
            if(produtosAguardandoReserva) possuiProdutoAtivo = true;

            /*if(possuiProdutoAtivo == false)
            {
                string mensagem = "PRODUTO DESATIVADO - SEPARAR";
                registrarRetornoReenderecar(mensagem, idPedidoFornecedorItem, Convert.ToInt32(idusuarioexpedicao));
                return mensagem;
            }*/
        }

        if (produtoEstoque.pedidoId != null)
        {
            rnLog.InsereLogEtiqueta(usuario.nome, idPedidoFornecedorItem, "Tentativa de reendereçar produto que consta como separado para um pedido");
            string mensagem = produtoEstoque.tbProduto.produtoNome + " consta como separado para um pedido";
            registrarRetornoReenderecar(mensagem, idPedidoFornecedorItem, Convert.ToInt32(idusuarioexpedicao));
            return "0|" + mensagem;
        }

        string retorno = produtoEstoque.tbProduto.produtoNome;
        if ((produtoEstoque.tbPedidoFornecedorItem.liberado ?? false) == false)
        {
            rnLog.InsereLogEtiqueta(usuario.nome, idPedidoFornecedorItem, "Etiqueta lida para reendereçar");
            retorno += " não está liberado. Favor reendereçar";
            registrarRetornoReenderecar(retorno, idPedidoFornecedorItem, Convert.ToInt32(idusuarioexpedicao));
            return "1|" + retorno;
        }

        if (produtoEstoque.idEnderecamentoArea == null)
        {
            rnLog.InsereLogEtiqueta(usuario.nome, idPedidoFornecedorItem, "Etiqueta lida para reendereçar");
            retorno += " não está endereçado. Favor endereçar";
            registrarRetornoReenderecar(retorno, idPedidoFornecedorItem, Convert.ToInt32(idusuarioexpedicao));
            return retorno;
        }
        retorno += " A:" + produtoEstoque.tbEnderecamentoArea.area;

        if (produtoEstoque.idEnderecamentoRua == null)
        {
            retorno += " não está endereçado. Favor endereçar";
            registrarRetornoReenderecar(retorno, idPedidoFornecedorItem, Convert.ToInt32(idusuarioexpedicao));
            return retorno;
        }
        retorno += " R:" + produtoEstoque.tbEnderecamentoRua.rua;

        if (produtoEstoque.idEnderecamentoPredio == null)
        {
            retorno += " não está endereçado. Favor endereçar";
            registrarRetornoReenderecar(retorno, idPedidoFornecedorItem, Convert.ToInt32(idusuarioexpedicao));
            return "1|" + retorno;
        }
        retorno += " P:" + produtoEstoque.tbEnderecamentoPredio.predio;
        retorno += " L:" + produtoEstoque.tbEnderecamentoPredio.tbEnderecamentoLado.lado;

        if (produtoEstoque.idEnderecamentoAndar == null)
        {
            retorno += " não está endereçado. Favor endereçar";
            registrarRetornoReenderecar(retorno, idPedidoFornecedorItem, Convert.ToInt32(idusuarioexpedicao));
            return "1|" + retorno;
        }
        retorno += " P:" + produtoEstoque.tbEnderecamentoAndar.andar;
        registrarRetornoReenderecar(retorno, idPedidoFornecedorItem, Convert.ToInt32(idusuarioexpedicao));
        rnLog.InsereLogEtiqueta(usuario.nome, idPedidoFornecedorItem, "Etiqueta lida para reendereçar");
        return "1|" + retorno;
    }

    [WebMethod]
    public string ConferirEnderecoProdutoRegistrar(string etiqueta, string endereco, string idusuarioexpedicao)
    {
       
        int idPedidoFornecedorItem = 0;
        int.TryParse(etiqueta, out idPedidoFornecedorItem);

        if (idPedidoFornecedorItem == 0)
        {
            return "Etiqueta Incorreta";
        }
        int idUsuario = Convert.ToInt32(idusuarioexpedicao);
        var data = new dbCommerceDataContext();
        var usuario = (from c in data.tbUsuarioExpedicaos where c.idUsuarioExpedicao == idUsuario select c).First();
        int idEnderecamentoAndar = 0;
        var enderecoSplit = endereco.Split('-');
        if (enderecoSplit.Count() != 2)
        {
            rnLog.InsereLogEtiqueta(usuario.nome, idPedidoFornecedorItem, "Tentativa de reendereçar em endereco incorreto " + endereco);
            return "Etiqueta de endereço invalida";
        }


        int.TryParse(enderecoSplit[0], out idEnderecamentoAndar);
        if (idEnderecamentoAndar == 0)
        {
            rnLog.InsereLogEtiqueta(usuario.nome, idPedidoFornecedorItem, "Tentativa de reendereçar em endereco incorreto " + endereco);
            return "Etiqueta de endereço inválida";
        }
        
        var produtoEstoque = (from c in data.tbProdutoEstoques where c.idPedidoFornecedorItem == idPedidoFornecedorItem select c).FirstOrDefault();
        if (produtoEstoque == null)
        {
            rnLog.InsereLogEtiqueta(usuario.nome, idPedidoFornecedorItem, "Tentativa de reendereçar produto que não consta em estoque " + endereco);
            return "Produto não consta em estoque";
        }

        var transferenciasDeProduto = (from c in data.tbTransferenciaEnderecoProdutos where c.idPedidoFornecedorItem == idPedidoFornecedorItem && c.dataEnderecamento == null select c).ToList();
        foreach(var transferenciaDeProduto in transferenciasDeProduto)
        {
            transferenciaDeProduto.dataEnderecamento = DateTime.Now;
        }
        data.SubmitChanges();
        if (produtoEstoque.enviado)
        {
            rnLog.InsereLogEtiqueta(usuario.nome, idPedidoFornecedorItem, "Tentativa de reendereçar produto que consta como enviado " + endereco);
            registrarRetornoReenderecar(produtoEstoque.tbProduto.produtoNome + " consta como enviado", idPedidoFornecedorItem, Convert.ToInt32(idusuarioexpedicao));
            return produtoEstoque.tbProduto.produtoNome + " consta como enviado";
        }

        if (produtoEstoque.pedidoId != null)
        {
            rnLog.InsereLogEtiqueta(usuario.nome, idPedidoFornecedorItem, "Tentativa de reendereçar produto que consta como separado para um pedido " + endereco);
            registrarRetornoReenderecar(produtoEstoque.tbProduto.produtoNome + " consta como separado para um pedido", idPedidoFornecedorItem, Convert.ToInt32(idusuarioexpedicao));
            return produtoEstoque.tbProduto.produtoNome + " consta como separado para um pedido";
        }

        string retorno = produtoEstoque.tbProduto.produtoNome;
        if ((produtoEstoque.tbPedidoFornecedorItem.liberado ?? false) == false)
        {
            retorno += " não está liberado. Favor reendereçar";
            registrarRetornoReenderecar(retorno, idPedidoFornecedorItem, Convert.ToInt32(idusuarioexpedicao));
            //return retorno;
        }

        if (produtoEstoque.idEnderecamentoAndar == idEnderecamentoAndar)
        {
            produtoEstoque.dataConferencia = DateTime.Now;
            data.SubmitChanges();
            registrarRetornoReenderecar("Endereço correto", idPedidoFornecedorItem, Convert.ToInt32(idusuarioexpedicao));
            return "Endereço correto";
        }
        else
        {
            produtoEstoque.dataConferencia = DateTime.Now;
            int tipo = 0;
            try
            {
                tipo = Convert.ToInt32(endereco.Split('-')[1]);
            }
            catch (Exception ex)
            {

            }

            if (tipo == 1)
            {
                var area =
                    (from c in data.tbEnderecamentoAreas where c.idEnderecamentoArea == idEnderecamentoAndar select c).FirstOrDefault();
                if (area == null) return "0";
                produtoEstoque.idEnderecamentoArea = idEnderecamentoAndar;
                produtoEstoque.idCentroDistribuicao = (area.idCentroDistribuicao ?? 1);
                produtoEstoque.liberado = true;

                retorno += " A:" + produtoEstoque.tbEnderecamentoArea.area;
                rnLog.InsereLogEtiqueta(usuario.nome, idPedidoFornecedorItem, "Produto reenderecado " + endereco);
                data.SubmitChanges();
            }
            if (tipo == 2)
            {
                var rua = (from c in data.tbEnderecamentoRuas where c.idEnderecamentoRua == idEnderecamentoAndar select c).FirstOrDefault();
                if (rua == null) return "0";
                produtoEstoque.idEnderecamentoArea = rua.idEnderecamentoArea;
                produtoEstoque.idEnderecamentoRua = rua.idEnderecamentoRua;
                produtoEstoque.liberado = true;
                produtoEstoque.idCentroDistribuicao = (rua.tbEnderecamentoArea.idCentroDistribuicao ?? 1);
                rnLog.InsereLogEtiqueta(usuario.nome, idPedidoFornecedorItem, "Produto reenderecado " + endereco);
                data.SubmitChanges();
            }
            if (tipo == 3)
            {
                var predio = (from c in data.tbEnderecamentoPredios where c.idEnderecamentoPredio == idEnderecamentoAndar select c).FirstOrDefault();
                if (predio == null) return "0";
                produtoEstoque.idEnderecamentoArea = predio.tbEnderecamentoRua.idEnderecamentoArea;
                produtoEstoque.idEnderecamentoRua = predio.idEnderecamentoRua;
                produtoEstoque.idEnderecamentoPredio = predio.idEnderecamentoPredio;
                produtoEstoque.liberado = true;
                produtoEstoque.idCentroDistribuicao = (predio.tbEnderecamentoRua.tbEnderecamentoArea.idCentroDistribuicao ?? 1);
                rnLog.InsereLogEtiqueta(usuario.nome, idPedidoFornecedorItem, "Produto reenderecado " + endereco);
                data.SubmitChanges();
            }
            if (tipo == 4)
            {
                var andar = (from c in data.tbEnderecamentoAndars where c.idEnderecamentoAndar == idEnderecamentoAndar select c).FirstOrDefault();
                if (andar == null) return "0";

                produtoEstoque.idEnderecamentoArea = andar.tbEnderecamentoPredio.tbEnderecamentoRua.idEnderecamentoArea;
                produtoEstoque.idEnderecamentoRua = andar.tbEnderecamentoPredio.idEnderecamentoRua;
                produtoEstoque.idEnderecamentoPredio = andar.idEnderecamentoPredio;
                produtoEstoque.idEnderecamentoAndar = andar.idEnderecamentoAndar;
                produtoEstoque.idCentroDistribuicao = (andar.tbEnderecamentoPredio.tbEnderecamentoRua.tbEnderecamentoArea.idCentroDistribuicao ?? 1);

                if (produtoEstoque.liberado ?? false == false)
                {
                    produtoEstoque.liberado = true;
                    var pedidoItem =
                    (from c in data.tbPedidoFornecedorItems
                     where c.idPedidoFornecedorItem == idPedidoFornecedorItem
                     select c).First();
                    pedidoItem.liberado = true;
                    data.SubmitChanges();
                }

                AdicionaQueueReservaEstoque(produtoEstoque.produtoId);

                rnLog.InsereLogEtiqueta(usuario.nome, idPedidoFornecedorItem, "Produto reenderecado " + endereco);
                data.SubmitChanges();
            }
            if (tipo == 5)
            {
                var apartamento = (from c in data.tbEnderecamentoApartamentos where c.idEnderecamentoApartamento == idEnderecamentoAndar select c).FirstOrDefault();
                if (apartamento == null) return "0";
                produtoEstoque.idEnderecamentoArea = apartamento.tbEnderecamentoAndar.tbEnderecamentoPredio.tbEnderecamentoRua.idEnderecamentoArea;
                produtoEstoque.idEnderecamentoRua = apartamento.tbEnderecamentoAndar.tbEnderecamentoPredio.idEnderecamentoRua;
                produtoEstoque.idEnderecamentoPredio = apartamento.tbEnderecamentoAndar.idEnderecamentoPredio;
                produtoEstoque.idEnderecamentoAndar = apartamento.idEnderecamentoAndar;
                produtoEstoque.idEnderecamentoApartamento = apartamento.idEnderecamentoApartamento;
                produtoEstoque.liberado = true;
                produtoEstoque.idCentroDistribuicao = (apartamento.tbEnderecamentoAndar.tbEnderecamentoPredio.tbEnderecamentoRua.tbEnderecamentoArea.idCentroDistribuicao ?? 1);
                rnLog.InsereLogEtiqueta(usuario.nome, idPedidoFornecedorItem, "Produto reenderecado " + endereco);
                data.SubmitChanges();
                return "1";
            }
            registrarRetornoReenderecar("Produto Reendereçado", idPedidoFornecedorItem, Convert.ToInt32(idusuarioexpedicao));
            return "Produto Reendereçado";
        }

    }

    [WebMethod]
    public string ConferirEnderecoProdutoTransferir(string etiqueta, string idusuarioexpedicao)
    {
        int idPedidoFornecedorItem = 0;
        int.TryParse(etiqueta, out idPedidoFornecedorItem);
        
        if (idPedidoFornecedorItem == 0)
        {
            return "Etiqueta Incorreta";
        }

        var data = new dbCommerceDataContext();
        var produtoEstoque =
            (from c in data.tbProdutoEstoques where c.idPedidoFornecedorItem == idPedidoFornecedorItem select c)
                .FirstOrDefault();
        if (produtoEstoque == null)
        {
            return "Produto não consta em estoque";
        }

        if (produtoEstoque.enviado)
        {
            string mensagem = produtoEstoque.tbProduto.produtoNome + " consta como enviado";
            registrarRetornoReenderecar(mensagem, idPedidoFornecedorItem, Convert.ToInt32(idusuarioexpedicao));
            return mensagem;
        }

        if(produtoEstoque.pedidoIdReserva != null)
        {
            var pedidoReserva = (from c in data.tbPedidos where c.pedidoId == produtoEstoque.pedidoIdReserva select c).FirstOrDefault();
            if(pedidoReserva != null)
            {
                var limite = Convert.ToDateTime("17/02/2017");
                var prazoMaximo = pedidoReserva.prazoMaximoPostagemAtualizado;
                if (prazoMaximo == null) prazoMaximo = pedidoReserva.prazoFinalPedido;
                if (prazoMaximo == null) prazoMaximo = DateTime.Now;
                if (prazoMaximo.Value.Date > limite.Date)
                {
                    string mensagem = "Transferir produto para novo CD";
                    registrarRetornoReenderecar(mensagem, idPedidoFornecedorItem, Convert.ToInt32(idusuarioexpedicao));
                    return mensagem;
                }
            }
        }
        else
        {
            string mensagem = "Transferir produto para novo CD";
            registrarRetornoReenderecar(mensagem, idPedidoFornecedorItem, Convert.ToInt32(idusuarioexpedicao));
            return mensagem;
        }

        if (produtoEstoque.pedidoId != null)
        {
            string mensagem = produtoEstoque.tbProduto.produtoNome + " consta como separado para um pedido";
            registrarRetornoReenderecar(mensagem, idPedidoFornecedorItem, Convert.ToInt32(idusuarioexpedicao));
            return mensagem;
        }

        string retorno = produtoEstoque.tbProduto.produtoNome;
        if ((produtoEstoque.tbPedidoFornecedorItem.liberado ?? false) == false)
        {
            retorno += " não está liberado. Favor reendereçar";
            registrarRetornoReenderecar(retorno, idPedidoFornecedorItem, Convert.ToInt32(idusuarioexpedicao));
            return retorno;
        }

        if (produtoEstoque.idEnderecamentoArea == null)
        {
            retorno += " não está endereçado. Favor endereçar";
            registrarRetornoReenderecar(retorno, idPedidoFornecedorItem, Convert.ToInt32(idusuarioexpedicao));
            return retorno;
        }
        retorno += " A:" + produtoEstoque.tbEnderecamentoArea.area;

        if (produtoEstoque.idEnderecamentoRua == null)
        {
            retorno += " não está endereçado. Favor endereçar";
            registrarRetornoReenderecar(retorno, idPedidoFornecedorItem, Convert.ToInt32(idusuarioexpedicao));
            return retorno;
        }
        retorno += " R:" + produtoEstoque.tbEnderecamentoRua.rua;

        if (produtoEstoque.idEnderecamentoPredio == null)
        {
            retorno += " não está endereçado. Favor endereçar";
            registrarRetornoReenderecar(retorno, idPedidoFornecedorItem, Convert.ToInt32(idusuarioexpedicao));
            return retorno;
        }
        retorno += " P:" + produtoEstoque.tbEnderecamentoPredio.predio;
        retorno += " L:" + produtoEstoque.tbEnderecamentoPredio.tbEnderecamentoLado.lado;

        if (produtoEstoque.idEnderecamentoAndar == null)
        {
            retorno += " não está endereçado. Favor endereçar";
            registrarRetornoReenderecar(retorno, idPedidoFornecedorItem, Convert.ToInt32(idusuarioexpedicao));
            return retorno;
        }
        retorno += " P:" + produtoEstoque.tbEnderecamentoAndar.andar;
        registrarRetornoReenderecar(retorno, idPedidoFornecedorItem, Convert.ToInt32(idusuarioexpedicao));
        return retorno;
    }

    [WebMethod]
    public string ConferirEnderecoProdutoTransferirRegistrar(string etiqueta, string endereco, string idusuarioexpedicao)
    {
        int idPedidoFornecedorItem = 0;
        int.TryParse(etiqueta, out idPedidoFornecedorItem);

        if (idPedidoFornecedorItem == 0)
        {
            return "Etiqueta Incorreta";
        }

        int idEnderecamentoAndar = 0;
        var enderecoSplit = endereco.Split('-');
        if (enderecoSplit.Count() != 2)
        {
            return "Etiqueta de endereço invalida";
        }

        int.TryParse(enderecoSplit[0], out idEnderecamentoAndar);
        if (idEnderecamentoAndar == 0)
        {
            return "Etiqueta de endereço inválida";
        }

        var data = new dbCommerceDataContext();
        var produtoEstoque = (from c in data.tbProdutoEstoques where c.idPedidoFornecedorItem == idPedidoFornecedorItem select c).FirstOrDefault();
        if (produtoEstoque == null)
        {
            return "Produto não consta em estoque";
        }

        if (produtoEstoque.enviado)
        {
            registrarRetornoReenderecar(produtoEstoque.tbProduto.produtoNome + " consta como enviado", idPedidoFornecedorItem, Convert.ToInt32(idusuarioexpedicao));
            return produtoEstoque.tbProduto.produtoNome + " consta como enviado";
        }

        if (produtoEstoque.pedidoId != null)
        {
            registrarRetornoReenderecar(produtoEstoque.tbProduto.produtoNome + " consta como separado para um pedido", idPedidoFornecedorItem, Convert.ToInt32(idusuarioexpedicao));
            return produtoEstoque.tbProduto.produtoNome + " consta como separado para um pedido";
        }

        string retorno = produtoEstoque.tbProduto.produtoNome;
        if ((produtoEstoque.tbPedidoFornecedorItem.liberado ?? false) == false)
        {
            retorno += " não está liberado. Favor reendereçar";
            registrarRetornoReenderecar(retorno, idPedidoFornecedorItem, Convert.ToInt32(idusuarioexpedicao));
            return retorno;
        }

        if (produtoEstoque.idEnderecamentoAndar == idEnderecamentoAndar)
        {
            produtoEstoque.dataConferencia = DateTime.Now;
            data.SubmitChanges();
            registrarRetornoReenderecar("Endereço correto", idPedidoFornecedorItem, Convert.ToInt32(idusuarioexpedicao));
            return "Endereço correto";
        }
        else
        {
            produtoEstoque.dataConferencia = DateTime.Now;
            int tipo = 0;
            try
            {
                tipo = Convert.ToInt32(endereco.Split('-')[1]);
            }
            catch (Exception ex)
            {

            }

            if (tipo == 1)
            {
                var area =
                    (from c in data.tbEnderecamentoAreas where c.idEnderecamentoArea == idEnderecamentoAndar select c).FirstOrDefault();
                if (area == null) return "0";
                produtoEstoque.idEnderecamentoArea = idEnderecamentoAndar;
                produtoEstoque.idCentroDistribuicao = (area.idCentroDistribuicao ?? 1);
                data.SubmitChanges();
            }
            if (tipo == 2)
            {
                var rua = (from c in data.tbEnderecamentoRuas where c.idEnderecamentoRua == idEnderecamentoAndar select c).FirstOrDefault();
                if (rua == null) return "0";
                produtoEstoque.idEnderecamentoArea = rua.idEnderecamentoArea;
                produtoEstoque.idEnderecamentoRua = rua.idEnderecamentoRua;
                produtoEstoque.idCentroDistribuicao = (rua.tbEnderecamentoArea.idCentroDistribuicao ?? 1);
                data.SubmitChanges();
            }
            if (tipo == 3)
            {
                var predio = (from c in data.tbEnderecamentoPredios where c.idEnderecamentoPredio == idEnderecamentoAndar select c).FirstOrDefault();
                if (predio == null) return "0";
                produtoEstoque.idEnderecamentoArea = predio.tbEnderecamentoRua.idEnderecamentoArea;
                produtoEstoque.idEnderecamentoRua = predio.idEnderecamentoRua;
                produtoEstoque.idEnderecamentoPredio = predio.idEnderecamentoPredio;
                produtoEstoque.idCentroDistribuicao = (predio.tbEnderecamentoRua.tbEnderecamentoArea.idCentroDistribuicao ?? 1);
                data.SubmitChanges();
            }
            if (tipo == 4)
            {
                var andar = (from c in data.tbEnderecamentoAndars where c.idEnderecamentoAndar == idEnderecamentoAndar select c).FirstOrDefault();
                if (andar == null) return "0";

                produtoEstoque.idEnderecamentoArea = andar.tbEnderecamentoPredio.tbEnderecamentoRua.idEnderecamentoArea;
                produtoEstoque.idEnderecamentoRua = andar.tbEnderecamentoPredio.idEnderecamentoRua;
                produtoEstoque.idEnderecamentoPredio = andar.idEnderecamentoPredio;
                produtoEstoque.idEnderecamentoAndar = andar.idEnderecamentoAndar;
                produtoEstoque.idCentroDistribuicao = (andar.tbEnderecamentoPredio.tbEnderecamentoRua.tbEnderecamentoArea.idCentroDistribuicao ?? 1);
                data.SubmitChanges();
            }
            if (tipo == 5)
            {
                var apartamento = (from c in data.tbEnderecamentoApartamentos where c.idEnderecamentoApartamento == idEnderecamentoAndar select c).FirstOrDefault();
                if (apartamento == null) return "0";
                produtoEstoque.idEnderecamentoArea = apartamento.tbEnderecamentoAndar.tbEnderecamentoPredio.tbEnderecamentoRua.idEnderecamentoArea;
                produtoEstoque.idEnderecamentoRua = apartamento.tbEnderecamentoAndar.tbEnderecamentoPredio.idEnderecamentoRua;
                produtoEstoque.idEnderecamentoPredio = apartamento.tbEnderecamentoAndar.idEnderecamentoPredio;
                produtoEstoque.idEnderecamentoAndar = apartamento.idEnderecamentoAndar;
                produtoEstoque.idEnderecamentoApartamento = apartamento.idEnderecamentoApartamento;
                produtoEstoque.idCentroDistribuicao = (apartamento.tbEnderecamentoAndar.tbEnderecamentoPredio.tbEnderecamentoRua.tbEnderecamentoArea.idCentroDistribuicao ?? 1);
                data.SubmitChanges();
                return "1";
            }
            registrarRetornoReenderecar("Produto Reendereçado", idPedidoFornecedorItem, Convert.ToInt32(idusuarioexpedicao));
            return "Produto Reendereçado";
        }

    }


    private void registrarRetornoReenderecar(string mensagem, int idPedidoFornecedorItem, int idUsuarioExpedicao)
    {
        var data = new dbCommerceDataContext();
        var etiqueta = (from c in data.tbProdutoEstoques where c.idPedidoFornecedorItem == idPedidoFornecedorItem select c).FirstOrDefault();
        etiqueta.dataConferencia = DateTime.Now;
        var usuario = (from c in data.tbUsuarioExpedicaos where c.idUsuarioExpedicao == idUsuarioExpedicao select c).FirstOrDefault();
        if(usuario != null)
        {
            etiqueta.usuarioConferencia = usuario.nome;
        }
        etiqueta.mensagemConferencia = mensagem;
        data.SubmitChanges();

    }
    public static void AdicionaQueueReservaEstoque(int produtoId)
    {
        var data = new dbCommerceDataContext();
        var queue = new tbQueue();
        queue.agendamento = DateTime.Now;
        queue.andamento = false;
        queue.concluido = false;
        queue.idRelacionado = produtoId;
        queue.mensagem = "";
        queue.tipoQueue = 21;
        data.tbQueues.InsertOnSubmit(queue);
        data.SubmitChanges();
        data.Dispose();
    }

    public static void AdicionaQueueChecagemPedidoCompleto(int pedidoId)
    {
        var data = new dbCommerceDataContext();
        var queue = new tbQueue();
        queue.agendamento = DateTime.Now;
        queue.andamento = false;
        queue.concluido = false;
        queue.idRelacionado = pedidoId;
        queue.mensagem = "";
        queue.tipoQueue = 8;
        data.tbQueues.InsertOnSubmit(queue);
        data.SubmitChanges();
        data.Dispose();
    }
}
