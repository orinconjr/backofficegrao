﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;

/// <summary>
/// Summary description for rnFrete
/// </summary>
public class rnFrete
{
    public class RetornoCalculoFrete
    {
        public decimal valor { get; set; }
        public int prazo { get; set; }
        public List<string> detalhamento { get; set; }
        public int pesoAferido { get; set; }
    }

    public class Pacotes
    {
        public int peso { get; set; }
        public decimal largura { get; set; }
        public decimal altura { get; set; }
        public decimal comprimento { get; set; }
    }
    public static List<site_CepFretePorCepPesoTotalResult> montaFretePorPedidoId(int pedidoId)
    {
        var pedidoDc = new dbCommerceDataContext();
        var pedido = (from c in pedidoDc.tbPedidos where c.pedidoId == pedidoId select c).First();
        string cepDeEntrega = pedido.endCep;

        var itensPedidoDc = new dbCommerceDataContext();
        var itensPedido = (from c in itensPedidoDc.tbItensPedidos
                           where c.pedidoId == pedidoId
                           select new { c.produtoId, c.itemQuantidade, cancelado = c.cancelado == null ? false : c.cancelado, brinde = c.brinde == null ? false : c.brinde }).Where(x => x.cancelado == false && x.brinde == false);

        var listaProdutos = itensPedido.Select(x => x.produtoId).ToList();
        var produtosDc = new dbCommerceDataContext();
        long cep = Convert.ToInt64(cepDeEntrega.Replace("-", ""));
        var produtos = (from c in produtosDc.tbProdutos
                        where listaProdutos.Contains(c.produtoId)
                        select new { c.produtoId, c.produtoPeso, c.produtoPrecoPromocional, c.produtoPreco, c.produtoPrecoDeCusto, descontoProgressivoFrete = c.descontoProgressivoFrete == null ? "False" : c.descontoProgressivoFrete, c.descontoProgressivoFretePorcentagem });


        decimal pesoTotalProdutoComum = 0;
        decimal precoTotalProdutoComum = 0;
        foreach (var produto in produtos.Where(x => x.descontoProgressivoFrete.ToLower() != "true"))
        {
            var produtoPreco = produto.produtoPrecoPromocional > 0 &&
                                 produto.produtoPrecoPromocional < produto.produtoPreco
                ? produto.produtoPrecoPromocional
                : produto.produtoPreco;

            pesoTotalProdutoComum += Convert.ToDecimal(Convert.ToDecimal(produto.produtoPeso) * itensPedido.First(x => x.produtoId == produto.produtoId).itemQuantidade);
            precoTotalProdutoComum += (decimal)produtoPreco * itensPedido.First(x => x.produtoId == produto.produtoId).itemQuantidade;
        }

        decimal pesoTotalProdutoProporcional = 0;
        decimal precoTotalProdutoProporcionalCheio = 0;
        foreach (var produto in produtos.Where(x => x.descontoProgressivoFrete.ToLower() == "true"))
        {
            var produtoPreco = produto.produtoPrecoPromocional > 0 &&
                                 produto.produtoPrecoPromocional < produto.produtoPreco
                ? produto.produtoPrecoPromocional
                : produto.produtoPreco;

            pesoTotalProdutoProporcional += Convert.ToDecimal(Convert.ToDecimal(produto.produtoPeso) * itensPedido.First(x => x.produtoId == produto.produtoId).itemQuantidade);
            precoTotalProdutoProporcionalCheio += (decimal)produtoPreco * itensPedido.First(x => x.produtoId == produto.produtoId).itemQuantidade;
        }


        var tipoDeEntregaDc = new dbCommerceDataContext();
        var tiposdeentrega = (from c in tipoDeEntregaDc.tbTipoDeEntregas where c.ativo == true select c).ToList();
        if (produtos.Any(x => x.descontoProgressivoFrete.ToLower() == "true"))
        {
            tiposdeentrega = tiposdeentrega.Where(x => x.freteProporcional == true).ToList();
        }

        var fretes = new List<site_CepFretePorCepPesoTotalResult>();
        foreach (var tipoDeEntrega in tiposdeentrega)
        {
            var tiposDeEntregaGrupo = (from c in tipoDeEntregaDc.tbTipoDeEntregas select c).ToList();
            if (tipoDeEntrega.grupoEntrega > 0)
            {
                tiposDeEntregaGrupo = tiposDeEntregaGrupo.Where(x => x.grupoEntrega == tipoDeEntrega.grupoEntrega).ToList();
                if (produtos.Any(x => x.descontoProgressivoFrete.ToLower() == "true"))
                {
                    tiposDeEntregaGrupo = tiposDeEntregaGrupo.Where(x => x.freteProporcional == true).ToList();
                }
            }
            else
            {
                tiposDeEntregaGrupo = tiposDeEntregaGrupo.Where(x => x.tipoDeEntregaId == tipoDeEntrega.tipoDeEntregaId).ToList();
            }

            var faixasComDesconto = (from c in tipoDeEntregaDc.tbFaixaDeCepDescontos
                                     where
                                         c.valorInicial <= precoTotalProdutoComum && c.valorFinal > precoTotalProdutoComum &&
                                         c.tipoDeEntregaId == tipoDeEntrega.tipoDeEntregaId
                                     select c);

            int porcentagemDeDesconto = 0;
            foreach (var faixaDesconto in faixasComDesconto)
            {
                long faixaInicial = Convert.ToInt64(faixaDesconto.faixaInicial);
                long faixaFinal = Convert.ToInt64(faixaDesconto.faixaFinal);
                if (faixaInicial <= cep && faixaFinal > cep) porcentagemDeDesconto = Convert.ToInt32(faixaDesconto.porcentagemDeDesconto);
            }

            decimal valor = 0;
            int prazo = 0;
            bool valorLocalizado = false;
            bool valorAdicionado = false;

            foreach (var tipo in tiposDeEntregaGrupo)
            {
                bool valorComumLocalizado = false;
                bool valorProgressivoLocalizado = true;
                decimal precoDaEntregaComum = 0;

                if (pesoTotalProdutoComum > 0)
                {
                    var valoresComum = CalculaFrete(tipo.tipoDeEntregaId, Convert.ToInt32(pesoTotalProdutoComum), cepDeEntrega.Replace("-", ""), precoTotalProdutoComum, 0, 0, 0);

                    decimal valorDoDescontoComum = (valoresComum.valor * porcentagemDeDesconto) / 100;
                    precoDaEntregaComum = valoresComum.valor - valorDoDescontoComum;
                    if (valoresComum.prazo > prazo) prazo = valoresComum.prazo;

                    if (valoresComum.valor > 0)
                    {
                        valorComumLocalizado = true;
                    }
                }
                else
                {
                    valorComumLocalizado = true;
                }

                decimal precoEntregaProdutoProporcional = 0;
                int prazoEntregaProdutoProporcional = 0;
                foreach (var produto in produtos.Where(x => x.descontoProgressivoFrete.ToLower() == "true"))
                {
                    decimal produtoPreco = produto.produtoPrecoPromocional > 0 &&
                                           produto.produtoPrecoPromocional < produto.produtoPreco
                        ? Convert.ToDecimal(produto.produtoPrecoPromocional)
                        : produto.produtoPreco;
                    var pesoProdutoProporcional = Convert.ToDecimal(produto.produtoPeso * Convert.ToInt32(itensPedido.First(x => x.produtoId == produto.produtoId).itemQuantidade));
                    var precoProdutoProporcionalCheio = produtoPreco * Convert.ToInt32(itensPedido.First(x => x.produtoId == produto.produtoId).itemQuantidade);

                    var valoresProporcional = CalculaFrete(tipo.tipoDeEntregaId, Convert.ToInt32(pesoProdutoProporcional), cepDeEntrega.Replace("-", ""), precoProdutoProporcionalCheio, 0, 0, 0);
                    if (!(valoresProporcional.valor > 0))
                    {
                        valorProgressivoLocalizado = false;
                    }
                    else
                    {
                        if (valoresProporcional.prazo > prazoEntregaProdutoProporcional) prazoEntregaProdutoProporcional = valoresProporcional.prazo;
                        var precoDaEntregaProporcional = valoresProporcional.valor;

                        if (produto.descontoProgressivoFretePorcentagem > 0)
                        {
                            decimal valorDoDescontoProporcional = (precoProdutoProporcionalCheio / 100) * Convert.ToInt32(produto.descontoProgressivoFretePorcentagem);
                            precoDaEntregaProporcional = valoresProporcional.valor - valorDoDescontoProporcional;
                            if (precoDaEntregaProporcional < 0) precoDaEntregaProporcional = 0;
                        }
                        else if (tipo.descontoFreteProporcional > 0)
                        {
                            decimal valorDoDescontoProporcional = (precoProdutoProporcionalCheio / 100) * Convert.ToInt32(tipoDeEntrega.descontoFreteProporcional);
                            precoDaEntregaProporcional = valoresProporcional.valor - valorDoDescontoProporcional;
                            if (precoDaEntregaProporcional < 0) precoDaEntregaProporcional = 0;
                        }
                        precoEntregaProdutoProporcional += precoDaEntregaProporcional;
                    }
                }

                if (valorComumLocalizado && valorProgressivoLocalizado)
                {
                    valorLocalizado = true;
                    var valorTotalDoFrete = precoEntregaProdutoProporcional + precoDaEntregaComum;
                    if (!valorAdicionado)
                    {
                        valor = valorTotalDoFrete;
                        if (prazoEntregaProdutoProporcional > prazo) prazo = prazoEntregaProdutoProporcional;
                        valorAdicionado = true;
                    }
                    else
                    {
                        if (valor > valorTotalDoFrete)
                        {
                            valor = valorTotalDoFrete;
                            if (prazoEntregaProdutoProporcional > prazo) prazo = prazoEntregaProdutoProporcional;
                        }
                    }
                }
            }
            if (valorLocalizado)
            {
                var frete = new site_CepFretePorCepPesoTotalResult();
                frete.faixaDeCepId = 0;
                frete.faixaDeCepPreco = valor;
                frete.porcentagemDeDesconto = 0;
                frete.porcentagemDoSeguro = 0;
                frete.prazo = prazo;
                frete.tipoDeEntregaNome = tipoDeEntrega.tipoDeEntregaNome;
                frete.tipodeEntregaId = tipoDeEntrega.tipoDeEntregaId;
                fretes.Add(frete);
            }
        }

        return fretes;
    }

    public static List<site_CepFretePorCepPesoTotalResult> montaFrete(string cepDeEntrega, int pedidoId)
    {
        var fretes = new List<site_CepFretePorCepPesoTotalResult>();


        var pedidoDc = new dbCommerceDataContext();
        long cep = Convert.ToInt64(cepDeEntrega.Replace("-", ""));

        var itensPedidoDc = new dbCommerceDataContext();
        var itensPedido = (from c in itensPedidoDc.tbItensPedidos
                           where c.pedidoId == pedidoId
                           select new { c.produtoId, c.itemQuantidade, cancelado = c.cancelado ?? false, brinde = c.brinde ?? false }).Where(x => x.cancelado == false && x.brinde == false);

        var listaProdutos = itensPedido.Select(x => x.produtoId).ToList();
        var produtosDc = new dbCommerceDataContext();

        var produtos = (from c in produtosDc.tbProdutos
                        where listaProdutos.Contains(c.produtoId)
                        select new { c.produtoId, c.produtoPeso, c.produtoPrecoPromocional, c.produtoPreco, c.produtoPrecoDeCusto, descontoProgressivoFrete = c.descontoProgressivoFrete ?? "False", c.descontoProgressivoFretePorcentagem });


        decimal pesoTotalProdutoComum = 0;
        decimal precoTotalProdutoComum = 0;
        foreach (var produto in produtos.Where(x => x.descontoProgressivoFrete.ToLower() != "true"))
        {
            var produtoPreco = produto.produtoPrecoPromocional > 0 &&
                                 produto.produtoPrecoPromocional < produto.produtoPreco
                ? produto.produtoPrecoPromocional
                : produto.produtoPreco;

            pesoTotalProdutoComum += Convert.ToDecimal(Convert.ToDecimal(produto.produtoPeso) * itensPedido.First(x => x.produtoId == produto.produtoId).itemQuantidade);
            precoTotalProdutoComum += (decimal)produtoPreco * itensPedido.First(x => x.produtoId == produto.produtoId).itemQuantidade;
        }

        var tiposdeentrega = (from c in pedidoDc.tbTipoDeEntregas where c.ativo == true select c).ToList();
        if (produtos.Any(x => x.descontoProgressivoFrete.ToLower() == "true"))
        {
            tiposdeentrega = tiposdeentrega.Where(x => x.freteProporcional == true).ToList();
        }

        foreach (var tipoDeEntrega in tiposdeentrega)
        {
            var tiposDeEntregaGrupo = (from c in pedidoDc.tbTipoDeEntregas select c).ToList();
            if (tipoDeEntrega.grupoEntrega > 0)
            {
                tiposDeEntregaGrupo = tiposDeEntregaGrupo.Where(x => x.grupoEntrega == tipoDeEntrega.grupoEntrega).ToList();
                if (produtos.Any(x => x.descontoProgressivoFrete.ToLower() == "true"))
                {
                    tiposDeEntregaGrupo = tiposDeEntregaGrupo.Where(x => x.freteProporcional == true).ToList();
                }
            }
            else
            {
                tiposDeEntregaGrupo = tiposDeEntregaGrupo.Where(x => x.tipoDeEntregaId == tipoDeEntrega.tipoDeEntregaId).ToList();
            }

            var faixasComDesconto = (from c in pedidoDc.tbFaixaDeCepDescontos
                                     where
                                         c.valorInicial <= precoTotalProdutoComum && c.valorFinal > precoTotalProdutoComum &&
                                         c.tipoDeEntregaId == tipoDeEntrega.tipoDeEntregaId
                                     select c);

            int porcentagemDeDesconto = 0;
            foreach (var faixaDesconto in faixasComDesconto)
            {
                long faixaInicial = Convert.ToInt64(faixaDesconto.faixaInicial);
                long faixaFinal = Convert.ToInt64(faixaDesconto.faixaFinal);
                if (faixaInicial <= cep && faixaFinal > cep) porcentagemDeDesconto = Convert.ToInt32(faixaDesconto.porcentagemDeDesconto);
            }

            decimal valor = 0;
            int prazo = 0;
            bool valorLocalizado = false;
            bool valorAdicionado = false;

            foreach (var tipo in tiposDeEntregaGrupo)
            {
                bool valorComumLocalizado = false;
                bool valorProgressivoLocalizado = true;
                decimal precoDaEntregaComum = 0;

                if (pesoTotalProdutoComum > 0)
                {
                    var valoresComum = CalculaFrete(tipo.tipoDeEntregaId, Convert.ToInt32(pesoTotalProdutoComum), cepDeEntrega.Replace("-", ""), precoTotalProdutoComum, 0, 0, 0);

                    decimal valorDoDescontoComum = (valoresComum.valor * porcentagemDeDesconto) / 100;
                    precoDaEntregaComum = valoresComum.valor - valorDoDescontoComum;
                    if (valoresComum.prazo > prazo) prazo = valoresComum.prazo;

                    if (valoresComum.valor > 0)
                    {
                        valorComumLocalizado = true;
                    }
                }
                else
                {
                    valorComumLocalizado = true;
                }

                decimal precoEntregaProdutoProporcional = 0;
                int prazoEntregaProdutoProporcional = 0;
                foreach (var produto in produtos.Where(x => x.descontoProgressivoFrete.ToLower() == "true"))
                {
                    decimal produtoPreco = produto.produtoPrecoPromocional > 0 &&
                                           produto.produtoPrecoPromocional < produto.produtoPreco
                        ? Convert.ToDecimal(produto.produtoPrecoPromocional)
                        : produto.produtoPreco;
                    var pesoProdutoProporcional = Convert.ToDecimal(produto.produtoPeso * Convert.ToInt32(itensPedido.First(x => x.produtoId == produto.produtoId).itemQuantidade));
                    var precoProdutoProporcionalCheio = produtoPreco * Convert.ToInt32(itensPedido.First(x => x.produtoId == produto.produtoId).itemQuantidade);

                    var valoresProporcional = CalculaFrete(tipo.tipoDeEntregaId, Convert.ToInt32(pesoProdutoProporcional), cepDeEntrega.Replace("-", ""), precoProdutoProporcionalCheio, 0, 0, 0);
                    if (!(valoresProporcional.valor > 0))
                    {
                        valorProgressivoLocalizado = false;
                    }
                    else
                    {
                        if (valoresProporcional.prazo > prazoEntregaProdutoProporcional) prazoEntregaProdutoProporcional = valoresProporcional.prazo;
                        var precoDaEntregaProporcional = valoresProporcional.valor;

                        if (produto.descontoProgressivoFretePorcentagem > 0)
                        {
                            decimal valorDoDescontoProporcional = (precoProdutoProporcionalCheio / 100) * Convert.ToInt32(produto.descontoProgressivoFretePorcentagem);
                            precoDaEntregaProporcional = valoresProporcional.valor - valorDoDescontoProporcional;
                            if (precoDaEntregaProporcional < 0) precoDaEntregaProporcional = 0;
                        }
                        else if (tipo.descontoFreteProporcional > 0)
                        {
                            decimal valorDoDescontoProporcional = (precoProdutoProporcionalCheio / 100) * Convert.ToInt32(tipoDeEntrega.descontoFreteProporcional);
                            precoDaEntregaProporcional = valoresProporcional.valor - valorDoDescontoProporcional;
                            if (precoDaEntregaProporcional < 0) precoDaEntregaProporcional = 0;
                        }
                        precoEntregaProdutoProporcional += precoDaEntregaProporcional;
                    }
                }

                if (valorComumLocalizado && valorProgressivoLocalizado)
                {
                    valorLocalizado = true;
                    var valorTotalDoFrete = precoEntregaProdutoProporcional + precoDaEntregaComum;
                    if (!valorAdicionado)
                    {
                        valor = valorTotalDoFrete;
                        if (prazoEntregaProdutoProporcional > prazo) prazo = prazoEntregaProdutoProporcional;
                        valorAdicionado = true;
                    }
                    else
                    {
                        if (valor > valorTotalDoFrete)
                        {
                            valor = valorTotalDoFrete;
                            if (prazoEntregaProdutoProporcional > prazo) prazo = prazoEntregaProdutoProporcional;
                        }
                    }
                }
            }
            if (valorLocalizado)
            {
                var frete = new site_CepFretePorCepPesoTotalResult();
                frete.faixaDeCepId = 0;
                frete.faixaDeCepPreco = valor;
                frete.porcentagemDeDesconto = 0;
                frete.porcentagemDoSeguro = 0;
                frete.prazo = prazo;
                frete.tipoDeEntregaNome = tipoDeEntrega.tipoDeEntregaNome;
                frete.tipodeEntregaId = tipoDeEntrega.tipoDeEntregaId;
                fretes.Add(frete);
            }
        }

        return fretes;
    }

    public static decimal calculaFreteJadlogPorPeso(int peso, long cep)
    {
        var faixasDc = new dbCommerceDataContext();
        var faixasDePreco = faixasDc.site_CepFretePorCepTipoDeEntrega(cep, 9).ToList().OrderByDescending(x => x.faixaDeCepPesoFinal);
        decimal faixaMaxima = 0;
        if (faixasDePreco.FirstOrDefault() != null) faixaMaxima = faixasDePreco.FirstOrDefault().faixaDeCepPesoFinal;
        bool possuiCadastro = true;
        int pesoTotalProdutoComum = peso;


        if (faixaMaxima > 0)
        {
            decimal pesoCalculadoComum = 0;
            decimal precoDaEntregaComum = 0;


            while (pesoCalculadoComum < pesoTotalProdutoComum)
            {
                decimal pesoACalcular = pesoTotalProdutoComum - pesoCalculadoComum;
                //Response.Write("Peso a calcular: " + pesoACalcular + "<br>");
                if (pesoACalcular > faixaMaxima)
                {
                    pesoCalculadoComum = pesoCalculadoComum + faixaMaxima;
                    var pesodafaixa = (from c in faixasDePreco
                                       where
                                           c.faixaDeCepPesoInicial < faixaMaxima &&
                                           c.faixaDeCepPesoFinal >= faixaMaxima
                                       select c).First().faixaDeCepPreco;
                    precoDaEntregaComum = precoDaEntregaComum + pesodafaixa;
                }
                else
                {
                    pesoCalculadoComum = pesoCalculadoComum + pesoACalcular;
                    var pesodafaixaRetorno = (from c in faixasDePreco
                                              where
                                                  c.faixaDeCepPesoInicial <= pesoACalcular &&
                                                  c.faixaDeCepPesoFinal >= pesoACalcular
                                              select c).FirstOrDefault();
                    if (pesodafaixaRetorno != null)
                    {
                        var pesodafaixa = pesodafaixaRetorno.faixaDeCepPreco;
                        precoDaEntregaComum = precoDaEntregaComum + pesodafaixa;
                    }
                    else
                    {
                        possuiCadastro = false;
                    }
                }
            }
            return precoDaEntregaComum;
        }

        return 0;
    }
    public static RetornoCalculoFrete CalculaFreteWebserviceJadlog(int peso, long cep, string modalidade, decimal valorPedido)
    {
        string valorNota = Convert.ToDecimal(valorPedido).ToString("0.00");
        var retornoCalculo = new RetornoCalculoFrete();
        bool erroServiceJad = false;
        decimal valorJad = 0;
        int prazoJad = 0;
        decimal pesoEmKg = Convert.ToDecimal(peso) / 1000;

        try
        {
            int modalidadeJad = 3;
            if (modalidade == "jadlogexpressa") modalidadeJad = 0;
            if (modalidade == "jadlogcom") modalidadeJad = 9;
            if (modalidade == "jadlogrodo") modalidadeJad = 4;
            var serviceJad = new serviceJadFrete.ValorFreteBeanService();
            serviceJad.Timeout = 900;
            var retornoServiceJad = serviceJad.valorar(modalidadeJad, "L2f0M1e6", "N", valorNota, "0,00", "14910000", cep.ToString().Replace("-", "").Replace(" ", ""), pesoEmKg.ToString("0.00"), "N", "D", "10924051000163");
            //var retornoServiceJad = serviceJad.valorar(modalidadeJad, "M2c0F1m4", "N", valorNota, "0,00", "14910000", cep.ToString().Replace("-", "").Replace(" ", ""), pesoEmKg.ToString("0.00"), "N", "D", "20907518000110");
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(retornoServiceJad);
            XmlNodeList parentNode = xmlDoc.GetElementsByTagName("Jadlog_Valor_Frete");
            foreach (XmlNode childrenNode in parentNode)
            {
                string retorno = childrenNode.ChildNodes[1].InnerText;
                valorJad = Convert.ToDecimal(retorno);
            }
        }
        catch (Exception)
        {
            erroServiceJad = true;
        }

        var faixasDc = new dbCommerceDataContext();
        var faixas = faixasDc.admin_TipoDeEntregaPorFaixaDeCep(cep).Where(x => x.tipodeEntregaId == 9).FirstOrDefault();
        if (faixas != null)
        {
            prazoJad = faixas.prazo + 1;
        }

        retornoCalculo.valor = valorJad;
        retornoCalculo.prazo = prazoJad;
        if (erroServiceJad)
        {
            retornoCalculo.valor = 0;
            retornoCalculo.prazo = 0;
        }
        return retornoCalculo;
    }

    public static bool GravaPedidoWebserviceJadlog(int idPedidoEnvio)
    {
        var pedidoDc = new dbCommerceDataContext();
        var pedido = (from c in pedidoDc.tbPedidoEnvios where c.idPedidoEnvio == idPedidoEnvio select c.tbPedido).First();
        var pedidoEnvio = (from c in pedidoDc.tbPedidoEnvios where c.idPedidoEnvio == idPedidoEnvio select c).First();

        var clientesDc = new dbCommerceDataContext();
        var cliente = (from c in clientesDc.tbClientes where c.clienteId == pedido.clienteId select c).First();
        var volumes = (from c in pedidoDc.tbPedidoPacotes where c.idPedidoEnvio == idPedidoEnvio && (c.concluido ?? true) == true && c.peso > 0 select c);

        int idPedidoCliente = rnFuncoes.retornaIdCliente(pedido.pedidoId);

        string CodCliente = "40745";
        string Password = "L2F0M0E1";
        string Remetente = "LINDSAY FERRANDO ME";
        string RemetenteCNPJ = "10924051000163";
        string RemetenteIE = "633459940118";
        string RemetenteEndereco = "AV ANA COSTA 416";
        string RemetenteBairro = "GONZAGA";
        string RemetenteCEP = "11060002";
        string RemetenteTelefone = "01135228379";

        //string CodCliente = "50002";
        //string Password = "M2c0F1m4";
        //string Remetente = "MAYARA CAROLINA FERRO ME";
        //string RemetenteCNPJ = "20907518000110";
        //string RemetenteIE = "674017393110";
        //string RemetenteEndereco = "RUA RODRIGUES ALVES 27";
        //string RemetenteBairro = "CENTRO";
        //string RemetenteCEP = "14910000";
        //string RemetenteTelefone = "01135228379";

        string Destino = "";
        string Destinatario = "";
        string DestinatarioCNPJ = "";
        string DestinatarioIE = "";
        string DestinatarioEndereco = "";
        string DestinatarioBairro = "";
        string DestinatarioCEP = "";
        string DestinatarioTelefone = "";


        if (!string.IsNullOrEmpty(pedido.endNomeDoDestinatario))
        {
            string enderecoCompleto = pedido.endRua + " - " + pedido.endNumero + " - " + pedido.endComplemento;
            Destino = rnFuncoes.removeCaracteresEspeciaisDeixaEspaco(rnFuncoes.limpaStringDeixaEspaco(pedido.endCidade)).ToUpper();
            Destinatario = idPedidoCliente + " " + rnFuncoes.removeCaracteresEspeciaisDeixaEspaco(rnFuncoes.limpaStringDeixaEspaco(pedido.endNomeDoDestinatario)).ToUpper();
            DestinatarioCNPJ = cliente.clienteCPFCNPJ.Replace(".", "").Replace("-", "").Replace("/", "");
            DestinatarioEndereco = rnFuncoes.removeCaracteresEspeciaisDeixaEspaco(rnFuncoes.limpaStringDeixaEspaco(enderecoCompleto)).ToUpper();
            DestinatarioBairro = rnFuncoes.removeCaracteresEspeciaisDeixaEspaco(rnFuncoes.limpaStringDeixaEspaco(pedido.endBairro)).ToUpper();
            DestinatarioCEP = pedido.endCep.Replace("-", "").Replace(" ", "");
            DestinatarioTelefone = cliente.clienteFoneResidencial.Replace(" ", "").Replace("-", "");
        }
        else
        {
            string enderecoCompleto = cliente.clienteRua + " - " + cliente.clienteNumero + " - " + cliente.clienteComplemento;
            Destino = rnFuncoes.removeCaracteresEspeciaisDeixaEspaco(rnFuncoes.limpaStringDeixaEspaco(cliente.clienteCidade)).ToUpper();
            Destinatario = idPedidoCliente + " " + rnFuncoes.removeCaracteresEspeciaisDeixaEspaco(rnFuncoes.limpaStringDeixaEspaco(cliente.clienteNome)).ToUpper();
            DestinatarioCNPJ = cliente.clienteCPFCNPJ.Replace(".", "").Replace("-", "").Replace("/", "");
            DestinatarioEndereco = rnFuncoes.removeCaracteresEspeciaisDeixaEspaco(rnFuncoes.limpaStringDeixaEspaco(enderecoCompleto)).ToUpper();
            DestinatarioBairro = rnFuncoes.removeCaracteresEspeciaisDeixaEspaco(rnFuncoes.limpaStringDeixaEspaco(cliente.clienteBairro));
            DestinatarioCEP = cliente.clienteCep.Replace("-", "").Replace(" ", "");
            DestinatarioTelefone = cliente.clienteFoneResidencial.Replace(" ", "").Replace("-", "");
        }

        //Destinatario = "TESTE DE PEDIDO - NÃO EMITIR";

        
        string ColetaResponsavel = "";
        string Volumes = volumes.Count().ToString();

        if (volumes.Count() > 0)
        {
            var pesoTotal = (Convert.ToDecimal(volumes.Count() == 0 ? 1000 : volumes.Sum(x => x.peso)) / 1000);
            string PesoReal = pesoTotal.ToString("0.00");
            string Especie = "CONFECCOES";
            string Conteudo = "CONFECCOES";
            string Nr_Pedido = rnFuncoes.retornaIdCliente(pedido.pedidoId).ToString();
            string Nr_NF = "";
            string Danfe = "";
            string Serie_Nf = "";
            string ValorDeclarado = "";
            if (pedidoEnvio.nfeNumero > 0)
            {
                Nr_NF = pedidoEnvio.nfeNumero.ToString();
                Danfe = pedidoEnvio.nfeKey;
                Serie_Nf = "01";
                ValorDeclarado = Convert.ToDecimal(pedidoEnvio.nfeValor).ToString("0.00");
            }
            else
            {
                Nr_NF = idPedidoCliente.ToString() + idPedidoEnvio.ToString();
                ValorDeclarado = "100,00";
            }
            string Observacoes = "";
            string Modalidade = "3";
            string wCentroCusto = "";
            string wContaCorrente = "0034802";
            string wTipo = "K";
            string CodUnidade = "795";

            bool erroService = false;

            if (pedidoEnvio.formaDeEnvio.ToLower() == "jadlogexpressa")
            {
                Modalidade = "0";
            }

            string codigo = "";
            /*try
            {
                var serviceJad = new serviceJadEnvio.NotfisBeanService();
                var retornoServiceJad = serviceJad.inserir(CodCliente, Password, Remetente, RemetenteCNPJ, RemetenteIE, RemetenteEndereco, RemetenteBairro, RemetenteCEP,
                    RemetenteTelefone, Destino, Destinatario, DestinatarioCNPJ, DestinatarioIE, DestinatarioEndereco, DestinatarioBairro, DestinatarioCEP, DestinatarioTelefone,
                    ColetaResponsavel, Volumes, PesoReal, Especie, Conteudo, Nr_Pedido, Nr_NF, Danfe, Serie_Nf, ValorDeclarado, Observacoes, Modalidade, wCentroCusto, wContaCorrente,
                    wTipo, CodUnidade);

                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(retornoServiceJad);
                XmlNodeList parentNode = xmlDoc.GetElementsByTagName("Jadlog_Pedido_eletronico_Inserir");
                foreach (XmlNode childrenNode in parentNode)
                {
                    string retorno = childrenNode.ChildNodes[1].InnerText;
                    if (!String.IsNullOrEmpty(retorno))
                    {
                        codigo = retorno;
                    }
                
                }
            }
            catch (Exception)
            {
                //return false;
            }*/

            foreach (var pacote in volumes)
            {
                var pacoteAlterarDc = new dbCommerceDataContext();
                var detalhePacote = (from c in pacoteAlterarDc.tbPedidoPacotes where c.idPedidoPacote == pacote.idPedidoPacote select c).First();
                detalhePacote.codJadlog = codigo;
                pacoteAlterarDc.SubmitChanges();
            }
        }

        return true;

    }

    public static RetornoCalculoFrete CalculaFreteWebserviceTnt(int pedidoId)
    {
        var pedidoDc = new dbCommerceDataContext();
        var clienteDc = new dbCommerceDataContext();
        var pedido = (from c in pedidoDc.tbPedidos where c.pedidoId == pedidoId select c).First();
        var cliente = (from c in clienteDc.tbClientes where c.clienteId == pedido.clienteId select c).First();
        var volumes = (from c in pedidoDc.tbPedidoPacotes where c.idPedido == pedidoId select c);

        int pesoTotal = volumes.Sum(x => x.peso);
        long cep = Convert.ToInt64(pedido.endCep.Replace("-", ""));
        var valoresTnt = CalculaFreteWebserviceTnt(pesoTotal, pedido.endCep, Convert.ToDecimal(pedido.valorCobrado), cliente.clienteCPFCNPJ);
        return valoresTnt;
    }

    public static RetornoCalculoFrete CalculaFreteWebserviceTnt(int peso, string cep, decimal valorPedido, string cpfDestinatario)
    {
        var serviceTnt = new serviceTntCalculoFrete.CalculoFrete();
        var parametros = new serviceTntCalculoFrete.CotacaoWebService();
        parametros.login = "atendimento@bark.com.br";
        parametros.senha = "";
        parametros.nrIdentifClienteRem = "10924051000163";
        parametros.nrInscricaoEstadualRemetente = "633459940118";
        parametros.nrIdentifClienteDest = cpfDestinatario;
        parametros.tpSituacaoTributariaRemetente = "ME";
        parametros.tpPessoaRemetente = "J";
        parametros.nrInscricaoEstadualDestinatario = "";
        parametros.tpPessoaDestinatario = "F";
        parametros.tpSituacaoTributariaDestinatario = "NC";
        parametros.cepOrigem = "14910000";
        parametros.cepDestino = cep.Trim().Replace("-", "").Replace(" ", "");
        parametros.vlMercadoria = valorPedido.ToString("0.00").Replace(",", ".");
        parametros.psReal = (Convert.ToDecimal(peso) / 1000).ToString("0.000").Replace(",", ".");
        parametros.tpServico = "RNC";
        parametros.tpFrete = "C";
        parametros.cdDivisaoCliente = 1;
        parametros.cdDivisaoClienteSpecified = true;
        int prazo = 0;
        decimal valor = 0;
        try
        {
        
            var calculoFrete = serviceTnt.calculaFrete(parametros);
            prazo = Convert.ToInt32(calculoFrete.prazoEntrega);
            valor = Convert.ToDecimal(calculoFrete.vlTotalFrete.Replace(".", ","));
        }
        catch { }
        var retornoCalculo = new RetornoCalculoFrete();
        retornoCalculo.prazo = prazo;
        retornoCalculo.valor = valor;
        return retornoCalculo;
    }

    public static RetornoCalculoFrete CalculaFrete(int idTipoDeEntrega, string cep, decimal valorPedido, List<Pacotes> pacotes)
    {

        List<string> detalhesValor = new List<string>();

        long cepFormatado = 0;
        Int64.TryParse(cep.Replace("-", "").Trim(), out cepFormatado);
        var retorno = new RetornoCalculoFrete() { valor = 0, prazo = 0 };
        var data = new dbCommerceDataContext();
        var tipoDeEntrega = (from c in data.tbTipoDeEntregas where c.tipoDeEntregaId == idTipoDeEntrega select c).FirstOrDefault();
        if (tipoDeEntrega == null)
        {
            return retorno;
        }

        if(tipoDeEntrega.limiteMaiorDimensao > 0)
        {
            if(pacotes.Any(x => x.altura > tipoDeEntrega.limiteMaiorDimensao | x.largura > tipoDeEntrega.limiteMaiorDimensao | x.comprimento > tipoDeEntrega.limiteMaiorDimensao))
            {
                return retorno;
            }
        }

        if (tipoDeEntrega.limiteSomaDimensoes > 0)
        {
            if (pacotes.Any(x => (x.altura + x.largura + x.comprimento) > tipoDeEntrega.limiteSomaDimensoes))
            {
                return retorno;
            }
        }

        int pesoTotal = pacotes.Sum(x => x.peso);


        if (tipoDeEntrega.idTipoEntregaTipoTabela == 1)
        {
            var faixasDeCep = data.site_CepFretePorCepTipoDeEntrega(cepFormatado, idTipoDeEntrega).ToList();
            var faixa = faixasDeCep.FirstOrDefault(x => pesoTotal > x.faixaDeCepPesoInicial && pesoTotal <= x.faixaDeCepPesoFinal);
            if (faixa != null)
            {
                decimal seguro = 0;
                if ((faixa.porcentagemDoSeguro ?? 0) > 0)
                {
                    seguro = (valorPedido / 100) * Convert.ToDecimal(faixa.porcentagemDoSeguro);
                }

                retorno.valor = faixa.faixaDeCepPreco + seguro;
                retorno.prazo = faixa.prazo;
                retorno.pesoAferido = pesoTotal;
            }
        }
        else if (tipoDeEntrega.idTipoEntregaTipoTabela == 2)
        {
            var faixa = data.site_CepFaixaPorTipoDeEntrega(cepFormatado, idTipoDeEntrega).FirstOrDefault();
            if (faixa == null)
            {
                var faixasTotal1 = (from c in data.tbFaixaDeCeps
                                    where c.tipodeEntregaId == tipoDeEntrega.tipoDeEntregaId && (c.ativo ?? true) == true
                                    select new
                                    {
                                        faixaInicial = c.faixaInicial,
                                        faixaFinal = c.faixaFinal
                                    }).ToList();


                var faixasTotalInicial = faixasTotal1.Select(x => Convert.ToInt64(x.faixaInicial)).ToList();
                var faixasTotalFinal = faixasTotal1.Select(x => Convert.ToInt64(x.faixaFinal)).ToList();
                var proximaFaixa = (from c in faixasTotalInicial where c > cepFormatado orderby c select c).FirstOrDefault();
                var faixaAnterior = (from c in faixasTotalFinal where c < cepFormatado orderby c descending select c).FirstOrDefault();

                if (proximaFaixa > 0 && faixaAnterior > 0)
                {
                    var compararProximaFaixa = proximaFaixa - cepFormatado;
                    var compararFaixaAnterior = cepFormatado - faixaAnterior;
                    if (tipoDeEntrega.cepAproximado == true)
                    {                        
                        if (compararProximaFaixa < compararFaixaAnterior)
                            faixa = data.site_CepFaixaPorTipoDeEntrega(proximaFaixa, idTipoDeEntrega).FirstOrDefault();
                        else
                            faixa = data.site_CepFaixaPorTipoDeEntrega(faixaAnterior, idTipoDeEntrega).FirstOrDefault();

                        try
                        {
                            // resolveu com próxima faixa, mas o cep pesquisado não existe
                            //rnEmails.EnviaEmail("atendimento@graodegente.com.br", "renato@bark.com.br", "andre@bark.com.br,danny@bark.com.br", "", "", "O cep: " + cepFormatado + "</br>" + "Não foi encontrado no banco de dados, favor incluí-lo para melhor precisão nos calculos de prazos e valores de frete.</br>Tipo de entrega Id: " + idTipoDeEntrega, "CEP não encontrado no banco de dados");
                        }
                        catch (Exception) { }
                    }
                }
                else
                {
                    try
                    {
                        //rnEmails.EnviaEmail("atendimento@graodegente.com.br", "renato@bark.com.br", "andre@bark.com.br,danny@bark.com.br", "", "", "O cep: " + cepFormatado + "</br>" + "Não foi encontrado no banco de dados, favor incluí-lo para melhor precisão nos calculos de prazos e valores de frete.</br>Tipo de entrega Id: " + idTipoDeEntrega, "CEP não encontrado no banco de dados");
                    }
                    catch (Exception) { }

                    //if (proximaFaixa != null) faixa = data.site_CepFaixaPorTipoDeEntrega(proximaFaixa, idTipoDeEntrega).FirstOrDefault();
                }
            }
            if (faixa != null)
            {
                if (faixa.idTipoDeEntregaTabelaPreco != null)
                {
                    decimal valorCalculado = 0;
                    var tabela = (from c in data.tbTipoDeEntregaTabelaPrecos
                                  where c.idTipoDeEntregaTabelaPreco == faixa.idTipoDeEntregaTabelaPreco
                                  select c).FirstOrDefault();
                    if (tabela != null)
                    {
                        int pesoFinal = pesoTotal;
                        if (tipoDeEntrega.cubagem)
                        {
                            int fatorCubagem = tipoDeEntrega.divisaoCubagem;
                            if(tabela.fatorCubagem > 0)
                            {
                                fatorCubagem = (int)tabela.fatorCubagem;
                            }

                            decimal pesoTotalCubagem = 0;
                            foreach (var volume in pacotes)
                            {
                                var cubagem = (Convert.ToDecimal(volume.altura * volume.largura * volume.comprimento) / fatorCubagem);
                                if (cubagem > 10 && cubagem > (Convert.ToDecimal(volume.peso) / 1000))
                                {
                                    pesoTotalCubagem += cubagem;
                                }
                                else
                                {
                                    pesoTotalCubagem += (Convert.ToDecimal(volume.peso) / 1000);
                                }
                            }
                            pesoFinal = Convert.ToInt32(pesoTotalCubagem * 1000);
                        }

                        retorno.pesoAferido = pesoFinal;
                        var tabelaPrecos = (from c in data.tbTipoDeEntregaTabelaPrecoValors
                                            where c.idTipoDeEntregaTabelaPreco == faixa.idTipoDeEntregaTabelaPreco
                                            select c).ToList();
                        var valorFaixa = (from c in tabelaPrecos where pesoFinal > c.pesoInicial && pesoFinal <= c.pesoFinal select c).FirstOrDefault();
                        if (valorFaixa != null)
                        {
                            valorCalculado = valorFaixa.valor;
                            detalhesValor.Add("Valor da faixa: " + valorCalculado.ToString("C"));
                        }
                        else
                        {
                            if (tipoDeEntrega.pesoAdicionalSomaPesoMaximo)
                            {
                                var pesoMaximo = tabelaPrecos.OrderByDescending(x => x.pesoFinal).FirstOrDefault();
                                if (pesoMaximo != null)
                                {
                                    valorCalculado = pesoMaximo.valor;
                                    var diferencaPeso = Convert.ToInt32(Convert.ToDecimal(pesoFinal - pesoMaximo.pesoFinal) / 1000);
                                    valorCalculado += diferencaPeso * tabela.valorKgAdicional;
                                }
                                else
                                {
                                    valorCalculado = Convert.ToInt32(Convert.ToDecimal(pesoFinal) / 1000) * tabela.valorKgAdicional;
                                }
                            }
                            else
                            {
                                valorCalculado = Convert.ToInt32(Convert.ToDecimal(pesoFinal) / 1000) * tabela.valorKgAdicional;
                                detalhesValor.Add("Valor da faixa com peso adicional: " + valorCalculado.ToString("C"));
                            }
                        }

                        if (tabela.valorSeguro > 0 && valorPedido > 0)
                        {
                            valorCalculado += (valorPedido / 100) * tabela.valorSeguro;
                            detalhesValor.Add("Valor com seguro: " + valorCalculado.ToString("C"));
                            detalhesValor.Add("Valor do seguro: " + ((valorPedido / 100) * tabela.valorSeguro).ToString("C"));
                        }


                        var tarifasObrigatorias = (from c in data.tbTipoDeEntregaTarifas where c.idTipoDeEntrega == idTipoDeEntrega && c.taxaObrigatoria == true && c.valorSobreFrete == false select c);
                        foreach (var tarifa in tarifasObrigatorias)
                        {
                            var valorTarifa = (valorPedido / 100) * tarifa.percentual;
                            if (valorTarifa < tarifa.valor) valorTarifa = tarifa.valor;
                            valorCalculado += valorTarifa;
                            detalhesValor.Add("Valor com tarifa " + tarifa.nome + ": " + valorCalculado.ToString("C"));
                            detalhesValor.Add("Valor da tarifa " + tarifa.nome + ": " + valorTarifa.ToString("C"));
                        }


                        var tarifasFaixaNota = (from c in data.tbFaixaDeCepTarifas where c.idFaixaDeCep == faixa.faixaDeCepId && c.tbTipoDeEntregaTarifa.valorSobreFrete == false select c);
                        foreach (var tarifa in tarifasFaixaNota)
                        {
                            var valorTarifa = (valorPedido / 100) * tarifa.tbTipoDeEntregaTarifa.percentual;
                            if (valorTarifa < tarifa.tbTipoDeEntregaTarifa.valor) valorTarifa = tarifa.tbTipoDeEntregaTarifa.valor;
                            valorCalculado += valorTarifa;
                            detalhesValor.Add("Valor com tarifa " + tarifa.tbTipoDeEntregaTarifa.nome + ": " + valorCalculado.ToString("C"));
                            detalhesValor.Add("Valor da tarifa " + tarifa.tbTipoDeEntregaTarifa.nome + ": " + valorTarifa.ToString("C"));
                        }


                        var tarifasSobreFrete = (from c in data.tbFaixaDeCepTarifas where c.idFaixaDeCep == faixa.faixaDeCepId && c.tbTipoDeEntregaTarifa.valorSobreFrete == true select c);
                        foreach (var tarifa in tarifasSobreFrete)
                        {
                            if (tarifa.tbTipoDeEntregaTarifa.percentual > 0)
                            {
                                if (tarifa.tbTipoDeEntregaTarifa.valorSobreFreteDivisao)
                                {
                                    var valorTarifa = valorCalculado / tarifa.tbTipoDeEntregaTarifa.percentual;
                                    if (valorTarifa > valorCalculado) valorCalculado = valorTarifa;
                                    detalhesValor.Add("Valor com tarifa " + tarifa.tbTipoDeEntregaTarifa.nome + ": " + valorCalculado.ToString("C"));
                                    detalhesValor.Add("Valor da tarifa " + tarifa.tbTipoDeEntregaTarifa.nome + ": " + valorTarifa.ToString("C"));
                                }
                                else
                                {
                                    var valorTarifa = (valorCalculado / 100) * tarifa.tbTipoDeEntregaTarifa.percentual;
                                    if (valorTarifa < tarifa.tbTipoDeEntregaTarifa.valor) valorTarifa = tarifa.tbTipoDeEntregaTarifa.valor;
                                    valorCalculado += valorTarifa;
                                    detalhesValor.Add("Valor com tarifa " + tarifa.tbTipoDeEntregaTarifa.nome + ": " + valorCalculado.ToString("C"));
                                    detalhesValor.Add("Valor da tarifa " + tarifa.tbTipoDeEntregaTarifa.nome + ": " + valorTarifa.ToString("C"));
                                }
                            }
                        }


                        var tarifasObrigatoriasSobreFrete = (from c in data.tbTipoDeEntregaTarifas where c.idTipoDeEntrega == idTipoDeEntrega && c.taxaObrigatoria == true && c.valorSobreFrete == true select c);
                        foreach (var tarifa in tarifasObrigatoriasSobreFrete)
                        {
                            if (tarifa.valorSobreFreteDivisao)
                            {
                                var valorTarifa = valorCalculado / tarifa.percentual;
                                if (valorTarifa > valorCalculado) valorCalculado = valorTarifa;
                                detalhesValor.Add("Valor com tarifa " + tarifa.nome + ": " + valorCalculado.ToString("C"));
                                detalhesValor.Add("Valor da tarifa " + tarifa.nome + ": " + valorTarifa.ToString("C"));
                            }
                            else
                            {
                                var valorTarifa = (valorCalculado / 100) * tarifa.percentual;
                                if (valorTarifa < tarifa.valor) valorTarifa = tarifa.valor;
                                valorCalculado += valorTarifa;
                                detalhesValor.Add("Valor com tarifa " + tarifa.nome + ": " + valorCalculado.ToString("C"));
                                detalhesValor.Add("Valor da tarifa " + tarifa.nome + ": " + valorTarifa.ToString("C"));
                            }
                        }

                        retorno.valor = valorCalculado;
                        retorno.prazo = faixa.prazo;
                        retorno.detalhamento = detalhesValor;
                    }
                }
            }
        }
        return retorno;
    }

    public static RetornoCalculoFrete CalculaFrete(int idTipoDeEntrega, int peso, string cep, decimal valorPedido, decimal largura, decimal altura, decimal profundidade)
    {
        var pacotes = new List<Pacotes>();
        var pacote = new Pacotes() {
            altura = altura,
            comprimento = profundidade,
            largura = largura,
            peso = peso
        };
        pacotes.Add(pacote);

        return CalculaFrete(idTipoDeEntrega, cep, valorPedido, pacotes);

    }




}