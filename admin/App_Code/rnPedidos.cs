﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml;
using System.Xml.Linq;
using DevExpress.XtraRichEdit.API.Word;
using HtmlAgilityPack;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Web.Configuration;
using System.Collections;
using rakutenPedidos;

/// <summary>
/// Summary description for rnPedidos
/// </summary>
public class rnPedidos
{
    public static Page page()
    {
        return (Page)HttpContext.Current.Handler;
    }

    public static DataSet pedidoSelecionaUltimoId()
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("pedidoSelecionaUltimoId");

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet pedidoSeleciona_PorPedidoId(int pedidoId)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("pedidoSeleciona_PorPedidoId");

        db.AddInParameter(dbCommand, "pedidoId", DbType.Int32, pedidoId);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet itensPedidoContaESomaItens_PorItemPedidoId(int pedidoId)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("itensPedidoContaESomaItens_PorItemPedidoId");

        db.AddInParameter(dbCommand, "pedidoId", DbType.Int32, pedidoId);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet pedidoSeleciona_PorTipoDeEntregaId(int tipoDeEntregaId)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("pedidoSeleciona_PorTipoDeEntregaId");

        db.AddInParameter(dbCommand, "tipoDeEntregaId", DbType.Int32, tipoDeEntregaId);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet pedidoSelecionaAdmin()
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("pedidoSelecionaAdmin");

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }
    public static DataSet pedidoSelecionaDuplicados()
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("pedidoSelecionaDuplicados");

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }
    public static DataSet pedidoSelecionaPrazoDeFabricacaoAdmin()
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("pedidoSelecionaPrazoDeFabricacaoAdmin");

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }
    public static DataSet pedidoSelecionaEmbaladoAdmin()
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("pedidoSelecionaEmbaladoAdmin");

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet pedidoSelecionaProdutosFabricacaoAdmin()
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("pedidoSelecionaProdutosFabricacaoAdmin");

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet itensPedidoSeleciona_PorItemPedidoId(int itemPedidoId)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("itensPedidoSeleciona_PorItemPedidoId");

        db.AddInParameter(dbCommand, "itemPedidoId", DbType.Int32, itemPedidoId);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet itensPedidoConta(int pedidoId)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("itensPedidoConta");

        db.AddInParameter(dbCommand, "pedidoId", DbType.Int32, pedidoId);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet itensPedidoSeleciona_PorPedidoId()
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("itensPedidoSeleciona_PorPedidoId");

        db.AddInParameter(dbCommand, "pedidoId", DbType.Int32, page().Request.Cookies["cliente"]["pedidoId"].ToString());

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet itensPedidoSelecionaAdmin_PorPedidoId(int pedidoId)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("itensPedidoSeleciona_PorPedidoId");

        db.AddInParameter(dbCommand, "pedidoId", DbType.Int32, pedidoId);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }
    public static DataSet itensPedidoSelecionaDesagrupadosAdmin_PorPedidoId(int pedidoId)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("itensPedidoSelecionaDesagrupados_PorPedidoId");

        db.AddInParameter(dbCommand, "pedidoId", DbType.Int32, pedidoId);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet itensPedidoSelecionaComboAdmin_PorPedidoId(int pedidoId)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("itensPedidoSelecionaCombo_PorPedidoId");

        db.AddInParameter(dbCommand, "pedidoId", DbType.Int32, pedidoId);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet pedidoSeleciona_PorClienteId(int clienteId)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("pedidoSeleciona_PorClienteId");

        db.AddInParameter(dbCommand, "clienteId", DbType.Int32, clienteId);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet pedidoSeleciona_PorPedidoKey(string pedidoKey)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("pedidoSeleciona_PorPedidoKey");

        db.AddInParameter(dbCommand, "pedidoKey", DbType.String, pedidoKey);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet pedidoSelecionaAdmin_PorClienteId(int clienteId)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("pedidoSelecionaAdmin_PorClienteId");

        db.AddInParameter(dbCommand, "clienteId", DbType.Int32, clienteId);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet pedidoSelecionaUltimo_PorClienteId(int clienteId)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("pedidoSelecionaUltimo_PorClienteId");

        db.AddInParameter(dbCommand, "clienteId", DbType.Int32, clienteId);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet itensPedidoSeleciona_PorProdutoId(int produtoId)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("itensPedidoSeleciona_PorProdutoId");

        db.AddInParameter(dbCommand, "produtoId", DbType.Int32, produtoId);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet pedidoSelecionaAdmin_PorPedidoId(int pedidoId)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("pedidoSelecionaAdmin_PorPedidoId");

        db.AddInParameter(dbCommand, "pedidoId", DbType.Int32, pedidoId);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet itensPedidoSeleciona_PorPedidoIdEItenPedidoId(int itemPedidoId, int pedidoId)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("itensPedidoSeleciona_PorPedidoIdEItenPedidoId");

        db.AddInParameter(dbCommand, "itemPedidoId", DbType.Int32, itemPedidoId);
        db.AddInParameter(dbCommand, "pedidoId", DbType.Int32, pedidoId);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet calculaCep(int faixaDeCep, decimal pesoTotal)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("calculaCep");

        db.AddInParameter(dbCommand, "faixaDeCep", DbType.Int64, faixaDeCep);
        db.AddInParameter(dbCommand, "pesoTotal", DbType.Decimal, pesoTotal);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet selecionaCepPorCepPesoFaixaDeCepId(int faixaDeCep, decimal pesoTotal, int faixaDeCepId)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("selecionaCepPorCepPesoFaixaDeCepId");

        db.AddInParameter(dbCommand, "faixaDeCep", DbType.Int64, faixaDeCep);
        db.AddInParameter(dbCommand, "pesoTotal", DbType.Decimal, pesoTotal);
        db.AddInParameter(dbCommand, "faixaDeCepId", DbType.Int32, faixaDeCepId);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet faixaDeCepDescontoSelecionaDesconto(int faixaDeCep, double valor, int tipoDeEntregaId)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("faixaDeCepDescontoSelecionaDesconto");

        db.AddInParameter(dbCommand, "faixaDeCep", DbType.Int64, faixaDeCep);
        db.AddInParameter(dbCommand, "valor", DbType.Double, valor);
        db.AddInParameter(dbCommand, "tipoDeEntregaId", DbType.Int32, tipoDeEntregaId);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet calculaCepInternacional(int paisId, decimal pesoTotal)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("calculaCepInternacional");

        db.AddInParameter(dbCommand, "paisId", DbType.Int64, paisId);
        db.AddInParameter(dbCommand, "pesoTotal", DbType.Decimal, pesoTotal);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static bool itemPedidoExclui(int itemPedidoId)
    {
        bool result = false;

        Database db = DatabaseFactory.CreateDatabase();

        DbCommand dbCommand = db.GetStoredProcCommand("itemPedidoExclui");

        db.AddInParameter(dbCommand, "itemPedidoId", DbType.Int32, itemPedidoId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
            DbTransaction transaction = connection.BeginTransaction();

            try
            {
                db.ExecuteNonQuery(dbCommand, transaction);

                transaction.Commit();

                result = true;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);

            }
            connection.Close();

            return result;
        }
    }

    public static bool itensPedidoExclui_PorPedidoId(int pedidoId)
    {
        bool result = false;

        Database db = DatabaseFactory.CreateDatabase();

        DbCommand dbCommand = db.GetStoredProcCommand("itensPedidoExclui_PorPedidoId");

        db.AddInParameter(dbCommand, "pedidoId", DbType.Int32, pedidoId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
            DbTransaction transaction = connection.BeginTransaction();

            try
            {
                db.ExecuteNonQuery(dbCommand, transaction);

                transaction.Commit();

                result = true;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);

            }
            connection.Close();

            return result;
        }
    }

    public static bool pedidoAlteraStatusdataDaConclusao(int statusDoPedido, DateTime dataHoraDoPedido, double valorDaParcela, int pedidoId)
    {
        bool result = false;

        Database db = DatabaseFactory.CreateDatabase();

        DbCommand dbCommand = db.GetStoredProcCommand("pedidoAlteraStatusdataDaConclusao");

        db.AddInParameter(dbCommand, "statusDoPedido", DbType.Int32, statusDoPedido);
        db.AddInParameter(dbCommand, "dataHoraDoPedido", DbType.DateTime, dataHoraDoPedido);
        db.AddInParameter(dbCommand, "valorDaParcela", DbType.Double, valorDaParcela);
        db.AddInParameter(dbCommand, "pedidoId", DbType.Int32, pedidoId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
            DbTransaction transaction = connection.BeginTransaction();

            try
            {
                db.ExecuteNonQuery(dbCommand, transaction);

                transaction.Commit();

                result = true;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);

            }
            connection.Close();

            return result;
        }
    }

    public static bool pedidoAlteraCartao(double valorDosJuros, decimal porcentagemDoJuros, int numeroDeParcelas, string numeroDoCartao,
                                          string nomeDoCartao, string validadeDoCartao, string codDeSegurancaDoCartao,
                                          double valorDaParcela, DateTime dataHoraDoPedido, int statusDoPedido, int pedidoId)
    {
        bool result = false;

        Database db = DatabaseFactory.CreateDatabase();

        DbCommand dbCommand = db.GetStoredProcCommand("pedidoAlteraCartao");

        db.AddInParameter(dbCommand, "valorDosJuros", DbType.Double, valorDosJuros);
        db.AddInParameter(dbCommand, "porcentagemDoJuros", DbType.Decimal, porcentagemDoJuros);
        db.AddInParameter(dbCommand, "numeroDeParcelas", DbType.Int32, numeroDeParcelas);
        db.AddInParameter(dbCommand, "numeroDoCartao", DbType.String, numeroDoCartao);
        db.AddInParameter(dbCommand, "nomeDoCartao", DbType.String, nomeDoCartao);
        db.AddInParameter(dbCommand, "validadeDoCartao", DbType.String, validadeDoCartao);
        db.AddInParameter(dbCommand, "codDeSegurancaDoCartao", DbType.String, codDeSegurancaDoCartao);
        db.AddInParameter(dbCommand, "valorDaParcela", DbType.Double, valorDaParcela);
        db.AddInParameter(dbCommand, "dataHoraDoPedido", DbType.DateTime, dataHoraDoPedido);
        db.AddInParameter(dbCommand, "statusDoPedido", DbType.Int32, statusDoPedido);
        db.AddInParameter(dbCommand, "pedidoId", DbType.Int32, pedidoId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
            DbTransaction transaction = connection.BeginTransaction();

            try
            {
                db.ExecuteNonQuery(dbCommand, transaction);

                transaction.Commit();

                result = true;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);

            }
            connection.Close();

            return result;
        }
    }

    public static bool pedidoAlteraStatus(int statusDoPedido, int pedidoId)
    {
        bool result = false;

        Database db = DatabaseFactory.CreateDatabase();

        DbCommand dbCommand = db.GetStoredProcCommand("pedidoAlteraStatus");

        db.AddInParameter(dbCommand, "statusDoPedido", DbType.Int32, statusDoPedido);
        db.AddInParameter(dbCommand, "pedidoId", DbType.Int32, pedidoId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
            DbTransaction transaction = connection.BeginTransaction();

            try
            {
                db.ExecuteNonQuery(dbCommand, transaction);

                transaction.Commit();

                result = true;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);

            }
            connection.Close();

            return result;
        }
    }

    public static bool pedidoAlteraTraking(string numeroDoTracking, string numeroDoTracking2, string numeroDoTracking3, string numeroDoTracking4, int pedidoId)
    {
        bool result = false;

        Database db = DatabaseFactory.CreateDatabase();

        DbCommand dbCommand = db.GetStoredProcCommand("pedidoAlteraTraking");

        db.AddInParameter(dbCommand, "numeroDoTracking", DbType.String, numeroDoTracking);
        db.AddInParameter(dbCommand, "numeroDoTracking2", DbType.String, numeroDoTracking2);
        db.AddInParameter(dbCommand, "numeroDoTracking3", DbType.String, numeroDoTracking3);
        db.AddInParameter(dbCommand, "numeroDoTracking4", DbType.String, numeroDoTracking4);
        db.AddInParameter(dbCommand, "pedidoId", DbType.Int32, pedidoId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
            DbTransaction transaction = connection.BeginTransaction();

            try
            {
                db.ExecuteNonQuery(dbCommand, transaction);

                transaction.Commit();

                result = true;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);

            }
            connection.Close();

            return result;
        }
    }

    public static bool pedidoAlteraDespesas(decimal valorDespesaFrete, decimal valorDespesaPagamento, decimal valorDespesaOutras, int pedidoId, bool pedidoPagoFornecedor)
    {
        bool result = false;

        Database db = DatabaseFactory.CreateDatabase();

        DbCommand dbCommand = db.GetStoredProcCommand("pedidoAlteraDespesas");

        db.AddInParameter(dbCommand, "valorDespesaFrete", DbType.Decimal, valorDespesaFrete);
        db.AddInParameter(dbCommand, "valorDespesaPagamento", DbType.Decimal, valorDespesaPagamento);
        db.AddInParameter(dbCommand, "valorDespesaOutras", DbType.Decimal, valorDespesaOutras);
        db.AddInParameter(dbCommand, "pedidoId", DbType.Int32, pedidoId);
        db.AddInParameter(dbCommand, "pedidoPagoFornecedor", DbType.String, pedidoPagoFornecedor.ToString());

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
            DbTransaction transaction = connection.BeginTransaction();

            try
            {
                db.ExecuteNonQuery(dbCommand, transaction);

                transaction.Commit();

                result = true;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);

            }
            connection.Close();

            return result;
        }
    }


    public static bool itensPedidoAlteraCustoProduto(decimal valorCusto, int itemPedidoId, int entreguePeloFornecedor)
    {
        bool result = false;

        Database db = DatabaseFactory.CreateDatabase();

        DbCommand dbCommand = db.GetStoredProcCommand("itensPedidoAlteraCustoProduto");

        db.AddInParameter(dbCommand, "valorCusto", DbType.Decimal, valorCusto);
        db.AddInParameter(dbCommand, "itemPedidoId", DbType.Int32, itemPedidoId);
        db.AddInParameter(dbCommand, "entreguePeloFornecedor", DbType.Int32, entreguePeloFornecedor);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
            DbTransaction transaction = connection.BeginTransaction();

            try
            {
                db.ExecuteNonQuery(dbCommand, transaction);

                transaction.Commit();

                result = true;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);

            }
            connection.Close();

            return result;
        }
    }

    public static bool pedidoAlteraNoCarrinho(string ipDoCliente, int tipoDePagamentoId, int condDePagamentoId, int tipoDeEntregaId, double valorDoFrete, string prazoDeEntrega, string cupomDeDesconto, double valorDoDescontoDoCupom,
                                              double valorDoDescontoDoPagamento, double valorDosItens, double valorTotalGeral, double valorCobrado, decimal pesoDoPedido, double valorDoEmbrulhoECartao, int pedidoId)
    {
        bool result = false;

        Database db = DatabaseFactory.CreateDatabase();

        DbCommand dbCommand = db.GetStoredProcCommand("pedidoAlteraNoCarrinho");

        db.AddInParameter(dbCommand, "ipDoCliente", DbType.String, ipDoCliente);
        db.AddInParameter(dbCommand, "tipoDePagamentoId", DbType.Int32, tipoDePagamentoId);
        db.AddInParameter(dbCommand, "condDePagamentoId", DbType.Int32, condDePagamentoId);
        db.AddInParameter(dbCommand, "tipoDeEntregaId", DbType.Int32, tipoDeEntregaId);
        db.AddInParameter(dbCommand, "valorDoFrete", DbType.Double, valorDoFrete);
        db.AddInParameter(dbCommand, "prazoDeEntrega", DbType.String, prazoDeEntrega);
        db.AddInParameter(dbCommand, "cupomDeDesconto", DbType.String, cupomDeDesconto);
        db.AddInParameter(dbCommand, "valorDoDescontoDoCupom", DbType.Double, valorDoDescontoDoCupom);
        db.AddInParameter(dbCommand, "valorDoDescontoDoPagamento", DbType.Double, valorDoDescontoDoPagamento);
        db.AddInParameter(dbCommand, "valorDosItens", DbType.Double, valorDosItens);
        db.AddInParameter(dbCommand, "valorTotalGeral", DbType.Double, valorTotalGeral);
        db.AddInParameter(dbCommand, "valorCobrado", DbType.Double, valorCobrado);
        db.AddInParameter(dbCommand, "pesoDoPedido", DbType.Decimal, pesoDoPedido);
        db.AddInParameter(dbCommand, "valorDoEmbrulhoECartao", DbType.Double, valorDoEmbrulhoECartao);
        db.AddInParameter(dbCommand, "pedidoId", DbType.Int32, pedidoId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
            DbTransaction transaction = connection.BeginTransaction();

            try
            {
                db.ExecuteNonQuery(dbCommand, transaction);

                transaction.Commit();

                result = true;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);

            }
            connection.Close();

            return result;
        }
    }

    public static bool pedidoAlteraEndereco(string endNomeDoDestinatario, string endRua, string endNumero, string endComplemento, string endBairro, string endCidade,
                                          string endEstado, string endPais, string endReferenciaParaEntrega, int tipoDeEntregaId, double valorDoFrete, string prazoDeEntrega, double valorTotalGeral, double valorCobrado, string endCep, int pedidoId)
    {
        bool result = false;

        Database db = DatabaseFactory.CreateDatabase();

        DbCommand dbCommand = db.GetStoredProcCommand("pedidoAlteraEndereco");

        db.AddInParameter(dbCommand, "endNomeDoDestinatario", DbType.String, endNomeDoDestinatario);
        db.AddInParameter(dbCommand, "endRua", DbType.String, endRua);
        db.AddInParameter(dbCommand, "endNumero", DbType.String, endNumero);
        db.AddInParameter(dbCommand, "endComplemento", DbType.String, endComplemento);
        db.AddInParameter(dbCommand, "endBairro", DbType.String, endBairro);
        db.AddInParameter(dbCommand, "endCidade", DbType.String, endCidade);
        db.AddInParameter(dbCommand, "endEstado", DbType.String, endEstado);
        db.AddInParameter(dbCommand, "endPais", DbType.String, endPais);
        db.AddInParameter(dbCommand, "endReferenciaParaEntrega", DbType.String, endReferenciaParaEntrega);
        db.AddInParameter(dbCommand, "tipoDeEntregaId", DbType.Int32, tipoDeEntregaId);
        db.AddInParameter(dbCommand, "valorDoFrete", DbType.Double, valorDoFrete);
        db.AddInParameter(dbCommand, "prazoDeEntrega", DbType.String, prazoDeEntrega);
        db.AddInParameter(dbCommand, "valorTotalGeral", DbType.Double, valorTotalGeral);
        db.AddInParameter(dbCommand, "valorCobrado", DbType.Double, valorCobrado);
        db.AddInParameter(dbCommand, "endCep", DbType.String, endCep);
        db.AddInParameter(dbCommand, "pedidoId", DbType.Int32, pedidoId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
            DbTransaction transaction = connection.BeginTransaction();

            try
            {
                db.ExecuteNonQuery(dbCommand, transaction);

                transaction.Commit();

                result = true;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);

            }
            connection.Close();

            return result;
        }
    }

    public static bool itensPedidoAltera(decimal itemQuantidade, double itemValor, string itemPresente, string ItemMensagemDoCartao, double valorDoEmbrulhoECartao, int itemPedidoId)
    {
        bool result = false;

        Database db = DatabaseFactory.CreateDatabase();

        DbCommand dbCommand = db.GetStoredProcCommand("itensPedidoAltera");

        db.AddInParameter(dbCommand, "itemQuantidade", DbType.Decimal, itemQuantidade);
        db.AddInParameter(dbCommand, "itemValor", DbType.Double, itemValor);
        db.AddInParameter(dbCommand, "itemPresente", DbType.String, itemPresente);
        db.AddInParameter(dbCommand, "ItemMensagemDoCartao", DbType.String, ItemMensagemDoCartao);
        db.AddInParameter(dbCommand, "valorDoEmbrulhoECartao", DbType.String, valorDoEmbrulhoECartao);
        db.AddInParameter(dbCommand, "itemPedidoId", DbType.Int32, itemPedidoId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
            DbTransaction transaction = connection.BeginTransaction();

            try
            {
                db.ExecuteNonQuery(dbCommand, transaction);

                transaction.Commit();

                result = true;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);

            }
            connection.Close();

            return result;
        }
    }

    public static DataSet itensPedidoSeleciona_PorPedidoIdEProdutoId(int pedidoId, int produtoId)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("itensPedidoSeleciona_PorPedidoIdEProdutoId");

        db.AddInParameter(dbCommand, "pedidoId", DbType.Int32, pedidoId);
        db.AddInParameter(dbCommand, "produtoId", DbType.Int32, produtoId);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static bool itemPedidoAlteraCompreJuntoId(int compreJuntoId)
    {
        bool result = false;

        Database db = DatabaseFactory.CreateDatabase();

        DbCommand dbCommand = db.GetStoredProcCommand("itemPedidoAlteraCompreJuntoId");

        db.AddInParameter(dbCommand, "compreJuntoId", DbType.Int32, compreJuntoId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
            DbTransaction transaction = connection.BeginTransaction();

            try
            {
                db.ExecuteNonQuery(dbCommand, transaction);

                transaction.Commit();

                result = true;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);

            }
            connection.Close();

            return result;
        }
    }

    public static void pedidoEItensInclui(int produtoId, int itemQuantidade, decimal itemValor, int compreJuntoId, int garantiaId)
    {
        string produtoIdDaEmpresa = rnProdutos.produtoSeleciona_PorProdutoId(produtoId).Tables[0].Rows[0]["produtoIdDaEmpresa"].ToString();
        string produtoCodDeBarras = rnProdutos.produtoSeleciona_PorProdutoId(produtoId).Tables[0].Rows[0]["produtoCodDeBarras"].ToString();
        if (page().Request.Cookies["cliente"] == null)
        {
            //Se ainda não existir o cookie gravo ele
            HttpCookie cliente = new HttpCookie("cliente");
            if (rnPedidos.pedidoSelecionaUltimoId().Tables[0].Rows.Count > 0)
            {
                cliente["clienteId"] = (int.Parse(rnPedidos.pedidoSelecionaUltimoId().Tables[0].Rows[0]["pedidoId"].ToString()) + 1).ToString();
                cliente["pedidoId"] = (int.Parse(rnPedidos.pedidoSelecionaUltimoId().Tables[0].Rows[0]["pedidoId"].ToString()) + 1).ToString();
                cliente["pedidoKey"] = page().Session.SessionID + (int.Parse(rnPedidos.pedidoSelecionaUltimoId().Tables[0].Rows[0]["pedidoId"].ToString()) + 1).ToString();
            }
            else
            {
                cliente["clienteId"] = "10000";
                cliente["pedidoId"] = "10000";
                cliente["pedidoKey"] = page().Session.SessionID + "10000";
            }
            cliente["clienteLogado"] = "False";
            cliente.Expires = DateTime.Now.AddDays(365);
            page().Response.Cookies.Add(cliente);

            //Crio um novo pedido
            rnPedidos.pedidoInclui(int.Parse(page().Request.Cookies["cliente"]["clienteId"].ToString()), page().Session.SessionID + (int.Parse(rnPedidos.pedidoSelecionaUltimoId().Tables[0].Rows[0]["pedidoId"].ToString()) + 1).ToString());

            //Incluo oa itens na tabela tbItensPedido
            rnPedidos.itensPedidoInclui(int.Parse(page().Request.Cookies["cliente"]["pedidoId"].ToString()), produtoId, produtoIdDaEmpresa, produtoCodDeBarras, itemQuantidade, itemValor, compreJuntoId, garantiaId);
        }
        else
        {
            string clienteId = page().Request.Cookies["cliente"]["clienteId"].ToString();

            string pedidoId;
            if (page().Request.Cookies["cliente"]["pedidoId"] == null)
            {
                rnPedidos.pedidoInclui(int.Parse(page().Request.Cookies["cliente"]["clienteId"].ToString()), page().Session.SessionID + (int.Parse(rnPedidos.pedidoSelecionaUltimoId().Tables[0].Rows[0]["pedidoId"].ToString()) + 1).ToString());
                pedidoId = int.Parse(rnPedidos.pedidoSelecionaUltimoId().Tables[0].Rows[0]["pedidoId"].ToString()).ToString();
            }
            else
                pedidoId = page().Request.Cookies["cliente"]["pedidoId"].ToString();

            string clienteLogado = page().Request.Cookies["cliente"]["clienteLogado"].ToString();

            HttpCookie cliente = new HttpCookie("cliente");
            cliente["clienteId"] = clienteId.ToString();
            cliente["pedidoId"] = pedidoId.ToString();

            if (page().Request.Cookies["cliente"]["pedidoKey"] != null)
                cliente["pedidoKey"] = page().Request.Cookies["cliente"]["pedidoKey"].ToString();
            else
                cliente["pedidoKey"] = page().Session.SessionID + (int.Parse(rnPedidos.pedidoSelecionaUltimoId().Tables[0].Rows[0]["pedidoId"].ToString())).ToString();

            cliente["clienteLogado"] = clienteLogado;
            cliente.Expires = DateTime.Now.AddDays(365);
            page().Response.Cookies.Add(cliente);

            //Incluo oa itens na tabela tbItensPedido
            rnPedidos.itensPedidoInclui(int.Parse(pedidoId), produtoId, produtoIdDaEmpresa, produtoCodDeBarras, itemQuantidade, itemValor, compreJuntoId, garantiaId);
        }
    }

    public static bool pedidoInclui(int clienteId, string pedidoKey)
    {
        bool result = false;

        Database db = DatabaseFactory.CreateDatabase();

        DbCommand dbCommand = db.GetStoredProcCommand("pedidoInclui");

        db.AddInParameter(dbCommand, "clienteId", DbType.Int32, clienteId);
        db.AddInParameter(dbCommand, "pedidoKey", DbType.String, pedidoKey);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
            DbTransaction transaction = connection.BeginTransaction();

            try
            {
                db.ExecuteNonQuery(dbCommand, transaction);

                transaction.Commit();

                result = true;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);

            }
            connection.Close();

            return result;
        }
    }

    public static bool itensPedidoInclui(int pedidoId, int produtoId, string produtoIdDaEmpresa, string produtoCodDeBarra, decimal itemQuantidade, decimal itemValor, int compreJuntoId, int garantiaId)
    {
        bool result = false;

        Database db = DatabaseFactory.CreateDatabase();

        DbCommand dbCommand = db.GetStoredProcCommand("itensPedidoInclui");

        db.AddInParameter(dbCommand, "pedidoId", DbType.Int32, pedidoId);
        db.AddInParameter(dbCommand, "produtoId", DbType.Int32, produtoId);
        db.AddInParameter(dbCommand, "produtoIdDaEmpresa", DbType.String, produtoIdDaEmpresa);
        db.AddInParameter(dbCommand, "produtoCodDeBarra", DbType.String, produtoCodDeBarra);
        db.AddInParameter(dbCommand, "itemQuantidade", DbType.Decimal, itemQuantidade);
        db.AddInParameter(dbCommand, "itemValor", DbType.Decimal, itemValor);
        db.AddInParameter(dbCommand, "compreJuntoId", DbType.Int32, compreJuntoId);
        db.AddInParameter(dbCommand, "garantiaId", DbType.Int32, garantiaId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
            DbTransaction transaction = connection.BeginTransaction();

            try
            {
                db.ExecuteNonQuery(dbCommand, transaction);

                transaction.Commit();

                result = true;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);

            }
            connection.Close();

            return result;
        }
    }

    public static bool itensPedidoCompreJuntoInclui(int pedidoId, int produtoId, string produtoIdDaEmpresa, string produtoCodDeBarra, decimal itemQuantidade, decimal itemValor, int compreJuntoId, int garantiaId)
    {
        bool result = false;

        Database db = DatabaseFactory.CreateDatabase();

        DbCommand dbCommand = db.GetStoredProcCommand("itensPedidoCompreJuntoInclui");

        db.AddInParameter(dbCommand, "pedidoId", DbType.Int32, pedidoId);
        db.AddInParameter(dbCommand, "produtoId", DbType.Int32, produtoId);
        db.AddInParameter(dbCommand, "produtoIdDaEmpresa", DbType.String, produtoIdDaEmpresa);
        db.AddInParameter(dbCommand, "produtoCodDeBarra", DbType.String, produtoCodDeBarra);
        db.AddInParameter(dbCommand, "itemQuantidade", DbType.Decimal, itemQuantidade);
        db.AddInParameter(dbCommand, "itemValor", DbType.Decimal, itemValor);
        db.AddInParameter(dbCommand, "compreJuntoId", DbType.Int32, compreJuntoId);
        db.AddInParameter(dbCommand, "garantiaId", DbType.Int32, garantiaId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
            DbTransaction transaction = connection.BeginTransaction();

            try
            {
                db.ExecuteNonQuery(dbCommand, transaction);

                transaction.Commit();

                result = true;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);

            }
            connection.Close();

            return result;
        }
    }


    public static bool pedidoAlteraClienteId(int pedidoId, int clienteId)
    {
        bool result = false;

        Database db = DatabaseFactory.CreateDatabase();

        DbCommand dbCommand = db.GetStoredProcCommand("pedidoAlteraClienteId");

        db.AddInParameter(dbCommand, "pedidoId", DbType.Int32, pedidoId);
        db.AddInParameter(dbCommand, "clienteId", DbType.Int32, clienteId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
            DbTransaction transaction = connection.BeginTransaction();

            try
            {
                db.ExecuteNonQuery(dbCommand, transaction);

                transaction.Commit();

                result = true;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);

            }
            connection.Close();

            return result;
        }
    }

    public static DataSet fornecedoresComVendasPorData(string dataInicial, string dataFinal)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("admin_fornecedoresComVendasPorData");

        db.AddInParameter(dbCommand, "dataInicial", DbType.String, dataInicial);
        db.AddInParameter(dbCommand, "dataFinal", DbType.String, dataFinal);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet fornecedoresComVendasPedidos(string entregue)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("admin_fornecedoresComVendasPedidos");

        db.AddInParameter(dbCommand, "entregue", DbType.String, entregue);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }
    public static DataSet pedidosFornecedorConfirmados(string entregue)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("admin_pedidosFornecedorConfirmados");

        db.AddInParameter(dbCommand, "entregue", DbType.String, entregue);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet pedidosFornecedorConfirmadosPorPeriodo(string entregue, string dataInicial, string dataFinal)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("admin_pedidosFornecedorConfirmadosPorPeriodo2");

        db.AddInParameter(dbCommand, "entregue", DbType.String, entregue);
        db.AddInParameter(dbCommand, "dataInicial", DbType.String, dataInicial);
        db.AddInParameter(dbCommand, "dataFinal", DbType.String, dataFinal);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet fornecedoresComVendas()
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("admin_fornecedoresComVendasPedidoFornecedor");

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }
    public static DataSet pedidosFornecedorIndividuais()
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("admin_pedidosFornecedorIndividuais");

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }
    public static DataSet pedidosFornecedorPendentes()
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("admin_pedidosFornecedorPendentes");

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet pedidoFornecedorPorPedidoFornecedorId(int idPedidoFornecedor)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("admin_pedidoFornecedorPorPedidoFornecedorId");

        db.AddInParameter(dbCommand, "idPedidoFornecedor", DbType.Int32, idPedidoFornecedor);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet pedidoFornecedorPorPedidoFornecedorIndividualId(int idPedidoFornecedor)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("admin_pedidoFornecedorIndividualPorPedidoFornecedorId");

        db.AddInParameter(dbCommand, "idPedidoFornecedor", DbType.Int32, idPedidoFornecedor);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }
    public static DataSet admin_pedidosCompletosSeparacaoEstoque()
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("admin_pedidosCompletosSeparacaoEstoque");


        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }
    public static DataSet admin_pedidosCompletosSeparacaoEstoque2()
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("admin_pedidosCompletosSeparacaoEstoque2");


        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static void alteraEstoqueProdutosPorPedido(int pedidoId)
    {
        alteraEstoqueProdutosPorPedido(pedidoId, false);
    }

    public static void alteraEstoqueProdutosPorPedido(int pedidoId, bool ignorarEnviados)
    {
        var interacoesDc = new dbCommerceDataContext();
        var interacao = (from c in interacoesDc.tbPedidoInteracoes
                         where c.interacao.ToString().ToLower() == "pedido enviado" && c.alteracaoDeEstatus.ToLower() == "true" && c.pedidoId == pedidoId
                         select c).Any();
        if (!interacao | ignorarEnviados)
        {
            var itensPedidoDc = new dbCommerceDataContext();
            var itensPedido = (from c in itensPedidoDc.tbItensPedidos where c.pedidoId == pedidoId select c);
            foreach (var itemPedido in itensPedido)
            {
                var produtosDc = new dbCommerceDataContext();
                var produtosFilho = (from c in produtosDc.tbProdutoRelacionados where c.idProdutoPai == itemPedido.produtoId select c);
                if (produtosFilho.Any())
                {
                    foreach (var produtoRelacionado in produtosFilho)
                    {
                        var produtoDc = new dbCommerceDataContext();
                        var produto = (from c in produtoDc.tbProdutos where c.produtoId == produtoRelacionado.idProdutoFilho select c).FirstOrDefault();
                        if (produto != null)
                        {
                            if (produto.estoqueReal == null) produto.estoqueReal = 0;
                            var logEstoque = new tbProdutoLogEstoque();
                            logEstoque.idProduto = produto.produtoId;
                            logEstoque.estoqueAnterior = Convert.ToInt32(produto.estoqueReal);
                            logEstoque.idOperacao = pedidoId;

                            produto.estoqueReal -= Convert.ToInt32("0" + itemPedido.itemQuantidade);
                            produtoDc.SubmitChanges();

                            logEstoque.estoqueAlterado = Convert.ToInt32(produto.estoqueReal);
                            logEstoque.data = DateTime.Now;
                            logEstoque.descricao = "Pedido enviado " + pedidoId;
                            produtoDc.tbProdutoLogEstoques.InsertOnSubmit(logEstoque);
                            produtoDc.SubmitChanges();
                        }
                    }
                }
                else
                {
                    var produtoDc = new dbCommerceDataContext();
                    var produto = (from c in produtoDc.tbProdutos where c.produtoId == itemPedido.produtoId select c).FirstOrDefault();
                    if (produto != null)
                    {
                        if (produto.estoqueReal == null) produto.estoqueReal = 0;
                        var logEstoque = new tbProdutoLogEstoque();
                        logEstoque.idProduto = produto.produtoId;
                        logEstoque.estoqueAnterior = Convert.ToInt32(produto.estoqueReal);
                        logEstoque.idOperacao = pedidoId;

                        produto.estoqueReal -= Convert.ToInt32("0" + itemPedido.itemQuantidade);
                        produtoDc.SubmitChanges();

                        logEstoque.estoqueAlterado = Convert.ToInt32(produto.estoqueReal);
                        logEstoque.data = DateTime.Now;
                        logEstoque.descricao = "Pedido enviado " + pedidoId;
                        produtoDc.tbProdutoLogEstoques.InsertOnSubmit(logEstoque);
                        produtoDc.SubmitChanges();
                    }
                }
            }
        }
    }

    public static void alteraEstoqueProdutosEnviados(int pedidoId)
    {
        var interacoesDc = new dbCommerceDataContext();
        var listaProdutos = rnPedidos.retornaProdutosEtiqueta(pedidoId);

        foreach (var listaProduto in listaProdutos)
        {
            var produtoDc = new dbCommerceDataContext();
            var produto = (from c in produtoDc.tbProdutos where c.produtoId == listaProduto.produtoId select c).FirstOrDefault();
            if (produto != null)
            {
                if (produto.estoqueReal == null) produto.estoqueReal = 0;
                var logEstoque = new tbProdutoLogEstoque();
                logEstoque.idProduto = produto.produtoId;
                logEstoque.estoqueAnterior = Convert.ToInt32(produto.estoqueReal);
                logEstoque.idOperacao = pedidoId;

                produto.estoqueReal -= Convert.ToInt32("0" + listaProduto.itemQuantidade);
                produtoDc.SubmitChanges();

                logEstoque.estoqueAlterado = Convert.ToInt32(produto.estoqueReal);
                logEstoque.data = DateTime.Now;
                logEstoque.descricao = "Pedido enviado " + pedidoId;
                produtoDc.tbProdutoLogEstoques.InsertOnSubmit(logEstoque);
                produtoDc.SubmitChanges();
            }
        }

    }
    public static void retiraVinculoPedidoFornecedor(int pedidoId)
    {
        var pedidosDc = new dbCommerceDataContext();
        var itensPedido = (from c in pedidosDc.tbPedidoFornecedorItems where c.idPedido == pedidoId && c.entregue == false select c);
        foreach (var item in itensPedido)
        {
            item.idItemPedido = null;
            item.idPedido = null;
        }
        var itensPedidoSeparado = (from c in pedidosDc.tbProdutoEstoques where c.pedidoId == pedidoId && c.enviado == false select c);
        foreach (var itemPedidoSeparado in itensPedidoSeparado)
        {
            itemPedidoSeparado.itemPedidoId = null;
            itemPedidoSeparado.idPedidoEnvio = null;
            itemPedidoSeparado.pedidoId = null;
        }
        pedidosDc.SubmitChanges();
    }

    public static void retiraVinculoEstoqueItemPedido(int itemPedidoId)
    {
        var pedidosDc = new dbCommerceDataContext();
        var itensPedido = (from c in pedidosDc.tbPedidoFornecedorItems where c.idItemPedido == itemPedidoId select c);
        foreach (var item in itensPedido)
        {
            item.idItemPedido = null;
            item.idPedido = null;
        }
        pedidosDc.SubmitChanges();

        var itensPedidoSeparado = (from c in pedidosDc.tbProdutoEstoques where c.itemPedidoId == itemPedidoId && c.enviado == false select c);
        foreach (var itemPedidoSeparado in itensPedidoSeparado)
        {
            itemPedidoSeparado.itemPedidoId = null;
            itemPedidoSeparado.idPedidoEnvio = null;
            itemPedidoSeparado.pedidoId = null;
            pedidosDc.SubmitChanges();
        }
        var produtosDc = new dbCommerceDataContext();
        var reservas = (from c in produtosDc.tbProdutoReservaEstoques where c.idItemPedido == itemPedidoId select c);
        foreach (var item in reservas)
        {
            produtosDc.tbProdutoReservaEstoques.DeleteOnSubmit(item);
        }

        var itemDoPedido = (from c in produtosDc.tbItensPedidos where c.itemPedidoId == itemPedidoId select c).FirstOrDefault();
        if (itemDoPedido != null)
        {
            var queueLiberar = new tbQueue();
            queueLiberar.tipoQueue = 20; //Libera produtos cancelados
            queueLiberar.agendamento = DateTime.Now;
            queueLiberar.idRelacionado = itemDoPedido.tbPedido.pedidoId;
            queueLiberar.mensagem = "";
            queueLiberar.concluido = false;
            queueLiberar.andamento = false;
            produtosDc.tbQueues.InsertOnSubmit(queueLiberar);
            produtosDc.SubmitChanges();
        }

        produtosDc.SubmitChanges();
    }

    public static void checaPedidosPendentesJadlog()
    {
        var pedidosDc = new dbCommerceDataContext();
        /*var pedidosJad = (from c in pedidosDc.tbPedidos
                          where c.codPedidoJadlog != null && c.codPedidoJadlog != "" && (c.numeroDoTracking4 == "" | c.numeroDoTracking4 == null)
                          select c);
        foreach (var pedido in pedidosJad)
        {
            string numeroTracking = checaRastreioJad(pedido.codPedidoJadlog);
            if (!string.IsNullOrEmpty(numeroTracking))
            {
                pedido.numeroDoTracking4 = numeroTracking;
                pedidosDc.SubmitChanges();
                rnEmails.enviaCodigoDoTrakingJadlog(pedido.pedidoId);
            }
        }*/

        var pedidosJad2 = (from c in pedidosDc.tbPedidoPacotes
                           where c.codJadlog != null && c.codJadlog != "" && (c.rastreio == "" | c.rastreio == null)
                           select new { c.codJadlog, c.idPedido }).Distinct().ToList();
        foreach (var pedido in pedidosJad2)
        {
            string numeroTracking = checaRastreioJad(pedido.codJadlog);
            if (!string.IsNullOrEmpty(numeroTracking))
            {
                var itensCodigo = (from c in pedidosDc.tbPedidoPacotes where c.codJadlog == pedido.codJadlog select c);
                foreach (var pacote in itensCodigo)
                {
                    pacote.rastreio = numeroTracking;
                }
                pedidosDc.SubmitChanges();
                rnEmails.enviaCodigoDoTrakingJadlog(pedido.idPedido, numeroTracking);
            }
        }
    }
    public static void checaPedidosPendentesTnt()
    {
        var pedidosDc = new dbCommerceDataContext();
        var pedidosTnt = (from c in pedidosDc.tbPedidoEnvios
                          join d in pedidosDc.tbPedidoPacotes on c.idPedidoEnvio equals d.idPedidoEnvio into pacotes
                          where c.nfeStatus == 2 && pacotes.Any(x => x.despachado == true) && pacotes.Any(x => (x.rastreio ?? "") == "")
                          select c);
        foreach (var pedido in pedidosTnt)
        {
            string numeroTracking = checaRastreioTnt((int)pedido.nfeNumero);
            if (!string.IsNullOrEmpty(numeroTracking))
            {
                var itensCodigo = (from c in pedidosDc.tbPedidoPacotes where c.idPedidoEnvio == pedido.idPedidoEnvio select c);
                foreach (var pacote in itensCodigo)
                {
                    pacote.rastreio = numeroTracking;
                }
                pedidosDc.SubmitChanges();
            }
        }
    }

    private static string checaRastreioJad(string codPedidoJadlog)
    {
        //Cod Lindsay
        string CodCliente = "10924051000163";
        string Password = "L2F0M0E1";

        //Cod Mayara
        //string CodCliente = "20907518000110";
        //string Password = "M2c0F1m4";

        var serviceJad = new serviceJadTracking.TrackingBeanService();
        var retornoServiceJad = serviceJad.consultar(CodCliente, Password, codPedidoJadlog);
        //Response.Write(retornoServiceJad + "<br>");
        string numeroTracking = "";

        XmlDocument xmlDoc = new XmlDocument();
        xmlDoc.LoadXml(retornoServiceJad);
        XmlNodeList parentNode = xmlDoc.GetElementsByTagName("ND");

        foreach (XmlNode childrenNode in parentNode)
        {
            try
            {
                string retorno = childrenNode.ChildNodes[0].InnerText;
                numeroTracking = retorno;
            }
            catch (Exception)
            {

            }
        }
        return numeroTracking;
    }
    private static string checaRastreioTnt(int nota)
    {
        string numeroTracking = "";
        var parametros = new serviceTntLocalizacao.LocalizacaoIn();
        parametros.usuario = "atendimento@bark.com.br";
        parametros.nf = nota;
        parametros.nfSpecified = true;
        parametros.nfSerie = "1";
        parametros.cnpj = "10924051000163";

        try
        {
            var consulta = new serviceTntLocalizacao.Localizacao();
            var retorno = consulta.localizaMercadoria(parametros);
            numeroTracking = retorno.conhecimento;
        }
        catch (Exception)
        {

        }
        //retorno.localizacao;
        //retorno.conhecimento;
        //http://app.tntbrasil.com.br/LocalizacaoSimplificada/resultado_localizacao.asp?cnpj=01404654135&nota=7858

        string teste = "";
        return numeroTracking;
    }
    /*
    public static List<tbPedido> verificaPedidosCompletos()
    {
        
        var pedidosCompletos = new List<int>();

        var pedidosDc = new dbCommerceDataContext();
        var itensPedidoDc = new dbCommerceDataContext();
        var produtosDc = new dbCommerceDataContext();
        var pedidosSeparacao = (from c in pedidosDc.tbPedidos where c.statusDoPedido == 11 select c.pedidoId);

        foreach (var pedidoSeparacao in pedidosSeparacao)
        {
            var listaProdutos = new List<produtoQuantidade>();
            var itensPedido = (from c in itensPedidoDc.tbItensPedidos where c.pedidoId == pedidoSeparacao select c);
            foreach (var itemPedido in itensPedido)
            {
                bool cancelado = itemPedido.cancelado == null ? false : (bool)itemPedido.cancelado;
                if (!cancelado)
                {
                    var produtosFilho =
                        (from c in produtosDc.tbProdutoRelacionados
                            where c.idProdutoPai == itemPedido.produtoId
                            select c);
                    if (produtosFilho.Any())
                    {
                        foreach (var relacionado in produtosFilho)
                        {
                            var parcial = (from c in pedidosDc.tbItensPedidoEnvioParcials
                                where
                                    c.idPedido == pedidoSeparacao && c.idProduto == relacionado.idProdutoFilho &&
                                    c.idItemPedido == itemPedido.itemPedidoId && c.enviado == false
                                select c).Any();
                            if (!parcial)
                            {
                                var produtoQuantidadeCheck =
                                    listaProdutos.FirstOrDefault(x => x.produtoId == relacionado.idProdutoFilho);
                                if (produtoQuantidadeCheck != null)
                                {
                                    produtoQuantidadeCheck.quantidade += Convert.ToInt32(itemPedido.itemQuantidade);

                                }
                                else
                                {
                                    var produtoQuantidade = new produtoQuantidade();
                                    produtoQuantidade.produtoId = relacionado.idProdutoFilho;
                                    produtoQuantidade.quantidade = Convert.ToInt32(itemPedido.itemQuantidade);
                                    listaProdutos.Add(produtoQuantidade);
                                }
                            }
                        }
                    }
                    else
                    {
                        var parcial = (from c in pedidosDc.tbItensPedidoEnvioParcials
                                       where
                                           c.idPedido == pedidoSeparacao && c.idProduto == itemPedido.produtoId &&
                                           c.idItemPedido == itemPedido.itemPedidoId && c.enviado == false
                                       select c).Any();
                        if (!parcial)
                        {
                            var produtoQuantidadeCheck =
                                listaProdutos.FirstOrDefault(x => x.produtoId == itemPedido.produtoId);
                            if (produtoQuantidadeCheck != null)
                            {
                                produtoQuantidadeCheck.quantidade += Convert.ToInt32(itemPedido.itemQuantidade);

                            }
                            else
                            {
                                var produtoQuantidade = new produtoQuantidade();
                                produtoQuantidade.produtoId = itemPedido.produtoId;
                                produtoQuantidade.quantidade = Convert.ToInt32(itemPedido.itemQuantidade);
                                listaProdutos.Add(produtoQuantidade);
                            }
                        }
                    }
                }
            }

            bool totalCompleto = true;
            foreach (var produto in listaProdutos)
            {
                if (totalCompleto)
                {
                    var itensFaltando = (from c in produtosDc.tbPedidoProdutoFaltandos
                                         where c.pedidoId == pedidoSeparacao && c.produtoId == produto.produtoId && c.entregue == false
                                         select c);
                    if (itensFaltando.Any())
                    {
                        totalCompleto = false;
                    }

                    if (totalCompleto)
                    {
                        int totalReservado = 0;
                        var reservas = (from c in produtosDc.tbProdutoReservaEstoques
                                        where c.idProduto == produto.produtoId && c.idPedido == pedidoSeparacao
                                        select c);
                        if (reservas.Any())
                        {
                            totalReservado = reservas.Sum(x => x.quantidade);
                        }
                        int totalPedidosFornecedor = 0;
                        var pedidosFornecedor = (from c in pedidosDc.tbPedidoFornecedorItems
                                                 where c.idProduto == produto.produtoId && c.idPedido == pedidoSeparacao && c.entregue == true
                                                 select c);
                        if (pedidosFornecedor.Any())
                        {
                            totalPedidosFornecedor = pedidosFornecedor.Sum(x => x.quantidade);
                        }

                        if ((totalReservado + totalPedidosFornecedor) < produto.quantidade)
                        {
                            totalCompleto = false;
                        }
                    }
                }
            }

            if (totalCompleto)
            {
                pedidosCompletos.Add(pedidoSeparacao);
            }
        }


        var pedidos = (from c in pedidosDc.tbPedidos where pedidosCompletos.Contains(c.pedidoId) orderby c.prioridadeEnvio, c.prazoFinalPedido, c.dataHoraDoPedido select c).ToList();
        return pedidos;
    }

    */



    public static List<tbPedido> verificaPedidosAguardandoCd2()
    {
        var data = new dbCommerceDataContext();
        var pedidos = (from c in data.tbPedidos
                       join d in data.tbItemPedidoEstoques on c.pedidoId equals d.tbItensPedido.pedidoId into itensEstoque
                       join e in data.tbNotaFiscals on c.pedidoId equals e.idPedido into notas
                       where c.statusDoPedido == 11 &&
                       itensEstoque.Count(x => x.cancelado == false && x.enviado == false && x.reservado == false && (x.reservado == false && x.autorizarParcial == true) == false) == 0
                       && itensEstoque.Count(x => x.cancelado == false && x.enviado == false) > 0
                       orderby (c.entregaEnviarPor == "JadlogExpressa" ? 0 : c.prioridadeEnvio == null ? 9999 : c.prioridadeEnvio),
                       (
                       notas.OrderByDescending(x => x.idNotaFiscal).FirstOrDefault() != null ?
                       (notas.OrderByDescending(x => x.idNotaFiscal).FirstOrDefault().dataHora < c.prazoFinalPedido ?
                       notas.OrderByDescending(x => x.idNotaFiscal).FirstOrDefault().dataHora :
                       c.prazoMaximoPostagemAtualizado != null ?
                       c.prazoMaximoPostagemAtualizado : c.prazoFinalPedido) :
                       c.prazoMaximoPostagemAtualizado != null ?
                       c.prazoMaximoPostagemAtualizado :
                       c.prazoFinalPedido == null ? DateTime.Now : c.prazoFinalPedido),
                       c.dataHoraDoPedido
                       select c).ToList();
        pedidos = pedidos.Where(x => x.envioLiberado == true).ToList();
        return pedidos;
    }

    public static List<tbPedido> verificaPedidosCompletos()
    {
        return verificaPedidosCompletos(1);
    }
    public static List<tbPedido> verificaPedidosCompletosSeparadosCd1e2()
    {
        var data = new dbCommerceDataContext();
        //var pedidos = (from c in data.tbPedidos where c.envioLiberado == true && c.statusDoPedido == 11 orderby (c.entregaEnviarPor == "JadlogExpressa" ? 0 : c.prioridadeEnvio == null ? 9999 : c.prioridadeEnvio), (c.prazoFinalPedido != null ? c.prazoFinalPedido : c.marketplace == true ? DateTime.Now : DateTime.Now.AddDays(999)), c.dataHoraDoPedido select c).ToList();
        //return pedidos;
        var pedidosCd1 = (from c in data.tbPedidos
                            where c.statusDoPedido == 11 && c.envioLiberado == true &&
                            c.itensPendenteEnvioCd1 > 0 && c.itensPendetenEnvioCd2 == 0 && c.separado == true  && c.separadoCd1 == true
                          select c).ToList();
        var pedidosCd2 = (from c in data.tbPedidos
                          where c.statusDoPedido == 11 && c.envioLiberado == true &&
                          c.itensPendenteEnvioCd1 > 0 && c.itensPendetenEnvioCd2 > 0 && c.separadoCd2 == true
                          select c).ToList();
        var pedidosGeral = pedidosCd1.Union(pedidosCd2).ToList();
        pedidosGeral = (from c in pedidosGeral                            
                            orderby (c.entregaEnviarPor == "JadlogExpressa" ? 0 : c.prioridadeEnvio == null ? 9999 : c.prioridadeEnvio),
                            (c.prazoMaximoPostagemAtualizado != null ?
                            c.prazoMaximoPostagemAtualizado :
                            c.prazoFinalPedido == null ? DateTime.Now : c.prazoFinalPedido),
                            c.dataHoraDoPedido
                            select c).ToList();
        /*var pedidos = (from c in data.tbPedidos
                       join d in data.tbItemPedidoEstoques on c.pedidoId equals d.tbItensPedido.pedidoId into itensEstoque
                       join e in data.tbNotaFiscals on c.pedidoId equals e.idPedido into notas
                       join f in data.tbProdutoEstoques on c.pedidoId equals f.pedidoId into itensSeparados
                       where c.statusDoPedido == 11 && c.envioLiberado == true &&
                       itensEstoque.Count(x => x.cancelado == false && x.enviado == false && x.reservado == false && (x.reservado == false && x.autorizarParcial == true) == false) == 0
                       && itensEstoque.Count(x => x.cancelado == false && x.enviado == false) > 0 &&
                       itensEstoque.Count(x => x.cancelado == false && x.enviado == false && x.reservado == true) == itensSeparados.Count(x => x.enviado == false) &&
                       (itensEstoque.Count(x => x.cancelado == false && x.enviado == false && x.reservado == true && x.idCentroDistribuicao == 1) <= itensSeparados.Count(x => x.idCentroDistribuicao == 1 && x.enviado == false) && itensEstoque.Count(x => x.cancelado == false && x.enviado == false && x.reservado == true && x.idCentroDistribuicao == 2) <= itensSeparados.Count(x => x.idCentroDistribuicao == 2 && x.enviado == false))
                       orderby (c.entregaEnviarPor == "JadlogExpressa" ? 0 : c.prioridadeEnvio == null ? 9999 : c.prioridadeEnvio),
                       (
                       notas.OrderByDescending(x => x.idNotaFiscal).FirstOrDefault() != null ?
                       (notas.OrderByDescending(x => x.idNotaFiscal).FirstOrDefault().dataHora < c.prazoFinalPedido ?
                       notas.OrderByDescending(x => x.idNotaFiscal).FirstOrDefault().dataHora :
                       c.prazoMaximoPostagemAtualizado != null ?
                       c.prazoMaximoPostagemAtualizado : c.prazoFinalPedido) :
                       c.prazoMaximoPostagemAtualizado != null ?
                       c.prazoMaximoPostagemAtualizado :
                       c.prazoFinalPedido == null ? DateTime.Now : c.prazoFinalPedido),
                       c.dataHoraDoPedido
                       select c).ToList();*/
        return pedidosGeral;
    }

    public static List<tbPedido> verificaPedidosCompletos(int idCentroDistribuicao)
    {
        return verificaPedidosCompletos(idCentroDistribuicao, false);
    }

    public static List<tbPedido> verificaPedidosCompletos(int idCentroDistribuicao, bool unicos)
    {
        var data = new dbCommerceDataContext();
        //var pedidos = (from c in data.tbPedidos where c.envioLiberado == true && c.statusDoPedido == 11 orderby (c.entregaEnviarPor == "JadlogExpressa" ? 0 : c.prioridadeEnvio == null ? 9999 : c.prioridadeEnvio), (c.prazoFinalPedido != null ? c.prazoFinalPedido : c.marketplace == true ? DateTime.Now : DateTime.Now.AddDays(999)), c.dataHoraDoPedido select c).ToList();
        //return pedidos;
        var prioridadeEnvio = DateTime.Now.AddDays(2);
        var prioridadeEnvioGrande = DateTime.Now.AddDays(50);
        var pedidos = (from c in data.tbPedidos
                       join d in data.tbItemPedidoEstoques on c.pedidoId equals d.tbItensPedido.pedidoId into itensEstoque
                       join e in data.tbNotaFiscals on c.pedidoId equals e.idPedido into notas
                       where c.statusDoPedido == 11 &&
                       itensEstoque.Count(x => x.cancelado == false && x.enviado == false && x.reservado == false && (x.reservado == false && x.autorizarParcial == true) == false) == 0
                       && itensEstoque.Count(x => x.cancelado == false && x.enviado == false) > 0
                       orderby (c.entregaEnviarPor == "JadlogExpressa" ? 0 : c.prioridadeEnvio == null ? 9999 : c.prioridadeEnvio),
                       c.prazoMaximoPostagemAtualizado > prioridadeEnvio ? prioridadeEnvioGrande : c.prazoMaximoPostagemAtualizado,
                       c.dataHoraDoPedido
                       select c).ToList();
        if (idCentroDistribuicao == 1 && unicos == false) pedidos = pedidos.Where(x => x.envioLiberado == true && x.itensPendenteEnvioCd1 > 0).ToList();
        if(idCentroDistribuicao == 1 && unicos) pedidos = pedidos.Where(x => x.envioLiberado == true && x.itensPendetenEnvioCd2 == 0 && x.itensPendenteEnvioCd1 > 0).ToList();
        if (idCentroDistribuicao == 2) pedidos = pedidos.Where(x => x.envioLiberadoCd2 == true | (x.itensPendetenEnvioCd2 > 0 && x.separadoCd2 == true) ).ToList();
        if (idCentroDistribuicao == 3)
        {
            pedidos = (from c in data.tbPedidos
                       join d in data.tbItemPedidoEstoques on c.pedidoId equals d.tbItensPedido.pedidoId into itensEstoque
                       join e in data.tbNotaFiscals on c.pedidoId equals e.idPedido into notas
                       where c.statusDoPedido == 11 
                       && itensEstoque.Count(x => x.cancelado == false && x.enviado == false && x.idCentroDistribuicao == 3) > 0 && c.envioLiberadoCd3 == true
                       orderby (c.entregaEnviarPor == "JadlogExpressa" ? 0 : c.prioridadeEnvio == null ? 9999 : c.prioridadeEnvio),
                                (c.prazoMaximoPostagemAtualizado != null ?
                                c.prazoMaximoPostagemAtualizado :
                                c.prazoFinalPedido == null ? DateTime.Now : c.prazoFinalPedido),
                       c.dataHoraDoPedido
                       select c).ToList();
        }
        if (idCentroDistribuicao == 4)
        {
            pedidos = (from c in data.tbPedidos
                       join d in data.tbItemPedidoEstoques on c.pedidoId equals d.tbItensPedido.pedidoId into itensEstoque
                       join e in data.tbNotaFiscals on c.pedidoId equals e.idPedido into notas
                       where c.statusDoPedido == 11
                       && itensEstoque.Count(x => x.cancelado == false && x.enviado == false && x.idCentroDistribuicao == 4) > 0 && c.envioLiberadoCd4 == true
                       orderby (c.entregaEnviarPor == "JadlogExpressa" ? 0 : c.prioridadeEnvio == null ? 9999 : c.prioridadeEnvio),
                                (c.prazoMaximoPostagemAtualizado != null ?
                                c.prazoMaximoPostagemAtualizado :
                                c.prazoFinalPedido == null ? DateTime.Now : c.prazoFinalPedido),
                       c.dataHoraDoPedido
                       select c).ToList();
        }
        return pedidos;
    }


    public static List<int> verificaIdsPedidosCompletos(bool? separado, bool? separacaoIniciada)
    {
        return verificaIdsPedidosCompletos(1, separado, separacaoIniciada, new List<int>());
    }


    public static List<int> verificaIdsPedidosCompletosSeparadosCd4e5(int idCentroDistribuicao, bool? separado, bool? separacaoIniciada, List<int> enderecosSeparacao, int idComputadorExpedicao)
    {
        return verificaIdsPedidosCompletos(idCentroDistribuicao, separado, separacaoIniciada, enderecosSeparacao, true, idComputadorExpedicao);
    }

    public static List<int> verificaIdsPedidosCompletosSeparadosCd4e5(int idCentroDistribuicao, bool? separado, bool? separacaoIniciada, List<int> enderecosSeparacao)
    {
        return verificaIdsPedidosCompletos(idCentroDistribuicao, separado, separacaoIniciada, enderecosSeparacao, true, 0);
    }

    public static List<int> verificaIdsPedidosCompletos(int idCentroDistribuicao, bool? separado, bool? separacaoIniciada, List<int> enderecosSeparacao)
    {
        return verificaIdsPedidosCompletos(idCentroDistribuicao, separado, separacaoIniciada, enderecosSeparacao, null, 0);
    }

    public static List<int> verificaIdsPedidosCompletos(int idCentroDistribuicao, bool? separado, bool? separacaoIniciada, List<int> enderecosSeparacao, bool? separadoCd2, int idComputadorExpedicao)
    {
        var data = new dbCommerceDataContext();
        //var pedidos = (from c in data.tbPedidos where c.envioLiberado == true && c.statusDoPedido == 11 orderby (c.entregaEnviarPor == "JadlogExpressa" ? 0 : c.prioridadeEnvio == null ? 9999 : c.prioridadeEnvio), (c.prazoFinalPedido != null ? c.prazoFinalPedido : c.marketplace == true ? DateTime.Now : DateTime.Now.AddDays(999)), c.dataHoraDoPedido select c).ToList();
        //return pedidos;
        if (idCentroDistribuicao == 1)
        {
            var pedidosCd1 = (from c in data.tbPedidos
                              join d in data.tbItemPedidoEstoques on c.pedidoId equals d.tbItensPedido.pedidoId into itensEstoque
                              where c.statusDoPedido == 11 && c.envioLiberado == true && c.itensPendenteEnvioCd1 > 0
                              orderby (c.entregaEnviarPor == "JadlogExpressa" ? 0 : c.prioridadeEnvio == null ? 9999 : c.prioridadeEnvio),
                                (c.prazoMaximoPostagemAtualizado != null ?
                                c.prazoMaximoPostagemAtualizado :
                                c.prazoFinalPedido == null ? DateTime.Now : c.prazoFinalPedido),
                                c.dataHoraDoPedido
                              select c);
            if (separado != null)
            {
                if (separado == true) {
                    pedidosCd1 = pedidosCd1.Where(x => x.separado);
                    if(idComputadorExpedicao == 7)
                    {
                        if(pedidosCd1.Any(x => x.itensPendentesEnvioCd5 > 0))
                        {
                            pedidosCd1 = pedidosCd1.Where(x => x.itensPendentesEnvioCd5 > 0);
                        }
                    }
                }
                if (separado == false) pedidosCd1 = pedidosCd1.Where(x => x.separado == false && x.separadoCd1 == false);
            }

            if (separacaoIniciada != null)
            {
                if (separacaoIniciada == true) pedidosCd1 = pedidosCd1.Where(x => x.idUsuarioSeparacao != null);
                if (separacaoIniciada == false) pedidosCd1 = pedidosCd1.Where(x => x.idUsuarioSeparacao == null);
            }

            return pedidosCd1.Select(x => x.pedidoId).ToList();
        }
        else if (idCentroDistribuicao == 2)
        {
            var pedidosCd1 = (from c in data.tbPedidos
                              join d in data.tbItemPedidoEstoques on c.pedidoId equals d.tbItensPedido.pedidoId into itensEstoque
                              where c.statusDoPedido == 11 && c.envioLiberado == true && c.itensPendetenEnvioCd2 > 0 && c.envioLiberadoCd2 == true
                              select c);
            if (separado != null)
            {
                if (separado == true) pedidosCd1 = pedidosCd1.Where(x => x.separado);
                if (separado == false) pedidosCd1 = pedidosCd1.Where(x => x.separado == false);
            }

            if (separacaoIniciada != null)
            {
                if (separacaoIniciada == true) pedidosCd1 = pedidosCd1.Where(x => x.idUsuarioSeparacao != null);
                if (separacaoIniciada == false) pedidosCd1 = pedidosCd1.Where(x => x.idUsuarioSeparacao == null);
            }

            return pedidosCd1.Select(x => x.pedidoId).ToList();
        }
        else if (idCentroDistribuicao == 4)
        {
            var computadorExpedicao = (from c in data.tbComputadorExpedicaos where c.numeroComputador == idComputadorExpedicao select c).FirstOrDefault();
            var pedidosCd1 = (from c in data.tbPedidos
                              join d in data.tbItemPedidoEstoques on c.pedidoId equals d.tbItensPedido.pedidoId into itensEstoque
                              where c.statusDoPedido == 11 && c.itensPendentesEnvioCd4 > 0 && c.envioLiberadoCd4 == true 
                              && itensEstoque.Count(x => x.idCentroDistribuicao == idCentroDistribuicao && x.enviado == false) > 0
                              orderby
                              (c.itensPendentesEnvioCd5 > 0 && (c.prioridadeEnvio ?? 99) > 0 && c.separadoCd4 == true ? 0 : 1),
                              (c.entregaEnviarPor == "JadlogExpressa" ? 0 : c.prioridadeEnvio == null ? 9999 : c.prioridadeEnvio),
                                (c.prazoMaximoPostagemAtualizado != null ?
                                c.prazoMaximoPostagemAtualizado :
                                c.prazoFinalPedido == null ? DateTime.Now : c.prazoFinalPedido),
                                c.dataHoraDoPedido
                              select c);

            if (separado != null)
            {
                if (separado == true)
                {
                    pedidosCd1 = pedidosCd1.Where(x => x.separadoCd4 == true && ((x.itensPendentesEnvioCd5 ?? 0) == 0 ? true : (x.separadoCd5 == true)) == true);
                    if (computadorExpedicao != null)
                    {
                        if (computadorExpedicao.embalarCdMisto)
                        {
                            if (pedidosCd1.Any(x => x.itensPendentesEnvioCd5 > 0))
                            {
                                pedidosCd1 = pedidosCd1.Where(x => x.itensPendentesEnvioCd5 > 0);
                            }
                        }
                    }
                }
                if (separado == false) pedidosCd1 = pedidosCd1.Where(x => x.separadoCd4 == false);
            }

            if (separacaoIniciada != null)
            {
                if (separacaoIniciada == true) {
                    pedidosCd1 = pedidosCd1.Where(x => x.tbPedidoEnvios.Count(z => z.idCentroDeDistribuicao == 4 && z.dataFimSeparacao != null) > 0);
                }
                if (separacaoIniciada == false) pedidosCd1 = pedidosCd1.Where(x => x.tbPedidoEnvios.Count(z => z.idCentroDeDistribuicao == 4 && z.dataFimEmbalagem == null) == 0);
            }
            if (computadorExpedicao != null)
            {
                var listaSeparacaoIds = new List<int>();
                var enderecosSeparacaoComputador = (from c in data.tbComputadorExpedicaoEnderecoSeparacaos where c.idComputador == computadorExpedicao.idComputadorExpedicao select (c.idEnderecoSeparacao ?? 0)).ToList();
                if (enderecosSeparacao.Count() > 0)
                {
                    listaSeparacaoIds = enderecosSeparacao.ToList();
                }
                else if (enderecosSeparacaoComputador.Count() > 0)
                {
                    listaSeparacaoIds = enderecosSeparacaoComputador.ToList();
                }
                if (listaSeparacaoIds.Count() > 0)
                {
                    var idsPedidos = pedidosCd1.Select(x => x.pedidoId).ToList();
                    var idsPedidosFiltrados = (from c in data.tbPedidoEnvios where listaSeparacaoIds.Contains((c.idEnderecoSeparacao ?? 0)) && idsPedidos.Contains(c.idPedido) select c.idPedido).ToList();
                    var pedidosFiltrados = pedidosCd1.Where(x => idsPedidosFiltrados.Contains(x.pedidoId));
                    if (pedidosFiltrados.Count() > 0)
                    {
                        pedidosCd1 = pedidosFiltrados;
                    }
                }
            }
            if (separadoCd2 != null)
            {
                if (separadoCd2 == true ) pedidosCd1 = pedidosCd1.Where(x => (x.separadoCd5 == true | x.itensPendentesEnvioCd5 == 0));
                if (separadoCd2 == false) pedidosCd1 = pedidosCd1.Where(x => (x.separadoCd5 == false | x.itensPendentesEnvioCd5 == 0));
            }

            return pedidosCd1.Select(x => x.pedidoId).ToList();
        }
        else if (idCentroDistribuicao == 5)
        {
            var pedidosCd1 = (from c in data.tbPedidos
                              where c.statusDoPedido == 11 && c.itensPendentesEnvioCd5 > 0 && c.envioLiberadoCd5 == true
                              orderby
                              //(c.itensPendentesEnvioCd4 > 0 && (c.prioridadeEnvio ?? 99) > 0 ? 0 : 1),
                              (c.entregaEnviarPor == "JadlogExpressa" ? 0 : c.prioridadeEnvio == null ? 9999 : c.prioridadeEnvio),
                                (c.prazoMaximoPostagemAtualizado != null ?
                                c.prazoMaximoPostagemAtualizado :
                                c.prazoFinalPedido == null ? DateTime.Now : c.prazoFinalPedido),
                                c.dataHoraDoPedido
                              select c).ToList();
            if (separado != null)
            {
                if (separado == true) pedidosCd1 = pedidosCd1.Where(x => x.separadoCd5 == true).ToList();
                if (separado == false) pedidosCd1 = pedidosCd1.Where(x => x.separadoCd5 == false).ToList();
            }

            if (separacaoIniciada != null)
            {
                if (separacaoIniciada == true)
                {
                    pedidosCd1 = pedidosCd1.Where(x => x.tbPedidoEnvios.Count(z => (z.idCentroDeDistribuicao == 4 | z.idCentroDeDistribuicao == 5) && z.dataInicioSeparacaoCd2 != null  && z.dataFimSeparacaoCd2 == null) > 0).ToList();
                }
                if (separacaoIniciada == false) pedidosCd1 = pedidosCd1.Where(x => x.tbPedidoEnvios.Count(z => (z.idCentroDeDistribuicao == 4 | z.idCentroDeDistribuicao == 5) && z.dataInicioSeparacaoCd2 != null && z.dataFimSeparacaoCd2 == null) == 0).ToList();
            }
            return pedidosCd1.Select(x => x.pedidoId).ToList();
        }
        else
        {

            var pedidos = (from c in data.tbPedidos
                           join d in data.tbItemPedidoEstoques on c.pedidoId equals d.tbItensPedido.pedidoId into itensEstoque
                           join e in data.tbNotaFiscals on c.pedidoId equals e.idPedido into notas
                           where c.statusDoPedido == 11 &&
                           itensEstoque.Count(x => x.cancelado == false && x.enviado == false && x.reservado == false && (x.reservado == false && x.autorizarParcial == true) == false && x.idCentroDistribuicao == idCentroDistribuicao) == 0
                           && itensEstoque.Count(x => x.cancelado == false && x.enviado == false && x.idCentroDistribuicao == idCentroDistribuicao) > 0
                           orderby 
                                (c.entregaEnviarPor == "JadlogExpressa" ? 0 : c.prioridadeEnvio == null ? 9999 : c.prioridadeEnvio),
                                (c.prazoMaximoPostagemAtualizado != null ?
                                c.prazoMaximoPostagemAtualizado :
                                c.prazoFinalPedido == null ? DateTime.Now : c.prazoFinalPedido),
                           c.dataHoraDoPedido
                           select c);
            if (idCentroDistribuicao == 1) pedidos = pedidos.Where(x => x.envioLiberado == true);
            if (separado != null)
            {
                if (separado == true) pedidos = pedidos.Where(x => x.separado);
                if (separado == false) pedidos = pedidos.Where(x => x.separado == false);
            }

            if (separacaoIniciada != null)
            {
                if (separacaoIniciada == true) pedidos = pedidos.Where(x => x.idUsuarioSeparacao != null);
                if (separacaoIniciada == false) pedidos = pedidos.Where(x => x.idUsuarioSeparacao == null);
            }
            return pedidos.Select(x => x.pedidoId).ToList();
        }
    }
    public static List<int> verificaIdsPedidosCompletosCd2SeparadosCd1()
    {
        var data = new dbCommerceDataContext();
        var pedidos = (from c in data.tbPedidos
                       join d in data.tbItemPedidoEstoques on c.pedidoId equals d.tbItensPedido.pedidoId into itensEstoque
                       join e in data.tbNotaFiscals on c.pedidoId equals e.idPedido into notas
                       join f in data.tbProdutoEstoques on c.pedidoId equals f.pedidoId into produtosSeparados
                       join g in data.tbPedidoEnvios on c.pedidoId equals g.idPedido into envios
                       where c.statusDoPedido == 11 && c.envioLiberado == true &&
                       envios.Count(x => x.idUsuarioSeparacaoCd2 != null && x.dataFimSeparacaoCd2 == null && x.idCentroDeDistribuicao < 3) == 0 &&
                       itensEstoque.Count(x => x.cancelado == false && x.enviado == false && x.reservado == false && (x.reservado == false && x.autorizarParcial == true) == false && x.idCentroDistribuicao == 2) == 0
                       && itensEstoque.Count(x => x.cancelado == false && x.enviado == false && x.idCentroDistribuicao == 2) > 0 &&
                       (itensEstoque.Count(x => x.cancelado == false && x.enviado == false && x.reservado == true && x.idCentroDistribuicao == 1) <= produtosSeparados.Count(x => x.idCentroDistribuicao == 1 && x.enviado == false) && itensEstoque.Count(x => x.cancelado == false && x.enviado == false && x.reservado == true && x.idCentroDistribuicao == 2) > produtosSeparados.Count(x => x.idCentroDistribuicao == 2 && x.enviado == false))
                       orderby (c.entregaEnviarPor == "JadlogExpressa" ? 0 : c.prioridadeEnvio == null ? 9999 : c.prioridadeEnvio),
                                (c.prazoMaximoPostagemAtualizado != null ?
                                c.prazoMaximoPostagemAtualizado :
                                c.prazoFinalPedido == null ? DateTime.Now : c.prazoFinalPedido),
                       c.dataHoraDoPedido
                       select c);
        
        return pedidos.Select(x => x.pedidoId).ToList();
    }
    public static List<int> verificaIdsPedidosCompletosCd3()
    {
        var data = new dbCommerceDataContext();
        var pedidos = (from c in data.tbPedidos
                       join d in data.tbItemPedidoEstoques on c.pedidoId equals d.tbItensPedido.pedidoId into itensEstoque
                       join g in data.tbPedidoEnvios on c.pedidoId equals g.idPedido into envios
                       where c.statusDoPedido == 11
                           && itensEstoque.Count(x => x.cancelado == false && x.enviado == false && x.idCentroDistribuicao == 3) > 0 && c.envioLiberadoCd3 == true && c.separadoCd3 == false
                       orderby (c.entregaEnviarPor == "JadlogExpressa" ? 0 : c.prioridadeEnvio == null ? 9999 : c.prioridadeEnvio),
                                (c.prazoMaximoPostagemAtualizado != null ?
                                c.prazoMaximoPostagemAtualizado :
                                c.prazoFinalPedido == null ? DateTime.Now : c.prazoFinalPedido),
                       c.dataHoraDoPedido
                       select c);

        return pedidos.Select(x => x.pedidoId).ToList();
    }

    public static int contaPedidosCompletos(bool? separado, bool? separacaoIniciada)
    {
        return contaPedidosCompletos(1, separado, separacaoIniciada);
    }
    public static int contaPedidosCompletos(int idCentroDistribuicao, bool? separado, bool? separacaoIniciada)
    {
        var data = new dbCommerceDataContext();
        if (idCentroDistribuicao == 1)
        {
            var pedidosCd1 = (from c in data.tbPedidos
                              join d in data.tbItemPedidoEstoques on c.pedidoId equals d.tbItensPedido.pedidoId into itensEstoque
                              where c.statusDoPedido == 11 && c.envioLiberado == true && c.itensPendenteEnvioCd1 > 0 &&
                              itensEstoque.Count(x => x.idCentroDistribuicao == 0 | x.idCentroDistribuicao == 1) > 0
                              select c);
            if (separado != null)
            {
                if (separado == true) pedidosCd1 = pedidosCd1.Where(x => x.separado);
                if (separado == false) pedidosCd1 = pedidosCd1.Where(x => x.separado == false);
            }

            if (separacaoIniciada != null)
            {
                if (separacaoIniciada == true) pedidosCd1 = pedidosCd1.Where(x => x.idUsuarioSeparacao != null);
                if (separacaoIniciada == false) pedidosCd1 = pedidosCd1.Where(x => x.idUsuarioSeparacao == null);
            }
            return pedidosCd1.Count();
        }
        else if (idCentroDistribuicao == 2)
        {
            var pedidosCd1 = (from c in data.tbPedidos
                              join d in data.tbItemPedidoEstoques on c.pedidoId equals d.tbItensPedido.pedidoId into itensEstoque
                              where c.statusDoPedido == 11 && c.envioLiberado == true && c.itensPendetenEnvioCd2 > 0 && c.envioLiberadoCd2 == true
                              && itensEstoque.Count(x => x.idCentroDistribuicao == 2) > 0
                              select c);
            if (separado != null)
            {
                if (separado == true) pedidosCd1 = pedidosCd1.Where(x => x.separado);
                if (separado == false) pedidosCd1 = pedidosCd1.Where(x => x.separado == false);
            }

            if (separacaoIniciada != null)
            {
                if (separacaoIniciada == true) pedidosCd1 = pedidosCd1.Where(x => x.idUsuarioSeparacao != null);
                if (separacaoIniciada == false) pedidosCd1 = pedidosCd1.Where(x => x.idUsuarioSeparacao == null);
            }

            return pedidosCd1.Count();
        }
        else if (idCentroDistribuicao == 3)
        {
            var pedidosCd1 = (from c in data.tbPedidos
                              join d in data.tbItemPedidoEstoques on c.pedidoId equals d.tbItensPedido.pedidoId into itensEstoque
                              where c.statusDoPedido == 11 && c.itensPendetenEnvioCd2 > 0 && c.envioLiberadoCd2 == true
                              && itensEstoque.Count(x => x.idCentroDistribuicao == 3) > 0
                              select c);
            if (separado != null)
            {
                if (separado == true) pedidosCd1 = pedidosCd1.Where(x => x.separado);
                if (separado == false) pedidosCd1 = pedidosCd1.Where(x => x.separado == false);
            }

            if (separacaoIniciada != null)
            {
                if (separacaoIniciada == true) pedidosCd1 = pedidosCd1.Where(x => x.idUsuarioSeparacao != null);
                if (separacaoIniciada == false) pedidosCd1 = pedidosCd1.Where(x => x.idUsuarioSeparacao == null);
            }

            return pedidosCd1.Count();
        }
        else if (idCentroDistribuicao == 4)
        {
            var pedidosCd1 = (from c in data.tbPedidos
                              join d in data.tbItemPedidoEstoques on c.pedidoId equals d.tbItensPedido.pedidoId into itensEstoque
                              where c.statusDoPedido == 11 && c.itensPendentesEnvioCd4 > 0 && c.envioLiberadoCd4 == true
                              && itensEstoque.Count(x => x.idCentroDistribuicao == 4) > 0
                              select c);
            if (separado != null)
            {
                if (separado == true) pedidosCd1 = pedidosCd1.Where(x => x.separadoCd4 == true);
                if (separado == false) pedidosCd1 = pedidosCd1.Where(x => x.separadoCd4 == false);
            }

            if (separacaoIniciada != null)
            {
                if (separacaoIniciada == true)
                {
                    pedidosCd1 = pedidosCd1.Where(x => x.tbPedidoEnvios.Count(z => z.idCentroDeDistribuicao == 4 && z.dataFimSeparacao == null) > 0);
                }
                if (separacaoIniciada == false) pedidosCd1 = pedidosCd1.Where(x => x.tbPedidoEnvios.Count(z => z.idCentroDeDistribuicao == 4 && z.dataFimSeparacao == null) == 0);
            }

            return pedidosCd1.Count();
        }
        else if (idCentroDistribuicao == 5)
        {
            var pedidosCd1 = (from c in data.tbPedidos
                              join d in data.tbItemPedidoEstoques on c.pedidoId equals d.tbItensPedido.pedidoId into itensEstoque
                              where c.statusDoPedido == 11 && c.itensPendentesEnvioCd5 > 0 && c.envioLiberadoCd5 == true
                              && itensEstoque.Count(x => x.idCentroDistribuicao == 5) > 0
                              select c);
            if (separado != null)
            {
                if (separado == true) pedidosCd1 = pedidosCd1.Where(x => x.separadoCd5 == true);
                if (separado == false) pedidosCd1 = pedidosCd1.Where(x => x.separadoCd5 == false);
            }

            if (separacaoIniciada != null)
            {
                if (separacaoIniciada == true)
                {
                    pedidosCd1 = pedidosCd1.Where(x => x.tbPedidoEnvios.Count(z => (z.idCentroDeDistribuicao == 4 | z.idCentroDeDistribuicao == 5) && z.dataInicioSeparacaoCd2 != null && z.dataFimSeparacaoCd2 == null) > 0);
                }
                if (separacaoIniciada == false) pedidosCd1 = pedidosCd1.Where(x => x.tbPedidoEnvios.Count(z => (z.idCentroDeDistribuicao == 4 | z.idCentroDeDistribuicao == 5) && z.dataInicioSeparacaoCd2 != null && z.dataFimSeparacaoCd2 == null) == 0);
            }

            return pedidosCd1.Count();
        }
        return 0;
    }


    public static List<tbPedido> verificaChecaPedidosCompletos()
    {
        var pedidosCompletos = new List<int>();

        var pedidosDc = new dbCommerceDataContext();
        var itensPedidoDc = new dbCommerceDataContext();
        var produtosDc = new dbCommerceDataContext();
        var pedidosGeral = pedidosDc.admin_itensPedidoParaPedidoCompleto().ToList();
        var pedidosSeparacao = pedidosGeral.Select(x => x.pedidoId).Distinct();

        foreach (var pedidoSeparacao in pedidosSeparacao)
        {
            var listaProdutos = new List<produtoQuantidade>();
            var itensPedido = pedidosGeral.Where(x => x.pedidoId == pedidoSeparacao);
            int totalProdutosParcial = 0;
            int totalProdutosParcialEntregue = 0;
            int totalProdutosNaoParcial = 0;
            int totalProdutosNaoParcialEntregues = 0;
            bool totalCompleto = true;

            foreach (var itemPedido in itensPedido)
            {
                if (totalCompleto)
                {
                    bool enviado = itemPedido.enviado;
                    bool parcial = itemPedido.parcial == 1;

                    if (parcial | enviado == false)
                    {
                        if (parcial)
                        {
                            totalProdutosParcial++;
                        }
                        else
                        {
                            totalProdutosNaoParcial++;
                        }


                        int totalReservado = itemPedido.reservas;
                        int totalPedidosFornecedor = itemPedido.pedidosFornecedor;
                        int totalFaltando = itemPedido.faltandoNaoEntregues;

                        if ((totalReservado + totalPedidosFornecedor - totalFaltando) < itemPedido.itemQuantidade)
                        {
                            var segundoEnvio =
                                (from c in pedidosDc.tbItensPedidos
                                 where c.pedidoId == itemPedido.pedidoId && c.enviado == true
                                 select c).Any();
                            if (!parcial)
                            {
                                totalCompleto = false;
                            }
                            else
                            {
                                if (segundoEnvio) totalCompleto = false;
                            }
                        }
                        else
                        {
                            if (parcial)
                            {
                                totalProdutosParcialEntregue++;
                            }
                            else
                            {
                                totalProdutosNaoParcialEntregues++;
                            }
                        }
                    }
                }
            }

            if (totalCompleto)
            {
                if (totalProdutosNaoParcial > 0)
                {
                    if (totalProdutosNaoParcial <= totalProdutosNaoParcialEntregues)
                    {
                        pedidosCompletos.Add(pedidoSeparacao);
                    }
                }
                else
                {
                    if (totalProdutosParcial <= totalProdutosParcialEntregue)
                    {
                        pedidosCompletos.Add(pedidoSeparacao);
                    }
                }
            }
        }

        var pedidos = (from c in pedidosDc.tbPedidos where pedidosCompletos.Contains(c.pedidoId) orderby (c.entregaEnviarPor == "JadlogExpressa" ? 0 : c.prioridadeEnvio == null ? 9999 : c.prioridadeEnvio), (c.prazoFinalPedido != null ? c.prazoFinalPedido : c.marketplace == true ? DateTime.Now : DateTime.Now.AddDays(999)), c.dataHoraDoPedido select c).ToList();
        foreach (var pedido in pedidos)
        {
            pedido.envioLiberado = true;
        }
        var pedidosNaoCompletos = (from c in pedidosDc.tbPedidos
                                   where c.envioLiberado == true && !pedidos.Select(x => x.pedidoId).Contains(c.pedidoId)
                                   select c);
        foreach (var completo in pedidosNaoCompletos)
        {
            completo.envioLiberado = false;
        }
        pedidosDc.SubmitChanges();
        return pedidos;
    }

    /*
    public static List<tbPedido> verificaPedidosCompletos()
    {

        var pedidosCompletos = new List<int>();

        var pedidosDc = new dbCommerceDataContext();
        var itensPedidoDc = new dbCommerceDataContext();
        var produtosDc = new dbCommerceDataContext();
        var pedidosSeparacao = pedidosDc.admin_pedidosComItensEntregues().Select(x => x.pedidoId);

        foreach (var pedidoSeparacao in pedidosSeparacao)
        {
            var listaProdutos = new List<produtoQuantidade>();
            var itensPedido = (from c in itensPedidoDc.tbItensPedidos where c.pedidoId == pedidoSeparacao select c);
            int totalProdutosParcial = 0;
            int totalProdutosParcialEntregue = 0;
            int totalProdutosNaoParcial = 0;
            int totalProdutosNaoParcialEntregues = 0;
            bool totalCompleto = true;

            var itensFaltandoGeral = (from c in produtosDc.tbPedidoProdutoFaltandos
                                 where
                                     c.pedidoId == pedidoSeparacao &&
                                     c.entregue == false && c.itemPedidoId == null
                                 select c).Any();

            if (itensFaltandoGeral) totalCompleto = false;

            foreach (var itemPedido in itensPedido)
            {
                if (totalCompleto)
                {
                    bool cancelado = itemPedido.cancelado == null ? false : (bool) itemPedido.cancelado;
                    bool enviado = itemPedido.enviado == null ? false : (bool)itemPedido.enviado;
                    if (!cancelado)
                    {
                        var listaProdutosIds = new List<int>();
                        var produtosFilho =
                            (from c in produtosDc.tbProdutoRelacionados
                                where c.idProdutoPai == itemPedido.produtoId
                                select c);
                        if (produtosFilho.Any())
                        {
                            foreach (var relacionado in produtosFilho)
                            {
                                listaProdutosIds.Add(relacionado.idProdutoFilho);
                            }
                        }
                        else
                        {
                            listaProdutosIds.Add(itemPedido.produtoId);
                        }

                        foreach (var produtosId in listaProdutosIds)
                        {
                            var parcial = (from c in pedidosDc.tbItensPedidoEnvioParcials
                                           where
                                               c.idPedido == pedidoSeparacao && c.idProduto == produtosId &&
                                               c.idItemPedido == itemPedido.itemPedidoId && c.enviado == false
                                           select c).Any();
                            if (parcial == true | enviado == false)
                            {
                                if (parcial)
                                {
                                    totalProdutosParcial++;
                                }
                                else
                                {
                                    totalProdutosNaoParcial++;
                                }


                                int totalReservado = 0;
                                var reservas = (from c in produtosDc.tbProdutoReservaEstoques
                                    where
                                        c.idProduto == produtosId && c.idPedido == pedidoSeparacao &&
                                        c.idItemPedido == itemPedido.itemPedidoId
                                    select c);
                                if (reservas.Any())
                                {
                                    totalReservado = reservas.Sum(x => x.quantidade);
                                }

                                int totalPedidosFornecedor = 0;
                                var pedidosFornecedor = (from c in pedidosDc.tbPedidoFornecedorItems
                                    where
                                        c.idProduto == produtosId && c.idPedido == pedidoSeparacao && c.entregue == true &&
                                        c.idItemPedido == itemPedido.itemPedidoId
                                    select c);
                                if (pedidosFornecedor.Any())
                                {
                                    totalPedidosFornecedor = pedidosFornecedor.Sum(x => x.quantidade);
                                }

                                var itensFaltando = (from c in produtosDc.tbPedidoProdutoFaltandos
                                    where
                                        c.pedidoId == pedidoSeparacao && c.produtoId == produtosId &&
                                        c.entregue == false && c.itemPedidoId == itemPedido.itemPedidoId
                                    select c).Any();
                                if (itensFaltando)
                                {
                                    totalCompleto = false;
                                }

                                if (pedidosFornecedor.Any())
                                {
                                    totalPedidosFornecedor = pedidosFornecedor.Sum(x => x.quantidade);
                                }

                                if ((totalReservado + totalPedidosFornecedor) < itemPedido.itemQuantidade)
                                {
                                    if (!parcial)
                                    {
                                        totalCompleto = false;
                                    }
                                }
                                else
                                {
                                    if (parcial)
                                    {
                                        totalProdutosParcialEntregue++;
                                    }
                                    else
                                    {
                                        totalProdutosNaoParcialEntregues++;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if (totalCompleto)
            {
                if (totalProdutosNaoParcial > 0)
                {
                    if (totalProdutosNaoParcial <= totalProdutosNaoParcialEntregues)
                    {
                        pedidosCompletos.Add(pedidoSeparacao);
                    }
                }
                else
                {
                    if (totalProdutosParcial <= totalProdutosParcialEntregue)
                    {
                        pedidosCompletos.Add(pedidoSeparacao);
                    }
                }
            }
        }


        var pedidos = (from c in pedidosDc.tbPedidos where pedidosCompletos.Contains(c.pedidoId) orderby (c.prioridadeEnvio == null ? 9999 : c.prioridadeEnvio), (c.prazoFinalPedido == null ? DateTime.Now.AddDays(999) : c.prazoFinalPedido), c.dataHoraDoPedido select c).ToList();
        return pedidos;
    }*/


    private class produtoQuantidade
    {
        public int produtoId { get; set; }
        public int quantidade { get; set; }
    }


    public static void recalculaTotaisPedido(int pedidoId)
    {
        var pedidoDc = new dbCommerceDataContext();
        var pedido = (from c in pedidoDc.tbPedidos where c.pedidoId == pedidoId select c).First();

        double pesoAnterior = (double)pedido.pesoDoPedido;
        decimal valorCobradoAnterior = (decimal)pedido.valorCobrado;
        decimal valorDoDescontoDoPagamentoAnterior = pedido.valorDoDescontoDoPagamento ?? 0;
        decimal valorDoFreteAnterior = (decimal)pedido.valorDoFrete;
        decimal valorDosItensAnterior = (decimal)pedido.valorDosItens;
        decimal valorTotalGeralAnterior = (decimal)pedido.valorTotalGeral;

        double peso = 0;
        decimal valorCobrado = 0;
        decimal valorDoDescontoDoPagamento = 0;
        decimal valorDoFrete = Convert.ToDecimal(pedido.valorDoFrete);
        decimal valorDosItens = 0;
        decimal valorTotalGeral = 0;

        var itensPedidoDc = new dbCommerceDataContext();
        var itensPedido = (from c in itensPedidoDc.tbItensPedidos where c.pedidoId == pedidoId select c);
        foreach (var itemPedido in itensPedido)
        {
            bool cancelado = itemPedido.cancelado == null ? false : (bool)itemPedido.cancelado;
            bool brinde = itemPedido.brinde == null ? false : (bool)itemPedido.brinde;
            if (!cancelado && itemPedido.itemQuantidade > 0 && !brinde)
            {
                var produtoDc = new dbCommerceDataContext();
                var produto = (from c in produtoDc.tbProdutos where c.produtoId == itemPedido.produtoId select c).First();
                peso += (produto.produtoPeso * Convert.ToDouble(itemPedido.itemQuantidade));
                var precoDoProduto = itemPedido.itemValor;
                valorDosItens += ((decimal)precoDoProduto * itemPedido.itemQuantidade);
            }
        }

        var frete = rnFrete.montaFrete(pedido.endCep, pedidoId).FirstOrDefault();
        if (frete != null)
        {
            valorDoFrete = frete.faixaDeCepPreco;

            if (pedido.prazoDeEntrega != frete.prazo.ToString())
                pedido.prazoDeEntrega = frete.prazo.ToString();
        }

        valorTotalGeral = valorDoFrete + valorDosItens;


        var condicaoDePagamento = (from c in pedidoDc.tbCondicoesDePagamentos where c.condicaoId == pedido.condDePagamentoId select c).First();
        int numeroMaximoDeParcelas = condicaoDePagamento.numeroMaximoDeParcelas;
        int semJurosAte = condicaoDePagamento.parcelasSemJuros;
        double taxaDeJuros = condicaoDePagamento.taxaDeJuros;
        taxaDeJuros = taxaDeJuros / 100;
        decimal valorMinimoDaParcela = condicaoDePagamento.valorMinimoDaParcela;
        valorDoDescontoDoPagamento = Convert.ToDecimal(pedido.valorDoDescontoDoPagamento);
        valorCobrado = valorTotalGeral - valorDoDescontoDoPagamento;

        //int quantidadeDeParcelas = (int)pedido.numeroDeParcelas == 0 ? 1 : (int)pedido.numeroDeParcelas;
        int quantidadeDeParcelas = (pedido.numeroDeParcelas ?? 0) == 0 ? 1 : (int)pedido.numeroDeParcelas;
        decimal valorDaParcela = 0;
        if (quantidadeDeParcelas <= semJurosAte)
        {
            valorDaParcela = valorCobrado / quantidadeDeParcelas;
        }
        else
        {
            valorDaParcela = valorCobrado / quantidadeDeParcelas;
            valorDaParcela = Convert.ToDecimal((double)valorDaParcela * (Math.Pow((1 + taxaDeJuros), quantidadeDeParcelas)));
        }

        if (valorDaParcela < valorMinimoDaParcela)
        {
            while (valorDaParcela < valorMinimoDaParcela && quantidadeDeParcelas > 1)
            {
                quantidadeDeParcelas -= 1;
                if (quantidadeDeParcelas <= semJurosAte)
                {
                    valorDaParcela = valorCobrado / quantidadeDeParcelas;
                }
                else
                {
                    valorDaParcela = valorCobrado / quantidadeDeParcelas;
                    valorDaParcela = Convert.ToDecimal((double)valorDaParcela * (Math.Pow((1 + taxaDeJuros), quantidadeDeParcelas)));
                }
            }
        }

        string interacao = "";
        if (peso != pesoAnterior | valorCobrado != valorCobradoAnterior |
            valorDoDescontoDoPagamento != valorDoDescontoDoPagamentoAnterior | valorDoFrete != valorDoFreteAnterior |
            valorDosItens != valorDosItensAnterior
            | valorTotalGeral != valorTotalGeralAnterior)
        {
            interacao += "<b>Valores atualizados:</b><br>";
            if (valorCobrado != valorCobradoAnterior) interacao += "Valor de " + valorCobradoAnterior.ToString("C") + " atualizado para " + valorCobrado.ToString("C") + "<br>";
            if (valorDoFrete != valorDoFreteAnterior) interacao += "Frete de " + valorDoFreteAnterior.ToString("C") + " atualizado para " + valorDoFrete.ToString("C") + "<br>";
            if (quantidadeDeParcelas != pedido.numeroDeParcelas) interacao += "Parcelas de " + pedido.numeroDeParcelas + " atualizado para " + quantidadeDeParcelas + "<br>";
            if (valorDaParcela != pedido.valorDaParcela) interacao += "Valor da Parcela de " + Convert.ToDecimal(pedido.valorDaParcela).ToString("C") + " atualizado para " + valorDaParcela.ToString("C") + "<br>";
        }

        pedido.pesoDoPedido = peso;
        pedido.valorCobrado = valorCobrado;
        //pedido.valorDoDescontoDoPagamento = valorDoDescontoDoPagamento;
        pedido.valorDoFrete = valorDoFrete;
        pedido.valorDosItens = valorDosItens;
        pedido.valorTotalGeral = valorTotalGeral;
        pedido.numeroDeParcelas = quantidadeDeParcelas;
        pedido.valorDaParcela = valorDaParcela;

        try
        {
            pedidoDc.SubmitChanges();
        }
        catch (System.Data.Linq.ChangeConflictException ex)
        {

            foreach (System.Data.Linq.ObjectChangeConflict objConflict in pedidoDc.ChangeConflicts)
            {
                foreach (System.Data.Linq.MemberChangeConflict memberConflict in objConflict.MemberConflicts)
                {
                    memberConflict.Resolve(System.Data.Linq.RefreshMode.KeepCurrentValues);
                }
            }
            pedidoDc.SubmitChanges(System.Data.Linq.ConflictMode.ContinueOnConflict);
        }

        rnInteracoes.interacaoInclui(pedidoId, interacao, "Sistema", "False");
        rnPagamento.verificaPagamentoCompleto(pedidoId);
    }

    public static void recalculaTotaisPedido(int pedidoId, bool recalcularFrete = true)
    {
        var pedidoDc = new dbCommerceDataContext();
        var pedido = (from c in pedidoDc.tbPedidos where c.pedidoId == pedidoId select c).First();

        double pesoAnterior = (double)pedido.pesoDoPedido;
        decimal valorCobradoAnterior = (decimal)pedido.valorCobrado;
        decimal valorDoDescontoDoPagamentoAnterior = pedido.valorDoDescontoDoPagamento ?? 0;
        decimal valorDoFreteAnterior = (decimal)pedido.valorDoFrete;
        decimal valorDosItensAnterior = (decimal)pedido.valorDosItens;
        decimal valorTotalGeralAnterior = (decimal)pedido.valorTotalGeral;

        double peso = 0;
        decimal valorCobrado = 0;
        decimal valorDoDescontoDoPagamento = 0;
        decimal valorDoDescontoDoCupom = 0;
        decimal valorDoFrete = Convert.ToDecimal(pedido.valorDoFrete);
        decimal valorDosItens = 0;
        decimal valorTotalGeral = 0;

        var itensPedidoDc = new dbCommerceDataContext();
        var itensPedido = (from c in itensPedidoDc.tbItensPedidos where c.pedidoId == pedidoId select c);
        foreach (var itemPedido in itensPedido)
        {
            bool cancelado = itemPedido.cancelado == null ? false : (bool)itemPedido.cancelado;
            bool brinde = itemPedido.brinde == null ? false : (bool)itemPedido.brinde;
            bool calcularTotais = itemPedido.tbItemPedidoTipoAdicao != null ? itemPedido.tbItemPedidoTipoAdicao.cobrarCliente : true;
            if (!cancelado && itemPedido.itemQuantidade > 0 && !brinde && calcularTotais)
            {
                var produtoDc = new dbCommerceDataContext();
                var produto = (from c in produtoDc.tbProdutos where c.produtoId == itemPedido.produtoId select c).First();
                peso += (produto.produtoPeso * Convert.ToDouble(itemPedido.itemQuantidade));
                var precoDoProduto = itemPedido.itemValor;
                valorDosItens += ((decimal)precoDoProduto * itemPedido.itemQuantidade);
            }
        }

        var frete = rnFrete.montaFrete(pedido.endCep, pedidoId).FirstOrDefault();
        if (frete != null && recalcularFrete)
        {
            valorDoFrete = frete.faixaDeCepPreco;

            if (pedido.prazoDeEntrega != frete.prazo.ToString())
                pedido.prazoDeEntrega = frete.prazo.ToString();
        }

        valorTotalGeral = valorDoFrete + valorDosItens;


        var condicaoDePagamento = (from c in pedidoDc.tbCondicoesDePagamentos where c.condicaoId == pedido.condDePagamentoId select c).First();
        int numeroMaximoDeParcelas = condicaoDePagamento.numeroMaximoDeParcelas;
        int semJurosAte = condicaoDePagamento.parcelasSemJuros;
        double taxaDeJuros = condicaoDePagamento.taxaDeJuros;
        taxaDeJuros = taxaDeJuros / 100;
        decimal valorMinimoDaParcela = condicaoDePagamento.valorMinimoDaParcela;
        valorDoDescontoDoPagamento = Convert.ToDecimal(pedido.valorDoDescontoDoPagamento);
        valorDoDescontoDoCupom = Convert.ToDecimal(pedido.valorDoDescontoDoCupom);
        valorCobrado = valorTotalGeral - valorDoDescontoDoPagamento - valorDoDescontoDoCupom;

        //valorCobrado = valorTotalGeral - valorDoDescontoDoPagamento;
        //int quantidadeDeParcelas = (int)pedido.numeroDeParcelas == 0 ? 1 : (int)pedido.numeroDeParcelas;
        int quantidadeDeParcelas = (pedido.numeroDeParcelas ?? 0) == 0 ? 1 : (int)pedido.numeroDeParcelas;
        decimal valorDaParcela = 0;
        if (quantidadeDeParcelas <= semJurosAte)
        {
            valorDaParcela = valorCobrado / quantidadeDeParcelas;
        }
        else
        {
            valorDaParcela = valorCobrado / quantidadeDeParcelas;
            valorDaParcela = Convert.ToDecimal((double)valorDaParcela * (Math.Pow((1 + taxaDeJuros), quantidadeDeParcelas)));
        }

        if (valorDaParcela < valorMinimoDaParcela)
        {
            while (valorDaParcela < valorMinimoDaParcela && quantidadeDeParcelas > 1)
            {
                quantidadeDeParcelas -= 1;
                if (quantidadeDeParcelas <= semJurosAte)
                {
                    valorDaParcela = valorCobrado / quantidadeDeParcelas;
                }
                else
                {
                    valorDaParcela = valorCobrado / quantidadeDeParcelas;
                    valorDaParcela = Convert.ToDecimal((double)valorDaParcela * (Math.Pow((1 + taxaDeJuros), quantidadeDeParcelas)));
                }
            }
        }

        string interacao = "";
        if (peso != pesoAnterior | valorCobrado != valorCobradoAnterior |
            valorDoDescontoDoPagamento != valorDoDescontoDoPagamentoAnterior | valorDoFrete != valorDoFreteAnterior |
            valorDosItens != valorDosItensAnterior
            | valorTotalGeral != valorTotalGeralAnterior)
        {
            interacao += "<b>Valores atualizados:</b><br>";
            if (valorCobrado != valorCobradoAnterior) interacao += "Valor de " + valorCobradoAnterior.ToString("C") + " atualizado para " + valorCobrado.ToString("C") + "<br>";
            if (valorDoFrete != valorDoFreteAnterior) interacao += "Frete de " + valorDoFreteAnterior.ToString("C") + " atualizado para " + valorDoFrete.ToString("C") + "<br>";
            if (quantidadeDeParcelas != (pedido.numeroDeParcelas ?? 1)) interacao += "Parcelas de " + (pedido.numeroDeParcelas ?? 1) + " atualizado para " + quantidadeDeParcelas + "<br>";
            if (valorDaParcela != pedido.valorDaParcela) interacao += "Valor da Parcela de " + Convert.ToDecimal(pedido.valorDaParcela).ToString("C") + " atualizado para " + valorDaParcela.ToString("C") + "<br>";
        }

        pedido.pesoDoPedido = peso;
        pedido.valorCobrado = valorCobrado;
        //pedido.valorDoDescontoDoPagamento = valorDoDescontoDoPagamento;
        pedido.valorDoFrete = valorDoFrete;
        pedido.valorDosItens = valorDosItens;
        pedido.valorTotalGeral = valorTotalGeral;
        pedido.numeroDeParcelas = quantidadeDeParcelas;
        pedido.valorDaParcela = valorDaParcela;

        try
        {
            pedidoDc.SubmitChanges();
        }
        catch (System.Data.Linq.ChangeConflictException ex)
        {

            foreach (System.Data.Linq.ObjectChangeConflict objConflict in pedidoDc.ChangeConflicts)
            {
                foreach (System.Data.Linq.MemberChangeConflict memberConflict in objConflict.MemberConflicts)
                {
                    memberConflict.Resolve(System.Data.Linq.RefreshMode.KeepCurrentValues);
                }
            }
            pedidoDc.SubmitChanges(System.Data.Linq.ConflictMode.ContinueOnConflict);
        }

        rnInteracoes.interacaoInclui(pedidoId, interacao, "Sistema", "False");
        rnPagamento.verificaPagamentoCompleto(pedidoId);
    }
    public static void reservaProduto(int pedidoId, int produtoId, int quantidade, int itemPedidoId)
    {
        var produtosDc = new dbCommerceDataContext();
        var reserva = new tbProdutoReservaEstoque();
        reserva.idPedido = pedidoId;
        reserva.idProduto = produtoId;
        reserva.dataHora = DateTime.Now;
        reserva.idItemPedido = itemPedidoId;
        reserva.quantidade = 1;
        produtosDc.tbProdutoReservaEstoques.InsertOnSubmit(reserva);
        produtosDc.SubmitChanges();
    }
    public static void cancelaReservasProduto(int idItemPedidoEstoque)
    {
        var data = new dbCommerceDataContext();
        var itemEstoque = (from c in data.tbItemPedidoEstoques where c.idItemPedidoEstoque == idItemPedidoEstoque select c).First();
        itemEstoque.reservado = false;
        itemEstoque.dataReserva = null;

        var produtoEstoque = (from c in data.tbProdutoEstoques where c.idItemPedidoEstoqueReserva == itemEstoque.idItemPedidoEstoque select c).FirstOrDefault();
        if (produtoEstoque != null)
        {
            produtoEstoque.idItemPedidoEstoqueReserva = null;
            produtoEstoque.itemPedidoIdReserva = null;
            produtoEstoque.pedidoIdReserva = null;
        }
        data.SubmitChanges();
    }

    public static int reservaProdutoOuPedeFornecedor(int pedidoId, int produtoId, int quantidade, int itemPedidoId)
    {
        var data = new dbCommerceDataContext();
        #region Manda para tabela de Queue para poder verificar se coloca na fila de completos
        var queue = new tbQueue();
        queue.agendamento = DateTime.Now;
        queue.andamento = false;
        queue.concluido = false;
        queue.idRelacionado = produtoId;
        queue.tipoQueue = 21; // Gera combo
        queue.mensagem = "";
        data.tbQueues.InsertOnSubmit(queue);
        data.SubmitChanges();
        #endregion

        return 0;


        HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
        usuarioLogadoId = HttpContext.Current.Request.Cookies["usuarioLogadoId"];
        if (usuarioLogadoId == null)
        {
            return 0;
        }

        int retorno = 0;
        int totalDeItens = 0;
        int totalEmEstoque = 0;

        var produtosDc = new dbCommerceDataContext();
        var pagamentos =
            (from c in produtosDc.tbPedidoPagamentos
             where c.cancelado == false && c.pago == true && c.pedidoId == pedidoId
             select c).Any();

        for (int i = 1; i <= quantidade; i++)
        {
            var itemPedido = (from c in produtosDc.tbItensPedidos where c.itemPedidoId == itemPedidoId select c).First();
            var produtosFilho = (from c in produtosDc.tbItensPedidoCombos where c.idItemPedido == itemPedidoId select c);
            int totalProdutosFilho = produtosFilho.Count();
            if ((totalProdutosFilho > 0) && itemPedido.produtoId == produtoId)
            {
                foreach (var produtoRelacionado in produtosFilho)
                {
                    totalDeItens++;
                    int fornecedorId = produtoRelacionado.tbProduto.produtoFornecedor;
                    var fornecedorDc = new dbCommerceDataContext();
                    bool estoqueConferido = (from c in fornecedorDc.tbProdutoFornecedors where c.fornecedorId == fornecedorId select c).FirstOrDefault().estoqueConferido ?? false;
                    var estoqueDoItem = produtosDc.admin_produtoEmEstoque(produtoRelacionado.produtoId).FirstOrDefault();
                    int estoqueTotalDoItem = estoqueDoItem == null ? 0 : Convert.ToInt32(estoqueDoItem.estoqueLivre);

                    if (estoqueTotalDoItem >= 1 && estoqueConferido)
                    {
                        var reserva = new tbProdutoReservaEstoque();
                        reserva.idPedido = pedidoId;
                        reserva.idProduto = produtoRelacionado.produtoId;
                        reserva.dataHora = DateTime.Now;
                        reserva.quantidade = 1;
                        reserva.idItemPedido = itemPedidoId;
                        produtosDc.tbProdutoReservaEstoques.InsertOnSubmit(reserva);
                        produtosDc.SubmitChanges();
                    }
                    else
                    {
                        if (pagamentos) incluiProdutoPedidoProvisorio(produtoRelacionado.produtoId, pedidoId, "", 1, itemPedidoId);
                    }
                }
            }
            else
            {
                totalDeItens++;

                var produto = (from c in produtosDc.tbProdutos where c.produtoId == produtoId select c).First();
                var estoqueDoItem = produtosDc.admin_produtoEmEstoque(produto.produtoId).FirstOrDefault();
                int estoqueTotalDoItem = estoqueDoItem == null ? 0 : Convert.ToInt32(estoqueDoItem.estoqueLivre);
                var pedidosFornecedorSemVinculo =
                                                (from c in produtosDc.tbPedidoFornecedorItems
                                                 where
                                                     c.entregue == false && c.idPedido == null &&
                                                     c.idItemPedido == null && c.idProduto == produto.produtoId
                                                 orderby c.tbPedidoFornecedor.dataLimite
                                                 select c).FirstOrDefault();

                int fornecedorId = produto.produtoFornecedor;
                var fornecedorDc = new dbCommerceDataContext();
                bool estoqueConferido = (from c in fornecedorDc.tbProdutoFornecedors where c.fornecedorId == fornecedorId select c).FirstOrDefault().estoqueConferido ?? false;
                if (estoqueTotalDoItem >= 1 && estoqueConferido)
                {
                    var reserva = new tbProdutoReservaEstoque();
                    reserva.idPedido = pedidoId;
                    reserva.idProduto = produtoId;
                    reserva.dataHora = DateTime.Now;
                    reserva.idItemPedido = itemPedidoId;
                    reserva.quantidade = 1;
                    produtosDc.tbProdutoReservaEstoques.InsertOnSubmit(reserva);
                    produtosDc.SubmitChanges();
                }
                else if (pedidosFornecedorSemVinculo != null)
                {
                    pedidosFornecedorSemVinculo.idItemPedido = itemPedidoId;
                    pedidosFornecedorSemVinculo.idPedido = pedidoId;
                    produtosDc.SubmitChanges();
                }
                else
                {
                    if (pagamentos) incluiProdutoPedidoProvisorio(produtoId, pedidoId, "", 1, itemPedidoId);
                }
            }
        }
        return retorno; // Pedido ao Fornecedor
    }

    private static void incluiProdutoPedidoProvisorio(int produtoId, int pedidoId, string motivo, int quantidade, int idItemPedido)
    {
        HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
        usuarioLogadoId = HttpContext.Current.Request.Cookies["usuarioLogadoId"];
        if (usuarioLogadoId == null)
        {
            return;
        }

        var produtosDc = new dbCommerceDataContext();
        var produto = (from c in produtosDc.tbProdutos where c.produtoId == produtoId select c).First();

        int fornecedorId = produto.produtoFornecedor;
        var fornecedorDc = new dbCommerceDataContext();
        var fornecedor = (from c in fornecedorDc.tbProdutoFornecedors where c.fornecedorId == fornecedorId select c).First();


        bool possuiPrazoInicioFabricacao = false;
        var inicioFabricacaoFornecedor = produto.tbProdutoFornecedor.dataInicioFabricacao == null ? DateTime.Now : (DateTime)produto.tbProdutoFornecedor.dataInicioFabricacao;
        if (produto.tbProdutoFornecedor.dataFimFabricacao != null)
        {
            if (((DateTime)produto.tbProdutoFornecedor.dataFimFabricacao).Date <= DateTime.Now.Date && inicioFabricacaoFornecedor.Date > DateTime.Now.Date)
            {
                possuiPrazoInicioFabricacao = true;
            }
        }
        if (fornecedor.gerarPedido && possuiPrazoInicioFabricacao == false)
        {
            var pedidosDc = new dbCommerceDataContext();
            var pedidoFornecedorCheck =
                (from c in pedidosDc.tbPedidoFornecedors
                 where c.idFornecedor == fornecedorId && c.pendente == true
                 select c).FirstOrDefault();
            var pedidoFornecedor = pedidoFornecedorCheck == null ? new tbPedidoFornecedor() : pedidoFornecedorCheck;
            pedidoFornecedor.data = DateTime.Now;
            pedidoFornecedor.idFornecedor = fornecedorId;
            pedidoFornecedor.confirmado = false;
            pedidoFornecedor.avulso = true;
            pedidoFornecedor.pendente = true;
            if (fornecedor.fornecedorPrazoPedidos > 0)
            {
                int diasPrazo = Convert.ToInt32(fornecedor.fornecedorPrazoPedidos);
                if ((fornecedor.prazoFornecedorDiasUteis ?? false) == true)
                {
                    int diasPrazoFinal = rnFuncoes.retornaPrazoDiasUteis(diasPrazo, 0);
                    diasPrazo = diasPrazoFinal;
                }
                pedidoFornecedor.dataLimite = DateTime.Now.AddDays(diasPrazo);
            }
            else
            {
                pedidoFornecedor.dataLimite = DateTime.Now.AddDays(1);
            }
            pedidoFornecedor.usuario = "";
            if (usuarioLogadoId != null)
            {
                pedidoFornecedor.usuario = rnUsuarios.usuarioSeleciona_PorUsuarioId(int.Parse(usuarioLogadoId.Value.ToString())).Tables[0].Rows[0]["usuarioNome"].ToString();
            }
            if (pedidoFornecedorCheck == null) pedidosDc.tbPedidoFornecedors.InsertOnSubmit(pedidoFornecedor);
            pedidosDc.SubmitChanges();

            int idPedidoFornecedor = pedidoFornecedor.idPedidoFornecedor;

            var itemPedidoCustoZeroTrocaPorDefeito = (from c in pedidosDc.tbItensPedidos where c.itemPedidoId == idItemPedido && c.valorCusto == 0 select c).Any();

            var pedidoFornecedorItem = new tbPedidoFornecedorItem();
            pedidoFornecedorItem.custo = itemPedidoCustoZeroTrocaPorDefeito ? 0 : (decimal)produto.produtoPrecoDeCusto;
            pedidoFornecedorItem.brinde = false;
            pedidoFornecedorItem.diferenca = 0;
            pedidoFornecedorItem.idPedidoFornecedor = idPedidoFornecedor;
            pedidoFornecedorItem.idProduto = produto.produtoId;
            pedidoFornecedorItem.entregue = false;
            pedidoFornecedorItem.quantidade = quantidade;
            pedidoFornecedorItem.motivo = motivo;
            pedidoFornecedorItem.idPedido = pedidoId;
            pedidoFornecedorItem.idItemPedido = idItemPedido;
            pedidosDc.tbPedidoFornecedorItems.InsertOnSubmit(pedidoFornecedorItem);
            pedidosDc.SubmitChanges();
        }
        else
        {
            var data = new dbCommerceDataContext();
            for (int i = 1; i <= quantidade; i++)
            {
                var aguardando = new tbItemPedidoAguardandoEstoque();
                aguardando.idItemPedido = idItemPedido;
                aguardando.dataSolicitacao = DateTime.Now;
                aguardando.idProduto = produtoId;
                aguardando.reservado = false;
                data.tbItemPedidoAguardandoEstoques.InsertOnSubmit(aguardando);
            }
            data.SubmitChanges();
        }
    }


    public static List<listaItensPedido> retornaProdutosEtiqueta(int pedidoId)
    {
        return retornaProdutosEtiqueta(pedidoId, 1);
    }

    public static List<listaItensPedido> retornaProdutosEtiqueta(int pedidoId, int idCentroDeDistribuicao)
    {
        var lista = new List<listaItensPedido>();
        var data = new dbCommerceDataContext();
        var itensFaltandoEntregar = (from c in data.tbItemPedidoEstoques
                                     where c.tbItensPedido.pedidoId == pedidoId && c.enviado == false && c.cancelado == false && (c.reservado == false && c.autorizarParcial == false)
                                     && c.idCentroDistribuicao == idCentroDeDistribuicao
                                     select c).Any();
        if (itensFaltandoEntregar) return lista;

        var itensPedidoEstoque = (from c in data.tbItemPedidoEstoques
                                  where
                                      c.tbItensPedido.pedidoId == pedidoId && c.enviado == false && c.cancelado == false &&
                                      c.reservado == true && c.idCentroDistribuicao == idCentroDeDistribuicao
                                  select c);
        foreach (var itemPedidoEstoque in itensPedidoEstoque)
        {
            //var produto = (from c in produtosDc.tbProdutos where c.produtoId == produtosId select c).First();
            //var fornecedorDc = new dbCommerceDataContext();
            //var fornecedor = (from c in fornecedorDc.tbProdutoFornecedors where c.fornecedorId == produto.produtoFornecedor select c).First();
            var itemEtiqueta = new listaItensPedido();
            int produtoAlternativoId = 0;
            if (itemPedidoEstoque.tbProduto.produtoAlternativoId != null) int.TryParse(itemPedidoEstoque.tbProduto.produtoAlternativoId.ToString(), out produtoAlternativoId);
            itemEtiqueta.itemPedidoId = itemPedidoEstoque.itemPedidoId;
            itemEtiqueta.produtoId = itemPedidoEstoque.produtoId;
            itemEtiqueta.produtoNome = itemPedidoEstoque.tbProduto.produtoNome;
            itemEtiqueta.produtoIdDaEmpresa = itemPedidoEstoque.tbProduto.produtoIdDaEmpresa;
            itemEtiqueta.fornecedorNome = itemPedidoEstoque.tbProduto.tbProdutoFornecedor.fornecedorNome;
            itemEtiqueta.itemQuantidade = 1;
            itemEtiqueta.produtoAlternativoId = produtoAlternativoId;
            lista.Add(itemEtiqueta);
        }
        return lista;



        var pedidosDc = new dbCommerceDataContext();
        var itensPedidoDc = new dbCommerceDataContext();
        var produtosDc = new dbCommerceDataContext();
        var pedidosSeparacao = (from c in pedidosDc.tbPedidos where c.pedidoId == pedidoId select c.pedidoId);

        var listaProdutos = new List<produtoQuantidade>();
        var itensPedido = (from c in itensPedidoDc.tbItensPedidos where c.pedidoId == pedidoId select c);
        int totalProdutosParcial = 0;
        int totalProdutosParcialEntregue = 0;
        int totalProdutosNaoParcial = 0;
        int totalProdutosNaoParcialEntregues = 0;

        foreach (var itemPedido in itensPedido)
        {
            bool cancelado = itemPedido.cancelado == null ? false : (bool)itemPedido.cancelado;
            if (!cancelado)
            {
                var listaProdutosIds = new List<int>();
                var produtosFilho =
                    (from c in produtosDc.tbItensPedidoCombos
                     where c.idItemPedido == itemPedido.itemPedidoId
                     select c);
                if (produtosFilho.Any())
                {
                    foreach (var relacionado in produtosFilho)
                    {
                        listaProdutosIds.Add(relacionado.produtoId);
                    }
                }
                else
                {
                    listaProdutosIds.Add(itemPedido.produtoId);
                }

                foreach (var produtosId in listaProdutosIds)
                {
                    bool enviado = false;
                    if (produtosFilho.Any())
                    {
                        enviado = produtosFilho.First(x => x.produtoId == produtosId).enviado;

                    }
                    else
                    {
                        enviado = (itemPedido.enviado ?? false);
                    }
                    var parcial = (from c in pedidosDc.tbItensPedidoEnvioParcials
                                   where
                                       c.idPedido == pedidoId && c.idProduto == produtosId &&
                                       c.idItemPedido == itemPedido.itemPedidoId && c.enviado == false
                                   select c).Any();
                    if (enviado == false | parcial)
                    {
                        var produtoch = (from c in produtosDc.tbProdutos where c.produtoId == produtosId select c).First();
                        bool totalCompleto = true;
                        if (parcial)
                        {
                            totalProdutosParcial++;
                        }
                        else
                        {
                            totalProdutosNaoParcial++;
                        }


                        int totalReservado = 0;
                        var reservas = (from c in produtosDc.tbProdutoReservaEstoques
                                        where
                                            (c.idProduto == produtosId | c.idProduto == produtoch.produtoAlternativoId) 
                                            && c.idPedido == pedidoId && c.idItemPedido == itemPedido.itemPedidoId
                                        select c);
                        if (reservas.Any())
                        {
                            totalReservado = reservas.Sum(x => x.quantidade);
                        }

                        int totalPedidosFornecedor = 0;
                        var pedidosFornecedor = (from c in pedidosDc.tbPedidoFornecedorItems
                                                 where (c.idProduto == produtosId | c.idProduto == produtoch.produtoAlternativoId) && c.idPedido == pedidoId && c.entregue == true &&
                                  c.idItemPedido == itemPedido.itemPedidoId
                                                 select c);
                        if (pedidosFornecedor.Any())
                        {
                            totalPedidosFornecedor = pedidosFornecedor.Sum(x => x.quantidade);
                        }

                        var itensFaltando = (from c in produtosDc.tbPedidoProdutoFaltandos
                                             where
                                                 c.pedidoId == pedidoId && (c.produtoId == produtosId | c.produtoId == produtoch.produtoAlternativoId) && c.entregue == false &&
                                                 c.itemPedidoId == itemPedido.itemPedidoId
                                             select c).Any();
                        if (itensFaltando)
                        {
                            totalCompleto = false;
                        }

                        if (pedidosFornecedor.Any())
                        {
                            totalPedidosFornecedor = pedidosFornecedor.Sum(x => x.quantidade);
                        }

                        if ((totalReservado + totalPedidosFornecedor) < itemPedido.itemQuantidade)
                        {
                            totalCompleto = false;
                        }

                        if (totalCompleto)
                        {
                            var produto = (from c in produtosDc.tbProdutos where c.produtoId == produtosId select c).First();
                            var fornecedorDc = new dbCommerceDataContext();
                            var fornecedor = (from c in fornecedorDc.tbProdutoFornecedors where c.fornecedorId == produto.produtoFornecedor select c).First();
                            var itemEtiqueta = new listaItensPedido();
                            int produtoAlternativoId = 0;
                            if (produto.produtoAlternativoId != null) int.TryParse(produto.produtoAlternativoId.ToString(), out produtoAlternativoId);
                            itemEtiqueta.itemPedidoId = itemPedido.itemPedidoId;
                            itemEtiqueta.produtoId = produtosId;
                            itemEtiqueta.produtoNome = produto.produtoNome;
                            itemEtiqueta.produtoIdDaEmpresa = produto.produtoIdDaEmpresa;
                            itemEtiqueta.fornecedorNome = fornecedor.fornecedorNome;
                            itemEtiqueta.itemQuantidade = itemPedido.itemQuantidade;
                            itemEtiqueta.produtoAlternativoId = produtoAlternativoId;
                            lista.Add(itemEtiqueta);
                        }
                    }
                }
            }
        }

        return lista;
    }



    public static List<listaItensPedido> retornaProdutosEtiquetaLote(List<int> pedidos, int idCentroDeDistribuicao)
    {
        var lista = new List<listaItensPedido>();
        var data = new dbCommerceDataContext();
        var itensFaltandoEntregar = (from c in data.tbItemPedidoEstoques
                                     where pedidos.Contains(c.tbItensPedido.pedidoId) && c.enviado == false && c.cancelado == false && (c.reservado == false && c.autorizarParcial == false)
                                     && c.idCentroDistribuicao == idCentroDeDistribuicao
                                     select c).Any();
        if (itensFaltandoEntregar) return lista;

        var itensPedidoEstoque = (from c in data.tbItemPedidoEstoques
                                  where
                                      pedidos.Contains(c.tbItensPedido.pedidoId) && c.enviado == false && c.cancelado == false &&
                                      c.reservado == true && c.idCentroDistribuicao == idCentroDeDistribuicao
                                  select c);
        foreach (var itemPedidoEstoque in itensPedidoEstoque)
        {
            //var produto = (from c in produtosDc.tbProdutos where c.produtoId == produtosId select c).First();
            //var fornecedorDc = new dbCommerceDataContext();
            //var fornecedor = (from c in fornecedorDc.tbProdutoFornecedors where c.fornecedorId == produto.produtoFornecedor select c).First();
            var itemEtiqueta = new listaItensPedido();
            int produtoAlternativoId = 0;
            if (itemPedidoEstoque.tbProduto.produtoAlternativoId != null) int.TryParse(itemPedidoEstoque.tbProduto.produtoAlternativoId.ToString(), out produtoAlternativoId);
            itemEtiqueta.itemPedidoId = itemPedidoEstoque.itemPedidoId;
            itemEtiqueta.produtoId = itemPedidoEstoque.produtoId;
            itemEtiqueta.produtoNome = itemPedidoEstoque.tbProduto.produtoNome;
            itemEtiqueta.produtoIdDaEmpresa = itemPedidoEstoque.tbProduto.produtoIdDaEmpresa;
            itemEtiqueta.fornecedorNome = itemPedidoEstoque.tbProduto.tbProdutoFornecedor.fornecedorNome;
            itemEtiqueta.itemQuantidade = 1;
            itemEtiqueta.produtoAlternativoId = produtoAlternativoId;
            lista.Add(itemEtiqueta);
        }
        return lista;        
    }

    public class listaItensPedido
    {
        public int itemPedidoId { get; set; }
        public int produtoId { get; set; }
        public string produtoNome { get; set; }
        public string produtoIdDaEmpresa { get; set; }
        public string fornecedorNome { get; set; }
        public decimal itemQuantidade { get; set; }
        public int produtoAlternativoId { get; set; }
    }


    public static DataSet pedidosPagamentoConfirmado()
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("admin_pedidosPagamentoConfirmado");

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }
    public static DataSet pedidosProdutoNaoEncomendado(int produtoId)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("admin_pedidosProdutoNaoEncomendado");
        db.AddInParameter(dbCommand, "produtoId", DbType.Int32, produtoId);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static void atualizaTodosStatusRastreio()
    {
        var data = new dbCommerceDataContext();
        var pacotes = (from c in data.tbPedidoPacotes where (c.rastreamentoConcluido ?? false) == false && c.rastreio != "" && c.formaDeEnvio != "" && c.formaDeEnvio != null orderby c.idPedidoEnvio descending select c);
        foreach (var pacote in pacotes)
        {
            if (pacote.rastreio != "" && pacote.rastreio != null)
            {
                if (pacote.formaDeEnvio.ToLower() == "jadlog" | pacote.formaDeEnvio.ToLower() == "jadlogexpressa")
                {
                    string status = consultaStatusJadlog(pacote.rastreio).Trim();
                    pacote.statusAtual = status;
                    if (status.ToLower() == "entregue")
                    {
                        pacote.rastreamentoConcluido = true;
                        //TODO:Alterar status nos marketplaces
                    }
                    data.SubmitChanges();
                }
                else if (pacote.formaDeEnvio.ToLower() == "pac" | pacote.formaDeEnvio.ToLower() == "sedex")
                {
                    string status = consultaStatusCorreios(pacote.rastreio).Trim();
                    pacote.statusAtual = status;
                    if (status.ToLower() == "entrega efetuada")
                    {
                        pacote.rastreamentoConcluido = true;
                    }
                    data.SubmitChanges();
                }
                else if (pacote.formaDeEnvio.ToLower() == "tnt")
                {
                    var envio = pacote.tbPedidoEnvio;
                    if (envio != null)
                    {
                        string status = consultaStatusTnt((int)envio.nfeNumero).Trim();
                        pacote.statusAtual = status;
                        if (status.ToLower() == "entrega realizada")
                        {
                            pacote.rastreamentoConcluido = true;
                        }
                        data.SubmitChanges();
                    }
                }
            }
        }
    }

    public static void atualizaStatusRastreio(int idPedido)
    {
        var data = new dbCommerceDataContext();
        var pacotes = (from c in data.tbPedidoPacotes where c.idPedido == idPedido && (c.rastreamentoConcluido ?? false) == false && c.rastreio != "" && c.formaDeEnvio != "" && c.formaDeEnvio != null select c);
        foreach (var pacote in pacotes)
        {
            if (pacote.rastreio != "" && pacote.rastreio != null)
            {
                if (pacote.formaDeEnvio.ToLower() == "jadlog" | pacote.formaDeEnvio.ToLower() == "jadlogexpressa")
                {
                    string status = consultaStatusJadlog(pacote.rastreio).Trim();
                    pacote.statusAtual = status;
                    if (status.ToLower() == "entregue")
                    {
                        pacote.rastreamentoConcluido = true;
                        //TODO:Alterar status nos marketplaces
                    }
                    data.SubmitChanges();
                }
                else if (pacote.formaDeEnvio.ToLower() == "pac" | pacote.formaDeEnvio.ToLower() == "sedex")
                {
                    string status = consultaStatusCorreios(pacote.rastreio).Trim();
                    pacote.statusAtual = status;
                    if (status.ToLower() == "entrega efetuada")
                    {
                        pacote.rastreamentoConcluido = true;
                    }
                    data.SubmitChanges();
                }
                else if (pacote.formaDeEnvio.ToLower() == "tnt")
                {
                    var envio = pacote.tbPedidoEnvio;
                    if (envio != null)
                    {
                        try
                        {
                            string status = consultaStatusTnt((int)envio.nfeNumero).Trim();
                            pacote.statusAtual = status;
                            if (status.ToLower() == "entrega realizada")
                            {
                                pacote.rastreamentoConcluido = true;
                            }
                            data.SubmitChanges();
                        }
                        catch (Exception)
                        {

                        }
                    }
                }
            }
        }
    }

    public static string consultaStatusCorreios(string rastreio)
    {
        string sUrlHtml = @"http://websro.correios.com.br/sro_bin/txect01$.QueryList?P_LINGUA=001&P_TIPO=001&P_COD_UNI=" + rastreio;

        string retorno = "";
        try
        {
            HtmlWeb oHtmlWeb = new HtmlAgilityPack.HtmlWeb();
            HtmlDocument oHtmlDocument = oHtmlWeb.Load(sUrlHtml);
            HtmlNode oRootNode = oHtmlDocument.DocumentNode;
            int linhaAtual = 0;
            var tabelas = from table in oRootNode.SelectNodes("//table").Cast<HtmlNode>()
                          select table;
            foreach (var tabela in tabelas)
            {
                var linhas = from row in tabela.SelectNodes("//tr").Cast<HtmlNode>() select row;
                foreach (HtmlNode linha in linhas)
                {
                    linhaAtual++;
                    if (linhaAtual == 2)
                    {
                        var colunas = from column in linha.SelectNodes("//td").Cast<HtmlNode>() select column;
                        int colunaAtual = 0;
                        foreach (var coluna in colunas)
                        {
                            colunaAtual++;
                            if (colunaAtual == 6)
                            {
                                return coluna.InnerText;
                            }
                        }
                    }
                }

            }
        }
        catch (Exception)
        {
        }
        return "ERRO";
    }
    public static string consultaStatusTnt(int nota)
    {
        string localizacao = "";
        var parametros = new serviceTntLocalizacao.LocalizacaoIn();
        parametros.usuario = "atendimento@bark.com.br";
        parametros.nf = nota;
        parametros.nfSpecified = true;
        parametros.nfSerie = "1";
        parametros.cnpj = "10924051000163";

        try
        {
            var consulta = new serviceTntLocalizacao.Localizacao();
            var retorno = consulta.localizaMercadoria(parametros);
            localizacao = retorno.localizacao;
        }
        catch (Exception)
        {

        }
        //retorno.localizacao;
        //retorno.conhecimento;
        //http://app.tntbrasil.com.br/LocalizacaoSimplificada/resultado_localizacao.asp?cnpj=01404654135&nota=7858

        return localizacao;
    }
    public static string consultaStatusJadlog(string codPedidoJadlog)
    {
        try
        {
            //Cod Lindsay
            string CodCliente = "10924051000163";
            string Password = "L2F0M0E1";

            var serviceJad = new serviceJadTracking.TrackingBeanService();
            var retornoServiceJad = serviceJad.consultar(CodCliente, Password, codPedidoJadlog);
            //Response.Write(retornoServiceJad + "<br>");
            string numeroTracking = "";

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(retornoServiceJad);


            XmlNodeList parentNodeStatus = xmlDoc.GetElementsByTagName("Status");
            string statusAtual = "";
            try
            {
                statusAtual = parentNodeStatus[0].InnerText.Trim();
            }
            catch (Exception)
            {

            }

            if (statusAtual.ToLower() == "entregue") return statusAtual;

            XmlNodeList parentNode = xmlDoc.GetElementsByTagName("Evento");

            foreach (XmlNode childrenNode in parentNode)
            {
                try
                {
                    string retorno = childrenNode.ChildNodes[2].InnerText;
                    numeroTracking = retorno;
                    if (!string.IsNullOrEmpty(statusAtual) && statusAtual.Trim().ToLower() != numeroTracking.Trim().ToLower())
                    {
                        numeroTracking = statusAtual + " - " + numeroTracking;
                    }
                }
                catch (Exception)
                {

                }
            }
            return numeroTracking;
        }
        catch (Exception)
        {
            return "Erro";
        }
    }

    public static int confirmarPagamentoPedido(int pedidoId, string usuario)
    {
        int retorno = 0;

        using (var pedidoAlteraContext = new dbCommerceDataContext())
        {
            var pedido = (from c in pedidoAlteraContext.tbPedidos where c.pedidoId == pedidoId select c).First();
            var clientesDc = new dbCommerceDataContext();
            var cliente = (from c in clientesDc.tbClientes where c.clienteId == pedido.clienteId select c).First();
            pedido.dataConfirmacaoPagamento = DateTime.Now;

            if (pedido.statusDoPedido != 5)// só alterar o status se ainda não tiver sido enviado
            {
                pedido.statusDoPedido = 3;
                rnInteracoes.interacaoInclui(pedidoId, "Pagamento Confirmado", usuario, "True");
            }

            if (pedido.prazoMaximoPedidos != null)
            {

                int prazoInicioFabricacao = 0;
                if (pedido.prazoInicioFabricao != null)
                {
                    int diferencaDias = (DateTime.Now - Convert.ToDateTime(pedido.dataHoraDoPedido)).Days;
                    prazoInicioFabricacao = (int)pedido.prazoInicioFabricao - diferencaDias;
                    if (prazoInicioFabricacao < 0) prazoInicioFabricacao = 0;
                }

                int prazoFinal = rnFuncoes.retornaPrazoDiasUteis((int)pedido.prazoMaximoPedidos, prazoInicioFabricacao);

                // verifica se é necessário gravar prazoFinalPedido
                if (pedido.prazoFinalPedido == null)
                {
                    pedido.prazoFinalPedido = DateTime.Now.AddDays(prazoFinal);
                }

                //verificação especifica para plano maternidade
                if (pedido.tipoDePagamentoId == 11)// plano maternidade
                {
                    var cobrancas = (from c in pedidoAlteraContext.tbPedidoPagamentos
                                     where c.pedidoId == pedidoId && c.cancelado == false
                                     orderby c.dataVencimento
                                     select c).ToList();

                    DateTime prazoFinalComparacao = cobrancas.OrderByDescending(x => x.dataVencimento).First().dataVencimento.AddDays(2);// colocado acréscimo de mais 2 dias pois a transportadora despacha no dia seguinte da postagem

                    if (prazoFinalComparacao < DateTime.Now.AddDays(prazoFinal))
                        pedido.prazoFinalPedido = prazoFinalComparacao;
                }

                pedidoAlteraContext.SubmitChanges();
            }


            var queuePrazo = new tbQueue();
            queuePrazo.agendamento = DateTime.Now;
            queuePrazo.andamento = false;
            queuePrazo.concluido = false;
            queuePrazo.mensagem = "";
            queuePrazo.tipoQueue = 5;
            queuePrazo.idRelacionado = pedidoId;
            pedidoAlteraContext.tbQueues.InsertOnSubmit(queuePrazo);
            pedidoAlteraContext.SubmitChanges();

            var queue = new tbQueue();
            queue.agendamento = DateTime.Now;
            queue.andamento = false;
            queue.concluido = false;
            queue.mensagem = "";
            queue.tipoQueue = 6;
            queue.idRelacionado = pedidoId;
            pedidoAlteraContext.tbQueues.InsertOnSubmit(queue);
            pedidoAlteraContext.SubmitChanges();
            //rnEstoque.ReservarEstoque(pedidoId);


            var queueConfirmacaoDados = new tbQueue();
            queueConfirmacaoDados.agendamento = DateTime.Now.AddMinutes(1);
            queueConfirmacaoDados.andamento = false;
            queueConfirmacaoDados.concluido = false;
            queueConfirmacaoDados.mensagem = "";
            queueConfirmacaoDados.tipoQueue = 22;
            queueConfirmacaoDados.idRelacionado = pedidoId;
            pedidoAlteraContext.tbQueues.InsertOnSubmit(queueConfirmacaoDados);
            pedidoAlteraContext.SubmitChanges();


            var completo = verificaReservasPedidoFornecedorCompleto(pedidoId);
            if (completo)
            {
                if (pedido.separado)
                {
                    pedido.separado = false;
                    pedido.idUsuarioSeparacao = null;
                    pedido.dataInicioSeparacao = null;
                    pedido.dataFimSeparacao = null;
                }

                pedido.statusDoPedido = 11;
                pedidoAlteraContext.SubmitChanges();

                rnInteracoes.interacaoInclui(pedidoId, "Separação de Estoque", usuario, "True");
                rnEmails.enviaEmailPedidoProcessado(pedidoId);
                //rnEmails.enviaAlteracaoDeStatus(cliente.clienteNome, pedidoId.ToString(), "Separação de Estoque", cliente.clienteEmail);
                retorno = 1;
            }

            AdicionaQueueChecagemCompleto(pedidoId);
        }

        rnEmails.enviaEmailPagamentoConfirmado(pedidoId);

        return retorno;
    }
    public static void verificaReservaEstoquePedido(int pedidoId)
    {
        var produtosDc = new dbCommerceDataContext();
        var pedidosDc = new dbCommerceDataContext();
        var itensDc = new dbCommerceDataContext();
        var itensPedido = (from c in itensDc.tbItensPedidos where c.pedidoId == pedidoId select c);
        foreach (var item in itensPedido)
        {
            bool cancelado = item.cancelado ?? false;
            if (!cancelado)
            {
                var relacionados = (from c in produtosDc.tbProdutoRelacionados where c.idProdutoPai == item.produtoId select c);
                if (relacionados.Any())
                {
                    foreach (var relacionado in relacionados)
                    {
                        verificaReservaEstoquePedidoProduto(pedidoId, relacionado.idProdutoFilho, item.itemPedidoId, Convert.ToInt32(item.itemQuantidade));
                    }
                }
                else
                {
                    verificaReservaEstoquePedidoProduto(pedidoId, item.produtoId, item.itemPedidoId, Convert.ToInt32(item.itemQuantidade));
                }
            }
        }
    }
    public static void verificaReservaEstoquePedidoProduto(int pedidoId, int produtoId, int itemPedidoId, int quantidade)
    {
        var produtosDc = new dbCommerceDataContext();
        var pedidosDc = new dbCommerceDataContext();
        var itensDc = new dbCommerceDataContext();

        var reservasAtuais = (from c in produtosDc.tbProdutoReservaEstoques
                              where c.idPedido == pedidoId && c.idItemPedido == itemPedidoId && c.idProduto == produtoId
                              select c).Count();
        var pedidosAtuais = (from c in pedidosDc.tbPedidoFornecedorItems
                             where c.idPedido == pedidoId && c.idItemPedido == itemPedidoId && c.idProduto == produtoId
                             select c).Count();
        if (reservasAtuais + pedidosAtuais < Convert.ToInt32(quantidade))
        {
            var estoque = produtosDc.admin_produtoEmEstoque(produtoId).FirstOrDefault();
            if (estoque != null)
            {
                int totalPedir = Convert.ToInt32(quantidade) - reservasAtuais - pedidosAtuais;
                int estoqueLivre = Convert.ToInt32(estoque.estoqueLivre);
                if (estoqueLivre > 0)
                {
                    if (estoqueLivre < totalPedir)
                    {
                        totalPedir = estoqueLivre;
                    }
                    for (int i = 1; i <= totalPedir; i++)
                    {
                        reservaProduto(pedidoId, produtoId, 1, itemPedidoId);
                    }
                }
            }
        }
    }

    public static void AdicionaQueueChecagemPedidoCompleto(int pedidoId)
    {
        var data = new dbCommerceDataContext();
        var queue = new tbQueue();
        queue.agendamento = DateTime.Now;
        queue.andamento = false;
        queue.concluido = false;
        queue.idRelacionado = pedidoId;
        queue.mensagem = "";
        queue.tipoQueue = 8;
        data.tbQueues.InsertOnSubmit(queue);
        data.SubmitChanges();
        data.Dispose();
    }


   
    public static bool verificaReservasPedidoFornecedorCompleto(int pedidoId)
    {

        var data = new dbCommerceDataContext();
        var produtosAguardandoEstoque = (from c in data.tbItemPedidoEstoques
                                         where
                                             c.dataLimite != null && c.cancelado == false && c.reservado == false &&
                                             c.tbItensPedido.pedidoId == pedidoId
                                         select c).ToList();
        var aguardandoGeral = (from c in data.tbItemPedidoEstoques
                               where produtosAguardandoEstoque.Select(x => x.produtoId).Contains(c.produtoId) && c.reservado == false && c.cancelado == false && c.dataLimite != null
                               orderby c.dataLimite
                               select new
                               {
                                   c.idItemPedidoEstoque,
                                   c.tbItensPedido.pedidoId,
                                   c.dataCriacao,
                                   dataLimiteReserva = c.dataLimite,
                                   idProduto = c.produtoId,
                                   idItemPedido = c.itemPedidoId
                               }).ToList();
        var pedidosFornecedorGeral = (from c in data.tbPedidoFornecedorItems
                                      join d in data.tbProdutoFornecedors on c.tbPedidoFornecedor.idFornecedor equals d.fornecedorId
                                      where c.entregue == false && produtosAguardandoEstoque.Select(x => x.produtoId).Contains(c.idProduto)
                                      orderby c.tbPedidoFornecedor.dataLimite, c.tbPedidoFornecedor.idPedidoFornecedor
                                      select new
                                      {
                                          c.idPedidoFornecedor,
                                          c.idPedidoFornecedorItem,
                                          dataLimiteFornecedor = c.tbPedidoFornecedor.dataLimite,
                                          c.idProduto,
                                          d.fornecedorNome
                                      }).ToList();
        bool totalItensPedidos = true;
        foreach (var produtoAguardandoEstoque in produtosAguardandoEstoque)
        {
            var aguardando = (from c in aguardandoGeral
                              where c.idProduto == produtoAguardandoEstoque.produtoId
                              orderby c.dataLimiteReserva
                              select c).ToList();
            var pedidoFornecedor = (from c in pedidosFornecedorGeral
                                    where c.idProduto == produtoAguardandoEstoque.produtoId
                                    orderby c.dataLimiteFornecedor, c.idPedidoFornecedor
                                    select c).ToList();
            var listaFinal = (from c in aguardando.Select((item, index) => new { item, index })
                              join d in pedidoFornecedor.Select((item, index) => new { item, index }) on c.index equals d.index into
                                  pedidosFornecedorListaInterna
                              from e in pedidosFornecedorListaInterna.DefaultIfEmpty()
                              select new
                              {
                                  c.item.idItemPedidoEstoque,
                                  c.item.pedidoId,
                                  c.item.dataCriacao,
                                  c.item.dataLimiteReserva,
                                  c.item.idItemPedido,
                                  idPedidoFornecedor = e != null ? e.item.idPedidoFornecedor : 0,
                                  dataLimiteFornecedor = e != null ? (DateTime?)e.item.dataLimiteFornecedor : null,
                                  idPedidoFornecedorItem = e != null ? (int?)e.item.idPedidoFornecedorItem : null,
                                  fornecedorNome = e != null ? e.item.fornecedorNome : ""
                              }
                ).ToList();
            var itemNaLista = listaFinal.FirstOrDefault(x => x.idItemPedidoEstoque == produtoAguardandoEstoque.idItemPedidoEstoque);
            if (itemNaLista != null)
            {
                if (itemNaLista.idPedidoFornecedor == 0)
                {
                    totalItensPedidos = false;
                }
            }
        }

        return totalItensPedidos;


        var produtosDc = new dbCommerceDataContext();
        var pedidosDc = new dbCommerceDataContext();
        var itensDc = new dbCommerceDataContext();
        var itensPedido = (from c in itensDc.tbItensPedidos where c.pedidoId == pedidoId select c);
        bool completo = true;

        foreach (var item in itensPedido)
        {
            bool cancelado = item.cancelado ?? false;
            if (!cancelado)
            {
                var relacionados = (from c in produtosDc.tbProdutoRelacionados where c.idProdutoPai == item.produtoId select c);
                if (relacionados.Any())
                {
                    foreach (var relacionado in relacionados)
                    {
                        var reservasAtuais = (from c in produtosDc.tbProdutoReservaEstoques
                                              where c.idPedido == pedidoId && c.idItemPedido == item.itemPedidoId && c.idProduto == relacionado.idProdutoFilho
                                              select c).Count();
                        var pedidosAtuais = (from c in pedidosDc.tbPedidoFornecedorItems
                                             where c.idPedido == pedidoId && c.idItemPedido == item.itemPedidoId && c.idProduto == relacionado.idProdutoFilho
                                             select c).Count();
                        if (reservasAtuais + pedidosAtuais < Convert.ToInt32(item.itemQuantidade))
                        {
                            completo = false;
                        }
                    }
                }
                else
                {
                    var reservasAtuais = (from c in produtosDc.tbProdutoReservaEstoques
                                          where c.idPedido == pedidoId && c.idItemPedido == item.itemPedidoId && c.idProduto == item.produtoId
                                          select c).Count();
                    var pedidosAtuais = (from c in pedidosDc.tbPedidoFornecedorItems
                                         where c.idPedido == pedidoId && c.idItemPedido == item.itemPedidoId && c.idProduto == item.produtoId
                                         select c).Count();
                    if (reservasAtuais + pedidosAtuais < Convert.ToInt32(item.itemQuantidade))
                    {
                        completo = false;
                    }
                }

            }
        }

        return completo;
    }

    public static string DefineEntregaPedido(int pedidoId, int idPedidoEnvio)
    {
        var pedidoDc = new dbCommerceDataContext();
        var clienteDc = new dbCommerceDataContext();
        var pedido = (from c in pedidoDc.tbPedidos where c.pedidoId == pedidoId select c).First();
        var cliente = (from c in clienteDc.tbClientes where c.clienteId == pedido.clienteId select c).First();
        var volumes = (from c in pedidoDc.tbPedidoPacotes where c.idPedido == pedidoId && (c.concluido ?? true) == false && c.idPedidoEnvio == idPedidoEnvio select c);
        var envio = (from c in pedidoDc.tbPedidoEnvios where c.idPedidoEnvio == idPedidoEnvio select c).First();
        envio.nfeObrigatoria = true;
        var produtosEnvio = (from c in pedidoDc.tbProdutoEstoques where c.idPedidoEnvio == idPedidoEnvio select c.produtoId).ToList();
        var produtosDc = new dbCommerceDataContext();
        //var produtosCubagem =
        //    (from c in produtosDc.tbProdutos
        //     where produtosEnvio.Contains(c.produtoId) && c.cubagem.ToLower() == "true"
        //     select c).Any();
        var produtosPrecos =
            (from c in produtosDc.tbProdutos
             where produtosEnvio.Contains(c.produtoId)
             select new { preco = (c.produtoPrecoPromocional ?? 0) > c.produtoPreco ? c.produtoPrecoPromocional : c.produtoPreco, c.produtoPrecoDeCusto });
        decimal valorTotalDoPedido = 0;
        decimal valorTotalDoCustoDoPedido = 0;
        try
        {
            if (produtosPrecos.Any())
            {
                valorTotalDoPedido = produtosPrecos.Sum(x => (decimal)x.preco);
                valorTotalDoCustoDoPedido = produtosPrecos.Sum(x => (decimal)x.produtoPrecoDeCusto);
            }
            else
            {
                valorTotalDoPedido = (decimal)pedido.valorCobrado;
                valorTotalDoCustoDoPedido = (decimal)pedido.valorCobrado / 2;
            }
            int pesoTotal = volumes.Sum(x => x.peso);
            long cep = Convert.ToInt64(pedido.endCep.Replace("-", "").Replace(".", "").Replace(" ", "").Trim());


            //decimal custoImpostos = 0;
            //var empresaSimples = (from c in pedidoDc.tbEmpresas
            //                      join d in pedidoDc.tbNotaFiscals on c.idEmpresa equals d.idCNPJ into totalNotas
            //                      where c.simples == true && c.ativo == true
            //                      select new
            //                      {
            //                          c.idEmpresa,
            //                          total =
            //                              (totalNotas.Any(
            //                                  x => x.dataHora.Month == DateTime.Now.Month && x.dataHora.Year == DateTime.Now.Year) == true
            //                                  ? (decimal)
            //                                      totalNotas.Where(
            //                                          x => x.dataHora.Month == DateTime.Now.Month && x.dataHora.Year == DateTime.Now.Year)
            //                                          .Sum(x => x.valor)
            //                                  : (decimal)0)
            //                      }
            //    ).Where(x => x.total < 200000).OrderByDescending(x => x.total).FirstOrDefault();
            //if (empresaSimples == null)
            //{
            //    custoImpostos += ((valorTotalDoPedido / 100) * Convert.ToDecimal("15")); // Aliquota lucro presumido
            //}
            //else
            //{
            //    custoImpostos += ((valorTotalDoPedido / 100) * Convert.ToDecimal("15")); // Aliquota simples
            //}


            #region Calculo na Jadlog
            var valoresJadlog = rnFrete.CalculaFrete(13, Convert.ToInt32(pesoTotal), pedido.endCep.Replace("-", "").Trim(), valorTotalDoPedido, 0, 0, 0);
            //var valoresJadlog = rnFrete.CalculaFreteWebserviceJadlog(pesoTotal, cep, "", valorTotalDoPedido);
            //if (valoresJadlog.valor <= 0)
            //{
            //    valoresJadlog = rnFrete.CalculaFreteWebserviceJadlog(pesoTotal, cep, "", valorTotalDoPedido);
            //}

            //if (valoresJadlog.valor <= 0)
            //{
            //    var faixasTotal = (from c in pedidoDc.tbFaixaDeCeps
            //                       where c.tipodeEntregaId == 13
            //                       select new
            //                       {
            //                           faixa = Convert.ToInt64(c.faixaInicial)
            //                       }).ToList();
            //    var proximaFaixa = (from c in faixasTotal where c.faixa > cep orderby c.faixa select c).FirstOrDefault();
            //    if (proximaFaixa != null)
            //    {
            //        //var valoresJadlog2 = rnFrete.CalculaFreteWebserviceJadlog(pesoTotal, Convert.ToInt64(proximaFaixa.faixa), modalidadeJadlog, valorTotalDoPedido);
            //        var valoresJadlog2 = rnFrete.CalculaFrete(13, Convert.ToInt32(pesoTotal), proximaFaixa.faixa.ToString(), valorTotalDoPedido, 0, 0, 0);
            //        var valorTransporteJadlog = new tbPedidoEnvioValorTransporte();
            //        valorTransporteJadlog.idPedidoEnvio = idPedidoEnvio;
            //        valorTransporteJadlog.prazo = valoresJadlog2.prazo;
            //        valorTransporteJadlog.servico = "jadlog";
            //        valorTransporteJadlog.valor = Convert.ToDecimal(valoresJadlog2.valor.ToString("0.00"));
            //        pedidoDc.tbPedidoEnvioValorTransportes.InsertOnSubmit(valorTransporteJadlog);
            //        pedidoDc.SubmitChanges();
            //    }
            //}
            //else
            //{
            var valorTransporteJadlog = new tbPedidoEnvioValorTransporte();
            valorTransporteJadlog.idPedidoEnvio = idPedidoEnvio;
            valorTransporteJadlog.prazo = valoresJadlog.prazo;
            valorTransporteJadlog.servico = "jadlog";
            valorTransporteJadlog.valor = Convert.ToDecimal(valoresJadlog.valor.ToString("0.00"));
            pedidoDc.tbPedidoEnvioValorTransportes.InsertOnSubmit(valorTransporteJadlog);
            pedidoDc.SubmitChanges();
            //}
            #endregion


            /*
            #region Calculo na Jadlog Expressa

            bool usarExpressa = false;
            if (usarExpressa)
            {
                var valoresJadlogExpressa = rnFrete.CalculaFreteWebserviceJadlog(pesoTotal, cep, "jadlogexpressa",
                    valorTotalDoPedido);

                if (valoresJadlogExpressa.valor <= 0)
                {
                    valoresJadlogExpressa = rnFrete.CalculaFreteWebserviceJadlog(pesoTotal, cep, "jadlogexpressa",
                        valorTotalDoPedido);
                }

                if (valoresJadlogExpressa.valor <= 0)
                {
                    var faixasTotal = (from c in pedidoDc.tbFaixaDeCeps
                                       where c.tipodeEntregaId == 13
                                       select new
                                       {
                                           faixa = Convert.ToInt64(c.faixaInicial)
                                       }).ToList();
                    var proximaFaixa = (from c in faixasTotal where c.faixa > cep orderby c.faixa select c).FirstOrDefault();
                    if (proximaFaixa != null)
                    {
                        var valoresJadlog2 = rnFrete.CalculaFreteWebserviceJadlog(pesoTotal,
                            Convert.ToInt64(proximaFaixa.faixa), "jadlogexpressa", valorTotalDoPedido);
                        var valorTransporteJadlog = new tbPedidoEnvioValorTransporte();
                        valorTransporteJadlog.idPedidoEnvio = idPedidoEnvio;
                        valorTransporteJadlog.prazo = valoresJadlog2.prazo;
                        valorTransporteJadlog.servico = "jadlogexpressa";
                        valorTransporteJadlog.valor = Convert.ToDecimal(valoresJadlog2.valor.ToString("0.00"));
                        pedidoDc.tbPedidoEnvioValorTransportes.InsertOnSubmit(valorTransporteJadlog);
                        pedidoDc.SubmitChanges();
                    }
                }
                else
                {
                    var valorTransporteJadlog = new tbPedidoEnvioValorTransporte();
                    valorTransporteJadlog.idPedidoEnvio = idPedidoEnvio;
                    valorTransporteJadlog.prazo = valoresJadlogExpressa.prazo;
                    valorTransporteJadlog.servico = "jadlogexpressa";
                    valorTransporteJadlog.valor = Convert.ToDecimal(valoresJadlogExpressa.valor.ToString("0.00"));
                    pedidoDc.tbPedidoEnvioValorTransportes.InsertOnSubmit(valorTransporteJadlog);
                    pedidoDc.SubmitChanges();

                }
            }

            #endregion

            #region Calculo na Jadlog Com

            bool usrJadlogCom = true;
            if (usrJadlogCom)
            {
                var valoresJadlogCom = rnFrete.CalculaFreteWebserviceJadlog(pesoTotal, cep, "jadlogcom", valorTotalDoPedido);
                if (valoresJadlogCom.valor <= 0)
                {
                    var faixasTotal = (from c in pedidoDc.tbFaixaDeCeps
                                       where c.tipodeEntregaId == 13
                                       select new
                                       {
                                           faixa = Convert.ToInt64(c.faixaInicial)
                                       }).ToList();
                    var proximaFaixa = (from c in faixasTotal where c.faixa > cep orderby c.faixa select c).FirstOrDefault();
                    if (proximaFaixa != null)
                    {
                        var valoresJadlog2 = rnFrete.CalculaFreteWebserviceJadlog(pesoTotal, Convert.ToInt64(proximaFaixa.faixa), "jadlogcom", valorTotalDoPedido);
                        var valorTransporteJadlog = new tbPedidoEnvioValorTransporte();
                        valorTransporteJadlog.idPedidoEnvio = idPedidoEnvio;
                        valorTransporteJadlog.prazo = valoresJadlog2.prazo;
                        valorTransporteJadlog.servico = "jadlogcom";
                        valorTransporteJadlog.valor = Convert.ToDecimal(valoresJadlog2.valor.ToString("0.00"));
                        pedidoDc.tbPedidoEnvioValorTransportes.InsertOnSubmit(valorTransporteJadlog);
                        pedidoDc.SubmitChanges();
                    }
                }
                else
                {
                    var valorTransporteJadlog = new tbPedidoEnvioValorTransporte();
                    valorTransporteJadlog.idPedidoEnvio = idPedidoEnvio;
                    valorTransporteJadlog.prazo = valoresJadlogCom.prazo;
                    valorTransporteJadlog.servico = "jadlogcom";
                    valorTransporteJadlog.valor = Convert.ToDecimal(valoresJadlogCom.valor.ToString("0.00"));
                    pedidoDc.tbPedidoEnvioValorTransportes.InsertOnSubmit(valorTransporteJadlog);
                    pedidoDc.SubmitChanges();

                }
            }

            #endregion

            #region Calculo na Jadlog Rodo

            bool usrJadlogRodo = true;
            if (usrJadlogRodo)
            {
                var valoresJadlogRodo = rnFrete.CalculaFreteWebserviceJadlog(pesoTotal, cep, "jadlogrodo", valorTotalDoPedido);
                if (valoresJadlogRodo.valor <= 0)
                {
                    var faixasTotal = (from c in pedidoDc.tbFaixaDeCeps
                                       where c.tipodeEntregaId == 13
                                       select new
                                       {
                                           faixa = Convert.ToInt64(c.faixaInicial)
                                       }).ToList();
                    var proximaFaixa = (from c in faixasTotal where c.faixa > cep orderby c.faixa select c).FirstOrDefault();
                    if (proximaFaixa != null)
                    {
                        var valoresJadlog2 = rnFrete.CalculaFreteWebserviceJadlog(pesoTotal, Convert.ToInt64(proximaFaixa.faixa), "jadlogrodo", valorTotalDoPedido);
                        var valorTransporteJadlog = new tbPedidoEnvioValorTransporte();
                        valorTransporteJadlog.idPedidoEnvio = idPedidoEnvio;
                        valorTransporteJadlog.prazo = valoresJadlog2.prazo;
                        valorTransporteJadlog.servico = "jadlogrodo";
                        valorTransporteJadlog.valor = Convert.ToDecimal(valoresJadlog2.valor.ToString("0.00"));
                        pedidoDc.tbPedidoEnvioValorTransportes.InsertOnSubmit(valorTransporteJadlog);
                        pedidoDc.SubmitChanges();
                    }
                }
                else
                {
                    var valorTransporteJadlog = new tbPedidoEnvioValorTransporte();
                    valorTransporteJadlog.idPedidoEnvio = idPedidoEnvio;
                    valorTransporteJadlog.prazo = valoresJadlogRodo.prazo;
                    valorTransporteJadlog.servico = "jadlogrodo";
                    valorTransporteJadlog.valor = Convert.ToDecimal(valoresJadlogRodo.valor.ToString("0.00"));
                    pedidoDc.tbPedidoEnvioValorTransportes.InsertOnSubmit(valorTransporteJadlog);
                    pedidoDc.SubmitChanges();

                }
            }

            #endregion*/

            #region Calculo Correios
            //var consultaCorreios = rnFrete.CalculaFrete(12, Convert.ToInt32(pesoTotal), pedido.endCep.Replace("-", "").Trim(), valorTotalDoPedido, 0, 0, 0);
            //var valorTransporteCorreios = new tbPedidoEnvioValorTransporte();
            //valorTransporteCorreios.idPedidoEnvio = idPedidoEnvio;
            //valorTransporteCorreios.prazo = consultaCorreios.prazo;
            //valorTransporteCorreios.servico = "pac";
            //valorTransporteCorreios.valor = Convert.ToDecimal(consultaCorreios.valor.ToString("0.00"));
            //pedidoDc.tbPedidoEnvioValorTransportes.InsertOnSubmit(valorTransporteCorreios);
            //pedidoDc.SubmitChanges();
            #endregion


            #region Calculo TNT
            //var valoresTnt = rnFrete.CalculaFreteWebserviceTnt(pesoTotal, pedido.endCep, valorTotalDoPedido, cliente.clienteCPFCNPJ);
            var valoresTnt = rnFrete.CalculaFrete(10, Convert.ToInt32(pesoTotal), pedido.endCep.Replace("-", "").Trim(), valorTotalDoPedido, 0, 0, 0);

            //if (valoresTnt.valor <= 0)
            //{
            //    valoresTnt = rnFrete.CalculaFreteWebserviceTnt(pesoTotal, pedido.endCep, valorTotalDoPedido, cliente.clienteCPFCNPJ);
            //}

            if (valoresTnt.valor <= 0)
            {
                var faixasTotal = (from c in pedidoDc.tbFaixaDeCeps
                                   where c.tipodeEntregaId == 10 && c.ativo == true
                                   select new
                                   {
                                       faixa = Convert.ToInt64(c.faixaInicial)
                                   }).ToList();
                var proximaFaixa = (from c in faixasTotal where c.faixa > cep orderby c.faixa select c).FirstOrDefault();
                if (proximaFaixa != null)
                {
                    var valoresJadlog2 = rnFrete.CalculaFrete(10, Convert.ToInt32(pesoTotal), proximaFaixa.faixa.ToString(), valorTotalDoPedido, 0, 0, 0);
                    //var valoresJadlog2 = rnFrete.CalculaFreteWebserviceJadlog(pesoTotal, Convert.ToInt64(proximaFaixa.faixa), modalidadeJadlog, valorTotalDoPedido);
                    var valorTransporteTnt = new tbPedidoEnvioValorTransporte();
                    valorTransporteTnt.idPedidoEnvio = idPedidoEnvio;
                    valorTransporteTnt.prazo = valoresJadlog2.prazo;
                    valorTransporteTnt.servico = "tnt";
                    valorTransporteTnt.valor = Convert.ToDecimal(valoresJadlog2.valor.ToString("0.00"));
                    pedidoDc.tbPedidoEnvioValorTransportes.InsertOnSubmit(valorTransporteTnt);
                    pedidoDc.SubmitChanges();
                }
            }
            else
            {
                var valorTransporteTnt = new tbPedidoEnvioValorTransporte();
                valorTransporteTnt.idPedidoEnvio = idPedidoEnvio;
                valorTransporteTnt.prazo = valoresTnt.prazo;
                valorTransporteTnt.servico = "tnt";
                valorTransporteTnt.valor = Convert.ToDecimal(valoresTnt.valor.ToString("0.00"));
                pedidoDc.tbPedidoEnvioValorTransportes.InsertOnSubmit(valorTransporteTnt);
                pedidoDc.SubmitChanges();

            }
            #endregion



            var valores = (from c in pedidoDc.tbPedidoEnvioValorTransportes where c.valor > 0 && c.idPedidoEnvio == idPedidoEnvio select c);

            //if ((pedido.endEstado != "ES" && pedido.endEstado != "MG" && pedido.endEstado != "SP" && pedido.endEstado != "PR" && pedido.endEstado != "RS" && pedido.endEstado != "SC" && pedido.endEstado != "RJ" && pedido.endEstado != "GO" && pedido.endEstado != "DF") | pedido.tipoDePagamentoId == 10)
            //{
            //    envio.nfeObrigatoria = true;
            //}

            //if (pedido.endEstado == "RJ" && pedido.endCidade.Trim().ToLower() == "angra dos reis")
            //{
            //    envio.nfeObrigatoria = true;
            //}

            int prazoEntregaPedido = 0;
            int.TryParse(pedido.prazoDeEntrega, out prazoEntregaPedido);

            var valoresUtilizaveis = valores;



            var data = new dbCommerceDataContext();
            //var moveis = (from c in data.tbProdutoEstoques
            //              where c.idPedidoEnvio == envio.idPedidoEnvio && (c.tbProduto.produtoFornecedor == 72 | c.tbProduto.produtoFornecedor == 112)
            //              select c).Any();


            //valoresUtilizaveis = valoresUtilizaveis.Where(x => x.servico != "pac");
            //if (Convert.ToInt32(pesoTotal) >= 25000 | moveis)
            //{

            //}
            //else
            //{
            //    //valoresUtilizaveis.Where(x => x.servico != "tnt");
            //}



            var liberadas = (from c in data.tbTransportadoraLiberadas select c).First();

            if ((liberadas.jadlog == false && liberadas.tnt == true))
            {
                valoresUtilizaveis = valoresUtilizaveis.Where(x => x.servico != "jadlog");
                valoresUtilizaveis = valoresUtilizaveis.Where(x => x.servico != "jadlogexpressa");
                valoresUtilizaveis = valoresUtilizaveis.Where(x => x.servico != "jadlogcom");
                valoresUtilizaveis = valoresUtilizaveis.Where(x => x.servico != "jadlogrodo");
            }
            if (liberadas.jadlog == true && liberadas.tnt == false)
            {
                valoresUtilizaveis = valoresUtilizaveis.Where(x => x.servico != "tnt");
            }

            if (envio.nfeObrigatoria != true && liberadas.jadlog)
            {
                //comentado no dia 04/09/2015 - os calculos serão realizados tbm com base nos valores da TNT - Renato 
                //valoresUtilizaveis = valoresUtilizaveis.Where(x => x.servico != "tnt"); // Solicitado que quando nota não for obrigatório não enviar pela TNT - 05/11/2014 - André
                //valoresUtilizaveis = valoresUtilizaveis.Where(x => x.servico != "jadlogexpressa"); // Solicitado que quando nota não for obrigatório não enviar pela JadlogExpressa - 05/11/2014 - André
            }

            //if (produtosCubagem) valoresUtilizaveis = valoresUtilizaveis.Where(x => x.servico != "pac");
            if (pedido.formaDeEnvio == "jadlogexpressa") valoresUtilizaveis.Where(x => x.servico.ToLower() == "jadlog" && x.servico.ToLower() != "tnt");
            if (pedido.endEstado.ToLower() == "pe")//Pedidos Pernambuco sair pela Jadlog
            {
                valoresUtilizaveis = valoresUtilizaveis.Where(x => x.servico == "jadlog");// pega só jadlog
            }

            //if (pedido.endEstado.ToLower() == "ce" | 
            //    pedido.endEstado.ToLower() == "pb" |
            //    pedido.endEstado.ToLower() == "pb" |
            //    pedido.endEstado.ToLower() == "pi" |
            //    pedido.endEstado.ToLower() == "to" |
            //    pedido.endEstado.ToLower() == "rj" |
            //    pedido.endEstado.ToLower() == "ms"
            //    )//Pedidos GNRE não sair pela Jad
            //{
            //    valoresUtilizaveis = valoresUtilizaveis.Where(x => x.servico == "tnt");// pega só tnt
            //}
            //if (pedido.endEstado.ToLower() == "rj")//Pedidos GNRE não sair pela Jad
            //{
            //    valoresUtilizaveis = valoresUtilizaveis.Where(x => x.servico == "tnt");// pega só tnt
            //}

            /*
            if (envio.nfeObrigatoria == true)
            {
                var valorTntCalculado =
                    valoresUtilizaveis.OrderBy(x => x.valor).FirstOrDefault(x => x.servico.ToLower() == "tnt");
                var valorJadlogCalculado =
                    valoresUtilizaveis.OrderBy(x => x.valor).FirstOrDefault(x => x.servico.ToLower() == "jadlog");
                var valorJadlogExpressaCalculado =
                    valoresUtilizaveis.OrderBy(x => x.valor)
                        .FirstOrDefault(x => x.servico.ToLower() == "jadlogexpressa");
                if (valorJadlogExpressaCalculado != null)
                {
                    decimal valorTntComImposto = valorTntCalculado == null
                        ? 99999
                        : valorTntCalculado.valor + custoImpostos;
                    decimal valorJadlogComImposto = valorJadlogCalculado == null
                        ? 99999
                        : valorJadlogCalculado.valor + custoImpostos;
                    if (valorJadlogExpressaCalculado.valor < valorTntComImposto &&
                        valorJadlogExpressaCalculado.valor < valorJadlogComImposto)
                    {
                        valoresUtilizaveis =
                            valoresUtilizaveis.Where(
                                x => x.servico.ToLower() != "jadlog" && x.servico.ToLower() == "tnt");
                        envio.nfeObrigatoria = false;
                    }
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(envio.tbPedido.formaDeEnvio))
                {
                    if (envio.tbPedido.formaDeEnvio != "jadlogexpressa")
                    {
                        valoresUtilizaveis = valoresUtilizaveis.Where(x => x.servico.ToLower() != "jadlogexpressa");
                    }
                }
            }*/

            var valorSelecionado = valoresUtilizaveis.Where(x => x.valor > 0).OrderBy(x => x.valor).FirstOrDefault();
            if (valorSelecionado != null)
            {
                envio.valorSelecionado = valorSelecionado.valor;
                envio.prazoSelecionado = valorSelecionado.prazo;
                envio.formaDeEnvio = valorSelecionado.servico;
                if (valorSelecionado.servico.ToLower() == "tnt")
                {
                    envio.nfeObrigatoria = true;
                }
            }


            if (envio.valorSelecionado == null | envio.valorSelecionado == 0)
            {
                envio.valorSelecionado = 0;
                envio.prazoSelecionado = 5;
                envio.formaDeEnvio = "tnt";
            }


            if (!string.IsNullOrEmpty(envio.tbPedido.formaDeEnvio))
            {
                envio.valorSelecionado = valoresJadlog.valor;
                envio.prazoSelecionado = valoresJadlog.prazo;
                envio.formaDeEnvio = envio.tbPedido.formaDeEnvio.ToLower();
            }

            if (pedido.endEstado.ToLower() == "pe")//Pedidos Pernambuco sair pela Jadlog
            {
                envio.valorSelecionado = 0;
                envio.prazoSelecionado = 10;
                envio.formaDeEnvio = "jadlog";
            }


            if (envio.tbPedido.gerarReversa)
            {
                envio.valorSelecionado = valoresJadlog.valor;
                envio.prazoSelecionado = valoresJadlog.prazo;
                envio.formaDeEnvio = "jadlog";

                //if (!string.IsNullOrEmpty(envio.tbPedido.formaDeEnvio))
                //{
                //    envio.formaDeEnvio = envio.tbPedido.formaDeEnvio.ToLower() == "jadlogexpressa" ? "jadlogexpressa" : "jadlog";
                //}
            }

            //if (envio.formaDeEnvio == "jadlogexpressa")
            //{
            //    envio.nfeObrigatoria = false;
            //    envio.formaDeEnvio = "jadlogexpressa";
            //}

            foreach (var volume in volumes)
            {
                volume.formaDeEnvio = envio.formaDeEnvio;
            }
            envio.nfeObrigatoria = true;


            var dataAlt = new dbCommerceDataContext();
            var envioAlt = (from c in dataAlt.tbPedidoEnvios where c.idPedidoEnvio == envio.idPedidoEnvio select c).First();
            envioAlt.nfeObrigatoria = envio.nfeObrigatoria;
            envioAlt.valorSelecionado = envio.valorSelecionado;
            envioAlt.prazoSelecionado = envio.prazoSelecionado;
            envioAlt.formaDeEnvio = envio.formaDeEnvio;
            envioAlt.valorSelecionado = envio.valorSelecionado;

            var dataValidacaoNota = DateTime.Now.AddDays(-10);
            if (envioAlt.nfeNumero == null | envioAlt.nfeNumero == 0)
            {
                var notaEmitida = (from c in dataAlt.tbNotaFiscals
                                   where c.idPedido == pedidoId && c.dataHora > dataValidacaoNota && (c.idPedidoEnvio == 0 | c.idPedidoEnvio == null)
                                   orderby c.idNotaFiscal descending
                                   select c).FirstOrDefault();
                if (notaEmitida != null)
                {
                    envioAlt.nfeStatus = notaEmitida.statusNfe;
                    envioAlt.nfeKey = notaEmitida.nfeKey;
                    envioAlt.nfeValor = notaEmitida.valor;
                    notaEmitida.idPedidoEnvio = envioAlt.idPedidoEnvio;
                    dataAlt.SubmitChanges();
                }
            }
            dataAlt.SubmitChanges();

            string retornoEnvio = envio.formaDeEnvio;
            if (retornoEnvio == "jadlogcom" | retornoEnvio == "jadlogrodo") retornoEnvio = "jadlog";
            return retornoEnvio;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }

    }

    public static string SimulaEntregaPedido(int pedidoId, int idPedidoEnvio)
    {
        var pedidoDc = new dbCommerceDataContext();
        var clienteDc = new dbCommerceDataContext();
        var pedido = (from c in pedidoDc.tbPedidos where c.pedidoId == pedidoId select c).First();
        var cliente = (from c in clienteDc.tbClientes where c.clienteId == pedido.clienteId select c).First();
        var volumes = (from c in pedidoDc.tbPedidoPacotes where c.idPedido == pedidoId && c.idPedidoEnvio == idPedidoEnvio select c);
        var envio = (from c in pedidoDc.tbPedidoEnvios where c.idPedidoEnvio == idPedidoEnvio select c).First();
        var produtosEnvio = (from c in pedidoDc.tbProdutoEstoques where c.idPedidoEnvio == idPedidoEnvio select c.produtoId).ToList();
        var produtosDc = new dbCommerceDataContext();
        var produtosCubagem =
            (from c in produtosDc.tbProdutos
             where produtosEnvio.Contains(c.produtoId) && c.cubagem.ToLower() == "true"
             select c).Any();
        var produtosPrecos =
            (from c in produtosDc.tbProdutos
             where produtosEnvio.Contains(c.produtoId)
             select new { preco = (c.produtoPrecoPromocional ?? 0) > c.produtoPreco ? c.produtoPrecoPromocional : c.produtoPreco });
        decimal valorTotalDoPedido = 0;
        if (produtosPrecos.Any())
        {
            valorTotalDoPedido = produtosPrecos.Sum(x => (decimal)x.preco);
        }
        else
        {
            valorTotalDoPedido = (decimal)pedido.valorCobrado;
        }
        int pesoTotal = volumes.Sum(x => x.peso);
        long cep = Convert.ToInt64(pedido.endCep.Replace("-", "").Trim());

        string Retorno = "";

        #region Calculo na Jadlog
        string modalidadeJadlog = "";
        if (pedido.formaDeEnvio == "jadlogexpressa") modalidadeJadlog = "jadlogexpressa";

        var valoresJadlog = rnFrete.CalculaFrete(13, Convert.ToInt32(pesoTotal), pedido.endCep.Replace("-", "").Trim(), valorTotalDoPedido, 0, 0, 0);

        if (modalidadeJadlog == "jadlogexpressa")
        {
            valoresJadlog = rnFrete.CalculaFreteWebserviceJadlog(pesoTotal, cep, modalidadeJadlog, valorTotalDoPedido);
        }
        var valoresJadlogWs = rnFrete.CalculaFreteWebserviceJadlog(pesoTotal, cep, modalidadeJadlog, valorTotalDoPedido);
        Retorno += "Valor jadlog tabela: " + valoresJadlog.valor.ToString("C") + "<br>";
        Retorno += "Valor jadlog ws: " + valoresJadlogWs.valor.ToString("C") + "<br>";
        #endregion

        #region Calculo TNT
        var valoresTnt = rnFrete.CalculaFrete(10, Convert.ToInt32(pesoTotal), pedido.endCep.Replace("-", "").Trim(), valorTotalDoPedido, 0, 0, 0);
        var valoresTntws = rnFrete.CalculaFreteWebserviceTnt(pesoTotal, pedido.endCep, valorTotalDoPedido, cliente.clienteCPFCNPJ);
        Retorno += "Valor tnt tabela: " + valoresTnt.valor.ToString("C") + "<br>";
        Retorno += "Valor tnt ws: " + valoresTntws.valor.ToString("C") + "<br>";

        #endregion

        return Retorno;
    }

    public static string SimulaEntregaPedidoMoveis()
    {
        var data = new dbCommerceDataContext();
        var pedidos = (from c in data.tbPedidoPacotes
                       join e in data.tbItensPedidoCombos on c.idPedido equals e.tbItensPedido.pedidoId
                       join d in data.tbProdutos on e.produtoId equals d.produtoId
                       where d.produtoFornecedor == 72 && c.idPedidoEnvio != null
                       select c.idPedidoEnvio).ToList().Distinct().ToList();

        var pedidosNaoCombo = (from c in data.tbPedidoPacotes
                               join e in data.tbItensPedidos on c.idPedido equals e.pedidoId
                               join d in data.tbProdutos on e.produtoId equals d.produtoId
                               where d.produtoFornecedor == 72 && c.idPedidoEnvio != null
                               select c.idPedidoEnvio).ToList().Distinct().ToList();

        pedidos.AddRange(pedidosNaoCombo);

        pedidos = pedidos.Distinct().ToList();

        decimal totalDiferenca = 0;
        decimal totalMoveis = 0;
        decimal totalOutros = 0;
        decimal totalMoveisOutros = 0;
        decimal totalNormal = 0;

        string Retorno = "";

        foreach (var pacoteEnvio in pedidos)
        {
            var envio = (from c in data.tbPedidoEnvios where c.idPedidoEnvio == pacoteEnvio select c).FirstOrDefault();
            if (envio != null)
            {
                try
                {
                    var pedidoDc = new dbCommerceDataContext();
                    var clienteDc = new dbCommerceDataContext();
                    var pedido = (from c in pedidoDc.tbPedidos where c.pedidoId == envio.idPedido select c).First();
                    var cliente = (from c in clienteDc.tbClientes where c.clienteId == pedido.clienteId select c).First();
                    var volumes = (from c in pedidoDc.tbPedidoPacotes where c.idPedido == envio.idPedido && c.idPedidoEnvio == envio.idPedidoEnvio select c);
                    var produtosEnvio = (from c in pedidoDc.tbProdutoEstoques where c.idPedidoEnvio == envio.idPedidoEnvio select c.produtoId).ToList();
                    var produtosDc = new dbCommerceDataContext();
                    var produtosCubagem =
                        (from c in produtosDc.tbProdutos
                         where produtosEnvio.Contains(c.produtoId) && c.cubagem.ToLower() == "true"
                         select c).Any();
                    var produtosPrecos =
                        (from c in produtosDc.tbProdutos
                         where produtosEnvio.Contains(c.produtoId)
                         select new { preco = (c.produtoPrecoPromocional ?? 0) > c.produtoPreco ? c.produtoPrecoPromocional : c.produtoPreco });
                    decimal valorTotalDoPedido = 0;
                    if (produtosPrecos.Any())
                    {
                        valorTotalDoPedido = produtosPrecos.Sum(x => (decimal)x.preco);
                    }
                    else
                    {
                        valorTotalDoPedido = (decimal)pedido.valorCobrado;
                    }
                    int pesoTotalMoveis = volumes.Where(x => x.peso >= 30000).Sum(x => x.peso);
                    int pesoTotalOutros = volumes.Where(x => x.peso < 30000).Sum(x => x.peso);
                    int pesoTotalTotal = volumes.Sum(x => x.peso);


                    long cep = Convert.ToInt64(pedido.endCep.Replace("-", "").Trim());


                    #region Calculo na Jadlog
                    string modalidadeJadlog = "";
                    if (pedido.formaDeEnvio == "jadlogexpressa") modalidadeJadlog = "jadlogexpressa";
                    var valoresJadlogMoveis = rnFrete.CalculaFrete(13, Convert.ToInt32(pesoTotalMoveis), pedido.endCep.Replace("-", "").Trim(), valorTotalDoPedido, 0, 0, 0);
                    if (modalidadeJadlog == "jadlogexpressa")
                    {
                        valoresJadlogMoveis = rnFrete.CalculaFreteWebserviceJadlog(pesoTotalMoveis, cep, modalidadeJadlog, valorTotalDoPedido);
                    }
                    //var valoresJadlogWsMoveis = rnFrete.CalculaFreteWebserviceJadlog(pesoTotalMoveis, cep, modalidadeJadlog, valorTotalDoPedido);
                    Retorno += "Valor jadlog moveis: " + valoresJadlogMoveis.valor.ToString("C") + "<br>";
                    //Retorno += "Valor jadlog ws: " + valoresJadlogWsMoveis.valor.ToString("C") + "<br>";


                    var valoresJadlogOutros = rnFrete.CalculaFrete(13, Convert.ToInt32(pesoTotalOutros), pedido.endCep.Replace("-", "").Trim(), valorTotalDoPedido, 0, 0, 0);
                    if (modalidadeJadlog == "jadlogexpressa")
                    {
                        valoresJadlogMoveis = rnFrete.CalculaFreteWebserviceJadlog(pesoTotalOutros, cep, modalidadeJadlog, valorTotalDoPedido);
                    }
                    //var valoresJadlogWsOutros = rnFrete.CalculaFreteWebserviceJadlog(pesoTotalOutros, cep, modalidadeJadlog, valorTotalDoPedido);
                    Retorno += "Valor jadlog outros: " + valoresJadlogOutros.valor.ToString("C") + "<br>";
                    // Retorno += "Valor jadlog ws: " + valoresJadlogWs.valor.ToString("C") + "<br>";

                    var valoresJadlogTotal = rnFrete.CalculaFrete(13, Convert.ToInt32(pesoTotalTotal), pedido.endCep.Replace("-", "").Trim(), valorTotalDoPedido, 0, 0, 0);
                    if (modalidadeJadlog == "jadlogexpressa")
                    {
                        valoresJadlogMoveis = rnFrete.CalculaFreteWebserviceJadlog(pesoTotalTotal, cep, modalidadeJadlog, valorTotalDoPedido);
                    }
                    //var valoresJadlogWsOutros = rnFrete.CalculaFreteWebserviceJadlog(pesoTotalOutros, cep, modalidadeJadlog, valorTotalDoPedido);
                    Retorno += "Valor jadlog total: " + valoresJadlogTotal.valor.ToString("C") + "<br>";
                    Retorno += "Valor jadlog diferença: " + (valoresJadlogOutros.valor + valoresJadlogMoveis.valor - valoresJadlogTotal.valor).ToString("C") + "<br>";
                    // Retorno += "Valor jadlog ws: " + valoresJadlogWs.valor.ToString("C") + "<br>";



                    #endregion


                    #region Calculo TNT
                    var valoresTntMoveis = rnFrete.CalculaFrete(10, Convert.ToInt32(pesoTotalMoveis), pedido.endCep.Replace("-", "").Trim(), valorTotalDoPedido, 0, 0, 0);
                    //var valoresTntws = rnFrete.CalculaFreteWebserviceTnt(pesoTotalMoveis, pedido.endCep, valorTotalDoPedido, cliente.clienteCPFCNPJ);
                    Retorno += "Valor tnt moveis: " + valoresTntMoveis.valor.ToString("C") + "<br>";
                    //Retorno += "Valor tnt ws: " + valoresTntws.valor.ToString("C") + "<br>";

                    var valoresTntOutros = rnFrete.CalculaFrete(10, Convert.ToInt32(pesoTotalOutros), pedido.endCep.Replace("-", "").Trim(), valorTotalDoPedido, 0, 0, 0);
                    //var valoresTntws = rnFrete.CalculaFreteWebserviceTnt(pesoTotal, pedido.endCep, valorTotalDoPedido, cliente.clienteCPFCNPJ);
                    Retorno += "Valor tnt outros: " + valoresTntOutros.valor.ToString("C") + "<br>";
                    //Retorno += "Valor tnt ws: " + valoresTntws.valor.ToString("C") + "<br>";

                    var valoresTntTotal = rnFrete.CalculaFrete(10, Convert.ToInt32(pesoTotalTotal), pedido.endCep.Replace("-", "").Trim(), valorTotalDoPedido, 0, 0, 0);
                    //var valoresTntws = rnFrete.CalculaFreteWebserviceTnt(pesoTotal, pedido.endCep, valorTotalDoPedido, cliente.clienteCPFCNPJ);
                    Retorno += "Valor tnt total: " + valoresTntTotal.valor.ToString("C") + "<br>";
                    //Retorno += "Valor tnt ws: " + valoresTntws.valor.ToString("C") + "<br>";
                    Retorno += "Valor tnt diferença: " + (valoresTntMoveis.valor + valoresTntOutros.valor - valoresTntTotal.valor).ToString("C") + "<br>";
                    var moveisEscolhido = valoresTntMoveis.valor < valoresJadlogMoveis.valor
                        ? valoresTntMoveis.valor
                        : valoresJadlogMoveis.valor;
                    var outrosEscolhido = valoresJadlogMoveis.valor < valoresJadlogOutros.valor
                        ? valoresJadlogMoveis.valor
                        : valoresJadlogOutros.valor;
                    var totalEscolhido = valoresTntTotal.valor < valoresJadlogTotal.valor
                        ? valoresTntTotal.valor
                        : valoresJadlogTotal.valor;

                    totalMoveis += moveisEscolhido;
                    totalOutros += outrosEscolhido;
                    totalMoveisOutros += moveisEscolhido + outrosEscolhido;
                    totalNormal += totalEscolhido;

                    Retorno += "Valor escolhidos diferença: " + (moveisEscolhido + outrosEscolhido - totalEscolhido).ToString("C") + "<br>";
                    Retorno += "<br><br>";

                    totalDiferenca += (moveisEscolhido + outrosEscolhido - totalEscolhido);

                    #endregion
                }
                catch (Exception)
                {

                }
                Retorno += "<br><br>";

            }
        }



        Retorno += "Total da Diferença: " + totalDiferenca.ToString("C") + "<br>";
        Retorno += "Total Moveis: " + totalMoveis.ToString("C") + "<br>";
        Retorno += "Total Outros: " + totalOutros.ToString("C") + "<br>";
        Retorno += "Total Normal: " + totalNormal.ToString("C") + "<br>";


        Retorno += "<br><br>";

        return Retorno;
    }

    public static void AtualizaDescontoPedido(int pedidoId)
    {
        var data = new dbCommerceDataContext();
        var pedido = (from c in data.tbPedidos where c.pedidoId == pedidoId select c).First();
        decimal descontoCobrancas = 0;

        var pagamentos = (from c in data.tbPedidoPagamentos where c.cancelado == false && c.pedidoId == pedidoId && c.valorDesconto > 0 select c).ToList();
        if (pagamentos.Any())
        {
            descontoCobrancas = pagamentos.Sum(x => x.valorDesconto == null ? 0 : (decimal)x.valorDesconto);
        }

        decimal descontoAntigo = Convert.ToDecimal(pedido.valorDoDescontoDoPagamento);
        decimal diferencaDesconto = descontoAntigo - descontoCobrancas;
        decimal novoTotal = Convert.ToDecimal(pedido.valorCobrado) + diferencaDesconto;

        if (descontoAntigo != descontoCobrancas)
        {
            string interacao = "Desconto do pedido atualizado " + Convert.ToDecimal(pedido.valorDoDescontoDoPagamento).ToString("C") + " para " + descontoCobrancas.ToString("C") + "<br>";
            interacao += "Total do pedido atualizado de " + Convert.ToDecimal(pedido.valorCobrado).ToString("C") + " para " + novoTotal.ToString("C") + "<br>";
            rnInteracoes.interacaoInclui(pedidoId, interacao, rnUsuarios.retornaNomeUsuarioLogado(), "False");

            pedido.valorDoDescontoDoPagamento = descontoCobrancas;
            pedido.valorCobrado = novoTotal;
            data.SubmitChanges();
        }

        rnPagamento.atualizaDescontoMaximoPedido(pedidoId);
    }



    public static void AdicionaQueueAtualizaPrazo(int pedidoId)
    {
        var data = new dbCommerceDataContext();
        var queue = new tbQueue();
        queue.agendamento = DateTime.Now;
        queue.andamento = false;
        queue.concluido = false;
        queue.idRelacionado = pedidoId;
        queue.mensagem = "";
        queue.tipoQueue = 5;
        data.tbQueues.InsertOnSubmit(queue);
        data.SubmitChanges();
        data.Dispose();
    }

    public static void AdicionaQueueChecagemCompleto(int pedidoId)
    {
        var data = new dbCommerceDataContext();
        var queue = new tbQueue();
        queue.agendamento = DateTime.Now;
        queue.andamento = false;
        queue.concluido = false;
        queue.idRelacionado = pedidoId;
        queue.mensagem = "";
        queue.tipoQueue = 8;
        data.tbQueues.InsertOnSubmit(queue);
        data.SubmitChanges();
        data.Dispose();
    }
}
