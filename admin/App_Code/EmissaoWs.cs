﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Threading;
using System.Web;
using System.Web.Services;
using System.Xml;

/// <summary>
/// Summary description for EmissaoWs
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class EmissaoWs : System.Web.Services.WebService
{

    public EmissaoWs()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }


    public class detalhesEmissaoTnt
    {
        public string danfe { get; set; }
        public bool volumeCarregado { get; set; }
        public bool pedidoTotalmenteCarregado { get; set; }
        public int volumesFaltando { get; set; }
        public string gnre { get; set; }
        public string comprovantePagamentoGnre { get; set; }
    }

    [WebMethod]
    public detalhesEmissaoTnt CarregaVolumePac(int idPedido, int numerovolume, int idUsuario)
    {
        var retorno = new detalhesEmissaoTnt();

        try
        {
            var data = new dbCommerceDataContext();
            var volumeCarregar =
                (from c in data.tbPedidoPacotes
                 where c.idPedido == idPedido && c.numeroVolume == numerovolume
                 orderby c.idPedidoPacote descending
                 select c).First();
            volumeCarregar.despachado = true;
            volumeCarregar.dataDeCarregamento = DateTime.Now;
            data.SubmitChanges();
            rnInteracoes.interacaoInclui(volumeCarregar.idPedido, "Volume " + numerovolume + " carregado", rnUsuarios.usuarioSeleciona_PorUsuarioId(idUsuario).Tables[0].Rows[0]["usuarioNome"].ToString(), "False");

            var pedidoPacotes = (from c in data.tbPedidoPacotes where c.idPedidoEnvio == volumeCarregar.idPedidoEnvio select c).ToList();
            var volumes = (from c in pedidoPacotes where c.idPedidoEnvio == volumeCarregar.idPedidoEnvio && (c.despachado ?? false) == false select c).ToList();

            if (volumes.Count == 0)
            {
                retorno.pedidoTotalmenteCarregado = true;
            }
            retorno.volumeCarregado = true;
            retorno.volumesFaltando = volumes.Count;
            return retorno;
        }
        catch (Exception)
        {
            retorno.volumeCarregado = false;
            return retorno;
        }
    }

    [WebMethod]
    public detalhesEmissaoTnt CarregaVolumeTnt(string cnpj, int numeronota, int numerovolume, int idUsuario)
    {
        var retorno = new detalhesEmissaoTnt();

        try
        {
            var data = new dbCommerceDataContext();
            var empresa = (from c in data.tbEmpresas where c.cnpj == cnpj select c).First();
            var nota = (from c in data.tbNotaFiscals where c.idCNPJ == empresa.idEmpresa && c.numeroNota == numeronota select c).First();
            var volumeCarregar =
                (from c in data.tbPedidoPacotes
                 where c.idPedidoEnvio == nota.idPedidoEnvio && c.numeroVolume == numerovolume
                 select c).First();
            volumeCarregar.despachado = true;
            volumeCarregar.dataDeCarregamento = DateTime.Now;
            data.SubmitChanges();
            rnInteracoes.interacaoInclui(volumeCarregar.idPedido, "Volume " + numerovolume + " carregado", rnUsuarios.usuarioSeleciona_PorUsuarioId(idUsuario).Tables[0].Rows[0]["usuarioNome"].ToString(), "False");

            var pedidoPacotes = (from c in data.tbPedidoPacotes where c.idPedidoEnvio == nota.idPedidoEnvio select c).ToList();
            var volumes = (from c in pedidoPacotes where c.idPedidoEnvio == nota.idPedidoEnvio && (c.despachado ?? false) == false select c).ToList();

            if (volumes.Count == 0)
            {
                if (!string.IsNullOrEmpty(nota.linkDanfe))
                {
                    if (string.IsNullOrEmpty(nota.danfeBase64))
                    {
                        retorno.danfe = rnNotaFiscal.retornaDanfeBase64NotaPorNumeroNota(nota.numeroNota.ToString(), nota.idNotaFiscal);
                    }
                    else
                    {
                        retorno.danfe = nota.danfeBase64;
                    }
                    if (nota.statusGnre == 2)
                    {
                        retorno.gnre = "http://www.graodegente.com.br/admin/gnre.aspx?id=" + nota.idNotaFiscal;
                    }
                    if (nota.statusPagamentoGnre == 2)
                    {
                        retorno.gnre = "http://www.graodegente.com.br/admin/gnreRecibo.aspx?id=" + nota.idNotaFiscal;
                    }
                }
                retorno.pedidoTotalmenteCarregado = true;


                int pesoTotal = pedidoPacotes.Sum(x => x.peso);
                string cep = volumeCarregar.tbPedido.endCep.Replace("-", "").Trim();
                var valorTotalDoPedido = Convert.ToDecimal(volumeCarregar.tbPedido.valorCobrado);
                //var clienteCPFCNPJ = (from c in data.tbClientes where c.clienteId == volumeCarregar.tbPedido.clienteId select c).FirstOrDefault().clienteCPFCNPJ;
                //var valoresTnt = rnFrete.CalculaFreteWebserviceTnt(pesoTotal, cep, valorTotalDoPedido, clienteCPFCNPJ);
                var valoresTnt = rnFrete.CalculaFrete(10, Convert.ToInt32(pesoTotal), cep.Replace("-", "").Trim(), valorTotalDoPedido, 0, 0, 0);
                if (valoresTnt.prazo > 0)
                {
                    var diasUteisPrazo = rnFuncoes.retornaPrazoDiasUteis((valoresTnt.prazo + 1), 0);
                    var prazoFinal = DateTime.Now.AddDays(diasUteisPrazo);
                    foreach (var pacote in pedidoPacotes)
                    {
                        pacote.prazoFinalEntrega = prazoFinal;
                        data.SubmitChanges();
                    }
                }
            }
            retorno.volumeCarregado = true;
            retorno.volumesFaltando = volumes.Count;
            return retorno;
        }
        catch (Exception)
        {
            retorno.volumeCarregado = false;
            return retorno;
        }
    }

    [WebMethod]
    public bool NotaImpressaTnt(string cnpj, int numeronota, int idUsuario)
    {
        try
        {
            var data = new dbCommerceDataContext();
            var empresa = (from c in data.tbEmpresas where c.cnpj == cnpj select c).First();
            var nota = (from c in data.tbNotaFiscals where c.idCNPJ == empresa.idEmpresa && c.numeroNota == numeronota select c).First();
            var envio = (from c in data.tbPedidoEnvios where c.idPedidoEnvio == nota.idPedidoEnvio select c).First();
            envio.nfeImpressa = true;
            data.SubmitChanges();
            rnInteracoes.interacaoInclui(nota.idPedido, "Nota fiscal marcada como impressa", rnUsuarios.usuarioSeleciona_PorUsuarioId(idUsuario).Tables[0].Rows[0]["usuarioNome"].ToString(), "False");

            string xmlNota = "";
            if (string.IsNullOrEmpty(nota.xmlBase64))
            {
                xmlNota = rnNotaFiscal.retornaXmlNota(nota.numeroNota, nota.idNotaFiscal);
            }
            else
            {
                xmlNota = rnNotaFiscal.retornaXmlNotaFromBase64(nota.xmlBase64);
            }

            if (envio.formaDeEnvio == "belle")
            {
                var queueNota = new tbQueue();
                queueNota.agendamento = DateTime.Now;
                queueNota.idRelacionado = numeronota;
                queueNota.tipoQueue = 27;
                queueNota.concluido = false;
                queueNota.andamento = false;
                queueNota.mensagem = xmlNota;
                data.tbQueues.InsertOnSubmit(queueNota);
                data.SubmitChanges();
            }
            else if (envio.formaDeEnvio == "plimor")
            {
                var queueNota = new tbQueue();
                queueNota.agendamento = DateTime.Now;
                queueNota.idRelacionado = numeronota;
                queueNota.tipoQueue = 41;
                queueNota.concluido = false;
                queueNota.andamento = false;
                queueNota.mensagem = xmlNota;
                data.tbQueues.InsertOnSubmit(queueNota);
                data.SubmitChanges();
            }
            else if (envio.formaDeEnvio == "dialogo")
            {
                var queueNota = new tbQueue();
                queueNota.agendamento = DateTime.Now;
                queueNota.idRelacionado = numeronota;
                queueNota.tipoQueue = 44;
                queueNota.concluido = false;
                queueNota.andamento = false;
                queueNota.mensagem = xmlNota;
                data.tbQueues.InsertOnSubmit(queueNota);
                data.SubmitChanges();
            }
            else if (envio.formaDeEnvio == "nowlog")
            {
                var queueNota = new tbQueue();
                queueNota.agendamento = DateTime.Now;
                queueNota.idRelacionado = numeronota;
                queueNota.tipoQueue = 44;
                queueNota.concluido = false;
                queueNota.andamento = false;
                queueNota.mensagem = xmlNota;
                data.tbQueues.InsertOnSubmit(queueNota);
                data.SubmitChanges();
            }
            else if (envio.formaDeEnvio == "totalexpress")
            {
                var queueNota = new tbQueue();
                queueNota.agendamento = DateTime.Now;
                queueNota.idRelacionado = numeronota;
                queueNota.tipoQueue = 44;
                queueNota.concluido = false;
                queueNota.andamento = false;
                queueNota.mensagem = xmlNota;
                data.tbQueues.InsertOnSubmit(queueNota);
                data.SubmitChanges();
            }
            else if (envio.formaDeEnvio == "transfolha")
            {
                var queueNota = new tbQueue();
                queueNota.agendamento = DateTime.Now;
                queueNota.idRelacionado = numeronota;
                queueNota.tipoQueue = 44;
                queueNota.concluido = false;
                queueNota.andamento = false;
                queueNota.mensagem = xmlNota;
                data.tbQueues.InsertOnSubmit(queueNota);
                data.SubmitChanges();
            }
            else
            {
                var queueNota = new tbQueue();
                queueNota.agendamento = DateTime.Now;
                queueNota.idRelacionado = numeronota;
                queueNota.tipoQueue = 3;
                queueNota.concluido = false;
                queueNota.andamento = false;
                queueNota.mensagem = xmlNota;
                data.tbQueues.InsertOnSubmit(queueNota);
                data.SubmitChanges();
            }


            var queue = new tbQueue();
            queue.agendamento = DateTime.Now;
            queue.idRelacionado = nota.idPedidoEnvio;
            queue.tipoQueue = 2;
            queue.concluido = false;
            queue.andamento = false;
            queue.mensagem = "";
            data.tbQueues.InsertOnSubmit(queue);
            data.SubmitChanges();

            return true;

        }
        catch (Exception)
        {
            return false;
        }
    }


    [WebMethod]
    public string HelloWorld()
    {
        return "Hello World";
    }

    [WebMethod]
    public string ContaEmissoesPendentes(string chave, int idCentroDistribuicao)
    {
        if (chave != "glmp152029") return "0";

        var data = new dbCommerceDataContext();


        var pedidosJad = (from c in data.tbPedidoEnvios
                          join d in data.tbPedidoPacotes on c.idPedidoEnvio equals d.idPedidoEnvio into pacotes
                          join e in data.tbClientes on c.tbPedido.clienteId equals e.clienteId
                          where (pacotes.Where(x => x.codJadlog != "" && x.codJadlog != null && (x.rastreio == "" | x.rastreio == null)).Count() > 0)
                          orderby c.prioridade ?? 5, c.dataFimEmbalagem
                          select c);
        if (idCentroDistribuicao > 1)
        {
            if (idCentroDistribuicao == 4 | idCentroDistribuicao == 5)
            {
                pedidosJad = pedidosJad.Where(x => x.idCentroDeDistribuicao == 4 | x.idCentroDeDistribuicao == 5);
            }
            else
            {
                pedidosJad = pedidosJad.Where(x => x.idCentroDeDistribuicao == idCentroDistribuicao);
            }

        }
        else
        {
            pedidosJad = pedidosJad.Where(x => x.idCentroDeDistribuicao <= idCentroDistribuicao);
        }
        return pedidosJad.Count().ToString();
    }

    [WebMethod]
    public RetornoEmissao RetornaProximaEmissao(string chave, int idCentroDistribuicao, int idPedidoEnvio, int pedidoId)
    {
        if (chave != "glmp152029") return null;

        var data = new dbCommerceDataContext();

        var pedidosJad = (from c in data.tbPedidoEnvios
                          join d in data.tbPedidoPacotes on c.idPedidoEnvio equals d.idPedidoEnvio into pacotes
                          join e in data.tbClientes on c.tbPedido.clienteId equals e.clienteId
                          join produto in data.tbProdutoEstoques on c.idPedidoEnvio equals produto.idPedidoEnvio into produtos
                          where (pacotes.Where(x => x.codJadlog != "" && x.codJadlog != null && (x.rastreio == "" | x.rastreio == null)).Count() > 0)
                          orderby (c.idCentroDeDistribuicao > 1 ? produtos.OrderBy(x => x.produtoId).First().produtoId : 0), c.prioridade ?? 5, c.dataFimEmbalagem
                          select new RetornoEmissao
                          {
                              pedidoId = c.tbPedido.pedidoId,
                              dataFimEmbalagem = c.dataFimEmbalagem ?? DateTime.Now,
                              nfeNumero = c.nfeNumero ?? 0,
                              idPedidoEnvio = c.idPedidoEnvio,
                              clienteCPFCNPJ = e.clienteCPFCNPJ,
                              endNomeDoDestinatario = c.tbPedido.endNomeDoDestinatario,
                              endRua = c.tbPedido.endRua,
                              endNumero = c.tbPedido.endNumero,
                              endComplemento = c.tbPedido.endComplemento ?? "",
                              endCidade = c.tbPedido.endCidade,
                              endBairro = c.tbPedido.endBairro,
                              endEstado = c.tbPedido.endEstado,
                              endCep = c.tbPedido.endCep,
                              formaDeEnvio = c.formaDeEnvio,
                              gerarReversa = c.tbPedido.gerarReversa,
                              totalPacotes = pacotes.Count(),
                              clienteFoneResidencial = e.clienteFoneResidencial == "" ? e.clienteFoneCelular : e.clienteFoneResidencial,
                              prioridade = c.prioridade ?? 5,
                              idCentroDistribuicao = c.idCentroDeDistribuicao,
                              pesos = pacotes.Select(x => ((decimal)x.peso) / 1000).ToList(),
                              nota = c.nfeNumero ?? 0,
                              danfe = "",
                              produtos = produtos.Select(x => x.tbProduto.produtoNome).ToList(),
                              codJadlog = pacotes.First().codJadlog,
                              cnpj = ""
                          });
        if (idPedidoEnvio > 0)
        {
            pedidosJad = pedidosJad.Where(x => x.idPedidoEnvio == idPedidoEnvio);
        }
        if (pedidoId > 0)
        {
            pedidosJad = pedidosJad.Where(x => x.pedidoId == pedidoId);
        }
        if (idCentroDistribuicao > 1)
        {
            if (idCentroDistribuicao == 4 | idCentroDistribuicao == 5)
            {
                pedidosJad = pedidosJad.Where(x => x.idCentroDistribuicao == 4 | x.idCentroDistribuicao == 5);
            }
            else
            {
                pedidosJad = pedidosJad.Where(x => x.idCentroDistribuicao == idCentroDistribuicao);
            }
        }
        else
        {
            pedidosJad = pedidosJad.Where(x => x.idCentroDistribuicao <= idCentroDistribuicao);
        }

        var pedidoJad = pedidosJad.FirstOrDefault();
        if (pedidoJad != null)
        {
            pedidoJad.pesos = new List<decimal>();
            var pacotes = (from c in data.tbPedidoPacotes where c.idPedidoEnvio == pedidoJad.idPedidoEnvio select c).ToList();
            foreach (var pacote in pacotes)
            {
                var cubagem = Math.Round((Convert.ToDecimal(pacote.altura * pacote.largura * pacote.profundidade) / 6000), 2);
                if (cubagem > 10 && cubagem > (Convert.ToDecimal(pacote.peso) / 1000))
                {
                    pedidoJad.pesos.Add(cubagem);
                }
                else
                {
                    pedidoJad.pesos.Add(Math.Round((Convert.ToDecimal(pacote.peso) / 1000), 2));
                }
            }
            var nota =
                (from c in data.tbNotaFiscals where c.idPedidoEnvio == pedidoJad.idPedidoEnvio select c).FirstOrDefault();
            if (nota != null)
            {
                if (!string.IsNullOrEmpty(nota.linkDanfe))
                {
                    pedidoJad.cnpj = nota.tbEmpresa.cnpj;
                    if (string.IsNullOrEmpty(nota.danfeBase64))
                    {
                        pedidoJad.danfe = rnNotaFiscal.retornaDanfeBase64NotaPorNumeroNota(nota.numeroNota.ToString(), nota.idNotaFiscal);
                    }
                    else
                    {
                        pedidoJad.danfe = nota.danfeBase64;
                    }
                    pedidoJad.nfeNumero = nota.numeroNota;
                    if (nota.statusGnre == 2)
                    {
                        pedidoJad.gnre = "http://www.graodegente.com.br/admin/gnre.aspx?id=" + nota.idNotaFiscal;
                    }
                    if (nota.statusPagamentoGnre == 2)
                    {
                        pedidoJad.gnre = "http://www.graodegente.com.br/admin/gnreRecibo.aspx?id=" + nota.idNotaFiscal;
                    }
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }
        return pedidoJad;
    }


    [WebMethod]
    public void AtualizaEntregasPedido( int idPedidoEnvio)
    {
        rnEntrega.AtualizaEntregasPedido(idPedidoEnvio);
    }

    [WebMethod]
    public List<RetornoEmissao> ListaEmissoes(string chave, int idCentroDistribuicao)
    {
        if (chave != "glmp152029") return null;

        var data = new dbCommerceDataContext();


        var agora = DateTime.Now;
        IQueryable<RetornoEmissao> pedidosJad;
        pedidosJad = (from c in data.tbPedidoEnvios
                      join d in data.tbPedidoPacotes on c.idPedidoEnvio equals d.idPedidoEnvio into pacotes
                      join e in data.tbClientes on c.tbPedido.clienteId equals e.clienteId
                      join f in data.tbNotaFiscals on c.idPedidoEnvio equals f.idPedidoEnvio
                      join produto in data.tbProdutoEstoques on c.idPedidoEnvio equals produto.idPedidoEnvio into produtos
                      where
                          (pacotes.Where(x => (x.rastreio == "" | x.rastreio == null) && x.codJadlog != "" && x.idEnderecoPacote != null).Count() > 0)                          
                      orderby c.tbPedido.prazoMaximoPostagemAtualizado < agora ? c.tbPedido.prazoMaximoPostagemAtualizado : c.dataFimEmbalagem
                      select new RetornoEmissao
                      {
                          pedidoId = c.tbPedido.pedidoId,
                          dataFimEmbalagem = c.tbPedido.prazoMaximoPostagemAtualizado < agora ? (c.tbPedido.prazoMaximoPostagemAtualizado ?? agora) : (c.dataFimEmbalagem ?? agora),
                          nfeNumero = c.nfeNumero ?? 0,
                          idPedidoEnvio = c.idPedidoEnvio,
                          clienteCPFCNPJ = e.clienteCPFCNPJ,
                          endNomeDoDestinatario = c.tbPedido.endNomeDoDestinatario,
                          endRua = c.tbPedido.endRua,
                          endNumero = c.tbPedido.endNumero,
                          endComplemento = c.tbPedido.endComplemento ?? "",
                          endCidade = c.tbPedido.endCidade,
                          endBairro = c.tbPedido.endBairro,
                          endEstado = c.tbPedido.endEstado,
                          endCep = c.tbPedido.endCep,
                          formaDeEnvio = c.formaDeEnvio,
                          gerarReversa = c.tbPedido.gerarReversa,
                          totalPacotes = pacotes.Count(),
                          clienteFoneResidencial =
                              e.clienteFoneResidencial == "" ? e.clienteFoneCelular : e.clienteFoneResidencial,
                          prioridade = c.prioridade ?? 5,
                          idCentroDistribuicao = c.idCentroDeDistribuicao,
                          pesos = pacotes.Select(x => ((decimal)x.peso) / 1000).ToList(),
                          nota = c.nfeNumero ?? 0,
                          danfe =
                              (c.nfeNumero ?? 0) > 0
                                  ? (from n in data.tbNotaFiscals
                                     where n.numeroNota == c.nfeNumero && n.idPedidoEnvio == c.idPedidoEnvio
                                     select n).First().linkDanfe
                                  : "",
                          produtos = produtos.Select(x => x.tbProduto.produtoNome).ToList(),
                          enderecos = string.Join(",", pacotes.Select(x => x.tbEnderecoPacote.enderecoPacote))
                        });

        return pedidosJad.ToList();
    }


    [WebMethod]
    public List<RetornoEmissao> ListaEmissoesTransportadoras(string chave, string transportadora)
    {
        if (chave != "glmp152029") return null;

        var data = new dbCommerceDataContext();

        string transportadoraNormalizada = transportadora == "correios" ? "pac" : transportadora;

        var agora = DateTime.Now;
        IQueryable<RetornoEmissao> pedidosJad;
        pedidosJad = (from c in data.tbPedidoEnvios
                      join d in data.tbPedidoPacotes on c.idPedidoEnvio equals d.idPedidoEnvio into pacotes
                      join e in data.tbClientes on c.tbPedido.clienteId equals e.clienteId
                      join produto in data.tbProdutoEstoques on c.idPedidoEnvio equals produto.idPedidoEnvio into produtos
                      where
                          (pacotes.Where(x => (x.rastreio == "" | x.rastreio == null) &&  x.idEnderecoPacote != null).Count() > 0) &&
                          c.formaDeEnvio == transportadoraNormalizada && (c.nfeImpressa ?? false) == false
                      orderby c.tbPedido.prazoMaximoPostagemAtualizado < agora ? c.tbPedido.prazoMaximoPostagemAtualizado : c.dataFimEmbalagem
                      select new RetornoEmissao
                      {
                          pedidoId = c.tbPedido.pedidoId,
                          dataFimEmbalagem = c.tbPedido.prazoMaximoPostagemAtualizado < agora ? (c.tbPedido.prazoMaximoPostagemAtualizado ?? agora) : (c.dataFimEmbalagem ?? agora),
                          nfeNumero = c.nfeNumero ?? 0,
                          idPedidoEnvio = c.idPedidoEnvio,
                          clienteCPFCNPJ = e.clienteCPFCNPJ,
                          endNomeDoDestinatario = c.tbPedido.endNomeDoDestinatario,
                          endRua = c.tbPedido.endRua,
                          endNumero = c.tbPedido.endNumero,
                          endComplemento = c.tbPedido.endComplemento ?? "",
                          endCidade = c.tbPedido.endCidade,
                          endBairro = c.tbPedido.endBairro,
                          endEstado = c.tbPedido.endEstado,
                          endCep = c.tbPedido.endCep,
                          formaDeEnvio = c.formaDeEnvio,
                          gerarReversa = c.tbPedido.gerarReversa,
                          totalPacotes = pacotes.Count(),
                          clienteFoneResidencial =
                              e.clienteFoneResidencial == "" ? e.clienteFoneCelular : e.clienteFoneResidencial,
                          prioridade = c.prioridade ?? 5,
                          idCentroDistribuicao = c.idCentroDeDistribuicao,
                          pesos = pacotes.Select(x => ((decimal)x.peso) / 1000).ToList(),
                          nota = c.nfeNumero ?? 0,
                          danfe =
                              (c.nfeNumero ?? 0) > 0
                                  ? (from n in data.tbNotaFiscals
                                     where n.numeroNota == c.nfeNumero && n.idPedidoEnvio == c.idPedidoEnvio
                                     select n).First().linkDanfe
                                  : "",
                          produtos = produtos.Select(x => x.tbProduto.produtoNome).ToList(),
                          enderecos = string.Join(",", pacotes.Select(x => x.tbEnderecoPacote.enderecoPacote))
                      });

        return pedidosJad.ToList();
    }


    [WebMethod]
    public List<RetornoNotas> ListaNotasProcessamento(string chave, int idCentroDistribuicao)
    {
        if (chave != "glmp152029") return null;

        var data = new dbCommerceDataContext();
        var pedidosNotaProcessamento =
            (from c in data.tbPedidoEnvios
             join d in data.tbNotaFiscals on c.nfeNumero equals d.numeroNota into notaObj
             where (c.nfeObrigatoria ?? false) && (c.formaDeEnvio.Contains("jadlog")) && (c.nfeStatus != 2) && (c.nfeNumero ?? 0) > 0 && c.idCentroDeDistribuicao >= idCentroDistribuicao
             select new RetornoNotas
             {
                 pedidoId = c.tbPedido.pedidoId,
                 dataFimEmbalagem = c.dataFimEmbalagem ?? DateTime.Now,
                 idPedidoEnvio = c.idPedidoEnvio,
                 nfeNumero = c.nfeNumero ?? 0,
                 status = notaObj.FirstOrDefault(x => x.idPedido == c.tbPedido.pedidoId) == null ? "Aguardando Processamento" : notaObj.FirstOrDefault(x => x.idPedido == c.tbPedido.pedidoId).ultimoStatus
             });


        return pedidosNotaProcessamento.ToList();
    }

    [WebMethod]
    public bool GravarRastreio(string chave, int idPedidoEnvio, string rastreio)
    {
        if (chave != "glmp152029") return false;
        var data = new dbCommerceDataContext();

        var pacotesRastreioDuplicado =
            (from c in data.tbPedidoPacotes
             where c.rastreio == rastreio && c.idPedidoEnvio != idPedidoEnvio
             select c).Any();
        if (pacotesRastreioDuplicado)
        {
            string mensagem = "";
            mensagem += "Rastreio duplicado<br>";
            mensagem += rastreio;
            rnEmails.EnviaEmail("", "andre@bark.com.br;ouvidoria@graodegente.com.br", "", "", "", mensagem, "Rastreio Duplicado");
            return false;
        }

        var pacotes = (from c in data.tbPedidoPacotes where c.idPedidoEnvio == idPedidoEnvio select c);
        foreach (var pacote in pacotes)
        {
            pacote.rastreio = rastreio;
            data.SubmitChanges();
        }
        return true;
    }


    [WebMethod]
    public void EmiteNotas(string chave)
    {
        //rnNotaFiscal.checaNotasPendentes();
        //rnNotaFiscal.emiteNotasCompletas();
    }

    [WebMethod]
    public void EmiteNotasFiscais(string chave)
    {
        rnNotaFiscal.checaNotasPendentes();
        rnNotaFiscal.emiteNotasCompletas();
        rnNotaFiscal.emiteNotasCompletasGnre();
    }

    [WebMethod]
    public RetornoChecarVolumePesar ChecarVolumesPesar(string chave, int pedidoId)
    {
        if (chave != "glmp152029") return null;
        var data = new dbCommerceDataContext();
        var pacote =
            (from c in data.tbPedidoPacotes
             where c.idPedido == pedidoId && c.concluido == false
             select c).ToList();
        var retorno = new RetornoChecarVolumePesar();
        retorno.totalVolumes = pacote.Count;
        retorno.pedidoId = pedidoId;

        if (retorno.totalVolumes > 0)
        {
            retorno.idPedidoEnvio = pacote.First().tbPedidoEnvio.idPedidoEnvio;
            var produtos = (from c in data.tbProdutoEstoques
                            where c.idPedidoEnvio == retorno.idPedidoEnvio
                            select new ListaProdutosVolumesPesar
                            {
                                altura = (c.tbProduto.altura ?? 0),
                                comprimento = (c.tbProduto.profundidade ?? 0),
                                etiqueta = c.idPedidoFornecedorItem,
                                largura = (c.tbProduto.largura ?? 0),
                                peso = Convert.ToInt32(c.tbProduto.produtoPeso),
                                produtoId = c.produtoId,
                                idCentroDistribuicao = c.idCentroDistribuicao
                            }).ToList();
            retorno.produtos = produtos;
        }
        else
        {
            var ultimoPacote =
               (from c in data.tbPedidoPacotes
                where c.idPedido == pedidoId
                orderby c.idPedidoPacote descending
                select c).FirstOrDefault();
            if (ultimoPacote != null)
            {
                if (!string.IsNullOrEmpty(ultimoPacote.formaDeEnvio))
                {
                    retorno.transportadora = ultimoPacote.formaDeEnvio;
                }
                var produtos = (from c in data.tbProdutoEstoques
                                where c.idPedidoEnvio == retorno.idPedidoEnvio
                                select new ListaProdutosVolumesPesar
                                {
                                    altura = (c.tbProduto.altura ?? 0),
                                    comprimento = (c.tbProduto.profundidade ?? 0),
                                    etiqueta = c.idPedidoFornecedorItem,
                                    largura = (c.tbProduto.largura ?? 0),
                                    peso = Convert.ToInt32(c.tbProduto.produtoPeso),
                                    produtoId = c.produtoId,
                                    idCentroDistribuicao = c.idCentroDistribuicao
                                }).ToList();
                retorno.produtos = produtos;
            }
        }
        return retorno;
    }

    [WebMethod]
    public RetornoChecarVolumePesar ChecarVolumesPesarNota(string chave, string cnpj, int numeronota, int numerovolume, int idCentroDistribuicao)
    {
        if (chave != "glmp152029") return null;
        var data = new dbCommerceDataContext();
        var empresa = (from c in data.tbEmpresas where c.cnpj == cnpj select c).First();
        var nota = (from c in data.tbNotaFiscals
                    join d in data.tbPedidoEnvios on c.idPedidoEnvio equals d.idPedidoEnvio
                    where c.idCNPJ == empresa.idEmpresa && c.numeroNota == numeronota
                    select new
                    {
                        c.idPedidoEnvio,
                        c.idPedido,
                        d.volumesEmbalados
                    }).First();
        var pacote =
            (from c in data.tbPedidoPacotes
             where c.idPedidoEnvio == nota.idPedidoEnvio && c.concluido == false && c.tbPedidoEnvio.idCentroDeDistribuicao == idCentroDistribuicao
             select c).ToList();
        if (idCentroDistribuicao == 5)
        {
            pacote =
            (from c in data.tbPedidoPacotes
             where c.idPedidoEnvio == nota.idPedidoEnvio && c.concluido == false && (c.tbPedidoEnvio.idCentroDeDistribuicao == 4 | c.tbPedidoEnvio.idCentroDeDistribuicao == 5)
             select c).ToList();
        }
        /*var envio = (from c in data.tbPedidoEnvios where c.idPedidoEnvio == nota.idPedidoEnvio select c).First();
        if(envio.volumesEmbalados != pacote.Count)
        {
            envio.volumesEmbalados = pacote.Count;
            data.SubmitChanges();
        }*/

        var retorno = new RetornoChecarVolumePesar();
        retorno.pedidoId = nota.idPedido;
        retorno.totalVolumes = pacote.Count;
        if (retorno.totalVolumes > 0)
        {
            retorno.idPedidoEnvio = pacote.First().tbPedidoEnvio.idPedidoEnvio;
            var produtos = (from c in data.tbProdutoEstoques
                            where c.idPedidoEnvio == retorno.idPedidoEnvio
                            select new ListaProdutosVolumesPesar
                            {
                                altura = (c.tbProduto.altura ?? 0),
                                comprimento = (c.tbProduto.profundidade ?? 0),
                                etiqueta = c.idPedidoFornecedorItem,
                                largura = (c.tbProduto.largura ?? 0),
                                peso = Convert.ToInt32(c.tbProduto.produtoPeso),
                                produtoId = c.produtoId,
                                idCentroDistribuicao = c.idCentroDistribuicao
                            }).ToList();
            retorno.produtos = produtos;
        }
        else
        {
            var ultimoPacote =
               (from c in data.tbPedidoPacotes
                where c.idPedidoEnvio == nota.idPedidoEnvio
                orderby c.idPedidoPacote descending
                select c).FirstOrDefault();
            if (ultimoPacote != null)
            {
                if (!string.IsNullOrEmpty(ultimoPacote.formaDeEnvio))
                {
                    retorno.transportadora = ultimoPacote.formaDeEnvio;
                }
                var produtos = (from c in data.tbProdutoEstoques
                                where c.idPedidoEnvio == retorno.idPedidoEnvio
                                select new ListaProdutosVolumesPesar
                                {
                                    altura = (c.tbProduto.altura ?? 0),
                                    comprimento = (c.tbProduto.profundidade ?? 0),
                                    etiqueta = c.idPedidoFornecedorItem,
                                    largura = (c.tbProduto.largura ?? 0),
                                    peso = Convert.ToInt32(c.tbProduto.produtoPeso),
                                    produtoId = c.produtoId,
                                    idCentroDistribuicao = c.idCentroDistribuicao
                                }).ToList();
                retorno.produtos = produtos;
            }
        }
        return retorno;
    }

    [WebMethod]
    public RetornoChecarVolumePesar ChecarVolumesPesarCds(string chave, int pedidoId, int idCentroDistribuicao)
    {
        if (chave != "glmp152029") return null;
        var data = new dbCommerceDataContext();
        var pacote =
            (from c in data.tbPedidoPacotes
             where c.idPedido == pedidoId && c.concluido == false && c.tbPedidoEnvio.idCentroDeDistribuicao == idCentroDistribuicao
             select c).ToList();

        var retorno = new RetornoChecarVolumePesar();
        retorno.totalVolumes = pacote.Count;
        if (retorno.totalVolumes > 0)
        {
            retorno.idPedidoEnvio = pacote.First().tbPedidoEnvio.idPedidoEnvio;
            var produtos = (from c in data.tbProdutoEstoques
                            where c.idPedidoEnvio == retorno.idPedidoEnvio
                            select new ListaProdutosVolumesPesar
                            {
                                altura = (c.tbProduto.altura ?? 0),
                                comprimento = (c.tbProduto.profundidade ?? 0),
                                etiqueta = c.idPedidoFornecedorItem,
                                largura = (c.tbProduto.largura ?? 0),
                                peso = Convert.ToInt32(c.tbProduto.produtoPeso),
                                produtoId = c.produtoId,
                                idCentroDistribuicao = c.idCentroDistribuicao
                            }).ToList();
            retorno.produtos = produtos;
        }
        else
        {
            var ultimoPacote =
               (from c in data.tbPedidoPacotes
                where c.idPedido == pedidoId
                orderby c.idPedidoPacote descending
                select c).FirstOrDefault();
            if (ultimoPacote != null)
            {
                if (!string.IsNullOrEmpty(ultimoPacote.formaDeEnvio))
                {
                    retorno.transportadora = ultimoPacote.formaDeEnvio;
                }
                var produtos = (from c in data.tbProdutoEstoques
                                where c.idPedidoEnvio == retorno.idPedidoEnvio
                                select new ListaProdutosVolumesPesar
                                {
                                    altura = (c.tbProduto.altura ?? 0),
                                    comprimento = (c.tbProduto.profundidade ?? 0),
                                    etiqueta = c.idPedidoFornecedorItem,
                                    largura = (c.tbProduto.largura ?? 0),
                                    peso = Convert.ToInt32(c.tbProduto.produtoPeso),
                                    produtoId = c.produtoId,
                                    idCentroDistribuicao = c.idCentroDistribuicao
                                }).ToList();
                retorno.produtos = produtos;
            }
        }
        return retorno;
    }

    [WebMethod]
    public string GravarPesosFinalizarPedido(string chave, int pedidoId, int idPedidoEnvio, List<Cubagem> cubagens, int idUsuario)
    {
        if (chave != "glmp152029") return "";
        var data = new dbCommerceDataContext();
        foreach (var cubagem in cubagens)
        {
            var pacote = (from c in data.tbPedidoPacotes
                          where
                              c.idPedidoEnvio == idPedidoEnvio && c.idPedido == pedidoId && c.numeroVolume == cubagem.numeroVolume &&
                              (c.concluido ?? false) == false
                          select c).FirstOrDefault();
            if (pacote != null)
            {
                pacote.largura = cubagem.largura;
                pacote.altura = cubagem.altura;
                pacote.profundidade = cubagem.comprimento;
                pacote.peso = cubagem.peso;
                pacote.cubado = cubagem.cubado;
                pacote.dataPesagem = System.DateTime.Now;
                pacote.idUsuarioPesagem = idUsuario;
                pacote.idEnderecoPacote = 4;
                pacote.concluido = true;
            }
            data.SubmitChanges();
        }
        string usuario = "";
        if (idUsuario > 0)
        {
            usuario = rnUsuarios.usuarioSeleciona_PorUsuarioId(idUsuario).Tables[0].Rows[0]["usuarioNome"].ToString();
        }
        else
        {
            usuario = "Sistema";
        }
        rnInteracoes.interacaoInclui(pedidoId, "Pedido Pesado", usuario, "False");
        var entrega = rnEntrega.DefineEntregaPedido(pedidoId, idPedidoEnvio);

        try
        {
            ThreadPool.QueueUserWorkItem(state => ProcessaNotaEnvio(idPedidoEnvio));
        }
        catch (Exception ex)
        {

        }
        return entrega;
    }

    private void ProcessaNotaEnvio(int idPedidoEnvio)
    {
        rnNotaFiscal.emiteNotasCompletas(idPedidoEnvio);
        //Thread.Sleep(5000);
        //rnNotaFiscal.checaNotasPendentes(idPedidoEnvio);
    }

    public class Cubagem
    {
        public int peso { get; set; }
        public int largura { get; set; }
        public int altura { get; set; }
        public int comprimento { get; set; }
        public int numeroVolume { get; set; }
        public bool cubado { get; set; }
    }

    public class RetornoChecarVolumePesar
    {
        public int totalVolumes { get; set; }
        public int idPedidoEnvio { get; set; }
        public List<ListaProdutosVolumesPesar> produtos { get; set; }
        public string transportadora { get; set; }
        public int pedidoId { get; set; }
    }
    public class ListaProdutosVolumesPesar
    {
        public int peso { get; set; }
        public int largura { get; set; }
        public int altura { get; set; }
        public int comprimento { get; set; }
        public int produtoId { get; set; }
        public int etiqueta { get; set; }
        public int idCentroDistribuicao { get; set; }
    }


    public class RetornoEmissao
    {
        public int pedidoId { get; set; }
        public DateTime dataFimEmbalagem { get; set; }
        public int nfeNumero { get; set; }
        public int idPedidoEnvio { get; set; }
        public string clienteCPFCNPJ { get; set; }
        public string endNomeDoDestinatario { get; set; }
        public string endRua { get; set; }
        public string endNumero { get; set; }
        public string endComplemento { get; set; }
        public string endCidade { get; set; }
        public string endBairro { get; set; }
        public string endEstado { get; set; }
        public string endCep { get; set; }
        public string formaDeEnvio { get; set; }
        public bool gerarReversa { get; set; }
        public int totalPacotes { get; set; }
        public string clienteFoneResidencial { get; set; }
        public int prioridade { get; set; }
        public int idCentroDistribuicao { get; set; }
        public List<decimal> pesos { get; set; }
        public int nota { get; set; }
        public string danfe { get; set; }
        public List<string> produtos { get; set; }
        public string gnre { get; set; }
        public string comprovantePagamentoGnre { get; set; }
        public string cnpj { get; set; }
        public string codJadlog { get; set; }
        public string enderecos { get; set; }
    }

    public class RetornoAguardandoCarregamentoHeader
    {
        public int TotalDePedidos { get; set; }
        public int TotalDePedidosParcial { get; set; }
        public List<RetornoAguardandoCarregamento> ListaCarregamento { get; set; }
    }

    public class RetornoAguardandoCarregamento
    {
        public int pedidoId { get; set; }
        public string endNomeDoDestinatario { get; set; }
        public string endCidade { get; set; }
        public string endBairro { get; set; }
        public string endEstado { get; set; }
        public string formaDeEnvio { get; set; }
        public int totalPacotes { get; set; }
        public int totalPacotesCd1 { get; set; }
        public int totalPacotesCd2 { get; set; }
        public int totalCarregados { get; set; }
        public int totalCarregadosCd1 { get; set; }
        public int totalCarregadosCd2 { get; set; }

    }

    public class RetornoNotas
    {
        public int pedidoId { get; set; }
        public DateTime dataFimEmbalagem { get; set; }
        public int idPedidoEnvio { get; set; }
        public int nfeNumero { get; set; }
        public string status { get; set; }
    }

    public class RetornoEmissaoCorreios
    {
        public string danfe { get; set; }
        public bool sucesso { get; set; }
        public string mensagem { get; set; }
        public string codigo { get; set; }
        public string xml { get; set; }
    }

    [WebMethod]
    public RetornoEmissaoCorreios EmitirCorreiosNota(string cnpj, int numeronota, int numerovolume, int idUsuario)
    {

        var data = new dbCommerceDataContext();
        var empresa = (from c in data.tbEmpresas where c.cnpj == cnpj select c).First();
        var nota = (from c in data.tbNotaFiscals
                    join d in data.tbPedidoEnvios on c.idPedidoEnvio equals d.idPedidoEnvio
                    where c.idCNPJ == empresa.idEmpresa && c.numeroNota == numeronota
                    select new
                    {
                        c.idPedidoEnvio,
                        c.idPedido,
                        d.volumesEmbalados
                    }).First();
        return EmitirCorreiosInterno(nota.idPedidoEnvio ?? 0, nota.idPedido, numerovolume, nota.volumesEmbalados ?? numerovolume);
    }

    [WebMethod]
    public RetornoEmissaoCorreios EmitirCorreios(int pedidoId, int numeroVolume, int totalVolumes)
    {
        return EmitirCorreiosInterno(0, pedidoId, numeroVolume, totalVolumes);
    }

    public RetornoEmissaoCorreios EmitirCorreiosInterno(int idPedidoEnvio, int pedidoId, int numeroVolume, int totalVolumes)
    {
        //string usuarioCorreios = "epsadmin1234";
        //string senhaCorreios = "glmp152029";




        string usuarioCorreios = "epsadmin1664";
        string senhaCorreios = "654321";

        string contratoCorreios = "9912304790";
        string cartaoCorreios = "0064356663";
        string codAdministrativoCorreios = "0012322504";
        string perfilImportacaoCorreios = "1476";

        var retorno = new RetornoEmissaoCorreios();
        var data = new dbCommerceDataContext();
        var envios = (from c in data.tbPedidoEnvios
                      where c.idPedido == pedidoId && c.volumesEmbalados == totalVolumes && c.formaDeEnvio.ToLower() == "pac"
                      orderby c.idPedidoEnvio descending
                      select c).ToList();

        if (idPedidoEnvio > 0) envios = envios.Where(x => x.idPedidoEnvio == idPedidoEnvio).ToList();
        var envio = envios.FirstOrDefault();

        var nota = (from c in data.tbNotaFiscals where c.idPedidoEnvio == envio.idPedidoEnvio select c).First();
        retorno.danfe = rnNotaFiscal.retornaDanfeBase64NotaPorNumeroNota(nota.numeroNota.ToString(), nota.idNotaFiscal);
        if (envio == null)
        {
            retorno.sucesso = false;
            retorno.mensagem = "Envio não localizado";
            return retorno;
        }
        var pacotes = (from c in data.tbPedidoPacotes where c.idPedidoEnvio == envio.idPedidoEnvio select c).ToList();
        var pacote = (from c in pacotes where c.numeroVolume == numeroVolume select c).FirstOrDefault();
        if (pacote == null)
        {
            retorno.sucesso = false;
            retorno.mensagem = "Pacote não localizado";
            return retorno;
        }

        if (envio.idCentroDeDistribuicao == 4)
        {
            usuarioCorreios = "epsadmin6945";
            senhaCorreios = "011017";
            contratoCorreios = "9912425402";
            cartaoCorreios = "007359455";
            codAdministrativoCorreios = "0017350760";
            perfilImportacaoCorreios = "3912";
        }


        if (string.IsNullOrEmpty(pacote.rastreio))
        {
            var pedido = pacote.tbPedido;
            var cliente = (from c in data.tbClientes where c.clienteId == pedido.clienteId select c).First();

            var postagem = new visualSetPostagem.Postagem();
            postagem.PerfilVipp = new visualSetPostagem.PerfilVipp();
            postagem.PerfilVipp.Usuario = usuarioCorreios;
            postagem.PerfilVipp.Token = senhaCorreios;
            postagem.PerfilVipp.IdPerfil = perfilImportacaoCorreios;

            postagem.ContratoEct = new visualSetPostagem.ContratoEct();
            postagem.ContratoEct.NrContrato = contratoCorreios;
            postagem.ContratoEct.CodigoAdministrativo = codAdministrativoCorreios;
            postagem.ContratoEct.NrCartao = cartaoCorreios;

            postagem.Destinatario = new visualSetPostagem.Destinatario();
            postagem.Destinatario.CnpjCpf = System.Text.RegularExpressions.Regex.Replace(cliente.clienteCPFCNPJ, "[^0-9]+", "");
            postagem.Destinatario.IeRg = cliente.clienteRGIE;
            postagem.Destinatario.Nome = pedido.endNomeDoDestinatario;
            postagem.Destinatario.Endereco = pedido.endRua;
            postagem.Destinatario.Numero = pedido.endNumero;
            postagem.Destinatario.Complemento = pedido.endComplemento;
            postagem.Destinatario.Bairro = pedido.endBairro;
            postagem.Destinatario.Cidade = pedido.endCidade;
            postagem.Destinatario.UF = pedido.endEstado;
            postagem.Destinatario.Cep = pedido.endCep.Replace("-", "").Replace(" ", "").Replace(".", "");
            postagem.Destinatario.Email = cliente.clienteEmail;
            var notas = new List<visualSetPostagem.NotaFiscal>();
            notas.Add(new visualSetPostagem.NotaFiscal()
            {
                DtNotaFiscal = envio.dataFimEmbalagem.Value.ToShortDateString(),
                NrNotaFiscal = envio.nfeNumero.ToString(),
                SerieNotaFiscal = "1",
                VlrTotalNota = (envio.nfeValor ?? (pedido.valorCobrado ?? 0)).ToString("0.00")
            });
            postagem.NotasFiscais = notas.ToArray();

            var volumes = new List<visualSetPostagem.VolumeObjeto>();
            var pacoteDetalhe = pacote;
            //foreach (var pacoteDetalhe in pacotes)
            //{
            var idInterno = rnFuncoes.retornaIdCliente(pedidoId);
            string codigoBarras = idInterno.ToString() + "-" + numeroVolume + "/" + totalVolumes;

            var volume = new visualSetPostagem.VolumeObjeto();
            volume.Peso = pacoteDetalhe.peso.ToString();
            volume.Altura = pacoteDetalhe.altura.ToString();
            volume.Largura = pacoteDetalhe.largura.ToString();
            volume.Comprimento = pacoteDetalhe.profundidade.ToString();
            volume.CodigoBarraVolume = codigoBarras;
            volume.CodigoBarraCliente = codigoBarras;
            volume.ObservacaoVisual = "";
            volume.PosicaoVolume = numeroVolume.ToString();
            volumes.Add(volume);
            //}

            postagem.Volumes = volumes.ToArray();

            var postar = new visualSetPostagem.PostagemVipp();
            var resultadoPostagem = postar.PostarObjeto(postagem);
            var result = resultadoPostagem.ToString();
            XmlDocument xml = new XmlDocument();
            xml.LoadXml(resultadoPostagem.OuterXml);
            XmlNodeList StatusPostagemNode = xml.DocumentElement.SelectNodes("/Postagem/StatusPostagem");

            bool postagemValida = false;
            foreach (XmlNode node in StatusPostagemNode)
            {
                if (node.InnerText.ToLower() == "valida") postagemValida = true;
            }

            if (postagemValida)
            {
                XmlNodeList VolumesMainNode = xml.DocumentElement.SelectNodes("/Postagem/Volumes");
                foreach (XmlNode volumeMainNode in VolumesMainNode)
                {
                    foreach (XmlNode volumeNode in volumeMainNode.ChildNodes)
                    {
                        string etiqueta = volumeNode.SelectSingleNode("Etiqueta").InnerText;
                        string codigoBarra = volumeNode.SelectSingleNode("CodigoBarraVolume").InnerText;
                        string posicaoVolume = volumeNode.SelectSingleNode("PosicaoVolume").InnerText;
                        int volumeEtiqueta = Convert.ToInt32(posicaoVolume);

                        var pacoteAlterar = (from c in pacotes where c.numeroVolume == numeroVolume select c).FirstOrDefault();
                        pacoteAlterar.rastreio = etiqueta;
                        data.SubmitChanges();
                    }
                }

                var queue = new tbQueue();
                queue.agendamento = DateTime.Now;
                queue.idRelacionado = pacote.idPedidoEnvio;
                queue.tipoQueue = 2;
                queue.concluido = false;
                queue.andamento = false;
                queue.mensagem = "";
                data.tbQueues.InsertOnSubmit(queue);
                data.SubmitChanges();
            }
            else
            {
                retorno.sucesso = false;
                return retorno;
            }
        }

        pacote = (from c in pacotes where c.numeroVolume == numeroVolume select c).FirstOrDefault();
        if (!string.IsNullOrEmpty(pacote.rastreio))
        {
            using (WebClient client = new WebClient())
            {
                byte[] response =
                client.UploadValues("http://vipp.visualset.com.br/vipp/remoto/ImpressaoRemota.php", new NameValueCollection()
                {
                    { "Usr", usuarioCorreios },
                    { "Pwd", senhaCorreios },
                    { "Filtro", "1" },
                    { "Ordem", "1" },
                    { "Saída", "0" },
                    { "Lista", pacote.rastreio }
                });
                string etiquetaVipp = System.Text.Encoding.UTF8.GetString(response);

                retorno.codigo = pacote.rastreio;
                retorno.sucesso = true;
                retorno.xml = etiquetaVipp;
                return retorno;
            }
        }

        return retorno;
    }

    [WebMethod]
    public RetornoAguardandoCarregamentoHeader ListaAguardandoCarregamentoCaminhao(string chave, string transportadora)
    {
        return ListaAguardandoCarregamentoCaminhaoMethod(chave, transportadora, 1);
    }

    [WebMethod]
    public RetornoAguardandoCarregamentoHeader ListaAguardandoCarregamentoCaminhaoCdsNew(string chave, int idusuario, int idcentrodistribuicao)
    {
        return ListaAguardandoCarregamentoCaminhaoMethodNew(chave, idusuario, idcentrodistribuicao);
    }

    [WebMethod]
    public RetornoAguardandoCarregamentoHeader ListaAguardandoCarregamentoCaminhaoCds(string chave, string transportadora, int idCentroDistribuicao)
    {
        return ListaAguardandoCarregamentoCaminhaoMethod(chave, transportadora, idCentroDistribuicao);
    }

    public RetornoAguardandoCarregamentoHeader ListaAguardandoCarregamentoCaminhaoMethodNew(string chave, int idusuario, int idcentrodistribuicao)
    {
        if (chave != "glmp152029") return null;

        int carregamentoCd1 = 1;
        int carregamentoCd2 = 2;

        if (idcentrodistribuicao == 3)
        {
            carregamentoCd1 = 3;
            carregamentoCd2 = 0;
        }

        if (idcentrodistribuicao == 4)
        {
            carregamentoCd1 = 4;
            carregamentoCd2 = 5;
        }

        var data = new dbCommerceDataContext();

        var usuarioPermissao = (from c in data.tbUsuarioExpedicaoPermissaos
                                where c.idUsuarioExpedicao == idusuario
                                select c).FirstOrDefault();

        var pedidosAguardandoCarregamento = (from c in data.tbPedidoEnvios
                                             join d in data.tbPedidoPacotes on c.idPedidoEnvio equals d.idPedidoEnvio into pacotes
                                             join e in data.tbClientes on c.tbPedido.clienteId equals e.clienteId
                                             join produto in data.tbProdutoEstoques on c.idPedidoEnvio equals produto.idPedidoEnvio into produtos
                                             where
                                             (pacotes.Where(x => (x.carregadoCaminhao ?? false) == false && (x.rastreamentoConcluido ?? false) == false).Count() > 0) &&
                                             (c.formaDeEnvio == "pac" | c.formaDeEnvio == "sedex" ? "correios" : c.formaDeEnvio) == usuarioPermissao.filtro &&
                                             (c.idCentroDeDistribuicao == carregamentoCd1 | c.idCentroDeDistribuicao == carregamentoCd2)

                                             select new RetornoAguardandoCarregamento
                                             {
                                                 pedidoId = c.tbPedido.pedidoId,
                                                 endNomeDoDestinatario = c.tbPedido.endNomeDoDestinatario,
                                                 endCidade = c.tbPedido.endCidade,
                                                 endBairro = c.tbPedido.endBairro,
                                                 endEstado = c.tbPedido.endEstado,
                                                 formaDeEnvio = c.formaDeEnvio,
                                                 totalPacotes = pacotes.Count(),
                                                 totalCarregados = pacotes.Count(x => (x.carregadoCaminhao ?? false) == true),
                                                 totalPacotesCd1 = pacotes.Where(x => (x.idCentroDistribuicao ?? 1) == carregamentoCd1).Count(),
                                                 totalPacotesCd2 = pacotes.Where(x => (x.idCentroDistribuicao ?? 1) == carregamentoCd2).Count(),
                                                 totalCarregadosCd1 = pacotes.Where(x => (x.carregadoCaminhao ?? false) == true && (x.idCentroDistribuicao ?? 1) == carregamentoCd1).Count(),
                                                 totalCarregadosCd2 = pacotes.Where(x => (x.carregadoCaminhao ?? false) == true && (x.idCentroDistribuicao ?? 1) == carregamentoCd2).Count(),

                                             }).OrderByDescending(x => x.totalCarregados).ThenBy(x => x.pedidoId).Take(10).ToList();

        int totalPacote = (from c in data.tbPedidoEnvios
                           join d in data.tbPedidoPacotes on c.idPedidoEnvio equals d.idPedidoEnvio into pacotes
                           where
                           (pacotes.Where(x => (x.carregadoCaminhao ?? false) == false && (x.rastreamentoConcluido ?? false) == false).Count() > 0) &&
                           (c.formaDeEnvio == "pac" | c.formaDeEnvio == "sedex" ? "correios" : c.formaDeEnvio) == usuarioPermissao.filtro &&
                           (c.idCentroDeDistribuicao == carregamentoCd1 | c.idCentroDeDistribuicao == carregamentoCd2)

                           select new RetornoAguardandoCarregamento
                           {
                               totalPacotesCd1 = pacotes.Where(x => (x.idCentroDistribuicao ?? 1) == carregamentoCd1).Count(),
                               totalPacotesCd2 = pacotes.Where(x => (x.idCentroDistribuicao ?? 1) == carregamentoCd2).Count(),

                           }).Sum(x => x.totalPacotesCd1 + x.totalPacotesCd2);

        int totalParcial = (from c in data.tbPedidoEnvios
                            join d in data.tbPedidoPacotes on c.idPedidoEnvio equals d.idPedidoEnvio into pacotes
                            where
                            (pacotes.Where(x => (x.carregadoCaminhao ?? false) == false && (x.rastreamentoConcluido ?? false) == false).Count() > 0) &&
                            (c.formaDeEnvio == "pac" | c.formaDeEnvio == "sedex" ? "correios" : c.formaDeEnvio) == usuarioPermissao.filtro &&
                            (c.idCentroDeDistribuicao == carregamentoCd1 | c.idCentroDeDistribuicao == carregamentoCd2)

                            select new RetornoAguardandoCarregamento
                            {
                                totalCarregados = pacotes.Count(x => (x.carregadoCaminhao ?? false) == true)

                            }).Count(x => x.totalCarregados > 0);

        RetornoAguardandoCarregamentoHeader header = new RetornoAguardandoCarregamentoHeader()
        {
            TotalDePedidos = totalPacote,
            TotalDePedidosParcial = totalParcial
        };

        foreach (var PAC in pedidosAguardandoCarregamento)
            PAC.pedidoId = rnFuncoes.retornaIdCliente(PAC.pedidoId);

        header.ListaCarregamento = pedidosAguardandoCarregamento.ToList();

        return header;
    }



    public RetornoAguardandoCarregamentoHeader ListaAguardandoCarregamentoCaminhaoMethod(string chave, string transportadora, int idCentroDistribuicao)
    {
        if (chave != "glmp152029") return null;

        int carregamentoCd1 = 1;
        int carregamentoCd2 = 2;

        if (idCentroDistribuicao == 3)
        {
            carregamentoCd1 = 3;
            carregamentoCd2 = 0;
        }

        if (idCentroDistribuicao == 4)
        {
            carregamentoCd1 = 4;
            carregamentoCd2 = 5;
        }

        var data = new dbCommerceDataContext();


        var pedidosAguardandoCarregamento = (from c in data.tbPedidoEnvios
                                             join d in data.tbPedidoPacotes on c.idPedidoEnvio equals d.idPedidoEnvio into pacotes
                                             join e in data.tbClientes on c.tbPedido.clienteId equals e.clienteId
                                             join produto in data.tbProdutoEstoques on c.idPedidoEnvio equals produto.idPedidoEnvio into produtos
                                             where
                                             (pacotes.Where(x => (x.carregadoCaminhao ?? false) == false && (x.rastreamentoConcluido ?? false) == false).Count() > 0) &&
                                             (c.formaDeEnvio == "pac" | c.formaDeEnvio == "sedex" ? "correios" : c.formaDeEnvio) == transportadora &&
                                             (c.idCentroDeDistribuicao == carregamentoCd1 | c.idCentroDeDistribuicao == carregamentoCd2)

                                             select new RetornoAguardandoCarregamento
                                             {
                                                 pedidoId = c.tbPedido.pedidoId,
                                                 endNomeDoDestinatario = c.tbPedido.endNomeDoDestinatario,
                                                 endCidade = c.tbPedido.endCidade,
                                                 endBairro = c.tbPedido.endBairro,
                                                 endEstado = c.tbPedido.endEstado,
                                                 formaDeEnvio = c.formaDeEnvio,
                                                 totalPacotes = pacotes.Count(),
                                                 totalCarregados = pacotes.Count(x => (x.carregadoCaminhao ?? false) == true),
                                                 totalPacotesCd1 = pacotes.Where(x => (x.idCentroDistribuicao ?? 1) == carregamentoCd1).Count(),
                                                 totalPacotesCd2 = pacotes.Where(x => (x.idCentroDistribuicao ?? 1) == carregamentoCd2).Count(),
                                                 totalCarregadosCd1 = pacotes.Where(x => (x.carregadoCaminhao ?? false) == true && (x.idCentroDistribuicao ?? 1) == carregamentoCd1).Count(),
                                                 totalCarregadosCd2 = pacotes.Where(x => (x.carregadoCaminhao ?? false) == true && (x.idCentroDistribuicao ?? 1) == carregamentoCd2).Count(),

                                             }).OrderByDescending(x => x.totalCarregados).ThenBy(x => x.pedidoId).Take(100).ToList();

        int totalPacote = (from c in data.tbPedidoEnvios
                           join d in data.tbPedidoPacotes on c.idPedidoEnvio equals d.idPedidoEnvio into pacotes
                           where
                           (pacotes.Where(x => (x.carregadoCaminhao ?? false) == false && (x.rastreamentoConcluido ?? false) == false).Count() > 0) &&
                           (c.formaDeEnvio == "pac" | c.formaDeEnvio == "sedex" ? "correios" : c.formaDeEnvio) == transportadora &&
                           (c.idCentroDeDistribuicao == carregamentoCd1 | c.idCentroDeDistribuicao == carregamentoCd2)

                           select new RetornoAguardandoCarregamento
                           {
                               totalPacotesCd1 = pacotes.Where(x => (x.idCentroDistribuicao ?? 1) == carregamentoCd1).Count(),
                               totalPacotesCd2 = pacotes.Where(x => (x.idCentroDistribuicao ?? 1) == carregamentoCd2).Count(),

                           }).Sum(x => x.totalPacotesCd1 + x.totalPacotesCd2);

        int totalParcial = (from c in data.tbPedidoEnvios
                            join d in data.tbPedidoPacotes on c.idPedidoEnvio equals d.idPedidoEnvio into pacotes
                            where
                            (pacotes.Where(x => (x.carregadoCaminhao ?? false) == false && (x.rastreamentoConcluido ?? false) == false).Count() > 0) &&
                            (c.formaDeEnvio == "pac" | c.formaDeEnvio == "sedex" ? "correios" : c.formaDeEnvio) == transportadora &&
                            (c.idCentroDeDistribuicao == carregamentoCd1 | c.idCentroDeDistribuicao == carregamentoCd2)

                            select new RetornoAguardandoCarregamento
                            {
                                totalCarregados = pacotes.Count(x => (x.carregadoCaminhao ?? false) == true)

                            }).Count(x => x.totalCarregados > 0);

        RetornoAguardandoCarregamentoHeader header = new RetornoAguardandoCarregamentoHeader()
        {
            TotalDePedidos = totalPacote,
            TotalDePedidosParcial = totalParcial
        };

        foreach (var PAC in pedidosAguardandoCarregamento)
            PAC.pedidoId = rnFuncoes.retornaIdCliente(PAC.pedidoId);

        header.ListaCarregamento = pedidosAguardandoCarregamento.ToList();

        return header;
    }



    public class RetornoCarregarVolume
    {
        public bool sucesso { get; set; }
        public string mensagem { get; set; }
    }


    [WebMethod]
    public RetornoCarregarVolume RetornarVolumeCaminhaoSeparar(string codigoDeBarras, int idusuario, int idCentroDistribuicao)
    {
        var cnpj = string.Empty;
        var numeroNota = string.Empty;
        var volAtual = string.Empty;
        var volTotal = string.Empty;

        var retorno = new RetornoCarregarVolume();

        retorno.sucesso = false;
        retorno.mensagem = "Pedido inválido";

        if (codigoDeBarras.Length == 31)
        {
            cnpj = codigoDeBarras.Substring(0, 14);
            numeroNota = codigoDeBarras.Substring(14, 9);
            volAtual = codigoDeBarras.Substring(23, 4);
            volTotal = codigoDeBarras.Substring(27, 4);

            var data = new dbCommerceDataContext();

            var usuarioPermissao = (from c in data.tbUsuarioExpedicaoPermissaos
                                    where c.idUsuarioExpedicao == idusuario
                                    select c).FirstOrDefault();

            var empresa = (from c in data.tbEmpresas where c.cnpj == cnpj select c).First();
            var nota = (from c in data.tbNotaFiscals
                        join d in data.tbPedidoEnvios on c.idPedidoEnvio equals d.idPedidoEnvio
                        where c.idCNPJ == empresa.idEmpresa && c.numeroNota == int.Parse(numeroNota)
                        select new
                        {
                            c.idPedidoEnvio,
                            c.idPedido,
                            d.volumesEmbalados
                        }).First();
            var pacote =
                (from c in data.tbPedidoPacotes
                 where c.idPedidoEnvio == nota.idPedidoEnvio && c.numeroVolume == int.Parse(volAtual)
                 select c).FirstOrDefault();

            if (pacote != null)
            {
                string formaNormalizada = pacote.formaDeEnvio;
                if (pacote.formaDeEnvio == "pac" | pacote.formaDeEnvio == "sedex") formaNormalizada = "correios";
                if (pacote.formaDeEnvio == "jadlogcom" | pacote.formaDeEnvio == "jadlogrodo" | pacote.formaDeEnvio == "jadlogexpressa") formaNormalizada = "jadlog";

                if (formaNormalizada.ToLower() != usuarioPermissao.filtro.ToLower())
                {
                    retorno.sucesso = false;
                    retorno.mensagem = "Este volume pertence a transportadora " + pacote.formaDeEnvio;
                    rnInteracoes.interacaoInclui(pacote.idPedido, "Transportadora " + usuarioPermissao.filtro + " tentou carregar volume da transportadora " + pacote.formaDeEnvio, null, "False");
                }
                else if (pacote.carregadoCaminhao == true)
                {
                    retorno.sucesso = false;
                    retorno.mensagem = "Este volume já foi carregado anteriormente";
                    rnInteracoes.interacaoInclui(pacote.idPedido, "Transportadora " + usuarioPermissao.filtro + " carregou volume novamente", null, "False");
                }
                else
                {
                    if (formaNormalizada.ToLower() == "correios" && string.IsNullOrEmpty(pacote.rastreio))
                    {
                        retorno.sucesso = false;
                        retorno.mensagem = "Não foi feita a emissão deste volume";
                        rnInteracoes.interacaoInclui(pacote.idPedido, "Transportadora " + usuarioPermissao.filtro + " tentou carregar volume sem realizar emissão", null, "False");
                    }
                    else if (formaNormalizada.ToLower() == "jadlog" && string.IsNullOrEmpty(pacote.rastreio))
                    {
                        retorno.sucesso = false;
                        retorno.mensagem = "Não foi feita a emissão deste volume";
                        rnInteracoes.interacaoInclui(pacote.idPedido, "Transportadora " + usuarioPermissao.filtro + " tentou carregar volume sem realizar emissão", null, "False");
                    }
                    else if (formaNormalizada.ToLower() == "tnt" && (pacote.tbPedidoEnvio.nfeImpressa ?? false) == false)
                    {
                        retorno.sucesso = false;
                        retorno.mensagem = "Não foi feita a impressão da nota fiscal deste volume";
                        rnInteracoes.interacaoInclui(pacote.idPedido, "Transportadora " + usuarioPermissao.filtro + " tentou carregar volume sem realizar emissão", null, "False");
                    }
                    else if (formaNormalizada.ToLower() == "plimor" && (pacote.tbPedidoEnvio.nfeImpressa ?? false) == false)
                    {
                        retorno.sucesso = false;
                        retorno.mensagem = "Não foi feita a impressão da nota fiscal deste volume";
                        rnInteracoes.interacaoInclui(pacote.idPedido, "Transportadora " + usuarioPermissao.filtro + " tentou carregar volume sem realizar emissão", null, "False");
                    }
                    else if ((pacote.formaDeEnvio == "pac" | pacote.formaDeEnvio == "sedex"
                        ? "correios"
                        : pacote.formaDeEnvio).ToLower() == "belle" && (pacote.tbPedidoEnvio.nfeImpressa ?? false) == false)
                    {
                        retorno.sucesso = false;
                        retorno.mensagem = "Não foi feita a impressão da nota fiscal deste volume";
                        rnInteracoes.interacaoInclui(pacote.idPedido,
                            "Transportadora " + usuarioPermissao.filtro + " tentou carregar volume sem realizar emissão", null,
                            "False");
                    }
                }

                DateTime dtHoje = System.DateTime.Now;
                pacote.carregadoCaminhao = true;
                pacote.dataCarregamentoCaminhao = dtHoje;
                retorno.mensagem = "Volume Carregado";
                retorno.sucesso = true;
                pacote.idUsuarioCarregamentoCaminhao = idusuario;
                data.SubmitChanges();
                rnInteracoes.interacaoInclui(pacote.idPedido,
                    "Volume " + pacote.numeroVolume + " carregado no caminhão da " + usuarioPermissao.filtro, null, "False");
            }
        }
        return retorno;
    }

    [WebMethod]
    public RetornoCarregarVolume CarregarVolumeCaminhao(string chave, int idUsuario, int pedidoId, int volume, string transportadora)
    {
        if (chave != "glmp152029") return null;
        var retorno = new RetornoCarregarVolume();
        var data = new dbCommerceDataContext();
        var pacote = (from c in data.tbPedidoPacotes where c.idPedido == pedidoId && c.numeroVolume == volume orderby c.idPedidoEnvio descending select c).FirstOrDefault();
        string usuario = "";
        if (idUsuario > 0)
        {
            usuario = rnUsuarios.usuarioSeleciona_PorUsuarioId(idUsuario).Tables[0].Rows[0]["usuarioNome"].ToString();
        }
        else
        {
            usuario = "Sistema";
        }
        if (pacote != null)
        {
            string formaNormalizada = pacote.formaDeEnvio;
            if (pacote.formaDeEnvio == "pac" | pacote.formaDeEnvio == "sedex") formaNormalizada = "correios";
            if (pacote.formaDeEnvio == "jadlogcom" | pacote.formaDeEnvio == "jadlogrodo" | pacote.formaDeEnvio == "jadlogexpressa") formaNormalizada = "jadlog";

            if (formaNormalizada.ToLower() != transportadora.ToLower())
            {
                retorno.sucesso = false;
                retorno.mensagem = "Este volume pertence a transportadora " + pacote.formaDeEnvio;
                rnInteracoes.interacaoInclui(pedidoId, "Transportadora " + transportadora + " tentou carregar volume da transportadora " + pacote.formaDeEnvio, usuario, "False");
            }
            else if (pacote.carregadoCaminhao == true)
            {
                retorno.sucesso = false;
                retorno.mensagem = "Este volume já foi carregado anteriormente";
                rnInteracoes.interacaoInclui(pedidoId, "Transportadora " + transportadora + " carregou volume novamente", usuario, "False");
            }
            else
            {
                if (formaNormalizada.ToLower() == "correios" && string.IsNullOrEmpty(pacote.rastreio))
                {
                    retorno.sucesso = false;
                    retorno.mensagem = "Não foi feita a emissão deste volume";
                    rnInteracoes.interacaoInclui(pedidoId, "Transportadora " + transportadora + " tentou carregar volume sem realizar emissão", usuario, "False");
                }
                else if (formaNormalizada.ToLower() == "jadlog" && string.IsNullOrEmpty(pacote.rastreio))
                {
                    retorno.sucesso = false;
                    retorno.mensagem = "Não foi feita a emissão deste volume";
                    rnInteracoes.interacaoInclui(pedidoId, "Transportadora " + transportadora + " tentou carregar volume sem realizar emissão", usuario, "False");
                }
                else if (formaNormalizada.ToLower() == "tnt" && (pacote.tbPedidoEnvio.nfeImpressa ?? false) == false)
                {
                    retorno.sucesso = false;
                    retorno.mensagem = "Não foi feita a impressão da nota fiscal deste volume";
                    rnInteracoes.interacaoInclui(pedidoId, "Transportadora " + transportadora + " tentou carregar volume sem realizar emissão", usuario, "False");
                }
                else if (formaNormalizada.ToLower() == "plimor" && (pacote.tbPedidoEnvio.nfeImpressa ?? false) == false)
                {
                    retorno.sucesso = false;
                    retorno.mensagem = "Não foi feita a impressão da nota fiscal deste volume";
                    rnInteracoes.interacaoInclui(pedidoId, "Transportadora " + transportadora + " tentou carregar volume sem realizar emissão", usuario, "False");
                }
                else if ((pacote.formaDeEnvio == "pac" | pacote.formaDeEnvio == "sedex"
                    ? "correios"
                    : pacote.formaDeEnvio).ToLower() == "belle" && (pacote.tbPedidoEnvio.nfeImpressa ?? false) == false)
                {
                    retorno.sucesso = false;
                    retorno.mensagem = "Não foi feita a impressão da nota fiscal deste volume";
                    rnInteracoes.interacaoInclui(pedidoId,
                        "Transportadora " + transportadora + " tentou carregar volume sem realizar emissão", usuario,
                        "False");
                }
                else
                {
                    pacote.carregadoCaminhao = true;
                    retorno.mensagem = "Volume Carregado";
                    retorno.sucesso = true;
                    pacote.idUsuarioCarregamentoCaminhao = idUsuario;
                    data.SubmitChanges();
                    rnInteracoes.interacaoInclui(pedidoId,
                        "Volume " + volume + " carregado no caminhão da " + transportadora, usuario, "False");
                }
            }
        }
        else
        {
            retorno.sucesso = false;
            retorno.mensagem = "Volume não localizado";
        }

        return retorno;
    }

}
