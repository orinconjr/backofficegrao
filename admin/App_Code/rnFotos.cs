﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Web.Configuration;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;

/// <summary>
/// Summary description for rnFotos
/// </summary>
public class rnFotos
{
    public static DataSet produtoFotoDestaqueSeleciona_PorProdutoId(int produtoId)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("produtoFotoDestaqueSeleciona_PorProdutoId");

        db.AddInParameter(dbCommand, "produtoId", DbType.Int32, produtoId);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet produtoFotoSeleciona_PorProdutoId(int produtoId)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("produtoFotoSeleciona_PorProdutoId");

        db.AddInParameter(dbCommand, "produtoId", DbType.Int32, produtoId);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet produtoFotoSelecionaUltimoId()
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("produtoFotoSelecionaUltimoId");

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static bool fotosInclui(int produtoId, string produtoFoto, string produtoFotoDescricao)
    {
        bool result = false;

        Database db = DatabaseFactory.CreateDatabase();

        DbCommand dbCommand = db.GetStoredProcCommand("produtoFotoInclui");

        db.AddInParameter(dbCommand, "produtoId", DbType.Int32, produtoId);
        db.AddInParameter(dbCommand, "produtoFoto", DbType.String, produtoFoto);
        db.AddInParameter(dbCommand, "produtoFotoDescricao", DbType.String, produtoFotoDescricao);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
            DbTransaction transaction = connection.BeginTransaction();

            try
            {
                db.ExecuteNonQuery(dbCommand, transaction);

                transaction.Commit();

                result = true;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
                
            }
            connection.Close();

            return result;
        }
    }

    public static bool fotosIncluiComNome(int produtoId, string produtoFoto, string produtoFotoDescricao)
    {
        bool result = false;

        Database db = DatabaseFactory.CreateDatabase();

        DbCommand dbCommand = db.GetStoredProcCommand("produtoFotoInclui");

        db.AddInParameter(dbCommand, "produtoId", DbType.Int32, produtoId);
        db.AddInParameter(dbCommand, "produtoFoto", DbType.String, produtoFoto);
        db.AddInParameter(dbCommand, "produtoFotoDescricao", DbType.String, produtoFotoDescricao);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
            DbTransaction transaction = connection.BeginTransaction();

            try
            {
                db.ExecuteNonQuery(dbCommand, transaction);

                transaction.Commit();

                result = true;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);

            }
            connection.Close();

            return result;
        }
    }

    #region gera thumbnail
    public static void ResizeImageFile(int Size, string Location, string Target, ImageFormat Format)
    {
        //string PhotoPath = Server.MapPath("~/http://www.jozan.com.br/images/");
        Bitmap fullSizeImg = new Bitmap(Location);
        int width = fullSizeImg.Width;
        int height = fullSizeImg.Height;
        int x = 0;
        int y = 0;

        //Determine dimensions of resized version of the image 
        if (width > height)
        {
            width = (int)Decimal.Round(Convert.ToDecimal((decimal)Size * ((decimal)width / (decimal)height)), 0);

            height = Size;
            // moves cursor so that crop is more centered 
            x = Convert.ToInt32(Math.Ceiling(((decimal)(width - height) / 2M)));
        }
        else if (height > width)
        {
            height = (int)Decimal.Round(Convert.ToDecimal((decimal)Size * ((decimal)height / (decimal)width)), 0);
            width = Size;
            // moves cursor so that crop is more centered 
            y = Convert.ToInt32(Math.Ceiling(((decimal)(height - width) / 2M)));
        }
        else
        {
            width = Size;
            height = Size;
        }

        //// required in case thumbnail creation fails?
        //System.Drawing.Image.GetThumbnailImageAbort dummyCallback = new System.Drawing.Image.GetThumbnailImageAbort(ThumbnailCallback);

        EncoderParameter qualityParam = new EncoderParameter(Encoder.Quality, 100L);
        ImageCodecInfo codecInfo = GetEncoderInfo("image/jpeg");
        EncoderParameters encoderParams = new EncoderParameters(1);
        encoderParams.Param[0] = qualityParam;

        //First Resize the Existing Image 
        System.Drawing.Image thumbNailImg;
        //thumbNailImg = fullSizeImg.GetThumbnailImage(width, height, null, System.IntPtr.Zero);
        thumbNailImg = resizeImage(fullSizeImg, new System.Drawing.Size(width, height));
        //Clean up / Dispose... 
        fullSizeImg.Dispose();

        //Create a Crop Frame to apply to the Resized Image 
        Bitmap myBitmapCropped = new Bitmap(Size, Size);
        Graphics myGraphic = Graphics.FromImage(myBitmapCropped);

        //Apply the Crop to the Resized Image 
        myGraphic.DrawImage(thumbNailImg, new Rectangle(0, 0, width, height), x, y, width, height, GraphicsUnit.Pixel);
        //Clean up / Dispose... 
        myGraphic.Dispose();

        //Save the Croped and Resized image as a new square thumnail 
        myBitmapCropped.Save(Target, codecInfo, encoderParams);

        //Clean up / Dispose... 
        thumbNailImg.Dispose();
        myBitmapCropped.Dispose();
    }

    public static void ResizeImageFileAzure(int Size, string Location, string Target, ImageFormat Format)
    {
        // Retrieve storage account from connection string.
        CloudStorageAccount storageAccount = CloudStorageAccount.Parse(ConfigurationManager.AppSettings["discoAzure"]);

        // Create the blob client.
        CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

        // Retrieve reference to a previously created container.
        CloudBlobContainer container = blobClient.GetContainerReference("fotos");

        // Retrieve reference to a blob named "photo1.jpg".
        CloudBlockBlob blockBlob = container.GetBlockBlobReference(Location);

        Stream fileStream = new MemoryStream();
        blockBlob.DownloadToStream(fileStream);
        fileStream.Seek(0, SeekOrigin.Begin);
        Bitmap fullSizeImg = new Bitmap(fileStream);

        int width = fullSizeImg.Width;
        int height = fullSizeImg.Height;
        int x = 0;
        int y = 0;

        //Determine dimensions of resized version of the image 
        if (width > height)
        {
            width = (int)Decimal.Round(Convert.ToDecimal((decimal)Size * ((decimal)width / (decimal)height)), 0);

            height = Size;
            // moves cursor so that crop is more centered 
            x = Convert.ToInt32(Math.Ceiling(((decimal)(width - height) / 2M)));
        }
        else if (height > width)
        {
            height = (int)Decimal.Round(Convert.ToDecimal((decimal)Size * ((decimal)height / (decimal)width)), 0);
            width = Size;
            // moves cursor so that crop is more centered 
            y = Convert.ToInt32(Math.Ceiling(((decimal)(height - width) / 2M)));
        }
        else
        {
            width = Size;
            height = Size;
        }

        //// required in case thumbnail creation fails?
        //System.Drawing.Image.GetThumbnailImageAbort dummyCallback = new System.Drawing.Image.GetThumbnailImageAbort(ThumbnailCallback);

        EncoderParameter qualityParam = new EncoderParameter(Encoder.Quality, 100L);
        ImageCodecInfo codecInfo = GetEncoderInfo("image/jpeg");
        EncoderParameters encoderParams = new EncoderParameters(1);
        encoderParams.Param[0] = qualityParam;

        //First Resize the Existing Image 
        System.Drawing.Image thumbNailImg;
        //thumbNailImg = fullSizeImg.GetThumbnailImage(width, height, null, System.IntPtr.Zero);
        thumbNailImg = resizeImage(fullSizeImg, new System.Drawing.Size(width, height));
        //Clean up / Dispose... 
        fullSizeImg.Dispose();

        //Create a Crop Frame to apply to the Resized Image 
        Bitmap myBitmapCropped = new Bitmap(Size, Size);
        Graphics myGraphic = Graphics.FromImage(myBitmapCropped);

        //Apply the Crop to the Resized Image 
        myGraphic.DrawImage(thumbNailImg, new Rectangle(0, 0, width, height), x, y, width, height, GraphicsUnit.Pixel);
        //Clean up / Dispose... 
        myGraphic.Dispose();

        //Save the Croped and Resized image as a new square thumnail 
        //myBitmapCropped.Save(Target, codecInfo, encoderParams);

        MemoryStream streamCropped = new MemoryStream();
        myBitmapCropped.Save(streamCropped, codecInfo, encoderParams);

        CloudBlockBlob blob = container.GetBlockBlobReference(Target);
        streamCropped.Seek(0, SeekOrigin.Begin);
        blob.UploadFromStream(streamCropped);
        blob.Properties.ContentType = "image/jpeg";

        //Clean up / Dispose... 
        thumbNailImg.Dispose();
        myBitmapCropped.Dispose();
        fileStream.Dispose();
        streamCropped.Dispose();

    }
    private static System.Drawing.Image resizeImage(System.Drawing.Image imgToResize, Size size)
    {
        int sourceWidth = imgToResize.Width;
        int sourceHeight = imgToResize.Height;

        float nPercent = 0;
        float nPercentW = 0;
        float nPercentH = 0;

        nPercentW = ((float)size.Width / (float)sourceWidth);
        nPercentH = ((float)size.Height / (float)sourceHeight);

        if (nPercentH < nPercentW)
            nPercent = nPercentH;
        else
            nPercent = nPercentW;

        int destWidth = (int)size.Width;
        int destHeight = (int)size.Height;

        Bitmap b = new Bitmap(destWidth, destHeight);
        Graphics g = Graphics.FromImage((System.Drawing.Image)b);
        g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;

        g.DrawImage(imgToResize, 0, 0, destWidth, destHeight);
        g.Dispose();

        return (System.Drawing.Image)b;
    }
    public static ImageCodecInfo GetEncoderInfo(string mimeType)
    {
        ImageCodecInfo[] codecs = ImageCodecInfo.GetImageEncoders();

        for (int i = 0; i < codecs.Length; i++)
        {
            if (codecs[i].MimeType == mimeType)
            {
                return codecs[i];
            }
        }

        return null;
    }
    #endregion

    public static bool produtoFotoAltera(string produtoFotoDescricao, int produtoFotoId, int ordenacao)
    {
        using(var data = new dbCommerceDataContext())
        {
            var foto = (from c in data.tbProdutoFotos where c.produtoFotoId == produtoFotoId select c).FirstOrDefault();
            if(foto != null)
            {
                foto.produtoFotoDescricao = produtoFotoDescricao;
                foto.ordenacao = ordenacao > 0 ? (int?)ordenacao : null;
                data.SubmitChanges();
            }
        }
        bool result = false;

        Database db = DatabaseFactory.CreateDatabase();

        DbCommand dbCommand = db.GetStoredProcCommand("produtoFotoAltera");

        db.AddInParameter(dbCommand, "produtoFotoDescricao", DbType.String, produtoFotoDescricao);
        db.AddInParameter(dbCommand, "produtoFotoId", DbType.Int32, produtoFotoId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
            DbTransaction transaction = connection.BeginTransaction();

            try
            {
                db.ExecuteNonQuery(dbCommand, transaction);

                transaction.Commit();

                result = true;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
                
            }
            connection.Close();

            return result;
        }
    }
    
    public static bool produtoFotoDestaqueAltera(int produtoId, string produtoFoto)
    {
        bool result = false;

        Database db = DatabaseFactory.CreateDatabase();

        DbCommand dbCommand = db.GetStoredProcCommand("produtoFotoDestaqueAltera");

        db.AddInParameter(dbCommand, "produtoId", DbType.Int32, produtoId);
        db.AddInParameter(dbCommand, "produtoFoto", DbType.String, produtoFoto);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
            DbTransaction transaction = connection.BeginTransaction();

            try
            {
                db.ExecuteNonQuery(dbCommand, transaction);

                transaction.Commit();

                result = true;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
                
            }
            connection.Close();

            return result;
        }
    }

    public static bool produtoFotoExclui(string produtoFoto)
    {
        
        bool result = false;

        Database db = DatabaseFactory.CreateDatabase();

        DbCommand dbCommand = db.GetStoredProcCommand("produtoFotoExclui");

        db.AddInParameter(dbCommand, "produtoFoto", DbType.String, produtoFoto);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
            DbTransaction transaction = connection.BeginTransaction();

            try
            {
                db.ExecuteNonQuery(dbCommand, transaction);

                transaction.Commit();

                result = true;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
                
            }
            connection.Close();

            return result;
        }
    }
    public static void fotoExcluiAzure(string foto, string blobContainer)
    {
        CloudStorageAccount storageAccount = CloudStorageAccount.Parse(ConfigurationManager.AppSettings["discoAzure"]);
        CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
        CloudBlobContainer container = blobClient.GetContainerReference(blobContainer);

        CloudBlockBlob fotoBlob = container.GetBlockBlobReference(foto);
        if (fotoBlob.Exists()) fotoBlob.Delete();
    }
}
