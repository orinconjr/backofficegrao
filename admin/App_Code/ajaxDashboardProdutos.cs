﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.Script.Serialization;

/// <summary>
/// Summary description for ajaxDashboardProdutos
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class ajaxDashboardProdutos : System.Web.Services.WebService
{

    public ajaxDashboardProdutos()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string HelloWorld()
    {
        return "Hello World";
    }



    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string ForaDeLinha(int fornecedor = 0)
    {

        int categoria = 0;
        int marca = 0;

        string result = "";

        using (var data = new dbCommerceDataContext())
        {

            using (
                var txn = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required,
                    new System.Transactions.TransactionOptions
                    {
                        IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                    }))

            {
                var foraDeLinha = (from c in data.tbProdutos where c.foraDeLinha && c.produtoAtivo == "True" select new { c.produtoId, c.produtoFornecedor });
                if (fornecedor > 0)
                    foraDeLinha = foraDeLinha.Where(x => x.produtoFornecedor == fornecedor);

                if (categoria > 0)
                    foraDeLinha = (from c in foraDeLinha
                                   join cat in data.tbJuncaoProdutoCategorias on c.produtoId equals cat.produtoId
                                   where cat.categoriaId == categoria
                                   select c);

                result = new JavaScriptSerializer().Serialize(foraDeLinha.Count().ToString());
            }
        }
        return result;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string ForaDeLinhaEstoque0(int fornecedor = 0, int categoria = 0, int marca = 0)
    {
        string result = "";

        using (var data = new dbCommerceDataContext())
        {

            using (
            var txn = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required,
                new System.Transactions.TransactionOptions
                {
                    IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                }))

            {


                var foraDeLinhaEstoque0 = (from c in data.tbProdutos
                                           where c.foraDeLinha && c.estoqueReal == 0 && c.produtoAtivo == "True"
                                           select new { c.produtoId, c.produtoFornecedor });
                if (fornecedor > 0)
                    foraDeLinhaEstoque0 = foraDeLinhaEstoque0.Where(x => x.produtoFornecedor == fornecedor);

                if (categoria > 0)
                    foraDeLinhaEstoque0 = (from c in foraDeLinhaEstoque0
                                           join cat in data.tbJuncaoProdutoCategorias on c.produtoId equals cat.produtoId
                                           where cat.categoriaId == categoria
                                           select c);

                result = new JavaScriptSerializer().Serialize(foraDeLinhaEstoque0.Count().ToString());
            }
        }
        return result;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string ForaDeLinhaEstoqueBaixo(int fornecedor = 0, int categoria = 0, int marca = 0)
    {
        string result = "";

        using (var data = new dbCommerceDataContext())
        {

            using (
           var txn = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required,
               new System.Transactions.TransactionOptions
               {
                   IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
               }))

            {


                var foraDeLinhaEstoqueBaixo = (from c in data.tbProdutos
                                               where c.foraDeLinha && (c.estoqueReal > 0 && c.estoqueReal < 11) && c.produtoAtivo == "True"
                                               select new { c.produtoId, c.produtoFornecedor });
                if (fornecedor > 0)
                    foraDeLinhaEstoqueBaixo = foraDeLinhaEstoqueBaixo.Where(x => x.produtoFornecedor == fornecedor);

                if (categoria > 0)
                    foraDeLinhaEstoqueBaixo = (from c in foraDeLinhaEstoqueBaixo
                                               join cat in data.tbJuncaoProdutoCategorias on c.produtoId equals cat.produtoId
                                               where cat.categoriaId == categoria
                                               select c);
                result = new JavaScriptSerializer().Serialize(foraDeLinhaEstoqueBaixo.Count().ToString());
            }
        }
        return result;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string EstoqueBaixo(int fornecedor = 0, int categoria = 0, int marca = 0)
    {

        string result = "";

        using (var data = new dbCommerceDataContext())
        {

            using (
                var txn = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required,
                    new System.Transactions.TransactionOptions
                    {
                        IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                    }))

            {
                var estoqueBaixo = (from c in data.tbProdutos
                                    where
                                        c.produtoEstoqueAtual > 0 && c.produtoEstoqueAtual < 11 && c.produtoAtivo == "True"
                                    select new { c.produtoId, c.produtoFornecedor });
                if (fornecedor > 0)
                    estoqueBaixo = estoqueBaixo.Where(x => x.produtoFornecedor == fornecedor);

                if (categoria > 0)
                    estoqueBaixo = (from c in estoqueBaixo
                                    join cat in data.tbJuncaoProdutoCategorias on c.produtoId equals cat.produtoId
                                    where cat.categoriaId == categoria
                                    select c);

                result = new JavaScriptSerializer().Serialize(estoqueBaixo.Count().ToString());
            }
        }
        return result;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string ProdutoDesativadoComEstoqueMaiorQue0(int fornecedor = 0, int categoria = 0, int marca = 0)
    {

        string result = "";

        using (var data = new dbCommerceDataContext())
        {

            using (
                var txn = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required,
                    new System.Transactions.TransactionOptions
                    {
                        IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                    }))

            {
                var listaEstoque = (from c in data.tbProdutos
                                    join pe in data.tbProdutoEstoques on c.produtoId equals pe.produtoId
                                    where !pe.enviado && pe.itemPedidoIdReserva == null
                                    select new { pe.produtoId, pe.enviado, pe.itemPedidoIdReserva, c.produtoFornecedor, c.produtoAtivo, c.estoqueReal, c.produtoPaiId, c.produtoNome }).ToList();

                if (fornecedor > 0)
                    listaEstoque = listaEstoque.Where(x => x.produtoFornecedor == fornecedor).ToList();

                if (categoria > 0)
                    listaEstoque = (from c in listaEstoque
                                    join cat in data.tbJuncaoProdutoCategorias on c.produtoId equals cat.produtoId
                                    where cat.categoriaId == categoria
                                    select c).Distinct().ToList();

                var listaProdutoCombo = (from pe in listaEstoque
                                         join d in data.tbProdutoRelacionados on pe.produtoId equals d.idProdutoFilho
                                         where
                                             pe.produtoAtivo == "False" && pe.estoqueReal > 0
                                         select new
                                         {
                                             pe.produtoId,
                                             produto = pe.produtoNome,
                                             ativo = pe.produtoAtivo,
                                             pe.produtoPaiId,
                                             pe.estoqueReal
                                         }).Distinct().ToList();

                var produtoPaiDoCombo = (from pc in listaProdutoCombo
                                         join c in data.tbProdutoRelacionados on pc.produtoId equals c.idProdutoPai
                                         where c.tbProduto.produtoAtivo == "False"
                                         select new
                                         {
                                             c.tbProduto.produtoId,
                                             produto = c.tbProduto.produtoNome,
                                             ativo = c.tbProduto.produtoAtivo,
                                             c.tbProduto.estoqueReal
                                         }).Distinct().ToList();

                var listaProdutoNaoCombo = (from pe in listaEstoque
                                            join d in data.tbProdutoRelacionados on pe.produtoId equals d.idProdutoFilho into nc
                                            from d in nc.DefaultIfEmpty()
                                            where
                                                !nc.Select(x => x.idProdutoFilho).Any() && pe.produtoAtivo == "False" && pe.estoqueReal > 0
                                            select new
                                            {
                                                pe.produtoId,
                                                produto = pe.produtoNome,
                                                ativo = pe.produtoAtivo,
                                                pe.estoqueReal
                                            }).Distinct().ToList();

                var listaFinal = produtoPaiDoCombo.Union(listaProdutoNaoCombo).ToList();

                result = new JavaScriptSerializer().Serialize(listaFinal.Count().ToString());
            }
        }
        return result;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string ProdutoAtivoSemCategoria(int fornecedor = 0, int categoria = 0, int marca = 0)
    {
        string result = "";

        using (var data = new dbCommerceDataContext())
        {

            using (
                var txn = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required,
                    new System.Transactions.TransactionOptions
                    {
                        IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                    }))

            {

                var produtoAtivoSemCategoria = (from c in data.tbProdutos
                                                where c.produtoAtivo == "True"
                                                join cat in data.tbJuncaoProdutoCategorias on c.produtoId equals cat.produtoId into psc
                                                from cat in psc.DefaultIfEmpty()
                                                where false
                                                select new { c.produtoId, c.produtoFornecedor });

                if (fornecedor > 0)
                    produtoAtivoSemCategoria = produtoAtivoSemCategoria.Where(x => x.produtoFornecedor == fornecedor);

                if (categoria > 0)
                    produtoAtivoSemCategoria = (from c in produtoAtivoSemCategoria
                                                join cat in data.tbJuncaoProdutoCategorias on c.produtoId equals cat.produtoId
                                                where cat.categoriaId == categoria
                                                select c);

                result = new JavaScriptSerializer().Serialize(produtoAtivoSemCategoria.Count().ToString());
            }
        }
        return result;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string CategoriaAtivaSemProduto(int fornecedor = 0, int categoria = 0, int marca = 0)
    {

        string result = "";

        using (var data = new dbCommerceDataContext())
        {

            using (
                var txn = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required,
                    new System.Transactions.TransactionOptions
                    {
                        IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                    }))

            {
                var categoriaAtivaSemProduto = (from c in data.tbProdutos
                                                where c.produtoAtivo == "True"
                                                join cat in data.tbJuncaoProdutoCategorias on c.produtoId equals cat.produtoId into psc
                                                from cat in psc.DefaultIfEmpty()
                                                where !psc.Select(x => x.produtoId == null && (x.tbProdutoCategoria.exibirSite ?? false)).Any()
                                                select new { c.produtoId, c.produtoFornecedor });
                if (fornecedor > 0)
                    categoriaAtivaSemProduto = categoriaAtivaSemProduto.Where(x => x.produtoFornecedor == fornecedor);

                if (categoria > 0)
                    categoriaAtivaSemProduto = (from c in categoriaAtivaSemProduto
                                                join cat in data.tbJuncaoProdutoCategorias on c.produtoId equals cat.produtoId
                                                where cat.categoriaId == categoria
                                                select c);

                result = new JavaScriptSerializer().Serialize(categoriaAtivaSemProduto.Count().ToString());
            }
        }
        return result;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string CategoriaAtivaSemDescricao()
    {
        string result = "";

        using (var data = new dbCommerceDataContext())
        {

            using (
                var txn = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required,
                    new System.Transactions.TransactionOptions
                    {
                        IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                    }))

            {
                var categoriaAtivaSemDescricao = (from c in data.tbProdutoCategorias
                                                  where c.exibirSite == true && (c.categoriaDescricao ?? "") == ""
                                                  select new { c.categoriaId });

                result = new JavaScriptSerializer().Serialize(categoriaAtivaSemDescricao.Count().ToString());
            }
        }
        return result;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string ProdutoAtivoSemFoto(int fornecedor = 0, int categoria = 0, int marca = 0)
    {
        string result = "";

        using (var data = new dbCommerceDataContext())
        {

            using (
                var txn = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required,
                    new System.Transactions.TransactionOptions
                    {
                        IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                    }))

            {
                var produtoAtivoSemFoto = (from c in data.tbProdutos
                                           join pf in data.tbProdutoFotos on c.produtoId equals pf.produtoId into psf
                                           from pf in psf.DefaultIfEmpty()
                                           where c.produtoAtivo == "True" && !psf.Select(x => x.produtoId).Any()
                                           select new { c.produtoId, c.produtoFornecedor }).Distinct();
                if (fornecedor > 0)
                    produtoAtivoSemFoto = produtoAtivoSemFoto.Where(x => x.produtoFornecedor == fornecedor);

                if (categoria > 0)
                    produtoAtivoSemFoto = (from c in produtoAtivoSemFoto
                                           join cat in data.tbJuncaoProdutoCategorias on c.produtoId equals cat.produtoId
                                           where cat.categoriaId == categoria
                                           select c);

                result = new JavaScriptSerializer().Serialize(produtoAtivoSemFoto.Count().ToString());
            }
        }
        return result;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string ProdutoMostrandoComoEsgotado(int fornecedor = 0, int categoria = 0, int marca = 0)
    {
        string result = "";

        using (var data = new dbCommerceDataContext())
        {

            using (
                var txn = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required,
                    new System.Transactions.TransactionOptions
                    {
                        IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                    }))

            {
                var produtoAtivosComEstoque0 = (from c in data.tbProdutos
                                                where
                                                    c.produtoAtivo == "True" &&
                                                    (c.produtoEstoqueAtual <= 0 && (c.produtoEstoqueAtual <= c.produtoEstoqueMinimo))
                                                select new
                                                {
                                                    c.produtoId,
                                                    c.produtoFornecedor
                                                });
                if (fornecedor > 0)
                    produtoAtivosComEstoque0 = produtoAtivosComEstoque0.Where(x => x.produtoFornecedor == fornecedor);

                if (categoria > 0)
                    produtoAtivosComEstoque0 = (from c in produtoAtivosComEstoque0
                                                join cat in data.tbJuncaoProdutoCategorias on c.produtoId equals cat.produtoId
                                                where cat.categoriaId == categoria
                                                select c);

                result = new JavaScriptSerializer().Serialize(produtoAtivosComEstoque0.Count().ToString());
            }
        }
        return result;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string ProdutoAtivoSemDescricao(int fornecedor = 0, int categoria = 0, int marca = 0)
    {
        string result = "";

        using (var data = new dbCommerceDataContext())
        {

            using (
                var txn = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required,
                    new System.Transactions.TransactionOptions
                    {
                        IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                    }))

            {
                var produtoAtivoSemDescricao = (from c in data.tbProdutos
                                                where c.produtoAtivo == "True" && (c.produtoDescricao ?? "") == ""
                                                select new { c.produtoId, c.produtoFornecedor });
                if (fornecedor > 0)
                    produtoAtivoSemDescricao = produtoAtivoSemDescricao.Where(x => x.produtoFornecedor == fornecedor);

                if (categoria > 0)
                    produtoAtivoSemDescricao = (from c in produtoAtivoSemDescricao
                                                join cat in data.tbJuncaoProdutoCategorias on c.produtoId equals cat.produtoId
                                                where cat.categoriaId == categoria
                                                select c);

                result = new JavaScriptSerializer().Serialize(produtoAtivoSemDescricao.Count().ToString());
            }
        }
        return result;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string ProdutoAtivoSemColecao(int fornecedor = 0, int categoria = 0, int marca = 0)
    {
        string result = "";

        using (var data = new dbCommerceDataContext())
        {

            using (
                var txn = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required,
                    new System.Transactions.TransactionOptions
                    {
                        IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                    }))

            {
                var produtoAtivoSemColecao = (from c in data.tbProdutos
                                              join pc in data.tbJuncaoProdutoColecaos on c.produtoId equals pc.produtoId into asc
                                              from pc in asc.DefaultIfEmpty()
                                              where c.produtoAtivo == "True" && !asc.Select(x => x.produtoId).Any()
                                              select new
                                              {
                                                  c.produtoId,
                                                  c.produtoFornecedor
                                              }).Distinct();
                if (fornecedor > 0)
                    produtoAtivoSemColecao = produtoAtivoSemColecao.Where(x => x.produtoFornecedor == fornecedor);

                if (categoria > 0)
                    produtoAtivoSemColecao = (from c in produtoAtivoSemColecao
                                              join cat in data.tbJuncaoProdutoCategorias on c.produtoId equals cat.produtoId
                                              where cat.categoriaId == categoria
                                              select c);

                result = new JavaScriptSerializer().Serialize(produtoAtivoSemColecao.Count().ToString());
            }
        }
        return result;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string ProdutoAtivoSemComposicao(int fornecedor = 0, int categoria = 0, int marca = 0)
    {
        string result = "";

        using (var data = new dbCommerceDataContext())
        {

            using (
                var txn = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required,
                    new System.Transactions.TransactionOptions
                    {
                        IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                    }))

            {
                var produtoAtivoSemComposicao = (from c in data.tbProdutos
                                                 join ia in data.tbInformacaoAdcionals on c.produtoId equals ia.produtoId into psia
                                                 from ia in psia.DefaultIfEmpty()
                                                 where c.produtoAtivo == "True" && !psia.Select(x => x.produtoId).Any()
                                                 select new { c.produtoId, c.produtoFornecedor }).Distinct();
                if (fornecedor > 0)
                    produtoAtivoSemComposicao = produtoAtivoSemComposicao.Where(x => x.produtoFornecedor == fornecedor);

                if (categoria > 0)
                    produtoAtivoSemComposicao = (from c in produtoAtivoSemComposicao
                                                 join cat in data.tbJuncaoProdutoCategorias on c.produtoId equals cat.produtoId
                                                 where cat.categoriaId == categoria
                                                 select c);

                result = new JavaScriptSerializer().Serialize(produtoAtivoSemComposicao.Count().ToString());
            }
        }
        return result;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string ProdutoAtivoEmPromocao(int fornecedor = 0, int categoria = 0, int marca = 0)
    {
        string result = "";

        using (var data = new dbCommerceDataContext())
        {

            using (
                var txn = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required,
                    new System.Transactions.TransactionOptions
                    {
                        IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                    }))

            {
                var produtoAtivoEmPromocao = (from c in data.tbProdutos
                                              where c.produtoAtivo == "True" && (c.produtoPromocao ?? "") == "True"
                                              select new { c.produtoId, c.produtoFornecedor });
                if (fornecedor > 0)
                    produtoAtivoEmPromocao = produtoAtivoEmPromocao.Where(x => x.produtoFornecedor == fornecedor);

                if (categoria > 0)
                    produtoAtivoEmPromocao = (from c in produtoAtivoEmPromocao
                                              join cat in data.tbJuncaoProdutoCategorias on c.produtoId equals cat.produtoId
                                              where cat.categoriaId == categoria
                                              select c);

                result = new JavaScriptSerializer().Serialize(produtoAtivoEmPromocao.Count().ToString());
            }
        }
        return result;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string FiltroSemProdutos(int fornecedor = 0, int categoria = 0, int marca = 0)
    {
        string result = "";

        using (var data = new dbCommerceDataContext())
        {

            using (
                var txn = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required,
                    new System.Transactions.TransactionOptions
                    {
                        IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                    }))

            {
                var filtros =
                    (from c in data.tbProdutoCategorias
                     where c.categoriaPaiId > 0 && c.exibirSite == false && c.idSite == 1
                     select c);

                var filtroSemProduto = (from c in filtros
                                        join d in data.tbJuncaoProdutoCategorias on c.categoriaId equals d.categoriaId into fsp
                                        from d in fsp.DefaultIfEmpty()
                                        where !fsp.Select(x => x.categoriaId).Any()
                                        select new
                                        {
                                            categoria = c.categoriaNome,
                                            d.tbProduto.produtoFornecedor
                                        });
                if (fornecedor > 0)
                    filtroSemProduto = filtroSemProduto.Where(x => x.produtoFornecedor == fornecedor);

                result = new JavaScriptSerializer().Serialize(filtroSemProduto.Count().ToString());
            }
        }
        return result;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string PrecoDeCustoMaiorQuePrecoVenda(int fornecedor = 0, int categoria = 0, int marca = 0)
    {
        string result = "";

        using (var data = new dbCommerceDataContext())
        {

            using (
                var txn = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required,
                    new System.Transactions.TransactionOptions
                    {
                        IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                    }))

            {
                var precoDeCustoMaiorQuePrecoVenda = (from c in data.tbProdutos
                                                      where c.produtoAtivo == "True" && (c.produtoPrecoDeCusto > c.produtoPreco)
                                                      select new { c.produtoId, c.produtoFornecedor });
                if (fornecedor > 0)
                    precoDeCustoMaiorQuePrecoVenda =
                        precoDeCustoMaiorQuePrecoVenda.Where(x => x.produtoFornecedor == fornecedor);

                if (categoria > 0)
                    precoDeCustoMaiorQuePrecoVenda = (from c in precoDeCustoMaiorQuePrecoVenda
                                                      join cat in data.tbJuncaoProdutoCategorias on c.produtoId equals cat.produtoId
                                                      where cat.categoriaId == categoria
                                                      select c);

                result = new JavaScriptSerializer().Serialize(precoDeCustoMaiorQuePrecoVenda.Count().ToString());
            }
        }
        return result;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string ProdutosComEstoque0OuNegativo(int fornecedor = 0, int categoria = 0, int marca = 0)
    {
        string result = "";

        using (var data = new dbCommerceDataContext())
        {

            using (
                var txn = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required,
                    new System.Transactions.TransactionOptions
                    {
                        IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                    }))

            {
                var estoque0OuNegativo = (from c in data.tbProdutos
                                          where c.produtoEstoqueAtual <= 0 && c.produtoAtivo == "True"
                                          select new { c.produtoId, c.produtoFornecedor });

                if (fornecedor > 0)
                    estoque0OuNegativo = estoque0OuNegativo.Where(x => x.produtoFornecedor == fornecedor);

                if (categoria > 0)
                    estoque0OuNegativo = (from c in estoque0OuNegativo
                                          join cat in data.tbJuncaoProdutoCategorias on c.produtoId equals cat.produtoId
                                          where cat.categoriaId == categoria
                                          select c);

                result = new JavaScriptSerializer().Serialize(estoque0OuNegativo.Count().ToString());
            }
        }
        return result;
    }

    [WebMethod]
    public string MarcasPorFornecedor(int fornecedor = 0)
    {
        string result = "";

        using (var data = new dbCommerceDataContext())
        {

            using (
                var txn = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required,
                    new System.Transactions.TransactionOptions
                    {
                        IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                    }))

            {
                List<int> marcas = new List<int>();
                marcas =
                    (from c in data.tbProdutos
                     where c.produtoFornecedor == fornecedor
                     select new { c.produtoMarca }).Distinct().Select(x => x.produtoMarca).ToList();

                var dados = (from c in data.tbMarcas where marcas.Contains(c.marcaId) orderby c.marcaNome select new { c.marcaId, c.marcaNome }).ToList();

                result = new JavaScriptSerializer().Serialize(dados);
            }
        }

        return result;
    }

    [WebMethod]
    public string CategoriasPorFornecedor(int fornecedor = 0)
    {
        string result = "";
        using (var data = new dbCommerceDataContext())
        {

            using (
                var txn = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required,
                    new System.Transactions.TransactionOptions
                    {
                        IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                    }))

            {
                var categoriasPorFornecedor = (from c in data.tbProdutos
                                               join pc in data.tbJuncaoProdutoCategorias on c.produtoId equals pc.produtoId
                                               where c.produtoFornecedor == fornecedor && pc.tbProdutoCategoria.exibirSite == true
                                               select new { pc.tbProdutoCategoria.categoriaId, pc.tbProdutoCategoria.categoriaNome }).Distinct().OrderBy(x => x.categoriaNome).ToList();

                result = new JavaScriptSerializer().Serialize(categoriasPorFornecedor);
            }
        }

        return result;
    }
}
