﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

/// <summary>
/// Summary description for rnEntrega
/// </summary>
public class rnEntrega
{
    public static string DefineEntregaPedido(int pedidoId, int idPedidoEnvio)
    {
        var data = new dbCommerceDataContext();
        var pedido = (from c in data.tbPedidos where c.pedidoId == pedidoId select c).First();
        var volumes = (from c in data.tbPedidoPacotes where c.idPedido == pedidoId && c.idPedidoEnvio == idPedidoEnvio select c);
        var envio = (from c in data.tbPedidoEnvios where c.idPedidoEnvio == idPedidoEnvio select c).First();
        envio.nfeObrigatoria = true;
        List<int> produtosEnvio;

        var produtosPacotes = new List<rnFrete2.Pacotes>();
        using (var txn = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required,
            new System.Transactions.TransactionOptions
            {
                IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
            }))
        {

            var produtosLista = (from c in data.tbProdutoEstoques
                                 where c.idPedidoEnvio == idPedidoEnvio
                                 select new
                                 {
                                     c.produtoId,
                                     c.tbProduto.produtoPeso,
                                     c.tbProduto.largura,
                                     c.tbProduto.altura,
                                     c.tbProduto.profundidade
                                 }).ToList();
            produtosEnvio = (from c in produtosLista select c.produtoId).ToList();


            produtosPacotes = (from c in produtosLista
                               select new rnFrete2.Pacotes
                               {
                                   altura = c.altura ?? 0,
                                   comprimento = c.profundidade ?? 0,
                                   largura = c.largura ?? 0,
                                   peso = Convert.ToInt32(c.produtoPeso)
                               }).ToList();
        }

        var produtosPrecos =
            (from c in data.tbProdutos
             where produtosEnvio.Contains(c.produtoId)
             select new { preco = (c.produtoPrecoPromocional ?? 0) > c.produtoPreco ? c.produtoPrecoPromocional : c.produtoPreco, c.produtoPrecoDeCusto, c.altura, c.largura, c.profundidade });
        decimal valorTotalDoPedido = 0;
        decimal valorTotalDoCustoDoPedido = 0;

        if (envio.idCentroDeDistribuicao < 4)
        {
            var hoje = DateTime.Now.Date;
            try
            {
                if (produtosPrecos.Any())
                {
                    valorTotalDoPedido = produtosPrecos.Sum(x => (decimal)x.preco);
                    valorTotalDoCustoDoPedido = produtosPrecos.Sum(x => (decimal)x.produtoPrecoDeCusto);
                }
                else
                {
                    valorTotalDoPedido = (decimal)pedido.valorCobrado;
                    valorTotalDoCustoDoPedido = (decimal)pedido.valorCobrado / 2;
                }
                int pesoTotal = volumes.Sum(x => x.peso);
                var pacotes = new List<rnFrete.Pacotes>();
                pacotes = (from c in volumes
                           select new rnFrete.Pacotes
                           {
                               altura = Convert.ToInt32(c.altura),
                               comprimento = Convert.ToInt32(c.profundidade),
                               largura = Convert.ToInt32(c.largura),
                               peso = c.peso
                           }).ToList();
                long cep = Convert.ToInt64(pedido.endCep.Replace("-", "").Replace(".", "").Replace(" ", "").Trim());

                #region Calculo na Jadlog
                var valoresJadlog = rnFrete.CalculaFrete(13, pedido.endCep.Replace("-", "").Trim(), valorTotalDoPedido, pacotes);
                var valoresJadlogCom = rnFrete.CalculaFreteWebserviceJadlog(valoresJadlog.pesoAferido, cep, "jadlogcom", valorTotalDoPedido);
                if (valoresJadlog.valor > 0)
                {
                    var valorJadlogWs = rnFrete.CalculaFreteWebserviceJadlog(valoresJadlog.pesoAferido, cep, "jadlog", valorTotalDoPedido);
                    if (valorJadlogWs.valor > 0)
                    {
                        valoresJadlog = valorJadlogWs;
                    }
                }

                #endregion

                #region Calculo TNT
                var valoresTnt = rnFrete.CalculaFrete(10, Convert.ToInt32(pesoTotal), pedido.endCep.Replace("-", "").Trim(), valorTotalDoPedido, 0, 0, 0);
                #endregion

                #region Calculo Correios

                if (!volumes.Any(x => x.cubado == true))
                {
                    int prazoCorreios = 0;
                    decimal valorTotalCorreios = 0;
                    bool correiosPacoteIndividual = true;
                    if (correiosPacoteIndividual)
                    {
                        foreach (var pacote in volumes)
                        {
                            decimal valorDoPacote = valorTotalDoPedido / volumes.Count();
                            var consultaCorreios = rnFrete.CalculaFrete(12, Convert.ToInt32(pesoTotal),
                            pedido.endCep.Replace("-", "").Trim(), valorTotalDoPedido, 0, 0, 0);
                            valorTotalCorreios += consultaCorreios.valor;
                            if (consultaCorreios.prazo > prazoCorreios) prazoCorreios = consultaCorreios.prazo;
                        }
                    }
                    else
                    {
                        var consultaCorreios = rnFrete.CalculaFrete(12, Convert.ToInt32(pesoTotal),
                            pedido.endCep.Replace("-", "").Trim(), valorTotalDoPedido, 0, 0, 0);
                        valorTotalCorreios += consultaCorreios.valor;
                        if (consultaCorreios.prazo > prazoCorreios) prazoCorreios = consultaCorreios.prazo;
                    }


                    var valorTransporteCorreios = new tbPedidoEnvioValorTransporte();
                    valorTransporteCorreios.idPedidoEnvio = idPedidoEnvio;
                    valorTransporteCorreios.prazo = prazoCorreios;
                    valorTransporteCorreios.servico = "pac";
                    valorTransporteCorreios.valor = Convert.ToDecimal(valorTotalCorreios.ToString("0.00"));
                    data.tbPedidoEnvioValorTransportes.InsertOnSubmit(valorTransporteCorreios);
                }

                #endregion


                #region Calculo na Belle Express
                var valoresBelle = rnFrete.CalculaFrete(17, Convert.ToInt32(pesoTotal), pedido.endCep.Replace("-", "").Trim(), valorTotalDoPedido, 0, 0, 0);
                #endregion


                #region Calculo na Plimor
                var valoresPlimor = rnFrete.CalculaFrete(19, Convert.ToInt32(pesoTotal), pedido.endCep.Replace("-", "").Trim(), valorTotalDoPedido, 0, 0, 0);
                #endregion

                var cepLong = Convert.ToInt64(pedido.endCep.Replace("-", ""));
                var valorTransporteTnt = new tbPedidoEnvioValorTransporte();
                valorTransporteTnt.idPedidoEnvio = idPedidoEnvio;
                valorTransporteTnt.prazo = valoresTnt.prazo;
                valorTransporteTnt.servico = "tnt";
                valorTransporteTnt.valor = Convert.ToDecimal(valoresTnt.valor.ToString("0.00"));
                data.tbPedidoEnvioValorTransportes.InsertOnSubmit(valorTransporteTnt);


                var valorTransporteJadlog = new tbPedidoEnvioValorTransporte();
                valorTransporteJadlog.idPedidoEnvio = idPedidoEnvio;
                valorTransporteJadlog.prazo = valoresJadlog.prazo;
                valorTransporteJadlog.servico = "jadlog";
                valorTransporteJadlog.valor = Convert.ToDecimal(valoresJadlog.valor.ToString("0.00"));
                data.tbPedidoEnvioValorTransportes.InsertOnSubmit(valorTransporteJadlog);

                var valorTransporteBelle = new tbPedidoEnvioValorTransporte();
                valorTransporteBelle.idPedidoEnvio = idPedidoEnvio;
                valorTransporteBelle.prazo = valoresBelle.prazo;
                valorTransporteBelle.servico = "belle";
                valorTransporteBelle.valor = Convert.ToDecimal(valoresBelle.valor.ToString("0.00"));
                data.tbPedidoEnvioValorTransportes.InsertOnSubmit(valorTransporteBelle);


                var valorTransportePlimor = new tbPedidoEnvioValorTransporte();
                valorTransportePlimor.idPedidoEnvio = idPedidoEnvio;
                valorTransportePlimor.prazo = valoresPlimor.prazo;
                valorTransportePlimor.servico = "plimor";
                valorTransportePlimor.valor = Convert.ToDecimal(valoresPlimor.valor.ToString("0.00"));
                data.tbPedidoEnvioValorTransportes.InsertOnSubmit(valorTransportePlimor);

                if (valoresJadlogCom.valor > 0 && valoresJadlog.valor > 0)
                {
                    var valorTransporteJadlogCom = new tbPedidoEnvioValorTransporte();
                    valorTransporteJadlogCom.idPedidoEnvio = idPedidoEnvio;
                    valorTransporteJadlogCom.prazo = valoresJadlogCom.prazo;
                    valorTransporteJadlogCom.servico = "jadlogcom";
                    valorTransporteJadlogCom.valor = Convert.ToDecimal(valoresJadlogCom.valor.ToString("0.00"));
                    data.tbPedidoEnvioValorTransportes.InsertOnSubmit(valorTransporteJadlogCom);
                    data.SubmitChanges();
                }

                data.SubmitChanges();

                var valores = (from c in data.tbPedidoEnvioValorTransportes where c.valor > 0 && c.idPedidoEnvio == idPedidoEnvio select c);

                int prazoEntregaPedido = 0;
                int.TryParse(pedido.prazoDeEntrega, out prazoEntregaPedido);
                var valoresUtilizaveis = valores;

                var liberadas = (from c in data.tbTransportadoraLiberadas select c).First();
                if ((liberadas.jadlog == false && envio.idCentroDeDistribuicao < 3) | (liberadas.jadlogcd3 == false && envio.idCentroDeDistribuicao == 3))
                {
                    valoresUtilizaveis = valoresUtilizaveis.Where(x => x.servico != "jadlog");
                    valoresUtilizaveis = valoresUtilizaveis.Where(x => x.servico != "jadlogexpressa");
                    valoresUtilizaveis = valoresUtilizaveis.Where(x => x.servico != "jadlogcom");
                    valoresUtilizaveis = valoresUtilizaveis.Where(x => x.servico != "jadlogrodo");
                }

                if ((liberadas.tnt == false && envio.idCentroDeDistribuicao < 3) | (liberadas.tntcd3 == false && envio.idCentroDeDistribuicao == 3))
                {
                    valoresUtilizaveis = valoresUtilizaveis.Where(x => x.servico != "tnt");
                }
                var enviosCorreiosHoje = (from c in data.tbPedidoEnvios
                                          where
                        c.formaDeEnvio.ToLower() == "pac" && c.dataFimEmbalagem != null &&
                        c.dataFimEmbalagem.Value.Date == hoje
                                          select c).Count();

                var enviosBelleHoje = (from c in data.tbPedidoEnvios
                                       where
                     c.formaDeEnvio.ToLower() == "belle" && c.dataFimEmbalagem != null &&
                     c.dataFimEmbalagem.Value.Date == hoje
                                       select c).Count();

                var enviosPlimorHoje = (from c in data.tbPedidoEnvios
                                        where
                      c.formaDeEnvio.ToLower() == "plimor" && c.dataFimEmbalagem != null &&
                      c.dataFimEmbalagem.Value.Date == hoje
                                        select c).Count();

                if (((liberadas.correios == false && envio.idCentroDeDistribuicao < 3) | (liberadas.correioscd3 == false && envio.idCentroDeDistribuicao == 3)) | enviosCorreiosHoje >= liberadas.limiteCorreios)
                {
                    valoresUtilizaveis = valoresUtilizaveis.Where(x => x.servico != "pac");
                }


                if (((liberadas.belle == false && envio.idCentroDeDistribuicao < 3) | (liberadas.bellecd3 == false && envio.idCentroDeDistribuicao == 3)) | enviosBelleHoje >= liberadas.limiteBelle)
                {
                    valoresUtilizaveis = valoresUtilizaveis.Where(x => x.servico != "belle");
                }

                if (((liberadas.plimor == false && envio.idCentroDeDistribuicao < 3) | (liberadas.plimorcd3 == false && envio.idCentroDeDistribuicao == 3)) | enviosPlimorHoje >= liberadas.limitePlimor)
                {
                    valoresUtilizaveis = valoresUtilizaveis.Where(x => x.servico != "plimor");
                }

                if (cepLong > 01000000 && cepLong < 09999999)
                {
                    valoresUtilizaveis = valoresUtilizaveis.Where(x => x.servico != "tnt");
                }
                /*if (pedido.endEstado.ToLower() == "pe")//Pedidos Pernambuco sair pela Jadlog
                {
                    valoresUtilizaveis = valoresUtilizaveis.Where(x => x.servico == "jadlog");// pega só jadlog
                }*/
                if (pedido.endEstado.ToLower() == "rj")//Pedidos Pernambuco sair pela Jadlog
                {
                    valoresUtilizaveis = valoresUtilizaveis.Where(x => x.servico != "pac");// nao envar pac no rio
                }

                if (envio.idCentroDeDistribuicao == 3)
                {
                    int larguraTntCd3 = liberadas.larguraMaximaTntCd3 ?? 0;
                    if (larguraTntCd3 > 0)
                    {
                        if (produtosPrecos.Any(x => x.largura > larguraTntCd3 | x.altura > larguraTntCd3 | x.profundidade > larguraTntCd3))
                        {
                            valoresUtilizaveis = valoresUtilizaveis.Where(x => x.servico != "tnt");
                        }
                    }
                }


                if (envio.idCentroDeDistribuicao == 4)
                {

                    valoresUtilizaveis = valoresUtilizaveis.Where(x => x.servico == "jadlog");
                }


                var valorSelecionado = valoresUtilizaveis.Where(x => x.valor > 0).OrderBy(x => x.valor).FirstOrDefault();
                if (valorSelecionado != null)
                {
                    envio.valorSelecionado = valorSelecionado.valor;
                    envio.prazoSelecionado = valorSelecionado.prazo;
                    envio.formaDeEnvio = valorSelecionado.servico;
                }


                if ((envio.valorSelecionado == null | envio.valorSelecionado == 0) && envio.idCentroDeDistribuicao != 4)
                {
                    int prazoPedido = 0;
                    int.TryParse(pedido.prazoDeEntrega, out prazoPedido);
                    envio.valorSelecionado = 0;
                    envio.prazoSelecionado = prazoPedido;
                    envio.formaDeEnvio = "tnt";
                }
                if ((envio.valorSelecionado == null | envio.valorSelecionado == 0) && envio.idCentroDeDistribuicao == 4)
                {
                    int prazoPedido = 0;
                    int.TryParse(pedido.prazoDeEntrega, out prazoPedido);
                    envio.valorSelecionado = 0;
                    envio.prazoSelecionado = prazoPedido;
                    envio.formaDeEnvio = "jadlog";
                }

                if (!string.IsNullOrEmpty(envio.tbPedido.formaDeEnvio))
                {
                    envio.valorSelecionado = valoresJadlog.valor;
                    envio.prazoSelecionado = valoresJadlog.prazo;
                    envio.formaDeEnvio = envio.tbPedido.formaDeEnvio.ToLower();
                }

                if (envio.tbPedido.gerarReversa)
                {
                    envio.valorSelecionado = valoresJadlog.valor;
                    envio.prazoSelecionado = valoresJadlog.prazo;
                    envio.formaDeEnvio = "jadlog";
                }


                foreach (var volume in volumes)
                {
                    volume.formaDeEnvio = envio.formaDeEnvio;
                }
                envio.nfeObrigatoria = true;
                data.SubmitChanges();

                var dataAlt = new dbCommerceDataContext();
                var envioAlt = (from c in dataAlt.tbPedidoEnvios where c.idPedidoEnvio == envio.idPedidoEnvio select c).First();
                envioAlt.nfeObrigatoria = envio.nfeObrigatoria;
                envioAlt.valorSelecionado = envio.valorSelecionado;
                envioAlt.prazoSelecionado = envio.prazoSelecionado;
                envioAlt.formaDeEnvio = envio.formaDeEnvio;
                envioAlt.valorSelecionado = envio.valorSelecionado;
                dataAlt.SubmitChanges();

                string retornoEnvio = envio.formaDeEnvio;
                if (retornoEnvio == "jadlogcom" | retornoEnvio == "jadlogrodo") retornoEnvio = "jadlog";
                return retornoEnvio;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        else
        {
            var hoje = DateTime.Now.Date;
            var cepLong = Convert.ToInt64(pedido.endCep.Replace("-", ""));
            var frete = rnFrete2.Instance;
            if (produtosPrecos.Any())
            {
                valorTotalDoPedido = produtosPrecos.Sum(x => (decimal)x.preco);
                valorTotalDoCustoDoPedido = produtosPrecos.Sum(x => (decimal)x.produtoPrecoDeCusto);
            }
            else
            {
                valorTotalDoPedido = (decimal)pedido.valorCobrado;
                valorTotalDoCustoDoPedido = (decimal)pedido.valorCobrado / 2;
            }
            int pesoTotal = volumes.Sum(x => x.peso);
            var pacotes = new List<rnFrete2.Pacotes>();
            pacotes = (from c in volumes
                       select new rnFrete2.Pacotes
                       {
                           altura = Convert.ToInt32(c.altura),
                           comprimento = Convert.ToInt32(c.profundidade),
                           largura = Convert.ToInt32(c.largura),
                           peso = c.peso
                       }).ToList();
            long cep = Convert.ToInt64(pedido.endCep.Replace("-", "").Replace(".", "").Replace(" ", "").Trim());

            #region Calculo na Jadlog
            var valoresJadlog = frete.CalculaFrete(13, pedido.endCep.Replace("-", "").Trim(), valorTotalDoPedido, pacotes);
            var valorTransporteJadlog = new tbPedidoEnvioValorTransporte();
            valorTransporteJadlog.idPedidoEnvio = idPedidoEnvio;
            valorTransporteJadlog.prazo = valoresJadlog.prazo;
            valorTransporteJadlog.servico = "jadlog";
            valorTransporteJadlog.valor = Convert.ToDecimal(valoresJadlog.valor.ToString("0.00"));
            valorTransporteJadlog.tipoDeEntregaId = 13;
            data.tbPedidoEnvioValorTransportes.InsertOnSubmit(valorTransporteJadlog);
            /*var valoresJadlogCom = rnFrete.CalculaFreteWebserviceJadlog(valoresJadlog.pesoAferido, cep, "jadlogcom", valorTotalDoPedido);
            if (valoresJadlog.valor > 0)
            {
                var valorJadlogWs = rnFrete.CalculaFreteWebserviceJadlog(valoresJadlog.pesoAferido, cep, "jadlog", valorTotalDoPedido);
                if (valorJadlogWs.valor > 0)
                {
                    valoresJadlog = valorJadlogWs;
                }
            }*/

            #endregion

            #region Calculo TNT
            var valoresTnt = frete.CalculaFrete(10, pedido.endCep.Replace("-", "").Trim(), valorTotalDoPedido, pacotes);
            var valorTransporteTnt = new tbPedidoEnvioValorTransporte();
            valorTransporteTnt.idPedidoEnvio = idPedidoEnvio;
            valorTransporteTnt.prazo = valoresTnt.prazo;
            valorTransporteTnt.servico = "tnt";
            valorTransporteTnt.valor = Convert.ToDecimal(valoresTnt.valor.ToString("0.00"));
            valorTransporteTnt.tipoDeEntregaId = 10;
            data.tbPedidoEnvioValorTransportes.InsertOnSubmit(valorTransporteTnt);


            if (cepLong > 30000000 && cepLong < 39999999)
            {
                    var valoresTntJacutinga = rnFrete.CalculaFrete(22, Convert.ToInt32(pesoTotal), pedido.endCep.Replace("-", "").Trim(), valorTotalDoPedido, 0, 0, 0);
                    var valorTransporteTntJacutinga = new tbPedidoEnvioValorTransporte();
                    valorTransporteTntJacutinga.idPedidoEnvio = idPedidoEnvio;
                    valorTransporteTntJacutinga.prazo = valoresTntJacutinga.prazo;
                    valorTransporteTntJacutinga.servico = "tntjacutinga";
                    valorTransporteTntJacutinga.valor = Convert.ToDecimal(valoresTntJacutinga.valor.ToString("0.00"));
                    valorTransporteTntJacutinga.tipoDeEntregaId = 22;
                    data.tbPedidoEnvioValorTransportes.InsertOnSubmit(valorTransporteTntJacutinga);
                
            }
            #endregion

            #region Calculo Correios

            if (!volumes.Any(x => x.cubado == true))
            {
                int prazoCorreios = 0;
                decimal valorTotalCorreios = 0;
                bool correiosPacoteIndividual = true;
                if (correiosPacoteIndividual)
                {
                    int indice = 0;
                    foreach (var pacote in volumes)
                    {
                        decimal valorDoPacote = valorTotalDoPedido / volumes.Count();

                        var consultaCorreios = frete.CalculaFrete(20, pedido.endCep.Replace("-", "").Trim(), valorTotalDoPedido, pacotes.Skip(indice).Take(1).ToList(), produtosPacotes);
                        valorTotalCorreios += consultaCorreios.valor;
                        if (consultaCorreios.prazo > prazoCorreios) prazoCorreios = consultaCorreios.prazo;
                        indice++;
                    }
                }
                else
                {
                    var consultaCorreios = frete.CalculaFrete(20, Convert.ToInt32(pesoTotal),
                        pedido.endCep.Replace("-", "").Trim(), valorTotalDoPedido, 0, 0, 0);
                    valorTotalCorreios += consultaCorreios.valor;
                    if (consultaCorreios.prazo > prazoCorreios) prazoCorreios = consultaCorreios.prazo;
                }


                var valorTransporteCorreios = new tbPedidoEnvioValorTransporte();
                valorTransporteCorreios.idPedidoEnvio = idPedidoEnvio;
                valorTransporteCorreios.prazo = prazoCorreios;
                valorTransporteCorreios.servico = "pac";
                valorTransporteCorreios.valor = Convert.ToDecimal(valorTotalCorreios.ToString("0.00"));
                valorTransporteCorreios.tipoDeEntregaId = 20;
                data.tbPedidoEnvioValorTransportes.InsertOnSubmit(valorTransporteCorreios);
            }

            #endregion

            #region Calculo na Plimor
            var valoresPlimor = frete.CalculaFrete(21, Convert.ToInt32(pesoTotal), pedido.endCep.Replace("-", "").Trim(), valorTotalDoPedido, 0, 0, 0);
            var valorTransportePlimor = new tbPedidoEnvioValorTransporte();
            valorTransportePlimor.idPedidoEnvio = idPedidoEnvio;
            valorTransportePlimor.prazo = valoresPlimor.prazo;
            valorTransportePlimor.servico = "plimor";
            valorTransportePlimor.valor = Convert.ToDecimal(valoresPlimor.valor.ToString("0.00"));
            valorTransportePlimor.tipoDeEntregaId = 21;
            data.tbPedidoEnvioValorTransportes.InsertOnSubmit(valorTransportePlimor);
            #endregion


            #region Calculo na LBR
            var valoresLbr = frete.CalculaFrete(29, pedido.endCep.Replace("-", "").Trim(), valorTotalDoPedido, pacotes);
            var valorTransporteLbr = new tbPedidoEnvioValorTransporte();
            valorTransporteLbr.idPedidoEnvio = idPedidoEnvio;
            valorTransporteLbr.prazo = valoresLbr.prazo;
            valorTransporteLbr.servico = "lbr";
            valorTransporteLbr.valor = Convert.ToDecimal(valoresLbr.valor.ToString("0.00"));
            valorTransporteLbr.tipoDeEntregaId = 29;
            data.tbPedidoEnvioValorTransportes.InsertOnSubmit(valorTransporteLbr);
            #endregion


            #region Calculo na Jamef
            var valoresJamef = frete.CalculaFrete(28, pedido.endCep.Replace("-", "").Trim(), valorTotalDoPedido, pacotes);
            var valorTransporteJamef = new tbPedidoEnvioValorTransporte();
            valorTransporteJamef.idPedidoEnvio = idPedidoEnvio;
            valorTransporteJamef.prazo = valoresLbr.prazo;
            valorTransporteJamef.servico = "jamef";
            valorTransporteJamef.valor = Convert.ToDecimal(valoresJamef.valor.ToString("0.00"));
            valorTransporteJamef.tipoDeEntregaId = 28;
            data.tbPedidoEnvioValorTransportes.InsertOnSubmit(valorTransporteJamef);
            #endregion



            #region Calculo na Nowlog
            var valoresNowlog = frete.CalculaFrete(25, pedido.endCep.Replace("-", "").Trim(), valorTotalDoPedido, pacotes);
            var valorTransporteNowlog = new tbPedidoEnvioValorTransporte();
            valorTransporteNowlog.idPedidoEnvio = idPedidoEnvio;
            valorTransporteNowlog.prazo = valoresNowlog.prazo;
            valorTransporteNowlog.servico = "nowlog";
            valorTransporteNowlog.valor = Convert.ToDecimal(valoresNowlog.valor.ToString("0.00"));
            valorTransporteNowlog.tipoDeEntregaId = 25;
            data.tbPedidoEnvioValorTransportes.InsertOnSubmit(valorTransporteNowlog);
            #endregion

            #region Calculo na Dialogo
            var valoresDialogo = frete.CalculaFrete(26, pedido.endCep.Replace("-", "").Trim(), valorTotalDoPedido, pacotes);
            var valorTransporteDialogo = new tbPedidoEnvioValorTransporte();
            valorTransporteDialogo.idPedidoEnvio = idPedidoEnvio;
            valorTransporteDialogo.prazo = valoresDialogo.prazo;
            valorTransporteDialogo.servico = "dialogo";
            valorTransporteDialogo.valor = Convert.ToDecimal(valoresDialogo.valor.ToString("0.00"));
            valorTransporteDialogo.tipoDeEntregaId = 26;
            data.tbPedidoEnvioValorTransportes.InsertOnSubmit(valorTransporteDialogo);
            #endregion

            #region Calculo na TotalExpress
            var valoresTotal = frete.CalculaFrete(32, pedido.endCep.Replace("-", "").Trim(), valorTotalDoPedido, pacotes);
            var valorTransporteTotal = new tbPedidoEnvioValorTransporte();
            valorTransporteTotal.idPedidoEnvio = idPedidoEnvio;
            valorTransporteTotal.prazo = valoresTotal.prazo;
            valorTransporteTotal.servico = "totalexpress";
            valorTransporteTotal.valor = Convert.ToDecimal(valoresTotal.valor.ToString("0.00"));
            valorTransporteTotal.tipoDeEntregaId = 32;
            data.tbPedidoEnvioValorTransportes.InsertOnSubmit(valorTransporteTotal);
            #endregion



            #region Calculo na Transfolha
            var valoresTransfolha = frete.CalculaFrete(31, pedido.endCep.Replace("-", "").Trim(), valorTotalDoPedido, pacotes);
            var valorTransporteTransfolha = new tbPedidoEnvioValorTransporte();
            valorTransporteTransfolha.idPedidoEnvio = idPedidoEnvio;
            valorTransporteTransfolha.prazo = valoresTransfolha.prazo;
            valorTransporteTransfolha.servico = "transfolha";
            valorTransporteTransfolha.valor = Convert.ToDecimal(valoresTransfolha.valor.ToString("0.00"));
            valorTransporteTransfolha.tipoDeEntregaId = 31;
            data.tbPedidoEnvioValorTransportes.InsertOnSubmit(valorTransporteTransfolha);
            #endregion

            /*if (valoresJadlogCom.valor > 0 && valoresJadlog.valor > 0)
            {
                var valorTransporteJadlogCom = new tbPedidoEnvioValorTransporte();
                valorTransporteJadlogCom.idPedidoEnvio = idPedidoEnvio;
                valorTransporteJadlogCom.prazo = valoresJadlogCom.prazo;
                valorTransporteJadlogCom.servico = "jadlogcom";
                valorTransporteJadlogCom.valor = Convert.ToDecimal(valoresJadlogCom.valor.ToString("0.00"));
                data.tbPedidoEnvioValorTransportes.InsertOnSubmit(valorTransporteJadlogCom);
                data.SubmitChanges();
            }*/

            data.SubmitChanges();

            int transportadoraId = 0;
            if (!string.IsNullOrEmpty(envio.tbPedido.formaDeEnvio))
            {
                int.TryParse(envio.tbPedido.formaDeEnvio, out transportadoraId);
            }
            var valores = (from c in data.tbPedidoEnvioValorTransportes where c.idPedidoEnvio == idPedidoEnvio select c).ToList();
            //valores = valores.Where(x => x.servico == "pac" | x.servico == "tnt" | x.servico == "plimor" | x.servico.Contains("jadlog")).ToList();
            int prazoEntregaPedido = 0;
            int.TryParse(pedido.prazoDeEntrega, out prazoEntregaPedido);
            var valoresUtilizaveis = valores;
            if (transportadoraId > 0)
            {
                if (valores.Any(x => x.tbTipoDeEntrega.idTransportadora == transportadoraId))
                {
                    valoresUtilizaveis = valores.Where(x => x.tbTipoDeEntrega.idTransportadora == transportadoraId).ToList();
                }
            }

            if (transportadoraId == 0)
            {
                valoresUtilizaveis = valoresUtilizaveis.Where(x => x.valor > 0).ToList();

                if ((cepLong > 01000000 && cepLong < 09999999) | (cepLong > 20000000 && cepLong < 28999999) | (cepLong > 30000000 && cepLong < 39999999))
                {
                    valoresUtilizaveis = valoresUtilizaveis.Where(x => x.servico != "tnt").ToList();
                }
                valoresUtilizaveis = valoresUtilizaveis.Where(x => x.servico != "pac").ToList();
                if (volumes.Any(x => x.idCentroDistribuicao == 5) | volumes.Any(x => x.peso > 20000)) valoresUtilizaveis = valoresUtilizaveis.Where(x => x.servico != "jamef").ToList();
                //valoresUtilizaveis = valoresUtilizaveis.Where(x => x.servico != "lbr").ToList();
                /*var liberadas = (from c in data.tbTransportadoraLiberadas select c).First();
                if ((liberadas.jadlog == false && envio.idCentroDeDistribuicao < 3) | (liberadas.jadlogcd3 == false && envio.idCentroDeDistribuicao == 3))
                {
                    valoresUtilizaveis = valoresUtilizaveis.Where(x => x.servico != "jadlog");
                    valoresUtilizaveis = valoresUtilizaveis.Where(x => x.servico != "jadlogexpressa");
                    valoresUtilizaveis = valoresUtilizaveis.Where(x => x.servico != "jadlogcom");
                    valoresUtilizaveis = valoresUtilizaveis.Where(x => x.servico != "jadlogrodo");
                }

                if ((liberadas.tnt == false && envio.idCentroDeDistribuicao < 3) | (liberadas.tntcd3 == false && envio.idCentroDeDistribuicao == 3))
                {
                    valoresUtilizaveis = valoresUtilizaveis.Where(x => x.servico != "tnt");
                }*/

                /*if (pedido.endEstado.ToLower() == "rj")//Pedidos Pernambuco sair pela Jadlog
                {
                    valoresUtilizaveis = valoresUtilizaveis.Where(x => x.servico != "pac");// nao envar pac no rio
                }*/
            }


            var valorSelecionado = valoresUtilizaveis.OrderBy(x => x.valor).FirstOrDefault();
            if (transportadoraId == 0) valorSelecionado = valoresUtilizaveis.Where(x => x.valor > 0).OrderBy(x => x.valor).FirstOrDefault();
            if (valorSelecionado != null)
            {
                envio.valorSelecionado = valorSelecionado.valor;
                envio.prazoSelecionado = valorSelecionado.prazo;
                envio.formaDeEnvio = valorSelecionado.servico;
                envio.tipoDeEntregaId = valorSelecionado.tipoDeEntregaId;

            }


            if ((envio.valorSelecionado == null | envio.valorSelecionado == 0) && transportadoraId == 0)
            {
                int prazoPedido = 0;
                int.TryParse(pedido.prazoDeEntrega, out prazoPedido);
                envio.valorSelecionado = 0;
                envio.prazoSelecionado = prazoPedido;
                envio.formaDeEnvio = "jadlog";
                envio.tipoDeEntregaId = 13;
            }

            /*if (!string.IsNullOrEmpty(envio.tbPedido.formaDeEnvio))
            {
                if (envio.tbPedido.formaDeEnvio == "jadlog" | envio.tbPedido.formaDeEnvio == "pac")
                {
                    envio.valorSelecionado = valoresJadlog.valor;
                    envio.prazoSelecionado = valoresJadlog.prazo;
                    envio.formaDeEnvio = envio.tbPedido.formaDeEnvio.ToLower();
                }
            }*/

            /*if (envio.tbPedido.gerarReversa)
            {
                envio.valorSelecionado = valoresJadlog.valor;
                envio.prazoSelecionado = valoresJadlog.prazo;
                envio.formaDeEnvio = "jadlog";
            }*/


            
            envio.nfeObrigatoria = true;
            //data.SubmitChanges();

            
                var dataAlt = new dbCommerceDataContext();
                var volumesAlt = (from c in dataAlt.tbPedidoPacotes where c.idPedido == pedidoId && c.idPedidoEnvio == idPedidoEnvio select c);
                foreach (var volume in volumesAlt)
                {
                    volume.formaDeEnvio = envio.formaDeEnvio;
                }
                var envioAlt = (from c in dataAlt.tbPedidoEnvios where c.idPedidoEnvio == envio.idPedidoEnvio select c).First();
                envioAlt.nfeObrigatoria = envio.nfeObrigatoria;
                envioAlt.valorSelecionado = envio.valorSelecionado;
                envioAlt.prazoSelecionado = envio.prazoSelecionado;
                envioAlt.formaDeEnvio = envio.formaDeEnvio;
                envioAlt.valorSelecionado = envio.valorSelecionado;
                envioAlt.tipoDeEntregaId = envio.tipoDeEntregaId;
                envioAlt.nfeObrigatoria = true;
                if (envio.tipoDeEntregaId == null)
                {
                    var transporte = (from c in data.tbPedidoEnvioValorTransportes where c.idPedidoEnvio == idPedidoEnvio && c.servico.ToLower() == envio.formaDeEnvio.ToLower() select c).FirstOrDefault();
                    if (transporte != null) envioAlt.tipoDeEntregaId = transporte.tipoDeEntregaId;
                }
                dataAlt.SubmitChanges();
                
            
            string retornoEnvio = envio.formaDeEnvio;
            if (retornoEnvio == "jadlogcom" | retornoEnvio == "jadlogrodo") retornoEnvio = "jadlog";
            return retornoEnvio;
        }


    }

    public static void AtualizaEntregasPedido(int idPedidoEnvio)
    {
        var data = new dbCommerceDataContext();
        var volumes = (from c in data.tbPedidoPacotes where c.idPedidoEnvio == idPedidoEnvio select c);
        var envio = (from c in data.tbPedidoEnvios where c.idPedidoEnvio == idPedidoEnvio select c).First();
        var pedido = (from c in data.tbPedidos where c.pedidoId == envio.idPedido select c).First();
        envio.nfeObrigatoria = true;
        List<int> produtosEnvio;

        var produtosPacotes = new List<rnFrete2.Pacotes>();
        using (var txn = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required,
            new System.Transactions.TransactionOptions
            {
                IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
            }))
        {

            var produtosLista = (from c in data.tbProdutoEstoques
                                 where c.idPedidoEnvio == idPedidoEnvio
                                 select new
                                 {
                                     c.produtoId,
                                     c.tbProduto.produtoPeso,
                                     c.tbProduto.largura,
                                     c.tbProduto.altura,
                                     c.tbProduto.profundidade
                                 }).ToList();
            produtosEnvio = (from c in produtosLista select c.produtoId).ToList();


            produtosPacotes = (from c in produtosLista
                               select new rnFrete2.Pacotes
                               {
                                   altura = c.altura ?? 0,
                                   comprimento = c.profundidade ?? 0,
                                   largura = c.largura ?? 0,
                                   peso = Convert.ToInt32(c.produtoPeso)
                               }).ToList();
        }

        var produtosPrecos =
            (from c in data.tbProdutos
             where produtosEnvio.Contains(c.produtoId)
             select new { preco = (c.produtoPrecoPromocional ?? 0) > c.produtoPreco ? c.produtoPrecoPromocional : c.produtoPreco, c.produtoPrecoDeCusto, c.altura, c.largura, c.profundidade });
        decimal valorTotalDoPedido = 0;
        decimal valorTotalDoCustoDoPedido = 0;


        var hoje = DateTime.Now.Date;
        var cepLong = Convert.ToInt64(pedido.endCep.Replace("-", ""));
        var frete = rnFrete2.Instance;
        if (produtosPrecos.Any())
        {
            valorTotalDoPedido = produtosPrecos.Sum(x => (decimal)x.preco);
            valorTotalDoCustoDoPedido = produtosPrecos.Sum(x => (decimal)x.produtoPrecoDeCusto);
        }
        else
        {
            valorTotalDoPedido = (decimal)pedido.valorCobrado;
            valorTotalDoCustoDoPedido = (decimal)pedido.valorCobrado / 2;
        }
        int pesoTotal = volumes.Sum(x => x.peso);
        var pacotes = new List<rnFrete2.Pacotes>();
        pacotes = (from c in volumes
                   select new rnFrete2.Pacotes
                   {
                       altura = Convert.ToInt32(c.altura),
                       comprimento = Convert.ToInt32(c.profundidade),
                       largura = Convert.ToInt32(c.largura),
                       peso = c.peso
                   }).ToList();
        long cep = Convert.ToInt64(pedido.endCep.Replace("-", "").Replace(".", "").Replace(" ", "").Trim());


        var valoresTransporte = (from c in data.tbPedidoEnvioValorTransportes where c.idPedidoEnvio == idPedidoEnvio select c).ToList();
        var tabelas = (from c in data.tbTipoDeEntregas where (c.habilitarSimulacao ?? false) == true select c).ToList();
        foreach (var tabela in tabelas)
        {
            if (!valoresTransporte.Any(x => x.tipoDeEntregaId == tabela.tipoDeEntregaId))
            {
                var valoresFrete = frete.CalculaFrete(tabela.tipoDeEntregaId, pedido.endCep.Replace("-", "").Trim(), valorTotalDoPedido, pacotes);
                var valorFrete = new tbPedidoEnvioValorTransporte();
                valorFrete.idPedidoEnvio = idPedidoEnvio;
                valorFrete.prazo = valoresFrete.prazo;
                valorFrete.servico = tabela.idTransportadoraString;
                valorFrete.valor = Convert.ToDecimal(valoresFrete.valor.ToString("0.00"));
                valorFrete.tipoDeEntregaId = tabela.tipoDeEntregaId;
                data.tbPedidoEnvioValorTransportes.InsertOnSubmit(valorFrete);
                data.SubmitChanges();
            }
        }
    }
}