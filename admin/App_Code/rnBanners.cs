﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for rnBanners
/// </summary>
public class rnBanners
{
	public rnBanners()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public static DataSet bannerAdminSeleciona(string categoriaId)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("produtoAdminSeleciona");

        db.AddInParameter(dbCommand, "categoriaId", DbType.String, categoriaId);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }
}