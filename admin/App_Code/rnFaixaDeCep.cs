﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Web.Configuration;
using System.Collections;

/// <summary>
/// Summary description for rnFaixaDeCep
/// </summary>
public class rnFaixaDeCep
{
    public static DataSet faixaDeCepSeleciona_PorTipoDeEntregaId_FaixaInicial_FaixaFinal(int tipodeEntregaId, int faixaInicial, int faixaFinal)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("faixaDeCepSeleciona_PorTipoDeEntregaId_FaixaInicial_FaixaFinal");

        db.AddInParameter(dbCommand, "tipodeEntregaId", DbType.Int32, tipodeEntregaId);
        db.AddInParameter(dbCommand, "faixaInicial", DbType.Int32, faixaInicial);
        db.AddInParameter(dbCommand, "faixaFinal", DbType.Int32, faixaFinal);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet faixaDeCep_PorFaixaDeCepId(int faixaDeCepId)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("faixaDeCep_PorFaixaDeCepId");

        db.AddInParameter(dbCommand, "faixaDeCepId", DbType.Int32, faixaDeCepId);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet faixaDeCepSeleciona_PorFaiaxaDeCepIdDiferenteTipoDeEntregaId_FaixaInicial_FaixaFinal(int faixaDeCepId, int tipodeEntregaId, int faixaInicial, int faixaFinal)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("faixaDeCepSeleciona_PorFaiaxaDeCepIdDiferenteTipoDeEntregaId_FaixaInicial_FaixaFinal");

        db.AddInParameter(dbCommand, "faixaDeCepId", DbType.Int32, faixaDeCepId);
        db.AddInParameter(dbCommand, "tipodeEntregaId", DbType.Int32, tipodeEntregaId);
        db.AddInParameter(dbCommand, "faixaInicial", DbType.Int32, faixaInicial);
        db.AddInParameter(dbCommand, "faixaFinal", DbType.Int32, faixaFinal);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet faixaDeCepSeleciona_PorFaixaDeCepPesoTotalFaixaDeCepId(int faixaDeCep, decimal pesoTotal, int faixaDeCepId)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("faixaDeCepSeleciona_PorFaixaDeCepPesoTotalFaixaDeCepId");

        db.AddInParameter(dbCommand, "faixaDeCep", DbType.Int64, faixaDeCep);
        db.AddInParameter(dbCommand, "pesoTotal", DbType.Decimal, pesoTotal);
        db.AddInParameter(dbCommand, "faixaDeCepId", DbType.Int32, faixaDeCepId);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }
}
