﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Web.Configuration;


/// <summary>
/// Summary description for rnTipoDeEntrega
/// </summary>
public class rnTipoDeEntrega
{
    public static DataSet tipoDeEntregaSeleciona_PorTipoDeEntregaNome(string tipoDeEntregaNome)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("tipoDeEntregaSeleciona_PorTipoDeEntregaNome");

        db.AddInParameter(dbCommand, "tipoDeEntregaNome", DbType.String, tipoDeEntregaNome);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet tipoDeEntregaSeleciona_PorTipoDeEntregaId(int tipoDeEntregaId)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("tipoDeEntregaSeleciona_PorTipoDeEntregaId");

        db.AddInParameter(dbCommand, "tipoDeEntregaId", DbType.Int32, tipoDeEntregaId);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet tipoDeEntregaSeleciona()
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("tipoDeEntregaSeleciona");

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }
}
