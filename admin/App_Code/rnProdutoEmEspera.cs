﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Web.Configuration;

/// <summary>
/// Summary description for rnProdutoEmEspera
/// </summary>
public class rnProdutoEmEspera
{
    public static DataSet produtoSelecionaEmEspera()
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("produtoSelecionaEmEspera");

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static bool selecionaClientesEsperandoProdutos_PorProdutoId(int produtoId)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("selecionaClientesEsperandoProdutos_PorProdutoId");

        db.AddInParameter(dbCommand, "produtoId", DbType.Int32, produtoId);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return rnEmails.enviaAvisoReposicaoDeEstoque(ds.Tables[0]);
    }

    public static bool produtoEmEsperaExclui(int produtoId)
    {
        bool result = false;

        Database db = DatabaseFactory.CreateDatabase();

        DbCommand dbCommand = db.GetStoredProcCommand("produtoEmEsperaExclui");

        db.AddInParameter(dbCommand, "produtoId", DbType.Int32, produtoId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
            DbTransaction transaction = connection.BeginTransaction();

            try
            {
                db.ExecuteNonQuery(dbCommand, transaction);

                transaction.Commit();

                result = true;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
                
            }
            connection.Close();

            return result;
        }
    }
}
