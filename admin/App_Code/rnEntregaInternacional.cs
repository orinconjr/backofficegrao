﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Web.Configuration;
using System.Collections;

/// <summary>
/// Summary description for rnEntregaInternacional
/// </summary>
public class rnEntregaInternacional
{
    public static DataSet paisesPrecoDeEntregaSeleciona_PorPaisId(int paisId)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("paisesPrecoDeEntregaSeleciona_PorPaisId");

        db.AddInParameter(dbCommand, "paisId", DbType.Int32, paisId);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet faixaDeCepInternacionalSeleciona_PorFaixaDeCepPesoTotalPaisId(decimal pesoTotal, int paisId)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("faixaDeCepInternacionalSeleciona_PorFaixaDeCepPesoTotalPaisId");

        db.AddInParameter(dbCommand, "pesoTotal", DbType.Decimal, pesoTotal);
        db.AddInParameter(dbCommand, "paisId", DbType.Int32, paisId);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet paisesPrecoDeEntregaSeleciona_PorPaisIdPesoInicialPesoFinal(int paisId, decimal pesoInicial, decimal pesoFinal)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("paisesPrecoDeEntregaSeleciona_PorPaisIdPesoInicialPesoFinal");

        db.AddInParameter(dbCommand, "paisId", DbType.Int32, paisId);
        db.AddInParameter(dbCommand, "pesoInicial", DbType.Decimal, pesoInicial);
        db.AddInParameter(dbCommand, "pesoFinal", DbType.Decimal, pesoFinal);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static bool faixaDeCepInternacionalPesoPrecoInclui(int paisId, decimal faixaDeCepPreco, decimal pesoInicial, decimal pesoFinal, string tipoDeEntregaNome)
    {
        bool result = false;

        Database db = DatabaseFactory.CreateDatabase();

        DbCommand dbCommand = db.GetStoredProcCommand("faixaDeCepInternacionalPesoPrecoInclui");

        db.AddInParameter(dbCommand, "paisId", DbType.Int32, paisId);
        db.AddInParameter(dbCommand, "faixaDeCepPreco", DbType.Decimal, faixaDeCepPreco);
        db.AddInParameter(dbCommand, "pesoInicial", DbType.Decimal, pesoInicial);
        db.AddInParameter(dbCommand, "pesoFinal", DbType.Decimal, pesoFinal);
        db.AddInParameter(dbCommand, "tipoDeEntregaNome", DbType.String, tipoDeEntregaNome);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
            DbTransaction transaction = connection.BeginTransaction();

            try
            {
                db.ExecuteNonQuery(dbCommand, transaction);

                transaction.Commit();

                result = true;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
                
            }
            connection.Close();

            return result;
        }
    }

    public static bool entregaInternacionalAlterar(int paisId, decimal faixaDeCepPreco, decimal pesoInicial, decimal pesoFinal, string tipoDeEntregaNome, int paisEntregaId)
    {
        bool result = false;

        Database db = DatabaseFactory.CreateDatabase();

        DbCommand dbCommand = db.GetStoredProcCommand("entregaInternacionalAlterar");

        db.AddInParameter(dbCommand, "paisId", DbType.Int32, paisId);
        db.AddInParameter(dbCommand, "faixaDeCepPreco", DbType.Decimal, faixaDeCepPreco);
        db.AddInParameter(dbCommand, "pesoInicial", DbType.Decimal, pesoInicial);
        db.AddInParameter(dbCommand, "pesoFinal", DbType.Decimal, pesoFinal);
        db.AddInParameter(dbCommand, "tipoDeEntregaNome", DbType.String, tipoDeEntregaNome);
        db.AddInParameter(dbCommand, "paisEntregaId", DbType.Int32, paisEntregaId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
            DbTransaction transaction = connection.BeginTransaction();

            try
            {
                db.ExecuteNonQuery(dbCommand, transaction);

                transaction.Commit();

                result = true;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
                
            }
            connection.Close();

            return result;
        }
    }

    public static bool entregaInternacionalExcluir(int paisEntregaId)
    {
        bool result = false;

        Database db = DatabaseFactory.CreateDatabase();

        DbCommand dbCommand = db.GetStoredProcCommand("entregaInternacionalExcluir");

        db.AddInParameter(dbCommand, "paisEntregaId", DbType.Int32, paisEntregaId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
            DbTransaction transaction = connection.BeginTransaction();

            try
            {
                db.ExecuteNonQuery(dbCommand, transaction);

                transaction.Commit();

                result = true;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
                
            }
            connection.Close();

            return result;
        }
    }
}
