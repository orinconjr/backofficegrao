﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AmazingCloudSearch;
using AmazingCloudSearch.Contract;
using Newtonsoft.Json;
using rakutenPedidos;

/// <summary>
/// Summary description for rnBuscaCloudSearch
/// </summary>
public class rnBuscaCloudSearch
{
    private static string apiKey = "graodegente-gktkrqswsa2wfxve4reqjoegfq.sa-east-1.cloudsearch.amazonaws.com";
    private static string apiVersion = "2013-01-01";

    public class produtoBusca : CloudSearchDocument
    {
        public string categorianome { get; set; }

        public string categoriaurl { get; set; }

        public List<int> colecaoids { get; set; }

        public List<string> colecaonomes { get; set; }

        public int exibirdiferencacombo { get; set; }

        public List<int> filtroids { get; set; }

        public List<string> filtronomes { get; set; }

        public string fotodestaque { get; set; }

        public double preco { get; set; }

        public int produtobrindes { get; set; }

        public int produtoexclusivo { get; set; }

        public int produtoid { get; set; }

        public int produtolancamento { get; set; }

        public string produtonome { get; set; }

        public int produtopecas { get; set; }

        public double produtopreco { get; set; }

        public double produtoprecopromocional { get; set; }

        public int produtopromocao { get; set; }

        public string produtourl { get; set; }

        public double valordiferencacombo { get; set; }

        public int relevancia { get; set; }

        public string categoriatags { get; set; }

        public string produtotags { get; set; }

        public int produtoativo { get; set; }

        public int produtoprincipal { get; set; }

        public int categoriaid { get; set; }

        public int parcelamentomaximo { get; set; }

        public double parcelamentomaximovalorparcela { get; set; }
        public string primeirapalavra { get; set; }
        public int estoquereal { get; set; }
        public int relevanciafornecedor { get; set; }
        public int relevanciamanual { get; set; }
    }

    public static AmazingCloudSearch.Contract.Result.AddResult AtualizarTodos(int inicio, int quantidade)
    {
        var cloudSearch = new CloudSearch<rnBuscaCloudSearch.produtoBusca>(apiKey, apiVersion);
        var listaProdutos = new List<rnBuscaCloudSearch.produtoBusca>();

        var data = new dbCommerceDataContext();
        var produtos = (from c in data.tbProdutos select c).ToList();
        int produtoInicial = produtos.OrderBy(x => x.produtoId).First().produtoId;
        int produtoFinal = produtos.OrderByDescending(x => x.produtoId).First().produtoId;
        if (quantidade > 0) produtos = produtos.Skip(inicio).Take(quantidade).ToList();

        //produtoInicial = 26081;
        //produtoFinal = 26081;

        for (int indprod = produtoFinal; indprod >= produtoInicial; indprod--)
        {
            try
            {
                var produto = (from c in data.tbProdutos where c.produtoId == indprod select c).FirstOrDefault();
                if (produto != null)
                {
                    if (produto.produtoId == 23810)
                    {
                        string bp = "";
                    }
                    var produtoAdd = new rnBuscaCloudSearch.produtoBusca();
                    var categorias =
                        (from c in data.tbJuncaoProdutoCategorias where c.produtoId == produto.produtoId select c)
                            .ToList();
                    var categoriaRaiz =
                        categorias.FirstOrDefault(
                            x => x.tbProdutoCategoria.categoriaPaiId == 0 && x.tbProdutoCategoria.exibirSite == true);
                    if (categoriaRaiz != null)
                    {
                        string termoBusca = produto.produtoNome;
                        var valorExibicao = produto.produtoPrecoPromocional > 0 &&
                                            produto.produtoPrecoPromocional < produto.produtoPreco
                            ? (decimal) produto.produtoPrecoPromocional
                            : produto.produtoPreco;
                        string adicionar = "";
                        int relevancia = produto.relevancia ?? 0;
                        string primeirapalavra = "";
                        if (produto.produtoNome.Contains(" "))
                        {
                            primeirapalavra = produto.produtoNome.Split(' ')[0];
                        }
                        else
                        {
                            primeirapalavra = rnFuncoes.removeAcentos(produto.produtoNome).ToLower();
                        }
                        produtoAdd.primeirapalavra = primeirapalavra;
                        produtoAdd.Id = produto.produtoId.ToString();
                        produtoAdd.produtoid = produto.produtoId;
                        produtoAdd.fotodestaque = produto.fotoDestaque == null ? "" : produto.fotoDestaque;
                        produtoAdd.produtoexclusivo = ((produto.produtoExclusivo ?? "").ToLower() == "true" ? 1 : 0);
                        produtoAdd.produtolancamento = ((produto.produtoLancamento ?? "").ToLower() == "true" ? 1 : 0);
                        produtoAdd.produtopromocao = ((produto.produtoPromocao ?? "").ToLower() == "true" ? 1 : 0);
                        int brindes = 0;
                        int.TryParse(produto.produtoBrindes, out brindes);
                        produtoAdd.produtobrindes = brindes;

                        int pecas = 0;
                        int.TryParse(produto.produtoPecas, out pecas);
                        produtoAdd.produtopecas = pecas;


                        produtoAdd.relevancia = relevancia;
                        produtoAdd.produtonome = produto.produtoNome;
                        produtoAdd.produtourl = produto.produtoUrl.Replace("\\", "-");
                        produtoAdd.produtotags = (produto.produtoTags ?? "").Replace(",", " ");
                        produtoAdd.categorianome = categoriaRaiz.tbProdutoCategoria.categoriaNomeExibicao.Replace("\\",
                            "-");
                        produtoAdd.categoriaurl = categoriaRaiz.tbProdutoCategoria.categoriaUrl.Replace("\\", "-");
                        produtoAdd.categoriatags = (categoriaRaiz.tbProdutoCategoria.categoriaTags ?? "").Replace(",",
                            " ");
                        produtoAdd.preco = Convert.ToDouble(valorExibicao);
                        produtoAdd.produtopreco = Convert.ToDouble(produto.produtoPreco);
                        produtoAdd.produtoprecopromocional = Convert.ToDouble(produto.produtoPrecoPromocional);
                        produtoAdd.exibirdiferencacombo = (produto.exibirDiferencaCombo ?? "").ToLower() == "true"
                            ? 1
                            : 0;
                        produtoAdd.valordiferencacombo = Convert.ToDouble(produto.valorDiferencaCombo);
                        produtoAdd.produtoativo = ((produto.produtoAtivo ?? "").ToLower() == "true" ? 1 : 0);
                        produtoAdd.produtoprincipal = ((produto.produtoPrincipal ?? "").ToLower() == "true" ? 1 : 0);
                        produtoAdd.categoriaid = categoriaRaiz.categoriaId;
                        produtoAdd.parcelamentomaximo = produto.parcelamentoMaximo;
                        produtoAdd.parcelamentomaximovalorparcela =
                            Convert.ToDouble(produto.parcelamentoMaximoValorParcela);

                        var listaCategoriaNomes = new List<string>();
                        var listaCategoriaIds = new List<int>();

                        int categoriasCount = categorias.Count();
                        for (int i = 0; i < categoriasCount; i++)
                        {
                            listaCategoriaNomes.Add(categorias[i].tbProdutoCategoria.categoriaNomeExibicao);
                            listaCategoriaIds.Add(categorias[i].tbProdutoCategoria.categoriaId);
                            termoBusca += " " + categorias[i].tbProdutoCategoria.categoriaNomeExibicao;
                        }
                        produtoAdd.filtroids = listaCategoriaIds;
                        produtoAdd.filtronomes = listaCategoriaNomes;

                        var listaColecaoNomes = new List<string>();
                        var listaColecaoIds = new List<int>();
                        var colecoes =
                            (from c in data.tbJuncaoProdutoColecaos where c.produtoId == produto.produtoId select c)
                                .ToList();
                        int colecoesCount = colecoes.Count();
                        if (colecoesCount > 0)
                        {
                            for (int i = 0; i < colecoesCount; i++)
                            {
                                termoBusca += " " + colecoes[i].tbColecao.colecaoNome;
                                listaColecaoNomes.Add(colecoes[i].tbColecao.colecaoNome);
                                listaColecaoIds.Add(colecoes[i].tbColecao.colecaoId);
                            }
                        }
                        produtoAdd.colecaoids = listaColecaoIds;
                        produtoAdd.colecaonomes = listaColecaoNomes;
                        //produtoAdd.termoBusca = termoBusca;
                        produtoAdd.estoquereal = (produto.estoqueReal ?? 0);
                        produtoAdd.relevanciamanual = produto.relevanciaManual;
                        produtoAdd.relevanciafornecedor = produto.tbProdutoFornecedor.relevanciaProdutos;

                        if (string.IsNullOrEmpty(produtoAdd.fotodestaque)) produtoAdd.fotodestaque = "semfoto";
                        listaProdutos.Add(produtoAdd);
                    }
                    else
                    {
                        HttpContext.Current.Response.Write("Sem categoria: " + produto.produtoId + "<br>");
                    }
                }
                else
                {
                    ExcluirProduto(indprod);
                }
            }
            catch (Exception ex)
            {
                //HttpContext.Current.Response.Write(produto.produtoId + "<br>");
            }
        }

        return cloudSearch.Add(listaProdutos);
    }
    public static AmazingCloudSearch.Contract.Result.AddResult AtualizarProduto(int produtoId)
    {
        return new AmazingCloudSearch.Contract.Result.AddResult();
        var cloudSearch = new CloudSearch<rnBuscaCloudSearch.produtoBusca>(apiKey, apiVersion);

        var data = new dbCommerceDataContext();
        var produto = (from c in data.tbProdutos select c).Where(x => x.produtoId == produtoId).FirstOrDefault();

        var produtoAdd = new rnBuscaCloudSearch.produtoBusca();
        if (produto != null)
        {
            try
            {

                var categorias = (from c in data.tbJuncaoProdutoCategorias where c.produtoId == produto.produtoId select c).ToList();
                var categoriaRaiz = categorias.FirstOrDefault(x => x.tbProdutoCategoria.categoriaPaiId == 0 && x.tbProdutoCategoria.exibirSite == true);
                if (categoriaRaiz != null)
                {
                    string termoBusca = produto.produtoNome;
                    var valorExibicao = produto.produtoPrecoPromocional > 0 &&
                                        produto.produtoPrecoPromocional < produto.produtoPreco
                        ? (decimal)produto.produtoPrecoPromocional
                        : produto.produtoPreco;
                    string adicionar = "";
                    int relevancia = produto.relevancia ?? 0;
                    string primeirapalavra = "";
                    if (produto.produtoNome.Contains(" "))
                    {
                        primeirapalavra = produto.produtoNome.Split(' ')[0];
                    }
                    else
                    {
                        primeirapalavra = rnFuncoes.removeAcentos(produto.produtoNome).ToLower();
                    }
                    produtoAdd.primeirapalavra = primeirapalavra;
                    produtoAdd.Id = produto.produtoId.ToString();
                    produtoAdd.produtoid = produto.produtoId;
                    produtoAdd.fotodestaque = produto.fotoDestaque == null ? "" : produto.fotoDestaque;
                    produtoAdd.produtoexclusivo = ((produto.produtoExclusivo ?? "").ToLower() == "true" ? 1 : 0);
                    produtoAdd.produtolancamento = ((produto.produtoLancamento ?? "").ToLower() == "true" ? 1 : 0);
                    produtoAdd.produtopromocao = ((produto.produtoPromocao ?? "").ToLower() == "true" ? 1 : 0);
                    int brindes = 0;
                    int.TryParse(produto.produtoBrindes, out brindes);
                    produtoAdd.produtobrindes = brindes;

                    int pecas = 0;
                    int.TryParse(produto.produtoPecas, out pecas);
                    produtoAdd.produtopecas = pecas;


                    produtoAdd.relevancia = relevancia;
                    produtoAdd.produtonome = produto.produtoNome;
                    produtoAdd.produtourl = produto.produtoUrl.Replace("\\", "-");
                    produtoAdd.produtotags = (produto.produtoTags ?? "").Replace(",", " ");
                    produtoAdd.categorianome = categoriaRaiz.tbProdutoCategoria.categoriaNomeExibicao.Replace("\\",
                        "-");
                    produtoAdd.categoriaurl = categoriaRaiz.tbProdutoCategoria.categoriaUrl.Replace("\\", "-");
                    produtoAdd.categoriatags = (categoriaRaiz.tbProdutoCategoria.categoriaTags ?? "").Replace(",",
                        " ");
                    produtoAdd.preco = Convert.ToDouble(valorExibicao);
                    produtoAdd.produtopreco = Convert.ToDouble(produto.produtoPreco);
                    produtoAdd.produtoprecopromocional = Convert.ToDouble(produto.produtoPrecoPromocional);
                    produtoAdd.exibirdiferencacombo = (produto.exibirDiferencaCombo ?? "").ToLower() == "true"
                        ? 1
                        : 0;
                    produtoAdd.valordiferencacombo = Convert.ToDouble(produto.valorDiferencaCombo);
                    produtoAdd.produtoativo = ((produto.produtoAtivo ?? "").ToLower() == "true" ? 1 : 0);
                    produtoAdd.produtoprincipal = ((produto.produtoPrincipal ?? "").ToLower() == "true" ? 1 : 0);
                    produtoAdd.categoriaid = categoriaRaiz.categoriaId;
                    produtoAdd.parcelamentomaximo = produto.parcelamentoMaximo;
                    produtoAdd.parcelamentomaximovalorparcela =
                        Convert.ToDouble(produto.parcelamentoMaximoValorParcela);

                    var listaCategoriaNomes = new List<string>();
                    var listaCategoriaIds = new List<int>();

                    int categoriasCount = categorias.Count();
                    for (int i = 0; i < categoriasCount; i++)
                    {
                        listaCategoriaNomes.Add(categorias[i].tbProdutoCategoria.categoriaNomeExibicao);
                        listaCategoriaIds.Add(categorias[i].tbProdutoCategoria.categoriaId);
                        termoBusca += " " + categorias[i].tbProdutoCategoria.categoriaNomeExibicao;
                    }
                    produtoAdd.filtroids = listaCategoriaIds;
                    produtoAdd.filtronomes = listaCategoriaNomes;

                    var listaColecaoNomes = new List<string>();
                    var listaColecaoIds = new List<int>();
                    var colecoes =
                        (from c in data.tbJuncaoProdutoColecaos where c.produtoId == produto.produtoId select c)
                            .ToList();
                    int colecoesCount = colecoes.Count();
                    if (colecoesCount > 0)
                    {
                        for (int i = 0; i < colecoesCount; i++)
                        {
                            termoBusca += " " + colecoes[i].tbColecao.colecaoNome;
                            listaColecaoNomes.Add(colecoes[i].tbColecao.colecaoNome);
                            listaColecaoIds.Add(colecoes[i].tbColecao.colecaoId);
                        }
                    }
                    produtoAdd.colecaoids = listaColecaoIds;
                    produtoAdd.colecaonomes = listaColecaoNomes;
                    //produtoAdd.termoBusca = termoBusca;
                    produtoAdd.estoquereal = (produto.estoqueReal ?? 0);
                    produtoAdd.relevanciamanual = produto.relevanciaManual;
                    produtoAdd.relevanciafornecedor = produto.tbProdutoFornecedor.relevanciaProdutos;

                    if (string.IsNullOrEmpty(produtoAdd.fotodestaque)) produtoAdd.fotodestaque = "semfoto";

                }
                else
                {
                    HttpContext.Current.Response.Write("Sem categoria: " + produto.produtoId + "<br>");
                }

            }
            catch (Exception ex)
            {
                HttpContext.Current.Response.Write(produto.produtoId + "<br>");
            }

            var combos = (from c in data.tbProdutoRelacionados where c.idProdutoFilho == produtoId && c.idProdutoPai != produtoId select c.idProdutoPai).Distinct().ToList();
            foreach (var combo in combos)
            {
                AtualizarProduto(combo);
            }
        }
        //var update = cloudSearch.Update(produtoAdd);
        return cloudSearch.Add(produtoAdd);
    }
    public static AmazingCloudSearch.Contract.Result.DeleteResult ExcluirProduto(int produtoId)
    {

        var data = new dbCommerceDataContext();
        var cloudSearch = new CloudSearch<rnBuscaCloudSearch.produtoBusca>(apiKey, apiVersion);
        var produtoAdd = new rnBuscaCloudSearch.produtoBusca();
        produtoAdd.Id = produtoId.ToString();
        return cloudSearch.Delete(produtoAdd);
        return null;
    }
    public static AmazingCloudSearch.Contract.Result.DeleteResult RemoveProduto(int produtoId)
    {
        var cloudSearch = new CloudSearch<rnBuscaCloudSearch.produtoBusca>(apiKey, apiVersion);

        var produtoRemove = new rnBuscaCloudSearch.produtoBusca();
        produtoRemove.Id = produtoId.ToString();

        return cloudSearch.Delete(produtoRemove);
    }


}