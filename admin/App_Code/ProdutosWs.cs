﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

/// <summary>
/// Summary description for ProdutosWs
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class ProdutosWs : System.Web.Services.WebService
{

    public class DetalhesProdutoWs
    {
        public int produtoid { get; set; }
        public string produtoNome { get; set; }
        public string produtoIdDaEmpresa { get; set; }
        public string complementoIdDaEmpresa { get; set; }
        public string produtoFoto { get; set; }
        public string fornecedorNome { get; set; }
    }
    public class DetalhesEndereco
    {
        public int idEnderecamentoAndar { get; set; }
        public string area { get; set; }
        public string rua { get; set; }
        public string lado { get; set; }
        public string predio { get; set; }
        public string andar { get; set; }
        public int idCentroDistribuicao { get; set; }
    }
    public class RetornoCarrinhoEndereco
    {
        public int idEnderecamentoTransferencia { get; set; }
        public string enderecamentoTransferencia { get; set; }
        public int idCentroDistribuicao { get; set; }
    }
    public class RetornoEnderecoPacote
    {
        public int idEnderecoPacote { get; set; }
        public string enderecoPacote { get; set; }
    }
    public class RetornoSenhasEmbalagem
    {
        public int idUsuarioExpedicao { get; set; }
        public string nome { get; set; }
        public string senha { get; set; }
        public int idCentroDistribuicao { get; set; }
    }
    public ProdutosWs()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public DetalhesProdutoWs RetornaProduto(int produtoId, string chave)
    {
        if (chave != "glmp152029")
        {
            return null;
        }

        var data = new dbCommerceDataContext();
        var produto = (from c in data.tbProdutos
                       where c.produtoId == produtoId
                       select new
                       {
                           c.produtoId,
                           c.produtoNome,
                           c.produtoIdDaEmpresa,
                           c.fotoDestaque,
                           c.produtoFornecedor,
                           c.tbProdutoFornecedor.fornecedorNome
                       }).FirstOrDefault();
        if (produto != null)
        {
            var retorno = new DetalhesProdutoWs();
            retorno.produtoid = produto.produtoId;
            retorno.produtoNome = produto.produtoNome;
            retorno.produtoIdDaEmpresa = produto.produtoIdDaEmpresa;
            retorno.produtoFoto = produto.fotoDestaque;
            retorno.fornecedorNome = produto.fornecedorNome;
            return retorno;
        }

        return null;
    }

    [WebMethod]
    public List<DetalhesEndereco> RetornarListaEnderecos(string chave)
    {
        if (chave != "glmp152029")
        {
            return null;
        }

        var data = new dbCommerceDataContext();
        var enderecos = (from c in data.tbEnderecamentoAndars
                         select new DetalhesEndereco()
                         {
                             idEnderecamentoAndar = c.idEnderecamentoAndar,
                             andar = c.andar,
                             area = c.tbEnderecamentoPredio.tbEnderecamentoRua.tbEnderecamentoArea.area,
                             lado = c.tbEnderecamentoPredio.tbEnderecamentoLado.lado,
                             predio = c.tbEnderecamentoPredio.predio,
                             rua = c.tbEnderecamentoPredio.tbEnderecamentoRua.rua,
                             idCentroDistribuicao = c.tbEnderecamentoPredio.tbEnderecamentoRua.tbEnderecamentoArea.idCentroDistribuicao ?? 1
                         }).ToList();
        return enderecos;
    }

    [WebMethod]
    public List<tbEnderecoSeparacao> RetornarListaEstanteSeparacao(string chave)
    {
        if (chave != "glmp152029")
        {
            return null;
        }

        var data = new dbCommerceDataContext();
        var lista = (from c in data.tbEnderecoSeparacaos select c).ToList();

        var enderecos =
            (from c in lista select c).Select(
                x =>
                    new tbEnderecoSeparacao
                    {
                        idEnderecamentoSeparacao = x.idEnderecamentoSeparacao,
                        enderecoSeparacao = x.enderecoSeparacao,
                        idCentroDistribuicao = x.idCentroDistribuicao
                    }).ToList();

        return enderecos;
    }

    [WebMethod]
    public List<RetornoSenhasEmbalagem> RetornarListaSenhasEmbalagem(string chave)
    {
        if (chave != "glmp152029")
        {
            return null;
        }

        var data = new dbCommerceDataContext();
        var enderecos = (from c in data.tbUsuarioExpedicaos
                         where c.senha != "senhadesativada"
                         select new RetornoSenhasEmbalagem
                         {
                             idUsuarioExpedicao = c.idUsuarioExpedicao,
                             nome = c.nome,
                             senha = c.senha,
                             idCentroDistribuicao = c.idCentroDistribuicao
                         }).ToList();
        return enderecos;
    }

    [WebMethod]
    public List<RetornoCarrinhoEndereco> RetornarListaCarrinhosEnderecamento(string chave)
    {
        if (chave != "glmp152029")
        {
            return null;
        }

        var data = new dbCommerceDataContext();
        var enderecos = (from c in data.tbEnderecamentoTransferencias
                         select new RetornoCarrinhoEndereco
                         {
                             idEnderecamentoTransferencia = c.idEnderecamentoTransferencia,
                             enderecamentoTransferencia = c.enderecamentoTransferencia,
                             idCentroDistribuicao = c.idCentroDistribuicao ?? 1
                         }).ToList();
        return enderecos;
    }


    [WebMethod]
    public List<RetornoEnderecoPacote> RetornarListaEnderecoPacote(string chave)
    {
        if (chave != "glmp152029")
        {
            return null;
        }

        var data = new dbCommerceDataContext();
        var enderecos = (from c in data.tbEnderecoPacotes where c.idEnderecoPacote > 8
                         select new RetornoEnderecoPacote
                         {
                             idEnderecoPacote = c.idEnderecoPacote,
                             enderecoPacote = c.enderecoPacote
                         }).ToList();
        return enderecos;
    }

    [WebMethod]
    public string AdicionarProdutoEnviadoAoEstoque(string chave, int idPedidoFornecedorItem)
    {
        if (chave != "glmp152029")
        {
            return "";
        }

        var data = new dbCommerceDataContext();
        var itemEnviado = (from c in data.tbProdutoEstoques
                           where c.idPedidoFornecedorItem == idPedidoFornecedorItem && c.enviado == true
                           select c).FirstOrDefault();
        if (itemEnviado == null) return "";


        var produtoJaAdicionado = (from c in data.tbProdutoEstoques
                                   where c.enviado == false && c.tbPedidoFornecedorItem.idProduto == itemEnviado.produtoId &&
                                         c.tbPedidoFornecedorItem.motivo == "item enviado " + itemEnviado.idPedidoFornecedorItem
                                   select c).Any();
        if (produtoJaAdicionado) return "";

        string notaEtiqueta = "";

        int fornecedorId = 71;
        var fornecedorDc = new dbCommerceDataContext();
        var fornecedor = (from c in fornecedorDc.tbProdutoFornecedors where c.fornecedorId == fornecedorId select c).First();
        var pedidoFornecedor = new tbPedidoFornecedor();
        pedidoFornecedor.data = DateTime.Now;
        pedidoFornecedor.idFornecedor = fornecedorId;
        pedidoFornecedor.confirmado = false;
        pedidoFornecedor.avulso = true;
        pedidoFornecedor.pendente = true;
        pedidoFornecedor.pendente = false;

        if (fornecedor.fornecedorPrazoPedidos > 0)
        {
            int diasPrazo = Convert.ToInt32(fornecedor.fornecedorPrazoPedidos);
            if ((fornecedor.prazoFornecedorDiasUteis ?? false) == true)
            {
                int diasPrazoFinal = rnFuncoes.retornaPrazoDiasUteis(diasPrazo, 0);
                diasPrazo = diasPrazoFinal;
            }
            pedidoFornecedor.dataLimite = DateTime.Now.AddDays(diasPrazo);
        }
        else
        {
            pedidoFornecedor.dataLimite = DateTime.Now.AddDays(1);
        }
        pedidoFornecedor.usuario = "Sistema";
        data.tbPedidoFornecedors.InsertOnSubmit(pedidoFornecedor);
        data.SubmitChanges();

        int idPedidoFornecedor = pedidoFornecedor.idPedidoFornecedor;


        var produto = (from c in data.tbProdutos where c.produtoId == itemEnviado.produtoId select c).First();

        var pedidoFornecedorItem = new tbPedidoFornecedorItem();
        pedidoFornecedorItem.custo = (decimal)produto.produtoPrecoDeCusto;
        pedidoFornecedorItem.brinde = false;
        pedidoFornecedorItem.diferenca = 0;
        pedidoFornecedorItem.idPedidoFornecedor = idPedidoFornecedor;
        pedidoFornecedorItem.idProduto = produto.produtoId;
        pedidoFornecedorItem.entregue = false;
        pedidoFornecedorItem.quantidade = 1;
        pedidoFornecedorItem.motivo = "item enviado " + itemEnviado.idPedidoFornecedorItem;
        data.tbPedidoFornecedorItems.InsertOnSubmit(pedidoFornecedorItem);
        data.SubmitChanges();

        return pedidoFornecedorItem.idPedidoFornecedorItem.ToString();

    }



    [WebMethod]
    public string AdicionarProdutoEnviadoAoEstoqueUsuario(string chave, int idPedidoFornecedorItem, int idUsuario)
    {
        if (chave != "glmp152029")
        {
            return "";
        }

        
        var data = new dbCommerceDataContext();

        var usuario = (from c in data.tbUsuarios where c.usuarioId == idUsuario select c).First();
        rnLog.InsereLogEtiqueta(usuario.usuarioNome, idPedidoFornecedorItem, "Etiqueta lida como enviada para ser adicionada ao estoque");
        var itemEnviado = (from c in data.tbProdutoEstoques
                           where c.idPedidoFornecedorItem == idPedidoFornecedorItem && c.enviado == true
                           select c).FirstOrDefault();
        if (itemEnviado == null) return "";


        var produtoJaAdicionado = (from c in data.tbProdutoEstoques
                                   where c.enviado == false && c.tbPedidoFornecedorItem.idProduto == itemEnviado.produtoId &&
                                         c.tbPedidoFornecedorItem.motivo == "item enviado " + itemEnviado.idPedidoFornecedorItem
                                   select c).Any();
        
        if (produtoJaAdicionado) return "";

        string notaEtiqueta = "";

        int fornecedorId = 71;
        var fornecedorDc = new dbCommerceDataContext();
        var fornecedor = (from c in fornecedorDc.tbProdutoFornecedors where c.fornecedorId == fornecedorId select c).First();
        var pedidoFornecedor = new tbPedidoFornecedor();
        pedidoFornecedor.data = DateTime.Now;
        pedidoFornecedor.idFornecedor = fornecedorId;
        pedidoFornecedor.confirmado = false;
        pedidoFornecedor.avulso = true;
        pedidoFornecedor.pendente = true;
        pedidoFornecedor.pendente = false;

        if (fornecedor.fornecedorPrazoPedidos > 0)
        {
            int diasPrazo = Convert.ToInt32(fornecedor.fornecedorPrazoPedidos);
            if ((fornecedor.prazoFornecedorDiasUteis ?? false) == true)
            {
                int diasPrazoFinal = rnFuncoes.retornaPrazoDiasUteis(diasPrazo, 0);
                diasPrazo = diasPrazoFinal;
            }
            pedidoFornecedor.dataLimite = DateTime.Now.AddDays(diasPrazo);
        }
        else
        {
            pedidoFornecedor.dataLimite = DateTime.Now.AddDays(1);
        }
        pedidoFornecedor.usuario = usuario.usuarioNome;
        data.tbPedidoFornecedors.InsertOnSubmit(pedidoFornecedor);
        data.SubmitChanges();

        int idPedidoFornecedor = pedidoFornecedor.idPedidoFornecedor;


        var produto = (from c in data.tbProdutos where c.produtoId == itemEnviado.produtoId select c).First();

        var pedidoFornecedorItem = new tbPedidoFornecedorItem();
        pedidoFornecedorItem.custo = (decimal)produto.produtoPrecoDeCusto;
        pedidoFornecedorItem.brinde = false;
        pedidoFornecedorItem.diferenca = 0;
        pedidoFornecedorItem.idPedidoFornecedor = idPedidoFornecedor;
        pedidoFornecedorItem.idProduto = produto.produtoId;
        pedidoFornecedorItem.entregue = false;
        pedidoFornecedorItem.quantidade = 1;
        pedidoFornecedorItem.motivo = "item enviado " + itemEnviado.idPedidoFornecedorItem;
        data.tbPedidoFornecedorItems.InsertOnSubmit(pedidoFornecedorItem);
        data.SubmitChanges();

        rnLog.InsereLogEtiqueta(usuario.usuarioNome, pedidoFornecedorItem.idPedidoFornecedorItem, "Etiqueta adicionada à partir da etiqueta enviada " + idPedidoFornecedorItem);
        return pedidoFornecedorItem.idPedidoFornecedorItem.ToString();

    }
}
