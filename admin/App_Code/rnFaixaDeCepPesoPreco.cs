﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Web.Configuration;
using System.Collections;

/// <summary>
/// Summary description for rnFaixaDeCepPesoPreco
/// </summary>
public class rnFaixaDeCepPesoPreco
{
    public static DataSet faixaDeCepPesoPrecoSeleciona_PorPesoInicialPesoFinal(int faixaDeCepPesoInicial, int faixaDeCepPesoFinal)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("faixaDeCepPesoPrecoSeleciona_PorPesoInicialPesoFinal");

        db.AddInParameter(dbCommand, "faixaDeCepPesoInicial", DbType.Int32, faixaDeCepPesoInicial);
        db.AddInParameter(dbCommand, "faixaDeCepPesoFinal", DbType.Int32, faixaDeCepPesoFinal);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet faixaDeCepPesoPrecoSeleciona_PorFaixaDeCepId(int faixaDeCepId)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("faixaDeCepPesoPrecoSeleciona_PorFaixaDeCepId");

        db.AddInParameter(dbCommand, "faixaDeCepId", DbType.Int32, faixaDeCepId);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static DataSet faixaDeCepPesoSeleciona_PorFaiaxaDeCepIdPesoInicialPesoFinal(int faixaDeCepId, decimal faixaDeCepPesoInicial, decimal faixaDeCepPesoFinal)
    {
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("faixaDeCepPesoSeleciona_PorFaiaxaDeCepIdPesoInicialPesoFinal");

        db.AddInParameter(dbCommand, "faixaDeCepId", DbType.Int32, faixaDeCepId);
        db.AddInParameter(dbCommand, "faixaDeCepPesoInicial", DbType.Decimal, faixaDeCepPesoInicial);
        db.AddInParameter(dbCommand, "faixaDeCepPesoFinal", DbType.Decimal, faixaDeCepPesoFinal);

        DataSet ds = null;
        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }

    public static bool faixaDeCepPesoPrecoInclui(int faixaDeCepId, int faixaDeCepPesoInicial, int faixaDeCepPesoFinal, decimal faixaDeCepPreco)
    {
        bool result = false;

        Database db = DatabaseFactory.CreateDatabase();

        DbCommand dbCommand = db.GetStoredProcCommand("faixaDeCepPesoPrecoInclui");

        db.AddInParameter(dbCommand, "faixaDeCepId", DbType.Int32, faixaDeCepId);
        db.AddInParameter(dbCommand, "faixaDeCepPesoInicial", DbType.Int32, faixaDeCepPesoInicial);
        db.AddInParameter(dbCommand, "faixaDeCepPesoFinal", DbType.Int32, faixaDeCepPesoFinal);
        db.AddInParameter(dbCommand, "faixaDeCepPreco", DbType.Decimal, faixaDeCepPreco);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
            DbTransaction transaction = connection.BeginTransaction();

            try
            {
                db.ExecuteNonQuery(dbCommand, transaction);

                transaction.Commit();

                result = true;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
                
            }
            connection.Close();

            return result;
        }
    }

    public static bool faixaDeCepPesoPrecoAlterar(int faixaDeCepId, int faixaDeCepPesoInicial, int faixaDeCepPesoFinal, decimal faixaDeCepPreco, int faixaDeCepPesoPrecoId)
    {
        bool result = false;

        Database db = DatabaseFactory.CreateDatabase();

        DbCommand dbCommand = db.GetStoredProcCommand("faixaDeCepPesoPrecoAlterar");

        db.AddInParameter(dbCommand, "faixaDeCepId", DbType.Int32, faixaDeCepId);
        db.AddInParameter(dbCommand, "faixaDeCepPesoInicial", DbType.Int32, faixaDeCepPesoInicial);
        db.AddInParameter(dbCommand, "faixaDeCepPesoFinal", DbType.Int32, faixaDeCepPesoFinal);
        db.AddInParameter(dbCommand, "faixaDeCepPreco", DbType.Decimal, faixaDeCepPreco);
        db.AddInParameter(dbCommand, "@faixaDeCepPesoPrecoId", DbType.Int32, faixaDeCepPesoPrecoId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
            DbTransaction transaction = connection.BeginTransaction();

            try
            {
                db.ExecuteNonQuery(dbCommand, transaction);

                transaction.Commit();

                result = true;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
                
            }
            connection.Close();

            return result;
        }
    }

    public static bool faixaDeCepPesoPrecoExclui(int faixaDeCepPesoPrecoId)
    {
        bool result = false;

        Database db = DatabaseFactory.CreateDatabase();

        DbCommand dbCommand = db.GetStoredProcCommand("faixaDeCepPesoPrecoExclui");

        db.AddInParameter(dbCommand, "faixaDeCepPesoPrecoId", DbType.String, faixaDeCepPesoPrecoId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
            DbTransaction transaction = connection.BeginTransaction();

            try
            {
                db.ExecuteNonQuery(dbCommand, transaction);

                transaction.Commit();

                result = true;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
                
            }
            connection.Close();

            return result;
        }
    }
}
