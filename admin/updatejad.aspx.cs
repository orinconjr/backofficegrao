﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SpreadsheetLight;

public partial class updatejad : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        var data = new dbCommerceDataContext();
        using (SLDocument sl = new SLDocument("c:\\tabela\\tabelajad.xlsx", "tabela"))
        {
            SLWorksheetStatistics stats = sl.GetWorksheetStatistics();
            int iStartColumnIndex = stats.StartColumnIndex;
            for (int row = stats.StartRowIndex; row <= 55; ++row)
            {
                string estado = sl.GetCellValueAsString(row, 1);
                string tarifa = sl.GetCellValueAsString(row, 3);
                string nome = estado.ToLower() + " - " + tarifa;
                string precoadicional = sl.GetCellValueAsString(row, 34).Replace(".", ",");
                decimal valorAdicional = Convert.ToDecimal(precoadicional);

                var tabelaPreco =
                    (from c in data.tbTipoDeEntregaTabelaPrecos
                        where c.idTipoDeEntrega == 13 && c.nome.ToLower() == nome
                        select c).FirstOrDefault();
                if (tabelaPreco != null)
                {
                    tabelaPreco.valorKgAdicional = valorAdicional;
                    data.SubmitChanges();
                    for (int i = 1; i <= 30; i++)
                    {
                        int kgInicial = (i - 1)*1000;
                        int kgFinal = i*1000;

                        var tabelaValor = (from c in data.tbTipoDeEntregaTabelaPrecoValors
                            where
                                c.idTipoDeEntregaTabelaPreco == tabelaPreco.idTipoDeEntregaTabelaPreco &&
                                c.pesoInicial == kgInicial && c.pesoFinal == kgFinal
                            select c).FirstOrDefault();
                        if (tabelaValor != null)
                        {
                            var valor = sl.GetCellValueAsString(row, i + 3).Replace(".", ",");
                            decimal valorFormatado = Convert.ToDecimal(valor);
                            tabelaValor.valor = valorFormatado;
                            data.SubmitChanges();
                        }
                        else
                        {
                            Response.Write("Nao encontrado: " + nome + " - " + kgInicial + " - " + kgFinal);
                        }
                    }

                }


            }

        }


        Response.Write("<script>alert('Produtos atualizados com sucesso.');</script>");
    }
}