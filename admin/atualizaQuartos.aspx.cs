﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class atualizaQuartos : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        var data = new dbCommerceDataContext();
        //var produtosQuarto = (from c in data.tbJuncaoProdutoCategorias where c.categoriaId == 412 | c.categoriaId == 679 select c);
        var produtosQuarto = (from c in data.tbProdutos where c.exibirDiferencaCombo.ToLower() == "true" select c);

        foreach (var produto in produtosQuarto)
        {
            //decimal valorTotalCombo = 0;
            //decimal valorDiferencaCombo = 0;
 
            //var relacionados = (from c in data.tbProdutoRelacionados
            //                    join d in data.tbProdutos on c.idProdutoFilho equals d.produtoId
            //                    where c.idProdutoPai == produto.produtoId
            //                    select
            //                        new
            //                        {
            //                            c.tbProduto.produtoPreco
            //                        });
            //if (relacionados.Any()) valorTotalCombo = relacionados.Sum(x => x.produtoPreco);
            //valorDiferencaCombo = valorTotalCombo - (produto.produtoPrecoPromocional > 0
            //    ? (produto.produtoPrecoPromocional ?? 0)
            //    : produto.produtoPreco);
            //var dataAlt = new dbCommerceDataContext();
            //var produtoAlt =
            //    (from c in dataAlt.tbProdutos where c.produtoId == produto.produtoId select c).FirstOrDefault();
            ////produtoAlt.exibirDiferencaCombo = "True";
            //produtoAlt.valorDiferencaCombo = Convert.ToInt32(Math.Floor(valorDiferencaCombo));
            //dataAlt.SubmitChanges();

            rnProdutos.AtualizaDescontoCombo(produto.produtoId);
            rnBuscaCloudSearch.AtualizarProduto(produto.produtoId);
        }

        //var produtosDiferenca =
        //    (from c in data.tbProdutos where c.exibirDiferencaCombo.ToLower() == "true" select c).ToList();
        //foreach (var produtoDiferenca in produtosDiferenca)
        //{
        //    var categoria = (from c in data.tbJuncaoProdutoCategorias where (c.categoriaId == 412 | c.categoriaId == 679) && c.produtoId == produtoDiferenca.produtoId select c).Any();
        //    if (!categoria)
        //    {
        //        var dataAlt = new dbCommerceDataContext();
        //        var produtoAlt =
        //            (from c in dataAlt.tbProdutos where c.produtoId == produtoDiferenca.produtoId select c).FirstOrDefault();
        //        produtoAlt.exibirDiferencaCombo = "False";
        //        dataAlt.SubmitChanges();
        //    }
        //}
        /*var produtos = (from c in data.tbJuncaoProdutoCategorias where c.categoriaId == 413 | c.categoriaId == 414 select c.tbProduto).ToList();
        foreach (var produtoCheck in produtos)
        {
            int produtoId = produtoCheck.produtoId;
            var combos = (from c in data.tbProdutoRelacionados where c.idProdutoFilho == produtoId select c);
            foreach (var combo in combos)
            {
                var produtoPai = (from c in data.tbProdutos where c.produtoId == combo.idProdutoPai select c).FirstOrDefault();
                if (produtoPai != null)
                {
                    if ((produtoPai.exibirDiferencaCombo ?? "").ToLower() == "true")
                    {
                        var produto = (from c in data.tbProdutos where c.produtoId == produtoId select c).FirstOrDefault();
                        produto.pertenceComboDiferenca = true;
                        data.SubmitChanges();
                    }
                }
            }
        }*/

    }
}