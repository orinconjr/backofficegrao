﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class atualizarentregaprecozero : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        var data = new dbCommerceDataContext();
        var pedidos = (from c in data.tbPedidos
                       join d in data.tbPedidoEnvios on c.pedidoId equals d.idPedido
                       join f in data.tbPedidoPacotes on d.idPedidoEnvio equals f.idPedidoEnvio into pacotes
                       where pacotes.Any()
                       select new
                       {
                           c.pedidoId,
                           d.formaDeEnvio,
                           d.idPedidoEnvio,
                           d.nfeNumero,
                           dataSendoEmbalado = d.dataInicioEmbalagem,
                           d.volumesEmbalados,
                           pesoTotal = pacotes.Sum(x => x.peso),
                           d.valorSelecionado,
                           d.valorCobradoTransportadora,
                           rastreio = pacotes.FirstOrDefault() == null ? "" : pacotes.FirstOrDefault().rastreio,
                           c.endCep,
                           c.valorCobrado
                       }).Where(x => x.valorSelecionado > 500).ToList();
        foreach (var pedido in pedidos)
        {
            var pedidoDc = new dbCommerceDataContext();
            var produtosEnvio = (from c in pedidoDc.tbProdutoEstoques where c.idPedidoEnvio == pedido.idPedidoEnvio select c.produtoId).ToList();
            var produtosPrecos =
                (from c in pedidoDc.tbProdutos
                 where produtosEnvio.Contains(c.produtoId)
                 select new { preco = (c.produtoPrecoPromocional ?? 0) > c.produtoPreco ? c.produtoPrecoPromocional : c.produtoPreco });
            decimal valorTotalDoPedido = 0;
            var volumes = (from c in pedidoDc.tbPedidoPacotes where c.idPedidoEnvio == pedido.idPedidoEnvio select c);
            int pesoTotal = volumes.Sum(x => x.peso);
            long cep = Convert.ToInt64(pedido.endCep.Replace("-", "").Trim());
            if (produtosPrecos.Any())
            {
                valorTotalDoPedido = produtosPrecos.Sum(x => (decimal)x.preco);
            }
            else
            {
                valorTotalDoPedido = (decimal)pedido.valorCobrado;
            }
            if (pedido.formaDeEnvio.ToLower() == "jadlog")
            {
                string modalidadeJadlog = "";
                if (pedido.formaDeEnvio == "jadlogexpressa") modalidadeJadlog = "jadlogexpressa";
                var valoresJadlog = rnFrete.CalculaFreteWebserviceJadlog(pesoTotal, cep, modalidadeJadlog, valorTotalDoPedido);
                if (valoresJadlog.valor > 0)
                {
                    var pedidoAlt =
                        (from c in data.tbPedidoEnvios where c.idPedidoEnvio == pedido.idPedidoEnvio select c).First();
                    pedidoAlt.valorSelecionado = valoresJadlog.valor;
                    data.SubmitChanges();
                }
                else
                {
                    var faixasTotal = (from c in data.tbFaixaDeCeps
                        where c.tipodeEntregaId == 11
                        select new
                        {
                            faixa = Convert.ToInt64(c.faixaInicial)
                        }).ToList();
                    var proximaFaixa = (from c in faixasTotal where c.faixa > cep select c).FirstOrDefault();
                    if (proximaFaixa != null)
                    {
                        var valoresJadlog2 = rnFrete.CalculaFreteWebserviceJadlog(pesoTotal, Convert.ToInt64(proximaFaixa.faixa), modalidadeJadlog, valorTotalDoPedido);
                        if (valoresJadlog2.valor > 0)
                        {
                            var pedidoAlt =
                                (from c in data.tbPedidoEnvios where c.idPedidoEnvio == pedido.idPedidoEnvio select c).First();
                            pedidoAlt.valorSelecionado = valoresJadlog2.valor;
                            data.SubmitChanges();
                        }
                    }
                }
            }
        }
        
    }
}
