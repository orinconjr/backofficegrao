﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="pesquisarCobranca.aspx.cs" Inherits="pesquisarCobranca" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div style="margin:25px 0 0 25px;">
            Quatro ultimos digitos do cartão:<br />
            <asp:TextBox ID="txtFinalNumeroCartao" runat="server" MaxLength="4" Width="132"></asp:TextBox>
            <asp:Button ID="btnPesquisar" runat="server" Text="Pesquisar" Height="22" Width="70" OnClick="btnPesquisar_Click" />
        </div>
        <br />
        <div>
            <asp:Literal ID="litDadosCobranca" runat="server"></asp:Literal>
        </div>
    </form>
</body>
</html>
