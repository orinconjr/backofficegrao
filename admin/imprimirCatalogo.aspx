﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="imprimirCatalogo.aspx.cs" Inherits="imprimirCatalogo" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

      <style type="text/css">
        body {
            font-size: 14px;
            font-family: Arial;
          
        }

        .tableComposicao table {
            display: block;
            margin-top: 10px;
        }

            .tableComposicao table tr:nth-child(2n+2) {
                background-color: #f7f7e8;
            }

                .tableComposicao table tr:nth-child(2n+2) .templateDetalheNome {
                    color: #7D8043;
                    font-weight: bold;
                }

            .tableComposicao table td, .tableComposicao table th {
                padding: 4px 2px;
            }

        .modal {
            position: fixed;
            z-index: 999;
            height: 100%;
            width: 100%;
            top: 0;
            background-color: Black;
            filter: alpha(opacity=60);
            opacity: 0.6;
            -moz-opacity: 0.8;
        }

        .center {
            z-index: 1000;
            margin: 300px auto;
            padding: 10px;
            width: 130px;
            background-color: White;
            border-radius: 10px;
            filter: alpha(opacity=100);
            opacity: 1;
            -moz-opacity: 1;
        }

            .center img {
                height: 128px;
                width: 128px;
            }

        .RowStyle {
            height: 260px;
            overflow: hidden;
            border: solid 1px #000;
        }

        td {
            height: 260px;
            overflow: hidden;
            padding: 5px;
        }

        .page-break {
            page-break-before: always;
             border: solid 1px #000;
        }
    </style>

   <%-- <script>
        function imprimirTela() {
            var conteudo = document.getElementById('conteudo').innerHTML,
           tela_impressao = window.open('about:blank');

            tela_impressao.document.write(conteudo);
            tela_impressao.window.print();
            tela_impressao.window.close();
        }


        </script>--%>

    </head>
<body>
    <form id="form1" runat="server">
       
        <div style="width: 980px; margin: auto;">
            <%--<input type="button" onclick="imprimirTela()" value="Imprimir" style="position: fixed; top: 0; left: 0;" />--%>

            <div id="conteudo">

               <asp:GridView ID="GridView1" runat="server" ShowHeader="False" AutoGenerateColumns="false" BorderStyle="None" GridLines="Vertical" OnRowDataBound="GridView1_OnRowDataBound"
                            Font-Names="Arial" AllowPaging="True" PageSize="100" OnPageIndexChanging="GridView1_OnPageIndexChanging" RowStyle-CssClass="RowStyle">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:Image ID="Image1" runat="server" Width="250" Height="250" Style="display: block; overflow: hidden; height: 260px;"
                                            ImageUrl='<%# ConfigurationManager.AppSettings["caminhoCDN"] + "fotos/" + Eval("produtoId") + "/" + Eval("fotoDestaque") + ".jpg" %>' />
                                    </ItemTemplate>
                                    <ItemStyle VerticalAlign="top"></ItemStyle>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <table cellspacing="0" border="0" style="border: None;">
                                            <tr>
                                                <td align="top" style="border-right: solid 1px #000; width: 200px;">
                                                    <%#Eval("produtoNome").ToString() %>
                                                </td>
                                                <td align="top">
                                                    <%#Eval("Referencia")  %><br />
                                                    largura(cm): <%#Eval("largura")  %> - altura(cm): <%#Eval("altura")  %><br />
                                                    <div class="tableComposicao">
                                                        <%#Regex.Replace(Eval("descricao").ToString().Replace("{padding:0;margin:0;}","").Replace("body","").Replace("&nbsp;",""), "<.*?>", String.Empty)  %>
                                                        <%#Regex.Replace(Eval("composicao").ToString().Replace("{padding:0;margin:0;}","").Replace("body","").Replace("&nbsp;",""), "<.*?>", String.Empty)  %>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </ItemTemplate>
                                    <ItemStyle VerticalAlign="top"></ItemStyle>
                                </asp:TemplateField>
                                <%--<asp:BoundField DataField="referencia" HeaderText="Referencia"
                        ItemStyle-Height="150" />--%>
                            </Columns>

                        </asp:GridView>

            </div>

        </div>
    </form>
</body>
</html>
