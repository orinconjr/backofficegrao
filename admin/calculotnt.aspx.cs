﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

public partial class calculotnt : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnCalcular_Click(object sender, EventArgs e)
    {
        long cep = Convert.ToInt64(txtCep.Text.Replace("-", ""));
        int peso = Convert.ToInt32(txtPeso.Text.Replace("-", ""));
        decimal pesoEmKg = Convert.ToDecimal(peso) / 1000;
        string cpfDestinatario = txtCpfDestinatario.Text;

        var serviceTnt = new serviceTntCalculoFrete.CalculoFrete();
        var parametros = new serviceTntCalculoFrete.CotacaoWebService();
        parametros.login = "atendimento@bark.com.br";
        parametros.senha = "";
        parametros.nrIdentifClienteRem = "10924051000163";
        parametros.nrInscricaoEstadualRemetente = "633459940118";
        parametros.nrIdentifClienteDest = cpfDestinatario;
        parametros.tpSituacaoTributariaRemetente = "ME";
        parametros.tpPessoaRemetente = "J";
        parametros.nrInscricaoEstadualDestinatario = "";
        parametros.tpPessoaDestinatario = "F";
        parametros.tpSituacaoTributariaDestinatario = "NC";
        parametros.cepOrigem = "14910000";
        parametros.cepDestino = cep.ToString().Replace("-", "").Replace(" ", "");
        parametros.vlMercadoria = txtValor.Text.Replace(",", ".");
        parametros.psReal = (Convert.ToDecimal(peso) / 1000).ToString("0.000").Replace(",", ".");
        parametros.tpServico = "RNC";
        parametros.tpFrete = "C";
        parametros.cdDivisaoCliente = 1;
        parametros.cdDivisaoClienteSpecified = true;
        int prazo = 0;
        decimal valor = 0;
        try
        {

            var calculoFrete = serviceTnt.calculaFrete(parametros);
            txtValores.Text = "";
            if (calculoFrete.errorList.Length > 0)
            {
                for (int i = 0; i < calculoFrete.errorList.Length; i++)
                {
                    txtValores.Text += "Erro: " + calculoFrete.errorList[i].ToString() + "<br>";
                }

            }
            else
            {
                prazo = Convert.ToInt32(calculoFrete.prazoEntrega);
                valor = Convert.ToDecimal(calculoFrete.vlTotalFrete.Replace(".", ","));
                txtValores.Text = "Prazo: " + prazo + " Valor: " + valor;
            }

        }
        catch { }
    }
}