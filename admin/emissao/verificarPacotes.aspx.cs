﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class emissao_verificarPacotes : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            var embalarWs = new EmbalagemWs();
            var contagem = embalarWs.ContarPedidosCd2(0);
            lblQuantidadePedidoEmbalar.Text = "Qtd. Pedidos Aguardando Embalagem - " + contagem.totalPedidos;

            var firstOrDefault = rnPedidos.verificaPedidosCompletosSeparadosCd1e2().Where(x => x.separado).ToList().FirstOrDefault();
            if (firstOrDefault != null)
            {
                litProximoPedidoEmbalar.Text = firstOrDefault.pedidoId + " (" + rnFuncoes.retornaIdCliente(firstOrDefault.pedidoId) + ")";

                var proximosEmbalar =
                    rnPedidos.verificaPedidosCompletosSeparadosCd1e2()
                        .Where(x => x.separado && x.pedidoId != firstOrDefault.pedidoId)
                        .Take(10).ToList();

                foreach (var proximoEmb in proximosEmbalar)
                {
                    litProximosPedidosEmbalar.Text += rnFuncoes.retornaIdCliente(proximoEmb.pedidoId) + " - ";
                }
            }



            int totalPedidos = rnPedidos.contaPedidosCompletos(false, false);
            if (totalPedidos == 0) return;

            var pedidosCompletos = rnPedidos.verificaIdsPedidosCompletos(false, false);

            int pedidoAtual = 0;
            var pedidoSeparar = pedidosCompletos[pedidoAtual];
            int possuiItens = 0;
            possuiItens = rnPedidos.retornaProdutosEtiqueta(pedidoSeparar).Count;
            while (pedidoAtual < totalPedidos && possuiItens == 0)
            {
                pedidoAtual++;
                pedidoSeparar = pedidosCompletos[pedidoAtual];
                possuiItens = rnPedidos.retornaProdutosEtiqueta(pedidoSeparar).Count;
            }

            litProximoPedidoSeparar.Text = pedidoSeparar + " (" + rnFuncoes.retornaIdCliente(pedidoSeparar) + ")";

            var proximos10 = pedidosCompletos.Where(x => x != pedidoSeparar).Take(10);

            foreach (var n in proximos10)
            {
                litProximos10Separar.Text += rnFuncoes.retornaIdCliente(n) + " - ";
            }

            litQtdPedidosEmbalarCD2.Text = TotalPedidosParaEmbalarCd2();
            litProximoPedidoEmbalarCd2.Text = ProximoPedidoEmbalarCd2();
        }
    }

    private void fillGrid(bool rebind, int pedidoId)
    {

        if (pedidoId.ToString().Length > 6)
        {
            pedidoId = rnFuncoes.retornaIdInterno(pedidoId);
        }

        using (var data = new dbCommerceDataContext())
        {
            var dados = (from c in data.tbPedidoPacotes
                         where c.idPedido == pedidoId
                         orderby c.idPedidoPacote
                         select new
                         {
                             c.idPedidoPacote,
                             c.idPedido,
                             c.peso,
                             c.rastreio,
                             c.formaDeEnvio,
                             c.carregadoCaminhao,
                             c.dataDeCarregamento,
                             c.idPedidoEnvio,
                             codJadlog = c.codJadlog ?? "null",
                             etiquetaImpressa = (c.etiquetaImpressa ?? false)
                         }).ToList();

            grdPacotes.DataSource = dados.Select(x => new
            {
                x.idPedidoPacote,
                x.idPedido,
                x.peso,
                x.rastreio,
                x.formaDeEnvio,
                carregadoCaminhao = x.carregadoCaminhao + (x.dataDeCarregamento == null ? "" : " - " + x.dataDeCarregamento.Value.ToString("G")),
                x.idPedidoEnvio,
                dataDeCarregamento = x.dataDeCarregamento == null ? "" : x.dataDeCarregamento.ToString(),
                foiCarregado = (x.carregadoCaminhao ?? false),
                x.codJadlog,
                x.etiquetaImpressa
            });

            var pedidoEnvio = (from c in data.tbPedidoEnvios
                               where c.idPedido == pedidoId
                               orderby c.idPedidoEnvio
                               select new
                               {
                                   c.idPedidoEnvio,
                                   c.idPedido,
                                   c.idUsuarioSeparacao,
                                   c.dataInicioSeparacao,
                                   c.dataFimSeparacao,
                                   c.idUsuarioEmbalagem,
                                   c.dataInicioEmbalagem,
                                   c.dataFimEmbalagem,
                                   c.formaDeEnvio,
                                   c.volumesEmbalados,
                                   c.nfeNumero,
                                   c.tbEnderecoSeparacao.enderecoSeparacao
                               }).ToList();

            grdPedidoEnvio.DataSource = pedidoEnvio;

            var itensSeparados = (from c in data.tbProdutoEstoques
                                  where c.pedidoId == pedidoId
                                  select new
                                  {
                                      c.idProdutoEstoque,
                                      c.pedidoId,
                                      c.produtoId,
                                      c.idPedidoFornecedorItem,
                                      c.enviado,
                                      c.idPedidoEnvio,
                                      c.idCentroDistribuicao,
                                      c.conferidoEmbalagem
                                  });

            grdItensSeparados.DataSource = itensSeparados;

            if ((!Page.IsPostBack && !Page.IsCallback) | rebind)
            {
                grdPacotes.DataBind();
                grdPedidoEnvio.DataBind();
                grdItensSeparados.DataBind();
            }

        }
    }

    protected void btnPesquisar_OnClick(object sender, EventArgs e)
    {
        int pedidoId = 0;
        bool result = Int32.TryParse(txtPedidoId.Text, out pedidoId);

        if (pedidoId.ToString().Length > 6)
        {
            pedidoId = rnFuncoes.retornaIdInterno(pedidoId);
        }

        if (pedidoId > 0)
            fillGrid(true, pedidoId);
        else
        {
            grdPacotes.DataSource = null;
            grdPacotes.DataBind();

            grdPedidoEnvio.DataSource = null;
            grdPedidoEnvio.DataBind();
        }


    }

    protected void OnCommand(object sender, CommandEventArgs e)
    {
        var button = (Button)sender;

        var textbox = (TextBox)button.Parent.FindControl("txtFormaDeEnvio");
        int idPedidoPacote = Convert.ToInt32(e.CommandArgument);

        if (textbox.Text != "")
            using (var data = new dbCommerceDataContext())
            {
                var pacote = (from c in data.tbPedidoPacotes where c.idPedidoPacote == idPedidoPacote select c).First();
                pacote.formaDeEnvio = textbox.Text.ToLower().Trim();
                data.SubmitChanges();
            }



        fillGrid(true, Convert.ToInt32(txtPedidoId.Text));

        //ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('" + textbox.Text + "');", true);
    }

    protected void btnMarcarEtiquetaComoImpressa_OnCommand(object sender, CommandEventArgs e)
    {
        int idPedidoPacote = Convert.ToInt32(e.CommandArgument);

        using (var data = new dbCommerceDataContext())
        {
            var pacote = (from c in data.tbPedidoPacotes where c.idPedidoPacote == idPedidoPacote select c).First();
            pacote.etiquetaImpressa = true;
            data.SubmitChanges();
        }
        fillGrid(true, Convert.ToInt32(txtPedidoId.Text));
    }

    protected void FormaDeEnvioEnvio_OnCommand(object sender, CommandEventArgs e)
    {
        var button = (Button)sender;

        var textbox = (TextBox)button.Parent.FindControl("txtFormaDeEnvio");
        int idPedidoEnvio = Convert.ToInt32(e.CommandArgument);

        if (textbox.Text != "")
            using (var data = new dbCommerceDataContext())
            {
                var envio = (from c in data.tbPedidoEnvios where c.idPedidoEnvio == idPedidoEnvio select c).First();
                envio.formaDeEnvio = textbox.Text.ToLower().Trim();
                data.SubmitChanges();
            }

        fillGrid(true, Convert.ToInt32(txtPedidoId.Text));

        //ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('" + textbox.Text + "');", true);
    }

    protected void FinalizarEmbalagemUsuario_OnCommand(object sender, CommandEventArgs e)
    {
        int idPedidoEnvio = Convert.ToInt32(e.CommandArgument);

        using (var data = new dbCommerceDataContext())
        {
            var envio = (from c in data.tbPedidoEnvios where c.idPedidoEnvio == idPedidoEnvio select c).First();
            envio.dataFimEmbalagem = DateTime.Now;
            data.SubmitChanges();
        }

        fillGrid(true, Convert.ToInt32(txtPedidoId.Text));

        //ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('" + textbox.Text + "');", true);
    }

    protected void UsuarioEmbalagemEnvio_OnCommand(object sender, CommandEventArgs e)
    {

        var button = (Button)sender;

        var textbox = (TextBox)button.Parent.FindControl("txtUsuarioDeEnvio");
        int idPedidoEnvio = Convert.ToInt32(e.CommandArgument);

        if (textbox.Text != "")
            using (var data = new dbCommerceDataContext())
            {
                var envio = (from c in data.tbPedidoEnvios where c.idPedidoEnvio == idPedidoEnvio select c).First();
                envio.idUsuarioEmbalagem = Convert.ToInt32(textbox.Text.ToLower().Trim());
                data.SubmitChanges();
            }

        fillGrid(true, Convert.ToInt32(txtPedidoId.Text));

    }

    protected void btnInformarCarregamento_OnCommand(object sender, CommandEventArgs e)
    {
        int idPedidoPacote = Convert.ToInt32(e.CommandArgument);

        using (var data = new dbCommerceDataContext())
        {
            var carregamento = (from c in data.tbPedidoPacotes where c.idPedidoPacote == idPedidoPacote select c).First();
            carregamento.carregadoCaminhao = true;
            data.SubmitChanges();
        }
        fillGrid(true, Convert.ToInt32(txtPedidoId.Text));
    }

    protected void btnInformarDataCarregamento_OnCommand(object sender, CommandEventArgs e)
    {
        try
        {
            int idPedidoPacote = Convert.ToInt32(e.CommandArgument);

            using (var data = new dbCommerceDataContext())
            {
                var carregamento = (from c in data.tbPedidoPacotes where c.idPedidoPacote == idPedidoPacote select c).First();
                carregamento.dataDeCarregamento = DateTime.Now;
                data.SubmitChanges();
            }
            fillGrid(true, Convert.ToInt32(txtPedidoId.Text));
        }
        catch (Exception)
        {

            throw;
        }

    }

    protected string ProximoPedidoEmbalarCd2()
    {
        string result = "";

        try
        {
            using (var data = new dbCommerceDataContext())
            {
                var pedidosCd1e2 = (from c in data.tbPedidoPacotes
                                    where c.idCentroDistribuicao == 2 && c.etiquetaImpressa == false && (c.concluido ?? false)
                                    select c.tbPedido).ToList();
                var pedidosCd2 = (from c in data.tbPedidos
                                  join d in data.tbItemPedidoEstoques on c.pedidoId equals d.tbItensPedido.pedidoId into itensEstoque
                                  join e in data.tbNotaFiscals on c.pedidoId equals e.idPedido into notas
                                  join f in data.tbProdutoEstoques on c.pedidoId equals f.pedidoId into itensSeparados
                                  where c.statusDoPedido == 11 &&
                                  itensEstoque.Count(x => x.cancelado == false && x.enviado == false && x.reservado == false && (x.reservado == false && x.autorizarParcial == true) == false) == 0
                                  && itensEstoque.Count(x => x.cancelado == false && x.enviado == false) > 0 &&
                                  itensEstoque.Count(x => x.cancelado == false && x.enviado == false && x.reservado == true) == itensSeparados.Count(x => x.enviado == false) &&
                                  (itensEstoque.Count(x => x.cancelado == false && x.enviado == false && x.reservado == true && x.idCentroDistribuicao == 1) == 0 && itensEstoque.Count(x => x.cancelado == false && x.enviado == false && x.reservado == true && x.idCentroDistribuicao == 2) <= itensSeparados.Count(x => x.idCentroDistribuicao == 2 && x.enviado == false))
                                  orderby (c.entregaEnviarPor == "JadlogExpressa" ? 0 : c.prioridadeEnvio ?? 9999),
                                  (
                                  notas.OrderByDescending(x => x.idNotaFiscal).FirstOrDefault() != null ?
                                  (notas.OrderByDescending(x => x.idNotaFiscal).FirstOrDefault().dataHora < c.prazoFinalPedido ?
                                  notas.OrderByDescending(x => x.idNotaFiscal).FirstOrDefault().dataHora :
                                  c.prazoMaximoPostagemAtualizado != null ?
                                  c.prazoMaximoPostagemAtualizado : c.prazoFinalPedido) :
                                  c.prazoMaximoPostagemAtualizado != null ?
                                  c.prazoMaximoPostagemAtualizado :
                                  c.prazoFinalPedido == null ? DateTime.Now : c.prazoFinalPedido),
                                  c.dataHoraDoPedido
                                  select c).ToList();

                var pedidosTotal = pedidosCd1e2.Union(pedidosCd2);

                var pedidoSelecionado = pedidosTotal.FirstOrDefault();

                result = pedidoSelecionado != null ? pedidoSelecionado.pedidoId.ToString() : "Não há pedido";

            }
        }
        catch (Exception)
        {

            return result;
        }


        return result;
    }

    protected string TotalPedidosParaEmbalarCd2()
    {
        string result = "";

        try
        {
            using (var data = new dbCommerceDataContext())
            {
                var pedidosCd1e2 = (from c in data.tbPedidoPacotes
                                    where c.idCentroDistribuicao == 2 && c.etiquetaImpressa == false
        && (c.concluido ?? false) == true
                                    select c.tbPedido).ToList();
                var pedidosCd2 = (from c in data.tbPedidos
                                  join d in data.tbItemPedidoEstoques on c.pedidoId equals d.tbItensPedido.pedidoId into itensEstoque
                                  join e in data.tbNotaFiscals on c.pedidoId equals e.idPedido into notas
                                  join f in data.tbProdutoEstoques on c.pedidoId equals f.pedidoId into itensSeparados
                                  where c.statusDoPedido == 11 &&
                                  itensEstoque.Count(x => x.cancelado == false && x.enviado == false && x.reservado == false && (x.reservado == false && x.autorizarParcial == true) == false) == 0
                                  && itensEstoque.Count(x => x.cancelado == false && x.enviado == false) > 0 &&
                                  itensEstoque.Count(x => x.cancelado == false && x.enviado == false && x.reservado == true) == itensSeparados.Count(x => x.enviado == false) &&
                                  (itensEstoque.Count(x => x.cancelado == false && x.enviado == false && x.reservado == true && x.idCentroDistribuicao == 1) == 0 && itensEstoque.Count(x => x.cancelado == false && x.enviado == false && x.reservado == true && x.idCentroDistribuicao == 2) <= itensSeparados.Count(x => x.idCentroDistribuicao == 2 && x.enviado == false))
                                  orderby (c.entregaEnviarPor == "JadlogExpressa" ? 0 : c.prioridadeEnvio == null ? 9999 : c.prioridadeEnvio),
                                  (
                                  notas.OrderByDescending(x => x.idNotaFiscal).FirstOrDefault() != null ?
                                  (notas.OrderByDescending(x => x.idNotaFiscal).FirstOrDefault().dataHora < c.prazoFinalPedido ?
                                  notas.OrderByDescending(x => x.idNotaFiscal).FirstOrDefault().dataHora :
                                  c.prazoMaximoPostagemAtualizado != null ?
                                  c.prazoMaximoPostagemAtualizado : c.prazoFinalPedido) :
                                  c.prazoMaximoPostagemAtualizado != null ?
                                  c.prazoMaximoPostagemAtualizado :
                                  c.prazoFinalPedido == null ? DateTime.Now : c.prazoFinalPedido),
                                  c.dataHoraDoPedido
                                  select c).ToList();

                var pedidosTotal = pedidosCd1e2.Union(pedidosCd2);

                result = pedidosTotal.Count().ToString();
            }
        }
        catch (Exception)
        {
            result = "Não há pedido";
        }

        return result;
    }
}