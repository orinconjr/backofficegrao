﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using DevExpress.Web.ASPxGridView;
using System.ServiceModel.Dispatcher;
using System.ServiceModel.Description;

public partial class emissao_pedidos2 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        gravaPendentesJadlog();
        fillGrid(false);
        if (popRastreio.ShowOnPageLoad == true)
        {
            txtRastreio.Focus();
            Page.RegisterStartupScript("SetFocus", "<script>document.getElementById('" + txtRastreio.ClientID + "').focus();</script>");
        }
        else
        {
            scriptBlock.Text = "";
        }
    }
    private void gravaPendentesJadlog()
    {
        var pedidosDc = new dbCommerceDataContext();
        var pendentesJadlog = (from c in pedidosDc.tbPedidoPacotes
                               where (c.codJadlog ?? "") == "" && (c.tbPedidoEnvio.formaDeEnvio == "jadlog" | c.tbPedidoEnvio.formaDeEnvio == "jadlogexpressa") &&
                                   (c.tbPedidoEnvio.nfeObrigatoria == true ? c.tbPedidoEnvio.nfeStatus : 2) == 2
                               select c.tbPedidoEnvio);
        foreach (var pendenteJadlog in pendentesJadlog)
        {
            if (pendenteJadlog.idPedidoEnvio != null)
            {
                pedidosDc.SubmitChanges();
            }
            rnFrete.GravaPedidoWebserviceJadlog(pendenteJadlog.idPedidoEnvio);
        }
    }
    private void fillGrid(bool rebind)
    {
        bool notaAlterada = false;
        if ((!Page.IsPostBack && !Page.IsCallback && Request.QueryString["desvincular"] == null) | rebind)
        {
            try
            {
                //rnPedidos.checaPedidosPendentesJadlog();
                //rnPedidos.checaPedidosPendentesTnt();
            }
            catch (Exception)
            {

            }
            try
            {
                notaAlterada = rnNotaFiscal.checaNotasPendentes();
                rnNotaFiscal.emiteNotasCompletas();
            }
            catch (Exception ex) { }
        }

        List<int> idsEnvios = new List<int>();

        var pedidosDc = new dbCommerceDataContext();


        var pedidosJad = (from c in pedidosDc.tbPedidoEnvios
                          join d in pedidosDc.tbPedidoPacotes on c.idPedidoEnvio equals d.idPedidoEnvio into pacotes
                          join e in pedidosDc.tbClientes on c.tbPedido.clienteId equals e.clienteId
                          where (pacotes.Where(x => x.codJadlog != "" && x.codJadlog != null && (x.rastreio == "" | x.rastreio == null)).Count() > 0) && c.idCentroDeDistribuicao < 2
                          orderby c.dataFimEmbalagem
                          select new
                          {
                              c.tbPedido.pedidoId,
                              c.dataFimEmbalagem,
                              nfeNumero = c.nfeNumero ?? 0,
                              c.idPedidoEnvio,
                              e.clienteCPFCNPJ,
                              c.tbPedido.endNomeDoDestinatario,
                              c.tbPedido.endRua,
                              c.tbPedido.endNumero,
                              endComplemento = c.tbPedido.endComplemento ?? "",
                              c.tbPedido.endCidade,
                              c.tbPedido.endBairro,
                              c.tbPedido.endEstado,
                              c.tbPedido.endCep,
                              c.formaDeEnvio,
                              c.tbPedido.gerarReversa,
                              totalPacotes = pacotes.Count(),
                              e.clienteFoneResidencial
                          }).ToList();

        var pedidosImpar = pedidosJad.Where(x => x.pedidoId % 2 != 0);

        grdEmissaoJadlog.DataSource = pedidosImpar.OrderBy(x => x.dataFimEmbalagem);
        if ((!Page.IsPostBack && !Page.IsCallback) | rebind) grdEmissaoJadlog.DataBind();//| notaAlterada
        idsEnvios.AddRange(pedidosJad.Select(x => x.idPedidoEnvio));

    }

    protected void grdEmissaoJadlog_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType != GridViewRowType.Data)
        {
            return;
        }

        int nfe = 0;
        int.TryParse(grdEmissaoJadlog.GetRowValues(e.VisibleIndex, new string[] { "nfeNumero" }).ToString(), out nfe);

        int idPedidoEnvio = 0;
        int.TryParse(grdEmissaoJadlog.GetRowValues(e.VisibleIndex, new string[] { "idPedidoEnvio" }).ToString(), out idPedidoEnvio);

        var pedidoDc = new dbCommerceDataContext();
        var envio = (from c in pedidoDc.tbPedidoEnvios where c.idPedidoEnvio == idPedidoEnvio select c).First();

        bool destaque = false;

        if (envio.formaDeEnvio.ToLower() == "jadlogexpressa")
        {
            destaque = true;
        }
        if (envio.tbPedido.gerarReversa)
        {
            destaque = true;
        }

        HyperLink btnExportarNota = (HyperLink)grdEmissaoJadlog.FindRowCellTemplateControl(e.VisibleIndex, grdEmissaoJadlog.Columns["exportar"] as GridViewDataColumn, "btnExportarNota");
        if (nfe > 0)
        {
            btnExportarNota.Visible = true;
            if (!string.IsNullOrEmpty(envio.tbPedido.nfeAccessKey))
            {
                btnExportarNota.NavigateUrl = "http://www.graodegente.com.br/admin/notas/danfes/" + nfe.ToString() + ".pdf";
            }
            else
            {

                destaque = true;
                var nota = (from c in pedidoDc.tbNotaFiscals where c.numeroNota == envio.nfeNumero && c.idPedidoEnvio == envio.idPedidoEnvio select c).First();
                if (!string.IsNullOrEmpty(nota.linkDanfe))
                {
                    btnExportarNota.NavigateUrl = nota.linkDanfe;
                }
                else
                {
                    btnExportarNota.NavigateUrl = rnNotaFiscal.retornaDanfeNotaPorNumeroNota(envio.nfeNumero.ToString(), nota.idNotaFiscal);
                }
            }
        }
        else
        {
            btnExportarNota.Visible = false;
        }

        if (destaque)
        {
            int i = 0;
            while ((i < e.Row.Cells.Count))
            {
                e.Row.Cells[i].Style.Add("background", "#FF151C");
                i++;
            }
        }
        TextBox txtPesoVolumes = (TextBox)grdEmissaoJadlog.FindRowCellTemplateControl(e.VisibleIndex, grdEmissaoJadlog.Columns["pesoVolumes"] as GridViewDataColumn, "txtPesoVolumes");

        string pesoVolumes = "";
        var pesos = (from c in pedidoDc.tbPedidoPacotes where c.idPedidoEnvio == idPedidoEnvio && (c.rastreio == "" | c.rastreio == null) select c);
        foreach (var peso in pesos)
        {
            if (!string.IsNullOrEmpty(pesoVolumes)) pesoVolumes += Environment.NewLine;
            pesoVolumes += (Convert.ToDecimal(peso.peso) / 1000);
        }
        txtPesoVolumes.Text = pesoVolumes;

        TextBox txtDDD = (TextBox)grdEmissaoJadlog.FindRowCellTemplateControl(e.VisibleIndex, grdEmissaoJadlog.Columns["ddd"] as GridViewDataColumn, "txtDDD");
        TextBox txtTelefone = (TextBox)grdEmissaoJadlog.FindRowCellTemplateControl(e.VisibleIndex, grdEmissaoJadlog.Columns["telefone"] as GridViewDataColumn, "txtTelefone");

        var cliente = (from c in pedidoDc.tbClientes where c.clienteId == envio.tbPedido.clienteId select c).First();
        var telsemespaco = cliente.clienteFoneResidencial.Replace(" ", "");
        var telspli = telsemespaco.Split(')');
        string ddd = "";
        string telefone = "";
        if (telspli.Length > 1)
        {
            ddd = telspli[0].Replace("(", "").Replace(")", "").Trim();
            telefone = telspli[1].Replace("-", "").Trim();
        }
        txtDDD.Text = ddd;
        txtTelefone.Text = telefone;

        if (e.RowType != GridViewRowType.Data)
        {
            return;
        }


    }
    protected void grdEmissaoJadlog_OnCustomUnboundColumnData(object sender, ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "pedidoIdCliente")
        {
            int pedidoId = Convert.ToInt32(e.GetListSourceFieldValue("pedidoId"));
            e.Value = rnFuncoes.retornaIdCliente(pedidoId);
        }
        if (e.Column.FieldName == "pesoTotal")
        {
            var pedidosDc = new dbCommerceDataContext();
            int idPedidoEnvio = Convert.ToInt32(e.GetListSourceFieldValue("idPedidoEnvio"));
            int pesoTotal = 0;
            try
            {
                pesoTotal = (from c in pedidosDc.tbPedidoPacotes where c.idPedidoEnvio == idPedidoEnvio && (c.rastreio == "" | c.rastreio == null) select c).Sum(x => x.peso);
            }
            catch (Exception)
            {

            }
            e.Value = pesoTotal;
        }
        //if (e.Column.FieldName == "pesoVolumes")
        //{
        //    var pedidosDc = new dbCommerceDataContext();
        //    int idPedidoEnvio = Convert.ToInt32(e.GetListSourceFieldValue("idPedidoEnvio"));
        //    string pesoVolumes = "";
        //    var pesos = (from c in pedidosDc.tbPedidoPacotes where c.idPedidoEnvio == idPedidoEnvio && (c.rastreio == "" | c.rastreio == null) select c);
        //    foreach (var peso in pesos)
        //    {
        //        if (!string.IsNullOrEmpty(pesoVolumes)) pesoVolumes += Environment.NewLine;
        //        pesoVolumes += (Convert.ToDecimal(peso.peso) / 1000);
        //    }
        //    e.Value = pesoVolumes;
        //}
        if (e.Column.FieldName == "nfe")
        {
            int idPedidoEnvio = Convert.ToInt32(e.GetListSourceFieldValue("idPedidoEnvio"));
            int pedidoId = Convert.ToInt32(e.GetListSourceFieldValue("pedidoId"));
            int nfeNumero = Convert.ToInt32(e.GetListSourceFieldValue("nfeNumero"));

            if (nfeNumero > 0)
            {
                e.Value = nfeNumero.ToString();
            }
            else
            {
                e.Value = rnFuncoes.retornaIdCliente(pedidoId).ToString() + idPedidoEnvio.ToString();
            }
        }
        if (e.Column.FieldName == "servicoEnvio")
        {
            var formaDeEnvio = e.GetListSourceFieldValue("formaDeEnvio").ToString();
            if (formaDeEnvio == "jadlogexpressa")
            {
                e.Value = "EXPRESSA";
            }
            else
            {
                e.Value = "Normal";
            }

            var reversa = Convert.ToBoolean(e.GetListSourceFieldValue("gerarReversa"));
            if (reversa)
            {
                e.Value = e.Value + " / REVERSA";
            }
        }
    }


    public void AlertShow(string message)
    {
        base.ClientScript.RegisterStartupScript(base.GetType(), "myalert", "alert('" + message + "'); ", true);
    }


    protected void btnRastreio_OnCommand(object sender, CommandEventArgs e)
    {
        popRastreio.ShowOnPageLoad = true;
        txtRastreio.Text = "";
        hdfRastreio.Value = e.CommandArgument.ToString();
        Page.RegisterStartupScript("SetFocus", "<script>document.getElementById('" + txtRastreio.ClientID + "').focus();</script>");
    }
    protected void btnGravarRastreio_OnClick(object sender, EventArgs e)
    {
        int idPedidoEnvio = Convert.ToInt32(hdfRastreio.Value);
        var data = new dbCommerceDataContext();

        var pacotesRastreioDuplicado =
           (from c in data.tbPedidoPacotes
            where c.rastreio == txtRastreio.Text && c.idPedidoEnvio != idPedidoEnvio
            select c).Any();
        if (pacotesRastreioDuplicado)
        {
            string mensagem = "";
            mensagem += "Rastreio duplicado<br>";
            mensagem += txtRastreio.Text;
            rnEmails.EnviaEmail("", "andre@bark.com.br;ouvidoria@graodegente.com.br", "", "", "", mensagem, "Rastreio Duplicado");
            AlertShow("Código de Rastreio Duplicado. Verifique o código ou entre em contato com o suporte.");
            txtRastreio.Text = "";
            return;
        }

        var pacotes = (from c in data.tbPedidoPacotes where c.idPedidoEnvio == idPedidoEnvio select c);
        foreach (var pacote in pacotes)
        {
            pacote.rastreio = txtRastreio.Text;
            data.SubmitChanges();
        }
        hdfRastreio.Value = "";
        txtRastreio.Text = "";
        popRastreio.ShowOnPageLoad = false;
        fillGrid(true);
    }

}