﻿<%@ Page Language="C#" AutoEventWireup="true" Theme="Glass" CodeFile="pedidos1.aspx.cs" Inherits="emissao_pedidos1" %>

<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.v11.1" namespace="DevExpress.Web.ASPxPopupControl" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>BarkCommerce - Sistema Administrativo</title>
    <link href="../admin/estilos/estilos.css" rel="stylesheet" type="text/css" />
    <script language="JavaScript" type="text/javascript" src="../admin/js/funcoes.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
              <style type="text/css">
                  .dxgv {
                      vertical-align: top !important;
                  }
              </style>
    <asp:Literal runat="server" ID="scriptBlock"></asp:Literal>
    <script type="text/javascript">
        var abreRastreio = function (idPedidoEnvio) {
            poprastreio.Show();
            document.getElementById("hdfRastreio").value = idPedidoEnvio;
            document.getElementById("txtRastreio").value = "";
            document.getElementById("txtRastreio").focus();
            return false;
        }
    </script>
</head>
<body>
    <form id="formulario" runat="server">
    <table align="center" cellpadding="0" cellspacing="0" class="tablePrincipal" 
        style="width: 100%">
        <tr>
            <td>
    <table cellpadding="0" cellspacing="0" style="width: 920px" align="center">
        <tr>
            <td>
                <table cellpadding="0" cellspacing="0" style="width: 920px">
                    <tr>
                        <td style="width: 210px; height: 97px">
                        </td>
                        <td style="width: 140px; height: 97px">
                        </td>
                        <td style="width: 189px; height: 97px">
                        </td>
                        <td style="width: 208px; height: 97px">
                        </td>
                        <td style="height: 97px">
                        </td>
                    </tr>
                    <tr valign="bottom">
                        <td style="width: 210px">
                            &nbsp;</td>
                        <td class="textoCabecalho" style="width: 140px">
                        </td>
                        <td class="textoCabecalho" style="width: 189px">
                            
                        </td>
                        <td class="textoCabecalho" style="width: 208px">
                        </td>
                        <td>
                        </td>
                        <td class="textoCabecalho">Seu IP:
                            <asp:Label ID="lblIp" runat="server" Font-Bold="True" Text="200.258.110.123" 
                EnableTheming="False"></asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td height="12"></td>
        </tr>
        
        <tr>
            <td height="30"></td>
        </tr>
        <tr>
            <td bgcolor="White" style="height: 300px" valign="top">
                
                <table cellpadding="0" cellspacing="0" style="width: 920px">
    <tr>
        <td class="tituloPaginas" valign="top">
            <asp:Label ID="lblAcao" runat="server">Pedidos aguardando Emissão</asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            
    <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
        <tr class="rotulos" style="font-weight: bold; font-size: 14px;">
            <td>
                Aguardando Emissão na Jadlog
            </td>
        </tr>
        <tr>
            <td>
                <dxwgv:ASPxGridView ID="grdEmissaoJadlog" runat="server" AutoGenerateColumns="False" Width="834px" Cursor="auto" KeyFieldName="pedidoId"
                     OnHtmlRowCreated="grdEmissaoJadlog_HtmlRowCreated" OnHtmlRowPrepared="grdEmissaoJadlog_HtmlRowPrepared"
                    OnCustomUnboundColumnData="grdEmissaoJadlog_OnCustomUnboundColumnData" Settings-ShowHorizontalScrollBar="True">
                    <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                    <Styles>
                        <Footer Font-Bold="True">
                        </Footer>
                    </Styles>
                    <SettingsText ConfirmDelete="Tem certeza que deseja excluir?" emptydatarow="Nenhum registro encontrado." />
                    <SettingsPager Position="TopAndBottom" PageSize="10" ShowDisabledButtons="False" AlwaysShowPager="True">
                        <Summary AllPagesText="Páginas: {0} - {1} ({2} items)" Text="Página {0} de {1} ({2} registros encontrados)" />
                    </SettingsPager>
                    <settings showfilterrow="True" ShowGroupButtons="False" ShowHeaderFilterButton="True" ShowFooter="True" />
                    <SettingsEditing EditFormColumnCount="4" PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1" PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter" PopupEditFormWidth="700px" />
                    <Columns>
                        <dxwgv:GridViewDataTextColumn Caption="Rastreio" Name="rastreio" VisibleIndex="0" Width="80">
                            <DataItemTemplate>
                                <a href="javascript:;" onclick='abreRastreio("<%# Eval("idPedidoEnvio") %>")' >Gravar Rastreio</a>
                            </DataItemTemplate>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="ID" FieldName="pedidoId" VisibleIndex="0" Width="50" Visible="False">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="ID envio" FieldName="idPedidoEnvio" VisibleIndex="0" Width="50" Visible="False">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="NFE" FieldName="nfeNumero" VisibleIndex="0" Width="50" Visible="False">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Serviço" FieldName="servicoEnvio" UnboundType="String" VisibleIndex="0" Width="50">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="CPF" FieldName="clienteCPFCNPJ" VisibleIndex="0" Width="50">
                            <DataItemTemplate>
                                <div style="height: 100%;">
                                    <asp:TextBox Width="90" runat="server" ID="txtCpf" Text='<%# Eval("clienteCPFCNPJ").ToString().Trim().Replace("-", "") %>'></asp:TextBox>
                                </div>
                            </DataItemTemplate>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Destinatário" FieldName="endNomeDoDestinatario" VisibleIndex="0" Width="50">
                            <DataItemTemplate>
                                <div style="height: 100%;">
                                    <asp:TextBox Width="90" runat="server" ID="txtDestinatario" Text='<%# rnFuncoes.removeCaracteresEspeciaisDeixaEspaco(rnFuncoes.limpaStringDeixaEspaco(Eval("endNomeDoDestinatario").ToString())).ToLower() %>'></asp:TextBox>
                                </div>
                            </DataItemTemplate>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Rua" FieldName="endRua" VisibleIndex="0" Width="50">
                            <DataItemTemplate>
                                <div style="height: 100%;">
                                <asp:TextBox Width="50" runat="server" ID="txtRua" Text='<%# rnFuncoes.removeCaracteresEspeciaisDeixaEspaco(rnFuncoes.limpaStringDeixaEspaco(Eval("endRua").ToString())).ToLower() + " - " + rnFuncoes.removeCaracteresEspeciaisDeixaEspaco(rnFuncoes.limpaStringDeixaEspaco(Eval("endNumero").ToString())).ToLower() + " - " + rnFuncoes.removeCaracteresEspeciaisDeixaEspaco(rnFuncoes.limpaStringDeixaEspaco(Eval("endComplemento").ToString())).ToLower() %>'></asp:TextBox>
                            </div>
                                    </DataItemTemplate>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Cidade" FieldName="endCidade" VisibleIndex="0" Width="50">
                            <DataItemTemplate>
                                <div style="height: 100%;">
                                <asp:TextBox Width="50" runat="server" ID="txtCidade" Text='<%#  rnFuncoes.removeCaracteresEspeciaisDeixaEspaco(rnFuncoes.limpaStringDeixaEspaco(Eval("endCidade").ToString())).ToLower()  %>'></asp:TextBox>
                                </div>
                            </DataItemTemplate>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Bairro" FieldName="endBairro" VisibleIndex="0" Width="50">
                            <DataItemTemplate>
                                <div style="height: 100%;">
                                <asp:TextBox Width="50" runat="server" ID="txtBairro" Text='<%#  rnFuncoes.removeCaracteresEspeciaisDeixaEspaco(rnFuncoes.limpaStringDeixaEspaco(Eval("endBairro").ToString())).ToLower() %>'></asp:TextBox>
                                </div>
                            </DataItemTemplate>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="UF" FieldName="endEstado" VisibleIndex="0" Width="50">
                            <DataItemTemplate>
                                <div style="height: 100%;">
                                <asp:TextBox Width="50" runat="server" ID="txtEstado" Text='<%# Eval("endEstado").ToString().ToLower() %>'></asp:TextBox>
                                </div>
                            </DataItemTemplate>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="CEP" FieldName="endCep" VisibleIndex="0" Width="50">
                            <DataItemTemplate>
                                <div style="height: 100%;">
                                <asp:TextBox Width="50" runat="server" ID="txtCep" Text='<%# Eval("endCep").ToString().Trim().Replace("-", "") %>'></asp:TextBox>
                                </div>
                            </DataItemTemplate>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="DDD" FieldName="ddd" UnboundType="String" VisibleIndex="0" Width="50">
                            <DataItemTemplate>
                                <asp:TextBox Width="50" runat="server" ID="txtDDD"></asp:TextBox>
                            </DataItemTemplate>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Telefone" FieldName="telefone" UnboundType="String" VisibleIndex="0" Width="100">
                            <DataItemTemplate>
                                <asp:TextBox Width="100" runat="server" ID="txtTelefone"></asp:TextBox>
                            </DataItemTemplate>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Pacotes" FieldName="totalPacotes" VisibleIndex="0" Width="50">
                            <DataItemTemplate>
                                <asp:TextBox Width="50" runat="server" ID="txtPacotes" Text='<%# Eval("totalPacotes") %>'></asp:TextBox>
                            </DataItemTemplate>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Peso Volumes" FieldName="pesoVolumes" VisibleIndex="0" Width="100" UnboundType="String">
                            <DataItemTemplate>
                                <asp:TextBox TextMode="MultiLine" Height="96" Width="100" runat="server" ID="txtPesoVolumes"></asp:TextBox>
                            </DataItemTemplate>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Pedido" FieldName="pedidoIdCliente" VisibleIndex="0" Width="50" UnboundType="Integer">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataDateColumn Caption="Data de Emissão" FieldName="dataFimEmbalagem" VisibleIndex="0">
                            <propertiesdateedit displayformatstring="g"></propertiesdateedit>
                        </dxwgv:GridViewDataDateColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Nota" FieldName="nfe" VisibleIndex="0" Width="50" UnboundType="Integer">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Peso Total" FieldName="pesoTotal" VisibleIndex="0" Width="50" UnboundType="Integer">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Exportar Nota" Name="exportar" VisibleIndex="0" Width="80">
                            <DataItemTemplate>
                                <asp:HyperLink runat="server" ID="btnExportarNota" Target="_blank">Imprimir Nota</asp:HyperLink>                    
                            </DataItemTemplate>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Despriorizar" Name="despriorizar" VisibleIndex="0" Width="80">
                            <DataItemTemplate> 
                                 <asp:LinkButton runat="server" ID="lbtEnviarParaFimDaFila" Text="Fim da fila" OnCommand="lbtEnviarParaInicioOuFimDaFila_Command" CommandArgument='<%# Eval("idPedidoEnvio") + "#" + Eval("prioridade") %>'></asp:LinkButton>                
                                 <asp:HiddenField runat="server" ID="hfPrioridade" Value='<%# Eval("prioridade").ToString() %>' />    
                            </DataItemTemplate>
                        </dxwgv:GridViewDataTextColumn>
                    </Columns>
                    <StylesEditors>
                        <Label Font-Bold="True">
                        </Label>
                    </StylesEditors>
                </dxwgv:ASPxGridView>
                <br />&nbsp;
                <br />&nbsp;
                <br />&nbsp;
                <br />&nbsp;
                <br />&nbsp;
                <br />&nbsp;
                <br />&nbsp;
                <br />&nbsp;
                <br />&nbsp;
                <br />&nbsp;
                <br />&nbsp;
                <br />&nbsp;
                <br />&nbsp;
                <br />&nbsp;
                <br />&nbsp;
                <dx:ASPxPopupControl ID="popRastreio" runat="server" CloseAction="CloseButton" Modal="True"
                    PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="TopSides" ClientInstanceName="poprastreio"
                    HeaderText="Rastreio" AllowDragging="True" PopupAnimationType="None" EnableViewState="True">
                    <ContentCollection>
                        <dx:PopupControlContentControl ID="PopupControlContentControl2" runat="server">
                            <div style="width: 600px; height: 300px; overflow: scroll;">
                                <div style="margin: 15px; overflow: hidden; padding-bottom: 20px;">
                                    <div>
                                        Código de Rastreio:<br />
                                        <asp:TextBox ID="txtRastreio" ClientIDMode="Static" runat="server" CssClass="campos" MaxLength="14"  Width="130px"></asp:TextBox>
                                        
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtRastreio" Display="None" ErrorMessage="Preencha o código de Rastreio." SetFocusOnError="True" ValidationGroup="rastreio"></asp:RequiredFieldValidator>
                                    </div>
                                    <div style="padding-top: 15px;">
                                        <asp:Button runat="server" ID="btnGravarRastreio" OnClick="btnGravarRastreio_OnClick" Text="Gravar Rastreio" ValidationGroup="rastreio"/>
                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True" ShowSummary="False" DisplayMode="List" ValidationGroup="rastreio" />
                                        <asp:HiddenField runat="server" ID="hdfRastreio" ClientIDMode="Static"/>
                                    </div>
                                </div>
                            </div>
                        </dx:PopupControlContentControl>
                    </ContentCollection>
                    <ContentStyle>
                        <Paddings PaddingBottom="5px" />
                    </ContentStyle>
                </dx:ASPxPopupControl>
            </td>
        </tr>          
    </table>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
