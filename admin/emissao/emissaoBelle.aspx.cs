﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxGridView;

public partial class emissao_emissaoBelle : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["alterarbelle"] != null)
        {
            int idAlterar = rnFuncoes.retornaIdInterno(Convert.ToInt32(Request.QueryString["alterarbelle"]));
            var data = new dbCommerceDataContext();
            var pacotes = (from c in data.tbPedidoPacotes where c.idPedido == idAlterar select c);
            foreach (var pacote in pacotes)
            {
                pacote.codJadlog = "";
                pacote.formaDeEnvio = "belle";
                data.SubmitChanges();
            }
            var envios = (from c in data.tbPedidoEnvios where c.idPedido == idAlterar select c);
            foreach (var envio in envios)
            {
                envio.formaDeEnvio = "belle";
                envio.nfeObrigatoria = true;
                data.SubmitChanges();
            }
        }
        if (Request.QueryString["desvincular"] != null)
        {
            //desvincularNota(Convert.ToInt32(Request.QueryString["desvincular"].ToString()));
        }
        if (Request.QueryString["email"] != null)
        {
            int idEnvio = Convert.ToInt32(Request.QueryString["email"]);

        }
        if (Request.QueryString["email1"] != null)
        {
            int idEnvio = Convert.ToInt32(Request.QueryString["email1"]);
            rnEmails.enviaCodigoDoTrakingJadlog(idEnvio, "andre@bark.com.br");
        }

        fillGrid(false);
        if (popRastreio.ShowOnPageLoad == true)
        {
            txtRastreio.Focus();
            Page.RegisterStartupScript("SetFocus", "<script>document.getElementById('" + txtRastreio.ClientID + "').focus();</script>");
        }
        else
        {
            scriptBlock.Text = "";
        }
    }

    private void fillGrid(bool rebind)
    {
        bool notaAlterada = false;

        List<int> idsEnvios = new List<int>();

        var pedidosDc = new dbCommerceDataContext();

        var pedidosNotaProcessamento =
            (from c in pedidosDc.tbPedidoEnvios
             join d in pedidosDc.tbNotaFiscals on c.nfeNumero equals d.numeroNota into notaObj
             //join d in pedidosDc.tbNotaFiscals on new { c.nfeNumero, c.idPedido } equals new { d.numeroNota, d.idPedido } into notaObj
             where (c.formaDeEnvio == "belle") && (c.nfeStatus != 2) && (c.nfeNumero ?? 0) > 0
             select new
             {
                 c.tbPedido.pedidoId,
                 c.dataFimEmbalagem,
                 c.idPedidoEnvio,
                 c.nfeNumero,
                 status = notaObj.FirstOrDefault(x => x.idPedido == c.tbPedido.pedidoId) == null ? "Aguardando Processamento" : notaObj.FirstOrDefault(x => x.idPedido == c.tbPedido.pedidoId).ultimoStatus,
                 c.tbPedido.endEstado
             }).OrderBy(x => x.dataFimEmbalagem);
        grdNotaProcessamento.DataSource = pedidosNotaProcessamento;
        if ((!Page.IsPostBack && !Page.IsCallback) | rebind) grdNotaProcessamento.DataBind();//| notaAlterada
        idsEnvios.AddRange(pedidosNotaProcessamento.Select(x => x.idPedidoEnvio));

        #region notas Belle
        var pedidosBelle = (from c in pedidosDc.tbPedidoEnvios
                            join d in pedidosDc.tbPedidoPacotes on c.idPedidoEnvio equals d.idPedidoEnvio into pacotes
                            where (pacotes.Any(x => (x.despachado ?? false) == false)) && c.nfeStatus == 2 && c.formaDeEnvio == "belle" && (c.nfeImpressa ?? false) == false
                            orderby c.tbPedido.dataEmbaladoFim
                            select new { c.tbPedido.pedidoId, c.dataFimEmbalagem, nfeNumero = c.nfeNumero ?? 0, c.idPedidoEnvio, c.tbPedido.endEstado });
        grdEmissaoBelle.DataSource = pedidosBelle;
        if ((!Page.IsPostBack && !Page.IsCallback) | rebind) grdEmissaoBelle.DataBind();//| notaAlterada
        idsEnvios.AddRange(pedidosBelle.Select(x => x.idPedidoEnvio));

        grdNotasImpressasBelle.DataSource = (from c in pedidosDc.tbPedidoEnvios
                                             join d in pedidosDc.tbPedidoPacotes on c.idPedidoEnvio equals d.idPedidoEnvio into pacotes
                                             join n in pedidosDc.tbNotaFiscals on c.idPedidoEnvio equals n.idPedidoEnvio
                                             where (c.formaDeEnvio == "belle" && (c.nfeImpressa ?? false))
                                             orderby c.dataFimEmbalagem descending 
                                             select new { c.tbPedido.pedidoId, c.dataFimEmbalagem, nfeNumero = c.nfeNumero ?? 0, c.idPedidoEnvio, c.tbPedido.endEstado, danfe = n.nfeKey }).Take(500); 
        if ((!Page.IsPostBack && !Page.IsCallback) | rebind) grdNotasImpressasBelle.DataBind();//| notaAlterada

        var pedidosBelleDespacho = (from c in pedidosDc.tbPedidoEnvios
                                    join d in pedidosDc.tbPedidoPacotes on c.idPedidoEnvio equals d.idPedidoEnvio into pacotes
                                    where (pacotes.Any(x => (x.despachado ?? false) == false && x.formaDeEnvio == "belle")) && c.nfeStatus == 2 && c.formaDeEnvio == "belle" && (c.nfeImpressa ?? false) == true
                                    orderby c.tbPedido.dataEmbaladoFim
                                    select new { c.tbPedido.pedidoId, c.dataFimEmbalagem, nfeNumero = c.nfeNumero ?? 0, c.idPedidoEnvio, c.tbPedido.endEstado });
        grdBelleAguardando.DataSource = pedidosBelleDespacho;

        if ((!Page.IsPostBack && !Page.IsCallback) | rebind) grdBelleAguardando.DataBind();//| notaAlterada
        if ((!Page.IsPostBack && !Page.IsCallback) | rebind)//| notaAlterada
        {
            var produtosEstoque = (from c in pedidosDc.tbProdutoEstoques where c.enviado == false && idsEnvios.Contains(c.idPedidoEnvio ?? 0) select c);
            foreach (var produtoEstoque in produtosEstoque)
            {
                produtoEstoque.enviado = true;
                produtoEstoque.dataEnvio = DateTime.Now;
            }
            pedidosDc.SubmitChanges();
        }
        #endregion notas Belle
    }

    protected void grd_OnCustomUnboundColumnData(object sender, ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "pedidoIdCliente")
        {
            int pedidoId = Convert.ToInt32(e.GetListSourceFieldValue("pedidoId"));
            e.Value = rnFuncoes.retornaIdCliente(pedidoId);
        }
    }

    protected void btnGerarNota_OnCommand(object sender, CommandEventArgs e)
    {
        int idPedidoEnvio = Convert.ToInt32(e.CommandArgument);
        var pedidosDc = new dbCommerceDataContext();
        var produtosIds = (from c in pedidosDc.tbProdutoEstoques where c.idPedidoEnvio == idPedidoEnvio select c.produtoId).ToList();
        hdfIdPedidoEnvio.Value = idPedidoEnvio.ToString();

        var produtosDc = new dbCommerceDataContext();
        var produtos = (from c in produtosDc.tbProdutos where produtosIds.Contains(c.produtoId) select c);
        lstProdutoNota.DataSource = produtos;
        lstProdutoNota.DataBind();

        var pedido = (from c in pedidosDc.tbPedidoEnvios where c.idPedidoEnvio == idPedidoEnvio select new { c.tbPedido.endCep, c.tbPedido.endCidade, c.tbPedido.endEstado }).First();
        litCidade.Text = pedido.endCidade;
        litEstado.Text = pedido.endEstado;
        var cepsDc = new dbCommerceDataContext();
        try
        {
            string cepOut = pedido.endCep.Replace("-", "");

            var endereco = (from c in cepsDc.tbCepEnderecos where c.cep == cepOut.Replace("-", "") select c).FirstOrDefault();

            if (endereco != null)
            {
                var cidade = (from c in cepsDc.tbCepCidades where c.id_cidade == endereco.id_cidade select c).FirstOrDefault();
                if (cidade != null)
                {
                    txtCodigoIbge.Text = cidade.cod_ibge;
                }
            }

        }
        catch (Exception)
        {

        }
        pcNota.ShowOnPageLoad = true;
    }

    protected void lstProdutoNota_DataBound(object sender, ListViewItemEventArgs e)
    {
        HiddenField hiddenProdutoId = (HiddenField)e.Item.FindControl("hiddenProdutoId");
        int produtoId = Convert.ToInt32(hiddenProdutoId.Value);
        #region fotos
        Image fotoDestaque = (Image)e.Item.FindControl("fotoDestaque");
        if (rnFotos.produtoFotoDestaqueSeleciona_PorProdutoId(produtoId).Tables[0].Rows.Count > 0)
            fotoDestaque.ImageUrl = ConfigurationManager.AppSettings["caminhoVirtual"] + "fotos/" + produtoId + "/pequena_" + rnFotos.produtoFotoDestaqueSeleciona_PorProdutoId(produtoId).Tables[0].Rows[0]["produtoFoto"].ToString() + ".jpg";
        else
            fotoDestaque.ImageUrl = ConfigurationManager.AppSettings["caminhoVirtual"] + "fotos/naoExiste/pequena.jpg";
        #endregion
    }

    protected void btnGerarNota_Click(object sender, EventArgs e)
    {
        foreach (ListViewItem item in lstProdutoNota.Items)
        {
            HiddenField hiddenProdutoId = (HiddenField)item.FindControl("hiddenProdutoId");
            TextBox txtncm = (TextBox)item.FindControl("txtncm");
            int produtoId = Convert.ToInt32(hiddenProdutoId.Value);

            var produtoDc = new dbCommerceDataContext();
            var produto = (from c in produtoDc.tbProdutos where c.produtoId == produtoId select c).FirstOrDefault();
            produto.ncm = txtncm.Text;
            produtoDc.SubmitChanges();
        }

        var xmlNotaFiscal = rnNotaFiscal.gerarNotaFiscalEnvio(Convert.ToInt32(hdfIdPedidoEnvio.Value), txtCodigoIbge.Text);

        int idPedidoEnvio = Convert.ToInt32(hdfIdPedidoEnvio.Value);
        rnNotaFiscal.autorizarNota(xmlNotaFiscal.xml, idPedidoEnvio, xmlNotaFiscal.idNotaFiscal);

        hdfIdPedidoEnvio.Value = "";
        txtCodigoIbge.Text = "";
        pcNota.ShowOnPageLoad = false;
        fillGrid(true);
    }

    protected void gridEmissaoBelle_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType != GridViewRowType.Data)
        {
            return;
        }

        int nfe = 0;
        int.TryParse(grdEmissaoBelle.GetRowValues(e.VisibleIndex, new string[] { "nfeNumero" }).ToString(), out nfe);

        int idPedidoEnvio = 0;
        int.TryParse(grdEmissaoBelle.GetRowValues(e.VisibleIndex, new string[] { "idPedidoEnvio" }).ToString(), out idPedidoEnvio);

        var pedidoDc = new dbCommerceDataContext();
        var envio = (from c in pedidoDc.tbPedidoEnvios where c.idPedidoEnvio == idPedidoEnvio select c).First();

        HyperLink btnExportarNota = (HyperLink)grdEmissaoBelle.FindRowCellTemplateControl(e.VisibleIndex, grdEmissaoBelle.Columns["exportar"] as GridViewDataColumn, "btnExportarNota");
        if (nfe > 0)
        {
            btnExportarNota.Visible = true;
            if (!string.IsNullOrEmpty(envio.tbPedido.nfeAccessKey))
            {
                btnExportarNota.NavigateUrl = "http://www.graodegente.com.br/admin/notas/danfes/" + nfe.ToString() + ".pdf";
            }
            else
            {
                var nota = (from c in pedidoDc.tbNotaFiscals where c.numeroNota == envio.nfeNumero && c.idPedidoEnvio == envio.idPedidoEnvio select c).First();
                if (!string.IsNullOrEmpty(nota.linkDanfe))
                {
                    btnExportarNota.NavigateUrl = nota.linkDanfe;
                }
                else
                {
                    btnExportarNota.NavigateUrl = rnNotaFiscal.retornaDanfeNotaPorNumeroNota(envio.nfeNumero.ToString(), nota.idNotaFiscal);
                }
            }
        }
        else
        {
            btnExportarNota.Visible = false;
        }
    }

    protected void btnRastreio_OnCommand(object sender, CommandEventArgs e)
    {
        popRastreio.ShowOnPageLoad = true;
        txtRastreio.Text = "";
        hdfRastreio.Value = e.CommandArgument.ToString();
        Page.RegisterStartupScript("SetFocus", "<script>document.getElementById('" + txtRastreio.ClientID + "').focus();</script>");
    }

    public void AlertShow(string message)
    {
        base.ClientScript.RegisterStartupScript(base.GetType(), "myalert", "alert('" + message + "'); ", true);
    }

    protected void btnGravarRastreio_OnClick(object sender, EventArgs e)
    {
        int idPedidoEnvio = Convert.ToInt32(hdfRastreio.Value);
        var data = new dbCommerceDataContext();

        var pacotesRastreioDuplicado =
            (from c in data.tbPedidoPacotes
             where c.rastreio == txtRastreio.Text && c.idPedidoEnvio != idPedidoEnvio
             select c).Any();
        if (pacotesRastreioDuplicado)
        {
            string mensagem = "";
            mensagem += "Rastreio duplicado<br>";
            mensagem += txtRastreio.Text;
            rnEmails.EnviaEmail("", "andre@bark.com.br;ouvidoria@graodegente.com.br", "", "", "", mensagem, "Rastreio Duplicado");
            AlertShow("Código de Rastreio Duplicado. Verifique o código ou entre em contato com o suporte.");
            txtRastreio.Text = "";
            return;
        }

        var pacotes = (from c in data.tbPedidoPacotes where c.idPedidoEnvio == idPedidoEnvio select c);
        foreach (var pacote in pacotes)
        {
            pacote.rastreio = txtRastreio.Text;
            data.SubmitChanges();
        }
        hdfRastreio.Value = "";
        txtRastreio.Text = "";
        popRastreio.ShowOnPageLoad = false;
        fillGrid(true);
    }

    protected void grdNotaProcessamento_OnCustomUnboundColumnData(object sender, ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "pedidoIdCliente")
        {
            int pedidoId = Convert.ToInt32(e.GetListSourceFieldValue("pedidoId"));
            e.Value = rnFuncoes.retornaIdCliente(pedidoId);
        }
        if (e.Column.FieldName == "informacao")
        {
            string nfeNumero = e.GetListSourceFieldValue("nfeNumero").ToString();
            //e.Value = rnNotaFiscal.retornaInformacaoNotaPorNumeroNota(nfeNumero);
        }
    }

    protected void btnImpresso_OnCommand(object sender, CommandEventArgs e)
    {
        int idPedidoEnvio = Convert.ToInt32(e.CommandArgument);
        var data = new dbCommerceDataContext();
        var pedido = (from c in data.tbPedidoEnvios where c.idPedidoEnvio == idPedidoEnvio select c).First();
        pedido.nfeImpressa = true;
        data.SubmitChanges();
        fillGrid(true);
    }

    protected void lbtEnviarParaInicioOuFimDaFila_Command(object sender, CommandEventArgs e)
    {

        try
        {
            string prioridade = e.CommandArgument.ToString().Split('#')[1];
            int idPedidoEnvio = Convert.ToInt32(e.CommandArgument.ToString().Split('#')[0]);

            using (var data = new dbCommerceDataContext())
            {
                var pedidoEnvio = (from p in data.tbPedidoEnvios where p.idPedidoEnvio == idPedidoEnvio select p).FirstOrDefault();

                if (prioridade == "999")
                {
                    pedidoEnvio.prioridade = 1;
                }
                else
                {
                    pedidoEnvio.prioridade = 999;
                }

                data.SubmitChanges();
            }
        }
        catch (Exception ex)
        {
            Response.Write("<script>alert('Erro: " + ex.Message + "');</script>");
        }

        fillGrid(true);

    }

    protected void grdEmissaoBelle_OnCustomUnboundColumnData(object sender, ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "pedidoIdCliente")
        {
            int pedidoId = Convert.ToInt32(e.GetListSourceFieldValue("pedidoId"));
            e.Value = rnFuncoes.retornaIdCliente(pedidoId);
        }
        if (e.Column.FieldName == "pesoTotal")
        {
            var pedidosDc = new dbCommerceDataContext();
            int idPedidoEnvio = Convert.ToInt32(e.GetListSourceFieldValue("idPedidoEnvio"));
            int pesoTotal = 0;
            try
            {
                pesoTotal = (from c in pedidosDc.tbPedidoPacotes where c.idPedidoEnvio == idPedidoEnvio && (c.rastreio == "" | c.rastreio == null) select c).Sum(x => x.peso);
            }
            catch (Exception)
            {

            }
            e.Value = pesoTotal;
        }
        //if (e.Column.FieldName == "pesoVolumes")
        //{
        //    var pedidosDc = new dbCommerceDataContext();
        //    int idPedidoEnvio = Convert.ToInt32(e.GetListSourceFieldValue("idPedidoEnvio"));
        //    string pesoVolumes = "";
        //    var pesos = (from c in pedidosDc.tbPedidoPacotes where c.idPedidoEnvio == idPedidoEnvio && (c.rastreio == "" | c.rastreio == null) select c);
        //    foreach (var peso in pesos)
        //    {
        //        if (!string.IsNullOrEmpty(pesoVolumes)) pesoVolumes += Environment.NewLine;
        //        pesoVolumes += (Convert.ToDecimal(peso.peso) / 1000);
        //    }
        //    e.Value = pesoVolumes;
        //}
        if (e.Column.FieldName == "nfe")
        {
            int idPedidoEnvio = Convert.ToInt32(e.GetListSourceFieldValue("idPedidoEnvio"));
            int pedidoId = Convert.ToInt32(e.GetListSourceFieldValue("pedidoId"));
            int nfeNumero = Convert.ToInt32(e.GetListSourceFieldValue("nfeNumero"));

            if (nfeNumero > 0)
            {
                e.Value = nfeNumero.ToString();
            }
            else
            {
                e.Value = rnFuncoes.retornaIdCliente(pedidoId).ToString() + idPedidoEnvio.ToString();
            }
        }
        if (e.Column.FieldName == "servicoEnvio")
        {
            var formaDeEnvio = e.GetListSourceFieldValue("formaDeEnvio").ToString();
            if (formaDeEnvio == "jadlogexpressa")
            {
                e.Value = "EXPRESSA";
            }
            else
            {
                e.Value = "Normal";
            }

            var reversa = Convert.ToBoolean(e.GetListSourceFieldValue("gerarReversa"));
            if (reversa)
            {
                e.Value = e.Value + " / REVERSA";
            }
        }
    }
}