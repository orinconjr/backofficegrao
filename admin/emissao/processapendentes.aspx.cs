﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class emissao_processapendentes : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //gravaPendentesJadlog();
            //bool notaAlterada = rnNotaFiscal.checaNotasPendentes();
            if(Request.QueryString["notas"] != null) rnNotaFiscal.emiteNotasCompletas();
            if(Request.QueryString["notas2"] != null) rnNotaFiscal.checaNotasPendentes();
    }
    private void gravaPendentesJadlog()
    {
        var pedidosDc = new dbCommerceDataContext();
        var pendentesJadlog = (from c in pedidosDc.tbPedidoPacotes
                               where (c.codJadlog ?? "") == "" && (c.tbPedidoEnvio.formaDeEnvio == "jadlog" | c.tbPedidoEnvio.formaDeEnvio == "jadlogexpressa") &&
                                   (c.tbPedidoEnvio.nfeObrigatoria == true ? c.tbPedidoEnvio.nfeStatus : 2) == 2
                               select c.tbPedidoEnvio);
        foreach (var pendenteJadlog in pendentesJadlog)
        {
            rnFrete.GravaPedidoWebserviceJadlog(pendenteJadlog.idPedidoEnvio);
        }
    }
}