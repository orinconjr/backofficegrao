﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

public partial class emissao_baixarDanfe : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            var data = new dbCommerceDataContext();
            var empresas = (from c in data.tbEmpresas select c).ToList();
            ddlCnpj.DataSource = empresas;
            ddlCnpj.DataBind();

            ddlCnpj2.DataSource = empresas;
            ddlCnpj2.DataBind();
        }

        if (Request.QueryString["baixarEmLote"] != null)
        {
            baixarEmLote();
        }

        if (Request.QueryString["checarNotasEnviar"] != null)
        {
            rnNotaFiscal.emiteNotasCompletas();
        }

        if (Request.QueryString["reenviarNotasTransp"] != null)
        {
            reenviarNotasTransp(Request.QueryString["reenviarNotasTransp"].ToLower().Trim());
        }
    }

    protected void btnGerarLinkDanfe_Click(object sender, EventArgs e)
    {
        int cnpj = Convert.ToInt32(ddlCnpj.SelectedValue);
        var listaIds = txtNota.Text.Split(',');
        foreach (var id in listaIds)
        {
            int nota = Convert.ToInt32(id);
            var danfe = rnNotaFiscal.gravaDanfeBase64(nota, cnpj, chkAtualizarLinkDanfe.Checked);
            if (danfe)
            {
                litRetorno.Text += "Danfe baixada - ";
            }
            else
            {
                litRetorno.Text += "Erro ao baixar Danfe - ";
            }
        }
    }


    protected void baixarEmLote()
    {
        var pedidosDc = new dbCommerceDataContext();

        var notasUnion = new List<tbNotaFiscal>();

        var pedidosNotaProcessamentoEnvios =
            (from c in pedidosDc.tbPedidoEnvios
             join d in pedidosDc.tbNotaFiscals on c.idPedidoEnvio equals d.idPedidoEnvio
             where (c.nfeStatus == 1)
             select d).ToList();

        var notasProcessamentoGeral = (from c in pedidosDc.tbNotaFiscals where c.statusNfe == 1 select c).ToList();
        notasUnion = pedidosNotaProcessamentoEnvios.Union(notasProcessamentoGeral).ToList();


        foreach (var pedido in notasUnion)
        {
            try
            {
                rnNotaFiscal.gravaDanfeBase64(pedido.numeroNota, (int)pedido.idCNPJ, true);
            }
            catch (Exception)
            {

                throw;
            }


        }
    }

    protected void btnReservarNumeroNota_OnClick(object sender, EventArgs e)
    {
        try
        {
            int idCnpj = Convert.ToInt32(ddlCnpj2.SelectedValue);

            using (var notaDc = new dbCommerceDataContext())
            {
                int numero = 1;
                var numeroNota = (from c in notaDc.tbNotaFiscals where c.idCNPJ == idCnpj orderby c.numeroNota descending select c).FirstOrDefault();
                if (numeroNota != null)
                {
                    numero = (numeroNota.numeroNota + 1);
                }

                var notaNova = new tbNotaFiscal
                {
                    idPedido = 0,
                    numeroNota = numero,
                    dataHora = DateTime.Now,
                    idPedidoEnvio = 0,
                    ultimaChecagem = DateTime.Now,
                    ultimoStatus = "Reserva de numero",
                    idCNPJ = idCnpj
                };

                try
                {
                    notaDc.tbNotaFiscals.InsertOnSubmit(notaNova);
                    notaDc.SubmitChanges();

                    litRetorno2.Text = "num. nota: " + numero;
                }
                catch (Exception ioEx)
                {
                    if (ioEx.Message.IndexOf("nota-duplicada", StringComparison.Ordinal) > -1)
                    {
                        #region Validar Nota  

                        bool notaValida = false;
                        while (!notaValida)
                        {
                            using (var notaDcDuplicada = new dbCommerceDataContext())
                            {
                                numeroNota = (from c in notaDcDuplicada.tbNotaFiscals
                                              where c.idCNPJ == idCnpj
                                              orderby c.numeroNota descending
                                              select c).FirstOrDefault();
                                if (numeroNota != null)
                                {
                                    numero = (numeroNota.numeroNota + 1);
                                }

                                var notaNovaValida = new tbNotaFiscal
                                {
                                    idPedido = 0,
                                    numeroNota = numero,
                                    dataHora = DateTime.Now,
                                    idPedidoEnvio = 0,
                                    ultimaChecagem = DateTime.Now,
                                    ultimoStatus = "Reserva de numero",
                                    idCNPJ = idCnpj
                                };

                                try
                                {
                                    notaDcDuplicada.tbNotaFiscals.InsertOnSubmit(notaNovaValida);
                                    notaDcDuplicada.SubmitChanges();
                                    notaValida = true;
                                    litRetorno2.Text = "num. nota: " + numero;
                                }
                                catch (Exception)
                                {
                                    notaValida = false;
                                }
                            }
                        }
                        #endregion Validar nota
                    }
                }
            }
        }
        catch (Exception ex)
        {

            litRetorno2.Text = ex.Message;
        }


    }

    protected void reenviarNotasTransp(string transportadora)
    {
        try
        {
            using (var data = new dbCommerceDataContext())
            {
                var pendentesTransp =
                  (from c in data.tbPedidoEnvios where c.nfeStatus == 1 && c.formaDeEnvio == transportadora select c).ToList();

                foreach (var pedente in pendentesTransp)
                {
                    pedente.nfeStatus = null;
                    data.SubmitChanges();
                }
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('ERRO: " + ex.Message + "');", true);
        }
       

    }
}