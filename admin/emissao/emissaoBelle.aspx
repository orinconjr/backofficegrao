﻿<%@ Page Language="C#" AutoEventWireup="true" Theme="Glass" CodeFile="emissaoBelle.aspx.cs" Inherits="emissao_emissaoBelle" %>

<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxPopupControl" Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>
<%@ Register TagPrefix="dxwgv" Namespace="DevExpress.Web.ASPxGridView" Assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>BarkCommerce - Sistema Administrativo</title>
    <link href="../admin/estilos/estilos.css" rel="stylesheet" type="text/css" />
    <script language="JavaScript" type="text/javascript" src="../admin/js/funcoes.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <style type="text/css">
        .dxgv {
            vertical-align: top !important;
        }
    </style>
    <asp:Literal runat="server" ID="scriptBlock"></asp:Literal>
    <script type="text/javascript">
        var abreRastreio = function (idPedidoEnvio) {
            poprastreio.Show();
            document.getElementById("hdfRastreio").value = idPedidoEnvio;
            document.getElementById("txtRastreio").value = "";
            document.getElementById("txtRastreio").focus();
            return false;
        }
    </script>
</head>
<body>
    <form id="formulario" runat="server">

        <table align="center" cellpadding="0" cellspacing="0" class="tablePrincipal" style="width: 100%">
            <tr>
                <td>
                    <table cellpadding="0" cellspacing="0" style="width: 920px" align="center">
                        <tr>
                            <td>
                                <table cellpadding="0" cellspacing="0" style="width: 920px">
                                    <tr>
                                        <td style="width: 210px; height: 97px"></td>
                                        <td style="width: 140px; height: 97px"></td>
                                        <td style="width: 189px; height: 97px"></td>
                                        <td style="width: 208px; height: 97px"></td>
                                        <td style="height: 97px"></td>
                                    </tr>
                                    <tr valign="bottom">
                                        <td style="width: 210px">&nbsp;</td>
                                        <td class="textoCabecalho" style="width: 140px"></td>
                                        <td class="textoCabecalho" style="width: 189px"></td>
                                        <td class="textoCabecalho" style="width: 208px"></td>
                                        <td></td>
                                        <td class="textoCabecalho">Seu IP:
                            <asp:Label ID="lblIp" runat="server" Font-Bold="True" Text="200.258.110.123"
                                EnableTheming="False"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height="12"></td>
                        </tr>

                        <tr>
                            <td height="30"></td>
                        </tr>
                        <tr>
                            <td bgcolor="White" style="height: 300px" valign="top">

                                <table cellpadding="0" cellspacing="0" style="width: 920px">
                                    <tr>
                                        <td>

                                            <table align="center" cellpadding="0" cellspacing="0"
                                                style="width: 100%">
                                                <tr>
                                                    <td class="tituloPaginas" valign="top">
                                                        <asp:Label ID="lblAcao" runat="server">Pedidos aguardando Emissão</asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table cellpadding="0" cellspacing="0" style="width: 920px" align="center">

                                                            <tr>
                                                                <td>

                                                                    <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
                                                                        <tr class="rotulos" style="font-weight: bold; font-size: 14px;">
                                                                            <td>Nota fiscal em Processamento
                                                                            </td>
                                                                        </tr>

                                                                        <tr>
                                                                            <td>
                                                                                <dxwgv:ASPxGridView ID="grdNotaProcessamento" runat="server" AutoGenerateColumns="False" Width="834px" Cursor="auto" KeyFieldName="pedidoId"
                                                                                    OnCustomUnboundColumnData="grdNotaProcessamento_OnCustomUnboundColumnData">
                                                                                    <SettingsPager Position="TopAndBottom" PageSize="50" ShowDisabledButtons="False" AlwaysShowPager="True">
                                                                                        <Summary AllPagesText="Páginas: {0} - {1} ({2} items)" Text="Página {0} de {1} ({2} registros encontrados)" />
                                                                                    </SettingsPager>
                                                                                    <Settings ShowFilterRow="True" ShowGroupButtons="False" ShowHeaderFilterButton="True" ShowFooter="True" />
                                                                                    <SettingsEditing EditFormColumnCount="4" PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1" PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter" PopupEditFormWidth="700px" />
                                                                                    <Columns>
                                                                                        <dxwgv:GridViewDataTextColumn Caption="ID" FieldName="pedidoId" VisibleIndex="0" Width="50" Visible="False">
                                                                                        </dxwgv:GridViewDataTextColumn>
                                                                                        <dxwgv:GridViewDataTextColumn Caption="NFE" FieldName="nfeNumero" VisibleIndex="0" Width="50" Visible="True">
                                                                                        </dxwgv:GridViewDataTextColumn>
                                                                                        <dxwgv:GridViewDataTextColumn Caption="Pedido" FieldName="pedidoIdCliente" VisibleIndex="0" Width="50" UnboundType="Integer">
                                                                                        </dxwgv:GridViewDataTextColumn>
                                                                                        <dxwgv:GridViewDataTextColumn Caption="Estado" FieldName="endEstado" VisibleIndex="0" Width="40" Visible="True">
                                                                                        </dxwgv:GridViewDataTextColumn>
                                                                                        <dxwgv:GridViewDataTextColumn Caption="Informação" FieldName="status" VisibleIndex="0">
                                                                                        </dxwgv:GridViewDataTextColumn>
                                                                                        <dxwgv:GridViewDataDateColumn Caption="Data de Emissão" FieldName="dataFimEmbalagem" VisibleIndex="0">
                                                                                            <PropertiesDateEdit DisplayFormatString="g"></PropertiesDateEdit>
                                                                                        </dxwgv:GridViewDataDateColumn>
                                                                                        <dxwgv:GridViewDataTextColumn Caption="Exportar Nota" Name="exportar" VisibleIndex="0" Width="80">
                                                                                            <DataItemTemplate>
                                                                                                <asp:LinkButton runat="server" OnCommand="btnGerarNota_OnCommand" ID="btnGerarNota" CommandArgument='<%# Eval("idPedidoEnvio") %>' EnableViewState="False">Reenviar Nota</asp:LinkButton>
                                                                                            </DataItemTemplate>
                                                                                        </dxwgv:GridViewDataTextColumn>
                                                                                    </Columns>
                                                                                    <StylesEditors>
                                                                                        <Label Font-Bold="True">
                                                                                        </Label>
                                                                                    </StylesEditors>
                                                                                </dxwgv:ASPxGridView>
                                                                            </td>
                                                                        </tr>
                                                                        <tr class="rotulos" style="font-weight: bold; font-size: 14px;">
                                                                            <td>Aguardando Impressão de Nota Fiscal Belle
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <dxwgv:ASPxGridView ID="grdEmissaoBelle" runat="server" AutoGenerateColumns="False" Width="834px" Cursor="auto" KeyFieldName="pedidoId"
                                                                                    OnHtmlRowCreated="gridEmissaoBelle_HtmlRowCreated"
                                                                                    OnCustomUnboundColumnData="grdEmissaoBelle_OnCustomUnboundColumnData">
                                                                                    <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                                                                                    <Styles>
                                                                                        <Footer Font-Bold="True">
                                                                                        </Footer>
                                                                                    </Styles>
                                                                                    <SettingsText ConfirmDelete="Tem certeza que deseja excluir?" EmptyDataRow="Nenhum registro encontrado." />
                                                                                    <SettingsPager Position="TopAndBottom" PageSize="50" ShowDisabledButtons="False" AlwaysShowPager="True">
                                                                                        <Summary AllPagesText="Páginas: {0} - {1} ({2} items)" Text="Página {0} de {1} ({2} registros encontrados)" />
                                                                                    </SettingsPager>
                                                                                    <Settings ShowFilterRow="True" ShowGroupButtons="False" ShowHeaderFilterButton="True" ShowFooter="True" />
                                                                                    <SettingsEditing EditFormColumnCount="4" PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1" PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter" PopupEditFormWidth="700px" />
                                                                                    <Columns>
                                                                                        <dxwgv:GridViewDataTextColumn Caption="Rastreio" Name="rastreio" VisibleIndex="0">
                                                                                            <DataItemTemplate>
                                                                                                <asp:LinkButton runat="server" OnCommand="btnImpresso_OnCommand" ID="btnImpresso" CommandArgument='<%# Eval("idPedidoEnvio") %>' EnableViewState="False">Nota Impressa</asp:LinkButton>
                                                                                            </DataItemTemplate>
                                                                                        </dxwgv:GridViewDataTextColumn>
                                                                                        <dxwgv:GridViewDataTextColumn Caption="ID" FieldName="pedidoId" VisibleIndex="0" Width="50" Visible="False">
                                                                                        </dxwgv:GridViewDataTextColumn>
                                                                                        <dxwgv:GridViewDataTextColumn Caption="ID envio" FieldName="idPedidoEnvio" VisibleIndex="0" Width="50" Visible="False">
                                                                                        </dxwgv:GridViewDataTextColumn>
                                                                                        <dxwgv:GridViewDataTextColumn Caption="NFE" FieldName="nfeNumero" VisibleIndex="0" Width="50" Visible="False">
                                                                                        </dxwgv:GridViewDataTextColumn>
                                                                                        <dxwgv:GridViewDataTextColumn Caption="Pedido" FieldName="pedidoIdCliente" VisibleIndex="0" UnboundType="Integer">
                                                                                        </dxwgv:GridViewDataTextColumn>
                                                                                        <dxwgv:GridViewDataTextColumn Caption="Estado" FieldName="endEstado" VisibleIndex="0" UnboundType="String">
                                                                                        </dxwgv:GridViewDataTextColumn>
                                                                                        <dxwgv:GridViewDataDateColumn Caption="Data de Emissão" FieldName="dataFimEmbalagem" VisibleIndex="0">
                                                                                            <PropertiesDateEdit DisplayFormatString="g"></PropertiesDateEdit>
                                                                                        </dxwgv:GridViewDataDateColumn>
                                                                                        <dxwgv:GridViewDataTextColumn Caption="Nota" FieldName="nfe" VisibleIndex="0" UnboundType="Integer">
                                                                                        </dxwgv:GridViewDataTextColumn>
                                                                                        <dxwgv:GridViewDataTextColumn Caption="Peso Total" FieldName="pesoTotal" VisibleIndex="0" UnboundType="Integer">
                                                                                        </dxwgv:GridViewDataTextColumn>
                                                                                        <dxwgv:GridViewDataTextColumn Caption="Peso Volumes" FieldName="pesoVolumes" VisibleIndex="0" UnboundType="String">
                                                                                        </dxwgv:GridViewDataTextColumn>
                                                                                        <dxwgv:GridViewDataTextColumn Caption="Exportar Nota" Name="exportar" VisibleIndex="0">
                                                                                            <DataItemTemplate>
                                                                                                <asp:HyperLink runat="server" ID="btnExportarNota" Target="_blank">Imprimir Nota Fiscal</asp:HyperLink>
                                                                                            </DataItemTemplate>
                                                                                        </dxwgv:GridViewDataTextColumn>
                                                                                    </Columns>
                                                                                    <StylesEditors>
                                                                                        <Label Font-Bold="True">
                                                                                        </Label>
                                                                                    </StylesEditors>
                                                                                </dxwgv:ASPxGridView>
                                                                                <dx:ASPxPopupControl ID="pcNota" runat="server" CloseAction="CloseButton" Modal="True"
                                                                                    PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="TopSides" ClientInstanceName="pcNota"
                                                                                    HeaderText="Gerar Nota Fiscal" AllowDragging="True" PopupAnimationType="None" EnableViewState="True">
                                                                                    <ContentCollection>
                                                                                        <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server">
                                                                                            <div style="width: 1000px; height: 500px; overflow: scroll;">
                                                                                                <div style="overflow: hidden;">
                                                                                                    <div style="overflow: hidden; margin-bottom: 10px;">
                                                                                                        <div style="float: left; width: 80px;">
                                                                                                            Foto
                                                                                                        </div>
                                                                                                        <div style="float: left; width: 690px; margin-right: 10px;">
                                                                                                            Nome
                                                                                                        </div>
                                                                                                        <div style="float: left; width: 100px;">
                                                                                                            NCM
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <asp:ListView runat="server" ID="lstProdutoNota" OnItemDataBound="lstProdutoNota_DataBound">
                                                                                                        <ItemTemplate>
                                                                                                            <div style="overflow: hidden; margin-bottom: 10px;">
                                                                                                                <div style="float: left; width: 80px;">
                                                                                                                    <asp:HiddenField runat="server" ID="hiddenProdutoId" Value='<%#Eval("produtoId") %>' />
                                                                                                                    <asp:Image ID="fotoDestaque" runat="server" Height="70px" Width="70px" />
                                                                                                                </div>
                                                                                                                <div style="float: left; width: 690px; margin-right: 10px;">
                                                                                                                    <asp:Label ID="lblProdutoNome" runat="server" Text='<%# Bind("produtoNome") %>'></asp:Label>
                                                                                                                </div>
                                                                                                                <div style="float: left; width: 100px;">
                                                                                                                    <asp:TextBox ID="txtncm" runat="server" CssClass="campos" Text='<%# Bind("ncm") %>' Width="90px"></asp:TextBox>
                                                                                                                    <asp:RequiredFieldValidator ID="rqvProdutoNome" runat="server"
                                                                                                                        ControlToValidate="txtncm" Display="None"
                                                                                                                        ErrorMessage="Preencha o NCM." SetFocusOnError="True" ValidationGroup="notaFiscal"></asp:RequiredFieldValidator>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </ItemTemplate>
                                                                                                    </asp:ListView>
                                                                                                </div>
                                                                                                <div style="margin-top: 10px; overflow: hidden; padding-bottom: 20px;">
                                                                                                    <div style="float: left;">
                                                                                                        Cidade:
                                                                                <asp:Literal runat="server" ID="litCidade"></asp:Literal>
                                                                                                        - Estado:
                                                                                <asp:Literal runat="server" ID="litEstado"></asp:Literal><br />
                                                                                                        Código do IBGE:<br />
                                                                                                        <asp:TextBox ID="txtCodigoIbge" runat="server" CssClass="campos" Width="90px"></asp:TextBox>
                                                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtCodigoIbge" Display="None" ErrorMessage="Preencha o código do IBGE." SetFocusOnError="True" ValidationGroup="notaFiscal"></asp:RequiredFieldValidator>
                                                                                                    </div>
                                                                                                    <div style="float: right; margin-right: 15px;">
                                                                                                        <asp:Button runat="server" ID="Button1" OnClick="btnGerarNota_Click" Text="Gerar Nota Fiscal" ValidationGroup="notaFiscal" />
                                                                                                        <asp:ValidationSummary ID="vlds" runat="server" ShowMessageBox="True" ShowSummary="False" DisplayMode="List" ValidationGroup="notaFiscal" />
                                                                                                        <asp:HiddenField runat="server" ID="hdfIdPedidoEnvio" />
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </dx:PopupControlContentControl>
                                                                                    </ContentCollection>
                                                                                    <ContentStyle>
                                                                                        <Paddings PaddingBottom="5px" />
                                                                                    </ContentStyle>
                                                                                </dx:ASPxPopupControl>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>&nbsp;</td>
                                                                        </tr>
                                                                        <tr class="rotulos" style="font-weight: bold; font-size: 14px;">
                                                                            <td>Notas Fiscais Marcadas Como Impressa                                                                              
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <dxwgv:ASPxGridView ID="grdNotasImpressasBelle" runat="server" AutoGenerateColumns="False" Width="834px" Cursor="auto" 
                                                                                    KeyFieldName="pedidoId"
                                                                                   
                                                                                    OnCustomUnboundColumnData="grdEmissaoBelle_OnCustomUnboundColumnData">
                                                                                    <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                                                                                    <Styles>
                                                                                        <Footer Font-Bold="True">
                                                                                        </Footer>
                                                                                    </Styles>
                                                                                    <SettingsText ConfirmDelete="Tem certeza que deseja excluir?" EmptyDataRow="Nenhum registro encontrado." />
                                                                                    <SettingsPager Position="TopAndBottom" PageSize="25" ShowDisabledButtons="False" AlwaysShowPager="True">
                                                                                        <Summary AllPagesText="Páginas: {0} - {1} ({2} items)" Text="Página {0} de {1} ({2} registros encontrados)" />
                                                                                    </SettingsPager>
                                                                                    <Settings ShowFilterRow="True" ShowGroupButtons="False" ShowHeaderFilterButton="True" ShowFooter="True" />
                                                                                    <SettingsEditing EditFormColumnCount="4" PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1" PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter" PopupEditFormWidth="700px" />
                                                                                    <Columns>
                                                                                        <dxwgv:GridViewDataTextColumn Caption="ID" FieldName="pedidoId" VisibleIndex="0" Width="50" Visible="False">
                                                                                        </dxwgv:GridViewDataTextColumn>
                                                                                        <dxwgv:GridViewDataTextColumn Caption="Pedido" FieldName="pedidoIdCliente" VisibleIndex="0" Width="100" UnboundType="Integer">
                                                                                        </dxwgv:GridViewDataTextColumn>
                                                                                        <dxwgv:GridViewDataTextColumn Caption="NFE" FieldName="nfeNumero" VisibleIndex="0" Width="50" Visible="False">
                                                                                        </dxwgv:GridViewDataTextColumn>
                                                                                        <dxwgv:GridViewDataTextColumn Caption="Estado" FieldName="endEstado" VisibleIndex="0" Width="50" UnboundType="String">
                                                                                        </dxwgv:GridViewDataTextColumn>
                                                                                        <dxwgv:GridViewDataDateColumn Caption="Data de Emissão" FieldName="dataFimEmbalagem" Width="110" VisibleIndex="0">
                                                                                            <PropertiesDateEdit DisplayFormatString="g"></PropertiesDateEdit>
                                                                                        </dxwgv:GridViewDataDateColumn>
                                                                                        <dxwgv:GridViewDataTextColumn Caption="Nota" FieldName="nfe" VisibleIndex="0" Width="70" UnboundType="Integer">
                                                                                        </dxwgv:GridViewDataTextColumn>
                                                                                        <dxwgv:GridViewDataTextColumn Caption="Nota Danfe" FieldName="danfe" VisibleIndex="0" UnboundType="Integer">
                                                                                        </dxwgv:GridViewDataTextColumn>
                                                                                    </Columns>
                                                                                    <StylesEditors>
                                                                                        <Label Font-Bold="True">
                                                                                        </Label>
                                                                                    </StylesEditors>
                                                                                </dxwgv:ASPxGridView>
                                                                                
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>&nbsp;</td>
                                                                        </tr>
                                                                        <tr class="rotulos" style="font-weight: bold; font-size: 14px;">
                                                                            <td>Aguardando Carregamento Belle - <a href="http://www.graodegente.com.br/admin/admin/pedidosaguardandocarregamento.aspx" target="_blank">Marcar carregamento</a>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <dxwgv:ASPxGridView ID="grdBelleAguardando" runat="server" AutoGenerateColumns="False" Width="834px" Cursor="auto" KeyFieldName="pedidoId"
                                                                                    OnCustomUnboundColumnData="grdEmissaoBelle_OnCustomUnboundColumnData">
                                                                                    <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                                                                                    <Styles>
                                                                                        <Footer Font-Bold="True">
                                                                                        </Footer>
                                                                                    </Styles>
                                                                                    <SettingsText ConfirmDelete="Tem certeza que deseja excluir?" EmptyDataRow="Nenhum registro encontrado." />
                                                                                    <SettingsPager Position="TopAndBottom" PageSize="50" ShowDisabledButtons="False" AlwaysShowPager="True">
                                                                                        <Summary AllPagesText="Páginas: {0} - {1} ({2} items)" Text="Página {0} de {1} ({2} registros encontrados)" />
                                                                                    </SettingsPager>
                                                                                    <Settings ShowFilterRow="True" ShowGroupButtons="False" ShowHeaderFilterButton="True" ShowFooter="True" />
                                                                                    <SettingsEditing EditFormColumnCount="4" PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1" PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter" PopupEditFormWidth="700px" />
                                                                                    <Columns>
                                                                                        <dxwgv:GridViewDataTextColumn Caption="ID" FieldName="pedidoId" VisibleIndex="0" Width="50" Visible="False">
                                                                                        </dxwgv:GridViewDataTextColumn>
                                                                                        <dxwgv:GridViewDataTextColumn Caption="ID envio" FieldName="idPedidoEnvio" VisibleIndex="0" Width="50" Visible="False">
                                                                                        </dxwgv:GridViewDataTextColumn>
                                                                                        <dxwgv:GridViewDataTextColumn Caption="NFE" FieldName="nfeNumero" VisibleIndex="0" Width="50" Visible="False">
                                                                                        </dxwgv:GridViewDataTextColumn>
                                                                                        <dxwgv:GridViewDataTextColumn Caption="Pedido" FieldName="pedidoIdCliente" VisibleIndex="0" UnboundType="Integer">
                                                                                        </dxwgv:GridViewDataTextColumn>
                                                                                        <dxwgv:GridViewDataTextColumn Caption="Estado" FieldName="endEstado" VisibleIndex="0" UnboundType="String">
                                                                                        </dxwgv:GridViewDataTextColumn>
                                                                                        <dxwgv:GridViewDataDateColumn Caption="Data de Emissão" FieldName="dataFimEmbalagem" VisibleIndex="0">
                                                                                            <PropertiesDateEdit DisplayFormatString="g"></PropertiesDateEdit>
                                                                                        </dxwgv:GridViewDataDateColumn>
                                                                                        <dxwgv:GridViewDataTextColumn Caption="Nota" FieldName="nfe" VisibleIndex="0" UnboundType="Integer">
                                                                                        </dxwgv:GridViewDataTextColumn>
                                                                                        <dxwgv:GridViewDataTextColumn Caption="Peso Total" FieldName="pesoTotal" VisibleIndex="0" Width="50" UnboundType="Integer">
                                                                                        </dxwgv:GridViewDataTextColumn>
                                                                                    </Columns>
                                                                                    <StylesEditors>
                                                                                        <Label Font-Bold="True">
                                                                                        </Label>
                                                                                    </StylesEditors>
                                                                                </dxwgv:ASPxGridView>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>&nbsp;</td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>

                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="height: 176px; background-image: url('../admin/images/rodape.jpg');">
                                                        <table cellpadding="0" cellspacing="0" style="width: 920px">
                                                            <tr>
                                                                <td style="width: 705px; height: 63px"></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 705px">&nbsp;</td>
                                                                <td>
                                                                    <asp:HyperLink ID="btEmail" runat="server"
                                                                        ImageUrl="../admin/images/email.gif" NavigateUrl="mailto:suporte@bark.com.br"></asp:HyperLink>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                            <dx:ASPxPopupControl ID="popRastreio" runat="server" CloseAction="CloseButton" Modal="True"
                                                PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="TopSides" ClientInstanceName="poprastreio"
                                                HeaderText="Rastreio" AllowDragging="True" PopupAnimationType="None" EnableViewState="True">
                                                <ContentCollection>
                                                    <dx:PopupControlContentControl ID="PopupControlContentControl2" runat="server">
                                                        <div style="width: 600px; height: 300px; overflow: scroll;">
                                                            <div style="margin: 15px; overflow: hidden; padding-bottom: 20px;">
                                                                <div>
                                                                    Código de Rastreio:<br />
                                                                    <asp:TextBox ID="txtRastreio" ClientIDMode="Static" runat="server" CssClass="campos" MaxLength="14" Width="130px"></asp:TextBox>

                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtRastreio" Display="None" ErrorMessage="Preencha o código de Rastreio." SetFocusOnError="True" ValidationGroup="rastreio"></asp:RequiredFieldValidator>
                                                                </div>
                                                                <div style="padding-top: 15px;">
                                                                    <asp:Button runat="server" ID="btnGravarRastreio" OnClick="btnGravarRastreio_OnClick" Text="Gravar Rastreio" ValidationGroup="rastreio" />
                                                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True" ShowSummary="False" DisplayMode="List" ValidationGroup="rastreio" />
                                                                    <asp:HiddenField runat="server" ID="hdfRastreio" ClientIDMode="Static" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </dx:PopupControlContentControl>
                                                </ContentCollection>
                                                <ContentStyle>
                                                    <Paddings PaddingBottom="5px" />
                                                </ContentStyle>
                                            </dx:ASPxPopupControl>


                                        </td>
                                    </tr>
                                </table>

                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
