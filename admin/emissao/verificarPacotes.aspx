﻿<%@ Page Language="C#" AutoEventWireup="true" Theme="Glass" CodeFile="verificarPacotes.aspx.cs" Inherits="emissao_verificarPacotes" Culture="pt-BR" UICulture="pt-BR" %>

<%@ Register TagPrefix="dxwgv" Namespace="DevExpress.Web.ASPxGridView" Assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>BarkCommerce - Sistema Administrativo</title>
    <link href="../admin/estilos/estilos.css" rel="stylesheet" type="text/css" />
    <script language="JavaScript" type="text/javascript" src="../admin/js/funcoes.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <table align="center" cellpadding="0" cellspacing="0" class="tablePrincipal" style="width: 100%">
            <tr>
                <td>
                    <table cellpadding="0" cellspacing="0" style="width: 920px" align="center">
                        <tr>
                            <td>
                                <table cellpadding="0" cellspacing="0" style="width: 920px">
                                    <tr>
                                        <td style="width: 210px; height: 97px"></td>
                                        <td style="width: 140px; height: 97px"></td>
                                        <td style="width: 189px; height: 97px"></td>
                                        <td style="width: 208px; height: 97px"></td>
                                        <td style="height: 97px"></td>
                                    </tr>
                                    <tr valign="bottom">
                                        <td style="width: 210px">&nbsp;</td>
                                        <td class="textoCabecalho" style="width: 140px"></td>
                                        <td class="textoCabecalho" style="width: 189px"></td>
                                        <td class="textoCabecalho" style="width: 208px"></td>
                                        <td></td>
                                        <td class="textoCabecalho">Seu IP:
                            <asp:Label ID="lblIp" runat="server" Font-Bold="True" Text="200.258.110.123"
                                EnableTheming="False"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height="12"></td>
                        </tr>

                        <tr>
                            <td height="30"></td>
                        </tr>
                        <tr>
                            <td bgcolor="White" style="height: 300px" valign="top">

                                <table cellpadding="0" cellspacing="0" style="width: 920px">
                                    <tr>
                                        <td>

                                            <table align="center" cellpadding="0" cellspacing="0" style="width: 100%">
                                                <tr>
                                                    <td class="tituloPaginas" valign="top">
                                                        <asp:Label ID="lblAcao" runat="server">Pesquisa de Pacotes</asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table cellpadding="0" cellspacing="0" style="width: 920px" align="center">
                                                            <tr>
                                                                <td>
                                                                    <table align="center" cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td>Pedido ID:
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtPedidoId" MaxLength="12"></asp:TextBox>
                                                                            </td>
                                                                            <td>
                                                                                <asp:Button runat="server" ID="btnPesquisar" Text="Pesquisar" OnClick="btnPesquisar_OnClick" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>

                                                                    <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
                                                                        <tr class="rotulos" style="font-weight: bold; font-size: 14px;">
                                                                            <td>Pacotes Transportadora
                                                                            </td>
                                                                        </tr>

                                                                        <tr>
                                                                            <td>
                                                                                <dxwgv:ASPxGridView ID="grdPacotes" runat="server" AutoGenerateColumns="False" Width="834px" Cursor="auto"
                                                                                    KeyFieldName="idPedidoPacote">
                                                                                    <SettingsPager Position="TopAndBottom" PageSize="50" ShowDisabledButtons="False" AlwaysShowPager="True">
                                                                                        <Summary AllPagesText="Páginas: {0} - {1} ({2} items)" Text="Página {0} de {1} ({2} registros encontrados)" />
                                                                                    </SettingsPager>
                                                                                    <Settings ShowFilterRow="True" ShowGroupButtons="False" ShowHeaderFilterButton="True" ShowFooter="True" />
                                                                                    <SettingsEditing EditFormColumnCount="4" PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1" PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter" PopupEditFormWidth="700px" />
                                                                                    <Columns>
                                                                                        <dxwgv:GridViewDataTextColumn Caption="ID" FieldName="idPedidoPacote" VisibleIndex="0" Width="50">
                                                                                        </dxwgv:GridViewDataTextColumn>
                                                                                        <dxwgv:GridViewDataTextColumn Caption="Id Pedido" FieldName="idPedido" VisibleIndex="0" Width="50">
                                                                                        </dxwgv:GridViewDataTextColumn>
                                                                                        <dxwgv:GridViewDataTextColumn Caption="Peso" FieldName="peso" VisibleIndex="0" Width="50">
                                                                                        </dxwgv:GridViewDataTextColumn>
                                                                                        <dxwgv:GridViewDataTextColumn Caption="Cod Jad" FieldName="codJadlog" VisibleIndex="0" Width="50">
                                                                                        </dxwgv:GridViewDataTextColumn>
                                                                                        <dxwgv:GridViewDataTextColumn Caption="Rastreio" FieldName="rastreio" VisibleIndex="0" Width="150">
                                                                                        </dxwgv:GridViewDataTextColumn>
                                                                                        <dxwgv:GridViewDataTextColumn Caption="Forma de Envio" FieldName="formaDeEnvio" VisibleIndex="0" Width="150">
                                                                                            <DataItemTemplate>
                                                                                                <asp:Label runat="server" Text='<%#Eval("formaDeEnvio") %>' Visible='<%# Eval("formaDeEnvio").ToString() != "" %>'></asp:Label>
                                                                                                <asp:TextBox runat="server" ID="txtFormaDeEnvio" Text='<%#Eval("formaDeEnvio") %>' Visible='<%# Eval("formaDeEnvio").ToString() == "" %>' Width="85" MaxLength="10"></asp:TextBox>
                                                                                                <asp:Button runat="server" Text="Gravar" Visible='<%# Eval("formaDeEnvio").ToString() == "" %>' OnCommand="OnCommand" CommandArgument='<%# Eval("idPedidoPacote")%>' />
                                                                                            </DataItemTemplate>
                                                                                        </dxwgv:GridViewDataTextColumn>
                                                                                        <dxwgv:GridViewDataTextColumn Caption="etiquetaImpressa" FieldName="etiquetaImpressa" VisibleIndex="0">
                                                                                            <DataItemTemplate>
                                                                                                <%#Eval("etiquetaImpressa") %>
                                                                                                <asp:Button runat="server" ID="btnMarcarEtiquetaComoImpressa" Text="Gravar" Visible='<%# !Convert.ToBoolean(Eval("etiquetaImpressa")) %>' OnCommand="btnMarcarEtiquetaComoImpressa_OnCommand" CommandArgument='<%# Eval("idPedidoPacote")%>' />
                                                                                            </DataItemTemplate>
                                                                                        </dxwgv:GridViewDataTextColumn>
                                                                                        <dxwgv:GridViewDataTextColumn Caption="Carregado" FieldName="carregadoCaminhao" VisibleIndex="0" ShowInCustomizationForm="True" Width="160">
                                                                                            <DataItemTemplate>
                                                                                                <asp:Button runat="server" ID="btnInformarCarregamento" Text="carregado" Visible='<%# !Convert.ToBoolean(Eval("foiCarregado")) && Eval("dataDeCarregamento").ToString() != "" %>' OnCommand="btnInformarCarregamento_OnCommand" CommandArgument='<%# Eval("idPedidoPacote")%>' />
                                                                                                <asp:Button runat="server" ID="btnInformarDataCarregamento" Text="data carregado" Visible='<%# Convert.ToBoolean(Eval("foiCarregado")) && Eval("dataDeCarregamento").ToString() == "" %>' OnCommand="btnInformarDataCarregamento_OnCommand" CommandArgument='<%# Eval("idPedidoPacote")%>' OnClientClick="return confirm('Será preenchida a data de carregamento como data de hoje, confirma?');" />
                                                                                                <%# Eval("carregadoCaminhao")%>
                                                                                            </DataItemTemplate>
                                                                                        </dxwgv:GridViewDataTextColumn>
                                                                                        <dxwgv:GridViewDataTextColumn Caption="idPedidoEnvio" FieldName="idPedidoEnvio" VisibleIndex="0">
                                                                                        </dxwgv:GridViewDataTextColumn>
                                                                                        <%--<dxwgv:GridViewDataDateColumn Caption="Data de Emissão" FieldName="dataFimEmbalagem" VisibleIndex="0">
                                                                                            <PropertiesDateEdit DisplayFormatString="g"></PropertiesDateEdit>
                                                                                        </dxwgv:GridViewDataDateColumn>
                                                                                        <dxwgv:GridViewDataTextColumn Caption="Exportar Nota" Name="exportar" VisibleIndex="0" Width="80">
                                                                                            <DataItemTemplate>
                                                                                                <asp:LinkButton runat="server" OnCommand="btnGerarNota_OnCommand" ID="btnGerarNota" CommandArgument='<%# Eval("idPedidoEnvio") %>' EnableViewState="False">Reenviar Nota</asp:LinkButton>
                                                                                            </DataItemTemplate>
                                                                                        </dxwgv:GridViewDataTextColumn>--%>
                                                                                    </Columns>
                                                                                    <StylesEditors>
                                                                                        <Label Font-Bold="True">
                                                                                        </Label>
                                                                                    </StylesEditors>
                                                                                </dxwgv:ASPxGridView>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>&nbsp;</td>
                                                                        </tr>
                                                                        <tr class="rotulos" style="font-weight: bold; font-size: 14px;">
                                                                            <td>Pedido Envio
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <dxwgv:ASPxGridView ID="grdPedidoEnvio" runat="server" AutoGenerateColumns="False" Width="834px" Cursor="auto"
                                                                                    KeyFieldName="idPedidoEnvio">
                                                                                    <SettingsPager Position="TopAndBottom" PageSize="50" ShowDisabledButtons="False" AlwaysShowPager="True">
                                                                                        <Summary AllPagesText="Páginas: {0} - {1} ({2} items)" Text="Página {0} de {1} ({2} registros encontrados)" />
                                                                                    </SettingsPager>
                                                                                    <Settings ShowFilterRow="True" ShowGroupButtons="False" ShowHeaderFilterButton="True" ShowFooter="True" />
                                                                                    <SettingsEditing EditFormColumnCount="4" PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1" PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter" PopupEditFormWidth="700px" />
                                                                                    <Columns>
                                                                                        <dxwgv:GridViewDataTextColumn Caption="ID" FieldName="idPedidoEnvio" VisibleIndex="0" Width="50">
                                                                                        </dxwgv:GridViewDataTextColumn>
                                                                                        <dxwgv:GridViewDataTextColumn Caption="IdPedido" FieldName="idPedido" VisibleIndex="0" Width="50">
                                                                                        </dxwgv:GridViewDataTextColumn>
                                                                                        <dxwgv:GridViewDataTextColumn Caption="idUsuarioSep." FieldName="idUsuarioSeparacao" VisibleIndex="0" Width="50">
                                                                                        </dxwgv:GridViewDataTextColumn>
                                                                                        <dxwgv:GridViewDataTextColumn Caption="IniSeparação" FieldName="dataInicioSeparacao" VisibleIndex="0" Width="50">
                                                                                        </dxwgv:GridViewDataTextColumn>
                                                                                        <dxwgv:GridViewDataTextColumn Caption="FimSeparação" FieldName="dataFimSeparacao" VisibleIndex="0" Width="50">
                                                                                        </dxwgv:GridViewDataTextColumn>
                                                                                        <dxwgv:GridViewDataTextColumn Caption="idUsuarioEmb." FieldName="idUsuarioEmbalagem" VisibleIndex="0" Width="145">
                                                                                            <DataItemTemplate>
                                                                                                <asp:Label runat="server" Text='<%#Eval("idUsuarioEmbalagem") %>' Visible='<%# (Eval("idUsuarioEmbalagem") == null ? "" : Eval("idUsuarioEmbalagem").ToString()) != "" %>'></asp:Label>
                                                                                                <asp:TextBox runat="server" ID="txtUsuarioDeEnvio" Text='<%#Eval("idUsuarioEmbalagem") %>' Visible='<%# (Eval("idUsuarioEmbalagem") == null ? "" : Eval("idUsuarioEmbalagem").ToString()) == "" %>' Width="85" MaxLength="10"></asp:TextBox>
                                                                                                <asp:Button runat="server" Text="Gravar" Visible='<%# (Eval("idUsuarioEmbalagem") == null ? "" : Eval("idUsuarioEmbalagem").ToString()) == "" %>' OnCommand="UsuarioEmbalagemEnvio_OnCommand" CommandArgument='<%# Eval("idPedidoEnvio")%>' />
                                                                                            </DataItemTemplate>
                                                                                        </dxwgv:GridViewDataTextColumn>
                                                                                        <dxwgv:GridViewDataTextColumn Caption="IniEmb." FieldName="dataInicioEmbalagem" VisibleIndex="0" Width="150">
                                                                                        </dxwgv:GridViewDataTextColumn>
                                                                                        <dxwgv:GridViewDataTextColumn Caption="FimEmb." FieldName="dataFimEmbalagem" VisibleIndex="0" Width="150">
                                                                                            <DataItemTemplate>
                                                                                                <asp:Label runat="server" Text='<%#Eval("dataFimEmbalagem") %>' Visible='<%# Eval("dataFimEmbalagem") != null %>'></asp:Label>
                                                                                                <asp:Button runat="server" Text="Fim Emb." Visible='<%# Eval("dataInicioEmbalagem") != null && Eval("dataFimEmbalagem") == null %>' OnCommand="FinalizarEmbalagemUsuario_OnCommand" CommandArgument='<%# Eval("idPedidoEnvio")%>' ToolTip="Finalizar Embalagem" />
                                                                                            </DataItemTemplate>
                                                                                        </dxwgv:GridViewDataTextColumn>
                                                                                        <dxwgv:GridViewDataTextColumn Caption="F.Envio" FieldName="formaDeEnvio" VisibleIndex="0" Width="150">
                                                                                            <DataItemTemplate>
                                                                                                <asp:Label runat="server" Text='<%#Eval("formaDeEnvio") %>' Visible='<%# (Eval("formaDeEnvio") == null ? "" : Eval("formaDeEnvio").ToString()) != "" %>'></asp:Label>
                                                                                                <asp:TextBox runat="server" ID="txtFormaDeEnvio" Text='<%#Eval("formaDeEnvio") %>' Visible='<%# (Eval("formaDeEnvio") == null ? "" : Eval("formaDeEnvio").ToString()) == "" %>' Width="85" MaxLength="10"></asp:TextBox>
                                                                                                <asp:Button runat="server" Text="Gravar" Visible='<%# (Eval("formaDeEnvio") == null ? "" : Eval("formaDeEnvio").ToString()) == "" %>' OnCommand="FormaDeEnvioEnvio_OnCommand" CommandArgument='<%# Eval("idPedidoEnvio")%>' />
                                                                                            </DataItemTemplate>
                                                                                        </dxwgv:GridViewDataTextColumn>
                                                                                        <dxwgv:GridViewDataTextColumn Caption="Volumes" FieldName="volumesEmbalados" VisibleIndex="0" Width="50">
                                                                                        </dxwgv:GridViewDataTextColumn>
                                                                                        <dxwgv:GridViewDataTextColumn Caption="Nfe" FieldName="nfeNumero" VisibleIndex="0" Width="50">
                                                                                        </dxwgv:GridViewDataTextColumn>
                                                                                        <dxwgv:GridViewDataTextColumn Caption="Prat. Separação" FieldName="enderecoSeparacao" VisibleIndex="0" Width="100">
                                                                                        </dxwgv:GridViewDataTextColumn>
                                                                                    </Columns>
                                                                                    <StylesEditors>
                                                                                        <Label Font-Bold="True">
                                                                                        </Label>
                                                                                    </StylesEditors>
                                                                                </dxwgv:ASPxGridView>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>&nbsp;</td>
                                                                        </tr>
                                                                        <tr class="rotulos" style="font-weight: bold; font-size: 14px;">
                                                                            <td>Itens Separados
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <dxwgv:ASPxGridView ID="grdItensSeparados" runat="server" AutoGenerateColumns="False" Width="834px" Cursor="auto"
                                                                                    KeyFieldName="idProdutoEstoque">
                                                                                    <SettingsPager Position="TopAndBottom" PageSize="50" ShowDisabledButtons="False" AlwaysShowPager="True">
                                                                                        <Summary AllPagesText="Páginas: {0} - {1} ({2} items)" Text="Página {0} de {1} ({2} registros encontrados)" />
                                                                                    </SettingsPager>
                                                                                    <Settings ShowFilterRow="True" ShowGroupButtons="False" ShowHeaderFilterButton="True" ShowFooter="True" />
                                                                                    <SettingsEditing EditFormColumnCount="4" PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1" PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter" PopupEditFormWidth="700px" />
                                                                                    <Columns>
                                                                                        <dxwgv:GridViewDataTextColumn Caption="idProdutoEstoque" FieldName="idProdutoEstoque" VisibleIndex="0" Width="50">
                                                                                        </dxwgv:GridViewDataTextColumn>
                                                                                        <dxwgv:GridViewDataTextColumn Caption="Id Pedido" FieldName="pedidoId" VisibleIndex="0" Width="50">
                                                                                        </dxwgv:GridViewDataTextColumn>
                                                                                        <dxwgv:GridViewDataTextColumn Caption="Id Produto" FieldName="produtoId" VisibleIndex="0" Width="50">
                                                                                        </dxwgv:GridViewDataTextColumn>
                                                                                        <dxwgv:GridViewDataTextColumn Caption="Etiqueta" FieldName="idPedidoFornecedorItem" VisibleIndex="0" Width="50">
                                                                                        </dxwgv:GridViewDataTextColumn>
                                                                                        <dxwgv:GridViewDataTextColumn Caption="Enviado" FieldName="enviado" VisibleIndex="0" Width="50">
                                                                                        </dxwgv:GridViewDataTextColumn>
                                                                                        <dxwgv:GridViewDataTextColumn Caption="idPedidoEnvio" FieldName="idPedidoEnvio" VisibleIndex="0" Width="50">
                                                                                        </dxwgv:GridViewDataTextColumn>
                                                                                        <dxwgv:GridViewDataTextColumn Caption="idCentroDistribuicao" FieldName="idCentroDistribuicao" VisibleIndex="0" Width="50">
                                                                                        </dxwgv:GridViewDataTextColumn>
                                                                                        <dxwgv:GridViewDataTextColumn Caption="conf. Emb." FieldName="conferidoEmbalagem" VisibleIndex="0" Width="50">
                                                                                        </dxwgv:GridViewDataTextColumn>
                                                                                    </Columns>
                                                                                    <StylesEditors>
                                                                                        <Label Font-Bold="True">
                                                                                        </Label>
                                                                                    </StylesEditors>
                                                                                </dxwgv:ASPxGridView>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>

                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="tituloPaginas" valign="top">
                                                        <asp:Label ID="lblQuantidadePedidoEmbalar" runat="server">Qtd. Pedidos Aguardando Embalagem</asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="tituloPaginas" valign="top">Proximo pedido a ser embalado (CD1) -
                                                        <asp:Literal runat="server" ID="litProximoPedidoEmbalar"></asp:Literal>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding: 0 0 15px 30px; font-family: Tahoma;">Proximos embalar (CD1)
                                                        <asp:Literal runat="server" ID="litProximosPedidosEmbalar"></asp:Literal>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="tituloPaginas" valign="top">Proximo pedido a ser Separado (CD1) -
                                                        <asp:Literal runat="server" ID="litProximoPedidoSeparar"></asp:Literal>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding: 0 0 15px 30px; font-family: Tahoma;">proximos 10 a separar: 
                                                        <asp:Literal runat="server" ID="litProximos10Separar"></asp:Literal>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="tituloPaginas" valign="top">Qtd. Pedidos Aguardando Embalagem (CD2)
                                                        <asp:Literal runat="server" ID="litQtdPedidosEmbalarCD2"></asp:Literal>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="tituloPaginas" valign="top">Proximo pedido a ser embalado (CD2) -
                                                        <asp:Literal runat="server" ID="litProximoPedidoEmbalarCd2"></asp:Literal>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="height: 176px; background-image: url('../admin/images/rodape.jpg');">
                                                        <table cellpadding="0" cellspacing="0" style="width: 920px">
                                                            <tr>
                                                                <td style="width: 705px; height: 63px"></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 705px">&nbsp;</td>
                                                                <td>
                                                                    <asp:HyperLink ID="btEmail" runat="server"
                                                                        ImageUrl="../admin/images/email.gif" NavigateUrl="mailto:suporte@bark.com.br"></asp:HyperLink>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
