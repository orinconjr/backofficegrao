﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="baixarDanfe.aspx.cs" Inherits="emissao_baixarDanfe" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>BarkCommerce - Sistema Administrativo</title>
    <link rel="stylesheet" type="text/css" href="../admin/estilos/estilos.css">
</head>
<body>
    <form id="form1" runat="server">

        <table cellpadding="0" cellspacing="0" style="width: 100%">
            <tr>
                <td height="1000"
                    style="background-image: url('../admin/images/bkLogin.jpg'); background-repeat: no-repeat; background-position: top"
                    valign="top">
                    <table cellpadding="0" cellspacing="0" style="width: 920px" align="center">
                        <tr>
                            <td style="padding-top: 110px; padding-left: 294px">
                                <table cellpadding="0" cellspacing="0" style="width: 184px; background-color: #001121;">
                                    <tr>
                                        <td>
                                            <span class="textoCabecalho" style="font-weight: bold">
                                                <asp:CheckBox runat="server" ID="chkAtualizarLinkDanfe" Text="Atualizar Link do Danfe" /></span><br />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span class="textoCabecalho" style="font-weight: bold">Empresa:</span><br />
                                            <asp:DropDownList runat="server" ID="ddlCnpj" DataValueField="idEmpresa" DataTextField="nome"></asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span class="textoCabecalho" style="font-weight: bold">Nota:</span><br />
                                            <asp:TextBox ID="txtNota" runat="server" Width="232px" CssClass="campos"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" style="padding-top: 5px">
                                            <asp:Button runat="server" ID="btnGerarLinkDanfe" OnClick="btnGerarLinkDanfe_Click" Text="Baixar Danfe" Width="232" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" style="padding-top: 5px">
                                            <span class="textoCabecalho" style="font-weight: bold">
                                                <asp:Literal runat="server" ID="litRetorno"></asp:Literal></span><br />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <table cellpadding="0" cellspacing="0" style="width: 920px; padding-top: 10px;" align="center">
                        <tr>
                            <td style="padding-left: 294px">

                                <table cellpadding="0" cellspacing="0" style="width: 184px; border-top: 2px solid #72e7fb;">
                                    <tr>
                                        <td>
                                            <span class="textoCabecalho" style="font-weight: bold">Reservar numero de nota Empresa:</span><br />
                                            <asp:DropDownList runat="server" ID="ddlCnpj2" DataValueField="idEmpresa" DataTextField="nome"></asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" style="padding-top: 5px">
                                            <asp:Button runat="server" ID="btnReservarNumeroNota" OnClick="btnReservarNumeroNota_OnClick" Text="Reservar Numero de Nota" Width="232" OnClientClick="return confirm('Reservar numero de nota?');" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" style="text-align: center;">
                                            <span class="textoCabecalho" style="font-weight: bold; font-size: 1em;">
                                                <asp:Literal runat="server" ID="litRetorno2"></asp:Literal></span><br />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>

    </form>
</body>
</html>
