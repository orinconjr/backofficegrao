﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class emissao_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        rnNotaFiscal.emiteNotasCompletasGnre();
    }

    protected void imbEntrar_Click(object sender, ImageClickEventArgs e)
    {
        var usuarioDc = new dbCommerceDataContext();
        if (txtSenha.Text.ToLower() == "emissao")
        {
            HttpCookie usuarioTransportadoraLogado = new HttpCookie("usuarioTransportadoraLogado");
            usuarioTransportadoraLogado.Value = txtSenha.Text.ToLower();
            usuarioTransportadoraLogado.Expires = DateTime.Now.AddMinutes(30);
            Response.Cookies.Add(usuarioTransportadoraLogado);
            usuarioTransportadoraLogado.Expires = DateTime.Now.AddHours(10);
            Response.Redirect("~/emissao/pedidos.aspx");
        }
        else if (txtSenha.Text.ToLower() == "tnt1")
        {
            HttpCookie usuarioTransportadoraLogado = new HttpCookie("usuarioTransportadoraLogadoTnt");
            usuarioTransportadoraLogado.Value = txtSenha.Text.ToLower();
            usuarioTransportadoraLogado.Expires = DateTime.Now.AddHours(10);
            Response.Cookies.Add(usuarioTransportadoraLogado);
            Response.Redirect("~/emissao/emissaotntpar.aspx");
        }
        else if (txtSenha.Text.ToLower() == "tnt2")
        {
            HttpCookie usuarioTransportadoraLogado = new HttpCookie("usuarioTransportadoraLogadoTnt");
            usuarioTransportadoraLogado.Value = txtSenha.Text.ToLower();
            usuarioTransportadoraLogado.Expires = DateTime.Now.AddHours(10);
            Response.Cookies.Add(usuarioTransportadoraLogado);
            Response.Redirect("~/emissao/emissaotntimpar.aspx");
        }
        else if (txtSenha.Text.ToLower() == "tnt")
        {
            HttpCookie usuarioTransportadoraLogado = new HttpCookie("usuarioTransportadoraLogadoTnt");
            usuarioTransportadoraLogado.Value = txtSenha.Text.ToLower();
            usuarioTransportadoraLogado.Expires = DateTime.Now.AddHours(10);
            Response.Cookies.Add(usuarioTransportadoraLogado);
            Response.Redirect("~/emissao/emissaoTNT.aspx");
        }
        else
        {
            Response.Write("<script>alert('Dados inválidos, Tente novamente.');</script>");
        }
    }
}