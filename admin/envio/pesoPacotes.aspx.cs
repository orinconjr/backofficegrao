﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;

public partial class envio_pesoPacotes : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {


            txtPesoPacote.Attributes.Add("onkeypress", "return soNumero(event);");
            txtNumeroPedido.Attributes.Add("onkeypress", "return soNumeroDigitoBarra(event);");
            txtLargura.Attributes.Add("onkeypress", "return soNumero(event);");
            txtAltura.Attributes.Add("onkeypress", "return soNumero(event);");
            txtProfundidade.Attributes.Add("onkeypress", "return soNumero(event);");

            if (!Page.IsPostBack)
            {
                txtNumeroPedido.Focus();
            }

            HttpCookie usuarioExpedicaoLogadoId = new HttpCookie("usuarioExpedicaoLogadoId");
            usuarioExpedicaoLogadoId = Request.Cookies["usuarioExpedicaoLogadoId"];
            if (usuarioExpedicaoLogadoId != null)
            {
                int idUsuarioExpedicao = Convert.ToInt32(usuarioExpedicaoLogadoId.Value);
                var pedidosDc = new dbCommerceDataContext();
                var pedido =
                    (from c in pedidosDc.tbPedidos
                        where c.idUsuarioEmbalado == idUsuarioExpedicao && c.dataEmbaladoFim == null
                        select c).FirstOrDefault();
                if (pedido == null)
                {
                    Response.Redirect("Default.aspx");
                }
                pedidoId.Text = rnFuncoes.retornaIdCliente(pedido.pedidoId).ToString();
                var pacotes = (from c in pedidosDc.tbPedidoPacotes where c.idPedido == pedido.pedidoId && (c.concluido ?? true) == false select c).Count();
                litNumeroPacote.Text = (pacotes + 1).ToString();

                bool compactar = (pedido.compactarPacote != null) != true;
                if (pacotes == pedido.volumesEmbalados && (pedido.formaDeEnvio.ToLower() == "jadlog" | pedido.formaDeEnvio.ToLower() == "jadlogexpressa"))
                {
                    gravaPedidoServiceJadlog(pedido.pedidoId);

                    var pedidosData = new dbCommerceDataContext();
                    var pedidoMod = (from c in pedidosData.tbPedidos
                                     where c.pedidoId == pedido.pedidoId
                                     select c).FirstOrDefault();
                    pedidoMod.dataEmbaladoFim = DateTime.Now;
                    pedidosData.SubmitChanges();
                    Response.Redirect("enviarPor.aspx");
                }
            }
        }
    }

    protected void btnGravarPesoPacote_OnClick(object sender, EventArgs e)
    {
        HttpCookie usuarioExpedicaoLogadoId = new HttpCookie("usuarioExpedicaoLogadoId");
        usuarioExpedicaoLogadoId = Request.Cookies["usuarioExpedicaoLogadoId"];
        if (usuarioExpedicaoLogadoId != null)
        {
            int idUsuarioExpedicao = Convert.ToInt32(usuarioExpedicaoLogadoId.Value);
            var pedidosDc = new dbCommerceDataContext();
            var pedido =
                (from c in pedidosDc.tbPedidos
                 where c.idUsuarioEmbalado == idUsuarioExpedicao && c.dataEmbaladoFim == null
                 select c).FirstOrDefault();
            if (pedido == null)
            {
                Response.Redirect("Default.aspx");
            }
            int pesoPacote = 0;
            int.TryParse(txtPesoPacote.Text, out pesoPacote);
            if (pesoPacote == 0)
            {
                Response.Write("<script>alert('Peso do pacote inválido.');</script>");
                return;
            }

            int numeroVolume = 1;
            int.TryParse(txtNumeroPedido.Text.Split('-')[1].Split('/')[0], out numeroVolume);

            var validaPacote =
                (from c in pedidosDc.tbPedidoPacotes
                 where c.numeroVolume == numeroVolume && c.idPedido == pedido.pedidoId && (c.concluido ?? true) == false
                    select c).Count();

            if (validaPacote == 0)
            {
                var pedidoPacotes = new tbPedidoPacote();
                pedidoPacotes.numeroVolume = numeroVolume;
                pedidoPacotes.idPedido = pedido.pedidoId;
                pedidoPacotes.peso = pesoPacote;
                int largura = 0;
                int altura = 0;
                int profundidade = 0;
                int.TryParse(txtLargura.Text, out largura);
                int.TryParse(txtAltura.Text, out altura);
                int.TryParse(txtProfundidade.Text, out profundidade);

                pedidoPacotes.largura = largura;
                pedidoPacotes.altura = altura;
                pedidoPacotes.profundidade = profundidade;
                pedidoPacotes.rastreio = "";
                pedidoPacotes.concluido = false;
                pedidosDc.tbPedidoPacotes.InsertOnSubmit(pedidoPacotes);
                pedidosDc.SubmitChanges();
            }

            var pacotes = (from c in pedidosDc.tbPedidoPacotes where c.idPedido == pedido.pedidoId && (c.concluido ?? true) == false select c).Count();
            if (pacotes == pedido.volumesEmbalados)
            {
                string entrega = defineEntrega(pedido.pedidoId);
                //pedido.formaDeEnvio = entrega;
                //pedidosDc.SubmitChanges();
                rnPedidos.alteraEstoqueProdutosEnviados(pedido.pedidoId);

                var parciais = (from c in pedidosDc.tbItensPedidoEnvioParcials
                               where
                                   c.idPedido == pedido.pedidoId && c.enviado == false
                               select c);
                var listaProdutos = rnPedidos.retornaProdutosEtiqueta(pedido.pedidoId);
                var itensPedidoDc = new dbCommerceDataContext();

                foreach (var listaProduto in listaProdutos)
                {
                    var parcial =
                        (from c in parciais
                            where c.idItemPedido == listaProduto.itemPedidoId && c.idProduto == listaProduto.produtoId
                            select c).FirstOrDefault();
                    if (parcial != null)
                    {
                        parcial.enviado = true;
                    }
                    var itemPedido =
                        (from c in itensPedidoDc.tbItensPedidos
                            where c.itemPedidoId == listaProduto.itemPedidoId
                            select c).First();
                    itemPedido.enviado = true;
                    itensPedidoDc.SubmitChanges();
                    pedidosDc.SubmitChanges();
                }
                var parciaisPendentes = (from c in pedidosDc.tbItensPedidoEnvioParcials
                    where
                        c.idPedido == pedido.pedidoId && c.enviado == false
                    select c).Any();

                if (entrega.ToLower() == "sedex" | entrega.ToLower() == "pac")
                {
                    //pedido.dataEmbaladoFim = DateTime.Now;
                    var clientesDc = new dbCommerceDataContext();
                    var cliente = (from c in clientesDc.tbClientes where c.clienteId == pedido.clienteId select c).First();
                    if (!parciaisPendentes)
                    {
                        rnPedidos.pedidoAlteraStatus(5, pedido.pedidoId);
                        interacaoInclui("Pedido Enviado", "True", pedido.pedidoId, idUsuarioExpedicao);
                        rnEmails.enviaAlteracaoDeStatus(cliente.clienteNome, pedido.pedidoId.ToString(),
                            "Pedido Enviado", cliente.clienteEmail);
                    }
                    else
                    {
                        rnPedidos.pedidoAlteraStatus(11, pedido.pedidoId);
                        interacaoInclui("Pedido Enviado Parcialmente", "False", pedido.pedidoId, idUsuarioExpedicao);
                        var pedidosData = new dbCommerceDataContext();
                        var pedidoMod = (from c in pedidosData.tbPedidos
                                         where c.pedidoId == pedido.pedidoId
                                         select c).FirstOrDefault();
                        pedidoMod.dataEmbaladoFim = DateTime.Now;
                        if (parciaisPendentes)
                        {
                            pedidoMod.separado = false;
                            pedidoMod.idUsuarioSeparacao = null;
                            pedidoMod.dataInicioSeparacao = null;
                            pedidoMod.dataFimSeparacao = null;
                        }
                        pedidosData.SubmitChanges();
                    }
                    Response.Redirect("enviarPor.aspx");
                }
                else
                {
                    gravaPedidoServiceJadlog(pedido.pedidoId);
                    var clientesDc = new dbCommerceDataContext();
                    var cliente = (from c in clientesDc.tbClientes where c.clienteId == pedido.clienteId select c).First();

                    if (!parciaisPendentes)
                    {
                        rnPedidos.pedidoAlteraStatus(5, pedido.pedidoId);
                        interacaoInclui("Pedido Enviado", "True", pedido.pedidoId, idUsuarioExpedicao);
                        rnEmails.enviaAlteracaoDeStatus(cliente.clienteNome, pedido.pedidoId.ToString(), "Pedido Enviado", cliente.clienteEmail);
                        //rnEmails.enviaAlteracaoDeStatus(cliente.clienteNome, pedido.pedidoId.ToString(), "Pedido Enviado", "andre@bark.com.br");
                    }
                    else
                    {
                        rnPedidos.pedidoAlteraStatus(11, pedido.pedidoId);
                        interacaoInclui("Pedido Enviado Parcialmente", "True", pedido.pedidoId, idUsuarioExpedicao);
                    }
                    var pedidosData = new dbCommerceDataContext();
                    var pedidoMod = (from c in pedidosData.tbPedidos
                                     where c.pedidoId == pedido.pedidoId
                                     select c).FirstOrDefault();
                    pedidoMod.dataEmbaladoFim = DateTime.Now;
                    if (parciaisPendentes)
                    {
                        pedidoMod.separado = false;
                        pedidoMod.idUsuarioSeparacao = null;
                        pedidoMod.dataInicioSeparacao = null;
                        pedidoMod.dataFimSeparacao = null;
                    }
                    int prazo = pedidoMod.prazoSelecionado ?? 1;
                    int prazoDiasUteis = rnFuncoes.retornaPrazoDiasUteis(prazo, 0);
                    var pacotesAlterar =
                        (from c in pedidosData.tbPedidoPacotes
                         where c.idPedido == pedido.pedidoId && (c.concluido ?? true) == false
                         select c);
                    foreach (var pacote in pacotesAlterar)
                    {
                        pacote.concluido = true;
                        pacote.formaDeEnvio = entrega;
                        pacote.statusAtual = "Emissão";
                        pacote.rastreamentoConcluido = false;
                        pacote.prazoFinalEntrega = DateTime.Now.AddDays(prazoDiasUteis);
                    }
                    pedidosData.SubmitChanges();
                    var produtosEstoqueAlterarDc = new dbCommerceDataContext();
                    var produtosEstoque =
                        (from c in produtosEstoqueAlterarDc.tbProdutoEstoques
                            where c.pedidoId == pedido.pedidoId && c.enviado == false
                            select c);
                    foreach (var produtoEstoque in produtosEstoque)
                    {
                        produtoEstoque.enviado = true;
                        produtoEstoque.dataEnvio = DateTime.Now;
                    }
                    produtosEstoqueAlterarDc.SubmitChanges();

                    Response.Redirect("enviarPor.aspx");
                }
            }
            else
            {
                pnCodigoPedido.Visible = true;
                txtNumeroPedido.Text = "";
                txtNumeroPedido.Focus();
                pnPesoPacote.Visible = false;
            }
        }
    }

    private void gravaPedidoServiceJadlog(int pedidoId)
    {
        var pedidoDc = new dbCommerceDataContext();
        var pedido = (from c in pedidoDc.tbPedidos where c.pedidoId == pedidoId select c).First();
        var clientesDc = new dbCommerceDataContext();
        var cliente = (from c in clientesDc.tbClientes where c.clienteId == pedido.clienteId select c).First();
        var volumes = (from c in pedidoDc.tbPedidoPacotes where c.idPedido == pedidoId && (c.concluido ?? true) == false select c);

        int idPedidoCliente = rnFuncoes.retornaIdCliente(pedidoId);

        string CodCliente = "40745";
        string Password = "L2F0M0E1";
        string Remetente = "LINDSAY FERRANDO ME";
        string RemetenteCNPJ = "10924051000163";
        string RemetenteIE = "633459940118";
        string RemetenteEndereco = "AV ANA COSTA 416";
        string RemetenteBairro = "GONZAGA";
        string RemetenteCEP = "11060002";
        string RemetenteTelefone = "01135228379";
        string Destino = "";
        string Destinatario = "";
        string DestinatarioCNPJ = "";
        string DestinatarioIE = "";
        string DestinatarioEndereco = "";
        string DestinatarioBairro = "";
        string DestinatarioCEP = "";
        string DestinatarioTelefone = "";


        if (!string.IsNullOrEmpty(pedido.endNomeDoDestinatario))
        {
            string enderecoCompleto = pedido.endRua + " - " + pedido.endNumero + " - " + pedido.endComplemento;
            Destino = rnFuncoes.removeCaracteresEspeciaisDeixaEspaco(rnFuncoes.limpaStringDeixaEspaco(pedido.endCidade)).ToUpper();
            Destinatario = idPedidoCliente + " " + rnFuncoes.removeCaracteresEspeciaisDeixaEspaco(rnFuncoes.limpaStringDeixaEspaco(pedido.endNomeDoDestinatario)).ToUpper();
            DestinatarioCNPJ = cliente.clienteCPFCNPJ.Replace(".", "").Replace("-", "").Replace("/", "");
            DestinatarioEndereco = rnFuncoes.removeCaracteresEspeciaisDeixaEspaco(rnFuncoes.limpaStringDeixaEspaco(enderecoCompleto)).ToUpper();
            DestinatarioBairro = rnFuncoes.removeCaracteresEspeciaisDeixaEspaco(rnFuncoes.limpaStringDeixaEspaco(pedido.endBairro)).ToUpper();
            DestinatarioCEP = pedido.endCep.Replace("-", "").Replace(" ", "");
            DestinatarioTelefone = cliente.clienteFoneResidencial.Replace(" ", "").Replace("-", "");
        }
        else
        {
            string enderecoCompleto = cliente.clienteRua + " - " + cliente.clienteNumero + " - " + cliente.clienteComplemento;
            Destino = rnFuncoes.removeCaracteresEspeciaisDeixaEspaco(rnFuncoes.limpaStringDeixaEspaco(cliente.clienteCidade)).ToUpper();
            Destinatario = idPedidoCliente + " " + rnFuncoes.removeCaracteresEspeciaisDeixaEspaco(rnFuncoes.limpaStringDeixaEspaco(cliente.clienteNome)).ToUpper();
            DestinatarioCNPJ = cliente.clienteCPFCNPJ.Replace(".", "").Replace("-", "").Replace("/", "");
            DestinatarioEndereco = rnFuncoes.removeCaracteresEspeciaisDeixaEspaco(rnFuncoes.limpaStringDeixaEspaco(enderecoCompleto)).ToUpper();
            DestinatarioBairro = rnFuncoes.removeCaracteresEspeciaisDeixaEspaco(rnFuncoes.limpaStringDeixaEspaco(cliente.clienteBairro));
            DestinatarioCEP = cliente.clienteCep.Replace("-", "").Replace(" ", "");
            DestinatarioTelefone = cliente.clienteFoneResidencial.Replace(" ", "").Replace("-", "");
        }

        //Destinatario = "TESTE DE PEDIDO - NÃO EMITIR";

        string ColetaResponsavel = "";
        string Volumes = pedido.volumesEmbalados.ToString();
        var pesoTotal = (Convert.ToDecimal(volumes.Sum(x => x.peso)) / 1000);
        string PesoReal = pesoTotal.ToString("0.00");
        string Especie = "CONFECCOES";
        string Conteudo = "CONFECCOES";
        string Nr_Pedido = rnFuncoes.retornaIdCliente(pedido.pedidoId).ToString();
        string Nr_NF = "";
        string Danfe = "";
        string Serie_Nf = "";
        string ValorDeclarado = "";
        if (pedido.nfeNumero > 0)
        {
            Nr_NF = pedido.nfeNumero.ToString();
            Danfe = pedido.nfeAccessKey;
            Serie_Nf = "01";
            ValorDeclarado = Convert.ToDecimal(pedido.valorCobrado).ToString("0.00");
        }
        else
        {
            Nr_NF = idPedidoCliente.ToString();
            ValorDeclarado = "100,00";
        }
        string Observacoes = "";
        string Modalidade = "3";
        string wCentroCusto = "";
        string wContaCorrente = "0034802";
        string wTipo = "K";
        string CodUnidade = "795";

        bool erroService = false;

        if (pedido.formaDeEnvio.ToLower() == "jadlogexpressa")
        {
            Modalidade = "0";
        }

        try
        {
            var serviceJad = new serviceJadEnvio.NotfisBeanService();
            var retornoServiceJad = serviceJad.inserir(CodCliente, Password, Remetente, RemetenteCNPJ, RemetenteIE, RemetenteEndereco, RemetenteBairro, RemetenteCEP,
                RemetenteTelefone, Destino, Destinatario, DestinatarioCNPJ, DestinatarioIE, DestinatarioEndereco, DestinatarioBairro, DestinatarioCEP, DestinatarioTelefone,
                ColetaResponsavel, Volumes, PesoReal, Especie, Conteudo, Nr_Pedido, Nr_NF, Danfe, Serie_Nf, ValorDeclarado, Observacoes, Modalidade, wCentroCusto, wContaCorrente,
                wTipo, CodUnidade);

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(retornoServiceJad);
            XmlNodeList parentNode = xmlDoc.GetElementsByTagName("Jadlog_Pedido_eletronico_Inserir");
            foreach (XmlNode childrenNode in parentNode)
            {
                try
                {
                    string retorno = childrenNode.ChildNodes[1].InnerText;
                    if (!String.IsNullOrEmpty(retorno))
                    {
                        if (retorno == "-1" | retorno == "-2" | retorno == "-99")
                        {
                            Response.Write("<script>alert('Falha ao enviar pedido à Jadlog: " + childrenNode.ChildNodes[2].InnerText + ".');</script>");
                            return;
                        }
                        foreach (var pacote in volumes)
                        {
                            var pacoteAlterarDc = new dbCommerceDataContext();
                            var detalhePacote = (from c in pacoteAlterarDc.tbPedidoPacotes where c.idPedidoPacote == pacote.idPedidoPacote select c).First();
                            detalhePacote.codJadlog = retorno;
                            pacoteAlterarDc.SubmitChanges();
                        }
                        //pedido.codPedidoJadlog = retorno;
                    }
                }
                catch (Exception)
                {

                }
            }
        }
        catch (Exception)
        {
            erroService = true;
        }

        if (erroService)
        {
            foreach (var pacote in volumes)
            {
                var pacoteAlterarDc = new dbCommerceDataContext();
                var detalhePacote = (from c in pacoteAlterarDc.tbPedidoPacotes where c.idPedidoPacote == pacote.idPedidoPacote select c).First();
                detalhePacote.codJadlog = "1";
                pacoteAlterarDc.SubmitChanges();
            }
            pedido.pendenteEnvioJadlog = true;
            pedidoDc.SubmitChanges();
        }

    }

    private string  defineEntrega(int pedidoId)
    {
        var pedidoDc = new dbCommerceDataContext();
        var pedido = (from c in pedidoDc.tbPedidos where c.pedidoId == pedidoId select c).First();

        var volumes = (from c in pedidoDc.tbPedidoPacotes where c.idPedido == pedidoId && (c.concluido ?? true) == false select c);
        int pesoTotal = volumes.Sum(x => x.peso);
        decimal pesoEmKg = Convert.ToDecimal(pesoTotal)/1000;
        decimal valorCorreios = 0;
        int prazoCorreios = 0;

        string codigoServicoCorreios = "41106";
        if (pedido.formaDeEnvio == "Sedex")
        {
            codigoServicoCorreios = "40010";
        }

        bool erroCorreios = false;
        foreach (var volume in volumes)
        {
            try
            {
                int largura = volume.largura == null ? 0 : Convert.ToInt32(volume.largura);
                int altura = volume.altura == null ? 0 : Convert.ToInt32(volume.altura);
                int profundidade = volume.profundidade == null ? 0 : Convert.ToInt32(volume.profundidade);

                bool maoPropria = pedido.maoPropria == true ? true : false;
                var consultaCorreios = rnFuncoes.calculaPrecoServiceCorreios(pedido.endCep.Replace("-", "").Replace(" ", ""), "41106", maoPropria, volume.peso, largura, altura, profundidade);
                var valorPacote = consultaCorreios.Tables[0].Rows[0]["Valor"].ToString().Replace(".", ",");
                var prazoPacote = consultaCorreios.Tables[0].Rows[0]["PrazoEntrega"];
                if (Convert.ToInt32(prazoPacote) > prazoCorreios) prazoCorreios = Convert.ToInt32(prazoPacote);
                decimal valor = Convert.ToDecimal(valorPacote);
                if (valor < 1) erroCorreios = true;
                valorCorreios += valor;
            }
            catch (Exception)
            {
                erroCorreios = true;
            }
        }
        if (erroCorreios) valorCorreios = 0;
        string valorNota = Convert.ToDecimal(pedido.valorCobrado).ToString("0.00");
        prazoCorreios++;
        //servico jadlog: Package: 3
        //senha jad: L2F0M0E1
        //seguro: N
        //vVlDec: Valor da Nota
        //vVlColeta: Valor da Coleta
        //vCepOrig: 14910000
        //vCepDest: cep destino
        //vPeso: Peso em kg ex 13,23
        //vFrap: a pagar destino? N
        //vEntrega: D
        //vCnpj: 10924051000163

        bool erroServiceJad = false;
        decimal valorJad = 0;
        int prazoJad = 0;

        try
        {
            int modalidadeJad = 3;
            if (pedido.formaDeEnvioObrigatoria == true && pedido.formaDeEnvio.ToLower() == "jadlogexpressa")
                modalidadeJad = 0;
            var serviceJad = new serviceJadFrete.ValorFreteBeanService();
            var retornoServiceJad = serviceJad.valorar(modalidadeJad, "L2F0M0E1", "N", valorNota, "0,00", "14910000", pedido.endCep.Replace("-", "").Replace(" ", ""), pesoEmKg.ToString("0.00"), "N", "D", "10924051000163");
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(retornoServiceJad);
            XmlNodeList parentNode = xmlDoc.GetElementsByTagName("Jadlog_Valor_Frete");
            foreach (XmlNode childrenNode in parentNode)
            {
                string retorno = childrenNode.ChildNodes[1].InnerText;
                valorJad = Convert.ToDecimal(retorno);
            }
        }
        catch (Exception)
        {
            erroServiceJad = true;
        }

        var faixasDc = new dbCommerceDataContext();
        long cep = Convert.ToInt64(pedido.endCep.Replace("-", ""));
        var faixas = faixasDc.admin_TipoDeEntregaPorFaixaDeCep(cep).Where(x => x.tipodeEntregaId == 9).FirstOrDefault();
        if (faixas != null)
        {
            prazoJad = faixas.prazo + 1;
        }

        if (erroServiceJad)
        {
            valorJad = rnFrete.calculaFreteJadlogPorPeso(pesoTotal, cep);
        }

        pedido.valorCorreios = valorCorreios;
        pedido.prazoCorreios = prazoCorreios;
        pedido.valorJadlog = valorJad;
        pedido.prazoJadlog = prazoJad;

        if (pedido.formaDeEnvioObrigatoria == true | pedido.formaDeEnvio == "Sedex")
        {
            if (pedido.formaDeEnvio.ToLower() == "sedex" | pedido.formaDeEnvio.ToLower() == "pac")
            {
                pedido.valorSelecionado = valorCorreios;
                pedido.prazoSelecionado = prazoCorreios;
                pedidoDc.SubmitChanges();
                return pedido.formaDeEnvio;
            }
            else
            {
                pedido.valorSelecionado = valorJad;
                pedido.prazoSelecionado = prazoJad;
                pedidoDc.SubmitChanges();
                return pedido.formaDeEnvio;
            }
        }
        else if (valorJad > 0 && (valorJad < valorCorreios | valorCorreios == 0))
        {
            if (prazoJad > 0 && prazoCorreios > 0 && valorCorreios > 0 && (prazoCorreios - 2) <= prazoJad)
            {
                decimal diferenca = valorCorreios - valorJad;
                var porcentagemMenor = (valorJad/100)*10;
                if (diferenca <= porcentagemMenor)
                {
                    pedido.valorSelecionado = valorCorreios;
                    pedido.prazoSelecionado = prazoCorreios;
                    pedido.formaDeEnvio = "PAC";
                    pedidoDc.SubmitChanges();
                    return "PAC";
                }
            }
            pedido.valorSelecionado = valorJad;
            pedido.prazoSelecionado = prazoJad;
            pedido.formaDeEnvio = "Jadlog";
            pedidoDc.SubmitChanges();
            return "Jadlog";
        }
        else
        {
            if (prazoJad > 0 && prazoCorreios > 0 && (prazoJad - 2) <= prazoCorreios)
            {
                decimal diferenca = valorJad - valorCorreios;
                var porcentagemMenor = (valorCorreios / 100) * 10;
                if (diferenca <= porcentagemMenor)
                {
                    pedido.valorSelecionado = valorJad;
                    pedido.prazoSelecionado = prazoJad;
                    pedido.formaDeEnvio = "Jadlog";
                    pedidoDc.SubmitChanges();
                    return "Jadlog";
                }
            }

            pedido.valorSelecionado = valorCorreios;
            pedido.prazoSelecionado = prazoCorreios;
            pedido.formaDeEnvio = "PAC";
            pedidoDc.SubmitChanges();
            return "PAC";
        }
        
    }

    protected void btnConfirmar_OnClick(object sender, EventArgs e)
    {
        HttpCookie usuarioExpedicaoLogadoId = new HttpCookie("usuarioExpedicaoLogadoId");
        usuarioExpedicaoLogadoId = Request.Cookies["usuarioExpedicaoLogadoId"];
        if (usuarioExpedicaoLogadoId != null)
        {
            int numeroPedido = 0;
            int.TryParse(txtNumeroPedido.Text.Split('-')[0], out numeroPedido);

            int idUsuarioExpedicao = Convert.ToInt32(usuarioExpedicaoLogadoId.Value);
            var pedidosDc = new dbCommerceDataContext();
            var pedido =
                (from c in pedidosDc.tbPedidos
                    where c.idUsuarioEmbalado == idUsuarioExpedicao && c.dataEmbaladoFim == null
                    select c).FirstOrDefault();
            if (numeroPedido != rnFuncoes.retornaIdCliente(pedido.pedidoId))
            {
                Response.Write("<script>alert('Pedido incorreto. Favor verificar e tentar novamente');</script>");
                txtNumeroPedido.Text = "";
                txtNumeroPedido.Focus();
                return;
            }

            int numeroVolume = 0;
            int.TryParse(txtNumeroPedido.Text.Split('-')[1].Split('/')[0], out numeroVolume);
            int idInterno = rnFuncoes.retornaIdInterno(numeroPedido);

            var pacote =
                (from c in pedidosDc.tbPedidoPacotes
                 where c.idPedido == idInterno && c.numeroVolume == numeroVolume && (c.concluido ?? true) == false
                 select c).FirstOrDefault();
            if (pacote != null | numeroVolume == 0)
            {
                Response.Write("<script>alert('Este volume já foi enviado. Favor verificar e tentar novamente');</script>");
                txtNumeroPedido.Text = "";
                txtNumeroPedido.Focus();
                return;
            }

            pnCodigoPedido.Visible = false;
            pnPesoPacote.Visible = true;
            txtPesoPacote.Text = "";
            txtLargura.Text = "";
            txtAltura.Text = "";
            txtProfundidade.Text = "";
            txtPesoPacote.Focus();
        }
    }


    public void interacaoInclui(string interacao, string alteracaoDeEstatus, int pedidoId, int idUsuarioExpedicao)
    {
        var dataUsuario = new dbCommerceDataContext();
        var usuario =
            (from c in dataUsuario.tbUsuarioExpedicaos where c.idUsuarioExpedicao == idUsuarioExpedicao select c)
                .FirstOrDefault();
        if (usuario != null)
        {
            rnInteracoes.interacaoInclui(pedidoId, interacao, usuario.nome, alteracaoDeEstatus);
        }
    }
}