﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="listaProdutos.aspx.cs" Inherits="envio_listaProdutos" %>


<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>
        <% =ConfigurationManager.AppSettings["nomeDoSite"]%></title>
    <link href="../admin/estilos/estilos.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .style1
        {
            width: 635px;
        }
        .tahoma-16-000000-b
        {
            font-family: Tahoma;
            font-size: 16px;
            color: #000000;
            text-decoration: none;
            font-weight: bold;
            width: 234px;
        }
        .tahoma-14-000000
        {
            font-family: Tahoma;
            font-size: 14px;
            color: #000000;
            text-decoration: none;
        }
        .style2
        {
            font-family: Tahoma;
            font-size: 16px;
            color: #000000;
            text-decoration: none;
            font-weight: bold;
        }
        .style3
        {
            font-family: Tahoma;
            font-size: 14px;
            color: #000000;
            text-decoration: none;
        }
        .style4
        {
            font-family: Tahoma;
            font-size: 16px;
            color: #000000;
            text-decoration: none;
            font-weight: bold;
            width: 73px;
        }
        .style5
        {
            font-family: Tahoma;
            font-size: 14px;
            color: #000000;
            text-decoration: none;
            width: 73px;
        }
        .style12
        {
            font-family: Tahoma;
            font-size: 16px;
            color: #000000;
            text-decoration: none;
            font-weight: bold;
            width: 73px;
        }
        .style13
        {
            font-family: Tahoma;
            font-size: 14px;
            color: #000000;
            text-decoration: none;
            width: 75px;
        }
        .style16
        {
            width: 635px;
        }
        .style15
        {
            font-family: Tahoma;
            font-size: 16px;
            color: #000000;
            text-decoration: none;
        }
        .style16
        {
            width: 635px;
        }
        .style17
        {
            width: 254px;
        }
        .style21
        {
            width: 665px;
        }
        .style26
        {
            font-family: Tahoma;
            font-size: 16px;
            color: #000000;
            text-decoration: none;
            font-weight: bold;
            width: 88px;
        }
        .style27
        {
            font-family: Tahoma;
            font-size: 16px;
            color: #000000;
            text-decoration: none;
            font-weight: bold;
            width: 78px;
        }
        .style28
        {
            font-family: Tahoma;
            font-size: 11px;
            color: #000000;
            width: 153px;
        }
    </style>
    <style type="text/css" media="print">
        .escondeBotao {
            display: none;
        }
    </style>
</head>
<body style="background-color: White">
    <form id="formulario" runat="server">
        <asp:HiddenField runat="server" ID="hdfPedidoId"/>
    <div style="padding-bottom: 40px; padding-top: 20px;" class="escondeBotao">
        <asp:Button runat="server"  Font-Size="18" Text="Impressão Concluída" ID="btnImpressaoConcluida" OnClick="btnImpressaoConcluida_OnClick"/>
    </div>
    <table cellpadding="0" cellspacing="0" width="635">
        <tr>
            <td>
                <table cellpadding="0" cellspacing="0" width="635">
                    <tr>
                        <td>
                            <asp:Image runat="server" ID="imgCodigoDeBarras"/>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold; font-size: 16px;">
                            <span class="style15">Pedido: <asp:Label ID="lblPedidoIdInterno" runat="server"></asp:Label><br/>&nbsp;</span>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold; font-size: 22px;">
                            <span class="style15"><asp:Label ID="lblEndereco" runat="server"></asp:Label><br/>&nbsp;</span>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-size: 12px; padding-top: 10px;">
                            <div style="display: none;">
                                <span class="style15">Cód. produtos: <asp:Label ID="lblCodigosProdutos" runat="server"></asp:Label><br/>&nbsp;</span>
                            </div>
                        </td>
                    </tr>
                    <tr style="display: none;">
                        <td style="font-weight: bold; font-size: 16px; padding-top: 10px;">
                            <span class="style15">Enviar por: <asp:Label ID="lblEnviarPor" runat="server"></asp:Label><br/>&nbsp;</span>
                        </td>
                    </tr>
                    <tr runat="server" id="trCompactar" Visible="False">
                        <td style="font-weight: bold; font-size: 16px; padding-bottom: 10px;">
                            <span class="style15">ATENÇÃO: Compactar Pedido</span>                            
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:DataList ID="dtlInteracao" runat="server" CellPadding="0" DataSourceID="sqlInteracaoes">
                                <ItemTemplate>
                                    <table cellpadding="0" cellspacing="0" class="rotulos" style="width: 782px"><tr>
                                            <td height="25"style="padding-left: 20px; width: 140px; border-top-style: solid; border-top-width: 1px; border-top-color: #7EACB1;">
                                                <asp:Label ID="lblData" runat="server" Text='<%# Bind("interacaoData") %>'></asp:Label>
                                            </td>
                                            <td style="width: 200px; border-top-style: solid; border-top-width: 1px; border-top-color: #7EACB1;">
                                                <asp:Label ID="lblusuario" runat="server" Text='<%# Bind("usuarioQueInteragiu") %>'></asp:Label>
                                            </td>
                                            <td style="border-top-style: solid; border-width: 1px; border-color: #7EACB1">
                                                <asp:Literal ID="ltrInteracao" runat="server" Text='<%# Bind("interacao") %>'></asp:Literal>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                                <HeaderTemplate>
                                    <table cellpadding="0" cellspacing="0" class="rotulos" style="width: 782px">
                                        <tr bgcolor="#D4E3E6">
                                            <td height="25" style="padding-left: 20px; width: 140px;">
                                                <b>Data:</b></td>
                                            <td style="width: 200px">
                                                <b>Usuário:</b></td>
                                            <td>
                                                <b>Interação:</b></td>
                                        </tr>
                                    </table>
                                </HeaderTemplate>
                            </asp:DataList>
                            <asp:ObjectDataSource ID="sqlInteracaoes" runat="server" SelectMethod="interacoSeleciona_PorPedidoId" TypeName="rnInteracoes">
                                <SelectParameters>
                                </SelectParameters>
                            </asp:ObjectDataSource>
                        </td>
                    </tr>
                    <tr>
                        <td class="style16"  style="border: 1px solid #000000;">
                            <span class="style15">
                            &nbsp;<br/>Itens do Pedido:</span><br/>
                            <asp:DataList ID="dtlItensPedido" runat="server" CellPadding="0" 
                            Width="100%" OnItemDataBound="DataList1_ItemDataBound">
                                                        <ItemTemplate>
                                                            <table cellpadding="0" cellspacing="0" class="rotulos" style="width: 100%">
                                                                <tr>
                                                                    <td>
                                                                        <table cellpadding="0" cellspacing="0">
                                                                            <tr>
                                                                                <td class="textoPreto" style="padding-left: 10px">
                                                                                    <b>Códigos: </b><asp:Label ID="lblCodigos" runat="server" Text=''></asp:Label><br />
                                                                                    <asp:HiddenField ID="txtItemPedidoId" runat="server" Value='<%# Bind("itemPedidoId") %>' />
                                                                                    <asp:Label ID="lblProdutoNome" runat="server" Text='<%# Bind("produtoNome") %>'></asp:Label>
                                                                                    <br />
                                                                                    <b>Id da empresa:</b>
                                                                                    <asp:Label ID="lblProdutoIdEmpresa" runat="server" Text='<%# Bind("produtoIdDaEmpresa") %>'></asp:Label>
                                                                                    <br />
                                                                                    <b>Código de Barras:</b>
                                                                                    <asp:Label ID="lblCodigoDeBarras" runat="server" 
                                                                                        Text='<%# Bind("produtoIdDaEmpresa") %>'></asp:Label>
                                                                                    <br />
                                                                                    <b>Fornecedor:</b>
                                                                                    <asp:Label ID="lblFornecedor" runat="server" Text='<%# Bind("fornecedorNome") %>'></asp:Label><br />
                                                                                    <b>Quantidade:</b> <asp:Label ID="lblQuantidade" runat="server" Text='<%# Bind("itemQuantidade") %>'></asp:Label><br/>
                                                                                    <%--<b>Preço de Custo:</b> <asp:Label ID="lblPrecoDeCusto" runat="server"></asp:Label> - 
                                                                                    <b>Preço de Venda:</b> <asp:Label ID="lblPrecoDeVenda" runat="server"></asp:Label> - 
                                                                                    <b>Margem:</b> <asp:Label ID="lblMargem" runat="server"></asp:Label>--%>
                                                                                    <div style="padding-left: 30px;">
                                                                                        <asp:ListView runat="server" ID="lstCombo">
                                                                                            <ItemTemplate><br/>
                                                                                                    <asp:Label ID="lblProdutoNome" runat="server" Text='<%# Bind("produtoNome") %>'></asp:Label>
                                                                                                    <br />
                                                                                                    <b>Id da empresa:</b>
                                                                                                    <asp:Label ID="lblProdutoIdEmpresa" runat="server" 
                                                                                                        Text='<%# Bind("produtoIdDaEmpresa") %>'></asp:Label>
                                                                                                    <br />
                                                                                                    <b>Código de Barras:</b>
                                                                                                    <asp:Label ID="lblCodigoDeBarras" runat="server" 
                                                                                                        Text='<%# Bind("produtoIdDaEmpresa") %>'></asp:Label>
                                                                                                    <br />
                                                                                                    <b>Fornecedor:</b>
                                                                                                    <asp:Label ID="lblFornecedor" runat="server" Text='<%# Bind("fornecedorNome") %>'></asp:Label><br />

                                                                                            </ItemTemplate>
                                                                                        </asp:ListView>
                                                                                   </div>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="6">
                                                                        <hr color="#7EACB1" size="1" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ItemTemplate>
                                                        <HeaderTemplate>
                                                            
                                                        </HeaderTemplate>
                                                    </asp:DataList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            
                                                    
                        <asp:ObjectDataSource ID="sqlItensPedido" runat="server" SelectMethod="itensPedidoSelecionaDesagrupadosAdmin_PorPedidoId" TypeName="rnPedidos">
                        </asp:ObjectDataSource>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                
            </td>
        </tr>
    </table>
    </form>
</body>
</html>

