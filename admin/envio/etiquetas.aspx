﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="etiquetas.aspx.cs" Inherits="envio_etiquetas" %>


<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>
        <% =ConfigurationManager.AppSettings["nomeDoSite"]%></title>
    <link href="../admin/estilos/estilos.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .style1
        {
            width: 635px;
        }
        .tahoma-16-000000-b
        {
            font-family: Tahoma;
            font-size: 16px;
            color: #000000;
            text-decoration: none;
            font-weight: bold;
            width: 234px;
        }
        .tahoma-14-000000
        {
            font-family: Tahoma;
            font-size: 14px;
            color: #000000;
            text-decoration: none;
        }
        .style2
        {
            font-family: Tahoma;
            font-size: 16px;
            color: #000000;
            text-decoration: none;
            font-weight: bold;
        }
        .style3
        {
            font-family: Tahoma;
            font-size: 14px;
            color: #000000;
            text-decoration: none;
        }
        .style4
        {
            font-family: Tahoma;
            font-size: 16px;
            color: #000000;
            text-decoration: none;
            font-weight: bold;
            width: 73px;
        }
        .style5
        {
            font-family: Tahoma;
            font-size: 14px;
            color: #000000;
            text-decoration: none;
            width: 73px;
        }
        .style12
        {
            font-family: Tahoma;
            font-size: 16px;
            color: #000000;
            text-decoration: none;
            font-weight: bold;
            width: 73px;
        }
        .style13
        {
            font-family: Tahoma;
            font-size: 14px;
            color: #000000;
            text-decoration: none;
            width: 75px;
        }
        .style16
        {
            width: 635px;
        }
        .style15
        {
            font-family: Tahoma;
            font-size: 16px;
            color: #000000;
            text-decoration: none;
        }
        .style16
        {
            width: 635px;
        }
        .style17
        {
            width: 254px;
        }
        .style21
        {
            width: 665px;
        }
        .style26
        {
            font-family: Tahoma;
            font-size: 16px;
            color: #000000;
            text-decoration: none;
            font-weight: bold;
            width: 88px;
        }
        .style27
        {
            font-family: Tahoma;
            font-size: 16px;
            color: #000000;
            text-decoration: none;
            font-weight: bold;
            width: 78px;
        }
        .style28
        {
            font-family: Tahoma;
            font-size: 11px;
            color: #000000;
            width: 153px;
        }
    </style>
    <style type="text/css" media="print">
        .escondeBotao {
            display: none;
        }
        .break {
            page-break-after: always;
        }
    </style>
</head>
<body style="background-color: White" onload=window.print();>
    <form id="formulario" runat="server">
        <div style="padding-bottom: 40px; padding-top: 20px;" class="escondeBotao">
            <asp:Button runat="server"  Font-Size="18" Text="Impressão Concluída" ID="btnImpressaoConcluida" OnClick="btnImpressaoConcluida_OnClick"/>
        </div>
        <asp:ListView runat="server" ID="lstEtiqueta" OnItemDataBound="lstEtiqueta_OnItemDataBound">
            <ItemTemplate>
                <table cellpadding="0" cellspacing="0" width="635" class="break">
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" width="635">
                                <tr>
                                    <td style="text-align: center;">
                                        <asp:Image runat="server" ID="imgCodigoDeBarras"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold; font-size: 16px; text-align: center;">
                                        <asp:HiddenField ID="hiddenPedidoId" runat="server" Value='<%# Container.DataItem.ToString() %>'/>
                                        <span class="style15" style="font-size: 26px;"><asp:Label ID="lblPedidoIdInterno" runat="server"></asp:Label>-<%# Container.DataItemIndex + 1 %>/<asp:Literal runat="server" ID="litTotalPacotes"></asp:Literal><br/>&nbsp;</span><br/>
                                    </td>
                                </tr>
                                <tr style="display: none;">
                                    <td style="font-weight: bold; font-size: 16px; padding-top: 15px;">
                                        <span class="style15">Forma de Envio: <asp:Label ID="lblFormaEnvio" runat="server"></asp:Label><br/>&nbsp;</span><br/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="style15">
                                            Tel. Residencial: <asp:Label ID="lblTelResidencial" runat="server"></asp:Label><br/>
                                            Tel. Comercial: <asp:Label ID="lblTelComercial" runat="server"></asp:Label><br/>
                                            Celular: <asp:Label ID="lblTelCelular" runat="server"></asp:Label><br/><br/>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="border: 1px solid #000000; padding-left: 10px;">
                                        <span class="tahoma-16-000000-b">Destinatário:</span>
                                    </td>
                                </tr>
                                <tr >
                                    <td class="style16" valign="top" style="border: 1px solid #000000; border-top: 0;  padding-left: 10px;">
                                        &nbsp;<br/>
                                        <span class="style15"><asp:Label ID="lblNomeDoClienteEntrega1" runat="server"></asp:Label><br />
                                            <asp:Label ID="lblRuaEntrega1" runat="server"></asp:Label> <asp:Label ID="lblNumeroEntrega1" runat="server"></asp:Label>, <asp:Label ID="lblComplementoEntrega1" runat="server"></asp:Label><br />
                                            <asp:Label ID="lblBairroEntrega1" runat="server"></asp:Label> - <asp:Label ID="lblCidadeEntrega1" runat="server"></asp:Label> - <asp:Label ID="lblEstadoEntrega1" runat="server"></asp:Label><br />
                                            <asp:Label ID="lblPaisEntrega1" runat="server" visible="false"></asp:Label>
                                            <asp:Label ID="lblCepEntrega1" runat="server"></asp:Label><br />
                                            <asp:Label ID="lblReferenciaParaEntregaEntrega1" runat="server"></asp:Label></span><br/>&nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;<br/>
                                        &nbsp;<br/>
                                        &nbsp;<br/>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                
            </ItemTemplate>
        </asp:ListView>
    </form>
</body>
</html>

