﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="cancelarJad.aspx.cs" Inherits="envio_cancelarJad" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:ListView runat="server" ID="lstPedidosJad">
            <ItemTemplate>
                Pedido: <%# Eval("pedidoId") %><br/>
                <asp:Button runat="server" ID="btnCancelar" OnCommand="btnCancelar_OnCommand" CommandArgument='<%# Eval("codPedidoJadlog") %>' Text="Cancelar" /><br/><br/>
            </ItemTemplate>
        </asp:ListView>
    </div>
    </form>
</body>
</html>
