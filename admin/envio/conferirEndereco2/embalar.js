﻿var produto = "";
var idUsuario = 0;
var senhaUsuario = "";
var idPedido = 0;
var carregando = false;

function playSound(filename) {
    //document.getElementById("sound").innerHTML = '<audio autoplay="autoplay"><source src="' + filename + '" type="audio/mpeg" /></audio>';
    document.getElementById('alerta').play();
    
}


$(document).ready(function () {
    $(document).on('click', '#btnEntrar', function (e) {
        logar($("#txtSenha").val());
        $.mobile.loading('show', {
            text: 'Carregando',
            textVisible: true,
            theme: 'b',
            html: ""
        });
    });
    $(document).on('keypress', '#txtEnderecoPedido', function (e) {
        if (e.which == 13) {
            $('#btnEnderecarPedido').click();
        }
    });
    $(document).on('click', '#btnProduto', function (e) {
        $.mobile.loading('show', {
            text: 'Carregando',
            textVisible: true,
            theme: 'b',
            html: ""
        });
        enderecarPedido();
    });
    $(document).on('keypress', '#txtSenha', function (e) {
        if (e.which == 13) {
            $('#btnEntrar').click();
        }
    });
    $(document).on('keypress', '#txtProduto', function (e) {
        if (e.which == 13) {
            $('#btnProduto').click();
        }
    });
    $(document).on('click', '#btnMensagemOk', function (e) {
        playSound("");
        $("#popMensagem").popup("close");
    });
});

var etiquetaProduto = "";


function logar(senha) {
    carregando = false;
    var usrService = "../separarPedido/SepararWs.asmx/LogarSeparacao";
    var dados = "{'senha': '" + senha + "'}";
    $.ajax({
        url: usrService,
        data: dados,
        dataType: "json",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataFilter: function (data) { return data; },
        success: function (data) {
            var retorno = data.d;
            if (retorno == "0") {
                alert("Senha incorreta");
                $("#txtSenha").val("");
                $("#txtSenha").focus();
                $.mobile.loading('hide');
            } else {
                var retornoSplit = retorno.split('_');
                idUsuario = parseInt(retornoSplit[0]);
                senha = $("#txtSenha").val();
                $.mobile.changePage("registrar.html", { transition: "slideup" });
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {

        }
    });
}

function enderecarPedido() {
    var enderecoPedido = $("#txtProduto").val();
    if (enderecoPedido == "") {
        $.mobile.loading('hide');
        $("#txtProduto").focus();
        return;
    }
    if (enderecoPedido.indexOf('-') > -1) {
        $("#txtProduto").val("");
        var usrService = "SepararWs.asmx/ConferirEnderecoProdutoRegistrar";
        var dados = "{'endereco': '" + enderecoPedido + "', 'etiqueta': '" + etiquetaProduto + "', 'idusuarioexpedicao': '" + idUsuario + "'}";
        $.ajax({
            url: usrService,
            data: dados,
            dataType: "json",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataFilter: function (data) { return data; },
            success: function (data) {
                etiquetaProduto = "";
                var retorno = data.d;
                $.mobile.loading('hide');
                // Enderecao
                $("#endereco").html(retorno);
                $("#txtProduto").focus();
                setTimeout(function () {
                    $("#txtProduto").focus();
                }, 420); // After 420 ms

            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {

            }
        });
    } else {
        etiquetaProduto = enderecoPedido;
        $("#txtProduto").val("");
        var usrService = "SepararWs.asmx/ConferirEnderecoProduto";
        var dados = "{'etiqueta': '" + enderecoPedido + "', 'idusuarioexpedicao': '" + idUsuario + "'}";
        $.ajax({
            url: usrService,
            data: dados,
            dataType: "json",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataFilter: function (data) { return data; },
            success: function (data) {
                var retorno = data.d;
                $.mobile.loading('hide');
                // Enderecao
                var retornoSplit = retorno.split('|');
                if (retornoSplit[0] == "0") {
                    $("#mensagemPop").text(retornoSplit[1]);
                    $("#popMensagem").popup("open");
                    playSound("alarme.wav");
                    
                }
                console.log(retorno);
                console.log(retornoSplit);
                $("#endereco").html(retornoSplit[1]);
                $("#txtProduto").focus();
                setTimeout(function () {
                    $("#txtProduto").focus();
                }, 420); // After 420 ms

            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {

            }
        });
    }
    
}