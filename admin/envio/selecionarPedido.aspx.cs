﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class envio_selecionarPedido : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        HttpCookie usuarioExpedicaoLogadoId = new HttpCookie("usuarioExpedicaoLogadoId");
        usuarioExpedicaoLogadoId = Request.Cookies["usuarioExpedicaoLogadoId"];
        if (usuarioExpedicaoLogadoId != null)
        {
            var pedidosDc = new dbCommerceDataContext();
            var sendoEmbalados = rnPedidos.verificaPedidosCompletos().Where(x => x.separado).ToList();
            litQuantidadePedidos.Text = sendoEmbalados.Count().ToString();


            int idUsuarioExpedicao = Convert.ToInt32(usuarioExpedicaoLogadoId.Value);
            var hoje = DateTime.Now;
            var pedidosHoje =
                (from c in pedidosDc.tbPedidos
                 where c.dataEmbaladoFim != null && c.idUsuarioEmbalado == idUsuarioExpedicao
                 select c);
            pedidosHoje = pedidosHoje.Where(x => x.dataEmbaladoFim.Value.Date == hoje.Date);
            litEmbaladosHoje.Text = pedidosHoje.Count().ToString();
        }
        else
        {
            Response.Redirect("Default.aspx");
        }
    }

    protected void btnSelecionarPedido_OnClick(object sender, EventArgs e)
    {
        HttpCookie usuarioExpedicaoLogadoId = new HttpCookie("usuarioExpedicaoLogadoId");
        usuarioExpedicaoLogadoId = Request.Cookies["usuarioExpedicaoLogadoId"];
        if (usuarioExpedicaoLogadoId != null)
        {
            int idUsuarioExpedicao = Convert.ToInt32(usuarioExpedicaoLogadoId.Value);
            var pedidosDc = new dbCommerceDataContext();

            var sendoEmbaladoUsuario =
                (from c in pedidosDc.tbPedidos
                    where c.idUsuarioEmbalado == idUsuarioExpedicao && c.dataEmbaladoFim == null
                    select c).Count();
            if (sendoEmbaladoUsuario > 0)
            {
                Response.Redirect("Default.aspx");
            }
            else
            {
                //var sendoEmbalados = pedidosDc.admin_pedidosCompletosSeparacaoEstoque().FirstOrDefault();
                var sendoEmbalados = rnPedidos.verificaPedidosCompletos().Where(x => x.separado).ToList().FirstOrDefault();
                if (sendoEmbalados != null)
                {
                    var pedido = (from c in pedidosDc.tbPedidos where c.pedidoId == sendoEmbalados.pedidoId select c).FirstOrDefault();
                    string enviarPor = determinaEntrega(pedido.pedidoId);
                    bool compactarPacote = compactar(enviarPor);

                    pedido.idUsuarioEmbalado = idUsuarioExpedicao;
                    pedido.dataSendoEmbalado = DateTime.Now;
                    pedido.dataEmbaladoFim = null;
                    pedido.volumesEmbalados = null;

                    pedido.formaDeEnvio = enviarPor;
                    pedido.compactarPacote = compactarPacote;
                    pedido.statusDoPedido = 4;
                    
                    pedidosDc.SubmitChanges();

                    rnPedidos.pedidoAlteraStatus(4, pedido.pedidoId);
                    var clientesDc = new dbCommerceDataContext();
                    var cliente = (from c in clientesDc.tbClientes where c.clienteId == pedido.clienteId select c).First();
                    interacaoInclui("Seu pedido está sendo embalado", "True", pedido.pedidoId, idUsuarioExpedicao);
                    //rnEmails.enviaAlteracaoDeStatus(cliente.clienteNome, pedido.pedidoId.ToString(), "Seu pedido está sendo embalado", cliente.clienteEmail);
                    //rnEmails.enviaAlteracaoDeStatus(cliente.clienteNome, pedido.pedidoId.ToString(), "Seu pedido está sendo embalado", "andre@bark.com.br");
                    Response.Redirect("listaProdutos.aspx");

                }
                else
                {
                    Response.Redirect("selecionarPedido.aspx");
                }
            }
        }
    }
    public void interacaoInclui(string interacao, string alteracaoDeEstatus, int pedidoId, int idUsuarioExpedicao)
    {
        var dataUsuario = new dbCommerceDataContext();
        var usuario =
            (from c in dataUsuario.tbUsuarioExpedicaos where c.idUsuarioExpedicao == idUsuarioExpedicao select c)
                .FirstOrDefault();
        if (usuario != null)
        {
            rnInteracoes.interacaoInclui(pedidoId, interacao, usuario.nome, alteracaoDeEstatus);
        }
    }

    private bool compactar(string tipo)
    {
        if (tipo.ToLower() == "sedex" | tipo.ToLower() == "pac")
        {
            return true;
        }
        return false;
    }
    private string determinaEntrega(int idPedido)
    {
        var pedidoDc = new dbCommerceDataContext();
        var pedido = (from c in pedidoDc.tbPedidos where c.pedidoId == idPedido select c).First();
        string tipo = "";
        if (pedido.tipoDeEntregaId == 5)
        {
            pedido.formaDeEnvioObrigatoria = true;
            pedidoDc.SubmitChanges();
            return "Sedex";
        }
        if (!string.IsNullOrEmpty(pedido.formaDeEnvio))
        {
            pedido.formaDeEnvioObrigatoria = true;
            pedidoDc.SubmitChanges();
            return pedido.formaDeEnvio;
        }

        var itensPedidoDc = new dbCommerceDataContext();
        List<int> produtos = new List<int>();
        produtos.AddRange((from c in itensPedidoDc.tbItensPedidos where c.pedidoId == pedido.pedidoId select c.pedidoId).ToList());
        var produtosDc = new dbCommerceDataContext();
        var produtosCubagem =
            (from c in produtosDc.tbProdutos
             where produtos.Contains(c.produtoId)
             where c.cubagem == "True"
             select c).Any();
        if (produtosCubagem)
        {
            pedido.formaDeEnvioObrigatoria = true;
            pedido.formaDeEnvio = "Jadlog";
            pedidoDc.SubmitChanges();
            return "Jadlog";
        }

        var faixasDc = new dbCommerceDataContext();
        long cep = Convert.ToInt64(pedido.endCep.Replace("-", ""));
        var faixas = faixasDc.admin_TipoDeEntregaPorFaixaDeCep(cep).Where(x => x.tipodeEntregaId == 9).FirstOrDefault();
        if (faixas != null)
        {
            if (faixas.interiorizacao == true)
            {
                return "PAC";
            }
            else
            {
                return "Jadlog";
            }
        }
        else
        {
            return "PAC";
        }
        return tipo;
    }
}