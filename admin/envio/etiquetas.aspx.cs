﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class envio_etiquetas : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        int idPedido = 0;
        int.TryParse(Request.QueryString["idPedido"], out idPedido);

        if (idPedido > 0)
        {
            btnImpressaoConcluida.Visible = false;
        }
        HttpCookie usuarioExpedicaoLogadoId = new HttpCookie("usuarioExpedicaoLogadoId");
        usuarioExpedicaoLogadoId = Request.Cookies["usuarioExpedicaoLogadoId"];
        if (usuarioExpedicaoLogadoId != null | idPedido > 0)
        {
            int idUsuarioExpedicao = 0;
            if(idPedido == 0) idUsuarioExpedicao = Convert.ToInt32(usuarioExpedicaoLogadoId.Value);
            var pedidosDc = new dbCommerceDataContext();
            var pedido =
                (from c in pedidosDc.tbPedidos
                    where (c.idUsuarioEmbalado == idUsuarioExpedicao && c.volumesEmbalados > 0 && c.dataEmbaladoFim == null) | c.pedidoId == idPedido
                    orderby c.dataSendoEmbalado descending 
                    select c).FirstOrDefault();
            if (pedido == null)
            {
                Response.Redirect("Default.aspx");
            }
            else
            {
                List<int> volumes = new List<int>();
                int totalVolumesEmbalados = pedido.volumesEmbalados == null ? 1 : (int)pedido.volumesEmbalados;
                for (int i = 0; i < totalVolumesEmbalados; i++)
                {
                    volumes.Add(pedido.pedidoId);
                }
                lstEtiqueta.DataSource = volumes;
                lstEtiqueta.DataBind();
            }
        }
    }

    protected void lstEtiqueta_OnItemDataBound(object sender, ListViewItemEventArgs e)
    {
        Image imgCodigoDeBarras = (Image)e.Item.FindControl("imgCodigoDeBarras");
        HiddenField hiddenPedidoId = (HiddenField)e.Item.FindControl("hiddenPedidoId");
        Literal litTotalPacotes = (Literal)e.Item.FindControl("litTotalPacotes");
        imgCodigoDeBarras.ImageUrl = string.Format("../BarCode.ashx?code={0}&ShowText={1}",
                                            rnFuncoes.retornaIdCliente(Convert.ToInt32(hiddenPedidoId.Value)).ToString() + "-" + (e.Item.DataItemIndex + 1),
                                            0);

        Label lblPedidoIdInterno = (Label)e.Item.FindControl("lblPedidoIdInterno");
        //Label lblCPF = (Label)e.Item.FindControl("lblCPF");
        Label lblTelResidencial = (Label)e.Item.FindControl("lblTelResidencial");
        Label lblTelComercial = (Label)e.Item.FindControl("lblTelComercial");
        Label lblTelCelular = (Label)e.Item.FindControl("lblTelCelular");
        Label lblNomeDoClienteEntrega1 = (Label)e.Item.FindControl("lblNomeDoClienteEntrega1");
        Label lblRuaEntrega1 = (Label)e.Item.FindControl("lblRuaEntrega1");
        Label lblNumeroEntrega1 = (Label)e.Item.FindControl("lblNumeroEntrega1");
        Label lblComplementoEntrega1 = (Label)e.Item.FindControl("lblComplementoEntrega1");
        Label lblBairroEntrega1 = (Label)e.Item.FindControl("lblBairroEntrega1");
        Label lblCidadeEntrega1 = (Label)e.Item.FindControl("lblCidadeEntrega1");
        Label lblEstadoEntrega1 = (Label)e.Item.FindControl("lblEstadoEntrega1");
        Label lblCepEntrega1 = (Label)e.Item.FindControl("lblCepEntrega1");
        Label lblReferenciaParaEntregaEntrega1 = (Label)e.Item.FindControl("lblReferenciaParaEntregaEntrega1");
        Label lblFormaEnvio = (Label)e.Item.FindControl("lblFormaEnvio");

        var pedidoDc = new dbCommerceDataContext();
        int pedidoId = Convert.ToInt32(hiddenPedidoId.Value.ToString());
        var pedido = (from c in pedidoDc.tbPedidos where c.pedidoId == pedidoId select c).FirstOrDefault();
        lblFormaEnvio.Text = pedido.formaDeEnvio == "Jadlog" ? "Transportadora" : pedido.formaDeEnvio;
        if (pedido.maoPropria == true)
        {
            lblFormaEnvio.Text = lblFormaEnvio.Text + " - Mão Própria";
        }

        litTotalPacotes.Text = pedido.volumesEmbalados.ToString();

        int clienteId = int.Parse(rnPedidos.pedidoSeleciona_PorPedidoId(int.Parse(hiddenPedidoId.Value.ToString())).Tables[0].Rows[0]["clienteId"].ToString());
        lblPedidoIdInterno.Text = rnFuncoes.retornaIdCliente(Convert.ToInt32(hiddenPedidoId.Value.ToString())).ToString();
        //lblCPF.Text = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteCPFCNPJ"].ToString();
        lblTelResidencial.Text = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteFoneResidencial"].ToString();
        lblTelComercial.Text = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteFoneComercial"].ToString();
        lblTelCelular.Text = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteFoneCelular"].ToString();


        if (rnPedidos.pedidoSeleciona_PorPedidoId(int.Parse(hiddenPedidoId.Value.ToString())).Tables[0].Rows[0]["endNomeDoDestinatario"].ToString() != string.Empty)
        {
            lblNomeDoClienteEntrega1.Text = rnPedidos.pedidoSeleciona_PorPedidoId(int.Parse(hiddenPedidoId.Value.ToString())).Tables[0].Rows[0]["endNomeDoDestinatario"].ToString();
            lblRuaEntrega1.Text = rnPedidos.pedidoSeleciona_PorPedidoId(int.Parse(hiddenPedidoId.Value.ToString())).Tables[0].Rows[0]["endRua"].ToString();
            lblNumeroEntrega1.Text = rnPedidos.pedidoSeleciona_PorPedidoId(int.Parse(hiddenPedidoId.Value.ToString())).Tables[0].Rows[0]["endNumero"].ToString();
            lblComplementoEntrega1.Text = rnPedidos.pedidoSeleciona_PorPedidoId(int.Parse(hiddenPedidoId.Value.ToString())).Tables[0].Rows[0]["endComplemento"].ToString();
            lblBairroEntrega1.Text = rnPedidos.pedidoSeleciona_PorPedidoId(int.Parse(hiddenPedidoId.Value.ToString())).Tables[0].Rows[0]["endBairro"].ToString();
            lblCidadeEntrega1.Text = rnPedidos.pedidoSeleciona_PorPedidoId(int.Parse(hiddenPedidoId.Value.ToString())).Tables[0].Rows[0]["endCidade"].ToString();
            lblEstadoEntrega1.Text = rnPedidos.pedidoSeleciona_PorPedidoId(int.Parse(hiddenPedidoId.Value.ToString())).Tables[0].Rows[0]["endEstado"].ToString();
            //lblPaisEntrega1.Text = rnPedidos.pedidoSeleciona_PorPedidoId(int.Parse(lblPedidoId.Text.ToString())).Tables[0].Rows[0]["endPais"].ToString();
            lblCepEntrega1.Text = rnPedidos.pedidoSeleciona_PorPedidoId(int.Parse(hiddenPedidoId.Value.ToString())).Tables[0].Rows[0]["endCep"].ToString();
            lblReferenciaParaEntregaEntrega1.Text = rnPedidos.pedidoSeleciona_PorPedidoId(int.Parse(hiddenPedidoId.Value.ToString())).Tables[0].Rows[0]["endReferenciaParaEntrega"].ToString();
        }
        else
        {
            lblNomeDoClienteEntrega1.Text = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteNome"].ToString();
            lblRuaEntrega1.Text = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteRua"].ToString();
            lblNumeroEntrega1.Text = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteNumero"].ToString();
            lblComplementoEntrega1.Text = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteComplemento"].ToString();
            lblBairroEntrega1.Text = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteBairro"].ToString();
            lblCidadeEntrega1.Text = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteCidade"].ToString();
            lblEstadoEntrega1.Text = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteEstado"].ToString();
            lblCepEntrega1.Text = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteCep"].ToString();
            lblReferenciaParaEntregaEntrega1.Text = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteReferenciaParaEntrega"].ToString();
        }
    }

    protected void btnImpressaoConcluida_OnClick(object sender, EventArgs e)
    {
        Response.Redirect("Default.aspx");
    }
}