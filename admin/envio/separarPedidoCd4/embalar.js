﻿var produto = "";
var idUsuario = 0;
var senhaUsuario = "";
var idPedido = 0;
var carregando = false;

function playSound(filename) {
    document.getElementById("sound").innerHTML = '<audio autoplay="autoplay"><source src="' + filename + '" type="audio/mpeg" /></audio>';
}

$(document).ready(function () {
    $(document).on('click', '#btnEntrar', function (e) {
        logar($("#txtSenha").val());
        $.mobile.loading('show', {
            text: 'Carregando',
            textVisible: true,
            theme: 'b',
            html: ""
        });
    });
    $(document).on('click', '#btnEmbalar', function (e) {
        $.mobile.loading('show', {
            text: 'Carregando',
            textVisible: true,
            theme: 'b',
            html: ""
        });
        SepararPedido();
    });
    $(document).on('click', '#btnProduto', function (e) {
        $.mobile.loading('show', {
            text: 'Carregando',
            textVisible: true,
            theme: 'b',
            html: ""
        });
        verificaProduto();
    });
    $(document).on('keypress', '#txtEnderecoPedido', function (e) {
        if (e.which == 13) {
            $('#btnEnderecarPedido').click();
        }
    });
    $(document).on('click', '#btnEnderecarPedido', function (e) {
        $.mobile.loading('show', {
            text: 'Carregando',
            textVisible: true,
            theme: 'b',
            html: ""
        });
        enderecarPedido();
    });
    $(document).on('keypress', '#txtSenha', function (e) {
        if (e.which == 13) {
            $('#btnEntrar').click();
        }
    });
    $(document).on('keypress', '#txtProduto', function (e) {
        if (e.which == 13) {
            $('#btnProduto').click();
        }
    });
    $(document).on('click', '#btnConfirmarProdutoNao', function (e) {
        produto = "";
        $("#popConfirmarProduto").popup("close");
        setTimeout(function () {
            $("#txtProduto").focus();
        }, 420); // After 420 ms				
    });
    $(document).on('click', '#btnConfirmarProdutoSim', function (e) {
        adicionaProduto();
        $("#popConfirmarProduto").popup("close");
        $.mobile.loading('show', {
            text: 'Carregando',
            textVisible: true,
            theme: 'b',
            html: ""
        });
    });
    $(document).on('click', '#btnProdutoErradoOk', function (e) {
        produto = "";
        $("#popProdutoErrado").popup("close");
    });
    $(document).on('click', '#btnProdutoSeparadoOk', function (e) {
        produto = "";
        $("#popProdutoSeparado").popup("close");
    });
    $(document).on('click', '#btnProdutoSeparadoQuantidadeOk', function (e) {
        produto = "";
        $("#popProdutoSeparadoQuantidade").popup("close");
    });
    $(document).on('click', '#btnProdutoSeparadoOutroOk', function (e) {
        produto = "";
        $("#popProdutoSeparadoOutro").popup("close");
    });
    $(document).on('click', '#btnProdutoEnviadoOutroOk', function (e) {
        produto = "";
        $("#popProdutoEnviadoOutro").popup("close");
    });
    $(document).on('click', '#btnProdutoNaoLocalizadoOk', function (e) {
        produto = "";
        $("#popProdutoNaoEncontrado").popup("close");
    });
    $(document).on('click', '#btnProdutoEspecificoOutroPedidoOk', function (e) {
        produto = "";
        $("#popProdutoEspecificoOutroPedido").popup("close");
    });


});

function logar(senha) {
    carregando = false;
    var usrService = "SepararWs.asmx/LogarSeparacaoCds";
    var dados = "{'senha': '" + senha + "'}";
    $.ajax({
        url: usrService,
        data: dados,
        dataType: "json",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataFilter: function (data) { return data; },
        success: function (data) {
            var retorno = data.d;
            if (retorno == "0") {
                alert("Senha incorreta");
                $("#txtSenha").val("");
                $("#txtSenha").focus();
                $.mobile.loading('hide');
            } else {
                var retornoSplit = retorno.split('_');
                idUsuario = parseInt(retornoSplit[0]);
                senha = $("#txtSenha").val();
                var funcao = retornoSplit[1];
                if (funcao == "enderecar") {
                    alert("Você está endereçando produtos. Favor finalizar antes de iniciar uma separação de pedido.");
                    window.location.href = '../guardarPedido';
                }
                else if (funcao == "1") {
                    carregaEmbalar();
                } else {
                    idPedido = parseInt(funcao);
                    carregaSeparar(true);
                }
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {

        }
    });
}

function SepararPedido() {
    if (!carregando) {
        carregando = true;
        var usrService = "SepararWs.asmx/SepararPedido";
        var dados = "{'idUsuario': '" + idUsuario + "','senha': '" + senhaUsuario + "'}";

        $.ajax({
            url: usrService,
            data: dados,
            dataType: "json",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataFilter: function (data) { return data; },
            success: function (data) {
                carregando = false;
                var retorno = data.d;
                if (retorno == "0") {
                    alert("Não existem pedidos para serem embalados");
                    $.mobile.loading('hide');
                } else {
                    idPedido = parseInt(retorno);
                    carregaSeparar(true);
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                carregando = false;
                alert(errorThrown);
            }
        });
    }
}

function carregaEmbalar() {
    $.mobile.changePage("separar.html", { transition: "slideup" });
    var usrService = "SepararWs.asmx/ContaEmbalarCds";
    var dados = "{'idUsuario': '" + idUsuario + "','senha': '" + senhaUsuario + "'}";
    $.ajax({
        url: usrService,
        data: dados,
        dataType: "json",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataFilter: function (data) { return data; },
        success: function (data) {
            var retorno = data.d;
            $("#quantidadeEmbalar").html(retorno);
            $.mobile.loading('hide');
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert(errorThrown);

        }
    });
}

function carregaSeparar(recarregar) {
    if (recarregar) $.mobile.changePage("registrar.html", { transition: "slideup" });

    var usrService = "SepararWs.asmx/RegistrarCds";
    var dados = "{'idPedido': '" + idPedido + "','idUsuario': '" + idUsuario + "'}";
    $("#numeroPedido").html("Pedido " + idPedido);
    $.ajax({
        url: usrService,
        data: dados,
        dataType: "json",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataFilter: function (data) { return data; },
        success: function (data) {
            var retorno = data.d;

            if (retorno == "enderecar") {
                alert("Pedido separado totalmente. Endereçar a separação.");
                $.mobile.loading('hide');
                $.mobile.changePage("enderecar.html", { transition: "slideup" });
            } else {
                var listaProdutos = "";
                var produtos = JSON.parse(retorno);
                $("#numeroPedido").html("Pedido " + produtos[0].pedidoId);
                for (var i = 0; i < produtos.length; i++) {
                    var produto = produtos[i];
                    if (produto.separado == false) {
                        listaProdutos += "<li rel='" + produto.produtoId + "/" + produto.produtoFoto + ".jpg'>";
                        listaProdutos += "<div style'width: 100%; overflow:auto;'>";
                        listaProdutos += "<b style=\"font-size:26px\">" + produto.codigoProduto + "</b><br>";
                        if (produto.etiqueta != '') {
                            listaProdutos += "<b style=\"font-size:22px\">Etiqueta: " + produto.etiqueta + "</b><br>";
                        }
                        if (produto.area != '') {
                            listaProdutos += "<b>Área: </b>" + produto.area + "<br>";
                        }
                        if (produto.rua != '') {
                            listaProdutos += "<b>Rua: </b>" + produto.rua + "<br>";
                        }
                        if (produto.predio != '') {
                            listaProdutos += "<b>Prédio: </b>" + produto.predio + " (lado " + produto.lado + ")<br>";
                        }
                        if (produto.andar != '') {
                            listaProdutos += "<b>Andar: </b>" + produto.andar + "<br>";
                        }
                        if (produto.apartamento != '') {
                            listaProdutos += "<b>Apartamento: </b>" + produto.apartamento + "<br>";
                        }
                        //if (produto.codigoProduto == '') {
                        listaProdutos += "<b>Produto: </b>" + produto.produtoNome + "<br>";
                        listaProdutos += "<b>ID: </b>" + produto.produtoIdDaEmpresa + "-" + produto.complementoIdDaEmpresa + "<br>";
                        //}
                        listaProdutos += "<b>Fornecedor: </b>" + produto.fornecedorNome + "<br>";
                        //listaProdutos += produto.fornecedorNome + " - " + produto.categoria + "<br>";
                        listaProdutos += "<b>Quantidade: </b>" + produto.quantidade + "<br>";
                        listaProdutos += "<b>Separados: </b>" + produto.separados + "<br>";
                        listaProdutos += "<a href='enderecos.aspx?produto=" + produto.produtoId + "' target='_blank'>Enderecos</a><br>";
                        listaProdutos += "<br>&nbsp;</div>";
                        listaProdutos += "</li>";
                    }
                }
                for (var i = 0; i < produtos.length; i++) {
                    var produto = produtos[i];
                    if (produto.separado == true) {
                        listaProdutos += "<li style='background: #D2E9FF'  rel='" + produto.produtoId + "/" + produto.produtoFoto + ".jpg'>";
                        listaProdutos += "<b style=\"font-size:26px\">" + produto.codigoProduto + "</b><br>";
                        //if (produto.codigoProduto == '') {
                        listaProdutos += "<b>Produto: </b>" + produto.produtoNome + "<br>";
                        listaProdutos += "<b>ID: </b>" + produto.produtoIdDaEmpresa + "-" + produto.complementoIdDaEmpresa + "<br>";
                        //}
                        listaProdutos += "<b>Fornecedor: </b>" + produto.fornecedorNome + "<br>";
                        //listaProdutos += produto.fornecedorNome + " - " + produto.categoria + "<br>";
                        listaProdutos += "<b>Quantidade: </b>" + produto.quantidade + "<br>";
                        listaProdutos += "<b>Separados: </b>" + produto.separados + "<br><br>&nbsp;</div>";
                        listaProdutos += "</li>";
                    }
                }
                $("#listaProdutos").html(listaProdutos);
                $.mobile.loading('hide');
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert(errorThrown);
        }
    });
}


function verificaProduto() {
    produto = $("#txtProduto").val();
    if (produto == "") {
        $.mobile.loading('hide');
        $("#txtProduto").focus();
        return;
    }
    $("#txtProduto").val("");
    var idNumeroPedido = produto;
    var usrService = "SepararWs.asmx/VerificaProdutoV2cds";
    var dados = "{'idNumeroPedido': '" + idNumeroPedido + "','idPedido': '" + idPedido + "','idUsuario': '" + idUsuario + "'}";
    $.ajax({
        url: usrService,
        data: dados,
        dataType: "json",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataFilter: function (data) { return data; },
        success: function (data) {
            var retorno = data.d;
            // Produto não faz parte do pedido
            if (retorno == "0") {
                produto = "";
                $("#popProdutoErrado").popup("open");
                $.mobile.loading('hide');
            }
            //Produto totalmente separado
            if (retorno == "1") {
                produto = "";
                $("#popProdutoSeparado").popup("open");
                carregaSeparar(false);
            }
            //Produto válido, adicionar ao pedido
            if (retorno.lastIndexOf("2_", 0) === 0) {

                if (retorno == "2_0") {
                    $.mobile.loading('hide');
                    alert("Erro ao dicionar o produto");
                    produto = "";
                    setTimeout(function () {
                        $("#txtProduto").focus();
                    }, 420); // After 420 ms
                }
                if (retorno == "2_1") {
                    produto = "";
                    carregaSeparar(false);
                    setTimeout(function () {
                        $("#txtProduto").focus();
                    }, 420); // After 420 ms
                }
                if (retorno == "2_2") {
                    alert("Pedido separado totalmente. Endereçar a separação.");
                    $.mobile.changePage("enderecar.html", { transition: "slideup" });
                }
            }
            // Produto separado, mas falta quantidade
            if (retorno == "3") {
                produto = "";
                $("#popProdutoSeparadoQuantidade").popup("open");
                carregaSeparar(false);
            }
            // Etiqueta faz parte de outro pedido, apenas separado
            if (retorno == "4") {
                produto = "";
                $("#popProdutoSeparadoOutro").popup("open");
                $.mobile.loading('hide');
            }
            // Etiqueta faz parte de outro pedido, enviado
            if (retorno == "5") {
                produto = "";
                $("#popProdutoEnviadoOutro").popup("open");
                $.mobile.loading('hide');
            }
            // Etiqueta não encontrada
            if (retorno == "6") {
                produto = "";
                $("#popProdutoErrado").popup("open");
                $.mobile.loading('hide');
            }
            if (retorno == "7") {
                produto = "";
                $("#popProdutoEspecificoOutroPedido").popup("open");
                $.mobile.loading('hide');
            }

        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert(errorThrown);
        }
    });
}

function adicionaProduto() {
    var idNumeroPedido = produto;
    var usrService = "SepararWs.asmx/AdicionaProdutoCds";
    var dados = "{'idNumeroPedido': '" + idNumeroPedido + "','idPedido': '" + idPedido + "','idUsuario': '" + idUsuario + "'}";
    $.ajax({
        url: usrService,
        data: dados,
        dataType: "json",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataFilter: function (data) { return data; },
        success: function (data) {
            var retorno = data.d;
            if (retorno == "0") {
                $.mobile.loading('hide');
                alert("Erro ao dicionar o produto");
                produto = "";
                setTimeout(function () {
                    $("#txtProduto").focus();
                }, 420); // After 420 ms
            }
            if (retorno == "1") {
                produto = "";
                carregaSeparar(false);
                setTimeout(function () {
                    $("#txtProduto").focus();
                }, 420); // After 420 ms
            }
            if (retorno == "2") {
                alert("Pedido separado totalmente. Endereçar a separação.");
                $.mobile.changePage("enderecar.html", { transition: "slideup" });
            }

        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert(errorThrown);
        }
    });
}



function enderecarPedido() {
    var enderecoPedido = $("#txtEnderecoPedido").val();
    if (enderecoPedido == "") {
        $.mobile.loading('hide');
        $("#txtEnderecoPedido").focus();
        return;
    }
    $("#txtEnderecoPedido").val("");
    var usrService = "SepararWs.asmx/EnderecarPedidoCds";
    var dados = "{'endereco': '" + enderecoPedido + "','idPedido': '" + idPedido + "','idUsuario': '" + idUsuario + "'}";
    $.ajax({
        url: usrService,
        data: dados,
        dataType: "json",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataFilter: function (data) { return data; },
        success: function (data) {
            var retorno = data.d;
            // Enderecao
            if (retorno == "0") {
                alert("Pedido endereçado");
                $.mobile.changePage("index.html", { transition: "slideup" });
            }
            //Produto totalmente separado
            if (retorno == "1") {
                alert("Falha ao endereçar pedido");
                $.mobile.loading('hide');
                $("#txtEnderecoPedido").focus();
            }

        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert(errorThrown);
        }
    });
}