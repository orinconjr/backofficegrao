﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="enderecos.aspx.cs" Inherits="envio_separarPedido_enderecos" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
    <form id="form1" runat="server">
            <div style="font-family: arial;">
                <div>
                    <b style="font-size:16px"> <asp:Literal runat="server" ID="litProdutoNome"></asp:Literal></b><br/>                
                </div>
                <asp:ListView runat="server" ID="listaEnderecos">
                    <ItemTemplate>
                        <div style="padding-bottom: 40px; font-size: 12px; font-family: arial">
                            <b>Área: </b> <%# Eval("area") %><br>
                            <b>Rua: </b> <%# Eval("rua") %><br>
                            <b>Lado: </b> <%# Eval("lado") %><br>
                            <b>Prédio: </b> <%# Eval("predio") %><br>
                            <b>Andar: </b> <%# Eval("andar") %><br/>
                            <b>Código: </b> <%# Eval("codigoProduto") %>
                        </div>
                    </ItemTemplate>
                </asp:ListView>
                <div>
                    <asp:Image runat="server" ID="imgProduto" Width="100%" />
                    <br/>                
                </div>
        </div>
    </form>
</body>
</html>
