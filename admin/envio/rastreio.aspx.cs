﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class envio_rastreio : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            HttpCookie usuarioExpedicaoLogadoId = new HttpCookie("usuarioExpedicaoLogadoId");
            usuarioExpedicaoLogadoId = Request.Cookies["usuarioExpedicaoLogadoId"];
            if (usuarioExpedicaoLogadoId != null)
            {
                int idUsuarioExpedicao = Convert.ToInt32(usuarioExpedicaoLogadoId.Value);
                var pedidosDc = new dbCommerceDataContext();
                var pedido =
                    (from c in pedidosDc.tbPedidos
                        where c.idUsuarioEmbalado == idUsuarioExpedicao && c.dataEmbaladoFim == null
                        select c).FirstOrDefault();
                if (pedido == null)
                {
                    Response.Redirect("Default.aspx");
                }
                litEnviarPor.Text = pedido.formaDeEnvio;
                litPedido.Text = rnFuncoes.retornaIdCliente(pedido.pedidoId).ToString();
                var pacotes =
                    (from c in pedidosDc.tbPedidoPacotes
                     where c.idPedido == pedido.pedidoId && (c.concluido ?? true) == false
                        orderby c.numeroVolume
                        select c);
                lstCodigoRastreio.DataSource = pacotes;
                lstCodigoRastreio.DataBind();
            }
        }
    }

    protected void lstCodigoRastreio_OnItemDataBound(object sender, ListViewItemEventArgs e)
    {
        var txtRastreio = (TextBox)e.Item.FindControl("txtRastreio");
    }

    protected void btnConfirmar_OnClick(object sender, EventArgs e)
    {
        HttpCookie usuarioExpedicaoLogadoId = new HttpCookie("usuarioExpedicaoLogadoId");
        usuarioExpedicaoLogadoId = Request.Cookies["usuarioExpedicaoLogadoId"];
        if (usuarioExpedicaoLogadoId != null)
        {
            int idUsuarioExpedicao = Convert.ToInt32(usuarioExpedicaoLogadoId.Value);
            var pedidosDc = new dbCommerceDataContext();
            var pedido =
                (from c in pedidosDc.tbPedidos
                    where c.idUsuarioEmbalado == idUsuarioExpedicao && c.dataEmbaladoFim == null
                 select c).FirstOrDefault();
            bool todosPreenchidos = true;
            foreach (var item in lstCodigoRastreio.Items)
            {
                var txtRastreio = (TextBox)item.FindControl("txtRastreio");
                var volume = (from c in pedidosDc.tbPedidoPacotes where c.idPedido == pedido.pedidoId && c.numeroVolume == item.DataItemIndex + 1 select c).First();
                volume.rastreio = txtRastreio.Text;
                volume.formaDeEnvio = pedido.formaDeEnvio;
                volume.statusAtual = "Emissão";
                volume.rastreamentoConcluido = false;
                volume.prazoFinalEntrega = DateTime.Now.AddDays(rnFuncoes.retornaPrazoDiasUteis((int)(pedido.prazoSelecionado ?? 1), 0));

                if (string.IsNullOrEmpty(txtRastreio.Text))
                {
                    todosPreenchidos = false;
                }
                pedidosDc.SubmitChanges();
            }
            if(todosPreenchidos) pedido.dataEmbaladoFim = DateTime.Now;
            pedidosDc.SubmitChanges();

            
            var clientesDc = new dbCommerceDataContext();
            var cliente = (from c in clientesDc.tbClientes where c.clienteId == pedido.clienteId select c).First();


            rnEmails.enviaCodigoDoTrakingCorreios(cliente.clienteNome, pedido.pedidoId.ToString(), cliente.clienteEmail);
            var pacotesAlterar =
                        (from c in pedidosDc.tbPedidoPacotes
                         where c.idPedido == pedido.pedidoId && (c.concluido ?? true) == false
                         select c);
            foreach (var pacote in pacotesAlterar)
            {
                pacote.concluido = true;
            }
            pedidosDc.SubmitChanges();


            var produtosEstoqueAlterarDc = new dbCommerceDataContext();
            var produtosEstoque =
                (from c in produtosEstoqueAlterarDc.tbProdutoEstoques
                 where c.pedidoId == pedido.pedidoId && c.enviado == false
                 select c);
            foreach (var produtoEstoque in produtosEstoque)
            {
                produtoEstoque.enviado = true;
                produtoEstoque.dataEnvio = DateTime.Now;
            }
            produtosEstoqueAlterarDc.SubmitChanges();

            try
            {
                if (pedido.marketplace == true && !string.IsNullOrEmpty(pedido.marketplaceId) && pedido.condDePagamentoId == rnIntegracoes.rakutenCondicaoDePagamentoId)
                {
                    rnIntegracoes.atualizaStatusPedidoRakuten(pedido.pedidoId, 5);
                }
                else if (pedido.marketplace == true && !string.IsNullOrEmpty(pedido.marketplaceId) && pedido.condDePagamentoId == rnIntegracoes.extraCondicaoDePagamentoId)
                {
                    rnIntegracoes.atualizaStatusPedidoExtra(pedido.pedidoId, 5);
                }
                else if (pedido.marketplace == true && !string.IsNullOrEmpty(pedido.marketplaceId) && pedido.condDePagamentoId == mlIntegracao.mlCondicaoDePagamentoId)
                {
                    mlIntegracao.atualizaStatusPedidoMl(pedido.pedidoId, 5);
                }
            }
            catch (Exception ex)
            {
                
            }
            

            Response.Write("<script>alert('Código de Rastreio gravado com sucesso.');</script>");
            Response.Write("<script>window.location=('default.aspx');</script>");
        }
        else
        {
            Response.Redirect("Default.aspx");
        }
    }

    protected void lstCodigoRastreio_OnDataBound(object sender, EventArgs e)
    {
        var txtRastreio = (TextBox)lstCodigoRastreio.Items[0].FindControl("txtRastreio");
        txtRastreio.Focus();

        int totalRastreios = lstCodigoRastreio.Items.Count;
        int itemAtual = 0;
        foreach (var item in lstCodigoRastreio.Items)
        {
            if (itemAtual + 1 < totalRastreios)
            {
                var txtRastreio1 = (TextBox)lstCodigoRastreio.Items[itemAtual].FindControl("txtRastreio");
                var txtRastreioProximo = (TextBox)lstCodigoRastreio.Items[itemAtual + 1].FindControl("txtRastreio");
                txtRastreio1.Attributes.Add("onkeypress", "return enterPassaFoco(event, '" + txtRastreioProximo.ClientID + "');");
            }
            itemAtual++;
        }
        


    }
}