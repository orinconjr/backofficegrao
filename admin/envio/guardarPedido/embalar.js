﻿var produto = "";
var idUsuario = 0;
var senhaUsuario = "";
var idPedido = 0;
var carregando = false;
var endereco = "";
var pedidoSeparar = "";


$(document).ready(function () {
    $(document).on('click', '#btnEntrar', function (e) {
        logar($("#txtSenha").val());
        $.mobile.loading('show', {
            text: 'Carregando',
            textVisible: true,
            theme: 'b',
            html: ""
        });
    });
    $(document).on('click', '#btnEmbalar', function (e) {
        $.mobile.loading('show', {
            text: 'Carregando',
            textVisible: true,
            theme: 'b',
            html: ""
        });
        SepararPedido();
    });
    $(document).on('click', '#btnProduto', function (e) {
        $.mobile.loading('show', {
            text: 'Carregando',
            textVisible: true,
            theme: 'b',
            html: ""
        });
        if (produto == "") {
            verificaProduto();
        } else {
            adicionaProduto();
        }
    });
    $(document).on('keypress', '#txtSenha', function (e) {
        if (e.which == 13) {
            $('#btnEntrar').click();
        }
    });
    $(document).on('keypress', '#txtProduto', function (e) {
        if (e.which == 13) {
            $('#btnProduto').click();
        }
    });
    $(document).on('click', '#btnConfirmarProdutoNao', function (e) {
        produto = "";
        $("#popConfirmarProduto").popup("close");
        produto = "";
        $("#txtProduto").val("");
        $("#txtProduto").focus();
        produto = "";
    });
    $(document).on('click', '#btnConfirmarProdutoSim', function (e) {
        $("#popConfirmarProduto").popup("close");
        $("#txtProduto").val("");
        $("#txtProduto").focus();
        $("#lblCampo").html("Endereco");
        $("#btnProduto").html("Endereçar");
        $.mobile.loading('hide');
							
    });
    $(document).on('click', '#btnProdutoErradoOk', function (e) {
        produto = "";			
        $( "#popProdutoErrado" ).popup( "close" );					
    });
    $(document).on('click', '#btnProdutoEnderecadoFalhaEndereco', function (e) {
        $("#popProdutoRegistradoFalhaEndereco").popup("close");
    });
    $(document).on('click', '#btnProdutoEnderecadoFalha', function (e) {
        $("#popProdutoRegistradoFalha").popup("close");
    });
    $(document).on('click', '#btnProdutoEnviadoOk', function (e) {
        produto = "";
        $("#popProdutoEnviado").popup("close");
        produto = "";
        $("#txtProduto").val("");
        $("#txtProduto").focus();
        produto = "";
    });
    $(document).on('click', '#btnProdutoEnderecadoOk', function (e) {
        $("#txtProduto").val("");
        $("#lblCampo").html("Produto");
        $("#btnProduto").html("Carregar Produto");
        $("#popProdutoRegistrado").popup("close");
        $.mobile.loading('hide'); 
        produto = "";
        setTimeout(function () {
            $("#txtProduto").focus();
        }, 420); // After 420 ms
    });
    $(document).on('click', '#page', function (e) {
        setTimeout(function () {
            $("#txtProduto").focus();
        }, 420); // After 420 ms
    });
    $(document).on('keypress', '#btnProdutoEnderecadoOk', function (e) {
        if (e.which == 13) {
            $('#btnProduto').click();
        }
    });

});

var idTransferenciaEndereco = "";

function logar(senha) {
    carregando = false;
    var usrService = "../separarPedido/SepararWs.asmx/LogarSeparacao";
    var dados = "{'senha': '" + senha + "'}";
    $.ajax({
        url: usrService,
        data: dados,
        dataType: "json",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataFilter: function (data) { return data; },
        success: function (data) {
            var retorno = data.d;
            if (retorno == "0") {
                alert("Senha incorreta");
                $("#txtSenha").val("");
                $("#txtSenha").focus();
                $.mobile.loading('hide');
            } else {
                var retornoSplit = retorno.split('_');
                idUsuario = parseInt(retornoSplit[0]);
                senha = $("#txtSenha").val();
                var funcao = retornoSplit[1];
                if (funcao == "enderecar" | funcao == "1") {
                    carregaSeparar(true);
                }
                else {
                    alert("Você está separando um pedido. Favor finalizar antes de iniciar um endereçamento");
                    window.location.href = '../separarPedido';
                }
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {

        }
    });
}

function carregaSeparar(recarregar) {
    if(recarregar) $.mobile.changePage("registrar.html", { transition: "slideup" });
    $("#txtProduto").focus();
    $("#lblCampo").html("Produto");


    var usrService = "../separarPedido/SepararWs.asmx/VerificaEnderecoPendente";
    var dados = "{'idUsuarioExpedicao': '" + idUsuario + "'}";
    $.ajax({
        url: usrService,
        data: dados,
        dataType: "json",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataFilter: function (data) { return data; },
        success: function (data) {
            var retorno = data.d;
            if (retorno == "0") {
                $("#lblCampo").html("Leia o carrinho de transferencia");
                $.mobile.loading('hide');
                idTransferenciaEndereco = "";
                $("#btnProduto").html("Carregar Carrinho");
            } else {
                idTransferenciaEndereco = retorno;
                CarregaListaEnderecamento();
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {

        }
    });
}


function AtribuiCarrinhoUsuario() {
    var endereco = $("#txtProduto").val();
    var usrService = "../separarPedido/SepararWs.asmx/AtribuiCarrinhoUsuario";
    var dados = "{'idEndereco': '" + endereco + "','idUsuarioExpedicao': '" + idUsuario + "'}";
    $.ajax({
        url: usrService,
        data: dados,
        dataType: "json",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataFilter: function (data) { return data; },
        success: function (data) {
            var retorno = data.d;
            if (retorno == "0") {
                alert("Endereço provisório inválido");
                $.mobile.loading('hide');
            } else {
                idTransferenciaEndereco = retorno;
                CarregaListaEnderecamento();
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {

        }
    });
}


function CarregaListaEnderecamento() {
    $("#txtProduto").val("");
    $("#lblCampo").html("Produto");
    $("#btnProduto").html("Carregar Produto");
    produto = "";


    var usrService = "../separarPedido/SepararWs.asmx/CarregaListaEnderecoProvisorio";
    var dados = "{'idUsuario': '" + idUsuario + "'}";
    $.ajax({
        url: usrService,
        data: dados,
        dataType: "json",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataFilter: function (data) { return data; },
        success: function (data) {
            var retorno = data.d;
            var listaProdutos = "";
            var produtos = JSON.parse(retorno);
            if (produtos.length == 0) {
                $.mobile.changePage("index.html", { transition: "slideup" });
            } else {
                $("#totalProdutos").html("Falta endereçar: " + produtos.length + " produtos");
                var totalCarregar = produtos.length;
                if (totalCarregar > 100) totalCarregar = 100;
                for (var i = 0; i < totalCarregar; i++) {
                    var produto = produtos[i];
                    listaProdutos += "<li style='padding-bottom: 10px;'>";
                    listaProdutos += "<div style'width: 100%; overflow:auto;'>";
                    listaProdutos += "<b>Etiqueta: </b>" + produto.idPedidoFornecedorItem + "<br>";
                    listaProdutos += "<b>Produto: </b>" + produto.produtoNome + "<br>";
                    listaProdutos += "<b>Codigo: </b>" + produto.codigoProduto + "<br>";
                    listaProdutos += "</li>";
                }
                $("#listaProdutos").html(listaProdutos);
                $.mobile.loading('hide');
                setTimeout(function() {
                    $("#txtProduto").focus();
                }, 420); // After 420 ms
            }

        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {

        }
    });


    return;
}

function verificaProduto() {
    pedidoSeparar = "";
    if (idTransferenciaEndereco == "") {
        AtribuiCarrinhoUsuario();
    } else {
        produto = $("#txtProduto").val();
        if (produto == "") {
            $.mobile.loading('hide');
            $("#txtProduto").focus();
            return;
        }

        if (produto.indexOf('-') > -1) {
            $.mobile.loading('hide');
            $("#popProdutoErrado").popup("open");
            $("#txtProduto").val("");
            $("#txtProduto").focus();
            produto = "";
            return;
        }

        $("#mensagem").html("");
        $("#mensagemSeparar").hide();

        var usrService = "../separarPedido/SepararWs.asmx/VerificaProdutoEnderecamentoTransferencia";

        var dados = "{'etiqueta': '" + produto + "','idTransferenciaEndereco': '" + idTransferenciaEndereco + "'}";
        $.ajax({
            url: usrService,
            data: dados,
            dataType: "json",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataFilter: function(data) { return data; },
            success: function(data) {
                var retorno = data.d;
                if (retorno == "0") {
                    produto = "";
                    $("#popProdutoEnviado").popup("open");
                    $.mobile.loading('hide');
                    $("#txtProduto").val("");
                    $("#txtProduto").focus();
                    produto = "";
                    return;
                }
                if (retorno == "1") {
                    produto = "";
                    $("#popProdutoEnviado").popup("open");
                    $.mobile.loading('hide');
                    $("#txtProduto").val("");
                    $("#txtProduto").focus();
                    produto = "";
                    return;
                }
                if (retorno.lastIndexOf("2_", 0) === 0) {
                    pedidoSeparar = retorno.split('_')[1];
                    $("#txtProduto").val("");
                    $("#txtProduto").focus();
                    $("#lblCampo").html("Endereco");
                    $("#btnProduto").html("Endereçar");
                    $("#mensagemSeparar").show();
                    $.mobile.loading('hide');
                }
                if (retorno == "3") {
                    $("#txtProduto").val("");
                    $("#txtProduto").focus();
                    $("#lblCampo").html("Endereco");
                    $("#btnProduto").html("Endereçar");
                    $.mobile.loading('hide');
                }
                if (retorno == "4") {
                    produto = "";
                    alert("Produto não faz parte do seu endereçamento");
                    $.mobile.loading('hide');
                    $("#txtProduto").val("");
                    $("#txtProduto").focus();
                    produto = "";
                    return;
                }

                $.mobile.loading('hide');
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {

            }
        });
    }
}

function adicionaProduto() {
    endereco = $("#txtProduto").val();
    if (endereco == "") {
        $("#txtProduto").val("");
        $.mobile.loading('hide');
        $("#txtProduto").focus();
        return;
    }



    var usrService = "../separarPedido/SepararWs.asmx/EnderecarProdutoTransferencia";
    var dados = "{'etiqueta': '" + produto + "', 'endereco': '" + endereco + "','idTransferenciaEndereco': '" + idTransferenciaEndereco + "'}";

    if (pedidoSeparar != "") {
        usrService = "../separarPedido/SepararWs.asmx/EnderecarProdutoTransferenciaPedido";
        dados = "{'etiqueta': '" + produto + "', 'endereco': '" + endereco + "','idTransferenciaEndereco': '" + idTransferenciaEndereco + "','pedido': '" + pedidoSeparar + "'}";
    }

    $.ajax({
        url: usrService,
        data: dados,
        dataType: "json",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataFilter: function (data) { return data; },
        success: function (data) {
            var retorno = data.d;
            if (retorno == "0") {
                $("#popProdutoRegistradoFalha").popup("open");
                $.mobile.loading('hide');
                $("#txtProduto").val("");
                $("#txtProduto").focus();
                return;
            }
            if (retorno == "0_1") {
                $("#popProdutoRegistradoFalhaEndereco").popup("open");
                $.mobile.loading('hide');
                $("#txtProduto").val("");
                $("#txtProduto").focus();
                return;
            }
            if (retorno == "1") {
                $("#txtProduto").val("");
                $("#lblCampo").html("Produto");
                $("#mensagem").html("Produto endereçado com sucesso");
                $("#btnProduto").html("Carregar Produto");
                $("#popProdutoRegistrado").popup("close");
                $("#mensagemSeparar").hide();
                produto = "";
                pedidoSeparar = "";
                setTimeout(function () {
                    $("#txtProduto").focus();
                }, 420); // After 420 ms
                CarregaListaEnderecamento();
                return;
            }
            $.mobile.loading('hide');
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {

        }
    });
}