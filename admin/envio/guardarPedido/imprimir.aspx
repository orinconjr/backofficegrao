﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="imprimir.aspx.cs" Inherits="envio_guardarPedido_imprimir" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,900,500' rel='stylesheet' type='text/css'>
    <style type="text/css">
        .robotoRegular {
            font-family: 'Roboto', sans-serif;
        }
        .robotoBold {
            font-family: 'Roboto', sans-serif;
            font-weight: 700;
        }
        .robotoUltraBold {
            font-family: 'Roboto', sans-serif;
            font-weight: 900;
        }
        @page{
margin-left: 0px;
margin-right: 0px;
margin-top: 0px;
margin-bottom: 0px;
}
        @media print
{    
    .no-print, .no-print *
    {
        display: none !important;
    }
}
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div class="no-print">
        <asp:Literal runat="server" ID="zebraPrint"></asp:Literal><br/>
        Inicio:<br/>
        <asp:TextBox runat="server" runat="server" ID="txtInicio"></asp:TextBox><br/>
        Fim:<br/>
        <asp:TextBox runat="server" runat="server" ID="txtFim"></asp:TextBox><br/>
        Área:<br/>
        <asp:TextBox runat="server" runat="server" ID="txtArea"></asp:TextBox><br/>
        Rua:<br/>
        <asp:TextBox runat="server" runat="server" ID="txtRua"></asp:TextBox><br/>
        Lado:<br/>
        <asp:TextBox runat="server" runat="server" ID="txtLado"></asp:TextBox><br/>
        Prédio:<br/>
        <asp:TextBox runat="server" runat="server" ID="txtPredio"></asp:TextBox><br/>
        Andar:<br/>
        <asp:TextBox runat="server" runat="server" ID="txtAndar"></asp:TextBox><br/>
        <asp:Button runat="server" ID="btnGerar" Text="Gerar" /><br /><br/>
            </div>
    <div style="width: 1123px">
        <asp:ListView runat="server" ID="lstCodigos" OnItemDataBound="lstCodigos_OnItemDataBound">
            <ItemTemplate>
                <div style="float: left; width: 255px; height: 175px; border-right: 1px solid;border-bottom: 1px solid;">
                    <table style="width: 100%; padding: 0; border: 0;">
                        <tr>
                            <td class="robotoUltraBold" style="font-size: 32px; vertical-align: bottom; padding-left: 10px">ANDAR</td>
                            <td class="robotoUltraBold" style="font-size: 90px; vertical-align: bottom; line-height: 85px; text-align: center"><%#Eval("andar") %></td>
                        </tr>
                        <tr>
                            <td class="robotoRegular" style="font-size: 20px; padding-left: 10px">Rua <%#Eval("rua") %> <span style="font-size: 14px">(lado <%#Eval("lado") %>)</span><br/>
                                Prédio <%#Eval("predio") %>
                            </td>
                            <td style=" text-align: center">
                               <asp:Image runat="server" ID="imgCodigoDeBarras"/>
                            </td>
                        </tr>
                    </table>
                </div>
                </ItemTemplate>
        </asp:ListView>
    </div>
    </form>
</body>
</html>
