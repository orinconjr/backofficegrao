﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class envio_pacotes : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ddlQuantidadePacotes.Focus();
        HttpCookie usuarioExpedicaoLogadoId = new HttpCookie("usuarioExpedicaoLogadoId");
        usuarioExpedicaoLogadoId = Request.Cookies["usuarioExpedicaoLogadoId"];
        if (usuarioExpedicaoLogadoId != null)
        {
            int idUsuarioExpedicao = Convert.ToInt32(usuarioExpedicaoLogadoId.Value);
            var pedidosDc = new dbCommerceDataContext();
            var pedido =
                (from c in pedidosDc.tbPedidos
                 where c.idUsuarioEmbalado == idUsuarioExpedicao && c.dataEmbaladoFim == null
                 select c).FirstOrDefault();
            if (pedido == null)
            {
                Response.Redirect("Default.aspx");
            }
            pedidoId.Text = rnFuncoes.retornaIdCliente(pedido.pedidoId).ToString();
        }
    }

    protected void btnImprimirEtiquetas_OnClick(object sender, EventArgs e)
    {
        HttpCookie usuarioExpedicaoLogadoId = new HttpCookie("usuarioExpedicaoLogadoId");
        usuarioExpedicaoLogadoId = Request.Cookies["usuarioExpedicaoLogadoId"];
        if (usuarioExpedicaoLogadoId != null)
        {
            int idUsuarioExpedicao = Convert.ToInt32(usuarioExpedicaoLogadoId.Value);
            var pedidosDc = new dbCommerceDataContext();
            var pedido =
                (from c in pedidosDc.tbPedidos
                 where c.idUsuarioEmbalado == idUsuarioExpedicao && c.dataEmbaladoFim == null
                 select c).FirstOrDefault();
            if (pedido == null)
            {
                Response.Redirect("Default.aspx");
            }
            int pacotes = 0;
            int.TryParse(ddlQuantidadePacotes.SelectedValue, out pacotes);
            if (pacotes == 0)
            {
                Response.Write("<script>alert('Quantidade de Pacotes Inválido.');</script>");
                return;
            }

            pedido.volumesEmbalados = pacotes;
            pedidosDc.SubmitChanges();
            Response.Redirect("etiquetas.aspx");
        }
    }
}