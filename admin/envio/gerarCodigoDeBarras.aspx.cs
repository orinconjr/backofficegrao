﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class envio_gerarCodigoDeBarras : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        imgCodigoDeBarras.ImageUrl = string.Format("../BarCode.ashx?code={0}&ShowText={1}", Request.QueryString["codigo"], 0);
        litNome.Text = Request.QueryString["nome"];
    }
}