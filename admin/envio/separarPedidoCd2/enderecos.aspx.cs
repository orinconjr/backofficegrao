﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class envio_separarPedido_enderecos : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["produto"] != null)
        {
            int produtoId = 0;
            int.TryParse(Request.QueryString["produto"], out produtoId);

            var data = new dbCommerceDataContext();
            var produtoNome =
                (from c in data.tbProdutos where c.produtoId == produtoId select new {c.produtoNome, c.fotoDestaque}).FirstOrDefault();
            if (produtoNome != null)
            {
                litProdutoNome.Text = produtoNome.produtoNome;
                imgProduto.ImageUrl = "https://dmhxz00kguanp.cloudfront.net/fotos/" + produtoId + "/" +
                                      produtoNome.fotoDestaque + ".jpg";
                var etiquetas = (from c in data.tbProdutoEstoques
                                     where c.enviado == false && (c.pedidoId == null) && c.produtoId == produtoId && c.liberado == true
                                     orderby (c.dataConferencia ?? c.dataEntrada) descending
                                     select new
                                     {
                                         codigoProduto = c.tbPedidoFornecedorItem.codigoProduto,
                                         area = c.idEnderecamentoArea == null ? "" : c.tbEnderecamentoArea.area,
                                         rua = c.idEnderecamentoRua == null ? "" : c.tbEnderecamentoRua.rua,
                                         predio = c.idEnderecamentoPredio == null ? "" : c.tbEnderecamentoPredio.predio,
                                         lado = c.idEnderecamentoPredio == null ? "" : c.tbEnderecamentoPredio.tbEnderecamentoLado.lado,
                                         andar = c.idEnderecamentoAndar == null ? "" : c.tbEnderecamentoAndar.andar,
                                         apartamento = c.idEnderecamentoApartamento == null ? "" : c.tbEnderecamentoApartamento.apartamento
                                     }).Take(15);
                listaEnderecos.DataSource = etiquetas;
                listaEnderecos.DataBind();
            }
        }
    }
}