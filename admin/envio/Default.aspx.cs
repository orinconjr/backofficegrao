﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class envio_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void imbEntrar_Click(object sender, ImageClickEventArgs e)
    {
        var usuarioDc = new dbCommerceDataContext();
        var usuarioExpedicao = (from c in usuarioDc.tbUsuarioExpedicaos where c.senha == txtSenha.Text select c).FirstOrDefault();
        if (usuarioExpedicao != null)
        {
            HttpCookie usuarioExpedicaoLogadoId = new HttpCookie("usuarioExpedicaoLogadoId");
            usuarioExpedicaoLogadoId.Value = usuarioExpedicao.idUsuarioExpedicao.ToString();
            usuarioExpedicaoLogadoId.Expires = DateTime.Now.AddMinutes(30);
            Response.Cookies.Add(usuarioExpedicaoLogadoId);

            var pedidosDc = new dbCommerceDataContext();
            var pedido =
                (from c in pedidosDc.tbPedidos
                 where c.idUsuarioEmbalado == usuarioExpedicao.idUsuarioExpedicao && c.dataEmbaladoFim == null
                    select c).FirstOrDefault();
            if (pedido == null)
            {
                Response.Redirect("selecionarPedido.aspx");
            }
            else
            {
                if (pedido.volumesEmbalados > 0)
                {
                    var pacotes = (from c in pedidosDc.tbPedidoPacotes where c.idPedido == pedido.pedidoId && (c.concluido ?? true) == false select c).Count();
                    if (pacotes < pedido.volumesEmbalados)
                    {
                        Response.Redirect("pesoPacotes.aspx");
                    }
                    else
                    {
                        if (pedido.formaDeEnvio.ToLower() == "sedex" | pedido.formaDeEnvio.ToLower() == "pac")
                        {
                            Response.Redirect("rastreio.aspx");
                        }
                        else
                        {
                            Response.Redirect("pesoPacotes.aspx");
                        }
                    }
                }
                else
                {
                    Response.Redirect("pacotes.aspx");
                }
            }
        }
        else
        {
            Response.Write("<script>alert('Dados inválidos, Tente novamente.');</script>");
        }
    }
}