﻿var produto = "";
var idUsuario = 0;
var senhaUsuario = "";
var idPedido = 0;
var carregando = false;
var endereco = "";
var produtos = [];
var totalProdutos = 0;

$(document).ready(function () {
    $(document).on('click', '#btnEntrar', function (e) {
        logar($("#txtSenha").val());
        $.mobile.loading('show', {
            text: 'Carregando',
            textVisible: true,
            theme: 'b',
            html: ""
        });
    });
    $(document).on('click', '#btnEmbalar', function (e) {
        $.mobile.loading('show', {
            text: 'Carregando',
            textVisible: true,
            theme: 'b',
            html: ""
        });
        SepararPedido();
    });
    $(document).on('click', '#btnProduto', function (e) {
        $.mobile.loading('show', {
            text: 'Carregando',
            textVisible: true,
            theme: 'b',
            html: ""
        });
        if (produto == "") {
            verificaProduto();
        } else {
            adicionaProduto();
        }
    });
    $(document).on('keypress', '#txtProduto', function (e) {
        if (e.which == 13) {
            $('#btnProduto').click();
        }
    });
    $(document).on('keypress', '#txtSenha', function (e) {
        if (e.which == 13) {
            $('#btnEntrar').click();
        }
    });
    $(document).on('click', '#btnConfirmarProdutoNao', function (e) {
        produto = "";
        $("#popConfirmarProduto").popup("close");
        produto = "";
        $("#txtProduto").val("");
        $("#txtProduto").focus();
        produto = "";
    });
    $(document).on('click', '#btnConfirmarProdutoSim', function (e) {
        $("#popConfirmarProduto").popup("close");
        produtos.push(produto);
        produto = "";
        $("#txtProduto").focus();
        //$("#lblCampo").html("Endereco");
        //$("#btnProduto").html("Endereçar");
        $.mobile.loading('hide');
							
    });
    $(document).on('click', '#btnProdutoErradoOk', function (e) {
        produto = "";			
        $( "#popProdutoErrado" ).popup( "close" );					
    });
    $(document).on('click', '#btnProdutoEnderecadoFalha', function (e) {
        $("#popProdutoRegistradoFalha").popup("close");
    });
    $(document).on('click', '#btnProdutoEnviadoOk', function (e) {
        produto = "";
        $("#popProdutoEnviado").popup("close");
        produto = "";
        $("#txtProduto").val("");
        $("#txtProduto").focus();
        produto = "";
    });
    $(document).on('click', '#btnProdutoEnderecadoOk', function (e) {
        $("#txtProduto").val("");
        $("#lblCampo").html("Produto");
        $("#btnProduto").html("Carregar Produto");
        $("#popProdutoRegistrado").popup("close");
        $.mobile.loading('hide'); 
        produto = "";
        setTimeout(function () {
            $("#txtProduto").focus();
        }, 420); // After 420 ms
    });
    $(document).on('click', '#page', function (e) {
        setTimeout(function () {
            $("#txtProduto").focus();
        }, 420); // After 420 ms
    });
    $(document).on('keypress', '#btnProdutoEnderecadoOk', function (e) {
        if (e.which == 13) {
            $('#btnProduto').click();
        }
    });

});

function logar(senha) {
    carregando = false;
    var usrService = "../separarPedido/SepararWs.asmx/LogarSeparacao";
    var dados = "{'senha': '" + senha + "'}";
    $.ajax({
        url: usrService,
        data: dados,
        dataType: "json",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataFilter: function (data) { return data; },
        success: function (data) {
            var retorno = data.d;
            if (retorno == "0") {
                alert("Senha incorreta");
                $("#txtSenha").val("");
                $("#txtSenha").focus();
                $.mobile.loading('hide');
            } else {
                carregaSeparar(true);
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {

        }
    });
}

function carregaSeparar(recarregar) {
    if(recarregar) $.mobile.changePage("registrar.html", { transition: "slideup" });
    $("#txtProduto").focus();
    $("#lblCampo").html("Produto");
    $.mobile.loading('hide');

    return;

    var dados = "{'idPedido': '" + idPedido + "'}";
    $.ajax({
        url: usrService,
        data: dados,
        dataType: "json",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataFilter: function (data) { return data; },
        success: function (data) {
            var retorno = data.d;
            var listaProdutos = "";
            var produtos = JSON.parse(retorno);
            $("#numeroPedido").html("Pedido " + produtos[0].pedidoId);
            for (var i = 0; i < produtos.length; i++) {
                var produto = produtos[i];
                if (produto.separado == false) {
                    listaProdutos += "<li rel='" + produto.produtoId + "/" + produto.produtoFoto + ".jpg'>";
                    listaProdutos += "<div style'width: 100%; overflow:auto;'>";
                    listaProdutos += "<b style=\"font-size:26px\">" + produto.codigoProduto + "</b><br>";
                    //if (produto.codigoProduto == '') {
                        listaProdutos += "<b>Produto: </b>" + produto.produtoNome + "<br>";
                        listaProdutos += "<b>ID: </b>" + produto.produtoIdDaEmpresa + "-" + produto.complementoIdDaEmpresa + "<br>";
                    //}
                    listaProdutos += "<b>Fornecedor: </b>" + produto.fornecedorNome + "<br>";
                    //listaProdutos += produto.fornecedorNome + " - " + produto.categoria + "<br>";
                    listaProdutos += "<b>Quantidade: </b>" + produto.quantidade + "<br>";
                    listaProdutos += "<b>Separados: </b>" + produto.separados + "<br><br>&nbsp;</div>";
                    listaProdutos += "</li>";
                }
            }
            for (var i = 0; i < produtos.length; i++) {
                var produto = produtos[i];
                if (produto.separado == true) {
                    listaProdutos += "<li style='background: #D2E9FF'  rel='" + produto.produtoId + "/" + produto.produtoFoto + ".jpg'>";
                    listaProdutos += "<b style=\"font-size:26px\">" + produto.codigoProduto + "</b><br>";
                    //if (produto.codigoProduto == '') {
                        listaProdutos += "<b>Produto: </b>" + produto.produtoNome + "<br>";
                        listaProdutos += "<b>ID: </b>" + produto.produtoIdDaEmpresa + "-" + produto.complementoIdDaEmpresa + "<br>";
                    //}
                    listaProdutos += "<b>Fornecedor: </b>" + produto.fornecedorNome + "<br>";
                    //listaProdutos += produto.fornecedorNome + " - " + produto.categoria + "<br>";
                    listaProdutos += "<b>Quantidade: </b>" + produto.quantidade + "<br>";
                    listaProdutos += "<b>Separados: </b>" + produto.separados + "<br><br>&nbsp;</div>";
                    listaProdutos += "</li>";
                }
            }
            $("#listaProdutos").html(listaProdutos);
            $.mobile.loading('hide');
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {

        }
    });
}

function TryParseInt(str, defaultValue) {
    var retValue = defaultValue;
    if (str !== null) {
        if (str.length > 0) {
            if (!isNaN(str)) {
                retValue = parseInt(str);
            }
        }
    }
    return retValue;
}

function verificaProduto() {

    produto = $("#txtProduto").val();
    if (produto == "") {
        $.mobile.loading('hide');
        $("#txtProduto").focus();
        return;
    }

    if (produto.indexOf('-') > -1) {
        adicionaProduto();
        return;
    } else {
        var validaValor = TryParseInt(produto, null);
        if (validaValor == null) {
            $("#popProdutoRegistradoFalha").popup("open");
            $.mobile.loading('hide');
            $("#txtProduto").val("");
            $("#txtProduto").focus();
            return;
        }
    }


    var usrService = "../separarPedido/SepararWs.asmx/VerificaProdutoEnderecamento";

    var dados = "{'etiqueta': '" + produto + "'}";
    $.ajax({
        url: usrService,
        data: dados,
        dataType: "json",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataFilter: function (data) { return data; },
        success: function (data) {
            var retorno = data.d;
            if (retorno == "0") {
                produto = "";
                $("#popProdutoEnviado").popup("open");
                $.mobile.loading('hide');
                $("#txtProduto").val("");
                $("#txtProduto").focus();
                produto = "";
                return;
            }
            if (retorno == "1") {
                produto = "";
                $("#popProdutoEnviado").popup("open");
                $.mobile.loading('hide');
                $("#txtProduto").val("");
                $("#txtProduto").focus();
                produto = "";
                return;
            }
            if (retorno == "2") {
                //$("#popConfirmarProduto").popup("open");
                $("#txtProduto").val("");
                $("#txtProduto").focus();
                produtos.push(produto);
                produto = "";
                //$("#lblCampo").html("Endereco");
                //$("#btnProduto").html("Endereçar");
                listaProdutosLote();
                $.mobile.loading('hide');
            }
            if (retorno == "3") {
                $("#txtProduto").val("");
                $("#txtProduto").focus();
                produtos.push(produto);
                produto = "";
                //$("#lblCampo").html("Endereco");
                //$("#btnProduto").html("Endereçar");
                listaProdutosLote();
                $.mobile.loading('hide');
            }
            
            $.mobile.loading('hide');
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {

        }
    });
}

function listaProdutosLote() {
    var listaProdutos = "";
    for (var i = 0; i < produtos.length; i++) {
        var produto = produtos[i];
            listaProdutos += "<li>";
            listaProdutos += "<div style'width: 100%; overflow:auto;'>";
            listaProdutos += "<b style=\"font-size:26px\">" + produto + "</b><br></div>";
            listaProdutos += "</li>";
    }
    $("#listaProdutos").html(listaProdutos);
}

function adicionaProduto() {
    endereco = $("#txtProduto").val();
    if (endereco == "") {
        $("#txtProduto").val("");
        $.mobile.loading('hide');
        $("#txtProduto").focus();
        return;
    }



    var usrService = "../separarPedido/SepararWs.asmx/EnderecarProduto";


    totalProdutos = produtos.length;
    var totalErros = 0;

    var produtosOriginal = produtos.slice(0);
    for (var i = 0; i < produtosOriginal.length; i++) {

        var produto = produtosOriginal[i];
        var dados = "{'etiqueta': '" + produto + "', 'endereco': '" + endereco + "'}";
        $.ajax({
            url: usrService,
            data: dados,
            dataType: "json",
            async: false,
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataFilter: function(data) { return data; },
            success: function(data) {
                var retorno = data.d;
                if (retorno == "0") {
                    $("#popProdutoRegistradoFalha").popup("open");
                    $.mobile.loading('hide');
                    $("#txtProduto").val("");
                    $("#txtProduto").focus();
                    totalErros++;
                    if (produtos.length - totalErros == 0) {
                        listaProdutosLote();
                        $("#popProdutoRegistrado").popup("open");
                        $("#btnProdutoEnderecadoOk").focus();
                        $.mobile.loading('hide');
                    }
                }
                if (retorno == "1") {
                    var position = $.inArray(produto, produtos);
                    if (~position) produtos.splice(position, 1);
                    //produtos.splice(i, 1);
                    if (produtos.length - totalErros == 0) {
                        listaProdutosLote();
                        $("#popProdutoRegistrado").popup("open");
                        $("#btnProdutoEnderecadoOk").focus();
                        $.mobile.loading('hide');
                    }
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {

            }
        });
    }
}