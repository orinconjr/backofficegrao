﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class envio_listaProdutos : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            int idPedido = 0;
            int.TryParse(Request.QueryString["idPedido"], out idPedido);
            //if (idPedido.ToString().Length > 6) idPedido = rnFuncoes.retornaIdCliente(idPedido);
            if (idPedido > 0)
            {
                btnImpressaoConcluida.Visible = false;
            }
            HttpCookie usuarioExpedicaoLogadoId = new HttpCookie("usuarioExpedicaoLogadoId");
            usuarioExpedicaoLogadoId = Request.Cookies["usuarioExpedicaoLogadoId"];
            if (usuarioExpedicaoLogadoId != null | idPedido > 0)
            {
                int idUsuarioExpedicao = 0;
                if (idPedido == 0) idUsuarioExpedicao = Convert.ToInt32(usuarioExpedicaoLogadoId.Value);
                var pedidosDc = new dbCommerceDataContext();
                var pedido =
                    (from c in pedidosDc.tbPedidos
                     where (c.idUsuarioEmbalado == idUsuarioExpedicao && c.dataEmbaladoFim == null) | c.pedidoId == idPedido
                     select c).FirstOrDefault();
                if (pedido == null)
                {
                    Response.Redirect("Default.aspx");
                }

                var pedidosEnvio = (from c in pedidosDc.tbPedidoEnvios
                                    where c.idPedido == idPedido
                                    orderby c.dataInicioEmbalagem descending
                                    select c).FirstOrDefault();
                if (pedidosEnvio == null) Response.Redirect("Default.aspx");

                if (pedidosEnvio.idEnderecoSeparacao != null)
                {
                    lblEndereco.Text = pedidosEnvio.tbEnderecoSeparacao.enderecoSeparacao;
                }

                hdfPedidoId.Value = pedido.pedidoId.ToString();

                using (var txn = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required,
                    new System.Transactions.TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted }))
                {
                    var codigosProduto = (from c in pedidosDc.tbProdutoEstoques
                                          where
                                              c.pedidoId == pedido.pedidoId && c.enviado == false &&
                                              c.idPedidoEnvio == pedidosEnvio.idPedidoEnvio
                                          select c).ToList();

                    foreach (var codigoProduto in codigosProduto)
                    {
                        string etiquetaOculta = codigoProduto.idPedidoFornecedorItem.ToString().Substring(0, 1);
                        for (int i = 1; i < codigoProduto.idPedidoFornecedorItem.ToString().Length - 1; i++)
                        {
                            etiquetaOculta += "x";
                        }
                        etiquetaOculta += codigoProduto.idPedidoFornecedorItem.ToString().Substring(codigoProduto.idPedidoFornecedorItem.ToString().Length - 1, 1);

                        //lblCodigosProdutos.Text += codigoProduto.idPedidoFornecedorItem + " - ";
                        lblCodigosProdutos.Text += etiquetaOculta + " - ";
                    }
                }

                lblPedidoIdInterno.Text = rnFuncoes.retornaIdCliente(pedido.pedidoId).ToString();
                imgCodigoDeBarras.ImageUrl = string.Format("../BarCode.ashx?code={0}&ShowText={1}",
                                                rnFuncoes.retornaIdCliente(pedido.pedidoId).ToString(),
                                                0);
                lblEnviarPor.Text = pedido.formaDeEnvio;
                if (pedido.compactarPacote == true)
                {
                    trCompactar.Visible = true;
                }

                /*tbItensPedido.itemPedidoId, tbItensPedido.pedidoId, tbProdutos.produtoNome, tbItensPedido.produtoId, tbItensPedido.itemQuantidade, tbItensPedido.itemPresente, tbItensPedido.valorCusto, 
                      tbItensPedido.garantiaId, tbItensPedido.ItemMensagemDoCartao, tbItensPedido.itemValor, tbProdutos.produtoId AS Expr1, 
                      tbProdutos.produtoIdDaEmpresa, tbProdutos.produtoCodDeBarras, tbProdutoFornecedor.fornecedorNome, tbProdutos.produtoPrecoDeCusto, tbItensPedido.entreguePeloFornecedor, tbItensPedido.prazoDeFabricacao,
tbProdutos.ncm, tbProdutos.cfop, tbProdutos.complementoIdDaEmpresa*/
                sqlItensPedido.SelectParameters.Add("pedidoId", pedido.pedidoId.ToString());
                sqlInteracaoes.SelectParameters.Add("pedidoId", pedido.pedidoId.ToString());

                dtlItensPedido.DataSource = rnPedidos.retornaProdutosEtiqueta(pedido.pedidoId, pedidosEnvio.idCentroDeDistribuicao);
                dtlItensPedido.DataBind();
            }
        }
    }


    protected void DataList1_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            var data = new dbCommerceDataContext();

            int produtoId = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "produtoId"));
            int itemPedidoId = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "itemPedidoId"));
            var produto = (from c in data.tbProdutos where c.produtoId == produtoId select c).First();

            string codigoDeBarras = produto.produtoCodDeBarras;
            decimal produtoPreco = produto.produtoPreco;
            decimal produtoPrecoDeCusto = Convert.ToDecimal(produto.produtoPrecoDeCusto);
            decimal produtoPrecoPromocional = Convert.ToDecimal(rnProdutos.produtoSeleciona_PorProdutoId(produtoId).Tables[0].Rows[0]["produtoPrecoPromocional"].ToString());
            decimal valorDeVenda = produtoPrecoPromocional > 0 ? produtoPrecoPromocional : produtoPreco;
            Label lblPrecoDeCusto = (Label)e.Item.FindControl("lblPrecoDeCusto");
            Label lblPrecoDeVenda = (Label)e.Item.FindControl("lblPrecoDeVenda");
            Label lblMargem = (Label)e.Item.FindControl("lblMargem");
            Label lblCodigos = (Label)e.Item.FindControl("lblCodigos");
            Label lblCodigoDeBarras = (Label)e.Item.FindControl("lblCodigoDeBarras");
            ListView lstCombo = (ListView)e.Item.FindControl("lstCombo");
            lblCodigoDeBarras.Text = codigoDeBarras;

            var pedidosDc = new dbCommerceDataContext();

            using (
                var txn = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required,
                    new System.Transactions.TransactionOptions
                    {
                        IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                    }))

            {

                var codigosProduto = (from c in pedidosDc.tbProdutoEstoques
                    where
                        c.pedidoId == Convert.ToInt32(hdfPedidoId.Value) && c.enviado == false &&
                        (c.produtoId == produtoId | c.produtoId == produto.produtoAlternativoId)
                    select c);
                foreach (var codigoProduto in codigosProduto)
                {
                    //lblCodigos.Text += codigoProduto.idPedidoFornecedorItem + " - ";

                    string etiquetaOculta = codigoProduto.idPedidoFornecedorItem.ToString().Substring(0, 1);
                    for (int i = 1; i < codigoProduto.idPedidoFornecedorItem.ToString().Length - 1; i++)
                    {
                        etiquetaOculta += "x";
                    }
                    etiquetaOculta += codigoProduto.idPedidoFornecedorItem.ToString().Substring(codigoProduto.idPedidoFornecedorItem.ToString().Length - 1, 1);

                    //lblCodigosProdutos.Text += codigoProduto.idPedidoFornecedorItem + " - ";
                    lblCodigos.Text += etiquetaOculta + " - ";
                }
            }

            //var combo = (from c in data.tbItensPedidoCombos where c.idItemPedido == itemPedidoId select new
            //        {
            //            c.tbProduto.produtoNome,
            //            c.tbProduto.produtoIdDaEmpresa,
            //            c.tbProduto.tbProdutoFornecedor.fornecedorNome
            //        }).ToList();
            //if (combo.Any())
            //{
            //    //lstCombo.DataSource = rnProdutos.produtoSelecionaCombo_porProdutoId(produtoId);
            //    //lstCombo.DataSource = combo;
            //    //lstCombo.DataBind();
            //}

            //lblPrecoDeCusto.Text = "R$ " + produtoPrecoDeCusto.ToString("0.00");
            //lblPrecoDeVenda.Text = "R$ " + valorDeVenda.ToString("0.00");
            //lblMargem.Text = (((valorDeVenda*100)/produtoPrecoDeCusto) - 100).ToString("0.##") + "%";


            /*Label lblQuantidade = (Label)e.Item.FindControl("lblQuantidade");
            Label lblPreco = (Label)e.Item.FindControl("lblPreco");

            decimal itemQuantidade = decimal.Parse(lblQuantidade.Text);

            lblPrecoTotal.Text = String.Format("{0:c}", decimal.Parse((produtoPreco * itemQuantidade).ToString()));*/
        }
    }

    protected void btnImpressaoConcluida_OnClick(object sender, EventArgs e)
    {
        Response.Redirect("Default.aspx");
    }


}