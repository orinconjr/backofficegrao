﻿var produto = "";
var idUsuario = 0;
var senhaUsuario = "";
var idPedido = 0;
var carregando = false;

$(document).ready(function () {
    $(document).on('click', '#btnEntrar', function (e) {
        logar($("#txtSenha").val());
        $.mobile.loading('show', {
            text: 'Carregando',
            textVisible: true,
            theme: 'b',
            html: ""
        });
    });
    $(document).on('click', '#btnEmbalar', function (e) {
        $.mobile.loading('show', {
            text: 'Carregando',
            textVisible: true,
            theme: 'b',
            html: ""
        });
        SepararPedido();
    });
    $(document).on('click', '#btnProduto', function (e) {
        $.mobile.loading('show', {
            text: 'Carregando',
            textVisible: true,
            theme: 'b',
            html: ""
        });
        verificaProduto();
    });
    $(document).on('keypress', '#txtEnderecoPedido', function (e) {
        if (e.which == 13) {
            $('#btnEnderecarPedido').click();
        }
    });
    $(document).on('click', '#btnEnderecarPedido', function (e) {
        $.mobile.loading('show', {
            text: 'Carregando',
            textVisible: true,
            theme: 'b',
            html: ""
        });
        enderecarPedido();
    });
    $(document).on('keypress', '#txtSenha', function (e) {
        if (e.which == 13) {
            $('#btnEntrar').click();
        }
    });
    $(document).on('keypress', '#txtProduto', function (e) {
        if (e.which == 13) {
            $('#btnProduto').click();
        }
    });
    $(document).on('click', '#btnConfirmarProdutoNao', function (e) {
        produto = "";
        $("#popConfirmarProduto").popup("close");
        setTimeout(function () {
            $("#txtProduto").focus();
        }, 420); // After 420 ms				
    });
    $(document).on('click', '#btnConfirmarProdutoSim', function (e) {
        adicionaProduto();
        $("#popConfirmarProduto").popup("close");
        $.mobile.loading('show', {
            text: 'Carregando',
            textVisible: true,
            theme: 'b',
            html: ""
        });									
    });
    $(document).on('click', '#btnProdutoErradoOk', function (e) {
        produto = "";			
        $( "#popProdutoErrado" ).popup( "close" );					
    });
    $(document).on('click', '#btnProdutoSeparadoOk', function (e) {
        produto = "";			
        $("#popProdutoSeparado").popup("close");
    });
    $(document).on('click', '#btnProdutoSeparadoQuantidadeOk', function (e) {
        produto = "";			
        $("#popProdutoSeparadoQuantidade").popup("close");
    });
    $(document).on('click', '#btnProdutoSeparadoOutroOk', function (e) {
        produto = "";			
        $("#popProdutoSeparadoOutro").popup("close");
    });
    $(document).on('click', '#btnProdutoEnviadoOutroOk', function (e) {
        produto = "";			
        $("#popProdutoEnviadoOutro").popup("close");
    });
    $(document).on('click', '#btnProdutoNaoLocalizadoOk', function (e) {
        produto = "";			
        $("#popProdutoNaoEncontrado").popup("close");
    });
});




function enderecarPedido() {
    var enderecoPedido = $("#txtEnderecoPedido").val();
    if (enderecoPedido == "") {
        $.mobile.loading('hide');
        $("#txtEnderecoPedido").focus();
        return;
    }
    $("#txtEnderecoPedido").val("");
    var usrService = "SepararWs.asmx/ConferirEndereco";
    var dados = "{'endereco': '" + enderecoPedido + "'}";
    $.ajax({
        url: usrService,
        data: dados,
        dataType: "json",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataFilter: function (data) { return data; },
        success: function (data) {
            var retorno = data.d;
            $.mobile.loading('hide');
            // Enderecao
            $("#endereco").html(retorno);
            $("#txtEnderecoPedido").focus();

        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {

        }
    });
}