﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="enviarPor.aspx.cs" Inherits="envio_enviarPor" %>


<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dxwgv" %>

<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>BarkCommerce - Sistema Administrativo</title>
    <link href="../admin/estilos/estilos.css" rel="stylesheet" type="text/css" />
    <script language="JavaScript" type="text/javascript" src="../admin/js/funcoes.js"></script>
</head>
<body>
    <form id="formulario" runat="server">
    <table align="center" cellpadding="0" cellspacing="0" class="tablePrincipal" 
        style="width: 100%">
        <tr>
            <td>
    <table cellpadding="0" cellspacing="0" style="width: 920px" align="center">
        <tr>
            <td>
                <table cellpadding="0" cellspacing="0" style="width: 920px">
                    <tr>
                        <td style="width: 210px; height: 97px">
                        </td>
                        <td style="width: 140px; height: 97px">
                        </td>
                        <td style="width: 189px; height: 97px">
                        </td>
                        <td style="width: 208px; height: 97px">
                        </td>
                        <td style="height: 97px">
                        </td>
                    </tr>
                    <tr valign="bottom">
                        <td style="width: 210px">
                            &nbsp;</td>
                        <td class="textoCabecalho" style="width: 140px">
                        </td>
                        <td class="textoCabecalho" style="width: 189px">
                            
                        </td>
                        <td class="textoCabecalho" style="width: 208px">
                                        </td>
                                        <td>
                                        </td>
                                        <td class="textoCabecalho">Seu IP:
                                            <asp:Label ID="lblIp" runat="server" Font-Bold="True" Text="200.258.110.123" 
                                EnableTheming="False"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
            </td>
        </tr>
        <tr>
            <td height="12"></td>
        </tr>
        
        <tr>
            <td height="30"></td>
        </tr>
        <tr>
            <td bgcolor="White" style="height: 300px" valign="top">
                
                <table cellpadding="0" cellspacing="0" style="width: 920px">
    <tr>
        <td class="tituloPaginas" valign="top">
            <asp:Label ID="lblAcao" runat="server">Etiquetar</asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            
    <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr class="rotulos">
            <td style="text-align: center; font-size: 26px;">
                Enviar pedido por:<br/>
                <asp:Literal runat="server" ID="litEnviarPor"></asp:Literal>
            </td>
        </tr>
        <tr class="rotulos">
            <td style="text-align: center; padding-top: 20px;">
                <asp:Button runat="server" ID="btnSelecionarPedido" Text="Concluir" OnClick="btnSelecionarPedido_OnClick" Font-Size="15" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
    </table>
        </td>
    </tr>
</table>

            </td>
        </tr>
        <tr>
            <td style="height: 176px; background-image: url('../admin/images/rodape.jpg');">
                <table cellpadding="0" cellspacing="0" style="width: 920px">
                    <tr>
                        <td style="width: 705px; height: 63px">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 705px">
                            &nbsp;</td>
                        <td>
                                            <asp:HyperLink ID="btEmail" runat="server" 
                                ImageUrl="../admin/images/email.gif" NavigateUrl="mailto:suporte@bark.com.br"></asp:HyperLink>
                                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>

