﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class envio_guardarPedido_imprimir : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        int inicio = 0;
        int fim = 0;
        if (!string.IsNullOrEmpty(txtInicio.Text)) inicio = Convert.ToInt32(txtInicio.Text);
        if (!string.IsNullOrEmpty(txtInicio.Text)) fim = Convert.ToInt32(txtFim.Text);
        if (Page.IsPostBack)
        {
            if (inicio == 0 && fim == 0) fim = 1000000;
        }
        var and = txtAndar.Text;
        var area = txtArea.Text;
        var rua = txtRua.Text;
        var lado = txtLado.Text;
        var predio = txtPredio.Text;

        var data = new dbCommerceDataContext();

        var produtosCodigo =
            (from c in data.tbProdutoEstoques
                where c.idEnderecamentoAndar != null && c.idEnderecamentoArea == null
                select c);
        foreach (var produtoCodigo in produtosCodigo)
        {
            var andar = (from c in data.tbEnderecamentoAndars where c.idEnderecamentoAndar == produtoCodigo.idEnderecamentoAndar select c).FirstOrDefault();

            var produto =
                (from c in data.tbProdutoEstoques where c.idPedidoFornecedorItem == produtoCodigo.idPedidoFornecedorItem select c)
                    .First();
            produto.idEnderecamentoArea = andar.tbEnderecamentoPredio.tbEnderecamentoRua.idEnderecamentoArea;
            produto.idEnderecamentoRua = andar.tbEnderecamentoPredio.idEnderecamentoRua;
            produto.idEnderecamentoPredio = andar.idEnderecamentoPredio;
            produto.idEnderecamentoAndar = andar.idEnderecamentoAndar;

            var pedidoItem =
                (from c in data.tbPedidoFornecedorItems
                 where c.idPedidoFornecedorItem == produtoCodigo.idPedidoFornecedorItem
                 select c).First();

            data.SubmitChanges();
        }


        var andares = (from c in data.tbEnderecamentoAndars
            where c.idEnderecamentoAndar >= inicio && c.idEnderecamentoAndar <= fim
            select new
            {
                c.idEnderecamentoAndar,
                c.andar,
                c.tbEnderecamentoPredio.predio,
                c.tbEnderecamentoPredio.tbEnderecamentoRua.rua,
                c.tbEnderecamentoPredio.tbEnderecamentoRua.tbEnderecamentoArea.area,
                c.tbEnderecamentoPredio.tbEnderecamentoLado.lado
            }).ToList();
        if (!string.IsNullOrEmpty(and))
        {
            andares = andares.Where(x => x.andar == and).ToList();
        }
        if (!string.IsNullOrEmpty(area))
        {
            andares = andares.Where(x => x.area.ToLower() == area.ToLower()).ToList();
        }
        if (!string.IsNullOrEmpty(rua))
        {
            andares = andares.Where(x => x.rua == rua).ToList();
        }
        if (!string.IsNullOrEmpty(lado))
        {
            andares = andares.Where(x => x.lado.ToLower() == lado.ToLower()).ToList();
        }
        if (!string.IsNullOrEmpty(predio))
        {
            andares = andares.Where(x => x.predio == predio).ToList();
        }
        lstCodigos.DataSource = andares;
        lstCodigos.DataBind();

        var saida = new StringBuilder();
        foreach (var andar in andares)
        {
            saida.Append("N<br>");
            saida.Append("q800<br>");
            saida.Append("Q600,26<br>");
            saida.AppendFormat("B500,350,0,3,2,5,200,B,\"{0}\"<br>", andar.idEnderecamentoAndar + "-4");
            saida.Append("A70,300,0,4,2,5,N,\"ANDAR\"<br>");
            saida.AppendFormat("A70,440,0,1,2,4,N,\"Área {0}\"<br>", andar.area);
            saida.AppendFormat("A70,490,0,1,2,4,N,\"Rua {0} (lado {1})\"<br>", andar.rua, andar.lado);
            saida.AppendFormat("A70,540,0,1,2,4,N,\"Predio {0}\"<br>", andar.predio);
            saida.AppendFormat("A300,180,0,5,2,5,N,\"{0}\"<br>", andar.andar);
            saida.Append("P1,1<br>");
        }
        saida.Append("<br>");
        saida.Append("<br>");
        zebraPrint.Text = saida.ToString();
    }

    protected void lstCodigos_OnItemDataBound(object sender, ListViewItemEventArgs e)
    {
        Image imagem = (Image)e.Item.FindControl("imgCodigoDeBarras");
        dynamic item = e.Item.DataItem;

        imagem.ImageUrl = string.Format("../../BarCode.ashx?code={0}&ShowText={1}", item.idEnderecamentoAndar + "-4", 0);
    }
}