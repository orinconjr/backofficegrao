﻿var produto = "";
var idUsuario = "";
var senhaUsuario = "";
var idPedido = 0;
var carregando = false;
var endereco = "";
var pedidoSeparar = "";

function playSound(filename) {
    //document.getElementById("sound").innerHTML = '<audio autoplay="autoplay"><source src="' + filename + '" type="audio/mpeg" /></audio>';
    document.getElementById('alerta').play();

}


$(document).ready(function () {
    $(document).on('click', '#btnEntrar', function (e) {
        logar($("#txtSenha").val());
        $.mobile.loading('show', {
            text: 'Carregando',
            textVisible: true,
            theme: 'b',
            html: ""
        });
    });
    $(document).on('click', '#btnEmbalar', function (e) {
        $.mobile.loading('show', {
            text: 'Carregando',
            textVisible: true,
            theme: 'b',
            html: ""
        });
        SepararPedido();
    });
    $(document).on('click', '#btnProduto', function (e) {
        $.mobile.loading('show', {
            text: 'Carregando',
            textVisible: true,
            theme: 'b',
            html: ""
        });
        console.log("btnProduto: " + idUsuario);
        produto = $("#txtProduto").val();
        if (produto == "") {
            $.mobile.loading('hide');
            $("#txtProduto").focus();
            return;
        } else {
            verifcarCodigoBarra(produto);
        }


    });
    $(document).on('keypress', '#txtSenha', function (e) {
        if (e.which == 13) {
            $('#btnEntrar').click();
        }
    });
    $(document).on('keypress', '#txtProduto', function (e) {
        if (e.which == 13) {
            $('#btnProduto').click();
        }
    });
    $(document).on('click', '#btnConfirmarProdutoNao', function (e) {
        produto = "";
        $("#popConfirmarProduto").popup("close");
        produto = "";
        $("#txtProduto").val("");
        $("#txtProduto").focus();
        produto = "";
    });
    $(document).on('click', '#btnConfirmarProdutoSim', function (e) {
        $("#popConfirmarProduto").popup("close");
        $("#txtProduto").val("");
        $("#txtProduto").focus();
        $("#lblCampo").html("Endereco");
        $("#btnProduto").html("Endereçar");
        $.mobile.loading('hide');

    });
    $(document).on('click', '#btnProdutoErradoOk', function (e) {
        produto = "";
        $("#popProdutoErrado").popup("close");
    });
    $(document).on('click', '#btnProdutoEnderecadoFalhaEndereco', function (e) {
        $("#popProdutoRegistradoFalhaEndereco").popup("close");
    });
    $(document).on('click', '#btnProdutoEnderecadoFalha', function (e) {
        $("#popProdutoRegistradoFalha").popup("close");
    });
    $(document).on('click', '#btnProdutoEnviadoOk', function (e) {
        produto = "";
        $("#popProdutoEnviado").popup("close");
        produto = "";
        $("#txtProduto").val("");
        $("#txtProduto").focus();
        produto = "";
    });
    $(document).on('click', '#btnProdutoEnderecadoOk', function (e) {
        $("#txtProduto").val("");
        $("#lblCampo").html("Produto");
        $("#btnProduto").html("Carregar Produto");
        $("#popProdutoRegistrado").popup("close");
        $.mobile.loading('hide');
        produto = "";
        setTimeout(function () {
            $("#txtProduto").focus();
        }, 420); // After 420 ms
    });
    $(document).on('click', '#page', function (e) {
        setTimeout(function () {
            $("#txtProduto").focus();
        }, 420); // After 420 ms
    });
    $(document).on('keypress', '#btnProdutoEnderecadoOk', function (e) {
        if (e.which == 13) {
            $('#btnProduto').click();
        }
    });
    $(document).on('click', '#btnMensagemOk', function (e) {
        playSound("");
        $("#popMensagem").popup("close");
    });
});

var idTransferenciaEndereco = "";

function logar(senha) {
    carregando = false;
    verificarPermissaoUsuarioExpedicao(senha);
}

function verificarPermissaoUsuarioExpedicao(senha) {
    var usrService = "../separarPedido/SepararWs.asmx/VerifcarPermissaoUsuarioExpedicao";
    var dados = "{'senha': '" + senha + "'}";

    $.ajax({
        url: usrService,
        data: dados,
        dataType: "json",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataFilter: function (data) { return data; },
        success: function (data) {
            idUsuario = data.d;
            console.log(idUsuario);
            if (idUsuario > 0 ) {
                carregaSeparar(true);
            }
            else {
                alert("Senha incorreta");
                $("#txtSenha").val("");
                $("#txtSenha").focus();
                $.mobile.loading('hide');
            }

        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {

        }
    });
}

function verifcarCodigoBarra(codiooDeBarras) {
    var usrService = "../separarPedido/EmissaoWs.asmx/RetornarVolumeCaminhaoSeparar";
    var dados = "{'codigoDeBarras': '" + codiooDeBarras + "','idusuario': '" + idUsuario + "','idCentroDistribuicao': 4}";

    $.ajax({
        url: usrService,
        data: dados,
        dataType: "json",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataFilter: function (data) { return data; },
        success: function (data) {
            var retorno = data.d;
            console.log(retorno);
            console.log("Retorno: " + retorno.sucesso);

            if (retorno.sucesso == true) {
                carregaSeparar(true);
                $("#txtProduto").val("");
            }
            else {
                $("#mensagemPop").text(retorno.mensagem);
                $("#popMensagem").popup("open");
                playSound("alarme.wav");
            }
            $.mobile.loading('hide');
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {

        }
    });
}


//function verifcarCodigoBarra(codiooDeBarras) {
//    var usrService = "../separarPedido/EmissaoWs.asmx/RetornarVolumeCaminhaoSeparar";
//    var dados = "{'codigoDeBarras': '" + codiooDeBarras + "','transportadora': '" + idUsuario + "','idCentroDistribuicao': 1}";

//    $.ajax({
//        url: usrService,
//        data: dados,
//        dataType: "json",
//        type: "POST",
//        contentType: "application/json; charset=utf-8",
//        dataFilter: function (data) { return data; },
//        success: function (data) {
//            var retorno = data.d;
//            console.log(retorno);
//            console.log("Retorno: " + retorno.sucesso);

//            if (retorno.sucesso.val(true)) {
//                carregaSeparar(true);
//            }
//            else {
//                $("#mensagemPop").text(retorno.mensagem);
//                $("#popMensagem").popup("open");
//                playSound("alarme.wav");
//            }
//            $.mobile.loading('hide');
//        },
//        error: function (XMLHttpRequest, textStatus, errorThrown) {

//        }
//    });
//}


function carregaSeparar(recarregar) {
    if (recarregar) $.mobile.changePage("registrar.html", { transition: "slideup" });
    $("#txtProduto").focus();
    $("#lblCampo").html("Produto");


    var usrService = "../separarPedido/EmissaoWs.asmx/ListaAguardandoCarregamentoCaminhaoCdsNew";
    var dados = "{'chave': 'glmp152029','idusuario': '" + idUsuario + "','idcentrodistribuicao': 4}";
    $.ajax({
        url: usrService,
        data: dados,
        dataType: "json",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataFilter: function (data) { return data; },
        success: function (data) {
            var retorno = data.d;
            var lista = "";
            if (retorno.TotalDePedidos.length > 0) {
                $("#totalProdutos").html("Não há pacotes para carregar");
                $("#totalParcial").html("Não há pacotes parcialmente carregados")
            } else {
                $("#totalProdutos").html("Falta carregar: " + retorno.TotalDePedidos + " volumes");
                $("#totalParcial").html("Falta carregar: " + retorno.TotalDePedidosParcial + " volumes parcial");

                for (var i = 0; i < retorno.ListaCarregamento.length; i++) {
                    var dados = retorno.ListaCarregamento[i];
                    var color = "";

                    if (i % 2 == 0) {
                        color = "#ece9e9";
                    }else {
                        color = "#FFFFFF";
                    }

                    lista += "<li style='padding-bottom: 10px;background-color: " + color + "'>";
                    lista += "<div style'width: 100%; overflow:auto;'>";
                    lista += "<b>Pacotes: </b>" + dados.pedidoId + "<br>";
                    lista += "<b>Pacotes: </b>" + dados.totalPacotes + " (" + dados.totalPacotesCd1 + "/" + dados.totalPacotesCd2 + ") <br>";
                    lista += "<b>Carregados: </b>" + dados.totalCarregados + " (" + dados.totalCarregadosCd1 + "/" + dados.totalCarregadosCd2 + ") <br>";
                    lista += "</li>";
                }
                $("#listaProdutos").html(lista);
                $.mobile.loading('hide');
                setTimeout(function () {
                    $("#txtProduto").focus();
                }, 420); // After 420 ms
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {

        }
    });
}


function CarregaListaEnderecamento() {
    $("#txtProduto").val("");
    $("#lblCampo").html("Produto");
    $("#btnProduto").html("Carregar Produto");
    produto = "";


    var usrService = "../separarPedido/SepararWs.asmx/CarregaListaEnderecoProvisorio";
    var dados = "{'idUsuario': '" + idUsuario + "'}";
    $.ajax({
        url: usrService,
        data: dados,
        dataType: "json",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataFilter: function (data) { return data; },
        success: function (data) {


        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {

        }
    });


    return;
}

function verificaProduto() {
    pedidoSeparar = "";
    if (idTransferenciaEndereco == "") {
        AtribuiCarrinhoUsuario();
    } else {
        produto = $("#txtProduto").val();
        if (produto == "") {
            $.mobile.loading('hide');
            $("#txtProduto").focus();
            return;
        }

        if (produto.indexOf('-') > -1) {
            $.mobile.loading('hide');
            $("#popProdutoErrado").popup("open");
            $("#txtProduto").val("");
            $("#txtProduto").focus();
            produto = "";
            return;
        }

        $("#mensagem").html("");
        $("#mensagemSeparar").hide();

        var usrService = "../separarPedido/SepararWs.asmx/VerificaProdutoEnderecamentoTransferencia";

        var dados = "{'etiqueta': '" + produto + "','idTransferenciaEndereco': '" + idTransferenciaEndereco + "'}";
        $.ajax({
            url: usrService,
            data: dados,
            dataType: "json",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataFilter: function (data) { return data; },
            success: function (data) {
                var retorno = data.d;
                if (retorno == "0") {
                    produto = "";
                    $("#popProdutoEnviado").popup("open");
                    $.mobile.loading('hide');
                    $("#txtProduto").val("");
                    $("#txtProduto").focus();
                    produto = "";
                    return;
                }
                if (retorno == "1") {
                    produto = "";
                    $("#popProdutoEnviado").popup("open");
                    $.mobile.loading('hide');
                    $("#txtProduto").val("");
                    $("#txtProduto").focus();
                    produto = "";
                    return;
                }
                if (retorno.lastIndexOf("2_", 0) === 0) {
                    pedidoSeparar = retorno.split('_')[1];
                    $("#txtProduto").val("");
                    $("#txtProduto").focus();
                    $("#lblCampo").html("Endereco");
                    $("#btnProduto").html("Endereçar");
                    $("#mensagemSeparar").show();
                    $.mobile.loading('hide');
                }
                if (retorno == "3") {
                    $("#txtProduto").val("");
                    $("#txtProduto").focus();
                    $("#lblCampo").html("Endereco");
                    $("#btnProduto").html("Endereçar");
                    $.mobile.loading('hide');
                }
                if (retorno == "4") {
                    produto = "";
                    alert("Produto não faz parte do seu endereçamento");
                    $.mobile.loading('hide');
                    $("#txtProduto").val("");
                    $("#txtProduto").focus();
                    produto = "";
                    return;
                }

                $.mobile.loading('hide');
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {

            }
        });
    }
}

function adicionaProduto() {
    endereco = $("#txtProduto").val();
    if (endereco == "") {
        $("#txtProduto").val("");
        $.mobile.loading('hide');
        $("#txtProduto").focus();
        return;
    }



    var usrService = "../separarPedido/SepararWs.asmx/EnderecarProdutoTransferencia";
    var dados = "{'etiqueta': '" + produto + "', 'endereco': '" + endereco + "','idTransferenciaEndereco': '" + idTransferenciaEndereco + "'}";

    if (pedidoSeparar != "") {
        usrService = "../separarPedido/SepararWs.asmx/EnderecarProdutoTransferenciaPedido";
        dados = "{'etiqueta': '" + produto + "', 'endereco': '" + endereco + "','idTransferenciaEndereco': '" + idTransferenciaEndereco + "','pedido': '" + pedidoSeparar + "'}";
    }

    $.ajax({
        url: usrService,
        data: dados,
        dataType: "json",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataFilter: function (data) { return data; },
        success: function (data) {
            var retorno = data.d;
            if (retorno == "0") {
                $("#popProdutoRegistradoFalha").popup("open");
                $.mobile.loading('hide');
                $("#txtProduto").val("");
                $("#txtProduto").focus();
                return;
            }
            if (retorno == "0_1") {
                $("#popProdutoRegistradoFalhaEndereco").popup("open");
                $.mobile.loading('hide');
                $("#txtProduto").val("");
                $("#txtProduto").focus();
                return;
            }
            if (retorno == "1") {
                $("#txtProduto").val("");
                $("#lblCampo").html("Produto");
                $("#mensagem").html("Produto endereçado com sucesso");
                $("#btnProduto").html("Carregar Produto");
                $("#popProdutoRegistrado").popup("close");
                $("#mensagemSeparar").hide();
                produto = "";
                pedidoSeparar = "";
                setTimeout(function () {
                    $("#txtProduto").focus();
                }, 420); // After 420 ms
                CarregaListaEnderecamento();
                return;
            }
            $.mobile.loading('hide');
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {

        }
    });
}