﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

public partial class envio_cancelarJad : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            var pedidosDc = new dbCommerceDataContext();
            var pedidosJad = (from c in pedidosDc.tbPedidos where c.codPedidoJadlog != null select c);
            lstPedidosJad.DataSource = pedidosJad;
            lstPedidosJad.DataBind();

            if (Request.QueryString["recadastrar"] != null)
            {
                foreach (var pedido in pedidosJad)
                {
                    cancelar(pedido.codPedidoJadlog);
                    gravaPedidoServiceJadlog(pedido.pedidoId);
                }
            }

            if (Request.QueryString["cadastrar"] != null)
            {
                gravaPedidoServiceJadlog(Convert.ToInt32(Request.QueryString["cadastrar"]));
            }

            if (Request.QueryString["corrigir"] != null)
            {
                List<int> pedidos = new List<int>();
                pedidos.Add(518160157);
                pedidos.Add(580026141);
                pedidos.Add(354582301);
                pedidos.Add(531463965);
                pedidos.Add(1068334877);
                pedidos.Add(647135005);
                pedidos.Add(580026141);
                pedidos.Add(76709661);
                pedidos.Add(110264093);
                pedidos.Add(848461597);
                pedidos.Add(1024622365);
                pedidos.Add(202538781);
                pedidos.Add(269647645);
                pedidos.Add(101875485);
                pedidos.Add(974290717);
                pedidos.Add(165838621);
                pedidos.Add(300056349);
                pedidos.Add(467828509);
                pedidos.Add(518160157);
                pedidos.Add(853704477);
                pedidos.Add(677543709);
                pedidos.Add(727875357);
                pedidos.Add(442662685);
                pedidos.Add(631406365);
                pedidos.Add(723681053);
                pedidos.Add(102924061);
                pedidos.Add(262307613);
                pedidos.Add(612531997);
                pedidos.Add(780304157);
                pedidos.Add(230850333);
                pedidos.Add(246054685);


                List<int> pedidosNao = new List<int>();
                foreach (var pedido in pedidos)
                {
                    int pedidoId = rnFuncoes.retornaIdInterno(pedido);
                    pedidosNao.Add(pedidoId);
                }
                var pedidosreemitir = (from c in pedidosDc.tbPedidos where c.codPedidoJadlog != null && !pedidosNao.Contains(c.pedidoId) select c);
                foreach (var pedido in pedidosreemitir)
                {
                    gravaPedidoServiceJadlog(pedido.pedidoId);
                }

            }
        }
    }

    protected void btnCancelar_OnCommand(object sender, CommandEventArgs e)
    {
        var codPedidoJadlog = e.CommandArgument.ToString();
        cancelar(codPedidoJadlog);

        var pedidosDc = new dbCommerceDataContext();
        var pedidosJad = (from c in pedidosDc.tbPedidos where c.codPedidoJadlog == codPedidoJadlog select c).First();
        //gravaPedidoServiceJadlog(pedidosJad.pedidoId);
    }

    private void cancelar(string codigo)
    {
        string codPedidoJadlog = codigo;
        int CodCliente = 40745;
        string Password = "L2F0M0E1";
        var serviceJad = new serviceJadEnvio.NotfisBeanService();
        var retornoServiceJad = serviceJad.cancelar(CodCliente, Password, codPedidoJadlog, "Endereco Incorreto");


        var pedidosDc = new dbCommerceDataContext();
        var pedidosJad = (from c in pedidosDc.tbPedidos where c.codPedidoJadlog == codPedidoJadlog select c).FirstOrDefault();

        XmlDocument xmlDoc = new XmlDocument();
        xmlDoc.LoadXml(retornoServiceJad);
        XmlNodeList parentNode = xmlDoc.GetElementsByTagName("Jadlog_Pedido_eletronico_Cancelar");
        foreach (XmlNode childrenNode in parentNode)
        {
            try
            {
                string retorno = childrenNode.ChildNodes[1].InnerText;
                if (!String.IsNullOrEmpty(retorno))
                {
                    if (retorno == "-1" | retorno == "-2" | retorno == "-99")
                    {
                        Response.Write("<script>alert('Falha ao cancelar: " + childrenNode.ChildNodes[2].InnerText + ".');</script>");
                        return;
                    }
                    pedidosJad.codPedidoJadlog = null;
                    pedidosDc.SubmitChanges();
                }
            }
            catch (Exception)
            {

            }
        }

    }
    private void gravaPedidoServiceJadlog(int pedidoId)
    {
        var pedidoDc = new dbCommerceDataContext();
        var pedido = (from c in pedidoDc.tbPedidos where c.pedidoId == pedidoId select c).First();
        var clientesDc = new dbCommerceDataContext();
        var cliente = (from c in clientesDc.tbClientes where c.clienteId == pedido.clienteId select c).First();
        var volumes = (from c in pedidoDc.tbPedidoPacotes where c.idPedido == pedidoId select c);

        int idPedidoCliente = rnFuncoes.retornaIdCliente(pedidoId);

        string CodCliente = "40745";
        string Password = "L2F0M0E1";
        string Remetente = "LINDSAY FERRANDO ME";
        string RemetenteCNPJ = "10924051000163";
        string RemetenteIE = "633459940118";
        string RemetenteEndereco = "AV ANA COSTA 416";
        string RemetenteBairro = "GONZAGA";
        string RemetenteCEP = "11060002";
        string RemetenteTelefone = "01135228379";
        string Destino = "";
        string Destinatario = "";
        string DestinatarioCNPJ = "";
        string DestinatarioIE = "";
        string DestinatarioEndereco = "";
        string DestinatarioBairro = "";
        string DestinatarioCEP = "";
        string DestinatarioTelefone = "";

        
        if (!string.IsNullOrEmpty(pedido.endNomeDoDestinatario))
        {
            string enderecoCompleto = pedido.endRua + " - " + pedido.endNumero + " - " + pedido.endComplemento;
            Destino = rnFuncoes.limpaString(pedido.endCidade).ToUpper();
            Destinatario = idPedidoCliente + " " + rnFuncoes.limpaString(pedido.endNomeDoDestinatario).ToUpper();
            DestinatarioCNPJ = cliente.clienteCPFCNPJ.Replace(".", "").Replace("-", "").Replace("/", "");
            DestinatarioEndereco = rnFuncoes.limpaString(enderecoCompleto).ToUpper();
            DestinatarioBairro = rnFuncoes.limpaString(pedido.endBairro).ToUpper();
            DestinatarioCEP = pedido.endCep.Replace("-", "");
            DestinatarioTelefone = cliente.clienteFoneResidencial.Replace(" ", "").Replace("-", "");
        }
        else
        {
            string enderecoCompleto = cliente.clienteRua + " - " + cliente.clienteNumero + " - " + cliente.clienteComplemento;
            Destino = rnFuncoes.limpaString(cliente.clienteCidade).ToUpper();
            Destinatario = idPedidoCliente + " " + rnFuncoes.limpaString(cliente.clienteNome).ToUpper();
            DestinatarioCNPJ = cliente.clienteCPFCNPJ.Replace(".", "").Replace("-", "").Replace("/", "");
            DestinatarioEndereco = rnFuncoes.limpaString(enderecoCompleto).ToUpper();
            DestinatarioBairro = rnFuncoes.limpaString(cliente.clienteBairro);
            DestinatarioCEP = cliente.clienteCep.Replace("-", "");
            DestinatarioTelefone = cliente.clienteFoneResidencial.Replace(" ", "").Replace("-", "");
        }

        //Destinatario = "TESTE DE PEDIDO - NÃO EMITIR";

        string ColetaResponsavel = "";
        string Volumes = pedido.volumesEmbalados.ToString();
        var pesoTotal = (Convert.ToDecimal(volumes.Sum(x => x.peso)) / 1000);
        string PesoReal = pesoTotal.ToString("0.00");
        string Especie = "CONFECCOES";
        string Conteudo = "CONFECCOES";
        string Nr_Pedido = rnFuncoes.retornaIdCliente(pedido.pedidoId).ToString();
        string Nr_NF = "";
        string Danfe = "";
        string Serie_Nf = "";
        string ValorDeclarado = "";
        if (pedido.nfeNumero > 0)
        {
            Nr_NF = pedido.nfeNumero.ToString();
            Danfe = pedido.nfeAccessKey;
            Serie_Nf = "01";
            ValorDeclarado = Convert.ToDecimal(pedido.valorCobrado).ToString("0.00");
        }
        else
        {
            Nr_NF = idPedidoCliente.ToString();
            ValorDeclarado = "100,00";
        }
        string Observacoes = "";
        string Modalidade = "3";
        string wCentroCusto = "";
        string wContaCorrente = "0034802";
        string wTipo = "K";
        string CodUnidade = "795";

        var serviceJad = new serviceJadEnvio.NotfisBeanService();
        var retornoServiceJad = serviceJad.inserir(CodCliente, Password, Remetente, RemetenteCNPJ, RemetenteIE, RemetenteEndereco, RemetenteBairro, RemetenteCEP,
            RemetenteTelefone, Destino, Destinatario, DestinatarioCNPJ, DestinatarioIE, DestinatarioEndereco, DestinatarioBairro, DestinatarioCEP, DestinatarioTelefone,
            ColetaResponsavel, Volumes, PesoReal, Especie, Conteudo, Nr_Pedido, Nr_NF, Danfe, Serie_Nf, ValorDeclarado, Observacoes, Modalidade, wCentroCusto, wContaCorrente,
            wTipo, CodUnidade);

        Response.Write(retornoServiceJad + "<br>");
        XmlDocument xmlDoc = new XmlDocument();
        xmlDoc.LoadXml(retornoServiceJad);
        XmlNodeList parentNode = xmlDoc.GetElementsByTagName("Jadlog_Pedido_eletronico_Inserir");
        foreach (XmlNode childrenNode in parentNode)
        {
            try
            {
                string retorno = childrenNode.ChildNodes[1].InnerText;
                if (!String.IsNullOrEmpty(retorno))
                {
                    if (retorno == "-1" | retorno == "-2" | retorno == "-99")
                    {
                        Response.Write("<script>alert('Falha ao enviar pedido à Jadlog: " + childrenNode.ChildNodes[2].InnerText + ".');</script>");
                        return;
                    }
                    pedido.codPedidoJadlog = retorno;
                    pedidoDc.SubmitChanges();
                    Response.Write(retorno + "<br><br>");
                }
            }
            catch (Exception)
            {

            }
        }

    }
}