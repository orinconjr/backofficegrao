﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class envio_enviarPor : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        HttpCookie usuarioExpedicaoLogadoId = new HttpCookie("usuarioExpedicaoLogadoId");
        usuarioExpedicaoLogadoId = Request.Cookies["usuarioExpedicaoLogadoId"];
        if (usuarioExpedicaoLogadoId != null)
        {
            int idUsuarioExpedicao = Convert.ToInt32(usuarioExpedicaoLogadoId.Value);
            var pedidosDc = new dbCommerceDataContext();
            var pedido =
                (from c in pedidosDc.tbPedidos
                 where c.idUsuarioEmbalado == idUsuarioExpedicao && c.volumesEmbalados > 0
                 orderby c.dataSendoEmbalado descending
                 select c).FirstOrDefault();
            if (pedido == null)
            {
                Response.Redirect("Default.aspx");
            }
            else
            {
                litEnviarPor.Text = pedido.formaDeEnvio.ToUpper();
            }
        }
    }

    protected void btnSelecionarPedido_OnClick(object sender, EventArgs e)
    {
        Response.Redirect("Default.aspx");
    }
}