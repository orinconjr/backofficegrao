﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="carregarPedido.aspx.cs" Inherits="transportadora_carregarPedido" %>

<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v11.1" Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>BarkCommerce - Sistema Administrativo</title>
    <link href="../admin/estilos/estilos.css" rel="stylesheet" type="text/css" />
    <script language="JavaScript" type="text/javascript" src="../admin/js/funcoes.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <style type="text/css">
        .dxgv {
            vertical-align: top !important;
        }
    </style>
    <asp:Literal runat="server" ID="scriptBlock"></asp:Literal>
</head>
<body>
    <form id="formulario" runat="server">
        <table align="center" cellpadding="0" cellspacing="0" class="tablePrincipal" style="width: 100%">
            <tr>
                <td>
                    <table cellpadding="0" cellspacing="0" style="width: 920px" align="center">
                        <tr>
                            <td>
                                <table cellpadding="0" cellspacing="0" style="width: 920px">
                                    <tr>
                                        <td style="width: 210px; height: 97px"></td>
                                        <td style="width: 140px; height: 97px"></td>
                                        <td style="width: 189px; height: 97px"></td>
                                        <td style="width: 208px; height: 97px"></td>
                                        <td style="height: 97px"></td>
                                    </tr>
                                    <tr valign="bottom">
                                        <td style="width: 210px">&nbsp;</td>
                                        <td class="textoCabecalho" style="width: 140px"></td>
                                        <td class="textoCabecalho" style="width: 189px"></td>
                                        <td class="textoCabecalho" style="width: 208px"></td>
                                        <td></td>
                                        <td class="textoCabecalho">Seu IP:
                                        <asp:Label ID="lblIp" runat="server" Font-Bold="True" Text="200.258.110.123" EnableTheming="False"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height="12"></td>
                        </tr>

                        <tr>
                            <td height="30"></td>
                        </tr>
                        <tr>
                            <td bgcolor="White" style="height: 300px" valign="top">
                                <table cellpadding="0" cellspacing="0" style="width: 920px">
                                    <tr>
                                        <td>
                                            <table align="center" cellpadding="0" cellspacing="0" style="width: 100%">
                                                <tr>
                                                    <td class="tituloPaginas" valign="top">
                                                        <asp:Label ID="lblAcao" runat="server">
                                                            Informações para Transportadora
                                                        </asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table cellpadding="0" cellspacing="0" style="width: 920px" align="center">
                                                            <tr>
                                                                <td>
                                                                    <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
                                                                        <tr class="rotulos" style="font-weight: bold; font-size: 14px;">
                                                                            <td style="padding-bottom: 20px;">
                                                                                <table>
                                                                                    <tr>
                                                                                        <td>
                                                                                            Nota Fiscal:<br/>
                                                                                            <asp:TextBox runat="server" id="txtNota">
                                                                                            </asp:TextBox>
                                                                                        </td>
                                                                                        <td style="padding-left: 20px;">
                                                                                            &nbsp;<br/>
                                                                                            <asp:Button runat="server" ID="btnCarregar" OnClick="btnCarregar_OnClick" Text="Carregar Pedido"/>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:HiddenField runat="server" ID="hdfPedido" />
                                                                                <asp:Panel runat="server" ID="pnListaPedidos" Visible="False">
                                                                                    <table class="rotulos">
                                                                                        <tr style="font-weight: bold;">
                                                                                            <td style="padding-top: 20px;">
                                                                                                Cliente
                                                                                            </td>
                                                                                            <td style="padding-left: 20px;padding-top: 20px; ">
                                                                                                Selecionar
                                                                                            </td>
                                                                                        </tr>
                                                                                        <asp:ListView runat="server" ID="lstPedidos">
                                                                                            <ItemTemplate>
                                                                                                <tr>
                                                                                                    <td><%# Eval("clienteNome") %></td>
                                                                                                    <td style="padding-left: 20px;">
                                                                                                        <asp:LinkButton runat="server" ID="btnSelecionarPedido" OnCommand="btnSelecionarPedido_OnCommand" CommandArgument='<%# Eval("idPedido") %>'>Carregar Pedido</asp:LinkButton>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </ItemTemplate>
                                                                                        </asp:ListView>
                                                                                    </table>
                                                                                </asp:Panel>
                                                                                <asp:Panel runat="server" ID="pnInformacoesPedido" Visible="False">
                                                                                    <div class="rotulos">
                                                                                        <asp:Repeater runat="server" ID="rptInformacoesPedido">
                                                                                            <ItemTemplate>
                                                                                                <b>Cliente:</b> <%# Eval("nome") %><br/>
                                                                                                <b>Endereço:</b> <%# Eval("endereco") %><br/>
                                                                                                <b>Número:</b> <%# Eval("numero") %><br/>
                                                                                                <b>Complemento:</b> <%# Eval("complemento") %><br/>
                                                                                                <b>Referência para Entrega:</b> <%# Eval("referencia") %><br/>
                                                                                                <b>Cidade / Estado:</b> <%# Eval("cidade") %> / <%# Eval("estado") %><br/>
                                                                                                <b>Tel 1:</b> <%# Eval("tel1") %><br/>
                                                                                                <b>Tel 2:</b> <%# Eval("tel2") %><br/>
                                                                                                <b>Tel 3:</b> <%# Eval("tel3") %><br/>
                                                                                                <b>Pessoa / Telefone de Recado:</b> <%# Eval("pessoaRecado") %> - <%# Eval("foneRecado") %>
                                                                                            </ItemTemplate>
                                                                                        </asp:Repeater>
                                                                                    </div>
                                                                                    <div style="padding-top: 20px;">
                                                                                        <table cellpadding="0" cellspacing="0" style="width: 100%">
                                                                                            <tr>
                                                                                                <td style="padding-top: 20px; padding-right: 20px;" class="rotulos">
                                                                                                    <b>Interação</b><br/>
                                                                                                    <asp:TextBox runat="server" ID="txtInteracao" TextMode="MultiLine" Width="100%" Height="50px"></asp:TextBox>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="rotulos" valign="middle" align="right" style="padding-top: 5px; padding-bottom: 5px; padding-right: 20px;">
                                                                                                    <asp:ImageButton ID="btSalvarInteracao" runat="server" ImageUrl="../admin/images/btSalvarPeq1.jpg" OnClick="btSalvarInteracao_Click" />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:DataList ID="dtlInteracao" runat="server" CellPadding="0">
                                                                                                        <ItemTemplate>
                                                                                                            <table cellpadding="0" cellspacing="0" class="rotulos" style="width: 782px">
                                                                                                                <tr>
                                                                                                                    <td height="25" style="padding-left: 20px; width: 140px; border-top-style: solid; border-top-width: 1px; border-top-color: #7EACB1;">
                                                                                                                        <asp:Label ID="lblData" runat="server" Text='<%# Bind("data") %>'></asp:Label>
                                                                                                                    </td>
                                                                                                                    <td style="width: 200px; border-top-style: solid; border-top-width: 1px; border-top-color: #7EACB1;">
                                                                                                                        <asp:Label ID="lblusuario" runat="server"
                                                                                                                            Text='<%# Bind("usuario") %>'></asp:Label>
                                                                                                                    </td>
                                                                                                                    <td style="border-top-style: solid; border-width: 1px; border-color: #7EACB1">
                                                                                                                        <asp:Literal ID="ltrInteracao" runat="server" Text='<%# Bind("interacao") %>'></asp:Literal>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderTemplate>
                                                                                                            <table cellpadding="0" cellspacing="0" class="rotulos" style="width: 782px">
                                                                                                                <tr bgcolor="#D4E3E6">
                                                                                                                    <td height="25" style="padding-left: 20px; width: 140px;">
                                                                                                                        <b>Data:</b></td>
                                                                                                                    <td style="width: 200px">
                                                                                                                        <b>Usuário:</b></td>
                                                                                                                    <td>
                                                                                                                        <b>Interação:</b></td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </HeaderTemplate>
                                                                                                    </asp:DataList>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </div>
                                                                                </asp:Panel>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>&nbsp;</td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="height: 176px; background-image: url('../admin/images/rodape.jpg');">
                                                        <table cellpadding="0" cellspacing="0" style="width: 920px">
                                                            <tr>
                                                                <td style="width: 705px; height: 63px"></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 705px">&nbsp;</td>
                                                                <td>
                                                                    <asp:HyperLink ID="btEmail" runat="server"
                                                                        ImageUrl="../admin/images/email.gif" NavigateUrl="mailto:suporte@bark.com.br"></asp:HyperLink>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>

                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>

