﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class transportadora_carregarPedido : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            HttpCookie usuarioTransportadoraLogadoId = new HttpCookie("usuarioTransportadoraLogadoId");
            usuarioTransportadoraLogadoId = Request.Cookies["usuarioTransportadoraLogadoId"];
            if (usuarioTransportadoraLogadoId != null)
            {
                if (rnUsuarios.usuarioPaginasPermitidaSeleciona_PorUsuarioIdPaginaPermitidaNome(int.Parse(usuarioTransportadoraLogadoId.Value.ToString()), "acessotransportadora").Tables[0].Rows.Count == 0)
                {
                    Response.Redirect("Default.aspx");
                }
            }
            else
            {
                Response.Redirect("Default.aspx");
            }
        }
    }

    protected void btnCarregar_OnClick(object sender, EventArgs e)
    {
        hdfPedido.Value = "";
        int numeroNota = 0;
        int.TryParse(txtNota.Text, out numeroNota);
        if (numeroNota > 0)
        {
            var data = new dbCommerceDataContext();
            var notas = (from c in data.tbNotaFiscals
                         join d in data.tbPedidos on c.idPedido equals d.pedidoId
                         join f in data.tbClientes on d.clienteId equals f.clienteId
                         where c.numeroNota == numeroNota select new
                         {
                             c.idPedido,
                             f.clienteNome
                         }).ToList();
            if (notas.Count > 1)
            {
                lstPedidos.DataSource = notas;
                lstPedidos.DataBind();
                pnListaPedidos.Visible = true;
                pnInformacoesPedido.Visible = false;
            }
            if (notas.Count == 1)
            {
                hdfPedido.Value = notas.First().idPedido.ToString();
                CarregaInformacoesPedido();
            }

            if (notas.Count == 0)
            {
                Response.Write("<script>alert('Nota fiscal não localizada.');</script>");
                txtNota.Focus();
            }
        }
    }

    private void CarregaInformacoesPedido()
    {
        int pedidoId = Convert.ToInt32(hdfPedido.Value);
        var data = new dbCommerceDataContext();
        var pedido = (from c in data.tbPedidos
            join d in data.tbClientes on c.clienteId equals d.clienteId
            where c.pedidoId == pedidoId
            select new
            {
                nome = d.clienteNome,
                endereco = c.endRua,
                numero = c.endNumero,
                complemento = c.endComplemento,
                referencia = c.endReferenciaParaEntrega,
                cidade = c.endCidade,
                estado = c.endEstado,
                tel1 = d.clienteFoneCelular,
                tel2 = d.clienteFoneResidencial,
                tel3 = d.clienteFoneComercial,
                pessoaRecado = "",
                foneRecado = ""
            });
        rptInformacoesPedido.DataSource = pedido;
        rptInformacoesPedido.DataBind();
        pnListaPedidos.Visible = false;
        pnInformacoesPedido.Visible = true;
        FillInteracoes();
    }
    protected void btnSelecionarPedido_OnCommand(object sender, CommandEventArgs e)
    {
        hdfPedido.Value = e.CommandArgument.ToString();
        CarregaInformacoesPedido();
    }

    protected void btSalvarInteracao_Click(object sender, ImageClickEventArgs e)
    {
        HttpCookie usuarioTransportadoraLogadoId = new HttpCookie("usuarioTransportadoraLogadoId");
        usuarioTransportadoraLogadoId = Request.Cookies["usuarioTransportadoraLogadoId"];
        string usuario = "";
        if (usuarioTransportadoraLogadoId != null)
        {
            usuario = rnUsuarios.usuarioSeleciona_PorUsuarioId(int.Parse(usuarioTransportadoraLogadoId.Value.ToString())).Tables[0].Rows[0]["usuarioNome"].ToString();
        }

        var data = new dbCommerceDataContext();
        var interacao = new tbInteracaoTransportadora();
        interacao.data = DateTime.Now;
        interacao.pedidoId = Convert.ToInt32(hdfPedido.Value);
        interacao.interacao = txtInteracao.Text;
        interacao.usuario = usuario;
        data.tbInteracaoTransportadoras.InsertOnSubmit(interacao);
        data.SubmitChanges();

        rnEmails.EnviaEmail("", "logisticagraodegente@gmail.com", "mayara@graodegente.com.br", "", "", txtInteracao.Text,
            "Informações para Transportadora: Pedido " + hdfPedido.Value);
        txtInteracao.Text = "";
        Response.Write("<script>alert('Interação gravada com sucesso.');</script>");
        FillInteracoes();
    }

    private void FillInteracoes()
    {
        int pedidoId = Convert.ToInt32(hdfPedido.Value);
        var data = new dbCommerceDataContext();
        var interacoes =
            (from c in data.tbInteracaoTransportadoras where c.pedidoId == pedidoId orderby c.data descending select c);
        dtlInteracao.DataSource = interacoes;
        dtlInteracao.DataBind();
    }
}