﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class transportadora_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void imbEntrar_Click(object sender, ImageClickEventArgs e)
    {
        var data = new dbCommerceDataContext();

        var usuario = (from c in data.tbUsuarios
            where c.usuarioNome.ToLower() == txtUsuario.Text && c.usuarioSenha.ToLower() == txtSenha.Text
            select c).FirstOrDefault();
        if (usuario != null)
        {
            HttpCookie usuarioTransportadoraLogadoId = new HttpCookie("usuarioTransportadoraLogadoId");
            usuarioTransportadoraLogadoId.Value = usuario.usuarioId.ToString();
            usuarioTransportadoraLogadoId.Expires = DateTime.Now.AddHours(12);
            Response.Cookies.Add(usuarioTransportadoraLogadoId);

            Response.Redirect("carregarPedido.aspx");

        }
        else
        {
            Response.Write("<script>alert('Dados inválidos, Tente novamente.');</script>");
        }
    }
}