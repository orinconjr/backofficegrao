﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxGridView;

public partial class fornecedor : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["id"] != null)
        {
            try
            {
                int id = Convert.ToInt32(DecodeFrom64(Request.QueryString["id"]));
                var pedidoDC = new dbCommerceDataContext();
                var pedidos = pedidoDC.pedidoSelecionaProdutosFabricacaoPorFornecedorAdmin(id);
                grd.DataSource = pedidos;
                if (!Page.IsPostBack && !Page.IsCallback) grd.DataBind();
                grd.GroupBy(grd.Columns["pedidoId"]);
                //grd.ExpandAll();

            }
            catch (Exception)
            {
                Response.Redirect("http://www.graodegente.com.br");
            }
        }
        else
        {
            Response.Redirect("http://www.graodegente.com.br");
        }
    }

    protected void gridClientes_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType != GridViewRowType.Data)
        {
            return;
        }

        int itemPedidoId = (int)grd.GetRowValues(e.VisibleIndex, new string[] { grd.KeyFieldName });

        var itensData = new dbCommerceDataContext();
        var prazos = (from c in itensData.tbItensPedidos
                      where c.itemPedidoId == itemPedidoId && c.entreguePeloFornecedor == false
                      orderby c.prazoDeFabricacao
                      select c);
        if (prazos.Any())
        {
            var prazo = prazos.First();
            var agora = Convert.ToDateTime(DateTime.Now.ToShortDateString()).AddHours(23);
            var doisdias = DateTime.Now.Add(TimeSpan.FromDays(2));

            if (prazo.prazoDeFabricacao <= doisdias && prazo.prazoDeFabricacao >= agora)
            {
                int i = 0;
                while ((i < e.Row.Cells.Count))
                {
                    e.Row.Cells[i].Style.Add("background", "#FDF057");
                    i++;
                }
            }
            if (prazo.prazoDeFabricacao <= agora)
            {
                int i = 0;
                while ((i < e.Row.Cells.Count))
                {
                    e.Row.Cells[i].Style.Add("background", "#f04c4c");
                    i++;
                }
            }
        }


    }



    protected void btPdf_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WritePdfToResponse();
    }
    protected void btXsl_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteXlsToResponse();
    }
    protected void btRtf_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteRtfToResponse();
    }
    protected void btCsv_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteCsvToResponse();
    }

    static public string DecodeFrom64(string encodedData)
    {

        byte[] encodedDataAsBytes

            = System.Convert.FromBase64String(encodedData);

        string returnValue =

           System.Text.ASCIIEncoding.ASCII.GetString(encodedDataAsBytes);

        return returnValue;

    }
}