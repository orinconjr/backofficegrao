﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

public partial class simularFrete : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if(Request.QueryString["id"] != null)
        {
            int idPedidoEnvio = Convert.ToInt32(Request.QueryString["id"]);

            var data = new dbCommerceDataContext();

            var pacotes = (from c in data.tbPedidoPacotes where c.idPedidoEnvio == idPedidoEnvio select c);
            foreach(var pacote in pacotes)
            {
                pacote.peso = 0;
                pacote.largura = null;
                pacote.altura = null;
                pacote.profundidade = null;
                pacote.codJadlog = null;
                pacote.concluido = false;
                pacote.formaDeEnvio = null;
                pacote.cubado = false;
            }

            var precos = (from c in data.tbPedidoEnvioValorTransportes where c.idPedidoEnvio == idPedidoEnvio select c);
            foreach(var preco in precos)
            {
                data.tbPedidoEnvioValorTransportes.DeleteOnSubmit(preco);
            }

            var envio = (from c in data.tbPedidoEnvios where c.idPedidoEnvio == idPedidoEnvio select c).First();
            envio.formaDeEnvio = null;
            envio.valorSelecionado = null;
            envio.prazoSelecionado = null;
            data.SubmitChanges();
        }
    }

    protected void btnCalcular_Click(object sender, EventArgs e)
    {

        var data = new dbCommerceDataContext();
        var dataLocal = new dbCommerceDataContext();
        //var dataLocal = new dbCommerceDataContext("Data Source=127.0.0.1;Initial Catalog=graodegenteDev;User ID=sa;Password=Bark152029;Min Pool Size=10; Max Pool Size=1000");
        //var tabelasDePreco = (from c in dataLocal.tbTipoDeEntregaTabelaPrecos where c.idTipoDeEntrega == 18 select c).ToList();
        /*foreach(var tabelaDePreco in tabelasDePreco)
        {
            var tabelaAdicionar = new tbTipoDeEntregaTabelaPreco()
            {
                idTipoDeEntregaTabelaPreco = tabelaDePreco.idTipoDeEntregaTabelaPreco,
                nome = tabelaDePreco.nome,
                valorKgAdicional = tabelaDePreco.valorKgAdicional,
                valorSeguro = tabelaDePreco.valorSeguro
            };
            data.tbTipoDeEntregaTabelaPrecos.InsertOnSubmit(tabelaAdicionar);
            data.SubmitChanges();

            var tabelasValor = (from c in dataLocal.tbTipoDeEntregaTabelaPrecoValors where c.idTipoDeEntregaTabelaPreco == tabelaDePreco.idTipoDeEntregaTabelaPreco select c).ToList();
            foreach(var tabelaValor in tabelasValor)
            {
                var tabelaValorAdicionar = new tbTipoDeEntregaTabelaPrecoValor()
                {
                    idTipoDeEntregaTabelaPreco = tabelaAdicionar.idTipoDeEntregaTabelaPreco,
                    pesoInicial = tabelaValor.pesoInicial,
                    pesoFinal = tabelaValor.pesoFinal,
                    valor = tabelaValor.valor
                };
                data.tbTipoDeEntregaTabelaPrecoValors.InsertOnSubmit(tabelaValorAdicionar);
                data.SubmitChanges();
            }

            var tabelasFaxa = (from c in dataLocal.tbFaixaDeCeps where c.idTipoDeEntregaTabelaPreco == tabelaDePreco.idTipoDeEntregaTabelaPreco select c).ToList();
            foreach (var tabelaFaixa in tabelasFaxa)
            {
                var faixaAdicionar = new tbFaixaDeCep()
                {
                    tipodeEntregaId = tabelaFaixa.tipodeEntregaId,
                    faixaDeCepDescricao = tabelaFaixa.faixaDeCepDescricao,
                    faixaInicial = tabelaFaixa.faixaInicial,
                    faixaFinal = tabelaFaixa.faixaFinal,
                    prazo = tabelaFaixa.prazo,
                    porcentagemDeDesconto = tabelaFaixa.porcentagemDeDesconto,
                    porcentagemDoSeguro = tabelaFaixa.porcentagemDoSeguro,
                    dataDaCriacao = tabelaFaixa.dataDaCriacao,
                    ativo = tabelaFaixa.ativo,
                    interiorizacao = tabelaFaixa.interiorizacao,
                    idTipoDeEntregaTabelaPreco = tabelaDePreco.idTipoDeEntregaTabelaPreco
                };
                data.tbFaixaDeCeps.InsertOnSubmit(faixaAdicionar);
                data.SubmitChanges();
            }
        }
        */
        long cep = Convert.ToInt64(txtCep.Text.Replace("-", ""));
        int peso = Convert.ToInt32(txtPeso.Text.Replace("-", ""));
        decimal pesoEmKg = Convert.ToDecimal(peso) / 1000;
        txtValores.Text = "";
        
        decimal valorPackage = 0;

        try
        {
            var pacotes = new List<rnFrete2.Pacotes>();
            pacotes.Add(new rnFrete2.Pacotes() { peso = Convert.ToInt32(Convert.ToInt32(txtPeso.Text)), altura = 0, comprimento = 0, largura = 0 });
            var freteInst = rnFrete2.Instance;
            var valoresPackage = freteInst.CalculaFrete(29, txtCep.Text.Replace("-", "").Trim(), Convert.ToDecimal(txtValor.Text), pacotes);

            txtValores.Text += "===================================================== Frete LBR  =====================================================<br><br>";

            txtValores.Text += "Package: " + valoresPackage.valor.ToString("C") + "<br>";
            foreach (var detalhe in valoresPackage.detalhamento)
            {
                txtValores.Text += detalhe + "<br>";
            }
        }
        catch (Exception ex)
        {

        }

        try
        {
            var valoresPackage = rnFrete.CalculaFrete(13, Convert.ToInt32(Convert.ToInt32(txtPeso.Text)), txtCep.Text.Replace("-", "").Trim(), Convert.ToDecimal(txtValor.Text), 0, 0, 0);
            
            txtValores.Text += "===================================================== Frete JadLog =====================================================<br><br>";
            
            txtValores.Text += "Package: " + valoresPackage.valor.ToString("C") + "<br>";
            foreach (var detalhe in valoresPackage.detalhamento)
            {
                txtValores.Text += detalhe + "<br>";
            }
        }
        catch (Exception ex)
        {
            
        }

        try
        {
            var valoresPackage = rnFrete.CalculaFreteWebserviceJadlog(Convert.ToInt32(Convert.ToInt32(txtPeso.Text)), Convert.ToInt64(txtCep.Text.Replace("-", "").Trim()), "jadlogexpressa", Convert.ToDecimal(txtValor.Text));
            //rnFrete.CalculaFreteWebserviceJadlog(valoresJadlog.pesoAferido, cep, "jadlog", valorTotalDoPedido);
            txtValores.Text += "===================================================== Frete JadLog Expressa =====================================================<br><br>";

            txtValores.Text += "Expressa: " + valoresPackage.valor.ToString("C") + "<br>";
            foreach (var detalhe in valoresPackage.detalhamento)
            {
                txtValores.Text += detalhe + "<br>";
            }
        }
        catch (Exception ex)
        {

        }



        try
        {
            var valoresTnt = rnFrete.CalculaFrete(10, Convert.ToInt32(Convert.ToInt32(txtPeso.Text)), txtCep.Text.Replace("-", "").Trim(), Convert.ToDecimal(txtValor.Text), 0, 0, 0);

            txtValores.Text += "<br>===================================================== Frete TNT =====================================================<br><br>";

            txtValores.Text += "TNT: " + valoresTnt.valor.ToString("C") + "<br>";
            foreach (var detalhe in valoresTnt.detalhamento)
            {
                txtValores.Text += detalhe + "<br>";
            }
        }
        catch (Exception ex)
        {

        }

        try
        {
            var valoresPlimor = rnFrete.CalculaFrete(19, Convert.ToInt32(Convert.ToInt32(txtPeso.Text)), txtCep.Text.Replace("-", "").Trim(), Convert.ToDecimal(txtValor.Text), 0, 0, 0);
            txtValores.Text += "<br>===================================================== Frete Plimor =====================================================<br><br>";
            txtValores.Text += "Plimor: " + valoresPlimor.valor.ToString("C") + "<br>";
            foreach(var detalhe in valoresPlimor.detalhamento)
            {
                txtValores.Text += detalhe + "<br>";
            }
        }
        catch (Exception ex)
        {

        }
    }
}