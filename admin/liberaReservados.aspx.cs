﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class liberaReservados : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        var data = new dbCommerceDataContext();
        var itensPedidoReservados = (from c in data.tbItemPedidoEstoques where c.reservado == true && c.cancelado == false && c.enviado == false
                                     && c.tbProduto.produtoFornecedor == 72 && c.tbItensPedido.tbPedido.statusDoPedido == 2 select c).ToList();
        foreach (var itensPedidoReservado in itensPedidoReservados)
        {
            var pedidos = (from c in data.tbProdutoEstoques where c.idItemPedidoEstoqueReserva == itensPedidoReservado.idItemPedidoEstoque && c.enviado == false select c).FirstOrDefault();
            {
                var pedidoID = pedidos.pedidoIdReserva;
                var possuiPagamentosPagos =
                    (from c in data.tbPedidoPagamentos
                        where c.pago == true && c.pedidoId == pedidos.pedidoIdReserva
                        select c).Any();
                if (!possuiPagamentosPagos)
                {
                    if (pedidos != null)
                    {
                        var itemPedidoEstoque =
                            (from c in data.tbItemPedidoEstoques
                                where c.idItemPedidoEstoque == itensPedidoReservado.idItemPedidoEstoque
                                select c).First();
                        itemPedidoEstoque.reservado = false;
                        itemPedidoEstoque.dataReserva = null;
                        itemPedidoEstoque.dataLimite = null;

                        pedidos.pedidoIdReserva = null;
                        pedidos.idItemPedidoEstoqueReserva = null;
                        pedidos.itemPedidoIdReserva = null;
                        data.SubmitChanges();
                        var queue = new tbQueue();
                        queue.agendamento = DateTime.Now;
                        queue.andamento = false;
                        queue.concluido = false;
                        queue.idRelacionado = pedidos.produtoId;
                        queue.mensagem = "";
                        queue.tipoQueue = 21;
                        data.tbQueues.InsertOnSubmit(queue);
                        data.SubmitChanges();

                        Response.Write(pedidoID + "<br>");
                    }
                }
            }
        }
    }
}