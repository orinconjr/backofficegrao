﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="consultarNfGrao.aspx.cs" Inherits="consultarNfGrao" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            Empresa:<br />
            <asp:DropDownList runat="server" ID="ddlEmpresas" DataTextField="empresa" DataValueField="idEmpresa" /><br /><br />
            Data inicial: &nbsp;<asp:TextBox ID="txtDataInicial" runat="server" Text="" Width="90px" MaxLength="10" />
            Data final &nbsp;<asp:TextBox ID="txtDataFinal" runat="server" Text="" Width="90px" MaxLength="10" />
            <br />

            Numero(s) da(s) nota(s):
                <br />
            <asp:RadioButtonList ID="rblDanfeXml" runat="server" RepeatDirection="Horizontal">
                <asp:ListItem Text="DANFE" Value="danfe" Selected="True"></asp:ListItem>
                <asp:ListItem Text="XML" Value="xml"></asp:ListItem>
                <asp:ListItem Text="DOWNLOAD DANFE" Value="downloaddanfe"></asp:ListItem>
            </asp:RadioButtonList>
            <br />
            <asp:TextBox runat="server" TextMode="MultiLine" Height="75" Width="339" ID="txtNotas"></asp:TextBox>&nbsp;&nbsp;
            <asp:Button runat="server" Text="Pesquisar" OnClick="OnClick" ValidationGroup="pesquisa" />
            <br /><br />

            <br />
            <asp:TextBox runat="server" TextMode="MultiLine" Height="75" Width="339" ID="txtNotasAtualizarInvoicy"></asp:TextBox>&nbsp;&nbsp;
            <asp:Button runat="server" id="btnAtualizarNotasInvoicy" Text="Atualizar Notas com Invoicy" OnClick="btnAtualizarNotasInvoicy_Click" />
            <br />
            <br />

            <asp:GridView ID="GridView1" runat="server" DataKeyNames="ultimoStatus"  AutoGenerateColumns="False" OnRowDataBound="GridView1_RowDataBound">
                <Columns>
                    <asp:BoundField DataField="numeronota" HeaderText="Nota" />
                    <asp:BoundField DataField="dataReceitaFederal" HeaderText="Data" />
                    <asp:TemplateField HeaderText="Link Danfe">
                        <ItemTemplate>
                            <asp:HyperLink ID="hplLinkDanfe" runat="server" Visible='<%# Eval("linkdanfe") != null%>' NavigateUrl='<%#Eval("linkdanfe") %>'>DANFE</asp:HyperLink>
                            <%# Eval("linkdanfe") == null ? "" : "" %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Status no Invoicy">
                        <ItemTemplate>
                            <asp:Label ID="lblStatusInvoicy" runat="server" Text=""></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
            <asp:Literal runat="server" ID="infoNotaXml"></asp:Literal>
           <br /><br />
            <br /><br /><br />
            <strong>Consultar UF do cliente</strong><br />
            NFe&nbsp;<asp:TextBox ID="txtNfe" runat="server"></asp:TextBox>&nbsp;&nbsp;<asp:Button ID="btConsultarUF" runat="server" Text="Consultar UF" OnClick="btConsultarUF_Click" /><br />

            <asp:Label ID="lblUF" runat="server" Text=""></asp:Label>


            <br />
            <br />
        </div>
    </form>
</body>
</html>
