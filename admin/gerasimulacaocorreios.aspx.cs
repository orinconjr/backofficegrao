﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class gerasimulacaocorreios : System.Web.UI.Page
{
    public class envios
    {

    }
    protected void Page_Load(object sender, EventArgs e)
    {
        var data = new dbCommerceDataContext();
        data.DeferredLoadingEnabled = false;
        var dataInicio = Convert.ToDateTime("01/01/2018");
        var dataFim = Convert.ToDateTime("01/02/2018");
        var envios = (from c in data.tbPedidoEnvios where c.dataFimEmbalagem >= dataInicio && c.dataFimEmbalagem < dataFim orderby c.dataFimEmbalagem select c).ToList();
        var transportadorasL = (from c in data.tbTipoDeEntregas where c.tipoDeEntregaId == 10 select c).ToList();
        var volumesGeralL = (from c in data.tbPedidoPacotes where c.tbPedidoEnvio.dataFimEmbalagem >= dataInicio && c.tbPedidoEnvio.dataFimEmbalagem < dataFim select c).ToList();
        var valoresL = (from c in data.tbPedidoEnvioValorTransportes where c.tbPedidoEnvio.dataFimEmbalagem >= dataInicio && c.tbPedidoEnvio.dataFimEmbalagem < dataFim select c).ToList();

        var pedidos = (from c in data.tbPedidoEnvios where c.dataFimEmbalagem >= dataInicio && c.dataFimEmbalagem < dataFim select c.tbPedido).ToList();

        data.Dispose();
        ConcurrentBag<tbPedidoEnvio> enviosBag = new ConcurrentBag<tbPedidoEnvio>(envios);


        ConcurrentBag<tbTipoDeEntrega> transportadoras = new ConcurrentBag<tbTipoDeEntrega>(transportadorasL);
        ConcurrentBag<tbPedidoPacote> volumesGeral = new ConcurrentBag<tbPedidoPacote>(volumesGeralL);
        ConcurrentBag<tbPedidoEnvioValorTransporte> valores = new ConcurrentBag<tbPedidoEnvioValorTransporte>(valoresL);


        //var precos = (from c in data.tbPedidoEnvioValorTransportes where c.tbPedidoEnvio.dataFimEmbalagem >= dataInicio && c.tbPedidoEnvio.dataFimEmbalagem < dataFim select c).ToList();
        int total = envios.Count;
        int atual = 0;
        Parallel.ForEach(envios, envio =>

        //foreach (var envio in envios)
        {
            atual++;
            var volumes = (from c in volumesGeral where c.idPedidoEnvio == envio.idPedidoEnvio select c).ToList();
            if (volumes.Count > 0)
            {
                foreach (var transportadora in transportadoras)
                {
                    var validavalor = (from c in valores where c.idPedidoEnvio == envio.idPedidoEnvio && c.servico.ToLower() == "tntibitinga" select c).Any();
                    if (!validavalor)
                    {
                        try
                        {
                            var pedido = pedidos.First(x => x.pedidoId == envio.idPedido);
                            var cepPedido = pedido.endCep;
                            var valorPedido = pedido.valorCobrado ?? 0;
                            int pesoTotal = volumes.Sum(x => x.peso);
                            var pacotes = new List<rnFrete2.Pacotes>();
                            pacotes = (from c in volumes
                                       select new rnFrete2.Pacotes
                                       {
                                           altura = Convert.ToInt32(c.altura),
                                           comprimento = Convert.ToInt32(c.profundidade),
                                           largura = Convert.ToInt32(c.largura),
                                           peso = c.peso
                                       }).ToList();
                            long cep = Convert.ToInt64(pedido.endCep.Replace("-", "").Replace(".", "").Replace(" ", "").Trim());
                            var frete = rnFrete2.Instance;

                            var consultaCorreios = frete.CalculaFrete(transportadora.tipoDeEntregaId, pedido.endCep.Replace("-", "").Trim(), valorPedido, pacotes);
                            bool result = false;

                            Database db = DatabaseFactory.CreateDatabase();

                            DbCommand dbCommand = db.GetStoredProcCommand("admin_inserevalortransporte");
                            decimal valor = consultaCorreios.valor;
                            if (valor > 0) valor = valor + ((valor / 100) * 15);
                            db.AddInParameter(dbCommand, "idPedidoEnvio", DbType.Int32, envio.idPedidoEnvio);
                            db.AddInParameter(dbCommand, "servico", DbType.String, "tntibitinga");
                            db.AddInParameter(dbCommand, "valor", DbType.Double, valor);
                            db.AddInParameter(dbCommand, "prazo", DbType.Int32, consultaCorreios.prazo);

                            using (DbConnection connection = db.CreateConnection())
                            {
                                connection.Open();
                                DbTransaction transaction = connection.BeginTransaction();

                                try
                                {
                                    db.ExecuteNonQuery(dbCommand, transaction);

                                    transaction.Commit();

                                    result = true;
                                }
                                catch (Exception exc)
                                {
                                    //throw new Exception(e.Message);

                                }
                                connection.Close();

                            }
                        }
                        catch (Exception exee)
                        {
                            Response.Write(exee.ToString());
                        }
                        //dataCadastro.admin_inserevalortransporte(, , , );                     
                    }
                }
            }
        //};
        });

        Response.Write("fim");
    }
}