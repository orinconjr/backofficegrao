﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

public partial class calculoFrete : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

/*
        var data = new dbCommerceDataContext();
        var inicio = Convert.ToDateTime("01/07/2017");
        var fim = Convert.ToDateTime("01/08/2017");
        var estoques = (from c in data.tbProdutoEstoques
                        join d in data.tbJuncaoProdutoCategorias on c.produtoId equals d.produtoId into categorias
                        where c.tbPedidoEnvio.dataFimEmbalagem > inicio && c.tbPedidoEnvio.dataFimEmbalagem < fim && categorias.Count(x => x.categoriaId == 413) > 0
                        select c.tbPedidoEnvio).Distinct().ToList();
        decimal totalTnt = 0;
        decimal totalCorreios = 0;
        decimal totalPlimor = 0;
        decimal totalJadlog = 0;

        decimal totalTntAntes = 0;
        decimal totalCorreiosAntes = 0;
        decimal totalPlimorAntes = 0;
        decimal totalJadlogAntes = 0;

        decimal totalNota = 0;
        int totalEnvios = 0;

        foreach (var estoque in estoques)
        {
            var pacotesEnvio = (from c in data.tbPedidoPacotes where c.idPedidoEnvio == estoque.idPedidoEnvio select c).ToList();
            if (pacotesEnvio.Any() && estoque.formaDeEnvio != null)
            {
                totalEnvios++;
                totalNota += estoque.nfeValor ?? 0;
                int pesoNormal = pacotesEnvio.Sum(x => x.peso);
                var pacotes = new List<rnFrete.Pacotes>();
                foreach (var pacoteEnvio in pacotesEnvio)
                {
                    var pacote = new rnFrete.Pacotes()
                    {
                        altura = Convert.ToDecimal(pacoteEnvio.altura),
                        comprimento = Convert.ToDecimal(pacoteEnvio.profundidade),
                        largura = Convert.ToDecimal(pacoteEnvio.largura),
                        peso = Convert.ToInt32(pacoteEnvio.peso)
                    };
                    pacotes.Add(pacote);
                }
                var valoresJadlogAntes = rnFrete.CalculaFrete(13, estoque.tbPedido.endCep.Replace("-", "").Trim(), estoque.nfeValor ?? 1, pacotes);
                decimal valorTotalCorreiosAntes = 0;
                foreach (var pacote in pacotes)
                {
                    decimal valorDoPacote = estoque.nfeValor ?? 1 / pacotes.Count();
                    var consultaCorreios = rnFrete.CalculaFrete(12, Convert.ToInt32(pacote.peso),
                        estoque.tbPedido.endCep.Replace("-", "").Trim(), valorDoPacote,
                        Convert.ToDecimal((pacote.largura)), Convert.ToDecimal((pacote.altura)),
                        Convert.ToDecimal((pacote.comprimento)));
                    valorTotalCorreiosAntes += consultaCorreios.valor;
                }


                var pacoteRemover = pacotes.OrderBy(x => x.peso).FirstOrDefault();
                if (pacoteRemover != null)
                {
                    int pesoPacoteRemover = pacoteRemover.peso;
                    pacotes.Remove(pacoteRemover);
                    var pacote = new rnFrete.Pacotes()
                    {
                        altura = Convert.ToDecimal(66),
                        comprimento = Convert.ToDecimal(37),
                        largura = Convert.ToDecimal(40),
                        peso = Convert.ToInt32(pesoPacoteRemover)
                    };
                    pacotes.Add(pacote);
                }

                var valoresJadlog = rnFrete.CalculaFrete(13, estoque.tbPedido.endCep.Replace("-", "").Trim(), estoque.nfeValor ?? 1, pacotes);
                var valoresTnt = rnFrete.CalculaFrete(10, Convert.ToInt32(pesoNormal), estoque.tbPedido.endCep.Replace("-", "").Trim(), estoque.nfeValor ?? 1, 0, 0, 0);
                decimal valorTotalCorreios = 0;
                foreach (var pacote in pacotes)
                {
                    decimal valorDoPacote = estoque.nfeValor ?? 1 / pacotes.Count();
                    var consultaCorreios = rnFrete.CalculaFrete(12, Convert.ToInt32(pacote.peso),
                        estoque.tbPedido.endCep.Replace("-", "").Trim(), valorDoPacote,
                        Convert.ToDecimal((pacote.largura)), Convert.ToDecimal((pacote.altura)),
                        Convert.ToDecimal((pacote.comprimento)));
                    valorTotalCorreios += consultaCorreios.valor;
                }

                var valoresPlimor = rnFrete.CalculaFrete(19, Convert.ToInt32(pesoNormal), estoque.tbPedido.endCep.Replace("-", "").Trim(), estoque.nfeValor ?? 1, 0, 0, 0);

                

                /*if (estoque.formaDeEnvio.ToLower() == "pac") totalCorreiosAntes += estoque.valorSelecionado ?? 0;
                if (estoque.formaDeEnvio.ToLower() == "tnt") totalTntAntes += estoque.valorSelecionado ?? 0;
                if (estoque.formaDeEnvio.ToLower() == "jadlog") totalJadlogAntes += estoque.valorSelecionado ?? 0;
                if (estoque.formaDeEnvio.ToLower() == "plimor") totalPlimorAntes += estoque.valorSelecionado ?? 0;*/

        /*

                if (valoresJadlog.valor > 0 &&
                    valoresJadlog.valor < (valoresTnt.valor == 0 ? 99999 : valoresTnt.valor) &&
                    valoresJadlog.valor < (valorTotalCorreios == 0 ? 99999 : valorTotalCorreios) &&
                    valoresJadlog.valor < (valoresPlimor.valor == 0 ? 99999 : valoresPlimor.valor))
                {
                    totalJadlog += valoresJadlog.valor;
                }

                if (valoresTnt.valor > 0 &&
                    valoresTnt.valor < (valoresJadlog.valor == 0 ? 99999 : valoresJadlog.valor) &&
                    valoresTnt.valor < (valorTotalCorreios == 0 ? 99999 : valorTotalCorreios) &&
                    valoresTnt.valor < (valoresPlimor.valor == 0 ? 99999 : valoresPlimor.valor))
                {
                    totalTnt += valoresTnt.valor;
                }

                if (valorTotalCorreios > 0 &&
                    valorTotalCorreios < (valoresTnt.valor == 0 ? 99999 : valoresTnt.valor) &&
                    valorTotalCorreios < (valoresJadlog.valor == 0 ? 99999 : valoresJadlog.valor) &&
                    valorTotalCorreios < (valoresPlimor.valor == 0 ? 99999 : valoresPlimor.valor))
                {
                    totalCorreios += valorTotalCorreios;
                }

                if (valoresPlimor.valor > 0 &&
                    valoresPlimor.valor < (valoresTnt.valor == 0 ? 99999 : valoresTnt.valor) &&
                    valoresPlimor.valor < (valorTotalCorreios == 0 ? 99999 : valorTotalCorreios) &&
                    valoresPlimor.valor < (valoresJadlog.valor == 0 ? 99999 : valoresJadlog.valor))
                {
                    totalPlimor += valoresPlimor.valor;
                }



                if (valoresJadlogAntes.valor > 0 &&
                    valoresJadlogAntes.valor < (valoresTnt.valor == 0 ? 99999 : valoresTnt.valor) &&
                    valoresJadlogAntes.valor < (valorTotalCorreios == 0 ? 99999 : valorTotalCorreiosAntes) &&
                    valoresJadlogAntes.valor < (valoresPlimor.valor == 0 ? 99999 : valoresPlimor.valor))
                {
                    totalJadlogAntes += valoresJadlogAntes.valor;
                }

                if (valoresTnt.valor > 0 &&
                    valoresTnt.valor < (valoresJadlog.valor == 0 ? 99999 : valoresJadlogAntes.valor) &&
                    valoresTnt.valor < (valorTotalCorreios == 0 ? 99999 : valorTotalCorreiosAntes) &&
                    valoresTnt.valor < (valoresPlimor.valor == 0 ? 99999 : valoresPlimor.valor))
                {
                    totalTntAntes += valoresTnt.valor;
                }

                if (valorTotalCorreiosAntes > 0 &&
                    valorTotalCorreiosAntes < (valoresTnt.valor == 0 ? 99999 : valoresTnt.valor) &&
                    valorTotalCorreiosAntes < (valoresJadlog.valor == 0 ? 99999 : valoresJadlogAntes.valor) &&
                    valorTotalCorreiosAntes < (valoresPlimor.valor == 0 ? 99999 : valoresPlimor.valor))
                {
                    totalCorreiosAntes += valorTotalCorreiosAntes;
                }

                if (valoresPlimor.valor > 0 &&
                    valoresPlimor.valor < (valoresTnt.valor == 0 ? 99999 : valoresTnt.valor) &&
                    valoresPlimor.valor < (valorTotalCorreios == 0 ? 99999 : valorTotalCorreiosAntes) &&
                    valoresPlimor.valor < (valoresJadlog.valor == 0 ? 99999 : valoresJadlogAntes.valor))
                {
                    totalPlimorAntes += valoresPlimor.valor;
                }
            }
        }

        Response.Write("totalTnt: " +  totalTnt + "<br>");
        Response.Write("totalCorreios: " + totalCorreios + "<br>");
        Response.Write("totalPlimor: " + totalPlimor + "<br>");
        Response.Write("totalJadlog: " + totalJadlog + "<br>");
        decimal totalAgora = totalTnt + totalCorreios + totalPlimor + totalJadlog;
        Response.Write("totalAgora: " + totalAgora + "<br>");

        Response.Write("tototal totalTntAntes: " + totalTntAntes + "<br>");
        Response.Write("tototal totalCorreiosAntes: " + totalCorreiosAntes + "<br>");
        Response.Write("tototal totalPlimorAntes: " + totalPlimorAntes + "<br>");
        Response.Write("tototal totalJadlogAntes: " + totalJadlogAntes + "<br>");
        decimal totalAntes = totalTntAntes + totalCorreiosAntes + totalPlimorAntes + totalJadlogAntes;
        Response.Write("tototal totalAntes: " + totalAntes + "<br>");


        Response.Write("tototal totalNota: " + totalNota + "<br>");
        Response.Write("tototal totalEnvios: " + totalEnvios + "<br>");*/
    }

    protected void btnCalcular_Click(object sender, EventArgs e)
    {


        decimal altura = Convert.ToDecimal(txtAltura.Text);
        decimal largura = Convert.ToDecimal(txtLargura.Text);
        decimal comprimento = Convert.ToDecimal(txtComprimento.Text);

        long cep = Convert.ToInt64(txtCep.Text.Replace("-", ""));
        int peso = Convert.ToInt32(txtPeso.Text.Replace("-", ""));
        decimal pesoEmKg = Convert.ToDecimal(peso) / 1000;
        txtValores.Text = "";

        decimal valorExpressa = 0;
        decimal valorPackage = 0;
        decimal valorRodo = 0;
        decimal valorCom = 0;

        try
        {
            var serviceJad = new serviceJadFrete.ValorFreteBeanService();
            var valoresPackage = rnFrete.CalculaFrete(13, Convert.ToInt32(Convert.ToInt32(txtPeso.Text)), txtCep.Text.Replace("-", "").Trim(), Convert.ToDecimal(txtValor.Text), altura, largura, comprimento);
            var retornoExpressa = serviceJad.valorar(0, "L2f0M1e6", "N", "100,00", "0,00", "14910000", cep.ToString().Replace("-", "").Replace(" ", ""), pesoEmKg.ToString("0.00"), "N", "D", "10924051000163");
            var retornoPackage = serviceJad.valorar(3, "L2f0M1e6", "N", "100,00", "0,00", "14910000", cep.ToString().Replace("-", "").Replace(" ", ""), pesoEmKg.ToString("0.00"), "N", "D", "10924051000163");
            var retornoRodo = serviceJad.valorar(4, "L2f0M1e6", "N", "100,00", "0,00", "14910000", cep.ToString().Replace("-", "").Replace(" ", ""), pesoEmKg.ToString("0.00"), "N", "D", "10924051000163");
            var retornCom = serviceJad.valorar(9, "L2f0M1e6", "N", "100,00", "0,00", "14910000", cep.ToString().Replace("-", "").Replace(" ", ""), pesoEmKg.ToString("0.00"), "N", "D", "10924051000163");
            //var retornoServiceJad = serviceJad.valorar(modalidadeJad, "M2c0F1m4", "N", valorNota, "0,00", "14910000", cep.ToString().Replace("-", "").Replace(" ", ""), pesoEmKg.ToString("0.00"), "N", "D", "20907518000110");
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(retornoExpressa);
            XmlNodeList parentNode = xmlDoc.GetElementsByTagName("Jadlog_Valor_Frete");
            foreach (XmlNode childrenNode in parentNode)
            {
                string retorno = childrenNode.ChildNodes[1].InnerText;
                valorExpressa = Convert.ToDecimal(retorno);
            }

            xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(retornoPackage);
            parentNode = xmlDoc.GetElementsByTagName("Jadlog_Valor_Frete");
            foreach (XmlNode childrenNode in parentNode)
            {
                string retorno = childrenNode.ChildNodes[1].InnerText;
                valorPackage = Convert.ToDecimal(retorno);
            }

            xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(retornoRodo);
            parentNode = xmlDoc.GetElementsByTagName("Jadlog_Valor_Frete");
            foreach (XmlNode childrenNode in parentNode)
            {
                string retorno = childrenNode.ChildNodes[1].InnerText;
                valorRodo = Convert.ToDecimal(retorno);
            }
            xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(retornCom);
            parentNode = xmlDoc.GetElementsByTagName("Jadlog_Valor_Frete");
            foreach (XmlNode childrenNode in parentNode)
            {
                string retorno = childrenNode.ChildNodes[1].InnerText;
                valorCom = Convert.ToDecimal(retorno);
            }

            txtValores.Text += "===================================================== Frete JadLog =====================================================<br><br>";

            txtValores.Text += "Expressa: " + valorExpressa.ToString("C") + "<br>";
            txtValores.Text += "Package: " + valorPackage.ToString("C") + "<br>";
            txtValores.Text += "COM: " + valorCom.ToString("C") + "<br>";
            txtValores.Text += "Rodo: " + valorRodo.ToString("C") + "<br>";
            txtValores.Text += "Package Tabela: " + valoresPackage.valor.ToString("C") + "<br>";
        }
        catch (Exception ex)
        {

            txtValores.Text += "<br>===================================================== Erro Frete JadLog =====================================================<br><br>";
            txtValores.Text += "Erro: " + ex.Message + "<br>";
        }


        if (!String.IsNullOrEmpty(txtValor.Text))
        {
            string cpfDestinatario = txtCpfDestinatario.Text;

            var serviceTnt = new serviceTntCalculoFrete.CalculoFrete();
            var parametros = new serviceTntCalculoFrete.CotacaoWebService();
            parametros.login = "atendimento@bark.com.br";
            parametros.senha = "";
            parametros.nrIdentifClienteRem = "10924051000163";
            parametros.nrInscricaoEstadualRemetente = "633459940118";
            parametros.nrIdentifClienteDest = cpfDestinatario;
            parametros.tpSituacaoTributariaRemetente = "ME";
            parametros.tpPessoaRemetente = "J";
            parametros.nrInscricaoEstadualDestinatario = "";
            parametros.tpPessoaDestinatario = "F";
            parametros.tpSituacaoTributariaDestinatario = "NC";
            parametros.cepOrigem = "14910000";
            parametros.cepDestino = cep.ToString().Replace("-", "").Replace(" ", "");
            parametros.vlMercadoria = txtValor.Text.Replace(",", ".");
            parametros.psReal = (Convert.ToDecimal(peso) / 1000).ToString("0.000").Replace(",", ".");
            parametros.tpServico = "RNC";
            parametros.tpFrete = "C";
            parametros.cdDivisaoCliente = 1;
            parametros.cdDivisaoClienteSpecified = true;
            int prazo = 0;
            decimal valor = 0;
            try
            {

                var calculoFrete = serviceTnt.calculaFrete(parametros);
                if (calculoFrete.errorList.Length > 0)
                {
                    for (int i = 0; i < calculoFrete.errorList.Length; i++)
                    {
                        txtValores.Text += "<br>===================================================== Erro Frete TNT =====================================================<br><br>";
                        txtValores.Text += "Erro: " + calculoFrete.errorList[i].ToString() + "<br>";
                    }

                }
                else
                {
                    txtValores.Text += "<br>===================================================== Frete TNT =====================================================<br><br>";
                    prazo = Convert.ToInt32(calculoFrete.prazoEntrega);
                    valor = Convert.ToDecimal(calculoFrete.vlTotalFrete.Replace(".", ","));
                    txtValores.Text += "Prazo: " + prazo + " dias - Valor: " + valor;
                }

            }
            catch { }
        }

        if (!String.IsNullOrEmpty(txtValor.Text))
        {
            try
            {
                int prazo = 0;
                decimal valor = 0;
                var valoresTnt = rnFrete.CalculaFrete(22, Convert.ToInt32(Convert.ToInt32(txtPeso.Text)), txtCep.Text.Replace("-", "").Trim(), Convert.ToDecimal(txtValor.Text), altura, largura, comprimento);
                
                    txtValores.Text += "<br>===================================================== Frete TNT 2=====================================================<br><br>";
                    prazo = Convert.ToInt32(valoresTnt.prazo);
                    valor = valoresTnt.valor;
                    txtValores.Text += "Prazo: " + prazo + " dias - Valor: " + valor;
                

            }
            catch { }
        }


        if (!String.IsNullOrEmpty(txtValor.Text))
        {
            try
            {
                int prazo = 0;
                decimal valor = 0;
                var valoresTnt = rnFrete.CalculaFrete(26, Convert.ToInt32(Convert.ToInt32(txtPeso.Text)), txtCep.Text.Replace("-", "").Trim(), Convert.ToDecimal(txtValor.Text), altura, largura, comprimento);

                txtValores.Text += "<br>===================================================== Dialogo =====================================================<br><br>";
                prazo = Convert.ToInt32(valoresTnt.prazo);
                valor = valoresTnt.valor;
                txtValores.Text += "Prazo: " + prazo + " dias - Valor: " + valor;
                foreach (var detalhe in valoresTnt.detalhamento)
                {
                    txtValores.Text += "<br>" + detalhe;
                }


            }
            catch { }
        }

        if (!String.IsNullOrEmpty(txtValor.Text))
        {
            try
            {
                int prazo = 0;
                decimal valor = 0;
                var valoresTnt = rnFrete.CalculaFrete(21, Convert.ToInt32(Convert.ToInt32(txtPeso.Text)), txtCep.Text.Replace("-", "").Trim(), Convert.ToDecimal(txtValor.Text), altura, largura, comprimento);

                txtValores.Text += "<br>===================================================== Plimor Jacutinga =====================================================<br><br>";
                prazo = Convert.ToInt32(valoresTnt.prazo);
                valor = valoresTnt.valor;
                txtValores.Text += "Prazo: " + prazo + " dias - Valor: " + valor;
                foreach (var detalhe in valoresTnt.detalhamento)
                {
                    txtValores.Text += "<br>" + detalhe;
                }


            }
            catch { }
        }


        if (!String.IsNullOrEmpty(txtValor.Text))
        {
            try
            {
                int prazo = 0;
                decimal valor = 0;
                var valoresTnt = rnFrete.CalculaFrete(20, Convert.ToInt32(Convert.ToInt32(txtPeso.Text)), txtCep.Text.Replace("-", "").Trim(), Convert.ToDecimal(txtValor.Text), altura, largura, comprimento);

                txtValores.Text += "<br>===================================================== Correios =====================================================<br><br>";
                prazo = Convert.ToInt32(valoresTnt.prazo);
                valor = valoresTnt.valor;
                txtValores.Text += "Prazo: " + prazo + " dias - Valor: " + valor;
                foreach (var detalhe in valoresTnt.detalhamento)
                {
                    txtValores.Text += "<br>" + detalhe;
                }


            }
            catch { }
        }


        if (!String.IsNullOrEmpty(txtValor.Text))
        {
            try
            {
                int prazo = 0;
                decimal valor = 0;
                var valoresTnt = rnFrete.CalculaFrete(24, Convert.ToInt32(Convert.ToInt32(txtPeso.Text)), txtCep.Text.Replace("-", "").Trim(), Convert.ToDecimal(txtValor.Text), altura, largura, comprimento);

                txtValores.Text += "<br>===================================================== Correios Grandes Formatos =====================================================<br><br>";
                prazo = Convert.ToInt32(valoresTnt.prazo);
                valor = valoresTnt.valor;
                txtValores.Text += "Prazo: " + prazo + " dias - Valor: " + valor;
                foreach (var detalhe in valoresTnt.detalhamento)
                {
                    txtValores.Text += "<br>" + detalhe;
                }


            }
            catch { }
        }
    }
}