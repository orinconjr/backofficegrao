﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="calculoFrete.aspx.cs" Inherits="calculoFrete" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            CEP:<br />
            <asp:TextBox runat="server" ID="txtCep"></asp:TextBox><br />
            Largura:<br />
            <asp:TextBox runat="server" ID="txtLargura" Text="0"></asp:TextBox><br />
            Altura:<br />
            <asp:TextBox runat="server" ID="txtAltura" Text="0"></asp:TextBox><br />
            Comprimento:<br />
            <asp:TextBox runat="server" ID="txtComprimento" Text="0"></asp:TextBox><br />
            Peso:<br />
            <asp:TextBox runat="server" ID="txtPeso"></asp:TextBox><br />
            Valor Pedido:<br />
            <asp:TextBox runat="server" ID="txtValor"></asp:TextBox><br />
            Cpf Destinatario:<br />
            <asp:TextBox runat="server" ID="txtCpfDestinatario" Text="00000000000"></asp:TextBox><br />
            <asp:Button runat="server" ID="btnCalcular" Text="Calcular" OnClick="btnCalcular_Click" /><br />
            <br />

            <asp:Literal runat="server" ID="txtValores"></asp:Literal>
        </div>
    </form>
</body>
</html>
