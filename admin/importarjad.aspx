﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="importarjad.aspx.cs" Inherits="importarjad" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxLoadingPanel" Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxCallback" Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxEditors" Assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
                <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
                    <tr>
                        <td class="textoPreto">
                            Selecione a planilha (Excel)
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:FileUpload ID="fluArquivo" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="textoPreto">
                            <asp:ImageButton ID="btnSalvar" runat="server" ImageAlign="AbsMiddle" ImageUrl="images/btSalvarPeq1.jpg" onclick="btnSalvar_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td class="textoPreto">
                            <dx:ASPxButton ID="btnEnviarGravar" runat="server" Text="Atualizar Relevância" AutoPostBack="False">
                                <ClientSideEvents Click="function(s, e) {
                                    cbGravar.PerformCallback();
                                    LoadingPanel.Show();
                                }" />
                            </dx:ASPxButton>
                            <dx:ASPxLoadingPanel ID="LoadingPanel" runat="server" ClientInstanceName="LoadingPanel" Modal="True" Text="Carregando... Por favor Aguarde&hellip;">
                            </dx:ASPxLoadingPanel>
                            <dx:ASPxCallback OnCallback="btnAtualizar_OnCallback" ID="cbGravar" runat="server" ClientInstanceName="cbGravar">
                                <ClientSideEvents CallbackComplete="function(s, e) { 
                                    LoadingPanel.Hide(); 
                                    grd.Refresh();
                                    }" />
                            </dx:ASPxCallback>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                    </tr>
                </table>
    </div>
    </form>
</body>
</html>
