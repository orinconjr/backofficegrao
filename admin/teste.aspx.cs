﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class teste : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        var data = new dbCommerceDataContext();
        var tabela = (from c in data.tbInformacaoAdcionals where c.informacaoAdcionalId == 834881 select c).First();
        var resultado = removeLinks(tabela.informacaoAdcionalConteudo);
        Response.Write(resultado);
    }

    public static string removeLinks(string html)
    {
        HtmlDocument wDocumento = new HtmlDocument();
        wDocumento.LoadHtml(html);
        if (wDocumento.DocumentNode.SelectNodes("//a[@href]") != null)
        {
            foreach (HtmlNode links in wDocumento.DocumentNode.SelectNodes("//a[@href]"))
            {
                RemoveElementKeepText(links);
            }
        }
        return wDocumento.DocumentNode.InnerHtml;
    }

    private static void RemoveElementKeepText(HtmlNode node)
    {
        //node.ParentNode.RemoveChild(node, true);
        HtmlNode parent = node.ParentNode;
        HtmlNode prev = node.PreviousSibling;
        HtmlNode next = node.NextSibling;

        foreach (HtmlNode child in node.ChildNodes)
        {
            if (prev != null)
                parent.InsertAfter(child, prev);
            else if (next != null)
                parent.InsertBefore(child, next);
            else
                parent.AppendChild(child);

        }
        node.Remove();
    }
}