﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class bidder : System.Web.UI.Page
{
    public class relatorioAgregado
    {
        public int sku { get; set; }
        public int categoria { get; set; }
        public decimal valor_item { get; set; }
        public decimal lucro_item { get; set; }
        public decimal conversoes_item { get; set; }
        public bool producao3d { get; set; }
        public int visualizacoes_lista_ga { get; set; }
        public int visualizacoes_detalhes_ga { get; set; }
        public int adicoes_carrinho_ga { get; set; }
        public decimal ctr_lista_ga
        {
            get
            {
                return visualizacoes_lista_ga == 0 ? 0 : Decimal.Round(Convert.ToDecimal(visualizacoes_detalhes_ga) * 100 / Convert.ToDecimal(visualizacoes_lista_ga), 2);
            }
        }
        public decimal taxa_carrinho_ga
        {
            get
            {
                return visualizacoes_detalhes_ga == 0 ? 0 : Decimal.Round(Convert.ToDecimal(adicoes_carrinho_ga) * 100 / Convert.ToDecimal(visualizacoes_detalhes_ga), 2);
            }
        }

        public int cliques_adwords { get; set; }
        public decimal custo_adwords { get; set; }
        public int impressoes_adwords { get; set; }
        public decimal conversoes_adwords { get; set; }
        public decimal ctr_adwords
        {
            get
            {
                return impressoes_adwords == 0 ? 0 : Decimal.Round(Convert.ToDecimal(cliques_adwords) * 100 / Convert.ToDecimal(impressoes_adwords), 2);
            }
        }
        public decimal cpc_adwords
        {
            get
            {
                return cliques_adwords == 0 ? 0 : Decimal.Round(Convert.ToDecimal(custo_adwords) / Convert.ToDecimal(cliques_adwords), 2);
            }
        }
        public decimal taxa_conversao_adwords
        {
            get
            {
                return cliques_adwords == 0 ? 0 : Decimal.Round(Convert.ToDecimal(conversoes_adwords) * 100 / Convert.ToDecimal(cliques_adwords), 2);
            }
        }
        public decimal cpa_adwords
        {
            get
            {
                return conversoes_adwords == 0 ? 0 : Decimal.Round(Convert.ToDecimal(custo_adwords) / Convert.ToDecimal(conversoes_adwords), 2);
            }
        }
        public decimal lucro_adwords
        {
            get
            {
                return lucro_item * conversoes_adwords;
            }
        }
        public int visualizacoes_lista_konduza { get; set; }
        public int cliques_konduza { get; set; }
        public int visualizacoes_detalhes_konduza { get; set; }
        public int visualizacoes_detalhes_unicos_konduza { get; set; }
        public int adicoes_carrinho_konduza { get; set; }
        public int compras_konduza { get; set; }
        public decimal lucro_konduza
        {
            get
            {
                return lucro_item * compras_konduza;
            }
        }
        public decimal ctr_lista_konduza
        {
            get
            {
                return visualizacoes_lista_konduza == 0 ? 0 : Decimal.Round(Convert.ToDecimal(cliques_konduza) * 100 / Convert.ToDecimal(visualizacoes_lista_konduza), 2);
            }
        }

        public decimal taxa_carrinho_konduza
        {
            get
            {
                return visualizacoes_detalhes_unicos_konduza == 0 ? 0 : Decimal.Round(Convert.ToDecimal(adicoes_carrinho_konduza) * 100 / Convert.ToDecimal(visualizacoes_detalhes_unicos_konduza), 2);
            }
        }
        public decimal taxa_compra_konduza
        {
            get
            {
                return visualizacoes_detalhes_unicos_konduza == 0 ? 0 : Decimal.Round(Convert.ToDecimal(conversoes_item) * 100 / Convert.ToDecimal(visualizacoes_detalhes_unicos_konduza), 2);
            }
        }

        public decimal deltaTaxaCarrinhoGa = 1;
        public decimal deltaTaxaCarrinhoKonduza = 1;

        public decimal taxa_carrinho_media
        {
            get
            {
                return Decimal.Round(((taxa_carrinho_ga * deltaTaxaCarrinhoGa) + (taxa_carrinho_konduza * deltaTaxaCarrinhoKonduza)) / 2, 2);
            }
        }


        public decimal deltaCtrGa = 1;
        public decimal deltaCtrAdwords = 1.5M;
        public decimal deltaCtrKonduza = 1;

        public decimal ctr_medio
        {
            get
            {
                return Decimal.Round(((ctr_adwords * deltaCtrAdwords) + (ctr_lista_konduza * deltaCtrKonduza)) / 2, 2);
            }
        }

        public decimal deltaTaxaCompraAdwords = 1;
        public decimal deltaTaxaCompraKonduza = 1.8M;

        public decimal taxa_conversao_media
        {
            get
            {
                return Decimal.Round(((taxa_conversao_adwords * deltaTaxaCompraAdwords) + (taxa_compra_konduza * deltaTaxaCompraKonduza)) / 2, 2);
            }
        }

        public decimal cliques_previstos { get; set; }
        public decimal conversoes_previstas { get; set; }
        public decimal receita_revista { get; set; }
        public decimal lucro_previsto { get; set; }
        public decimal cpc_previsto { get; set; }
    }


    public class relatorioFinal
    {
        public int sku { get; set; }
        public decimal valor_item { get; set; }
        public decimal lucro_item { get; set; }
        public string adGroup { get; set; }
        public int categoria { get; set; }

        public decimal ctr_medio { get; set; }
        public decimal taxa_conversao_media { get; set; }

        public decimal cliques_previstos_1d { get; set; }
        public decimal conversoes_previstas_1d { get; set; }
        public decimal receita_prevista_1d { get; set; }
        public decimal lucro_previsto_1d { get; set; }
        public decimal cpc_previsto_1d { get; set; }
        public decimal cpc_previsto_1d_ajustado { get; set; }

        public decimal cliques_previstos_7d { get; set; }
        public decimal conversoes_previstas_7d { get; set; }
        public decimal receita_prevista_7d { get; set; }
        public decimal lucro_previsto_7d { get; set; }
        public decimal cpc_previsto_7d { get; set; }
        public decimal cpc_previsto_7d_ajustado { get; set; }

        public decimal cliques_previstos_15d { get; set; }
        public decimal conversoes_previstas_15d { get; set; }
        public decimal receita_prevista_15d { get; set; }
        public decimal lucro_previsto_15d { get; set; }
        public decimal cpc_previsto_15d { get; set; }
        public decimal cpc_previsto_15d_ajustado { get; set; }

        public decimal cliques_previstos_30d { get; set; }
        public decimal conversoes_previstas_30d { get; set; }
        public decimal receita_prevista_30d { get; set; }
        public decimal lucro_previsto_30d { get; set; }
        public decimal cpc_previsto_30d { get; set; }
        public decimal cpc_previsto_30d_ajustado { get; set; }



        public decimal cliques_previstos_90d { get; set; }
        public decimal conversoes_previstas_90d { get; set; }
        public decimal receita_prevista_90d { get; set; }
        public decimal lucro_previsto_90d { get; set; }
        public decimal cpc_previsto_90d { get; set; }
        public decimal cpc_previsto_90d_ajustado { get; set; }


        public decimal cpc_medio_previsto { get; set; }
        public decimal indice_ajuste { get; set; }
        public decimal cpc_previsto_ajustado { get; set; }
    }

    public class adGroup
    {
        public int categoriaId { get; set; }
        public string adGroupName { get; set; }
    }
    public class shopping_ga
    {
        public int produtoId { get; set; }
        public int visualizacoes_lista_ga { get; set; }
        public int visualizacoes_detalhes_ga { get; set; }
        public int adicoes_carrinho_ga { get; set; }
    }
    public class shopping_adwords
    {
        public int produtoId { get; set; }
        public int cliques_adwords { get; set; }
        public int impressoes_adwords { get; set; }
        public decimal custo_adwords { get; set; }
        public decimal conversoes_adwords { get; set; }
    }

    public class konduza
    {
        public int produtoId { get; set; }
        public int visualizacoes_lista_konduza { get; set; }
        public int cliques_konduza { get; set; }
        public int visualizacoes_detalhes_konduza { get; set; }
        public int visualizacoes_detalhes_unicos_konduza { get; set; }
        public int adicoes_carrinho_konduza { get; set; }
        public int compras_konduza { get; set; }
    }
    protected void Page_Load(object sender, EventArgs e)
    {

        //campanha_kit_berco();
        //campanha_produtos();

        var listaGrupos = new List<adGroup>();
   
        listaGrupos.Add(new adGroup { categoriaId = 413, adGroupName = "kit-berco" });
        bid_list(listaGrupos, "kit-berco", 3);



        listaGrupos.Add(new adGroup { categoriaId = 1001, adGroupName = "berco" });
        listaGrupos.Add(new adGroup { categoriaId = 990, adGroupName = "colchao-berco-e-colchonete" });
        bid_list(listaGrupos, "berco", 3);

        listaGrupos = new List<adGroup>();
        listaGrupos.Add(new adGroup { categoriaId = 990, adGroupName = "colchao" });
        listaGrupos.Add(new adGroup { categoriaId = 1424, adGroupName = "mini-cama" });
        listaGrupos.Add(new adGroup { categoriaId = 1003, adGroupName = "guarda-roupas" });
        listaGrupos.Add(new adGroup { categoriaId = 1002, adGroupName = "comodas" });
        listaGrupos.Add(new adGroup { categoriaId = 1005, adGroupName = "quarto" });
        listaGrupos.Add(new adGroup { categoriaId = 1004, adGroupName = "cama-auxiliar" });
        listaGrupos.Add(new adGroup { categoriaId = 1251, adGroupName = "poltronas" });
        listaGrupos.Add(new adGroup { categoriaId = 761, adGroupName = "prateleiras" });
        listaGrupos.Add(new adGroup { categoriaId = 1459, adGroupName = "cabanas" });
        listaGrupos.Add(new adGroup { categoriaId = 1617, adGroupName = "mesas" });
        bid_list(listaGrupos, "moveis", 3);

        listaGrupos = new List<adGroup>();
        listaGrupos.Add(new adGroup { categoriaId = 279, adGroupName = "cortina-quarto-de-bebe" });
        listaGrupos.Add(new adGroup { categoriaId = 412, adGroupName = "quarto-de-bebe" });
        listaGrupos.Add(new adGroup { categoriaId = 414, adGroupName = "kit-cama-baba" });
        listaGrupos.Add(new adGroup { categoriaId = 415, adGroupName = "tapete-quarto-de-bebe" });
        listaGrupos.Add(new adGroup { categoriaId = 519, adGroupName = "abajur-quarto-de-bebe" });
        listaGrupos.Add(new adGroup { categoriaId = 521, adGroupName = "almofada-quarto-de-bebe" });
        listaGrupos.Add(new adGroup { categoriaId = 522, adGroupName = "saia-berco" });
        listaGrupos.Add(new adGroup { categoriaId = 523, adGroupName = "bolsa-maternidade" });
        listaGrupos.Add(new adGroup { categoriaId = 525, adGroupName = "mosquiteiro-bebe" });
        listaGrupos.Add(new adGroup { categoriaId = 526, adGroupName = "toalha-bebe-e-roupao-bebe" });
        listaGrupos.Add(new adGroup { categoriaId = 608, adGroupName = "decoracao-quarto-de-bebe-pano-pelucia" });
        listaGrupos.Add(new adGroup { categoriaId = 609, adGroupName = "lencol-berco-e-minicama" });
        listaGrupos.Add(new adGroup { categoriaId = 613, adGroupName = "porta-fraldas-quarto-de-bebe" });
        listaGrupos.Add(new adGroup { categoriaId = 620, adGroupName = "trocador" });
        listaGrupos.Add(new adGroup { categoriaId = 637, adGroupName = "porta-maternidade" });
        listaGrupos.Add(new adGroup { categoriaId = 638, adGroupName = "almofada-amamentacao" });
        listaGrupos.Add(new adGroup { categoriaId = 640, adGroupName = "mobile-quarto-de-bebe" });
        listaGrupos.Add(new adGroup { categoriaId = 641, adGroupName = "capas-carrinho-cadeira-bebe" });
        listaGrupos.Add(new adGroup { categoriaId = 642, adGroupName = "fraldas-bebe" });
        listaGrupos.Add(new adGroup { categoriaId = 644, adGroupName = "cabide-roupinha-bebe" });
        listaGrupos.Add(new adGroup { categoriaId = 679, adGroupName = "quarto-de-bebe-sem-cama-baba" });
        listaGrupos.Add(new adGroup { categoriaId = 777, adGroupName = "naninha-bebe" });
        listaGrupos.Add(new adGroup { categoriaId = 778, adGroupName = "lencol-cama-baba" });
        listaGrupos.Add(new adGroup { categoriaId = 780, adGroupName = "lixeiras-quarto-de-bebe" });
        listaGrupos.Add(new adGroup { categoriaId = 781, adGroupName = "cesta-quarto-de-bebe" });
        listaGrupos.Add(new adGroup { categoriaId = 782, adGroupName = "potes-quarto-de-bebe" });
        listaGrupos.Add(new adGroup { categoriaId = 783, adGroupName = "farmacinha-quarto-de-bebe" });
        listaGrupos.Add(new adGroup { categoriaId = 785, adGroupName = "quadros-quarto-de-bebe" });
        listaGrupos.Add(new adGroup { categoriaId = 786, adGroupName = "ursinho" });
        listaGrupos.Add(new adGroup { categoriaId = 788, adGroupName = "boneca-de-pano-e-boneco-de-pano" });
        listaGrupos.Add(new adGroup { categoriaId = 789, adGroupName = "decoracao-madeira-quarto-de-bebe" });
        listaGrupos.Add(new adGroup { categoriaId = 790, adGroupName = "kit-acessorios-quarto-de-bebe" });
        listaGrupos.Add(new adGroup { categoriaId = 791, adGroupName = "kit-higiene-quarto-de-bebe" });
        listaGrupos.Add(new adGroup { categoriaId = 792, adGroupName = "porta-chupeta" });
        listaGrupos.Add(new adGroup { categoriaId = 871, adGroupName = "kit-berco-desmontavel" });
        listaGrupos.Add(new adGroup { categoriaId = 937, adGroupName = "manta-bebe-e-cueiro" });
        listaGrupos.Add(new adGroup { categoriaId = 938, adGroupName = "porta-bebe" });
        listaGrupos.Add(new adGroup { categoriaId = 939, adGroupName = "cobertor-bebe-e-edredom" });
        listaGrupos.Add(new adGroup { categoriaId = 941, adGroupName = "babador-bebe" });
        listaGrupos.Add(new adGroup { categoriaId = 944, adGroupName = "varao-para-cortina-e-porta-fraldas" });
        listaGrupos.Add(new adGroup { categoriaId = 946, adGroupName = "prateleira-completa-quarto-de-bebe" });
        listaGrupos.Add(new adGroup { categoriaId = 947, adGroupName = "saia-cama-baba" });
        listaGrupos.Add(new adGroup { categoriaId = 948, adGroupName = "saia-berco-desmontavel" });
        listaGrupos.Add(new adGroup { categoriaId = 950, adGroupName = "lencol-berco-desmontavel" });
        listaGrupos.Add(new adGroup { categoriaId = 951, adGroupName = "porta-treco-quarto-de-bebe" });
        listaGrupos.Add(new adGroup { categoriaId = 952, adGroupName = "porta-cotonete-quarto-de-bebe" });
        listaGrupos.Add(new adGroup { categoriaId = 953, adGroupName = "lencol-carrinho-bebe" });
        listaGrupos.Add(new adGroup { categoriaId = 955, adGroupName = "travesseiro-berco" });
        listaGrupos.Add(new adGroup { categoriaId = 969, adGroupName = "pantufas-bebe" });
        listaGrupos.Add(new adGroup { categoriaId = 970, adGroupName = "lembrancinha-maternidade" });
        listaGrupos.Add(new adGroup { categoriaId = 971, adGroupName = "aparador-porta-quarto-de-bebe" });
        listaGrupos.Add(new adGroup { categoriaId = 973, adGroupName = "rolinho-protetor-bebe" });
        listaGrupos.Add(new adGroup { categoriaId = 991, adGroupName = "quarto-de-bebe-gemeos" });
        listaGrupos.Add(new adGroup { categoriaId = 993, adGroupName = "quarto-de-bebe-gemeos-sem-cama-baba" });
        listaGrupos.Add(new adGroup { categoriaId = 994, adGroupName = "kit-berco-para-gemeos" });
        listaGrupos.Add(new adGroup { categoriaId = 995, adGroupName = "fronha-berco-bebe" });
        listaGrupos.Add(new adGroup { categoriaId = 1007, adGroupName = "protetor-pescoco-bebe" });
        listaGrupos.Add(new adGroup { categoriaId = 1035, adGroupName = "roupinhas" });
        listaGrupos.Add(new adGroup { categoriaId = 1056, adGroupName = "puffs" });
        listaGrupos.Add(new adGroup { categoriaId = 1090, adGroupName = "adesivo-parede-quarto-de-bebe" });
        listaGrupos.Add(new adGroup { categoriaId = 1142, adGroupName = "kit-mini-berco" });
        listaGrupos.Add(new adGroup { categoriaId = 1265, adGroupName = "dossel-berco" });
        listaGrupos.Add(new adGroup { categoriaId = 1385, adGroupName = "quarto-montessoriano-de-bebe" });
        listaGrupos.Add(new adGroup { categoriaId = 1456, adGroupName = "kit-mini-cama-montessoriano" });
        listaGrupos.Add(new adGroup { categoriaId = 1460, adGroupName = "fio-de-luz-e-varal-decorativo" });
        listaGrupos.Add(new adGroup { categoriaId = 1461, adGroupName = "mochilas-maternidade" });
        listaGrupos.Add(new adGroup { categoriaId = 1614, adGroupName = "montessoriano" });
        listaGrupos.Add(new adGroup { categoriaId = 1738, adGroupName = "kit-cama-kids" });
        listaGrupos.Add(new adGroup { categoriaId = 1793, adGroupName = "papel-de-parede" });




        bid_list(listaGrupos, "produtos", 3);
    }


    private void bid_list(List<adGroup> listaGrupos, string nomeCampanha, int roiEsperado)
    {
        var data = new dbCommerceDataContext();
        var produtosKit = (from c in data.tbJuncaoProdutoCategorias where listaGrupos.Select(x => x.categoriaId).Contains(c.categoriaId) && c.tbProduto.produtoAtivo.ToLower() == "true" && c.tbProduto.produtoEstoqueAtual > 0 select c.tbProduto).Distinct().ToList();
        var produtosCategorias = (from c in data.tbJuncaoProdutoCategorias where listaGrupos.Select(x => x.categoriaId).Contains(c.categoriaId) && c.tbProduto.produtoAtivo.ToLower() == "true" && c.tbProduto.produtoEstoqueAtual > 0 select c).ToList();
        var produtosIds = produtosKit.Select(x => x.produtoId).Distinct().ToList();

        ConcurrentBag<relatorioFinal> relatorioFinal = new ConcurrentBag<relatorioFinal>();


        Parallel.ForEach(produtosKit, produto =>
        {
            var relatorioItem = new relatorioFinal();
            relatorioItem.adGroup = listaGrupos.First(x => x.categoriaId == produtosCategorias.First(y => y.produtoId == produto.produtoId).categoriaId).adGroupName;
            relatorioItem.sku = produto.produtoId;
            relatorioFinal.Add(relatorioItem);
        });
        FileInfo file_konduza_new_d_kit_berco = new FileInfo("c:\\bid_manager\\konduza-new-d-" + nomeCampanha + ".csv");
        if (file_konduza_new_d_kit_berco.Exists)
        {
            file_konduza_new_d_kit_berco.Delete();
        }
        using (StreamWriter swRelatorio = file_konduza_new_d_kit_berco.CreateText())
        {

            swRelatorio.WriteLine("Campaign	Ad Group\tProduct Group\tMax CPC");
            foreach (var produto in relatorioFinal)
            {
                swRelatorio.WriteLine("konduza-new-d-" + nomeCampanha + "\t" + produto.adGroup + "\t* / Item ID='" + produto.sku + "'\t" + (0.50M).ToString("0.00").Replace(",", "."));
            }
        }

        FileInfo file_konduza_new_m_kit_berco = new FileInfo("c:\\bid_manager\\konduza-new-m-" + nomeCampanha + ".csv");
        if (file_konduza_new_m_kit_berco.Exists)
        {
            file_konduza_new_m_kit_berco.Delete();
        }
        using (StreamWriter swRelatorio = file_konduza_new_m_kit_berco.CreateText())
        {

            swRelatorio.WriteLine("Campaign	Ad Group\tProduct Group\tMax CPC");
            foreach (var produto in relatorioFinal)
            {
                swRelatorio.WriteLine("konduza-new-m-" + nomeCampanha + "\t" + produto.adGroup + "\t* / Item ID='" + produto.sku + "'\t" + (0.50M).ToString("0.00").Replace(",", "."));
            }
        }
    }


    private void bid_generator(List<adGroup> listaGrupos, string nomeCampanha, int roiEsperado)
    {
        var data = new dbCommerceDataContext();
        var produtosKit = (from c in data.tbJuncaoProdutoCategorias where listaGrupos.Select(x => x.categoriaId).Contains(c.categoriaId) && c.tbProduto.produtoAtivo.ToLower() == "true" && c.tbProduto.produtoEstoqueAtual > 0 select c.tbProduto).Distinct().ToList();
        var produtosCategorias = (from c in data.tbJuncaoProdutoCategorias where listaGrupos.Select(x => x.categoriaId).Contains(c.categoriaId) && c.tbProduto.produtoAtivo.ToLower() == "true" && c.tbProduto.produtoEstoqueAtual > 0 select c).ToList();
        var produtosIds = produtosKit.Select(x => x.produtoId).Distinct().ToList();

        ConcurrentBag<relatorioAgregado> relatorio1 = new ConcurrentBag<relatorioAgregado>();
        ConcurrentBag<relatorioAgregado> relatorio7 = new ConcurrentBag<relatorioAgregado>();
        ConcurrentBag<relatorioAgregado> relatorio15 = new ConcurrentBag<relatorioAgregado>();
        ConcurrentBag<relatorioAgregado> relatorio30 = new ConcurrentBag<relatorioAgregado>();
        ConcurrentBag<relatorioAgregado> relatorio90 = new ConcurrentBag<relatorioAgregado>();
        ConcurrentBag<relatorioFinal> relatorioFinal = new ConcurrentBag<relatorioFinal>();

        List<int> periodos = new List<int>();
        periodos.Add(1);
        periodos.Add(7);
        periodos.Add(15);
        periodos.Add(30);
        periodos.Add(90);
        foreach (var periodo in periodos)
        {
            var dias = DateTime.Now.AddDays(periodo * (-1));
            var vendasKitL = (from c in data.tbJuncaoProdutoCategorias
                              join d in data.tbItensPedidos on c.produtoId equals d.produtoId
                              where listaGrupos.Select(x => x.categoriaId).Contains(c.categoriaId) &&
                              c.tbProduto.produtoAtivo.ToLower() == "true" &&
                              c.tbProduto.produtoEstoqueAtual > 0
                              && d.dataDaCriacao > dias
                              select d).Distinct().ToList();
            ConcurrentBag<tbItensPedido> vendasKit = new ConcurrentBag<tbItensPedido>(vendasKitL);
            List<shopping_ga> shopping_ga = new List<bidder.shopping_ga>();
            List<shopping_adwords> shopping_adwords = new List<bidder.shopping_adwords>();
            List<konduza> konduza = new List<bidder.konduza>();
            using (var reader = new StreamReader(@"C:\bid_manager\shopping_ga_" + periodo + "d.csv"))
            {
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Split(';');
                    int produtoId = 0;
                    int.TryParse(values[0], out produtoId);
                    if (produtoId > 0)
                    {
                        shopping_ga.Add(new bidder.shopping_ga() {
                            produtoId = produtoId,
                            adicoes_carrinho_ga = Convert.ToInt32(values[3].Replace(".", "")),
                            visualizacoes_detalhes_ga = Convert.ToInt32(values[2].Replace(".", "")),
                            visualizacoes_lista_ga = Convert.ToInt32(values[1].Replace(".", ""))
                        });
                    }
                }
            }

            using (var reader = new StreamReader(@"C:\bid_manager\shopping_adwords_" + periodo + "d.csv"))
            {
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Split(';');
                    int produtoId = 0;
                    int.TryParse(values[0], out produtoId);
                    if (produtoId > 0)
                    {
                        shopping_adwords.Add(new bidder.shopping_adwords()
                        {
                            produtoId = produtoId,
                            cliques_adwords = Convert.ToInt32(values[1].Replace(".", "").Trim()),
                            conversoes_adwords = Convert.ToDecimal(values[4].Replace("R$ ", "").Replace("\"", "")),
                            custo_adwords = Convert.ToDecimal(values[2].ToLower().Replace("r", "").Replace("$", "").Replace(" ", "").Replace("\"", "").Trim()),
                            impressoes_adwords = Convert.ToInt32(values[3].Replace(".", "").Trim())
                        });
                    }
                }
            }
            using (var reader = new StreamReader(@"C:\bid_manager\konduza_" + periodo + "d.csv"))
            {
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Split(';');
                    int produtoId = 0;
                    int.TryParse(values[0], out produtoId);
                    if (produtoId > 0)
                    {
                        konduza.Add(new bidder.konduza()
                        {
                            produtoId = produtoId,
                            visualizacoes_lista_konduza = Convert.ToInt32(values[1].Replace(".", "")),
                            cliques_konduza = Convert.ToInt32(values[2].Replace(".", "")),
                            visualizacoes_detalhes_konduza = Convert.ToInt32(values[3].Replace(".", "")),
                            visualizacoes_detalhes_unicos_konduza = Convert.ToInt32(values[4].Replace(".", "")),
                            adicoes_carrinho_konduza = Convert.ToInt32(values[5].Replace(".", "")),
                            compras_konduza = Convert.ToInt32(values[8].Replace(".", ""))
                        });
                    }
                }
            }
            Parallel.ForEach(produtosKit, produto =>
            {
                var relatorioItem = new relatorioAgregado();
                relatorioItem.sku = produto.produtoId;
                relatorioItem.conversoes_item = vendasKit.Count(x => x.produtoId == produto.produtoId);
                relatorioItem.valor_item = (produto.produtoPrecoPromocional > 0 ? (produto.produtoPrecoPromocional ?? 0) : produto.produtoPreco);
                relatorioItem.lucro_item = Decimal.Round((produto.produtoPrecoPromocional > 0 ? (produto.produtoPrecoPromocional ?? 0) : produto.produtoPreco) - (produto.produtoPrecoDeCusto ?? 0), 2);
                relatorioItem.producao3d = produto.criacaoPropria ?? false;
                relatorioItem.categoria = produtosCategorias.First(y => y.produtoId == produto.produtoId).categoriaId;

                var shopping_ga_item = shopping_ga.FirstOrDefault(x => x.produtoId == produto.produtoId);
                if(shopping_ga_item != null)
                {
                    relatorioItem.adicoes_carrinho_ga = shopping_ga_item.adicoes_carrinho_ga;
                    relatorioItem.visualizacoes_detalhes_ga = shopping_ga_item.visualizacoes_detalhes_ga;
                    relatorioItem.visualizacoes_lista_ga = shopping_ga_item.visualizacoes_lista_ga;
                }

                var shopping_adwords_item = shopping_adwords.FirstOrDefault(x => x.produtoId == produto.produtoId);
                if (shopping_adwords_item != null)
                {
                    relatorioItem.cliques_adwords = shopping_adwords_item.cliques_adwords;
                    relatorioItem.conversoes_adwords = shopping_adwords_item.conversoes_adwords;
                    relatorioItem.custo_adwords = shopping_adwords_item.custo_adwords;
                    relatorioItem.impressoes_adwords = shopping_adwords_item.impressoes_adwords;
                }

                var konduza_item = konduza.FirstOrDefault(x => x.produtoId == produto.produtoId);
                if (konduza_item != null)
                {
                    relatorioItem.visualizacoes_lista_konduza = konduza_item.visualizacoes_lista_konduza;
                    relatorioItem.cliques_konduza = konduza_item.cliques_konduza;
                    relatorioItem.visualizacoes_detalhes_konduza = konduza_item.visualizacoes_detalhes_konduza;
                    relatorioItem.visualizacoes_detalhes_unicos_konduza = konduza_item.visualizacoes_detalhes_unicos_konduza;
                    relatorioItem.adicoes_carrinho_konduza = konduza_item.adicoes_carrinho_konduza;
                    relatorioItem.compras_konduza = konduza_item.compras_konduza;
                }


                if (periodo == 1) relatorio1.Add(relatorioItem);
                if (periodo == 7) relatorio7.Add(relatorioItem);
                if (periodo == 15) relatorio15.Add(relatorioItem);
                if (periodo == 30) relatorio30.Add(relatorioItem);
                if (periodo == 90) relatorio90.Add(relatorioItem);
            });

            /*using (var reader = new StreamReader(@"C:\bid_manager\shopping_ga_" + periodo + "d.csv"))
            {
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Split(';');
                    int produtoId = 0;
                    int.TryParse(values[0], out produtoId);
                    if (produtoId > 0)
                    {

                        var relatorioItem = (periodo == 1 ? relatorio1 : periodo == 7 ? relatorio7 : periodo == 15 ? relatorio15 : periodo == 30 ? relatorio30 : relatorio90).FirstOrDefault(x => x.sku == produtoId);
                        if (relatorioItem != null)
                        {
                            relatorioItem.visualizacoes_lista_ga = Convert.ToInt32(values[1].Replace(".", ""));
                            relatorioItem.visualizacoes_detalhes_ga = Convert.ToInt32(values[2].Replace(".", ""));
                            relatorioItem.adicoes_carrinho_ga = Convert.ToInt32(values[3].Replace(".", ""));
                        }
                    }
                }
            }*/


            /*using (var reader = new StreamReader(@"C:\bid_manager\shopping_adwords_" + periodo + "d.csv"))
            {
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Split(';');
                    int produtoId = 0;
                    int.TryParse(values[0], out produtoId);
                    if (produtoId > 0)
                    {

                        var relatorioItem = (periodo == 1 ? relatorio1 : periodo == 7 ? relatorio7 : periodo == 15 ? relatorio15 : periodo == 30 ? relatorio30 : relatorio90).FirstOrDefault(x => x.sku == produtoId);
                        if (relatorioItem != null)
                        {
                            relatorioItem.cliques_adwords = Convert.ToInt32(values[1].Replace(".", ""));
                            relatorioItem.custo_adwords = Convert.ToDecimal(values[2].ToLower().Replace("r", "").Replace("$", "").Replace(" ", "").Replace("\"", ""));
                            relatorioItem.impressoes_adwords = Convert.ToInt32(values[3].Replace(".", ""));
                            relatorioItem.conversoes_adwords = Convert.ToDecimal(values[4].Replace("R$ ", "").Replace("\"", ""));
                        }
                    }
                }
            }*/

            /*using (var reader = new StreamReader(@"C:\bid_manager\konduza_" + periodo + "d.csv"))
            {
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Split(';');
                    int produtoId = 0;
                    int.TryParse(values[0], out produtoId);
                    if (produtoId > 0)
                    {

                        var relatorioItem = (periodo == 1 ? relatorio1 : periodo == 7 ? relatorio7 : periodo == 15 ? relatorio15 : periodo == 30 ? relatorio30 : relatorio90).FirstOrDefault(x => x.sku == produtoId);
                        if (relatorioItem != null)
                        {
                            relatorioItem.visualizacoes_lista_konduza = Convert.ToInt32(values[1].Replace(".", ""));
                            relatorioItem.cliques_konduza = Convert.ToInt32(values[2].Replace(".", ""));
                            relatorioItem.visualizacoes_detalhes_konduza = Convert.ToInt32(values[3].Replace(".", ""));
                            relatorioItem.visualizacoes_detalhes_unicos_konduza = Convert.ToInt32(values[4].Replace(".", ""));
                            relatorioItem.adicoes_carrinho_konduza = Convert.ToInt32(values[5].Replace(".", ""));
                            relatorioItem.compras_konduza = Convert.ToInt32(values[8].Replace(".", ""));

                        }
                    }
                }
            }*/

            Parallel.ForEach((periodo == 1 ? relatorio1 : periodo == 7 ? relatorio7 : periodo == 15 ? relatorio15 : periodo == 30 ? relatorio30 : relatorio90), produto =>
            {
                var totalViewsAdwords = (periodo == 1 ? relatorio1 : periodo == 7 ? relatorio7 : periodo == 15 ? relatorio15 : periodo == 30 ? relatorio30 : relatorio90).Where(x => x.categoria == produto.categoria).Sum(x => x.impressoes_adwords);
                produto.cliques_previstos = (totalViewsAdwords / 100) * produto.ctr_medio;
                produto.receita_revista = ((produto.cliques_previstos / 100) * produto.taxa_conversao_media) * produto.valor_item;
                produto.lucro_previsto = ((produto.cliques_previstos / 100) * produto.taxa_conversao_media) * produto.lucro_item;
                produto.cpc_previsto = produto.cliques_previstos == 0 ? 0 : (produto.receita_revista) / ((produto.cliques_previstos) * roiEsperado);
            });

            FileInfo relatorioFile = new FileInfo("c:\\bid_manager\\" + nomeCampanha + "relatorio_full_" + periodo + ".csv");
            if (relatorioFile.Exists)
            {
                relatorioFile.Delete();
            }
            using (StreamWriter swRelatorio = relatorioFile.CreateText())
            {

                swRelatorio.WriteLine("sku;lucro_item;producao3d;" + "visualizacoes_lista_ga;" + "visualizacoes_detalhes_ga;" + "adicoes_carrinho_ga;" + "ctr_lista_ga;" + "taxa_carrinho_ga;" + "cliques_adwords;" + "custo_adwords;" + "impressoes_adwords;" + "conversoes_adwords;" + "ctr_adwords; cpc_adwords; taxa_conversao_adwords; cpa_adwords;lucro_adwords;" + "visualizacoes_lista_konduza;" + "cliques_konduza;" + "visualizacoes_detalhes_konduza;" + "visualizacoes_detalhes_unicos_konduza;" + "adicoes_carrinho_konduza;" + "compras_konduza;ctr_lista_konduza;taxa_carrinho_konduza;taxa_compra_konduza;lucro_konduza;taxa_carrinho_media;ctr_medio;taxa_conversao_media;cliques_previstos;receita_revista;lucro_previsto;cpc_previsto;");
                foreach (var produto in (periodo == 1 ? relatorio1 : periodo == 7 ? relatorio7 : periodo == 15 ? relatorio15 : periodo == 30 ? relatorio30 : relatorio90))
                {
                    swRelatorio.WriteLine(produto.sku + ";" + produto.lucro_item + ";" + (produto.producao3d ? "s" : "n") + ";" + produto.visualizacoes_lista_ga + ";" + produto.visualizacoes_detalhes_ga + ";" + produto.adicoes_carrinho_ga + ";" + produto.ctr_lista_ga + ";" + produto.taxa_carrinho_ga + ";" + produto.cliques_adwords + ";" + produto.custo_adwords + ";" + produto.impressoes_adwords + ";" + produto.conversoes_adwords + ";" + produto.ctr_adwords + ";" + produto.cpc_adwords + ";" + produto.taxa_conversao_adwords + ";" + produto.cpa_adwords + ";" + produto.lucro_adwords + ";" + produto.visualizacoes_lista_konduza + ";" + produto.cliques_konduza + ";" + produto.visualizacoes_detalhes_konduza + ";" + produto.visualizacoes_detalhes_unicos_konduza + ";" + produto.adicoes_carrinho_konduza + ";" + produto.compras_konduza + ";" + produto.ctr_lista_konduza + ";" + produto.taxa_carrinho_konduza + ";" + produto.taxa_compra_konduza + ";" + produto.lucro_konduza + ";" + produto.taxa_carrinho_media + ";" + produto.ctr_medio + ";" + produto.taxa_conversao_media + ";" + produto.cliques_previstos + ";" + produto.receita_revista + ";" + produto.lucro_previsto + ";" + produto.cpc_previsto + ";");
                }
            }
        }

        Parallel.ForEach(produtosKit, produto =>
        {
            var item1d = relatorio1.First(x => x.sku == produto.produtoId);
            var item7d = relatorio7.First(x => x.sku == produto.produtoId);
            var item15d = relatorio15.First(x => x.sku == produto.produtoId);
            var item30d = relatorio30.First(x => x.sku == produto.produtoId);
            var item90d = relatorio90.First(x => x.sku == produto.produtoId);
            decimal deltaCtr1d = 0.03M;
            decimal deltaCtr7d = 0.07M;
            decimal deltaCtr15d = 0.1M;
            decimal deltaCtr30d = 0.3M;
            decimal deltaCtr90d = 0.5M;

            decimal deltaTaxa1d = 0.03M;
            decimal deltaTaxa7d = 0.07M;
            decimal deltaTaxa15d = 0.1M;
            decimal deltaTaxa30d = 0.3M;
            decimal deltaTaxa90d = 0.5M;

            decimal deltaIndiceReal1d = Convert.ToDecimal("0,03");
            decimal deltaIndiceReal7d = Convert.ToDecimal("0,07");
            decimal deltaIndiceReal15d = Convert.ToDecimal("0,1");
            decimal deltaIndiceReal30d = Convert.ToDecimal("0,3");
            decimal deltaIndiceReal90d = Convert.ToDecimal("0,5");


            var relatorioItem = new relatorioFinal();
            relatorioItem.categoria = produtosCategorias.First(y => y.produtoId == produto.produtoId).categoriaId;
            var vendaMaxima1 = relatorio1.Where(x => x.categoria == relatorioItem.categoria).OrderByDescending(x => x.receita_revista).First().receita_revista;
            var vendaMaxima7 = relatorio7.Where(x => x.categoria == relatorioItem.categoria).OrderByDescending(x => x.receita_revista).First().receita_revista;
            var vendaMaxima15 = relatorio15.Where(x => x.categoria == relatorioItem.categoria).OrderByDescending(x => x.receita_revista).First().receita_revista;
            var vendaMaxima30 = relatorio30.Where(x => x.categoria == relatorioItem.categoria).OrderByDescending(x => x.receita_revista).First().receita_revista;
            var vendaMaxima90 = relatorio90.Where(x => x.categoria == relatorioItem.categoria).OrderByDescending(x => x.receita_revista).First().receita_revista;
            var vendaMaxima1real = relatorio1.Where(x => x.categoria == relatorioItem.categoria).OrderByDescending(x => (x.valor_item * x.conversoes_item)).Select(x => (x.valor_item * x.conversoes_item)).First();
            var vendaMaxima7real = relatorio7.Where(x => x.categoria == relatorioItem.categoria).OrderByDescending(x => (x.valor_item * x.conversoes_item)).Select(x => (x.valor_item * x.conversoes_item)).First();
            var vendaMaxima15real = relatorio15.Where(x => x.categoria == relatorioItem.categoria).OrderByDescending(x => (x.valor_item * x.conversoes_item)).Select(x => (x.valor_item * x.conversoes_item)).First();
            var vendaMaxima30real = relatorio30.Where(x => x.categoria == relatorioItem.categoria).OrderByDescending(x => (x.valor_item * x.conversoes_item)).Select(x => (x.valor_item * x.conversoes_item)).First();
            var vendaMaxima90real = relatorio90.Where(x => x.categoria == relatorioItem.categoria).OrderByDescending(x => (x.valor_item * x.conversoes_item)).Select(x => (x.valor_item * x.conversoes_item)).First();
            decimal indiceAjuste1 = vendaMaxima1 == 0 ? Convert.ToDecimal("0") : (relatorio1.First(x => x.sku == produto.produtoId).receita_revista * 100) / vendaMaxima1;
            decimal indiceAjuste7 = vendaMaxima7 == 0 ? Convert.ToDecimal("0") : (relatorio7.First(x => x.sku == produto.produtoId).receita_revista * 100) / vendaMaxima7;
            decimal indiceAjuste15 = vendaMaxima15 == 0 ? Convert.ToDecimal("0") : (relatorio15.First(x => x.sku == produto.produtoId).receita_revista * 100) / vendaMaxima15;
            decimal indiceAjuste30 = vendaMaxima30 == 0 ? Convert.ToDecimal("0") : (relatorio30.First(x => x.sku == produto.produtoId).receita_revista * 100) / vendaMaxima30;
            decimal indiceAjuste90 = vendaMaxima90 == 0 ? Convert.ToDecimal("0") : (relatorio90.First(x => x.sku == produto.produtoId).receita_revista * 100) / vendaMaxima90;
            decimal indiceAjuste1real = vendaMaxima1real == 0 ? Convert.ToDecimal("0") : (relatorio1.Where(x => x.sku == produto.produtoId).Select(x => (x.valor_item * x.conversoes_item)).First() * 100) / vendaMaxima1real;
            decimal indiceAjuste7real = vendaMaxima7real == 0 ? Convert.ToDecimal("0") : (relatorio7.Where(x => x.sku == produto.produtoId).Select(x => (x.valor_item * x.conversoes_item)).First() * 100) / vendaMaxima7real;
            decimal indiceAjuste15real = vendaMaxima15real == 0 ? Convert.ToDecimal("0") : (relatorio15.Where(x => x.sku == produto.produtoId).Select(x => (x.valor_item * x.conversoes_item)).First() * 100) / vendaMaxima15real;
            decimal indiceAjuste30real = vendaMaxima30real == 0 ? Convert.ToDecimal("0") : (relatorio30.Where(x => x.sku == produto.produtoId).Select(x => (x.valor_item * x.conversoes_item)).First() * 100) / vendaMaxima30real;
            decimal indiceAjuste90real = vendaMaxima90real == 0 ? Convert.ToDecimal("0") : (relatorio90.Where(x => x.sku == produto.produtoId).Select(x => (x.valor_item * x.conversoes_item)).First() * 100) / vendaMaxima90real;

            decimal indiceReajusteMedio = (((indiceAjuste1 * deltaIndiceReal1d) + (indiceAjuste7 * deltaIndiceReal7d) + (indiceAjuste15 * deltaIndiceReal15d) + (indiceAjuste30 * deltaIndiceReal30d) + (indiceAjuste90 * deltaIndiceReal90d)));
            decimal indiceReajusteMedioReal = (((indiceAjuste1real * deltaIndiceReal1d) + (indiceAjuste7real * deltaIndiceReal7d) + (indiceAjuste15real * deltaIndiceReal15d) + (indiceAjuste30real * deltaIndiceReal30d) + (indiceAjuste90real * deltaIndiceReal90d)));
            decimal detalAjuste90 = Convert.ToDecimal("0,7");
            relatorioItem.indice_ajuste = ((indiceReajusteMedio * detalAjuste90) + indiceReajusteMedioReal) / 2;

            decimal ctr_medio_1d = item1d.ctr_medio;
            decimal ctr_medio_7d = item7d.ctr_medio;
            decimal ctr_medio_15d = item15d.ctr_medio;
            decimal ctr_medio_30d = item30d.ctr_medio;
            decimal ctr_medio_90d = item90d.ctr_medio;

            decimal ctr_geral_medio_1d = relatorio1.Where(x => x.ctr_medio > 0).Sum(x => x.ctr_medio) / relatorio1.Count(x => x.ctr_medio > 0);
            decimal ctr_geral_medio_7d = relatorio7.Where(x => x.ctr_medio > 0).Sum(x => x.ctr_medio) / relatorio7.Count(x => x.ctr_medio > 0);
            decimal ctr_geral_medio_15d = relatorio15.Where(x => x.ctr_medio > 0).Sum(x => x.ctr_medio) / relatorio15.Count(x => x.ctr_medio > 0);
            decimal ctr_geral_medio_30d = relatorio30.Where(x => x.ctr_medio > 0).Sum(x => x.ctr_medio) / relatorio30.Count(x => x.ctr_medio > 0);
            decimal ctr_geral_medio_90d = relatorio90.Where(x => x.ctr_medio > 0).Sum(x => x.ctr_medio) / relatorio90.Count(x => x.ctr_medio > 0);

            decimal visualizacoes_detalhes_1d = item1d.visualizacoes_detalhes_unicos_konduza;
            decimal visualizacoes_detalhes_7d = item7d.visualizacoes_detalhes_unicos_konduza;
            decimal visualizacoes_detalhes_15d = item15d.visualizacoes_detalhes_unicos_konduza;
            decimal visualizacoes_detalhes_30d = item30d.visualizacoes_detalhes_unicos_konduza;
            decimal visualizacoes_detalhes_90d = item90d.visualizacoes_detalhes_unicos_konduza;


            decimal visualizacoes_geral_detalhes = relatorio1.Where(x => x.categoria == relatorioItem.categoria).Where(x => x.visualizacoes_detalhes_unicos_konduza > 0).Sum(x => x.visualizacoes_detalhes_unicos_konduza) / relatorio1.Where(x => x.categoria == relatorioItem.categoria).Count(x => x.visualizacoes_detalhes_unicos_konduza > 0);
            decimal visualizacoes_geral_detalhes_7d = relatorio7.Where(x => x.categoria == relatorioItem.categoria).Where(x => x.visualizacoes_detalhes_unicos_konduza > 0).Sum(x => x.visualizacoes_detalhes_unicos_konduza) / relatorio7.Where(x => x.categoria == relatorioItem.categoria).Count(x => x.visualizacoes_detalhes_unicos_konduza > 0);
            decimal visualizacoes_geral_detalhes_15d = relatorio15.Where(x => x.categoria == relatorioItem.categoria).Where(x => x.visualizacoes_detalhes_unicos_konduza > 0).Sum(x => x.visualizacoes_detalhes_unicos_konduza) / relatorio15.Where(x => x.categoria == relatorioItem.categoria).Count(x => x.visualizacoes_detalhes_unicos_konduza > 0);
            decimal visualizacoes_geral_detalhes_30d = relatorio30.Where(x => x.categoria == relatorioItem.categoria).Where(x => x.visualizacoes_detalhes_unicos_konduza > 0).Sum(x => x.visualizacoes_detalhes_unicos_konduza) / relatorio30.Where(x => x.categoria == relatorioItem.categoria).Count(x => x.visualizacoes_detalhes_unicos_konduza > 0);
            decimal visualizacoes_geral_detalhes_90d = relatorio90.Where(x => x.categoria == relatorioItem.categoria).Where(x => x.visualizacoes_detalhes_unicos_konduza > 0).Sum(x => x.visualizacoes_detalhes_unicos_konduza) / relatorio90.Where(x => x.categoria == relatorioItem.categoria).Count(x => x.visualizacoes_detalhes_unicos_konduza > 0);

            if (ctr_medio_1d > ctr_geral_medio_1d && visualizacoes_detalhes_1d < visualizacoes_geral_detalhes)
            {
                ctr_medio_1d = relatorio1.Where(x => x.ctr_medio > 0 && x.visualizacoes_detalhes_unicos_konduza < visualizacoes_geral_detalhes).Sum(x => x.ctr_medio) / relatorio1.Count(x => x.ctr_medio > 0 && x.visualizacoes_detalhes_unicos_konduza < visualizacoes_geral_detalhes);
            }
            if (ctr_medio_7d > ctr_geral_medio_7d && visualizacoes_detalhes_7d < visualizacoes_geral_detalhes)
            {
                ctr_medio_7d = relatorio7.Where(x => x.ctr_medio > 0 && x.visualizacoes_detalhes_unicos_konduza < visualizacoes_geral_detalhes).Sum(x => x.ctr_medio) / relatorio7.Count(x => x.ctr_medio > 0 && x.visualizacoes_detalhes_unicos_konduza < visualizacoes_geral_detalhes);
            }
            if (ctr_medio_15d > ctr_geral_medio_15d && visualizacoes_detalhes_15d < visualizacoes_geral_detalhes)
            {
                ctr_medio_15d = relatorio15.Where(x => x.ctr_medio > 0 && x.visualizacoes_detalhes_unicos_konduza < visualizacoes_geral_detalhes).Sum(x => x.ctr_medio) / relatorio15.Count(x => x.ctr_medio > 0 && x.visualizacoes_detalhes_unicos_konduza < visualizacoes_geral_detalhes);
            }
            if (ctr_medio_30d > ctr_geral_medio_30d && visualizacoes_detalhes_30d < visualizacoes_geral_detalhes)
            {
                ctr_medio_30d = relatorio30.Where(x => x.ctr_medio > 0 && x.visualizacoes_detalhes_unicos_konduza < visualizacoes_geral_detalhes).Sum(x => x.ctr_medio) / relatorio30.Count(x => x.ctr_medio > 0 && x.visualizacoes_detalhes_unicos_konduza < visualizacoes_geral_detalhes);
            }
            if (ctr_medio_90d > ctr_geral_medio_90d && visualizacoes_detalhes_90d < visualizacoes_geral_detalhes)
            {
                ctr_medio_90d = relatorio90.Where(x => x.ctr_medio > 0 && x.visualizacoes_detalhes_unicos_konduza < visualizacoes_geral_detalhes).Sum(x => x.ctr_medio) / relatorio90.Count(x => x.ctr_medio > 0 && x.visualizacoes_detalhes_unicos_konduza < visualizacoes_geral_detalhes);
            }


            relatorioItem.ctr_medio = ((((item1d.ctr_medio * deltaCtr1d) + (item7d.ctr_medio * deltaCtr7d) + (item15d.ctr_medio * deltaCtr15d) + (item30d.ctr_medio * deltaCtr30d) + (item90d.ctr_medio * deltaCtr90d))) / 100) * relatorioItem.indice_ajuste;
            relatorioItem.taxa_conversao_media = ((((item1d.taxa_conversao_media * deltaTaxa1d) + (item7d.taxa_conversao_media * deltaTaxa7d) + (item15d.taxa_conversao_media * deltaTaxa15d) + (item30d.taxa_conversao_media * deltaTaxa30d) + (item90d.taxa_conversao_media * deltaTaxa90d))) / 100) * relatorioItem.indice_ajuste;
            relatorioItem.valor_item = (produto.produtoPrecoPromocional > 0 ? (produto.produtoPrecoPromocional ?? 0) : produto.produtoPreco);
            relatorioItem.lucro_item = Decimal.Round((produto.produtoPrecoPromocional > 0 ? (produto.produtoPrecoPromocional ?? 0) : produto.produtoPreco) - (produto.produtoPrecoDeCusto ?? 0), 2);
            relatorioItem.adGroup = listaGrupos.First(x => x.categoriaId == produtosCategorias.First(y => y.produtoId == produto.produtoId).categoriaId).adGroupName;



            var totalViewsAdwords1d = (relatorio1).Where(x => x.categoria == relatorioItem.categoria).Sum(x => x.impressoes_adwords);
            relatorioItem.cliques_previstos_1d = (totalViewsAdwords1d / 100) * relatorioItem.ctr_medio;
            relatorioItem.receita_prevista_1d = ((relatorioItem.cliques_previstos_1d / 100) * relatorioItem.taxa_conversao_media) * relatorioItem.valor_item;
            relatorioItem.lucro_previsto_1d = ((relatorioItem.cliques_previstos_1d / 100) * relatorioItem.taxa_conversao_media) * relatorioItem.lucro_item;
            relatorioItem.cpc_previsto_1d = relatorioItem.cliques_previstos_1d == 0 ? 0 : (relatorioItem.receita_prevista_1d) / ((relatorioItem.cliques_previstos_1d) * roiEsperado);


            var totalViewsAdwords7d = (relatorio7).Where(x => x.categoria == relatorioItem.categoria).Sum(x => x.impressoes_adwords);
            relatorioItem.cliques_previstos_7d = (totalViewsAdwords7d / 100) * relatorioItem.ctr_medio;
            relatorioItem.receita_prevista_7d = ((relatorioItem.cliques_previstos_7d / 100) * relatorioItem.taxa_conversao_media) * relatorioItem.valor_item;
            relatorioItem.lucro_previsto_7d = ((relatorioItem.cliques_previstos_7d / 100) * relatorioItem.taxa_conversao_media) * relatorioItem.lucro_item;
            relatorioItem.cpc_previsto_7d = relatorioItem.cliques_previstos_7d == 0 ? 0 : (relatorioItem.receita_prevista_7d) / ((relatorioItem.cliques_previstos_7d) * roiEsperado);


            var totalViewsAdwords15d = (relatorio15).Where(x => x.categoria == relatorioItem.categoria).Sum(x => x.impressoes_adwords);
            relatorioItem.cliques_previstos_15d = (totalViewsAdwords15d / 100) * relatorioItem.ctr_medio;
            relatorioItem.receita_prevista_15d = ((relatorioItem.cliques_previstos_15d / 100) * relatorioItem.taxa_conversao_media) * relatorioItem.valor_item;
            relatorioItem.lucro_previsto_15d = ((relatorioItem.cliques_previstos_15d / 100) * relatorioItem.taxa_conversao_media) * relatorioItem.lucro_item;
            relatorioItem.cpc_previsto_15d = relatorioItem.cliques_previstos_15d == 0 ? 0 : (relatorioItem.receita_prevista_15d) / ((relatorioItem.cliques_previstos_15d) * roiEsperado);


            var totalViewsAdwords30d = (relatorio30).Where(x => x.categoria == relatorioItem.categoria).Sum(x => x.impressoes_adwords);
            relatorioItem.cliques_previstos_30d = (totalViewsAdwords30d / 100) * relatorioItem.ctr_medio;
            relatorioItem.receita_prevista_30d = ((relatorioItem.cliques_previstos_30d / 100) * relatorioItem.taxa_conversao_media) * relatorioItem.valor_item;
            relatorioItem.lucro_previsto_30d = ((relatorioItem.cliques_previstos_30d / 100) * relatorioItem.taxa_conversao_media) * relatorioItem.lucro_item;
            relatorioItem.cpc_previsto_30d = relatorioItem.cliques_previstos_30d == 0 ? 0 : (relatorioItem.receita_prevista_30d) / ((relatorioItem.cliques_previstos_30d) * roiEsperado);


            var totalViewsAdwords90d = (relatorio90).Where(x => x.categoria == relatorioItem.categoria).Sum(x => x.impressoes_adwords);
            relatorioItem.cliques_previstos_90d = (totalViewsAdwords90d / 100) * relatorioItem.ctr_medio;
            relatorioItem.receita_prevista_90d = ((relatorioItem.cliques_previstos_90d / 100) * relatorioItem.taxa_conversao_media) * relatorioItem.valor_item;
            relatorioItem.lucro_previsto_90d = ((relatorioItem.cliques_previstos_90d / 100) * relatorioItem.taxa_conversao_media) * relatorioItem.lucro_item;
            relatorioItem.cpc_previsto_90d = relatorioItem.cliques_previstos_90d == 0 ? 0 : (relatorioItem.receita_prevista_90d) / ((relatorioItem.cliques_previstos_90d) * roiEsperado);


            relatorioItem.sku = produto.produtoId;
            relatorioFinal.Add(relatorioItem);
        });

        Parallel.ForEach(relatorioFinal, produto =>
        {


            int deltaCpc1d = 1;
            int deltaCpc7d = 1;
            int deltaCpc15d = 1;
            int deltaCpc30d = 1;
            int deltaCpc90d = 1;

            produto.cpc_medio_previsto = ((produto.cpc_previsto_1d * deltaCpc1d) + (produto.cpc_previsto_7d * deltaCpc7d) + (produto.cpc_previsto_15d * deltaCpc15d) + (produto.cpc_previsto_30d * deltaCpc30d) + (produto.cpc_previsto_90d * deltaCpc90d)) / 5;

        });

        decimal fatorDeCorrecao = 30;
        foreach (var categoria in listaGrupos)
        {
            int indice = 0;
            decimal cpcMinimoAdwords = relatorio90.Where(x => x.categoria == categoria.categoriaId).Where(x => x.cpc_adwords > 0 && x.cliques_adwords > 10).OrderBy(x => x.cpc_adwords).FirstOrDefault() == null ? 0 : relatorio90.Where(x => x.categoria == categoria.categoriaId).Where(x => x.cpc_adwords > 0 && x.cliques_adwords > 10).OrderBy(x => x.cpc_adwords).First().cpc_adwords;
            decimal cpcMaximoAdwords = relatorio90.Where(x => x.categoria == categoria.categoriaId).OrderByDescending(x => x.cpc_adwords).FirstOrDefault() == null ? 0 : cpcMaximoAdwords = relatorio90.Where(x => x.categoria == categoria.categoriaId).OrderByDescending(x => x.cpc_adwords).First().cpc_adwords;
            foreach (var produto in relatorioFinal.Where(x => x.categoria == categoria.categoriaId).OrderByDescending(x => x.cpc_medio_previsto))
            {
                var adwordsIndice = relatorio90.Where(x => x.categoria == categoria.categoriaId).Where(x => x.cpc_adwords > 0 && x.cliques_adwords > 10).OrderByDescending(x => x.cpc_adwords).Skip(indice).FirstOrDefault();
                if (adwordsIndice != null)
                {
                    produto.cpc_previsto_ajustado = adwordsIndice.cpc_adwords + ((adwordsIndice.cpc_adwords / 100) * fatorDeCorrecao);
                }
                else
                {
                    produto.cpc_previsto_ajustado = relatorioFinal.Where(x => x.categoria == categoria.categoriaId && x.cpc_previsto_ajustado > 0).OrderBy(x => x.cpc_previsto_ajustado).First().cpc_previsto_ajustado;
                }
                indice++;
            }
        }

        FileInfo relatorioFilefinal = new FileInfo("c:\\bid_manager\\" + nomeCampanha + "_relatorio_full_final.csv");
        if (relatorioFilefinal.Exists)
        {
            relatorioFilefinal.Delete();
        }
        using (StreamWriter swRelatorio = relatorioFilefinal.CreateText())
        {

            swRelatorio.WriteLine("sku;nome;valor_item;lucro_item;ctr_medio;taxa_conversao_media;cliques_previstos_1d;conversoes_previstas_1d;receita_prevista_1d;lucro_previsto_1d;cpc_previsto_1d;cliques_previstos_7d;conversoes_previstas_7d;receita_prevista_7d;lucro_previsto_7d;cpc_previsto_7d;cliques_previstos_15d;conversoes_previstas_15d;receita_prevista_15d;lucro_previsto_15d;cpc_previsto_15d;cliques_previstos_30d;conversoes_previstas_30d;receita_prevista_30d;lucro_previsto_30d;cpc_previsto_30d;cliques_previstos_90d;conversoes_previstas_90d;receita_prevista_90d;lucro_previsto_90d;cpc_previsto_90d;cpc_medio_previsto;indice_ajuste;cpc_previsto_ajustado;");
            foreach (var produto in relatorioFinal)
            {
                swRelatorio.WriteLine(produto.sku + ";" + produtosKit.First(x => x.produtoId == produto.sku).produtoNome.Replace(";", "") + ";" + produto.valor_item + ";" + (produto.lucro_item) + ";" + produto.ctr_medio + ";"
                    + produto.taxa_conversao_media + ";" + produto.cliques_previstos_1d + ";" + produto.conversoes_previstas_1d + ";" + produto.receita_prevista_1d + ";"
                    + produto.lucro_previsto_1d + ";" + produto.cpc_previsto_1d + ";" + produto.cliques_previstos_7d + ";" + produto.conversoes_previstas_7d + ";" + produto.receita_prevista_7d
                    + ";" + produto.lucro_previsto_7d + ";" + produto.cpc_previsto_7d + ";" + produto.cliques_previstos_15d + ";" + produto.conversoes_previstas_15d + ";"
                    + produto.receita_prevista_15d + ";" + produto.lucro_previsto_15d + ";" + produto.cpc_previsto_15d + ";" + produto.cliques_previstos_30d
                    + ";" + produto.conversoes_previstas_30d + ";" + produto.receita_prevista_30d + ";" + produto.lucro_previsto_30d + ";" + produto.cpc_previsto_30d
                    + ";" + produto.cliques_previstos_90d + ";" + produto.conversoes_previstas_90d + ";" + produto.receita_prevista_90d + ";" + produto.lucro_previsto_90d
                    + ";" + produto.cpc_previsto_90d + ";" + produto.cpc_medio_previsto + ";" + produto.indice_ajuste + ";" + produto.cpc_previsto_ajustado + ";");
            }
        }

        FileInfo file_konduza_new_d_kit_berco = new FileInfo("c:\\bid_manager\\konduza-new-d-" + nomeCampanha + ".csv");
        if (file_konduza_new_d_kit_berco.Exists)
        {
            file_konduza_new_d_kit_berco.Delete();
        }
        using (StreamWriter swRelatorio = file_konduza_new_d_kit_berco.CreateText())
        {

            swRelatorio.WriteLine("Campaign	Ad Group\tProduct Group\tMax CPC");
            foreach (var produto in relatorioFinal)
            {
                swRelatorio.WriteLine("konduza-new-d-" + nomeCampanha + "\t" + produto.adGroup + "\t* / Item ID='" + produto.sku + "'\t" + produto.cpc_previsto_ajustado.ToString("0.00").Replace(",", "."));
            }
        }

        FileInfo file_konduza_new_m_kit_berco = new FileInfo("c:\\bid_manager\\konduza-new-m-" + nomeCampanha + ".csv");
        if (file_konduza_new_m_kit_berco.Exists)
        {
            file_konduza_new_m_kit_berco.Delete();
        }
        using (StreamWriter swRelatorio = file_konduza_new_m_kit_berco.CreateText())
        {

            swRelatorio.WriteLine("Campaign	Ad Group\tProduct Group\tMax CPC");
            foreach (var produto in relatorioFinal)
            {
                swRelatorio.WriteLine("konduza-new-m-" + nomeCampanha + "\t" + produto.adGroup + "\t* / Item ID='" + produto.sku + "'\t" + produto.cpc_previsto_ajustado.ToString("0.00").Replace(",", "."));
            }
        }

        foreach (var categoria in listaGrupos)
        {
            Response.Write("<h1>" + categoria.adGroupName + "</h1><br>");
            foreach (var produto in relatorioFinal.Where(x => x.adGroup == categoria.adGroupName).OrderByDescending(x => x.cpc_previsto_ajustado).ToList())
            {
                Response.Write("<img src=\"http://cdn2.graodegente.com.br/fotos/" + produto.sku + "/media_" + produtosKit.First(x => x.produtoId == produto.sku).fotoDestaque + ".jpg\" /><br>" + produto.sku + " - " + produto.cpc_previsto_ajustado.ToString("C") + " - " + produto.cpc_medio_previsto.ToString("C") + " - " + produto.valor_item.ToString("C") + " - " + produtosKit.First(x => x.produtoId == produto.sku).produtoNome + "<br><br>");
            }
        }
    }

    private void campanha_produtos()
    {
        string nomeCampanha = "produtos";
        var listaGrupos = new List<adGroup>();
        listaGrupos.Add(new adGroup { categoriaId = 279, adGroupName = "cortina-quarto-de-bebe" });
        listaGrupos.Add(new adGroup { categoriaId = 412, adGroupName = "quarto-de-bebe" });
        listaGrupos.Add(new adGroup { categoriaId = 414, adGroupName = "kit-cama-baba" });
        listaGrupos.Add(new adGroup { categoriaId = 415, adGroupName = "tapete-quarto-de-bebe" });
        listaGrupos.Add(new adGroup { categoriaId = 519, adGroupName = "abajur-quarto-de-bebe" });
        listaGrupos.Add(new adGroup { categoriaId = 521, adGroupName = "almofada-quarto-de-bebe" });
        listaGrupos.Add(new adGroup { categoriaId = 522, adGroupName = "saia-berco" });
        listaGrupos.Add(new adGroup { categoriaId = 523, adGroupName = "bolsa-maternidade" });
        listaGrupos.Add(new adGroup { categoriaId = 525, adGroupName = "mosquiteiro-bebe" });
        listaGrupos.Add(new adGroup { categoriaId = 526, adGroupName = "toalha-bebe-e-roupao-bebe" });
        listaGrupos.Add(new adGroup { categoriaId = 608, adGroupName = "decoracao-quarto-de-bebe-pano-pelucia" });
        listaGrupos.Add(new adGroup { categoriaId = 609, adGroupName = "lencol-berco-e-minicama" });
        listaGrupos.Add(new adGroup { categoriaId = 613, adGroupName = "porta-fraldas-quarto-de-bebe" });
        listaGrupos.Add(new adGroup { categoriaId = 620, adGroupName = "trocador" });
        listaGrupos.Add(new adGroup { categoriaId = 637, adGroupName = "porta-maternidade" });
        listaGrupos.Add(new adGroup { categoriaId = 638, adGroupName = "almofada-amamentacao" });
        listaGrupos.Add(new adGroup { categoriaId = 640, adGroupName = "mobile-quarto-de-bebe" });
        listaGrupos.Add(new adGroup { categoriaId = 641, adGroupName = "capas-carrinho-cadeira-bebe" });
        listaGrupos.Add(new adGroup { categoriaId = 642, adGroupName = "fraldas-bebe" });
        listaGrupos.Add(new adGroup { categoriaId = 644, adGroupName = "cabide-roupinha-bebe" });
        listaGrupos.Add(new adGroup { categoriaId = 679, adGroupName = "quarto-de-bebe-sem-cama-baba" });
        listaGrupos.Add(new adGroup { categoriaId = 777, adGroupName = "naninha-bebe" });
        listaGrupos.Add(new adGroup { categoriaId = 778, adGroupName = "lencol-cama-baba" });
        listaGrupos.Add(new adGroup { categoriaId = 780, adGroupName = "lixeiras-quarto-de-bebe" });
        listaGrupos.Add(new adGroup { categoriaId = 781, adGroupName = "cesta-quarto-de-bebe" });
        listaGrupos.Add(new adGroup { categoriaId = 782, adGroupName = "potes-quarto-de-bebe" });
        listaGrupos.Add(new adGroup { categoriaId = 783, adGroupName = "farmacinha-quarto-de-bebe" });
        listaGrupos.Add(new adGroup { categoriaId = 785, adGroupName = "quadros-quarto-de-bebe" });
        listaGrupos.Add(new adGroup { categoriaId = 786, adGroupName = "ursinho" });
        listaGrupos.Add(new adGroup { categoriaId = 788, adGroupName = "boneca-de-pano-e-boneco-de-pano" });
        listaGrupos.Add(new adGroup { categoriaId = 789, adGroupName = "decoracao-madeira-quarto-de-bebe" });
        listaGrupos.Add(new adGroup { categoriaId = 790, adGroupName = "kit-acessorios-quarto-de-bebe" });
        listaGrupos.Add(new adGroup { categoriaId = 791, adGroupName = "kit-higiene-quarto-de-bebe" });
        listaGrupos.Add(new adGroup { categoriaId = 792, adGroupName = "porta-chupeta" });
        listaGrupos.Add(new adGroup { categoriaId = 871, adGroupName = "kit-berco-desmontavel" });
        listaGrupos.Add(new adGroup { categoriaId = 937, adGroupName = "manta-bebe-e-cueiro" });
        listaGrupos.Add(new adGroup { categoriaId = 938, adGroupName = "porta-bebe" });
        listaGrupos.Add(new adGroup { categoriaId = 939, adGroupName = "cobertor-bebe-e-edredom" });
        listaGrupos.Add(new adGroup { categoriaId = 941, adGroupName = "babador-bebe" });
        listaGrupos.Add(new adGroup { categoriaId = 944, adGroupName = "varao-para-cortina-e-porta-fraldas" });
        listaGrupos.Add(new adGroup { categoriaId = 946, adGroupName = "prateleira-completa-quarto-de-bebe" });
        listaGrupos.Add(new adGroup { categoriaId = 947, adGroupName = "saia-cama-baba" });
        listaGrupos.Add(new adGroup { categoriaId = 948, adGroupName = "saia-berco-desmontavel" });
        listaGrupos.Add(new adGroup { categoriaId = 950, adGroupName = "lencol-berco-desmontavel" });
        listaGrupos.Add(new adGroup { categoriaId = 951, adGroupName = "porta-treco-quarto-de-bebe" });
        listaGrupos.Add(new adGroup { categoriaId = 952, adGroupName = "porta-cotonete-quarto-de-bebe" });
        listaGrupos.Add(new adGroup { categoriaId = 953, adGroupName = "lencol-carrinho-bebe" });
        listaGrupos.Add(new adGroup { categoriaId = 955, adGroupName = "travesseiro-berco" });
        listaGrupos.Add(new adGroup { categoriaId = 969, adGroupName = "pantufas-bebe" });
        listaGrupos.Add(new adGroup { categoriaId = 970, adGroupName = "lembrancinha-maternidade" });
        listaGrupos.Add(new adGroup { categoriaId = 971, adGroupName = "aparador-porta-quarto-de-bebe" });
        listaGrupos.Add(new adGroup { categoriaId = 973, adGroupName = "rolinho-protetor-bebe" });
        listaGrupos.Add(new adGroup { categoriaId = 991, adGroupName = "quarto-de-bebe-gemeos" });
        listaGrupos.Add(new adGroup { categoriaId = 993, adGroupName = "quarto-de-bebe-gemeos-sem-cama-baba" });
        listaGrupos.Add(new adGroup { categoriaId = 994, adGroupName = "kit-berco-para-gemeos" });
        listaGrupos.Add(new adGroup { categoriaId = 995, adGroupName = "fronha-berco-bebe" });
        listaGrupos.Add(new adGroup { categoriaId = 1007, adGroupName = "protetor-pescoco-bebe" });
        listaGrupos.Add(new adGroup { categoriaId = 1035, adGroupName = "roupinhas" });
        listaGrupos.Add(new adGroup { categoriaId = 1056, adGroupName = "puffs" });
        listaGrupos.Add(new adGroup { categoriaId = 1090, adGroupName = "adesivo-parede-quarto-de-bebe" });
        listaGrupos.Add(new adGroup { categoriaId = 1142, adGroupName = "kit-mini-berco" });
        listaGrupos.Add(new adGroup { categoriaId = 1265, adGroupName = "dossel-berco" });
        listaGrupos.Add(new adGroup { categoriaId = 1385, adGroupName = "quarto-montessoriano-de-bebe" });
        listaGrupos.Add(new adGroup { categoriaId = 1456, adGroupName = "kit-mini-cama-montessoriano" });
        listaGrupos.Add(new adGroup { categoriaId = 1460, adGroupName = "fio-de-luz-e-varal-decorativo" });
        listaGrupos.Add(new adGroup { categoriaId = 1614, adGroupName = "montessoriano" });

        var data = new dbCommerceDataContext();
        var produtosKit = (from c in data.tbJuncaoProdutoCategorias where listaGrupos.Select(x => x.categoriaId).Contains(c.categoriaId) && c.tbProduto.produtoAtivo.ToLower() == "true" && c.tbProduto.produtoEstoqueAtual > 0 select c.tbProduto).Distinct().ToList();
        var produtosCategorias = (from c in data.tbJuncaoProdutoCategorias where listaGrupos.Select(x => x.categoriaId).Contains(c.categoriaId) && c.tbProduto.produtoAtivo.ToLower() == "true" && c.tbProduto.produtoEstoqueAtual > 0 select c).Distinct().ToList();
        var produtosIds = produtosKit.Select(x => x.produtoId).Distinct().ToList();

        List<relatorioAgregado> relatorio1 = new List<relatorioAgregado>();
        List<relatorioAgregado> relatorio7 = new List<relatorioAgregado>();
        List<relatorioAgregado> relatorio15 = new List<relatorioAgregado>();
        List<relatorioAgregado> relatorio30 = new List<relatorioAgregado>();
        List<relatorioAgregado> relatorio90 = new List<relatorioAgregado>();
        List<relatorioFinal> relatorioFinal = new List<relatorioFinal>();

        List<int> periodos = new List<int>();
        periodos.Add(1);
        periodos.Add(7);
        periodos.Add(15);
        periodos.Add(30);
        periodos.Add(90);
        foreach (var periodo in periodos)
        {
            var dias = DateTime.Now.AddDays(periodo * (-1));

            //var vendasKit = (from c in data.tbItensPedidos where c.produtoId == 0 select c).ToList();
            var vendasKit = (from c in data.tbJuncaoProdutoCategorias
                             join d in data.tbItensPedidos on c.produtoId equals d.produtoId
                             where listaGrupos.Select(x => x.categoriaId).Contains(c.categoriaId) &&
                             c.tbProduto.produtoAtivo.ToLower() == "true" &&
                             c.tbProduto.produtoEstoqueAtual > 0
                             && d.dataDaCriacao > dias
                             select d).Distinct().ToList();

            // var vendasKit = (from c in data.tbItensPedidos where produtosIds.Contains(c.produtoId) && c.dataDaCriacao > dias select c).ToList();
            foreach (var produto in produtosKit)
            {
                var relatorioItem = new relatorioAgregado();
                relatorioItem.sku = produto.produtoId;
                relatorioItem.conversoes_item = vendasKit.Count(x => x.produtoId == produto.produtoId);
                relatorioItem.valor_item = (produto.produtoPrecoPromocional > 0 ? (produto.produtoPrecoPromocional ?? 0) : produto.produtoPreco);
                relatorioItem.lucro_item = Decimal.Round((produto.produtoPrecoPromocional > 0 ? (produto.produtoPrecoPromocional ?? 0) : produto.produtoPreco) - (produto.produtoPrecoDeCusto ?? 0), 2);
                relatorioItem.producao3d = produto.criacaoPropria ?? false;
                if (periodo == 1) relatorio1.Add(relatorioItem);
                if (periodo == 7) relatorio7.Add(relatorioItem);
                if (periodo == 15) relatorio15.Add(relatorioItem);
                if (periodo == 30) relatorio30.Add(relatorioItem);
                if (periodo == 90) relatorio90.Add(relatorioItem);
            }

            using (var reader = new StreamReader(@"C:\bid_manager\shopping_ga_" + periodo + "d.csv"))
            {
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Split(';');
                    int produtoId = 0;
                    int.TryParse(values[0], out produtoId);
                    if (produtoId > 0)
                    {

                        var relatorioItem = (periodo == 1 ? relatorio1 : periodo == 7 ? relatorio7 : periodo == 15 ? relatorio15 : periodo == 30 ? relatorio30 : relatorio90).FirstOrDefault(x => x.sku == produtoId);
                        if (relatorioItem != null)
                        {
                            relatorioItem.visualizacoes_lista_ga = Convert.ToInt32(values[1].Replace(".", ""));
                            relatorioItem.visualizacoes_detalhes_ga = Convert.ToInt32(values[2].Replace(".", ""));
                            relatorioItem.adicoes_carrinho_ga = Convert.ToInt32(values[3].Replace(".", ""));
                        }
                    }
                }
            }


            using (var reader = new StreamReader(@"C:\bid_manager\shopping_adwords_" + periodo + "d.csv"))
            {
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Split(';');
                    int produtoId = 0;
                    int.TryParse(values[0], out produtoId);
                    if (produtoId > 0)
                    {

                        var relatorioItem = (periodo == 1 ? relatorio1 : periodo == 7 ? relatorio7 : periodo == 15 ? relatorio15 : periodo == 30 ? relatorio30 : relatorio90).FirstOrDefault(x => x.sku == produtoId);
                        if (relatorioItem != null)
                        {
                            relatorioItem.cliques_adwords = Convert.ToInt32(values[1].Replace(".", ""));
                            relatorioItem.custo_adwords = Convert.ToDecimal(values[2].ToLower().Replace("r", "").Replace("$", "").Replace(" ", "").Replace("\"", ""));
                            relatorioItem.impressoes_adwords = Convert.ToInt32(values[3].Replace(".", ""));
                            relatorioItem.conversoes_adwords = Convert.ToDecimal(values[4].Replace("R$ ", "").Replace("\"", ""));
                        }
                    }
                }
            }

            using (var reader = new StreamReader(@"C:\bid_manager\konduza_" + periodo + "d.csv"))
            {
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Split(';');
                    int produtoId = 0;
                    int.TryParse(values[0], out produtoId);
                    if (produtoId > 0)
                    {

                        var relatorioItem = (periodo == 1 ? relatorio1 : periodo == 7 ? relatorio7 : periodo == 15 ? relatorio15 : periodo == 30 ? relatorio30 : relatorio90).FirstOrDefault(x => x.sku == produtoId);
                        if (relatorioItem != null)
                        {
                            relatorioItem.visualizacoes_lista_konduza = Convert.ToInt32(values[1].Replace(".", ""));
                            relatorioItem.cliques_konduza = Convert.ToInt32(values[2].Replace(".", ""));
                            relatorioItem.visualizacoes_detalhes_konduza = Convert.ToInt32(values[3].Replace(".", ""));
                            relatorioItem.visualizacoes_detalhes_unicos_konduza = Convert.ToInt32(values[4].Replace(".", ""));
                            relatorioItem.adicoes_carrinho_konduza = Convert.ToInt32(values[5].Replace(".", ""));
                            relatorioItem.compras_konduza = Convert.ToInt32(values[8].Replace(".", ""));

                        }
                    }
                }
            }

            int roiEsperado = 3;
            foreach (var produto in (periodo == 1 ? relatorio1 : periodo == 7 ? relatorio7 : periodo == 15 ? relatorio15 : periodo == 30 ? relatorio30 : relatorio90))
            {
                var totalViewsAdwords = (periodo == 1 ? relatorio1 : periodo == 7 ? relatorio7 : periodo == 15 ? relatorio15 : periodo == 30 ? relatorio30 : relatorio90).Sum(x => x.impressoes_adwords);
                produto.cliques_previstos = (totalViewsAdwords / 100) * produto.ctr_medio;
                produto.receita_revista = ((produto.cliques_previstos / 100) * produto.taxa_conversao_media) * produto.valor_item;
                produto.lucro_previsto = ((produto.cliques_previstos / 100) * produto.taxa_conversao_media) * produto.lucro_item;
                produto.cpc_previsto = produto.cliques_previstos == 0 ? 0 : (produto.receita_revista) / ((produto.cliques_previstos) * roiEsperado);
            }

            FileInfo relatorioFile = new FileInfo("c:\\bid_manager\\" + nomeCampanha + "relatorio_full_" + periodo + ".csv");
            if (relatorioFile.Exists)
            {
                relatorioFile.Delete();
            }
            using (StreamWriter swRelatorio = relatorioFile.CreateText())
            {

                swRelatorio.WriteLine("sku;lucro_item;producao3d;" + "visualizacoes_lista_ga;" + "visualizacoes_detalhes_ga;" + "adicoes_carrinho_ga;" + "ctr_lista_ga;" + "taxa_carrinho_ga;" + "cliques_adwords;" + "custo_adwords;" + "impressoes_adwords;" + "conversoes_adwords;" + "ctr_adwords; cpc_adwords; taxa_conversao_adwords; cpa_adwords;lucro_adwords;" + "visualizacoes_lista_konduza;" + "cliques_konduza;" + "visualizacoes_detalhes_konduza;" + "visualizacoes_detalhes_unicos_konduza;" + "adicoes_carrinho_konduza;" + "compras_konduza;ctr_lista_konduza;taxa_carrinho_konduza;taxa_compra_konduza;lucro_konduza;taxa_carrinho_media;ctr_medio;taxa_conversao_media;cliques_previstos;receita_revista;lucro_previsto;cpc_previsto;");
                foreach (var produto in (periodo == 1 ? relatorio1 : periodo == 7 ? relatorio7 : periodo == 15 ? relatorio15 : periodo == 30 ? relatorio30 : relatorio90))
                {
                    swRelatorio.WriteLine(produto.sku + ";" + produto.lucro_item + ";" + (produto.producao3d ? "s" : "n") + ";" + produto.visualizacoes_lista_ga + ";" + produto.visualizacoes_detalhes_ga + ";" + produto.adicoes_carrinho_ga + ";" + produto.ctr_lista_ga + ";" + produto.taxa_carrinho_ga + ";" + produto.cliques_adwords + ";" + produto.custo_adwords + ";" + produto.impressoes_adwords + ";" + produto.conversoes_adwords + ";" + produto.ctr_adwords + ";" + produto.cpc_adwords + ";" + produto.taxa_conversao_adwords + ";" + produto.cpa_adwords + ";" + produto.lucro_adwords + ";" + produto.visualizacoes_lista_konduza + ";" + produto.cliques_konduza + ";" + produto.visualizacoes_detalhes_konduza + ";" + produto.visualizacoes_detalhes_unicos_konduza + ";" + produto.adicoes_carrinho_konduza + ";" + produto.compras_konduza + ";" + produto.ctr_lista_konduza + ";" + produto.taxa_carrinho_konduza + ";" + produto.taxa_compra_konduza + ";" + produto.lucro_konduza + ";" + produto.taxa_carrinho_media + ";" + produto.ctr_medio + ";" + produto.taxa_conversao_media + ";" + produto.cliques_previstos + ";" + produto.receita_revista + ";" + produto.lucro_previsto + ";" + produto.cpc_previsto + ";");
                }
            }
        }

        foreach (var produto in produtosKit)
        {

            var item1d = relatorio1.First(x => x.sku == produto.produtoId);
            var item7d = relatorio7.First(x => x.sku == produto.produtoId);
            var item15d = relatorio15.First(x => x.sku == produto.produtoId);
            var item30d = relatorio30.First(x => x.sku == produto.produtoId);
            var item90d = relatorio90.First(x => x.sku == produto.produtoId);
            decimal deltaCtr1d = 0.03M;
            decimal deltaCtr7d = 0.07M;
            decimal deltaCtr15d = 0.1M;
            decimal deltaCtr30d = 0.3M;
            decimal deltaCtr90d = 0.5M;

            decimal deltaTaxa1d = 0.03M;
            decimal deltaTaxa7d = 0.07M;
            decimal deltaTaxa15d = 0.1M;
            decimal deltaTaxa30d = 0.3M;
            decimal deltaTaxa90d = 0.5M;

            decimal deltaIndiceReal1d = Convert.ToDecimal("0,03");
            decimal deltaIndiceReal7d = Convert.ToDecimal("0,07");
            decimal deltaIndiceReal15d = Convert.ToDecimal("0,1");
            decimal deltaIndiceReal30d = Convert.ToDecimal("0,3");
            decimal deltaIndiceReal90d = Convert.ToDecimal("0,5");


            var relatorioItem = new relatorioFinal();
            var vendaMaxima1 = relatorio1.OrderByDescending(x => x.receita_revista).First().receita_revista;
            var vendaMaxima7 = relatorio7.OrderByDescending(x => x.receita_revista).First().receita_revista;
            var vendaMaxima15 = relatorio15.OrderByDescending(x => x.receita_revista).First().receita_revista;
            var vendaMaxima30 = relatorio30.OrderByDescending(x => x.receita_revista).First().receita_revista;
            var vendaMaxima90 = relatorio90.OrderByDescending(x => x.receita_revista).First().receita_revista;
            var vendaMaxima1real = relatorio1.OrderByDescending(x => (x.valor_item * x.conversoes_item)).Select(x => (x.valor_item * x.conversoes_item)).First();
            var vendaMaxima7real = relatorio7.OrderByDescending(x => (x.valor_item * x.conversoes_item)).Select(x => (x.valor_item * x.conversoes_item)).First();
            var vendaMaxima15real = relatorio15.OrderByDescending(x => (x.valor_item * x.conversoes_item)).Select(x => (x.valor_item * x.conversoes_item)).First();
            var vendaMaxima30real = relatorio30.OrderByDescending(x => (x.valor_item * x.conversoes_item)).Select(x => (x.valor_item * x.conversoes_item)).First();
            var vendaMaxima90real = relatorio90.OrderByDescending(x => (x.valor_item * x.conversoes_item)).Select(x => (x.valor_item * x.conversoes_item)).First();
            decimal indiceAjuste1 = (relatorio1.First(x => x.sku == produto.produtoId).receita_revista * 100) / vendaMaxima1;
            decimal indiceAjuste7 = (relatorio7.First(x => x.sku == produto.produtoId).receita_revista * 100) / vendaMaxima7;
            decimal indiceAjuste15 = (relatorio15.First(x => x.sku == produto.produtoId).receita_revista * 100) / vendaMaxima15;
            decimal indiceAjuste30 = (relatorio30.First(x => x.sku == produto.produtoId).receita_revista * 100) / vendaMaxima30;
            decimal indiceAjuste90 = (relatorio90.First(x => x.sku == produto.produtoId).receita_revista * 100) / vendaMaxima90;
            decimal indiceAjuste1real = vendaMaxima1real == 0 ? Convert.ToDecimal("0") : (relatorio1.Where(x => x.sku == produto.produtoId).Select(x => (x.valor_item * x.conversoes_item)).First() * 100) / vendaMaxima1real;
            decimal indiceAjuste7real = vendaMaxima7real == 0 ? Convert.ToDecimal("0") : (relatorio7.Where(x => x.sku == produto.produtoId).Select(x => (x.valor_item * x.conversoes_item)).First() * 100) / vendaMaxima7real;
            decimal indiceAjuste15real = vendaMaxima15real == 0 ? Convert.ToDecimal("0") : (relatorio15.Where(x => x.sku == produto.produtoId).Select(x => (x.valor_item * x.conversoes_item)).First() * 100) / vendaMaxima15real;
            decimal indiceAjuste30real = vendaMaxima30real == 0 ? Convert.ToDecimal("0") : (relatorio30.Where(x => x.sku == produto.produtoId).Select(x => (x.valor_item * x.conversoes_item)).First() * 100) / vendaMaxima30real;
            decimal indiceAjuste90real = vendaMaxima90real == 0 ? Convert.ToDecimal("0") : (relatorio90.Where(x => x.sku == produto.produtoId).Select(x => (x.valor_item * x.conversoes_item)).First() * 100) / vendaMaxima90real;

            decimal indiceReajusteMedio = (((indiceAjuste1 * deltaIndiceReal1d) + (indiceAjuste7 * deltaIndiceReal7d) + (indiceAjuste15 * deltaIndiceReal15d) + (indiceAjuste30 * deltaIndiceReal30d) + (indiceAjuste90 * deltaIndiceReal90d)));
            decimal indiceReajusteMedioReal = (((indiceAjuste1real * deltaIndiceReal1d) + (indiceAjuste7real * deltaIndiceReal7d) + (indiceAjuste15real * deltaIndiceReal15d) + (indiceAjuste30real * deltaIndiceReal30d) + (indiceAjuste90real * deltaIndiceReal90d)));
            decimal detalAjuste90 = Convert.ToDecimal("0,7");
            relatorioItem.indice_ajuste = ((indiceReajusteMedio * detalAjuste90) + indiceReajusteMedioReal) / 2;
            //relatorioItem.indice_ajuste = indiceAjuste90real;
            decimal ctr_medio_1d = item1d.ctr_medio;
            decimal ctr_medio_7d = item7d.ctr_medio;
            decimal ctr_medio_15d = item15d.ctr_medio;
            decimal ctr_medio_30d = item30d.ctr_medio;
            decimal ctr_medio_90d = item90d.ctr_medio;

            decimal ctr_geral_medio_1d = relatorio1.Where(x => x.ctr_medio > 0).Sum(x => x.ctr_medio) / relatorio1.Count(x => x.ctr_medio > 0);
            decimal ctr_geral_medio_7d = relatorio7.Where(x => x.ctr_medio > 0).Sum(x => x.ctr_medio) / relatorio7.Count(x => x.ctr_medio > 0);
            decimal ctr_geral_medio_15d = relatorio15.Where(x => x.ctr_medio > 0).Sum(x => x.ctr_medio) / relatorio15.Count(x => x.ctr_medio > 0);
            decimal ctr_geral_medio_30d = relatorio30.Where(x => x.ctr_medio > 0).Sum(x => x.ctr_medio) / relatorio30.Count(x => x.ctr_medio > 0);
            decimal ctr_geral_medio_90d = relatorio90.Where(x => x.ctr_medio > 0).Sum(x => x.ctr_medio) / relatorio90.Count(x => x.ctr_medio > 0);

            decimal visualizacoes_detalhes_1d = item1d.visualizacoes_detalhes_unicos_konduza;
            decimal visualizacoes_detalhes_7d = item7d.visualizacoes_detalhes_unicos_konduza;
            decimal visualizacoes_detalhes_15d = item15d.visualizacoes_detalhes_unicos_konduza;
            decimal visualizacoes_detalhes_30d = item30d.visualizacoes_detalhes_unicos_konduza;
            decimal visualizacoes_detalhes_90d = item90d.visualizacoes_detalhes_unicos_konduza;


            decimal visualizacoes_geral_detalhes = relatorio1.Where(x => x.visualizacoes_detalhes_unicos_konduza > 0).Sum(x => x.visualizacoes_detalhes_unicos_konduza) / relatorio1.Count(x => x.visualizacoes_detalhes_unicos_konduza > 0);
            decimal visualizacoes_geral_detalhes_7d = relatorio7.Where(x => x.visualizacoes_detalhes_unicos_konduza > 0).Sum(x => x.visualizacoes_detalhes_unicos_konduza) / relatorio7.Count(x => x.visualizacoes_detalhes_unicos_konduza > 0);
            decimal visualizacoes_geral_detalhes_15d = relatorio15.Where(x => x.visualizacoes_detalhes_unicos_konduza > 0).Sum(x => x.visualizacoes_detalhes_unicos_konduza) / relatorio15.Count(x => x.visualizacoes_detalhes_unicos_konduza > 0);
            decimal visualizacoes_geral_detalhes_30d = relatorio30.Where(x => x.visualizacoes_detalhes_unicos_konduza > 0).Sum(x => x.visualizacoes_detalhes_unicos_konduza) / relatorio30.Count(x => x.visualizacoes_detalhes_unicos_konduza > 0);
            decimal visualizacoes_geral_detalhes_90d = relatorio90.Where(x => x.visualizacoes_detalhes_unicos_konduza > 0).Sum(x => x.visualizacoes_detalhes_unicos_konduza) / relatorio90.Count(x => x.visualizacoes_detalhes_unicos_konduza > 0);

            if (ctr_medio_1d > ctr_geral_medio_1d && visualizacoes_detalhes_1d < visualizacoes_geral_detalhes)
            {
                ctr_medio_1d = relatorio1.Where(x => x.ctr_medio > 0 && x.visualizacoes_detalhes_unicos_konduza < visualizacoes_geral_detalhes).Sum(x => x.ctr_medio) / relatorio1.Count(x => x.ctr_medio > 0 && x.visualizacoes_detalhes_unicos_konduza < visualizacoes_geral_detalhes);
            }
            if (ctr_medio_7d > ctr_geral_medio_7d && visualizacoes_detalhes_7d < visualizacoes_geral_detalhes)
            {
                ctr_medio_7d = relatorio7.Where(x => x.ctr_medio > 0 && x.visualizacoes_detalhes_unicos_konduza < visualizacoes_geral_detalhes).Sum(x => x.ctr_medio) / relatorio7.Count(x => x.ctr_medio > 0 && x.visualizacoes_detalhes_unicos_konduza < visualizacoes_geral_detalhes);
            }
            if (ctr_medio_15d > ctr_geral_medio_15d && visualizacoes_detalhes_15d < visualizacoes_geral_detalhes)
            {
                ctr_medio_15d = relatorio15.Where(x => x.ctr_medio > 0 && x.visualizacoes_detalhes_unicos_konduza < visualizacoes_geral_detalhes).Sum(x => x.ctr_medio) / relatorio15.Count(x => x.ctr_medio > 0 && x.visualizacoes_detalhes_unicos_konduza < visualizacoes_geral_detalhes);
            }
            if (ctr_medio_30d > ctr_geral_medio_30d && visualizacoes_detalhes_30d < visualizacoes_geral_detalhes)
            {
                ctr_medio_30d = relatorio30.Where(x => x.ctr_medio > 0 && x.visualizacoes_detalhes_unicos_konduza < visualizacoes_geral_detalhes).Sum(x => x.ctr_medio) / relatorio30.Count(x => x.ctr_medio > 0 && x.visualizacoes_detalhes_unicos_konduza < visualizacoes_geral_detalhes);
            }
            if (ctr_medio_90d > ctr_geral_medio_90d && visualizacoes_detalhes_90d < visualizacoes_geral_detalhes)
            {
                ctr_medio_90d = relatorio90.Where(x => x.ctr_medio > 0 && x.visualizacoes_detalhes_unicos_konduza < visualizacoes_geral_detalhes).Sum(x => x.ctr_medio) / relatorio90.Count(x => x.ctr_medio > 0 && x.visualizacoes_detalhes_unicos_konduza < visualizacoes_geral_detalhes);
            }


            relatorioItem.ctr_medio = ((((item1d.ctr_medio * deltaCtr1d) + (item7d.ctr_medio * deltaCtr7d) + (item15d.ctr_medio * deltaCtr15d) + (item30d.ctr_medio * deltaCtr30d) + (item90d.ctr_medio * deltaCtr90d))) / 100) * relatorioItem.indice_ajuste;
            relatorioItem.taxa_conversao_media = ((((item1d.taxa_conversao_media * deltaTaxa1d) + (item7d.taxa_conversao_media * deltaTaxa7d) + (item15d.taxa_conversao_media * deltaTaxa15d) + (item30d.taxa_conversao_media * deltaTaxa30d) + (item90d.taxa_conversao_media * deltaTaxa90d))) / 100) * relatorioItem.indice_ajuste;
            relatorioItem.valor_item = (produto.produtoPrecoPromocional > 0 ? (produto.produtoPrecoPromocional ?? 0) : produto.produtoPreco);
            relatorioItem.lucro_item = Decimal.Round((produto.produtoPrecoPromocional > 0 ? (produto.produtoPrecoPromocional ?? 0) : produto.produtoPreco) - (produto.produtoPrecoDeCusto ?? 0), 2);
            relatorioItem.adGroup = listaGrupos.First(x => x.categoriaId == produtosCategorias.First(y => y.produtoId == produto.produtoId).categoriaId).adGroupName;
            int roiEsperado = 3;



            var totalViewsAdwords1d = (relatorio1).Sum(x => x.impressoes_adwords);
            relatorioItem.cliques_previstos_1d = (totalViewsAdwords1d / 100) * relatorioItem.ctr_medio;
            relatorioItem.receita_prevista_1d = ((relatorioItem.cliques_previstos_1d / 100) * relatorioItem.taxa_conversao_media) * relatorioItem.valor_item;
            relatorioItem.lucro_previsto_1d = ((relatorioItem.cliques_previstos_1d / 100) * relatorioItem.taxa_conversao_media) * relatorioItem.lucro_item;
            relatorioItem.cpc_previsto_1d = relatorioItem.cliques_previstos_1d == 0 ? 0 : (relatorioItem.receita_prevista_1d) / ((relatorioItem.cliques_previstos_1d) * roiEsperado);


            var totalViewsAdwords7d = (relatorio7).Sum(x => x.impressoes_adwords);
            relatorioItem.cliques_previstos_7d = (totalViewsAdwords7d / 100) * relatorioItem.ctr_medio;
            relatorioItem.receita_prevista_7d = ((relatorioItem.cliques_previstos_7d / 100) * relatorioItem.taxa_conversao_media) * relatorioItem.valor_item;
            relatorioItem.lucro_previsto_7d = ((relatorioItem.cliques_previstos_7d / 100) * relatorioItem.taxa_conversao_media) * relatorioItem.lucro_item;
            relatorioItem.cpc_previsto_7d = relatorioItem.cliques_previstos_7d == 0 ? 0 : (relatorioItem.receita_prevista_7d) / ((relatorioItem.cliques_previstos_7d) * roiEsperado);


            var totalViewsAdwords15d = (relatorio15).Sum(x => x.impressoes_adwords);
            relatorioItem.cliques_previstos_15d = (totalViewsAdwords15d / 100) * relatorioItem.ctr_medio;
            relatorioItem.receita_prevista_15d = ((relatorioItem.cliques_previstos_15d / 100) * relatorioItem.taxa_conversao_media) * relatorioItem.valor_item;
            relatorioItem.lucro_previsto_15d = ((relatorioItem.cliques_previstos_15d / 100) * relatorioItem.taxa_conversao_media) * relatorioItem.lucro_item;
            relatorioItem.cpc_previsto_15d = relatorioItem.cliques_previstos_15d == 0 ? 0 : (relatorioItem.receita_prevista_15d) / ((relatorioItem.cliques_previstos_15d) * roiEsperado);


            var totalViewsAdwords30d = (relatorio30).Sum(x => x.impressoes_adwords);
            relatorioItem.cliques_previstos_30d = (totalViewsAdwords30d / 100) * relatorioItem.ctr_medio;
            relatorioItem.receita_prevista_30d = ((relatorioItem.cliques_previstos_30d / 100) * relatorioItem.taxa_conversao_media) * relatorioItem.valor_item;
            relatorioItem.lucro_previsto_30d = ((relatorioItem.cliques_previstos_30d / 100) * relatorioItem.taxa_conversao_media) * relatorioItem.lucro_item;
            relatorioItem.cpc_previsto_30d = relatorioItem.cliques_previstos_30d == 0 ? 0 : (relatorioItem.receita_prevista_30d) / ((relatorioItem.cliques_previstos_30d) * roiEsperado);


            var totalViewsAdwords90d = (relatorio90).Sum(x => x.impressoes_adwords);
            relatorioItem.cliques_previstos_90d = (totalViewsAdwords90d / 100) * relatorioItem.ctr_medio;
            relatorioItem.receita_prevista_90d = ((relatorioItem.cliques_previstos_90d / 100) * relatorioItem.taxa_conversao_media) * relatorioItem.valor_item;
            relatorioItem.lucro_previsto_90d = ((relatorioItem.cliques_previstos_90d / 100) * relatorioItem.taxa_conversao_media) * relatorioItem.lucro_item;
            relatorioItem.cpc_previsto_90d = relatorioItem.cliques_previstos_90d == 0 ? 0 : (relatorioItem.receita_prevista_90d) / ((relatorioItem.cliques_previstos_90d) * roiEsperado);


            /*produto.cliques_previstos = (totalViewsAdwords / 100) * produto.ctr_medio;
            produto.receita_revista = ((produto.cliques_previstos / 100) * produto.taxa_conversao_media) * produto.valor_item;
            produto.lucro_previsto = ((produto.cliques_previstos / 100) * produto.taxa_conversao_media) * produto.lucro_item;
            produto.cpc_previsto = produto.cliques_previstos == 0 ? 0 : (produto.receita_revista) / ((produto.cliques_previstos) * roiEsperado);*/



            relatorioItem.sku = produto.produtoId;
            relatorioFinal.Add(relatorioItem);
        }

        foreach (var produto in relatorioFinal)
        {


            int deltaCpc1d = 1;
            int deltaCpc7d = 1;
            int deltaCpc15d = 1;
            int deltaCpc30d = 1;
            int deltaCpc90d = 1;

            produto.cpc_medio_previsto = ((produto.cpc_previsto_1d * deltaCpc1d) + (produto.cpc_previsto_7d * deltaCpc7d) + (produto.cpc_previsto_15d * deltaCpc15d) + (produto.cpc_previsto_30d * deltaCpc30d) + (produto.cpc_previsto_90d * deltaCpc90d)) / 5;

        }

        decimal cpcMinimoAdwords = relatorio90.Where(x => x.cpc_adwords > 0 && x.cliques_adwords > 10).OrderBy(x => x.cpc_adwords).First().cpc_adwords;
        decimal cpcMaximoAdwords = relatorio90.OrderByDescending(x => x.cpc_adwords).First().cpc_adwords;
        int indice = 0;
        decimal fatorDeCorrecao = 30;
        foreach (var produto in relatorioFinal.OrderByDescending(x => x.cpc_medio_previsto))
        {
            var adwordsIndice = relatorio90.Where(x => x.cpc_adwords > 0 && x.cliques_adwords > 10).OrderByDescending(x => x.cpc_adwords).Skip(indice).FirstOrDefault();
            if (adwordsIndice != null)
            {
                produto.cpc_previsto_ajustado = adwordsIndice.cpc_adwords + ((adwordsIndice.cpc_adwords / 100) * fatorDeCorrecao);
            }
            else
            {
                produto.cpc_previsto_ajustado = cpcMinimoAdwords;
            }
            indice++;
        }

        FileInfo relatorioFilefinal = new FileInfo("c:\\bid_manager\\" + nomeCampanha + "_relatorio_full_final.csv");
        if (relatorioFilefinal.Exists)
        {
            relatorioFilefinal.Delete();
        }
        using (StreamWriter swRelatorio = relatorioFilefinal.CreateText())
        {

            swRelatorio.WriteLine("sku;nome;valor_item;lucro_item;ctr_medio;taxa_conversao_media;cliques_previstos_1d;conversoes_previstas_1d;receita_prevista_1d;lucro_previsto_1d;cpc_previsto_1d;cliques_previstos_7d;conversoes_previstas_7d;receita_prevista_7d;lucro_previsto_7d;cpc_previsto_7d;cliques_previstos_15d;conversoes_previstas_15d;receita_prevista_15d;lucro_previsto_15d;cpc_previsto_15d;cliques_previstos_30d;conversoes_previstas_30d;receita_prevista_30d;lucro_previsto_30d;cpc_previsto_30d;cliques_previstos_90d;conversoes_previstas_90d;receita_prevista_90d;lucro_previsto_90d;cpc_previsto_90d;cpc_medio_previsto;indice_ajuste;cpc_previsto_ajustado;");
            foreach (var produto in relatorioFinal)
            {
                swRelatorio.WriteLine(produto.sku + ";" + produtosKit.First(x => x.produtoId == produto.sku).produtoNome.Replace(";", "") + ";" + produto.valor_item + ";" + (produto.lucro_item) + ";" + produto.ctr_medio + ";"
                    + produto.taxa_conversao_media + ";" + produto.cliques_previstos_1d + ";" + produto.conversoes_previstas_1d + ";" + produto.receita_prevista_1d + ";"
                    + produto.lucro_previsto_1d + ";" + produto.cpc_previsto_1d + ";" + produto.cliques_previstos_7d + ";" + produto.conversoes_previstas_7d + ";" + produto.receita_prevista_7d
                    + ";" + produto.lucro_previsto_7d + ";" + produto.cpc_previsto_7d + ";" + produto.cliques_previstos_15d + ";" + produto.conversoes_previstas_15d + ";"
                    + produto.receita_prevista_15d + ";" + produto.lucro_previsto_15d + ";" + produto.cpc_previsto_15d + ";" + produto.cliques_previstos_30d
                    + ";" + produto.conversoes_previstas_30d + ";" + produto.receita_prevista_30d + ";" + produto.lucro_previsto_30d + ";" + produto.cpc_previsto_30d
                    + ";" + produto.cliques_previstos_90d + ";" + produto.conversoes_previstas_90d + ";" + produto.receita_prevista_90d + ";" + produto.lucro_previsto_90d
                    + ";" + produto.cpc_previsto_90d + ";" + produto.cpc_medio_previsto + ";" + produto.indice_ajuste + ";" + produto.cpc_previsto_ajustado + ";");
            }
        }

        FileInfo file_konduza_new_d_kit_berco = new FileInfo("c:\\bid_manager\\konduza-new-d-" + nomeCampanha + ".csv");
        if (file_konduza_new_d_kit_berco.Exists)
        {
            file_konduza_new_d_kit_berco.Delete();
        }
        using (StreamWriter swRelatorio = file_konduza_new_d_kit_berco.CreateText())
        {

            swRelatorio.WriteLine("Campaign	Ad Group\tProduct Group\tMax CPC");
            foreach (var produto in relatorioFinal)
            {
                swRelatorio.WriteLine("konduza-new-d-" + nomeCampanha + "\t" + produto.adGroup + "\t* / Item ID='" + produto.sku + "'\t" + produto.cpc_previsto_ajustado.ToString("0.00").Replace(",", "."));
            }
        }

        FileInfo file_konduza_new_m_kit_berco = new FileInfo("c:\\bid_manager\\konduza-new-m-" + nomeCampanha + ".csv");
        if (file_konduza_new_m_kit_berco.Exists)
        {
            file_konduza_new_m_kit_berco.Delete();
        }
        using (StreamWriter swRelatorio = file_konduza_new_m_kit_berco.CreateText())
        {

            swRelatorio.WriteLine("Campaign	Ad Group\tProduct Group\tMax CPC");
            foreach (var produto in relatorioFinal)
            {
                swRelatorio.WriteLine("konduza-new-m-" + nomeCampanha + "\t" + produto.adGroup + "\t* / Item ID='" + produto.sku + "'\t" + produto.cpc_previsto_ajustado.ToString("0.00").Replace(",", "."));
            }
        }

        foreach (var produto in relatorioFinal.OrderByDescending(x => x.cpc_previsto_ajustado).ToList())
        {
            Response.Write("<img src=\"http://cdn2.graodegente.com.br/fotos/" + produto.sku + "/media_" + produtosKit.First(x => x.produtoId == produto.sku).fotoDestaque + ".jpg\" /><br>" + produto.sku + " - " + produto.cpc_previsto_ajustado.ToString("C") + " - " + produto.cpc_medio_previsto.ToString("C") + " - " + produto.valor_item.ToString("C") + " - " + produtosKit.First(x => x.produtoId == produto.sku).produtoNome + "<br><br>");
        }
    }

    private void campanha_moveis()
    {
        string nomeCampanha = "moveis";
        var listaGrupos = new List<adGroup>();
        listaGrupos.Add(new adGroup { categoriaId = 990, adGroupName = "colchao" });
        listaGrupos.Add(new adGroup { categoriaId = 1424, adGroupName = "mini-cama" });
        listaGrupos.Add(new adGroup { categoriaId = 1003, adGroupName = "guarda-roupas" });
        listaGrupos.Add(new adGroup { categoriaId = 1002, adGroupName = "comodas" });
        listaGrupos.Add(new adGroup { categoriaId = 1005, adGroupName = "quarto" });
        listaGrupos.Add(new adGroup { categoriaId = 1004, adGroupName = "cama-auxiliar" });
        listaGrupos.Add(new adGroup { categoriaId = 1251, adGroupName = "poltronas" });
        listaGrupos.Add(new adGroup { categoriaId = 761, adGroupName = "prateleiras" });
        //listaGrupos.Add(new adGroup { categoriaId = 1385, adGroupName = "montessoriano" });
        listaGrupos.Add(new adGroup { categoriaId = 1459, adGroupName = "cabanas" });
        listaGrupos.Add(new adGroup { categoriaId = 1617, adGroupName = "mesas" });
        var data = new dbCommerceDataContext();
        var produtosKit = (from c in data.tbJuncaoProdutoCategorias where listaGrupos.Select(x => x.categoriaId).Contains(c.categoriaId) && c.tbProduto.produtoAtivo.ToLower() == "true" && c.tbProduto.produtoEstoqueAtual > 0 select c.tbProduto).Distinct().ToList();
        var produtosCategorias = (from c in data.tbJuncaoProdutoCategorias where listaGrupos.Select(x => x.categoriaId).Contains(c.categoriaId) && c.tbProduto.produtoAtivo.ToLower() == "true" && c.tbProduto.produtoEstoqueAtual > 0 select c).ToList();
        var produtosIds = produtosKit.Select(x => x.produtoId).Distinct().ToList();

        List<relatorioAgregado> relatorio1 = new List<relatorioAgregado>();
        List<relatorioAgregado> relatorio7 = new List<relatorioAgregado>();
        List<relatorioAgregado> relatorio15 = new List<relatorioAgregado>();
        List<relatorioAgregado> relatorio30 = new List<relatorioAgregado>();
        List<relatorioAgregado> relatorio90 = new List<relatorioAgregado>();
        List<relatorioFinal> relatorioFinal = new List<relatorioFinal>();

        List<int> periodos = new List<int>();
        periodos.Add(1);
        periodos.Add(7);
        periodos.Add(15);
        periodos.Add(30);
        periodos.Add(90);
        foreach (var periodo in periodos)
        {
            var dias = DateTime.Now.AddDays(periodo * (-1));
            var vendasKitL = (from c in data.tbItensPedidos where produtosIds.Contains(c.produtoId) && c.dataDaCriacao > dias select c).ToList();
            ConcurrentBag<tbItensPedido> vendasKit = new ConcurrentBag<tbItensPedido>(vendasKitL);

            foreach (var produto in produtosKit)
            {
                var relatorioItem = new relatorioAgregado();
                relatorioItem.sku = produto.produtoId;
                relatorioItem.conversoes_item = vendasKit.Count(x => x.produtoId == produto.produtoId);
                relatorioItem.valor_item = (produto.produtoPrecoPromocional > 0 ? (produto.produtoPrecoPromocional ?? 0) : produto.produtoPreco);
                relatorioItem.lucro_item = Decimal.Round((produto.produtoPrecoPromocional > 0 ? (produto.produtoPrecoPromocional ?? 0) : produto.produtoPreco) - (produto.produtoPrecoDeCusto ?? 0), 2);
                relatorioItem.producao3d = produto.criacaoPropria ?? false;
                if (periodo == 1) relatorio1.Add(relatorioItem);
                if (periodo == 7) relatorio7.Add(relatorioItem);
                if (periodo == 15) relatorio15.Add(relatorioItem);
                if (periodo == 30) relatorio30.Add(relatorioItem);
                if (periodo == 90) relatorio90.Add(relatorioItem);
            }

            using (var reader = new StreamReader(@"C:\bid_manager\shopping_ga_" + periodo + "d.csv"))
            {
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Split(';');
                    int produtoId = 0;
                    int.TryParse(values[0], out produtoId);
                    if (produtoId > 0)
                    {

                        var relatorioItem = (periodo == 1 ? relatorio1 : periodo == 7 ? relatorio7 : periodo == 15 ? relatorio15 : periodo == 30 ? relatorio30 : relatorio90).FirstOrDefault(x => x.sku == produtoId);
                        if (relatorioItem != null)
                        {
                            relatorioItem.visualizacoes_lista_ga = Convert.ToInt32(values[1].Replace(".", ""));
                            relatorioItem.visualizacoes_detalhes_ga = Convert.ToInt32(values[2].Replace(".", ""));
                            relatorioItem.adicoes_carrinho_ga = Convert.ToInt32(values[3].Replace(".", ""));
                        }
                    }
                }
            }


            using (var reader = new StreamReader(@"C:\bid_manager\shopping_adwords_" + periodo + "d.csv"))
            {
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Split(';');
                    int produtoId = 0;
                    int.TryParse(values[0], out produtoId);
                    if (produtoId > 0)
                    {

                        var relatorioItem = (periodo == 1 ? relatorio1 : periodo == 7 ? relatorio7 : periodo == 15 ? relatorio15 : periodo == 30 ? relatorio30 : relatorio90).FirstOrDefault(x => x.sku == produtoId);
                        if (relatorioItem != null)
                        {
                            relatorioItem.cliques_adwords = Convert.ToInt32(values[1].Replace(".", ""));
                            relatorioItem.custo_adwords = Convert.ToDecimal(values[2].ToLower().Replace("r", "").Replace("$", "").Replace(" ", "").Replace("\"", ""));
                            relatorioItem.impressoes_adwords = Convert.ToInt32(values[3].Replace(".", ""));
                            relatorioItem.conversoes_adwords = Convert.ToDecimal(values[4].Replace("R$ ", "").Replace("\"", ""));
                        }
                    }
                }
            }

            using (var reader = new StreamReader(@"C:\bid_manager\konduza_" + periodo + "d.csv"))
            {
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Split(';');
                    int produtoId = 0;
                    int.TryParse(values[0], out produtoId);
                    if (produtoId > 0)
                    {

                        var relatorioItem = (periodo == 1 ? relatorio1 : periodo == 7 ? relatorio7 : periodo == 15 ? relatorio15 : periodo == 30 ? relatorio30 : relatorio90).FirstOrDefault(x => x.sku == produtoId);
                        if (relatorioItem != null)
                        {
                            relatorioItem.visualizacoes_lista_konduza = Convert.ToInt32(values[1].Replace(".", ""));
                            relatorioItem.cliques_konduza = Convert.ToInt32(values[2].Replace(".", ""));
                            relatorioItem.visualizacoes_detalhes_konduza = Convert.ToInt32(values[3].Replace(".", ""));
                            relatorioItem.visualizacoes_detalhes_unicos_konduza = Convert.ToInt32(values[4].Replace(".", ""));
                            relatorioItem.adicoes_carrinho_konduza = Convert.ToInt32(values[5].Replace(".", ""));
                            relatorioItem.compras_konduza = Convert.ToInt32(values[8].Replace(".", ""));

                        }
                    }
                }
            }

            int roiEsperado = 3;
            foreach (var produto in (periodo == 1 ? relatorio1 : periodo == 7 ? relatorio7 : periodo == 15 ? relatorio15 : periodo == 30 ? relatorio30 : relatorio90))
            {
                var totalViewsAdwords = (periodo == 1 ? relatorio1 : periodo == 7 ? relatorio7 : periodo == 15 ? relatorio15 : periodo == 30 ? relatorio30 : relatorio90).Sum(x => x.impressoes_adwords);
                produto.cliques_previstos = (totalViewsAdwords / 100) * produto.ctr_medio;
                produto.receita_revista = ((produto.cliques_previstos / 100) * produto.taxa_conversao_media) * produto.valor_item;
                produto.lucro_previsto = ((produto.cliques_previstos / 100) * produto.taxa_conversao_media) * produto.lucro_item;
                produto.cpc_previsto = produto.cliques_previstos == 0 ? 0 : (produto.receita_revista) / ((produto.cliques_previstos) * roiEsperado);
            }

            FileInfo relatorioFile = new FileInfo("c:\\bid_manager\\" + nomeCampanha + "relatorio_full_" + periodo + ".csv");
            if (relatorioFile.Exists)
            {
                relatorioFile.Delete();
            }
            using (StreamWriter swRelatorio = relatorioFile.CreateText())
            {

                swRelatorio.WriteLine("sku;lucro_item;producao3d;" + "visualizacoes_lista_ga;" + "visualizacoes_detalhes_ga;" + "adicoes_carrinho_ga;" + "ctr_lista_ga;" + "taxa_carrinho_ga;" + "cliques_adwords;" + "custo_adwords;" + "impressoes_adwords;" + "conversoes_adwords;" + "ctr_adwords; cpc_adwords; taxa_conversao_adwords; cpa_adwords;lucro_adwords;" + "visualizacoes_lista_konduza;" + "cliques_konduza;" + "visualizacoes_detalhes_konduza;" + "visualizacoes_detalhes_unicos_konduza;" + "adicoes_carrinho_konduza;" + "compras_konduza;ctr_lista_konduza;taxa_carrinho_konduza;taxa_compra_konduza;lucro_konduza;taxa_carrinho_media;ctr_medio;taxa_conversao_media;cliques_previstos;receita_revista;lucro_previsto;cpc_previsto;");
                foreach (var produto in (periodo == 1 ? relatorio1 : periodo == 7 ? relatorio7 : periodo == 15 ? relatorio15 : periodo == 30 ? relatorio30 : relatorio90))
                {
                    swRelatorio.WriteLine(produto.sku + ";" + produto.lucro_item + ";" + (produto.producao3d ? "s" : "n") + ";" + produto.visualizacoes_lista_ga + ";" + produto.visualizacoes_detalhes_ga + ";" + produto.adicoes_carrinho_ga + ";" + produto.ctr_lista_ga + ";" + produto.taxa_carrinho_ga + ";" + produto.cliques_adwords + ";" + produto.custo_adwords + ";" + produto.impressoes_adwords + ";" + produto.conversoes_adwords + ";" + produto.ctr_adwords + ";" + produto.cpc_adwords + ";" + produto.taxa_conversao_adwords + ";" + produto.cpa_adwords + ";" + produto.lucro_adwords + ";" + produto.visualizacoes_lista_konduza + ";" + produto.cliques_konduza + ";" + produto.visualizacoes_detalhes_konduza + ";" + produto.visualizacoes_detalhes_unicos_konduza + ";" + produto.adicoes_carrinho_konduza + ";" + produto.compras_konduza + ";" + produto.ctr_lista_konduza + ";" + produto.taxa_carrinho_konduza + ";" + produto.taxa_compra_konduza + ";" + produto.lucro_konduza + ";" + produto.taxa_carrinho_media + ";" + produto.ctr_medio + ";" + produto.taxa_conversao_media + ";" + produto.cliques_previstos + ";" + produto.receita_revista + ";" + produto.lucro_previsto + ";" + produto.cpc_previsto + ";");
                }
            }
        }

        foreach (var produto in produtosKit)
        {

            var item1d = relatorio1.First(x => x.sku == produto.produtoId);
            var item7d = relatorio7.First(x => x.sku == produto.produtoId);
            var item15d = relatorio15.First(x => x.sku == produto.produtoId);
            var item30d = relatorio30.First(x => x.sku == produto.produtoId);
            var item90d = relatorio90.First(x => x.sku == produto.produtoId);
            decimal deltaCtr1d = 0.03M;
            decimal deltaCtr7d = 0.07M;
            decimal deltaCtr15d = 0.1M;
            decimal deltaCtr30d = 0.3M;
            decimal deltaCtr90d = 0.5M;

            decimal deltaTaxa1d = 0.03M;
            decimal deltaTaxa7d = 0.07M;
            decimal deltaTaxa15d = 0.1M;
            decimal deltaTaxa30d = 0.3M;
            decimal deltaTaxa90d = 0.5M;

            decimal deltaIndiceReal1d = Convert.ToDecimal("0,03");
            decimal deltaIndiceReal7d = Convert.ToDecimal("0,07");
            decimal deltaIndiceReal15d = Convert.ToDecimal("0,1");
            decimal deltaIndiceReal30d = Convert.ToDecimal("0,3");
            decimal deltaIndiceReal90d = Convert.ToDecimal("0,5");


            var relatorioItem = new relatorioFinal();
            var vendaMaxima1 = relatorio1.OrderByDescending(x => x.receita_revista).First().receita_revista;
            var vendaMaxima7 = relatorio7.OrderByDescending(x => x.receita_revista).First().receita_revista;
            var vendaMaxima15 = relatorio15.OrderByDescending(x => x.receita_revista).First().receita_revista;
            var vendaMaxima30 = relatorio30.OrderByDescending(x => x.receita_revista).First().receita_revista;
            var vendaMaxima90 = relatorio90.OrderByDescending(x => x.receita_revista).First().receita_revista;
            var vendaMaxima1real = relatorio1.OrderByDescending(x => (x.valor_item * x.conversoes_item)).Select(x => (x.valor_item * x.conversoes_item)).First();
            var vendaMaxima7real = relatorio7.OrderByDescending(x => (x.valor_item * x.conversoes_item)).Select(x => (x.valor_item * x.conversoes_item)).First();
            var vendaMaxima15real = relatorio15.OrderByDescending(x => (x.valor_item * x.conversoes_item)).Select(x => (x.valor_item * x.conversoes_item)).First();
            var vendaMaxima30real = relatorio30.OrderByDescending(x => (x.valor_item * x.conversoes_item)).Select(x => (x.valor_item * x.conversoes_item)).First();
            var vendaMaxima90real = relatorio90.OrderByDescending(x => (x.valor_item * x.conversoes_item)).Select(x => (x.valor_item * x.conversoes_item)).First();
            decimal indiceAjuste1 = (relatorio1.First(x => x.sku == produto.produtoId).receita_revista * 100) / vendaMaxima1;
            decimal indiceAjuste7 = (relatorio7.First(x => x.sku == produto.produtoId).receita_revista * 100) / vendaMaxima7;
            decimal indiceAjuste15 = (relatorio15.First(x => x.sku == produto.produtoId).receita_revista * 100) / vendaMaxima15;
            decimal indiceAjuste30 = (relatorio30.First(x => x.sku == produto.produtoId).receita_revista * 100) / vendaMaxima30;
            decimal indiceAjuste90 = (relatorio90.First(x => x.sku == produto.produtoId).receita_revista * 100) / vendaMaxima90;
            decimal indiceAjuste1real = vendaMaxima1real == 0 ? Convert.ToDecimal("0") : (relatorio1.Where(x => x.sku == produto.produtoId).Select(x => (x.valor_item * x.conversoes_item)).First() * 100) / vendaMaxima1real;
            decimal indiceAjuste7real = vendaMaxima7real == 0 ? Convert.ToDecimal("0") : (relatorio7.Where(x => x.sku == produto.produtoId).Select(x => (x.valor_item * x.conversoes_item)).First() * 100) / vendaMaxima7real;
            decimal indiceAjuste15real = vendaMaxima15real == 0 ? Convert.ToDecimal("0") : (relatorio15.Where(x => x.sku == produto.produtoId).Select(x => (x.valor_item * x.conversoes_item)).First() * 100) / vendaMaxima15real;
            decimal indiceAjuste30real = vendaMaxima30real == 0 ? Convert.ToDecimal("0") : (relatorio30.Where(x => x.sku == produto.produtoId).Select(x => (x.valor_item * x.conversoes_item)).First() * 100) / vendaMaxima30real;
            decimal indiceAjuste90real = vendaMaxima90real == 0 ? Convert.ToDecimal("0") : (relatorio90.Where(x => x.sku == produto.produtoId).Select(x => (x.valor_item * x.conversoes_item)).First() * 100) / vendaMaxima90real;

            decimal indiceReajusteMedio = (((indiceAjuste1 * deltaIndiceReal1d) + (indiceAjuste7 * deltaIndiceReal7d) + (indiceAjuste15 * deltaIndiceReal15d) + (indiceAjuste30 * deltaIndiceReal30d) + (indiceAjuste90 * deltaIndiceReal90d)));
            decimal indiceReajusteMedioReal = (((indiceAjuste1real * deltaIndiceReal1d) + (indiceAjuste7real * deltaIndiceReal7d) + (indiceAjuste15real * deltaIndiceReal15d) + (indiceAjuste30real * deltaIndiceReal30d) + (indiceAjuste90real * deltaIndiceReal90d)));
            decimal detalAjuste90 = Convert.ToDecimal("0,7");
            relatorioItem.indice_ajuste = ((indiceReajusteMedio * detalAjuste90) + indiceReajusteMedioReal) / 2;
            //relatorioItem.indice_ajuste = indiceAjuste90real;
            decimal ctr_medio_1d = item1d.ctr_medio;
            decimal ctr_medio_7d = item7d.ctr_medio;
            decimal ctr_medio_15d = item15d.ctr_medio;
            decimal ctr_medio_30d = item30d.ctr_medio;
            decimal ctr_medio_90d = item90d.ctr_medio;

            decimal ctr_geral_medio_1d = relatorio1.Where(x => x.ctr_medio > 0).Sum(x => x.ctr_medio) / relatorio1.Count(x => x.ctr_medio > 0);
            decimal ctr_geral_medio_7d = relatorio7.Where(x => x.ctr_medio > 0).Sum(x => x.ctr_medio) / relatorio7.Count(x => x.ctr_medio > 0);
            decimal ctr_geral_medio_15d = relatorio15.Where(x => x.ctr_medio > 0).Sum(x => x.ctr_medio) / relatorio15.Count(x => x.ctr_medio > 0);
            decimal ctr_geral_medio_30d = relatorio30.Where(x => x.ctr_medio > 0).Sum(x => x.ctr_medio) / relatorio30.Count(x => x.ctr_medio > 0);
            decimal ctr_geral_medio_90d = relatorio90.Where(x => x.ctr_medio > 0).Sum(x => x.ctr_medio) / relatorio90.Count(x => x.ctr_medio > 0);

            decimal visualizacoes_detalhes_1d = item1d.visualizacoes_detalhes_unicos_konduza;
            decimal visualizacoes_detalhes_7d = item7d.visualizacoes_detalhes_unicos_konduza;
            decimal visualizacoes_detalhes_15d = item15d.visualizacoes_detalhes_unicos_konduza;
            decimal visualizacoes_detalhes_30d = item30d.visualizacoes_detalhes_unicos_konduza;
            decimal visualizacoes_detalhes_90d = item90d.visualizacoes_detalhes_unicos_konduza;


            decimal visualizacoes_geral_detalhes = relatorio1.Where(x => x.visualizacoes_detalhes_unicos_konduza > 0).Sum(x => x.visualizacoes_detalhes_unicos_konduza) / relatorio1.Count(x => x.visualizacoes_detalhes_unicos_konduza > 0);
            decimal visualizacoes_geral_detalhes_7d = relatorio7.Where(x => x.visualizacoes_detalhes_unicos_konduza > 0).Sum(x => x.visualizacoes_detalhes_unicos_konduza) / relatorio7.Count(x => x.visualizacoes_detalhes_unicos_konduza > 0);
            decimal visualizacoes_geral_detalhes_15d = relatorio15.Where(x => x.visualizacoes_detalhes_unicos_konduza > 0).Sum(x => x.visualizacoes_detalhes_unicos_konduza) / relatorio15.Count(x => x.visualizacoes_detalhes_unicos_konduza > 0);
            decimal visualizacoes_geral_detalhes_30d = relatorio30.Where(x => x.visualizacoes_detalhes_unicos_konduza > 0).Sum(x => x.visualizacoes_detalhes_unicos_konduza) / relatorio30.Count(x => x.visualizacoes_detalhes_unicos_konduza > 0);
            decimal visualizacoes_geral_detalhes_90d = relatorio90.Where(x => x.visualizacoes_detalhes_unicos_konduza > 0).Sum(x => x.visualizacoes_detalhes_unicos_konduza) / relatorio90.Count(x => x.visualizacoes_detalhes_unicos_konduza > 0);

            if (ctr_medio_1d > ctr_geral_medio_1d && visualizacoes_detalhes_1d < visualizacoes_geral_detalhes)
            {
                ctr_medio_1d = relatorio1.Where(x => x.ctr_medio > 0 && x.visualizacoes_detalhes_unicos_konduza < visualizacoes_geral_detalhes).Sum(x => x.ctr_medio) / relatorio1.Count(x => x.ctr_medio > 0 && x.visualizacoes_detalhes_unicos_konduza < visualizacoes_geral_detalhes);
            }
            if (ctr_medio_7d > ctr_geral_medio_7d && visualizacoes_detalhes_7d < visualizacoes_geral_detalhes)
            {
                ctr_medio_7d = relatorio7.Where(x => x.ctr_medio > 0 && x.visualizacoes_detalhes_unicos_konduza < visualizacoes_geral_detalhes).Sum(x => x.ctr_medio) / relatorio7.Count(x => x.ctr_medio > 0 && x.visualizacoes_detalhes_unicos_konduza < visualizacoes_geral_detalhes);
            }
            if (ctr_medio_15d > ctr_geral_medio_15d && visualizacoes_detalhes_15d < visualizacoes_geral_detalhes)
            {
                ctr_medio_15d = relatorio15.Where(x => x.ctr_medio > 0 && x.visualizacoes_detalhes_unicos_konduza < visualizacoes_geral_detalhes).Sum(x => x.ctr_medio) / relatorio15.Count(x => x.ctr_medio > 0 && x.visualizacoes_detalhes_unicos_konduza < visualizacoes_geral_detalhes);
            }
            if (ctr_medio_30d > ctr_geral_medio_30d && visualizacoes_detalhes_30d < visualizacoes_geral_detalhes)
            {
                ctr_medio_30d = relatorio30.Where(x => x.ctr_medio > 0 && x.visualizacoes_detalhes_unicos_konduza < visualizacoes_geral_detalhes).Sum(x => x.ctr_medio) / relatorio30.Count(x => x.ctr_medio > 0 && x.visualizacoes_detalhes_unicos_konduza < visualizacoes_geral_detalhes);
            }
            if (ctr_medio_90d > ctr_geral_medio_90d && visualizacoes_detalhes_90d < visualizacoes_geral_detalhes)
            {
                ctr_medio_90d = relatorio90.Where(x => x.ctr_medio > 0 && x.visualizacoes_detalhes_unicos_konduza < visualizacoes_geral_detalhes).Sum(x => x.ctr_medio) / relatorio90.Count(x => x.ctr_medio > 0 && x.visualizacoes_detalhes_unicos_konduza < visualizacoes_geral_detalhes);
            }


            relatorioItem.ctr_medio = ((((item1d.ctr_medio * deltaCtr1d) + (item7d.ctr_medio * deltaCtr7d) + (item15d.ctr_medio * deltaCtr15d) + (item30d.ctr_medio * deltaCtr30d) + (item90d.ctr_medio * deltaCtr90d))) / 100) * relatorioItem.indice_ajuste;
            relatorioItem.taxa_conversao_media = ((((item1d.taxa_conversao_media * deltaTaxa1d) + (item7d.taxa_conversao_media * deltaTaxa7d) + (item15d.taxa_conversao_media * deltaTaxa15d) + (item30d.taxa_conversao_media * deltaTaxa30d) + (item90d.taxa_conversao_media * deltaTaxa90d))) / 100) * relatorioItem.indice_ajuste;
            relatorioItem.valor_item = (produto.produtoPrecoPromocional > 0 ? (produto.produtoPrecoPromocional ?? 0) : produto.produtoPreco);
            relatorioItem.lucro_item = Decimal.Round((produto.produtoPrecoPromocional > 0 ? (produto.produtoPrecoPromocional ?? 0) : produto.produtoPreco) - (produto.produtoPrecoDeCusto ?? 0), 2);
            relatorioItem.adGroup = listaGrupos.First(x => x.categoriaId == produtosCategorias.First(y => y.produtoId == produto.produtoId).categoriaId).adGroupName;
            int roiEsperado = 3;



            var totalViewsAdwords1d = (relatorio1).Sum(x => x.impressoes_adwords);
            relatorioItem.cliques_previstos_1d = (totalViewsAdwords1d / 100) * relatorioItem.ctr_medio;
            relatorioItem.receita_prevista_1d = ((relatorioItem.cliques_previstos_1d / 100) * relatorioItem.taxa_conversao_media) * relatorioItem.valor_item;
            relatorioItem.lucro_previsto_1d = ((relatorioItem.cliques_previstos_1d / 100) * relatorioItem.taxa_conversao_media) * relatorioItem.lucro_item;
            relatorioItem.cpc_previsto_1d = relatorioItem.cliques_previstos_1d == 0 ? 0 : (relatorioItem.receita_prevista_1d) / ((relatorioItem.cliques_previstos_1d) * roiEsperado);


            var totalViewsAdwords7d = (relatorio7).Sum(x => x.impressoes_adwords);
            relatorioItem.cliques_previstos_7d = (totalViewsAdwords7d / 100) * relatorioItem.ctr_medio;
            relatorioItem.receita_prevista_7d = ((relatorioItem.cliques_previstos_7d / 100) * relatorioItem.taxa_conversao_media) * relatorioItem.valor_item;
            relatorioItem.lucro_previsto_7d = ((relatorioItem.cliques_previstos_7d / 100) * relatorioItem.taxa_conversao_media) * relatorioItem.lucro_item;
            relatorioItem.cpc_previsto_7d = relatorioItem.cliques_previstos_7d == 0 ? 0 : (relatorioItem.receita_prevista_7d) / ((relatorioItem.cliques_previstos_7d) * roiEsperado);


            var totalViewsAdwords15d = (relatorio15).Sum(x => x.impressoes_adwords);
            relatorioItem.cliques_previstos_15d = (totalViewsAdwords15d / 100) * relatorioItem.ctr_medio;
            relatorioItem.receita_prevista_15d = ((relatorioItem.cliques_previstos_15d / 100) * relatorioItem.taxa_conversao_media) * relatorioItem.valor_item;
            relatorioItem.lucro_previsto_15d = ((relatorioItem.cliques_previstos_15d / 100) * relatorioItem.taxa_conversao_media) * relatorioItem.lucro_item;
            relatorioItem.cpc_previsto_15d = relatorioItem.cliques_previstos_15d == 0 ? 0 : (relatorioItem.receita_prevista_15d) / ((relatorioItem.cliques_previstos_15d) * roiEsperado);


            var totalViewsAdwords30d = (relatorio30).Sum(x => x.impressoes_adwords);
            relatorioItem.cliques_previstos_30d = (totalViewsAdwords30d / 100) * relatorioItem.ctr_medio;
            relatorioItem.receita_prevista_30d = ((relatorioItem.cliques_previstos_30d / 100) * relatorioItem.taxa_conversao_media) * relatorioItem.valor_item;
            relatorioItem.lucro_previsto_30d = ((relatorioItem.cliques_previstos_30d / 100) * relatorioItem.taxa_conversao_media) * relatorioItem.lucro_item;
            relatorioItem.cpc_previsto_30d = relatorioItem.cliques_previstos_30d == 0 ? 0 : (relatorioItem.receita_prevista_30d) / ((relatorioItem.cliques_previstos_30d) * roiEsperado);


            var totalViewsAdwords90d = (relatorio90).Sum(x => x.impressoes_adwords);
            relatorioItem.cliques_previstos_90d = (totalViewsAdwords90d / 100) * relatorioItem.ctr_medio;
            relatorioItem.receita_prevista_90d = ((relatorioItem.cliques_previstos_90d / 100) * relatorioItem.taxa_conversao_media) * relatorioItem.valor_item;
            relatorioItem.lucro_previsto_90d = ((relatorioItem.cliques_previstos_90d / 100) * relatorioItem.taxa_conversao_media) * relatorioItem.lucro_item;
            relatorioItem.cpc_previsto_90d = relatorioItem.cliques_previstos_90d == 0 ? 0 : (relatorioItem.receita_prevista_90d) / ((relatorioItem.cliques_previstos_90d) * roiEsperado);


            /*produto.cliques_previstos = (totalViewsAdwords / 100) * produto.ctr_medio;
            produto.receita_revista = ((produto.cliques_previstos / 100) * produto.taxa_conversao_media) * produto.valor_item;
            produto.lucro_previsto = ((produto.cliques_previstos / 100) * produto.taxa_conversao_media) * produto.lucro_item;
            produto.cpc_previsto = produto.cliques_previstos == 0 ? 0 : (produto.receita_revista) / ((produto.cliques_previstos) * roiEsperado);*/



            relatorioItem.sku = produto.produtoId;
            relatorioFinal.Add(relatorioItem);
        }

        foreach (var produto in relatorioFinal)
        {


            int deltaCpc1d = 1;
            int deltaCpc7d = 1;
            int deltaCpc15d = 1;
            int deltaCpc30d = 1;
            int deltaCpc90d = 1;

            produto.cpc_medio_previsto = ((produto.cpc_previsto_1d * deltaCpc1d) + (produto.cpc_previsto_7d * deltaCpc7d) + (produto.cpc_previsto_15d * deltaCpc15d) + (produto.cpc_previsto_30d * deltaCpc30d) + (produto.cpc_previsto_90d * deltaCpc90d)) / 5;

        }

        decimal cpcMinimoAdwords = relatorio90.Where(x => x.cpc_adwords > 0 && x.cliques_adwords > 10).OrderBy(x => x.cpc_adwords).First().cpc_adwords;
        decimal cpcMaximoAdwords = relatorio90.OrderByDescending(x => x.cpc_adwords).First().cpc_adwords;
        int indice = 0;
        decimal fatorDeCorrecao = 30;
        foreach (var produto in relatorioFinal.OrderByDescending(x => x.cpc_medio_previsto))
        {
            var adwordsIndice = relatorio90.Where(x => x.cpc_adwords > 0 && x.cliques_adwords > 10).OrderByDescending(x => x.cpc_adwords).Skip(indice).FirstOrDefault();
            if (adwordsIndice != null)
            {
                produto.cpc_previsto_ajustado = adwordsIndice.cpc_adwords + ((adwordsIndice.cpc_adwords / 100) * fatorDeCorrecao);
            }
            else
            {
                produto.cpc_previsto_ajustado = cpcMinimoAdwords;
            }
            indice++;
        }

        FileInfo relatorioFilefinal = new FileInfo("c:\\bid_manager\\" + nomeCampanha + "_relatorio_full_final.csv");
        if (relatorioFilefinal.Exists)
        {
            relatorioFilefinal.Delete();
        }
        using (StreamWriter swRelatorio = relatorioFilefinal.CreateText())
        {

            swRelatorio.WriteLine("sku;nome;valor_item;lucro_item;ctr_medio;taxa_conversao_media;cliques_previstos_1d;conversoes_previstas_1d;receita_prevista_1d;lucro_previsto_1d;cpc_previsto_1d;cliques_previstos_7d;conversoes_previstas_7d;receita_prevista_7d;lucro_previsto_7d;cpc_previsto_7d;cliques_previstos_15d;conversoes_previstas_15d;receita_prevista_15d;lucro_previsto_15d;cpc_previsto_15d;cliques_previstos_30d;conversoes_previstas_30d;receita_prevista_30d;lucro_previsto_30d;cpc_previsto_30d;cliques_previstos_90d;conversoes_previstas_90d;receita_prevista_90d;lucro_previsto_90d;cpc_previsto_90d;cpc_medio_previsto;indice_ajuste;cpc_previsto_ajustado;");
            foreach (var produto in relatorioFinal)
            {
                swRelatorio.WriteLine(produto.sku + ";" + produtosKit.First(x => x.produtoId == produto.sku).produtoNome.Replace(";", "") + ";" + produto.valor_item + ";" + (produto.lucro_item) + ";" + produto.ctr_medio + ";"
                    + produto.taxa_conversao_media + ";" + produto.cliques_previstos_1d + ";" + produto.conversoes_previstas_1d + ";" + produto.receita_prevista_1d + ";"
                    + produto.lucro_previsto_1d + ";" + produto.cpc_previsto_1d + ";" + produto.cliques_previstos_7d + ";" + produto.conversoes_previstas_7d + ";" + produto.receita_prevista_7d
                    + ";" + produto.lucro_previsto_7d + ";" + produto.cpc_previsto_7d + ";" + produto.cliques_previstos_15d + ";" + produto.conversoes_previstas_15d + ";"
                    + produto.receita_prevista_15d + ";" + produto.lucro_previsto_15d + ";" + produto.cpc_previsto_15d + ";" + produto.cliques_previstos_30d
                    + ";" + produto.conversoes_previstas_30d + ";" + produto.receita_prevista_30d + ";" + produto.lucro_previsto_30d + ";" + produto.cpc_previsto_30d
                    + ";" + produto.cliques_previstos_90d + ";" + produto.conversoes_previstas_90d + ";" + produto.receita_prevista_90d + ";" + produto.lucro_previsto_90d
                    + ";" + produto.cpc_previsto_90d + ";" + produto.cpc_medio_previsto + ";" + produto.indice_ajuste + ";" + produto.cpc_previsto_ajustado + ";");
            }
        }

        FileInfo file_konduza_new_d_kit_berco = new FileInfo("c:\\bid_manager\\konduza-new-d-" + nomeCampanha + ".csv");
        if (file_konduza_new_d_kit_berco.Exists)
        {
            file_konduza_new_d_kit_berco.Delete();
        }
        using (StreamWriter swRelatorio = file_konduza_new_d_kit_berco.CreateText())
        {

            swRelatorio.WriteLine("Campaign	Ad Group\tProduct Group\tMax CPC");
            foreach (var produto in relatorioFinal)
            {
                swRelatorio.WriteLine("konduza-new-d-" + nomeCampanha + "\t" + produto.adGroup + "\t* / Item ID='" + produto.sku + "'\t" + produto.cpc_previsto_ajustado.ToString("0.00").Replace(",", "."));
            }
        }

        FileInfo file_konduza_new_m_kit_berco = new FileInfo("c:\\bid_manager\\konduza-new-m-" + nomeCampanha + ".csv");
        if (file_konduza_new_m_kit_berco.Exists)
        {
            file_konduza_new_m_kit_berco.Delete();
        }
        using (StreamWriter swRelatorio = file_konduza_new_m_kit_berco.CreateText())
        {

            swRelatorio.WriteLine("Campaign	Ad Group\tProduct Group\tMax CPC");
            foreach (var produto in relatorioFinal)
            {
                swRelatorio.WriteLine("konduza-new-m-" + nomeCampanha + "\t" + produto.adGroup + "\t* / Item ID='" + produto.sku + "'\t" + produto.cpc_previsto_ajustado.ToString("0.00").Replace(",", "."));
            }
        }

        foreach (var produto in relatorioFinal.OrderByDescending(x => x.cpc_previsto_ajustado).ToList())
        {
            Response.Write("<img src=\"http://cdn2.graodegente.com.br/fotos/" + produto.sku + "/media_" + produtosKit.First(x => x.produtoId == produto.sku).fotoDestaque + ".jpg\" /><br>" + produto.sku + " - " + produto.cpc_previsto_ajustado.ToString("C") + " - " + produto.cpc_medio_previsto.ToString("C") + " - " + produto.valor_item.ToString("C") + " - " + produtosKit.First(x => x.produtoId == produto.sku).produtoNome + "<br><br>");
        }
    }

    private void campanha_moveis2()
    {
        string nomeCampanha = "moveis";
        var listaGrupos = new List<adGroup>();
        listaGrupos.Add(new adGroup { categoriaId = 990, adGroupName = "colchao" });
        listaGrupos.Add(new adGroup { categoriaId = 1424, adGroupName = "mini-cama" });
        listaGrupos.Add(new adGroup { categoriaId = 1003, adGroupName = "guarda-roupas" });
        listaGrupos.Add(new adGroup { categoriaId = 1002, adGroupName = "comodas" });
        listaGrupos.Add(new adGroup { categoriaId = 1005, adGroupName = "quarto" });
        listaGrupos.Add(new adGroup { categoriaId = 1004, adGroupName = "cama-auxiliar" });
        listaGrupos.Add(new adGroup { categoriaId = 1251, adGroupName = "poltronas" });
        listaGrupos.Add(new adGroup { categoriaId = 761, adGroupName = "prateleiras" });
        //listaGrupos.Add(new adGroup { categoriaId = 1385, adGroupName = "montessoriano" });
        listaGrupos.Add(new adGroup { categoriaId = 1459, adGroupName = "cabanas" });
        listaGrupos.Add(new adGroup { categoriaId = 1617, adGroupName = "mesas" });
        var data = new dbCommerceDataContext();
        var produtosKit = (from c in data.tbJuncaoProdutoCategorias where listaGrupos.Select(x => x.categoriaId).Contains(c.categoriaId) && c.tbProduto.produtoAtivo.ToLower() == "true" && c.tbProduto.produtoEstoqueAtual > 0 select c.tbProduto).Distinct().ToList();
        var produtosCategorias = (from c in data.tbJuncaoProdutoCategorias where listaGrupos.Select(x => x.categoriaId).Contains(c.categoriaId) && c.tbProduto.produtoAtivo.ToLower() == "true" && c.tbProduto.produtoEstoqueAtual > 0 select c).ToList();
        var produtosIds = produtosKit.Select(x => x.produtoId).Distinct().ToList();

        ConcurrentBag<relatorioAgregado> relatorio1 = new ConcurrentBag<relatorioAgregado>();
        ConcurrentBag<relatorioAgregado> relatorio7 = new ConcurrentBag<relatorioAgregado>();
        ConcurrentBag<relatorioAgregado> relatorio15 = new ConcurrentBag<relatorioAgregado>();
        ConcurrentBag<relatorioAgregado> relatorio30 = new ConcurrentBag<relatorioAgregado>();
        ConcurrentBag<relatorioAgregado> relatorio90 = new ConcurrentBag<relatorioAgregado>();
        ConcurrentBag<relatorioFinal> relatorioFinal = new ConcurrentBag<relatorioFinal>();

        List<int> periodos = new List<int>();
        periodos.Add(1);
        periodos.Add(7);
        periodos.Add(15);
        periodos.Add(30);
        periodos.Add(90);
        foreach (var periodo in periodos)
        {
            var dias = DateTime.Now.AddDays(periodo * (-1));
            var vendasKitL = (from c in data.tbItensPedidos where produtosIds.Contains(c.produtoId) && c.dataDaCriacao > dias select c).ToList();
            ConcurrentBag<tbItensPedido> vendasKit = new ConcurrentBag<tbItensPedido>(vendasKitL);

            Parallel.ForEach(produtosKit, produto =>
            {
                var relatorioItem = new relatorioAgregado();
                relatorioItem.sku = produto.produtoId;
                relatorioItem.conversoes_item = vendasKit.Count(x => x.produtoId == produto.produtoId);
                relatorioItem.valor_item = (produto.produtoPrecoPromocional > 0 ? (produto.produtoPrecoPromocional ?? 0) : produto.produtoPreco);
                relatorioItem.lucro_item = Decimal.Round((produto.produtoPrecoPromocional > 0 ? (produto.produtoPrecoPromocional ?? 0) : produto.produtoPreco) - (produto.produtoPrecoDeCusto ?? 0), 2);
                relatorioItem.producao3d = produto.criacaoPropria ?? false;
                if (periodo == 1) relatorio1.Add(relatorioItem);
                if (periodo == 7) relatorio7.Add(relatorioItem);
                if (periodo == 15) relatorio15.Add(relatorioItem);
                if (periodo == 30) relatorio30.Add(relatorioItem);
                if (periodo == 90) relatorio90.Add(relatorioItem);
            });
            /*foreach (var produto in produtosKit)
            {
                var relatorioItem = new relatorioAgregado();
                relatorioItem.sku = produto.produtoId;
                relatorioItem.conversoes_item = vendasKit.Count(x => x.produtoId == produto.produtoId);
                relatorioItem.valor_item = (produto.produtoPrecoPromocional > 0 ? (produto.produtoPrecoPromocional ?? 0) : produto.produtoPreco);
                relatorioItem.lucro_item = Decimal.Round((produto.produtoPrecoPromocional > 0 ? (produto.produtoPrecoPromocional ?? 0) : produto.produtoPreco) - (produto.produtoPrecoDeCusto ?? 0), 2);
                relatorioItem.producao3d = produto.criacaoPropria ?? false;
                if (periodo == 1) relatorio1.Add(relatorioItem);
                if (periodo == 7) relatorio7.Add(relatorioItem);
                if (periodo == 15) relatorio15.Add(relatorioItem);
                if (periodo == 30) relatorio30.Add(relatorioItem);
                if (periodo == 90) relatorio90.Add(relatorioItem);
            }*/

            using (var reader = new StreamReader(@"C:\bid_manager\shopping_ga_" + periodo + "d.csv"))
            {
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Split(';');
                    int produtoId = 0;
                    int.TryParse(values[0], out produtoId);
                    if (produtoId > 0)
                    {

                        var relatorioItem = (periodo == 1 ? relatorio1 : periodo == 7 ? relatorio7 : periodo == 15 ? relatorio15 : periodo == 30 ? relatorio30 : relatorio90).FirstOrDefault(x => x.sku == produtoId);
                        if (relatorioItem != null)
                        {
                            relatorioItem.visualizacoes_lista_ga = Convert.ToInt32(values[1].Replace(".", ""));
                            relatorioItem.visualizacoes_detalhes_ga = Convert.ToInt32(values[2].Replace(".", ""));
                            relatorioItem.adicoes_carrinho_ga = Convert.ToInt32(values[3].Replace(".", ""));
                        }
                    }
                }
            }


            using (var reader = new StreamReader(@"C:\bid_manager\shopping_adwords_" + periodo + "d.csv"))
            {
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Split(';');
                    int produtoId = 0;
                    int.TryParse(values[0], out produtoId);
                    if (produtoId > 0)
                    {

                        var relatorioItem = (periodo == 1 ? relatorio1 : periodo == 7 ? relatorio7 : periodo == 15 ? relatorio15 : periodo == 30 ? relatorio30 : relatorio90).FirstOrDefault(x => x.sku == produtoId);
                        if (relatorioItem != null)
                        {
                            relatorioItem.cliques_adwords = Convert.ToInt32(values[1].Replace(".", ""));
                            relatorioItem.custo_adwords = Convert.ToDecimal(values[2].ToLower().Replace("r", "").Replace("$", "").Replace(" ", "").Replace("\"", ""));
                            relatorioItem.impressoes_adwords = Convert.ToInt32(values[3].Replace(".", ""));
                            relatorioItem.conversoes_adwords = Convert.ToDecimal(values[4].Replace("R$ ", "").Replace("\"", ""));
                        }
                    }
                }
            }

            using (var reader = new StreamReader(@"C:\bid_manager\konduza_" + periodo + "d.csv"))
            {
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Split(';');
                    int produtoId = 0;
                    int.TryParse(values[0], out produtoId);
                    if (produtoId > 0)
                    {

                        var relatorioItem = (periodo == 1 ? relatorio1 : periodo == 7 ? relatorio7 : periodo == 15 ? relatorio15 : periodo == 30 ? relatorio30 : relatorio90).FirstOrDefault(x => x.sku == produtoId);
                        if (relatorioItem != null)
                        {
                            relatorioItem.visualizacoes_lista_konduza = Convert.ToInt32(values[1].Replace(".", ""));
                            relatorioItem.cliques_konduza = Convert.ToInt32(values[2].Replace(".", ""));
                            relatorioItem.visualizacoes_detalhes_konduza = Convert.ToInt32(values[3].Replace(".", ""));
                            relatorioItem.visualizacoes_detalhes_unicos_konduza = Convert.ToInt32(values[4].Replace(".", ""));
                            relatorioItem.adicoes_carrinho_konduza = Convert.ToInt32(values[5].Replace(".", ""));
                            relatorioItem.compras_konduza = Convert.ToInt32(values[8].Replace(".", ""));

                        }
                    }
                }
            }

            int roiEsperado = 3;
            Parallel.ForEach((periodo == 1 ? relatorio1 : periodo == 7 ? relatorio7 : periodo == 15 ? relatorio15 : periodo == 30 ? relatorio30 : relatorio90), produto =>
            {
                var totalViewsAdwords = (periodo == 1 ? relatorio1 : periodo == 7 ? relatorio7 : periodo == 15 ? relatorio15 : periodo == 30 ? relatorio30 : relatorio90).Sum(x => x.impressoes_adwords);
                produto.cliques_previstos = (totalViewsAdwords / 100) * produto.ctr_medio;
                produto.receita_revista = ((produto.cliques_previstos / 100) * produto.taxa_conversao_media) * produto.valor_item;
                produto.lucro_previsto = ((produto.cliques_previstos / 100) * produto.taxa_conversao_media) * produto.lucro_item;
                produto.cpc_previsto = produto.cliques_previstos == 0 ? 0 : (produto.receita_revista) / ((produto.cliques_previstos) * roiEsperado);
            });

            /*foreach (var produto in (periodo == 1 ? relatorio1 : periodo == 7 ? relatorio7 : periodo == 15 ? relatorio15 : periodo == 30 ? relatorio30 : relatorio90))
            {
                var totalViewsAdwords = (periodo == 1 ? relatorio1 : periodo == 7 ? relatorio7 : periodo == 15 ? relatorio15 : periodo == 30 ? relatorio30 : relatorio90).Sum(x => x.impressoes_adwords);
                produto.cliques_previstos = (totalViewsAdwords / 100) * produto.ctr_medio;
                produto.receita_revista = ((produto.cliques_previstos / 100) * produto.taxa_conversao_media) * produto.valor_item;
                produto.lucro_previsto = ((produto.cliques_previstos / 100) * produto.taxa_conversao_media) * produto.lucro_item;
                produto.cpc_previsto = produto.cliques_previstos == 0 ? 0 : (produto.receita_revista) / ((produto.cliques_previstos) * roiEsperado);
            }*/

            FileInfo relatorioFile = new FileInfo("c:\\bid_manager\\" + nomeCampanha + "relatorio_full_" + periodo + ".csv");
            if (relatorioFile.Exists)
            {
                relatorioFile.Delete();
            }
            using (StreamWriter swRelatorio = relatorioFile.CreateText())
            {

                swRelatorio.WriteLine("sku;lucro_item;producao3d;" + "visualizacoes_lista_ga;" + "visualizacoes_detalhes_ga;" + "adicoes_carrinho_ga;" + "ctr_lista_ga;" + "taxa_carrinho_ga;" + "cliques_adwords;" + "custo_adwords;" + "impressoes_adwords;" + "conversoes_adwords;" + "ctr_adwords; cpc_adwords; taxa_conversao_adwords; cpa_adwords;lucro_adwords;" + "visualizacoes_lista_konduza;" + "cliques_konduza;" + "visualizacoes_detalhes_konduza;" + "visualizacoes_detalhes_unicos_konduza;" + "adicoes_carrinho_konduza;" + "compras_konduza;ctr_lista_konduza;taxa_carrinho_konduza;taxa_compra_konduza;lucro_konduza;taxa_carrinho_media;ctr_medio;taxa_conversao_media;cliques_previstos;receita_revista;lucro_previsto;cpc_previsto;");
                foreach (var produto in (periodo == 1 ? relatorio1 : periodo == 7 ? relatorio7 : periodo == 15 ? relatorio15 : periodo == 30 ? relatorio30 : relatorio90))
                {
                    swRelatorio.WriteLine(produto.sku + ";" + produto.lucro_item + ";" + (produto.producao3d ? "s" : "n") + ";" + produto.visualizacoes_lista_ga + ";" + produto.visualizacoes_detalhes_ga + ";" + produto.adicoes_carrinho_ga + ";" + produto.ctr_lista_ga + ";" + produto.taxa_carrinho_ga + ";" + produto.cliques_adwords + ";" + produto.custo_adwords + ";" + produto.impressoes_adwords + ";" + produto.conversoes_adwords + ";" + produto.ctr_adwords + ";" + produto.cpc_adwords + ";" + produto.taxa_conversao_adwords + ";" + produto.cpa_adwords + ";" + produto.lucro_adwords + ";" + produto.visualizacoes_lista_konduza + ";" + produto.cliques_konduza + ";" + produto.visualizacoes_detalhes_konduza + ";" + produto.visualizacoes_detalhes_unicos_konduza + ";" + produto.adicoes_carrinho_konduza + ";" + produto.compras_konduza + ";" + produto.ctr_lista_konduza + ";" + produto.taxa_carrinho_konduza + ";" + produto.taxa_compra_konduza + ";" + produto.lucro_konduza + ";" + produto.taxa_carrinho_media + ";" + produto.ctr_medio + ";" + produto.taxa_conversao_media + ";" + produto.cliques_previstos + ";" + produto.receita_revista + ";" + produto.lucro_previsto + ";" + produto.cpc_previsto + ";");
                }
            }
        }

        Parallel.ForEach(produtosKit, produto =>
        {
            /*var totalViewsAdwords = (periodo == 1 ? relatorio1 : periodo == 7 ? relatorio7 : periodo == 15 ? relatorio15 : periodo == 30 ? relatorio30 : relatorio90).Sum(x => x.impressoes_adwords);
            produto.cliques_previstos = (totalViewsAdwords / 100) * produto.ctr_medio;
            produto.receita_revista = ((produto.cliques_previstos / 100) * produto.taxa_conversao_media) * produto.valor_item;
            produto.lucro_previsto = ((produto.cliques_previstos / 100) * produto.taxa_conversao_media) * produto.lucro_item;
            produto.cpc_previsto = produto.cliques_previstos == 0 ? 0 : (produto.receita_revista) / ((produto.cliques_previstos) * roiEsperado);*/


            //foreach (var produto in produtosKit)
            //{

            var item1d = relatorio1.First(x => x.sku == produto.produtoId);
            var item7d = relatorio7.First(x => x.sku == produto.produtoId);
            var item15d = relatorio15.First(x => x.sku == produto.produtoId);
            var item30d = relatorio30.First(x => x.sku == produto.produtoId);
            var item90d = relatorio90.First(x => x.sku == produto.produtoId);
            decimal deltaCtr1d = 0.03M;
            decimal deltaCtr7d = 0.07M;
            decimal deltaCtr15d = 0.1M;
            decimal deltaCtr30d = 0.3M;
            decimal deltaCtr90d = 0.5M;

            decimal deltaTaxa1d = 0.03M;
            decimal deltaTaxa7d = 0.07M;
            decimal deltaTaxa15d = 0.1M;
            decimal deltaTaxa30d = 0.3M;
            decimal deltaTaxa90d = 0.5M;

            decimal deltaIndiceReal1d = Convert.ToDecimal("0,03");
            decimal deltaIndiceReal7d = Convert.ToDecimal("0,07");
            decimal deltaIndiceReal15d = Convert.ToDecimal("0,1");
            decimal deltaIndiceReal30d = Convert.ToDecimal("0,3");
            decimal deltaIndiceReal90d = Convert.ToDecimal("0,5");


            var relatorioItem = new relatorioFinal();
            var vendaMaxima1 = relatorio1.OrderByDescending(x => x.receita_revista).First().receita_revista;
            var vendaMaxima7 = relatorio7.OrderByDescending(x => x.receita_revista).First().receita_revista;
            var vendaMaxima15 = relatorio15.OrderByDescending(x => x.receita_revista).First().receita_revista;
            var vendaMaxima30 = relatorio30.OrderByDescending(x => x.receita_revista).First().receita_revista;
            var vendaMaxima90 = relatorio90.OrderByDescending(x => x.receita_revista).First().receita_revista;
            var vendaMaxima1real = relatorio1.OrderByDescending(x => (x.valor_item * x.conversoes_item)).Select(x => (x.valor_item * x.conversoes_item)).First();
            var vendaMaxima7real = relatorio7.OrderByDescending(x => (x.valor_item * x.conversoes_item)).Select(x => (x.valor_item * x.conversoes_item)).First();
            var vendaMaxima15real = relatorio15.OrderByDescending(x => (x.valor_item * x.conversoes_item)).Select(x => (x.valor_item * x.conversoes_item)).First();
            var vendaMaxima30real = relatorio30.OrderByDescending(x => (x.valor_item * x.conversoes_item)).Select(x => (x.valor_item * x.conversoes_item)).First();
            var vendaMaxima90real = relatorio90.OrderByDescending(x => (x.valor_item * x.conversoes_item)).Select(x => (x.valor_item * x.conversoes_item)).First();
            decimal indiceAjuste1 = (relatorio1.First(x => x.sku == produto.produtoId).receita_revista * 100) / vendaMaxima1;
            decimal indiceAjuste7 = (relatorio7.First(x => x.sku == produto.produtoId).receita_revista * 100) / vendaMaxima7;
            decimal indiceAjuste15 = (relatorio15.First(x => x.sku == produto.produtoId).receita_revista * 100) / vendaMaxima15;
            decimal indiceAjuste30 = (relatorio30.First(x => x.sku == produto.produtoId).receita_revista * 100) / vendaMaxima30;
            decimal indiceAjuste90 = (relatorio90.First(x => x.sku == produto.produtoId).receita_revista * 100) / vendaMaxima90;
            decimal indiceAjuste1real = vendaMaxima1real == 0 ? Convert.ToDecimal("0") : (relatorio1.Where(x => x.sku == produto.produtoId).Select(x => (x.valor_item * x.conversoes_item)).First() * 100) / vendaMaxima1real;
            decimal indiceAjuste7real = vendaMaxima7real == 0 ? Convert.ToDecimal("0") : (relatorio7.Where(x => x.sku == produto.produtoId).Select(x => (x.valor_item * x.conversoes_item)).First() * 100) / vendaMaxima7real;
            decimal indiceAjuste15real = vendaMaxima15real == 0 ? Convert.ToDecimal("0") : (relatorio15.Where(x => x.sku == produto.produtoId).Select(x => (x.valor_item * x.conversoes_item)).First() * 100) / vendaMaxima15real;
            decimal indiceAjuste30real = vendaMaxima30real == 0 ? Convert.ToDecimal("0") : (relatorio30.Where(x => x.sku == produto.produtoId).Select(x => (x.valor_item * x.conversoes_item)).First() * 100) / vendaMaxima30real;
            decimal indiceAjuste90real = vendaMaxima90real == 0 ? Convert.ToDecimal("0") : (relatorio90.Where(x => x.sku == produto.produtoId).Select(x => (x.valor_item * x.conversoes_item)).First() * 100) / vendaMaxima90real;

            decimal indiceReajusteMedio = (((indiceAjuste1 * deltaIndiceReal1d) + (indiceAjuste7 * deltaIndiceReal7d) + (indiceAjuste15 * deltaIndiceReal15d) + (indiceAjuste30 * deltaIndiceReal30d) + (indiceAjuste90 * deltaIndiceReal90d)));
            decimal indiceReajusteMedioReal = (((indiceAjuste1real * deltaIndiceReal1d) + (indiceAjuste7real * deltaIndiceReal7d) + (indiceAjuste15real * deltaIndiceReal15d) + (indiceAjuste30real * deltaIndiceReal30d) + (indiceAjuste90real * deltaIndiceReal90d)));
            decimal detalAjuste90 = Convert.ToDecimal("0,7");
            relatorioItem.indice_ajuste = ((indiceReajusteMedio * detalAjuste90) + indiceReajusteMedioReal) / 2;
            //relatorioItem.indice_ajuste = indiceAjuste90real;
            decimal ctr_medio_1d = item1d.ctr_medio;
            decimal ctr_medio_7d = item7d.ctr_medio;
            decimal ctr_medio_15d = item15d.ctr_medio;
            decimal ctr_medio_30d = item30d.ctr_medio;
            decimal ctr_medio_90d = item90d.ctr_medio;

            decimal ctr_geral_medio_1d = relatorio1.Where(x => x.ctr_medio > 0).Sum(x => x.ctr_medio) / relatorio1.Count(x => x.ctr_medio > 0);
            decimal ctr_geral_medio_7d = relatorio7.Where(x => x.ctr_medio > 0).Sum(x => x.ctr_medio) / relatorio7.Count(x => x.ctr_medio > 0);
            decimal ctr_geral_medio_15d = relatorio15.Where(x => x.ctr_medio > 0).Sum(x => x.ctr_medio) / relatorio15.Count(x => x.ctr_medio > 0);
            decimal ctr_geral_medio_30d = relatorio30.Where(x => x.ctr_medio > 0).Sum(x => x.ctr_medio) / relatorio30.Count(x => x.ctr_medio > 0);
            decimal ctr_geral_medio_90d = relatorio90.Where(x => x.ctr_medio > 0).Sum(x => x.ctr_medio) / relatorio90.Count(x => x.ctr_medio > 0);

            decimal visualizacoes_detalhes_1d = item1d.visualizacoes_detalhes_unicos_konduza;
            decimal visualizacoes_detalhes_7d = item7d.visualizacoes_detalhes_unicos_konduza;
            decimal visualizacoes_detalhes_15d = item15d.visualizacoes_detalhes_unicos_konduza;
            decimal visualizacoes_detalhes_30d = item30d.visualizacoes_detalhes_unicos_konduza;
            decimal visualizacoes_detalhes_90d = item90d.visualizacoes_detalhes_unicos_konduza;


            decimal visualizacoes_geral_detalhes = relatorio1.Where(x => x.visualizacoes_detalhes_unicos_konduza > 0).Sum(x => x.visualizacoes_detalhes_unicos_konduza) / relatorio1.Count(x => x.visualizacoes_detalhes_unicos_konduza > 0);
            decimal visualizacoes_geral_detalhes_7d = relatorio7.Where(x => x.visualizacoes_detalhes_unicos_konduza > 0).Sum(x => x.visualizacoes_detalhes_unicos_konduza) / relatorio7.Count(x => x.visualizacoes_detalhes_unicos_konduza > 0);
            decimal visualizacoes_geral_detalhes_15d = relatorio15.Where(x => x.visualizacoes_detalhes_unicos_konduza > 0).Sum(x => x.visualizacoes_detalhes_unicos_konduza) / relatorio15.Count(x => x.visualizacoes_detalhes_unicos_konduza > 0);
            decimal visualizacoes_geral_detalhes_30d = relatorio30.Where(x => x.visualizacoes_detalhes_unicos_konduza > 0).Sum(x => x.visualizacoes_detalhes_unicos_konduza) / relatorio30.Count(x => x.visualizacoes_detalhes_unicos_konduza > 0);
            decimal visualizacoes_geral_detalhes_90d = relatorio90.Where(x => x.visualizacoes_detalhes_unicos_konduza > 0).Sum(x => x.visualizacoes_detalhes_unicos_konduza) / relatorio90.Count(x => x.visualizacoes_detalhes_unicos_konduza > 0);

            if (ctr_medio_1d > ctr_geral_medio_1d && visualizacoes_detalhes_1d < visualizacoes_geral_detalhes)
            {
                ctr_medio_1d = relatorio1.Where(x => x.ctr_medio > 0 && x.visualizacoes_detalhes_unicos_konduza < visualizacoes_geral_detalhes).Sum(x => x.ctr_medio) / relatorio1.Count(x => x.ctr_medio > 0 && x.visualizacoes_detalhes_unicos_konduza < visualizacoes_geral_detalhes);
            }
            if (ctr_medio_7d > ctr_geral_medio_7d && visualizacoes_detalhes_7d < visualizacoes_geral_detalhes)
            {
                ctr_medio_7d = relatorio7.Where(x => x.ctr_medio > 0 && x.visualizacoes_detalhes_unicos_konduza < visualizacoes_geral_detalhes).Sum(x => x.ctr_medio) / relatorio7.Count(x => x.ctr_medio > 0 && x.visualizacoes_detalhes_unicos_konduza < visualizacoes_geral_detalhes);
            }
            if (ctr_medio_15d > ctr_geral_medio_15d && visualizacoes_detalhes_15d < visualizacoes_geral_detalhes)
            {
                ctr_medio_15d = relatorio15.Where(x => x.ctr_medio > 0 && x.visualizacoes_detalhes_unicos_konduza < visualizacoes_geral_detalhes).Sum(x => x.ctr_medio) / relatorio15.Count(x => x.ctr_medio > 0 && x.visualizacoes_detalhes_unicos_konduza < visualizacoes_geral_detalhes);
            }
            if (ctr_medio_30d > ctr_geral_medio_30d && visualizacoes_detalhes_30d < visualizacoes_geral_detalhes)
            {
                ctr_medio_30d = relatorio30.Where(x => x.ctr_medio > 0 && x.visualizacoes_detalhes_unicos_konduza < visualizacoes_geral_detalhes).Sum(x => x.ctr_medio) / relatorio30.Count(x => x.ctr_medio > 0 && x.visualizacoes_detalhes_unicos_konduza < visualizacoes_geral_detalhes);
            }
            if (ctr_medio_90d > ctr_geral_medio_90d && visualizacoes_detalhes_90d < visualizacoes_geral_detalhes)
            {
                ctr_medio_90d = relatorio90.Where(x => x.ctr_medio > 0 && x.visualizacoes_detalhes_unicos_konduza < visualizacoes_geral_detalhes).Sum(x => x.ctr_medio) / relatorio90.Count(x => x.ctr_medio > 0 && x.visualizacoes_detalhes_unicos_konduza < visualizacoes_geral_detalhes);
            }


            relatorioItem.ctr_medio = ((((item1d.ctr_medio * deltaCtr1d) + (item7d.ctr_medio * deltaCtr7d) + (item15d.ctr_medio * deltaCtr15d) + (item30d.ctr_medio * deltaCtr30d) + (item90d.ctr_medio * deltaCtr90d))) / 100) * relatorioItem.indice_ajuste;
            relatorioItem.taxa_conversao_media = ((((item1d.taxa_conversao_media * deltaTaxa1d) + (item7d.taxa_conversao_media * deltaTaxa7d) + (item15d.taxa_conversao_media * deltaTaxa15d) + (item30d.taxa_conversao_media * deltaTaxa30d) + (item90d.taxa_conversao_media * deltaTaxa90d))) / 100) * relatorioItem.indice_ajuste;
            relatorioItem.valor_item = (produto.produtoPrecoPromocional > 0 ? (produto.produtoPrecoPromocional ?? 0) : produto.produtoPreco);
            relatorioItem.lucro_item = Decimal.Round((produto.produtoPrecoPromocional > 0 ? (produto.produtoPrecoPromocional ?? 0) : produto.produtoPreco) - (produto.produtoPrecoDeCusto ?? 0), 2);
            relatorioItem.adGroup = listaGrupos.First(x => x.categoriaId == produtosCategorias.First(y => y.produtoId == produto.produtoId).categoriaId).adGroupName;
            int roiEsperado = 3;



            var totalViewsAdwords1d = (relatorio1).Sum(x => x.impressoes_adwords);
            relatorioItem.cliques_previstos_1d = (totalViewsAdwords1d / 100) * relatorioItem.ctr_medio;
            relatorioItem.receita_prevista_1d = ((relatorioItem.cliques_previstos_1d / 100) * relatorioItem.taxa_conversao_media) * relatorioItem.valor_item;
            relatorioItem.lucro_previsto_1d = ((relatorioItem.cliques_previstos_1d / 100) * relatorioItem.taxa_conversao_media) * relatorioItem.lucro_item;
            relatorioItem.cpc_previsto_1d = relatorioItem.cliques_previstos_1d == 0 ? 0 : (relatorioItem.receita_prevista_1d) / ((relatorioItem.cliques_previstos_1d) * roiEsperado);


            var totalViewsAdwords7d = (relatorio7).Sum(x => x.impressoes_adwords);
            relatorioItem.cliques_previstos_7d = (totalViewsAdwords7d / 100) * relatorioItem.ctr_medio;
            relatorioItem.receita_prevista_7d = ((relatorioItem.cliques_previstos_7d / 100) * relatorioItem.taxa_conversao_media) * relatorioItem.valor_item;
            relatorioItem.lucro_previsto_7d = ((relatorioItem.cliques_previstos_7d / 100) * relatorioItem.taxa_conversao_media) * relatorioItem.lucro_item;
            relatorioItem.cpc_previsto_7d = relatorioItem.cliques_previstos_7d == 0 ? 0 : (relatorioItem.receita_prevista_7d) / ((relatorioItem.cliques_previstos_7d) * roiEsperado);


            var totalViewsAdwords15d = (relatorio15).Sum(x => x.impressoes_adwords);
            relatorioItem.cliques_previstos_15d = (totalViewsAdwords15d / 100) * relatorioItem.ctr_medio;
            relatorioItem.receita_prevista_15d = ((relatorioItem.cliques_previstos_15d / 100) * relatorioItem.taxa_conversao_media) * relatorioItem.valor_item;
            relatorioItem.lucro_previsto_15d = ((relatorioItem.cliques_previstos_15d / 100) * relatorioItem.taxa_conversao_media) * relatorioItem.lucro_item;
            relatorioItem.cpc_previsto_15d = relatorioItem.cliques_previstos_15d == 0 ? 0 : (relatorioItem.receita_prevista_15d) / ((relatorioItem.cliques_previstos_15d) * roiEsperado);


            var totalViewsAdwords30d = (relatorio30).Sum(x => x.impressoes_adwords);
            relatorioItem.cliques_previstos_30d = (totalViewsAdwords30d / 100) * relatorioItem.ctr_medio;
            relatorioItem.receita_prevista_30d = ((relatorioItem.cliques_previstos_30d / 100) * relatorioItem.taxa_conversao_media) * relatorioItem.valor_item;
            relatorioItem.lucro_previsto_30d = ((relatorioItem.cliques_previstos_30d / 100) * relatorioItem.taxa_conversao_media) * relatorioItem.lucro_item;
            relatorioItem.cpc_previsto_30d = relatorioItem.cliques_previstos_30d == 0 ? 0 : (relatorioItem.receita_prevista_30d) / ((relatorioItem.cliques_previstos_30d) * roiEsperado);


            var totalViewsAdwords90d = (relatorio90).Sum(x => x.impressoes_adwords);
            relatorioItem.cliques_previstos_90d = (totalViewsAdwords90d / 100) * relatorioItem.ctr_medio;
            relatorioItem.receita_prevista_90d = ((relatorioItem.cliques_previstos_90d / 100) * relatorioItem.taxa_conversao_media) * relatorioItem.valor_item;
            relatorioItem.lucro_previsto_90d = ((relatorioItem.cliques_previstos_90d / 100) * relatorioItem.taxa_conversao_media) * relatorioItem.lucro_item;
            relatorioItem.cpc_previsto_90d = relatorioItem.cliques_previstos_90d == 0 ? 0 : (relatorioItem.receita_prevista_90d) / ((relatorioItem.cliques_previstos_90d) * roiEsperado);


            /*produto.cliques_previstos = (totalViewsAdwords / 100) * produto.ctr_medio;
            produto.receita_revista = ((produto.cliques_previstos / 100) * produto.taxa_conversao_media) * produto.valor_item;
            produto.lucro_previsto = ((produto.cliques_previstos / 100) * produto.taxa_conversao_media) * produto.lucro_item;
            produto.cpc_previsto = produto.cliques_previstos == 0 ? 0 : (produto.receita_revista) / ((produto.cliques_previstos) * roiEsperado);*/



            relatorioItem.sku = produto.produtoId;
            relatorioFinal.Add(relatorioItem);
            //}
        });

        Parallel.ForEach(relatorioFinal, produto =>
        {
            //foreach (var produto in relatorioFinal)
            // {


            int deltaCpc1d = 1;
            int deltaCpc7d = 1;
            int deltaCpc15d = 1;
            int deltaCpc30d = 1;
            int deltaCpc90d = 1;

            produto.cpc_medio_previsto = ((produto.cpc_previsto_1d * deltaCpc1d) + (produto.cpc_previsto_7d * deltaCpc7d) + (produto.cpc_previsto_15d * deltaCpc15d) + (produto.cpc_previsto_30d * deltaCpc30d) + (produto.cpc_previsto_90d * deltaCpc90d)) / 5;

            //}
        });

        decimal cpcMinimoAdwords = relatorio90.Where(x => x.cpc_adwords > 0 && x.cliques_adwords > 10).OrderBy(x => x.cpc_adwords).First().cpc_adwords;
        decimal cpcMaximoAdwords = relatorio90.OrderByDescending(x => x.cpc_adwords).First().cpc_adwords;
        int indice = 0;
        decimal fatorDeCorrecao = 30;
        foreach (var produto in relatorioFinal.OrderByDescending(x => x.cpc_medio_previsto))
        {
            var adwordsIndice = relatorio90.Where(x => x.cpc_adwords > 0 && x.cliques_adwords > 10).OrderByDescending(x => x.cpc_adwords).Skip(indice).FirstOrDefault();
            if (adwordsIndice != null)
            {
                produto.cpc_previsto_ajustado = adwordsIndice.cpc_adwords + ((adwordsIndice.cpc_adwords / 100) * fatorDeCorrecao);
            }
            else
            {
                produto.cpc_previsto_ajustado = cpcMinimoAdwords;
            }
            indice++;
        }

        FileInfo relatorioFilefinal = new FileInfo("c:\\bid_manager\\" + nomeCampanha + "_relatorio_full_final.csv");
        if (relatorioFilefinal.Exists)
        {
            relatorioFilefinal.Delete();
        }
        using (StreamWriter swRelatorio = relatorioFilefinal.CreateText())
        {

            swRelatorio.WriteLine("sku;nome;valor_item;lucro_item;ctr_medio;taxa_conversao_media;cliques_previstos_1d;conversoes_previstas_1d;receita_prevista_1d;lucro_previsto_1d;cpc_previsto_1d;cliques_previstos_7d;conversoes_previstas_7d;receita_prevista_7d;lucro_previsto_7d;cpc_previsto_7d;cliques_previstos_15d;conversoes_previstas_15d;receita_prevista_15d;lucro_previsto_15d;cpc_previsto_15d;cliques_previstos_30d;conversoes_previstas_30d;receita_prevista_30d;lucro_previsto_30d;cpc_previsto_30d;cliques_previstos_90d;conversoes_previstas_90d;receita_prevista_90d;lucro_previsto_90d;cpc_previsto_90d;cpc_medio_previsto;indice_ajuste;cpc_previsto_ajustado;");
            foreach (var produto in relatorioFinal)
            {
                swRelatorio.WriteLine(produto.sku + ";" + produtosKit.First(x => x.produtoId == produto.sku).produtoNome.Replace(";", "") + ";" + produto.valor_item + ";" + (produto.lucro_item) + ";" + produto.ctr_medio + ";"
                    + produto.taxa_conversao_media + ";" + produto.cliques_previstos_1d + ";" + produto.conversoes_previstas_1d + ";" + produto.receita_prevista_1d + ";"
                    + produto.lucro_previsto_1d + ";" + produto.cpc_previsto_1d + ";" + produto.cliques_previstos_7d + ";" + produto.conversoes_previstas_7d + ";" + produto.receita_prevista_7d
                    + ";" + produto.lucro_previsto_7d + ";" + produto.cpc_previsto_7d + ";" + produto.cliques_previstos_15d + ";" + produto.conversoes_previstas_15d + ";"
                    + produto.receita_prevista_15d + ";" + produto.lucro_previsto_15d + ";" + produto.cpc_previsto_15d + ";" + produto.cliques_previstos_30d
                    + ";" + produto.conversoes_previstas_30d + ";" + produto.receita_prevista_30d + ";" + produto.lucro_previsto_30d + ";" + produto.cpc_previsto_30d
                    + ";" + produto.cliques_previstos_90d + ";" + produto.conversoes_previstas_90d + ";" + produto.receita_prevista_90d + ";" + produto.lucro_previsto_90d
                    + ";" + produto.cpc_previsto_90d + ";" + produto.cpc_medio_previsto + ";" + produto.indice_ajuste + ";" + produto.cpc_previsto_ajustado + ";");
            }
        }

        FileInfo file_konduza_new_d_kit_berco = new FileInfo("c:\\bid_manager\\konduza-new-d-" + nomeCampanha + ".csv");
        if (file_konduza_new_d_kit_berco.Exists)
        {
            file_konduza_new_d_kit_berco.Delete();
        }
        using (StreamWriter swRelatorio = file_konduza_new_d_kit_berco.CreateText())
        {

            swRelatorio.WriteLine("Campaign	Ad Group\tProduct Group\tMax CPC");
            foreach (var produto in relatorioFinal)
            {
                swRelatorio.WriteLine("konduza-new-d-" + nomeCampanha + "\t" + produto.adGroup + "\t* / Item ID='" + produto.sku + "'\t" + produto.cpc_previsto_ajustado.ToString("0.00").Replace(",", "."));
            }
        }

        FileInfo file_konduza_new_m_kit_berco = new FileInfo("c:\\bid_manager\\konduza-new-m-" + nomeCampanha + ".csv");
        if (file_konduza_new_m_kit_berco.Exists)
        {
            file_konduza_new_m_kit_berco.Delete();
        }
        using (StreamWriter swRelatorio = file_konduza_new_m_kit_berco.CreateText())
        {

            swRelatorio.WriteLine("Campaign	Ad Group\tProduct Group\tMax CPC");
            foreach (var produto in relatorioFinal)
            {
                swRelatorio.WriteLine("konduza-new-m-" + nomeCampanha + "\t" + produto.adGroup + "\t* / Item ID='" + produto.sku + "'\t" + produto.cpc_previsto_ajustado.ToString("0.00").Replace(",", "."));
            }
        }

        foreach (var produto in relatorioFinal.OrderByDescending(x => x.cpc_previsto_ajustado).ToList())
        {
            Response.Write("<img src=\"http://cdn2.graodegente.com.br/fotos/" + produto.sku + "/media_" + produtosKit.First(x => x.produtoId == produto.sku).fotoDestaque + ".jpg\" /><br>" + produto.sku + " - " + produto.cpc_previsto_ajustado.ToString("C") + " - " + produto.cpc_medio_previsto.ToString("C") + " - " + produto.valor_item.ToString("C") + " - " + produtosKit.First(x => x.produtoId == produto.sku).produtoNome + "<br><br>");
        }
    }

    private void campanha_berco()
    {
        string nomeCampanha = "berco";
        var listaGrupos = new List<adGroup>();
        listaGrupos.Add(new adGroup { categoriaId = 1001, adGroupName = "berco" });
        var data = new dbCommerceDataContext();
        var produtosKit = (from c in data.tbJuncaoProdutoCategorias where listaGrupos.Select(x => x.categoriaId).Contains(c.categoriaId) && c.tbProduto.produtoAtivo.ToLower() == "true" && c.tbProduto.produtoEstoqueAtual > 0 select c.tbProduto).Distinct().ToList();
        var produtosCategorias = (from c in data.tbJuncaoProdutoCategorias where listaGrupos.Select(x => x.categoriaId).Contains(c.categoriaId) && c.tbProduto.produtoAtivo.ToLower() == "true" && c.tbProduto.produtoEstoqueAtual > 0 select c).ToList();
        var produtosIds = produtosKit.Select(x => x.produtoId).Distinct().ToList();

        List<relatorioAgregado> relatorio1 = new List<relatorioAgregado>();
        List<relatorioAgregado> relatorio7 = new List<relatorioAgregado>();
        List<relatorioAgregado> relatorio15 = new List<relatorioAgregado>();
        List<relatorioAgregado> relatorio30 = new List<relatorioAgregado>();
        List<relatorioAgregado> relatorio90 = new List<relatorioAgregado>();
        List<relatorioFinal> relatorioFinal = new List<relatorioFinal>();

        List<int> periodos = new List<int>();
        periodos.Add(1);
        periodos.Add(7);
        periodos.Add(15);
        periodos.Add(30);
        periodos.Add(90);
        foreach (var periodo in periodos)
        {
            var dias = DateTime.Now.AddDays(periodo * (-1));
            var vendasKit = (from c in data.tbItensPedidos where produtosIds.Contains(c.produtoId) && c.dataDaCriacao > dias select c).ToList();
            foreach (var produto in produtosKit)
            {
                var relatorioItem = new relatorioAgregado();
                relatorioItem.sku = produto.produtoId;
                relatorioItem.conversoes_item = vendasKit.Count(x => x.produtoId == produto.produtoId);
                relatorioItem.valor_item = (produto.produtoPrecoPromocional > 0 ? (produto.produtoPrecoPromocional ?? 0) : produto.produtoPreco);
                relatorioItem.lucro_item = Decimal.Round((produto.produtoPrecoPromocional > 0 ? (produto.produtoPrecoPromocional ?? 0) : produto.produtoPreco) - (produto.produtoPrecoDeCusto ?? 0), 2);
                relatorioItem.producao3d = produto.criacaoPropria ?? false;
                if (periodo == 1) relatorio1.Add(relatorioItem);
                if (periodo == 7) relatorio7.Add(relatorioItem);
                if (periodo == 15) relatorio15.Add(relatorioItem);
                if (periodo == 30) relatorio30.Add(relatorioItem);
                if (periodo == 90) relatorio90.Add(relatorioItem);
            }

            using (var reader = new StreamReader(@"C:\bid_manager\shopping_ga_" + periodo + "d.csv"))
            {
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Split(';');
                    int produtoId = 0;
                    int.TryParse(values[0], out produtoId);
                    if (produtoId > 0)
                    {

                        var relatorioItem = (periodo == 1 ? relatorio1 : periodo == 7 ? relatorio7 : periodo == 15 ? relatorio15 : periodo == 30 ? relatorio30 : relatorio90).FirstOrDefault(x => x.sku == produtoId);
                        if (relatorioItem != null)
                        {
                            relatorioItem.visualizacoes_lista_ga = Convert.ToInt32(values[1].Replace(".", ""));
                            relatorioItem.visualizacoes_detalhes_ga = Convert.ToInt32(values[2].Replace(".", ""));
                            relatorioItem.adicoes_carrinho_ga = Convert.ToInt32(values[3].Replace(".", ""));
                        }
                    }
                }
            }


            using (var reader = new StreamReader(@"C:\bid_manager\shopping_adwords_" + periodo + "d.csv"))
            {
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Split(';');
                    int produtoId = 0;
                    int.TryParse(values[0], out produtoId);
                    if (produtoId > 0)
                    {

                        var relatorioItem = (periodo == 1 ? relatorio1 : periodo == 7 ? relatorio7 : periodo == 15 ? relatorio15 : periodo == 30 ? relatorio30 : relatorio90).FirstOrDefault(x => x.sku == produtoId);
                        if (relatorioItem != null)
                        {
                            relatorioItem.cliques_adwords = Convert.ToInt32(values[1].Replace(".", ""));
                            relatorioItem.custo_adwords = Convert.ToDecimal(values[2].ToLower().Replace("r", "").Replace("$", "").Replace(" ", "").Replace("\"", ""));
                            relatorioItem.impressoes_adwords = Convert.ToInt32(values[3].Replace(".", ""));
                            relatorioItem.conversoes_adwords = Convert.ToDecimal(values[4].Replace("R$ ", "").Replace("\"", ""));
                        }
                    }
                }
            }

            using (var reader = new StreamReader(@"C:\bid_manager\konduza_" + periodo + "d.csv"))
            {
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Split(';');
                    int produtoId = 0;
                    int.TryParse(values[0], out produtoId);
                    if (produtoId > 0)
                    {

                        var relatorioItem = (periodo == 1 ? relatorio1 : periodo == 7 ? relatorio7 : periodo == 15 ? relatorio15 : periodo == 30 ? relatorio30 : relatorio90).FirstOrDefault(x => x.sku == produtoId);
                        if (relatorioItem != null)
                        {
                            relatorioItem.visualizacoes_lista_konduza = Convert.ToInt32(values[1].Replace(".", ""));
                            relatorioItem.cliques_konduza = Convert.ToInt32(values[2].Replace(".", ""));
                            relatorioItem.visualizacoes_detalhes_konduza = Convert.ToInt32(values[3].Replace(".", ""));
                            relatorioItem.visualizacoes_detalhes_unicos_konduza = Convert.ToInt32(values[4].Replace(".", ""));
                            relatorioItem.adicoes_carrinho_konduza = Convert.ToInt32(values[5].Replace(".", ""));
                            relatorioItem.compras_konduza = Convert.ToInt32(values[8].Replace(".", ""));

                        }
                    }
                }
            }

            int roiEsperado = 3;
            foreach (var produto in (periodo == 1 ? relatorio1 : periodo == 7 ? relatorio7 : periodo == 15 ? relatorio15 : periodo == 30 ? relatorio30 : relatorio90))
            {
                var totalViewsAdwords = (periodo == 1 ? relatorio1 : periodo == 7 ? relatorio7 : periodo == 15 ? relatorio15 : periodo == 30 ? relatorio30 : relatorio90).Sum(x => x.impressoes_adwords);
                produto.cliques_previstos = (totalViewsAdwords / 100) * produto.ctr_medio;
                produto.receita_revista = ((produto.cliques_previstos / 100) * produto.taxa_conversao_media) * produto.valor_item;
                produto.lucro_previsto = ((produto.cliques_previstos / 100) * produto.taxa_conversao_media) * produto.lucro_item;
                produto.cpc_previsto = produto.cliques_previstos == 0 ? 0 : (produto.receita_revista) / ((produto.cliques_previstos) * roiEsperado);
            }

            FileInfo relatorioFile = new FileInfo("c:\\bid_manager\\" + nomeCampanha + "relatorio_full_" + periodo + ".csv");
            if (relatorioFile.Exists)
            {
                relatorioFile.Delete();
            }
            using (StreamWriter swRelatorio = relatorioFile.CreateText())
            {

                swRelatorio.WriteLine("sku;lucro_item;producao3d;" + "visualizacoes_lista_ga;" + "visualizacoes_detalhes_ga;" + "adicoes_carrinho_ga;" + "ctr_lista_ga;" + "taxa_carrinho_ga;" + "cliques_adwords;" + "custo_adwords;" + "impressoes_adwords;" + "conversoes_adwords;" + "ctr_adwords; cpc_adwords; taxa_conversao_adwords; cpa_adwords;lucro_adwords;" + "visualizacoes_lista_konduza;" + "cliques_konduza;" + "visualizacoes_detalhes_konduza;" + "visualizacoes_detalhes_unicos_konduza;" + "adicoes_carrinho_konduza;" + "compras_konduza;ctr_lista_konduza;taxa_carrinho_konduza;taxa_compra_konduza;lucro_konduza;taxa_carrinho_media;ctr_medio;taxa_conversao_media;cliques_previstos;receita_revista;lucro_previsto;cpc_previsto;");
                foreach (var produto in (periodo == 1 ? relatorio1 : periodo == 7 ? relatorio7 : periodo == 15 ? relatorio15 : periodo == 30 ? relatorio30 : relatorio90))
                {
                    swRelatorio.WriteLine(produto.sku + ";" + produto.lucro_item + ";" + (produto.producao3d ? "s" : "n") + ";" + produto.visualizacoes_lista_ga + ";" + produto.visualizacoes_detalhes_ga + ";" + produto.adicoes_carrinho_ga + ";" + produto.ctr_lista_ga + ";" + produto.taxa_carrinho_ga + ";" + produto.cliques_adwords + ";" + produto.custo_adwords + ";" + produto.impressoes_adwords + ";" + produto.conversoes_adwords + ";" + produto.ctr_adwords + ";" + produto.cpc_adwords + ";" + produto.taxa_conversao_adwords + ";" + produto.cpa_adwords + ";" + produto.lucro_adwords + ";" + produto.visualizacoes_lista_konduza + ";" + produto.cliques_konduza + ";" + produto.visualizacoes_detalhes_konduza + ";" + produto.visualizacoes_detalhes_unicos_konduza + ";" + produto.adicoes_carrinho_konduza + ";" + produto.compras_konduza + ";" + produto.ctr_lista_konduza + ";" + produto.taxa_carrinho_konduza + ";" + produto.taxa_compra_konduza + ";" + produto.lucro_konduza + ";" + produto.taxa_carrinho_media + ";" + produto.ctr_medio + ";" + produto.taxa_conversao_media + ";" + produto.cliques_previstos + ";" + produto.receita_revista + ";" + produto.lucro_previsto + ";" + produto.cpc_previsto + ";");
                }
            }
        }

        foreach (var produto in produtosKit)
        {

            var item1d = relatorio1.First(x => x.sku == produto.produtoId);
            var item7d = relatorio7.First(x => x.sku == produto.produtoId);
            var item15d = relatorio15.First(x => x.sku == produto.produtoId);
            var item30d = relatorio30.First(x => x.sku == produto.produtoId);
            var item90d = relatorio90.First(x => x.sku == produto.produtoId);
            decimal deltaCtr1d = 0.03M;
            decimal deltaCtr7d = 0.07M;
            decimal deltaCtr15d = 0.1M;
            decimal deltaCtr30d = 0.3M;
            decimal deltaCtr90d = 0.5M;

            decimal deltaTaxa1d = 0.03M;
            decimal deltaTaxa7d = 0.07M;
            decimal deltaTaxa15d = 0.1M;
            decimal deltaTaxa30d = 0.3M;
            decimal deltaTaxa90d = 0.5M;

            decimal deltaIndiceReal1d = Convert.ToDecimal("0,03");
            decimal deltaIndiceReal7d = Convert.ToDecimal("0,07");
            decimal deltaIndiceReal15d = Convert.ToDecimal("0,1");
            decimal deltaIndiceReal30d = Convert.ToDecimal("0,3");
            decimal deltaIndiceReal90d = Convert.ToDecimal("0,5");


            var relatorioItem = new relatorioFinal();
            var vendaMaxima1 = relatorio1.OrderByDescending(x => x.receita_revista).First().receita_revista;
            var vendaMaxima7 = relatorio7.OrderByDescending(x => x.receita_revista).First().receita_revista;
            var vendaMaxima15 = relatorio15.OrderByDescending(x => x.receita_revista).First().receita_revista;
            var vendaMaxima30 = relatorio30.OrderByDescending(x => x.receita_revista).First().receita_revista;
            var vendaMaxima90 = relatorio90.OrderByDescending(x => x.receita_revista).First().receita_revista;
            var vendaMaxima1real = relatorio1.OrderByDescending(x => (x.valor_item * x.conversoes_item)).Select(x => (x.valor_item * x.conversoes_item)).First();
            var vendaMaxima7real = relatorio7.OrderByDescending(x => (x.valor_item * x.conversoes_item)).Select(x => (x.valor_item * x.conversoes_item)).First();
            var vendaMaxima15real = relatorio15.OrderByDescending(x => (x.valor_item * x.conversoes_item)).Select(x => (x.valor_item * x.conversoes_item)).First();
            var vendaMaxima30real = relatorio30.OrderByDescending(x => (x.valor_item * x.conversoes_item)).Select(x => (x.valor_item * x.conversoes_item)).First();
            var vendaMaxima90real = relatorio90.OrderByDescending(x => (x.valor_item * x.conversoes_item)).Select(x => (x.valor_item * x.conversoes_item)).First();
            decimal indiceAjuste1 = (relatorio1.First(x => x.sku == produto.produtoId).receita_revista * 100) / vendaMaxima1;
            decimal indiceAjuste7 = (relatorio7.First(x => x.sku == produto.produtoId).receita_revista * 100) / vendaMaxima7;
            decimal indiceAjuste15 = (relatorio15.First(x => x.sku == produto.produtoId).receita_revista * 100) / vendaMaxima15;
            decimal indiceAjuste30 = (relatorio30.First(x => x.sku == produto.produtoId).receita_revista * 100) / vendaMaxima30;
            decimal indiceAjuste90 = (relatorio90.First(x => x.sku == produto.produtoId).receita_revista * 100) / vendaMaxima90;
            decimal indiceAjuste1real = vendaMaxima1real == 0 ? Convert.ToDecimal("0") : (relatorio1.Where(x => x.sku == produto.produtoId).Select(x => (x.valor_item * x.conversoes_item)).First() * 100) / vendaMaxima1real;
            decimal indiceAjuste7real = vendaMaxima7real == 0 ? Convert.ToDecimal("0") : (relatorio7.Where(x => x.sku == produto.produtoId).Select(x => (x.valor_item * x.conversoes_item)).First() * 100) / vendaMaxima7real;
            decimal indiceAjuste15real = vendaMaxima15real == 0 ? Convert.ToDecimal("0") : (relatorio15.Where(x => x.sku == produto.produtoId).Select(x => (x.valor_item * x.conversoes_item)).First() * 100) / vendaMaxima15real;
            decimal indiceAjuste30real = vendaMaxima30real == 0 ? Convert.ToDecimal("0") : (relatorio30.Where(x => x.sku == produto.produtoId).Select(x => (x.valor_item * x.conversoes_item)).First() * 100) / vendaMaxima30real;
            decimal indiceAjuste90real = vendaMaxima90real == 0 ? Convert.ToDecimal("0") : (relatorio90.Where(x => x.sku == produto.produtoId).Select(x => (x.valor_item * x.conversoes_item)).First() * 100) / vendaMaxima90real;

            decimal indiceReajusteMedio = (((indiceAjuste1 * deltaIndiceReal1d) + (indiceAjuste7 * deltaIndiceReal7d) + (indiceAjuste15 * deltaIndiceReal15d) + (indiceAjuste30 * deltaIndiceReal30d) + (indiceAjuste90 * deltaIndiceReal90d)));
            decimal indiceReajusteMedioReal = (((indiceAjuste1real * deltaIndiceReal1d) + (indiceAjuste7real * deltaIndiceReal7d) + (indiceAjuste15real * deltaIndiceReal15d) + (indiceAjuste30real * deltaIndiceReal30d) + (indiceAjuste90real * deltaIndiceReal90d)));
            decimal detalAjuste90 = Convert.ToDecimal("0,7");
            relatorioItem.indice_ajuste = ((indiceReajusteMedio * detalAjuste90) + indiceReajusteMedioReal) / 2;
            //relatorioItem.indice_ajuste = indiceAjuste90real;
            decimal ctr_medio_1d = item1d.ctr_medio;
            decimal ctr_medio_7d = item7d.ctr_medio;
            decimal ctr_medio_15d = item15d.ctr_medio;
            decimal ctr_medio_30d = item30d.ctr_medio;
            decimal ctr_medio_90d = item90d.ctr_medio;

            decimal ctr_geral_medio_1d = relatorio1.Where(x => x.ctr_medio > 0).Sum(x => x.ctr_medio) / relatorio1.Count(x => x.ctr_medio > 0);
            decimal ctr_geral_medio_7d = relatorio7.Where(x => x.ctr_medio > 0).Sum(x => x.ctr_medio) / relatorio7.Count(x => x.ctr_medio > 0);
            decimal ctr_geral_medio_15d = relatorio15.Where(x => x.ctr_medio > 0).Sum(x => x.ctr_medio) / relatorio15.Count(x => x.ctr_medio > 0);
            decimal ctr_geral_medio_30d = relatorio30.Where(x => x.ctr_medio > 0).Sum(x => x.ctr_medio) / relatorio30.Count(x => x.ctr_medio > 0);
            decimal ctr_geral_medio_90d = relatorio90.Where(x => x.ctr_medio > 0).Sum(x => x.ctr_medio) / relatorio90.Count(x => x.ctr_medio > 0);

            decimal visualizacoes_detalhes_1d = item1d.visualizacoes_detalhes_unicos_konduza;
            decimal visualizacoes_detalhes_7d = item7d.visualizacoes_detalhes_unicos_konduza;
            decimal visualizacoes_detalhes_15d = item15d.visualizacoes_detalhes_unicos_konduza;
            decimal visualizacoes_detalhes_30d = item30d.visualizacoes_detalhes_unicos_konduza;
            decimal visualizacoes_detalhes_90d = item90d.visualizacoes_detalhes_unicos_konduza;


            decimal visualizacoes_geral_detalhes = relatorio1.Where(x => x.visualizacoes_detalhes_unicos_konduza > 0).Sum(x => x.visualizacoes_detalhes_unicos_konduza) / relatorio1.Count(x => x.visualizacoes_detalhes_unicos_konduza > 0);
            decimal visualizacoes_geral_detalhes_7d = relatorio7.Where(x => x.visualizacoes_detalhes_unicos_konduza > 0).Sum(x => x.visualizacoes_detalhes_unicos_konduza) / relatorio7.Count(x => x.visualizacoes_detalhes_unicos_konduza > 0);
            decimal visualizacoes_geral_detalhes_15d = relatorio15.Where(x => x.visualizacoes_detalhes_unicos_konduza > 0).Sum(x => x.visualizacoes_detalhes_unicos_konduza) / relatorio15.Count(x => x.visualizacoes_detalhes_unicos_konduza > 0);
            decimal visualizacoes_geral_detalhes_30d = relatorio30.Where(x => x.visualizacoes_detalhes_unicos_konduza > 0).Sum(x => x.visualizacoes_detalhes_unicos_konduza) / relatorio30.Count(x => x.visualizacoes_detalhes_unicos_konduza > 0);
            decimal visualizacoes_geral_detalhes_90d = relatorio90.Where(x => x.visualizacoes_detalhes_unicos_konduza > 0).Sum(x => x.visualizacoes_detalhes_unicos_konduza) / relatorio90.Count(x => x.visualizacoes_detalhes_unicos_konduza > 0);

            if (ctr_medio_1d > ctr_geral_medio_1d && visualizacoes_detalhes_1d < visualizacoes_geral_detalhes)
            {
                ctr_medio_1d = relatorio1.Where(x => x.ctr_medio > 0 && x.visualizacoes_detalhes_unicos_konduza < visualizacoes_geral_detalhes).Sum(x => x.ctr_medio) / relatorio1.Count(x => x.ctr_medio > 0 && x.visualizacoes_detalhes_unicos_konduza < visualizacoes_geral_detalhes);
            }
            if (ctr_medio_7d > ctr_geral_medio_7d && visualizacoes_detalhes_7d < visualizacoes_geral_detalhes)
            {
                ctr_medio_7d = relatorio7.Where(x => x.ctr_medio > 0 && x.visualizacoes_detalhes_unicos_konduza < visualizacoes_geral_detalhes).Sum(x => x.ctr_medio) / relatorio7.Count(x => x.ctr_medio > 0 && x.visualizacoes_detalhes_unicos_konduza < visualizacoes_geral_detalhes);
            }
            if (ctr_medio_15d > ctr_geral_medio_15d && visualizacoes_detalhes_15d < visualizacoes_geral_detalhes)
            {
                ctr_medio_15d = relatorio15.Where(x => x.ctr_medio > 0 && x.visualizacoes_detalhes_unicos_konduza < visualizacoes_geral_detalhes).Sum(x => x.ctr_medio) / relatorio15.Count(x => x.ctr_medio > 0 && x.visualizacoes_detalhes_unicos_konduza < visualizacoes_geral_detalhes);
            }
            if (ctr_medio_30d > ctr_geral_medio_30d && visualizacoes_detalhes_30d < visualizacoes_geral_detalhes)
            {
                ctr_medio_30d = relatorio30.Where(x => x.ctr_medio > 0 && x.visualizacoes_detalhes_unicos_konduza < visualizacoes_geral_detalhes).Sum(x => x.ctr_medio) / relatorio30.Count(x => x.ctr_medio > 0 && x.visualizacoes_detalhes_unicos_konduza < visualizacoes_geral_detalhes);
            }
            if (ctr_medio_90d > ctr_geral_medio_90d && visualizacoes_detalhes_90d < visualizacoes_geral_detalhes)
            {
                ctr_medio_90d = relatorio90.Where(x => x.ctr_medio > 0 && x.visualizacoes_detalhes_unicos_konduza < visualizacoes_geral_detalhes).Sum(x => x.ctr_medio) / relatorio90.Count(x => x.ctr_medio > 0 && x.visualizacoes_detalhes_unicos_konduza < visualizacoes_geral_detalhes);
            }


            relatorioItem.ctr_medio = ((((item1d.ctr_medio * deltaCtr1d) + (item7d.ctr_medio * deltaCtr7d) + (item15d.ctr_medio * deltaCtr15d) + (item30d.ctr_medio * deltaCtr30d) + (item90d.ctr_medio * deltaCtr90d))) / 100) * relatorioItem.indice_ajuste;
            relatorioItem.taxa_conversao_media = ((((item1d.taxa_conversao_media * deltaTaxa1d) + (item7d.taxa_conversao_media * deltaTaxa7d) + (item15d.taxa_conversao_media * deltaTaxa15d) + (item30d.taxa_conversao_media * deltaTaxa30d) + (item90d.taxa_conversao_media * deltaTaxa90d))) / 100) * relatorioItem.indice_ajuste;
            relatorioItem.valor_item = (produto.produtoPrecoPromocional > 0 ? (produto.produtoPrecoPromocional ?? 0) : produto.produtoPreco);
            relatorioItem.lucro_item = Decimal.Round((produto.produtoPrecoPromocional > 0 ? (produto.produtoPrecoPromocional ?? 0) : produto.produtoPreco) - (produto.produtoPrecoDeCusto ?? 0), 2);
            relatorioItem.adGroup = listaGrupos.First(x => x.categoriaId == produtosCategorias.First(y => y.produtoId == produto.produtoId).categoriaId).adGroupName;
            int roiEsperado = 3;



            var totalViewsAdwords1d = (relatorio1).Sum(x => x.impressoes_adwords);
            relatorioItem.cliques_previstos_1d = (totalViewsAdwords1d / 100) * relatorioItem.ctr_medio;
            relatorioItem.receita_prevista_1d = ((relatorioItem.cliques_previstos_1d / 100) * relatorioItem.taxa_conversao_media) * relatorioItem.valor_item;
            relatorioItem.lucro_previsto_1d = ((relatorioItem.cliques_previstos_1d / 100) * relatorioItem.taxa_conversao_media) * relatorioItem.lucro_item;
            relatorioItem.cpc_previsto_1d = relatorioItem.cliques_previstos_1d == 0 ? 0 : (relatorioItem.receita_prevista_1d) / ((relatorioItem.cliques_previstos_1d) * roiEsperado);


            var totalViewsAdwords7d = (relatorio7).Sum(x => x.impressoes_adwords);
            relatorioItem.cliques_previstos_7d = (totalViewsAdwords7d / 100) * relatorioItem.ctr_medio;
            relatorioItem.receita_prevista_7d = ((relatorioItem.cliques_previstos_7d / 100) * relatorioItem.taxa_conversao_media) * relatorioItem.valor_item;
            relatorioItem.lucro_previsto_7d = ((relatorioItem.cliques_previstos_7d / 100) * relatorioItem.taxa_conversao_media) * relatorioItem.lucro_item;
            relatorioItem.cpc_previsto_7d = relatorioItem.cliques_previstos_7d == 0 ? 0 : (relatorioItem.receita_prevista_7d) / ((relatorioItem.cliques_previstos_7d) * roiEsperado);


            var totalViewsAdwords15d = (relatorio15).Sum(x => x.impressoes_adwords);
            relatorioItem.cliques_previstos_15d = (totalViewsAdwords15d / 100) * relatorioItem.ctr_medio;
            relatorioItem.receita_prevista_15d = ((relatorioItem.cliques_previstos_15d / 100) * relatorioItem.taxa_conversao_media) * relatorioItem.valor_item;
            relatorioItem.lucro_previsto_15d = ((relatorioItem.cliques_previstos_15d / 100) * relatorioItem.taxa_conversao_media) * relatorioItem.lucro_item;
            relatorioItem.cpc_previsto_15d = relatorioItem.cliques_previstos_15d == 0 ? 0 : (relatorioItem.receita_prevista_15d) / ((relatorioItem.cliques_previstos_15d) * roiEsperado);


            var totalViewsAdwords30d = (relatorio30).Sum(x => x.impressoes_adwords);
            relatorioItem.cliques_previstos_30d = (totalViewsAdwords30d / 100) * relatorioItem.ctr_medio;
            relatorioItem.receita_prevista_30d = ((relatorioItem.cliques_previstos_30d / 100) * relatorioItem.taxa_conversao_media) * relatorioItem.valor_item;
            relatorioItem.lucro_previsto_30d = ((relatorioItem.cliques_previstos_30d / 100) * relatorioItem.taxa_conversao_media) * relatorioItem.lucro_item;
            relatorioItem.cpc_previsto_30d = relatorioItem.cliques_previstos_30d == 0 ? 0 : (relatorioItem.receita_prevista_30d) / ((relatorioItem.cliques_previstos_30d) * roiEsperado);


            var totalViewsAdwords90d = (relatorio90).Sum(x => x.impressoes_adwords);
            relatorioItem.cliques_previstos_90d = (totalViewsAdwords90d / 100) * relatorioItem.ctr_medio;
            relatorioItem.receita_prevista_90d = ((relatorioItem.cliques_previstos_90d / 100) * relatorioItem.taxa_conversao_media) * relatorioItem.valor_item;
            relatorioItem.lucro_previsto_90d = ((relatorioItem.cliques_previstos_90d / 100) * relatorioItem.taxa_conversao_media) * relatorioItem.lucro_item;
            relatorioItem.cpc_previsto_90d = relatorioItem.cliques_previstos_90d == 0 ? 0 : (relatorioItem.receita_prevista_90d) / ((relatorioItem.cliques_previstos_90d) * roiEsperado);


            /*produto.cliques_previstos = (totalViewsAdwords / 100) * produto.ctr_medio;
            produto.receita_revista = ((produto.cliques_previstos / 100) * produto.taxa_conversao_media) * produto.valor_item;
            produto.lucro_previsto = ((produto.cliques_previstos / 100) * produto.taxa_conversao_media) * produto.lucro_item;
            produto.cpc_previsto = produto.cliques_previstos == 0 ? 0 : (produto.receita_revista) / ((produto.cliques_previstos) * roiEsperado);*/



            relatorioItem.sku = produto.produtoId;
            relatorioFinal.Add(relatorioItem);
        }

        foreach (var produto in relatorioFinal)
        {


            int deltaCpc1d = 1;
            int deltaCpc7d = 1;
            int deltaCpc15d = 1;
            int deltaCpc30d = 1;
            int deltaCpc90d = 1;

            produto.cpc_medio_previsto = ((produto.cpc_previsto_1d * deltaCpc1d) + (produto.cpc_previsto_7d * deltaCpc7d) + (produto.cpc_previsto_15d * deltaCpc15d) + (produto.cpc_previsto_30d * deltaCpc30d) + (produto.cpc_previsto_90d * deltaCpc90d)) / 5;

        }

        decimal cpcMinimoAdwords = relatorio90.Where(x => x.cpc_adwords > 0 && x.cliques_adwords > 10).OrderBy(x => x.cpc_adwords).First().cpc_adwords;
        decimal cpcMaximoAdwords = relatorio90.OrderByDescending(x => x.cpc_adwords).First().cpc_adwords;
        int indice = 0;
        decimal fatorDeCorrecao = 30;
        foreach (var produto in relatorioFinal.OrderByDescending(x => x.cpc_medio_previsto))
        {
            var adwordsIndice = relatorio90.Where(x => x.cpc_adwords > 0 && x.cliques_adwords > 10).OrderByDescending(x => x.cpc_adwords).Skip(indice).FirstOrDefault();
            if (adwordsIndice != null)
            {
                produto.cpc_previsto_ajustado = adwordsIndice.cpc_adwords + ((adwordsIndice.cpc_adwords / 100) * fatorDeCorrecao);
            }
            else
            {
                produto.cpc_previsto_ajustado = cpcMinimoAdwords;
            }
            indice++;
        }

        FileInfo relatorioFilefinal = new FileInfo("c:\\bid_manager\\" + nomeCampanha + "_relatorio_full_final.csv");
        if (relatorioFilefinal.Exists)
        {
            relatorioFilefinal.Delete();
        }
        using (StreamWriter swRelatorio = relatorioFilefinal.CreateText())
        {

            swRelatorio.WriteLine("sku;nome;valor_item;lucro_item;ctr_medio;taxa_conversao_media;cliques_previstos_1d;conversoes_previstas_1d;receita_prevista_1d;lucro_previsto_1d;cpc_previsto_1d;cliques_previstos_7d;conversoes_previstas_7d;receita_prevista_7d;lucro_previsto_7d;cpc_previsto_7d;cliques_previstos_15d;conversoes_previstas_15d;receita_prevista_15d;lucro_previsto_15d;cpc_previsto_15d;cliques_previstos_30d;conversoes_previstas_30d;receita_prevista_30d;lucro_previsto_30d;cpc_previsto_30d;cliques_previstos_90d;conversoes_previstas_90d;receita_prevista_90d;lucro_previsto_90d;cpc_previsto_90d;cpc_medio_previsto;indice_ajuste;cpc_previsto_ajustado;");
            foreach (var produto in relatorioFinal)
            {
                swRelatorio.WriteLine(produto.sku + ";" + produtosKit.First(x => x.produtoId == produto.sku).produtoNome.Replace(";", "") + ";" + produto.valor_item + ";" + (produto.lucro_item) + ";" + produto.ctr_medio + ";"
                    + produto.taxa_conversao_media + ";" + produto.cliques_previstos_1d + ";" + produto.conversoes_previstas_1d + ";" + produto.receita_prevista_1d + ";"
                    + produto.lucro_previsto_1d + ";" + produto.cpc_previsto_1d + ";" + produto.cliques_previstos_7d + ";" + produto.conversoes_previstas_7d + ";" + produto.receita_prevista_7d
                    + ";" + produto.lucro_previsto_7d + ";" + produto.cpc_previsto_7d + ";" + produto.cliques_previstos_15d + ";" + produto.conversoes_previstas_15d + ";"
                    + produto.receita_prevista_15d + ";" + produto.lucro_previsto_15d + ";" + produto.cpc_previsto_15d + ";" + produto.cliques_previstos_30d
                    + ";" + produto.conversoes_previstas_30d + ";" + produto.receita_prevista_30d + ";" + produto.lucro_previsto_30d + ";" + produto.cpc_previsto_30d
                    + ";" + produto.cliques_previstos_90d + ";" + produto.conversoes_previstas_90d + ";" + produto.receita_prevista_90d + ";" + produto.lucro_previsto_90d
                    + ";" + produto.cpc_previsto_90d + ";" + produto.cpc_medio_previsto + ";" + produto.indice_ajuste + ";" + produto.cpc_previsto_ajustado + ";");
            }
        }

        FileInfo file_konduza_new_d_kit_berco = new FileInfo("c:\\bid_manager\\konduza-new-d-" + nomeCampanha + ".csv");
        if (file_konduza_new_d_kit_berco.Exists)
        {
            file_konduza_new_d_kit_berco.Delete();
        }
        using (StreamWriter swRelatorio = file_konduza_new_d_kit_berco.CreateText())
        {

            swRelatorio.WriteLine("Campaign	Ad Group\tProduct Group\tMax CPC");
            foreach (var produto in relatorioFinal)
            {
                swRelatorio.WriteLine("konduza-new-d-" + nomeCampanha + "\t" + produto.adGroup + "\t* / Item ID='" + produto.sku + "'\t" + produto.cpc_previsto_ajustado.ToString("0.00").Replace(",", "."));
            }
        }

        FileInfo file_konduza_new_m_kit_berco = new FileInfo("c:\\bid_manager\\konduza-new-m-" + nomeCampanha + ".csv");
        if (file_konduza_new_m_kit_berco.Exists)
        {
            file_konduza_new_m_kit_berco.Delete();
        }
        using (StreamWriter swRelatorio = file_konduza_new_m_kit_berco.CreateText())
        {

            swRelatorio.WriteLine("Campaign	Ad Group\tProduct Group\tMax CPC");
            foreach (var produto in relatorioFinal)
            {
                swRelatorio.WriteLine("konduza-new-m-" + nomeCampanha + "\t" + produto.adGroup + "\t* / Item ID='" + produto.sku + "'\t" + produto.cpc_previsto_ajustado.ToString("0.00").Replace(",", "."));
            }
        }

        foreach (var produto in relatorioFinal.OrderByDescending(x => x.cpc_previsto_ajustado).ToList())
        {
            Response.Write("<img src=\"http://cdn2.graodegente.com.br/fotos/" + produto.sku + "/media_" + produtosKit.First(x => x.produtoId == produto.sku).fotoDestaque + ".jpg\" /><br>" + produto.sku + " - " + produto.cpc_previsto_ajustado.ToString("C") + " - " + produto.cpc_medio_previsto.ToString("C") + " - " + produto.valor_item.ToString("C") + " - " + produtosKit.First(x => x.produtoId == produto.sku).produtoNome + "<br><br>");
        }
    }

    private void campanha_kit_berco()
    {

        var data = new dbCommerceDataContext();
        var produtosKit = (from c in data.tbJuncaoProdutoCategorias where c.categoriaId == 413 && c.tbProduto.produtoAtivo.ToLower() == "true" && c.tbProduto.produtoEstoqueAtual > 0 select c.tbProduto).ToList();
        var produtosIds = produtosKit.Select(x => x.produtoId).ToList();

        List<relatorioAgregado> relatorio1 = new List<relatorioAgregado>();
        List<relatorioAgregado> relatorio7 = new List<relatorioAgregado>();
        List<relatorioAgregado> relatorio15 = new List<relatorioAgregado>();
        List<relatorioAgregado> relatorio30 = new List<relatorioAgregado>();
        List<relatorioAgregado> relatorio90 = new List<relatorioAgregado>();
        List<relatorioFinal> relatorioFinal = new List<relatorioFinal>();

        List<int> periodos = new List<int>();
        periodos.Add(1);
        periodos.Add(7);
        periodos.Add(15);
        periodos.Add(30);
        periodos.Add(90);
        foreach (var periodo in periodos)
        {
            var dias = DateTime.Now.AddDays(periodo * (-1));
            var vendasKit = (from c in data.tbItensPedidos where produtosIds.Contains(c.produtoId) && c.dataDaCriacao > dias select c).ToList();
            foreach (var produto in produtosKit)
            {
                var relatorioItem = new relatorioAgregado();
                relatorioItem.sku = produto.produtoId;
                relatorioItem.conversoes_item = vendasKit.Count(x => x.produtoId == produto.produtoId);
                relatorioItem.valor_item = (produto.produtoPrecoPromocional > 0 ? (produto.produtoPrecoPromocional ?? 0) : produto.produtoPreco);
                relatorioItem.lucro_item = Decimal.Round((produto.produtoPrecoPromocional > 0 ? (produto.produtoPrecoPromocional ?? 0) : produto.produtoPreco) - (produto.produtoPrecoDeCusto ?? 0), 2);
                relatorioItem.producao3d = produto.criacaoPropria ?? false;
                if (periodo == 1) relatorio1.Add(relatorioItem);
                if (periodo == 7) relatorio7.Add(relatorioItem);
                if (periodo == 15) relatorio15.Add(relatorioItem);
                if (periodo == 30) relatorio30.Add(relatorioItem);
                if (periodo == 90) relatorio90.Add(relatorioItem);
            }

            using (var reader = new StreamReader(@"C:\bid_manager\shopping_ga_" + periodo + "d.csv"))
            {
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Split(';');
                    int produtoId = 0;
                    int.TryParse(values[0], out produtoId);
                    if (produtoId > 0)
                    {

                        var relatorioItem = (periodo == 1 ? relatorio1 : periodo == 7 ? relatorio7 : periodo == 15 ? relatorio15 : periodo == 30 ? relatorio30 : relatorio90).FirstOrDefault(x => x.sku == produtoId);
                        if (relatorioItem != null)
                        {
                            relatorioItem.visualizacoes_lista_ga = Convert.ToInt32(values[1].Replace(".", ""));
                            relatorioItem.visualizacoes_detalhes_ga = Convert.ToInt32(values[2].Replace(".", ""));
                            relatorioItem.adicoes_carrinho_ga = Convert.ToInt32(values[3].Replace(".", ""));
                        }
                    }
                }
            }


            using (var reader = new StreamReader(@"C:\bid_manager\shopping_adwords_" + periodo + "d.csv"))
            {
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Split(';');
                    int produtoId = 0;
                    int.TryParse(values[0], out produtoId);
                    if (produtoId > 0)
                    {

                        var relatorioItem = (periodo == 1 ? relatorio1 : periodo == 7 ? relatorio7 : periodo == 15 ? relatorio15 : periodo == 30 ? relatorio30 : relatorio90).FirstOrDefault(x => x.sku == produtoId);
                        if (relatorioItem != null)
                        {
                            relatorioItem.cliques_adwords = Convert.ToInt32(values[1].Replace(".", ""));
                            relatorioItem.custo_adwords = Convert.ToDecimal(values[2].ToLower().Replace("r", "").Replace("$", "").Replace(" ", "").Replace("\"", ""));
                            relatorioItem.impressoes_adwords = Convert.ToInt32(values[3].Replace(".", ""));
                            relatorioItem.conversoes_adwords = Convert.ToDecimal(values[4].Replace("R$ ", "").Replace("\"", ""));
                        }
                    }
                }
            }

            using (var reader = new StreamReader(@"C:\bid_manager\konduza_" + periodo + "d.csv"))
            {
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Split(';');
                    int produtoId = 0;
                    int.TryParse(values[0], out produtoId);
                    if (produtoId > 0)
                    {

                        var relatorioItem = (periodo == 1 ? relatorio1 : periodo == 7 ? relatorio7 : periodo == 15 ? relatorio15 : periodo == 30 ? relatorio30 : relatorio90).FirstOrDefault(x => x.sku == produtoId);
                        if (relatorioItem != null)
                        {
                            relatorioItem.visualizacoes_lista_konduza = Convert.ToInt32(values[1].Replace(".", ""));
                            relatorioItem.cliques_konduza = Convert.ToInt32(values[2].Replace(".", ""));
                            relatorioItem.visualizacoes_detalhes_konduza = Convert.ToInt32(values[3].Replace(".", ""));
                            relatorioItem.visualizacoes_detalhes_unicos_konduza = Convert.ToInt32(values[4].Replace(".", ""));
                            relatorioItem.adicoes_carrinho_konduza = Convert.ToInt32(values[5].Replace(".", ""));
                            relatorioItem.compras_konduza = Convert.ToInt32(values[8].Replace(".", ""));

                        }
                    }
                }
            }

            int roiEsperado = 3;
            foreach (var produto in (periodo == 1 ? relatorio1 : periodo == 7 ? relatorio7 : periodo == 15 ? relatorio15 : periodo == 30 ? relatorio30 : relatorio90))
            {
                var totalViewsAdwords = (periodo == 1 ? relatorio1 : periodo == 7 ? relatorio7 : periodo == 15 ? relatorio15 : periodo == 30 ? relatorio30 : relatorio90).Sum(x => x.impressoes_adwords);
                produto.cliques_previstos = (totalViewsAdwords / 100) * produto.ctr_medio;
                produto.receita_revista = ((produto.cliques_previstos / 100) * produto.taxa_conversao_media) * produto.valor_item;
                produto.lucro_previsto = ((produto.cliques_previstos / 100) * produto.taxa_conversao_media) * produto.lucro_item;
                produto.cpc_previsto = produto.cliques_previstos == 0 ? 0 : (produto.receita_revista) / ((produto.cliques_previstos) * roiEsperado);
            }

            FileInfo relatorioFile = new FileInfo("c:\\bid_manager\\relatorio_full_" + periodo + ".csv");
            if (relatorioFile.Exists)
            {
                relatorioFile.Delete();
            }
            using (StreamWriter swRelatorio = relatorioFile.CreateText())
            {

                swRelatorio.WriteLine("sku;lucro_item;producao3d;" + "visualizacoes_lista_ga;" + "visualizacoes_detalhes_ga;" + "adicoes_carrinho_ga;" + "ctr_lista_ga;" + "taxa_carrinho_ga;" + "cliques_adwords;" + "custo_adwords;" + "impressoes_adwords;" + "conversoes_adwords;" + "ctr_adwords; cpc_adwords; taxa_conversao_adwords; cpa_adwords;lucro_adwords;" + "visualizacoes_lista_konduza;" + "cliques_konduza;" + "visualizacoes_detalhes_konduza;" + "visualizacoes_detalhes_unicos_konduza;" + "adicoes_carrinho_konduza;" + "compras_konduza;ctr_lista_konduza;taxa_carrinho_konduza;taxa_compra_konduza;lucro_konduza;taxa_carrinho_media;ctr_medio;taxa_conversao_media;cliques_previstos;receita_revista;lucro_previsto;cpc_previsto;");
                foreach (var produto in (periodo == 1 ? relatorio1 : periodo == 7 ? relatorio7 : periodo == 15 ? relatorio15 : periodo == 30 ? relatorio30 : relatorio90))
                {
                    swRelatorio.WriteLine(produto.sku + ";" + produto.lucro_item + ";" + (produto.producao3d ? "s" : "n") + ";" + produto.visualizacoes_lista_ga + ";" + produto.visualizacoes_detalhes_ga + ";" + produto.adicoes_carrinho_ga + ";" + produto.ctr_lista_ga + ";" + produto.taxa_carrinho_ga + ";" + produto.cliques_adwords + ";" + produto.custo_adwords + ";" + produto.impressoes_adwords + ";" + produto.conversoes_adwords + ";" + produto.ctr_adwords + ";" + produto.cpc_adwords + ";" + produto.taxa_conversao_adwords + ";" + produto.cpa_adwords + ";" + produto.lucro_adwords + ";" + produto.visualizacoes_lista_konduza + ";" + produto.cliques_konduza + ";" + produto.visualizacoes_detalhes_konduza + ";" + produto.visualizacoes_detalhes_unicos_konduza + ";" + produto.adicoes_carrinho_konduza + ";" + produto.compras_konduza + ";" + produto.ctr_lista_konduza + ";" + produto.taxa_carrinho_konduza + ";" + produto.taxa_compra_konduza + ";" + produto.lucro_konduza + ";" + produto.taxa_carrinho_media + ";" + produto.ctr_medio + ";" + produto.taxa_conversao_media + ";" + produto.cliques_previstos + ";" + produto.receita_revista + ";" + produto.lucro_previsto + ";" + produto.cpc_previsto + ";");
                }
            }
        }

        foreach (var produto in produtosKit)
        {

            var item1d = relatorio1.First(x => x.sku == produto.produtoId);
            var item7d = relatorio7.First(x => x.sku == produto.produtoId);
            var item15d = relatorio15.First(x => x.sku == produto.produtoId);
            var item30d = relatorio30.First(x => x.sku == produto.produtoId);
            var item90d = relatorio90.First(x => x.sku == produto.produtoId);
            decimal deltaCtr1d = 0.03M;
            decimal deltaCtr7d = 0.07M;
            decimal deltaCtr15d = 0.1M;
            decimal deltaCtr30d = 0.3M;
            decimal deltaCtr90d = 0.5M;

            decimal deltaTaxa1d = 0.03M;
            decimal deltaTaxa7d = 0.07M;
            decimal deltaTaxa15d = 0.1M;
            decimal deltaTaxa30d = 0.3M;
            decimal deltaTaxa90d = 0.5M;

            decimal deltaIndiceReal1d = Convert.ToDecimal("0,03");
            decimal deltaIndiceReal7d = Convert.ToDecimal("0,07");
            decimal deltaIndiceReal15d = Convert.ToDecimal("0,1");
            decimal deltaIndiceReal30d = Convert.ToDecimal("0,3");
            decimal deltaIndiceReal90d = Convert.ToDecimal("0,5");


            var relatorioItem = new relatorioFinal();
            var vendaMaxima1 = relatorio1.OrderByDescending(x => x.receita_revista).First().receita_revista;
            var vendaMaxima7 = relatorio7.OrderByDescending(x => x.receita_revista).First().receita_revista;
            var vendaMaxima15 = relatorio15.OrderByDescending(x => x.receita_revista).First().receita_revista;
            var vendaMaxima30 = relatorio30.OrderByDescending(x => x.receita_revista).First().receita_revista;
            var vendaMaxima90 = relatorio90.OrderByDescending(x => x.receita_revista).First().receita_revista;
            var vendaMaxima1real = relatorio1.OrderByDescending(x => (x.valor_item * x.conversoes_item)).Select(x => (x.valor_item * x.conversoes_item)).First();
            var vendaMaxima7real = relatorio7.OrderByDescending(x => (x.valor_item * x.conversoes_item)).Select(x => (x.valor_item * x.conversoes_item)).First();
            var vendaMaxima15real = relatorio15.OrderByDescending(x => (x.valor_item * x.conversoes_item)).Select(x => (x.valor_item * x.conversoes_item)).First();
            var vendaMaxima30real = relatorio30.OrderByDescending(x => (x.valor_item * x.conversoes_item)).Select(x => (x.valor_item * x.conversoes_item)).First();
            var vendaMaxima90real = relatorio90.OrderByDescending(x => (x.valor_item * x.conversoes_item)).Select(x => (x.valor_item * x.conversoes_item)).First();
            decimal indiceAjuste1 = (relatorio1.First(x => x.sku == produto.produtoId).receita_revista * 100) / vendaMaxima1;
            decimal indiceAjuste7 = (relatorio7.First(x => x.sku == produto.produtoId).receita_revista * 100) / vendaMaxima7;
            decimal indiceAjuste15 = (relatorio15.First(x => x.sku == produto.produtoId).receita_revista * 100) / vendaMaxima15;
            decimal indiceAjuste30 = (relatorio30.First(x => x.sku == produto.produtoId).receita_revista * 100) / vendaMaxima30;
            decimal indiceAjuste90 = (relatorio90.First(x => x.sku == produto.produtoId).receita_revista * 100) / vendaMaxima90;
            decimal indiceAjuste1real = vendaMaxima1real == 0 ? Convert.ToDecimal("0") : (relatorio1.Where(x => x.sku == produto.produtoId).Select(x => (x.valor_item * x.conversoes_item)).First() * 100) / vendaMaxima1real;
            decimal indiceAjuste7real = vendaMaxima7real == 0 ? Convert.ToDecimal("0") : (relatorio7.Where(x => x.sku == produto.produtoId).Select(x => (x.valor_item * x.conversoes_item)).First() * 100) / vendaMaxima7real;
            decimal indiceAjuste15real = vendaMaxima15real == 0 ? Convert.ToDecimal("0") : (relatorio15.Where(x => x.sku == produto.produtoId).Select(x => (x.valor_item * x.conversoes_item)).First() * 100) / vendaMaxima15real;
            decimal indiceAjuste30real = vendaMaxima30real == 0 ? Convert.ToDecimal("0") : (relatorio30.Where(x => x.sku == produto.produtoId).Select(x => (x.valor_item * x.conversoes_item)).First() * 100) / vendaMaxima30real;
            decimal indiceAjuste90real = vendaMaxima90real == 0 ? Convert.ToDecimal("0") : (relatorio90.Where(x => x.sku == produto.produtoId).Select(x => (x.valor_item * x.conversoes_item)).First() * 100) / vendaMaxima90real;

            decimal indiceReajusteMedio = (((indiceAjuste1 * deltaIndiceReal1d) + (indiceAjuste7 * deltaIndiceReal7d) + (indiceAjuste15 * deltaIndiceReal15d) + (indiceAjuste30 * deltaIndiceReal30d) + (indiceAjuste90 * deltaIndiceReal90d)));
            decimal indiceReajusteMedioReal = (((indiceAjuste1real * deltaIndiceReal1d) + (indiceAjuste7real * deltaIndiceReal7d) + (indiceAjuste15real * deltaIndiceReal15d) + (indiceAjuste30real * deltaIndiceReal30d) + (indiceAjuste90real * deltaIndiceReal90d)));
            decimal detalAjuste90 = Convert.ToDecimal("0,7");
            relatorioItem.indice_ajuste = ((indiceReajusteMedio * detalAjuste90) + indiceReajusteMedioReal) / 2;
            //relatorioItem.indice_ajuste = indiceAjuste90real;
            decimal ctr_medio_1d = item1d.ctr_medio;
            decimal ctr_medio_7d = item7d.ctr_medio;
            decimal ctr_medio_15d = item15d.ctr_medio;
            decimal ctr_medio_30d = item30d.ctr_medio;
            decimal ctr_medio_90d = item90d.ctr_medio;

            decimal ctr_geral_medio_1d = relatorio1.Where(x => x.ctr_medio > 0).Sum(x => x.ctr_medio) / relatorio1.Count(x => x.ctr_medio > 0);
            decimal ctr_geral_medio_7d = relatorio7.Where(x => x.ctr_medio > 0).Sum(x => x.ctr_medio) / relatorio7.Count(x => x.ctr_medio > 0);
            decimal ctr_geral_medio_15d = relatorio15.Where(x => x.ctr_medio > 0).Sum(x => x.ctr_medio) / relatorio15.Count(x => x.ctr_medio > 0);
            decimal ctr_geral_medio_30d = relatorio30.Where(x => x.ctr_medio > 0).Sum(x => x.ctr_medio) / relatorio30.Count(x => x.ctr_medio > 0);
            decimal ctr_geral_medio_90d = relatorio90.Where(x => x.ctr_medio > 0).Sum(x => x.ctr_medio) / relatorio90.Count(x => x.ctr_medio > 0);

            decimal visualizacoes_detalhes_1d = item1d.visualizacoes_detalhes_unicos_konduza;
            decimal visualizacoes_detalhes_7d = item7d.visualizacoes_detalhes_unicos_konduza;
            decimal visualizacoes_detalhes_15d = item15d.visualizacoes_detalhes_unicos_konduza;
            decimal visualizacoes_detalhes_30d = item30d.visualizacoes_detalhes_unicos_konduza;
            decimal visualizacoes_detalhes_90d = item90d.visualizacoes_detalhes_unicos_konduza;


            decimal visualizacoes_geral_detalhes = relatorio1.Where(x => x.visualizacoes_detalhes_unicos_konduza > 0).Sum(x => x.visualizacoes_detalhes_unicos_konduza) / relatorio1.Count(x => x.visualizacoes_detalhes_unicos_konduza > 0);
            decimal visualizacoes_geral_detalhes_7d = relatorio7.Where(x => x.visualizacoes_detalhes_unicos_konduza > 0).Sum(x => x.visualizacoes_detalhes_unicos_konduza) / relatorio7.Count(x => x.visualizacoes_detalhes_unicos_konduza > 0);
            decimal visualizacoes_geral_detalhes_15d = relatorio15.Where(x => x.visualizacoes_detalhes_unicos_konduza > 0).Sum(x => x.visualizacoes_detalhes_unicos_konduza) / relatorio15.Count(x => x.visualizacoes_detalhes_unicos_konduza > 0);
            decimal visualizacoes_geral_detalhes_30d = relatorio30.Where(x => x.visualizacoes_detalhes_unicos_konduza > 0).Sum(x => x.visualizacoes_detalhes_unicos_konduza) / relatorio30.Count(x => x.visualizacoes_detalhes_unicos_konduza > 0);
            decimal visualizacoes_geral_detalhes_90d = relatorio90.Where(x => x.visualizacoes_detalhes_unicos_konduza > 0).Sum(x => x.visualizacoes_detalhes_unicos_konduza) / relatorio90.Count(x => x.visualizacoes_detalhes_unicos_konduza > 0);

            if (ctr_medio_1d > ctr_geral_medio_1d && visualizacoes_detalhes_1d < visualizacoes_geral_detalhes)
            {
                ctr_medio_1d = relatorio1.Where(x => x.ctr_medio > 0 && x.visualizacoes_detalhes_unicos_konduza < visualizacoes_geral_detalhes).Sum(x => x.ctr_medio) / relatorio1.Count(x => x.ctr_medio > 0 && x.visualizacoes_detalhes_unicos_konduza < visualizacoes_geral_detalhes);
            }
            if (ctr_medio_7d > ctr_geral_medio_7d && visualizacoes_detalhes_7d < visualizacoes_geral_detalhes)
            {
                ctr_medio_7d = relatorio7.Where(x => x.ctr_medio > 0 && x.visualizacoes_detalhes_unicos_konduza < visualizacoes_geral_detalhes).Sum(x => x.ctr_medio) / relatorio7.Count(x => x.ctr_medio > 0 && x.visualizacoes_detalhes_unicos_konduza < visualizacoes_geral_detalhes);
            }
            if (ctr_medio_15d > ctr_geral_medio_15d && visualizacoes_detalhes_15d < visualizacoes_geral_detalhes)
            {
                ctr_medio_15d = relatorio15.Where(x => x.ctr_medio > 0 && x.visualizacoes_detalhes_unicos_konduza < visualizacoes_geral_detalhes).Sum(x => x.ctr_medio) / relatorio15.Count(x => x.ctr_medio > 0 && x.visualizacoes_detalhes_unicos_konduza < visualizacoes_geral_detalhes);
            }
            if (ctr_medio_30d > ctr_geral_medio_30d && visualizacoes_detalhes_30d < visualizacoes_geral_detalhes)
            {
                ctr_medio_30d = relatorio30.Where(x => x.ctr_medio > 0 && x.visualizacoes_detalhes_unicos_konduza < visualizacoes_geral_detalhes).Sum(x => x.ctr_medio) / relatorio30.Count(x => x.ctr_medio > 0 && x.visualizacoes_detalhes_unicos_konduza < visualizacoes_geral_detalhes);
            }
            if (ctr_medio_90d > ctr_geral_medio_90d && visualizacoes_detalhes_90d < visualizacoes_geral_detalhes)
            {
                ctr_medio_90d = relatorio90.Where(x => x.ctr_medio > 0 && x.visualizacoes_detalhes_unicos_konduza < visualizacoes_geral_detalhes).Sum(x => x.ctr_medio) / relatorio90.Count(x => x.ctr_medio > 0 && x.visualizacoes_detalhes_unicos_konduza < visualizacoes_geral_detalhes);
            }


            relatorioItem.ctr_medio = ((((item1d.ctr_medio * deltaCtr1d) + (item7d.ctr_medio * deltaCtr7d) + (item15d.ctr_medio * deltaCtr15d) + (item30d.ctr_medio * deltaCtr30d) + (item90d.ctr_medio * deltaCtr90d))) / 100) * relatorioItem.indice_ajuste;
            relatorioItem.taxa_conversao_media = ((((item1d.taxa_conversao_media * deltaTaxa1d) + (item7d.taxa_conversao_media * deltaTaxa7d) + (item15d.taxa_conversao_media * deltaTaxa15d) + (item30d.taxa_conversao_media * deltaTaxa30d) + (item90d.taxa_conversao_media * deltaTaxa90d))) / 100) * relatorioItem.indice_ajuste;
            relatorioItem.valor_item = (produto.produtoPrecoPromocional > 0 ? (produto.produtoPrecoPromocional ?? 0) : produto.produtoPreco);
            relatorioItem.lucro_item = Decimal.Round((produto.produtoPrecoPromocional > 0 ? (produto.produtoPrecoPromocional ?? 0) : produto.produtoPreco) - (produto.produtoPrecoDeCusto ?? 0), 2);

            int roiEsperado = 3;



            var totalViewsAdwords1d = (relatorio1).Sum(x => x.impressoes_adwords);
            relatorioItem.cliques_previstos_1d = (totalViewsAdwords1d / 100) * relatorioItem.ctr_medio;
            relatorioItem.receita_prevista_1d = ((relatorioItem.cliques_previstos_1d / 100) * relatorioItem.taxa_conversao_media) * relatorioItem.valor_item;
            relatorioItem.lucro_previsto_1d = ((relatorioItem.cliques_previstos_1d / 100) * relatorioItem.taxa_conversao_media) * relatorioItem.lucro_item;
            relatorioItem.cpc_previsto_1d = relatorioItem.cliques_previstos_1d == 0 ? 0 : (relatorioItem.receita_prevista_1d) / ((relatorioItem.cliques_previstos_1d) * roiEsperado);


            var totalViewsAdwords7d = (relatorio7).Sum(x => x.impressoes_adwords);
            relatorioItem.cliques_previstos_7d = (totalViewsAdwords7d / 100) * relatorioItem.ctr_medio;
            relatorioItem.receita_prevista_7d = ((relatorioItem.cliques_previstos_7d / 100) * relatorioItem.taxa_conversao_media) * relatorioItem.valor_item;
            relatorioItem.lucro_previsto_7d = ((relatorioItem.cliques_previstos_7d / 100) * relatorioItem.taxa_conversao_media) * relatorioItem.lucro_item;
            relatorioItem.cpc_previsto_7d = relatorioItem.cliques_previstos_7d == 0 ? 0 : (relatorioItem.receita_prevista_7d) / ((relatorioItem.cliques_previstos_7d) * roiEsperado);


            var totalViewsAdwords15d = (relatorio15).Sum(x => x.impressoes_adwords);
            relatorioItem.cliques_previstos_15d = (totalViewsAdwords15d / 100) * relatorioItem.ctr_medio;
            relatorioItem.receita_prevista_15d = ((relatorioItem.cliques_previstos_15d / 100) * relatorioItem.taxa_conversao_media) * relatorioItem.valor_item;
            relatorioItem.lucro_previsto_15d = ((relatorioItem.cliques_previstos_15d / 100) * relatorioItem.taxa_conversao_media) * relatorioItem.lucro_item;
            relatorioItem.cpc_previsto_15d = relatorioItem.cliques_previstos_15d == 0 ? 0 : (relatorioItem.receita_prevista_15d) / ((relatorioItem.cliques_previstos_15d) * roiEsperado);


            var totalViewsAdwords30d = (relatorio30).Sum(x => x.impressoes_adwords);
            relatorioItem.cliques_previstos_30d = (totalViewsAdwords30d / 100) * relatorioItem.ctr_medio;
            relatorioItem.receita_prevista_30d = ((relatorioItem.cliques_previstos_30d / 100) * relatorioItem.taxa_conversao_media) * relatorioItem.valor_item;
            relatorioItem.lucro_previsto_30d = ((relatorioItem.cliques_previstos_30d / 100) * relatorioItem.taxa_conversao_media) * relatorioItem.lucro_item;
            relatorioItem.cpc_previsto_30d = relatorioItem.cliques_previstos_30d == 0 ? 0 : (relatorioItem.receita_prevista_30d) / ((relatorioItem.cliques_previstos_30d) * roiEsperado);


            var totalViewsAdwords90d = (relatorio90).Sum(x => x.impressoes_adwords);
            relatorioItem.cliques_previstos_90d = (totalViewsAdwords90d / 100) * relatorioItem.ctr_medio;
            relatorioItem.receita_prevista_90d = ((relatorioItem.cliques_previstos_90d / 100) * relatorioItem.taxa_conversao_media) * relatorioItem.valor_item;
            relatorioItem.lucro_previsto_90d = ((relatorioItem.cliques_previstos_90d / 100) * relatorioItem.taxa_conversao_media) * relatorioItem.lucro_item;
            relatorioItem.cpc_previsto_90d = relatorioItem.cliques_previstos_90d == 0 ? 0 : (relatorioItem.receita_prevista_90d) / ((relatorioItem.cliques_previstos_90d) * roiEsperado);


            /*produto.cliques_previstos = (totalViewsAdwords / 100) * produto.ctr_medio;
            produto.receita_revista = ((produto.cliques_previstos / 100) * produto.taxa_conversao_media) * produto.valor_item;
            produto.lucro_previsto = ((produto.cliques_previstos / 100) * produto.taxa_conversao_media) * produto.lucro_item;
            produto.cpc_previsto = produto.cliques_previstos == 0 ? 0 : (produto.receita_revista) / ((produto.cliques_previstos) * roiEsperado);*/



            relatorioItem.sku = produto.produtoId;
            relatorioFinal.Add(relatorioItem);
        }

        foreach (var produto in relatorioFinal)
        {


            int deltaCpc1d = 1;
            int deltaCpc7d = 1;
            int deltaCpc15d = 1;
            int deltaCpc30d = 1;
            int deltaCpc90d = 1;

            produto.cpc_medio_previsto = ((produto.cpc_previsto_1d * deltaCpc1d) + (produto.cpc_previsto_7d * deltaCpc7d) + (produto.cpc_previsto_15d * deltaCpc15d) + (produto.cpc_previsto_30d * deltaCpc30d) + (produto.cpc_previsto_90d * deltaCpc90d)) / 5;

        }

        decimal cpcMinimoAdwords = relatorio90.Where(x => x.cpc_adwords > 0 && x.cliques_adwords > 10).OrderBy(x => x.cpc_adwords).First().cpc_adwords;
        decimal cpcMaximoAdwords = relatorio90.OrderByDescending(x => x.cpc_adwords).First().cpc_adwords;
        int indice = 0;
        decimal fatorDeCorrecao = 30;
        foreach (var produto in relatorioFinal.OrderByDescending(x => x.cpc_medio_previsto))
        {
            var adwordsIndice = relatorio90.Where(x => x.cpc_adwords > 0 && x.cliques_adwords > 10).OrderByDescending(x => x.cpc_adwords).Skip(indice).FirstOrDefault();
            if (adwordsIndice != null)
            {
                produto.cpc_previsto_ajustado = adwordsIndice.cpc_adwords + ((adwordsIndice.cpc_adwords / 100) * fatorDeCorrecao);
            }
            else
            {
                produto.cpc_previsto_ajustado = cpcMinimoAdwords;
            }
            indice++;
        }

        FileInfo relatorioFilefinal = new FileInfo("c:\\bid_manager\\relatorio_full_final.csv");
        if (relatorioFilefinal.Exists)
        {
            relatorioFilefinal.Delete();
        }
        using (StreamWriter swRelatorio = relatorioFilefinal.CreateText())
        {

            swRelatorio.WriteLine("sku;nome;valor_item;lucro_item;ctr_medio;taxa_conversao_media;cliques_previstos_1d;conversoes_previstas_1d;receita_prevista_1d;lucro_previsto_1d;cpc_previsto_1d;cliques_previstos_7d;conversoes_previstas_7d;receita_prevista_7d;lucro_previsto_7d;cpc_previsto_7d;cliques_previstos_15d;conversoes_previstas_15d;receita_prevista_15d;lucro_previsto_15d;cpc_previsto_15d;cliques_previstos_30d;conversoes_previstas_30d;receita_prevista_30d;lucro_previsto_30d;cpc_previsto_30d;cliques_previstos_90d;conversoes_previstas_90d;receita_prevista_90d;lucro_previsto_90d;cpc_previsto_90d;cpc_medio_previsto;indice_ajuste;cpc_previsto_ajustado;");
            foreach (var produto in relatorioFinal)
            {
                swRelatorio.WriteLine(produto.sku + ";" + produtosKit.First(x => x.produtoId == produto.sku).produtoNome.Replace(";", "") + ";" + produto.valor_item + ";" + (produto.lucro_item) + ";" + produto.ctr_medio + ";"
                    + produto.taxa_conversao_media + ";" + produto.cliques_previstos_1d + ";" + produto.conversoes_previstas_1d + ";" + produto.receita_prevista_1d + ";"
                    + produto.lucro_previsto_1d + ";" + produto.cpc_previsto_1d + ";" + produto.cliques_previstos_7d + ";" + produto.conversoes_previstas_7d + ";" + produto.receita_prevista_7d
                    + ";" + produto.lucro_previsto_7d + ";" + produto.cpc_previsto_7d + ";" + produto.cliques_previstos_15d + ";" + produto.conversoes_previstas_15d + ";"
                    + produto.receita_prevista_15d + ";" + produto.lucro_previsto_15d + ";" + produto.cpc_previsto_15d + ";" + produto.cliques_previstos_30d
                    + ";" + produto.conversoes_previstas_30d + ";" + produto.receita_prevista_30d + ";" + produto.lucro_previsto_30d + ";" + produto.cpc_previsto_30d
                    + ";" + produto.cliques_previstos_90d + ";" + produto.conversoes_previstas_90d + ";" + produto.receita_prevista_90d + ";" + produto.lucro_previsto_90d
                    + ";" + produto.cpc_previsto_90d + ";" + produto.cpc_medio_previsto + ";" + produto.indice_ajuste + ";" + produto.cpc_previsto_ajustado + ";");
            }
        }

        FileInfo file_konduza_new_d_kit_berco = new FileInfo("c:\\bid_manager\\konduza-new-d-kit-berco.csv");
        if (file_konduza_new_d_kit_berco.Exists)
        {
            file_konduza_new_d_kit_berco.Delete();
        }
        using (StreamWriter swRelatorio = file_konduza_new_d_kit_berco.CreateText())
        {

            swRelatorio.WriteLine("Campaign	Ad Group\tProduct Group\tMax CPC");
            foreach (var produto in relatorioFinal)
            {
                swRelatorio.WriteLine("konduza-new-d-kit-berco\tkit-berco\t* / Item ID='" + produto.sku + "'\t" + produto.cpc_previsto_ajustado.ToString("0.00").Replace(",", "."));
            }
        }

        FileInfo file_konduza_new_m_kit_berco = new FileInfo("c:\\bid_manager\\konduza-new-m-kit-berco.csv");
        if (file_konduza_new_m_kit_berco.Exists)
        {
            file_konduza_new_m_kit_berco.Delete();
        }
        using (StreamWriter swRelatorio = file_konduza_new_m_kit_berco.CreateText())
        {

            swRelatorio.WriteLine("Campaign	Ad Group\tProduct Group\tMax CPC");
            foreach (var produto in relatorioFinal)
            {
                swRelatorio.WriteLine("konduza-new-m-kit-berco\tkit-berco\t* / Item ID='" + produto.sku + "'\t" + produto.cpc_previsto_ajustado.ToString("0.00").Replace(",", "."));
            }
        }

        foreach (var produto in relatorioFinal.OrderByDescending(x => x.cpc_previsto_ajustado).ToList())
        {
            Response.Write("<img src=\"http://cdn2.graodegente.com.br/fotos/" + produto.sku + "/media_" + produtosKit.First(x => x.produtoId == produto.sku).fotoDestaque + ".jpg\" /><br>" + produto.sku + " - " + produto.cpc_previsto_ajustado.ToString("C") + " - " + produto.cpc_medio_previsto.ToString("C") + " - " + produto.valor_item.ToString("C") + " - " + produtosKit.First(x => x.produtoId == produto.sku).produtoNome + "<br><br>");
        }
    }
}