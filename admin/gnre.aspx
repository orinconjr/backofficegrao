﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="gnre.aspx.cs" Inherits="gnre" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        @page { 
            margin: 5px;
            padding:0px;
        }

        body{
            padding:0px;
            margin: 25px;
            font-size: 0.54rem;
        }

        table tr td{
            padding: 0 5px;
            border: 1px solid #000;
        }

        .columnone{
            width: 500px;
        }

        .gnre{
            font-size: 14px;
            height:25px;
            font-weight:bold;
            text-align: center;
        }

        .noborder{
            border-top: 0px;
            border-bottom: 0px;
            border-left: 0px;
            border-right: 0px;
        }

        .center{
            text-align: center;
        }

        .nobrdtb{
            border-top: 0px;
            border-bottom: 0px;
        }

        .noleft{
            border-left: 0px;
        }

        .nobottom{
            border-bottom: 0px;
        }

        .notop{
            border-top: 0px;
        }

        .noright{
            border-right: 0px;
        }

        .borderleft{
            width: 72%;
            border-top: 0px;
            border-bottom: 0px;
            border-right: 0px;
        }

        .borderbottom{
            border-top: 0px;
            border-left: 0px;
            border-right: 0px;
        }

        .borderright{
            width: 28%;
            border-top: 0px;
            border-bottom: 0px;
            border-left: 0px;
        }

        .fontTitle {
            color: #000000;
            font-size: 11px;
            font-weight: bold;
            line-height: 25px;
            font-family: "Arial";
        }

        .fontSubTitle {
            font-size: 8px;
            font-weight: bold;
            padding: 3px 0 5px 0;
            font-family: "Arial";
        }

        .titleField {
            float: left;
            font-size: 8px;
            font-weight: bold;
            line-height: 100%;
            padding-bottom: 3px;
            font-family: "Arial";
        }

        .conteudoField {
            float: left;
            font-size: 9px;
            line-height: 100%;
            padding-bottom: 3px;
            font-family: "Arial";
        }

        .nameVia {
            float: right;
            font-size: 8px;
            padding-top: 3px;
            line-height: 100%;
            font-family: "Arial";
        }

        .width-50 {
            width: 50px;
        }

        .width-60 {
            width: 60px;
        }

        .width-130 {
            width: 130px;
        }

        .ml-60 {
            margin-left: 60px;
        }

        .ml-30 {
            margin-left: 30px;
        }

        .mr-5 {
            margin-right: 5px;
        }

        .pt-5 {
            padding-top: 5px;
        }

        .nopadding {
            padding: 0;
        }

        .pb-15 {
            padding-bottom: 15px;
        }

        .pb-20 {
            padding-bottom: 20px;
        }

        .uppercase {
            text-transform: uppercase;
        }

        .clear-left {
            clear: left;
        }

        .right {
            float: right;
        }

        .alignRight {
            text-align: right;
        }

        .sideLeft tr:nth-child(1n+2) td {
            border-top: none;
        }

        .sideLeft td {
            padding-top: 2px;
            padding-bottom: 2px;
        }

        .sideLeft tr td:last-child {
            border-left: none;
        }

        .sideLeft tr td:first-child {
            border-left: none;
        }

        .sideLeft .titleField, .sideLeft .conteudoField {
            padding-bottom: 0;
        }

        .codigoBarras {
            float: left;
            color: #000000;
            font-size: 10px;
            margin-top: -10px;
            font-weight: bold;
            font-family: "Arial";
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ListView runat="server" id="lstBoleto" OnItemDataBound="lstBoleto_OnItemDataBound">
        <ItemTemplate>
            <table cellspacing="0" cellpadding="1" style="width:100%">
                <tr>
                    <td style="width: 72%;" valign="top" class="noborder nopadding">
                        <table cellspacing="0" cellpadding="1" style="width:100%">
                            <tr>
                                <td class="columnone gnre fontTitle" colspan="2">
                                    Guia Nacional de Recolhimento de Tributos Estaduais - GNRE
                                </td>
                            </tr>
                            <tr>
                                <td class="center nobrdtb fontSubTitle" colspan="2">
                                    Dados do Contribuinte Emitente
                                </td>
                            </tr>
                            <tr>
                                <td class="borderleft">
                                    <span class="titleField">
                                        Razão Social
                                    </span>
                                </td>
                                <td class="borderright" style="width: 50px">
                                    <span class="titleField ml-30">
                                        CNPJ/CPF/Insc. Est.
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td class="borderleft">
                                    <span class="conteudoField">
                                        LGF COMERCIO ELETRONICO LTDA.
                                    </span>
                                </td>
                                <td class="borderright">
                                    <span class="conteudoField ml-30">
                                        26.384.531/0001-19
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td class="notop nobottom" colspan="2">
                                    <span class="titleField width-60">
                                        Endereço:
                                    </span>
                                    <span class="conteudoField uppercase">
                                        R CORONEL HERCULANO COBRA 322
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td class="borderleft">
                                    <span class="titleField width-60">
                                        Município:
                                    </span>
                                    <span class="conteudoField">
                                        POUSO ALEGRE
                                    </span>
                                </td>
                                <td class="borderright">
                                    <span class="titleField width-50 mr-5 alignRight">
                                        UF:
                                    </span>
                                    <span class="conteudoField">
                                        MG
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td class="noright notop">
                                    <span class="titleField width-60">
                                        CEP:
                                    </span>
                                    <span class="conteudoField">
                                        37550000
                                    </span>
                                </td>
                                <td class="noleft notop">
                                    <span class="titleField width-50 mr-5 alignRight">
                                        Telefone:
                                    </span>
                                    <span class="conteudoField">
                                        01135228379
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td class="center nobrdtb fontSubTitle" colspan="2">
                                    Dados do Destinatário
                                </td>
                            </tr>
                            <tr>
                                <td class="notop nobottom" colspan="2">
                                    <span class="titleField width-130">
                                        CNPJ/CPF/Insc. Est.:
                                    </span>
                                    <span class="conteudoField">
                                        <%# Eval("DocumentoDestinatario") %>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td class="notop" colspan="2">
                                    <span class="titleField width-130">
                                        Município:
                                    </span>
                                    <span class="conteudoField">
                                         <%# Eval("MunicipioDestinatario") %>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td class="center nobrdtb fontSubTitle" colspan="2">
                                    Reservado á Fiscalização
                                </td>
                            </tr>
                            <tr>
                                <td class="notop nobottom" colspan="2">
                                    <span class="titleField">
                                        Convênio/Protocolo:
                                    </span>
                                    <span class="conteudoField">
                                    
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td class="notop pb-15" colspan="2">
                                    <span class="titleField">
                                        Produto:
                                    </span>
                                    <span class="conteudoField">
                                    
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td class="nobrdtb" colspan="2" style="height:47px" valign="top">
                                    <span class="titleField pt-5">
                                        Informações Complementares
                                    </span>
                                    <span class="conteudoField clear-left">
                                        <%# Eval("InformacoesComplementares") %>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td class="notop" colspan="2">
                                    <span class="titleField">
                                        Documento válido para pagamento até 
                                    </span>
                                    <span class="conteudoField ml-60">
                                         <%# Convert.ToDateTime(Eval("DataLimiteDePagamento")).ToShortDateString() %>
                                    </span>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td class="noborder nopadding sideLeft" valign="top">
                        <table cellspacing="0" cellpadding="1" style="width:100%; margin-left: -1px;">
                            <tr>
                                <td class="nobottom">
                                    <span class="titleField">
                                        UF Favorecida
                                    </span>
                                </td>
                                <td style="width: 44%" colspan="2" class="nobottom">
                                    <span class="titleField">
                                        Código da Receita
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td class="notop">
                                    <span class="conteudoField">
                                         <%# Eval("UFFavorecida") %>
                                    </span>
                                </td>
                                <td class="notop" colspan="2">
                                    <span class="conteudoField">
                                        <%# Eval("CodigoDaReceita") %>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" class="nobottom">
                                    <span class="titleField">
                                        Nº de Controle
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" align="right" class="notop">
                                    <span class="conteudoField right">
                                        <%# Eval("NumeroControle") %>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" class="nobottom">
                                    <span class="titleField">
                                        Data de Vencimento
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" align="right" class="notop">
                                    <span class="conteudoField right">
                                        <%# Convert.ToDateTime(Eval("DataLimiteDePagamento")).ToShortDateString() %>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" class="nobottom">
                                    <span class="titleField">
                                        Nº do Documento de Origem
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" align="right" class="notop">
                                    <span class="conteudoField right">
                                        <%# Eval("NumeroDoDocumentoDeOrigem") %>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="nobottom">
                                    <span class="titleField">
                                        Período de Referência
                                    </span>
                                </td>
                                <td class="nobottom" align="left">
                                    <span class="titleField">
                                        Parcela
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="notop" align="right">
                                    <span class="conteudoField right">
                                        /
                                    </span>
                                </td>
                                <td class="notop" align="right">
                                    <span class="conteudoField right">
                                        0
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" class="nobottom">
                                    <span class="titleField">
                                        Valor Principal
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" class="notop" align="right">
                                    <span class="conteudoField right">
                                        <%# Convert.ToDecimal(Eval("ValorPrincipal")).ToString("C") %>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" class="nobottom">
                                    <span class="titleField">
                                        Atualização Monetária
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" class="notop" align="right">
                                    <span class="conteudoField right">
                                        R$ 0,00
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" class="nobottom">
                                    <span class="titleField">
                                        Juros
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" class="notop" align="right">
                                    <span class="conteudoField right">
                                        <%# Convert.ToDecimal(Eval("ValorJuros")).ToString("C") %>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" class="nobottom">
                                    <span class="titleField">
                                        Multa
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" class="notop" align="right">
                                    <span class="conteudoField right">
                                        <%# Convert.ToDecimal(Eval("ValorMulta")).ToString("C") %>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" class="nobottom">
                                    <span class="titleField">
                                        Total a Recolher
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" class="notop" align="right">
                                    <span class="conteudoField right">
                                        <%# (Convert.ToDecimal(Eval("ValorPrincipal")) + Convert.ToDecimal(Eval("ValorJuros")) + Convert.ToDecimal(Eval("ValorMulta"))).ToString("C") %>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td class="noborder"></td>
                                <td class="noborder" colspan="2" style="text-align:right;">
                                    <span class="nameVia">
                                        1ª via Banco
                                    </span>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="noborder" style="padding-left: 50px;">
                        <span class="codigoBarras">
                             <%# Eval("RepresentacaooNumerica") %>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td class="noborder nopadding pb-20" style="font-family: 'Interleaved 2of5 NT'; font-size: 32px;">
                        <asp:Image runat="server" ID="imgCodigo1"  />
                    </td>
                </tr>
            </table>
            <br/>
            <hr style="margin-top:0px;border: 1px dotted #000; border-style: none none dotted;margin-bottom: 10px;"/>
            <table cellspacing="0" cellpadding="1" style="width:100%">
                <tr>
                    <td style="width: 72%;" valign="top" class="noborder nopadding">
                        <table cellspacing="0" cellpadding="1" style="width:100%">
                            <tr>
                                <td class="columnone gnre fontTitle" colspan="2">
                                    Guia Nacional de Recolhimento de Tributos Estaduais - GNRE
                                </td>
                            </tr>
                            <tr>
                                <td class="center nobrdtb fontSubTitle" colspan="2">
                                    Dados do Contribuinte Emitente
                                </td>
                            </tr>
                            <tr>
                                <td class="borderleft">
                                    <span class="titleField">
                                        Razão Social
                                    </span>
                                </td>
                                <td class="borderright" style="width: 50px">
                                    <span class="titleField ml-30">
                                        CNPJ/CPF/Insc. Est.
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td class="borderleft">
                                    <span class="conteudoField">
                                        LGF COMERCIO ELETRONICO LTDA.
                                    </span>
                                </td>
                                <td class="borderright">
                                    <span class="conteudoField ml-30">
                                        26.384.531/0001-19
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td class="notop nobottom" colspan="2">
                                    <span class="titleField width-60">
                                        Endereço:
                                    </span>
                                    <span class="conteudoField uppercase">
                                        R CORONEL HERCULANO COBRA 322
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td class="borderleft">
                                    <span class="titleField width-60">
                                        Município:
                                    </span>
                                    <span class="conteudoField">
                                        POUSO ALEGRE
                                    </span>
                                </td>
                                <td class="borderright">
                                    <span class="titleField width-50 mr-5 alignRight">
                                        UF:
                                    </span>
                                    <span class="conteudoField">
                                        MG
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td class="noright notop">
                                    <span class="titleField width-60">
                                        CEP:
                                    </span>
                                    <span class="conteudoField">
                                        14940000
                                    </span>
                                </td>
                                <td class="noleft notop">
                                    <span class="titleField width-50 mr-5 alignRight">
                                        Telefone:
                                    </span>
                                    <span class="conteudoField">
                                        37550000
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td class="center nobrdtb fontSubTitle" colspan="2">
                                    Dados do Destinatário
                                </td>
                            </tr>
                            <tr>
                                <td class="notop nobottom" colspan="2">
                                    <span class="titleField width-130">
                                        CNPJ/CPF/Insc. Est.:
                                    </span>
                                    <span class="conteudoField">
                                        <%# Eval("DocumentoDestinatario") %>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td class="notop" colspan="2">
                                    <span class="titleField width-130">
                                        Município:
                                    </span>
                                    <span class="conteudoField">
                                         <%# Eval("MunicipioDestinatario") %>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td class="center nobrdtb fontSubTitle" colspan="2">
                                    Reservado à Fiscalização
                                </td>
                            </tr>
                            <tr>
                                <td class="notop nobottom" colspan="2">
                                    <span class="titleField">
                                        Convênio/Protocolo:
                                    </span>
                                    <span class="conteudoField">
                                    
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td class="notop pb-15" colspan="2">
                                    <span class="titleField">
                                        Produto:
                                    </span>
                                    <span class="conteudoField">
                                    
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td class="nobrdtb" colspan="2" style="height:47px" valign="top">
                                    <span class="titleField pt-5">
                                        Informações Complementares
                                    </span>
                                    <span class="conteudoField clear-left">
                                        <%# Eval("InformacoesComplementares") %>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td class="notop" colspan="2">
                                    <span class="titleField">
                                        Documento válido para pagamento até 
                                    </span>
                                    <span class="conteudoField ml-60">
                                        <%# Convert.ToDateTime(Eval("DataLimiteDePagamento")).ToShortDateString() %>
                                    </span>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td class="noborder nopadding sideLeft" valign="top">
                        <table cellspacing="0" cellpadding="1" style="width:100%; margin-left: -1px;">
                            <tr>
                                <td class="nobottom">
                                    <span class="titleField">
                                        UF Favorecida
                                    </span>
                                </td>
                                <td style="width: 44%" colspan="2" class="nobottom">
                                    <span class="titleField">
                                        Código da Receita
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td class="notop">
                                    <span class="conteudoField">
                                         <%# Eval("UFFavorecida") %>
                                    </span>
                                </td>
                                <td class="notop" colspan="2">
                                    <span class="conteudoField">
                                        <%# Eval("CodigoDaReceita") %>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" class="nobottom">
                                    <span class="titleField">
                                        Nº de Controle
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" align="right" class="notop">
                                    <span class="conteudoField right">
                                        <%# Eval("NumeroControle") %>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" class="nobottom">
                                    <span class="titleField">
                                        Data de Vencimento
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" align="right" class="notop">
                                    <span class="conteudoField right">
                                        <%# Convert.ToDateTime(Eval("DataLimiteDePagamento")).ToShortDateString() %>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" class="nobottom">
                                    <span class="titleField">
                                        Nº do Documento de Origem
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" align="right" class="notop">
                                    <span class="conteudoField right">
                                        <%# Eval("NumeroDoDocumentoDeOrigem") %>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="nobottom">
                                    <span class="titleField">
                                        Período de Referência
                                    </span>
                                </td>
                                <td class="nobottom" align="left">
                                    <span class="titleField">
                                        Parcela
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="notop" align="right">
                                    <span class="conteudoField right">
                                        /
                                    </span>
                                </td>
                                <td class="notop" align="right">
                                    <span class="conteudoField right">
                                        0
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" class="nobottom">
                                    <span class="titleField">
                                        Valor Principal
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" class="notop" align="right">
                                    <span class="conteudoField right">
                                        <%# Convert.ToDecimal(Eval("ValorPrincipal")).ToString("C") %>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" class="nobottom">
                                    <span class="titleField">
                                        Atualização Monetária
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" class="notop" align="right">
                                    <span class="conteudoField right">
                                        R$ 0,00
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" class="nobottom">
                                    <span class="titleField">
                                        Juros
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" class="notop" align="right">
                                    <span class="conteudoField right">
                                        <%# Convert.ToDecimal(Eval("ValorJuros")).ToString("C") %>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" class="nobottom">
                                    <span class="titleField">
                                        Multa
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" class="notop" align="right">
                                    <span class="conteudoField right">
                                        <%# Convert.ToDecimal(Eval("ValorMulta")).ToString("C") %>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" class="nobottom">
                                    <span class="titleField">
                                        Total a Recolher
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" class="notop" align="right">
                                    <span class="conteudoField right">
                                        <%# (Convert.ToDecimal(Eval("ValorPrincipal")) + Convert.ToDecimal(Eval("ValorJuros")) + Convert.ToDecimal(Eval("ValorMulta"))).ToString("C") %>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td class="noborder"></td>
                                <td class="noborder" colspan="2" style="text-align:right;">
                                    <span class="nameVia">
                                        2ª via Contrinuinte
                                    </span>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="noborder" style="padding-left: 50px;">
                        <span class="codigoBarras">
                            <%# Eval("RepresentacaooNumerica") %>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td class="noborder nopadding pb-20">
                        <asp:Image runat="server" ID="imgCodigo2" />
                    </td>
                </tr>
            </table>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/><hr style="border-width: 1px; border-style: solid; page-break-after: always;">
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
        </ItemTemplate>
    </asp:ListView>
        Valor total do lote: <asp:Literal runat="server" ID="valorTotalLote"></asp:Literal>
    </form>
</body>
</html>
