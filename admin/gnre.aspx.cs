﻿using BarcodeLib.Barcode;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Image = System.Web.UI.WebControls.Image;

public partial class gnre : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
        usuarioLogadoId = Request.Cookies["usuarioLogadoId"];
        if (usuarioLogadoId != null)
        {
            int idNota = 0;
            decimal totalLote = 0;
            if (Request.QueryString["id"] != null)
            {
                var listRegistro = new List<RegistrodeGuia>();
                int.TryParse(Request.QueryString["id"], out idNota);
                var data = new dbCommerceDataContext();
                var notas = (from c in data.tbNotaFiscals where c.idLoteImpressaoGnre == idNota select c).ToList();
                foreach (var nota in notas)
                {
                    if (!string.IsNullOrEmpty(nota.resultado))
                    {
                        using (StringReader reader = new StringReader(nota.resultado))
                        {
                            string line;
                            while ((line = reader.ReadLine()) != null)
                            {
                                if (line.StartsWith("1"))
                                {
                                    var registro = new RegistrodeGuia(line);
                                    listRegistro.Add(registro);
                                    totalLote = totalLote + registro.ValorPrincipal + registro.ValorMulta + registro.ValorJuros;
                                }
                            }
                        }
                    }
                }

                lstBoleto.DataSource = listRegistro;
                lstBoleto.DataBind();
                valorTotalLote.Text = totalLote.ToString("C");
            }
        }
        else
        {
            Response.Clear();
            string content = @"R0lGODlhAQABAPcAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH5BAEAAP8ALAAAAAABAAEAAAgEAP8FBAA7";
            Response.ContentType = "image/gif";
            Response.BinaryWrite(System.Convert.FromBase64String(content));
            Response.End();
        }
    }

    public string GetImage(object img)
    {
        return "data:image/jpg;base64," + Convert.ToBase64String((byte[])img);
    }

    protected void lstBoleto_OnItemDataBound(object sender, ListViewItemEventArgs e)
    {
        var data = (RegistrodeGuia) e.Item.DataItem;
        var imgCodigo1 = (Image) e.Item.FindControl("imgCodigo1");
        var imgCodigo2 = (Image)e.Item.FindControl("imgCodigo2");
        //var imagem = Code128Rendering.MakeBarcodeImage(data.RepresentacaooNumerica, 1, true);
        var imagem = GerarCodigo(data.RepresentacaooNumerica);
        imgCodigo1.ImageUrl = GetImage(imagem);
        imgCodigo2.ImageUrl = GetImage(imagem);
    }

    public static byte[] converterDemo(System.Drawing.Image x)
    {
        ImageConverter _imageConverter = new ImageConverter();
        byte[] xByte = (byte[])_imageConverter.ConvertTo(x, typeof(byte[]));
        return xByte;
    }

    public byte[] GerarCodigo(string codigo)
    {
        string codigoFormatado = "";
        codigoFormatado = codigo.Substring(0, 11);
        codigoFormatado += codigo.Substring(0 + 12, 11);
        codigoFormatado += codigo.Substring(0 + 12 + 12, 11);
        codigoFormatado += codigo.Substring(0 + 12 + 12 + 12, 11);
        //Response.Write(codigoFormatado + "<br>");

        System.Drawing.Bitmap img = new C2of5i(codigoFormatado, 1, 50, codigoFormatado.Length).ToBitmap();
        using (MemoryStream ms = new MemoryStream())
        {
            img.Save(ms, ImageFormat.Jpeg);
            return ms.ToArray();
        }

        BarcodeLib.Barcode.Linear barcode = new BarcodeLib.Barcode.Linear();
        barcode.Type = BarcodeType.INTERLEAVED25;
        barcode.Data = codigo;
        barcode.N = 3;
        barcode.AddCheckSum = true;

        barcode.UOM = UnitOfMeasure.PIXEL;
        barcode.BarWidth = 1;
        barcode.BarHeight = 80;
        barcode.LeftMargin = 10;
        barcode.RightMargin = 10;
        barcode.TopMargin = 10;
        barcode.BottomMargin = 10;

        barcode.ImageFormat = System.Drawing.Imaging.ImageFormat.Png;
        // more barcode settings here
        

        // generate barcode & output to byte array
        byte[] barcodeInBytes = barcode.drawBarcodeAsBytes();
        return barcodeInBytes;
        // generate barcode and output to Bitmap object
        Bitmap barcodeInBitmap = barcode.drawBarcode();
    }
}


public abstract class BarCodeBase
{
    #region Variables
    private string _code;
    private int _height;
    private int _digits;

    private int _thin;
    private int _full;

    protected int xPos = 0;
    protected int yPos = 0;

    private string _contenttype;

    protected Brush BLACK = Brushes.Black;
    protected Brush WHITE = Brushes.White;

    #endregion

    #region Property
    /// <summary>
    /// The Barcode.
    /// </summary>
    public string Code
    {
        get
        {
            try
            {
                return _code;
            }
            catch
            {
                return "";
            }
        }
        set
        {
            try
            {
                _code = value;
            }
            catch
            {
                _code = null;
            }
        }
    }
    /// <summary>
    /// The width of the thin bar (pixels).
    /// </summary>
    public int Width
    {
        get
        {
            try
            {
                return _thin;
            }
            catch
            {
                return 1;
            }
        }
        set
        {
            try
            {
                int temp = value;
                _thin = temp;
                //					_half = temp * 2;
                _full = temp * 3;
            }
            catch
            {
                int temp = 1;
                _thin = temp;
                //					_half = temp * 2;
                _full = temp * 3;
            }
        }
    }
    /// <summary>
    /// The Height of barcode (pixels).
    /// </summary>
    public int Height
    {
        get
        {
            try
            {
                return _height;
            }
            catch
            {
                return 15;
            }
        }
        set
        {
            try
            {
                _height = value;
            }
            catch
            {
                _height = 1;
            }
        }
    }
    /// <summary>
    /// Number of digits of the barcode.
    /// </summary>
    public int Digits
    {
        get
        {
            try
            {
                return _digits;
            }
            catch
            {
                return 0;
            }
        }
        set
        {
            try
            {
                _digits = value;
            }
            catch
            {
                _digits = 0;
            }
        }
    }
    /// <summary>
    /// Content type of code. Default: image/jpeg
    /// </summary>
    public string ContentType
    {
        get
        {
            try
            {
                if (_contenttype == null)
                    return "image/jpeg";
                return _contenttype;
            }
            catch
            {
                return "image/jpeg";
            }
        }
        set
        {
            try
            {
                _contenttype = value;
            }
            catch
            {
                _contenttype = "image/jpeg";
            }
        }
    }
    protected int Thin
    {
        get
        {
            try
            {
                return _thin;
            }
            catch
            {
                return 1;
            }
        }
    }
    protected int Full
    {
        get
        {
            try
            {
                return _full;
            }
            catch
            {
                return 3;
            }
        }
    }
    #endregion
    protected virtual byte[] toByte(Bitmap bitmap)
    {
        MemoryStream mstream = new MemoryStream();
        ImageCodecInfo myImageCodecInfo = GetEncoderInfo(ContentType);

        EncoderParameter myEncoderParameter0 = new EncoderParameter(Encoder.Quality, (long)100);
        EncoderParameters myEncoderParameters = new EncoderParameters(1);
        myEncoderParameters.Param[0] = myEncoderParameter0;

        bitmap.Save(mstream, myImageCodecInfo, myEncoderParameters);

        return mstream.GetBuffer();
    }
    private static ImageCodecInfo GetEncoderInfo(string mimeType)
    {
        int j;
        ImageCodecInfo[] encoders;
        encoders = ImageCodecInfo.GetImageEncoders();
        for (j = 0; j < encoders.Length; ++j)
        {
            if (encoders[j].MimeType == mimeType)
                return encoders[j];
        }
        return null;
    }
    protected virtual void DrawPattern(ref Graphics g, string Pattern)
    {
        int tempWidth;

        for (int i = 0; i < Pattern.Length; i++)
        {
            if (Pattern[i] == '0')
                tempWidth = _thin;
            else
                tempWidth = _full;

            if (i % 2 == 0)
                g.FillRectangle(BLACK, xPos, yPos, tempWidth, _height);
            else
                g.FillRectangle(WHITE, xPos, yPos, tempWidth, _height);

            xPos += tempWidth;
        }
    }


}


public class C2of5i : BarCodeBase
{
    #region variables
    private string[] cPattern = new string[100];
    private const string START = "0000";
    private const string STOP = "1000";
    private Bitmap bitmap;
    private Graphics g;
    #endregion

    #region Constructor
    public C2of5i()
    {
    }
    /// <summary>
    /// Code 2 of 5 intrelaced Constructor
    /// </summary>
    /// <param name="Code">The string that contents the numeric code</param>
    /// <param name="BarWidth">The Width of each bar</param>
    /// <param name="Height">The Height of each bar</param>
    public C2of5i(string Code, int BarWidth, int Height)
    {
        this.Code = Code;
        this.Height = Height;
        this.Width = BarWidth;
    }
    /// <summary>
    /// Code 2 of 5 intrelaced Constructor
    /// </summary>
    /// <param name="Code">The string that contents the numeric code</param>
    /// <param name="BarWidth">The Width of each bar</param>
    /// <param name="Height">The Height of each bar</param>
    /// <param name="Digits">Number of digits of code</param>
    public C2of5i(string Code, int BarWidth, int Height, int Digits)
    {
        this.Code = Code;
        this.Height = Height;
        this.Width = BarWidth;
        this.Digits = Digits;
    }
    #endregion

    private void FillPatern()
    {
        int f;
        string strTemp;

        if (cPattern[0] == null)
        {
            cPattern[0] = "00110";
            cPattern[1] = "10001";
            cPattern[2] = "01001";
            cPattern[3] = "11000";
            cPattern[4] = "00101";
            cPattern[5] = "10100";
            cPattern[6] = "01100";
            cPattern[7] = "00011";
            cPattern[8] = "10010";
            cPattern[9] = "01010";
            //Create a draw pattern for each char from 0 to 99
            for (int f1 = 9; f1 >= 0; f1--)
            {
                for (int f2 = 9; f2 >= 0; f2--)
                {
                    f = f1 * 10 + f2;
                    strTemp = "";
                    for (int i = 0; i < 5; i++)
                    {
                        strTemp += cPattern[f1][i].ToString() + cPattern[f2][i].ToString();
                    }
                    cPattern[f] = strTemp;
                }
            }
        }
    }
    /// <summary>
    /// Generate the Bitmap of Barcode.
    /// </summary>
    /// <returns>Return System.Drawing.Bitmap</returns>
    public Bitmap ToBitmap()
    {
        int i;
        string ftemp;

        xPos = 0;
        yPos = 0;

        if (this.Digits == 0)
        {
            this.Digits = this.Code.Length;
        }

        if (this.Digits % 2 > 0) this.Digits++;

        while (this.Code.Length < this.Digits || this.Code.Length % 2 > 0)
        {
            this.Code = "0" + this.Code;
        }

        int _width = (2 * Full + 3 * Thin) * (Digits) + 7 * Thin + Full;

        bitmap = new Bitmap(_width, Height);
        g = Graphics.FromImage(bitmap);

        //Start Pattern
        DrawPattern(ref g, START);

        //Draw code
        this.FillPatern();
        while (this.Code.Length > 0)
        {
            i = Convert.ToInt32(this.Code.Substring(0, 2));
            if (this.Code.Length > 2)
                this.Code = this.Code.Substring(2, this.Code.Length - 2);
            else
                this.Code = "";
            ftemp = cPattern[i];
            DrawPattern(ref g, ftemp);
        }

        //Stop Patern
        DrawPattern(ref g, STOP);

        return bitmap;
    }
    /// <summary>
    /// Returns the byte array of Barcode
    /// </summary>
    /// <returns>byte[]</returns>
    public byte[] ToByte()
    {
        return base.toByte(ToBitmap());
    }
}