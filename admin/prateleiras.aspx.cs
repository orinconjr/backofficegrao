﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class prateleiras : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        var data = new dbCommerceDataContext();
        var pedidosGeral = (from c in data.tbItensPedidos where c.produtoId == 26818 && (c.tbPedido.statusDoPedido == 3 | c.tbPedido.statusDoPedido == 11) select c.pedidoId).ToList();
        var pedidosGeralCombo = (from c in data.tbItensPedidoCombos where c.produtoId == 26818 && (c.tbItensPedido.tbPedido.statusDoPedido == 3 | c.tbItensPedido.tbPedido.statusDoPedido == 11) select c.tbItensPedido.pedidoId).ToList();


        var listaPrateleira = new List<int>();
        var listaOutros = new List<int>();
        var listasGeral = pedidosGeral.Union(pedidosGeralCombo).ToList().Distinct();
        foreach (var lista in listasGeral)
        {
            int pedidoId = lista;
            var segundoEnvio =
                                (from c in data.tbItensPedidos
                                 where c.pedidoId == pedidoId && c.enviado == true
                                 select c).Any() | (from c in data.tbItensPedidoCombos
                                                    where c.tbItensPedido.pedidoId == pedidoId && c.enviado == true
                                                    select c).Any();
            var itensPedido = (from c in data.tbItensPedidos
                               join d in data.tbItensPedidoCombos on c.itemPedidoId equals d.idItemPedido into combo
                               where !combo.Any() && c.pedidoId == pedidoId && c.enviado == false
                               select new
                               {
                                   c.itemPedidoId,
                                   c.enviado,
                                   c.produtoId,
                                   c.itemQuantidade
                               }).ToList();
            var itensPedidoCombo = (from c in data.tbItensPedidoCombos
                                    where c.tbItensPedido.pedidoId == pedidoId && c.enviado == false
                                    select new
                                    {
                                        itemPedidoId = c.idItemPedido,
                                        c.enviado,
                                        c.produtoId,
                                        c.tbItensPedido.itemQuantidade
                                    }).ToList();
            var parciais = (from c in data.tbItensPedidoEnvioParcials where c.enviado == false && c.idPedido == pedidoId select c).ToList();
            var reservas = (from c in data.tbProdutoReservaEstoques where c.idPedido == pedidoId select c).ToList();
            var faltando = (from c in data.tbPedidoProdutoFaltandos where c.pedidoId == pedidoId && c.entregue == false select c).ToList();
            var pedidosFornecedor = (from c in data.tbPedidoFornecedorItems where c.idPedido == pedidoId select c).ToList();

            int totalProdutosParcial = 0;
            int totalProdutosParcialEntregue = 0;
            int totalProdutosNaoParcial = 0;
            int totalProdutosNaoParcialEntregues = 0;
            bool totalCompleto = true;
            bool prateleira = true;

            foreach (var itemPedido in itensPedido)
            {
                if (totalCompleto)
                {
                    bool enviado = (itemPedido.enviado ?? false);
                    bool parcial = parciais.Any(x => x.idItemPedido == itemPedido.itemPedidoId && x.idProduto == itemPedido.produtoId);

                    if (parcial | enviado == false)
                    {
                        if (parcial)
                        {
                            totalProdutosParcial++;
                        }
                        else
                        {
                            totalProdutosNaoParcial++;
                        }


                        int totalReservado = reservas.Count(x => x.idItemPedido == itemPedido.itemPedidoId && x.idProduto == itemPedido.produtoId);
                        int totalPedidosFornecedor = pedidosFornecedor.Count(x => x.entregue && x.idItemPedido == itemPedido.itemPedidoId && x.idProduto == itemPedido.produtoId);
                        int totalFaltando = faltando.Count(x => x.itemPedidoId == itemPedido.itemPedidoId && x.produtoId == itemPedido.produtoId);

                        if ((totalReservado + totalPedidosFornecedor - totalFaltando) < itemPedido.itemQuantidade)
                        {

                            if (!parcial)
                            {
                                if (itemPedido.produtoId != 26818) totalCompleto = false;
                                else prateleira = false;
                            }
                            else
                            {
                                if (segundoEnvio)
                                {
                                    if (itemPedido.produtoId != 26818) totalCompleto = false;
                                    else prateleira = false;
                                }
                            }
                        }
                        else
                        {
                            if (parcial)
                            {
                                totalProdutosParcialEntregue++;
                            }
                            else
                            {
                                totalProdutosNaoParcialEntregues++;
                            }
                        }
                    }
                }
            }


            foreach (var itemPedido in itensPedidoCombo)
            {
                if (totalCompleto)
                {
                    bool enviado = (itemPedido.enviado);
                    bool parcial = parciais.Any(x => x.idItemPedido == itemPedido.itemPedidoId && x.idProduto == itemPedido.produtoId);

                    if (parcial | enviado == false)
                    {
                        if (parcial)
                        {
                            totalProdutosParcial++;
                        }
                        else
                        {
                            totalProdutosNaoParcial++;
                        }


                        int totalReservado = reservas.Count(x => x.idItemPedido == itemPedido.itemPedidoId && x.idProduto == itemPedido.produtoId);
                        int totalPedidosFornecedor = pedidosFornecedor.Count(x => x.entregue && x.idItemPedido == itemPedido.itemPedidoId && x.idProduto == itemPedido.produtoId);
                        int totalFaltando = faltando.Count(x => x.itemPedidoId == itemPedido.itemPedidoId && x.produtoId == itemPedido.produtoId);

                        if ((totalReservado + totalPedidosFornecedor - totalFaltando) < itemPedido.itemQuantidade)
                        {

                            if (!parcial)
                            {
                                if (itemPedido.produtoId != 26818) totalCompleto = false;
                                else prateleira = false;
                            }
                            else
                            {
                                if (segundoEnvio)
                                {
                                    if (itemPedido.produtoId != 26818) totalCompleto = false;
                                    else prateleira = false;
                                }
                            }
                        }
                        else
                        {
                            if (parcial)
                            {
                                totalProdutosParcialEntregue++;
                            }
                            else
                            {
                                totalProdutosNaoParcialEntregues++;
                            }
                        }
                    }
                }
            }


            if (totalCompleto && prateleira == false)
            {
                listaPrateleira.Add(pedidoId);
            }

            if (totalCompleto == false && prateleira == true)
            {
                listaOutros.Add(pedidoId);
            }
        }

        litFaltaPrateleira.Text = string.Join(",", listaPrateleira);
        litFaltaOutros.Text = string.Join(",", listaOutros);
    }
}