﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using DevExpress.Web.ASPxGridView;

public partial class jadlog_pedidos : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            //checaPendentesJadlog();
            rnPedidos.checaPedidosPendentesJadlog();
        }
        catch (Exception)
        {

        }
        fillGrid();
    }

    private void atualizaPedidosJad()
    {
        
    }

    private void checaPendentesJadlog()
    {
        var pedidosDc = new dbCommerceDataContext();
        var pedidos = (from c in pedidosDc.tbPedidos where (c.pendenteEnvioJadlog ?? false) == true select c);
        foreach (var pedido in pedidos)
        {
            try
            {
                gravaPedidoServiceJadlog(pedido.pedidoId);
                pedido.pendenteEnvioJadlog = false;
                pedidosDc.SubmitChanges();
            }
            catch (Exception ex)
            {

            }
        }
    }


    private void gravaPedidoServiceJadlog(int pedidoId)
    {
        var pedidoDc = new dbCommerceDataContext();
        var pedido = (from c in pedidoDc.tbPedidos where c.pedidoId == pedidoId select c).First();
        var clientesDc = new dbCommerceDataContext();
        var cliente = (from c in clientesDc.tbClientes where c.clienteId == pedido.clienteId select c).First();
        var volumes = (from c in pedidoDc.tbPedidoPacotes where c.idPedido == pedidoId && (c.concluido ?? true) == false select c);

        int idPedidoCliente = rnFuncoes.retornaIdCliente(pedidoId);

        string CodCliente = "40745";
        string Password = "L2F0M0E1";
        string Remetente = "LINDSAY FERRANDO ME";
        string RemetenteCNPJ = "10924051000163";
        string RemetenteIE = "633459940118";
        string RemetenteEndereco = "AV ANA COSTA 416";
        string RemetenteBairro = "GONZAGA";
        string RemetenteCEP = "11060002";
        string RemetenteTelefone = "01135228379";
        string Destino = "";
        string Destinatario = "";
        string DestinatarioCNPJ = "";
        string DestinatarioIE = "";
        string DestinatarioEndereco = "";
        string DestinatarioBairro = "";
        string DestinatarioCEP = "";
        string DestinatarioTelefone = "";


        if (!string.IsNullOrEmpty(pedido.endNomeDoDestinatario))
        {
            string enderecoCompleto = pedido.endRua + " - " + pedido.endNumero + " - " + pedido.endComplemento;
            Destino = rnFuncoes.removeCaracteresEspeciaisDeixaEspaco(rnFuncoes.limpaStringDeixaEspaco(pedido.endCidade)).ToUpper();
            Destinatario = idPedidoCliente + " " + rnFuncoes.removeCaracteresEspeciaisDeixaEspaco(rnFuncoes.limpaStringDeixaEspaco(pedido.endNomeDoDestinatario)).ToUpper();
            DestinatarioCNPJ = cliente.clienteCPFCNPJ.Replace(".", "").Replace("-", "").Replace("/", "");
            DestinatarioEndereco = rnFuncoes.removeCaracteresEspeciaisDeixaEspaco(rnFuncoes.limpaStringDeixaEspaco(enderecoCompleto)).ToUpper();
            DestinatarioBairro = rnFuncoes.removeCaracteresEspeciaisDeixaEspaco(rnFuncoes.limpaStringDeixaEspaco(pedido.endBairro)).ToUpper();
            DestinatarioCEP = pedido.endCep.Replace("-", "").Replace(" ", "");
            DestinatarioTelefone = cliente.clienteFoneResidencial.Replace(" ", "").Replace("-", "");
        }
        else
        {
            string enderecoCompleto = cliente.clienteRua + " - " + cliente.clienteNumero + " - " + cliente.clienteComplemento;
            Destino = rnFuncoes.removeCaracteresEspeciaisDeixaEspaco(rnFuncoes.limpaStringDeixaEspaco(cliente.clienteCidade)).ToUpper();
            Destinatario = idPedidoCliente + " " + rnFuncoes.removeCaracteresEspeciaisDeixaEspaco(rnFuncoes.limpaStringDeixaEspaco(cliente.clienteNome)).ToUpper();
            DestinatarioCNPJ = cliente.clienteCPFCNPJ.Replace(".", "").Replace("-", "").Replace("/", "");
            DestinatarioEndereco = rnFuncoes.removeCaracteresEspeciaisDeixaEspaco(rnFuncoes.limpaStringDeixaEspaco(enderecoCompleto)).ToUpper();
            DestinatarioBairro = rnFuncoes.removeCaracteresEspeciaisDeixaEspaco(rnFuncoes.limpaStringDeixaEspaco(cliente.clienteBairro));
            DestinatarioCEP = cliente.clienteCep.Replace("-", "").Replace(" ", "");
            DestinatarioTelefone = cliente.clienteFoneResidencial.Replace(" ", "").Replace("-", "");
        }

        //Destinatario = "TESTE DE PEDIDO - NÃO EMITIR";

        string ColetaResponsavel = "";
        string Volumes = pedido.volumesEmbalados.ToString();
        var pesoTotal = (Convert.ToDecimal(volumes.Sum(x => x.peso)) / 1000);
        string PesoReal = pesoTotal.ToString("0.00");
        string Especie = "CONFECCOES";
        string Conteudo = "CONFECCOES";
        string Nr_Pedido = rnFuncoes.retornaIdCliente(pedido.pedidoId).ToString();
        string Nr_NF = "";
        string Danfe = "";
        string Serie_Nf = "";
        string ValorDeclarado = "";
        if (pedido.nfeNumero > 0)
        {
            Nr_NF = pedido.nfeNumero.ToString();
            Danfe = pedido.nfeAccessKey;
            Serie_Nf = "01";
            ValorDeclarado = Convert.ToDecimal(pedido.valorCobrado).ToString("0.00");
        }
        else
        {
            Nr_NF = idPedidoCliente.ToString();
            ValorDeclarado = "100,00";
        }
        string Observacoes = "";
        string Modalidade = "3";
        string wCentroCusto = "";
        string wContaCorrente = "0034802";
        string wTipo = "K";
        string CodUnidade = "795";

        bool erroService = false;

        if (pedido.formaDeEnvio.ToLower() == "jadlogexpressa")
        {
            Modalidade = "0";
        }

        try
        {
            var serviceJad = new serviceJadEnvio.NotfisBeanService();
            var retornoServiceJad = serviceJad.inserir(CodCliente, Password, Remetente, RemetenteCNPJ, RemetenteIE, RemetenteEndereco, RemetenteBairro, RemetenteCEP,
                RemetenteTelefone, Destino, Destinatario, DestinatarioCNPJ, DestinatarioIE, DestinatarioEndereco, DestinatarioBairro, DestinatarioCEP, DestinatarioTelefone,
                ColetaResponsavel, Volumes, PesoReal, Especie, Conteudo, Nr_Pedido, Nr_NF, Danfe, Serie_Nf, ValorDeclarado, Observacoes, Modalidade, wCentroCusto, wContaCorrente,
                wTipo, CodUnidade);

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(retornoServiceJad);
            XmlNodeList parentNode = xmlDoc.GetElementsByTagName("Jadlog_Pedido_eletronico_Inserir");
            foreach (XmlNode childrenNode in parentNode)
            {
                try
                {
                    string retorno = childrenNode.ChildNodes[1].InnerText;
                    if (!String.IsNullOrEmpty(retorno))
                    {
                        if (retorno == "-1" | retorno == "-2" | retorno == "-99")
                        {
                            return;
                        }
                        foreach (var pacote in volumes)
                        {
                            var pacoteAlterarDc = new dbCommerceDataContext();
                            var detalhePacote = (from c in pacoteAlterarDc.tbPedidoPacotes where c.idPedidoPacote == pacote.idPedidoPacote select c).First();
                            detalhePacote.codJadlog = retorno;
                            pacoteAlterarDc.SubmitChanges();
                        }
                        //pedido.codPedidoJadlog = retorno;
                    }
                }
                catch (Exception)
                {

                }
            }
        }
        catch (Exception)
        {
            erroService = true;
        }

        if (erroService)
        {
            pedido.pendenteEnvioJadlog = true;
            pedidoDc.SubmitChanges();
        }

    }
    private void fillGrid()
    {
        var pedidosDc = new dbCommerceDataContext();
        var pedidosJad = (from c in pedidosDc.tbPedidos 
                          join d in pedidosDc.tbPedidoPacotes on c.pedidoId equals d.idPedido into pacotes
            where (c.codPedidoJadlog != null && c.codPedidoJadlog != "" && (c.numeroDoTracking4 == "" | c.numeroDoTracking4 == null)) | (pacotes.Where(x => x.codJadlog != "" && x.codJadlog != null && (x.rastreio == "" | x.rastreio == null)).Count() > 0) orderby c.dataEmbaladoFim
            select c);
        grd.DataSource = pedidosJad;
        if(!Page.IsPostBack && !Page.IsCallback) grd.DataBind();

    }
    protected void grd_OnCustomUnboundColumnData(object sender, ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "pedidoIdCliente")
        {
            int pedidoId = Convert.ToInt32(e.GetListSourceFieldValue("pedidoId"));
            e.Value = rnFuncoes.retornaIdCliente(pedidoId);
        }
        if (e.Column.FieldName == "pesoTotal")
        {
            var pedidosDc = new dbCommerceDataContext();
            int pedidoId = Convert.ToInt32(e.GetListSourceFieldValue("pedidoId"));
            int pesoTotal = 0;
            try
            {
                pesoTotal = (from c in pedidosDc.tbPedidoPacotes where c.idPedido == pedidoId && (c.rastreio == "" | c.rastreio == null) select c).Sum(x => x.peso);
            }
            catch (Exception)
            {
                
            }
            e.Value = pesoTotal;
        }
        if (e.Column.FieldName == "pesoVolumes")
        {
            var pedidosDc = new dbCommerceDataContext();
            int pedidoId = Convert.ToInt32(e.GetListSourceFieldValue("pedidoId"));
            string pesoVolumes = "";
            var pesos = (from c in pedidosDc.tbPedidoPacotes where c.idPedido == pedidoId && (c.rastreio == "" | c.rastreio == null) select c);
            foreach (var peso in pesos)
            {
                if (!string.IsNullOrEmpty(pesoVolumes)) pesoVolumes += ", ";
                pesoVolumes += peso.peso;
            }
            e.Value = pesoVolumes;
        }
    }

    protected void btnExportar_OnCommand(object sender, CommandEventArgs e)
    {
        var pedidosDc = new dbCommerceDataContext();
        int pedidoId = Convert.ToInt32(e.CommandArgument);
        var pedido = (from c in pedidosDc.tbPedidos where c.pedidoId == pedidoId select c).FirstOrDefault();

        if (!string.IsNullOrEmpty(pedido.danfe))
        {

            //Create a stream for the file
            Stream stream = null;
            string url = "http://www.graodegente.com.br/admin/notas/danfes/" + pedido.danfe;
            //This controls how many bytes to read at a time and send to the client
            int bytesToRead = 10000;

            // Buffer to read bytes in chunk size specified above
            byte[] buffer = new Byte[bytesToRead];

            // The number of bytes read
            try
            {
                //Create a WebRequest to get the file
                HttpWebRequest fileReq = (HttpWebRequest)HttpWebRequest.Create(url);

                //Create a response for this request
                HttpWebResponse fileResp = (HttpWebResponse)fileReq.GetResponse();

                if (fileReq.ContentLength > 0)
                    fileResp.ContentLength = fileReq.ContentLength;

                //Get the Stream returned from the response
                stream = fileResp.GetResponseStream();

                // prepare the response to the client. resp is the client Response
                var resp = HttpContext.Current.Response;

                //Indicate the type of data being sent
                resp.ContentType = "application/octet-stream";

                //Name the file 
                resp.AddHeader("Content-Disposition", "attachment; filename=\"" + pedido.danfe + "\"");
                resp.AddHeader("Content-Length", fileResp.ContentLength.ToString());

                int length;
                do
                {
                    // Verify that the client is connected.
                    if (resp.IsClientConnected)
                    {
                        // Read data into the buffer.
                        length = stream.Read(buffer, 0, bytesToRead);

                        // and write it out to the response's output stream
                        resp.OutputStream.Write(buffer, 0, length);

                        // Flush the data
                        resp.Flush();

                        //Clear the buffer
                        buffer = new Byte[bytesToRead];
                    }
                    else
                    {
                        // cancel the download if client has disconnected
                        length = -1;
                    }
                } while (length > 0); //Repeat until no data is read
            }
            finally
            {
                if (stream != null)
                {
                    //Close the input stream
                    stream.Close();
                }
            }
        }
        else
        {
            Response.Write("<script>alert('Nota fiscal não emitida. Favor entrar em contato com a Grão de Gente');</script>");
        }
    }


    protected void btPdf_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WritePdfToResponse();
    }
    protected void btXsl_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteXlsToResponse();
    }
    protected void btRtf_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteRtfToResponse();
    }
    protected void btCsv_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteCsvToResponse();
    }

    protected void gridClientes_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType != GridViewRowType.Data)
        {
            return;
        }

        int pedidoId = 0;
        int.TryParse(grd.GetRowValues(e.VisibleIndex, new string[] { "pedidoId" }).ToString(), out pedidoId);
        LinkButton btnExportar = (LinkButton)grd.FindRowCellTemplateControl(e.VisibleIndex, grd.Columns["exportar"] as GridViewDataColumn, "btnExportar");

        var pedidosDc = new dbCommerceDataContext();
        var pedido = (from c in pedidosDc.tbPedidos where c.pedidoId == pedidoId select c).FirstOrDefault();
        if (!string.IsNullOrEmpty(pedido.danfe))
        {
            btnExportar.Visible = true;
        }
        else
        {
            btnExportar.Visible = false;
        }
    }
}