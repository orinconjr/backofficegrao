﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class jadlog_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void imbEntrar_Click(object sender, ImageClickEventArgs e)
    {
        var usuarioDc = new dbCommerceDataContext();
        if (txtSenha.Text.ToLower() == "jadlog")
        {
            HttpCookie usuarioTransportadoraLogado = new HttpCookie("usuarioTransportadoraLogado");
            usuarioTransportadoraLogado.Value = txtSenha.Text.ToLower();
            usuarioTransportadoraLogado.Expires = DateTime.Now.AddMinutes(30);
            Response.Cookies.Add(usuarioTransportadoraLogado);
            Response.Redirect("pedidos.aspx");
        }
        else
        {
            Response.Write("<script>alert('Dados inválidos, Tente novamente.');</script>");
        }
    }
}