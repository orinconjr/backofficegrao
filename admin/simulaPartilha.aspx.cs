﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class simulaPartilha : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        var partilha = rnNotaFiscal.CalculaDiferencialAliquota("ce", Convert.ToDecimal("152,56"), DateTime.Now);
        Response.Write(partilha.totalDiferencial.ToString("C") + "<br>");
        Response.Write(partilha.diferencialDestino.ToString("C") + "<br>");
        Response.Write(partilha.diferencialOrigem.ToString("C") + "<br>");
    }
}