﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="consultarNfGrao2.aspx.cs" Inherits="consultarNfGrao2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
        <style>
        .fieldsetatualizarprecos {
            width: 760px;
            float: left;
            clear: left;
            border-radius: 5px;
            margin-left: 25px;
        }

            .fieldsetatualizarprecos input[type=checkbox] {
                margin-top: 15px;
            }

            .fieldsetatualizarprecos label {
                margin-left: 5px;
                margin-right: 20px;
            }

        .meugrid {
            background-color: #F4FAFB;
            border: 0px none;
            /* border-collapse: separate !important; */
            overflow: hidden;
            font: 9pt Tahoma;
            color: #000;
            cursor: pointer;
            /* white-space: nowrap;*/
            font-weight: normal;
            text-align: left;
            width: 834px;
            border-collapse: collapse;
            float: left;
            clear: left;
            margin-top: 10px;
            margin-bottom: 10px;
            min-height: 500px;
            margin-left: 25px;
        }

            .meugrid th {
                background-image: url(../../App_Themes/Glass/GridView/gvHeaderBackground.gif);
                background-position: center top;
                background-repeat: repeat-x;
                background-color: #C0DDE0;
                overflow: hidden;
                height: 23px;
                line-height: 23px;
                padding: 4px 6px 5px;
                font-weight: normal;
                border: 1px solid #7EACB1;
                /* border-collapse: separate;*/
            }

            .meugrid td {
                overflow: hidden;
                height: 23px;
                line-height: 23px;
                padding: 4px 6px 5px;
                border: 1px solid #7EACB1;
            }

            .meugrid tr td:nth-child(4) {
                text-align: center;
            }

            .meugrid tr td:last-of-type {
                text-align: center;
            }

        .tabrotulos {
            width: 848px;
            margin: auto;
        }

        .materiaprima {
            width: 250px;
        }

            .materiaprima ul {
                margin: 0px;
                padding-left: 10px;
            }

                .materiaprima ul li {
                    max-width: 230px;
                    white-space: pre-wrap;
                }

        .meugrid tr td:last-of-type {
            text-align: center;
        }

        .nome {
            width: 237px;
        }

            .nome div {
                max-width: 230px;
                white-space: pre-wrap;
            }

        .rotulos td {
            font: bold 12px tahoma;
            padding-top: 20px;
            color: rgb(65, 105, 126);
            padding-left: 5px;
            padding-bottom: 5px;
        }

        /*****************************/
        .width150 {
        }

        .height150 {
        }

        .height80 {
        }

        .btn {
            -webkit-border-radius: 7px;
            -moz-border-radius: 7px;
            border-radius: 2px;
            font-family: Arial;
            color: #ffffff;
            font-size: 14px;
            background: #63c269;
            padding: 5px 10px 5px 10px;
            text-decoration: none;
            cursor: pointer;
            border: 0px;
        }

            .btn:hover {
                background: #a4dbab;
                text-decoration: none;
                color: black;
                font-weight: bold;
            }


        .marginTop5porcento {
            margin: 6%;
            font-size: 15px;
        }

        .quadroPrincipal {
            float: left;
            width: 150px;
            height: 155px;
            text-align: center;
            position: relative;
            font-family: monospace;
            border: 1px solid #B1B1B1;
            margin: 0 14px 5px 14px;
            border-radius: 15px;
        }

        .quadroInteriorQtds {
            margin-top: 5%;
            text-align: center;
            font-size: 30px;
        }

        .AlinharAoRodapeCentralizar {
            position: absolute;
            bottom: 5px;
            text-align: center;
            width: 150px;
        }
        .meubotao {
            cursor: pointer;
            font: bold 14px tahoma;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
  <div class="tituloPaginas" valign="top">
        Consultar NFes
    </div>
    <div>
        <fieldset class="fieldsetatualizarprecos">
            <legend style="font-weight: bold;">Consultar</legend>

            <table class="tabrotulos">
                  <tr class="rotulos">

                    <td colspan="2">Tipo de Documento:<br />
                        <asp:RadioButtonList ID="rblDanfeXml" runat="server" RepeatDirection="Horizontal">
                            <asp:ListItem Text="DANFE" Value="danfe" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="XML" Value="xml"></asp:ListItem>
                            <asp:ListItem Text="DOWNLOAD DANFE" Value="downloaddanfe"></asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                    
                    <td>Empresa:<br />
                        <asp:DropDownList runat="server" ID="ddlEmpresas" DataTextField="empresa" DataValueField="idEmpresa" />
                    </td>
                </tr>
                <tr class="rotulos">

                    <td>Data Inicial:<br />
                        <asp:TextBox runat="server" ID="txtDataInicial"></asp:TextBox>
                    </td>
                    <td>Data Final:<br />
                        <asp:TextBox runat="server" ID="txtDataFinal"></asp:TextBox>
                    </td>
                    <td>Numero(s) da(s) nota(s) (Se for mais de uma , separar por vírgula):<br />
                       <asp:TextBox runat="server" TextMode="MultiLine" Height="75" Width="100%" ID="txtNotas"></asp:TextBox>&nbsp;&nbsp;
                    </td>
                </tr>

                <tr class="rotulos">
                    <td>&nbsp;
                    </td>

                    <td>&nbsp;
                    </td>

                    <td>
                        <div style="width: 370px; text-align: right">
                            
                            <asp:Button runat="server" Text="Pesquisar" CssClass="meubotao"  OnClick="OnClick" ValidationGroup="pesquisa" />
                        </div>
                    </td>

                </tr>
            </table>
        </fieldset>


    </div>

    <div>
        <fieldset class="fieldsetatualizarprecos">
            <legend style="font-weight: bold;">Atualizar Notas com Invoicy</legend>
            <table class="tabrotulos">
                <tr class="rotulos">
                   <td colspan="2">Numero(s) da(s) nota(s) (Se for mais de uma , separar por vírgula):<br />
                       <asp:TextBox runat="server" TextMode="MultiLine" Height="75" Width="100%" ID="txtNotasAtualizarInvoicy"></asp:TextBox>&nbsp;&nbsp;
                    </td>
                    <td>
                        <asp:Button runat="server" id="btnAtualizarNotasInvoicy" Text="Atualizar" OnClick="btnAtualizarNotasInvoicy_Click" />
                    </td>
                </tr>
            </table>
        </fieldset>
    </div>

    <div>
        <fieldset class="fieldsetatualizarprecos">
            <legend style="font-weight: bold;">Consultar UF do cliente</legend>
            <table class="tabrotulos">
                <tr class="rotulos">
                   <td >Nfe<br />
                       <asp:TextBox ID="txtNfe" runat="server"></asp:TextBox>
                    </td>
                    <td>
                        <asp:Button ID="btConsultarUF" runat="server" Text="Consultar UF" OnClick="btConsultarUF_Click" />
                    </td>
                    <td> <asp:Label ID="lblUF" runat="server" Text=""></asp:Label></td>
                </tr>
            </table>
        </fieldset>
    </div>
    <div style="float:left;clear:left;width:100%">
        <asp:Label ID="infoNotaXml" runat="server" Text="Label"></asp:Label>
    </div>
   <div style="float:left;clear:left;width:100%">
        <asp:Label ID="infoNotaXml2" runat="server" Text="Label"></asp:Label>
    </div>
    <div align="center" style="min-height: 500px; float: left; clear: left; width:100%;">
        <asp:GridView ID="GridView1" runat="server"  CssClass="meugrid" DataKeyNames="ultimoStatus" AutoGenerateColumns="False" OnRowDataBound="GridView1_RowDataBound">
            <Columns>
                <asp:BoundField DataField="numeronota" HeaderText="Nota" />
                <asp:BoundField DataField="idPedido" HeaderText="Pedido" />
                <asp:BoundField DataField="dataReceitaFederal" HeaderText="Data" />
                <asp:TemplateField HeaderText="Link Danfe">
                    <ItemTemplate>
                        <asp:HyperLink ID="hplLinkDanfe" runat="server" Visible='<%# Eval("linkdanfe") != null%>' NavigateUrl='<%#Eval("linkdanfe") %>'>DANFE</asp:HyperLink>
                        <%# Eval("linkdanfe") == null ? "" : "" %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Status no Invoicy">
                    <ItemTemplate>
                        <asp:Label ID="lblStatusInvoicy" runat="server" Text=""></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
     
    </div>
    <div style="float: left; clear: left; width: 868px; margin-left: 25px; margin-top: 15px;">
        <strong>Quantidade de itens encontrados nesta busca:
        <span style="text-decoration: underline">
            <asp:Label ID="lblitensencontrados" runat="server" Text=""></asp:Label></span>
           
        </strong>
    </div>
    </form>
</body>
</html>
