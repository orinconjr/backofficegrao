﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Practices.EnterpriseLibrary.Data;

public partial class limpaProfileOtimizaIndice : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        int tentativas = 0;
        int tentativasMaximas = 3;
        int tempo = 0;
        bool sucessoLimpa = false;
        while (tentativas <= tentativasMaximas && sucessoLimpa == false)
        {
            try
            {
                sucessoLimpa = limpaProfile();
            }
            catch (Exception ex)
            {
                tentativas++;
                Thread.Sleep(new TimeSpan(0, 0, tempo));
                tempo++;
            }
        }

        tentativas = 0;
        bool sucessoOtimiza = false;
        while (tentativas <= tentativasMaximas && sucessoOtimiza == false)
        {
            try
            {
                sucessoOtimiza = otimizaIndices();
            }
            catch (Exception ex)
            {
                tentativas++;
                Thread.Sleep(new TimeSpan(0, 0, tempo));
                tempo++;
            }
        }

        Response.Write("ok");
    }

    private bool limpaProfile()
    {
        bool sucesso = true;
        for (int i = 1; i <= 20; i++)
        {
            Database db = DatabaseFactory.CreateDatabase();
            DbCommand dbCommand = db.GetStoredProcCommand("limpaProfile");
            dbCommand.CommandTimeout = 100000;
            db.AddInParameter(dbCommand, "dataMenorQue", DbType.Date, DateTime.Now.AddDays(-7));
            using (DbConnection connection = db.CreateConnection())
            {
                connection.Open();
                DbTransaction transaction = connection.BeginTransaction();

                try
                {
                    db.ExecuteNonQuery(dbCommand, transaction);
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    sucesso = false;
                }
                connection.Close();
            }
        }

        return sucesso;
    }
    private bool otimizaIndices()
    {
        bool sucesso = true;
        Database db = DatabaseFactory.CreateDatabase();
        DbCommand dbCommand = db.GetStoredProcCommand("otimizaIndices");
        dbCommand.CommandTimeout = 100000;
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
            DbTransaction transaction = connection.BeginTransaction();

            try
            {
                db.ExecuteNonQuery(dbCommand, transaction);
                transaction.Commit();
            }
            catch (Exception ex)
            {
                sucesso = false;
            }
            connection.Close();
        }
        return sucesso;
    }
}