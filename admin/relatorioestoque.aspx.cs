﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class relatorioestoque : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        var data = new dbCommerceDataContext();
        var produtos = (from c in data.tbPedidoFornecedorItems where c.idPedidoFornecedor >= 62148 && c.idPedidoFornecedor <= 62168 select new { c.idProduto, c.tbProduto.produtoNome }).Distinct().ToList();
        int TotalProdutos = 0;
        foreach (var produto in produtos)
        {
            try
            {
                var aguardando = (from c in data.tbItemPedidoEstoques
                                  where c.produtoId == produto.idProduto && c.reservado == false && c.cancelado == false && c.dataLimite != null
                                  orderby c.dataLimite
                                  select new
                                  {
                                      c.idItemPedidoEstoque,
                                      c.tbItensPedido.pedidoId,
                                      dataLimiteReserva = c.dataLimite
                                  }).ToList();
                var pedidos = (from c in data.tbPedidoFornecedorItems
                               join f in data.tbProdutoEstoques on c.idPedidoFornecedorItem equals f.idPedidoFornecedorItem into produtoEstoque
                               where (c.entregue == false | (c.entregue == true && (c.liberado ?? false) == false) | produtoEstoque.Any(x => (x.liberado ?? false) == false)) && c.idProduto == produto.idProduto && (c.motivo ?? "") == ""
                               orderby c.tbPedidoFornecedor.dataLimite, c.tbPedidoFornecedor.idPedidoFornecedor
                               select new
                               {
                                   c.idPedidoFornecedor,
                                   c.idPedidoFornecedorItem,
                                   dataLimiteFornecedor = c.tbPedidoFornecedor.dataLimite
                               }).ToList();
                var listaFinal = (from c in aguardando.Select((item, index) => new { item, index })
                                  join d in pedidos.Select((item, index) => new { item, index }) on c.index equals d.index into
                                      pedidosFornecedor
                                  from f in pedidosFornecedor.DefaultIfEmpty()
                                  select new
                                  {
                                      c.item.idItemPedidoEstoque,
                                      c.item.pedidoId,
                                      c.item.dataLimiteReserva,
                                      idPedidoFornecedor = e != null ? f.item.idPedidoFornecedor : 0,
                                      dataLimiteFornecedor = e != null ? (DateTime?)f.item.dataLimiteFornecedor : null,
                                      idPedidoFornecedorItem = e != null ? (int?)f.item.idPedidoFornecedorItem : null
                                  }
                    ).ToList().Where(x => x.idPedidoFornecedor >= 62148 && x.idPedidoFornecedor <= 62168).Count();
                TotalProdutos += listaFinal;
                if (listaFinal > 0) Response.Write("<b>ID: </b>" + produto.idProduto + " - <b>Produto: </b>" + produto.produtoNome + " - <b>Quantidade: </b>" + listaFinal + "<br>");
            }catch(Exception ex) { }
        }

        Response.Write("<b>Total de Produtos: </b>" + TotalProdutos + "<br>");


    }
}