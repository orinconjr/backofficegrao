﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class atualizamargem : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        var data = new dbCommerceDataContext();
        var produtos = (from c in data.tbProdutos where c.margemDeLucro == 0 select c).ToList();
        foreach(var produto in produtos)
        {
            var precoExibicao = produto.produtoPrecoPromocional > 0 && produto.produtoPrecoPromocional < produto.produtoPreco
                ? produto.produtoPrecoPromocional
                : produto.produtoPreco;
            var margem = produto.produtoPrecoDeCusto == 0 ? 100 : (((precoExibicao * 100) / produto.produtoPrecoDeCusto) - 100);
            produto.margemDeLucro = (decimal)margem;
            data.SubmitChanges();
        }
    }
}