﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class gerarValoresTransportadora : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        var data = new dbCommerceDataContext();
        var filtroData = Convert.ToDateTime("01/11/2017");
        var filtroDataFim = Convert.ToDateTime("01/12/2017");
        var envios = (from c in data.tbPedidoEnvios where c.dataFimEmbalagem >= filtroData && c.idCentroDeDistribuicao == 4 && c.dataFimEmbalagem < filtroDataFim select new { c.idPedidoEnvio,
            c.tbPedido.endCep,
            c.valorSelecionado,
            c.formaDeEnvio,
            c.tbPedido.pedidoId
        }).ToList();
        var enviosTransporte = (from c in data.tbPedidoEnvioValorTransportes
                                where c.tbPedidoEnvio.idCentroDeDistribuicao == 4 && c.tbPedidoEnvio.dataFimEmbalagem >= filtroData 
                                && c.tbPedidoEnvio.dataFimEmbalagem < filtroDataFim select new {
            c.idPedidoEnvio,
            c.valor,
            c.servico,
            c.prazo,
            c.tipoDeEntregaId
        }).ToList();
        foreach(var envio in envios)
        {
            long cep = Convert.ToInt64(envio.endCep.Replace("-", "").Replace(".", "").Replace(" ", "").Trim());
            var envioCorreios = (from c in enviosTransporte where c.idPedidoEnvio == envio.idPedidoEnvio && c.tipoDeEntregaId == IDDADROPDOWN select c).FirstOrDefault();
            if(envioCorreios != null)
            {
                //REALIZAR O CALCULO E INSERIR NA TABELA
                /*var valoresTotal = frete.CalculaFrete(32, pedido.endCep.Replace("-", "").Trim(), valorTotalDoPedido, pacotes);
                var valorTransporteTotal = new tbPedidoEnvioValorTransporte();
                valorTransporteTotal.idPedidoEnvio = idPedidoEnvio;
                valorTransporteTotal.prazo = valoresTotal.prazo;
                valorTransporteTotal.servico = "totalexpress";
                valorTransporteTotal.valor = Convert.ToDecimal(valoresTotal.valor.ToString("0.00"));
                valorTransporteTotal.tipoDeEntregaId = 32;
                data.tbPedidoEnvioValorTransportes.InsertOnSubmit(valorTransporteTotal);*/
            }

        }
        /*
        int totalEnvios = envios.Count();


        decimal totalJadlog = 0;
        decimal totalTnt = 0;
        decimal totalPlimor = 0;
        decimal totalPac = 0;

        int quantidadeJadlog = 0;
        int quantidadeTnt = 0;
        int quantidadePlimor = 0;
        int quantidadePac = 0;

        decimal totalAtual = 0;

        List<int> pedidos = new List<int>();
        foreach (var envio in envios)
        {
            var valores = (from c in enviosTransporte where c.idPedidoEnvio == envio.idPedidoEnvio select c).ToList();
            /*var cepLong = Convert.ToInt64(envio.endCep.Replace("-", ""));
            if (cepLong > 01000000 && cepLong < 09999999)
            {
                valores = valores.Where(x => x.servico != "tnt").ToList();
            }*/

            /*valores = valores.Where(x => x.servico != "pac3").ToList();
            valores = valores.Where(x => x.servico != "pac4").ToList();
            valores = valores.Where(x => x.servico != "pac5").ToList();
            //valores = valores.Where(x => x.servico != "pac6").ToList();

            var valorSelecionado = valores.Where(x => x.valor > 0).OrderBy(x => x.valor).FirstOrDefault();
            if (valorSelecionado != null)
            {
                if (envio.formaDeEnvio != valorSelecionado.servico)
                {
                    pedidos.Add(envio.pedidoId);
                }
                totalAtual += envio.valorSelecionado ?? valorSelecionado.valor;
                //Response.Write("Servico atual: " + (envio.valorSelecionado ?? 0).ToString("C") + " - " + envio.formaDeEnvio + "<br>");
                if (valorSelecionado.servico == "tnt")
                {
                    totalTnt += valorSelecionado.valor;
                    quantidadeTnt++;
                    //Response.Write((valorSelecionado.valor).ToString("C") + " - " + valorSelecionado.servico + "<br>");
                }
                if (valorSelecionado.servico == "jadlog" | valorSelecionado.servico == "jadlogcom")
                {
                    totalJadlog += valorSelecionado.valor;
                    quantidadeJadlog++;
                    //Response.Write((valorSelecionado.valor).ToString("C") + " - " + valorSelecionado.servico + "<br>");
                }
                if (valorSelecionado.servico == "plimor")
                {
                    totalPlimor += valorSelecionado.valor;
                    quantidadePlimor++;
                    //Response.Write((envio.valorSelecionado ?? 0).ToString("C") + " - " + envio.formaDeEnvio + "<br>");
                }
                if (valorSelecionado.servico == "pac6" | valorSelecionado.servico == "pac")
                {
                    totalPac += valorSelecionado.valor;
                    quantidadePac++;
                    //Response.Write((envio.valorSelecionado ?? 0).ToString("C") + " - " + envio.formaDeEnvio + "<br>");
                }
            }
        }
            Response.Write("<br>Total TNT: " + totalTnt.ToString("C") + " - Quantidade: " + quantidadeTnt);
            Response.Write("<br>Total Jadlog: " + totalJadlog.ToString("C") + " - Quantidade: " + quantidadeJadlog);
            Response.Write("<br>Total Plimor: " + totalPlimor.ToString("C") + " - Quantidade: " + quantidadePlimor);
        Response.Write("<br>Total Pac: " + totalPac.ToString("C") + " - Quantidade: " + quantidadePac);
        Response.Write("<br>Total novo: " + (totalTnt + totalJadlog + totalPlimor) + "<br><br>");
            */
        

    }
}