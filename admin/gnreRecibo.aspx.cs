﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class gnreRecibo : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        int idNota = 0;
        if (Request.QueryString["id"] != null)
        {
            int.TryParse(Request.QueryString["id"], out idNota);
            var data = new dbCommerceDataContext();
            var nota = (from c in data.tbNotaFiscals where c.idNotaFiscal == idNota select c);
            lstBoleto.DataSource = nota;
            lstBoleto.DataBind();
        }
    }
}