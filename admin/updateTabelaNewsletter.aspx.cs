﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Transactions;

public partial class updateTabelaNewsletter : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        var data = new dbCommerceDataContext();
        var clientes = (from c in data.tbClientes orderby c.clienteId select c).ToList();
        foreach (var cliente in clientes)
        {
            var newsletter = (from c in data.tbNewsletters where c.idNewsletter == cliente.clienteId select c).FirstOrDefault();
            using (TransactionScope trans = new TransactionScope())
            {
                using (dbCommerceDataContext context = new dbCommerceDataContext())
                {
                    if (newsletter != null)
                    {
                        if (newsletter.email != cliente.clienteEmail)
                        {
                            var novaNews = new tbNewsletter();
                            novaNews.confirmado = newsletter.confirmado;
                            novaNews.dataCadastro = newsletter.dataCadastro;
                            novaNews.email = newsletter.email;
                            novaNews.mes = newsletter.mes;
                            novaNews.nome = newsletter.nome;
                            novaNews.sexo = newsletter.sexo;
                            context.tbNewsletters.InsertOnSubmit(novaNews);

                            var cadastroNewsAnterior =
                                (from c in context.tbNewsletters
                                    where c.email == cliente.clienteEmail && c.idNewsletter != cliente.clienteId
                                    select c).FirstOrDefault();
                            if (cadastroNewsAnterior != null)
                            {
                                context.tbNewsletters.DeleteOnSubmit(cadastroNewsAnterior);
                            }

                            var newsAlterar =
                                (from c in context.tbNewsletters
                                    where c.idNewsletter == newsletter.idNewsletter
                                    select c).First();
                            newsAlterar.email = cliente.clienteEmail;
                            newsAlterar.sexo = "indefinido";
                            newsAlterar.mes = "0";
                            newsAlterar.nome = cliente.clienteNome;
                            newsAlterar.confirmado = true;
                            newsAlterar.dataCadastro = cliente.dataDaCriacao;
                            context.SubmitChanges();
                        }
                        else
                        {
                            var cadastroNewsAnterior =
                                 (from c in context.tbNewsletters
                                  where c.email == cliente.clienteEmail && c.idNewsletter != cliente.clienteId
                                  select c).FirstOrDefault();
                            if (cadastroNewsAnterior != null)
                            {
                                context.tbNewsletters.DeleteOnSubmit(cadastroNewsAnterior);
                            }

                            var newsAlterar =
                                (from c in context.tbNewsletters
                                 where c.idNewsletter == newsletter.idNewsletter
                                 select c).First();
                            newsAlterar.email = cliente.clienteEmail;
                            newsAlterar.sexo = "indefinido";
                            newsAlterar.mes = "0";
                            newsAlterar.nome = cliente.clienteNome;
                            newsAlterar.confirmado = true;
                            newsAlterar.dataCadastro = cliente.dataDaCriacao;
                            context.SubmitChanges(); 
                        }
                    }
                    else
                    {
                        string comando =
                            "insert into tbNewsletter (idNewsletter, email, sexo, mes, nome, confirmado, dataCadastro) VALUES (" +
                            cliente.clienteId + ", '" + cliente.clienteEmail.Replace("'", "") + "', 'indefinido', 0, '" +
                            cliente.clienteNome.Replace("'", "") + "', 1, '" + cliente.dataDaCriacao.Year + "-" +
                            cliente.dataDaCriacao.Month + "-" + cliente.dataDaCriacao.Day + " 00:00:00')";
                        context.ExecuteCommand("SET IDENTITY_INSERT tbNewsletter ON");
                        context.ExecuteCommand("delete from tbNewsletter where email = '" + cliente.clienteEmail + "'");
                        context.ExecuteCommand(comando);
                        context.ExecuteCommand("SET IDENTITY_INSERT tbNewsletter OFF");
                    }

                }
                trans.Complete();
            }
        }
    }
}