﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="teste360.aspx.cs" Inherits="teste360" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="//storage.googleapis.com/vrview/2.0/build/vrview.min.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div id="vrview"></div>
        <script>
            window.addEventListener('load', onVrViewLoad)
            function onVrViewLoad() {
                var vrView = new VRView.Player('#vrview', {
                    image: 'https://s3-sa-east-1.amazonaws.com/cdn2.graodegente.com.br/fotos/77821/imagem360.jpg',
                    is_stereo: false
                });
            }
        </script>
    </form>
</body>
</html>
