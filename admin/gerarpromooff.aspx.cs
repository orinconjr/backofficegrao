﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class gerarpromooff : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        var data = new dbCommerceDataContext();
        var produtosBrinde = (from c in data.tbProdutos
                              join d in data.tbJuncaoProdutoCategorias on c.produtoId equals d.produtoId
                              where c.produtoBrindes != "" && (d.categoriaId == 412 | d.categoriaId == 679) select c).ToList();
        foreach (var produtoBrinde in produtosBrinde)
        {
            var categoriaBrindeCheck =
            (from c in data.tbJuncaoProdutoCategorias
             where c.produtoId == produtoBrinde.produtoId && c.categoriaId == 986
             select c).Any();
            if (categoriaBrindeCheck == false)
            {
                var juncao = new tbJuncaoProdutoCategoria();
                juncao.categoriaId = 986;
                juncao.dataDaCriacao = DateTime.Now;
                juncao.produtoId = produtoBrinde.produtoId;
                juncao.produtoPaiId = 0;
                data.tbJuncaoProdutoCategorias.InsertOnSubmit(juncao);
                data.SubmitChanges();
            }
        }
        return;

        var produtosDesconto = (from c in data.tbProdutos where c.produtoPrecoPromocional > 0 && c.produtoAtivo == "True" select c).ToList();

        var desconto20 = new List<int>();
        var desconto40 = new List<int>();
        var desconto70 = new List<int>();
        var descontoBatistela = new List<int>();
        var descontoLaura = new List<int>();
        foreach (var produtoDesconto in produtosDesconto)
        {
            var calculoDesconto = 100 - (((produtoDesconto.produtoPrecoPromocional -
                                    (((decimal) produtoDesconto.produtoPrecoPromocional/100)*15))*100)/
                                  produtoDesconto.produtoPreco);
            if (calculoDesconto <= 20)
            {
                var categoriaBrindeCheck =
                (from c in data.tbJuncaoProdutoCategorias
                 where c.produtoId == produtoDesconto.produtoId && c.categoriaId == 964
                 select c).Any();
                if (categoriaBrindeCheck == false)
                {
                    var juncao = new tbJuncaoProdutoCategoria();
                    juncao.categoriaId = 964;
                    juncao.dataDaCriacao = DateTime.Now;
                    juncao.produtoId = produtoDesconto.produtoId;
                    juncao.produtoPaiId = 0;
                    data.tbJuncaoProdutoCategorias.InsertOnSubmit(juncao);
                    data.SubmitChanges();
                }
                desconto20.Add(produtoDesconto.produtoId);
            }
            else if (calculoDesconto <= 40)
            {
                var categoriaBrindeCheck =
                (from c in data.tbJuncaoProdutoCategorias
                 where c.produtoId == produtoDesconto.produtoId && c.categoriaId == 965
                 select c).Any();
                if (categoriaBrindeCheck == false)
                {
                    var juncao = new tbJuncaoProdutoCategoria();
                    juncao.categoriaId = 965;
                    juncao.dataDaCriacao = DateTime.Now;
                    juncao.produtoId = produtoDesconto.produtoId;
                    juncao.produtoPaiId = 0;
                    data.tbJuncaoProdutoCategorias.InsertOnSubmit(juncao);
                    data.SubmitChanges();
                }
                desconto40.Add(produtoDesconto.produtoId);
            }
            else if (calculoDesconto <= 70)
            {
                var categoriaBrindeCheck =
                (from c in data.tbJuncaoProdutoCategorias
                 where c.produtoId == produtoDesconto.produtoId && c.categoriaId == 966
                 select c).Any();
                if (categoriaBrindeCheck == false)
                {
                    var juncao = new tbJuncaoProdutoCategoria();
                    juncao.categoriaId = 966;
                    juncao.dataDaCriacao = DateTime.Now;
                    juncao.produtoId = produtoDesconto.produtoId;
                    juncao.produtoPaiId = 0;
                    data.tbJuncaoProdutoCategorias.InsertOnSubmit(juncao);
                    data.SubmitChanges();
                }
                desconto70.Add(produtoDesconto.produtoId);
            }
        }
        data.SubmitChanges();
        var listas20 =
            (from c in data.tbJuncaoProdutoCategorias
                where c.categoriaId == 964 && !desconto20.Contains(c.produtoId)
                select c);
        foreach (var lista20 in listas20)
        {
            data.tbJuncaoProdutoCategorias.DeleteOnSubmit(lista20);
        }
        var listas40 =
            (from c in data.tbJuncaoProdutoCategorias
                where c.categoriaId == 965 && !desconto40.Contains(c.produtoId)
                select c);
        foreach (var lista40 in listas40)
        {
            data.tbJuncaoProdutoCategorias.DeleteOnSubmit(lista40);
        }
        var listas70 =
            (from c in data.tbJuncaoProdutoCategorias
                where c.categoriaId == 966 && !desconto70.Contains(c.produtoId)
                select c);
        foreach (var lista70 in listas70)
        {
            data.tbJuncaoProdutoCategorias.DeleteOnSubmit(lista70);
        }
        data.SubmitChanges();
        
        var quartosBatistela = (from c in data.tbProdutos where c.produtoAtivo == "True" && c.produtoBrindes == "1" && c.produtoFornecedor == 41 select c).ToList();
        foreach (var produtoDesconto in quartosBatistela)
        {
            var categoriaBrindeCheck =
            (from c in data.tbJuncaoProdutoCategorias
             where c.produtoId == produtoDesconto.produtoId && c.categoriaId == 968
             select c).Any();
            if (categoriaBrindeCheck == false)
            {
                var juncao = new tbJuncaoProdutoCategoria();
                juncao.categoriaId = 968;
                juncao.dataDaCriacao = DateTime.Now;
                juncao.produtoId = produtoDesconto.produtoId;
                juncao.produtoPaiId = 0;
                data.tbJuncaoProdutoCategorias.InsertOnSubmit(juncao);
                data.SubmitChanges();
            }
        }

        var quartosLaura = (from c in data.tbProdutos where c.produtoAtivo == "True" && c.produtoBrindes == "1" && c.produtoFornecedor == 77 select c).ToList();
        foreach (var produtoDesconto in quartosLaura)
        {
            var categoriaBrindeCheck =
            (from c in data.tbJuncaoProdutoCategorias
             where c.produtoId == produtoDesconto.produtoId && c.categoriaId == 967
             select c).Any();
            if (categoriaBrindeCheck == false)
            {
                var juncao = new tbJuncaoProdutoCategoria();
                juncao.categoriaId = 967;
                juncao.dataDaCriacao = DateTime.Now;
                juncao.produtoId = produtoDesconto.produtoId;
                juncao.produtoPaiId = 0;
                data.tbJuncaoProdutoCategorias.InsertOnSubmit(juncao);
                data.SubmitChanges();
            }
        }
    }
}