﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class atualizaPedidosExtra : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        int pg = 1;
        if (Request.QueryString["pg"] != null)
        {
            int.TryParse(Request.QueryString["pg"], out pg);
        }
        int skip = 50*(pg - 1);
        var pedidosDc = new dbCommerceDataContext();
        var antes = DateTime.Now.AddMonths(-5);
        var listaIds = rnIntegracoes.listaPedidosPendentesExtra(true);
        int pedidoId = 0;
        if (Request.QueryString["id"] != null)
        {
            pedidoId = Convert.ToInt32(Request.QueryString["id"]);
        }
        var pedidos = (from c in pedidosDc.tbPedidos where c.condDePagamentoId == 25 && listaIds.Contains(c.pedidoId) | c.pedidoId == pedidoId orderby c.pedidoId descending select c);
        if (Request.QueryString["id"] != null)
        {
            int id = Convert.ToInt32(Request.QueryString["id"]);
            pedidos = pedidos.Where(x => x.pedidoId == id).OrderByDescending(x => x.pedidoId);
            var pedido = pedidos.First();
            Response.Write(rnIntegracoes.retornaAtualizaStatusPedidoExtra(pedido.pedidoId, 5));
            return;
        }
        //pedidos = pedidos.Skip(skip).Take(50).OrderByDescending(x => x.pedidoId);
        foreach (var pedido in pedidos)
        {
            bool possuiRastreio = false;
            if (!string.IsNullOrEmpty(pedido.numeroDoTracking) | !string.IsNullOrEmpty(pedido.numeroDoTracking2) |
                !string.IsNullOrEmpty(pedido.numeroDoTracking3) | !string.IsNullOrEmpty(pedido.numeroDoTracking4))
                possuiRastreio = true;
            var pacotes = (from c in pedidosDc.tbPedidoPacotes where c.idPedido == pedido.pedidoId select c).Any();
            if (pacotes) possuiRastreio = true;
            if (possuiRastreio)
            {
                try
                {
                    Response.Write(pedido.pedidoId + "<br>");
                    Response.Write(pedido.dataConfirmacaoPagamento + "<br>");
                    Response.Write(rnIntegracoes.atualizaStatusPedidoExtra(pedido.pedidoId, 5) + "<br>");
                }
                catch (Exception ex)
                {
                    Response.Write(pedido.pedidoId + "<br>");
                    Response.Write("erro");
                }
            }
        }

        listaIds = rnIntegracoes.listaPedidosEnviadosExtra(true, 500);
        Response.Write(listaIds.Count() + "<br>");
        pedidos = (from c in pedidosDc.tbPedidos where c.condDePagamentoId == 25 && listaIds.Contains(c.pedidoId) | c.pedidoId == pedidoId orderby c.pedidoId select c);
        foreach (var pedido in pedidos)
        {
            bool entregue = false;
            var pacotes = (from c in pedidosDc.tbPedidoPacotes where c.idPedido == pedido.pedidoId && c.statusAtual == "ENTREGUE" | c.statusAtual == "Entrega Efetuada" | c.statusAtual == "Entrega Realizada" select c).Any();
            if (pacotes) entregue = true;
            if (entregue)
            {
                try
                {
                    Response.Write(pedido.pedidoId + "<br>");
                    Response.Write(pedido.dataConfirmacaoPagamento + "<br>");
                    rnIntegracoes.atualizaStatusPedidoExtra(pedido.pedidoId, 5, true);
                }
                catch (Exception ex)
                {
                    Response.Write(pedido.pedidoId + "<br>");
                    Response.Write("erro");
                }
            }
        }
    }
}