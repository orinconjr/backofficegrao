﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="consultaNsu.aspx.cs" Inherits="consultaNsu" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">

        <asp:Panel runat="server" ID="pnlConsultaNsu">

            <div style="width: 305px;">
                <div>
                    Informe os NSU separando por vírgula:<br />
                    (ex.: 123456,789456,456123)
                <asp:TextBox runat="server" ID="txtNsu" TextMode="MultiLine" Height="80" Width="300"></asp:TextBox>
                </div>
                <div style="text-align: right;">
                    <asp:Button runat="server" ID="btnPesquisar" Text="Pesquisar" OnClick="btnPesquisar_Click" />
                </div>
            </div>
            <div>
                <asp:GridView ID="grd" runat="server" AutoGenerateColumns="False">
                    <Columns>
                        <asp:BoundField HeaderText="NSU" DataField="numeroCobranca" />
                        <asp:TemplateField HeaderText="Cliente ID">
                            <ItemTemplate>
                                <%# rnFuncoes.retornaIdInterno(Convert.ToInt32(Eval("pedidoId"))).ToString() %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="ID Pedido" DataField="pedidoId" />
                        <asp:BoundField HeaderText="Valor" DataField="valor" />
                    </Columns>
                </asp:GridView>
            </div>

        </asp:Panel>
        
        <asp:Panel runat="server" ID="pnlConsultaCodMaxiPago">
            
        </asp:Panel>
    </form>
</body>
</html>
