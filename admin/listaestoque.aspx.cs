﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class listaestoque : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        var output = new StringBuilder();
        output.Append("<table>");
        output.Append("<tr>");
        output.Append("<td>Id</td><td>Produto</td><td>Estoque Site</td><td>Estoque Real</td>");
        output.Append("</tr>");
        var data = new dbCommerceDataContext();
        var produtosEstoque = (from c in data.tbProdutos where c.produtoFornecedor == 72 select c).ToList();
        var estoques = data.admin_produtosEmEstoque().ToList();
        foreach (var produto in produtosEstoque)
        {
            var produtosFilho = (from c in data.tbProdutoRelacionados where c.idProdutoPai == produto.produtoId select c).Any();
            if (!produtosFilho)
            {
                var produtosPai =
                    (from c in data.tbProdutoRelacionados
                     where c.idProdutoFilho == produto.produtoId
                     select c.idProdutoPai).Distinct().ToList();

                if (produtosPai.Count() > 0)
                {
                    foreach (var produtoPai in produtosPai)
                    {
                        var produtoPaiAlterar =
                            (from c in data.tbProdutos where c.produtoId == produtoPai select c).FirstOrDefault();
                        if (produtoPaiAlterar != null)
                        {

                            var produtosCombo =
                                (from c in data.tbProdutoRelacionados where c.idProdutoPai == produtoPai select c).Distinct();
                            int estoqueLivreCombo = -1;
                            int estoquePrevistoCombo = 0;
                            foreach (var produtoCombo in produtosCombo)
                            {
                                var estoqueCombo =
                                    estoques.Where(x => x.produtoId == produtoCombo.idProdutoFilho).FirstOrDefault();
                                if (estoqueCombo != null)
                                {
                                    if (estoqueLivreCombo == -1)
                                        estoqueLivreCombo = (int) estoqueCombo.estoqueLivre +
                                                            (int) estoqueCombo.estoquePrevisto;
                                    else if ((int) estoqueCombo.estoqueLivre < estoqueLivreCombo)
                                        estoqueLivreCombo = (int) estoqueCombo.estoqueLivre +
                                                            (int) estoqueCombo.estoquePrevisto;
                                    //output.AppendFormat(
                                    //    "<tr><td><b>{0}</b></td><td>{1}</td><td>{2}</td><td>{3}</td><td>{4}</td></tr>",
                                    //    produtoCombo.idProdutoFilho, produtoCombo.idProdutoFilho,
                                    //    produtoCombo.idProdutoFilho, estoqueLivreCombo, estoqueCombo.estoqueLivre);
                                }
                                if (estoqueCombo != null)
                                {
                                    //    if (estoquePrevistoCombo == -1)
                                    //        estoquePrevistoCombo = (int) estoqueCombo.estoquePrevisto;
                                    //    else if ((int) estoqueCombo.estoquePrevisto < estoquePrevistoCombo)
                                    //        estoquePrevistoCombo = (int)estoqueCombo.estoquePrevisto;
                                    //    output.AppendFormat(
                                    //        "<tr><td><b>{0}</b></td><td>{1}</td><td>{2}</td><td>{3}</td><td>{4}</td></tr>",
                                    //        produtoCombo.idProdutoFilho, produtoCombo.idProdutoFilho,
                                    //        produtoCombo.idProdutoFilho, estoquePrevistoCombo, estoqueCombo.estoquePrevisto);
                                }
                            }
                            if ((estoqueLivreCombo + estoquePrevistoCombo) <= 0) produtoPaiAlterar.produtoAtivo = "False";
                            else if (produtoPaiAlterar.produtoAtivo == "False" &&
                                     (estoqueLivreCombo + estoquePrevistoCombo) > 0)
                                output.AppendFormat(
                                    "<tr><td><b>{0}</b></td><td>{1}</td><td>{2}</td><td>{3}</td><td>{4}</td></tr>",
                                    produtoPaiAlterar.produtoId, produtoPaiAlterar.produtoNome,
                                    produtoPaiAlterar.produtoEstoqueAtual, estoqueLivreCombo, estoquePrevistoCombo);
                            produtoPaiAlterar.produtoEstoqueAtual = estoqueLivreCombo + estoquePrevistoCombo;
                            data.SubmitChanges();
                            output.AppendFormat("<tr><td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td><td>{4}</td></tr>",
                                produtoPaiAlterar.produtoId, produtoPaiAlterar.produtoNome,
                                produtoPaiAlterar.produtoEstoqueAtual, estoqueLivreCombo, estoquePrevistoCombo);
                        }
                    }
                }
                var estoque = estoques.Where(x => x.produtoId == produto.produtoId).FirstOrDefault();
                int estoqueLivre = 0;
                if (estoque != null)
                {
                    estoqueLivre = (int)estoque.estoqueLivre;
                }
                int estoquePrevisto = 0;
                if (estoque != null)
                {
                    estoquePrevisto = (int)estoque.estoquePrevisto;
                }
                output.Append("<tr>");
                output.AppendFormat("<td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td><td>{4}</td>", produto.produtoId,
                    produto.produtoNome, produto.produtoEstoqueAtual, estoqueLivre, estoquePrevisto);
                output.Append("</tr>");
                produto.produtoEstoqueAtual = estoqueLivre + estoquePrevisto;
                if ((estoqueLivre + estoquePrevisto) <= 0) produto.produtoAtivo = "False";
                else if (produto.produtoAtivo == "False" && (estoqueLivre + estoquePrevisto) > 0)
                    output.AppendFormat(
                        "<tr><td><b>{0}</b></td><td>{1}</td><td>{2}</td><td>{3}</td><td>{4}</td></tr>",
                        produto.produtoId, produto.produtoNome,
                        produto.produtoEstoqueAtual, estoqueLivre, estoquePrevisto);

                data.SubmitChanges();
                
            }
        }
        output.Append("</table>");
        litEstoque.Text = output.ToString();
    }
}