﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class cancelarnota : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnCancelar_OnClick(object sender, EventArgs e)
    {
        Response.Write(rnNotaFiscal.cancelarNotaLindsay(Convert.ToInt32(txtNotaInutilizar.Text), txtSequencia.Text, txtMotivo.Text, Convert.ToInt32(txtNotaId.Text)));
        txtNotaInutilizar.Text = "";
    }
}