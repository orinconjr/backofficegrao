﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class gerarJadlog : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnGerar_OnClick(object sender, EventArgs e)
    {
        int id = 11;
        var faixasDc = new dbCommerceDataContext();
        var faixas = (from c in faixasDc.jadceps select c).ToList();
        //var faixaPrecosAdc = (from c in faixasDc.jadprecos where c.peso == "ADC" select c).ToList();
        /*foreach (var faixaAd in faixaPrecosAdc)
        {
            var faixaPrecos = (from c in faixasDc.jadprecos where c.peso != "ADC" && c.localidade == faixaAd.localidade select c).ToList();

            var tabelalocal = new tbTipoDeEntregaTabelaPreco();
            tabelalocal.nome = faixaAd.localidade + " - local";
            tabelalocal.idTipoDeEntrega = 11;
            tabelalocal.valorKgAdicional = Convert.ToDecimal(faixaAd.local.Replace(".", ","));
            if (faixaAd.localidade == "capital")
            {
                tabelalocal.valorSeguro = Convert.ToDecimal("0.33");
            }
            else
            {
                tabelalocal.valorSeguro = Convert.ToDecimal("0.66");
            }
            faixasDc.tbTipoDeEntregaTabelaPrecos.InsertOnSubmit(tabelalocal);
            faixasDc.SubmitChanges();
            foreach (var faixaPreco in faixaPrecos)
            {
                var tabelaprecos = new tbTipoDeEntregaTabelaPrecoValor();
                tabelaprecos.pesoFinal = Convert.ToInt32(faixaPreco.peso)*1000;
                tabelaprecos.pesoInicial = tabelaprecos.pesoFinal - 1000;
                tabelaprecos.idTipoDeEntregaTabelaPreco = tabelalocal.idTipoDeEntregaTabelaPreco;
                tabelaprecos.valor = Convert.ToDecimal(faixaPreco.local);
                faixasDc.tbTipoDeEntregaTabelaPrecoValors.InsertOnSubmit(tabelaprecos);
                faixasDc.SubmitChanges();
            }


            var tabelaa1 = new tbTipoDeEntregaTabelaPreco();
            tabelaa1.nome = faixaAd.localidade + " - a1";
            tabelaa1.idTipoDeEntrega = 11;
            tabelaa1.valorKgAdicional = Convert.ToDecimal(faixaAd.local.Replace(".", ",")) + Convert.ToInt32(Convert.ToDecimal(faixaAd.a1.Replace(".", ",")));
            if (faixaAd.localidade == "capital")
            {
                tabelaa1.valorSeguro = Convert.ToDecimal("0.33");
            }
            else
            {
                tabelaa1.valorSeguro = Convert.ToDecimal("0.66");
            }
            faixasDc.tbTipoDeEntregaTabelaPrecos.InsertOnSubmit(tabelaa1);
            faixasDc.SubmitChanges();
            foreach (var faixaPreco in faixaPrecos)
            {
                var tabelaprecos = new tbTipoDeEntregaTabelaPrecoValor();
                tabelaprecos.pesoFinal = Convert.ToInt32(faixaPreco.peso)*1000;
                tabelaprecos.pesoInicial = tabelaprecos.pesoFinal - 1000;
                tabelaprecos.idTipoDeEntregaTabelaPreco = tabelaa1.idTipoDeEntregaTabelaPreco;
                tabelaprecos.valor = Convert.ToDecimal(faixaPreco.local.Replace(".", ",")) + Convert.ToDecimal(faixaPreco.a1);
                faixasDc.tbTipoDeEntregaTabelaPrecoValors.InsertOnSubmit(tabelaprecos);
                faixasDc.SubmitChanges();
            }

            var tabelaa2 = new tbTipoDeEntregaTabelaPreco();
            tabelaa2.nome = faixaAd.localidade + " - a2";
            tabelaa2.idTipoDeEntrega = 11;
            tabelaa2.valorKgAdicional = Convert.ToDecimal(faixaAd.local.Replace(".", ",")) + Convert.ToInt32(Convert.ToDecimal(faixaAd.a2.Replace(".", ",")));
            if (faixaAd.localidade == "capital")
            {
                tabelaa2.valorSeguro = Convert.ToDecimal("0.33");
            }
            else
            {
                tabelaa2.valorSeguro = Convert.ToDecimal("0.66");
            }
            faixasDc.tbTipoDeEntregaTabelaPrecos.InsertOnSubmit(tabelaa2);
            faixasDc.SubmitChanges();
            foreach (var faixaPreco in faixaPrecos)
            {
                var tabelaprecos = new tbTipoDeEntregaTabelaPrecoValor();
                tabelaprecos.pesoFinal = Convert.ToInt32(faixaPreco.peso)*1000;
                tabelaprecos.pesoInicial = tabelaprecos.pesoFinal - 1000;
                tabelaprecos.idTipoDeEntregaTabelaPreco = tabelaa2.idTipoDeEntregaTabelaPreco;
                tabelaprecos.valor = Convert.ToDecimal(faixaPreco.local.Replace(".", ",")) + Convert.ToDecimal(faixaPreco.a2);
                faixasDc.tbTipoDeEntregaTabelaPrecoValors.InsertOnSubmit(tabelaprecos);
                faixasDc.SubmitChanges();
            }

            var tabelaa3 = new tbTipoDeEntregaTabelaPreco();
            tabelaa3.nome = faixaAd.localidade + " - a3";
            tabelaa3.idTipoDeEntrega = 11;
            tabelaa3.valorKgAdicional = Convert.ToDecimal(faixaAd.local.Replace(".", ",")) + Convert.ToInt32(Convert.ToDecimal(faixaAd.a3.Replace(".", ",")));
            if (faixaAd.localidade == "capital")
            {
                tabelaa3.valorSeguro = Convert.ToDecimal("0.33");
            }
            else
            {
                tabelaa3.valorSeguro = Convert.ToDecimal("0.66");
            }
            faixasDc.tbTipoDeEntregaTabelaPrecos.InsertOnSubmit(tabelaa3);
            faixasDc.SubmitChanges();
            foreach (var faixaPreco in faixaPrecos)
            {
                var tabelaprecos = new tbTipoDeEntregaTabelaPrecoValor();
                tabelaprecos.pesoFinal = Convert.ToInt32(faixaPreco.peso)*1000;
                tabelaprecos.pesoInicial = tabelaprecos.pesoFinal - 1000;
                tabelaprecos.idTipoDeEntregaTabelaPreco = tabelaa3.idTipoDeEntregaTabelaPreco;
                tabelaprecos.valor = Convert.ToDecimal(faixaPreco.local.Replace(".", ",")) + Convert.ToDecimal(faixaPreco.a3);
                faixasDc.tbTipoDeEntregaTabelaPrecoValors.InsertOnSubmit(tabelaprecos);
                faixasDc.SubmitChanges();
            }

            var tabelaa4 = new tbTipoDeEntregaTabelaPreco();
            tabelaa4.nome = faixaAd.localidade + " - a4";
            tabelaa4.idTipoDeEntrega = 11;
            tabelaa4.valorKgAdicional = Convert.ToDecimal(faixaAd.local.Replace(".", ",")) + Convert.ToInt32(Convert.ToDecimal(faixaAd.a4.Replace(".", ",")));
            if (faixaAd.localidade == "capital")
            {
                tabelaa4.valorSeguro = Convert.ToDecimal("0.33");
            }
            else
            {
                tabelaa4.valorSeguro = Convert.ToDecimal("0.66");
            }
            faixasDc.tbTipoDeEntregaTabelaPrecos.InsertOnSubmit(tabelaa4);
            faixasDc.SubmitChanges();
            foreach (var faixaPreco in faixaPrecos)
            {
                var tabelaprecos = new tbTipoDeEntregaTabelaPrecoValor();
                tabelaprecos.pesoFinal = Convert.ToInt32(faixaPreco.peso)*1000;
                tabelaprecos.pesoInicial = tabelaprecos.pesoFinal - 1000;
                tabelaprecos.idTipoDeEntregaTabelaPreco = tabelaa4.idTipoDeEntregaTabelaPreco;
                tabelaprecos.valor = Convert.ToDecimal(faixaPreco.local.Replace(".", ",")) + Convert.ToDecimal(faixaPreco.a4);
                faixasDc.tbTipoDeEntregaTabelaPrecoValors.InsertOnSubmit(tabelaprecos);
                faixasDc.SubmitChanges();
            }

            var tabelaa5 = new tbTipoDeEntregaTabelaPreco();
            tabelaa5.nome = faixaAd.localidade + " - a5";
            tabelaa5.idTipoDeEntrega = 11;
            tabelaa5.valorKgAdicional = Convert.ToDecimal(faixaAd.local.Replace(".", ",")) + Convert.ToInt32(Convert.ToDecimal(faixaAd.a5.Replace(".", ",")));
            if (faixaAd.localidade == "capital")
            {
                tabelaa5.valorSeguro = Convert.ToDecimal("0.33");
            }
            else
            {
                tabelaa5.valorSeguro = Convert.ToDecimal("0.66");
            }
            faixasDc.tbTipoDeEntregaTabelaPrecos.InsertOnSubmit(tabelaa5);
            faixasDc.SubmitChanges();
            foreach (var faixaPreco in faixaPrecos)
            {
                var tabelaprecos = new tbTipoDeEntregaTabelaPrecoValor();
                tabelaprecos.pesoFinal = Convert.ToInt32(faixaPreco.peso)*1000;
                tabelaprecos.pesoInicial = tabelaprecos.pesoFinal - 1000;
                tabelaprecos.idTipoDeEntregaTabelaPreco = tabelaa5.idTipoDeEntregaTabelaPreco;
                tabelaprecos.valor = Convert.ToDecimal(faixaPreco.local.Replace(".", ",")) + Convert.ToDecimal(faixaPreco.a5);
                faixasDc.tbTipoDeEntregaTabelaPrecoValors.InsertOnSubmit(tabelaprecos);
                faixasDc.SubmitChanges();
            }

            var tabelaa6 = new tbTipoDeEntregaTabelaPreco();
            tabelaa6.nome = faixaAd.localidade + " - a6";
            tabelaa6.idTipoDeEntrega = 11;
            tabelaa6.valorKgAdicional = Convert.ToDecimal(faixaAd.local.Replace(".", ",")) + Convert.ToInt32(Convert.ToDecimal(faixaAd.a6.Replace(".", ",")));
            if (faixaAd.localidade == "capital")
            {
                tabelaa6.valorSeguro = Convert.ToDecimal("0.33");
            }
            else
            {
                tabelaa6.valorSeguro = Convert.ToDecimal("0.66");
            }
            faixasDc.tbTipoDeEntregaTabelaPrecos.InsertOnSubmit(tabelaa6);
            faixasDc.SubmitChanges();
            foreach (var faixaPreco in faixaPrecos)
            {
                var tabelaprecos = new tbTipoDeEntregaTabelaPrecoValor();
                tabelaprecos.pesoFinal = Convert.ToInt32(faixaPreco.peso)*1000;
                tabelaprecos.pesoInicial = tabelaprecos.pesoFinal - 1000;
                tabelaprecos.idTipoDeEntregaTabelaPreco = tabelaa6.idTipoDeEntregaTabelaPreco;
                tabelaprecos.valor = Convert.ToDecimal(faixaPreco.local.Replace(".", ",")) + Convert.ToDecimal(faixaPreco.a6);
                faixasDc.tbTipoDeEntregaTabelaPrecoValors.InsertOnSubmit(tabelaprecos);
                faixasDc.SubmitChanges();
            }

        }*/
        int contagem = 0;
        foreach (var faixa in faixas)
        {
            contagem ++;
            if (contagem > 3611)
            {


                var faixaDeCepDc = new dbCommerceDataContext();
                var faixaDeCep = new tbFaixaDeCep();
                faixaDeCep.tipodeEntregaId = id;
                faixaDeCep.faixaDeCepDescricao = "JADLOG - " + faixa.uf + " - " + faixa.localidade;
                var cepSplit = faixa.cep.Split('a');
                if (cepSplit.Count() > 1)
                {
                    faixaDeCep.faixaInicial = cepSplit[0].Trim();
                    faixaDeCep.faixaFinal = cepSplit[1].Trim();
                }
                else
                {
                    faixaDeCep.faixaInicial = cepSplit[0].Trim();
                    faixaDeCep.faixaFinal = cepSplit[0].Trim();
                }
                faixaDeCep.prazo = Convert.ToInt32(faixa.prazo.Trim()) + 1;
                faixaDeCep.porcentagemDeDesconto = 0;
                faixaDeCep.porcentagemDoSeguro = 0;
                faixaDeCep.dataDaCriacao = DateTime.Now;
                if (faixa.tarifa.ToLower() == "interior")
                {
                    faixaDeCep.interiorizacao = true;
                }
                else
                {
                    faixaDeCep.interiorizacao = false;
                }

                int capitalLocal = 229;
                int capitala1 = 230;
                int capitala2 = 231;
                int capitala3 = 232;
                int capitala4 = 233;
                int capitala5 = 234;
                int capitala6 = 235;

                int interiorLocal = 236;
                int interiora1 = 237;
                int interiora2 = 238;
                int interiora3 = 239;
                int interiora4 = 240;
                int interiora5 = 241;
                int interiora6 = 242;

                if (faixa.uf.ToLower() == "sp")
                {
                    faixaDeCep.idTipoDeEntregaTabelaPreco = capitalLocal;
                    if (faixa.tarifa.ToLower() == "interior")
                    {
                        faixaDeCep.idTipoDeEntregaTabelaPreco = interiorLocal;
                    }
                }
                else if (faixa.uf.ToLower() == "mg" | faixa.uf.ToLower() == "pr" | faixa.uf.ToLower() == "rj" |
                         faixa.uf.ToLower() == "sc")
                {
                    faixaDeCep.idTipoDeEntregaTabelaPreco = capitala1;
                    if (faixa.tarifa.ToLower() == "interior")
                    {
                        faixaDeCep.idTipoDeEntregaTabelaPreco = interiora1;
                    }
                }
                else if (faixa.uf.ToLower() == "df" | faixa.uf.ToLower() == "es")
                {
                    faixaDeCep.idTipoDeEntregaTabelaPreco = capitala2;
                    if (faixa.tarifa.ToLower() == "interior")
                    {
                        faixaDeCep.idTipoDeEntregaTabelaPreco = interiora2;
                    }
                }
                else if (faixa.uf.ToLower() == "go" | faixa.uf.ToLower() == "ms" | faixa.uf.ToLower() == "rs")
                {
                    faixaDeCep.idTipoDeEntregaTabelaPreco = capitala3;
                    if (faixa.tarifa.ToLower() == "interior")
                    {
                        faixaDeCep.idTipoDeEntregaTabelaPreco = interiora3;
                    }
                }
                else if (faixa.uf.ToLower() == "al" | faixa.uf.ToLower() == "ba" | faixa.uf.ToLower() == "mt" |
                         faixa.uf.ToLower() == "pb" | faixa.uf.ToLower() == "pe" | faixa.uf.ToLower() == "se" |
                         faixa.uf.ToLower() == "to")
                {
                    faixaDeCep.idTipoDeEntregaTabelaPreco = capitala4;
                    if (faixa.tarifa.ToLower() == "interior")
                    {
                        faixaDeCep.idTipoDeEntregaTabelaPreco = interiora4;
                    }
                }
                else if (faixa.uf.ToLower() == "ac" | faixa.uf.ToLower() == "am" | faixa.uf.ToLower() == "ap" |
                         faixa.uf.ToLower() == "pa" | faixa.uf.ToLower() == "ro" | faixa.uf.ToLower() == "rr")
                {
                    faixaDeCep.idTipoDeEntregaTabelaPreco = capitala5;
                    if (faixa.tarifa.ToLower() == "interior")
                    {
                        faixaDeCep.idTipoDeEntregaTabelaPreco = interiora5;
                    }
                }
                else if (faixa.uf.ToLower() == "ce" | faixa.uf.ToLower() == "ma" | faixa.uf.ToLower() == "pi" |
                         faixa.uf.ToLower() == "rn")
                {
                    faixaDeCep.idTipoDeEntregaTabelaPreco = capitala6;
                    if (faixa.tarifa.ToLower() == "interior")
                    {
                        faixaDeCep.idTipoDeEntregaTabelaPreco = interiora6;
                    }
                }

                faixaDeCepDc.tbFaixaDeCeps.InsertOnSubmit(faixaDeCep);
                faixaDeCepDc.SubmitChanges();



                if (faixa.redespacho.Trim() == "FLUVIAL")
                {
                    var faixaTarifa = new tbFaixaDeCepTarifa();
                    faixaTarifa.idFaixaDeCep = faixaDeCep.faixaDeCepId;
                    faixaTarifa.idTipoDeEntregaTarifa = 12;
                    faixaDeCepDc.tbFaixaDeCepTarifas.InsertOnSubmit(faixaTarifa);
                    faixaDeCepDc.SubmitChanges();
                }
            }
        }
        
        
        /*foreach (var faixa in faixas)
        {
            var faixaDeCepDc = new dbCommerceDataContext();
            var faixaDeCep = new tbFaixaDeCep();
            faixaDeCep.tipodeEntregaId = id;
            faixaDeCep.faixaDeCepDescricao = "JADLOG - " + faixa.uf + " - " + faixa.localidade;
            var cepSplit = faixa.cep.Split('a');
            if (cepSplit.Count() > 1)
            {
                faixaDeCep.faixaInicial = cepSplit[0].Trim();
                faixaDeCep.faixaFinal = cepSplit[1].Trim();
            }
            else
            {
                faixaDeCep.faixaInicial = cepSplit[0].Trim();
                faixaDeCep.faixaFinal = cepSplit[0].Trim();
            }
            faixaDeCep.prazo = Convert.ToInt32(faixa.prazo.Trim()) + 1;
            faixaDeCep.porcentagemDeDesconto = 0;
            faixaDeCep.porcentagemDoSeguro = 0;
            if (faixa.redespacho.Trim() == "FLUVIAL")
            {
                faixaDeCep.porcentagemDoSeguro += 8;
            }
            faixaDeCep.dataDaCriacao = DateTime.Now;
            if (faixa.tarifa.ToLower() == "interior")
            {
                faixaDeCep.interiorizacao = true;
            }
            else
            {
                faixaDeCep.interiorizacao = false;
            }
            faixaDeCepDc.tbFaixaDeCeps.InsertOnSubmit(faixaDeCep);
            faixaDeCepDc.SubmitChanges();

            for (int i = 1; i < 31; i++)
            {
                var faixaPrecoDc = new dbCommerceDataContext();
                var faixaPreco = new tbFaixaDeCepPesoPreco();
                faixaPreco.faixaDeCepId = faixaDeCep.faixaDeCepId;
                faixaPreco.faixaDeCepPesoInicial = (i*1000) - 999;
                faixaPreco.faixaDeCepPesoFinal = (i*1000);
                faixaPreco.dataDaCriacao = DateTime.Now;
                var precosCapital = faixaPrecos.Where(x => x.peso == i.ToString() && x.localidade == "capital").First();
                var precosInterior = faixaPrecos.Where(x => x.peso == i.ToString() && x.localidade == "interior").First();
                if (faixa.uf.ToLower() == "sp")
                {
                    faixaPreco.faixaDeCepPreco = Convert.ToDecimal(precosCapital.local);
                    if (faixa.tarifa.ToLower() == "interior")
                    {
                        faixaPreco.faixaDeCepPreco = Convert.ToDecimal(precosCapital.local) +  + Convert.ToDecimal(precosInterior.local);
                    }
                }
                else if (faixa.uf.ToLower() == "mg" | faixa.uf.ToLower() == "pr" | faixa.uf.ToLower() == "rj" | faixa.uf.ToLower() == "sc")
                {
                    faixaPreco.faixaDeCepPreco = Convert.ToDecimal(precosCapital.a1);
                    if (faixa.tarifa.ToLower() == "interior")
                    {
                        faixaPreco.faixaDeCepPreco = Convert.ToDecimal(precosCapital.a1) + Convert.ToDecimal(precosInterior.a1);
                    }
                }
                else if (faixa.uf.ToLower() == "df" | faixa.uf.ToLower() == "es")
                {
                    faixaPreco.faixaDeCepPreco = Convert.ToDecimal(precosCapital.a2);
                    if (faixa.tarifa.ToLower() == "interior")
                    {
                        faixaPreco.faixaDeCepPreco = Convert.ToDecimal(precosCapital.a2) + Convert.ToDecimal(precosInterior.a2);
                    }
                }
                else if (faixa.uf.ToLower() == "go" | faixa.uf.ToLower() == "ms" | faixa.uf.ToLower() == "rs")
                {
                    faixaPreco.faixaDeCepPreco = Convert.ToDecimal(precosCapital.a3);
                    if (faixa.tarifa.ToLower() == "interior")
                    {
                        faixaPreco.faixaDeCepPreco = Convert.ToDecimal(precosCapital.a3) + Convert.ToDecimal(precosInterior.a3);
                    }
                }
                else if (faixa.uf.ToLower() == "al" | faixa.uf.ToLower() == "ba" | faixa.uf.ToLower() == "mt" | faixa.uf.ToLower() == "pb" | faixa.uf.ToLower() == "pe" | faixa.uf.ToLower() == "se" | faixa.uf.ToLower() == "to")
                {
                    faixaPreco.faixaDeCepPreco = Convert.ToDecimal(precosCapital.a4);
                    if (faixa.tarifa.ToLower() == "interior")
                    {
                        faixaPreco.faixaDeCepPreco = Convert.ToDecimal(precosCapital.a4) + Convert.ToDecimal(precosInterior.a4);
                    }
                }
                else if (faixa.uf.ToLower() == "ac" | faixa.uf.ToLower() == "am" | faixa.uf.ToLower() == "ap" | faixa.uf.ToLower() == "pa" | faixa.uf.ToLower() == "ro" | faixa.uf.ToLower() == "rr")
                {
                    faixaPreco.faixaDeCepPreco = Convert.ToDecimal(precosCapital.a5);
                    if (faixa.tarifa.ToLower() == "interior")
                    {
                        faixaPreco.faixaDeCepPreco = Convert.ToDecimal(precosCapital.a5) + Convert.ToDecimal(precosInterior.a5);
                    }
                }
                else if (faixa.uf.ToLower() == "ce" | faixa.uf.ToLower() == "ma" | faixa.uf.ToLower() == "pi" | faixa.uf.ToLower() == "rn")
                {
                    faixaPreco.faixaDeCepPreco = Convert.ToDecimal(precosCapital.a6);
                    if (faixa.tarifa.ToLower() == "interior")
                    {
                        faixaPreco.faixaDeCepPreco = Convert.ToDecimal(precosCapital.a6) + Convert.ToDecimal(precosInterior.a6);
                    }
                }

                faixaPrecoDc.tbFaixaDeCepPesoPrecos.InsertOnSubmit(faixaPreco);
                faixaPrecoDc.SubmitChanges();
            }

            for (int i = 31; i < 41; i++)
            {
                var faixaPrecoDc = new dbCommerceDataContext();
                var faixaPreco = new tbFaixaDeCepPesoPreco();
                faixaPreco.faixaDeCepId = faixaDeCep.faixaDeCepId;
                faixaPreco.faixaDeCepPesoInicial = (i * 1000) - 999;
                faixaPreco.faixaDeCepPesoFinal = (i * 1000);
                var precosCapitalAdc = faixaPrecosAdc.Where(x => x.localidade == "capital").First();
                var precosInteriorAdc = faixaPrecosAdc.Where(x => x.localidade == "interior").First();

                faixaPreco.dataDaCriacao = DateTime.Now;
                var precosCapital = faixaPrecos.Where(x => x.peso == "30" && x.localidade == "capital").First();
                var precosInterior = faixaPrecos.Where(x => x.peso == "30" && x.localidade == "interior").First();
                if (faixa.uf.ToLower() == "sp")
                {
                    faixaPreco.faixaDeCepPreco = Convert.ToDecimal(precosCapital.local) + (Convert.ToDecimal(precosCapitalAdc.local) * (i - 30));
                    if (faixa.tarifa.ToLower() == "interior")
                    {
                        faixaPreco.faixaDeCepPreco = Convert.ToDecimal(precosCapital.local) + (Convert.ToDecimal(precosCapitalAdc.local) * (i - 30)) + Convert.ToDecimal(precosInterior.local) + (Convert.ToDecimal(precosInteriorAdc.local) * (i - 30));
                    }
                }
                else if (faixa.uf.ToLower() == "mg" | faixa.uf.ToLower() == "pr" | faixa.uf.ToLower() == "rj" | faixa.uf.ToLower() == "sc")
                {
                    faixaPreco.faixaDeCepPreco = Convert.ToDecimal(precosCapital.a1) + (Convert.ToDecimal(precosCapitalAdc.a1) * (i - 30));
                    if (faixa.tarifa.ToLower() == "interior")
                    {
                        faixaPreco.faixaDeCepPreco = Convert.ToDecimal(precosCapital.a1) + (Convert.ToDecimal(precosCapitalAdc.a1) * (i - 30)) + Convert.ToDecimal(precosInterior.a1) + (Convert.ToDecimal(precosInteriorAdc.a1) * (i - 30));
                    }
                }
                else if (faixa.uf.ToLower() == "df" | faixa.uf.ToLower() == "es")
                {
                    faixaPreco.faixaDeCepPreco = Convert.ToDecimal(precosCapital.a2) + (Convert.ToDecimal(precosCapitalAdc.a2) * (i - 30));
                    if (faixa.tarifa.ToLower() == "interior")
                    {
                        faixaPreco.faixaDeCepPreco = Convert.ToDecimal(precosCapital.a2) + (Convert.ToDecimal(precosCapitalAdc.a2) * (i - 30)) + Convert.ToDecimal(precosInterior.a2) + (Convert.ToDecimal(precosInteriorAdc.a2) * (i - 30));
                    }
                }
                else if (faixa.uf.ToLower() == "go" | faixa.uf.ToLower() == "ms" | faixa.uf.ToLower() == "rs")
                {
                    faixaPreco.faixaDeCepPreco = Convert.ToDecimal(precosCapital.a3) + (Convert.ToDecimal(precosCapitalAdc.a3) * (i - 30));
                    if (faixa.tarifa.ToLower() == "interior")
                    {
                        faixaPreco.faixaDeCepPreco = Convert.ToDecimal(precosCapital.a3) + (Convert.ToDecimal(precosCapitalAdc.a3) * (i - 30)) + Convert.ToDecimal(precosInterior.a3) + (Convert.ToDecimal(precosInteriorAdc.a3) * (i - 30));
                    }
                }
                else if (faixa.uf.ToLower() == "al" | faixa.uf.ToLower() == "ba" | faixa.uf.ToLower() == "mt" | faixa.uf.ToLower() == "pb" | faixa.uf.ToLower() == "pe" | faixa.uf.ToLower() == "se" | faixa.uf.ToLower() == "to")
                {
                    faixaPreco.faixaDeCepPreco = Convert.ToDecimal(precosCapital.a4) + (Convert.ToDecimal(precosCapitalAdc.a4) * (i - 30));
                    if (faixa.tarifa.ToLower() == "interior")
                    {
                        faixaPreco.faixaDeCepPreco = Convert.ToDecimal(precosCapital.a4) + (Convert.ToDecimal(precosCapitalAdc.a4) * (i - 30)) + Convert.ToDecimal(precosInterior.a4) + (Convert.ToDecimal(precosInteriorAdc.a4) * (i - 30));
                    }
                }
                else if (faixa.uf.ToLower() == "ac" | faixa.uf.ToLower() == "am" | faixa.uf.ToLower() == "ap" | faixa.uf.ToLower() == "pa" | faixa.uf.ToLower() == "ro" | faixa.uf.ToLower() == "rr")
                {
                    faixaPreco.faixaDeCepPreco = Convert.ToDecimal(precosCapital.a5) + (Convert.ToDecimal(precosCapitalAdc.a5) * (i - 30));
                    if (faixa.tarifa.ToLower() == "interior")
                    {
                        faixaPreco.faixaDeCepPreco = Convert.ToDecimal(precosCapital.a5) + (Convert.ToDecimal(precosCapitalAdc.a5) * (i - 30)) + Convert.ToDecimal(precosInterior.a5) + (Convert.ToDecimal(precosInteriorAdc.a5) * (i - 30));
                    }
                }
                else if (faixa.uf.ToLower() == "ce" | faixa.uf.ToLower() == "ma" | faixa.uf.ToLower() == "pi" | faixa.uf.ToLower() == "rn")
                {
                    faixaPreco.faixaDeCepPreco = Convert.ToDecimal(precosCapital.a6) + (Convert.ToDecimal(precosCapitalAdc.a6) * (i - 30));
                    if (faixa.tarifa.ToLower() == "interior")
                    {
                        faixaPreco.faixaDeCepPreco = Convert.ToDecimal(precosCapital.a6) + (Convert.ToDecimal(precosCapitalAdc.a6) * (i - 30)) + Convert.ToDecimal(precosInterior.a6) + (Convert.ToDecimal(precosInteriorAdc.a6) * (i - 30));
                    }
                }

                faixaPrecoDc.tbFaixaDeCepPesoPrecos.InsertOnSubmit(faixaPreco);
                faixaPrecoDc.SubmitChanges();
            }
        }*/
        Response.Write("OK");
    }
}