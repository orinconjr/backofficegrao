﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class listaCompletosErro : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        var data = new dbCommerceDataContext();
        var pedidos =
            (from c in data.tbPedidos
             where c.statusDoPedido == 11 &&
                 c.prazoMaximoPostagemAtualizado == null &&
                 c.tipoDePagamentoId != 11 &&
                 (c.prazoInicioFabricao ?? 0) == 0  orderby c.prazoMaximoPedidos descending
             select c);
        foreach (var pedido in pedidos)
        {
            bool erroDatas = false;
            if (pedido.prazoMaximoPedidos == null)
            {
                Response.Write(pedido.pedidoId.ToString() + " - PrazoMaximoPedidos<br>");
                erroDatas = true;
            }
            if (pedido.dataConfirmacaoPagamento == null)
            {
                Response.Write(pedido.pedidoId.ToString() + " - DataConfirmacao<br>");
                erroDatas = true;
            }
            if (!erroDatas)
            {
                int prazoFinal = CalcularPrazoMaximoPedido(pedido.pedidoId);
                if (prazoFinal != ((int)pedido.prazoMaximoPedidos))
                {
                    var prazoFinalAtualizado = rnFuncoes.retornaPrazoDiasUteis(prazoFinal, (pedido.prazoInicioFabricao ?? 0), (DateTime)pedido.dataConfirmacaoPagamento);
                    var dataFinal = ((DateTime)pedido.dataConfirmacaoPagamento).AddDays(prazoFinalAtualizado);
                    Response.Write(pedido.pedidoId.ToString() + " - " + pedido.prazoMaximoPedidos + " - " + prazoFinal + " - " + dataFinal.ToShortDateString() + " - " + ((DateTime)pedido.prazoFinalPedido).ToShortDateString() + "<br>");
                    var itens = (from c in data.tbItensPedidos where c.pedidoId == pedido.pedidoId select c).ToList();
                    foreach (var item in itens)
                    {
                        Response.Write("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +
                                       item.tbProduto.tbProdutoFornecedor.fornecedorNome + " - " +
                                       item.tbProduto.produtoNome + "<br>");
                    }
                }
            }
        }
    }


    private int CalcularPrazoMaximoPedido(int pedidoId)
    {

        using (var data = new dbCommerceDataContext())
        {
            var pedido = (from c in data.tbPedidos where c.pedidoId == pedidoId select c).First();
            var itensPedido =
                (from c in data.tbItensPedidos
                 where c.pedidoId == pedidoId && (c.cancelado ?? false) == false
                 select c).ToList();
            int prazoProduto = 0;
            int prazoInicioFabricaoProduto = 0;
            foreach (var item in itensPedido)
            {
                var relacionados =
                    (from c in data.tbItensPedidoCombos
                     where c.idItemPedido == item.itemPedidoId
                     select c);
                if (relacionados.Any())
                {
                    foreach (var relacionado in relacionados)
                    {
                        var reservasAtuais = (from c in data.tbProdutoReservaEstoques
                                              where
                                                  c.idPedido == pedidoId && c.idItemPedido == item.itemPedidoId &&
                                                  c.idProduto == relacionado.produtoId
                                              select c).Count();
                        if (reservasAtuais < Convert.ToInt32(item.itemQuantidade))
                        {
                            var prazo =
                                (relacionado.tbProduto.tbProdutoFornecedor
                                    .fornecedorPrazoDeFabricacao ?? 0);
                            if (prazoProduto < prazo && prazo > 0)
                            {
                                prazoProduto = prazo;
                            }
                        }
                    }
                }
                else
                {
                    var reservasAtuais = (from c in data.tbProdutoReservaEstoques
                                          where
                                              c.idPedido == pedidoId && c.idItemPedido == item.itemPedidoId &&
                                              c.idProduto == item.produtoId
                                          select c).Count();
                    if (reservasAtuais < Convert.ToInt32(item.itemQuantidade))
                    {
                        var pedidosFornecedorItem = (from c in data.tbPedidoFornecedorItems
                                                     where
                                                         c.idItemPedido == item.itemPedidoId &&
                                                         c.idProduto == item.produtoId
                                                     select c).ToList();
                        var prazo =
                            (item.tbProduto.tbProdutoFornecedor
                                .fornecedorPrazoDeFabricacao ?? 0);
                        if (prazoProduto < prazo && prazo > 0)
                        {
                            prazoProduto = prazo;
                        }
                    }
                }
            }

            return (prazoProduto + 1);
        }

        return 0;
    }
}