﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Xml;
using Ionic.Zip;

public partial class consultarNf : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            using (var data = new dbCommerceDataContext())
            {
                var empresas = (from c in data.tbEmpresas select new { c.idEmpresa, empresa = c.cnpj + " - " + c.nome }).ToList();
                ddlEmpresas.DataSource = empresas;
                ddlEmpresas.DataBind();
            }
        }
    }
    protected void OnClick(object sender, EventArgs e)
    {
        var listNotas = new List<int>();
        int idEmpresa = Convert.ToInt32(ddlEmpresas.SelectedValue);


        if (rblDanfeXml.SelectedItem.Value == "danfe")
        {
            try
            {

                if (txtDataInicial.Text == "" && txtDataFinal.Text == "")
                {
                    string[] notas = txtNotas.Text.Split(',');
                    listNotas.AddRange(notas.Select(t => Convert.ToInt32(t)));

                    using (var data = new dbCommerceDataContext())
                    {
                        var pesquisaNotas = (from c in data.tbNotaFiscals where listNotas.Contains(c.numeroNota) && c.idCNPJ == idEmpresa select c).ToList();

                        GridView1.DataSource = pesquisaNotas;
                        GridView1.DataBind();
                    }
                }
                else if ((txtDataInicial.Text != "" && txtDataFinal.Text != "") && txtNotas.Text != "")
                {
                    string[] notas = txtNotas.Text.Split(',');
                    listNotas.AddRange(notas.Select(t => Convert.ToInt32(t)));

                    var dataIni = Convert.ToDateTime(txtDataInicial.Text);
                    var dataFim = Convert.ToDateTime(txtDataFinal.Text);

                    using (var data = new dbCommerceDataContext())
                    {
                        var pesquisaNotas = (from c in data.tbNotaFiscals where (c.dataHora.Date >= dataIni.Date && c.dataHora.Date <= dataFim.Date) && listNotas.Contains(c.numeroNota) && c.idCNPJ == idEmpresa select c).ToList();

                        GridView1.DataSource = pesquisaNotas;
                        GridView1.DataBind();
                    }
                }
                else
                {
                    using (var data = new dbCommerceDataContext())
                    {
                        var dataIni = Convert.ToDateTime(txtDataInicial.Text);
                        var dataFim = Convert.ToDateTime(txtDataFinal.Text);

                        if (txtDataInicial.Text != "" && txtDataFinal.Text != "")
                        {
                            var pesquisaNotas = (from c in data.tbNotaFiscals where (c.dataHora.Date >= dataIni.Date && c.dataHora.Date <= dataFim.Date) && c.idCNPJ == idEmpresa select c).ToList();

                            GridView1.DataSource = pesquisaNotas;
                            GridView1.DataBind();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                string erro = ex.Message;
                throw;
            }
        }
        else if (rblDanfeXml.SelectedItem.Value == "xml")
        {

            string[] notas;

            if (txtNotas.Text != "")
            {

                notas = txtNotas.Text.Split(',');
                listNotas.AddRange(notas.Select(t => Convert.ToInt32(t)));

            }
            else
            {
                using (var data = new dbCommerceDataContext())
                {
                    DateTime dataInicial = Convert.ToDateTime(txtDataInicial.Text);
                    DateTime dataFinal = Convert.ToDateTime(txtDataFinal.Text);

                    var notasNoPeriodo = (from c in data.tbNotaFiscals
                                          where
                                              c.idCNPJ == idEmpresa &&
                                              (c.dataHora.Date >= dataInicial.Date && c.dataHora.Date <= dataFinal.Date)
                                          select c.numeroNota);

                    notas = new string[notasNoPeriodo.Count()];
                    int index = 0;

                    foreach (var nota in notasNoPeriodo)
                    {
                        listNotas.Add(nota);
                        notas[index] = nota.ToString();
                        index++;
                    }
                }
            }

            infoNotaXml.Text = "";

            using (var data = new dbCommerceDataContext())
            {

                int numeroInicial = listNotas.Min();
                int numeroFinal = listNotas.Max();

                //var pesquisaNotas =
                //    (from c in data.tbNotaFiscals
                //     where listNotas.Contains(c.numeroNota) && c.idCNPJ == idEmpresa
                //     select new { c.numeroNota, c.idNotaFiscal, c.idCNPJ }).ToList();

                var pesquisaNotas =
                   (from c in data.tbNotaFiscals
                    where (c.numeroNota >= numeroInicial && c.numeroNota <= numeroFinal) && c.idCNPJ == idEmpresa
                    select new { c.numeroNota, c.idNotaFiscal, c.idCNPJ }).ToList();


                foreach (var nota in pesquisaNotas)
                {

                    //if (!File.Exists(Server.MapPath("~/notas/xmls/" + nota.idCNPJ + "_" + nota.numeroNota + ".xml")))
                    if (!File.Exists("c:/notas/xmls/" + nota.idCNPJ + "_" + nota.numeroNota + ".xml"))
                    {
                        XmlDocument doc = new XmlDocument();
                        string xml = rnNotaFiscal.retornaXmlNota(nota.numeroNota, nota.idNotaFiscal);


                        if (String.IsNullOrEmpty(xml))
                        {
                            infoNotaXml.Text += "nota: " + nota.numeroNota + " - INUTILIZADA<br>";
                        }
                        else
                        {
                            doc.LoadXml(xml);
                            doc.Save(Server.MapPath("~/notas/xmls/" + nota.idCNPJ + "_" + nota.numeroNota + ".xml"));
                        }
                    }
                }

                Response.ClearContent();
                Response.ClearHeaders();
                Response.Clear();
                Response.ContentType = "application/zip";
                Response.AddHeader("content-disposition", "filename=" + "download-xmls-" + idEmpresa + ".zip");

                using (ZipFile zip = new ZipFile())
                {

                    if (notas.Count() > 2000)
                    {

                    }

                    foreach (var nota in notas)
                    {
                        if (File.Exists(Server.MapPath("~/notas/xmls/" + idEmpresa + "_" + nota.Trim() + ".xml")))
                        {
                            #region Compacta txt's em Zip

                            byte[] zipContent = null;


                            var filesToInclude = new System.Collections.Generic.List<String>();

                            filesToInclude.Add(Server.MapPath("~/notas/xmls/" + idEmpresa + "_" + nota.Trim() + ".xml"));
                            string verificaArquivo = idEmpresa + "_" + nota.Trim() + ".xml";
                            var nameCount = zip.Count(entry => entry.FileName == verificaArquivo);
                            if (nameCount == 0)
                            {
                                zip.AddFiles(filesToInclude, false, "");
                            }

                            #endregion Compacta txt's em Zip
                        }

                    }

                    zip.Save(Response.OutputStream);

                }

            }

        }
        else
        {
            string[] notas;

            if (txtNotas.Text != "")
            {

                notas = txtNotas.Text.Trim().Split(',');
                listNotas.AddRange(notas.Select(t => Convert.ToInt32(t)));

            }
            else
            {
                using (var data = new dbCommerceDataContext())
                {
                    DateTime dataInicial = Convert.ToDateTime(txtDataInicial.Text);
                    DateTime dataFinal = Convert.ToDateTime(txtDataFinal.Text);

                    var notasNoPeriodo = (from c in data.tbNotaFiscals
                                          where
                                              c.idCNPJ == idEmpresa &&
                                              (c.dataHora.Date >= dataInicial.Date && c.dataHora.Date <= dataFinal.Date)
                                          select c.numeroNota);

                    notas = new string[notasNoPeriodo.Count()];
                    int index = 0;

                    foreach (var nota in notasNoPeriodo)
                    {
                        listNotas.Add(nota);
                        notas[index] = nota.ToString();
                        index++;
                    }
                }
            }

            infoNotaXml.Text = "";

            using (var data = new dbCommerceDataContext())
            {

                int numeroInicial = listNotas.Min();
                int numeroFinal = listNotas.Max();

                //var pesquisaNotas =
                //    (from c in data.tbNotaFiscals
                //     where listNotas.Contains(c.numeroNota) && c.idCNPJ == idEmpresa
                //     select new { c.numeroNota, c.idNotaFiscal, c.idCNPJ }).ToList();

                var pesquisaNotas =
                   (from c in data.tbNotaFiscals
                    where (c.numeroNota >= numeroInicial && c.numeroNota <= numeroFinal) && c.idCNPJ == idEmpresa
                    select new { c.numeroNota, c.idNotaFiscal, c.idCNPJ }).ToList();


                foreach (var nota in pesquisaNotas)
                {
                    int idCnpj = Convert.ToInt32(nota.idCNPJ);
                    var danfe = rnNotaFiscal.gravaDanfeBase64(nota.numeroNota, idCnpj, false);

                    if (danfe)
                    {

                    }
                    else
                    {
                        infoNotaXml.Text += "nota: " + nota.numeroNota + " - INUTILIZADA<br>";
                    }



                }

                Response.ClearContent();
                Response.ClearHeaders();
                Response.Clear();
                Response.ContentType = "application/zip";
                Response.AddHeader("content-disposition", "filename=" + "download-danfe-" + idEmpresa + ".zip");

                using (ZipFile zip = new ZipFile())
                {

                    foreach (var nota in notas)
                    {

                        //if (File.Exists(Server.MapPath("~/notas/danfes/" + idEmpresa + "_" + nota.Trim() + ".pdf")))
                        if (!File.Exists(Path.GetFullPath("C:/inetpub/wwwroot/admin/notas/danfes/" + idEmpresa + "_" + nota.Trim() + ".pdf")))
                            rnNotaFiscal.gravaDanfeBase64(nota.Trim(), idEmpresa, false);

                        if (File.Exists(Path.GetFullPath("C:/inetpub/wwwroot/admin/notas/danfes/" + idEmpresa + "_" + nota.Trim() + ".pdf")))
                        {
                            #region Compacta txt's em Zip

                            byte[] zipContent = null;

                            var filesToInclude = new List<String>
                            {
                                //Server.MapPath("~/notas/danfes/" + idEmpresa + "_" + nota.Trim() + ".pdf")
                                Path.GetFullPath("C:/inetpub/wwwroot/admin/notas/danfes/" + idEmpresa + "_" + nota.Trim() + ".pdf")
                            };

                            string verificaArquivo = idEmpresa + "_" + nota.Trim() + ".xml";
                            var nameCount = zip.Count(entry => entry.FileName == verificaArquivo);
                            if (nameCount == 0)
                            {
                                zip.AddFiles(filesToInclude, false, "");
                            }

                            #endregion Compacta txt's em Zip
                        }

                    }

                    zip.Save(Response.OutputStream);

                }

            }
        }

    }

    public string EmpresaCnpj(int empresaId)
    {
        switch (empresaId)
        {
            case 1:
                return "10924051000163"; ;
            case 2:
                return "20907518000110";
            case 3:
                return "22009513000104";
            case 4:
                return "22078196000170";
            default:
                return "23549499000196";
        }
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            int idNotaFiscal = Convert.ToInt32(GridView1.DataKeys[e.Row.RowIndex].Values[1]);
            string numeroNota = GridView1.DataKeys[e.Row.RowIndex].Values[0].ToString();
            string statusInvoicy = rnNotaFiscal.retornaInformacaoNotaPorNumeroNota(numeroNota, idNotaFiscal);

            string linkdanfe = "";
            try { linkdanfe = GridView1.DataKeys[e.Row.RowIndex].Values["linkdanfe"].ToString(); }
            catch (Exception) { }
            if (linkdanfe == "")
                linkdanfe = rnNotaFiscal.retornaDanfeNotaPorNumeroNota(numeroNota.ToString(),
                      idNotaFiscal);


            Label lblStatusInvoicy = (Label)e.Row.FindControl("lblStatusInvoicy");
            HyperLink hplLinkDanfe = (HyperLink)e.Row.FindControl("hplLinkDanfe");

            if (linkdanfe.IndexOf("https://app.invoicy.com.br:443///downloadpdf") > -1)
            {
                hplLinkDanfe.NavigateUrl = linkdanfe;
                hplLinkDanfe.Visible = true;
            }

            if (!String.IsNullOrEmpty(statusInvoicy))
                lblStatusInvoicy.Text = statusInvoicy;
            else
                lblStatusInvoicy.Text = "Nota não foi processada no Invoicy";

            if (statusInvoicy.Trim().ToLower() == "autorizado o uso da nf-e")
            {
                lblStatusInvoicy.ForeColor = System.Drawing.Color.Green;
            }
            else
            {
                hplLinkDanfe.Visible = false;
                lblStatusInvoicy.ForeColor = System.Drawing.Color.Red;
            }
        }
    }

    protected void btConsultarUF_Click(object sender, EventArgs e)
    {
        int numeroNota = Convert.ToInt32(txtNfe.Text);
        using (var data = new dbCommerceDataContext())
        {
            var UF = (from nf in data.tbNotaFiscals
                      join pe in data.tbPedidos on nf.idPedido equals pe.pedidoId
                      join cli in data.tbClientes on pe.clienteId equals cli.clienteId
                      where nf.numeroNota == numeroNota
                      select new
                      {
                          cli.clienteEstado

                      }
                      ).FirstOrDefault();
            if (UF != null)
                lblUF.Text = "UF do cliente: <strong>" + UF.clienteEstado;
            else
                lblUF.Text = "NFe não localizada";
        }

    }
}