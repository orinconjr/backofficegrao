﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ajustecielo : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        

        var data = new dbCommerceDataContext();
        /*
        var dataFiltro = Convert.ToDateTime("04/06/2014");
        var pedidos = (from c in data.tbPedidos
            where
                c.dataConfirmacaoPagamento < dataFiltro |
                (c.dataConfirmacaoPagamento == null && c.dataHoraDoPedido < dataFiltro)
            select c);
        foreach (var pedido in pedidos)
        {
            var datamod = new dbCommerceDataContext();
            var pagamentos = (from c in datamod.tbPedidoPagamentos where c.idOperadorPagamento == 4 && c.pedidoId == pedido.pedidoId select c);
            foreach (var pagamento in pagamentos)
            {
                pagamento.idOperadorPagamento = 3;
            }
            datamod.SubmitChanges();
            datamod.Dispose();
        }*/
        var pagamentos = (from c in data.tbPedidoPagamentos where c.idOperadorPagamento == 1 && c.pago == false && c.cancelado == false select c);
        foreach (var pagamento in pagamentos)
        {
            try
            {
                var outrosPagamentos = (from c in data.tbPedidoPagamentos where c.pedidoId == pagamento.pedidoId select c).Count();
                var status = pagamento.tbPedido.statusDoPedido;
                if ((status == 3 | status == 4 | status == 5 | status == 9 | status == 11) && outrosPagamentos == 1)
                {
                    pagamento.pago = true;
                    data.SubmitChanges();
                }
            }
            catch (Exception)
            {
                //Testar sync
            }
        }

        //Teste no branch
    }
}