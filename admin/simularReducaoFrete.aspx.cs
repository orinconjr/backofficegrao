﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class simularReducaoFrete : System.Web.UI.Page
{
    public class dadosSimulacao
    {
        public string servico { get; set; }
        public decimal valor { get; set; }
        public decimal quantidade { get; set; }
        public int mes { get; set; }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["pass"] != "glmp") return;
        if (Request.QueryString["servico"] != null)
        {
            gerarSimulacaoEspecifica(Request.QueryString["servico"]);
        }
        else
        {
            gerarSimulacaoGeral();
        }
        /*List < dadosSimulacao > valoresTotais = new List<dadosSimulacao>();
        List<dadosSimulacao> valoresAntes = new List<dadosSimulacao>();


        foreach (var envio in envios)
        {
            var menorPreco = precos.Where(x => listaServicos.Contains(x.servico) && x.valor > 0).OrderBy(x => x.valor).FirstOrDefault();
            var menorPrecoAnterior = precos.Where(x => listaServicosAnteriores.Contains(x.servico) && x.valor > 0).OrderBy(x => x.valor).FirstOrDefault();
            if (menorPreco != null && menorPrecoAnterior != null)
            {
                var valorTotalAdicionar = valoresTotais.FirstOrDefault(x => x.mes == (envio.dataFimEmbalagem ?? DateTime.Now).Month && x.servico == menorPreco.servico);
                if (valorTotalAdicionar != null)
                {
                    valorTotalAdicionar.quantidade++;
                    valorTotalAdicionar.valor += menorPreco.valor;
                }
                else
                {
                    valoresTotais.Add(new dadosSimulacao()
                    {
                        mes = (envio.dataFimEmbalagem ?? DateTime.Now).Month,
                        quantidade = 1,
                        servico = menorPreco.servico,
                        valor = menorPreco.valor
                    });
                }

                var valorAnteriordicionar = valoresAntes.FirstOrDefault(x => x.mes == (envio.dataFimEmbalagem ?? DateTime.Now).Month && x.servico == menorPrecoAnterior.servico);
                if (valorAnteriordicionar != null)
                {
                    valorAnteriordicionar.quantidade++;
                    valorAnteriordicionar.valor += menorPrecoAnterior.valor;
                }
                else
                {
                    valoresAntes.Add(new dadosSimulacao()
                    {
                        mes = (envio.dataFimEmbalagem ?? DateTime.Now).Month,
                        quantidade = 1,
                        servico = menorPrecoAnterior.servico,
                        valor = menorPrecoAnterior.valor
                    });
                }
            }
        }

        foreach (var mes in valoresTotais.Select(x => x.mes).Distinct().OrderBy(x => x))
        {
            Response.Write("Mes: " + mes + "<br>");
            decimal total = 0;
            foreach (var servico in valoresTotais.Where(x => x.mes == mes))
            {
                Response.Write("Transportadora: " + servico.servico + "<br>");
                Response.Write("Valor: " + servico.valor.ToString("C") + "<br>");
                Response.Write("Quantidade: " + servico.quantidade.ToString() + "<br>");
                total += servico.valor;
            }
            Response.Write("Total:" + total.ToString("C") + "<br>");
        }


        Response.Write("<br><br><br>");
        foreach (var mes in valoresAntes.Select(x => x.mes).Distinct().OrderBy(x => x))
        {
            Response.Write("Mes: " + mes + "<br>");
            decimal total = 0;
            foreach (var servico in valoresAntes.Where(x => x.mes == mes))
            {
                Response.Write("Transportadora: " + servico.servico + "<br>");
                Response.Write("Valor: " + servico.valor.ToString("C") + "<br>");
                Response.Write("Quantidade: " + servico.quantidade.ToString() + "<br>");
                total += servico.valor;
            }
            Response.Write("Total:" + total.ToString("C") + "<br>");
        }*/

        // gerarSimulacaoGeral();
    }

    private void gerarSimulacaoGeral()
    {

        var data = new dbCommerceDataContext();
        var dataInicio1 = Convert.ToDateTime("01/01/2018");
        var dataFim1 = Convert.ToDateTime("01/04/2018");
        var dataInicio2 = Convert.ToDateTime("01/01/2018");
        var dataFim2 = Convert.ToDateTime("01/02/2018");
        var envios = (from c in data.tbPedidoEnvios
                      where (c.dataFimEmbalagem >= dataInicio1 && c.dataFimEmbalagem < dataFim1) | (c.dataFimEmbalagem >= dataInicio2 && c.dataFimEmbalagem < dataFim2)
                      select new
                      {
                          c.idPedidoEnvio,
                          c.idPedido,
                          c.dataFimEmbalagem
                      }).ToList();
        var precosList = (from c in data.tbPedidoEnvioValorTransportes
                          where ((c.tbPedidoEnvio.dataFimEmbalagem >= dataInicio1 && c.tbPedidoEnvio.dataFimEmbalagem < dataFim1) | (c.tbPedidoEnvio.dataFimEmbalagem >= dataInicio2 && c.tbPedidoEnvio.dataFimEmbalagem < dataFim2)) && c.valor > 0
                          select new
                          {
                              c.idPedidoEnvio,
                              c.idPedidoEnvioValorTransporte,
                              c.servico,
                              c.valor,
                              c.prazo,
                              c.tbPedidoEnvio.tbPedido.endCep
                          }).ToList();
        var pedidos = (from c in data.tbPedidoEnvios
                       where (c.dataFimEmbalagem >= dataInicio1 && c.dataFimEmbalagem < dataFim1) | (c.dataFimEmbalagem >= dataInicio2 && c.dataFimEmbalagem < dataFim2)
                       select new
                       {
                           c.idPedido,
                           c.tbPedido.endCep
                       }).ToList();


        var precos = (from c in precosList
                      select new
                      {
                          valor = c.valor,
                          servico = c.servico,
                          idPedidoEnvioValorTransporte = c.idPedidoEnvioValorTransporte,
                          idPedidoEnvio = c.idPedidoEnvio,
                          prazo = c.prazo
                      }).ToList();
        
        var listaServicos = new List<string>();
        listaServicos.Add("jadlog");
        listaServicos.Add("tnt");
        listaServicos.Add("plimor");
        listaServicos.Add("pac");
        listaServicos.Add("nowlog");
        listaServicos.Add("dialogo");
        listaServicos.Add("exlog");
        listaServicos.Add("jamef");
        listaServicos.Add("lbr");
        listaServicos.Add("loggi");
        listaServicos.Add("transfolha");
        listaServicos.Add("totalexpress");
        listaServicos.Add("tntibitinga");

        var listaServicosAnteriores = new List<string>();
        listaServicosAnteriores.Add("jadlog");
        listaServicosAnteriores.Add("tnt");
        listaServicosAnteriores.Add("plimor");
        listaServicosAnteriores.Add("pac");

        var valoresSimulacao = (from c in envios
                                join d in precos on c.idPedidoEnvio equals d.idPedidoEnvio into precosGroup
                                select new
                                {
                                    c.idPedidoEnvio,
                                    mes = (c.dataFimEmbalagem ?? DateTime.Now).Month,
                                    menorServico = precosGroup.Where(x => listaServicos.Contains(x.servico)).Any() ? precosGroup.Where(x => listaServicos.Contains(x.servico)).OrderBy(x => x.valor).First().servico : "outros",
                                    menorValor = precosGroup.Where(x => listaServicos.Contains(x.servico)).Any() ? precosGroup.Where(x => listaServicos.Contains(x.servico)).OrderBy(x => x.valor).First().valor : 0,
                                    menorServicoAntes = precosGroup.Where(x => listaServicosAnteriores.Contains(x.servico)).Any() ? precosGroup.Where(x => listaServicosAnteriores.Contains(x.servico)).OrderBy(x => x.valor).First().servico : "outros",
                                    menorValorAntes = precosGroup.Where(x => listaServicosAnteriores.Contains(x.servico)).Any() ? precosGroup.Where(x => listaServicosAnteriores.Contains(x.servico)).OrderBy(x => x.valor).First().valor : 0
                                }).ToList();

        valoresSimulacao = valoresSimulacao.Where(x => x.menorValor > 0 && x.menorValorAntes > 0).ToList();

        foreach (var mes in valoresSimulacao.Select(x => x.mes).Distinct().OrderBy(x => x))
        {
            Response.Write("Mes: " + mes + "<br>");
            decimal total = 0;
            decimal totalAntes = 0;
            Response.Write("<b>Nova simulacao</b><br>");
            foreach (var servico in valoresSimulacao.
                                       GroupBy(hit => hit.menorServico).
                                       Select(group => new
                                       {
                                           ItemID = group.Key,
                                           menorValor = group.Sum(hit => hit.menorValor)
                                       }).
                                       OrderByDescending(hit => hit.menorValor).Select(x => x.ItemID).Distinct())
            {
                Response.Write("Transportadora: " + servico + "<br>");
                Response.Write("Valor: " + valoresSimulacao.Where(x => x.menorServico == servico && x.mes == mes).Sum(x => x.menorValor).ToString("C") + "<br>");
                Response.Write("Quantidade: " + valoresSimulacao.Where(x => x.menorServico == servico && x.mes == mes).Count().ToString() + "<br>");
                var totalTransportadoraAtual = valoresSimulacao.Where(x => x.menorServico == servico && x.mes == mes).Sum(x => x.menorValor);
                total += totalTransportadoraAtual;
            }

            Response.Write("<b>Anterior simulacao</b><br>");
            foreach (var servico in valoresSimulacao.
                                       GroupBy(hit => hit.menorServicoAntes).
                                       Select(group => new
                                       {
                                           ItemID = group.Key,
                                           menorValor = group.Sum(hit => hit.menorValorAntes)
                                       }).
                                       OrderByDescending(hit => hit.menorValor).Select(x => x.ItemID).Distinct())
            {
                Response.Write("Transportadora: " + servico + "<br>");
                Response.Write("Valor: " + valoresSimulacao.Where(x => x.menorServicoAntes == servico && x.mes == mes).Sum(x => x.menorValorAntes).ToString("C") + "<br>");
                Response.Write("Quantidade: " + valoresSimulacao.Where(x => x.menorServicoAntes == servico && x.mes == mes).Count().ToString() + "<br>");
                totalAntes += valoresSimulacao.Where(x => x.menorServicoAntes == servico && x.mes == mes).Sum(x => x.menorValorAntes);
            }
            Response.Write("Redução:" + (100 - ((total * 100) / totalAntes)).ToString("0.00") + "%<br>");
            Response.Write("Reduçao:" + (totalAntes - total).ToString("C") + "<br>");
            Response.Write("Total agora:" + total.ToString("C") + "<br>");
            Response.Write("Total antes:" + totalAntes.ToString("C") + "<br><br><br>");
        }
    }


    private void gerarSimulacaoEspecifica(string servicoSimulado)
    {
        if (Request.QueryString["pass"] != "glmp") return;

        var data = new dbCommerceDataContext();
        var dataInicio1 = Convert.ToDateTime("01/01/2018");
        var dataFim1 = Convert.ToDateTime("01/04/2018");
        var dataInicio2 = Convert.ToDateTime("01/01/2018");
        var dataFim2 = Convert.ToDateTime("01/02/2018");
        var envios = (from c in data.tbPedidoEnvios
                      where (c.dataFimEmbalagem >= dataInicio1 && c.dataFimEmbalagem < dataFim1) | (c.dataFimEmbalagem >= dataInicio2 && c.dataFimEmbalagem < dataFim2)
                      select new
                      {
                          c.idPedidoEnvio,
                          c.idPedido,
                          c.dataFimEmbalagem,
                          c.nfeValor
                      }).ToList();
        var pacotesList = (from c in data.tbPedidoPacotes
                      where (c.tbPedidoEnvio.dataFimEmbalagem >= dataInicio1 && c.tbPedidoEnvio.dataFimEmbalagem < dataFim1) | (c.tbPedidoEnvio.dataFimEmbalagem >= dataInicio2 && c.tbPedidoEnvio.dataFimEmbalagem < dataFim2)
                      select new
                      {
                          c.idPedidoEnvio,
                          c.idPedido,
                          c.peso,
                          cubagem = c.largura * c.altura * c.profundidade
                      }).ToList();
        var precosLista = (from c in data.tbPedidoEnvioValorTransportes
                           where ((c.tbPedidoEnvio.dataFimEmbalagem >= dataInicio1 && c.tbPedidoEnvio.dataFimEmbalagem < dataFim1) | (c.tbPedidoEnvio.dataFimEmbalagem >= dataInicio2 && c.tbPedidoEnvio.dataFimEmbalagem < dataFim2)) && c.valor > 0
                           select new
                           {
                               c.idPedidoEnvio,
                               c.idPedidoEnvioValorTransporte,
                               c.servico,
                               c.valor,
                               c.prazo,
                               c.tbPedidoEnvio.tbPedido.endCep,
                               c.tbPedidoEnvio.nfeValor
                           }).ToList();
        var precosList = (from c in precosLista
                          join d in pacotesList on c.idPedidoEnvio equals d.idPedidoEnvio into pacotes
                          where c.valor > 0
                          select new
                          {
                              c.idPedidoEnvio,
                              c.idPedidoEnvioValorTransporte,
                              c.servico,
                              c.valor,
                              c.prazo,
                              c.endCep,
                              peso = pacotes.Sum(x => x.peso),
                              c.nfeValor,
                              cubagem = pacotes.Sum(x => x.cubagem)
                          }).ToList();
        var pedidos = (from c in data.tbPedidoEnvios
                       where (c.dataFimEmbalagem >= dataInicio1 && c.dataFimEmbalagem < dataFim1) | (c.dataFimEmbalagem >= dataInicio2 && c.dataFimEmbalagem < dataFim2)
                       select new
                       {
                           c.idPedido,
                           c.tbPedido.endCep
                       }).ToList();


        var precos = (from c in precosList
                      select new
                      {
                          valor = c.valor,
                          servico = c.servico,
                          idPedidoEnvioValorTransporte = c.idPedidoEnvioValorTransporte,
                          idPedidoEnvio = c.idPedidoEnvio,
                          prazo = c.prazo,
                          c.endCep,
                          c.peso,
                          c.nfeValor,
                          c.cubagem
                      }).ToList();

        var precosListPac5 = (from c in precosList
                              select new
                              {
                                  valor = c.servico == "pac" ? Convert.ToInt64(c.endCep.Replace("-", "")) >= 01000000 && Convert.ToInt64(c.endCep.Replace("-", "")) <= 19999999 ? (c.valor * 0.90M) : (c.valor * 1M) : c.valor,
                                  servico = "pac3",
                                  idPedidoEnvioValorTransporte = c.idPedidoEnvioValorTransporte,
                                  idPedidoEnvio = c.idPedidoEnvio,
                                  prazo = c.prazo,
                                  c.endCep,
                                  c.peso,
                                  c.nfeValor,
                                  c.cubagem
                              }).ToList();

        precos = precos.Union(precosListPac5).ToList();
        var listaServicos = new List<string>();
        listaServicos.Add("jadlog");
        listaServicos.Add("tnt");
        listaServicos.Add("plimor");
        listaServicos.Add("pac3");
        listaServicos.Add("nowlog");
        listaServicos.Add("dialogo");
        listaServicos.Add("exlog");
        listaServicos.Add("jamef");
        listaServicos.Add("lbr");
        listaServicos.Add("loggi");
        listaServicos.Add("transfolha");
        listaServicos.Add("totalexpress");

        var listaServicosAnteriores = new List<string>();
        listaServicosAnteriores.Add("jadlog");
        listaServicosAnteriores.Add("tnt");
        listaServicosAnteriores.Add("plimor");
        listaServicosAnteriores.Add("pac");

        var valoresSimulacao = (from c in envios
                                join d in precos on c.idPedidoEnvio equals d.idPedidoEnvio into precosGroup
                                select new
                                {
                                    c.idPedidoEnvio,
                                    endCep = precosGroup.FirstOrDefault() == null ? "" : precosGroup.FirstOrDefault().endCep,
                                    peso = precosGroup.FirstOrDefault() == null ? 0 : precosGroup.FirstOrDefault().peso,
                                    nfeValor = precosGroup.FirstOrDefault() == null ? 0 : precosGroup.FirstOrDefault().nfeValor,
                                    cubagem = precosGroup.FirstOrDefault() == null ? 0 : precosGroup.FirstOrDefault().cubagem,
                                    mes = (c.dataFimEmbalagem ?? DateTime.Now).Month,
                                    menorServico = precosGroup.Where(x => listaServicos.Contains(x.servico)).Any() ? precosGroup.Where(x => listaServicos.Contains(x.servico)).OrderBy(x => x.valor).First().servico : "outros",
                                    menorValor = precosGroup.Where(x => listaServicos.Contains(x.servico)).Any() ? precosGroup.Where(x => listaServicos.Contains(x.servico)).OrderBy(x => x.valor).First().valor : 0,
                                    menorServicoSimulado = servicoSimulado,
                                    menorValorSimulado = precosGroup.Where(x => x.servico == servicoSimulado).Any() ? precosGroup.Where(x => x.servico == servicoSimulado).OrderBy(x => x.valor).First().valor : 0
                                }).ToList();
        valoresSimulacao = valoresSimulacao.Where(x => x.menorValor > 0 && x.menorValorSimulado > 0).ToList();

        valoresSimulacao = (from c in valoresSimulacao
                            select new
                            {
                                c.idPedidoEnvio,
                                c.endCep,
                                c.peso,
                                c.nfeValor,
                                c.cubagem,
                                c.mes,
                                c.menorServico,
                                menorValor = c.menorServico == servicoSimulado ? c.menorValor : c.menorValor * 0.95m,
                                c.menorServicoSimulado,
                                c.menorValorSimulado
                            }).ToList();
        valoresSimulacao = valoresSimulacao.Where(x => x.menorValorSimulado > 0).ToList();
        Response.Write("envio;cep;peso;cubagem;nfeValor;vencedora;valor;simulada;valorsimulada;<br>");

        foreach (var valorSimulacao in valoresSimulacao)
        {
            Response.Write(valorSimulacao.idPedidoEnvio + ";" + valorSimulacao.endCep + ";" + valorSimulacao.peso + ";" + valorSimulacao.cubagem + ";" + (valorSimulacao.nfeValor ?? 0).ToString("0.00") + ";" + valorSimulacao.menorServico + ";" + valorSimulacao.menorValor.ToString("0.00") + ";" + valorSimulacao.menorServicoSimulado + ";" + valorSimulacao.menorValorSimulado.ToString("0.00") + "<br>");
        }
        /*foreach (var mes in valoresSimulacao.Select(x => x.mes).Distinct().OrderBy(x => x))
        {
            Response.Write("Mes: " + mes + "<br>");
            decimal total = 0;
            decimal totalAntes = 0;
            Response.Write("<b>Nova simulacao</b><br>");
            foreach (var servico in valoresSimulacao.
                                       GroupBy(hit => hit.menorServico).
                                       Select(group => new
                                       {
                                           ItemID = group.Key,
                                           menorValor = group.Sum(hit => hit.menorValor)
                                       }).
                                       OrderByDescending(hit => hit.menorValor).Select(x => x.ItemID).Distinct())
            {
                Response.Write("Transportadora: " + servico + "<br>");
                Response.Write("Valor: " + valoresSimulacao.Where(x => x.menorServico == servico && x.mes == mes).Sum(x => x.menorValor).ToString("C") + "<br>");
                Response.Write("Quantidade: " + valoresSimulacao.Where(x => x.menorServico == servico && x.mes == mes).Count().ToString() + "<br>");
                total += valoresSimulacao.Where(x => x.menorServico == servico && x.mes == mes).Sum(x => x.menorValor);
            }

            Response.Write("<b>Anterior simulacao</b><br>");
            foreach (var servico in valoresSimulacao.
                                       GroupBy(hit => hit.menorServicoAntes).
                                       Select(group => new
                                       {
                                           ItemID = group.Key,
                                           menorValor = group.Sum(hit => hit.menorValorAntes)
                                       }).
                                       OrderByDescending(hit => hit.menorValor).Select(x => x.ItemID).Distinct())
            {
                Response.Write("Transportadora: " + servico + "<br>");
                Response.Write("Valor: " + valoresSimulacao.Where(x => x.menorServicoAntes == servico && x.mes == mes).Sum(x => x.menorValorAntes).ToString("C") + "<br>");
                Response.Write("Quantidade: " + valoresSimulacao.Where(x => x.menorServicoAntes == servico && x.mes == mes).Count().ToString() + "<br>");
                totalAntes += valoresSimulacao.Where(x => x.menorServicoAntes == servico && x.mes == mes).Sum(x => x.menorValorAntes);
            }
            Response.Write("Redução:" + (100 - ((total * 100) / totalAntes)).ToString("0.00") + "%<br>");
            Response.Write("Reduçao:" + (totalAntes - total).ToString("C") + "<br>");
            Response.Write("Total agora:" + total.ToString("C") + "<br>");
            Response.Write("Total antes:" + totalAntes.ToString("C") + "<br><br><br>");
        }*/
    }
}