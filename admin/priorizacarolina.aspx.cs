﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class priorizacarolina : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        var data = new dbCommerceDataContext();
        var itensCarolina = (from c in data.tbItemPedidoEstoques
            where
                c.cancelado == false && c.reservado == true && c.enviado == false && c.dataLimite != null &&
                c.tbProduto.produtoFornecedor == 72 && c.tbItensPedido.tbPedido.statusDoPedido == 11
            select c.tbItensPedido.pedidoId).ToList().Distinct();
        var itensCarolinaTeste = itensCarolina;
        foreach (var itemCarolina in itensCarolinaTeste)
        {
            var log = new rnLog();
            log.usuario = rnUsuarios.retornaNomeUsuarioLogado();
            log.descricoes.Add("Atualizando prazos carolina");
            var tipoRelacionado = new rnEnums.TipoRelacionadoIds() { idRegistroRelacionado = itemCarolina, tipoRelacionado = rnEnums.TipoRegistroRelacionado.Pedido };
            log.tiposRelacionados.Add(tipoRelacionado);


            var produtosAguardando = (from c in data.tbItemPedidoEstoques
                where c.tbItensPedido.pedidoId == itemCarolina &&
                      c.cancelado == false &&
                      c.reservado == false &&
                      c.enviado == false && c.tbProduto.produtoFornecedor != 72 && c.dataLimite != null
                select c).ToList();
            Response.Write("Pedido " + itemCarolina + "<br>");
            foreach (var produtoAguardando in produtosAguardando)
            {
                string saida = "Produto: " + produtoAguardando.produtoId + " - Limite atual: " +
                               produtoAguardando.dataLimite.Value.ToShortDateString();

                var itensFornecedor = (from c in data.tbPedidoFornecedorItems
                                       join d in data.tbProdutoFornecedors on c.tbPedidoFornecedor.idFornecedor equals d.fornecedorId
                                       where c.entregue == false | (c.entregue == true && (c.liberado ?? false) == false) && c.idProduto == produtoAguardando.produtoId
                                       select new { c.idProduto, c.idPedidoFornecedorItem, c.tbPedidoFornecedor.dataLimite, c.tbPedidoFornecedor.idPedidoFornecedor, c.entregue, d.fornecedorNome }).ToList().OrderBy(x => x.dataLimite).ThenBy(x => x.idPedidoFornecedor).ToList();
                var itensAguardandoEstoque =
                    (from c in data.tbItemPedidoEstoques
                     where c.cancelado == false && c.enviado == false && c.reservado == false && c.dataLimite != null && c.produtoId == produtoAguardando.produtoId
                     orderby c.dataLimite
                     select new
                    {
                        c.idItemPedidoEstoque,
                        c.tbItensPedido.pedidoId,
                        c.dataCriacao,
                        dataLimiteReserva = c.dataLimite,
                        idProduto = c.produtoId,
                        idItemPedido = c.itemPedidoId
                    }).ToList();


                var listaFinal = (from c in itensAguardandoEstoque.Select((item, index) => new { item, index })
                                  join d in itensFornecedor.Select((item, index) => new { item, index }) on c.index equals d.index into
                                      pedidosFornecedorListaInterna
                                  from f in pedidosFornecedorListaInterna.DefaultIfEmpty()
                                  select new
                                  {
                                      c.item.idItemPedidoEstoque,
                                      c.item.pedidoId,
                                      c.item.dataCriacao,
                                      c.item.dataLimiteReserva,
                                      c.item.idItemPedido,
                                      entregue = f != null ? f.item.entregue : false,
                                      idPedidoFornecedor = f != null ? f.item.idPedidoFornecedor : 99,
                                      fornecedorNome = f != null ? f.item.fornecedorNome : "",
                                      dataLimiteFornecedor = f != null ? (DateTime?)f.item.dataLimite : null,
                                      idPedidoFornecedorItem = f != null ? (int?)f.item.idPedidoFornecedorItem : null,
                                  }
                    ).ToList();

                var contagemItensAtrasados =
                    listaFinal.Where(
                        x => (x.dataLimiteFornecedor ?? DateTime.Now.AddDays(300)) > x.dataLimiteReserva.Value.AddDays(1))
                        .Count();
                bool maiorAtraso = true;
                int indiceModificacao = 0;
                var indiceDoItem = listaFinal.FindIndex(x => x.idItemPedidoEstoque == produtoAguardando.idItemPedidoEstoque);
                var itemIndiceAtual = listaFinal[indiceDoItem];

                var dataLimiteAlterada = (itemIndiceAtual.dataLimiteFornecedor ?? produtoAguardando.dataLimite);

                while (maiorAtraso)
                {
                    var itemModificacao = listaFinal[indiceModificacao];
                    var dataItemModificado = itemModificacao.dataLimiteReserva.Value.AddMinutes(-1);
                    dataLimiteAlterada = dataItemModificado;
                    var itensAguardandoEstoqueModificado =
                    (from c in itensAguardandoEstoque
                     orderby c.dataLimiteReserva
                     select new
                     {
                         c.idItemPedidoEstoque,
                         c.pedidoId,
                         c.dataCriacao,
                         dataLimiteReserva = (c.idItemPedidoEstoque == produtoAguardando.idItemPedidoEstoque ? dataItemModificado : c.dataLimiteReserva),
                         c.idProduto,
                         c.idItemPedido
                     }).ToList().OrderBy(x => x.dataLimiteReserva);

                    var listaFinalModificado = (from c in itensAguardandoEstoqueModificado.Select((item, index) => new { item, index })
                                      join d in itensFornecedor.Select((item, index) => new { item, index }) on c.index equals d.index into
                                          pedidosFornecedorListaInterna
                                      from f in pedidosFornecedorListaInterna.DefaultIfEmpty()
                                      select new
                                      {
                                          c.item.idItemPedidoEstoque,
                                          c.item.pedidoId,
                                          c.item.dataCriacao,
                                          c.item.dataLimiteReserva,
                                          c.item.idItemPedido,
                                          entregue = f != null ? f.item.entregue : false,
                                          idPedidoFornecedor = f != null ? f.item.idPedidoFornecedor : 99,
                                          fornecedorNome = f != null ? f.item.fornecedorNome : "",
                                          dataLimiteFornecedor = f != null ? (DateTime?)f.item.dataLimite : null,
                                          idPedidoFornecedorItem = f != null ? (int?)f.item.idPedidoFornecedorItem : null,
                                      }
                        ).ToList();

                    var contagemItensAtrasadosModificado =
                                        listaFinalModificado.Where(
                                            x => (x.dataLimiteFornecedor ?? DateTime.Now.AddDays(300)) > x.dataLimiteReserva.Value.AddDays(1))
                                            .Count();

                    if (contagemItensAtrasadosModificado <= contagemItensAtrasados | indiceDoItem == indiceModificacao)
                    {
                        maiorAtraso = false;
                    }
                    else
                    {
                        indiceModificacao++;
                    }
                }
                saida += " - Indice Inicial: " + indiceDoItem + " - Indice Modificado: " + indiceModificacao + " - Data Limite Nova: " + dataLimiteAlterada.Value.ToShortDateString();
                if (dataLimiteAlterada < produtoAguardando.dataLimite)
                {
                    log.descricoes.Add(saida);
                    saida = "Menor - " + saida;
                    var dataAlterar = new dbCommerceDataContext();
                    var itemPedidoAltera =
                        (from c in dataAlterar.tbItemPedidoEstoques
                         where c.idItemPedidoEstoque == produtoAguardando.idItemPedidoEstoque
                         select c).First();
                    itemPedidoAltera.dataLimite = dataLimiteAlterada;
                    dataAlterar.SubmitChanges();
                }
                Response.Write(saida + "<br>");
            }

            log.InsereLog();

        }
    }
}