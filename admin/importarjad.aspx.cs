﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxCallback;
using SpreadsheetLight;

public partial class importarjad : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if(Request.QueryString["simular"] != null)
        {
            var dataEnvios = new dbCommerceDataContext("Data Source=graodegente.cl6j8caodqlm.sa-east-1.rds.amazonaws.com;Initial Catalog=graodegente;User ID=graodegente;Password=Bark152029;Min Pool Size=10; Max Pool Size=1000");
            var envios = (from c in dataEnvios.tbPedidoEnvios where c.formaDeEnvio != "" && c.dataFimEmbalagem > Convert.ToDateTime("01/10/2016") && c.dataFimEmbalagem < Convert.ToDateTime("01/11/2016") select c).ToList();
            var valores = (from c in dataEnvios.tbPedidoEnvioValorTransportes where c.tbPedidoEnvio.formaDeEnvio != "" && c.tbPedidoEnvio.dataFimEmbalagem > Convert.ToDateTime("01/10/2016") && c.tbPedidoEnvio.dataFimEmbalagem < Convert.ToDateTime("01/11/2016") select c).ToList();
            decimal tnt = 0;
            decimal tntvolumes = 0;

            decimal jadlog = 0;
            decimal jadlogvolumes = 0;

            decimal belle = 0;
            decimal bellevolumes = 0;

            decimal plimor = 0;
            decimal plimorvolumes = 0;

            System.Threading.Tasks.Parallel.ForEach(envios, (envio) =>
            {
                var valorSelecionado = valores.OrderBy(x => x.valor).Where(x => x.idPedidoEnvio == envio.idPedidoEnvio && x.valor > 0 && x.servico != "tnt" && x.servico != "belle").FirstOrDefault();
                if (valorSelecionado != null)
                {
                    if (valorSelecionado.servico == "tnt_novo")
                    {
                        tnt += valorSelecionado.valor;
                        tntvolumes += 1;
                    }
                    else if (valorSelecionado.servico == "jadlog")
                    {
                        jadlog += valorSelecionado.valor;
                        jadlogvolumes += 1;
                    }
                    else if (valorSelecionado.servico == "belle")
                    {
                        belle += valorSelecionado.valor;
                        bellevolumes += 1;
                    }
                    else if (valorSelecionado.servico == "plimor")
                    {
                        plimor += valorSelecionado.valor;
                        plimorvolumes += 1;
                    }
                }
            });

            /*foreach (var envio in envios)
            {
                var valorSelecionado = valores.OrderBy(x => x.valor).Where(x => x.idPedidoEnvio == envio.idPedidoEnvio && x.valor > 0 && x.servico != "tnt").FirstOrDefault();
                if(valorSelecionado != null)
                {
                    if(valorSelecionado.servico == "tnt_novo")
                    {
                        tnt += valorSelecionado.valor;
                        tntvolumes += 1;
                    }
                    else if (valorSelecionado.servico == "jadlog")
                    {
                        jadlog += valorSelecionado.valor;
                        jadlogvolumes += 1;
                    }
                    else if (valorSelecionado.servico == "belle")
                    {
                        belle += valorSelecionado.valor;
                        bellevolumes += 1;
                    }
                }
            }*/

            Response.Write("Tnt: " + tnt.ToString("C") + "<br>");
            Response.Write("Tnt envios: " + tntvolumes.ToString() + "<br>");

            Response.Write("jadlog: " + jadlog.ToString("C") + "<br>");
            Response.Write("jadlog envios: " + jadlogvolumes.ToString() + "<br>");

            Response.Write("belle: " + belle.ToString("C") + "<br>");
            Response.Write("belle envios: " + bellevolumes.ToString() + "<br>");

            Response.Write("plimor: " + plimor.ToString("C") + "<br>");
            Response.Write("plimor envios: " + plimorvolumes.ToString() + "<br>");
        }
    }

    protected void btnSalvar_Click(object sender, ImageClickEventArgs e)
    {
        string caminho = MapPath("~/");
        //fluArquivo.SaveAs(caminho + "totalexpress.xls");

        string lastCepInicial = "";
        string lastCepFinal = "";
        int idTabelaPreco = 0;

        var data = new dbCommerceDataContext();
        
        using (SLDocument sl = new SLDocument("C:\\planilhas\\tabelajad.xlsx"))
        {
            SLWorksheetStatistics stats = sl.GetWorksheetStatistics();
            int iStartColumnIndex = stats.StartColumnIndex;
            for (int row = stats.StartRowIndex + 1; row <= stats.EndRowIndex; ++row)
            {
                /*string cidade = sl.GetCellValueAsString(row, 1);
                string cepInicial = sl.GetCellValueAsString(row, 2);
                string cepFinal = sl.GetCellValueAsString(row, 3);
                string uf = sl.GetCellValueAsString(row, 4);
                string rota = sl.GetCellValueAsString(row, 5);

                string tabela = (uf.Trim() + " - " + rota.Trim()).ToLower();
                var tabelaPreco = (from c in data.tbTipoDeEntregaTabelaPrecos where c.idTipoDeEntrega == 19 && c.nome.ToLower() == tabela select c).FirstOrDefault();
                if(tabelaPreco != null)
                {
                    var faixa = new tbFaixaDeCep() {
                        ativo = true,
                        dataDaCriacao = DateTime.Now,
                        faixaDeCepDescricao = cidade.Trim(),
                        faixaInicial = cepInicial.Trim(),
                        faixaFinal = cepFinal.Trim(),
                        idTipoDeEntregaTabelaPreco = tabelaPreco.idTipoDeEntregaTabelaPreco,
                        interiorizacao = rota.ToLower() == "interior",
                        porcentagemDeDesconto = 0,
                        porcentagemDoSeguro = 0,
                        prazo = 3,
                        tipodeEntregaId = 19
                    };
                    data.tbFaixaDeCeps.InsertOnSubmit(faixa);
                    data.SubmitChanges();
                }*/

                string uf = sl.GetCellValueAsString(row, 1);
                string tarifa = sl.GetCellValueAsString(row, 2);
                string f1 = sl.GetCellValueAsString(row, 3).Replace(".", ",");
                string f2 = sl.GetCellValueAsString(row, 4).Replace(".", ",");
                string f3 = sl.GetCellValueAsString(row, 5).Replace(".", ",");
                string f4 = sl.GetCellValueAsString(row, 6).Replace(".", ",");
                string f5 = sl.GetCellValueAsString(row, 7).Replace(".", ",");
                string f6 = sl.GetCellValueAsString(row, 8).Replace(".", ",");
                string f7 = sl.GetCellValueAsString(row, 9).Replace(".", ",");
                string f8 = sl.GetCellValueAsString(row, 10).Replace(".", ",");
                string f9 = sl.GetCellValueAsString(row, 11).Replace(".", ",");
                string f10 = sl.GetCellValueAsString(row, 12).Replace(".", ",");
                string f11 = sl.GetCellValueAsString(row, 13).Replace(".", ",");
                string f12 = sl.GetCellValueAsString(row, 14).Replace(".", ",");
                string f13 = sl.GetCellValueAsString(row, 15).Replace(".", ",");
                string f14 = sl.GetCellValueAsString(row, 16).Replace(".", ",");
                string f15 = sl.GetCellValueAsString(row, 17).Replace(".", ",");
                string f16 = sl.GetCellValueAsString(row, 18).Replace(".", ",");
                string f17 = sl.GetCellValueAsString(row, 19).Replace(".", ",");
                string f18 = sl.GetCellValueAsString(row, 20).Replace(".", ",");
                string f19 = sl.GetCellValueAsString(row, 21).Replace(".", ",");
                string f20 = sl.GetCellValueAsString(row, 22).Replace(".", ",");
                string f21 = sl.GetCellValueAsString(row, 23).Replace(".", ",");
                string f22 = sl.GetCellValueAsString(row, 24).Replace(".", ",");
                string f23 = sl.GetCellValueAsString(row, 25).Replace(".", ",");
                string f24 = sl.GetCellValueAsString(row, 26).Replace(".", ",");
                string f25 = sl.GetCellValueAsString(row, 27).Replace(".", ",");
                string f26 = sl.GetCellValueAsString(row, 28).Replace(".", ",");
                string f27 = sl.GetCellValueAsString(row, 29).Replace(".", ",");
                string f28 = sl.GetCellValueAsString(row, 30).Replace(".", ",");
                string f29 = sl.GetCellValueAsString(row, 31).Replace(".", ",");
                string f30 = sl.GetCellValueAsString(row, 32).Replace(".", ",");
                string adicional = sl.GetCellValueAsString(row, 33).Replace(".", ",");

                var tabela = (from c in data.tbTipoDeEntregaTabelaPrecos where c.idTipoDeEntrega == 13 && c.nome.Contains(uf) && c.nome.Contains(tarifa) select c).FirstOrDefault();
                if(tabela != null)
                {
                    tabela.valorKgAdicional = Convert.ToDecimal(adicional);
                    data.SubmitChanges();
                    var faixa1 = (from c in data.tbTipoDeEntregaTabelaPrecoValors where c.idTipoDeEntregaTabelaPreco == tabela.idTipoDeEntregaTabelaPreco && c.pesoFinal == 1000 select c).FirstOrDefault();                    
                    var faixa2 = (from c in data.tbTipoDeEntregaTabelaPrecoValors where c.idTipoDeEntregaTabelaPreco == tabela.idTipoDeEntregaTabelaPreco && c.pesoFinal == 2000 select c).FirstOrDefault();
                    var faixa3 = (from c in data.tbTipoDeEntregaTabelaPrecoValors where c.idTipoDeEntregaTabelaPreco == tabela.idTipoDeEntregaTabelaPreco && c.pesoFinal == 3000 select c).FirstOrDefault();
                    var faixa4 = (from c in data.tbTipoDeEntregaTabelaPrecoValors where c.idTipoDeEntregaTabelaPreco == tabela.idTipoDeEntregaTabelaPreco && c.pesoFinal == 4000 select c).FirstOrDefault();
                    var faixa5 = (from c in data.tbTipoDeEntregaTabelaPrecoValors where c.idTipoDeEntregaTabelaPreco == tabela.idTipoDeEntregaTabelaPreco && c.pesoFinal == 5000 select c).FirstOrDefault();
                    var faixa6 = (from c in data.tbTipoDeEntregaTabelaPrecoValors where c.idTipoDeEntregaTabelaPreco == tabela.idTipoDeEntregaTabelaPreco && c.pesoFinal == 6000 select c).FirstOrDefault();
                    var faixa7 = (from c in data.tbTipoDeEntregaTabelaPrecoValors where c.idTipoDeEntregaTabelaPreco == tabela.idTipoDeEntregaTabelaPreco && c.pesoFinal == 7000 select c).FirstOrDefault();
                    var faixa8 = (from c in data.tbTipoDeEntregaTabelaPrecoValors where c.idTipoDeEntregaTabelaPreco == tabela.idTipoDeEntregaTabelaPreco && c.pesoFinal == 8000 select c).FirstOrDefault();
                    var faixa9 = (from c in data.tbTipoDeEntregaTabelaPrecoValors where c.idTipoDeEntregaTabelaPreco == tabela.idTipoDeEntregaTabelaPreco && c.pesoFinal == 9000 select c).FirstOrDefault();
                    var faixa10 = (from c in data.tbTipoDeEntregaTabelaPrecoValors where c.idTipoDeEntregaTabelaPreco == tabela.idTipoDeEntregaTabelaPreco && c.pesoFinal == 10000 select c).FirstOrDefault();
                    var faixa11 = (from c in data.tbTipoDeEntregaTabelaPrecoValors where c.idTipoDeEntregaTabelaPreco == tabela.idTipoDeEntregaTabelaPreco && c.pesoFinal == 11000 select c).FirstOrDefault();
                    var faixa12 = (from c in data.tbTipoDeEntregaTabelaPrecoValors where c.idTipoDeEntregaTabelaPreco == tabela.idTipoDeEntregaTabelaPreco && c.pesoFinal == 12000 select c).FirstOrDefault();
                    var faixa13 = (from c in data.tbTipoDeEntregaTabelaPrecoValors where c.idTipoDeEntregaTabelaPreco == tabela.idTipoDeEntregaTabelaPreco && c.pesoFinal == 13000 select c).FirstOrDefault();
                    var faixa14 = (from c in data.tbTipoDeEntregaTabelaPrecoValors where c.idTipoDeEntregaTabelaPreco == tabela.idTipoDeEntregaTabelaPreco && c.pesoFinal == 14000 select c).FirstOrDefault();
                    var faixa15 = (from c in data.tbTipoDeEntregaTabelaPrecoValors where c.idTipoDeEntregaTabelaPreco == tabela.idTipoDeEntregaTabelaPreco && c.pesoFinal == 15000 select c).FirstOrDefault();
                    var faixa16 = (from c in data.tbTipoDeEntregaTabelaPrecoValors where c.idTipoDeEntregaTabelaPreco == tabela.idTipoDeEntregaTabelaPreco && c.pesoFinal == 16000 select c).FirstOrDefault();
                    var faixa17 = (from c in data.tbTipoDeEntregaTabelaPrecoValors where c.idTipoDeEntregaTabelaPreco == tabela.idTipoDeEntregaTabelaPreco && c.pesoFinal == 17000 select c).FirstOrDefault();
                    var faixa18 = (from c in data.tbTipoDeEntregaTabelaPrecoValors where c.idTipoDeEntregaTabelaPreco == tabela.idTipoDeEntregaTabelaPreco && c.pesoFinal == 18000 select c).FirstOrDefault();
                    var faixa19 = (from c in data.tbTipoDeEntregaTabelaPrecoValors where c.idTipoDeEntregaTabelaPreco == tabela.idTipoDeEntregaTabelaPreco && c.pesoFinal == 19000 select c).FirstOrDefault();
                    var faixa20 = (from c in data.tbTipoDeEntregaTabelaPrecoValors where c.idTipoDeEntregaTabelaPreco == tabela.idTipoDeEntregaTabelaPreco && c.pesoFinal == 20000 select c).FirstOrDefault();
                    var faixa21 = (from c in data.tbTipoDeEntregaTabelaPrecoValors where c.idTipoDeEntregaTabelaPreco == tabela.idTipoDeEntregaTabelaPreco && c.pesoFinal == 21000 select c).FirstOrDefault();
                    var faixa22 = (from c in data.tbTipoDeEntregaTabelaPrecoValors where c.idTipoDeEntregaTabelaPreco == tabela.idTipoDeEntregaTabelaPreco && c.pesoFinal == 22000 select c).FirstOrDefault();
                    var faixa23 = (from c in data.tbTipoDeEntregaTabelaPrecoValors where c.idTipoDeEntregaTabelaPreco == tabela.idTipoDeEntregaTabelaPreco && c.pesoFinal == 23000 select c).FirstOrDefault();
                    var faixa24 = (from c in data.tbTipoDeEntregaTabelaPrecoValors where c.idTipoDeEntregaTabelaPreco == tabela.idTipoDeEntregaTabelaPreco && c.pesoFinal == 24000 select c).FirstOrDefault();
                    var faixa25 = (from c in data.tbTipoDeEntregaTabelaPrecoValors where c.idTipoDeEntregaTabelaPreco == tabela.idTipoDeEntregaTabelaPreco && c.pesoFinal == 25000 select c).FirstOrDefault();
                    var faixa26 = (from c in data.tbTipoDeEntregaTabelaPrecoValors where c.idTipoDeEntregaTabelaPreco == tabela.idTipoDeEntregaTabelaPreco && c.pesoFinal == 26000 select c).FirstOrDefault();
                    var faixa27 = (from c in data.tbTipoDeEntregaTabelaPrecoValors where c.idTipoDeEntregaTabelaPreco == tabela.idTipoDeEntregaTabelaPreco && c.pesoFinal == 27000 select c).FirstOrDefault();
                    var faixa28 = (from c in data.tbTipoDeEntregaTabelaPrecoValors where c.idTipoDeEntregaTabelaPreco == tabela.idTipoDeEntregaTabelaPreco && c.pesoFinal == 28000 select c).FirstOrDefault();
                    var faixa29 = (from c in data.tbTipoDeEntregaTabelaPrecoValors where c.idTipoDeEntregaTabelaPreco == tabela.idTipoDeEntregaTabelaPreco && c.pesoFinal == 29000 select c).FirstOrDefault();
                    var faixa30 = (from c in data.tbTipoDeEntregaTabelaPrecoValors where c.idTipoDeEntregaTabelaPreco == tabela.idTipoDeEntregaTabelaPreco && c.pesoFinal == 30000 select c).FirstOrDefault();

                    try
                    {
                        faixa1.valor = Convert.ToDecimal(f1);
                        faixa2.valor = Convert.ToDecimal(f2);
                        faixa3.valor = Convert.ToDecimal(f3);
                        faixa4.valor = Convert.ToDecimal(f4);
                        faixa5.valor = Convert.ToDecimal(f5);
                        faixa6.valor = Convert.ToDecimal(f6);
                        faixa7.valor = Convert.ToDecimal(f7);
                        faixa8.valor = Convert.ToDecimal(f8);
                        faixa9.valor = Convert.ToDecimal(f9);
                        faixa10.valor = Convert.ToDecimal(f10);
                        faixa11.valor = Convert.ToDecimal(f11);
                        faixa12.valor = Convert.ToDecimal(f12);
                        faixa13.valor = Convert.ToDecimal(f13);
                        faixa14.valor = Convert.ToDecimal(f14);
                        faixa15.valor = Convert.ToDecimal(f15);
                        faixa16.valor = Convert.ToDecimal(f16);
                        faixa17.valor = Convert.ToDecimal(f17);
                        faixa18.valor = Convert.ToDecimal(f18);
                        faixa19.valor = Convert.ToDecimal(f19);
                        faixa20.valor = Convert.ToDecimal(f20);
                        faixa21.valor = Convert.ToDecimal(f21);
                        faixa22.valor = Convert.ToDecimal(f22);
                        faixa23.valor = Convert.ToDecimal(f23);
                        faixa24.valor = Convert.ToDecimal(f24);
                        faixa25.valor = Convert.ToDecimal(f25);
                        faixa26.valor = Convert.ToDecimal(f26);
                        faixa27.valor = Convert.ToDecimal(f27);
                        faixa28.valor = Convert.ToDecimal(f28);
                        faixa29.valor = Convert.ToDecimal(f29);
                        faixa30.valor = Convert.ToDecimal(f30);
                        data.SubmitChanges();
                    }
                    catch(Exception ex) { }
                }
                

            }
        }
        


        Response.Write("<script>alert('Produtos atualizados com sucesso.');</script>");
    }

    protected void btnAtualizar_OnCallback(object source, CallbackEventArgs e)
    {
        /*string caminho = MapPath("~/");

        string connStr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + caminho + "jadlogPrecos.xls;Extended Properties=Excel 12.0;";
        OleDbConnection cnnExcel = new OleDbConnection(connStr);
        OleDbCommand cmmPlan = null;
        OleDbDataReader drPlan = null;

        cnnExcel.Open();
        cmmPlan = new OleDbCommand("select * from [relatorio$]", cnnExcel);
        drPlan = cmmPlan.ExecuteReader(CommandBehavior.CloseConnection);
        var data = new dbCommerceDataContext();

        int contagem = 0;
        while (drPlan.Read())
        {
            try
            {
                int idTipoDeEntrega = 13;
                var tabelaPreco = new tbTipoDeEntregaTabelaPreco();
                tabelaPreco.idTipoDeEntrega = idTipoDeEntrega;
                tabelaPreco.nome = drPlan[0].ToString() + " - " + drPlan[1].ToString();
                tabelaPreco.valorKgAdicional = Convert.ToDecimal(drPlan[32].ToString());
                tabelaPreco.valorSeguro = drPlan[1].ToString().ToLower() == "capital"
                    ? Convert.ToDecimal("0,33")
                    : Convert.ToDecimal("0,66");
                data.tbTipoDeEntregaTabelaPrecos.InsertOnSubmit(tabelaPreco);
                data.SubmitChanges();

                for (int i = 2; i <= 31; i++)
                {
                    int pesoInicial = (i - 2) * 1000;
                    int pesoFinal = (i - 1) * 1000;
                    var tabelaValor = new tbTipoDeEntregaTabelaPrecoValor();
                    tabelaValor.idTipoDeEntregaTabelaPreco = tabelaPreco.idTipoDeEntregaTabelaPreco;
                    tabelaValor.pesoInicial = pesoInicial;
                    tabelaValor.pesoFinal = pesoFinal;
                    tabelaValor.valor = Convert.ToDecimal(drPlan[i].ToString());
                    data.tbTipoDeEntregaTabelaPrecoValors.InsertOnSubmit(tabelaValor);
                    data.SubmitChanges();
                }
            }
            catch (Exception)
            {

            }

        }

        cnnExcel.Close();
        cnnExcel.Dispose();*/


        //int id = 13;
        //var faixasDc = new dbCommerceDataContext();
        //var faixas = (from c in faixasDc.jadceps select c).ToList();

        //int contagem = 0;
        //foreach (var faixa in faixas)
        //{
        //    contagem++;
        //    if (contagem >= 0)
        //    {
        //        var faixaDeCepDc = new dbCommerceDataContext();
        //        var faixaDeCep = new tbFaixaDeCep();
        //        faixaDeCep.tipodeEntregaId = id;
        //        faixaDeCep.faixaDeCepDescricao = "JADLOG - " + faixa.uf + " - " + faixa.localidade;
        //        var cepSplit = faixa.cep.Split('a');
        //        if (cepSplit.Count() > 1)
        //        {
        //            faixaDeCep.faixaInicial = cepSplit[0].Trim();
        //            faixaDeCep.faixaFinal = cepSplit[1].Trim();
        //        }
        //        else
        //        {
        //            faixaDeCep.faixaInicial = cepSplit[0].Trim();
        //            faixaDeCep.faixaFinal = cepSplit[0].Trim();
        //        }
        //        faixaDeCep.prazo = Convert.ToInt32(faixa.prazo.Trim()) + 1;
        //        faixaDeCep.porcentagemDeDesconto = 0;
        //        faixaDeCep.porcentagemDoSeguro = 0;
        //        faixaDeCep.dataDaCriacao = DateTime.Now;
        //        if (faixa.tarifa.ToLower() == "interior")
        //        {
        //            faixaDeCep.interiorizacao = true;
        //        }
        //        else
        //        {
        //            faixaDeCep.interiorizacao = false;
        //        }
                
        //        string faixaFiltro = faixa.uf.ToLower().Trim() + " - " + faixa.tarifa.ToLower().Trim();
        //        var data = new dbCommerceDataContext();
        //        var tabelapreco = (from c in data.tbTipoDeEntregaTabelaPrecos
        //            where c.idTipoDeEntrega == id && c.nome.ToLower().Trim() == faixaFiltro
        //            select c).First();

        //        faixaDeCep.idTipoDeEntregaTabelaPreco = tabelapreco.idTipoDeEntregaTabelaPreco;
        //        faixaDeCepDc.tbFaixaDeCeps.InsertOnSubmit(faixaDeCep);
        //        faixaDeCepDc.SubmitChanges();



        //        if (faixa.redespacho.Trim() == "FLUVIAL")
        //        {
        //            var faixaTarifa = new tbFaixaDeCepTarifa();
        //            faixaTarifa.idFaixaDeCep = faixaDeCep.faixaDeCepId;
        //            faixaTarifa.idTipoDeEntregaTarifa = 13;
        //            faixaDeCepDc.tbFaixaDeCepTarifas.InsertOnSubmit(faixaTarifa);
        //            faixaDeCepDc.SubmitChanges();
        //        }
        //    }
        //}

    }
}