﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="usrComprasOrdemCondicoes.ascx.cs" Inherits="usrComprasOrdemCondicoes" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<style type="text/css">
    .auto-style1 {
        height: 24px;
    }
</style>
<asp:HiddenField runat="server" ID="hdfIdOrdem"></asp:HiddenField>

<asp:Panel runat="server" ID="pnCondSel">
    <table>
        <tr>
            <td>Condição de Pagamento: </td>
            <td>
                <dxe:aspxcombobox id="ddlCondPag" runat="server" valuefield="idComprasCondicaoPagamento" textfield="nome" width="200px" dropdownstyle="DropDown"></dxe:aspxcombobox>
            </td>
            <td style="padding-left: 15px">
                <asp:Button runat="server" ID="btnGravarCondicao" Text="Gravar condição de pagamento" OnClick="btnGravarCondicao_OnClick" /></td>
        </tr>
    </table>
</asp:Panel>

<asp:Panel runat="server" ID="pnCondicoes">
    <br />
    <table style="padding-left: 15px; padding-right: 15px; width: 100%">
        <tr class="rotulos">
            <td>
                <b>Condição de Pagamento:</b>
            </td>
            <td>
                <b>Datas:</b>
            </td>
            <td>
                <b>Valor parcela:</b>
            </td>
            <td>
                <b>Desconto:</b>
            </td>
            <td>
                <b>Acrescimo:</b>
            </td>
        </tr>
        <asp:ListView runat="server" ID="lstCondicoesPagamento" OnItemDataBound="lstCondicoesPagamento_OnItemDataBound">
            <ItemTemplate>
                <tr class="rotulos">
                    <td>
                        <%#Eval("nome") %>
                        <asp:HiddenField runat="server" ID="hdfIdCondicao" Value='<%#Eval("idComprasFornecedorCondicaoPagamento") %>'></asp:HiddenField>
                        <%--<asp:HiddenField runat="server" ID="hdIgnorarMes" Value='<%#Eval("ignorarMesAtual") %>'></asp:HiddenField>--%>
                    </td>
                    <td>
                        <asp:Literal runat="server" ID="litPrazos"></asp:Literal>
                    </td>
                    <td>
                        <asp:Literal runat="server" ID="litValorParcela"></asp:Literal>
                    </td>
                    <td>
                        <%#Eval("desconto") %>
                    </td>
                    <td>
                        <%#Eval("acrescimo") %>
                    </td>
                </tr>
            </ItemTemplate>
        </asp:ListView>
    </table>
</asp:Panel>
<asp:Panel runat="server" ID="pnShowSelCond" Visible="False">
    <div class="rotulos">
        <b>Condição de Pagamento: </b><asp:Label runat="server" ID="labelNoSelCond"><%=cstNenhuma%></asp:Label>
    </div>
</asp:Panel>
