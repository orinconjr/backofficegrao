﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class carregasimulacaocorreios : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        using (var data = new dbCommerceDataContext())
        {

            var dataInicio = Convert.ToDateTime("01/04/2016");
            var dataFim = Convert.ToDateTime("01/05/2016");
            var envios = (from c in data.tbPedidoEnvios where c.dataFimEmbalagem >= dataInicio && c.dataFimEmbalagem < dataFim && c.idPedidoEnvio < 235059 select c).ToList();
            var precos = (from c in data.tbPedidoEnvioValorTransportes where c.tbPedidoEnvio.dataFimEmbalagem >= dataInicio && c.tbPedidoEnvio.dataFimEmbalagem < dataFim && c.idPedidoEnvio < 235059 select c).ToList();

            var simulacaoPrecos = (from c in envios
                                   join f in precos on c.idPedidoEnvio equals f.idPedidoEnvio into precosEnvio
                                   select new
                                   {
                                       c.idPedidoEnvio,
                                       correios10 = (from d in precosEnvio where d.servico == "pacpeso" && d.valor > 0 select d).Any() ? ((from d in precosEnvio where d.servico == "pacpeso" select d).First().valor / 100) * 90 : 999999,
                                       correios20 = (from d in precosEnvio where d.servico == "pacpeso" && d.valor > 0 select d).Any() ? ((from d in precosEnvio where d.servico == "pacpeso" select d).First().valor / 100) * 80 : 999999,
                                       correios25 = (from d in precosEnvio where d.servico == "pacpeso" && d.valor > 0 select d).Any() ? ((from d in precosEnvio where d.servico == "pacpeso" select d).First().valor / 100) * 75 : 999999,
                                       jadlog = (from d in precosEnvio where d.servico == "jadlog" && d.valor > 0 select d).Any() ? (from d in precosEnvio where d.servico == "jadlog" select d).First().valor : 999999,
                                       tnt = (from d in precosEnvio where d.servico == "tnt" && d.valor > 0 select d).Any() ? (from d in precosEnvio where d.servico == "tnt" select d).First().valor : 999999
                                   });

            decimal totalCorreios10 = simulacaoPrecos.Where(x => x.correios10 < x.jadlog && x.correios10 < x.tnt).Sum(x => x.correios10);
            decimal totalCorreios20 = simulacaoPrecos.Where(x => x.correios20 < x.jadlog && x.correios20 < x.tnt).Sum(x => x.correios20);
            decimal totalCorreios25 = simulacaoPrecos.Where(x => x.correios25 < x.jadlog && x.correios25 < x.tnt).Sum(x => x.correios25);

            decimal qtotalCorreios10 = simulacaoPrecos.Where(x => x.correios10 < x.jadlog && x.correios10 < x.tnt).Count();
            decimal qtotalCorreios20 = simulacaoPrecos.Where(x => x.correios20 < x.jadlog && x.correios20 < x.tnt).Count();
            decimal qtotalCorreios25 = simulacaoPrecos.Where(x => x.correios25 < x.jadlog && x.correios25 < x.tnt).Count();

            decimal totalJad10 = simulacaoPrecos.Where(x => x.jadlog < x.correios10 && x.jadlog < x.tnt).Sum(x => x.jadlog);
            decimal totalJad20 = simulacaoPrecos.Where(x => x.jadlog < x.correios20 && x.jadlog < x.tnt).Sum(x => x.jadlog);
            decimal totalJad25 = simulacaoPrecos.Where(x => x.jadlog < x.correios25 && x.jadlog < x.tnt).Sum(x => x.jadlog);

            decimal qtotalJad10 = simulacaoPrecos.Where(x => x.jadlog < x.correios10 && x.jadlog < x.tnt).Count();
            decimal qtotalJad20 = simulacaoPrecos.Where(x => x.jadlog < x.correios20 && x.jadlog < x.tnt).Count();
            decimal qtotalJad25 = simulacaoPrecos.Where(x => x.jadlog < x.correios25 && x.jadlog < x.tnt).Count();

            decimal totalTnt10 = simulacaoPrecos.Where(x => x.tnt < x.correios10 && x.tnt < x.jadlog).Sum(x => x.tnt);
            decimal totalTnt20 = simulacaoPrecos.Where(x => x.tnt < x.correios20 && x.tnt < x.jadlog).Sum(x => x.tnt);
            decimal totalTnt25 = simulacaoPrecos.Where(x => x.tnt < x.correios25 && x.tnt < x.jadlog).Sum(x => x.tnt);

            decimal qtotalTnt10 = simulacaoPrecos.Where(x => x.tnt < x.correios10 && x.tnt < x.jadlog).Count();
            decimal qtotalTnt20 = simulacaoPrecos.Where(x => x.tnt < x.correios20 && x.tnt < x.jadlog).Count();
            decimal qtotalTnt25 = simulacaoPrecos.Where(x => x.tnt < x.correios25 && x.tnt < x.jadlog).Count();


            Response.Write("Tabela com 10%<br>");
            Response.Write("Correios:" + totalCorreios10 .ToString("C") + " / " + qtotalCorreios10  + "<br>");
            Response.Write("Jadlog:" + totalJad10.ToString("C") + " / " + qtotalJad10 + "<br>");
            Response.Write("TNT:" + totalTnt10.ToString("C") + " / " + qtotalTnt10 + "<br>");

            Response.Write("<br><br>Tabela com 20%<br>");
            Response.Write("Correios:" + totalCorreios20.ToString("C") + " / " + qtotalCorreios20 + "<br>");
            Response.Write("Jadlog:" + totalJad20.ToString("C") + " / " + qtotalJad20 + "<br>");
            Response.Write("TNT:" + totalTnt20.ToString("C") + " / " + qtotalTnt20 + "<br>");

            Response.Write("<br><br>Tabela com 25%<br>");
            Response.Write("Correios:" + totalCorreios25.ToString("C") + " / " + qtotalCorreios25 + "<br>");
            Response.Write("Jadlog:" + totalJad25.ToString("C") + " / " + qtotalJad25 + "<br>");
            Response.Write("TNT:" + totalTnt25.ToString("C") + " / " + qtotalTnt25 + "<br>");
        }
    }
}
