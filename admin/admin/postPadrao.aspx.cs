﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

public partial class admin_postPadrao : System.Web.UI.Page
{
    public string ImgUrl;
    public string ImgUrlLogoRodape;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtProdutoId.Attributes.Add("onkeypress", "return soNumero(event);");
            txtProdutoIdPostProduto.Attributes.Add("onkeypress", "return soNumero(event);");
            txtEspessuraBordaBalaoPostProduto.Attributes.Add("onkeypress", "return soNumero(event);");
            txtValorProduto.Attributes.Add("onkeyPress", "return moeda(this,'.',',',event);");
        }
        else
        {
            if (rblOpcaoPost.SelectedValue == "2")
                ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "document.getElementById('tbDadosPostPadrao').style.display = 'none';" +
                                                                                                "document.getElementById('tbDadosPostProduto').style.display = 'block';", true);

        }
    }

    protected void ExportToImage(object sender, EventArgs e)
    {
        string base64 = Request.Form[hfImageData.UniqueID].Split(',')[1];
        byte[] bytes = Convert.FromBase64String(base64);
        Response.Clear();
        Response.ContentType = "image/png";
        Response.AddHeader("Content-Disposition", "attachment; filename=HTML.png");
        Response.Buffer = true;
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.BinaryWrite(bytes);
        Response.End();
    }

    protected void btnMontarImagem_OnClick(object sender, EventArgs e)
    {
        lblProdutoNome.Text = "";
        lblProdutoNomePostProduto.Text = "";
        //txtProdutoNomePostProduto.Text = "";

        try
        {

            if (rblOpcaoPost.SelectedValue == "1")
            {
                if (string.IsNullOrEmpty(txtProdutoId.Text)) return;

                int produtoId = Convert.ToInt32(txtProdutoId.Text);
                MontaPostPadrao(produtoId);
                ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "document.getElementById('panel1').style.display = 'block';" +
                                                                                                "document.getElementById('panel2').style.display = 'none';", true);
            }
            else
            {
                if (string.IsNullOrEmpty(txtProdutoIdPostProduto.Text)) return;

                int produtoId = Convert.ToInt32(txtProdutoIdPostProduto.Text);
                MontaPostProduto(produtoId);
                ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "document.getElementById('panel1').style.display = 'none';" +
                                                                                                "document.getElementById('panel2').style.display = 'block';", true);
            }

        }
        catch (Exception ex)
        {

            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(),
                "alert('ERRO: " + ex.Message + "');", true);
        }

    }

    private void MontaPostPadrao(int produtoId)
    {
        string opcaoLayoutValor = ddlOplayoutValor.SelectedValue;
        // string opcaoLayoutBalao = ddlOplayoutBalao.SelectedValue;
        string opcaoLayoutLogo = ddlOplayoutLogo.SelectedValue;
        string opcaoLayoutTipo = ddlOplayoutTipo.SelectedValue;
        string opcaoTipoPost = ddlOpTipoPost.SelectedValue;
        string opcaoSelo = ddlOpSelo.SelectedValue;
        decimal descontoBoleto = 0;
        string fotodestaque = "";
        tbProduto produto;

        using (var data = new dbCommerceDataContext())
        {
            produto = (from c in data.tbProdutos where c.produtoId == produtoId select c).FirstOrDefault();
            descontoBoleto = (decimal)(data.tbCondicoesDePagamentos.First(x => x.condicaoId == 4).porcentagemDeDesconto / 100);
        }

        if (produto == null)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(),
                "alert('Produto não encontrado.');", true);
            return;
        }

        lblProdutoNome.Text = produto.produtoNome;

        bool mostrarPorcentagem = (produto.produtoPrecoPromocional ?? 0) > 0 && (produto.produtoPrecoPromocional ?? 0) < produto.produtoPreco;
        decimal produtoPreco = (produto.produtoPrecoPromocional ?? 0) > 0 && (produto.produtoPrecoPromocional ?? 0) < produto.produtoPreco
                ? Math.Round((produto.produtoPrecoPromocional ?? 0), 2)
                : Math.Round(produto.produtoPreco, 2);

        if (!string.IsNullOrEmpty(txtValorProduto.Text))
            produtoPreco = Math.Round(Convert.ToDecimal(txtValorProduto.Text), 2);

        decimal produtoPrecoNoBoleto = Math.Round(produtoPreco - (produtoPreco * descontoBoleto), 2);
        decimal porcentagemOff = (100 - ((produtoPrecoNoBoleto * 100) / produto.produtoPreco));

        //imgFoto.Src = "http:/fotos/" + produto.produtoId + "/" + produto.fotoDestaque + ".jpg";

        if (!Directory.Exists(@"C:\inetpub\wwwroot\admin\admin\postPadrao\fotos\" + produto.produtoId))//se não existir o diretório então cria
        {
            Directory.CreateDirectory(@"C:\inetpub\wwwroot\admin\admin\postPadrao\fotos\" + produto.produtoId);
        }
        //baixa as fotos do produto (temporário)

        List<tbProdutoFoto> verificaFotos;

        using (var data = new dbCommerceDataContext())
        {
            verificaFotos = (from c in data.tbProdutoFotos where c.produtoId == produto.produtoId select c).ToList();
        }

        if (verificaFotos.Any())
        {
            if (verificaFotos.Count > 0)
            {
                string localFilename = @"C:\inetpub\wwwroot\admin\admin\postPadrao\fotos\" + produto.produtoId + @"\";

                try
                {
                    foreach (tbProdutoFoto t in verificaFotos)
                    {
                        using (WebClient client = new WebClient())
                        {
                            client.DownloadFile("https://dmhxz00kguanp.cloudfront.net/fotos/" + produto.produtoId + "/" + t.produtoFoto + ".jpg", localFilename + t.produtoFoto + ".jpg");
                            // ImgUrl = "/admin/postPadrao/fotos/" + produto.produtoId + "/" + t.produtoFoto + ".jpg";
                            fotodestaque = t.produtoFoto;
                        }
                    }
                }
                catch (Exception ex)
                {
                }

            }
        }

       foreach (RepeaterItem itemRow in rptFotos.Items)
       {
           HtmlInputRadioButton radioBtn = (HtmlInputRadioButton)itemRow.FindControl("selecionarFoto");
           if (radioBtn.Checked)
           {
               fotodestaque = radioBtn.Value;
           }
       }


        ImgUrl = "/admin/postPadrao/fotos/" + produto.produtoId + "/" + fotodestaque + ".jpg";  //definido linha 146
        // ImgUrl = "/admin/postPadrao/fotos/" + produto.produtoId + "/" + produto.fotoDestaque + ".jpg";  //definido linha 146
        //ImgUrl = @"c:\inetpub\wwwroot\fotos\" + produto.produtoId + @"\" + produto.fotoDestaque + ".jpg";
        //ImgUrl = Path.GetFullPath(@"C:\inetpub\wwwroot\" + produto.produtoId + @"\" + produto.fotoDestaque + ".jpg");

        // ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "document.getElementsByClassName(\"porcentagem\")[0].style.backgroundImage = 'url(../imagens/bullet-" + opcaoLayoutBalao + ".png)';", true);
        // ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "document.getElementsByClassName(\"porcentagem\")[0].style.backgroundImage = 'url(../imagens/bullet-4.png)';", true);
        ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "document.getElementsByClassName(\"logoGrao\")[0].style.backgroundImage = 'url(../imagens/logo-mini-" + opcaoLayoutLogo + ".png)';", true);
        // ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "document.getElementsByClassName(\"logoGrao\")[0].style.backgroundImage = 'url(../imagens/logo-mini-branco.png)';", true);

        if (mostrarPorcentagem)
        {
            litPorcentagem.Text = Math.Floor(porcentagemOff).ToString();
        }
        else
        {
            //pPorcentagem.Attributes.Add("style", "display:none;");
            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "document.getElementsByClassName(\"porcentagem\")[0].style.display = 'none';", true);
        }

        if (opcaoLayoutValor == "1" && (produto.produtoPrecoPromocional ?? 0) == 0)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('Não é possivel montar imagem De Por com este produto, selecione outra opção');", true);
            return;
        }

        if (opcaoLayoutValor == "0")
        {
            litprecoBoletoInteiro.Text = produtoPrecoNoBoleto.ToString().Split(',')[0];
            precoBoletoCentavos.Text = produtoPrecoNoBoleto.ToString().Split(',')[1].Substring(0, 2);

            //postPadrao.Attributes.Add("class", "postPadrao showBoleto");
            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "document.getElementById(\"postPadrao\").className = 'postPadrao showBoleto';", true);
        }
        else if (opcaoLayoutValor == "1")
        {
            litPrecoParceladoInteiro.Text = produtoPreco.ToString().Split(',')[0];
            litPrecoParceladoCentavos.Text = produtoPreco.ToString().Split(',')[1].Substring(0, 2);

            litPrecoDe.Text = produto.produtoPreco.ToString("N");

            litPrecoPorInteiro.Text = produtoPrecoNoBoleto.ToString().Split(',')[0];
            litPrecoPorCentavos.Text = produtoPrecoNoBoleto.ToString().Split(',')[1].Substring(0, 2);

            //postPadrao.Attributes.Add("class", "postPadrao showDePor");
            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "document.getElementById(\"postPadrao\").className = 'postPadrao showDePor';", true);
        }
        else if (opcaoLayoutValor == "2")
        {
            decimal valorParcela = Math.Round((produtoPreco / 12), 2);

            litPrecoParceladoInteiro.Text = valorParcela.ToString().Split(',')[0];
            litPrecoParceladoCentavos.Text = valorParcela.ToString().Split(',')[1].Substring(0, 2);

            //postPadrao.Attributes.Add("class", "postPadrao showParcelado");
            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "document.getElementById(\"postPadrao\").className = 'postPadrao showParcelado';", true);
        }
        else if (opcaoLayoutValor == "3")//sem valor
        {
            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "document.getElementsByClassName(\"boxInfo\")[0].style.display = 'none';" +
                                                                                            "document.getElementsByClassName(\"porcentagem\")[0].style.display = 'none';" +
                                                                                            "document.getElementsByClassName(\"exclusivo\")[0].style.display = 'none';" +
                                                                                            "document.getElementsByClassName(\"logoGrao\")[0].style.display = 'none';" +
                                                                                            "document.getElementsByClassName(\"rodape\")[0].src = '../imagens/rodape.png';" +
                                                                                            "document.getElementsByClassName(\"rodape\")[0].style.position = 'absolute';" +
                                                                                            "document.getElementsByClassName(\"rodape\")[0].style.top = '753px';" +
                                                                                            "document.getElementsByClassName(\"rodape\")[0].style.left = '0px';" +
                                                                                            "document.getElementsByClassName(\"promocaoTempoLimitado\")[0].style.display = 'none';" +
                                                                                            "document.getElementsByClassName(\"postPadrao\")[0].style.height = '800px !important';" +
                                                                                            "document.getElementsByClassName(\"bgAnuncio\")[0].style.height = '800px !important';", true);
        }
        if(opcaoSelo != "nenhum" && opcaoLayoutTipo == "todoSiteOff"){
            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "document.getElementById(\"postPadrao\").classList.add(\"selo\");"+
                                                                                            "document.getElementsByClassName(\"seloRodape\")[0].style.backgroundImage = 'url(images/"+opcaoSelo+".png)';",true);
            
        }
        if(opcaoLayoutTipo == "promosohj"){
            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "document.getElementById(\"postPadrao\").classList.add(\"post-promocional-so-hj\");", true);
        }
        if(opcaoLayoutTipo == "blackfriday"){
            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "document.getElementById(\"postPadrao\").classList.add(\"blackfriday\");", true);
        }
        
        if(opcaoLayoutTipo == "decoMadeira"){
            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "document.getElementById(\"postPadrao\").classList.add(\"blackfriday\",\"decoMadeira\");", true);
        }

        if(opcaoLayoutTipo == "todoSiteOff"){
            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "document.getElementById(\"postPadrao\").classList.add(\"blackfriday\",\"todoSiteOff\");", true);
        }

        if(opcaoLayoutTipo == "diaDeOfertas"){
            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "document.getElementById(\"postPadrao\").classList.add(\"blackfriday\",\"diaDeOfertas\");", true);
        }
        
        if(opcaoLayoutTipo == "queridinhoMamaes"){
            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "document.getElementById(\"postPadrao\").classList.add(\"blackfriday\",\"queridinhoMamaes\");", true);
        }
        if(opcaoLayoutTipo == "fdsDescontos"){
            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "document.getElementById(\"postPadrao\").classList.add(\"blackfriday\",\"fdsDescontos\");", true);
        }
        if(opcaoLayoutTipo == "ultimaChance"){
            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "document.getElementById(\"postPadrao\").classList.add(\"blackfriday\",\"ultimaChance\");", true);
        }
        if(opcaoLayoutTipo == "domingoOfertas"){
            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "document.getElementById(\"postPadrao\").classList.add(\"blackfriday\",\"domingoOfertas\");", true);
        }
        if(opcaoLayoutTipo == "sabadoImperdivel"){
            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "document.getElementById(\"postPadrao\").classList.add(\"blackfriday\",\"sabadoImperdivel\");", true);
        }
        if(opcaoLayoutTipo == "alertaDeDescontos"){
            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "document.getElementById(\"postPadrao\").classList.add(\"blackfriday\",\"alertaDeDescontos\");", true);
        }
        if(opcaoLayoutTipo == "pPrecosGDescontos"){
            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "document.getElementById(\"postPadrao\").classList.add(\"blackfriday\",\"pPrecosGDescontos\");", true);
        }
        if(opcaoLayoutTipo == "novidadesDecorar"){
            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "document.getElementById(\"postPadrao\").classList.add(\"blackfriday\",\"novidadesDecorar\");", true);
        }
        if(opcaoLayoutTipo == "comboKitBerco"){
            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "document.getElementById(\"postPadrao\").classList.add(\"comboKitBerco\");", true);
        }



        if( opcaoTipoPost == "instagram"){
            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "document.getElementById(\"postPadrao\").style.height = '800px';"+
                                                                                            "document.getElementById(\"postPadrao\").classList.add(\"instagram\");", true);
        }

        // if (fupImagem.HasFile)
        // {
        //     string[] segments = fupImagem.FileName.Split('.');
        //     string fileExt = segments[segments.Length - 1];
        //     string tempFileName = Guid.NewGuid().ToString();

        //     fupImagem.SaveAs(Server.MapPath("../imagens/tarja/") + tempFileName + "." + fileExt);

        //     imgTarjaExclusivo.Src = "../imagens/tarja/" + tempFileName + "." + fileExt;

        //     DirectoryInfo Dir = new DirectoryInfo(Server.MapPath("../imagens/tarja/"));
        //     // Busca automaticamente todos os arquivos em todos os subdiretórios
        //     FileInfo[] files = Dir.GetFiles("*", SearchOption.AllDirectories);
        //     foreach (FileInfo file in files)
        //     {
        //         if (file.CreationTime.Date < DateTime.Now.Date)
        //         {
        //             File.Delete(Server.MapPath("../imagens/tarja/") + file.Name);
        //         }
        //     }

        //     ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "document.getElementById(\"boxImagem\").style.display = 'none';" +
        //                                                                                     "document.getElementById(\"divTarjaExclusivo\").style.display = 'block';", true);
        // }

        // if (fupFoto.HasFile)
        // {
        //     string[] segments = fupFoto.FileName.Split('.');
        //     string fileExt = segments[segments.Length - 1];
        //     string tempFileName = Guid.NewGuid().ToString();

        //     fupFoto.SaveAs(Server.MapPath("../imagens/tarja/") + tempFileName + "." + fileExt);

        //     ImgUrl = "../imagens/tarja/" + tempFileName + "." + fileExt;

        //     DirectoryInfo Dir = new DirectoryInfo(Server.MapPath("../imagens/tarja/"));
        //     // Busca automaticamente todos os arquivos em todos os subdiretórios
        //     FileInfo[] files = Dir.GetFiles("*", SearchOption.AllDirectories);
        //     foreach (FileInfo file in files)
        //     {
        //         if (file.CreationTime.Date < DateTime.Now.Date)
        //         {
        //             File.Delete(Server.MapPath("../imagens/tarja/") + file.Name);
        //         }
        //     }


        // }


        List<tbProdutoFoto> listaFotos;

        using (var data = new dbCommerceDataContext())
        {
            listaFotos = (from c in data.tbProdutoFotos where c.produtoId == produto.produtoId select c).ToList();
        }

        rptFotos.DataSource = null;
        rptFotos.DataBind();

        pnlListaFotos.Visible = false;

        // if (listaFotos.Any() && !fupFoto.HasFile)
        if (listaFotos.Any())
        {
            if (listaFotos.Count > 1)
            {
                rptFotos.DataSource = listaFotos;
                rptFotos.DataBind();
                pnlListaFotos.Visible = true;
            }
        }


        divVisualizarPost.Visible = true;
    }

    private void MontaPostProduto(int produtoId)
    {
        tbProduto produto;
        decimal descontoBoleto = 0;

        using (var data = new dbCommerceDataContext())
        {
            produto = (from c in data.tbProdutos where c.produtoId == produtoId select c).FirstOrDefault();
            descontoBoleto = (decimal)(data.tbCondicoesDePagamentos.First(x => x.condicaoId == 4).porcentagemDeDesconto / 100);
        }

        if (produto == null)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(),
                "alert('Produto não encontrado.');", true);
            return;
        }

        ckbEditarNomeProduto.Visible = true;
        lblProdutoNomePostProduto.Text = produto.produtoNome;
        //litNomeProdutoPostProduto.Text = produto.produtoNome;

        if (!ckbEditarNomeProduto.Checked)
        {
            txtProdutoNomePostProduto.Text = produto.produtoNome;
            litNomeProdutoPostProduto.Text = produto.produtoNome;
        }


        if (ckbEditarNomeProduto.Checked)
            litNomeProdutoPostProduto.Text = txtProdutoNomePostProduto.Text;



        bool mostrarPorcentagem = (produto.produtoPrecoPromocional ?? 0) > 0 && (produto.produtoPrecoPromocional ?? 0) < produto.produtoPreco;
        decimal produtoPreco = (produto.produtoPrecoPromocional ?? 0) > 0 && (produto.produtoPrecoPromocional ?? 0) < produto.produtoPreco
                ? Math.Round((produto.produtoPrecoPromocional ?? 0), 2)
                : Math.Round(produto.produtoPreco, 2);

        decimal produtoPrecoNoBoleto = Math.Round(produtoPreco - (produtoPreco * descontoBoleto), 2);
        decimal porcentagemOff = (100 - ((produtoPrecoNoBoleto * 100) / produto.produtoPreco));


        pOuParcela.InnerText = "";
        if (ddlLayoutPrecoParcelaPostProduto.SelectedValue != "0")
        {

            tbCondicoesDePagamento condicao;

            using (var data = new dbCommerceDataContext())
            {
                condicao = (from c in data.tbCondicoesDePagamentos select c).First(x => x.destaque.ToLower() == "true");
            }

            int parcelasSemJuros = condicao.parcelasSemJuros;
            double taxaDeJuros = condicao.taxaDeJuros / 100;
            decimal valorMinimoDaParcela = condicao.valorMinimoDaParcela;
            int numeroMaximoDeParcelas = condicao.numeroMaximoDeParcelas;
            decimal valorDaParcela;
            decimal produtoPrecoParcelado;

            for (int i = numeroMaximoDeParcelas; i <= numeroMaximoDeParcelas && i > 0; i--)
            {
                valorDaParcela = produtoPreco / i;

                if (valorDaParcela >= valorMinimoDaParcela)
                {
                    if (i > 1)
                    {
                        if (i <= parcelasSemJuros)
                        {
                            produtoPrecoParcelado = Math.Round(valorDaParcela, 2);
                            litValorInteiroPostProduto.Text = produtoPrecoParcelado.ToString().Split(',')[0];
                            litValorCentavosPostProduto.Text = produtoPrecoParcelado.ToString().Split(',')[1];
                            liMaximoParcelasProduto.Text = i.ToString();
                        }
                        else
                        {
                            produtoPrecoParcelado = Math.Round(Convert.ToDecimal((double)valorDaParcela * (Math.Pow((1 + taxaDeJuros), i))));
                            litValorInteiroPostProduto.Text = produtoPrecoParcelado.ToString().Split(',')[0];
                            litValorCentavosPostProduto.Text = produtoPrecoParcelado.ToString().Split(',')[1];
                            liMaximoParcelasProduto.Text = i.ToString();
                        }
                        break;
                    }
                }
            }

            pOuParcela.InnerText = "ou";

            if (ddlLayoutPrecoParcelaPostProduto.SelectedValue == "3")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "document.getElementById('divOuParcela').style.width = '242px';" +
                                                                                                "document.getElementById('divOuParcela').style.marginTop = '145px';", true);
            }
        }

        if (ddlLayoutPrecoAVistaPostProduto.SelectedValue != "0")
        {
            litValorAVistaInterioPostProduto.Text = produtoPrecoNoBoleto.ToString().Split(',')[0];
            litValorAVistaCentavosPostProduto.Text = produtoPrecoNoBoleto.ToString().Split(',')[1];

            if (ddlLayoutPrecoParcelaPostProduto.SelectedValue == "3")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "document.getElementById('divNoBotelo').style.width = '237px';" +
                                                                                                "document.getElementById('divNoBotelo').style.marginTop = '92px';", true);
            }
        }
        else
        {
            pOuParcela.InnerText = "";
        }


        //ImgUrl = "/fotos/" + produto.produtoId + "/" + produto.fotoDestaque + ".jpg";
        ImgUrl = Path.GetFullPath(@"C:\inetpub\wwwroot\" + produto.produtoId + @"\" + produto.fotoDestaque + ".jpg");

        //ImgUrl = Server.MapPath("../fotos/produto.png");

        ImgUrlLogoRodape = "../imagens/" + ddlCorLogoPostProduto.SelectedValue;

        if (mostrarPorcentagem)
        {
            litPorcentagemDesconto1PostProduto.Text = Math.Floor(porcentagemOff).ToString().Substring(0, 1);
            litPorcentagemDesconto2PostProduto.Text = Math.Floor(porcentagemOff).ToString().Substring(1, 1);
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "document.getElementsByClassName('desconto')[0].style.display = 'none';", true);
        }

        //if (ddlLayoutPorcetagemDescontoPostProduto.SelectedValue == "1")
        //{
        //    ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "document.getElementsByClassName('desconto')[0].className += ' pp';", true);
        //}
        //else if (ddlLayoutPorcetagemDescontoPostProduto.SelectedValue == "0")
        //{
        //    ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "document.getElementsByClassName('desconto')[0].style.display = 'none';", true);
        //}

        if (ddlLayoutPrecoParcelaPostProduto.SelectedValue == "1")
        {
            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "document.getElementsByClassName('pagamento')[0].className += ' pp';", true);
        }
        else if (ddlLayoutPrecoParcelaPostProduto.SelectedValue == "3")
        {
            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "document.getElementsByClassName('pagamento')[0].className += ' gg';", true);
        }
        else if (ddlLayoutPrecoParcelaPostProduto.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "document.getElementsByClassName('pagamento')[0].style.display = 'none';", true);
        }

        if (ddlLayoutPrecoAVistaPostProduto.SelectedValue == "1")
        {
            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "document.getElementsByClassName('avista')[0].className += ' pp';", true);
        }
        else if (ddlLayoutPrecoAVistaPostProduto.SelectedValue == "3")
        {
            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "document.getElementsByClassName('avista')[0].className += ' gg';", true);
        }
        else if (ddlLayoutPrecoAVistaPostProduto.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "document.getElementsByClassName('avista')[0].style.display = 'none';", true);
        }

        if (ddlTamanhoTituloPostProduto.SelectedValue == "1")
        {
            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "document.getElementById('h1TituloPostproduto').style.fontSize = '24px';", true);
        }

        if (fupImagemTituloPostProduto.HasFile)
        {
            string[] segments = fupImagemTituloPostProduto.FileName.Split('.');
            string fileExt = segments[segments.Length - 1];
            string tempFileName = Guid.NewGuid().ToString();

            fupImagemTituloPostProduto.SaveAs(Server.MapPath("../imagens/tarja/") + tempFileName + "." + fileExt);

            imgTituloPostProduto.Attributes["style"] = "height: 173px;float: left;width: 100%;background: url(../imagens/tarja/" + tempFileName + "." + fileExt + ") center center no-repeat";

            DirectoryInfo Dir = new DirectoryInfo(Server.MapPath("../imagens/tarja/"));
            // Busca automaticamente todos os arquivos em todos os subdiretórios
            FileInfo[] files = Dir.GetFiles("*", SearchOption.AllDirectories);
            foreach (FileInfo file in files.Where(file => file.CreationTime.Date < DateTime.Now.Date))
            {
                File.Delete(Server.MapPath("../imagens/tarja/") + file.Name);
            }
        }

        if (fupImagemTarjaPostProduto.HasFile)
        {
            string[] segments = fupImagemTarjaPostProduto.FileName.Split('.');
            string fileExt = segments[segments.Length - 1];
            string tempFileName = Guid.NewGuid().ToString();

            fupImagemTarjaPostProduto.SaveAs(Server.MapPath("../imagens/tarja/") + tempFileName + "." + fileExt);

            imgTarjaPostProduto.Src = "../imagens/tarja/" + tempFileName + "." + fileExt;

            DirectoryInfo Dir = new DirectoryInfo(Server.MapPath("../imagens/tarja/"));
            // Busca automaticamente todos os arquivos em todos os subdiretórios
            FileInfo[] files = Dir.GetFiles("*", SearchOption.AllDirectories);
            foreach (FileInfo file in files.Where(file => file.CreationTime.Date < DateTime.Now.Date))
            {
                File.Delete(Server.MapPath("../imagens/tarja/") + file.Name);
            }

        }

        if (ckbLimparImagens.Checked)
        {
            imgTituloPostProduto.Attributes["style"] = "height: 173px;float: left;width: 100%;";
            imgTarjaPostProduto.Src = "";
        }

        //if (fupImagemBalaoPostProduto.HasFile)
        //{
        //    string[] segments = fupImagemBalaoPostProduto.FileName.Split('.');
        //    string fileExt = segments[segments.Length - 1];
        //    string tempFileName = Guid.NewGuid().ToString();

        //    fupImagemBalaoPostProduto.SaveAs(Server.MapPath("../imagens/tarja/") + tempFileName + "." + fileExt);

        //    ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "document.getElementsByClassName('balao')[0].style.backgroundImage  = \"url('../imagens/tarja/" + tempFileName + "." + fileExt + "')\";", true);

        //    DirectoryInfo Dir = new DirectoryInfo(Server.MapPath("../imagens/tarja/"));
        //    // Busca automaticamente todos os arquivos em todos os subdiretórios
        //    FileInfo[] files = Dir.GetFiles("*", SearchOption.AllDirectories);
        //    foreach (FileInfo file in files.Where(file => file.CreationTime.Date < DateTime.Now.Date))
        //    {
        //        File.Delete(Server.MapPath("../imagens/tarja/") + file.Name);
        //    }

        //}

        ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "document.getElementsByClassName('bgAnuncio')[0].style.backgroundColor = '" + txtRgbBgPostProduto.Text + "';" +
                                                                                        "document.getElementsByClassName('blocoRight')[0].style.backgroundColor = '" + txtRgbBgCorDireitaPostProduto.Text + "';" +
                                                                                        "document.getElementsByClassName('desconto')[0].style.backgroundColor = '" + txtRgbBgCorBalaoPostProduto.Text + "';" +
                                                                                        "document.getElementsByClassName('desconto')[0].style.color = '" + txtRgbBgCorTextoBalaoPostProduto.Text + "';" +
                                                                                        "document.getElementsByClassName('desconto')[0].style.borderColor = '" + txtCorBordaBalaoPostProduto.Text + "';" +
                                                                                        "document.getElementsByClassName('desconto')[0].style.borderWidth = '" + txtEspessuraBordaBalaoPostProduto.Text + "px';" +
                                                                                        "document.getElementById('h1TituloPostproduto').style.color = '" + txtCorTituloPostProduto.Text + "';" +
                                                                                        "document.getElementsByClassName('avista')[0].style.color = '" + txtCorValoresPostProduto.Text + "';" +
                                                                                        "document.getElementsByClassName('pagamento')[0].style.color = '" + txtCorValoresPostProduto.Text + "';", true);


        if (ckbEditarNomeProduto.Checked)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "document.getElementById('" + lblProdutoNomePostProduto.ClientID + "').style.display = 'none';" +
                                                                                            "document.getElementById('" + txtProdutoNomePostProduto.ClientID + "').style.display = 'block';", true);
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "document.getElementById('" + lblProdutoNomePostProduto.ClientID + "').style.display = 'block';" +
                                                                                            "document.getElementById('" + txtProdutoNomePostProduto.ClientID + "').style.display = 'none';", true);
        }

        divVisualizarPostProduto.Visible = true;
    }
}