﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using DevExpress.Data.Filtering;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;

public partial class admin_pedidos : System.Web.UI.Page
{

    DateTemplate dateTemplate = null;
    string teste = "xxx";

    protected void Page_Load(object sender, EventArgs e)
    {
        var data = new dbCommerceDataContext();

        if (Request.QueryString["duplicados"] != null)
        {
            sqlPedidos.SelectMethod = "pedidoSelecionaDuplicados";
        }
        if (Request.QueryString["checa"] != null)
        {
            var pedidoDc = new dbCommerceDataContext();
            var idPedidoExtra = (from c in pedidoDc.tbPedidos where c.pedidoId == 23006 select c).First().marketplaceId;

            var request = (HttpWebRequest)WebRequest.Create(rnIntegracoes.extraUrl + "/orders/" + idPedidoExtra);
            request.Headers.Add("nova-auth-token", rnIntegracoes.extraAuthToken);
            request.Headers.Add("nova-app-token", rnIntegracoes.extraAppToken);
            request.Method = "GET";
            var location = String.Empty;
            var result = String.Empty;
            using (var response = (HttpWebResponse)request.GetResponse())
            using (var stream = response.GetResponseStream())
            using (var reader = new StreamReader(stream))
            {
                Response.Write(reader.ReadToEnd());
            }
        }
        //mlIntegracao.checaPedidosML();
        if (!Page.IsPostBack)
        {
            var usuarioLogadoId = Request.Cookies["usuarioLogadoId"];
            int usuarioId = Convert.ToInt32(usuarioLogadoId.Value);
            var permissaoTotais =  (from c in data.tbUsuarioPaginasPermitidas
                                        where c.paginaPermitidaNome == "visualizartotais" && c.usuarioId == usuarioId
                                        select c).Any();
            if(!permissaoTotais)
            {
                grd.Settings.ShowFooter = false;
            }
            try
            {
            }
            catch (Exception)
            {

            }
        }
        //rnIntegracoes.checaPedidosExtra(false);
        //rnIntegracoes.checaPedidosRakuten();
    }
    protected void btPdf_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WritePdfToResponse();
    }
    protected void btXsl_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteXlsToResponse();
    }
    protected void btRtf_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteRtfToResponse();
    }
    protected void btCsv_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteCsvToResponse();
    }

    protected void grid_AutoFilterCellEditorCreate(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewEditorCreateEventArgs e)
    {
        ASPxGridView grid = sender as ASPxGridView;
        if (e.Column.FieldName == "dataHoraDoPedido")
        {
            DropDownEditProperties dde = new DropDownEditProperties();
            dde.EnableClientSideAPI = true;
            dateTemplate = new DateTemplate();
            dde.DropDownWindowTemplate = dateTemplate;
            e.EditorProperties = dde;
        }
        if (e.Column.FieldName == "vencimentoParcela")
        {
            DropDownEditProperties dde = new DropDownEditProperties();
            dde.EnableClientSideAPI = true;
            dateTemplate = new DateTemplate();
            dde.DropDownWindowTemplate = dateTemplate;
            e.EditorProperties = dde;
        }
    }
    protected void grid_AutoFilterCellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
    {
        ASPxGridView grid = sender as ASPxGridView;
        //if (e.Column.FieldName == "dataHoraDoPedido")
        //{
        //    ASPxDropDownEdit dde = e.Editor as ASPxDropDownEdit;
        //    dde.ClientSideEvents.CloseUp = String.Format("function (s, e) {{ ApplyFilter(s, {0}, {1}); }}", dateTemplate.cIdFrom, dateTemplate.cIdTo);
        //    //dde.ClientSideEvents.DropDown = String.Format("function (s, e) {{ OnDropDown(s, {0}, {1}); }}", dateTemplate.cIdFrom, dateTemplate.cIdTo);
        //    dde.ReadOnly = true;
        //}
        if (e.Column.FieldName == "dataHoraDoPedido")
        {
            ASPxDropDownEdit dde = e.Editor as ASPxDropDownEdit;
            dde.ClientSideEvents.CloseUp = String.Format("function (s, e) {{ ApplyFilter(s, {0}, {1}, 'dataHoraDoPedido'); }}", dateTemplate.cIdFrom, dateTemplate.cIdTo);
            //dde.ClientSideEvents.DropDown = String.Format("function (s, e) {{ OnDropDown(s, {0}, {1}); }}", dateTemplate.cIdFrom, dateTemplate.cIdTo);
            dde.ReadOnly = true;
        }
        if (e.Column.FieldName == "vencimentoParcela")
        {
            ASPxDropDownEdit dde = e.Editor as ASPxDropDownEdit;
            dde.ClientSideEvents.CloseUp = String.Format("function (s, e) {{ ApplyFilter(s, {0}, {1}, 'vencimentoParcela'); }}", dateTemplate.cIdFrom, dateTemplate.cIdTo);
            //dde.ClientSideEvents.DropDown = String.Format("function (s, e) {{ OnDropDown(s, {0}, {1}); }}", dateTemplate.cIdFrom, dateTemplate.cIdTo);
            dde.ReadOnly = true;
        }
    }
    protected void grid_ProcessColumnAutoFilter(object sender, ASPxGridViewAutoFilterEventArgs e)
    {
        if (e.Value == "|") return;
        //if (e.Column.FieldName == "dataHoraDoPedido")
        //{
        //    if (e.Kind == GridViewAutoFilterEventKind.CreateCriteria)
        //    {
        //        Session["dataHoraDoPedido"] = e.Value;
        //        String[] dates = e.Value.Split('|');
        //        DateTime dateFrom = Convert.ToDateTime(dates[0]),
        //            dateTo = Convert.ToDateTime(dates[1]);
        //        e.Criteria = (new OperandProperty("dataHoraDoPedido") >= dateFrom) &
        //                     (new OperandProperty("dataHoraDoPedido") <= dateTo);
        //    }
        //    else
        //    {
        //        if (Session["dataHoraDoPedido"] != null)
        //            e.Value = Session["dataHoraDoPedido"].ToString();
        //    }
        //}
        if (e.Column.FieldName == "dataHoraDoPedido")
        {
            if (e.Kind == GridViewAutoFilterEventKind.CreateCriteria)
            {
                Session["dataHoraDoPedido"] = e.Value;
                String[] dates = e.Value.Split('|');
                DateTime dateFrom = Convert.ToDateTime(dates[0]),
                    dateTo = Convert.ToDateTime(dates[1]);
                e.Criteria = (new OperandProperty("dataHoraDoPedido") >= dateFrom) &
                             (new OperandProperty("dataHoraDoPedido") <= dateTo);
            }
            else
            {
                if (Session["dataHoraDoPedido"] != null)
                    e.Value = Session["dataHoraDoPedido"].ToString();
            }
        }
        if (e.Column.FieldName == "vencimentoParcela")
        {
            if (e.Kind == GridViewAutoFilterEventKind.CreateCriteria)
            {
                Session["vencimentoParcela"] = e.Value;
                String[] dates = e.Value.Split('|');
                DateTime dateFrom = Convert.ToDateTime(dates[0]),
                    dateTo = Convert.ToDateTime(dates[1]);
                e.Criteria = (new OperandProperty("vencimentoParcela") >= dateFrom) &
                             (new OperandProperty("vencimentoParcela") <= dateTo);
            }
            else
            {
                if (Session["vencimentoParcela"] != null)
                    e.Value = Session["vencimentoParcela"].ToString();
            }
        }
        if (e.Column.FieldName == "pedidoIdCliente")
        {

            if (e.Kind == GridViewAutoFilterEventKind.CreateCriteria)
            {
                Session["pedidoIdCliente"] = e.Value;
                int pedidoId = rnFuncoes.retornaIdInterno(Convert.ToInt32(e.Value));
                e.Criteria = (new OperandProperty("pedidoId") == pedidoId);

                //e.Criteria = (new OperandProperty("pedidoIdCliente") == e.Value);
            }
            else
            {
                if (Session["pedidoIdCliente"] != null)
                    e.Value = null;// Session["pedidoIdCliente"].ToString();
            }

        }

        if (e.Column.FieldName == "valorCobrado")
        {
            if (e.Kind == GridViewAutoFilterEventKind.CreateCriteria)
            {
                Session["valorCobrado"] = e.Value;
                string valorCobrado = e.Value;
                e.Criteria = (new OperandProperty("valorCobrado") == valorCobrado);

                //if (Session["valorCobrado"] != null)
                //{
                //    if (String.IsNullOrEmpty(Session["valorCobrado"].ToString()))
                //    {
                //        e.Criteria.IsNull();
                //    }
                //}

            }
            else
            {
                //if (Session["valorCobrado"] != null)
                //    if (!String.IsNullOrEmpty(Session["valorCobrado"].ToString()))
                //        e.Criteria.IsNull();

                if (Session["valorCobrado"] != null)
                    e.Value = Session["valorCobrado"].ToString();



            }
        }
    }
    protected void btnChecarPedidosMl_OnClick(object sender, EventArgs e)
    {
        //var meli = mlIntegracao.verificaAutenticacao("pedidos.aspx", Request.QueryString["code"]);
        mlIntegracao.checaPedidosML();
        //rnIntegracoes.checaPedidosExtra(true);
        grd.DataBind();
    }
    protected void grd_OnCustomUnboundColumnData(object sender, ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "pedidoIdCliente")
        {
            int pedidoId = Convert.ToInt32(e.GetListSourceFieldValue("pedidoId"));
            e.Value = rnFuncoes.retornaIdCliente(pedidoId);
        }
        if (e.Column.FieldName == "statusPagamento")
        {
            //decimal totalPago = 0;
            //decimal valorCobrado = 0;
            //int situacao = 0;
            //decimal.TryParse(e.GetListSourceFieldValue("totalPago").ToString(), out totalPago);
            //decimal.TryParse(e.GetListSourceFieldValue("valorCobrado").ToString(), out valorCobrado);
            //int.TryParse(e.GetListSourceFieldValue("statusDoPedido").ToString(), out situacao);

            //if (situacao == 3 | situacao == 4 | situacao == 5 | situacao == 11)
            //{
            //    e.Value = "Pagamento Confirmado";
            //}
            //else if (totalPago == 0)
            //{
            //    e.Value = "Aguardando Pagamento";
            //    return;
            //}
            //else if (totalPago > 0 && totalPago < valorCobrado)
            //{
            //    e.Value = "Pago Parcialmente";
            //    return;
            //}
            //else if (situacao == 2)
            //{
            //    e.Value = "Aguardando Pagamento";
            //    return;
            //}

            //e.Value = "";
        }
    }
    protected void sqlPedidosServer_Selecting(object sender, DevExpress.Data.Linq.LinqServerModeDataSourceSelectEventArgs e)
    {
        if (Request.QueryString["duplicados"] != null)
        {
            //sqlPedidos.SelectMethod = "pedidoSelecionaDuplicados";

            var data = new dbCommerceDataContext();
            e.KeyExpression = "pedidoId";
            e.DefaultSorting = "pedidoId desc";
            e.QueryableSource = data.viewPedidoSelecionaDuplicados;

            lblAcao.Text = "Pedidos em Duplicidade";
        }
        else
        {
            var data = new dbCommerceDataContext();
            e.KeyExpression = "pedidoId";
            e.DefaultSorting = "pedidoId desc";
            e.QueryableSource = data.viewPedidosAdmins;

            lblAcao.Text = "Pedidos";
        }
      
    }
}
