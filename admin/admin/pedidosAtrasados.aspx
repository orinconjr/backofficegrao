﻿<%@ Page Title="" Theme="Glass" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="pedidosAtrasados.aspx.cs" Inherits="admin_pedidosAtrasados" %>

<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dxwgv" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxPopupControl" Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>

<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v11.1" Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" runat="Server">
    <%--<script language="javascript" type="text/javascript">
        function ApplyFilter(dde, dateFrom, dateTo) {
            var d1 = dateFrom.GetText();
            var d2 = dateTo.GetText();
            if (d1 == "" || d2 == "")
                return;
            dde.SetText(d1 + "|" + d2);
            //grd.ApplyFilter("[dataHoraDoPedido] >= '" + d1 + " 00:00:00' && [dataHoraDoPedido] <= '" + d2 + " 23:59:59'");
            grd.AutoFilterByColumn("dataHoraDoPedido", dde.GetText());
        }

        function OnDropDown(s, dateFrom, dateTo) {
            var str = s.GetText();
            if (str == "") {
                // default date (1950-1961 for demo)     
                dateFrom.SetDate(new Date(1950, 0, 1));
                dateTo.SetDate(new Date(1960, 11, 31));
                return;
            }
            var d = str.split("|");
            dateFrom.SetText(d[0]);
            dateTo.SetText(d[1]);
        }
    </script>--%>

    <script language="javascript" type="text/javascript">
        function OnDropDownDataPedido(s, dateFrom, dateTo) {
            var str = s.GetText();
            if (str == "") {
                dateFrom.SetDate(new Date(1950, 0, 1));
                dateTo.SetDate(new Date(1960, 11, 31));
                return;
            }
            var d = str.split("|");
            dateFrom.SetText(d[0]);
            dateTo.SetText(d[1]);
        }

        function ApplyFilter(dde, dateFrom, dateTo, campo) {
            var colunaSplit = dde.name.split('_');
            var coluna = colunaSplit[colunaSplit.length - 1].replace("DXFREditorcol", "");
            var colunaFiltro = "";
            if (coluna == "4") colunaFiltro = "dataHoraDoPedido";
            if (coluna == "5") colunaFiltro = "prazoFinalPedido";

            var d1 = dateFrom.GetText();
            var d2 = dateTo.GetText();
            if (d1 == "" || d2 == "") return;
            dde.SetText(d1 + "|" + d2);
            grd.AutoFilterByColumn(colunaFiltro, d1 + "|" + d2);
        }
        function OnDropDown(s, dateFrom, dateTo) {
            var str = s.GetText();
            if (str == "") {
                dateFrom.SetDate(new Date(1950, 0, 1));
                dateTo.SetDate(new Date(1960, 11, 31));
                return;
            }
            var d = str.split("|");
            dateFrom.SetText(d[0]);
            dateTo.SetText(d[1]);
        }
        function grid_ContextMenu(s, e) {
            if (e.objectType == "header")
                pmColumnMenu.ShowAtPos(ASPxClientUtils.GetEventX(e.htmlEvent), ASPxClientUtils.GetEventY(e.htmlEvent));
        }
    </script>

    <dx:ASPxPopupControl ID="popAlterarRastreio" runat="server" CloseAction="CloseButton" Modal="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
        ClientInstanceName="popAlterarRastreio" HeaderText="Envio Parcial" AllowDragging="True" PopupAnimationType="None" EnableViewState="True" Height="200" AutoUpdatePosition="True">
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl4" runat="server">
                <div style="width: 300px; height: 100px;">
                    <table width="100%">
                        <tr class="rotulos">
                            <td class="rotulos">Motivo do Atraso
                            </td>
                            <td class="rotulos">
                                <asp:DropDownList runat="server" ID="ddlMotivoAtraso">
                                    <Items>
                                        <asp:ListItem Value="Troca Defeito" Text="Troca Defeito"></asp:ListItem>
                                        <asp:ListItem Value="Troca Substituição Produto" Text="Troca Substituição Produto"></asp:ListItem>
                                        <asp:ListItem Value="Presente" Text="Presente"></asp:ListItem>
                                        <asp:ListItem Value="Sinistro" Text="Sinistro"></asp:ListItem>
                                        <asp:ListItem Value="Inclusão de Produto" Text="Inclusão de Produto"></asp:ListItem>
                                        <asp:ListItem Value="Atraso de enxoval" Text="Atraso de enxoval"></asp:ListItem>
                                        <asp:ListItem Value="Atraso de móveis" Text="Atraso de móveis"></asp:ListItem>
                                        <asp:ListItem Value="Atraso de MDF" Text="Atraso de MDF"></asp:ListItem>
                                    </Items>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="rotulos" colspan="2" style="padding-top: 15px;">
                                <asp:HiddenField runat="server" ID="hdfPedidoAlterando" />
                                <asp:Button runat="server" ID="btnAlterarPedido" OnClick="btnAlterarPedido_OnClick" Text="Gravar" />
                            </td>
                        </tr>
                    </table>
                </div>
            </dx:PopupControlContentControl>
        </ContentCollection>
        <ContentStyle>
            <Paddings PaddingBottom="5px" />
        </ContentStyle>
    </dx:ASPxPopupControl>

    <table cellpadding="0" cellspacing="0" style="width: 920px">
        <tr>
            <td class="tituloPaginas" valign="top">
                <asp:Label ID="lblAcao" runat="server">Pedidos Atrasados</asp:Label>
            </td>
        </tr>
        <tr>
            <td>

                <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
                    <tr>
                        <td align="right">
                            <table cellpadding="0" cellspacing="0" style="width: 834px">
                                <tr>
                                    <td align="right">&nbsp;</td>
                                    <td height="38"
                                        style="background-image: url('images/bkExportar.jpg'); padding-bottom: 1px;"
                                        valign="bottom" width="231">
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 97px">&nbsp;</td>
                                                <td style="width: 33px">
                                                    <asp:ImageButton ID="btPdf" runat="server" ImageUrl="~/admin/images/btPdf.jpg" OnClick="btPdf_Click"
                                                        Style="margin-left: 5px" />
                                                </td>
                                                <td style="width: 31px">
                                                    <asp:ImageButton ID="btXsl" runat="server" ImageUrl="~/admin/images/btXls.jpg" OnClick="btXsl_Click"
                                                        Style="margin-left: 5px" />
                                                </td>
                                                <td style="width: 36px">
                                                    <asp:ImageButton ID="btRtf" runat="server" ImageUrl="~/admin/images/btRtf.jpg" OnClick="btRtf_Click"
                                                        Style="margin-left: 5px" />
                                                </td>
                                                <td valign="bottom">
                                                    <asp:ImageButton ID="btCsv" runat="server" ImageUrl="~/admin/images/btCsv.jpg" OnClick="btCsv_Click"
                                                        Style="margin-left: 5px" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td style="height: 34px">Completos: </td>
                                    <td style="height: 34px">
                                        <asp:RadioButtonList runat="server" ID="rblFiltro" RepeatDirection="Horizontal" OnSelectedIndexChanged="rblFiltro_OnSelectedIndexChanged" AutoPostBack="True">
                                            <asp:ListItem Value="0" Text="Não"></asp:ListItem>
                                            <asp:ListItem Value="1" Text="Todos" Selected="True"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Button ID="btnVerResumo" runat="server" Text="Ver resumo" OnClick="btnVerResumo_Click" />
                        </td>
                    </tr>
                    <tr id="trResumo" runat="server" visible="false">
                        <td>
                            <table style="width: 100%; vertical-align: text-top" cellpadding="0" cellspacing="0">
                                <tr style="text-align: center">
                                    <td style="width: 50%">
                                        <h2>Resumo - Motivo de Atraso</h2>
                                    </td>
                                    <td style="width: 50%">
                                        <h2>Resumo - Fornecedores </h2>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 50%; vertical-align: top">
                                        <dxwgv:ASPxGridView ID="grdResumoMotivoAtraso" ClientInstanceName="grdRM" runat="server" AutoGenerateColumns="false" Width="417px">
                                            <SettingsBehavior AllowSort="true" />
                                            <SettingsPager Mode="ShowAllRecords"></SettingsPager>
                                            <Columns>
                                                <dxwgv:GridViewDataColumn Caption="Motivo do Atraso" FieldName="motivoAtraso" Width="400px"></dxwgv:GridViewDataColumn>
                                                <dxwgv:GridViewDataColumn Caption="Quantidade" FieldName="qtdMotivoAtraso" Width="50px"></dxwgv:GridViewDataColumn>
                                                <dxwgv:GridViewDataColumn Caption="Percentual" FieldName="percentualMotivo"  Width="50px"></dxwgv:GridViewDataColumn>
                                            </Columns>
                                        </dxwgv:ASPxGridView>
                                    </td>
                                    <td style="width: 50%; vertical-align: top">
                                        <dxwgv:ASPxGridView ID="grdResumoFornecedores" ClientInstanceName="grdRF" runat="server" AutoGenerateColumns="false" Width="417px">
                                            <SettingsBehavior AllowSort="true" />
                                            <SettingsPager Mode="ShowAllRecords"></SettingsPager>
                                            <Columns>
                                                <dxwgv:GridViewDataColumn Caption="Fornecedor" FieldName="fornecedor" Width="400px"></dxwgv:GridViewDataColumn>
                                                <dxwgv:GridViewDataColumn Caption="Quantidade" FieldName="qtdFornecedor" Width="50px"></dxwgv:GridViewDataColumn>
                                                <dxwgv:GridViewDataColumn Caption="Percentual" FieldName="percentualFornecedor" Width="50px"></dxwgv:GridViewDataColumn>
                                            </Columns>
                                        </dxwgv:ASPxGridView>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <dxwgv:ASPxGridView ID="grd" ClientInstanceName="grd" runat="server" AutoGenerateColumns="False"
                                OnAutoFilterCellEditorCreate="grid_AutoFilterCellEditorCreate" OnAutoFilterCellEditorInitialize="grid_AutoFilterCellEditorInitialize"
                                OnProcessColumnAutoFilter="grid_ProcessColumnAutoFilter" KeyFieldName="pedidoId" Width="834px" Settings-ShowFilterRow="True" Settings-ShowFilterBar="Visible"
                                Cursor="auto" OnCustomUnboundColumnData="grd_OnCustomUnboundColumnData" OnHtmlRowCreated="grd_HtmlRowCreated">
                                <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                                <Styles>
                                    <Footer Font-Bold="True">
                                    </Footer>
                                </Styles>
                                <SettingsText ConfirmDelete="Tem certeza que deseja excluir?" EmptyDataRow="Nenhum registro encontrado." />
                                <SettingsPager Position="TopAndBottom" PageSize="50"
                                    ShowDisabledButtons="False" AlwaysShowPager="True">
                                    <Summary AllPagesText="Páginas: {0} - {1} ({2} items)"
                                        Text="Página {0} de {1} ({2} registros encontrados)" />
                                </SettingsPager>
                                <Settings ShowFilterRow="True" ShowGroupButtons="False" ShowFooter="True" />
                                <TotalSummary>
                                    <dxwgv:ASPxSummaryItem DisplayFormat="c" FieldName="valorCobrado"
                                        ShowInColumn="Valor" ShowInGroupFooterColumn="Valor" SummaryType="Sum" />
                                </TotalSummary>
                                <SettingsEditing EditFormColumnCount="4"
                                    PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1"
                                    PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter"
                                    PopupEditFormWidth="700px" />
                                <Columns>
                                    <dxwgv:GridViewDataTextColumn Caption="Id Cliente" FieldName="pedidoIdCliente" VisibleIndex="0" UnboundType="Integer">
                                        <EditFormSettings Visible="False" />
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Id" FieldName="pedidoId" Visible="False"
                                        VisibleIndex="0">
                                        <EditFormSettings Visible="False" />
                                    </dxwgv:GridViewDataTextColumn>
                                    <%--                                    <dxwgv:GridViewDataTextColumn Caption="produtoId" FieldName="produtoId" 
                                        VisibleIndex="3">
                                        <EditFormSettings Visible="False" />
                                    </dxwgv:GridViewDataTextColumn>--%>
                                    <dxwgv:GridViewDataTextColumn Caption="Nome" FieldName="clienteNome"
                                        VisibleIndex="1" Width="150px" Settings-AutoFilterCondition="Contains">
                                        <PropertiesTextEdit>
                                            <ValidationSettings>

                                                <RequiredField ErrorText="Preencha o nome." IsRequired="True" />

                                            </ValidationSettings>

                                        </PropertiesTextEdit>
                                        <Settings AutoFilterCondition="Contains" />
                                        <EditFormSettings CaptionLocation="Top" />
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Valor" FieldName="valorCobrado" Visible="False"
                                        VisibleIndex="2" Width="70px" Settings-AutoFilterCondition="Contains" Settings-FilterMode="DisplayText">
                                        <PropertiesTextEdit DisplayFormatString="c">
                                        </PropertiesTextEdit>
                                        <Settings FilterMode="DisplayText" AutoFilterCondition="Contains"></Settings>
                                        <EditFormSettings Visible="False" />
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataDateColumn Name="dataHoraDoPedido" Caption="Data" FieldName="dataHoraDoPedido" VisibleIndex="3" Width="120px">
                                        <PropertiesDateEdit DisplayFormatString="g"></PropertiesDateEdit>
                                    </dxwgv:GridViewDataDateColumn>
                                    <dxwgv:GridViewDataDateColumn Name="prazoFinalPedido" Caption="Prazo Final Cliente" FieldName="prazoFinalPedido" VisibleIndex="3" Width="120px">
                                        <PropertiesDateEdit DisplayFormatString="g"></PropertiesDateEdit>
                                    </dxwgv:GridViewDataDateColumn>
                                    <dxwgv:GridViewDataDateColumn Name="prazoMaximoPostagemAtualizado" Caption="Prazo Postagem Atualizado" FieldName="prazoMaximoPostagemAtualizado" VisibleIndex="3" Width="120px">
                                        <PropertiesDateEdit DisplayFormatString="g"></PropertiesDateEdit>
                                    </dxwgv:GridViewDataDateColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Motivo" FieldName="motivoAtraso">
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Motivo Fornecedor" FieldName="motivoFornecedor">
                                        <DataItemTemplate>
                                            <asp:Label ID="lblMotivoFornecedor" runat="server" Text=""></asp:Label>
                                        </DataItemTemplate>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Completo" FieldName="completo">
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Aguard. Person." FieldName="personalizadosPendentes">
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Fornecedor" FieldName="fornecedorPendente">
                                    </dxwgv:GridViewDataTextColumn>

                                    <dxwgv:GridViewDataTextColumn Caption="Alterar" FieldName="pedidoId">
                                        <DataItemTemplate>
                                            <asp:Button runat="server" OnCommand="alterarPedido" CommandArgument='<%# Eval("pedidoId") %>' Text="Alterar" />
                                        </DataItemTemplate>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataHyperLinkColumn Caption="Editar" FieldName="pedidoId"
                                        VisibleIndex="7" Width="30px">
                                        <PropertiesHyperLinkEdit ImageUrl="~/admin/images/btEditar.jpg"
                                            NavigateUrlFormatString="pedido.aspx?pedidoId={0}" Target="_blank">
                                        </PropertiesHyperLinkEdit>
                                        <Settings AllowAutoFilter="False" />
                                        <HeaderTemplate>
                                            <img alt="" src="images/legendaEditar.jpg" />
                                        </HeaderTemplate>
                                    </dxwgv:GridViewDataHyperLinkColumn>
                                    <dxwgv:GridViewCommandColumn ButtonType="Image" VisibleIndex="8">
                                        <ClearFilterButton Visible="True">
                                            <Image Url="~/admin/images/btExcluir.jpg" />
                                        </ClearFilterButton>
                                    </dxwgv:GridViewCommandColumn>
                                </Columns>
                                <StylesEditors>
                                    <Label Font-Bold="True">
                                    </Label>
                                </StylesEditors>
                            </dxwgv:ASPxGridView>
                            <dxwgv:ASPxGridViewExporter ID="grdEx" runat="server" FileName="pedidos"
                                GridViewID="grd" Landscape="True" PreserveGroupRowStates="True">
                            </dxwgv:ASPxGridViewExporter>
                            <asp:ObjectDataSource ID="sqlPedidos" runat="server" SelectMethod="pedidoSelecionaAdmin"
                                TypeName="rnPedidos"></asp:ObjectDataSource>
                            <asp:ObjectDataSource ID="sqlSituacao" runat="server" SelectMethod="situacaoSeleciona"
                                TypeName="rnSituacao"></asp:ObjectDataSource>
                            <asp:ObjectDataSource ID="sqlSituacaoInterna" runat="server" SelectMethod="situacaoInternaSeleciona"
                                TypeName="rnSituacao"></asp:ObjectDataSource>
                            <asp:ObjectDataSource ID="sqlPagamentos" runat="server" SelectMethod="condicaoDePagamentoSeleciona"
                                TypeName="rnCondicoesDePagamento"></asp:ObjectDataSource>
                            <asp:ObjectDataSource ID="sqlTipoDeEntrega" runat="server" SelectMethod="tipoDeEntregaSeleciona" TypeName="rnTipoDeEntrega"></asp:ObjectDataSource>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
