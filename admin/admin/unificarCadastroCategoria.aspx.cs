﻿using DevExpress.Web.ASPxEditors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_unificarCadastroCategoria : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
        usuarioLogadoId = Request.Cookies["usuarioLogadoId"];
        if (usuarioLogadoId != null)
        {
            if (!IsPostBack && !IsCallback)
            {
                fillDrops();
                fillProdutos();
            }
        }
        else
        {
            Response.Redirect("default.aspx");
        }
    }

    private void fillProdutos()
    {

        int pagina = Convert.ToInt32(hdfPaginaAtual.Value);
        int take = 100;
        var data = new dbCommerceDataContext();

        dynamic produtos;

        if (ddlCategoria.SelectedIndex > 0)
        {
            produtos = (from c in data.tbProdutos
                        where (c.flagConferido == false || c.flagConferido == null) && c.categoriaId == (int.Parse(ddlCategoria.SelectedItem.Value.ToString()))
                        select new
                        {
                            c.produtoId,
                            c.produtoNome,
                            c.fotoDestaque,
                            c.produtoFornecedor,
                            c.produtoMarca,
                            c.produtoUrl
                        }).Distinct().Take(take);
        }
        else
        {
            produtos = (from c in data.tbProdutos
                        where (c.flagConferido == false || c.flagConferido == null)
                        select new
                        {
                            c.produtoId,
                            c.produtoNome,
                            c.fotoDestaque,
                            c.produtoFornecedor,
                            c.produtoMarca,
                            c.produtoUrl
                        }).Distinct().Take(take);
        }

        lstProdutos.DataSource = produtos;
        lstProdutos.DataBind();
    }

    private void fillDrops()
    {
        if (!Page.IsPostBack && !Page.IsCallback)
        {
            var data = new dbCommerceDataContext();
            var categorias = (from c in data.tbCategorias where c.categoriaPaiId == 0 orderby c.nomeExibicao select c);

            ddlCategoria.TextField = "nomeExibicao";
            ddlCategoria.ValueField = "categoriaId";
            ddlCategoria.DataSource = categorias;
            ddlCategoria.DataBind();

            ddlCategoria.Items.Insert(0, new DevExpress.Web.ASPxEditors.ListEditItem() { Index = 0, Text = "Todas as categorias", Value = "0" });
            ddlCategoria.SelectedIndex = 0;
        }

    }

    protected void btnGravar_Click(object sender, EventArgs e)
    {
        try
        {
            var data = new dbCommerceDataContext();

            foreach (ListViewItem item in lstProdutos.Items)
            {
                ASPxComboBox cbo = (ASPxComboBox)item.FindControl("ddlCategoriaProduto");
                CheckBox chk = (CheckBox)item.FindControl("chkConferido");
                HiddenField hdnProdutoId = (HiddenField)item.FindControl("hdfProdutoId");

                var produto = (from c in data.tbProdutos where c.produtoId == int.Parse(hdnProdutoId.Value.ToString()) select c).FirstOrDefault();

                if (cbo.SelectedIndex > 0)
                    produto.categoriaId = int.Parse(cbo.SelectedItem.Value.ToString());

                if (chk != null && chk.Checked)
                    produto.flagConferido = true;
                data.SubmitChanges();

                chk.Checked = false;

            }
            fillProdutos();
        }
        catch (Exception ex)
        {
            lblMensagem.Text = "Erro: " + lblMensagem.Text;
        }
    }

    protected void lstProdutos_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        try
        {
            var data = new dbCommerceDataContext();

            if (e.Item.ItemType == ListViewItemType.DataItem)
            {
                HiddenField hdnProdutoId = (HiddenField)e.Item.FindControl("hdfProdutoId");
                CheckBox chk = (CheckBox)e.Item.FindControl("chkConferido");
                chk.Checked = false;
                var produto = (from c in data.tbProdutos where c.produtoId == int.Parse(hdnProdutoId.Value.ToString()) select c).FirstOrDefault();
                ASPxComboBox cbo = (ASPxComboBox)e.Item.FindControl("ddlCategoriaProduto");
                cbo.DataSourceID = "sqlCategoria";
                cbo.DataBind();
                cbo.Items.Insert(0, new ListEditItem() { Value = "0", Text = "Sem categoria" });
                cbo.SelectedIndex = 0;

                if (produto.categoriaId != null && produto.categoriaId > 0)
                {
                    var categoria = (from c in data.tbCategorias where c.categoriaId == produto.categoriaId select c).FirstOrDefault();
                    cbo.Value = categoria.categoriaId.ToString();
                    
                }
            }
        }
        catch (Exception ex)
        {
            lblMensagem.Text = "Erro: " + ex.Message;
        }
    }

    protected void lstProdutos_ItemCommand(object sender, ListViewCommandEventArgs e)
    {
        try
        {

        }
        catch (Exception ex)
        {
            lblMensagem.Text = "Erro: " + ex.Message;
        }
    }

    protected void chkConferido_CheckedChanged(object sender, EventArgs e)
    {
        try
        {

        }
        catch (Exception ex)
        {
            lblMensagem.Text = "Erro: " + ex.Message;
        }
    }

    protected void btnCarregar_Click(object sender, EventArgs e)
    {
        try
        {
            fillProdutos();
        }
        catch (Exception ex)
        {
            lblMensagem.Text = "Erro: " + ex.Message;
        }
    }
}