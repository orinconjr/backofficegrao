﻿<%@ Page Title="" Theme="Glass" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="descricaoTabelaGrupo.aspx.cs" Inherits="admin_descricaoTabelaGrupo" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>


<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" Runat="Server">
    <link href="estilos/tabelaDescricao.css" rel="stylesheet" />
    <style type="text/css">
        .btnAdicionarProduto {
            height: 27px;
        }
    </style>
    <table cellpadding="0" cellspacing="0" style="width: 920px">
        <tr>
            <td class="tituloPaginas" valign="top">
                <asp:Label ID="lblAcao" runat="server">Tabela de Descrição - Grupos</asp:Label>
                </td>
        </tr>
        <tr>
            <td>
                <asp:Panel runat="server" ID="pnItens">
                    <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-top: 10px; padding-bottom: 15px; text-align: right;">
                            <asp:Button runat="server" ID="btnAdicionar" OnClick="btnAdicionar_OnClick" Text="Adicionar Novo Produto"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" style="width: 834px">
                                <tr>
                                    <td valign="top">
                                        <dxwgv:ASPxGridView ID="grd" runat="server" AutoGenerateColumns="False" KeyFieldName="idDescricaoTabelaProdutoGrupo" Width="834px" Cursor="auto" EnableCallBacks="False">
                                            <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                                            <SettingsText ConfirmDelete="Tem certeza que deseja excluir?" emptydatarow="Nenhum registro encontrado." />
                                            <SettingsPager Position="TopAndBottom" PageSize="50" ShowDisabledButtons="False" AlwaysShowPager="True">
                                                <Summary AllPagesText="Páginas: {0} - {1} ({2} items)" Text="Página {0} de {1} ({2} registros encontrados)" />
                                            </SettingsPager>
                                            <settings showfilterrow="True" ShowGroupButtons="False" />
                                            <SettingsEditing EditFormColumnCount="4" 
                                                PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1" 
                                                PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter" 
                                                PopupEditFormWidth="700px" />
                                            <Columns>
                                                <dxwgv:GridViewDataTextColumn Caption="ID" FieldName="idDescricaoTabelaProdutoGrupo" ReadOnly="True" VisibleIndex="0" Width="80px">
                                                    <Settings AutoFilterCondition="Contains" />
                                                    <editformsettings visible="False" CaptionLocation="Top" VisibleIndex="2" />
                                                </dxwgv:GridViewDataTextColumn>
                                                <dxwgv:GridViewDataComboBoxColumn Caption="Site" FieldName="idSite" VisibleIndex="4" Width="130px">
                                                    <PropertiesComboBox DataSourceID="sqlSite" TextField="nome" ValueField="idSite" ValueType="System.String">
                                                    </PropertiesComboBox>
                                                </dxwgv:GridViewDataComboBoxColumn>
                                                <dxwgv:GridViewDataTextColumn Caption="Nome Interno" FieldName="nomeInterno" ReadOnly="True" VisibleIndex="0" Width="80px">
                                                    <Settings AutoFilterCondition="Contains" />
                                                    <editformsettings visible="False" CaptionLocation="Top" VisibleIndex="2" />
                                                </dxwgv:GridViewDataTextColumn>
                                                <dxwgv:GridViewDataTextColumn Caption="Nome Amigável" FieldName="nomeAmigavel" ReadOnly="True" VisibleIndex="0" Width="80px">
                                                    <Settings AutoFilterCondition="Contains" />
                                                    <editformsettings visible="False" CaptionLocation="Top" VisibleIndex="2" />
                                                </dxwgv:GridViewDataTextColumn>
                                                <dxwgv:GridViewDataTextColumn Caption="Editar" FieldName="idDescricaoTabelaProdutoGrupo" VisibleIndex="7" Width="30px">
                                                    <DataItemTemplate>
                                                        <asp:ImageButton runat="server" ImageUrl="~/admin/images/btEditar.jpg" ID="btnEditar" CommandArgument='<%# Eval("idDescricaoTabelaProdutoGrupo") %>' OnCommand="btnEditar_OnCommand"  />
                                                    </DataItemTemplate>
                                                    <Settings AllowAutoFilter="False" />
                                                    <HeaderTemplate>
                                                        <img alt="" src="images/legendaEditar.jpg" />
                                                    </HeaderTemplate>
                                                </dxwgv:GridViewDataTextColumn>
                                            </Columns>
                                            <StylesEditors>
                                                <Label Font-Bold="True">
                                                </Label>
                                            </StylesEditors>
                                        </dxwgv:ASPxGridView>
                                        <asp:LinqDataSource ID="sqlSite" runat="server" 
                                            ContextTypeName="dbCommerceDataContext" EnableDelete="True" 
                                            EnableInsert="True" EnableUpdate="True" TableName="tbSites">
                                        </asp:LinqDataSource>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                    </tr>
                </table>
                </asp:Panel>
                <asp:Panel runat="server" ID="pnNovo" Visible="False">
                    <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
                        <tr>
                            <td>
                                &nbsp;
                                <asp:TextBox runat="server" ID="txtListaProdutos" Visible="false"></asp:TextBox>
                                <asp:HiddenField runat="server" ID="hdfProdutoAlterar"/>
                            </td>
                        </tr>
                        <tr class="rotulos">
                            <td>
                                Site
                            </td>
                        </tr>
                        <tr class="campos">
                            <td>
                                <asp:DropDownList AutoPostBack="True" OnSelectedIndexChanged="ddlSite_OnSelectedIndexChanged" runat="server" CssClass="campos" ID="ddlSite" DataValueField="idSite" DataTextField="nome" />
                            </td>
                        </tr>
                        <tr class="rotulos">
                            <td>
                                Nome interno:
                            </td>
                        </tr>
                        <tr class="campos">
                            <td>
                                <asp:TextBox runat="server" ID="txtNomeInterno" CssClass="campos" Width="100%"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="rotulos">
                            <td style="padding-top: 10px;">
                                Título para o Site:
                            </td>
                        </tr>
                        <tr class="campos">
                            <td>
                                <asp:TextBox runat="server" ID="txtNomeSite" CssClass="campos" Width="100%"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="rotulos">
                            <td style="padding-top: 10px; font-weight: bold;">
                                Produtos
                            </td>
                        </tr>
                        <tr class="rotulos">
                            <td style="padding-top: 10px;">
                                <table>
                                    <tr class="rotulos">
                                        <td>
                                            Produto:<br/>
                                            <asp:DropDownList runat="server" ID="ddlProdutos" Width="735px" CssClass="campos" DataValueField="idDescricaoTabelaProduto" DataTextField="nomeInterno"/>
                                        </td>
                                        <td style="padding-top: 15px">
                                            <asp:Button runat="server" ID="btnAdicionarProduto" Text="Adicionar" OnClick="btnAdicionarDescricao_OnClick"/>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr class="rotulos">
                            <td style="padding-top: 10px;">
                                <table>
                                    <tr class="rotulos" style="font-weight: bold;">
                                        <td>
                                            Produto:
                                        </td>
                                        <td style="padding-left: 10px;">
                                            Remover
                                        </td>
                                    </tr>
                                    <asp:ListView runat="server" ID="lstItensAdicionar" OnItemDataBound="lstItensAdicionar_OnItemDataBound">
                                        <ItemTemplate>
                                            <tr class="rotulos">
                                                <td style="padding-left: 10px; width: 625px">
                                                    <asp:Literal runat="server" ID="litNomeAmigavel"></asp:Literal>
                                                </td>
                                                <td style="padding-left: 10px;">
                                                    <asp:LinkButton runat="server" ID="btnRemoverProduto" OnCommand="btnRemoverProduto_OnCommand" CommandArgument='<%# Eval("idDescricaoTabelaProdutoGrupoItem") %>'>
                                                        Remover Produto
                                                    </asp:LinkButton>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:ListView>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-top: 30px;">
                                <table class="templateDetalheTabela" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td class="templateDetalheTituloBloco" colspan="3"><asp:Literal runat="server" ID="litNomeAmigavel"></asp:Literal></td>
                                    </tr>
                                    <tr>
                                        <td class="templateDetalheTituloBlocoDivisor">
                                        </td>
                                    </tr>
                                    <tr class="templateDetalheTexto">
                                        <td class="templateDetalheNome" width="138"> Produto </td>
                                        <td class="templateDetalheQuantidade" width="32"> Qtd. </td>
                                        <td class="templateDetalheDescricao" width="456"> Descrição </td>
                                    </tr>
                                    <asp:ListView runat="server" ID="lstProdutosSimulacao" OnItemDataBound="lstProdutosSimulacao_OnItemDataBound">
                                        <ItemTemplate>
                                            <tr class="templateDetalheTitulo">
                                                <td class="templateDetalheNome">
                                                    <asp:Literal runat="server" ID="litNomeAmigavel"></asp:Literal> 
                                                </td>
                                                <td class="templateDetalheQuantidade">
                                                    <br>
                                                </td>
                                                <td class="templateDetalheDescricao">
                                                    <br>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="templateDetalheNomeDetalhe">
                                                    <br>
                                                </td>
                                                <td class="templateDetalheQuantidadeDetalhe">
                                                    <asp:ListView runat="server" ID="lstProdutosSimulacaoItensQuantidades">
                                                        <ItemTemplate>
                                                            <div>
                                                                <%# Eval("quantidade") %>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:ListView>
                                                </td>
                                                <td class="templateDetalheDescricaoDetalhe">
                                                    <asp:ListView runat="server" ID="lstProdutosSimulacaoItensDescricoes">
                                                        <ItemTemplate>
                                                            <div>
                                                                <%# Eval("descricao") %>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:ListView>
                                                </td>
                                                </tr>
                                        </ItemTemplate>
                                    </asp:ListView>
                                </table>
                            </td>
                        </tr>
                        <tr class="campos">
                            <td style="padding-top: 15px; text-align: right;">
                                <asp:Button runat="server" ID="btnCancelarProduto" Text="Cancelar" OnClick="btnCancelarProduto_OnClick"/>&nbsp;&nbsp;
                                <asp:Button runat="server" ID="btnGravarProduto" Text="Gravar Grupo de Produtos" OnClick="btnGravarProduto_OnClick"/>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
    </table>
</asp:Content>