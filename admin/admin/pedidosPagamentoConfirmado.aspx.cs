﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxGridView;

public partial class admin_pedidosPagamentoConfirmado : System.Web.UI.Page
{
    private static int pedidoId;
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void grdDetalhes_BeforePerformDataSelect(object sender, EventArgs e)
    {
        Session["pedidoId"] = (sender as ASPxGridView).GetMasterRowKeyValue();
        pedidoId = Convert.ToInt32((sender as ASPxGridView).GetMasterRowKeyValue());
    }

    protected void grd_OnCustomUnboundColumnData(object sender, ASPxGridViewColumnDataEventArgs e)
    {

    }

    protected void grdDetalhes_OnInit(object sender, EventArgs e)
    {
        ASPxGridView detgrid = sender as ASPxGridView;
        if (detgrid == null) return;
        var produtoId = (int)(sender as ASPxGridView).GetMasterRowFieldValues("produtoId");


        detgrid.DataSource = rnPedidos.pedidosProdutoNaoEncomendado(produtoId);
        detgrid.DataBind();
    }
}