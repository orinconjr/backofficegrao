﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="destaques.aspx.cs" Theme="Glass" Inherits="admin_destaques" %>

<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dxwgv" %>

<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<%@ Register assembly="CuteEditor" namespace="CuteEditor" tagprefix="CE" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" Runat="Server">
    <table cellpadding="0" cellspacing="0" style="width: 920px">
    <tr>
        <td class="tituloPaginas" valign="top">
            <asp:Label ID="lblAcao" runat="server">Destaques</asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            
    <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
        <tr>
            <td align="right">
                <table cellpadding="0" cellspacing="0" style="width: 834px">
                    <tr>
                        <td align="right">
                            &nbsp;</td>
                        <td height="38" 
                            style="background-image: url('images/bkExportar.jpg'); padding-bottom: 1px;" 
                            valign="bottom" width="231">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="width: 97px">
                                        &nbsp;</td>
                                    <td style="width: 33px">
                                        <asp:ImageButton ID="btPdf" runat="server" ImageUrl="~/admin/images/btPdf.jpg" 
                                            style="margin-left: 5px" />
                                    </td>
                                    <td style="width: 31px">
                                        <asp:ImageButton ID="btXsl" runat="server" ImageUrl="~/admin/images/btXls.jpg" 
                                            style="margin-left: 5px" />
                                    </td>
                                    <td style="width: 36px">
                                        <asp:ImageButton ID="btRtf" runat="server" ImageUrl="~/admin/images/btRtf.jpg" 
                                            style="margin-left: 5px" />
                                    </td>
                                    <td valign="bottom">
                                        <asp:ImageButton ID="btCsv" runat="server" ImageUrl="~/admin/images/btCsv.jpg" 
                                            style="margin-left: 5px" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                <dxwgv:ASPxGridView ID="grd" runat="server" AutoGenerateColumns="False" 
                    DataSourceID="sqlDestaque" KeyFieldName="produtoDestaqueId" Width="834px" 
                    Cursor="auto" onhtmlrowcreated="grd_HtmlRowCreated" 
                    onrowupdating="grd_RowUpdating" onrowinserting="grd_RowInserting">
                    <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                    <SettingsText ConfirmDelete="Tem certeza que deseja excluir?" 
                        emptydatarow="Nenhum registro encontrado." />
                    <SettingsPager Position="TopAndBottom" PageSize="50" 
                        ShowDisabledButtons="False" AlwaysShowPager="True">
                        <Summary AllPagesText="Páginas: {0} - {1} ({2} items)" 
                            Text="Página {0} de {1} ({2} registros encontrados)" />
                    </SettingsPager>
                    <settings showfilterrow="True" ShowGroupButtons="False" />
                    <SettingsEditing EditFormColumnCount="4" 
                        PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1" 
                        PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter" 
                        PopupEditFormWidth="700px" />
                    <Columns>
                        <dxwgv:GridViewDataTextColumn Caption="ID do Destaque" 
                            FieldName="produtoDestaqueId" ReadOnly="True" VisibleIndex="0">
                            <Settings AutoFilterCondition="Contains" />
                            <EditFormSettings Visible="False" />
                        </dxwgv:GridViewDataTextColumn>
                          <dxwgv:GridViewDataColumn Caption="Nome da página" FieldName="nomeDaPagina" 
                            Visible="True" VisibleIndex="2" Width="240">
                             <EditItemTemplate>
                                    <asp:DropDownList ID="drpNomeDaPagina" runat="server" Width="200px" SelectedValue='<%# Bind("nomeDaPagina") %>'>
                                        <asp:ListItem Value="">Selecione</asp:ListItem>
                                        <asp:ListItem Value="categoriaHome.aspx">Categoria</asp:ListItem>
                                        <asp:ListItem Value="default.aspx">Default</asp:ListItem>
                                        <asp:ListItem Value="lancamentos.aspx">Lançamentos</asp:ListItem>
                                        <asp:ListItem Value="promocoes.aspx">Promoções</asp:ListItem>
                                    </asp:DropDownList>
                                </EditItemTemplate>
                            <EditFormSettings CaptionLocation="Top" Visible="True" />
                        </dxwgv:GridViewDataColumn>
                        <dxwgv:GridViewDataComboBoxColumn Caption="Categoria" FieldName="categoriaId" 
                            Visible="True" VisibleIndex="2" Width="240">
                            <PropertiesComboBox DataSourceID="sqlCategoria" TextField="CategoriaNome" 
                                ValueField="categoriaId" ValueType="System.String">
                            </PropertiesComboBox>
                            <DataItemTemplate>
                            <asp:Label ID="lblCategoria" runat="server" Text='<%# Bind("categoriaNome") %>'></asp:Label>
                            </DataItemTemplate>
                            <EditFormSettings CaptionLocation="Top" Visible="True" />
                             <EditItemTemplate>
                                <asp:DropDownList ID="drpCategorias" 
                                    runat="server" CssClass="campos" 
                                    DataSourceID="sqlCategoria" DataTextField="categoriaNome" 
                                    DataValueField="categoriaId" Width="200px" 
                                    AppendDataBoundItems="True"  SelectedValue='<%# Bind("categoriaId") %>'>
                               <asp:ListItem Value="">Selecione</asp:ListItem>
                               <asp:ListItem Value="0">Nenhuma</asp:ListItem>
                                </asp:DropDownList> 
                             </EditItemTemplate>
                        </dxwgv:GridViewDataComboBoxColumn>
                        <dxwgv:GridViewDataColumn Caption="Destaque" FieldName="destaque" Visible="False" VisibleIndex="2">
                             <EditItemTemplate>
                              <CE:Editor ID="txtDestaque" runat="server" 
                                                  AutoConfigure="Full_noform" BaseHref="" ContextMenuMode="Simple" 
                                                  EditorWysiwygModeCss="~/admin/estilos/estilosEditor.css" FilesPath="" Height="600px" 
                                                  URLType="Default" UseRelativeLinks="False" 
                                                  Width="782px" 
                                                  RemoveServerNamesFromUrl="False" Text='<%# Eval("destaque") %>'>
                                                  <TextAreaStyle CssClass="campos" />
                                               <%--   <FrameStyle BackColor="White" BorderColor="#DDDDDD" BorderStyle="Solid" 
                                                      BorderWidth="1px" CssClass="CuteEditorFrame" Height="100%" Width="100%" />--%>
                              </CE:Editor>
                             </EditItemTemplate>
                            <editformsettings captionlocation="Top" visible="True" ColumnSpan="4" 
                                VisibleIndex="3" />
                        </dxwgv:GridViewDataColumn>
                        <dxwgv:GridViewCommandColumn VisibleIndex="4" Width="90px" ButtonType="Image">
                            <editbutton visible="True" Text="Editar">
                                <Image Url="~/admin/images/btEditar.jpg" />
                            </editbutton>
                            <NewButton Visible="True" Text="Novo">
                                <Image Url="~/admin/images/btNovo.jpg" />
                            </NewButton>
                            <deletebutton visible="True" Text="Excluir">
                                <Image Url="~/admin/images/btExcluir.jpg" />
                            </deletebutton>
                            <CancelButton Text="Cancelar">
                                <Image Url="~/admin/images/btCancelar.jpg" />
                            </CancelButton>
                            <UpdateButton Text="Salvar">
                                <Image Url="~/admin/images/btSalvarPeq.jpg" />
                            </UpdateButton>
                            <ClearFilterButton Visible="True" text="Limpar filtro">
                                <Image Url="~/admin/images/btLimparFiltro.jpg" />
                            </ClearFilterButton>
                            <HeaderTemplate>
                                <img alt="" src="images/legendaIcones.jpg" />
                            </HeaderTemplate>
                        </dxwgv:GridViewCommandColumn>
                    </Columns>
                    <StylesEditors>
                        <Label Font-Bold="True">
                        </Label>
                    </StylesEditors>
                </dxwgv:ASPxGridView>                
<%--                <dxwgv:ASPxGridViewExporter ID="grdEx" runat="server" FileName="fornecedores" 
                    GridViewID="grd" Landscape="True" PreserveGroupRowStates="True">
                </dxwgv:ASPxGridViewExporter>    --%>            
                <asp:ObjectDataSource ID="sqlDestaque" runat="server" SelectMethod="destaqueSeleciona" 
                    TypeName="rnDestaques" UpdateMethod="destaqueAlterar" 
                    InsertMethod="destaqueIncluir" DeleteMethod="destaqueExcluir">
                    <DeleteParameters>
                        <asp:Parameter Name="produtoDestaqueId" Type="Int32" />
                    </DeleteParameters>
                    <InsertParameters>
                        <asp:Parameter Name="nomeDaPagina" Type="String" />
                        <asp:Parameter Name="categoriaId" Type="Int32" />
                        <asp:Parameter Name="destaque" Type="String" />
                        <asp:Parameter Name="produtoDestaqueId" Type="Int32" />
                    </InsertParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="nomeDaPagina" Type="String" />
                        <asp:Parameter Name="categoriaId" Type="Int32" />
                        <asp:Parameter Name="destaque" Type="String" />
                        <asp:Parameter Name="produtoDestaqueId" Type="Int32" />
                    </UpdateParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="sqlCategoria" runat="server" 
                    SelectMethod="categoriasSelecionaNivelPai" TypeName="rnCategorias">
                </asp:ObjectDataSource>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
    </table>
        </td>
    </tr>
</table>
    </asp:Content>

