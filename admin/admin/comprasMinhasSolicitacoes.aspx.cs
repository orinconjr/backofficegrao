﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_comprasMinhasSolicitacoes : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        FillGrid(false);
    }

    protected void btnPesquisar_Click(object sender, ImageClickEventArgs e)
    {
        FillGrid(true);
    }

    private void FillGrid(bool rebind)
    {
        var data = new dbCommerceDataContext();
        var pedidos = (from c in data.tbComprasSolicitacaos
            join d in data.tbComprasSolicitacaoProdutos on c.idComprasSolicitacao equals d.idComprasSolicitacao into
                produtos
            where c.idUsuario == RetornaIdUsuarioLogado()
            select new
            {
                c.idComprasSolicitacao,
                c.dataSolicitacao,
                totalProdutos = produtos.Count(),
                aprovados = produtos.Count(x => x.statusSolicitacao == 1),
                reprovados = produtos.Count(x => x.statusSolicitacao == 2),
                pendentes = produtos.Count(x => x.statusSolicitacao == 0),
                c.motivo
            });
        if (rdbPendente.Checked) pedidos = pedidos.Where(x => x.pendentes > 0);
        if (rdbAprovados.Checked) pedidos = pedidos.Where(x => x.pendentes == 0 && x.reprovados == 0);
        if (rdbReprovados.Checked) pedidos = pedidos.Where(x => x.totalProdutos == x.reprovados);
        if (rdbParcialReprovados.Checked) pedidos = pedidos.Where(x => x.reprovados > 0 && x.aprovados > 0);
        grd.DataSource = pedidos;
        if((!Page.IsPostBack && !Page.IsCallback) | rebind) grd.DataBind();
    }


    private int RetornaIdUsuarioLogado()
    {
        HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
        usuarioLogadoId = Request.Cookies["usuarioLogadoId"];
        if (usuarioLogadoId == null)
        {
            return 0;
        }

        return Convert.ToInt32(usuarioLogadoId.Value);
    }

    protected void btPdf_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WritePdfToResponse();
    }
    protected void btXsl_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteXlsToResponse();
    }
    protected void btRtf_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteRtfToResponse();
    }
    protected void btCsv_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteCsvToResponse();
    }
}