﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Serialization;
using CarlosAg.ExcelXmlWriter;

public partial class admin_comprasProdutosCadastro : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Request.QueryString["id"] != null)
            {
                hdfIdProduto.Text = Request.QueryString["id"].ToString();
            }
            FillForm();
        }
    }

    private void FillForm()
    {
        imgProduto.Visible = false;

        if (!string.IsNullOrEmpty(hdfIdProduto.Text))
        {
            int idComprasProduto = 0;
            int.TryParse(hdfIdProduto.Text, out idComprasProduto);
            var data = new dbCommerceDataContext();
            var produto = (from c in data.tbComprasProdutos where c.idComprasProduto == idComprasProduto select c).FirstOrDefault();
            if (produto == null)
            {
                hdfIdProduto.Text = "";
                return;
            }
            txtProduto.Text = produto.produto;
            chkMateriaPrima.Checked = produto.materiaPrima;

            if (!string.IsNullOrEmpty(produto.imagemProduto))
            {
                imgProduto.Visible = true;
                imgProduto.ImageUrl = produto.imagemProduto;
            }
            FillGridFornecedores();
        }
    }

    private void FillGridFornecedores()
    {
        if (!string.IsNullOrEmpty(hdfIdProduto.Text))
        {
            int idComprasProduto = 0;
            int.TryParse(hdfIdProduto.Text, out idComprasProduto);

            var data = new dbCommerceDataContext();
            var produtoFornecedores = (from c in data.tbComprasProdutoFornecedors
                                       where c.idComprasProduto == idComprasProduto
                                       select new
                                       {
                                           c.idComprasProdutoFornecedor, 
                                           c.idComprasProduto,
                                           c.idComprasFornecedor,
                                           c.idComprasUnidadeMedida,
                                           c.valor,
                                           c.dataValor,
                                           c.nomeFornecedor,
                                           dif = (DateTime.Now - c.dataValor).Days
                                       }).ToList();
            grdFornecedores.DataSource = produtoFornecedores;
            grdFornecedores.DataBind();
        }
        else
        {
            var lista = new List<tbComprasProdutoFornecedor>();
            if (!string.IsNullOrEmpty(hdfLista.Text))
            {
                lista = Deserialize(hdfLista.Text);
            }
            grdFornecedores.DataSource = lista;
            grdFornecedores.DataBind();
        }
    }

    protected void btnAdicionar_OnClick(object sender, EventArgs e)
    {
        hdfIdFornecedor.Value = "";
        ddlFornecedor.SelectedItem = null;
        ddlUnidadesDeMedida.SelectedItem = null;
        txtValor.Value = 0;
        ddlFornecedor.Focus();
        popFornecedor.ShowOnPageLoad = true;
    }


    private void AdicionaItemListaTemporariaFornecedores(tbComprasProdutoFornecedor item)
    {
        var lista = new List<tbComprasProdutoFornecedor>();
        if (!string.IsNullOrEmpty(hdfLista.Text))
        {
            lista = Deserialize(hdfLista.Text);
        }

        lista.RemoveAll(x => x.idComprasFornecedor == item.idComprasFornecedor);

        lista.Add(item);
        hdfLista.Text = Serialize(lista);
        FillGridFornecedores();
    }


    protected void btnGravar_OnClick(object sender, EventArgs e)
    {
        var data = new dbCommerceDataContext();
        if (!string.IsNullOrEmpty(hdfIdProduto.Text))
        {
            var log = new rnLog();
            log.usuario = rnUsuarios.retornaNomeUsuarioLogado();
            int idComprasProduto = Convert.ToInt32(hdfIdProduto.Text);
            var produto = (from c in data.tbComprasProdutos where c.idComprasProduto == idComprasProduto select c).First();

            produto = UploadImagem(produto);

            if (txtProduto.Text != produto.produto)
            {
                log.descricoes.Add("Nome alterado de " + produto.produto + " para " + txtProduto.Text);
            }
            produto.produto = txtProduto.Text;
            produto.materiaPrima = chkMateriaPrima.Checked;
            data.SubmitChanges();
            var tipoRelacionado = new rnEnums.TipoRelacionadoIds() { idRegistroRelacionado = produto.idComprasProduto, tipoRelacionado = rnEnums.TipoRegistroRelacionado.CompraProduto };
            log.tiposRelacionados.Add(tipoRelacionado);
            log.tiposOperacao.Add(rnEnums.TipoOperacao.CompraProduto);
            log.InsereLog();
        }
        else
        {
            var produto = new tbComprasProduto();
            produto.produto = txtProduto.Text;
            produto.materiaPrima = chkMateriaPrima.Checked;
            produto.dataCadastro = DateTime.Now;

            produto = UploadImagem(produto);

            data.tbComprasProdutos.InsertOnSubmit(produto);

            data.SubmitChanges();

            var lista = new List<tbComprasProdutoFornecedor>();
            if (!string.IsNullOrEmpty(hdfLista.Text))
            {
                lista = Deserialize(hdfLista.Text);
            }

            foreach (var fornecedorCadastro in lista)
            {
                var fornecedor = new tbComprasProdutoFornecedor
                {
                    idComprasProduto = produto.idComprasProduto,
                    idComprasFornecedor = fornecedorCadastro.idComprasFornecedor,
                    idComprasUnidadeMedida = fornecedorCadastro.idComprasUnidadeMedida,
                    valor = fornecedorCadastro.valor,
                    nomeFornecedor = fornecedorCadastro.nomeFornecedor,
                    dataValor = DateTime.Now
                };
                data.tbComprasProdutoFornecedors.InsertOnSubmit(fornecedor);

                var fornecedorHistorico = new tbComprasProdutoFornecedorHistorico
                {
                    idComprasProduto = produto.idComprasProduto,
                    idComprasFornecedor = fornecedorCadastro.idComprasFornecedor,
                    idComprasUnidadeMedida = fornecedorCadastro.idComprasUnidadeMedida,
                    valor = fornecedorCadastro.valor,
                    data = DateTime.Now,
                    nomeFornecedor = fornecedorCadastro.nomeFornecedor,
                    idUsuario = RetornaIdUsuarioLogado()
                };
                data.tbComprasProdutoFornecedorHistoricos.InsertOnSubmit(fornecedorHistorico);
                data.SubmitChanges();
            }

            var log = new rnLog();
            log.usuario = rnUsuarios.retornaNomeUsuarioLogado();
            log.descricoes.Add(produto.produto + " cadastrado");

            var tipoRelacionado = new rnEnums.TipoRelacionadoIds() { idRegistroRelacionado = produto.idComprasProduto, tipoRelacionado = rnEnums.TipoRegistroRelacionado.CompraProduto };
            log.tiposRelacionados.Add(tipoRelacionado);
            log.tiposOperacao.Add(rnEnums.TipoOperacao.CompraProduto);
            log.InsereLog();
        }
        Response.Redirect("comprasProdutosLista.aspx");
    }

    private tbComprasProduto UploadImagem(tbComprasProduto produto)
    {
        if (fileUpLoad.HasFile)
        {
            var arquivo = Path.GetFileName(fileUpLoad.PostedFile.FileName);
            var extensao = Path.GetExtension(fileUpLoad.PostedFile.FileName);

            var tamArquivo = fileUpLoad.PostedFile.ContentLength;

            if (!Directory.Exists(Server.MapPath("~/admin/images/ProdutoCompras/")))
                Directory.CreateDirectory(Server.MapPath("~/admin/images/ProdutoCompras/"));

            var guid = Guid.NewGuid();

            fileUpLoad.SaveAs(Server.MapPath("~/admin/images/ProdutoCompras/") + guid.ToString() + extensao);
            produto.imagemProduto = "/admin/images/ProdutoCompras/" + guid.ToString() + extensao;
        }

        return produto;
    }

    private int RetornaIdUsuarioLogado()
    {
        HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
        usuarioLogadoId = Request.Cookies["usuarioLogadoId"];
        if (usuarioLogadoId == null)
        {
            return 0;
        }

        return Convert.ToInt32(usuarioLogadoId.Value);
    }
    public static string Serialize(List<tbComprasProdutoFornecedor> tData)
    {
        var serializer = new XmlSerializer(typeof(List<tbComprasProdutoFornecedor>));

        TextWriter writer = new StringWriter();
        serializer.Serialize(writer, tData);

        return writer.ToString();
    }

    public static List<tbComprasProdutoFornecedor> Deserialize(string tData)
    {
        var serializer = new XmlSerializer(typeof(List<tbComprasProdutoFornecedor>));

        TextReader reader = new StringReader(tData);

        return (List<tbComprasProdutoFornecedor>)serializer.Deserialize(reader);
    }

    protected void btnEditarFornecedor_OnCommand(object sender, CommandEventArgs e)
    {
        int idFornecedor = Convert.ToInt32(e.CommandArgument);
        if (!string.IsNullOrEmpty(hdfIdProduto.Text))
        {
            int idComprasProduto = 0;
            int.TryParse(hdfIdProduto.Text, out idComprasProduto);
            var data = new dbCommerceDataContext();

            var produtoFornecedorChecagem = (from c in data.tbComprasProdutoFornecedors
                                             where
                                                 c.idComprasProduto == idComprasProduto &&
                                                 c.idComprasFornecedor == idFornecedor
                                             select c).FirstOrDefault();
            if (produtoFornecedorChecagem != null)
            {
                ddlFornecedor.SelectedItem = ddlFornecedor.Items.FindByValue(produtoFornecedorChecagem.idComprasFornecedor.ToString());
                ddlUnidadesDeMedida.SelectedItem = ddlUnidadesDeMedida.Items.FindByValue(produtoFornecedorChecagem.idComprasUnidadeMedida.ToString());
                txtValor.Value = produtoFornecedorChecagem.valor;
                txtNomeFornecedor.Text = produtoFornecedorChecagem.nomeFornecedor;
            }
        }
        popFornecedor.ShowOnPageLoad = true;
    }

    protected void btnGravarFornecedor_OnClick(object sender, EventArgs e)
    {
        if (ddlFornecedor.SelectedItem == null)
        {
            Response.Write("<script>alert('Favor selecionar o Fornecedor.');</script>");
            return;
        }
        if (!string.IsNullOrEmpty(hdfIdProduto.Text))
        {
            int idComprasProduto = 0;
            int.TryParse(hdfIdProduto.Text, out idComprasProduto);
            var data = new dbCommerceDataContext();

            var produtoFornecedorChecagem = (from c in data.tbComprasProdutoFornecedors
                                             where
                                                 c.idComprasProduto == idComprasProduto &&
                                                 c.idComprasFornecedor == Convert.ToInt32(ddlFornecedor.SelectedItem.Value)
                                             select c).FirstOrDefault();
            if (produtoFornecedorChecagem != null)
            {
                produtoFornecedorChecagem.idComprasFornecedor = Convert.ToInt32(ddlFornecedor.SelectedItem.Value);
                produtoFornecedorChecagem.idComprasUnidadeMedida = Convert.ToInt32(ddlUnidadesDeMedida.SelectedItem.Value);
                produtoFornecedorChecagem.valor = Convert.ToDecimal(txtValor.Value);
                produtoFornecedorChecagem.dataValor = DateTime.Now;
                produtoFornecedorChecagem.nomeFornecedor = txtNomeFornecedor.Text;

                var fornecedorHistorico = new tbComprasProdutoFornecedorHistorico
                {
                    idComprasProduto = produtoFornecedorChecagem.idComprasProduto,
                    idComprasFornecedor = produtoFornecedorChecagem.idComprasFornecedor,
                    idComprasUnidadeMedida = produtoFornecedorChecagem.idComprasUnidadeMedida,
                    valor = produtoFornecedorChecagem.valor,
                    data = DateTime.Now,
                    idUsuario = RetornaIdUsuarioLogado(),
                    nomeFornecedor = produtoFornecedorChecagem.nomeFornecedor
                };
                data.tbComprasProdutoFornecedorHistoricos.InsertOnSubmit(fornecedorHistorico);
                data.SubmitChanges();
            }
            else
            {
                var produtoFornecedor = new tbComprasProdutoFornecedor();
                produtoFornecedor.idComprasProduto = idComprasProduto;
                produtoFornecedor.idComprasFornecedor = Convert.ToInt32(ddlFornecedor.SelectedItem.Value);
                produtoFornecedor.idComprasUnidadeMedida = Convert.ToInt32(ddlUnidadesDeMedida.SelectedItem.Value);
                produtoFornecedor.valor = Convert.ToDecimal(txtValor.Value);
                produtoFornecedor.dataValor = DateTime.Now;
                produtoFornecedor.nomeFornecedor = txtNomeFornecedor.Text;

                data.tbComprasProdutoFornecedors.InsertOnSubmit(produtoFornecedor);

                var fornecedorHistorico = new tbComprasProdutoFornecedorHistorico
                {
                    idComprasProduto = produtoFornecedor.idComprasProduto,
                    idComprasFornecedor = produtoFornecedor.idComprasFornecedor,
                    idComprasUnidadeMedida = produtoFornecedor.idComprasUnidadeMedida,
                    valor = produtoFornecedor.valor,
                    data = DateTime.Now,
                    idUsuario = RetornaIdUsuarioLogado(),
                    nomeFornecedor = produtoFornecedor.nomeFornecedor
                };
                data.tbComprasProdutoFornecedorHistoricos.InsertOnSubmit(fornecedorHistorico);
                data.SubmitChanges();
            }
        }
        else
        {
            var produtoFornecedor = new tbComprasProdutoFornecedor();
            produtoFornecedor.idComprasProduto = 0;
            produtoFornecedor.idComprasFornecedor = Convert.ToInt32(ddlFornecedor.SelectedItem.Value);
            produtoFornecedor.idComprasUnidadeMedida = Convert.ToInt32(ddlUnidadesDeMedida.SelectedItem.Value);
            produtoFornecedor.valor = Convert.ToDecimal(txtValor.Value);
            produtoFornecedor.dataValor = DateTime.Now;
            produtoFornecedor.nomeFornecedor = txtNomeFornecedor.Text;
            AdicionaItemListaTemporariaFornecedores(produtoFornecedor);
        }
        FillGridFornecedores();
        popFornecedor.ShowOnPageLoad = false;
    }

    protected void btnHistorico_OnCommand(object sender, CommandEventArgs e)
    {
        int idFornecedor = Convert.ToInt32(e.CommandArgument);
        int idComprasProduto = 0;
        int.TryParse(hdfIdProduto.Text, out idComprasProduto);
        var data = new dbCommerceDataContext();

        var historico = (from c in data.tbComprasProdutoFornecedorHistoricos
                         where c.idComprasProduto == idComprasProduto && c.idComprasFornecedor == idFornecedor
                         orderby c.data descending
                         select new
                         {
                             c.idComprasProdutoFornecedorHistorico,
                             c.idComprasProduto,
                             c.idComprasFornecedor,
                             c.idComprasUnidadeMedida,
                             c.valor,
                             c.data,
                             c.idUsuario,
                             c.nomeFornecedor
                         });
        grdHistoricoFornecedor.DataSource = historico;
        grdHistoricoFornecedor.DataBind();
        popHistoricoFornecedor.ShowOnPageLoad = true;
    }
}