﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class admin_pedidosClientesRomaneio : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            Label1.Text = Request.QueryString["romaneio"] + " - produto:" + Request.QueryString["produtoId"];
            fillGrid();
        }
    }

    protected void fillGrid()
    {
        var data = new dbCommerceDataContext();
        int idPedidoFornecedor = 0;
        try { idPedidoFornecedor = Convert.ToInt32(Request.QueryString["romaneio"]); } catch { }

        int produtoId = 0;
        try { produtoId = Convert.ToInt32(Request.QueryString["produtoId"]); } catch { }

        var pedidos = (from c in data.tbPedidoFornecedorItems
                           join ipd in data.tbItensPedidos on c.tbProdutoEstoques.FirstOrDefault().itemPedidoId equals ipd.itemPedidoId into ip
                       where c.idProduto == produtoId
                       && c.idPedidoFornecedor == idPedidoFornecedor
                       select new
                       {
                           c.tbProdutoEstoques.FirstOrDefault().pedidoId,
                           c.idPedidoFornecedorItem,
                             itemPedidoIda=ip.FirstOrDefault().itemPedidoId.ToString(),
                           brindea = ip.FirstOrDefault().brinde
                       }).ToList();
        GridView1.DataSource = pedidos;
        GridView1.DataBind();

    }


    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {

            HtmlAnchor linkeditarpedido = (HtmlAnchor)e.Row.FindControl("linkeditarpedido");
            string pedidoId = "";
            try { pedidoId = DataBinder.Eval(e.Row.DataItem, "pedidoId").ToString(); } catch (Exception ex) { }
            if(!String.IsNullOrEmpty(pedidoId))
            {
                linkeditarpedido.HRef = "pedido.aspx?pedidoId=" + DataBinder.Eval(e.Row.DataItem, "pedidoId");
            }
            else
            {
                linkeditarpedido.InnerHtml = "não vinculado";
            }
            

        }
    }
}