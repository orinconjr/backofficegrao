﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" Theme="Glass" CodeFile="pedidoFornecedor.aspx.cs" Inherits="admin_pedidoFornecedor" %>

<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxTimer" TagPrefix="dxt" %>
<%@ Register TagPrefix="dxe" Namespace="DevExpress.Web.ASPxMenu" Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>

<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxMenu" Assembly="DevExpress.Web.v11.1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" runat="Server">
    <script language="javascript" type="text/javascript">
        function OnDropDownDataPedido(s, dateFrom, dateTo) {
            var str = s.GetText();
            if (str == "") {
                dateFrom.SetDate(new Date(1950, 0, 1));
                dateTo.SetDate(new Date(1960, 11, 31));
                return;
            }
            var d = str.split("|");
            dateFrom.SetText(d[0]);
            dateTo.SetText(d[1]);
        }

        function ApplyFilter(dde, dateFrom, dateTo, campo) {
            var colunaSplit = dde.name.split('_');
            var coluna = colunaSplit[colunaSplit.length - 1].replace("DXFREditorcol", "");
            var colunaFiltro = "";
            if (coluna == "15") colunaFiltro = "dataUltimaEntrega";
            if (coluna == "14") colunaFiltro = "dataPagamento";
            if (coluna == "13") colunaFiltro = "dataVencimento";
            if (coluna == "12") colunaFiltro = "dataEntrega";
            if (coluna == "4") colunaFiltro = "dataLimite";
            if (coluna == "3") colunaFiltro = "data";

            var d1 = dateFrom.GetText();
            var d2 = dateTo.GetText();
            if (d1 == "" || d2 == "") return;
            dde.SetText(d1 + "|" + d2);
            grd.AutoFilterByColumn(colunaFiltro, d1 + "|" + d2);
        }
        function OnDropDown(s, dateFrom, dateTo) {
            var str = s.GetText();
            if (str == "") {
                dateFrom.SetDate(new Date(1950, 0, 1));
                dateTo.SetDate(new Date(1960, 11, 31));
                return;
            }
            var d = str.split("|");
            dateFrom.SetText(d[0]);
            dateTo.SetText(d[1]);
        }
        function grid_ContextMenu(s, e) {
            if (e.objectType == "header")
                pmColumnMenu.ShowAtPos(ASPxClientUtils.GetEventX(e.htmlEvent), ASPxClientUtils.GetEventY(e.htmlEvent));
        }
    </script>

    <dxe:ASPxPopupMenu ID="pmColumnMenu" runat="server" ClientInstanceName="pmColumnMenu">
        <Items>
            <dxe:MenuItem Name="cmdShowCustomization" Text="Escolher colunas">
            </dxe:MenuItem>
        </Items>
        <ClientSideEvents ItemClick="function(s, e) { if(e.item.name == 'cmdShowCustomization') grd.ShowCustomizationWindow(); }" />
    </dxe:ASPxPopupMenu>

    <table cellpadding="0" cellspacing="0" style="width: 920px">
        <tr>
            <td class="tituloPaginas" valign="top">
                <asp:Label ID="lblAcao" runat="server">Pedidos ao Fornecedor</asp:Label>
            </td>
        </tr>

        <tr>
            <td>

                <table align="center" cellpadding="0" cellspacing="0" style="width: 100%;">
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" style="width: 824px">
                                <tr>
                                    <td colspan="2" class="rotulos" style="text-align: center;">Entregas no Periodo
                                    </td>
                                </tr>
                                <tr>
                                    <td class="rotulos" style="width: 100px">Data inicial<br />
                                        <asp:TextBox ID="txtDataInicial" runat="server" CssClass="campos"
                                            Text='<%# Bind("faixaDeCepPesoInicial") %>'
                                            Width="90px" MaxLength="10"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rqvDataInicial" runat="server"
                                            ControlToValidate="txtDataInicial" Display="None"
                                            ErrorMessage="Preencha a data inicial."
                                            SetFocusOnError="True" ValidationGroup="filtroData"></asp:RequiredFieldValidator>
                                        <b>
                                            <asp:RegularExpressionValidator ID="rgeDataInicial" runat="server"
                                                ControlToValidate="txtDataInicial" Display="None"
                                                ErrorMessage="Por favor, preencha corretamente a data inicial."
                                                SetFocusOnError="True"
                                                ValidationExpression="^(([0-2]\d|[3][0-1])\/([0]\d|[1][0-2])\/[1-2][0-9]\d{2})$" ValidationGroup="filtroData"></asp:RegularExpressionValidator>
                                        </b>
                                    </td>
                                    <td class="rotulos" style="width: 100px">Data final<br __designer:mapid="401" />
                                        <asp:TextBox ID="txtDataFinal" runat="server" CssClass="campos"
                                            Text='<%# Bind("faixaDeCepPesoFinal") %>' ValidationGroup="grpInsert"
                                            Width="90px" MaxLength="10"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rqvDataFinal" runat="server"
                                            ControlToValidate="txtDataFinal" Display="None"
                                            ErrorMessage="Preencha a data final."
                                            SetFocusOnError="True" ValidationGroup="filtroData"></asp:RequiredFieldValidator>
                                        <b>
                                            <asp:RegularExpressionValidator ID="rgeDataInicial0" runat="server"
                                                ControlToValidate="txtDataFinal" Display="None"
                                                ErrorMessage="Por favor, preencha corretamente a data final"
                                                SetFocusOnError="True"
                                                ValidationExpression="^(([0-2]\d|[3][0-1])\/([0]\d|[1][0-2])\/[1-2][0-9]\d{2})$" ValidationGroup="filtroData"></asp:RegularExpressionValidator>
                                        </b>
                                    </td>
                                    <td valign="bottom">
                                        <asp:ImageButton ID="imbInsert" runat="server" OnClick="imbInsert_OnClick"
                                            ImageUrl="~/admin/images/btPesquisar.jpg" ValidationGroup="filtroData" />
                                        <asp:ValidationSummary ID="llds" runat="server" DisplayMode="List"
                                            ShowMessageBox="True" ShowSummary="False" ValidationGroup="filtroData" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            <table cellpadding="0" cellspacing="0" style="width: 395px">
                                <tr>
                                    <td align="right">&nbsp;</td>
                                    <td height="38"
                                        style="background-image: url('images/bkExportar.jpg'); padding-bottom: 1px;"
                                        valign="bottom" width="231">
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 97px">&nbsp;</td>
                                                <td style="width: 33px">
                                                    <asp:ImageButton ID="btPdf" runat="server" ImageUrl="~/admin/images/btPdf.jpg" OnClick="btPdf_Click" Enabled="false"
                                                        Style="margin-left: 5px" />
                                                </td>
                                                <td style="width: 31px">
                                                    <asp:ImageButton ID="btXsl" runat="server" ImageUrl="~/admin/images/btXls.jpg" CausesValidation="false" OnClick="btXsl_Click"
                                                        Style="margin-left: 5px" />
                                                </td>
                                                <td style="width: 36px">
                                                    <asp:ImageButton ID="btRtf" runat="server" ImageUrl="~/admin/images/btRtf.jpg" OnClick="btRtf_Click" Enabled="false"
                                                        Style="margin-left: 5px" />
                                                </td>
                                                <td valign="bottom">
                                                    <asp:ImageButton ID="btCsv" runat="server" ImageUrl="~/admin/images/btCsv.jpg" OnClick="btCsv_Click" Enabled="false"
                                                        Style="margin-left: 5px" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="rotulos" style="padding-bottom: 10px; padding-left: 10px;"></td>
                        <table style="width: 100%;">
                            <tr>
                                <td>Entregue:
                        <asp:RadioButton runat="server" ID="rdbNao" Text="Não" Checked="True" GroupName="filtroEntregue" AutoPostBack="True" OnCheckedChanged="rdb_OnCheckedChanged" />
                                    - 
                        <asp:RadioButton runat="server" ID="rdbTodos" Text="Todos" Checked="False" GroupName="filtroEntregue" AutoPostBack="True" OnCheckedChanged="rdb_OnCheckedChanged" />
                                </td>
                                <td style="text-align: right;">
                                    <asp:Button runat="server" ID="btnNovo" Text="Criar Pedido Avulso" PostBackUrl="pedidoFornecedorAvulsoNovo.aspx" CausesValidation="false" />
                                </td>
                            </tr>
                        </table>
                    </tr>
                    <tr>
                        <td style="text-align: right;">
                            <table style="width: 100%;">
                                <tr>
                                    <td style="text-align: left;">
                                        <a href="pedidoFornecedorNovo.aspx">Gerar Pedidos ao Fornecedor
                                        </a>
                                    </td>
                                    <td style="text-align: right;">
                                        <a href="pedidoFornecedorHistorico.aspx">Horário de Acesso do Fornecedor
                                        </a>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <dxt:ASPxTimer ID="timer1" runat="server" ClientInstanceName="myTimer" Enabled="False" Interval="1000" OnTick="timer1_OnTick">
                            </dxt:ASPxTimer>
                            <asp:HiddenField runat="server" ID="hiddenExportarId" />
                            <dxwgv:ASPxGridView EnableViewState="False" EnableCallBacks="False" ID="grd" ClientInstanceName="grd" runat="server" AutoGenerateColumns="False" Width="834px" Cursor="auto" OnHtmlRowCreated="gridClientes_HtmlRowCreated" KeyFieldName="idPedidoFornecedor"
                                OnCustomUnboundColumnData="grd_OnCustomUnboundColumnData" OnAutoFilterCellEditorCreate="grid_AutoFilterCellEditorCreate" OnAutoFilterCellEditorInitialize="grid_AutoFilterCellEditorInitialize"
                                OnProcessColumnAutoFilter="grid_ProcessColumnAutoFilter" Settings-ShowFilterRow="True" Settings-ShowFilterBar="Visible" OnDataBound="grd_OnDataBound">
                                <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                                <Styles>
                                    <Footer Font-Bold="True">
                                    </Footer>
                                </Styles>
                                <SettingsText ConfirmDelete="Tem certeza que deseja excluir?" EmptyDataRow="Nenhum registro encontrado." />
                                <SettingsPager Position="TopAndBottom" PageSize="40" ShowDisabledButtons="False" AlwaysShowPager="True">
                                    <Summary AllPagesText="Páginas: {0} - {1} ({2} items)" Text="Página {0} de {1} ({2} registros encontrados)" />
                                </SettingsPager>
                                <TotalSummary>
                                    <dxwgv:ASPxSummaryItem FieldName="qtdProdutos" ShowInColumn="qtdProdutos" ShowInGroupFooterColumn="qtdProdutos" SummaryType="Sum" DisplayFormat="{0}" />
                                    <dxwgv:ASPxSummaryItem FieldName="peso" ShowInColumn="peso" ShowInGroupFooterColumn="peso" SummaryType="Sum" DisplayFormat="#.###" />
                                    <dxwgv:ASPxSummaryItem FieldName="pesoFaltante" ShowInColumn="pesoFaltante" ShowInGroupFooterColumn="pesoFaltante" SummaryType="Sum" DisplayFormat="#.###" />
                                    <dxwgv:ASPxSummaryItem FieldName="faltaEntregar" ShowInColumn="faltaEntregar" ShowInGroupFooterColumn="faltaEntregar" SummaryType="Sum" DisplayFormat="{0}" />
                                    <dxwgv:ASPxSummaryItem FieldName="custo" ShowInColumn="custo" ShowInGroupFooterColumn="custo" SummaryType="Sum" DisplayFormat="{0:C}" />
                                    <dxwgv:ASPxSummaryItem FieldName="custoFaltante" ShowInColumn="custoFaltante" ShowInGroupFooterColumn="custoFaltante" SummaryType="Sum" DisplayFormat="{0:C}" />
                                    <dxwgv:ASPxSummaryItem FieldName="diferenca" ShowInColumn="diferenca" ShowInGroupFooterColumn="diferenca" SummaryType="Sum" DisplayFormat="{0:C}" />
                                    <dxwgv:ASPxSummaryItem FieldName="custoCorrigido" ShowInColumn="custoCorrigido" ShowInGroupFooterColumn="custoCorrigido" SummaryType="Sum" DisplayFormat="{0:C}" />
                                </TotalSummary>
                                <Settings ShowFilterRow="True" ShowGroupButtons="False" ShowHeaderFilterButton="True" ShowFooter="True" />
                                <SettingsEditing EditFormColumnCount="4" PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1" PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter" PopupEditFormWidth="700px" />
                                <Columns>
                                    <dxwgv:GridViewDataTextColumn Caption="#" Name="selecionar" VisibleIndex="0" Width="30px">
                                        <DataItemTemplate>
                                            <dxe:ASPxCheckBox ID="chkSelecionar" runat="server" ValueChecked="True" ValueType="System.String" ValueUnchecked="False">
                                            </dxe:ASPxCheckBox>
                                        </DataItemTemplate>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="ID" FieldName="idPedidoFornecedor" VisibleIndex="0" Width="50">
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Fornecedor" FieldName="fornecedorNome" VisibleIndex="0">
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataDateColumn Caption="Data do Pedido" FieldName="data" Name="data" VisibleIndex="0">
                                    </dxwgv:GridViewDataDateColumn>
                                    <dxwgv:GridViewDataDateColumn Caption="Data Prevista de Entrega" FieldName="dataLimite" Name="dataLimite" VisibleIndex="0">
                                    </dxwgv:GridViewDataDateColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Produtos" FieldName="qtdProdutos" VisibleIndex="0">
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Peso (Kg)" FieldName="peso" VisibleIndex="0">
                                         <PropertiesTextEdit DisplayFormatString="#.###" />
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Peso Faltante (Kg)" FieldName="pesoFaltante" VisibleIndex="0">
                                         <PropertiesTextEdit DisplayFormatString="#.###" />
                                    </dxwgv:GridViewDataTextColumn>

                                    <dxwgv:GridViewDataTextColumn Caption="Custo" FieldName="custo" VisibleIndex="0" >
                                        <PropertiesTextEdit DisplayFormatString="C" />
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Custo Faltante" FieldName="custoFaltante" VisibleIndex="0">
                                        <PropertiesTextEdit DisplayFormatString="C" />
                                    </dxwgv:GridViewDataTextColumn>

<%--                                    <dxwgv:GridViewDataTextColumn Caption="Falta" FieldName="faltaEntregar" VisibleIndex="0">
                                    </dxwgv:GridViewDataTextColumn>--%>

                                    <dxwgv:GridViewDataTextColumn Caption="Falta" FieldName="faltaEntregar" VisibleIndex="0">
                                        <DataItemTemplate>
                                            <a href="pedidoFornecedorVisualizar.aspx?idPedidoFornecedor=<%#Eval("idPedidoFornecedor") %>&itensPendentesEntrega=on" target="_blank" 
                                                title="Produtos pendentes de entrega"
                                                style="display: <%# Convert.ToInt32(Eval("faltaEntregar")) > 0 ? "block" : "none" %>">
                                                <%# Eval("faltaEntregar") %>
                                            </a>
                                            <%# Convert.ToInt32(Eval("faltaEntregar")) == 0 ? "0" : "" %>
                                        </DataItemTemplate>
                                    </dxwgv:GridViewDataTextColumn>

                                    <dxwgv:GridViewDataTextColumn Caption="Falta de Pedidos" FieldName="pedidosFaltaEntregar" VisibleIndex="0">
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Fornecedor ID" FieldName="fornecedorId" VisibleIndex="0" Visible="False">
                                    </dxwgv:GridViewDataTextColumn>

                                    <dxwgv:GridViewDataTextColumn Caption="Diferença" FieldName="diferenca" VisibleIndex="1" Visible="False">
                                        <PropertiesTextEdit DisplayFormatString="C" />
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataDateColumn Caption="Data de Entrega" FieldName="dataEntrega" Name="dataEntrega" VisibleIndex="1" Visible="False">
                                    </dxwgv:GridViewDataDateColumn>
                                    <dxwgv:GridViewDataDateColumn Caption="Data de Vencimento da Fatura" FieldName="dataVencimento" Name="dataVencimento" VisibleIndex="1" Visible="False">
                                    </dxwgv:GridViewDataDateColumn>
                                    <dxwgv:GridViewDataDateColumn Caption="Data de Pagamento da Fatura" FieldName="dataPagamento" Name="dataPagamento" VisibleIndex="1" Visible="False">
                                    </dxwgv:GridViewDataDateColumn>
                                    <dxwgv:GridViewDataDateColumn Caption="Última entrega" FieldName="dataUltimaEntrega" Name="dataUltimaEntrega" VisibleIndex="1" Visible="False">
                                    </dxwgv:GridViewDataDateColumn>
                                    <dxwgv:GridViewDataDateColumn Caption="Cronograma de Entrega" FieldName="dataPrevista" Name="dataPrevista" VisibleIndex="0" Visible="True">
                                    </dxwgv:GridViewDataDateColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Número da Cobrança" FieldName="numeroCobranca" VisibleIndex="1" Visible="False">
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Entregue" FieldName="entregue" Name="entregue" VisibleIndex="0" Width="30px" UnboundType="String">
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Sem Vinculo" FieldName="totalSemVinculo" VisibleIndex="1" Visible="False">
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Sem Vinculo (estoque)" FieldName="totalSemVinculoEstoque" VisibleIndex="1" Visible="False">
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Estoque" FieldName="pedidoEstoque" VisibleIndex="1" Visible="False">
                                    </dxwgv:GridViewDataTextColumn>

                                    <dxwgv:GridViewDataTextColumn Caption="Exportar" Name="exportar" VisibleIndex="0" Width="80">
                                        <DataItemTemplate>
                                            <asp:LinkButton runat="server" OnCommand="btnExportar_OnCommand" ID="btnExportar" CommandArgument='<%# Eval("idPedidoFornecedor") %>' EnableViewState="False">Exportar Excel</asp:LinkButton>
                                        </DataItemTemplate>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Reenviar" Name="reenviar" VisibleIndex="0" Width="80" Visible="False">
                                        <DataItemTemplate>
                                            <asp:LinkButton runat="server" OnCommand="btnReenviar_OnCommand" ID="btnReenviar" OnClientClick="return confirm('Deseja realmente reenviar este pedido ao Fornecedor?')" CommandArgument='<%# Eval("idPedidoFornecedor") %>' EnableViewState="False">Reenviar para Fornecedor</asp:LinkButton>
                                        </DataItemTemplate>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Confirmado" Name="confirmado" FieldName="confirmado" VisibleIndex="0" Width="80">
                                        <DataItemTemplate>
                                            <asp:LinkButton runat="server" Visible="False" OnClientClick="return confirm('Deseja realmente marcar o pedido como confirmado?')" OnCommand="btnConfirmar_OnCommand" ID="btnConfirmar" CommandArgument='<%# Eval("idPedidoFornecedor") %>' EnableViewState="False">Aguardando visualização do Fornecedor</asp:LinkButton>
                                            <asp:Label ID="lblConfirmado" runat="server" Text="Visualizado pelo Fornecedor"></asp:Label>
                                        </DataItemTemplate>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Ver" Name="ver" VisibleIndex="0" Width="60">
                                        <DataItemTemplate>
                                            <a href="pedidoFornecedorVisualizar.aspx?idPedidoFornecedor=<%# Eval("idPedidoFornecedor") %>">Visualizar Pedidos
                                            </a>
                                            <asp:LinkButton runat="server" Visible="False" OnCommand="btnVer_OnCommand" ID="btnVer" CommandArgument='' EnableViewState="False">Visualizar Pedidos</asp:LinkButton>
                                        </DataItemTemplate>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Detalhar" Name="detalhar" VisibleIndex="0" Width="60">
                                        <DataItemTemplate>
                                            <a href="relatorioVendasPedidoFornecedor.aspx?idPedidoFornecedor=<%# Eval("idPedidoFornecedor") %>">Detalhar
                                            </a>
                                        </DataItemTemplate>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Editar" Name="editar" VisibleIndex="0" Width="60">
                                        <DataItemTemplate>
                                            <a href="pedidoFornecedorEditar.aspx?idPedidoFornecedor=<%# Eval("idPedidoFornecedor") %>">Checar Entrega
                                            </a>
                                            <asp:LinkButton runat="server" Visible="False" OnCommand="btnEditar_OnCommand" ID="btnEditar" CommandArgument='<%# Eval("idPedidoFornecedor") %>' EnableViewState="False">Checar Entrega</asp:LinkButton>
                                        </DataItemTemplate>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataDateColumn Caption="Fechamentos" FieldName="Fechamentos" Name="Fechamentos" VisibleIndex="1" Visible="False">
                                    </dxwgv:GridViewDataDateColumn>
                                </Columns>
                                <StylesEditors>
                                    <Label Font-Bold="True">
                                    </Label>
                                </StylesEditors>
                                <ClientSideEvents ContextMenu="grid_ContextMenu" />
                                <SettingsCustomizationWindow Enabled="True" />
                            </dxwgv:ASPxGridView>
                            <asp:ObjectDataSource ID="sql" runat="server" SelectMethod="pedidosFornecedorConfirmados" TypeName="rnPedidos"></asp:ObjectDataSource>

                            <asp:ObjectDataSource ID="sql2" runat="server" SelectMethod="pedidosFornecedorConfirmadosPorPeriodo" TypeName="rnPedidos"></asp:ObjectDataSource>

                            <dxwgv:ASPxGridViewExporter ID="grdEx" runat="server" FileName="pedidosAoFornecedor"
                                GridViewID="grd" Landscape="True" PreserveGroupRowStates="True">
                            </dxwgv:ASPxGridViewExporter>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td class="rotulos">
                            <b>Pedidos Pendentes</b>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:HiddenField runat="server" ID="HiddenField1" />
                            <dxwgv:ASPxGridView EnableViewState="False" EnableCallBacks="False" ID="gridPendentes" ClientInstanceName="gridPendentes" runat="server" AutoGenerateColumns="False" Width="100%" Cursor="auto" On="gridPendentes_HtmlRowCreated" KeyFieldName="idPedidoFornecedor">
                                <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                                <Styles>
                                    <Footer Font-Bold="True">
                                    </Footer>
                                </Styles>
                                <SettingsText ConfirmDelete="Tem certeza que deseja excluir?" EmptyDataRow="Nenhum registro encontrado." />
                                <SettingsPager Position="TopAndBottom" PageSize="50" ShowDisabledButtons="False" AlwaysShowPager="True">
                                    <Summary AllPagesText="Páginas: {0} - {1} ({2} items)" Text="Página {0} de {1} ({2} registros encontrados)" />
                                </SettingsPager>
                                <Settings ShowFilterRow="True" ShowGroupButtons="False" ShowHeaderFilterButton="True" ShowFooter="True" />
                                <SettingsEditing EditFormColumnCount="4" PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1" PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter" PopupEditFormWidth="700px" />
                                <Columns>
                                    <dxwgv:GridViewDataTextColumn Caption="ID" FieldName="idPedidoFornecedor" VisibleIndex="0" Width="50">
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataDateColumn Caption="Data de Solicitação" FieldName="data" VisibleIndex="0">
                                    </dxwgv:GridViewDataDateColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Fornecedor" FieldName="fornecedorNome" VisibleIndex="0">
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Fornecedor ID" FieldName="fornecedorId" VisibleIndex="0">
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Fechar" Name="fechar" VisibleIndex="0" Width="60">
                                        <DataItemTemplate>
                                            <asp:LinkButton runat="server" OnCommand="btnFechar_OnCommand" ID="btnFechar" CommandArgument='<%# Eval("idPedidoFornecedor") %>' CausesValidation="False" EnableViewState="False">Fechar Pedido</asp:LinkButton>
                                        </DataItemTemplate>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Ver" Name="ver" VisibleIndex="0" Width="60">
                                        <DataItemTemplate>
                                            <asp:LinkButton runat="server" OnCommand="btnVer_OnCommand" ID="btnVer" CommandArgument='<%# Eval("idPedidoFornecedor") %>' CausesValidation="False" EnableViewState="False">Visualizar Pedidos</asp:LinkButton>
                                        </DataItemTemplate>
                                    </dxwgv:GridViewDataTextColumn>
                                </Columns>
                                <StylesEditors>
                                    <Label Font-Bold="True">
                                    </Label>
                                </StylesEditors>
                            </dxwgv:ASPxGridView>
                            <asp:ObjectDataSource ID="sqlPendentes" runat="server" SelectMethod="pedidosFornecedorPendentes" TypeName="rnPedidos"></asp:ObjectDataSource>
                        </td>
                    </tr>
                    <tr class="rotulos">
                        <td style="padding-top: 30px; font-size: 20px;">&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right; padding-top: 15px;">
                            <asp:Button runat="server" ID="btnGerarFaturas" Text="Gerar Faturas" OnClick="btnGerarFaturas_OnClick" Visible="False" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
