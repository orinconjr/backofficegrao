﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="relatorioChamado.aspx.cs" Inherits="admin_relatorioChamado" %>

<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.ASPxPivotGrid.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPivotGrid" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.ASPxPivotGrid.v11.1" Namespace="DevExpress.Web.ASPxPivotGrid" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" runat="Server">
    <table cellpadding="0" cellspacing="0" style="width: 920px">
        <tr>
            <td class="tituloPaginas" valign="top">
                <asp:Label ID="lblAcao" runat="server">Relatório Gerencial de Chamados</asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
                    <tr>
                        <td>
                            <asp:Label ID="lblMensagem" runat="server" ForeColor="Red"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <dx:ASPxPivotGrid OptionsData-DataFieldUnboundExpressionMode="UseSummaryValues" ID="grd" runat="server"
                                CustomizationFieldsLeft="600" CustomizationFieldsTop="400" ClientInstanceName="pivotGrid" Width="100%">
                                <Fields>
                                    <dx:PivotGridField Area="RowArea" FieldName="dataConcluidos"  Caption="Data conclusão" ID="DataConclusao" />
                                    <dx:PivotGridField Area="RowArea" FieldName="departamento"  Caption="Departamento" ID="ColumnDepartamento" />
                                    <dx:PivotGridField Area="RowArea" FieldName="usuario" Caption="Usuario" ID="ColumnUsuario" />
                                    <dx:PivotGridField Area="FilterArea" FieldName="dataConcluidos" Caption="Data Concluídos" ID="ColumnDataConcluidos" />
                                    <dx:PivotGridField Area="DataArea" FieldName="concluidos" Caption="Concluídos" ID="QtdChamadoConcluidos" />
                                    <dx:PivotGridField Area="DataArea" FieldName="naoConcluidos" Caption="Pendentes" ID="QtdChamadoAbertos" />
                                    
                                </Fields>
                                <OptionsView ShowFilterHeaders="True"  ShowHorizontalScrollBar="True" ShowColumnGrandTotals="False"  />
                                <OptionsCustomization AllowSortBySummary="True" AllowSort="True" AllowFilterInCustomizationForm="True" ></OptionsCustomization>
                                <OptionsPager RowsPerPage="100"></OptionsPager>
                            </dx:ASPxPivotGrid>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>

