﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxGridView;

public partial class admin_filaEstoque : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack && Request.QueryString["produtoId"] != null)
        {
            txtProduto.Text = Request.QueryString["produtoId"].ToString();
            fillGrid(true);
        }
    }

    private void fillGrid(bool rebind)
    {
        int produtoId = 0;
        int.TryParse(txtProduto.Text, out produtoId);

        var data = new dbCommerceDataContext();
        var aguardando = (from c in data.tbItemPedidoEstoques
                          where c.produtoId == produtoId && c.reservado == false && c.cancelado == false && c.dataLimite != null
                          orderby c.dataLimite
                          select new
                          {
                              c.idItemPedidoEstoque,
                              c.tbItensPedido.pedidoId,
                              dataLimiteReserva = c.dataLimite
                          }).ToList();
        var pedidos = (from c in data.tbPedidoFornecedorItems
                       join e in data.tbProdutoEstoques on c.idPedidoFornecedorItem equals e.idPedidoFornecedorItem into produtoEstoque
                       where (c.entregue == false  | produtoEstoque.Any(x => (x.liberado ?? false) == false)) && c.idProduto == produtoId && (c.motivo ?? "") == "" 
                       orderby c.tbPedidoFornecedor.dataLimite, c.tbPedidoFornecedor.idPedidoFornecedor
                       select new
                       {
                           c.idPedidoFornecedor,
                           c.idPedidoFornecedorItem,
                           dataLimiteFornecedor = c.tbPedidoFornecedor.dataLimite
                       }).ToList();
        var listaFinal = (from c in aguardando.Select((item, index) => new { item, index })
                          join d in pedidos.Select((item, index) => new { item, index }) on c.index equals d.index into
                              pedidosFornecedor
                          from e in pedidosFornecedor.DefaultIfEmpty()
                          select new
                          {
                              c.item.idItemPedidoEstoque,
                              c.item.pedidoId,
                              c.item.dataLimiteReserva,
                              idPedidoFornecedor = e != null ? e.item.idPedidoFornecedor : 0,
                              dataLimiteFornecedor = e != null ? (DateTime?)e.item.dataLimiteFornecedor : null,
                              idPedidoFornecedorItem = e != null ? (int?)e.item.idPedidoFornecedorItem : null

                          }
            ).ToList();
        grd.DataSource = listaFinal;
        if ((!Page.IsPostBack && !Page.IsCallback) | rebind) grd.DataBind();

        var usuarioLogadoId = Request.Cookies["usuarioLogadoId"];
        if (usuarioLogadoId != null)
            grd.Columns[9].Visible = rnUsuarios.usuarioPaginasPermitidaSeleciona_PorUsuarioIdPaginaPermitidaNome(int.Parse(usuarioLogadoId.Value), "priorizarpedido-filaestoque").Tables[0].Rows.Count == 1;

        //var listaGeralAguardando = (from c in data.tbItemPedidoEstoques
        //                            where c.reservado == false && c.cancelado == false && c.dataLimite != null
        //                            group c by new
        //                            {
        //                                c.produtoId,
        //                                dataLimite = c.dataLimite.Value.Date
        //                            } into itens
        //                            select new
        //                            {
        //                                idProduto = itens.Key.produtoId,
        //                                dataLimite = itens.Key.dataLimite.Date,
        //                                quantidade = itens.Count(),
        //                                quantidadeNaData = (from d in data.tbPedidoFornecedorItems
        //                                                    where d.entregue == false && d.idProduto == itens.Key.produtoId
        //                                                    where d.tbPedidoFornecedor.dataLimite.Date <= itens.Key.dataLimite.Date
        //                                                    select d).Count()
        //                            }).ToList();

        //var listaAtrasados = listaGeralAguardando.Where(x => x.quantidade > x.quantidadeNaData);
        var listaAguardando1 = "";
    }

    protected void gridClientes_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType != GridViewRowType.Data)
        {
            return;
        }
        bool completo = true;
        int pedidoId = 0;
        int.TryParse(grd.GetRowValues(e.VisibleIndex, new string[] { "pedidoId" }).ToString(), out pedidoId);


        var dataLimiteReserva = grd.GetRowValues(e.VisibleIndex, new string[] { "dataLimiteReserva" });
        var dataLimiteFornecedor = grd.GetRowValues(e.VisibleIndex, new string[] { "dataLimiteFornecedor" });
        if (dataLimiteFornecedor != null && dataLimiteReserva != null)
        {
            DateTime dataLimiteR = DateTime.Now;
            DateTime.TryParse(dataLimiteReserva.ToString(), out dataLimiteR);
            DateTime dataLimiteF = DateTime.Now;
            DateTime.TryParse(dataLimiteFornecedor.ToString(), out dataLimiteF);
            if (dataLimiteR.Date < dataLimiteF.Date)
            {
                int i = 0;
                while ((i < e.Row.Cells.Count))
                {
                    e.Row.Cells[i].Style.Add("background", "#FFC6C7");
                    i++;
                }
            }
            if (dataLimiteR.Date < DateTime.Now.Date)
            {
                int i = 0;
                while ((i < e.Row.Cells.Count))
                {
                    e.Row.Cells[i].Style.Add("background", "#FF151C");
                    i++;
                }
            }
        }

    }

    protected void btnGravar_OnClick(object sender, ImageClickEventArgs e)
    {

    }

    public void interacaoInclui(string interacao, string alteracaoDeEstatus, int pedidoId)
    {
        HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
        usuarioLogadoId = Request.Cookies["usuarioLogadoId"];
        if (usuarioLogadoId != null)
        {
            rnInteracoes.interacaoInclui(pedidoId, interacao, rnUsuarios.usuarioSeleciona_PorUsuarioId(int.Parse(usuarioLogadoId.Value.ToString())).Tables[0].Rows[0]["usuarioNome"].ToString(), alteracaoDeEstatus);
        }
    }

    protected void grd_OnCustomUnboundColumnData(object sender, ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "pedidoIdCliente")
        {
            int pedidoId = Convert.ToInt32(e.GetListSourceFieldValue("pedidoId"));
            e.Value = rnFuncoes.retornaIdCliente(pedidoId);
        }
    }


    protected void btnFiltrar_OnClick(object sender, EventArgs e)
    {
        int produtoId = 0;
        int.TryParse(txtProduto.Text, out produtoId);
        fillGrid(true);
    }


    protected void lbtEnviarParaInicioDaFila_OnCommand(object sender, CommandEventArgs e)
    {
        int pedidoId = Convert.ToInt32(e.CommandArgument.ToString());
        int produtoId = Convert.ToInt32(txtProduto.Text);
        using (var data = new dbCommerceDataContext())
        {
            var dadosItemPedido =
                (from c in data.tbItemPedidoEstoques
                 where c.tbItensPedido.pedidoId == pedidoId && c.produtoId == produtoId
                 select c);

            var atualPrimeiroDaFila = (from c in data.tbItemPedidoEstoques
                                       where c.produtoId == produtoId && c.reservado == false && c.cancelado == false && c.dataLimite != null
                                       orderby c.dataLimite
                                       select c).First();

            foreach (var produto in dadosItemPedido)
            {
                if (atualPrimeiroDaFila.dataLimite != null)
                    produto.dataLimite = atualPrimeiroDaFila.dataLimite.Value.AddSeconds(-1);


                var prazoDePostagemAtualPrimeiroDaFila = atualPrimeiroDaFila.tbItensPedido.tbPedido.prazoMaximoPostagemAtualizado ?? atualPrimeiroDaFila.tbItensPedido.tbPedido.prazoFinalPedido;

                if (prazoDePostagemAtualPrimeiroDaFila != null && (atualPrimeiroDaFila.dataLimite != null && prazoDePostagemAtualPrimeiroDaFila.Value < atualPrimeiroDaFila.dataLimite.Value.AddSeconds(-1)))
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('O atual primeiro da fila, pedido - " + atualPrimeiroDaFila.tbItensPedido.tbPedido.pedidoId + " - irá atrasar a postagem devido a essa priorização.')", true);
                }
                else
                {
                    data.SubmitChanges();
                }
            }
        }

        fillGrid(true);
    }

    protected void lbtSimulacao_OnCommand(object sender, CommandEventArgs e)
    {
        int pedidoId = Convert.ToInt32(e.CommandArgument.ToString());
        int produtoId = Convert.ToInt32(txtProduto.Text);
        using (var data = new dbCommerceDataContext())
        {
            var dadosItemPedido =
                (from c in data.tbItemPedidoEstoques
                 where c.tbItensPedido.pedidoId == pedidoId && c.produtoId == produtoId
                 select c);

            var atualPrimeiroDaFila = (from c in data.tbItemPedidoEstoques
                                       where c.produtoId == produtoId && c.reservado == false && c.cancelado == false && c.dataLimite != null
                                       orderby c.dataLimite
                                       select c).First();

            DadosProdutoFila produtoSimulacao = new DadosProdutoFila();
            List<DadosProdutoFila> listaProdutosSimulacao = new List<DadosProdutoFila>();

            foreach (var produto in dadosItemPedido)
            {

                produtoSimulacao.ProdutoId = produto.produtoId;
                produtoSimulacao.PedidoId = produto.tbItensPedido.pedidoId;
                produtoSimulacao.DataLimite = produto.dataLimite ?? new DateTime();

                listaProdutosSimulacao.Add(produtoSimulacao);

                if (atualPrimeiroDaFila.dataLimite != null)
                    produto.dataLimite = atualPrimeiroDaFila.dataLimite.Value.AddSeconds(-1);


                var prazoDePostagemAtualPrimeiroDaFila = atualPrimeiroDaFila.tbItensPedido.tbPedido.prazoMaximoPostagemAtualizado ?? atualPrimeiroDaFila.tbItensPedido.tbPedido.prazoFinalPedido;

                if (prazoDePostagemAtualPrimeiroDaFila != null && (atualPrimeiroDaFila.dataLimite != null && prazoDePostagemAtualPrimeiroDaFila.Value < atualPrimeiroDaFila.dataLimite.Value.AddSeconds(-1)))
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('O atual primeiro da fila, pedido - " + atualPrimeiroDaFila.tbItensPedido.tbPedido.pedidoId + " - irá atrasar a postagem devido a essa priorização.')", true);
                }
                else
                {
                    data.SubmitChanges();
                }
            }

            var aguardando = (from c in data.tbItemPedidoEstoques
                              where c.produtoId == produtoId && c.reservado == false && c.cancelado == false && c.dataLimite != null
                              orderby c.dataLimite
                              select new
                              {
                                  c.idItemPedidoEstoque,
                                  c.tbItensPedido.pedidoId,
                                  dataLimiteReserva = c.dataLimite
                              }).ToList();
            var pedidos = (from c in data.tbPedidoFornecedorItems
                           where (c.entregue == false | (c.entregue == true && (c.liberado ?? false) == false)) && c.idProduto == produtoId
                           orderby c.tbPedidoFornecedor.dataLimite, c.tbPedidoFornecedor.idPedidoFornecedor
                           select new
                           {
                               c.idPedidoFornecedor,
                               c.idPedidoFornecedorItem,
                               dataLimiteFornecedor = c.tbPedidoFornecedor.dataLimite,
                               prazoEnvio = c.tbPedido.prazoMaximoPostagemAtualizado ?? c.tbPedido.prazoFinalPedido
                           }).ToList();
            var listaFinal = (from c in aguardando.Select((item, index) => new { item, index })
                              join d in pedidos.Select((item, index) => new { item, index }) on c.index equals d.index into
                                  pedidosFornecedor
                              from p in pedidosFornecedor.DefaultIfEmpty()
                              select new
                              {
                                  c.item.idItemPedidoEstoque,
                                  c.item.pedidoId,
                                  c.item.dataLimiteReserva,
                                  idPedidoFornecedor = p != null ? p.item.idPedidoFornecedor : 0,
                                  dataLimiteFornecedor = p != null ? (DateTime?)p.item.dataLimiteFornecedor : null,
                                  idPedidoFornecedorItem = p != null ? (int?)p.item.idPedidoFornecedorItem : null,
                                  vaiAtrasar = p != null && c.item.dataLimiteReserva > p.item.prazoEnvio ? 1 : 0
                              }).ToList();

            DataGrid dgDados = new DataGrid();
            dgDados.DataSource = listaFinal;
            dgDados.DataBind();

            //foreach (var VARIABLE in produtoSimulacao)
            //{
                
            //}

        }
    }

    protected void grd_OnHtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType != GridViewRowType.Data)
        {
            return;
        }

        if (e.VisibleIndex > 0)
            return;

        if (!grd.Columns[9].Visible)
            return;

        LinkButton lbtEnviarParaInicioDaFila = (LinkButton)grd.FindRowCellTemplateControl(e.VisibleIndex, grd.Columns["priorizar"] as GridViewDataColumn, "lbtEnviarParaInicioDaFila");

        if (e.VisibleIndex == 0)
            lbtEnviarParaInicioDaFila.Visible = false;
    }

    class DadosProdutoFila
    {
        internal int PedidoId { get; set; }
        internal int ProdutoId { get; set; }
        internal DateTime DataLimite { get; set; }
    }
}