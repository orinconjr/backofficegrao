﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="relatorioMargemContribuicaoEstimada.aspx.cs" Inherits="admin_relatorioMargemContribuicaoEstimada" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dxwgv" %>
<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" Runat="Server">
    <table cellpadding="0" cellspacing="0" style="width: 920px">
    <tr>
        <td class="tituloPaginas" valign="top">
            <asp:Label ID="lblAcao" runat="server">Margem de Contribuição Estimada</asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            
    <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
        <tr>
            <td>
                <table cellpadding="0" cellspacing="0" style="width: 834px">
                    <tr>
                        <td class="rotulos" style="width: 100px">
                            Data inicial<br />
                                        <asp:TextBox ID="txtDataInicial" runat="server" CssClass="campos" 
                                                            Text='<%# Bind("faixaDeCepPesoInicial") %>' 
                                                            Width="90px" MaxLength="10"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rqvDataInicial" runat="server" 
                                                            ControlToValidate="txtDataInicial" Display="None" 
                                                            ErrorMessage="Preencha a data inicial." 
                                SetFocusOnError="True"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="rgeDataInicial" runat="server" 
                                            ControlToValidate="txtDataInicial" Display="None" 
                                            ErrorMessage="Por favor, preencha corretamente a data inicial." 
                                            SetFocusOnError="True" 
                                            
                                            
                                ValidationExpression="^(([0-2]\d|[3][0-1])\/([0]\d|[1][0-2])\/[1-2][0-9]\d{2})$"></asp:RegularExpressionValidator>
                            
                        </td>
                        <td class="rotulos" style="width: 100px">
                            Data final<br  />
                            <asp:TextBox ID="txtDataFinal" runat="server" CssClass="campos" 
                                                Text='<%# Bind("faixaDeCepPesoFinal") %>' ValidationGroup="grpInsert" 
                                                Width="90px" MaxLength="10"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rqvDataFinal" runat="server" 
                                                ControlToValidate="txtDataFinal" Display="None" 
                                                ErrorMessage="Preencha a data final." 
                                SetFocusOnError="True"></asp:RequiredFieldValidator>
                            <b>
                            <asp:RegularExpressionValidator ID="rgeDataInicial0" runat="server" 
                                            ControlToValidate="txtDataFinal" Display="None" 
                                            ErrorMessage="Por favor, preencha corretamente a data final" 
                                            SetFocusOnError="True" 
                                            
                                            
                                ValidationExpression="^(([0-2]\d|[3][0-1])\/([0]\d|[1][0-2])\/[1-2][0-9]\d{2})$"></asp:RegularExpressionValidator>
                            </b>
                        </td>
                        <td valign="bottom">
                            <asp:ImageButton ID="imbInsert" runat="server" ImageUrl="~/admin/images/btPesquisar.jpg" OnClick="imbInsert_OnClick" />
                            <asp:ValidationSummary ID="llds" runat="server" DisplayMode="List" 
                                ShowMessageBox="True" ShowSummary="False" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="rotulos">
                Receita Bruta: <asp:Literal runat="server" ID="litReceitaBruta"></asp:Literal>
            </td>
        </tr>
        <tr class="rotulos">
            <td>
                Desconto: <asp:Literal runat="server" ID="litDesconto"></asp:Literal>
            </td>
        </tr>
        <tr class="rotulos">
            <td>
                Tarifas de Pagamento: <asp:Literal runat="server" ID="litTarifasPagamento"></asp:Literal>
            </td>
        </tr>
        <tr class="rotulos">
            <td>
                Receita Líquida: <asp:Literal runat="server" ID="litReceitaLiquida"></asp:Literal>
            </td>
        </tr>
        <tr class="rotulos">
            <td>
                Custo de Mercadoria: <asp:Literal runat="server" ID="litCustoMercadoria"></asp:Literal>
            </td>
        </tr>
        <tr class="rotulos">
            <td>
                Imposto: <asp:Literal runat="server" ID="litImposto"></asp:Literal>
            </td>
        </tr>
        <tr class="rotulos">
            <td>
                Frete: <asp:Literal runat="server" ID="litFrete"></asp:Literal>
            </td>
        </tr>
        <tr class="rotulos">
            <td>
                Margem de Contribuição: <asp:Literal runat="server" ID="litMargemContribuicao"></asp:Literal>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
    </table>
        </td>
    </tr>
</table>
</asp:Content>