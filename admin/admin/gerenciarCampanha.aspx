﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" Theme="Glass" CodeFile="gerenciarCampanha.aspx.cs" Inherits="admin_gerenciarCampanha" %>

<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxTabControl" Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxClasses" Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>

<%@ Register Assembly="DevExpress.Web.v11.1" Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v11.1" Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" runat="Server">
    <style type="text/css">
        .left {
            float: left;
        }
    </style>
    <table cellpadding="0" cellspacing="0" style="width: 920px">
        <tr>
            <td class="tituloPaginas" valign="top">
                <asp:Label ID="lblAcao" runat="server">Produtos Campanha</asp:Label>
            </td>
        </tr>
    </table>

    <dx:ASPxPageControl ID="tabsSiteSeguro" runat="server" ActiveTabIndex="0" Width="100%">
        <TabPages>
            <dx:TabPage Text="Cadastrar Campanha">
                <ContentCollection>
                    <dx:ContentControl runat="server">

                        <table cellpadding="0" cellspacing="0" style="width: 920px">
                            <tr>
                                <td>
                                    <div class="left">&nbsp;&nbsp;Campanha:</div>
                                    <div>
                                        <asp:DropDownList runat="server" ID="ddlCampanhas" DataValueField="categoriaId" DataTextField="categoriaNome" AutoPostBack="True" OnSelectedIndexChanged="ddlCampanhas_OnSelectedIndexChanged" />
                                        <asp:Label runat="server" ID="lblUrlCategoria"></asp:Label>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:RadioButtonList ID="rblTipo" runat="server" RepeatDirection="Horizontal" AutoPostBack="true" CssClass="left" OnSelectedIndexChanged="OnSelectedIndexChanged">
                                        <asp:ListItem Text="Por Produto" Value="0" Selected="True" />
                                        <asp:ListItem Text="Por Fornecedor" Value="1" />
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding-left: 8px;">
                                    <br />
                                    <asp:Panel runat="server" ID="pnlPorProduto">
                                        Informar os Id's dos Produtos (ex.: 12345,67890,74185)<br />
                                        <asp:TextBox ID="txtIdsProdutos" runat="server" TextMode="MultiLine" Height="65" Width="500" />
                                        <br />
                                        <asp:CheckBox ID="ckbAddColecao" Text="Adicionar coleção a lista" runat="server" />
                                        <br />
                                    </asp:Panel>
                                    <asp:Panel runat="server" ID="pnlPorFornecedor" Visible="False">
                                        <asp:DropDownList runat="server" ID="ddlFornecedores" DataValueField="fornecedorId" DataTextField="fornecedorNome" />
                                    </asp:Panel>


                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <br />
                                    <div style="width: 515px; float: left; text-align: right;">
                                        <asp:Button runat="server" ID="btnSalvar" Text="Criar Campanha" OnClick="btnSalvar_Click" />
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:HyperLink runat="server" Target="_blank" ID="hplCampanha" Visible="False"></asp:HyperLink>
                                </td>
                            </tr>
                        </table>

                    </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>

            <dx:TabPage Text="Cadastrar Campanha Por Faixa de Desconto">
                <ContentCollection>
                    <dx:ContentControl runat="server">

                        <table cellpadding="0" cellspacing="0" style="width: 920px">
                            <tr>
                                <td>
                                    <div class="left">&nbsp;&nbsp;Campanha:</div>
                                    <asp:DropDownList runat="server" ID="ddlCampanhaFaixaDesconto" DataValueField="categoriaId" DataTextField="categoriaNome" />

                                    <br />
                                    <div class="left">&nbsp;&nbsp;Produtos com Desconto Entre (%):</div>
                                    <br />
                                    <div style="width: auto; float: left;">
                                        &nbsp;&nbsp;De:<asp:TextBox runat="server" ID="txtDescontoInicial" Width="50"></asp:TextBox>
                                        Até:<asp:TextBox runat="server" ID="txtDescontoFinal" Width="50"></asp:TextBox>
                                        <asp:Label runat="server" ID="lblQtdPRodutosFaixaDesconto"></asp:Label>
                                        <asp:Button runat="server" ID="btnQtdProdutosFaixaDesconto" Text="Informar Quantidade" OnClick="btnQtdProdutosFaixaDesconto_OnClick" />
                                    </div>
                                    <br />
                                    <br />
                                    <div style="width: 215px; float: left;">                    
                                        <br/>
                                        &nbsp;&nbsp;<asp:Button runat="server" ID="btnCadastrarCampanhaFaixaDesconto" Text="Criar Campanha" OnClick="btnCadastrarCampanhaFaixaDesconto_OnClick" Visible="False" />
                                    </div>
                                </td>
                            </tr>

                        </table>

                    </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>

            <dx:TabPage Text="Limpar Campanha">
                <ContentCollection>
                    <dx:ContentControl runat="server">

                        <table cellpadding="0" cellspacing="0" style="width: 920px">
                            <tr>
                                <td>
                                    <div class="left">&nbsp;&nbsp;Limpar Campanha:</div>
                                    <br />
                                    <div style="width: auto; float: left;">
                                        <asp:DropDownList runat="server" ID="ddlLimparCampanha" DataValueField="categoriaId" DataTextField="categoriaNome" Height="21px" />
                                    </div>
                                    <div style="width: 215px; float: left;">
                                        <asp:Button runat="server" ID="btnLimparCampanha" Text="Criar Campanha" OnClick="btnLimparCampanha_OnClick" OnClientClick="return confirm('Todos os produtos serão removidos da campanha informada!\nConfirma está ação?')" />
                                    </div>
                                </td>
                            </tr>

                        </table>

                    </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>

        </TabPages>
    </dx:ASPxPageControl>



</asp:Content>



