﻿<%@ Page Title="" Language="C#" Theme="Glass" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="acompanhamentoDePrazos.aspx.cs" Inherits="admin_acompanhamentoDePrazos" %>

<%@ Register TagPrefix="dx" Namespace="DevExpress.Data.Linq" Assembly="DevExpress.Web.v11.1.Linq, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>
<%@ Register TagPrefix="dxwgv" Namespace="DevExpress.Web.ASPxGridView" Assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>
<%@ Register TagPrefix="dxwgv" Namespace="DevExpress.Web.ASPxGridView.Export" Assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" runat="Server">

    <table cellpadding="0" cellspacing="0" style="width: 920px">
        <tr>
            <td class="tituloPaginas" valign="top">
                <asp:Label ID="lblAcao" runat="server">Acampanhamento de Prazos</asp:Label>
            </td>
        </tr>
        <tr>
            <td>

                <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
                    <tr>
                        <td align="right">
                            <table cellpadding="0" cellspacing="0" style="width: 834px">
                                <tr>
                                    <td align="right">&nbsp;</td>
                                    <td height="38"
                                        style="background-image: url('images/bkExportar.jpg'); padding-bottom: 1px;"
                                        valign="bottom" width="231">
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 97px">&nbsp;</td>
                                                <td style="width: 33px">
                                                    <asp:ImageButton ID="btPdf" runat="server" ImageUrl="~/admin/images/btPdf.jpg" 
                                                        Style="margin-left: 5px" />
                                                </td>
                                                <td style="width: 31px">
                                                    <asp:ImageButton ID="btXsl" runat="server" ImageUrl="~/admin/images/btXls.jpg" 
                                                        Style="margin-left: 5px" />
                                                </td>
                                                <td style="width: 36px">
                                                    <asp:ImageButton ID="btRtf" runat="server" ImageUrl="~/admin/images/btRtf.jpg" 
                                                        Style="margin-left: 5px" />
                                                </td>
                                                <td valign="bottom">
                                                    <asp:ImageButton ID="btCsv" runat="server" ImageUrl="~/admin/images/btCsv.jpg" 
                                                        Style="margin-left: 5px" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <dxwgv:ASPxGridView ID="grd" ClientInstanceName="grd" runat="server" AutoGenerateColumns="False"
                                DataSourceID="SqlDataSource1" KeyFieldName="produtoId" Width="834px" Settings-ShowFilterRow="True" Settings-ShowFilterBar="Visible"
                                Cursor="auto">
                                <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                                <Styles>
                                    <Footer Font-Bold="True">
                                    </Footer>
                                </Styles>
                                <SettingsText ConfirmDelete="Tem certeza que deseja excluir?" EmptyDataRow="Nenhum registro encontrado." />
                                <SettingsPager Position="TopAndBottom" PageSize="80"
                                    ShowDisabledButtons="False" AlwaysShowPager="True">
                                    <Summary AllPagesText="Páginas: {0} - {1} ({2} items)"
                                        Text="Página {0} de {1} ({2} registros encontrados)" />
                                </SettingsPager>
                                <Settings ShowFilterRow="True" ShowGroupButtons="False" ShowFooter="True" />
                                <TotalSummary>
                                    <dxwgv:ASPxSummaryItem DisplayFormat="c" FieldName="valorCobrado"
                                        ShowInColumn="Valor" ShowInGroupFooterColumn="Valor" SummaryType="Sum" />
                                </TotalSummary>
                                <SettingsEditing EditFormColumnCount="4"
                                    PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1"
                                    PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter"
                                    PopupEditFormWidth="700px" />
                                <Columns>
                                    <dxwgv:GridViewDataTextColumn Caption="ProdutoId" FieldName="produtoId" VisibleIndex="0">
                                        <EditFormSettings Visible="False" />
                                        <Settings AutoFilterCondition="Contains" />
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Produto" FieldName="produtoNome" VisibleIndex="0">
                                        <EditFormSettings Visible="False" />
                                        <Settings AutoFilterCondition="Contains" />
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Fornecedor" FieldName="fornecedorNome" VisibleIndex="0">
                                        <EditFormSettings Visible="False" />
                                        <Settings AutoFilterCondition="Contains" />
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Prazo Produto" FieldName="fornecedorPrazoDeFabricacao" VisibleIndex="0">
                                        <EditFormSettings Visible="False" />
                                        <Settings AutoFilterCondition="Contains" />
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Maior Prazo dos Itens" FieldName="MaiorPrazoDeFornecedor" VisibleIndex="0" Settings-AutoFilterCondition="Contains">
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Maior Prazo Fornecedor (Nome)" FieldName="MaiorPrazoDeFornecedorNome" VisibleIndex="0" Settings-AutoFilterCondition="Contains">
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Maior Prazo Fornecedor (Item)" FieldName="MaiorPrazoDeFornecedorNomeItem" VisibleIndex="0" Settings-AutoFilterCondition="Contains">
                                    </dxwgv:GridViewDataTextColumn>
                                </Columns>
                                <StylesEditors>
                                    <Label Font-Bold="True">
                                    </Label>
                                </StylesEditors>
                            </dxwgv:ASPxGridView>

                           <%-- <asp:SqlDataSource ConnectionString="<%$ ConnectionStrings:connectionString %>" runat="server" ID="SqlDataSource1"
                                               SelectCommand="SELECT * FROM (SELECT DISTINCT p.produtoId, p.produtoNome, pf.fornecedorNome ,pf.fornecedorPrazoDeFabricacao, 
                                                (SELECT TOP 1 pf2.fornecedorPrazoDeFabricacao FROM tbProdutos p2 
                                                   INNER JOIN tbProdutoRelacionado pl2 ON p2.produtoId = pl2.idProdutoFilho
                                                   INNER JOIN tbProdutoFornecedor pf2 ON p2.produtoFornecedor = pf2.fornecedorId 
                                                   WHERE pl2.idProdutoPai = p.produtoId
                                                  ORDER BY pf2.fornecedorPrazoDeFabricacao DESC) as MaiorPrazoDeFornecedor,
                                                (SELECT TOP 1 pf2.fornecedorNome + ' - ' + p2.produtoNome COLLATE Latin1_General_CI_AI FROM tbProdutos p2 
                                                   INNER JOIN tbProdutoRelacionado pl2 ON p2.produtoId = pl2.idProdutoFilho
                                                   INNER JOIN tbProdutoFornecedor pf2 ON p2.produtoFornecedor = pf2.fornecedorId 
                                                   WHERE pl2.idProdutoPai = p.produtoId
                                                  ORDER BY pf2.fornecedorPrazoDeFabricacao DESC) as MaiorPrazoDeFornecedorNome
                                                FROM tbProdutos p 
                                                INNER JOIN tbProdutoRelacionado pl ON p.produtoId = pl.idProdutoPai
                                                INNER JOIN tbProdutoFornecedor pf ON p.produtoFornecedor = pf.fornecedorId
                                                WHERE p.produtoAtivo = 'True'
                                                ) Tab WHERE Tab.fornecedorPrazoDeFabricacao <> Tab.MaiorPrazoDeFornecedor AND Tab.fornecedorPrazoDeFabricacao > 0 ORDER BY Tab.produtoNome"></asp:SqlDataSource>--%>
                             <asp:SqlDataSource ConnectionString="<%$ ConnectionStrings:connectionString %>" runat="server" ID="SqlDataSource1"
                                               SelectCommand="SELECT * FROM (SELECT DISTINCT p.produtoId, p.produtoNome, pf.fornecedorNome ,pf.fornecedorPrazoDeFabricacao, 
                                                (SELECT TOP 1 pf2.fornecedorPrazoDeFabricacao FROM tbProdutos p2 
                                                   INNER JOIN tbProdutoRelacionado pl2 ON p2.produtoId = pl2.idProdutoFilho
                                                   INNER JOIN tbProdutoFornecedor pf2 ON p2.produtoFornecedor = pf2.fornecedorId 
                                                   WHERE pl2.idProdutoPai = p.produtoId
                                                  ORDER BY pf2.fornecedorPrazoDeFabricacao DESC) as MaiorPrazoDeFornecedor,
                                                (SELECT TOP 1 pf2.fornecedorNome FROM tbProdutos p2 
                                                   INNER JOIN tbProdutoRelacionado pl2 ON p2.produtoId = pl2.idProdutoFilho
                                                   INNER JOIN tbProdutoFornecedor pf2 ON p2.produtoFornecedor = pf2.fornecedorId 
                                                   WHERE pl2.idProdutoPai = p.produtoId
                                                  ORDER BY pf2.fornecedorPrazoDeFabricacao DESC) as MaiorPrazoDeFornecedorNome,
											    (SELECT TOP 1 p2.produtoNome FROM tbProdutos p2 
													    INNER JOIN tbProdutoRelacionado pl2 ON p2.produtoId = pl2.idProdutoFilho
													    INNER JOIN tbProdutoFornecedor pf2 ON p2.produtoFornecedor = pf2.fornecedorId 
													    WHERE pl2.idProdutoPai = p.produtoId
												    ORDER BY pf2.fornecedorPrazoDeFabricacao DESC) as MaiorPrazoDeFornecedorNomeItem
                                                FROM tbProdutos p 
                                                INNER JOIN tbProdutoRelacionado pl ON p.produtoId = pl.idProdutoPai
                                                INNER JOIN tbProdutoFornecedor pf ON p.produtoFornecedor = pf.fornecedorId
                                                WHERE p.produtoAtivo = 'True'
                                                ) Tab WHERE Tab.fornecedorPrazoDeFabricacao <> Tab.MaiorPrazoDeFornecedor AND Tab.fornecedorPrazoDeFabricacao > 0 ORDER BY Tab.produtoNome"></asp:SqlDataSource>
                            <dxwgv:ASPxGridViewExporter ID="grdEx" runat="server" FileName="pedidos"
                                GridViewID="grd" Landscape="True" PreserveGroupRowStates="True">
                            </dxwgv:ASPxGridViewExporter>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

</asp:Content>

