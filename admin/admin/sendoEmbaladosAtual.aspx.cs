﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxGridView;

public partial class admin_sendoEmbaladosAtual : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["cancelar"] != null)
        {
            int pedidoId = Convert.ToInt32(Request.QueryString["cancelar"]);

            var pedidoDc = new dbCommerceDataContext();
            var pedido = (from c in pedidoDc.tbPedidos where c.pedidoId == pedidoId select c).First();
            pedido.idUsuarioEmbalado = null;
            pedido.dataSendoEmbalado = null;
            pedido.dataEmbaladoFim = null;
            pedido.volumesEmbalados = null;
            pedido.idUsuarioSeparacao = null;
            pedido.separado = false;
            pedido.dataInicioSeparacao = null;
            pedido.statusDoPedido = 11;
            pedido.envioLiberado = false;
            pedidoDc.SubmitChanges();

            var envio = (from c in pedidoDc.tbPedidoEnvios where c.idPedido == pedidoId && c.dataInicioEmbalagem == null orderby c.idPedidoEnvio descending select c).FirstOrDefault();
            if (envio != null)
            {
                var pacotes = (from c in pedidoDc.tbPedidoPacotes where c.idPedidoEnvio == envio.idPedidoEnvio select c);
                foreach (var pacote in pacotes)
                {
                    pedidoDc.tbPedidoPacotes.DeleteOnSubmit(pacote);
                }
                pedidoDc.SubmitChanges();

                var itensSeparados = (from c in pedidoDc.tbProdutoEstoques where c.idPedidoEnvio == envio.idPedidoEnvio select c).ToList();
                foreach (var itensSeparado in itensSeparados)
                {
                    itensSeparado.idPedidoEnvio = null;
                    itensSeparado.pedidoId = null;
                    itensSeparado.itemPedidoId = null;
                    itensSeparado.idItemPedidoEstoque = null;
                    pedidoDc.SubmitChanges();
                }
                pedidoDc.tbPedidoEnvios.DeleteOnSubmit(envio);
                pedidoDc.SubmitChanges();
            }
        }

        if (Request.QueryString["verificarDuplicados"] != null)
        {
            pnlGridRastreioDuplicado.Visible = true;
        }

        if (Request.QueryString["verificarQtdVolumesErrados"] != null)
        {
            pnlGridVolumesIncorretos.Visible = true;
        }

        fillGrid(false);

    }

    private void fillGrid(bool rebind)
    {
        var data = new dbCommerceDataContext();
        int idPedido = 0;
        int.TryParse(Request.QueryString["pedidoId"], out idPedido);

        int idCentroDistribuicao = rdbCd1.Checked ? 1 : rdbCd2.Checked ? 2 : rdbCd3.Checked ? 3 : rdbCd4.Checked ? 4 : 5;

        if (idCentroDistribuicao == 1)
        {
            using (
                var txn =
                    new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required,
                        new System.Transactions.TransactionOptions
                        {
                            IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                        }))

            {
                var pedidoSendoSeparados = (from c in data.tbPedidos
                                        where (c.separadoCd1 == false && c.itensPendenteEnvioCd1 > 0 && c.tbPedidoEnvios.Count(x => x.idCentroDeDistribuicao == 1 && x.dataFimSeparacao == null && x.dataInicioSeparacao != null) > 0) | c.pedidoId == idPedido
                                        orderby c.dataSendoEmbalado descending
                                        select new
                                        {
                                            c.pedidoId,
                                            c.dataSendoEmbalado,
                                            c.idUsuarioEmbalado,
                                            c.tbPedidoEnvios.First(x => x.idCentroDeDistribuicao == 1 && x.dataFimSeparacao == null && x.dataInicioSeparacao != null).idUsuarioSeparacao,
                                            (from d in data.tbPedidoEnvios where d.idPedido == c.pedidoId && d.idCentroDeDistribuicao == 1 && d.dataFimSeparacao == null && d.dataInicioSeparacao != null
                                             join e in data.tbUsuarioExpedicaos on d.idUsuarioSeparacao equals e.idUsuarioExpedicao select e).FirstOrDefault().nome,
                                            c.tbPedidoEnvios.First(x => x.idCentroDeDistribuicao == 1 && x.dataFimSeparacao == null && x.dataInicioSeparacao != null).dataInicioSeparacao
                                        });
            grdSendoSeparados.DataSource = pedidoSendoSeparados;


            var pedidoSendoEmbalados = (from c in data.tbPedidos
                                        where (c.statusDoPedido == 4 && c.tbPedidoEnvios.Count(x => x.idCentroDeDistribuicao == 1 && x.dataFimEmbalagem == null && x.dataInicioEmbalagem != null) > 0) | c.pedidoId == idPedido
                                        orderby c.dataSendoEmbalado descending
                                        select new
                                        {
                                            c.pedidoId,
                                            (from d in data.tbPedidoEnvios
                                             where d.idPedido == c.pedidoId && d.idCentroDeDistribuicao == 1 && d.dataFimEmbalagem == null && d.dataInicioEmbalagem != null
                                             select d).FirstOrDefault().dataInicioEmbalagem,
                                            (from d in data.tbPedidoEnvios
                                             where d.idPedido == c.pedidoId && d.idCentroDeDistribuicao == 1 && d.dataFimEmbalagem == null && d.dataInicioEmbalagem != null
                                             select d).FirstOrDefault().idUsuarioEmbalagem,
                                            c.idUsuarioSeparacao,
                                            (from d in data.tbPedidoEnvios
                                             where d.idPedido == c.pedidoId && d.idCentroDeDistribuicao == 1 && d.dataFimEmbalagem == null && d.dataInicioEmbalagem != null
                                             join e in data.tbUsuarioExpedicaos on d.idUsuarioEmbalagem equals e.idUsuarioExpedicao
                                             select e).FirstOrDefault().nome
                                        });
            grdSendoEmbalados.DataSource = pedidoSendoEmbalados;
            if ((!Page.IsPostBack && !Page.IsCallback) | rebind) grdSendoSeparados.DataBind();
            if ((!Page.IsPostBack && !Page.IsCallback) | rebind) grdSendoEmbalados.DataBind();
        }
        }
        if (idCentroDistribuicao == 2)
        {
            using (
                var txn =
                    new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required,
                        new System.Transactions.TransactionOptions
                        {
                            IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                        }))

            {
                var pedidoSendoSeparados = (from c in data.tbPedidos
                                        where (c.separadoCd1 == false && c.itensPendenteEnvioCd1 > 0 && c.tbPedidoEnvios.Count(x => (x.idCentroDeDistribuicao == 1 | x.idCentroDeDistribuicao == 2) && x.dataFimSeparacaoCd2 == null && x.dataInicioSeparacaoCd2 != null) > 0) | c.pedidoId == idPedido
                                        orderby c.dataSendoEmbalado descending
                                        select new
                                        {
                                            c.pedidoId,
                                            c.dataSendoEmbalado,
                                            c.idUsuarioEmbalado,
                                            c.tbPedidoEnvios.First(x => (x.idCentroDeDistribuicao == 1 | x.idCentroDeDistribuicao == 2) && x.dataFimSeparacaoCd2 == null && x.dataInicioSeparacaoCd2 != null).idUsuarioSeparacaoCd2,
                                            (from d in data.tbPedidoEnvios
                                             where d.idPedido == c.pedidoId && (d.idCentroDeDistribuicao == 1 | d.idCentroDeDistribuicao == 2) && d.dataFimSeparacaoCd2 == null && d.dataInicioSeparacaoCd2 != null
                                             join e in data.tbUsuarioExpedicaos on d.idUsuarioSeparacaoCd2 equals e.idUsuarioExpedicao
                                             select e).FirstOrDefault().nome,
                                            c.tbPedidoEnvios.First(x => (x.idCentroDeDistribuicao == 1 | x.idCentroDeDistribuicao == 2) && x.dataFimSeparacaoCd2 == null && x.dataInicioSeparacaoCd2 != null).dataInicioSeparacaoCd2
                                        });
            grdSendoSeparados.DataSource = pedidoSendoSeparados;


            var pedidoSendoEmbalados = (from c in data.tbPedidos
                                        where (c.statusDoPedido == 4 && c.tbPedidoEnvios.Count(x => (x.idCentroDeDistribuicao == 1 | x.idCentroDeDistribuicao == 2) && x.dataFimEmbalagem == null && x.dataInicioEmbalagem != null && x.dataInicioSeparacao != null && x.dataFimSeparacaoCd2 != null) > 0) | c.pedidoId == idPedido
                                        orderby c.dataSendoEmbalado descending
                                        select new
                                        {
                                            c.pedidoId,
                                            (from d in data.tbPedidoEnvios
                                             where d.idPedido == c.pedidoId && d.idCentroDeDistribuicao == 1 && d.dataFimEmbalagem == null && d.dataInicioEmbalagem != null
                                             select d).FirstOrDefault().dataInicioEmbalagem,
                                            (from d in data.tbPedidoEnvios
                                             where d.idPedido == c.pedidoId && d.idCentroDeDistribuicao == 1 && d.dataFimEmbalagem == null && d.dataInicioEmbalagem != null
                                             select d).FirstOrDefault().idUsuarioEmbalagem,
                                            c.idUsuarioSeparacao,
                                            (from d in data.tbPedidoEnvios
                                             where d.idPedido == c.pedidoId && d.idCentroDeDistribuicao == 1 && d.dataFimEmbalagem == null && d.dataInicioEmbalagem != null
                                             join e in data.tbUsuarioExpedicaos on d.idUsuarioEmbalagem equals e.idUsuarioExpedicao
                                             select e).FirstOrDefault().nome
                                        });
            grdSendoEmbalados.DataSource = pedidoSendoEmbalados;
            if ((!Page.IsPostBack && !Page.IsCallback) | rebind) grdSendoSeparados.DataBind();
            if ((!Page.IsPostBack && !Page.IsCallback) | rebind) grdSendoEmbalados.DataBind();
        }
        }
        if (idCentroDistribuicao == 3)
        {
            using (
                var txn =
                    new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required,
                        new System.Transactions.TransactionOptions
                        {
                            IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                        }))

            {
                var pedidoSendoSeparados = (from c in data.tbPedidos
                                        where (c.separadoCd1 == false && c.itensPendenteEnvioCd1 > 0 && c.tbPedidoEnvios.Count(x => (x.idCentroDeDistribuicao == 3) && x.dataFimSeparacaoCd2 == null && x.dataInicioSeparacaoCd2 != null) > 0) | c.pedidoId == idPedido
                                        orderby c.dataSendoEmbalado descending
                                        select new
                                        {
                                            c.pedidoId,
                                            c.dataSendoEmbalado,
                                            c.idUsuarioEmbalado,
                                            c.tbPedidoEnvios.First(x => (x.idCentroDeDistribuicao == 3) && x.dataFimSeparacaoCd2 == null && x.dataInicioSeparacaoCd2 != null).idUsuarioSeparacaoCd2,
                                            (from d in data.tbPedidoEnvios
                                             where d.idPedido == c.pedidoId && (d.idCentroDeDistribuicao == 1 | d.idCentroDeDistribuicao == 2) && d.dataFimSeparacaoCd2 == null && d.dataInicioSeparacaoCd2 != null
                                             join e in data.tbUsuarioExpedicaos on d.idUsuarioSeparacaoCd2 equals e.idUsuarioExpedicao
                                             select e).FirstOrDefault().nome,
                                            c.tbPedidoEnvios.First(x => (x.idCentroDeDistribuicao == 3) && x.dataFimSeparacaoCd2 == null && x.dataInicioSeparacaoCd2 != null).dataInicioSeparacaoCd2
                                        });
            grdSendoSeparados.DataSource = pedidoSendoSeparados;
            if ((!Page.IsPostBack && !Page.IsCallback) | rebind) grdSendoSeparados.DataBind();
            if ((!Page.IsPostBack && !Page.IsCallback) | rebind) grdSendoEmbalados.DataBind();


            var pedidoSendoEmbalados = (from c in data.tbPedidos
                                        where (c.statusDoPedido == 4 && c.tbPedidoEnvios.Count(x => (x.idCentroDeDistribuicao == 3) && x.dataFimEmbalagem == null && x.dataInicioEmbalagem != null && x.dataInicioSeparacao != null && x.dataFimSeparacaoCd2 != null) > 0) | c.pedidoId == idPedido
                                        orderby c.dataSendoEmbalado descending
                                        select new
                                        {
                                            c.pedidoId,
                                            (from d in data.tbPedidoEnvios
                                             where d.idPedido == c.pedidoId && d.idCentroDeDistribuicao == 1 && d.dataFimEmbalagem == null && d.dataInicioEmbalagem != null
                                             select d).FirstOrDefault().dataInicioEmbalagem,
                                            (from d in data.tbPedidoEnvios
                                             where d.idPedido == c.pedidoId && d.idCentroDeDistribuicao == 1 && d.dataFimEmbalagem == null && d.dataInicioEmbalagem != null
                                             select d).FirstOrDefault().idUsuarioEmbalagem,
                                            c.idUsuarioSeparacao,
                                            (from d in data.tbPedidoEnvios
                                             where d.idPedido == c.pedidoId && d.idCentroDeDistribuicao == 1 && d.dataFimEmbalagem == null && d.dataInicioEmbalagem != null
                                             join e in data.tbUsuarioExpedicaos on d.idUsuarioEmbalagem equals e.idUsuarioExpedicao
                                             select e).FirstOrDefault().nome
                                        });
            grdSendoEmbalados.DataSource = pedidoSendoEmbalados;
            if ((!Page.IsPostBack && !Page.IsCallback) | rebind) grdSendoSeparados.DataBind();
            if ((!Page.IsPostBack && !Page.IsCallback) | rebind) grdSendoEmbalados.DataBind();
        }
        }
        if (idCentroDistribuicao == 4)
        {
            using (
                var txn =
                    new System.Transactions.TransactionScope( System.Transactions.TransactionScopeOption.Required,
                        new System.Transactions.TransactionOptions
                        {
                            IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                        }))

            {
                var pedidoSendoSeparados = (from c in data.tbPedidos
                                            where (c.separadoCd4 == false && c.itensPendentesEnvioCd4 > 0 && c.tbPedidoEnvios.Count(x => x.idCentroDeDistribuicao == 4 && x.dataFimSeparacao == null && x.dataInicioSeparacao != null) > 0) | c.pedidoId == idPedido
                                            orderby c.dataSendoEmbalado descending
                                            select new
                                            {
                                                c.pedidoId,
                                                c.dataSendoEmbalado,
                                                c.idUsuarioEmbalado,
                                                c.tbPedidoEnvios.First(x => x.idCentroDeDistribuicao == 4 && x.dataFimSeparacao == null && x.dataInicioSeparacao != null).idUsuarioSeparacao,
                                                (from d in data.tbPedidoEnvios
                                                 where d.idPedido == c.pedidoId && d.idCentroDeDistribuicao == 4 && d.dataFimSeparacao == null && d.dataInicioSeparacao != null
                                                 join e in data.tbUsuarioExpedicaos on d.idUsuarioSeparacao equals e.idUsuarioExpedicao
                                                 select e).FirstOrDefault().nome,
                                                c.tbPedidoEnvios.First(x => x.idCentroDeDistribuicao == 4 && x.dataFimSeparacao == null && x.dataInicioSeparacao != null).dataInicioSeparacao
                                            });
                grdSendoSeparados.DataSource = pedidoSendoSeparados;


                var pedidoSendoEmbalados = (from c in data.tbPedidos
                                            where (c.statusDoPedido == 4 && c.tbPedidoEnvios.Count(x => x.idCentroDeDistribuicao == 4 && x.dataFimEmbalagem == null && x.dataInicioEmbalagem != null) > 0) | c.pedidoId == idPedido
                                            orderby c.dataSendoEmbalado descending
                                            select new
                                            {
                                                c.pedidoId,
                                                (from d in data.tbPedidoEnvios
                                                 where d.idPedido == c.pedidoId && d.idCentroDeDistribuicao == 4 && d.dataFimEmbalagem == null && d.dataInicioEmbalagem != null
                                                 select d).FirstOrDefault().dataInicioEmbalagem,
                                                (from d in data.tbPedidoEnvios
                                                 where d.idPedido == c.pedidoId && d.idCentroDeDistribuicao == 4 && d.dataFimEmbalagem == null && d.dataInicioEmbalagem != null
                                                 select d).FirstOrDefault().idUsuarioEmbalagem,
                                                c.idUsuarioSeparacao,
                                                (from d in data.tbPedidoEnvios
                                                 where d.idPedido == c.pedidoId && d.idCentroDeDistribuicao == 4 && d.dataFimEmbalagem == null && d.dataInicioEmbalagem != null
                                                 join e in data.tbUsuarioExpedicaos on d.idUsuarioEmbalagem equals e.idUsuarioExpedicao
                                                 select e).FirstOrDefault().nome
                                            });
                grdSendoEmbalados.DataSource = pedidoSendoEmbalados;
                if ((!Page.IsPostBack && !Page.IsCallback) | rebind) grdSendoSeparados.DataBind();
                if ((!Page.IsPostBack && !Page.IsCallback) | rebind) grdSendoEmbalados.DataBind();
            }
        }
        if (idCentroDistribuicao == 5)
        {
            using (
                var txn =
                    new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required,
                        new System.Transactions.TransactionOptions
                        {
                            IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                        }))

            {
                var pedidoSendoSeparados = (from c in data.tbPedidos
                                            where (c.separadoCd5 == false && c.itensPendentesEnvioCd5 > 0 && c.tbPedidoEnvios.Count(x => x.idCentroDeDistribuicao == 5 && x.dataFimSeparacao == null && x.dataInicioSeparacao != null) > 0) | c.pedidoId == idPedido
                                            orderby c.dataSendoEmbalado descending
                                            select new
                                            {
                                                c.pedidoId,
                                                c.dataSendoEmbalado,
                                                c.idUsuarioEmbalado,
                                                c.tbPedidoEnvios.First(x => x.idCentroDeDistribuicao == 5 && x.dataFimSeparacao == null && x.dataInicioSeparacao != null).idUsuarioSeparacao,
                                                (from d in data.tbPedidoEnvios
                                                 where d.idPedido == c.pedidoId && d.idCentroDeDistribuicao == 5 && d.dataFimSeparacao == null && d.dataInicioSeparacao != null
                                                 join e in data.tbUsuarioExpedicaos on d.idUsuarioSeparacao equals e.idUsuarioExpedicao
                                                 select e).FirstOrDefault().nome,
                                                c.tbPedidoEnvios.First(x => x.idCentroDeDistribuicao == 5 && x.dataFimSeparacao == null && x.dataInicioSeparacao != null).dataInicioSeparacao
                                            });
                grdSendoSeparados.DataSource = pedidoSendoSeparados;


                var pedidoSendoEmbalados = (from c in data.tbPedidos
                                            where (c.statusDoPedido == 4 && c.tbPedidoEnvios.Count(x => x.idCentroDeDistribuicao == 5 && x.dataFimEmbalagem == null && x.dataInicioEmbalagem != null) > 0) | c.pedidoId == idPedido
                                            orderby c.dataSendoEmbalado descending
                                            select new
                                            {
                                                c.pedidoId,
                                                (from d in data.tbPedidoEnvios
                                                 where d.idPedido == c.pedidoId && d.idCentroDeDistribuicao == 5 && d.dataFimEmbalagem == null && d.dataInicioEmbalagem != null
                                                 select d).FirstOrDefault().dataInicioEmbalagem,
                                                (from d in data.tbPedidoEnvios
                                                 where d.idPedido == c.pedidoId && d.idCentroDeDistribuicao == 5 && d.dataFimEmbalagem == null && d.dataInicioEmbalagem != null
                                                 select d).FirstOrDefault().idUsuarioEmbalagem,
                                                c.idUsuarioSeparacao,
                                                (from d in data.tbPedidoEnvios
                                                 where d.idPedido == c.pedidoId && d.idCentroDeDistribuicao == 5 && d.dataFimEmbalagem == null && d.dataInicioEmbalagem != null
                                                 join e in data.tbUsuarioExpedicaos on d.idUsuarioEmbalagem equals e.idUsuarioExpedicao
                                                 select e).FirstOrDefault().nome
                                            });
                grdSendoEmbalados.DataSource = pedidoSendoEmbalados;
                if ((!Page.IsPostBack && !Page.IsCallback) | rebind) grdSendoSeparados.DataBind();
                if ((!Page.IsPostBack && !Page.IsCallback) | rebind) grdSendoEmbalados.DataBind();
            }
        }


    }


    protected void btPdf_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WritePdfToResponse();
    }
    protected void btXsl_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteXlsToResponse();
    }
    protected void btRtf_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteRtfToResponse();
    }
    protected void btCsv_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteCsvToResponse();
    }
    protected void grdSendoSeparados_OnCustomUnboundColumnData(object sender, ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "pedidoIdCliente")
        {
            int pedidoId = Convert.ToInt32(e.GetListSourceFieldValue("pedidoId"));
            e.Value = rnFuncoes.retornaIdCliente(pedidoId);
        }
    }
    protected void grdSendoEmbalados_OnCustomUnboundColumnData(object sender, ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "pedidoIdCliente")
        {
            int pedidoId = Convert.ToInt32(e.GetListSourceFieldValue("pedidoId"));
            e.Value = rnFuncoes.retornaIdCliente(pedidoId);
        }
    }

    protected void grdVolumeErrado_OnCustomUnboundColumnData(object sender, ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "pedidoIdCliente")
        {
            int pedidoId = Convert.ToInt32(e.GetListSourceFieldValue("pedidoId"));
            e.Value = rnFuncoes.retornaIdCliente(pedidoId);
        }
    }

    protected void btnCancelar_OnCommand(object sender, CommandEventArgs e)
    {
        pcCancelarEmbalagem.ShowOnPageLoad = true;
        hdfPedidoId.Value = e.CommandArgument.ToString();
        int pedidoId = Convert.ToInt32(hdfPedidoId.Value);
        var data = new dbCommerceDataContext();
        var itensPedidoEstoque = (from c in data.tbItemPedidoEstoques
                                  where c.tbItensPedido.pedidoId == pedidoId && c.enviado == false && c.cancelado == false
                                  select new
                                  {
                                      c.itemPedidoId,
                                      c.tbItensPedido.pedidoId,
                                      c.idItemPedidoEstoque,
                                      c.tbProduto.produtoNome,
                                      c.produtoId
                                  }).Distinct();

        lstItensPedido.DataSource = itensPedidoEstoque;
        lstItensPedido.DataBind();
    }

    protected void btnExcluirRastreio_OnCommand(object sender, CommandEventArgs e)
    {
        int idPedidoPacote = Convert.ToInt32(e.CommandArgument);
        using (var data = new dbCommerceDataContext())
        {
            var excluirRastreio = (from c in data.tbPedidoPacotes where c.idPedidoPacote == idPedidoPacote select c).FirstOrDefault();
            data.tbPedidoPacotes.DeleteOnSubmit(excluirRastreio);

            var pedidoEnvio = (from c in data.tbPedidoEnvios where c.idPedido == excluirRastreio.idPedido orderby c.dataFimEmbalagem descending select c).FirstOrDefault();

            data.SubmitChanges();

            var pacotes = (from c in data.tbPedidoPacotes where c.idPedidoEnvio == pedidoEnvio.idPedidoEnvio select c);
            pedidoEnvio.volumesEmbalados = pacotes.Count();

            int numeroPacote = 0;
            foreach (var pacote in pacotes)
            {
                numeroPacote++;
                pacote.numeroVolume = numeroPacote;
            }

            data.SubmitChanges();

        }

        fillGridRastreioDuplicado();


    }

    protected void lstItensPedido_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        var produtosDc = new dbCommerceDataContext();
        HiddenField hdfProdutoId = (HiddenField)e.Item.FindControl("hdfProdutoId");
        HiddenField hdfQuantidade = (HiddenField)e.Item.FindControl("hdfQuantidade");
        HiddenField hdfItemPedidoId = (HiddenField)e.Item.FindControl("hdfItemPedidoId");
        Literal litNome = (Literal)e.Item.FindControl("litNome");
        CheckBox chkCancelar = (CheckBox)e.Item.FindControl("chkCancelar");

        int itemPedidoId = Convert.ToInt32(hdfItemPedidoId.Value);
        int produtoId = Convert.ToInt32(hdfProdutoId.Value);
        var produto = (from c in produtosDc.tbProdutos where c.produtoId == produtoId select c).First();

    }

    protected void lstItensPedidoCombo_OnItemDataBound(object sender, ListViewItemEventArgs e)
    {
        HiddenField hdfQuantidadeCombo = (HiddenField)e.Item.FindControl("hdfQuantidadeCombo");
        DropDownList ddlQuantidadeCombo = (DropDownList)e.Item.FindControl("ddlQuantidadeCombo");

        int quantidade = Convert.ToInt32(hdfQuantidadeCombo.Value);

        for (int i = 1; i <= quantidade; i++)
        {
            ddlQuantidadeCombo.Items.Add(new ListItem(i.ToString(), i.ToString()));
        }
        ddlQuantidadeCombo.SelectedValue = quantidade.ToString();
    }

    protected void btnCancelarEmbalagem_OnClick(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(txtMotivo.Text))
        {
            AlertShow("Favor preencher o motivo para cancelamento da embalagem");
            txtMotivo.Focus();
            return;
        }

        var data = new dbCommerceDataContext();
        int pedidoID = Convert.ToInt32(hdfPedidoId.Value);
        List<int> produtoIdsCancelar = new List<int>();
        if (pnProdutosCancelar.Visible)
        {
            foreach (var itemPedido in lstItensPedido.Items)
            {
                HiddenField hdfProdutoId = (HiddenField)itemPedido.FindControl("hdfProdutoId");
                CheckBox chkCancelar = (CheckBox)itemPedido.FindControl("chkCancelar");
                if (chkCancelar.Checked)
                {
                    int produtoId = Convert.ToInt32(hdfProdutoId.Value);
                    produtoIdsCancelar.Add(produtoId);
                }
            }
            if (produtoIdsCancelar.Count > 0)
            {
                pnProdutosCancelar.Visible = false;
                pnEtiquetasCancelar.Visible = true;
                var etiquetas = (from c in data.tbProdutoEstoques
                                 where c.enviado == false && c.pedidoId == null && c.liberado == true && produtoIdsCancelar.Contains(c.produtoId) && c.pedidoIdReserva == pedidoID
                                 select new
                                 {
                                     c.idPedidoFornecedorItem,
                                     c.tbProduto.produtoNome
                                 });
                lstEtiquetasCancelar.DataSource = etiquetas;
                lstEtiquetasCancelar.DataBind();
                return;
            }
        }

        var interacao = new StringBuilder();
        interacao.AppendFormat("<b>Pedido sendo embalado retornado ao status de Separação de Estoque.</b><br>");
        interacao.AppendFormat("Motivo: " + txtMotivo.Text);
        //foreach (var itemPedido in lstItensPedido.Items)
        //{
        //    HiddenField hdfProdutoId = (HiddenField)itemPedido.FindControl("hdfProdutoId");
        //    HiddenField hdfItemPedidoId = (HiddenField)itemPedido.FindControl("hdfItemPedidoId");
        //    HiddenField hdfItemPedidoEstoque = (HiddenField)itemPedido.FindControl("hdfItemPedidoEstoque");
        //    CheckBox chkCancelar = (CheckBox)itemPedido.FindControl("chkCancelar");

        //    if (chkCancelar.Checked)
        //    {
        //        int idItemPedidoEstoque = Convert.ToInt32(hdfItemPedidoEstoque.Value);
        //        rnPedidos.cancelaReservasProduto(idItemPedidoEstoque);
        //        int produtoId = Convert.ToInt32(hdfProdutoId.Value);

        //        var produto = (from c in data.tbProdutos where c.produtoId == produtoId select c).First();
        //        interacao.AppendFormat("<br>Produto Faltando: " + produto.produtoNome);
        //    }
        //}
        string nomeUsuario = rnUsuarios.retornaNomeUsuarioLogado();
        foreach (var itemCancelar in lstEtiquetasCancelar.Items)
        {
            HiddenField hdfIdPedidoFornecedorItem = (HiddenField)itemCancelar.FindControl("hdfIdPedidoFornecedorItem");
            CheckBox chkCancelar = (CheckBox)itemCancelar.FindControl("chkCancelar");

            if (chkCancelar.Checked)
            {
                int IdPedidoFornecedorItem = Convert.ToInt32(hdfIdPedidoFornecedorItem.Value);
                var etiqueta = (from c in data.tbProdutoEstoques where c.idPedidoFornecedorItem == IdPedidoFornecedorItem select c).FirstOrDefault();
                etiqueta.enviado = true;
                etiqueta.dataEnvio = DateTime.Now;
                etiqueta.tbPedidoFornecedorItem.motivo = "Produto retirado do estoque por " + rnUsuarios.retornaNomeUsuarioLogado();

                if (etiqueta.idItemPedidoEstoqueReserva != null)
                {
                    var itemPedido = (from c in data.tbItemPedidoEstoques where c.idItemPedidoEstoque == etiqueta.idItemPedidoEstoqueReserva && c.cancelado == false && c.enviado == false select c).FirstOrDefault();
                    if (itemPedido != null)
                    {
                        itemPedido.reservado = false;
                        itemPedido.dataReserva = null;
                        string interacaoPedido = itemPedido.tbProduto.produtoNome + " retirado do estoque. Produto retornado à lista de aguardando estoque.";
                        rnInteracoes.interacaoInclui(itemPedido.tbItensPedido.pedidoId, interacaoPedido, nomeUsuario, "False");
                        rnLog.InsereLogEtiqueta(nomeUsuario, etiqueta.idPedidoFornecedorItem, itemPedido.tbItensPedido.pedidoId, "Etiqueta removida do estoque no cancelamento de embalagem");
                    }
                    else
                    {
                        rnLog.InsereLogEtiqueta(nomeUsuario, etiqueta.idPedidoFornecedorItem, "Etiqueta removida do estoque no cancelamento de embalagem");
                    }
                    etiqueta.idItemPedidoEstoqueReserva = null;
                    etiqueta.idItemPedidoEstoque = null;
                    etiqueta.itemPedidoIdReserva = null;
                    etiqueta.itemPedidoId = null;
                    etiqueta.pedidoIdReserva = null;
                    etiqueta.pedidoId = null;
                    etiqueta.idPedidoEnvio = null;
                    //TODO:Adicionar status dessa remoção
                    data.SubmitChanges();
                }

                data.SubmitChanges();


                interacao.AppendFormat("<br>Etiqueta removida do estoque: " + etiqueta.idPedidoFornecedorItem + " - " + etiqueta.tbProduto.produtoNome);
                try
                {
                    adicionaInteracaoPedido("<br>Etiqueta removida do estoque: " + etiqueta.idPedidoFornecedorItem + " - " + etiqueta.tbProduto.produtoNome, "False", pedidoID);
                }
                catch (Exception ex)
                {
                }
            }
        }
        foreach (var itemPedido in lstItensPedido.Items)
        {
            HiddenField hdfProdutoId = (HiddenField)itemPedido.FindControl("hdfProdutoId");
            HiddenField hdfItemPedidoId = (HiddenField)itemPedido.FindControl("hdfItemPedidoId");
            HiddenField hdfItemPedidoEstoque = (HiddenField)itemPedido.FindControl("hdfItemPedidoEstoque");
            CheckBox chkCancelar = (CheckBox)itemPedido.FindControl("chkCancelar");

            if (chkCancelar.Checked)
            {
                int produtoId = Convert.ToInt32(hdfProdutoId.Value);

                var queueChecarReserva = new tbQueue();
                queueChecarReserva.agendamento = DateTime.Now;
                queueChecarReserva.andamento = false;
                queueChecarReserva.concluido = false;
                queueChecarReserva.idRelacionado = produtoId;
                queueChecarReserva.mensagem = "";
                queueChecarReserva.tipoQueue = 21;
                data.tbQueues.InsertOnSubmit(queueChecarReserva);
                data.SubmitChanges();
            }
        }

        var pedido = (from c in data.tbPedidos where c.pedidoId == pedidoID select c).First();
        pedido.idUsuarioEmbalado = null;
        pedido.dataSendoEmbalado = null;
        pedido.dataEmbaladoFim = null;
        pedido.volumesEmbalados = null;
        pedido.idUsuarioSeparacao = null;
        pedido.separado = false;
        pedido.dataInicioSeparacao = null;
        pedido.dataFimSeparacao = null;
        pedido.statusDoPedido = 11;
        pedido.envioLiberado = false;
        pedido.envioLiberadoCd4 = false;
        pedido.envioLiberadoCd5 = false;

        if (!string.IsNullOrEmpty(txtPriorizar.Text))
        {
            try
            {
                pedido.prioridadeEnvio = Convert.ToInt32(txtPriorizar.Text);
            }
            catch(Exception)
            {
                
            }
        }
        data.SubmitChanges();

        var envio = (from c in data.tbPedidoEnvios where c.idPedido == pedidoID orderby c.idPedidoEnvio descending select c).FirstOrDefault();
        if (envio != null)
        {
            var pacotes = (from c in data.tbPedidoPacotes where c.idPedidoEnvio == envio.idPedidoEnvio select c);
            foreach (var pacote in pacotes)
            {
                data.tbPedidoPacotes.DeleteOnSubmit(pacote);
            }
            data.SubmitChanges();

            var itensSeparados = (from c in data.tbProdutoEstoques where c.idPedidoEnvio == envio.idPedidoEnvio select c).ToList();
            int idTransferenciaEndereco = 0;
            if (itensSeparados.Count > 0)
            {
                var transferencia = new tbTransferenciaEndereco();
                transferencia.idEnderecamentoTransferencia = 2;
                transferencia.usuarioEntrada = rnUsuarios.retornaNomeUsuarioLogado();
                transferencia.idUsuarioExpedicao = 347;
                transferencia.dataCadastro = DateTime.Now;
                transferencia.dataInicio = DateTime.Now;
                data.tbTransferenciaEnderecos.InsertOnSubmit(transferencia);
                data.SubmitChanges();
                idTransferenciaEndereco = transferencia.idTransferenciaEndereco;
            }
            foreach (var itensSeparado in itensSeparados)
            {
                var itemTransferenciaEndereco = new tbTransferenciaEnderecoProduto();
                itemTransferenciaEndereco.idTransferenciaEndereco = idTransferenciaEndereco;
                itemTransferenciaEndereco.idPedidoFornecedorItem = itensSeparado.idPedidoFornecedorItem;
                data.tbTransferenciaEnderecoProdutos.InsertOnSubmit(itemTransferenciaEndereco);
                itensSeparado.idEnderecamentoAndar = null;
                itensSeparado.idEnderecamentoApartamento = null;
                itensSeparado.idEnderecamentoArea = null;
                itensSeparado.idEnderecamentoPredio = null;
                itensSeparado.idEnderecamentoRua = null;
                itensSeparado.idPedidoEnvio = null;
                itensSeparado.pedidoId = null;
                itensSeparado.idItemPedidoEstoque = null;
                itensSeparado.itemPedidoId = null;
                data.SubmitChanges();
            }
            data.tbPedidoEnvios.DeleteOnSubmit(envio);
            data.SubmitChanges();
        }

        txtMotivo.Text = "";
        adicionaInteracaoPedido(interacao.ToString(), "False", pedidoID);

        var queue = new tbQueue();
        queue.agendamento = DateTime.Now.AddMinutes(.30);
        queue.andamento = false;
        queue.concluido = false;
        queue.idRelacionado = pedidoID;
        queue.mensagem = "";
        queue.tipoQueue = 8;
        data.tbQueues.InsertOnSubmit(queue);
        data.SubmitChanges();

        rnEmails.EnviaEmail("", "barbaragraodegente@gmail.com", "", "", "", interacao.ToString() + "<br />usuário responsável: " + NomeUsuarioLogado(), "Pedido: " + pedidoID);
        Response.Redirect("sendoEmbaladosAtual.aspx");
    }


    public void AlertShow(string message)
    {
        base.ClientScript.RegisterStartupScript(base.GetType(), "myalert", "alert('" + message + "'); ", true);
    }

    public void adicionaInteracaoPedido(string interacao, string alteracaoDeEstatus, int pedidoId)
    {
        HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
        usuarioLogadoId = HttpContext.Current.Request.Cookies["usuarioLogadoId"];
        if (usuarioLogadoId != null)
        {
            rnInteracoes.interacaoInclui(pedidoId, interacao, rnUsuarios.usuarioSeleciona_PorUsuarioId(int.Parse(usuarioLogadoId.Value.ToString())).Tables[0].Rows[0]["usuarioNome"].ToString(), alteracaoDeEstatus);
        }
    }

    public string NomeUsuarioLogado()
    {
        var usuarioLogadoId = HttpContext.Current.Request.Cookies["usuarioLogadoId"];

        return rnUsuarios.usuarioSeleciona_PorUsuarioId(int.Parse(usuarioLogadoId.Value.ToString())).Tables[0].Rows[0]["usuarioNome"].ToString();
    }
    protected void btnPesquisarVolumeDuplicado_Click(object sender, EventArgs e)
    {
        fillGridRastreioDuplicado();
    }

    protected void fillGridRastreioDuplicado()
    {
        int pedidoId = Convert.ToInt32(txtPedidoVolumeDuplicado.Text);

        using (var data = new dbCommerceDataContext())
        {
            var pedidoVolumeDuplicado = (from c in data.tbPedidoPacotes where c.idPedido == pedidoId orderby c.peso select c);

            var result = (from d in pedidoVolumeDuplicado where pedidoVolumeDuplicado.Count(x => x.peso == d.peso) > 1 select d);

            grdVolumeErrado.DataSource = result.ToList();
            grdVolumeErrado.DataBind();

        }
    }

    protected void btnPesquisarQtdVolumesInformadoErrado_OnClick(object sender, EventArgs e)
    {
        try
        {

            int pedidoId = Convert.ToInt32(txtIdPedidoPesquisa.Text);

            if (pedidoId.ToString().Length > 6)
                pedidoId = rnFuncoes.retornaIdInterno(pedidoId);

            txtNovaQtdVolumes.Visible = false;
            btnAlterarQtdVolumes.Visible = false;

            using (var data = new dbCommerceDataContext())
            {
                var qtdVolumes =
                    (from c in data.tbPedidoEnvios where c.idPedido == pedidoId orderby c.dataFimEmbalagem descending select new { c.volumesEmbalados }).FirstOrDefault();

                if (qtdVolumes == null)
                {
                    AlertShow("Nenhum pedido encontrado.");
                    return;
                }

                lblQtdVolumesPedidoPesquisa.Text = qtdVolumes.ToString();

                txtNovaQtdVolumes.Visible = true;
                btnAlterarQtdVolumes.Visible = true;
            }
        }
        catch (Exception)
        {

            throw;
        }
    }

    protected void btnAlterarQtdVolumes_OnClick(object sender, EventArgs e)
    {
        try
        {
            if (string.IsNullOrEmpty(txtNovaQtdVolumes.Text)) return;

            int pedidoId = Convert.ToInt32(txtIdPedidoPesquisa.Text);

            if (pedidoId.ToString().Length > 6)
                pedidoId = rnFuncoes.retornaIdInterno(pedidoId);

            int novaQtdVolumes = Convert.ToInt32(txtNovaQtdVolumes.Text);

            using (var data = new dbCommerceDataContext())
            {
                var pedidoEnvio = (from c in data.tbPedidoEnvios where c.idPedido == pedidoId orderby c.dataFimEmbalagem descending select c).FirstOrDefault();

                var pacotes = (from c in data.tbPedidoPacotes where c.idPedidoEnvio == pedidoEnvio.idPedidoEnvio select c);

                for (int i = 1; i <= novaQtdVolumes; i++)
                {
                    var incluirVolume = new tbPedidoPacote()
                    {
                        idPedido = pedidoId,
                        peso = 0,
                        rastreio = "",
                        concluido = false,
                        formaDeEnvio = "",
                        idPedidoEnvio = pedidoEnvio.idPedidoEnvio
                    };

                    data.tbPedidoPacotes.InsertOnSubmit(incluirVolume);
                    data.SubmitChanges();
                }

                var novaQtdPacotes = (from c in data.tbPedidoPacotes where c.idPedidoEnvio == pedidoEnvio.idPedidoEnvio select c);

                int volumePacote = 0;
                foreach (var pacote in novaQtdPacotes)
                {
                    volumePacote++;
                    pacote.numeroVolume = volumePacote;
                    data.SubmitChanges();
                }

                pedidoEnvio.volumesEmbalados = novaQtdPacotes.Count();
                data.SubmitChanges();

                lblQtdVolumesPedidoPesquisa.Text = "{ volumesEmbalados = " + novaQtdPacotes.Count() + "}";
            }

            txtNovaQtdVolumes.Text = "";
            txtNovaQtdVolumes.Visible = false;
            btnAlterarQtdVolumes.Visible = false;



            AlertShow("Alteração realizada com sucesso.");
        }
        catch (Exception)
        {

            throw;
        }
    }


    protected void rdbCd1_CheckedChanged(object sender, EventArgs e)
    {
        fillGrid(true);
    }

    protected void rdbCd2_CheckedChanged(object sender, EventArgs e)
    {
        fillGrid(true);
    }

    protected void rdbCd3_CheckedChanged(object sender, EventArgs e)
    {
        fillGrid(true);
    }

    protected void rdbCd4_CheckedChanged(object sender, EventArgs e)
    {
        fillGrid(true);
    }

    protected void btnFiltroEmbalagem_Click(object sender, EventArgs e)
    {
        fillGrid(true);
    }
}