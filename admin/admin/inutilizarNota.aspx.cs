﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_inutilizarNota : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            FillEmpresas();
        }
    }

    protected void btnInutilizar_Click(object sender, EventArgs e)
    {
        string cnpjEmitente = DropDownList1.SelectedItem.Value;
        int notaInicial = Convert.ToInt32(txtnotainicial.Text);
        int notaFinal = Convert.ToInt32(txtnotafinal.Text);
        if (notaFinal < notaInicial)
        {
            Response.Write("<script>alert('Sequência inválida');</script>");
        }
        else
        {
            string retorno = rnNotaFiscal.inutilizarNota(Convert.ToInt32(txtnotainicial.Text), Convert.ToInt32(txtnotafinal.Text), cnpjEmitente);
            Response.Write("<script>alert('" + retorno + "');</script>");
        }
    }

    protected void btnCancelarNota_OnClick(object sender, EventArgs e)
    {
        try
        {
            string cnpjEmitente = ddlNotaCnpjCancelar.SelectedValue;
            var db = new dbCommerceDataContext();
            var empresaNota = (from c in db.tbEmpresas where c.cnpj == cnpjEmitente select c).First();
            string chaveDeAcesso = empresaNota.chaveDeAcesso;
            string EmpPk = empresaNota.EmpPK;


            var notas = new List<serviceNfe.InvoiCyRecepcaoDadosItem>();
            var serviceRecepcao = new serviceNfe.InvoiCy();
            var cabecalho = new serviceNfe.InvoiCyRecepcaoCabecalho();

            cabecalho.EmpPK = EmpPk;
            serviceRecepcao.Cabecalho = cabecalho;

            string dataEvent = string.Format("{0}T{1}", DateTime.Now.ToString("yyyy-MM-dd"), DateTime.Now.ToString("HH:mm:ss"));


            var nota = new serviceNfe.InvoiCyRecepcaoDadosItem();
            string documentoRequisicao = "<EnvioEvento>" +
                                        "<ModeloDocumento>NFe</ModeloDocumento>" +
                                        "<Versao>3.10</Versao>" +
                                        "<Evento>" +
                                        "<NtfCnpjEmissor>" + cnpjEmitente + "</NtfCnpjEmissor>" +
                                        "<NtfNumero>" + txtNumeroNotaCancelar.Text + "</NtfNumero>" +
                                        "<NtfSerie>1</NtfSerie>" +
                                        "<tpAmb>1</tpAmb>" +
                                        "<EveInf>" +
                                        "<EveDh>" + dataEvent + "</EveDh>" +
                                        "<EveFusoHorario>-03:00</EveFusoHorario>" +
                                        "<EveTp>110111</EveTp>" +
                                        "<EvenSeq>1</EvenSeq>" +
                                        "<Evedet>" +
                                        "<EveDesc>Cancelamento</EveDesc>" +
                                        "<EvenProt>0</EvenProt>" +
                                        "<EvexJust>Documento cancelado por não ter sido utilizado</EvexJust>" +
                                        "</Evedet>" +
                                        "</EveInf>" +
                                        "</Evento>" +
                                        "</EnvioEvento>";
            nota.Documento = documentoRequisicao;

            string valorParaHash = chaveDeAcesso + documentoRequisicao;
            cabecalho.EmpCK = rnFuncoes.gerarMd5(valorParaHash);


            notas.Add(nota);
 
            serviceRecepcao.Dados = notas.ToArray();

            var nfe = new serviceNfe.recepcao();
            var retornoNota = nfe.Execute(serviceRecepcao);
            rnNotaFiscal.atualizaDataReceitaFederal(Convert.ToInt32(txtNumeroNotaCancelar.Text), 0, empresaNota.idEmpresa);
            Response.Write("<script>alert('" + retornoNota.Mensagem[0].Descricao + "');</script>");
        }
        catch (Exception ex)
        {
            Response.Write("<script>alert('ERRO: " + ex.Message + "');</script>");
        }
       
    }

    protected void FillEmpresas()
    {
        using (var data = new dbCommerceDataContext())
        {
            var empresas = (from c in data.tbEmpresas orderby c.nome select c).ToList();
            ddlNotaCnpjCancelar.DataSource = empresas;
            ddlNotaCnpjCancelar.DataBind();

            DropDownList1.DataSource = empresas;
            DropDownList1.DataBind();
        }
    }
}