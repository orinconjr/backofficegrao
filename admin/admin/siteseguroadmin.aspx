﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" Theme="Glass" CodeFile="siteseguroadmin.aspx.cs" Inherits="admin_siteseguroadmin" %>

<%@ Register Assembly="DevExpress.Web.v11.1" Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v11.1" Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register TagPrefix="dxwgv" Namespace="DevExpress.Web.ASPxGridView" Assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>
<%@ Register TagPrefix="dxe" Namespace="DevExpress.Web.ASPxEditors" Assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>

<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" runat="Server">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script>

        function previewFileUpload(input) {

            var img = $("#ctl00_conteudoPrincipal_tabsSiteSeguro_imgFoto");

            if (input.files && input.files[0]) {

                var reader = new FileReader();
                reader.onload = function (e) {
                    $("#ctl00_conteudoPrincipal_tabsSiteSeguro_imgFoto").attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        function previewFileUploadDestaque(input) {

            var img = $("#ctl00_conteudoPrincipal_tabsSiteSeguro_imgFotoDestaque");

            if (input.files && input.files[0]) {

                var reader = new FileReader();
                reader.onload = function (e) {
                    $("#ctl00_conteudoPrincipal_tabsSiteSeguro_imgFotoDestaque").attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

    </script>

    <asp:ScriptManager runat="server"></asp:ScriptManager>

    <table cellpadding="0" cellspacing="0" style="width: 920px">
        <tr>
            <td class="tituloPaginas" valign="top">
                <asp:Label ID="lblAcao" runat="server">Site Seguro</asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <table align="center" cellpadding="0" cellspacing="0" style="width: 855px">
                    <tr class="rotulos">
                        <td class="headerStatus" style="" runat="server" id="tdHeaderStatus"></td>
                    </tr>
                    <tr>
                        <td>
                            <dx:ASPxPageControl ID="tabsSiteSeguro" runat="server" ActiveTabIndex="0" Width="100%">
                                <TabPages>
                                    <dx:TabPage Text="Depoimentos">
                                        <ContentCollection>
                                            <dx:ContentControl runat="server">

                                                <asp:Panel runat="server" ID="pnlListaDepoimentos">

                                                    <dxwgv:ASPxGridView ID="grdDepoimentos" runat="server" AutoGenerateColumns="False"
                                                        DataSourceID="sqlDSDepoimentos" KeyFieldName="Id" Cursor="auto" OnHtmlRowCreated="grdDepoimentos_OnHtmlRowCreated"
                                                        ClientInstanceName="grd" EnableCallBacks="False" OnHtmlRowPrepared="grdDepoimentos_OnHtmlRowPrepared"
                                                        OnCustomCallback="grdDepoimentos_OnCustomCallback">
                                                        <SettingsBehavior AutoFilterRowInputDelay="10200" ConfirmDelete="True"
                                                            AutoExpandAllGroups="True" ColumnResizeMode="Control" AllowGroup="False"
                                                            AllowFocusedRow="True" />
                                                        <SettingsText ConfirmDelete="Tem certeza que deseja excluir?"
                                                            EmptyDataRow="Nenhum registro encontrado."
                                                            GroupPanel="Arraste uma coluna aqui para agrupar." />
                                                        <SettingsPager Position="TopAndBottom" PageSize="30"
                                                            ShowDisabledButtons="False" AlwaysShowPager="True">
                                                            <Summary AllPagesText="Páginas: {0} - {1} ({2} items)"
                                                                Text="Página {0} de {1} ({2} registros encontrados)" />
                                                        </SettingsPager>
                                                        <Settings ShowFilterRow="True" ShowGroupedColumns="True" ShowFooter="True" />
                                                        <TotalSummary>
                                                            <dxwgv:ASPxSummaryItem DisplayFormat="0" FieldName="margem"
                                                                ShowInColumn="Margem" ShowInGroupFooterColumn="Margem"
                                                                SummaryType="Average" />
                                                        </TotalSummary>
                                                        <SettingsEditing EditFormColumnCount="4"
                                                            PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1"
                                                            PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter"
                                                            PopupEditFormWidth="700px" Mode="Inline" />
                                                        <Columns>
                                                            <dxwgv:GridViewDataTextColumn Caption="ID"
                                                                FieldName="id" Name="id" VisibleIndex="3" Visible="false"
                                                                Width="100px">
                                                                <Settings AutoFilterCondition="Contains" />
                                                                <DataItemTemplate>
                                                                    <asp:TextBox ID="txtId" runat="server" BorderStyle="None"
                                                                        CssClass="campos" Text='<%# Bind("id") %>' Width="100%"></asp:TextBox>

                                                                </DataItemTemplate>
                                                            </dxwgv:GridViewDataTextColumn>
                                                            <dxwgv:GridViewDataTextColumn Caption="Titulo"
                                                                FieldName="titulo" Name="titulo" VisibleIndex="3"
                                                                Width="80">
                                                                <DataItemTemplate>
                                                                    <asp:TextBox ID="txtTitulo" runat="server" BorderStyle="None"
                                                                        CssClass="campos" Text='<%# Bind("titulo") %>' Width="100%"></asp:TextBox>
                                                                    <%--<asp:HiddenField runat="server" ID="hfCategoriaId" Value='<%# Bind("categoriaId") %>' />--%>
                                                                </DataItemTemplate>
                                                            </dxwgv:GridViewDataTextColumn>
                                                            <dxwgv:GridViewDataTextColumn Caption="Conteudo"
                                                                FieldName="conteudo" Name="conteudo" VisibleIndex="3"
                                                                Width="300">
                                                                <DataItemTemplate>

                                                                    <asp:TextBox ID="txtConteudo" runat="server" BorderStyle="None" TextMode="MultiLine" Height="40"
                                                                        CssClass="campos" Text='<%# Bind("conteudo") %>' Width="100%"></asp:TextBox>

                                                                </DataItemTemplate>
                                                            </dxwgv:GridViewDataTextColumn>
                                                            <dxwgv:GridViewDataTextColumn Caption="Posição" Width="80"
                                                                FieldName="posicao" Name="posicao" VisibleIndex="3">
                                                                <DataItemTemplate>

                                                                    <asp:Label ID="lblPosicao" runat="server" Text='<%# Bind("posicao") %>'></asp:Label>

                                                                </DataItemTemplate>
                                                            </dxwgv:GridViewDataTextColumn>
                                                            <dxwgv:GridViewDataTextColumn Caption="Ativo" FieldName="ativo"
                                                                UnboundType="Boolean" VisibleIndex="22" Width="60px">
                                                                <DataItemTemplate>
                                                                    <asp:CheckBox runat="server" ID="ckbAtivo" Checked='<%# Bind("ativo") %>' Enabled="False" />
                                                                </DataItemTemplate>
                                                            </dxwgv:GridViewDataTextColumn>

                                                            <dxwgv:GridViewDataHyperLinkColumn Caption="Novo" VisibleIndex="0"
                                                                Width="35px">
                                                                <PropertiesHyperLinkEdit ImageUrl="~/admin/images/btNovo.jpg"
                                                                    NavigateUrlFormatString="produtoCad.aspx">
                                                                </PropertiesHyperLinkEdit>
                                                                <DataItemTemplate>
                                                                    <asp:ImageButton runat="server" ID="ibtnNovo" ImageUrl="images/btNovo.jpg" ToolTip="Novo" OnCommand="ibtnNovo_OnCommand" />
                                                                </DataItemTemplate>
                                                                <CellStyle HorizontalAlign="Center">
                                                                </CellStyle>
                                                            </dxwgv:GridViewDataHyperLinkColumn>
                                                            <dxwgv:GridViewDataHyperLinkColumn Caption="Alterar" VisibleIndex="1"
                                                                Width="35px">
                                                                <DataItemTemplate>
                                                                    <asp:ImageButton runat="server" ID="ibtnEditar" ImageUrl="images/btEditar.jpg" ToolTip="Alterar" OnCommand="ibtnEditar_OnCommand" CommandArgument='<%# Bind("id") %>' />

                                                                </DataItemTemplate>
                                                                <CellStyle HorizontalAlign="Center">
                                                                </CellStyle>
                                                            </dxwgv:GridViewDataHyperLinkColumn>
                                                            <dxwgv:GridViewDataTextColumn Caption="Excluir" Name="excluir" VisibleIndex="26" Width="42px" Visible="false">
                                                                <DataItemTemplate>
                                                                    <dxe:ASPxCheckBox ID="ckbExcluir" runat="server" ValueChecked="True" ValueType="System.String" ValueUnchecked="False">
                                                                    </dxe:ASPxCheckBox>
                                                                </DataItemTemplate>
                                                            </dxwgv:GridViewDataTextColumn>
                                                            <%--<dxwgv:GridViewCommandColumn VisibleIndex="3" Width="90px" ButtonType="Image">
                                                                <NewButton Visible="True" Text="Novo">
                                                                    <Image Url="~/admin/images/btNovo.jpg" />
                                                                </NewButton>
                                                            </dxwgv:GridViewCommandColumn>--%>
                                                        </Columns>

                                                    </dxwgv:ASPxGridView>

                                                    <asp:SqlDataSource ConnectionString="<%$ ConnectionStrings:connectionString %>" runat="server" ID="sqlDSDepoimentos"
                                                        SelectCommand="SELECT Id, titulo, conteudo, ativo, rede, CASE WHEN posicao = 1 THEN 'Topo'
                                                                                                                      WHEN posicao = 2 THEN 'Sub Topo'
                                                                                                                      WHEN posicao = 3 THEN 'Meio'
                                                                                                                      WHEN posicao = 4 THEN 'Inferior'
                                                                                                                 END as posicao
                                                                      FROM tbSiteSeguro WHERE tipo = 1"></asp:SqlDataSource>

                                                </asp:Panel>

                                                <asp:Panel runat="server" ID="pnlCadastrarDepoimento" Visible="False">

                                                    <div style="text-align: right">
                                                        <asp:Button runat="server" ID="btnVoltarListaDepoimentos" OnClick="btnVoltarListaDepoimentos_OnClick" Text="Voltar a Lista" />
                                                    </div>

                                                    <div style="width: 100%;">
                                                        <div>Título:</div>
                                                        <asp:TextBox runat="server" ID="txtTitulo" MaxLength="500" Width="600" ValidationGroup="cadastroDepoimento"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="rfvTxtTitulo" runat="server" ValidationGroup="cadastroDepoimento" Display="None"
                                                            ControlToValidate="txtTitulo" ErrorMessage="Informe o título!"></asp:RequiredFieldValidator>
                                                    </div>
                                                    <div style="width: 100%;">
                                                        <div>Subtitulo:</div>
                                                        <asp:TextBox runat="server" ID="txtSubtitulo" MaxLength="500" Width="600"></asp:TextBox>
                                                    </div>
                                                    <div style="width: 100%;">
                                                        <div>Conteúdo:</div>
                                                        <asp:TextBox runat="server" ID="txtConteudo" TextMode="MultiLine" Height="100" Width="100%" ValidationGroup="cadastroDepoimento"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="rfvTxtConteudo" runat="server" ValidationGroup="cadastroDepoimento" Display="None"
                                                            ControlToValidate="txtConteudo" ErrorMessage="Informe o conteúdo!"></asp:RequiredFieldValidator>
                                                    </div>
                                                    <br />
                                                    <div style="width: 100%;">
                                                        <div>Rede:</div>
                                                        <asp:DropDownList ID="ddlRede" runat="server" Width="172" Height="22">
                                                            <asp:ListItem Text="Facebook" Value="facebook" />
                                                            <asp:ListItem Text="Trusted" Value="trusted" />
                                                        </asp:DropDownList><br />
                                                    </div>
                                                    <br />
                                                    <div style="width: 100%;">
                                                        <div>Link do depoimento:</div>
                                                        <asp:TextBox runat="server" ID="txtReferencia" MaxLength="900" Width="600"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="rfvTxtReferencia" runat="server" ValidationGroup="cadastroDepoimento" Display="None"
                                                            ControlToValidate="txtReferencia" ErrorMessage="Informe o link do depoimento!"></asp:RequiredFieldValidator>
                                                    </div>
                                                    <br />
                                                    <div style="width: 100%;">
                                                        <asp:Image ID="imgFoto" runat="server" AlternateText="Foto" Style="max-width: 860px;" /><br />
                                                        Foto<br />
                                                        <asp:FileUpload ID="FileUploadFoto" runat="server" onchange="previewFileUpload(this)" Width="97%" />
                                                    </div>
                                                    <br />
                                                    <div style="width: 100%;">
                                                        <div>Posição:</div>
                                                        <asp:DropDownList ID="ddlPosicaoConteudo" runat="server" Width="172" Height="22">
                                                            <asp:ListItem Text="Topo" Value="1" />
                                                            <asp:ListItem Text="Sub Topo" Value="2" />
                                                            <asp:ListItem Text="Meio" Value="3" />
                                                            <asp:ListItem Text="Inferior" Value="4" />
                                                        </asp:DropDownList><br />
                                                    </div>
                                                    <br />
                                                    <div style="width: 100%;">
                                                        <asp:CheckBox runat="server" ID="ckbDepoimentoAtivo" Text="Ativo" />
                                                    </div>
                                                    <br />
                                                    <div style="width: 100%; text-align: right;">
                                                        <asp:Button runat="server" ID="btnSalvarDepoimento" Text="Salvar" Width="100" OnClick="btnSalvarDepoimento_OnClick" ValidationGroup="cadastroDepoimento" />
                                                    </div>
                                                </asp:Panel>

                                            </dx:ContentControl>
                                        </ContentCollection>
                                    </dx:TabPage>

                                    <dx:TabPage Text="Destaques">
                                        <ContentCollection>
                                            <dx:ContentControl runat="server">

                                                <asp:Panel runat="server" ID="pnlListaDestaques">

                                                    <dxwgv:ASPxGridView ID="grdDestaques" runat="server" AutoGenerateColumns="False"
                                                        DataSourceID="sqlDSDestaques" KeyFieldName="Id" Cursor="auto" OnHtmlRowCreated="grdDestaques_OnHtmlRowCreated"
                                                        ClientInstanceName="grd" EnableCallBacks="False" OnHtmlRowPrepared="grdDestaques_OnHtmlRowPrepared"
                                                        OnCustomCallback="grdDepoimentos_OnCustomCallback">
                                                        <SettingsBehavior AutoFilterRowInputDelay="10200" ConfirmDelete="True"
                                                            AutoExpandAllGroups="True" ColumnResizeMode="Control" AllowGroup="False"
                                                            AllowFocusedRow="True" />
                                                        <SettingsText ConfirmDelete="Tem certeza que deseja excluir?"
                                                            EmptyDataRow="Nenhum registro encontrado."
                                                            GroupPanel="Arraste uma coluna aqui para agrupar." />
                                                        <SettingsPager Position="TopAndBottom" PageSize="30"
                                                            ShowDisabledButtons="False" AlwaysShowPager="True">
                                                            <Summary AllPagesText="Páginas: {0} - {1} ({2} items)"
                                                                Text="Página {0} de {1} ({2} registros encontrados)" />
                                                        </SettingsPager>
                                                        <Settings ShowFilterRow="True" ShowGroupedColumns="True" ShowFooter="True" />
                                                        <TotalSummary>
                                                            <dxwgv:ASPxSummaryItem DisplayFormat="0" FieldName="margem"
                                                                ShowInColumn="Margem" ShowInGroupFooterColumn="Margem"
                                                                SummaryType="Average" />
                                                        </TotalSummary>
                                                        <SettingsEditing EditFormColumnCount="4"
                                                            PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1"
                                                            PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter"
                                                            PopupEditFormWidth="700px" Mode="Inline" />
                                                        <Columns>
                                                            <dxwgv:GridViewDataTextColumn Caption="ID"
                                                                FieldName="id" Name="id" VisibleIndex="3" Visible="false"
                                                                Width="100px">
                                                                <Settings AutoFilterCondition="Contains" />
                                                                <DataItemTemplate>
                                                                    <asp:TextBox ID="txtId" runat="server" BorderStyle="None"
                                                                        CssClass="campos" Text='<%# Bind("id") %>' Width="100%"></asp:TextBox>

                                                                </DataItemTemplate>
                                                            </dxwgv:GridViewDataTextColumn>
                                                            <dxwgv:GridViewDataTextColumn Caption="Titulo"
                                                                FieldName="titulo" Name="titulo" VisibleIndex="3"
                                                                Width="80">
                                                                <DataItemTemplate>
                                                                    <asp:TextBox ID="txtTitulo" runat="server" BorderStyle="None"
                                                                        CssClass="campos" Text='<%# Bind("titulo") %>' Width="100%"></asp:TextBox>
                                                                    <%--<asp:HiddenField runat="server" ID="hfCategoriaId" Value='<%# Bind("categoriaId") %>' />--%>
                                                                </DataItemTemplate>
                                                            </dxwgv:GridViewDataTextColumn>
                                                            <dxwgv:GridViewDataTextColumn Caption="Conteudo"
                                                                FieldName="conteudo" Name="conteudo" VisibleIndex="3"
                                                                Width="300">
                                                                <DataItemTemplate>

                                                                    <asp:TextBox ID="txtConteudo" runat="server" BorderStyle="None" TextMode="MultiLine" Height="40"
                                                                        CssClass="campos" Text='<%# Bind("conteudo") %>' Width="100%"></asp:TextBox>

                                                                </DataItemTemplate>
                                                            </dxwgv:GridViewDataTextColumn>

                                                            <dxwgv:GridViewDataTextColumn Caption="Posição" Width="80"
                                                                FieldName="posicao" Name="posicao" VisibleIndex="3">
                                                                <DataItemTemplate>

                                                                    <asp:Label ID="lblPosicaoDestaque" runat="server" Text='<%# Bind("posicao") %>'></asp:Label>

                                                                </DataItemTemplate>
                                                            </dxwgv:GridViewDataTextColumn>

                                                            <dxwgv:GridViewDataTextColumn Caption="Ativo" FieldName="ativo"
                                                                UnboundType="Boolean" VisibleIndex="22" Width="60px">
                                                                <DataItemTemplate>
                                                                    <asp:CheckBox runat="server" ID="ckbAtivo" Checked='<%# Bind("ativo") %>' Enabled="False" />
                                                                </DataItemTemplate>
                                                            </dxwgv:GridViewDataTextColumn>

                                                            <dxwgv:GridViewDataHyperLinkColumn Caption="Novo" VisibleIndex="0"
                                                                Width="35px">
                                                                <PropertiesHyperLinkEdit ImageUrl="~/admin/images/btNovo.jpg"
                                                                    NavigateUrlFormatString="produtoCad.aspx">
                                                                </PropertiesHyperLinkEdit>
                                                                <DataItemTemplate>
                                                                    <asp:ImageButton runat="server" ID="ibtnNovoDestaque" ImageUrl="images/btNovo.jpg" ToolTip="Novo" OnCommand="ibtnNovoDestaque_OnCommand" />
                                                                </DataItemTemplate>
                                                                <CellStyle HorizontalAlign="Center">
                                                                </CellStyle>
                                                            </dxwgv:GridViewDataHyperLinkColumn>
                                                            <dxwgv:GridViewDataHyperLinkColumn Caption="Alterar" VisibleIndex="1"
                                                                Width="35px">
                                                                <DataItemTemplate>
                                                                    <asp:ImageButton runat="server" ID="ibtnEditarDestaque" ImageUrl="images/btEditar.jpg" ToolTip="Alterar" OnCommand="ibtnEditarDestaque_OnCommand" CommandArgument='<%# Bind("id") %>' />

                                                                </DataItemTemplate>
                                                                <CellStyle HorizontalAlign="Center">
                                                                </CellStyle>
                                                            </dxwgv:GridViewDataHyperLinkColumn>
                                                            <dxwgv:GridViewDataTextColumn Caption="Excluir" Name="excluir" VisibleIndex="26" Width="42px" Visible="false">
                                                                <DataItemTemplate>
                                                                    <dxe:ASPxCheckBox ID="ckbExcluir" runat="server" ValueChecked="True" ValueType="System.String" ValueUnchecked="False">
                                                                    </dxe:ASPxCheckBox>
                                                                </DataItemTemplate>
                                                            </dxwgv:GridViewDataTextColumn>
                                                            <%--<dxwgv:GridViewCommandColumn VisibleIndex="3" Width="90px" ButtonType="Image">
                                                                <NewButton Visible="True" Text="Novo">
                                                                    <Image Url="~/admin/images/btNovo.jpg" />
                                                                </NewButton>
                                                            </dxwgv:GridViewCommandColumn>--%>
                                                        </Columns>
                                                    </dxwgv:ASPxGridView>

                                                    <asp:SqlDataSource ConnectionString="<%$ ConnectionStrings:connectionString %>" runat="server" ID="sqlDSDestaques"
                                                        SelectCommand="SELECT Id, titulo, conteudo, ativo, rede, CASE WHEN posicao = 1 THEN 'Topo' ELSE 'Inferior' END as posicao FROM tbSiteSeguro WHERE tipo = 2"></asp:SqlDataSource>

                                                </asp:Panel>

                                                <asp:Panel runat="server" ID="pnlCadastroDestaque" Visible="False">


                                                    <div style="text-align: right">
                                                        <asp:Button runat="server" ID="btnVoltarListaDestaques" OnClick="btnVoltarListaDestaques_OnClick" Text="Voltar a Lista" />
                                                    </div>

                                                    <div style="width: 100%;">
                                                        <div>Título:</div>
                                                        <asp:TextBox runat="server" ID="txtTituloDestaque" MaxLength="500" Width="600" ValidationGroup="cadastroDestaque"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="rdvTxtTituloDestaque" runat="server" ValidationGroup="cadastroDestaque" Display="None"
                                                            ControlToValidate="txtTituloDestaque" ErrorMessage="Informe o título!"></asp:RequiredFieldValidator>
                                                    </div>
                                                    <div style="width: 100%;">
                                                        <div>Subtitulo:</div>
                                                        <asp:TextBox runat="server" ID="txtSubTituloDestaque" MaxLength="500" Width="600"></asp:TextBox>
                                                    </div>
                                                    <div style="width: 100%;">
                                                        <div>Conteúdo:</div>
                                                        <asp:TextBox runat="server" ID="txtConteudoDestaque" TextMode="MultiLine" Height="100" Width="100%" ValidationGroup="cadastroDestaque"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="rdvTxtConteudoDestaque" runat="server" ValidationGroup="cadastroDestaque" Display="None"
                                                            ControlToValidate="txtConteudoDestaque" ErrorMessage="Informe o conteúdo!"></asp:RequiredFieldValidator>
                                                    </div>

                                                    <br />
                                                    <div style="width: 100%;">
                                                        <div>Link do Destaque:</div>
                                                        <asp:TextBox runat="server" ID="txtReferenciaDestaque" MaxLength="900" Width="600"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="rdvTxtReferenciaDestaque" runat="server" ValidationGroup="cadastroDestaque" Display="None"
                                                            ControlToValidate="txtReferenciaDestaque" ErrorMessage="Informe o link do depoimento!"></asp:RequiredFieldValidator>
                                                    </div>
                                                    <br />
                                                    <div style="width: 100%;">
                                                        <asp:Image ID="imgFotoDestaque" runat="server" AlternateText="Foto" Style="max-width: 860px;" /><br />
                                                        Foto<br />
                                                        <asp:FileUpload ID="FileUploadDestaque" runat="server" onchange="previewFileUploadDestaque(this)" Width="97%" />
                                                    </div>
                                                    <br />
                                                    <div style="width: 100%;">
                                                        <div>Posição:</div>
                                                        <asp:DropDownList ID="ddlPosicaoDestaque" runat="server" Width="172" Height="22">
                                                            <asp:ListItem Text="Topo" Value="1" />
                                                            <asp:ListItem Text="Inferior" Value="4" />
                                                        </asp:DropDownList><br />
                                                    </div>
                                                    <br />
                                                    <div style="width: 100%;">
                                                        <asp:CheckBox runat="server" ID="ckbDestaqueAtivo" Text="Ativo" />
                                                    </div>
                                                    <div style="width: 100%; text-align: right;">
                                                        <asp:Button runat="server" ID="btnSalvarDestaque" Text="Salvar" Width="100" OnClick="btnSalvarDestaque_OnClick" ValidationGroup="cadastroDestaque" />
                                                    </div>

                                                </asp:Panel>
                                            </dx:ContentControl>
                                        </ContentCollection>
                                    </dx:TabPage>


                                    <dx:TabPage Text="Videos">
                                        <ContentCollection>
                                            <dx:ContentControl runat="server">

                                                <asp:Panel runat="server" ID="pnlListaVideos">

                                                    <dxwgv:ASPxGridView ID="grdVideos" runat="server" AutoGenerateColumns="False"
                                                        DataSourceID="sqlDSVideos" KeyFieldName="Id" Cursor="auto" OnHtmlRowCreated="grdDestaques_OnHtmlRowCreated"
                                                        ClientInstanceName="grd" EnableCallBacks="False" OnHtmlRowPrepared="grdDestaques_OnHtmlRowPrepared"
                                                        OnCustomCallback="grdDepoimentos_OnCustomCallback">
                                                        <SettingsBehavior AutoFilterRowInputDelay="10200" ConfirmDelete="True"
                                                            AutoExpandAllGroups="True" ColumnResizeMode="Control" AllowGroup="False"
                                                            AllowFocusedRow="True" />
                                                        <SettingsText ConfirmDelete="Tem certeza que deseja excluir?"
                                                            EmptyDataRow="Nenhum registro encontrado."
                                                            GroupPanel="Arraste uma coluna aqui para agrupar." />
                                                        <SettingsPager Position="TopAndBottom" PageSize="30"
                                                            ShowDisabledButtons="False" AlwaysShowPager="True">
                                                            <Summary AllPagesText="Páginas: {0} - {1} ({2} items)"
                                                                Text="Página {0} de {1} ({2} registros encontrados)" />
                                                        </SettingsPager>
                                                        <Settings ShowFilterRow="True" ShowGroupedColumns="True" ShowFooter="True" />
                                                        <TotalSummary>
                                                            <dxwgv:ASPxSummaryItem DisplayFormat="0" FieldName="margem"
                                                                ShowInColumn="Margem" ShowInGroupFooterColumn="Margem"
                                                                SummaryType="Average" />
                                                        </TotalSummary>
                                                        <SettingsEditing EditFormColumnCount="4"
                                                            PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1"
                                                            PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter"
                                                            PopupEditFormWidth="700px" Mode="Inline" />
                                                        <Columns>
                                                            <dxwgv:GridViewDataTextColumn Caption="ID"
                                                                FieldName="id" Name="id" VisibleIndex="3" Visible="false"
                                                                Width="100px">
                                                                <Settings AutoFilterCondition="Contains" />
                                                                <DataItemTemplate>
                                                                    <asp:TextBox ID="txtId" runat="server" BorderStyle="None"
                                                                        CssClass="campos" Text='<%# Bind("id") %>' Width="100%"></asp:TextBox>

                                                                </DataItemTemplate>
                                                            </dxwgv:GridViewDataTextColumn>
                                                            <dxwgv:GridViewDataTextColumn Caption="Titulo"
                                                                FieldName="titulo" Name="titulo" VisibleIndex="3"
                                                                Width="80">
                                                                <DataItemTemplate>
                                                                    <asp:TextBox ID="txtTitulo" runat="server" BorderStyle="None"
                                                                        CssClass="campos" Text='<%# Bind("titulo") %>' Width="100%"></asp:TextBox>
                                                                    <%--<asp:HiddenField runat="server" ID="hfCategoriaId" Value='<%# Bind("categoriaId") %>' />--%>
                                                                </DataItemTemplate>
                                                            </dxwgv:GridViewDataTextColumn>

                                                            <dxwgv:GridViewDataTextColumn Caption="Subtitulo"
                                                                FieldName="subtitulo" Name="subtitulo" VisibleIndex="3"
                                                                Width="80">
                                                                <DataItemTemplate>
                                                                    <asp:TextBox ID="txtSubtitulo" runat="server" BorderStyle="None"
                                                                        CssClass="campos" Text='<%# Bind("subtitulo") %>' Width="100%"></asp:TextBox>
                                                                    <%--<asp:HiddenField runat="server" ID="hfCategoriaId" Value='<%# Bind("categoriaId") %>' />--%>
                                                                </DataItemTemplate>
                                                            </dxwgv:GridViewDataTextColumn>

                                                            <dxwgv:GridViewDataTextColumn Caption="Conteudo" Visible="False"
                                                                FieldName="conteudo" Name="conteudo" VisibleIndex="3"
                                                                Width="300">
                                                                <DataItemTemplate>

                                                                    <asp:TextBox ID="txtConteudo" runat="server" BorderStyle="None" TextMode="MultiLine" Height="40"
                                                                        CssClass="campos" Text='<%# Bind("conteudo") %>' Width="100%"></asp:TextBox>

                                                                </DataItemTemplate>
                                                            </dxwgv:GridViewDataTextColumn>

                                                            <dxwgv:GridViewDataTextColumn Caption="Posição" Width="80"
                                                                FieldName="posicao" Name="posicao" VisibleIndex="3">
                                                                <DataItemTemplate>

                                                                    <asp:Label ID="lblPosicaoVideo" runat="server" Text='<%# Bind("posicao") %>'></asp:Label>

                                                                </DataItemTemplate>
                                                            </dxwgv:GridViewDataTextColumn>

                                                            <dxwgv:GridViewDataTextColumn Caption="Ativo" FieldName="ativo"
                                                                UnboundType="Boolean" VisibleIndex="22" Width="60px">
                                                                <DataItemTemplate>
                                                                    <asp:CheckBox runat="server" ID="ckbAtivo" Checked='<%# Bind("ativo") %>' Enabled="False" />
                                                                </DataItemTemplate>
                                                            </dxwgv:GridViewDataTextColumn>

                                                            <dxwgv:GridViewDataHyperLinkColumn Caption="Novo" VisibleIndex="0"
                                                                Width="35px">
                                                                <PropertiesHyperLinkEdit ImageUrl="~/admin/images/btNovo.jpg"
                                                                    NavigateUrlFormatString="produtoCad.aspx">
                                                                </PropertiesHyperLinkEdit>
                                                                <DataItemTemplate>
                                                                    <asp:ImageButton runat="server" ID="ibtnNovoVideo" ImageUrl="images/btNovo.jpg" ToolTip="Novo" OnCommand="ibtnNovoVideo_OnCommand" />
                                                                </DataItemTemplate>
                                                                <CellStyle HorizontalAlign="Center">
                                                                </CellStyle>
                                                            </dxwgv:GridViewDataHyperLinkColumn>
                                                            <dxwgv:GridViewDataHyperLinkColumn Caption="Alterar" VisibleIndex="1"
                                                                Width="35px">
                                                                <DataItemTemplate>
                                                                    <asp:ImageButton runat="server" ID="ibtnEditarVideo" ImageUrl="images/btEditar.jpg" ToolTip="Alterar" OnCommand="ibtnEditarVideo_OnCommand" CommandArgument='<%# Bind("id") %>' />

                                                                </DataItemTemplate>
                                                                <CellStyle HorizontalAlign="Center">
                                                                </CellStyle>
                                                            </dxwgv:GridViewDataHyperLinkColumn>
                                                            <dxwgv:GridViewDataTextColumn Caption="Excluir" Name="excluir" VisibleIndex="26" Width="42px" Visible="false">
                                                                <DataItemTemplate>
                                                                    <dxe:ASPxCheckBox ID="ckbExcluir" runat="server" ValueChecked="True" ValueType="System.String" ValueUnchecked="False">
                                                                    </dxe:ASPxCheckBox>
                                                                </DataItemTemplate>
                                                            </dxwgv:GridViewDataTextColumn>
                                                            <%--<dxwgv:GridViewCommandColumn VisibleIndex="3" Width="90px" ButtonType="Image">
                                                                <NewButton Visible="True" Text="Novo">
                                                                    <Image Url="~/admin/images/btNovo.jpg" />
                                                                </NewButton>
                                                            </dxwgv:GridViewCommandColumn>--%>
                                                        </Columns>
                                                    </dxwgv:ASPxGridView>

                                                    <asp:SqlDataSource ConnectionString="<%$ ConnectionStrings:connectionString %>" runat="server" ID="sqlDSVideos"
                                                        SelectCommand="SELECT Id, titulo, subtitulo, conteudo, ativo, rede, CASE WHEN posicao = 1 THEN 'Topo'
                                                                                                                                 WHEN posicao = 4 THEN 'Inferior'
                                                                                                                                 WHEN posicao = 3 THEN 'Infra Estrutura' END as posicao 
                                                                      FROM tbSiteSeguro WHERE tipo = 3"></asp:SqlDataSource>

                                                </asp:Panel>

                                                <asp:Panel runat="server" ID="pnlCadastroVideo" Visible="False">


                                                    <div style="text-align: right">
                                                        <asp:Button runat="server" ID="btnVoltarListaVideos" OnClick="btnVoltarListaVideos_OnClick" Text="Voltar a Lista" />
                                                    </div>

                                                    <div style="width: 100%;">
                                                        <div>Título:</div>
                                                        <asp:TextBox runat="server" ID="txtTituloVideo" MaxLength="500" Width="600" ValidationGroup="cadastroVideo"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="rqvTxtTituloVideo" runat="server" ValidationGroup="cadastroVideo" Display="None"
                                                            ControlToValidate="txtTituloVideo" ErrorMessage="Informe o título!"></asp:RequiredFieldValidator>
                                                    </div>
                                                    <div style="width: 100%;">
                                                        <div>Subtitulo:</div>
                                                        <asp:TextBox runat="server" ID="txtSubtituloVideo" MaxLength="500" Width="600"></asp:TextBox>
                                                    </div>
                                                    <div style="width: 100%;">
                                                        <div>Link:(Usar formato, ex.: https://www.youtube.com/watch?v=OBHZlHCKusU)</div>
                                                        <asp:TextBox runat="server" ID="txtLinkVideo" MaxLength="500" Width="600" ValidationGroup="cadastroVideo"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="rqvTxtLinkVideo" runat="server" ValidationGroup="cadastroVideo" Display="None"
                                                            ControlToValidate="txtLinkVideo" ErrorMessage="Informe o link do video (youtube)!"></asp:RequiredFieldValidator>
                                                    </div>
                                                    <br />
                                                    <div style="width: 100%;">
                                                        <div>Posição:</div>
                                                        <asp:DropDownList ID="ddlPosicaoVideo" runat="server" Width="172" Height="22">
                                                            <asp:ListItem Text="Topo" Value="1" />
                                                            <asp:ListItem Text="Inferior" Value="4" />
                                                            <asp:ListItem Text="Infra Estrutura" Value="3" />
                                                        </asp:DropDownList><br />
                                                    </div>
                                                    <br />
                                                    <div style="width: 100%;">
                                                        <asp:CheckBox runat="server" ID="ckbVideoAtivo" Text="Ativo" />
                                                    </div>
                                                    <div style="width: 100%; text-align: right;">
                                                        <asp:Button runat="server" ID="btnSalvarVideo" Text="Salvar" Width="100" OnClick="btnSalvarVideo_OnClick" ValidationGroup="cadastroVideo" />
                                                    </div>

                                                </asp:Panel>

                                            </dx:ContentControl>
                                        </ContentCollection>
                                    </dx:TabPage>

                                    <dx:TabPage Text="Interações / Agendamentos" Visible="False">
                                        <ContentCollection>
                                            <dx:ContentControl runat="server">
                                            </dx:ContentControl>
                                        </ContentCollection>
                                    </dx:TabPage>

                                    <dx:TabPage Text="Nota Fiscal / Envios" Visible="False">
                                        <ContentCollection>
                                            <dx:ContentControl runat="server">
                                            </dx:ContentControl>
                                        </ContentCollection>
                                    </dx:TabPage>
                                </TabPages>
                            </dx:ASPxPageControl>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td></td>
        </tr>
    </table>

    <asp:ValidationSummary runat="server" ShowMessageBox="true" ValidationGroup="cadastroDepoimento" ShowSummary="false" />

    <asp:ValidationSummary runat="server" ShowMessageBox="true" ValidationGroup="cadastroDestaque" ShowSummary="false" />

    <asp:ValidationSummary runat="server" ShowMessageBox="true" ValidationGroup="cadastroVideo" ShowSummary="false" />

    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <asp:HiddenField runat="server" ID="hfDepoimentoId" />
            <asp:HiddenField runat="server" ID="hfDestaqueId" />
            <asp:HiddenField runat="server" ID="hfVideoId" />
        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>

