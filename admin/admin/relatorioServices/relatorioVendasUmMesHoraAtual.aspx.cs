﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Newtonsoft.Json;

public partial class admin_relatorioServices_relatorioVendasUmMesHoraAtual : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        string retorno = "Você não possui acesso à esta área";

        HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
        usuarioLogadoId = Request.Cookies["usuarioLogadoId"];
        if (usuarioLogadoId != null)
        {
            string paginaSolicitada = "vendasDoDia.aspx";
            if (!(rnUsuarios.usuarioPaginasPermitidaSeleciona_PorUsuarioIdPaginaPermitidaNome(int.Parse(usuarioLogadoId.Value.ToString()), paginaSolicitada).Tables[0].Rows.Count == 0))
            {
                Database db = DatabaseFactory.CreateDatabase();
                DbCommand dbCommand = db.GetStoredProcCommand("relatorioVendasUmMesHoraAtual");
                DataSet ds = null;
                ds = db.ExecuteDataSet(dbCommand);
                retorno = JsonConvert.SerializeObject(ds);
            }
        }
        Response.Clear();
        Response.ContentType = "application/json; charset=utf-8";
        Response.Write(retorno);
        Response.End();
    }
}