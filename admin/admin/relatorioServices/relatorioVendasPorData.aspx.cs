﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Newtonsoft.Json;

public partial class admin_relatorioServices_relatorioVendasPorData : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        bool integracao = false;
        if (Request.QueryString["key"] != null)
        {
            if (Request.QueryString["key"] == "565cc124-e0f3-4e1f-9319-3c5f0903096d")
            {
                integracao = true;
            }
        }
        string retorno = "Você não possui acesso à esta área";
        HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
        usuarioLogadoId = Request.Cookies["usuarioLogadoId"];
        if (usuarioLogadoId != null | integracao)
        {
            string usuario = "0";
            if (!integracao) usuario = usuarioLogadoId.Value;

            string paginaSolicitada = "vendasDoDia.aspx";
            if (rnUsuarios.usuarioPaginasPermitidaSeleciona_PorUsuarioIdPaginaPermitidaNome(int.Parse(usuario), paginaSolicitada).Tables[0].Rows.Count != 0 | integracao)
            {
                Database db = DatabaseFactory.CreateDatabase();
                DbCommand dbCommand = db.GetStoredProcCommand("relatorioVendasPorData");
                db.AddInParameter(dbCommand, "data", DbType.String, Request.QueryString["data"]);
                DataSet ds = null;
                ds = db.ExecuteDataSet(dbCommand);
                retorno = JsonConvert.SerializeObject(ds);

                if (Request.QueryString["total"] != null | Request.QueryString["tot"] != null)
                {
                    RetornoWs retornoJson = JsonConvert.DeserializeObject<RetornoWs>(retorno);
                    decimal valorTotal = retornoJson.Table.Sum(x => x.valor);
                    retorno = valorTotal.ToString("0");
                }

                if (Request.QueryString["compensado"] != null | Request.QueryString["com"] != null)
                {
                    var data = new dbCommerceDataContext();
                    var dataFormatada = Convert.ToDateTime(Request.QueryString["data"]);
                    decimal totalPedidosPagos = 0;
                    var pedidosPagos = (from c in data.tbPedidos
                        where c.dataHoraDoPedido.Value.Date == dataFormatada.Date && (c.statusDoPedido == 3 |
                                                                                      c.statusDoPedido == 4 |
                                                                                      c.statusDoPedido == 5 |
                                                                                      c.statusDoPedido == 9 |
                                                                                      c.statusDoPedido == 10 |
                                                                                      c.statusDoPedido == 11)
                        select c).ToList();
                    if(pedidosPagos.Count() > 0)
                    {
                        totalPedidosPagos = pedidosPagos.Sum(x => (x.valorCobrado ?? 0));
                    }
                    retorno = totalPedidosPagos.ToString("0");
                }
            }
        }
        Response.Clear();
        Response.ContentType = "application/json; charset=utf-8";
        Response.Write(retorno);
        Response.End();
    }

    private class RetornoWs
    {
        public List<RetornoRelatorio> Table { get; set; } 
    }
    public class RetornoRelatorio
    {
        public int hora { get; set; }
        public decimal valor { get; set; }
    }

}