﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using Npgsql;

public partial class admin_relatorioServices_analyticsVisitas : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string retorno = "Você não possui acesso à esta área";

        HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
        usuarioLogadoId = Request.Cookies["usuarioLogadoId"];
        if (usuarioLogadoId != null)
        {
            string paginaSolicitada = "vendasDoDia.aspx";
            if (!(rnUsuarios.usuarioPaginasPermitidaSeleciona_PorUsuarioIdPaginaPermitidaNome(int.Parse(usuarioLogadoId.Value.ToString()), paginaSolicitada).Tables[0].Rows.Count == 0))
            {
                var dia = DateTime.Now.Day;
                var mes  = DateTime.Now.Month - 1;
                var ano  = DateTime.Now.Year;
                var hora  = DateTime.Now.Hour;
                var minuto  = DateTime.Now.Minute;

                if (Request.QueryString["dia"] != null)
                {
                    dia = Convert.ToInt32(Request.QueryString["dia"]);
                }
                if (Request.QueryString["mes"] != null)
                {
                    mes = Convert.ToInt32(Request.QueryString["mes"]) - 1;
                }
                if (Request.QueryString["ano"] != null)
                {
                    ano = Convert.ToInt32(Request.QueryString["ano"]);
                }
                if (Request.QueryString["hora"] != null)
                {
                    hora = Convert.ToInt32(Request.QueryString["hora"]);
                }
                if (Request.QueryString["minuto"] != null)
                {
                    minuto = Convert.ToInt32(Request.QueryString["minuto"]);
                }

                string comandoSql = "";

                string nomesessoesHoje = "sessoesHoje" + ano + mes + dia;
                string nomesessoesHojeIds = "sessoesHojeIds" + ano + mes + dia;
                string nomesessoesHojeMidias = "sessoesHojeMidias" + ano + mes + dia;
                string nomevendasHojeMidias = "vendasHojeMidias" + ano + mes + dia;
                string pedidosOrigemSessao = "pedidosOrigemSessao" + ano + mes + dia;
                if (Request.QueryString["listamidias"] != null)
                {
                    comandoSql = @"select distinct origem from visit order by origem";
                }
                if (Request.QueryString["midiasdia"] != null)
                {
                    //    comandoSql =
                    //        @"DROP TABLE IF EXISTS " + nomesessoesHoje + @";
                    //DROP TABLE IF EXISTS " + nomesessoesHojeIds + @";
                    //DROP TABLE IF EXISTS " + nomesessoesHojeMidias + @";
                    //DROP TABLE IF EXISTS " + nomevendasHojeMidias + @";
                    //CREATE TEMPORARY TABLE " + nomesessoesHoje + @" AS (select count(distinct sessao) sessoes, hora from visit where ano = " +
                    //        ano + @" and mes = " + mes + @" and dia = " + dia + @" and hora <= " + hora + @" and minuto <= " + minuto + @" group by hora);
                    //CREATE TEMPORARY TABLE " + nomesessoesHojeIds + @" AS (select distinct sessao from visit where ano = " + ano +
                    //        @" and mes = " + mes + @" and dia = " + dia + @" and hora <= " + hora + @" and minuto <= " + minuto + @");
                    //CREATE TEMPORARY TABLE " + nomesessoesHojeMidias + @" AS (select *, (select top 1 origem from visit where sessao = " + nomesessoesHojeIds + @".sessao order by visit.datacompleta) origem, 
                    //(select top 1 hora from visit where sessao = " + nomesessoesHojeIds + @".sessao) hora from " + nomesessoesHojeIds + @");
                    //CREATE TEMPORARY TABLE " + nomevendasHojeMidias + @" AS 
                    //(select sum(pedidos) pedidos, (case when origem is null then 'direct' else origem end) as origem, hora from (select count(distinct pedidoId) pedidos, (case when origemSessao is null then ultimaOrigem else origemSessao end) origem, hora from (select pedidoId, 
                    //(select top 1 origem from visit where visit.user = pedido.user order by visit.datacompleta desc) ultimaOrigem,
                    //(select top 1 origem from visit where visit.sessao = pedido.session order by visit.datacompleta) origemSessao,
                    //pedido.hora from pedido
                    //where  ano = " + ano + @" and mes = " + mes + @" and dia = " + dia + @" and hora <= " + hora + @" and minuto <= " + minuto + @")
                    //group by hora, origem) group by hora, (case when origem is null then 'direct' else origem end));

                    //select sessoes, origem, (case when pedido is null then 0 else pedido end) from (select count(sessao) sessoes, " + nomesessoesHojeMidias + @".origem,
                    //(select sum(" + nomevendasHojeMidias + @".pedidos) from " + nomevendasHojeMidias + @" where " + nomesessoesHojeMidias + @".origem = " + nomevendasHojeMidias + @".origem) pedido
                    //from " + nomesessoesHojeMidias + @" group by " + nomesessoesHojeMidias + @".origem)";

                    comandoSql = @"DROP TABLE IF EXISTS " + pedidosOrigemSessao + @";
                    CREATE TEMPORARY TABLE " + pedidosOrigemSessao + @" AS
                    (select pedidoid, sessao,
                    (select top 1 origem from visit where visit.sessao = pedidos.sessao order by datacompleta) as origemsessao,
                    (select top 1 origem from visit where visit.user = pedidos.user and datacompleta < pedidos.datacompleta
                     and visit.origem is not null and visit.origem <> 'direct'
                    order by datacompleta desc) as ultimasessao
                    from(select distinct pedidoId, pedido.user, max(pedido.datacompleta) over(partition by pedidoId) as datacompleta, max(pedido.session) over(partition by pedidoId) as sessao
                    from pedido
                    where ano = " + ano + @" and mes = " + mes + @" and dia = " + dia + @" and hora <= " + hora + @" and minuto <= (case when hora < " + hora + @" then 60 else " + minuto + @" end)) pedidos);


                                        select origens.origem,
                    (select count(sessao) from visit where visit.origem = origens.origem and ano = " + ano + @" and mes = " + mes + @" and dia = " + dia + @" and hora <= " + hora + @" and minuto <= (case when hora < " + hora + @" then 60 else " + minuto + @" end)) as sessoes,
                    (select count(pedidoId) from " + pedidosOrigemSessao + @" where origens.origem = (case when " + pedidosOrigemSessao + @".origemsessao is null then 'direct' else " + pedidosOrigemSessao + @".origemsessao end)) as pedidosOrigemSessao,
                    (select count(pedidoId) from " + pedidosOrigemSessao + @" where origens.origem = (case when " + pedidosOrigemSessao + @".ultimasessao is null then 'direct' else " + pedidosOrigemSessao + @".ultimasessao end)) as pedidosUltimaSessao
                    from(select DISTINCT origem from visit where ano = " + ano + @" and mes = " + mes + @" and dia = " + dia + @" and hora <= " + hora + @" and minuto <= (case when hora < " + hora + @" then 60 else " + minuto + @" end)) origens";

                }
                if (Request.QueryString["midiasporhora"] != null)
                {
                    comandoSql =
                        @"DROP TABLE IF EXISTS pedidosOrigemSessaoHora201643; 
                        CREATE TEMPORARY TABLE pedidosOrigemSessaoHora201643 AS 
                        (select pedidoid, sessao, hora, 
                        (select top 1 origem from visit where visit.sessao = pedidos.sessao order by datacompleta) as origemsessao, 
                        (select top 1 origem from visit where visit.user = pedidos.user and datacompleta < pedidos.datacompleta and visit.origem is not null and visit.origem <> 'direct' order by datacompleta desc) as ultimasessao 
                        from 
                        (select distinct pedidoId, pedido.user, pedido.hora,
                        max(pedido.datacompleta) over(partition by pedidoId) as datacompleta, 
                        max(pedido.session) over(partition by pedidoId) as sessao 
                        from pedido 
                        where ano = " + ano + @" and mes = " + mes + @" and dia = " + dia + @" and hora <= " + hora + @" and minuto <= (case when hora < " + hora + @" then 60 else " + minuto + @" end)) pedidos); 

                        select origens.origem, origens.hora,
                        (select count(sessao) from visit 
                        where visit.origem = origens.origem and visit.hora = origens.hora and ano = " + ano + @" and mes = " + mes + @" and dia = " + dia + @" and hora <= " + hora + @" and minuto <= (case when hora < " + hora + @" then 60 else " + minuto + @" end)) as sessoes, 
                        (select count(pedidoId) from pedidosOrigemSessaoHora201643 
                        where origens.hora = pedidosOrigemSessaoHora201643.hora and origens.origem = (case when pedidosOrigemSessaoHora201643.origemsessao is null then 'direct' else pedidosOrigemSessaoHora201643.origemsessao end)) as pedidosOrigemSessao, 
                        (select count(pedidoId) from pedidosOrigemSessaoHora201643 
                        where origens.hora = pedidosOrigemSessaoHora201643.hora and origens.origem = (case when pedidosOrigemSessaoHora201643.ultimasessao is null then 'direct' else pedidosOrigemSessaoHora201643.ultimasessao end)) as pedidosUltimaSessao 
                        from(select DISTINCT origem, hora from visit where ano = " + ano + @" and mes = " + mes + @" and dia = " + dia + @" and hora <= " + hora + @" and minuto <= (case when hora < " + hora + @" then 60 else " + minuto + @" end)) origens;";
                }
                
                string ConnectionStr = "Server=gdganalytics.ctipjdzvlveb.us-east-1.redshift.amazonaws.com;Port=5439;Database=gdganalytics;User Id=gdganalytics;Password=Bark152029;Timeout=1000;Pooling=false;";
                NpgsqlConnection con = new NpgsqlConnection(ConnectionStr);
                NpgsqlCommand cmd = new NpgsqlCommand(comandoSql, con);
                NpgsqlDataAdapter m_dAdapter = new NpgsqlDataAdapter();
                cmd.CommandTimeout = 600;
                m_dAdapter.SelectCommand = cmd;

                con.Open();
                DataSet ds = new DataSet("relatorio");
                m_dAdapter.Fill(ds, "visits");
                retorno = JsonConvert.SerializeObject(ds);
                con.Close();
            }
        }

        Response.Clear();
        Response.ContentType = "application/json; charset=utf-8";
        Response.Write(retorno);
        Response.End();
    }
}