﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxGridView;
using Newtonsoft.Json.Converters;
using System.Web.Services;

public partial class admin_dashboard : System.Web.UI.Page
{
    internal class ListaProdutosSession
    {
        public int produtoId { get; set; }
        public string produto { get; set; }
        public string produtoAtivo { get; set; }
        public int? produtoPaiId { get; set; }
        public int? estoqueReal { get; set; }
        public bool foraDeLinha { get; set; }
        public string produtoNome { get; set; }
        public decimal margemDeLucro { get; set; }
        public int produtoEstoqueAtual { get; set; }
        public int? produtoEstoqueMinimo { get; set; }
        public string produtoDescricao { get; set; }
        public string produtoComposicao { get; set; }
        public string produtoPromocao { get; set; }
        public decimal produtoPreco { get; set; }
        public decimal? produtoPrecoPromocional { get; set; }
        public decimal? produtoPrecoDeCusto { get; set; }
        public int produtoFornecedor { get; set; }
        public int produtoMarca { get; set; }
    }

    internal class ListaProdutosEstoqueSession
    {
        public int produtoId { get; set; }
        public bool enviado { get; set; }
        public int? itemPedidoIdReserva { get; set; }
    }


    private static string Titulos(int index)
    {
        switch (index)
        {
            case 0:
                return "Produtos Fora de Linha";
            case 1:
                return "Produtos Fora de Linha Estoque 0";
            case 2:
                return "Produtos Fora de Linha Estoque Baixo";
            case 3:
                return "Produtos Estoque Baixo";
            case 4:
                return "Produtos Ativos sem Categoria";
            case 5:
                return "Categorias Ativas sem Produto";
            case 6:
                return "Produtos Ativos sem Foto";
            case 7:
                return "Produtos Ativos com Margem de Lucro Menor que 133";
            case 8:
                return "Combos Ativos com Margem de Lucro Menor que 122";
            case 9:
                return "Produtos Desativados com Estoque Maior que 0";
            case 10:
                return "Produtos Ativos com Estoque 0";
            case 11:
                return "Filtros sem Produtos Vinculados";
            case 12:
                return "Produtos Ativos sem Descrição";
            case 13:
                return "Produtos Ativos sem Coleção";
            case 14:
                return "Produtos Ativos sem Composição";
            case 15:
                return "Produtos Preço Promocional Maior que Preço de Venda";
            case 16:
                return "Produto Ativo em Promoção";
            case 17:
                return "Categoria Ativa sem Descrição";
            case 18:
                return "Estoque 0 ou Negativo";
            default:
                return "";
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ScriptManager.GetCurrent(this).RegisterPostBackControl(btXsl);

        if (!IsPostBack)
        {
            using (var data = new dbCommerceDataContext())
            {

                //var listaProdutos = (from c in data.tbProdutos
                //                     select new ListaProdutosSession
                //                     {
                //                         produtoId = c.produtoId,
                //                         produtoNome = c.produtoNome,
                //                         produtoAtivo = c.produtoAtivo == "True" ? "Sim" : "Nao",
                //                         produtoPaiId = c.produtoPaiId,
                //                         estoqueReal = c.estoqueReal,
                //                         foraDeLinha = c.foraDeLinha,
                //                         produtoEstoqueAtual = c.produtoEstoqueAtual,
                //                         produtoEstoqueMinimo = c.produtoEstoqueMinimo,
                //                         produtoDescricao = c.produtoDescricao,
                //                         produtoComposicao = c.produtoComposicao,
                //                         produtoPromocao = c.produtoPromocao,
                //                         produtoPreco = c.produtoPreco,
                //                         produtoPrecoPromocional = c.produtoPrecoPromocional,
                //                         produtoPrecoDeCusto = c.produtoPrecoDeCusto,
                //                         produtoFornecedor = c.produtoFornecedor,
                //                         produtoMarca = c.produtoMarca
                //                     }).ToList();

                //var listaEstoqueProdutos = (from c in listaProdutos
                //                            join pe in data.tbProdutoEstoques on c.produtoId equals pe.produtoId
                //                            where !pe.enviado && pe.itemPedidoIdReserva == null
                //                            select new ListaProdutosEstoqueSession
                //                            {
                //                                produtoId = pe.produtoId,
                //                                enviado = pe.enviado,
                //                                itemPedidoIdReserva = pe.itemPedidoIdReserva
                //                            }).ToList();

                //Session.Add("sessionProdutos", listaProdutos);
                //Session.Add("sessionProdutosEstoque", listaEstoqueProdutos);

            }



            //FillDashboard();

            ddlFornecedor.Items.Insert(0, new ListItem("Todo Site", "0"));
            ddlFiltros.Items.Insert(0, new ListItem("Todos Filtros", "0"));
        }
        else
        {

            grd.Columns[0].Visible =
            grd.Columns[1].Visible =
            grd.Columns[2].Visible =
            grd.Columns[3].Visible =
            grd.Columns[4].Visible =
            grd.Columns[5].Visible = true;
            grd.FilterExpression = "";
            grd.DataBind();

            //int fornecedor = Convert.ToInt32(ddlFornecedor.SelectedValue);
            //if (fornecedor == 0) ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "var obj = Object.create(DashboardProdutos);obj.init(0,0,0);", true);
            //if (fornecedor != 0) ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "var obj = Object.create(DashboardProdutos);obj.init(" + fornecedor + "," + (ddlCategoria.SelectedValue == "" ? hfCategoriaId.Value : ddlCategoria.SelectedValue) + ",0);", true);

            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "var obj = Object.create(DashboardProdutosDadosCash);obj.init();", true);

            if (string.IsNullOrEmpty(lblTituloDetalhamento.Text)) return;

            switch (lblTituloDetalhamento.Text)
            {
                case "Produtos Fora de Linha":
                    FillDetalharFL();
                    break;
                case "Produtos Fora de Linha Estoque 0":
                    FillDetalharFLE0();
                    break;
                case "Produtos Fora de Linha Estoque Baixo":
                    FillDetalharFLEB();
                    break;
                case "Produtos Estoque Baixo":
                    FillDetalharEB();
                    break;
                case "Produtos Ativos sem Categoria":
                    FillDetalharPASF();
                    break;
                case "Categorias Ativas sem Produto":
                    FillDetalharCASP();
                    break;
                case "Produtos Ativos sem Foto":
                    FillDetalharPASF();
                    break;
                case "Produtos Ativos com Margem de Lucro Menor que 133":
                    FillDetalharPAMN133();
                    break;
                case "Combos Ativos com Margem de Lucro Menor que 122":
                    FillDetalharCMN122();
                    break;
                case "Produtos Desativados com Estoque Maior que 0":
                    FillDetalharPDEM0();
                    break;
                case "Produtos Ativos com Estoque 0":
                    FillDetalharPASE0();
                    break;
                case "Filtros sem Produtos Vinculados":
                    FillDetalharFSP();
                    break;
                case "Produtos Ativos sem Descrição":
                    FillDetalharPASD();
                    break;
                case "Produtos Ativos sem Coleção":
                    FillDetalharPASCO();
                    break;
                case "Produtos Ativos sem Composição":
                    FillDetalharPASCOM();
                    break;
                case "Produto Ativo em Promoção":
                    FillDetalharPAEP();
                    break;
                case "Categoria Ativa sem Descrição":
                    FillDetalharCASD();
                    break;
                case "Estoque 0 ou Negativo":
                    FillDetalharE0N();
                    break;
            }

            int filtro = Convert.ToInt32(ddlFiltros.SelectedValue);
            if (filtro != 0) CarregaGrdPesquisaPorFiltro();

        }



    }

    protected void FillDashboard()
    {
        try
        {
            List<ListaProdutosSession> listaProdutosFromSession = (List<ListaProdutosSession>)Session["sessionProdutos"];
            List<ListaProdutosEstoqueSession> listaProdutosEstoqueFromSession = (List<ListaProdutosEstoqueSession>)Session["sessionProdutosEstoque"];


            //litForaDeLinha.Text = (from c in listaProdutosFromSession where c.foraDeLinha && c.produtoAtivo == "Sim" select new { c.produtoId }).Count().ToString();
            //litForaDeLinhaEstoque0.Text = (from c in listaProdutosFromSession where c.foraDeLinha && c.estoqueReal == 0 && c.produtoAtivo == "Sim" select new { c.produtoId }).Count().ToString();
            //litForaDeLinhaEstoqueBaixo.Text = (from c in listaProdutosFromSession where c.foraDeLinha && (c.estoqueReal > 0 && c.estoqueReal < 11) && c.produtoAtivo == "Sim" select new { c.produtoId }).Count().ToString();
            //litEstoqueBaixo.Text = (from c in listaProdutosFromSession where c.estoqueReal > 0 && c.estoqueReal < 11 && c.produtoAtivo == "Sim" select new { c.produtoId }).Count().ToString();

            using (var data = new dbCommerceDataContext())
            {
                data.ObjectTrackingEnabled = false;


                //litProdutoAtivoSemCategoria.Text = (from c in listaProdutosFromSession
                //                                    where c.produtoAtivo == "Sim"
                //                                    join cat in data.tbJuncaoProdutoCategorias on c.produtoId equals cat.produtoId into psc
                //                                    from cat in psc.DefaultIfEmpty()
                //                                    where c.produtoId == null
                //                                    select c).Count().ToString();


                //litCategoriaAtivaSemProduto.Text = (from c in listaProdutosFromSession
                //                                    where c.produtoAtivo == "Sim"
                //                                    join cat in data.tbJuncaoProdutoCategorias on c.produtoId equals cat.produtoId into psc
                //                                    from cat in psc.DefaultIfEmpty()
                //                                    where !psc.Select(x => x.produtoId == null && (x.tbProdutoCategoria.exibirSite ?? false)).Any()
                //                                    select c).Count().ToString();


                litCategoriaAtivaSemDescricao.Text = (from c in data.tbProdutoCategorias
                                                      where c.exibirSite == true && (c.categoriaDescricao ?? "") == ""
                                                      select new { c.categoriaId }).Count().ToString();

                litProdutoAtivoSemFoto.Text = (from c in listaProdutosFromSession
                                               where c.produtoAtivo == "Sim"
                                               join pf in data.tbProdutoFotos on c.produtoId equals pf.produtoId into psf
                                               from pf in psf.DefaultIfEmpty()
                                               where !psf.Select(x => x.produtoId).Any()
                                               select c).Count().ToString();

                //litProdutoAtivoComMargemMenor133.Text = (from c in listaProdutosFromSession
                //                                         where c.produtoAtivo == "Sim" && c.margemDeLucro < 132
                //                                         join pr in data.tbProdutoRelacionados on c.produtoId equals pr.idProdutoPai into pamn133
                //                                         from pr in pamn133.DefaultIfEmpty()
                //                                         where !pamn133.Select(x => x.idProdutoPai).Any()
                //                                         select c).Count().ToString();

                litComboComMargemMenor122.Text = (from c in listaProdutosFromSession
                                                  where c.produtoAtivo == "Sim" && c.margemDeLucro < 121
                                                  join pr in data.tbProdutoRelacionados on c.produtoId equals pr.idProdutoPai
                                                  group c by new
                                                  {
                                                      c.produtoId,
                                                      produto = c.produtoNome,
                                                      ativo = c.produtoAtivo == "True" ? "Sim" : "Nao"
                                                  }).Count().ToString();

                using (
                   var txn = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required,
                       new System.Transactions.TransactionOptions
                       {
                           IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                       }))

                {

                    var listaEstoque = (from c in listaProdutosFromSession
                                        join pe in listaProdutosEstoqueFromSession on c.produtoId equals pe.produtoId
                                        select pe).ToList();

                    var listaProdutoCombo = (from c in listaProdutosFromSession
                                             join d in data.tbProdutoRelacionados on c.produtoId equals d.idProdutoFilho
                                             join pe in listaEstoque on c.produtoId equals pe.produtoId
                                             where
                                                 c.produtoAtivo == "Nao" && c.estoqueReal > 0 && !pe.enviado && pe.itemPedidoIdReserva == null
                                             select new
                                             {
                                                 c.produtoId,
                                                 produto = c.produtoNome,
                                                 ativo = c.produtoAtivo,
                                                 c.produtoPaiId,
                                                 c.estoqueReal
                                             }).Distinct().ToList();

                    var produtoPaiDoCombo = (from pc in listaProdutoCombo
                                             join c in listaProdutosFromSession on pc.produtoPaiId equals c.produtoId
                                             where c.produtoAtivo == "Nao"
                                             select new
                                             {
                                                 c.produtoId,
                                                 produto = c.produtoNome,
                                                 ativo = c.produtoAtivo,
                                                 c.produtoPaiId,
                                                 c.estoqueReal
                                             }).Distinct().ToList();

                    var listaProdutoNaoCombo = (from c in listaProdutosFromSession
                                                join d in data.tbProdutoRelacionados on c.produtoId equals d.idProdutoFilho into nc
                                                join pe in listaEstoque on c.produtoId equals pe.produtoId
                                                from d in nc.DefaultIfEmpty()
                                                where
                                                    !nc.Select(x => x.idProdutoFilho).Any() && c.produtoAtivo == "Nao" && c.estoqueReal > 0 && !pe.enviado && pe.itemPedidoIdReserva == null
                                                select new
                                                {
                                                    c.produtoId,
                                                    produto = c.produtoNome,
                                                    ativo = c.produtoAtivo,
                                                    c.produtoPaiId,
                                                    c.estoqueReal
                                                }).Distinct().ToList();

                    var listaFinal = produtoPaiDoCombo.Union(listaProdutoNaoCombo).ToList();
                    litProdutosDesativadosComEstoqueMaiorQue0.Text = listaFinal.Count().ToString();
                    //var listaPrimeiraVerificacao = (from c in data.tbProdutos
                    //                                join pe in data.tbProdutoEstoques on c.produtoId equals pe.produtoId
                    //                                where c.produtoAtivo == "False" && c.estoqueReal > 0 && !pe.enviado && pe.idItemPedidoEstoqueReserva == null
                    //                                select new
                    //                                {
                    //                                    c.produtoId
                    //                                }).Distinct().ToList();

                    //var produtosNaoCombo = (from c in listaPrimeiraVerificacao
                    //                        join d in data.tbProdutoRelacionados on c.produtoId equals d.idProdutoPai into nc
                    //                        from d in nc.DefaultIfEmpty()
                    //                        where !nc.Select(x => x.idProdutoFilho).Any()
                    //                        select new { c.produtoId }).Distinct().ToList();

                    //var listaSegundaVerificacao = (from ipe in data.tbItemPedidoEstoques
                    //                               where !ipe.reservado && !ipe.cancelado
                    //                               select new
                    //                               {
                    //                                   ipe.produtoId
                    //                               }).Distinct().ToList();

                    //litProdutosDesativadosComEstoqueMaiorQue0.Text = (from c in produtosNaoCombo
                    //                                                  join d in listaSegundaVerificacao on c.produtoId equals d.produtoId into psp
                    //                                                  from d in psp.DefaultIfEmpty()
                    //                                                  where !psp.Select(x => x.produtoId).Any()
                    //                                                  select new { c.produtoId }).Distinct().ToList().Count().ToString();

                    //litProdutosDesativadosComEstoqueMaiorQue0.Text = (from c in listaPrimeiraVerificacao
                    //                                                  join ipe in data.tbItemPedidoEstoques on c.produtoId equals ipe.produtoId into psp
                    //                                                  from ipe in psp.DefaultIfEmpty()
                    //                                                  where psp.Select(x => !x.reservado && !x.cancelado).Any()
                    //                                                  select new
                    //                                                  {
                    //                                                      c.produtoId
                    //                                                  }).Distinct().ToList().Count().ToString();
                }

                litProdutosAtivosComEstoque0.Text = (from c in listaProdutosFromSession
                                                     where c.produtoAtivo == "Sim" && (c.produtoEstoqueAtual <= 0 && (c.produtoEstoqueAtual <= c.produtoEstoqueMinimo))
                                                     select new
                                                     {
                                                         c.produtoId
                                                     }).Count().ToString();

                litProdutoAtivoSemDescricao.Text = (from c in listaProdutosFromSession where c.produtoAtivo == "Sim" && (c.produtoDescricao ?? "") == "" select new { c.produtoId }).Count().ToString();

                litProdutoAtivoSemColecao.Text = (from c in listaProdutosFromSession
                                                  join pc in data.tbJuncaoProdutoColecaos on c.produtoId equals pc.produtoId into asc
                                                  from pc in asc.DefaultIfEmpty()
                                                  where c.produtoAtivo == "Sim" && !asc.Select(x => x.produtoId).Any()
                                                  select new
                                                  {
                                                      c.produtoId
                                                  }).Count().ToString();

                litProdutoAtivoSemComposicao.Text = (from c in listaProdutosFromSession
                                                     join ia in data.tbInformacaoAdcionals on c.produtoId equals ia.produtoId into psia
                                                     from ia in psia.DefaultIfEmpty()
                                                     where c.produtoAtivo == "Sim" && !psia.Select(x => x.produtoId).Any()
                                                     select new { c.produtoId }).Take(50).Count().ToString();

                litProdutoAtivoEmPromocao.Text = (from c in listaProdutosFromSession where c.produtoAtivo == "Sim" && (c.produtoPromocao ?? "") == "True" select new { c.produtoId }).Count().ToString();

                //litPrecoPromcionalMaiorQuePrecoVenda.Text = (from c in listaProdutosFromSession where c.produtoAtivo == "Sim" && (c.produtoPrecoPromocional > c.produtoPreco) select new { c.produtoId }).Count().ToString();

                litPrecoDeCustoMaiorQuePrecoVenda.Text = (from c in listaProdutosFromSession where c.produtoAtivo == "Sim" && (c.produtoPrecoDeCusto > c.produtoPreco) select new { c.produtoId }).Count().ToString();

                var filtros = (from c in data.tbProdutoCategorias where c.categoriaPaiId > 0 && c.exibirSite == false && c.idSite == 1 select c);

                litFiltroSemProduto.Text = (from c in filtros
                                            join d in data.tbJuncaoProdutoCategorias on c.categoriaId equals d.categoriaId into fsp
                                            from d in fsp.DefaultIfEmpty()
                                            where !fsp.Select(x => x.categoriaId).Any()
                                            select new
                                            {
                                                categoria = c.categoriaNome
                                            }).Distinct().ToList().Count().ToString();

            }
        }
        catch (Exception)
        {

            throw;
        }
    }

    protected void FillDashboardFiltro()
    {
        try
        {

            int fornecedor = Convert.ToInt32(ddlFornecedor.SelectedValue);
            int categoria = Convert.ToInt32(ddlCategoria.SelectedValue);

            using (var data = new dbCommerceDataContext())
            {
                data.ObjectTrackingEnabled = false;

                using (
                   var txn = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required,
                       new System.Transactions.TransactionOptions
                       {
                           IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                       }))

                {


                    var foraDeLinha = (from c in data.tbProdutos where c.foraDeLinha && c.produtoAtivo == "True" select new { c.produtoId, c.produtoFornecedor });
                    if (fornecedor > 0)
                        foraDeLinha = foraDeLinha.Where(x => x.produtoFornecedor == fornecedor);

                    if (categoria > 0)
                        foraDeLinha = (from c in foraDeLinha
                                       join cat in data.tbJuncaoProdutoCategorias on c.produtoId equals cat.produtoId
                                       where cat.categoriaId == categoria
                                       select c);

                    litForaDeLinha.Text = foraDeLinha.Count().ToString();

                    var foraDeLinhaEstoque0 = (from c in data.tbProdutos where c.foraDeLinha && c.estoqueReal == 0 && c.produtoAtivo == "True" select new { c.produtoId, c.produtoFornecedor });
                    if (fornecedor > 0)
                        foraDeLinhaEstoque0 = foraDeLinhaEstoque0.Where(x => x.produtoFornecedor == fornecedor);

                    if (categoria > 0)
                        foraDeLinhaEstoque0 = (from c in foraDeLinhaEstoque0
                                               join cat in data.tbJuncaoProdutoCategorias on c.produtoId equals cat.produtoId
                                               where cat.categoriaId == categoria
                                               select c);

                    litForaDeLinhaEstoque0.Text = foraDeLinhaEstoque0.Count().ToString();

                    var foraDeLinhaEstoqueBaixo = (from c in data.tbProdutos where c.foraDeLinha && (c.estoqueReal > 0 && c.estoqueReal < 11) && c.produtoAtivo == "True" select new { c.produtoId, c.produtoFornecedor });
                    if (fornecedor > 0)
                        foraDeLinhaEstoqueBaixo = foraDeLinhaEstoqueBaixo.Where(x => x.produtoFornecedor == fornecedor);

                    if (categoria > 0)
                        foraDeLinhaEstoqueBaixo = (from c in foraDeLinhaEstoqueBaixo
                                                   join cat in data.tbJuncaoProdutoCategorias on c.produtoId equals cat.produtoId
                                                   where cat.categoriaId == categoria
                                                   select c);

                    litForaDeLinhaEstoqueBaixo.Text = foraDeLinhaEstoqueBaixo.Count().ToString();

                    var estoqueBaixo = (from c in data.tbProdutos where c.estoqueReal > 0 && c.estoqueReal < 11 && c.produtoFornecedor == fornecedor && c.produtoAtivo == "True" select new { c.produtoId, c.produtoFornecedor });
                    if (fornecedor > 0)
                        estoqueBaixo = estoqueBaixo.Where(x => x.produtoFornecedor == fornecedor);

                    if (categoria > 0)
                        estoqueBaixo = (from c in estoqueBaixo
                                        join cat in data.tbJuncaoProdutoCategorias on c.produtoId equals cat.produtoId
                                        where cat.categoriaId == categoria
                                        select c);

                    litEstoqueBaixo.Text = estoqueBaixo.Count().ToString();

                    var produtoAtivoSemCategoria = (from c in data.tbProdutos
                                                    where c.produtoAtivo == "True"
                                                    join cat in data.tbJuncaoProdutoCategorias on c.produtoId equals cat.produtoId into psc
                                                    from cat in psc.DefaultIfEmpty()
                                                    where c.produtoId == null
                                                    select new { c.produtoId, c.produtoFornecedor });

                    if (fornecedor > 0)
                        produtoAtivoSemCategoria = produtoAtivoSemCategoria.Where(x => x.produtoFornecedor == fornecedor);

                    if (categoria > 0)
                        produtoAtivoSemCategoria = (from c in produtoAtivoSemCategoria
                                                    join cat in data.tbJuncaoProdutoCategorias on c.produtoId equals cat.produtoId
                                                    where cat.categoriaId == categoria
                                                    select c);

                    litProdutoAtivoSemCategoria.Text = produtoAtivoSemCategoria.Count().ToString();


                    var categoriaAtivaSemProduto = (from c in data.tbProdutos
                                                    where c.produtoAtivo == "True"
                                                    join cat in data.tbJuncaoProdutoCategorias on c.produtoId equals cat.produtoId into psc
                                                    from cat in psc.DefaultIfEmpty()
                                                    where !psc.Select(x => x.produtoId == null && (x.tbProdutoCategoria.exibirSite ?? false)).Any()
                                                    select new { c.produtoId, c.produtoFornecedor });
                    if (fornecedor > 0)
                        categoriaAtivaSemProduto = categoriaAtivaSemProduto.Where(x => x.produtoFornecedor == fornecedor);

                    if (categoria > 0)
                        categoriaAtivaSemProduto = (from c in categoriaAtivaSemProduto
                                                    join cat in data.tbJuncaoProdutoCategorias on c.produtoId equals cat.produtoId
                                                    where cat.categoriaId == categoria
                                                    select c);

                    litCategoriaAtivaSemProduto.Text = categoriaAtivaSemProduto.Count().ToString();


                    litCategoriaAtivaSemDescricao.Text = (from c in data.tbProdutoCategorias
                                                          where c.exibirSite == true && (c.categoriaDescricao ?? "") == ""
                                                          select new { c.categoriaId }).Count().ToString();

                    var produtoAtivoSemFoto = (from c in data.tbProdutos
                                               where c.produtoAtivo == "True"
                                               join pf in data.tbProdutoFotos on c.produtoId equals pf.produtoId into psf
                                               from pf in psf.DefaultIfEmpty()
                                               where !psf.Select(x => x.produtoId).Any()
                                               select new { c.produtoId, c.produtoFornecedor });
                    if (fornecedor > 0)
                        produtoAtivoSemFoto = produtoAtivoSemFoto.Where(x => x.produtoFornecedor == fornecedor);

                    if (categoria > 0)
                        produtoAtivoSemFoto = (from c in produtoAtivoSemFoto
                                               join cat in data.tbJuncaoProdutoCategorias on c.produtoId equals cat.produtoId
                                               where cat.categoriaId == categoria
                                               select c);

                    litProdutoAtivoSemFoto.Text = produtoAtivoSemFoto.Count().ToString();

                    //var produtoAtivoComMargemMenor133 = (from c in listaProdutosFromSession
                    //                                     where c.produtoAtivo == "Sim" && c.margemDeLucro < 132 && c.produtoFornecedor == fornecedor
                    //                                     join pr in data.tbProdutoRelacionados on c.produtoId equals pr.idProdutoPai into pamn133
                    //                                     from pr in pamn133.DefaultIfEmpty()
                    //                                     where !pamn133.Select(x => x.idProdutoPai).Any()
                    //                                     select c).Count().ToString();

                    //litProdutoAtivoComMargemMenor133.Text = produtoAtivoComMargemMenor133;

                    var comboComMargemMenor122 = (from c in data.tbProdutos
                                                  where c.produtoAtivo == "True" && c.margemDeLucro < 121
                                                  join pr in data.tbProdutoRelacionados on c.produtoId equals pr.idProdutoPai
                                                  group c by new
                                                  {
                                                      c.produtoId,
                                                      c.produtoFornecedor
                                                  });

                    if (fornecedor > 0)
                        comboComMargemMenor122 = comboComMargemMenor122.Where(x => x.Key.produtoFornecedor == fornecedor);

                    litComboComMargemMenor122.Text = comboComMargemMenor122.Count().ToString();


                    //var listaEstoque = (from c in listaProdutosFromSession
                    //                    join pe in listaProdutosEstoqueFromSession on c.produtoId equals pe.produtoId
                    //                    where c.produtoFornecedor == fornecedor
                    //                    select pe).ToList();

                    //var listaProdutoCombo = (from c in listaProdutosFromSession
                    //                         join d in data.tbProdutoRelacionados on c.produtoId equals d.idProdutoFilho
                    //                         join pe in listaEstoque on c.produtoId equals pe.produtoId
                    //                         where
                    //                             c.produtoAtivo == "Nao" && c.estoqueReal > 0 && !pe.enviado && pe.itemPedidoIdReserva == null
                    //                         select new
                    //                         {
                    //                             c.produtoId,
                    //                             c.produtoPaiId,
                    //                             c.produtoFornecedor
                    //                         }).Distinct().ToList();

                    //if (fornecedor > 0)
                    //    listaProdutoCombo = listaProdutoCombo.Where(x => x.produtoFornecedor == fornecedor).Distinct().ToList();

                    //if (categoria > 0)
                    //    listaProdutoCombo = (from c in listaProdutoCombo
                    //                         join cat in data.tbJuncaoProdutoCategorias on c.produtoId equals cat.produtoId
                    //                         where cat.categoriaId == categoria
                    //                         select c).Distinct().ToList();

                    //var produtoPaiDoCombo = (from pc in listaProdutoCombo
                    //                         join c in listaProdutosFromSession on pc.produtoPaiId equals c.produtoId
                    //                         where c.produtoAtivo == "Nao"
                    //                         select new
                    //                         {
                    //                             c.produtoId,
                    //                             c.produtoPaiId,
                    //                             c.produtoFornecedor
                    //                         }).Distinct().ToList();

                    //var listaProdutoNaoCombo = (from c in listaProdutosFromSession
                    //                            join d in data.tbProdutoRelacionados on c.produtoId equals d.idProdutoFilho into nc
                    //                            join pe in listaEstoque on c.produtoId equals pe.produtoId
                    //                            from d in nc.DefaultIfEmpty()
                    //                            where
                    //                                !nc.Select(x => x.idProdutoFilho).Any() && c.produtoAtivo == "Nao" && c.estoqueReal > 0 && !pe.enviado && pe.itemPedidoIdReserva == null
                    //                            select new
                    //                            {
                    //                                c.produtoId,
                    //                                c.produtoPaiId,
                    //                                c.produtoFornecedor
                    //                            }).Distinct().ToList();

                    //if (fornecedor > 0)
                    //    listaProdutoNaoCombo = listaProdutoNaoCombo.Where(x => x.produtoFornecedor == fornecedor).Distinct().ToList();

                    //if (categoria > 0)
                    //    listaProdutoNaoCombo = (from c in listaProdutoNaoCombo
                    //                            join cat in data.tbJuncaoProdutoCategorias on c.produtoId equals cat.produtoId
                    //                            where cat.categoriaId == categoria
                    //                            select c).Distinct().ToList();

                    var listaEstoque = (from c in data.tbProdutos
                                        join pe in data.tbProdutoEstoques on c.produtoId equals pe.produtoId
                                        where !pe.enviado && pe.itemPedidoIdReserva == null
                                        select new { pe.produtoId, pe.enviado, pe.itemPedidoIdReserva, c.produtoFornecedor, c.produtoAtivo, c.estoqueReal, c.produtoPaiId, c.produtoNome }).ToList();

                    if (fornecedor > 0)
                        listaEstoque = listaEstoque.Where(x => x.produtoFornecedor == fornecedor).ToList();

                    if (categoria > 0)
                        listaEstoque = (from c in listaEstoque
                                        join cat in data.tbJuncaoProdutoCategorias on c.produtoId equals cat.produtoId
                                        where cat.categoriaId == categoria
                                        select c).Distinct().ToList();

                    var listaProdutoCombo = (from pe in listaEstoque
                                             join d in data.tbProdutoRelacionados on pe.produtoId equals d.idProdutoFilho
                                             where
                                                 pe.produtoAtivo == "False" && pe.estoqueReal > 0
                                             select new
                                             {
                                                 pe.produtoId,
                                                 produto = pe.produtoNome,
                                                 ativo = pe.produtoAtivo,
                                                 pe.produtoPaiId,
                                                 pe.estoqueReal
                                             }).Distinct().ToList();

                    //var produtoPaiDoCombo = (from pc in listaProdutoCombo
                    //                         join c in data.tbProdutoRelacionados on pc.produtoId equals c.idProdutoPai
                    //                         join p in data.tbProdutos on c.idProdutoPai equals p.produtoId
                    //                         where p.produtoAtivo == "False"
                    //                         select new
                    //                         {
                    //                             p.produtoId,
                    //                             produto = p.produtoNome,
                    //                             ativo = p.produtoAtivo,
                    //                             p.estoqueReal
                    //                         }).Distinct().ToList();

                    var produtoPaiDoCombo = (from pc in listaProdutoCombo
                                             join c in data.tbProdutoRelacionados on pc.produtoId equals c.idProdutoPai
                                             select new
                                             {
                                                 produtoId = c.idProdutoPai
                                             }).Distinct().ToList();

                    List<int> listaProdutosPai = produtoPaiDoCombo.Select(x => x.produtoId).ToList();

                    var listaFinalProdutoPaiDoCombo = (from c in data.tbProdutos
                                                       where listaProdutosPai.Contains(c.produtoId)
                                                       select new
                                                       {
                                                           c.produtoId,
                                                           produto = c.produtoNome,
                                                           ativo = c.produtoAtivo,
                                                           c.estoqueReal
                                                       }).ToList();

                    var listaProdutoNaoCombo = (from pe in listaEstoque
                                                join d in data.tbProdutoRelacionados on pe.produtoId equals d.idProdutoFilho into nc
                                                from d in nc.DefaultIfEmpty()
                                                where
                                                    !nc.Select(x => x.idProdutoFilho).Any() && pe.produtoAtivo == "False" && pe.estoqueReal > 0
                                                select new
                                                {
                                                    pe.produtoId,
                                                    produto = pe.produtoNome,
                                                    ativo = pe.produtoAtivo,
                                                    pe.estoqueReal
                                                }).Distinct().ToList();

                    var listaFinal = listaFinalProdutoPaiDoCombo.Any() ? listaFinalProdutoPaiDoCombo.Union(listaProdutoNaoCombo).ToList() : listaProdutoNaoCombo.ToList();
                    litProdutosDesativadosComEstoqueMaiorQue0.Text = listaFinal.Count().ToString();


                    var produtoAtivosComEstoque0 = (from c in data.tbProdutos
                                                    where c.produtoAtivo == "True" && (c.produtoEstoqueAtual <= 0 && (c.produtoEstoqueAtual <= c.produtoEstoqueMinimo))
                                                    select new
                                                    {
                                                        c.produtoId,
                                                        c.produtoFornecedor
                                                    });
                    if (fornecedor > 0)
                        produtoAtivosComEstoque0 = produtoAtivosComEstoque0.Where(x => x.produtoFornecedor == fornecedor);

                    if (categoria > 0)
                        produtoAtivosComEstoque0 = (from c in produtoAtivosComEstoque0
                                                    join cat in data.tbJuncaoProdutoCategorias on c.produtoId equals cat.produtoId
                                                    where cat.categoriaId == categoria
                                                    select c);

                    litProdutosAtivosComEstoque0.Text = produtoAtivosComEstoque0.Count().ToString();

                    var produtoAtivoSemDescricao = (from c in data.tbProdutos where c.produtoAtivo == "True" && (c.produtoDescricao ?? "") == "" select new { c.produtoId, c.produtoFornecedor });
                    if (fornecedor > 0)
                        produtoAtivoSemDescricao = produtoAtivoSemDescricao.Where(x => x.produtoFornecedor == fornecedor);

                    if (categoria > 0)
                        produtoAtivoSemDescricao = (from c in produtoAtivoSemDescricao
                                                    join cat in data.tbJuncaoProdutoCategorias on c.produtoId equals cat.produtoId
                                                    where cat.categoriaId == categoria
                                                    select c);

                    litProdutoAtivoSemDescricao.Text = produtoAtivoSemDescricao.Count().ToString();

                    var produtoAtivoSemColecao = (from c in data.tbProdutos
                                                  join pc in data.tbJuncaoProdutoColecaos on c.produtoId equals pc.produtoId into asc
                                                  from pc in asc.DefaultIfEmpty()
                                                  where c.produtoAtivo == "True" && !asc.Select(x => x.produtoId).Any()
                                                  select new
                                                  {
                                                      c.produtoId,
                                                      c.produtoFornecedor
                                                  });
                    if (fornecedor > 0)
                        produtoAtivoSemColecao = produtoAtivoSemColecao.Where(x => x.produtoFornecedor == fornecedor);

                    if (categoria > 0)
                        produtoAtivoSemColecao = (from c in produtoAtivoSemColecao
                                                  join cat in data.tbJuncaoProdutoCategorias on c.produtoId equals cat.produtoId
                                                  where cat.categoriaId == categoria
                                                  select c);

                    litProdutoAtivoSemColecao.Text = produtoAtivoSemColecao.Count().ToString();

                    var produtoAtivoSemComposicao = (from c in data.tbProdutos
                                                     join ia in data.tbInformacaoAdcionals on c.produtoId equals ia.produtoId into psia
                                                     from ia in psia.DefaultIfEmpty()
                                                     where c.produtoAtivo == "True" && !psia.Select(x => x.produtoId).Any()
                                                     select new { c.produtoId, c.produtoFornecedor });
                    if (fornecedor > 0)
                        produtoAtivoSemComposicao = produtoAtivoSemComposicao.Where(x => x.produtoFornecedor == fornecedor);

                    if (categoria > 0)
                        produtoAtivoSemComposicao = (from c in produtoAtivoSemComposicao
                                                     join cat in data.tbJuncaoProdutoCategorias on c.produtoId equals cat.produtoId
                                                     where cat.categoriaId == categoria
                                                     select c);

                    litProdutoAtivoSemComposicao.Text = produtoAtivoSemComposicao.Count().ToString();

                    var produtoAtivoEmPromocao = (from c in data.tbProdutos where c.produtoAtivo == "True" && (c.produtoPromocao ?? "") == "True" select new { c.produtoId, c.produtoFornecedor });
                    if (fornecedor > 0)
                        produtoAtivoEmPromocao = produtoAtivoEmPromocao.Where(x => x.produtoFornecedor == fornecedor);

                    if (categoria > 0)
                        produtoAtivoEmPromocao = (from c in produtoAtivoEmPromocao
                                                  join cat in data.tbJuncaoProdutoCategorias on c.produtoId equals cat.produtoId
                                                  where cat.categoriaId == categoria
                                                  select c);

                    litProdutoAtivoEmPromocao.Text = produtoAtivoEmPromocao.Count().ToString();

                    //var precoPromocionalMaiorQuePrecoVenda = (from c in listaProdutosFromSession where c.produtoAtivo == "Sim" && (c.produtoPrecoPromocional > c.produtoPreco) && c.produtoFornecedor == fornecedor select new { c.produtoId }).Count().ToString();
                    //litPrecoPromcionalMaiorQuePrecoVenda.Text = precoPromocionalMaiorQuePrecoVenda;

                    var precoDeCustoMaiorQuePrecoVenda = (from c in data.tbProdutos where c.produtoAtivo == "Sim" && (c.produtoPrecoDeCusto > c.produtoPreco) select new { c.produtoId, c.produtoFornecedor });
                    if (fornecedor > 0)
                        precoDeCustoMaiorQuePrecoVenda = precoDeCustoMaiorQuePrecoVenda.Where(x => x.produtoFornecedor == fornecedor);

                    if (categoria > 0)
                        precoDeCustoMaiorQuePrecoVenda = (from c in precoDeCustoMaiorQuePrecoVenda
                                                          join cat in data.tbJuncaoProdutoCategorias on c.produtoId equals cat.produtoId
                                                          where cat.categoriaId == categoria
                                                          select c);

                    litPrecoDeCustoMaiorQuePrecoVenda.Text = precoDeCustoMaiorQuePrecoVenda.Count().ToString();

                    var filtros = (from c in data.tbProdutoCategorias where c.categoriaPaiId > 0 && c.exibirSite == false && c.idSite == 1 select c);

                    var filtroSemProduto = (from c in filtros
                                            join d in data.tbJuncaoProdutoCategorias on c.categoriaId equals d.categoriaId into fsp
                                            from d in fsp.DefaultIfEmpty()
                                            where !fsp.Select(x => x.categoriaId).Any()
                                            select new
                                            {
                                                categoria = c.categoriaNome,
                                                d.tbProduto.produtoFornecedor
                                            });
                    if (fornecedor > 0)
                        filtroSemProduto = filtroSemProduto.Where(x => x.produtoFornecedor == fornecedor);

                    litFiltroSemProduto.Text = filtroSemProduto.Distinct().Count().ToString();
                }
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('" + ex.Message + " - " + ex.StackTrace + "');", true);
        }
    }

    #region Detalhar

    protected void btnDetalharFL_OnCommand(object sender, CommandEventArgs e)
    {
        FillDetalharFL();
    }

    protected void btnDetalharFLE0_OnCommand(object sender, CommandEventArgs e)
    {
        FillDetalharFLE0();
    }

    protected void btnDetalharFLEB_OnCommand(object sender, CommandEventArgs e)
    {
        FillDetalharFLEB();
    }

    protected void btnDetalharEB_OnCommand(object sender, CommandEventArgs e)
    {
        FillDetalharEB();
    }

    protected void btnDetalharE0N_OnCommand(object sender, CommandEventArgs e)
    {
        FillDetalharE0N();
    }

    protected void btnDetalharPASC_OnCommand(object sender, CommandEventArgs e)
    {
        FillDetalharPASC();
    }

    protected void btnDetalharCASP_OnCommand(object sender, CommandEventArgs e)
    {
        FillDetalharCASP();
    }

    protected void btnDetalharCASD_OnCommand(object sender, CommandEventArgs e)
    {
        FillDetalharCASD();
    }

    protected void btnDetalharPASF_OnCommand(object sender, CommandEventArgs e)
    {
        FillDetalharPASF();
    }

    protected void btnDetalharPAMN133_OnCommand(object sender, CommandEventArgs e)
    {
        FillDetalharPAMN133();
    }

    protected void btnDetalharCMN122_OnCommand(object sender, CommandEventArgs e)
    {
        FillDetalharCMN122();
    }

    protected void brnDetalharPDEM0_OnCommand(object sender, CommandEventArgs e)
    {
        FillDetalharPDEM0();
    }

    protected void btnDetalharPASE0_OnCommand(object sender, CommandEventArgs e)
    {
        FillDetalharPASE0();
    }


    protected void btnDetalhePASD_OnCommand(object sender, CommandEventArgs e)
    {
        FillDetalharPASD();
    }

    protected void btnDetalhePASCO_OnCommand(object sender, CommandEventArgs e)
    {
        FillDetalharPASCO();
    }

    protected void btnDetalhePASCOM_OnCommand(object sender, CommandEventArgs e)
    {
        FillDetalharPASCOM();
    }

    protected void btnDetalhePAEP_OnCommand(object sender, CommandEventArgs e)
    {
        FillDetalharPAEP();
    }


    protected void btnDetalharFSP_OnCommand(object sender, CommandEventArgs e)
    {
        FillDetalharFSP();
    }

    protected void btnDetalharPPMPV_OnCommand(object sender, CommandEventArgs e)
    {
        FillDetalharPPMPV();
    }

    #endregion Detalhar

    #region Fill Grid
    /// <summary>
    /// Produtos fora de linha
    /// </summary>
    protected void FillDetalharFL()
    {
        lblTituloDetalhamento.Text = Titulos(0);
        lblTituloDetalhamento.Visible = true;

        using (var data = new dbCommerceDataContext())
        {
            int fornecedor = Convert.ToInt32(ddlFornecedor.SelectedValue);

            if (fornecedor == 0)
            {
                grd.DataSource = (from c in data.tbProdutos
                                  where c.foraDeLinha
                                  select new
                                  {
                                      c.produtoId,
                                      produto = c.produtoNome,
                                      ativo = c.produtoAtivo == "True" ? "Sim" : "Nao",
                                      c.produtoPaiId,
                                      estoqueReal = ""
                                  }).ToList();
            }
            else
            {
                var dados = (from c in data.tbProdutos
                             where c.foraDeLinha && c.produtoFornecedor == fornecedor
                             select
                                 new
                                 {
                                     c.produtoId,
                                     produto = c.produtoNome,
                                     ativo = c.produtoAtivo == "True" ? "Sim" : "Nao",
                                     c.produtoPaiId,
                                     estoqueReal = "",
                                     c.produtoMarca
                                 }).ToList();

                if (ddlMarca.SelectedValue != "0")
                {
                    int marca = Convert.ToInt32(ddlMarca.SelectedValue);
                    dados = dados.Where(x => x.produtoMarca == marca).ToList();
                }

                int categoria = Convert.ToInt32(ddlCategoria.SelectedValue == "" ? "0" : ddlCategoria.SelectedValue);

                if (categoria > 0)
                    dados = (from c in dados
                             join cat in data.tbJuncaoProdutoCategorias on c.produtoId equals cat.produtoId
                             where cat.categoriaId == categoria
                             select c).ToList();


                grd.DataSource = dados;
            }
            grd.DataBind();
            grd.Visible = true;

        }

        grd.Columns[2].Visible = true;
        grd.Columns[3].Visible = (lblTituloDetalhamento.Text == Titulos(7) | lblTituloDetalhamento.Text == Titulos(8) | lblTituloDetalhamento.Text == Titulos(11));
        grd.Columns[4].Visible = (lblTituloDetalhamento.Text == Titulos(9));
        grd.Columns[5].Visible = true;
    }

    /// <summary>
    /// Produtos fora de linha com estoque 0
    /// </summary>
    protected void FillDetalharFLE0()
    {
        lblTituloDetalhamento.Text = Titulos(1);
        lblTituloDetalhamento.Visible = true;

        using (var data = new dbCommerceDataContext())
        {
            int fornecedor = Convert.ToInt32(ddlFornecedor.SelectedValue);

            if (fornecedor == 0)
            {
                grd.DataSource = (from c in data.tbProdutos
                                  where c.foraDeLinha && c.estoqueReal == 0
                                  select new
                                  {
                                      c.produtoId,
                                      produto = c.produtoNome,
                                      ativo = c.produtoAtivo == "True" ? "Sim" : "Nao",
                                      c.produtoPaiId,
                                      estoqueReal = ""
                                  }).ToList();
            }
            else
            {
                var dados = (from c in data.tbProdutos where c.foraDeLinha && c.estoqueReal == 0 && c.produtoFornecedor == fornecedor select new { c.produtoId, produto = c.produtoNome, ativo = c.produtoAtivo == "True" ? "Sim" : "Nao", c.produtoPaiId, estoqueReal = "", c.produtoMarca }).ToList();


                if (ddlMarca.SelectedValue != "0")
                {
                    int marca = Convert.ToInt32(ddlMarca.SelectedValue);
                    dados = dados.Where(x => x.produtoMarca == marca).ToList();
                }

                int categoria = Convert.ToInt32(ddlCategoria.SelectedValue == "" ? "0" : ddlCategoria.SelectedValue);

                if (categoria > 0)
                    dados = (from c in dados
                             join cat in data.tbJuncaoProdutoCategorias on c.produtoId equals cat.produtoId
                             where cat.categoriaId == categoria
                             select c).ToList();

                grd.DataSource = dados;
            }
            grd.DataBind();
            grd.Visible = true;
        }

        grd.Columns[2].Visible = true;
        grd.Columns[3].Visible = (lblTituloDetalhamento.Text == Titulos(7) | lblTituloDetalhamento.Text == Titulos(8) | lblTituloDetalhamento.Text == Titulos(11));
        grd.Columns[4].Visible = (lblTituloDetalhamento.Text == Titulos(9));
        grd.Columns[5].Visible = true;
    }

    /// <summary>
    /// Produtos fora de linha com estoque baixo
    /// </summary>
    protected void FillDetalharFLEB()
    {
        lblTituloDetalhamento.Text = Titulos(2);
        lblTituloDetalhamento.Visible = true;

        using (var data = new dbCommerceDataContext())
        {
            int fornecedor = Convert.ToInt32(ddlFornecedor.SelectedValue);

            if (fornecedor == 0)
            {
                grd.DataSource = (from c in data.tbProdutos
                                  where c.foraDeLinha && (c.estoqueReal > 0 && c.estoqueReal < 11)
                                  select new
                                  {
                                      c.produtoId,
                                      produto = c.produtoNome + " - estoque: " + c.estoqueReal,
                                      ativo = c.produtoAtivo == "True" ? "Sim" : "Nao",
                                      c.produtoPaiId,
                                      estoqueReal = ""
                                  }).ToList();

            }
            else
            {
                var dados = (from c in data.tbProdutos where c.foraDeLinha && (c.estoqueReal > 0 && c.estoqueReal < 11) && c.produtoFornecedor == fornecedor select new { c.produtoId, produto = c.produtoNome + " - estoque: " + c.estoqueReal, ativo = c.produtoAtivo == "True" ? "Sim" : "Nao", c.produtoPaiId, estoqueReal = "", c.produtoMarca }).ToList();

                if (ddlMarca.SelectedValue != "0")
                {
                    int marca = Convert.ToInt32(ddlMarca.SelectedValue);
                    dados = dados.Where(x => x.produtoMarca == marca).ToList();
                }

                int categoria = Convert.ToInt32(ddlCategoria.SelectedValue == "" ? "0" : ddlCategoria.SelectedValue);

                if (categoria > 0)
                    dados = (from c in dados
                             join cat in data.tbJuncaoProdutoCategorias on c.produtoId equals cat.produtoId
                             where cat.categoriaId == categoria
                             select c).ToList();

                grd.DataSource = dados;

            }
            grd.DataBind();
            grd.Visible = true;
        }

        grd.Columns[2].Visible = true;
        grd.Columns[3].Visible = (lblTituloDetalhamento.Text == Titulos(7) | lblTituloDetalhamento.Text == Titulos(8) | lblTituloDetalhamento.Text == Titulos(11));
        grd.Columns[4].Visible = (lblTituloDetalhamento.Text == Titulos(9));
        grd.Columns[5].Visible = true;
    }

    /// <summary>
    /// Produtos com estoque baixo
    /// </summary>
    protected void FillDetalharEB()
    {
        lblTituloDetalhamento.Text = Titulos(3);
        lblTituloDetalhamento.Visible = true;

        using (var data = new dbCommerceDataContext())
        {
            int fornecedor = Convert.ToInt32(ddlFornecedor.SelectedValue);

            if (fornecedor == 0)
            {
                grd.DataSource = (from c in data.tbProdutos
                                  where c.produtoEstoqueAtual > 0 && c.produtoEstoqueAtual < 11
                                  select new
                                  {
                                      c.produtoId,
                                      produto = c.produtoNome + " - estoque: " + c.estoqueReal,
                                      ativo = c.produtoAtivo == "True" ? "Sim" : "Nao",
                                      c.produtoPaiId,
                                      estoqueReal = ""
                                  }).ToList();




            }
            else
            {
                var dados = (from c in data.tbProdutos
                             where c.estoqueReal > 0 && c.estoqueReal < 11 && c.produtoFornecedor == fornecedor
                             select new
                             {
                                 c.produtoId,
                                 produto = c.produtoNome + " - estoque: " + c.estoqueReal,
                                 ativo = c.produtoAtivo == "True" ? "Sim" : "Nao",
                                 c.produtoPaiId,
                                 estoqueReal = "",
                                 c.produtoMarca
                             }).ToList();

                if (ddlMarca.SelectedValue != "0" && ddlMarca.SelectedValue != "")
                {
                    int marca = Convert.ToInt32(ddlMarca.SelectedValue);
                    dados = dados.Where(x => x.produtoMarca == marca).ToList();
                }

                int categoria = Convert.ToInt32(ddlCategoria.SelectedValue == "" ? "0" : ddlCategoria.SelectedValue);

                if (categoria > 0)
                    dados = (from c in dados
                             join cat in data.tbJuncaoProdutoCategorias on c.produtoId equals cat.produtoId
                             where cat.categoriaId == categoria
                             select c).ToList();

                grd.DataSource = dados;
            }

            grd.DataBind();
            grd.Visible = true;
        }

        grd.Columns[2].Visible = true;
        grd.Columns[3].Visible = (lblTituloDetalhamento.Text == Titulos(7) | lblTituloDetalhamento.Text == Titulos(8) | lblTituloDetalhamento.Text == Titulos(11));
        grd.Columns[4].Visible = (lblTituloDetalhamento.Text == Titulos(9));
        grd.Columns[5].Visible = true;
    }

    /// <summary>
    /// Produtos com estoque 0 ou negativo
    /// </summary>
    protected void FillDetalharE0N()
    {
        lblTituloDetalhamento.Text = Titulos(18);
        lblTituloDetalhamento.Visible = true;

        using (var data = new dbCommerceDataContext())
        {
            int fornecedor = Convert.ToInt32(ddlFornecedor.SelectedValue);

            if (fornecedor == 0)
            {
                grd.DataSource = (from c in data.tbProdutos
                                  where c.produtoEstoqueAtual <= 0
                                  select new
                                  {
                                      c.produtoId,
                                      produto = c.produtoNome + " - estoque: " + c.estoqueReal,
                                      ativo = c.produtoAtivo == "True" ? "Sim" : "Nao",
                                      c.produtoPaiId,
                                      estoqueReal = ""
                                  }).ToList();

            }
            else
            {
                var dados = (from c in data.tbProdutos
                             where c.produtoEstoqueAtual <= 0 && c.produtoFornecedor == fornecedor
                             select new
                             {
                                 c.produtoId,
                                 produto = c.produtoNome + " - estoque: " + c.estoqueReal,
                                 ativo = c.produtoAtivo == "True" ? "Sim" : "Nao",
                                 c.produtoPaiId,
                                 estoqueReal = "",
                                 c.produtoMarca
                             }).ToList();

                if (ddlMarca.SelectedValue != "0")
                {
                    int marca = Convert.ToInt32(ddlMarca.SelectedValue);
                    dados = dados.Where(x => x.produtoMarca == marca).ToList();
                }

                int categoria = Convert.ToInt32(ddlCategoria.SelectedValue == "" ? "0" : ddlCategoria.SelectedValue);

                if (categoria > 0)
                    dados = (from c in dados
                             join cat in data.tbJuncaoProdutoCategorias on c.produtoId equals cat.produtoId
                             where cat.categoriaId == categoria
                             select c).ToList();

                grd.DataSource = dados;
            }

            grd.DataBind();
            grd.Visible = true;
        }

        grd.Columns[2].Visible = true;
        grd.Columns[3].Visible = (lblTituloDetalhamento.Text == Titulos(7) | lblTituloDetalhamento.Text == Titulos(8) | lblTituloDetalhamento.Text == Titulos(11));
        grd.Columns[4].Visible = (lblTituloDetalhamento.Text == Titulos(9));
        grd.Columns[5].Visible = true;
    }

    /// <summary>
    /// Produtos ativos sem categoria
    /// </summary>
    protected void FillDetalharPASC()
    {
        lblTituloDetalhamento.Text = Titulos(4);
        lblTituloDetalhamento.Visible = true;

        using (var data = new dbCommerceDataContext())
        {
            int fornecedor = Convert.ToInt32(ddlFornecedor.SelectedValue);

            if (fornecedor == 0)
            {
                grd.DataSource = (from c in data.tbProdutos
                                  where c.produtoAtivo == "True"
                                  join cat in data.tbJuncaoProdutoCategorias on c.produtoId equals cat.produtoId into psc
                                  from cat in psc.DefaultIfEmpty()
                                  where c.produtoId == null
                                  select new { c.produtoId, produto = c.produtoNome, ativo = c.produtoAtivo == "True" ? "Sim" : "Nao", c.produtoPaiId, estoqueReal = "" }).ToList();
            }
            else
            {
                var dados = (from c in data.tbProdutos
                             where c.produtoAtivo == "True" && c.produtoFornecedor == fornecedor
                             join cat in data.tbJuncaoProdutoCategorias on c.produtoId equals cat.produtoId into psc
                             from cat in psc.DefaultIfEmpty()
                             where c.produtoId == null
                             select new { c.produtoId, produto = c.produtoNome, ativo = c.produtoAtivo == "True" ? "Sim" : "Nao", c.produtoPaiId, estoqueReal = "", c.produtoMarca }).ToList();

                if (ddlMarca.SelectedValue != "0")
                {
                    int marca = Convert.ToInt32(ddlMarca.SelectedValue);
                    dados = dados.Where(x => x.produtoMarca == marca).ToList();
                }

                int categoria = Convert.ToInt32(ddlCategoria.SelectedValue == "" ? "0" : ddlCategoria.SelectedValue);

                if (categoria > 0)
                    dados = (from c in dados
                             join cat in data.tbJuncaoProdutoCategorias on c.produtoId equals cat.produtoId
                             where cat.categoriaId == categoria
                             select c).ToList();

                grd.DataSource = dados;
            }

            grd.DataBind();
            grd.Visible = true;

        }


        grd.Columns[2].Visible = true;
        grd.Columns[3].Visible = (lblTituloDetalhamento.Text == Titulos(7) | lblTituloDetalhamento.Text == Titulos(8) | lblTituloDetalhamento.Text == Titulos(11));
        grd.Columns[4].Visible = (lblTituloDetalhamento.Text == Titulos(9));
        grd.Columns[5].Visible = true;
    }

    /// <summary>
    /// Caegorias ativas sem produtos
    /// </summary>
    protected void FillDetalharCASP()
    {
        lblTituloDetalhamento.Text = Titulos(5);
        lblTituloDetalhamento.Visible = true;

        using (var data = new dbCommerceDataContext())
        {
            int fornecedor = Convert.ToInt32(ddlFornecedor.SelectedValue);

            if (fornecedor == 0)
            {
                grd.DataSource = (from c in data.tbProdutos
                                  where c.produtoAtivo == "True"
                                  join cat in data.tbJuncaoProdutoCategorias on c.produtoId equals cat.produtoId into psc
                                  from cat in psc.DefaultIfEmpty()
                                  where !psc.Select(x => x.produtoId == null && (x.tbProdutoCategoria.exibirSite ?? false)).Any()
                                  select new { c.produtoId, produto = c.produtoNome, ativo = c.produtoAtivo == "True" ? "Sim" : "Nao", c.produtoPaiId, estoqueReal = "" }).ToList();
            }
            else
            {
                var dados = (from c in data.tbProdutos
                             where c.produtoAtivo == "True" && c.produtoFornecedor == fornecedor
                             join cat in data.tbJuncaoProdutoCategorias on c.produtoId equals cat.produtoId into psc
                             from cat in psc.DefaultIfEmpty()
                             where !psc.Select(x => x.produtoId == null && (x.tbProdutoCategoria.exibirSite ?? false)).Any()
                             select new { c.produtoId, produto = c.produtoNome, ativo = c.produtoAtivo == "True" ? "Sim" : "Nao", c.produtoPaiId, estoqueReal = "", c.produtoMarca }).ToList();

                if (ddlMarca.SelectedValue != "0")
                {
                    int marca = Convert.ToInt32(ddlMarca.SelectedValue);
                    dados = dados.Where(x => x.produtoMarca == marca).ToList();
                }

                int categoria = Convert.ToInt32(ddlCategoria.SelectedValue == "" ? "0" : ddlCategoria.SelectedValue);

                if (categoria > 0)
                    dados = (from c in dados
                             join cat in data.tbJuncaoProdutoCategorias on c.produtoId equals cat.produtoId
                             where cat.categoriaId == categoria
                             select c).ToList();

                grd.DataSource = dados;
            }

            grd.DataBind();
            grd.Visible = true;

        }


        grd.Columns[2].Visible = true;
        grd.Columns[3].Visible = (lblTituloDetalhamento.Text == Titulos(7) | lblTituloDetalhamento.Text == Titulos(8) | lblTituloDetalhamento.Text == Titulos(11));
        grd.Columns[4].Visible = (lblTituloDetalhamento.Text == Titulos(9));
        grd.Columns[5].Visible = true;
    }

    /// <summary>
    /// Caegorias ativas sem descrição
    /// </summary>
    protected void FillDetalharCASD()
    {
        lblTituloDetalhamento.Text = Titulos(17);
        lblTituloDetalhamento.Visible = true;

        using (var data = new dbCommerceDataContext())
        {

            grd.DataSource = (from c in data.tbProdutoCategorias
                              where c.exibirSite == true && (c.categoriaDescricao ?? "") == ""
                              select new
                              {
                                  produtoId = c.categoriaId,
                                  categoria = c.categoriaNome,
                                  produto = "",
                                  ativo = "",
                                  produtoPaiId = ""
                              }).ToList();

            grd.DataBind();
            grd.Visible = true;

        }


        grd.Columns[2].Visible = false;
        grd.Columns[3].Visible = true;
        grd.Columns[4].Visible = (lblTituloDetalhamento.Text == Titulos(9));
        grd.Columns[5].Visible = false;
    }

    /// <summary>
    /// Produtos ativos sem foto
    /// </summary>
    protected void FillDetalharPASF()
    {
        lblTituloDetalhamento.Text = Titulos(6);
        lblTituloDetalhamento.Visible = true;

        using (var data = new dbCommerceDataContext())
        {
            int fornecedor = Convert.ToInt32(ddlFornecedor.SelectedValue);

            if (fornecedor == 0)
            {
                grd.DataSource = (from c in data.tbProdutos
                                  where c.produtoAtivo == "True"
                                  join pf in data.tbProdutoFotos on c.produtoId equals pf.produtoId into psf
                                  from pf in psf.DefaultIfEmpty()
                                  where !psf.Select(x => x.produtoId).Any()
                                  select new { c.produtoId, produto = c.produtoNome, ativo = c.produtoAtivo == "True" ? "Sim" : "Nao", c.produtoPaiId, estoqueReal = "" }).ToList();
            }
            else
            {
                var dados = (from c in data.tbProdutos
                             where c.produtoAtivo == "True" && c.produtoFornecedor == fornecedor
                             join pf in data.tbProdutoFotos on c.produtoId equals pf.produtoId into psf
                             from pf in psf.DefaultIfEmpty()
                             where !psf.Select(x => x.produtoId).Any()
                             select new { c.produtoId, produto = c.produtoNome, ativo = c.produtoAtivo == "True" ? "Sim" : "Nao", c.produtoPaiId, estoqueReal = "", c.produtoMarca }).ToList();

                if (ddlMarca.SelectedValue != "0")
                {
                    int marca = Convert.ToInt32(ddlMarca.SelectedValue);
                    dados = dados.Where(x => x.produtoMarca == marca).ToList();
                }

                int categoria = Convert.ToInt32(ddlCategoria.SelectedValue == "" ? "0" : ddlCategoria.SelectedValue);

                if (categoria > 0)
                    dados = (from c in dados
                             join cat in data.tbJuncaoProdutoCategorias on c.produtoId equals cat.produtoId
                             where cat.categoriaId == categoria
                             select c).ToList();

                grd.DataSource = dados;
            }

            grd.DataBind();
            grd.Visible = true;

        }


        grd.Columns[2].Visible = true;
        grd.Columns[3].Visible = (lblTituloDetalhamento.Text == Titulos(7) | lblTituloDetalhamento.Text == Titulos(8) | lblTituloDetalhamento.Text == Titulos(11));
        grd.Columns[4].Visible = (lblTituloDetalhamento.Text == Titulos(9));
        grd.Columns[5].Visible = true;
    }

    /// <summary>
    /// Produtos ativos com margem de lucro menor que 133 - não combo
    /// </summary>
    protected void FillDetalharPAMN133()
    {
        lblTituloDetalhamento.Text = Titulos(7);
        lblTituloDetalhamento.Visible = true;

        using (var data = new dbCommerceDataContext())
        {
            int fornecedor = Convert.ToInt32(ddlFornecedor.SelectedValue);

            if (fornecedor == 0)
            {
                grd.DataSource = (from c in data.tbProdutos
                                  where c.produtoAtivo == "True" && c.margemDeLucro < 132
                                  join pr in data.tbProdutoRelacionados on c.produtoId equals pr.idProdutoPai into pamn133
                                  from pr in pamn133.DefaultIfEmpty()
                                  where !pamn133.Select(x => x.idProdutoPai).Any()
                                  select new
                                  {
                                      c.produtoId,
                                      produto = c.produtoNome + " - margem: " + c.margemDeLucro,
                                      ativo = c.produtoAtivo == "True" ? "Sim" : "Nao",
                                      c.produtoPaiId,
                                      estoqueReal = ""
                                  }).ToList();
            }
            else
            {
                var dados = (from c in data.tbProdutos
                             where c.produtoAtivo == "True" && c.margemDeLucro < 132 && c.produtoFornecedor == fornecedor
                             join pr in data.tbProdutoRelacionados on c.produtoId equals pr.idProdutoPai into pamn133
                             from pr in pamn133.DefaultIfEmpty()
                             where !pamn133.Select(x => x.idProdutoPai).Any()
                             select new
                             {
                                 c.produtoId,
                                 produto = c.produtoNome + " - margem: " + c.margemDeLucro,
                                 ativo = c.produtoAtivo == "True" ? "Sim" : "Nao",
                                 c.produtoPaiId,
                                 estoqueReal = "",
                                 c.produtoMarca
                             }).ToList();

                if (ddlMarca.SelectedValue != "0")
                {
                    int marca = Convert.ToInt32(ddlMarca.SelectedValue);
                    dados = dados.Where(x => x.produtoMarca == marca).ToList();
                }

                int categoria = Convert.ToInt32(ddlCategoria.SelectedValue == "" ? "0" : ddlCategoria.SelectedValue);

                if (categoria > 0)
                    dados = (from c in dados
                             join cat in data.tbJuncaoProdutoCategorias on c.produtoId equals cat.produtoId
                             where cat.categoriaId == categoria
                             select c).ToList();

                grd.DataSource = dados;
            }

            grd.DataBind();
            grd.Visible = true;

        }


        grd.Columns[2].Visible = true;
        grd.Columns[3].Visible = (lblTituloDetalhamento.Text == Titulos(7) | lblTituloDetalhamento.Text == Titulos(8) | lblTituloDetalhamento.Text == Titulos(11));
        grd.Columns[4].Visible = (lblTituloDetalhamento.Text == Titulos(9));
        grd.Columns[5].Visible = true;
    }

    /// <summary>
    /// Produtos ativos com margem menor que 122 - produto combo
    /// </summary>
    protected void FillDetalharCMN122()
    {
        lblTituloDetalhamento.Text = Titulos(8);
        lblTituloDetalhamento.Visible = true;

        using (var data = new dbCommerceDataContext())
        {
            int fornecedor = Convert.ToInt32(ddlFornecedor.SelectedValue);

            if (fornecedor == 0)
            {
                grd.DataSource = (from c in data.tbProdutos
                                  where c.produtoAtivo == "True" && c.margemDeLucro < 121
                                  join pr in data.tbProdutoRelacionados on c.produtoId equals pr.idProdutoPai
                                  group c by new
                                  {
                                      c.produtoId,
                                      produto = c.produtoNome + " - margem: " + c.margemDeLucro,
                                      ativo = c.produtoAtivo == "True" ? "Sim" : "Nao",
                                      c.produtoPaiId,
                                      estoqueReal = ""
                                  } into g
                                  select new
                                  {
                                      g.Key.produtoId,
                                      g.Key.produto,
                                      g.Key.ativo,
                                      g.Key.produtoPaiId,
                                      g.Key.estoqueReal
                                  }).ToList();
            }
            else
            {
                var dados = (from c in data.tbProdutos
                             where c.produtoAtivo == "True" && c.margemDeLucro < 121 && c.produtoFornecedor == fornecedor
                             join pr in data.tbProdutoRelacionados on c.produtoId equals pr.idProdutoPai
                             group c by new
                             {
                                 c.produtoId,
                                 produto = c.produtoNome + " - margem: " + c.margemDeLucro,
                                 ativo = c.produtoAtivo == "True" ? "Sim" : "Nao",
                                 c.produtoPaiId,
                                 estoqueReal = "",
                                 c.produtoMarca
                             } into g
                             select new
                             {
                                 g.Key.produtoId,
                                 g.Key.produto,
                                 g.Key.ativo,
                                 g.Key.produtoPaiId,
                                 g.Key.estoqueReal,
                                 g.Key.produtoMarca
                             }).ToList();

                if (ddlMarca.SelectedValue != "0")
                {
                    int marca = Convert.ToInt32(ddlMarca.SelectedValue);
                    dados = dados.Where(x => x.produtoMarca == marca).ToList();
                }

                int categoria = Convert.ToInt32(ddlCategoria.SelectedValue == "" ? "0" : ddlCategoria.SelectedValue);

                if (categoria > 0)
                    dados = (from c in dados
                             join cat in data.tbJuncaoProdutoCategorias on c.produtoId equals cat.produtoId
                             where cat.categoriaId == categoria
                             select c).ToList();

                grd.DataSource = dados;
            }



            grd.DataBind();
            grd.Visible = true;

        }


        grd.Columns[2].Visible = true;
        grd.Columns[3].Visible = (lblTituloDetalhamento.Text == Titulos(7) | lblTituloDetalhamento.Text == Titulos(8) | lblTituloDetalhamento.Text == Titulos(11));
        grd.Columns[4].Visible = (lblTituloDetalhamento.Text == Titulos(9));
        grd.Columns[5].Visible = true;
    }

    /// <summary>
    /// Produtos desativados com estoque maior que 0
    /// </summary>
    protected void FillDetalharPDEM0()
    {
        lblTituloDetalhamento.Text = Titulos(9);
        lblTituloDetalhamento.Visible = true;

        using (var data = new dbCommerceDataContext())
        {

            using (
                var txn = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required,
                    new System.Transactions.TransactionOptions
                    {
                        IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                    }))

            {


                int fornecedor = Convert.ToInt32(ddlFornecedor.SelectedValue);

                if (fornecedor == 0)
                {
                    //var listaProdutoCombo = (from c in data.tbProdutos
                    //                         join d in data.tbProdutoRelacionados on c.produtoId equals d.idProdutoFilho
                    //                         join pe in data.tbProdutoEstoques on c.produtoId equals pe.produtoId
                    //                         where
                    //                             c.produtoAtivo == "False" && c.estoqueReal > 0 && !pe.enviado && pe.itemPedidoIdReserva == null
                    //                         select new
                    //                         {
                    //                             c.produtoId,
                    //                             produto = c.produtoNome,
                    //                             ativo = c.produtoAtivo == "True" ? "Sim" : "Nao",
                    //                             c.produtoPaiId,
                    //                             c.estoqueReal
                    //                         }).Distinct().ToList();

                    //var produtoPaiDoCombo = (from pc in listaProdutoCombo
                    //                         join c in data.tbProdutos on pc.produtoPaiId equals c.produtoId
                    //                         where c.produtoAtivo == "False"
                    //                         select new
                    //                         {
                    //                             c.produtoId,
                    //                             produto = c.produtoNome,
                    //                             ativo = c.produtoAtivo == "True" ? "Sim" : "Nao",
                    //                             c.produtoPaiId,
                    //                             c.estoqueReal
                    //                         }).Distinct().ToList();

                    //var listaProdutoNaoCombo = (from c in data.tbProdutos
                    //                            join d in data.tbProdutoRelacionados on c.produtoId equals d.idProdutoFilho into nc
                    //                            join pe in data.tbProdutoEstoques on c.produtoId equals pe.produtoId
                    //                            from d in nc.DefaultIfEmpty()
                    //                            where
                    //                                !nc.Select(x => x.idProdutoFilho).Any() && c.produtoAtivo == "False" && c.estoqueReal > 0 && !pe.enviado && pe.itemPedidoIdReserva == null
                    //                            select new
                    //                            {
                    //                                c.produtoId,
                    //                                produto = c.produtoNome,
                    //                                ativo = c.produtoAtivo == "True" ? "Sim" : "Nao",
                    //                                c.produtoPaiId,
                    //                                c.estoqueReal
                    //                            }).Distinct().ToList();

                    //var listaFinal = produtoPaiDoCombo.Union(listaProdutoNaoCombo).ToList();

                    var listaEstoque = (from c in data.tbProdutos
                                        join pe in data.tbProdutoEstoques on c.produtoId equals pe.produtoId
                                        where !pe.enviado && pe.itemPedidoIdReserva == null
                                        select new
                                        {
                                            pe.produtoId,
                                            pe.enviado,
                                            pe.itemPedidoIdReserva,
                                            c.produtoFornecedor,
                                            c.produtoAtivo,
                                            c.estoqueReal,
                                            c.produtoPaiId,
                                            c.produtoNome,
                                            etiqueta = pe.idPedidoFornecedorItem
                                        }).ToList();

                    var listaProdutoCombo = (from pe in listaEstoque
                                             join d in data.tbProdutoRelacionados on pe.produtoId equals d.idProdutoFilho
                                             where
                                                 pe.produtoAtivo == "False" && pe.estoqueReal > 0
                                             select new
                                             {
                                                 pe.produtoId,
                                                 produto = pe.produtoNome,
                                                 ativo = pe.produtoAtivo,
                                                 pe.produtoPaiId,
                                                 pe.estoqueReal,
                                                 pe.etiqueta
                                             }).Distinct().ToList();

                    var produtoPaiDoCombo = (from pc in listaProdutoCombo
                                             join c in data.tbProdutoRelacionados on pc.produtoId equals c.idProdutoPai
                                             where c.tbProduto.produtoAtivo == "False"
                                             select new
                                             {
                                                 c.tbProduto.produtoId,
                                                 produto = c.tbProduto.produtoNome,
                                                 ativo = c.tbProduto.produtoAtivo,
                                                 c.tbProduto.estoqueReal,
                                                 c.tbProduto.produtoPaiId,
                                                 pc.etiqueta
                                             }).Distinct().ToList();

                    var listaProdutoNaoCombo = (from pe in listaEstoque
                                                join d in data.tbProdutoRelacionados on pe.produtoId equals d.idProdutoFilho into nc
                                                from d in nc.DefaultIfEmpty()
                                                where
                                                    !nc.Select(x => x.idProdutoFilho).Any() && pe.produtoAtivo == "False" && pe.estoqueReal > 0
                                                select new
                                                {
                                                    pe.produtoId,
                                                    produto = pe.produtoNome,
                                                    ativo = pe.produtoAtivo,
                                                    pe.estoqueReal,
                                                    pe.produtoPaiId,
                                                    pe.etiqueta
                                                }).Distinct().ToList();

                    var listaFinal = produtoPaiDoCombo.Union(listaProdutoNaoCombo).OrderBy(x => x.produto).ToList();

                    grd.DataSource = listaFinal;
                }
                else
                {
                    //var listaProdutoCombo = (from c in data.tbProdutos
                    //                         join d in data.tbProdutoRelacionados on c.produtoId equals d.idProdutoFilho
                    //                         join pe in data.tbProdutoEstoques on c.produtoId equals pe.produtoId
                    //                         where
                    //                             c.produtoAtivo == "False" && c.estoqueReal > 0 && !pe.enviado && pe.itemPedidoIdReserva == null
                    //                         select new
                    //                         {
                    //                             c.produtoId,
                    //                             produto = c.produtoNome,
                    //                             ativo = c.produtoAtivo == "True" ? "Sim" : "Nao",
                    //                             c.produtoPaiId,
                    //                             c.estoqueReal,
                    //                             c.produtoFornecedor
                    //                         }).Distinct().ToList();

                    //if (fornecedor > 0)
                    //    listaProdutoCombo = listaProdutoCombo.Where(x => x.produtoFornecedor == fornecedor).Distinct().ToList();


                    //int categoria = Convert.ToInt32(ddlCategoria.SelectedValue);

                    //if (categoria > 0)
                    //    listaProdutoCombo = (from c in listaProdutoCombo
                    //                         join cat in data.tbJuncaoProdutoCategorias on c.produtoId equals cat.produtoId
                    //                         where cat.categoriaId == categoria
                    //                         select c).Distinct().ToList();

                    //var produtoPaiDoCombo = (from pc in listaProdutoCombo
                    //                         join c in data.tbProdutos on pc.produtoPaiId equals c.produtoId
                    //                         where c.produtoAtivo == "False"
                    //                         select new
                    //                         {
                    //                             c.produtoId,
                    //                             produto = c.produtoNome,
                    //                             ativo = c.produtoAtivo == "True" ? "Sim" : "Nao",
                    //                             c.produtoPaiId,
                    //                             c.estoqueReal,
                    //                             c.produtoFornecedor
                    //                         }).Distinct().ToList();

                    //var listaProdutoNaoCombo = (from c in data.tbProdutos
                    //                            join d in data.tbProdutoRelacionados on c.produtoId equals d.idProdutoFilho into nc
                    //                            join pe in data.tbProdutoEstoques on c.produtoId equals pe.produtoId
                    //                            from d in nc.DefaultIfEmpty()
                    //                            where
                    //                                !nc.Select(x => x.idProdutoFilho).Any() && c.produtoAtivo == "False" && c.produtoFornecedor == fornecedor &&
                    //                                c.estoqueReal > 0 && c.produtoFornecedor == fornecedor && !pe.enviado && pe.itemPedidoIdReserva == null
                    //                            select new
                    //                            {
                    //                                c.produtoId,
                    //                                produto = c.produtoNome,
                    //                                ativo = c.produtoAtivo == "True" ? "Sim" : "Nao",
                    //                                c.produtoPaiId,
                    //                                c.estoqueReal,
                    //                                c.produtoFornecedor
                    //                            }).Distinct().ToList();

                    //if (fornecedor > 0)
                    //    listaProdutoNaoCombo = listaProdutoNaoCombo.Where(x => x.produtoFornecedor == fornecedor).Distinct().ToList();

                    //if (categoria > 0)
                    //    listaProdutoNaoCombo = (from c in listaProdutoNaoCombo
                    //                            join cat in data.tbJuncaoProdutoCategorias on c.produtoId equals cat.produtoId
                    //                            where cat.categoriaId == categoria
                    //                            select c).Distinct().ToList();

                    //var listaFinal = produtoPaiDoCombo.Union(listaProdutoNaoCombo).ToList();

                    var listaEstoque = (from c in data.tbProdutos
                                        join pe in data.tbProdutoEstoques on c.produtoId equals pe.produtoId
                                        where !pe.enviado && pe.itemPedidoIdReserva == null
                                        select new { pe.produtoId, pe.enviado, pe.itemPedidoIdReserva, c.produtoFornecedor, c.produtoAtivo, c.estoqueReal, c.produtoPaiId, c.produtoNome }).ToList();

                    if (fornecedor > 0)
                        listaEstoque = listaEstoque.Where(x => x.produtoFornecedor == fornecedor).ToList();

                    int categoria = Convert.ToInt32((ddlCategoria.SelectedValue == "" ? "0" : ddlCategoria.SelectedValue));

                    if (categoria > 0)
                        listaEstoque = (from c in listaEstoque
                                        join cat in data.tbJuncaoProdutoCategorias on c.produtoId equals cat.produtoId
                                        where cat.categoriaId == categoria
                                        select c).Distinct().ToList();

                    var listaProdutoCombo = (from pe in listaEstoque
                                             join d in data.tbProdutoRelacionados on pe.produtoId equals d.idProdutoFilho
                                             where
                                                 pe.produtoAtivo == "False" && pe.estoqueReal > 0
                                             select new
                                             {
                                                 pe.produtoId,
                                                 produto = pe.produtoNome,
                                                 ativo = pe.produtoAtivo,
                                                 pe.produtoPaiId,
                                                 pe.estoqueReal
                                             }).Distinct().ToList();

                    var produtoPaiDoCombo = (from pc in listaProdutoCombo
                                             join c in data.tbProdutoRelacionados on pc.produtoId equals c.idProdutoPai
                                             where c.tbProduto.produtoAtivo == "False"
                                             select new
                                             {
                                                 c.tbProduto.produtoId,
                                                 produto = c.tbProduto.produtoNome,
                                                 ativo = c.tbProduto.produtoAtivo,
                                                 c.tbProduto.estoqueReal,
                                                 pc.produtoPaiId
                                             }).Distinct().ToList();

                    var listaProdutoNaoCombo = (from pe in listaEstoque
                                                join d in data.tbProdutoRelacionados on pe.produtoId equals d.idProdutoFilho into nc
                                                from d in nc.DefaultIfEmpty()
                                                where
                                                    !nc.Select(x => x.idProdutoFilho).Any() && pe.produtoAtivo == "False" && pe.estoqueReal > 0
                                                select new
                                                {
                                                    pe.produtoId,
                                                    produto = pe.produtoNome,
                                                    ativo = pe.produtoAtivo,
                                                    pe.estoqueReal,
                                                    pe.produtoPaiId
                                                }).Distinct().ToList();

                    var listaFinal = produtoPaiDoCombo.Union(listaProdutoNaoCombo).ToList();

                    grd.DataSource = listaFinal;


                }



                //var listaPrimeiraVerificacao = (from c in data.tbProdutos
                //                                join pe in data.tbProdutoEstoques on c.produtoId equals pe.produtoId
                //                                where
                //                                    c.produtoAtivo == "False" && c.estoqueReal > 0 && !pe.enviado &&
                //                                    pe.idItemPedidoEstoqueReserva == null
                //                                select new
                //                                {
                //                                    c.produtoId,
                //                                    produto = c.produtoNome,
                //                                    ativo = c.produtoAtivo == "True" ? "Sim" : "Nao",
                //                                    c.produtoPaiId,
                //                                    c.estoqueReal
                //                                }).Distinct().ToList();

                //var produtosCombo = (from c in listaPrimeiraVerificacao
                //                        join d in data.tbProdutoRelacionados on c.produtoId equals d.idProdutoPai into nc
                //                        from d in nc.DefaultIfEmpty()
                //                        where !nc.Select(x => x.idProdutoFilho).Any()
                //                        select new
                //                        {
                //                            c.produtoId,
                //                            c.produto,
                //                            c.ativo,
                //                            c.produtoPaiId,
                //                            c.estoqueReal
                //                        }).Distinct().ToList();



                //var listaSegundaVerificacao = (from ipe in data.tbItemPedidoEstoques
                //                               where !ipe.reservado && !ipe.cancelado
                //                               select new
                //                               {
                //                                   ipe.produtoId
                //                               }).Distinct().ToList();

                //grd.DataSource = (from c in produtosCombo
                //                  join ipe in listaSegundaVerificacao on c.produtoId equals ipe.produtoId into psp
                //                  from ipe in psp.DefaultIfEmpty()
                //                  where !psp.Select(x => x.produtoId).Any()
                //                  select new
                //                  {
                //                      c.produtoId,
                //                      c.produto,
                //                      c.ativo,
                //                      c.produtoPaiId,
                //                      c.estoqueReal
                //                  }).Distinct().ToList();
            }
            //grd.DataSource = (from c in listaPrimeiraVerificacao
            //                  join ipe in data.tbItemPedidoEstoques on c.produtoId equals ipe.produtoId into psp
            //                  from ipe in psp.DefaultIfEmpty()
            //                  where psp.Select(x => !x.reservado && !x.cancelado).Any()
            //                  select new
            //                  {
            //                      c.produtoId,
            //                      c.produto,
            //                      c.ativo,
            //                      c.produtoPaiId,
            //                      c.estoqueReal
            //                  }).Distinct().ToList();


            grd.DataBind();
            grd.Visible = true;

        }


        grd.Columns[2].Visible = true;
        grd.Columns[3].Visible = (lblTituloDetalhamento.Text == Titulos(7) | lblTituloDetalhamento.Text == Titulos(8) | lblTituloDetalhamento.Text == Titulos(11));
        grd.Columns[4].Visible = (lblTituloDetalhamento.Text == Titulos(9));
        grd.Columns[5].Visible = true;
    }

    /// <summary>
    /// Produtos ativos com estoque 0 e estoque minimo 0 (mostrando como esgotado)
    /// </summary>
    protected void FillDetalharPASE0()
    {
        lblTituloDetalhamento.Text = Titulos(10);
        lblTituloDetalhamento.Visible = true;

        using (var data = new dbCommerceDataContext())
        {
            int fornecedor = Convert.ToInt32(ddlFornecedor.SelectedValue);

            if (fornecedor == 0)
            {
                grd.DataSource = (from c in data.tbProdutos
                                  where c.produtoAtivo == "True" && (c.produtoEstoqueAtual <= 0 && (c.produtoEstoqueAtual <= c.produtoEstoqueMinimo))
                                  select new
                                  {
                                      c.produtoId,
                                      produto = c.produtoNome,
                                      ativo = c.produtoAtivo == "True" ? "Sim" : "Nao",
                                      c.produtoPaiId,
                                      c.estoqueReal
                                  }).Distinct().ToList();
            }
            else
            {
                var dados = (from c in data.tbProdutos
                             where c.produtoAtivo == "True" && (c.produtoEstoqueAtual <= 0 && (c.produtoEstoqueAtual <= c.produtoEstoqueMinimo)) && c.produtoFornecedor == fornecedor
                             select new
                             {
                                 c.produtoId,
                                 produto = c.produtoNome,
                                 ativo = c.produtoAtivo == "True" ? "Sim" : "Nao",
                                 c.produtoPaiId,
                                 c.estoqueReal,
                                 c.produtoMarca
                             }).Distinct().ToList();

                if (ddlMarca.SelectedValue != "0")
                {
                    int marca = Convert.ToInt32(ddlMarca.SelectedValue);
                    dados = dados.Where(x => x.produtoMarca == marca).ToList();
                }

                int categoria = Convert.ToInt32(ddlCategoria.SelectedValue == "" ? "0" : ddlCategoria.SelectedValue);

                if (categoria > 0)
                    dados = (from c in dados
                             join cat in data.tbJuncaoProdutoCategorias on c.produtoId equals cat.produtoId
                             where cat.categoriaId == categoria
                             select c).ToList();

                grd.DataSource = dados;
            }

            grd.DataBind();
            grd.Visible = true;

        }


        grd.Columns[2].Visible = true;
        grd.Columns[3].Visible = (lblTituloDetalhamento.Text == Titulos(7) | lblTituloDetalhamento.Text == Titulos(8) | lblTituloDetalhamento.Text == Titulos(11));
        grd.Columns[4].Visible = (lblTituloDetalhamento.Text == Titulos(9));
        grd.Columns[5].Visible = true;
    }

    /// <summary>
    /// Produtos ativos sem descrição
    /// </summary>
    protected void FillDetalharPASD()
    {
        lblTituloDetalhamento.Text = Titulos(12);
        lblTituloDetalhamento.Visible = true;

        using (var data = new dbCommerceDataContext())
        {

            int fornecedor = Convert.ToInt32(ddlFornecedor.SelectedValue);

            if (fornecedor == 0)
            {
                grd.DataSource = (from c in data.tbProdutos
                                  where c.produtoAtivo == "True" && (c.produtoDescricao ?? "") == ""
                                  select new
                                  {
                                      c.produtoId,
                                      produto = c.produtoNome,
                                      ativo = c.produtoAtivo == "True" ? "Sim" : "Nao",
                                      c.produtoPaiId,
                                      c.estoqueReal
                                  }).Distinct().ToList();
            }
            else
            {
                var dados = (from c in data.tbProdutos
                             where c.produtoAtivo == "True" && (c.produtoDescricao ?? "") == "" && c.produtoFornecedor == fornecedor
                             select new
                             {
                                 c.produtoId,
                                 produto = c.produtoNome,
                                 ativo = c.produtoAtivo == "True" ? "Sim" : "Nao",
                                 c.produtoPaiId,
                                 c.estoqueReal,
                                 c.produtoMarca
                             }).Distinct().ToList();

                if (ddlMarca.SelectedValue != "0" && ddlMarca.SelectedValue != "")
                {
                    int marca = Convert.ToInt32(ddlMarca.SelectedValue);
                    dados = dados.Where(x => x.produtoMarca == marca).ToList();
                }

                int categoria = Convert.ToInt32(ddlCategoria.SelectedValue == "" ? "0" : ddlCategoria.SelectedValue);

                if (categoria > 0)
                    dados = (from c in dados
                             join cat in data.tbJuncaoProdutoCategorias on c.produtoId equals cat.produtoId
                             where cat.categoriaId == categoria
                             select c).ToList();

                grd.DataSource = dados;
            }



            grd.DataBind();
            grd.Visible = true;

        }


        grd.Columns[2].Visible = true;
        grd.Columns[3].Visible = (lblTituloDetalhamento.Text == Titulos(7) | lblTituloDetalhamento.Text == Titulos(8) | lblTituloDetalhamento.Text == Titulos(11));
        grd.Columns[4].Visible = (lblTituloDetalhamento.Text == Titulos(9));
        grd.Columns[5].Visible = true;
    }


    /// <summary>
    /// Produtos ativos sem coleção
    /// </summary>
    protected void FillDetalharPASCO()
    {
        lblTituloDetalhamento.Text = Titulos(13);
        lblTituloDetalhamento.Visible = true;

        using (var data = new dbCommerceDataContext())
        {
            int fornecedor = Convert.ToInt32(ddlFornecedor.SelectedValue);

            if (fornecedor == 0)
            {

                grd.DataSource = (from c in data.tbProdutos
                                  join pc in data.tbJuncaoProdutoColecaos on c.produtoId equals pc.produtoId into asc
                                  from pc in asc.DefaultIfEmpty()
                                  where c.produtoAtivo == "True" && !asc.Select(x => x.produtoId).Any()
                                  select new
                                  {
                                      c.produtoId,
                                      produto = c.produtoNome,
                                      ativo = c.produtoAtivo == "True" ? "Sim" : "Nao",
                                      c.produtoPaiId,
                                      c.estoqueReal
                                  }).Distinct().ToList();
            }
            else
            {
                var dados = (from c in data.tbProdutos
                             join pc in data.tbJuncaoProdutoColecaos on c.produtoId equals pc.produtoId into asc
                             from pc in asc.DefaultIfEmpty()
                             where c.produtoAtivo == "True" && c.produtoFornecedor == fornecedor && !asc.Select(x => x.produtoId).Any()
                             select new
                             {
                                 c.produtoId,
                                 produto = c.produtoNome,
                                 ativo = c.produtoAtivo == "True" ? "Sim" : "Nao",
                                 c.produtoPaiId,
                                 c.estoqueReal,
                                 c.produtoMarca
                             }).Distinct().ToList();


                if (ddlMarca.SelectedValue != "0")
                {
                    int marca = Convert.ToInt32(ddlMarca.SelectedValue);
                    dados = dados.Where(x => x.produtoMarca == marca).ToList();
                }

                int categoria = Convert.ToInt32(ddlCategoria.SelectedValue == "" ? "0" : ddlCategoria.SelectedValue);

                if (categoria > 0)
                    dados = (from c in dados
                             join cat in data.tbJuncaoProdutoCategorias on c.produtoId equals cat.produtoId
                             where cat.categoriaId == categoria
                             select c).ToList();


                grd.DataSource = dados;
            }


            grd.DataBind();
            grd.Visible = true;

        }


        grd.Columns[2].Visible = true;
        grd.Columns[3].Visible = (lblTituloDetalhamento.Text == Titulos(7) | lblTituloDetalhamento.Text == Titulos(8) | lblTituloDetalhamento.Text == Titulos(11));
        grd.Columns[4].Visible = (lblTituloDetalhamento.Text == Titulos(9));
        grd.Columns[5].Visible = true;
    }

    /// <summary>
    /// Produtos ativos sem coleção
    /// </summary>
    protected void FillDetalharPASCOM()
    {
        lblTituloDetalhamento.Text = Titulos(14);
        lblTituloDetalhamento.Visible = true;

        using (var data = new dbCommerceDataContext())
        {
            int fornecedor = Convert.ToInt32(ddlFornecedor.SelectedValue);

            if (fornecedor == 0)
            {
                //grd.DataSource = (from c in data.tbProdutos
                //                  where c.produtoAtivo == "True" && (c.produtoComposicao ?? "") == ""
                //                  select new
                //                  {
                //                      c.produtoId,
                //                      produto = c.produtoNome,
                //                      ativo = c.produtoAtivo == "True" ? "Sim" : "Nao",
                //                      c.produtoPaiId,
                //                      c.estoqueReal
                //                  }).Distinct().ToList();

                grd.DataSource = (from c in data.tbProdutos
                                  join ia in data.tbInformacaoAdcionals on c.produtoId equals ia.produtoId into psia
                                  from ia in psia.DefaultIfEmpty()
                                  where c.produtoAtivo == "True" && !psia.Select(x => x.produtoId).Any()
                                  select new
                                  {
                                      c.produtoId,
                                      produto = c.produtoNome,
                                      ativo = c.produtoAtivo == "True" ? "Sim" : "Nao",
                                      c.produtoPaiId,
                                      c.estoqueReal

                                  }).Distinct().ToList();
            }
            else
            {
                //var dados = (from c in data.tbProdutos
                //             where c.produtoAtivo == "True" && (c.produtoComposicao ?? "") == ""
                //             select new
                //             {
                //                 c.produtoId,
                //                 produto = c.produtoNome,
                //                 ativo = c.produtoAtivo == "True" ? "Sim" : "Nao",
                //                 c.produtoPaiId,
                //                 c.estoqueReal,
                //                 c.produtoMarca
                //             }).Distinct().ToList();

                var dados = (from c in data.tbProdutos
                             join ia in data.tbInformacaoAdcionals on c.produtoId equals ia.produtoId into psia
                             from ia in psia.DefaultIfEmpty()
                             where c.produtoAtivo == "True" && c.produtoFornecedor == fornecedor && !psia.Select(x => x.produtoId).Any()
                             select new
                             {
                                 c.produtoId,
                                 produto = c.produtoNome,
                                 ativo = c.produtoAtivo == "True" ? "Sim" : "Nao",
                                 c.produtoPaiId,
                                 c.estoqueReal,
                                 c.produtoMarca
                             }).Distinct().ToList();

                if (ddlMarca.SelectedValue != "0")
                {
                    int marca = Convert.ToInt32(ddlMarca.SelectedValue);
                    dados = dados.Where(x => x.produtoMarca == marca).ToList();
                }

                int categoria = Convert.ToInt32(ddlCategoria.SelectedValue == "" ? "0" : ddlCategoria.SelectedValue);

                if (categoria > 0)
                    dados = (from c in dados
                             join cat in data.tbJuncaoProdutoCategorias on c.produtoId equals cat.produtoId
                             where cat.categoriaId == categoria
                             select c).ToList();

                grd.DataSource = dados;
            }


            grd.DataBind();
            grd.Visible = true;

        }


        grd.Columns[2].Visible = true;
        grd.Columns[3].Visible = (lblTituloDetalhamento.Text == Titulos(7) | lblTituloDetalhamento.Text == Titulos(8) | lblTituloDetalhamento.Text == Titulos(11));
        grd.Columns[4].Visible = (lblTituloDetalhamento.Text == Titulos(9));
        grd.Columns[5].Visible = true;
    }

    /// <summary>
    /// Produtos ativos em promoção
    /// </summary>
    protected void FillDetalharPAEP()
    {
        lblTituloDetalhamento.Text = Titulos(16);
        lblTituloDetalhamento.Visible = true;

        using (var data = new dbCommerceDataContext())
        {
            int fornecedor = Convert.ToInt32(ddlFornecedor.SelectedValue);

            if (fornecedor == 0)
            {
                grd.DataSource = (from c in data.tbProdutos
                                  where c.produtoAtivo == "True" && (c.produtoPromocao ?? "") == "True"
                                  select new
                                  {
                                      c.produtoId,
                                      produto = c.produtoNome,
                                      ativo = c.produtoAtivo == "True" ? "Sim" : "Nao",
                                      c.produtoPaiId,
                                      c.estoqueReal
                                  }).Distinct().ToList();
            }
            else
            {
                var dados = (from c in data.tbProdutos
                             where c.produtoAtivo == "True" && (c.produtoPromocao ?? "") == "True" && c.produtoFornecedor == fornecedor
                             select new
                             {
                                 c.produtoId,
                                 produto = c.produtoNome,
                                 ativo = c.produtoAtivo == "True" ? "Sim" : "Nao",
                                 c.produtoPaiId,
                                 c.estoqueReal,
                                 c.produtoMarca
                             }).Distinct().ToList();

                if (ddlMarca.SelectedValue != "0")
                {
                    int marca = Convert.ToInt32(ddlMarca.SelectedValue);
                    dados = dados.Where(x => x.produtoMarca == marca).ToList();
                }

                int categoria = Convert.ToInt32(ddlCategoria.SelectedValue == "" || ddlCategoria.SelectedValue == "0" ? hfCategoriaId.Value : ddlCategoria.SelectedValue);

                if (categoria > 0)
                    dados = (from c in dados
                             join cat in data.tbJuncaoProdutoCategorias on c.produtoId equals cat.produtoId
                             where cat.categoriaId == categoria
                             select c).ToList();

                grd.DataSource = dados;
            }


            grd.DataBind();
            grd.Visible = true;

        }


        grd.Columns[2].Visible = true;
        grd.Columns[3].Visible = (lblTituloDetalhamento.Text == Titulos(7) | lblTituloDetalhamento.Text == Titulos(8) | lblTituloDetalhamento.Text == Titulos(11));
        grd.Columns[4].Visible = (lblTituloDetalhamento.Text == Titulos(9));
        grd.Columns[5].Visible = true;
    }

    /// <summary>
    /// Filtros que foram criados mas estão sem produtos vinculados
    /// </summary>
    protected void FillDetalharFSP()
    {
        lblTituloDetalhamento.Text = Titulos(11);
        lblTituloDetalhamento.Visible = true;

        using (var data = new dbCommerceDataContext())
        {
            var filtros = (from c in data.tbProdutoCategorias where c.categoriaPaiId > 0 && c.exibirSite == false && c.idSite == 1 select c);
            var filtrosPais = (from c in data.tbProdutoCategorias where c.categoriaPaiId == 0 && c.exibirSite == false && c.idSite == 1 select c);

            grd.DataSource = (from c in filtros
                              join d in data.tbJuncaoProdutoCategorias on c.categoriaId equals d.categoriaId into fsp
                              from d in fsp.DefaultIfEmpty()
                              where !fsp.Select(x => x.categoriaId).Any()
                              select new
                              {
                                  produtoId = c.categoriaId,
                                  produto = filtrosPais.First(x => x.categoriaId == c.categoriaPaiId).categoriaNome,
                                  ativo = "",
                                  categoria = c.categoriaNome,
                                  estoqueReal = "",
                                  produtoPaiId = 0
                              }).Distinct().ToList();

            grd.DataBind();
            grd.Visible = true;
        }

        grd.Columns[2].Visible = false;
        grd.Columns[3].Visible = (lblTituloDetalhamento.Text == Titulos(7) | lblTituloDetalhamento.Text == Titulos(8) | lblTituloDetalhamento.Text == Titulos(11));
        grd.Columns[4].Visible = (lblTituloDetalhamento.Text == Titulos(9));
        grd.Columns[5].Visible = false;
    }

    /// <summary>
    /// Preço promocional maior que preço de venda
    /// </summary>
    protected void FillDetalharPPMPV()
    {
        lblTituloDetalhamento.Text = Titulos(15);
        lblTituloDetalhamento.Visible = true;

        using (var data = new dbCommerceDataContext())
        {
            int fornecedor = Convert.ToInt32(ddlFornecedor.SelectedValue);

            if (fornecedor == 0)
            {
                grd.DataSource = (from c in data.tbProdutos
                                  where c.produtoAtivo == "True" && (c.produtoPrecoPromocional > c.produtoPreco)
                                  select new
                                  {
                                      c.produtoId,
                                      produto = c.produtoNome,
                                      ativo = c.produtoAtivo == "True" ? "Sim" : "Nao",
                                      c.produtoPaiId,
                                      c.estoqueReal,
                                      c.produtoMarca
                                  }).Distinct().ToList();
            }
            else
            {
                var dados = (from c in data.tbProdutos
                             where c.produtoAtivo == "True" && (c.produtoPrecoPromocional > c.produtoPreco) && c.produtoFornecedor == fornecedor
                             select new
                             {
                                 c.produtoId,
                                 produto = c.produtoNome,
                                 ativo = c.produtoAtivo == "True" ? "Sim" : "Nao",
                                 c.produtoPaiId,
                                 c.estoqueReal,
                                 c.produtoMarca
                             }).Distinct().ToList();

                int categoria = Convert.ToInt32(ddlCategoria.SelectedValue == "" ? "0" : ddlCategoria.SelectedValue);

                if (categoria > 0)
                    dados = (from c in dados
                             join cat in data.tbJuncaoProdutoCategorias on c.produtoId equals cat.produtoId
                             where cat.categoriaId == categoria
                             select c).Distinct().ToList();

                grd.DataSource = dados;
            }



            grd.DataBind();
            grd.Visible = true;
        }

        grd.Columns[2].Visible = true;
        grd.Columns[3].Visible = (lblTituloDetalhamento.Text == Titulos(7) | lblTituloDetalhamento.Text == Titulos(8) | lblTituloDetalhamento.Text == Titulos(11));
        grd.Columns[4].Visible = (lblTituloDetalhamento.Text == Titulos(9));
        grd.Columns[5].Visible = true;
    }

    #endregion Fill Grid

    protected void grd_OnCustomUnboundColumnData(object sender, ASPxGridViewColumnDataEventArgs e)
    {
        if (!(lblTituloDetalhamento.Text == Titulos(7) | lblTituloDetalhamento.Text == Titulos(8))) return;
        if (e.Column.FieldName != "categoria") return;

        var produtoId = Convert.ToInt32(e.GetListSourceFieldValue("produtoId"));

        using (var data = new dbCommerceDataContext())
        {
            var categorias = (from c1 in data.tbJuncaoProdutoCategorias
                              join c2 in data.tbProdutoCategorias on c1.categoriaId equals c2.categoriaId
                              where c1.produtoId == produtoId && c2.exibirSite == true
                              select new { c2.categoriaNome });

            e.Value = "";

            foreach (var categoria in categorias)
            {
                e.Value += categoria.categoriaNome + Environment.NewLine;
            }
        }
    }

    protected void btXsl_Click(object sender, ImageClickEventArgs e)
    {
        if (grd.VisibleRowCount > 0)
            this.grdEx.WriteXlsToResponse();
    }

    protected void btXlsPM_OnClick(object sender, ImageClickEventArgs e)
    {
        if (grdProdutosPorMargem.VisibleRowCount > 0)
            this.grdExGrdProdutosPorMargem.WriteXlsToResponse();
    }

    protected void btXlsPF_OnClick(object sender, ImageClickEventArgs e)
    {
        if (grdPesquisaPorFiltro.VisibleRowCount > 0)
            this.grdExGrdPesquisaPorFiltro.WriteXlsToResponse();
    }

    protected void ddlFornecedor_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        int fornecedor = Convert.ToInt32(ddlFornecedor.SelectedValue);

        using (var data = new dbCommerceDataContext())
        {
            using (
                var txn = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required,
                    new System.Transactions.TransactionOptions
                    {
                        IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                    }))

            {
                var marcas = (from c in data.tbProdutos
                              where c.produtoFornecedor == fornecedor
                              select new { c.produtoMarca }).Distinct().Select(x => x.produtoMarca).ToList();

                var dados = (from c in data.tbMarcas where marcas.Contains(c.marcaId) select c).ToList();
                ddlMarca.Items.Clear();
                ddlMarca.DataSource = dados;
                ddlMarca.DataBind();

                ddlMarca.Items.Insert(0, new ListItem("Todas as Marcas", "0"));

                var categoriasPorFornecedor = (from c in data.tbProdutos
                                               join pc in data.tbJuncaoProdutoCategorias on c.produtoId equals pc.produtoId
                                               where c.produtoFornecedor == fornecedor && pc.tbProdutoCategoria.exibirSite == true
                                               select new { pc.tbProdutoCategoria.categoriaId, pc.tbProdutoCategoria.categoriaNome }).Distinct().OrderBy(x => x.categoriaNome).ToList();

                ddlCategoria.Items.Clear();
                ddlCategoria.DataSource = categoriasPorFornecedor;
                ddlCategoria.DataBind();

                ddlCategoria.Items.Insert(0, new ListItem("Todas as Categorias", "0"));

                if (hfCategoriaId.Value != "")
                    if (ddlCategoria.Items.FindByValue(hfCategoriaId.Value) != null)
                        ddlCategoria.SelectedValue = hfCategoriaId.Value;
            }
        }
        if (fornecedor == 0)
        {
            //FillDashboard();
            litForaDeLinha.Text = "";
            litForaDeLinhaEstoque0.Text = "";
            litForaDeLinhaEstoqueBaixo.Text = "";
            litEstoqueBaixo.Text = "";
            litProdutosDesativadosComEstoqueMaiorQue0.Text = "";
            litProdutoAtivoSemCategoria.Text = "";
            litCategoriaAtivaSemProduto.Text = "";
            litCategoriaAtivaSemDescricao.Text = "";
            litProdutoAtivoSemFoto.Text = "";
            litProdutosAtivosComEstoque0.Text = "";
            litProdutoAtivoSemDescricao.Text = "";
            litProdutoAtivoSemColecao.Text = "";
            litProdutoAtivoSemComposicao.Text = "";
            litProdutoAtivoEmPromocao.Text = "";
            //litProdutoAtivoComMargemMenor133.Text = "";
            litFiltroSemProduto.Text = "";
            litPrecoPromcionalMaiorQuePrecoVenda.Text = "";
            litPrecoDeCustoMaiorQuePrecoVenda.Text = "";
        }
        else
        {
            //FillDashboardFiltro();
            litForaDeLinha.Text = "";
            litForaDeLinhaEstoque0.Text = "";
            litForaDeLinhaEstoqueBaixo.Text = "";
            litEstoqueBaixo.Text = "";
            litProdutosDesativadosComEstoqueMaiorQue0.Text = "";
            litProdutoAtivoSemCategoria.Text = "";
            litCategoriaAtivaSemProduto.Text = "";
            litCategoriaAtivaSemDescricao.Text = "";
            litProdutoAtivoSemFoto.Text = "";
            litProdutosAtivosComEstoque0.Text = "";
            litProdutoAtivoSemDescricao.Text = "";
            litProdutoAtivoSemColecao.Text = "";
            litProdutoAtivoSemComposicao.Text = "";
            litProdutoAtivoEmPromocao.Text = "";
            //litProdutoAtivoComMargemMenor133.Text = "";
            litFiltroSemProduto.Text = "";
            litPrecoPromcionalMaiorQuePrecoVenda.Text = "";
            litPrecoDeCustoMaiorQuePrecoVenda.Text = "";
        }

    }

    protected void ddlMarca_OnSelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void ddlCategoria_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        //FillDashboardFiltro();
    }

    protected void ddlFiltros_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        int filtro = Convert.ToInt32(ddlFiltros.SelectedValue);

        try
        {
            if (filtro == 0)
            {
                ddlOpcoesFiltro.Items.Clear();
                ddlOpcoesFiltro.DataSource = null;
                ddlOpcoesFiltro.DataBind();

                ddlOpcoesFiltro.Items.Insert(0, new ListItem("Todas as Opções", "0"));
            }
            else
            {
                using (var data = new dbCommerceDataContext())
                {
                    var dados = (from c in data.tbProdutoCategorias
                                 where c.categoriaPaiId == filtro
                                 orderby c.categoriaNome
                                 select new { c.categoriaId, c.categoriaNome }).ToList();

                    ddlOpcoesFiltro.Items.Clear();
                    ddlOpcoesFiltro.DataSource = dados;
                    ddlOpcoesFiltro.DataBind();

                    ddlOpcoesFiltro.Items.Insert(0, new ListItem("Todas as Opções", "0"));
                }
            }

            lblTituloPesquisaPorFiltro.Text = "";

            grdPesquisaPorFiltro.DataSource = null;
            grdPesquisaPorFiltro.DataBind();
            grdPesquisaPorFiltro.Visible = false;
        }
        catch (Exception)
        {

            throw;
        }
    }

    protected void btnPesquisaPorFiltro_OnClick(object sender, EventArgs e)
    {
        CarregaGrdPesquisaPorFiltro();
    }

    protected void CarregaGrdPesquisaPorFiltro()
    {
        int filtro = Convert.ToInt32(ddlFiltros.SelectedValue);
        int opcaoDeFiltro = Convert.ToInt32((ddlOpcoesFiltro.SelectedValue == "" ? "0" : ddlOpcoesFiltro.SelectedValue));

        using (var data = new dbCommerceDataContext())
        {
            if (filtro != 0 && opcaoDeFiltro == 0)
            {
                List<int> opcoesDoFiltro = new List<int>();
                opcoesDoFiltro = (from c in data.tbProdutoCategorias where c.categoriaPaiId == filtro select new { c.categoriaId }).Select(x => x.categoriaId).ToList();

                var produtosComFiltro = (from c in data.tbJuncaoProdutoCategorias
                                         where opcoesDoFiltro.Contains(c.categoriaId) && c.tbProduto.produtoAtivo == "True"
                                         select new { c.tbProduto.produtoId }).Distinct().Select(x => x.produtoId).Take(2000).ToList();

                var dados = (from c in data.tbJuncaoProdutoCategorias
                             where !produtosComFiltro.Contains(c.produtoId)
                             select new
                             {
                                 c.produtoId,
                                 c.tbProduto.produtoNome,
                                 c.tbProduto.produtoPaiId,
                                 ativo = c.tbProduto.produtoAtivo == "True" ? "Sim" : "Nao"
                             }).Distinct().ToList();



                //, c.tbProduto.produtoPaiId, c.tbProduto.produtoNome

                grdPesquisaPorFiltro.DataSource = dados;
                grdPesquisaPorFiltro.DataBind();

                grdPesquisaPorFiltro.Visible = dados.Any();

                lblTituloPesquisaPorFiltro.Text = "Produtos que <b>NÃO</b> fazem parte do Filtro " + ddlFiltros.SelectedItem.Text;
            }
            else if (filtro != 0 && opcaoDeFiltro != 0)
            {
                var dados = (from c in data.tbJuncaoProdutoCategorias
                             where c.categoriaId == opcaoDeFiltro
                             select new
                             {
                                 c.produtoId,
                                 c.tbProduto.produtoNome,
                                 c.tbProduto.produtoPaiId,
                                 ativo = c.tbProduto.produtoAtivo == "True" ? "Sim" : "Nao"
                             }).ToList();

                grdPesquisaPorFiltro.DataSource = dados;
                grdPesquisaPorFiltro.DataBind();

                grdPesquisaPorFiltro.Visible = dados.Any();

                lblTituloPesquisaPorFiltro.Text = "Produtos que fazem parte do Filtro " + ddlFiltros.SelectedItem.Text + " / " + ddlOpcoesFiltro.SelectedItem.Text;
            }
        }
    }

    protected void btnPesquisarProdutosPelaMargem_OnClick(object sender, EventArgs e)
    {
        try
        {
            grdProdutosPorMargem.DataBind();

            grdProdutosPorMargem.Visible = true;
            return;

            if (Visible)
            {

            }
            decimal margemIni = Convert.ToDecimal(txtMargemIni.Text);
            decimal margemFim = Convert.ToDecimal(txtMargemFim.Text);

            using (var data = new dbCommerceDataContext())
            {
                var dados = (from c in data.tbProdutos
                             where (c.margemDeLucro >= margemIni && c.margemDeLucro <= margemFim)
                             select new
                             {
                                 c.produtoId,
                                 c.produtoNome,
                                 c.margemDeLucro,
                                 c.produtoPaiId,
                                 ativo = c.produtoAtivo == "True" ? "Sim" : "Nao"
                             }).ToList();

                grdProdutosPorMargem.DataSource = dados;
                grdProdutosPorMargem.DataBind();

                grdProdutosPorMargem.Visible = dados.Any();
            }
        }
        catch (Exception)
        {

            throw;
        }
    }

}