﻿using System;
using System.Activities;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Amazon.S3;
using Amazon.S3.Model;
using DevExpress.Web.ASPxGridView;
using Org.BouncyCastle.Asn1.X509;

public partial class admin_siteseguroadmin : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void grdDepoimentos_OnHtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        //throw new NotImplementedException();
    }

    protected void grdDepoimentos_OnHtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
    {
        //throw new NotImplementedException();
    }

    protected void grdDepoimentos_OnCustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        // throw new NotImplementedException();
    }



    protected void ibtnEditar_OnCommand(object sender, CommandEventArgs e)
    {
        int id = Convert.ToInt32(e.CommandArgument);
        hfDepoimentoId.Value = id.ToString();
        string posicao = "";


        //fillRelacionados();

        lblAcao.Text = "Editar Depoimento";
        pnlListaDepoimentos.Visible = false;
        //pnlEditar.Visible = false;
        pnlCadastrarDepoimento.Visible = true;

        try
        {
            using (var data = new dbCommerceDataContext())
            {
                var depoimento = (from d in data.tbSiteSeguros where d.Id == id select d).FirstOrDefault();

                txtTitulo.Text = depoimento.titulo;
                txtSubtitulo.Text = depoimento.subtitulo;
                txtConteudo.Text = depoimento.conteudo;
                ddlRede.SelectedValue = depoimento.rede;
                ddlPosicaoConteudo.SelectedValue = depoimento.posicao.ToString();
                txtReferencia.Text = depoimento.referencia;
                ckbDepoimentoAtivo.Checked = (bool)depoimento.ativo;

                if (depoimento.foto != "")
                {
                    imgFoto.ImageUrl = ConfigurationManager.AppSettings["caminhoVirtual"] + "banners/siteseguro/" + depoimento.foto;
                }
            }
        }
        catch (Exception)
        {

            throw;
        }
    }

    protected void ibtnEditarDestaque_OnCommand(object sender, CommandEventArgs e)
    {
        int id = Convert.ToInt32(e.CommandArgument);
        hfDestaqueId.Value = id.ToString();
        string posicao = "";


        //fillRelacionados();

        lblAcao.Text = "Editar Destaque";
        pnlListaDestaques.Visible = false;
        //pnlEditar.Visible = false;
        pnlCadastroDestaque.Visible = true;

        try
        {
            using (var data = new dbCommerceDataContext())
            {
                var destaque = (from d in data.tbSiteSeguros where d.Id == id select d).FirstOrDefault();

                txtTituloDestaque.Text = destaque.titulo;
                txtConteudoDestaque.Text = destaque.conteudo;
                ddlPosicaoDestaque.SelectedValue = destaque.posicao.ToString();
                txtReferenciaDestaque.Text = destaque.referencia;
                ckbDestaqueAtivo.Checked = (bool)destaque.ativo;

                if (destaque.foto != "")
                {
                    imgFotoDestaque.ImageUrl = ConfigurationManager.AppSettings["caminhoVirtual"] + "banners/siteseguro/" + destaque.foto;
                }
            }
        }
        catch (Exception)
        {

            throw;
        }
    }

    protected void ibtnEditarVideo_OnCommand(object sender, CommandEventArgs e)
    {
        int id = Convert.ToInt32(e.CommandArgument);
        hfVideoId.Value = id.ToString();
        string posicao = "";


        lblAcao.Text = "Editar Video";
        pnlListaVideos.Visible = false;
        pnlCadastroVideo.Visible = true;

        try
        {
            using (var data = new dbCommerceDataContext())
            {
                var video = (from d in data.tbSiteSeguros where d.Id == id select d).FirstOrDefault();

                txtTituloVideo.Text = video.titulo;
                txtSubtituloVideo.Text = video.subtitulo;
                ddlPosicaoVideo.SelectedValue = video.posicao.ToString();
                txtLinkVideo.Text = "https://www.youtube.com/watch?v=" + video.referencia;
                ckbVideoAtivo.Checked = (bool)video.ativo;
            }
        }
        catch (Exception)
        {

            throw;
        }
    }

    protected void ibtnNovo_OnCommand(object sender, CommandEventArgs e)
    {
        lblAcao.Text = "Cadastro de Depoimento";
        pnlListaDepoimentos.Visible = false;
        //pnlEditar.Visible = false;
        pnlCadastrarDepoimento.Visible = true;
        //divCategoriasSelecionadas.Visible = true;
        //pnlCategorias.Visible = true;
        hfDepoimentoId.Value = "";
        imgFoto.ImageUrl = "";
    }

    protected void ibtnNovoDestaque_OnCommand(object sender, CommandEventArgs e)
    {
        lblAcao.Text = "Cadastro de Destaque";
        pnlListaDestaques.Visible = false;
        //pnlEditar.Visible = false;
        pnlCadastroDestaque.Visible = true;
        //divCategoriasSelecionadas.Visible = true;
        //pnlCategorias.Visible = true;
        hfDestaqueId.Value = "";
        imgFotoDestaque.ImageUrl = "";
    }

    protected void ibtnNovoVideo_OnCommand(object sender, CommandEventArgs e)
    {
        lblAcao.Text = "Cadastro de Video";
        pnlListaVideos.Visible = false;
        //pnlEditar.Visible = false;
        pnlCadastroVideo.Visible = true;
        //divCategoriasSelecionadas.Visible = true;
        //pnlCategorias.Visible = true;
        hfVideoId.Value = "";
    }

    protected void btnVoltarListaDepoimentos_OnClick(object sender, EventArgs e)
    {
        lblAcao.Text = "Site Seguro";
        pnlListaDepoimentos.Visible = true;
        pnlCadastrarDepoimento.Visible = false;
        hfDepoimentoId.Value = "";
    }

    protected void btnVoltarListaDestaques_OnClick(object sender, EventArgs e)
    {
        lblAcao.Text = "Site Seguro";
        pnlListaDestaques.Visible = true;
        pnlCadastroDestaque.Visible = false;
        hfDestaqueId.Value = "";
    }

    protected void btnVoltarListaVideos_OnClick(object sender, EventArgs e)
    {
        lblAcao.Text = "Site Seguro";
        pnlListaVideos.Visible = true;
        pnlCadastroVideo.Visible = false;
        hfVideoId.Value = "";
    }

    protected void btnSalvarDepoimento_OnClick(object sender, EventArgs e)
    {

        try
        {

            if (string.IsNullOrEmpty(hfDepoimentoId.Value))
            {
                var novoDepoimento = new tbSiteSeguro
                {
                    tipo = 1,
                    titulo = txtTitulo.Text,
                    subtitulo = txtSubtitulo.Text,
                    conteudo = txtConteudo.Text,
                    ativo = true,
                    rede = ddlRede.SelectedValue,
                    referencia = txtReferencia.Text,
                    posicao = Convert.ToInt32(ddlPosicaoConteudo.SelectedValue),
                    dataDeCadastro = DateTime.Now
                };

                using (var data = new dbCommerceDataContext())
                {
                    data.tbSiteSeguros.InsertOnSubmit(novoDepoimento);
                    data.SubmitChanges();

                    if (FileUploadFoto.HasFile)
                    {
                        var fotoDepoimentoRenomear = (from s in data.tbSiteSeguros where s.Id == novoDepoimento.Id select s).FirstOrDefault();

                        string nomeFoto = novoDepoimento.Id + DateTime.Now.ToString("ddMMyyyyHHmmss") + FileUploadFoto.FileName;

                        if (fotoDepoimentoRenomear != null)
                        {
                            fotoDepoimentoRenomear.foto = nomeFoto;
                            data.SubmitChanges();
                        }

                        #region compactar Imagem Kraken
                        KrakenConfig _kConfig = new KrakenConfig();
                        string key = _kConfig.APIKey;
                        string secret = _kConfig.APISecret;

                        Kraken k = new Kraken(key, secret);

                        KrakenRequest kr = new KrakenRequest();

                        kr.Lossy = false;

                        if (_kConfig.UseCallbacks)
                        {
                            kr.CallbackUrl = _kConfig.CallbackURL;
                        }
                        else
                        {
                            kr.Wait = true;
                        }

                        // Can't trust the length of Stream. Converting to a MemoryStream
                        using (System.IO.MemoryStream ms = new System.IO.MemoryStream())
                        {
                            FileUploadFoto.FileContent.CopyTo(ms);

                            kr.File = ms.ToArray();
                        }

                        kr.s3_store = new KrakenRequestS3Amazon();
                        kr.s3_store.S3Key = "AKIAIP7IE6WVNK2K6TIQ";
                        kr.s3_store.S3Secret = "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";
                        kr.s3_store.S3Bucket = "cdn2.graodegente.com.br";
                        kr.s3_store.S3Region = "sa-east-1";
                        kr.s3_store.S3Path = "banners/siteseguro/" + nomeFoto;

                        if (ddlPosicaoConteudo.SelectedValue == "1" | ddlPosicaoConteudo.SelectedValue == "3" | ddlPosicaoConteudo.SelectedValue == "4")
                        {
                            kr.Resize = new KrakenResizeImage { Width = 120, Height = 120, Strategy = "auto" };
                        }
                        else
                        {
                            kr.Resize = new KrakenResizeImage { Width = 500, Height = 330, Strategy = "auto" };
                        }

                        var response = k.Upload(kr, FileUploadFoto.PostedFile.FileName, FileUploadFoto.PostedFile.ContentType);

                        if (response.Success == false || response.Error != null)
                        {

                        }

                        using (var webClient = new System.Net.WebClient())
                        using (var stream = webClient.OpenRead(response.KrakedUrl))
                        {

                            webClient.DownloadFile(response.KrakedUrl, ConfigurationManager.AppSettings["caminhoFisico"] + "banners\\siteseguro\\" + response.KrakedUrl.Substring(response.KrakedUrl.LastIndexOf('/') + 1));
                        }

                        #endregion
                    }
                }

                ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('Depoimento incluído com sucesso!');", true);

                lblAcao.Text = "Site Seguro";
                pnlListaDepoimentos.Visible = true;
                pnlCadastrarDepoimento.Visible = false;
                hfDepoimentoId.Value = "";
                grdDepoimentos.DataBind();

                txtTitulo.Text = "";
                txtSubtitulo.Text = "";
                txtReferencia.Text = "";
            }
            else
            {
                int id = Convert.ToInt32(hfDepoimentoId.Value);

                using (var data = new dbCommerceDataContext())
                {
                    var depoimento = (from d in data.tbSiteSeguros where d.Id == id select d).FirstOrDefault();

                    depoimento.titulo = txtTitulo.Text;
                    depoimento.subtitulo = txtSubtitulo.Text;
                    depoimento.conteudo = txtConteudo.Text;
                    depoimento.rede = ddlRede.SelectedValue;
                    depoimento.posicao = Convert.ToInt32(ddlPosicaoConteudo.SelectedValue);
                    depoimento.referencia = txtReferencia.Text;
                    depoimento.ativo = ckbDepoimentoAtivo.Checked;

                    if (FileUploadFoto.HasFile)
                    {

                        #region compactar Imagem Kraken
                        KrakenConfig _kConfig = new KrakenConfig();
                        string key = _kConfig.APIKey;
                        string secret = _kConfig.APISecret;

                        Kraken k = new Kraken(key, secret);

                        KrakenRequest kr = new KrakenRequest();

                        kr.Lossy = false;

                        if (_kConfig.UseCallbacks)
                        {
                            kr.CallbackUrl = _kConfig.CallbackURL;
                        }
                        else
                        {
                            kr.Wait = true;
                        }

                        // Can't trust the length of Stream. Converting to a MemoryStream
                        using (System.IO.MemoryStream ms = new System.IO.MemoryStream())
                        {
                            FileUploadFoto.FileContent.CopyTo(ms);

                            kr.File = ms.ToArray();
                        }

                        string nomeFoto = depoimento.Id.ToString() + DateTime.Now.ToString("ddMMyyyyHHmmss") + FileUploadFoto.FileName;

                        kr.s3_store = new KrakenRequestS3Amazon();
                        kr.s3_store.S3Key = "AKIAIP7IE6WVNK2K6TIQ";
                        kr.s3_store.S3Secret = "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";
                        kr.s3_store.S3Bucket = "cdn2.graodegente.com.br";
                        kr.s3_store.S3Region = "sa-east-1";
                        kr.s3_store.S3Path = "banners/siteseguro/" + nomeFoto;

                        if (ddlPosicaoConteudo.SelectedValue == "1" | ddlPosicaoConteudo.SelectedValue == "3" | ddlPosicaoConteudo.SelectedValue == "4")
                        {
                            kr.Resize = new KrakenResizeImage { Width = 120, Height = 120, Strategy = "auto" };
                        }
                        else
                        {
                            kr.Resize = new KrakenResizeImage { Width = 500, Height = 330, Strategy = "auto" };
                        }

                        var response = k.Upload(kr, FileUploadFoto.PostedFile.FileName, FileUploadFoto.PostedFile.ContentType);

                        if (response.Success == false || response.Error != null)
                        {

                        }

                        if (System.IO.File.Exists(ConfigurationManager.AppSettings["caminhoFisico"] + "banners\\siteseguro\\" + depoimento.foto))
                            System.IO.File.Delete(ConfigurationManager.AppSettings["caminhoFisico"] + "banners\\siteseguro\\" + depoimento.foto);


                        using (var webClient = new System.Net.WebClient())
                        using (var stream = webClient.OpenRead(response.KrakedUrl))
                        {

                            webClient.DownloadFile(response.KrakedUrl, ConfigurationManager.AppSettings["caminhoFisico"] + "banners\\siteseguro\\" + response.KrakedUrl.Substring(response.KrakedUrl.LastIndexOf('/') + 1));
                        }

                        #endregion

                        depoimento.foto = response.KrakedUrl.Substring(response.KrakedUrl.LastIndexOf('/') + 1);
                    }

                    data.SubmitChanges();
                }

                ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('Depoimento alterado com sucesso!');", true);

                lblAcao.Text = "Site Seguro";
                pnlListaDepoimentos.Visible = true;
                pnlCadastrarDepoimento.Visible = false;
                hfDepoimentoId.Value = "";
                grdDepoimentos.DataBind();

                txtTitulo.Text = "";
                txtSubtitulo.Text = "";
                txtReferencia.Text = "";
            }
        }
        catch (Exception)
        {


        }
    }

    protected void btnSalvarDestaque_OnClick(object sender, EventArgs e)
    {
        try
        {

            if (string.IsNullOrEmpty(hfDestaqueId.Value))
            {
                var novoDestaque = new tbSiteSeguro
                {
                    tipo = 2,
                    titulo = txtTituloDestaque.Text,
                    subtitulo = txtSubTituloDestaque.Text,
                    conteudo = txtConteudoDestaque.Text,
                    posicao = Convert.ToInt32(ddlPosicaoDestaque.SelectedValue),
                    ativo = true,
                    referencia = txtReferenciaDestaque.Text,
                    dataDeCadastro = DateTime.Now
                };

                using (var data = new dbCommerceDataContext())
                {
                    data.tbSiteSeguros.InsertOnSubmit(novoDestaque);
                    data.SubmitChanges();

                    if (FileUploadDestaque.HasFile)
                    {
                        var fotoDestaqueRenomear = (from s in data.tbSiteSeguros where s.Id == novoDestaque.Id select s).FirstOrDefault();

                        string nomeFoto = novoDestaque.Id + DateTime.Now.ToString("ddMMyyyyHHmmss") + FileUploadDestaque.FileName;

                        if (fotoDestaqueRenomear != null)
                        {
                            fotoDestaqueRenomear.foto = nomeFoto;
                            data.SubmitChanges();
                        }

                        #region compactar Imagem Kraken
                        KrakenConfig _kConfig = new KrakenConfig();
                        string key = _kConfig.APIKey;
                        string secret = _kConfig.APISecret;

                        Kraken k = new Kraken(key, secret);

                        KrakenRequest kr = new KrakenRequest();

                        kr.Lossy = false;

                        if (_kConfig.UseCallbacks)
                        {
                            kr.CallbackUrl = _kConfig.CallbackURL;
                        }
                        else
                        {
                            kr.Wait = true;
                        }

                        // Can't trust the length of Stream. Converting to a MemoryStream
                        using (System.IO.MemoryStream ms = new System.IO.MemoryStream())
                        {
                            FileUploadDestaque.FileContent.CopyTo(ms);

                            kr.File = ms.ToArray();
                        }



                        kr.s3_store = new KrakenRequestS3Amazon();
                        kr.s3_store.S3Key = "AKIAIP7IE6WVNK2K6TIQ";
                        kr.s3_store.S3Secret = "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";
                        kr.s3_store.S3Bucket = "cdn2.graodegente.com.br";
                        kr.s3_store.S3Region = "sa-east-1";
                        kr.s3_store.S3Path = "banners/siteseguro/" + nomeFoto;

                        if (ddlPosicaoDestaque.SelectedValue == "1")
                        {
                            kr.Resize = new KrakenResizeImage { Width = 240, Height = 240, Strategy = "auto" };
                        }
                        else
                        {
                            kr.Resize = new KrakenResizeImage { Width = 450, Height = 235, Strategy = "auto" };
                        }

                        var response = k.Upload(kr, FileUploadDestaque.PostedFile.FileName, FileUploadDestaque.PostedFile.ContentType);

                        if (response.Success == false || response.Error != null)
                        {

                        }

                        using (var webClient = new System.Net.WebClient())
                        using (var stream = webClient.OpenRead(response.KrakedUrl))
                        {

                            webClient.DownloadFile(response.KrakedUrl, ConfigurationManager.AppSettings["caminhoFisico"] + "banners\\siteseguro\\" + response.KrakedUrl.Substring(response.KrakedUrl.LastIndexOf('/') + 1));
                        }

                        #endregion
                    }
                }

                ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('Destaque incluído com sucesso!');", true);

                lblAcao.Text = "Site Seguro";
                pnlListaDestaques.Visible = true;
                pnlCadastroDestaque.Visible = false;
                grdDestaques.DataBind();

                txtTituloDestaque.Text = "";
                txtSubTituloDestaque.Text = "";
                txtConteudoDestaque.Text = "";
                txtReferenciaDestaque.Text = "";
            }
            else
            {
                int id = Convert.ToInt32(hfDestaqueId.Value);

                using (var data = new dbCommerceDataContext())
                {
                    var destaque = (from d in data.tbSiteSeguros where d.Id == id select d).FirstOrDefault();

                    destaque.titulo = txtTituloDestaque.Text;
                    destaque.subtitulo = txtSubTituloDestaque.Text;
                    destaque.conteudo = txtConteudoDestaque.Text;
                    destaque.posicao = Convert.ToInt32(ddlPosicaoDestaque.SelectedValue);
                    destaque.referencia = txtReferenciaDestaque.Text;
                    destaque.ativo = ckbDestaqueAtivo.Checked;

                    if (FileUploadDestaque.HasFile)
                    {

                        string caminhoBannerLocalParcial = "banners\\siteseguro\\" ;
                        string caminhoBannerRemotoParcial = "banners/siteseguro/";
                        string caminhoBanner = ConfigurationManager.AppSettings["caminhoFisico"] + caminhoBannerLocalParcial;
                        string nomeArquivoOriginal = destaque.Id + FileUploadDestaque.FileName;
                        string nomeArquivoWebp = destaque.Id + FileUploadDestaque.FileName.Split('.')[0] + ".webp";

                         //FileUploadDestaque.PostedFile.SaveAs(caminhoBanner + "original_" + nomeArquivoOriginal);
                        FileUploadDestaque.PostedFile.SaveAs(caminhoBanner + nomeArquivoOriginal);

                        var tinify = new Tinify();
                        //var fotoOtimizada = tinify.Shrink(caminhoBanner + "original_" + nomeArquivoOriginal, caminhoBanner + nomeArquivoOriginal);
                        uploadImagemAmazonS3(caminhoBannerLocalParcial, caminhoBannerRemotoParcial, nomeArquivoOriginal);
                        destaque.foto = nomeArquivoOriginal;
                        //#region compactar Imagem Kraken
                        //KrakenConfig _kConfig = new KrakenConfig();
                        //string key = _kConfig.APIKey;
                        //string secret = _kConfig.APISecret;

                        //Kraken k = new Kraken(key, secret);

                        //KrakenRequest kr = new KrakenRequest();

                        //kr.Lossy = false;

                        //if (_kConfig.UseCallbacks)
                        //{
                        //    kr.CallbackUrl = _kConfig.CallbackURL;
                        //}
                        //else
                        //{
                        //    kr.Wait = true;
                        //}

                        //// Can't trust the length of Stream. Converting to a MemoryStream
                        //using (System.IO.MemoryStream ms = new System.IO.MemoryStream())
                        //{
                        //    FileUploadDestaque.FileContent.CopyTo(ms);

                        //    kr.File = ms.ToArray();
                        //}

                        //string nomeFoto = destaque.Id.ToString() + DateTime.Now.ToString("ddMMyyyHHmmss") + FileUploadDestaque.FileName;

                        //kr.s3_store = new KrakenRequestS3Amazon();
                        //kr.s3_store.S3Key = "AKIAIP7IE6WVNK2K6TIQ";
                        //kr.s3_store.S3Secret = "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";
                        //kr.s3_store.S3Bucket = "cdn2.graodegente.com.br";
                        //kr.s3_store.S3Region = "sa-east-1";
                        //kr.s3_store.S3Path = "banners/siteseguro/" + nomeFoto;

                        //if (ddlPosicaoDestaque.SelectedValue == "1")
                        //{
                        //    kr.Resize = new KrakenResizeImage { Width = 240, Height = 240, Strategy = "auto" };
                        //}
                        //else
                        //{
                        //    kr.Resize = new KrakenResizeImage { Width = 450, Height = 235, Strategy = "fit" };
                        //}

                        //var response = k.Upload(kr, FileUploadDestaque.PostedFile.FileName, FileUploadDestaque.PostedFile.ContentType);

                        //if (response.Success == false || response.Error != null)
                        //{

                        //}

                        //if (System.IO.File.Exists(ConfigurationManager.AppSettings["caminhoFisico"] + "banners\\siteseguro\\" + destaque.foto))
                        //    System.IO.File.Delete(ConfigurationManager.AppSettings["caminhoFisico"] + "banners\\siteseguro\\" + destaque.foto);

                        //using (var webClient = new System.Net.WebClient())
                        //using (var stream = webClient.OpenRead(response.KrakedUrl))
                        //{
                        //    webClient.DownloadFile(response.KrakedUrl, ConfigurationManager.AppSettings["caminhoFisico"] + "banners\\siteseguro\\" + response.KrakedUrl.Substring(response.KrakedUrl.LastIndexOf('/') + 1));
                        //}

                        //destaque.foto = response.KrakedUrl.Substring(response.KrakedUrl.LastIndexOf('/') + 1);

                        //#endregion
                    }

                    data.SubmitChanges();
                }

                ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('Destaque alterado com sucesso!');", true);

                lblAcao.Text = "Site Seguro";
                pnlListaDestaques.Visible = true;
                pnlCadastroDestaque.Visible = false;
                hfDestaqueId.Value = "";
                grdDestaques.DataBind();

                txtTituloDestaque.Text = "";
                txtSubTituloDestaque.Text = "";
                txtConteudoDestaque.Text = "";
                txtReferenciaDestaque.Text = "";
            }
        }
        catch (Exception ex)
        {

            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('ERRO: " + ex.Message + "!');", true);
        }
    }

    private void uploadImagemAmazonS3(string caminhoLocal, string caminhoRemoto, string foto)
    {

        string s3ServiceUrl = "s3-sa-east-1.amazonaws.com";
        string bucketName = "cdn2.graodegente.com.br";
        string filePath = ConfigurationManager.AppSettings["caminhoFisico"] + caminhoLocal + foto;
        string fileContentType = "image/jpg";
        S3CannedACL fileCannedACL = S3CannedACL.PublicReadWrite;

        string accessKey = "AKIAIP7IE6WVNK2K6TIQ";
        string secretAccessKey = "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";

        try
        {
            AmazonS3Config config = new AmazonS3Config();
            config.ServiceURL = s3ServiceUrl;
            config.CommunicationProtocol = Protocol.HTTP;

            using (var s3Client = Amazon.AWSClientFactory.CreateAmazonS3Client(accessKey, secretAccessKey, config))
            {
                PutObjectRequest request = new PutObjectRequest
                {
                    BucketName = bucketName,
                    CannedACL = fileCannedACL,
                    Key = caminhoRemoto + foto,
                    FilePath = filePath,
                    ContentType = fileContentType
                };

                request.AddHeader("expires", "Thu, 21 Mar 2042 08:16:32 GMT");
                PutObjectResponse response = s3Client.PutObject(request);

            }

        }
        catch (AmazonS3Exception s3Exception)
        {
            Console.WriteLine(s3Exception.Message, s3Exception.InnerException);

            Console.ReadKey();
        }
    }

    protected void btnSalvarVideo_OnClick(object sender, EventArgs e)
    {
        try
        {
            string linkVideo = txtLinkVideo.Text.Split('=')[1];
            if (string.IsNullOrEmpty(hfVideoId.Value))
            {
                var novoVIdeo = new tbSiteSeguro
                {
                    tipo = 3,
                    titulo = txtTituloVideo.Text,
                    subtitulo = txtSubtituloVideo.Text,
                    ativo = true,
                    posicao = Convert.ToInt32(ddlPosicaoVideo.SelectedValue),
                    referencia = linkVideo,
                    dataDeCadastro = DateTime.Now
                };

                using (var data = new dbCommerceDataContext())
                {
                    data.tbSiteSeguros.InsertOnSubmit(novoVIdeo);
                    data.SubmitChanges();
                }

                ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('Video incluido com sucesso!');", true);

                lblAcao.Text = "Site Seguro";
                pnlListaVideos.Visible = true;
                pnlCadastroVideo.Visible = false;
                hfVideoId.Value = "";
                grdVideos.DataBind();

                txtTituloVideo.Text = "";
                txtSubtituloVideo.Text = "";
            }
            else
            {
                int id = Convert.ToInt32(hfVideoId.Value);

                using (var data = new dbCommerceDataContext())
                {
                    var video = (from d in data.tbSiteSeguros where d.Id == id select d).FirstOrDefault();

                    video.titulo = txtTituloVideo.Text;
                    video.subtitulo = txtSubtituloVideo.Text;
                    video.posicao = Convert.ToInt32(ddlPosicaoVideo.SelectedValue);
                    video.referencia = linkVideo;
                    video.ativo = ckbVideoAtivo.Checked;
                    data.SubmitChanges();
                }

                ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('Video alterado com sucesso!');", true);

                lblAcao.Text = "Site Seguro";
                pnlListaVideos.Visible = true;
                pnlCadastroVideo.Visible = false;
                hfVideoId.Value = "";
                grdVideos.DataBind();

                txtTituloVideo.Text = "";
                txtSubtituloVideo.Text = "";
            }
        }
        catch (Exception)
        {

            throw;
        }
    }


    protected void grdDestaques_OnHtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        //throw new NotImplementedException();
    }

    protected void grdDestaques_OnHtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
    {
        //throw new NotImplementedException();
    }


    public Bitmap CropBitmap(Bitmap bitmap, int cropX, int cropY, int cropWidth, int cropHeight)
    {
        Rectangle rect = new Rectangle(cropX, cropY, cropWidth, cropHeight);
        Bitmap cropped = bitmap.Clone(rect, bitmap.PixelFormat);
        return cropped;
    }
}