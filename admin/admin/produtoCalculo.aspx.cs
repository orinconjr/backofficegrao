﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;

public partial class admin_produtoCalculo : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
		if (!Page.IsPostBack)
        {
            HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
            usuarioLogadoId = Request.Cookies["usuarioLogadoId"];
            if (usuarioLogadoId == null) Response.Write("<script>window.location=('default.aspx');</script>");            
        }
    }
    protected void grid_HtmlRowCreated(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType != GridViewRowType.Data) return;

        TextBox txtPrecoConcorrencia = (TextBox)grid.FindRowCellTemplateControl(e.VisibleIndex, (GridViewDataColumn)grid.Columns["PrecoConcorrencia"], "txtPrecoConcorrencia");
        TextBox txtPrecoPromocional = (TextBox)grid.FindRowCellTemplateControl(e.VisibleIndex, (GridViewDataColumn)grid.Columns["PrecoPromocional"], "txtPrecoPromocional");
        TextBox txtCusto = (TextBox)grid.FindRowCellTemplateControl(e.VisibleIndex, (GridViewDataColumn)grid.Columns["Custo"], "txtCusto");
        Label lblMargemDaConcorrencia = (Label)grid.FindRowCellTemplateControl(e.VisibleIndex, (GridViewDataColumn)grid.Columns["PrecoConcorrencia"], "lblMargemDaConcorrencia");
        Label lblPrecoPromocional = (Label)grid.FindRowCellTemplateControl(e.VisibleIndex, (GridViewDataColumn)grid.Columns["PrecoPromocional"], "lblPrecoPromocional");
        Label lblPrecoVenda = (Label)grid.FindRowCellTemplateControl(e.VisibleIndex, (GridViewDataColumn)grid.Columns["PrecoPromocional"], "lblPrecoVenda");
        Button btnIgual = (Button)grid.FindRowCellTemplateControl(e.VisibleIndex, (GridViewDataColumn)grid.Columns["PrecoPromocional"], "btnIgual");

        txtPrecoConcorrencia.Attributes.Add("onkeyPress", "return moeda(this,'.',',',event);");
        txtPrecoPromocional.Attributes.Add("onkeypress", "return soNumero(event);");
        txtCusto.Attributes.Add("onkeyPress", "return moeda(this,'.',',',event);");
        txtPrecoConcorrencia.Attributes.Add("onkeyup", "javascript:return CalculoMargemConcorrencia('" + txtPrecoConcorrencia.ClientID + "', '" + lblMargemDaConcorrencia.ClientID + "', '" + txtCusto.ClientID + "');");
        txtPrecoPromocional.Attributes.Add("onkeyup", "javascript:return CalculoMargemVenda('" + txtPrecoPromocional.ClientID + "', '" + lblPrecoPromocional.ClientID + "'," + "'" + lblPrecoVenda.ClientID + "', '" + txtCusto.ClientID + "');");
        btnIgual.Attributes.Add("onclick", "javascript:return PrecoIgual('" + txtPrecoPromocional.ClientID + "');");
    }
    protected void btnGravar_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            string estoqueAtual;

            int inicio = 0;
            int fim = 0;

            if (grid.PageIndex == 0)
            {
                inicio = 0;
                fim = inicio + grid.GetCurrentPageRowValues("produtoId").Count;
            }
            else
            {
                inicio += inicio + grid.SettingsPager.PageSize * grid.PageIndex;
                fim += inicio + grid.GetCurrentPageRowValues("produtoId").Count;
            }

            for (int i = inicio; i < fim; i++)
            {
                TextBox txtId = (TextBox)grid.FindRowCellTemplateControl(i, grid.Columns["id"] as GridViewDataColumn, "txtId");

                bool precoAlterado = false;
                TextBox txtPrecoPromocional = (TextBox)grid.FindRowCellTemplateControl(i, grid.Columns["PrecoPromocional"] as GridViewDataColumn, "txtPrecoPromocional");
                if (txtPrecoPromocional.Text != "") precoAlterado = true;

                bool nomeAlterado = false;
                TextBox txtNome = (TextBox)grid.FindRowCellTemplateControl(i, grid.Columns["nome"] as GridViewDataColumn, "txtNome");
                TextBox txtNomeAntigo = (TextBox)grid.FindRowCellTemplateControl(i, grid.Columns["nome"] as GridViewDataColumn, "txtNomeAntigo");
                if (txtNome.Text != txtNomeAntigo.Text) nomeAlterado = true;

                bool custoAlterado = false;
                TextBox txtCusto = (TextBox)grid.FindRowCellTemplateControl(i, grid.Columns["Custo"] as GridViewDataColumn, "txtCusto");
                TextBox txtCustoAntigo = (TextBox)grid.FindRowCellTemplateControl(i, grid.Columns["Custo"] as GridViewDataColumn, "txtCustoAntigo");
                if (txtCusto.Text != txtCustoAntigo.Text) custoAlterado = true;

                bool estoqueAlterado = false;
                TextBox txtEstoque = (TextBox)grid.FindRowCellTemplateControl(i, grid.Columns["estoque"] as GridViewDataColumn, "txtEstoque");
                TextBox txtEstoqueAntigo = (TextBox)grid.FindRowCellTemplateControl(i, grid.Columns["estoque"] as GridViewDataColumn, "txtEstoqueAntigo");
                if (txtEstoque.Text != txtEstoqueAntigo.Text) estoqueAlterado = true;

                bool pesoAlterado = false;
                TextBox txtPeso = (TextBox)grid.FindRowCellTemplateControl(i, grid.Columns["peso"] as GridViewDataColumn, "txtPeso");
                TextBox txtPesoAntigo = (TextBox)grid.FindRowCellTemplateControl(i, grid.Columns["peso"] as GridViewDataColumn, "txtPesoAntigo");
                if (txtPeso.Text != txtPesoAntigo.Text) pesoAlterado = true;

                bool ativoAlterado = false;
                ASPxCheckBox ckbAtivo = (ASPxCheckBox)grid.FindRowCellTemplateControl(i, grid.Columns["ativo"] as GridViewDataColumn, "ckbAtivo");
                ASPxCheckBox ckbAtivoAntigo = (ASPxCheckBox)grid.FindRowCellTemplateControl(i, grid.Columns["ativo"] as GridViewDataColumn, "ckbAtivoAntigo");
                if (ckbAtivo.Checked != ckbAtivoAntigo.Checked) ativoAlterado = true;
                
                if(precoAlterado | nomeAlterado | custoAlterado | estoqueAlterado | pesoAlterado | ativoAlterado)
                {
                    var data = new dbCommerceDataContext();
                    int id = Convert.ToInt32(txtId.Text);
                    var produtos = (from c in data.tbProdutos where c.produtoId == id select c);
                    if(produtos.Count() > 0)
                    {
                        var produto = produtos.First();
                        produto.produtoNome = txtNome.Text;
                        produto.produtoPrecoDeCusto = Convert.ToDecimal(txtCusto.Text);
                        //produto.produtoEstoqueAtual = Convert.ToInt32(txtEstoque.Text);
                        produto.produtoPeso = Convert.ToDouble(txtPeso.Text);
                        produto.produtoAtivo = ckbAtivo.Checked ? "True" : "False";

                        if(precoAlterado)
                        {
                            var valorPrecoPromocional = Convert.ToInt32(txtPrecoPromocional.Text);
                            var valorCusto = Convert.ToDouble(txtCusto.Text);
                            var porcentagemPreco = Convert.ToDouble(valorPrecoPromocional) / 100;
                            var valorPromocao = (porcentagemPreco * valorCusto) + valorCusto;
                            var valorVenda = (valorPromocao * 0.3) + valorPromocao;

                            produto.produtoPrecoPromocional = Convert.ToDecimal(valorPromocao);
                            produto.produtoPreco = Convert.ToDecimal(valorVenda);
                        }
                    }
                    data.SubmitChanges();
                    data.Dispose();
                }
            }
        }
        catch (Exception)
        {

        }
        grid.DataBind();
    }
}