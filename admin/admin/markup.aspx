﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="markup.aspx.cs" Inherits="admin_markup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" runat="Server">
    <style>
        .meugrid {
            background-color: #F4FAFB;
            border: 0px none;
            /* border-collapse: separate !important; */
            overflow: hidden;
            font: 9pt Tahoma;
            color: #000;
            cursor: pointer;
            white-space: nowrap;
            font-weight: normal;
            width: 805px;
            text-align: left;
            margin-left: 4px;
            width: 805px;
        }

            .meugrid th {
                background-image: url(../../App_Themes/Glass/GridView/gvHeaderBackground.gif);
                background-position: center top;
                background-repeat: repeat-x;
                background-color: #C0DDE0;
                overflow: hidden;
                height: 23px;
                line-height: 23px;
                padding: 4px 6px 5px;
                font-weight: normal;
                border: 1px solid #7EACB1;
                /* border-collapse: separate;*/
            }

            .meugrid td {
                overflow: hidden;
                height: 23px;
                line-height: 23px;
                padding: 4px 6px 5px;
                border: 1px solid #7EACB1;
            }

            .meugrid tr td:last-of-type {
                text-align: center;
            }

        .rotulos td {
            font: bold 12px tahoma;
            padding-top: 20px;
            color: rgb(65, 105, 126);
            padding-left: 5px;
            padding-bottom: 5px;
        }

        .formCustos {
            float: left;
            clear: left;
            width: 880px;
            font: normal 9pt tahoma;
            border-radius: 5px;
            margin-left: 12px;
        }

            .formCustos .titesquerda {
                font-weight: bold;
                float: left;
                clear: left;
                width: 250px;
                margin-top: 5px;
            }

            .formCustos .titdireita {
                float: left;
                margin-top: 5px;
            }

            .formCustos input[type=text] {
                float: left;
                margin-top: 5px;
                width: 40px;
            }

            .formCustos input[type=submit] {
                float: left;
                clear: left;
                margin-top: 5px;
            }

        .botao {
            cursor: pointer;
            border-right: solid 1px #888;
        }
    </style>
    <script src="js/jquery.min.js"></script>
    <script type="text/javascript">
        function validaform() {
            var valido = true;
            var mensagem = "";
            var txtmargemlucro = $('input[id *= txtmargemlucro]').val().replace(",", ".");
            var txttributos = $('input[id *= txttributos]').val().replace(",", ".");

            if (txtmargemlucro == "0" || txtmargemlucro == "") {
                mensagem = "Informe a margen de lucro\n";
                valido = false;
            } else {
                if (isNaN(txtmargemlucro)) {
                    mensagem += "Informe apenas valores núméricos para a margen de lucro\n";
                    valido = false;
                }
            }

            if (txttributos == "0" || txttributos == "") {
                mensagem += "Informe o índice de tributos\n";
                valido = false;
            } else {
                if (isNaN(txttributos)) {
                    mensagem += "Informe apenas valores núméricos para a margen de lucro\n";
                    valido = false;
                }
            }

            if (!valido) {
                alert(mensagem);
            }
            $('input[id *= txtmargemlucro]').val(txtmargemlucro.replace(".", ","));
            $('input[id *= txttributos]').val(txttributos.replace(".", ","));
            return valido;

        }
    </script>
    <table cellpadding="0" cellspacing="0" style="width: 920px">
        <tr>
            <td class="tituloPaginas" valign="top">
                <asp:Label ID="lblAcao" runat="server">Índices de Markup por produto</asp:Label>
            </td>
        </tr>

        <tr>
            <td>

                <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
                    <tr>
                        <td>
                            <div style="float: left; clear: left;">
                                <img id="imgfotodestaque" runat="server" style="width: 250px;" />
                            </div>
                            <div style="float: left; font: bold 16px tahoma; color: #41697E; padding-left: 20px;">
                                <asp:Label ID="lblprodutonome" runat="server" Text="Label"></asp:Label>
                            </div>
                        </td>
                    </tr>



                    <tr>
                        <td style="text-align: right;">&nbsp;
                        </td>
                    </tr>


                </table>
            </td>
        </tr>
        <tr>
            <td>
                <fieldset class="formCustos">
                    <legend>Custos</legend>
                    <label class="titesquerda">Materia Prima (Subtotal):</label>
                    <label class="titdireita" id="lblmateriaprima" runat="server"></label>
                    <label class="titesquerda">Processos Internos:</label><label class="titdireita" id="lblprocessosinternos" runat="server"></label>
                    <label class="titesquerda">Processos Externos:</label><label class="titdireita" id="lblprocessosexternos" runat="server"></label>
                    <%-- <label class="titesquerda">Gastos Gerais de Fabricação (<%= rnConfiguracoes.GGF %>%):</label><label class="titdireita" id="lblggf" runat="server"></label>--%>
                    <label class="titesquerda" style="text-decoration: underline">Gastos Gerais de Fabricação :</label>
                    <div style="float: left; clear: left; width: 100%;">
                        <table style="width: 500px;" border="0">
                            <tr>
                                <td><strong>Produto</strong></td>
                                <td><strong>Porcentagem</strong></td>
                                <td><strong>Valor</strong></td>
                            </tr>
                            <asp:Repeater ID="rptgastosmateriaprima" runat="server">
                                <HeaderTemplate>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td><%# Eval("nomeDespesasGerais") %></td>
                                        <td><%# Eval("porcentagemDespesasGerais") %></td>
                                        <td>
                                            <%# Eval("valorDespesasGerais", "{0:c}") %>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                </FooterTemplate>
                            </asp:Repeater>
                            <tr>
                                <td><strong>Total Gastos Gerais Fab.</strong></td>
                                <td>
                                    <strong>
                                        <asp:Label ID="lbltotalporcentagemgastosgerais" runat="server" Text=""></asp:Label>
                                    </strong>
                                </td>
                                <td><strong>
                                    <asp:Label ID="lbltotalvalorgastosgerais" runat="server" Text=""></asp:Label>
                                </strong></td>
                            </tr>
                            <tr>
                                <td><strong>Materia Prima (Total):</strong></td>
                                <td>&nbsp;
                                </td>
                                <td><strong>
                                    <asp:Label ID="lbltotalmateriaprima" runat="server" Text=""></asp:Label>
                                </strong></td>
                            </tr>
                            <tr>
                                <td><strong>Margem de Lucro:</strong></td>
                                <td>
                                    <asp:TextBox ID="txtmargemlucro" runat="server"></asp:TextBox>
                                </td>
                                <td><strong>
                                    <asp:Label ID="lblmargemlucro" runat="server" Text=""></asp:Label>
                                </strong></td>
                            </tr>
                            <tr>
                                <td><strong>Tributos:</strong></td>
                                <td>
                                    <asp:TextBox ID="txttributos" runat="server"></asp:TextBox>
                                </td>
                                <td><strong>
                                    <asp:Label ID="lbltributos" runat="server" Text=""></asp:Label>
                                </strong></td>
                            </tr>
                            <tr style="background-color:#dadada; padding-top: 2px; padding-bottom: 2px;">
                                <td><strong>Total:</strong></td>
                                <td>&nbsp;
                                </td>
                                <td><strong>
                                    <asp:Label ID="lbltotal" runat="server" Text=""></asp:Label>
                                </strong></td>
                            </tr>
                        </table>

                    </div>





                    <asp:Button ID="bntsalvar" runat="server" OnClientClick="return validaform();" Text="Salvar" CssClass="botao" OnClick="bntsalvar_Click" />

                </fieldset>

            </td>

        </tr>
    </table>

    <asp:LinqDataSource ID="sqlComprasProduto" runat="server" ContextTypeName="dbCommerceDataContext" OrderBy="produto" TableName="tbComprasProdutos">
    </asp:LinqDataSource>
</asp:Content>

