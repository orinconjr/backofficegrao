﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using SpreadsheetLight;
using System.IO;
using System.Xml;
public partial class admin_relatorioPedidosFornecedor : System.Web.UI.Page
{

    public class pedidoFornecedor
    {
        public int idPedidoFornecedor { get; set; }
        public string fornecedorNome { get; set; }
        public DateTime? dataPagamento { get; set; }
        public decimal custo { get; set; }
        public int faltando { get; set; }
        public string pago { get; set; }
        public DateTime? dataPedido { get; set; }
        public DateTime dataDeEntrega { get; set; }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            txtDataInicial.Attributes.Add("onkeypress", "Mascaras('DATA', '" + txtDataInicial.ClientID + "');");
            txtDataFinal.Attributes.Add("onkeypress", "Mascaras('DATA', '" + txtDataFinal.ClientID + "');");
            preencheDdlFornecedor();
            if (validarcampos())
            {
                carregaGrid();
            }
        }

    }

    protected void preencheDdlFornecedor()
    {
        var data = new dbCommerceDataContext();
        var listafornecedores = (from c in data.tbProdutoFornecedors
                                 select new
                                 {
                                     c.fornecedorId,
                                     c.fornecedorNome
                                 }).OrderBy(x => x.fornecedorNome).ToList();
        ddlfornecedor.Items.Clear();
        ddlfornecedor.Items.Add(new ListItem("Todos", "0"));
        foreach (var item in listafornecedores)
        {
            ddlfornecedor.Items.Add(new ListItem(item.fornecedorNome, item.fornecedorId.ToString()));
        }
    }
    private void carregaGrid()
    {
        List<pedidoFornecedor> itens = getDados();
        GridView1.DataSource = itens;
        GridView1.DataBind();
        lblitensencontrados.Text = itens.Count().ToString();
        lblvalortotal.Text = Convert.ToDecimal(itens.Sum(x => x.custo)).ToString("C");
    }



    public List<pedidoFornecedor> getDados()
    {

        var dataInicial = DateTime.Now;
        var dataFinal = DateTime.Now;
        int idFornecedor = 0;
        try
        {
            dataInicial = Convert.ToDateTime(txtDataInicial.Text);
            dataFinal = Convert.ToDateTime(txtDataFinal.Text);
            idFornecedor = Convert.ToInt32(ddlfornecedor.SelectedItem.Value);
        }
        catch (Exception)
        {

        }
        var data = new dbCommerceDataContext();

        List<pedidoFornecedor> pedidosFornecedor = new List<pedidoFornecedor>();
        if (idFornecedor != 0)
        {
            pedidosFornecedor = (from itens in data.tbPedidoFornecedorItems
                                 join c in data.tbPedidoFornecedors on itens.idPedidoFornecedor equals c.idPedidoFornecedor
                                 join d in data.tbProdutoFornecedors on c.idFornecedor equals d.fornecedorId
                                 where itens.dataEntrega >= dataInicial && itens.dataEntrega < dataFinal.AddDays(1) &&
                                 itens.tbPedidoFornecedor.idFornecedor == idFornecedor
                                 select new
                                 {
                                     c.idPedidoFornecedor,
                                     d.fornecedorNome,
                                     c.dataPagamento,
                                     dataPedido = c.data,
                                     c.dataEntrega,


                                 }).GroupBy(x => new { x.idPedidoFornecedor, x.fornecedorNome, x.dataPagamento, x.dataPedido }).Select(x => new pedidoFornecedor()
                                 {
                                     idPedidoFornecedor = x.Key.idPedidoFornecedor,
                                     fornecedorNome = x.Key.fornecedorNome,
                                     dataPagamento = x.Key.dataPagamento,
                                     custo = (from c in data.tbPedidoFornecedorItems where c.idPedidoFornecedor == x.Key.idPedidoFornecedor select c.custo).Sum(),
                                     faltando = (from c in data.tbPedidoFornecedorItems where c.idPedidoFornecedor == x.Key.idPedidoFornecedor && c.entregue == false select c).Count(),
                                     pago = (x.Key.dataPagamento != null).ToString(),
                                     dataPedido = x.Key.dataPedido
                                 }).ToList();
        }
        else
        {
            pedidosFornecedor = (from itens in data.tbPedidoFornecedorItems
                                 join c in data.tbPedidoFornecedors on itens.idPedidoFornecedor equals c.idPedidoFornecedor
                                 join d in data.tbProdutoFornecedors on c.idFornecedor equals d.fornecedorId
                                 where itens.dataEntrega >= dataInicial && itens.dataEntrega < dataFinal.AddDays(1)
                                 select new
                                 {
                                     c.idPedidoFornecedor,
                                     d.fornecedorNome,
                                     c.dataPagamento,
                                     dataPedido = c.data
                                 }).GroupBy(x => new { x.idPedidoFornecedor, x.fornecedorNome, x.dataPagamento, x.dataPedido }).Select(x => new pedidoFornecedor()
                                 {
                                     idPedidoFornecedor = x.Key.idPedidoFornecedor,
                                     fornecedorNome = x.Key.fornecedorNome,
                                     dataPagamento = x.Key.dataPagamento,
                                     custo = (from c in data.tbPedidoFornecedorItems where c.idPedidoFornecedor == x.Key.idPedidoFornecedor select c.custo).Sum(),
                                     faltando = (from c in data.tbPedidoFornecedorItems where c.idPedidoFornecedor == x.Key.idPedidoFornecedor && c.entregue == false select c).Count(),
                                     pago = (x.Key.dataPagamento != null).ToString(),
                                     dataPedido = x.Key.dataPedido
                                 }).ToList();
        }

        if (rdbSim.Checked)
        {
            pedidosFornecedor = (from c in pedidosFornecedor where c.pago.ToLower() == "true" select c).ToList();
        }
        if (rdbNao.Checked)
        {
            pedidosFornecedor = (from c in pedidosFornecedor where c.pago.ToLower() == "false" select c).ToList();
        }
        if (rdbEntregueSim.Checked)
        {
            pedidosFornecedor = (from c in pedidosFornecedor where c.faltando == 0 select c).ToList();
        }
        if (!String.IsNullOrEmpty(txtIdPedidoFornecedor.Text))
        {
            pedidosFornecedor = (from c in pedidosFornecedor where c.idPedidoFornecedor == Convert.ToInt32(txtIdPedidoFornecedor.Text) select c).ToList();
        }


        if (ddlordenarpor.SelectedItem.Value == "idPedidoFornecedor")
            pedidosFornecedor = pedidosFornecedor.OrderBy(x => x.idPedidoFornecedor).ToList();

        if (ddlordenarpor.SelectedItem.Value == "fornecedorNome")
            pedidosFornecedor = pedidosFornecedor.OrderBy(x => x.fornecedorNome).ToList();

        if (ddlordenarpor.SelectedItem.Value == "Custo")
            pedidosFornecedor = pedidosFornecedor.OrderBy(x => x.custo).ToList();

        if (ddlordenarpor.SelectedItem.Value == "dataPedido")
            pedidosFornecedor = pedidosFornecedor.OrderBy(x => x.dataPedido).ToList();

        if (ddlordenarpor.SelectedItem.Value == "dataPagamento")
            pedidosFornecedor = pedidosFornecedor.OrderBy(x => x.dataPagamento).ToList();



        return pedidosFornecedor;
        //GridView1.DataSource = pedidosFornecedor;
        //GridView1.DataBind();


        //btnMarcarTodosComoPago.Visible = grd.VisibleRowCount > 0;

    }
    bool validarcampos()
    {
        string erro = "";
        bool valido = true;
        if (!String.IsNullOrEmpty(txtIdPedidoFornecedor.Text))
        {
            try { int teste = Convert.ToInt32(txtIdPedidoFornecedor.Text); }
            catch (Exception)
            {
                erro += @"Id pedido fornecedor invalido\n";
                valido = false;
            }
        }

        if (!String.IsNullOrEmpty(txtDataInicial.Text))
        {
            try { DateTime teste = Convert.ToDateTime(txtDataInicial.Text); }
            catch (Exception)
            {
                erro += @"Data inicial invalida\n";
                valido = false;
            }
        }

        if (!String.IsNullOrEmpty(txtDataFinal.Text))
        {
            try { DateTime teste = Convert.ToDateTime(txtDataFinal.Text); }
            catch (Exception)
            {
                erro += @"Data final invalida\n";
                valido = false;
            }
        }

        if (erro != "")
        {
            string meuscript = @"alert('" + erro + "');";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "MyScript", meuscript, true);

        }
        return valido;
    }
    protected void btnFiltrar_Click(object sender, EventArgs e)
    {
        if (validarcampos())
            carregaGrid();
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            string idPedidoFornecedor = DataBinder.Eval(e.Row.DataItem, "idPedidoFornecedor").ToString();
            HiddenField hdidPedidoFornecedor = (HiddenField)e.Row.FindControl("hdidPedidoFornecedor");
            hdidPedidoFornecedor.Value = idPedidoFornecedor;

            HtmlAnchor linkeditar = (HtmlAnchor)e.Row.FindControl("linkeditar");
            linkeditar.HRef = "pedidoFornecedorEditar.aspx?idPedidoFornecedor=" + idPedidoFornecedor;

            bool pago = false;
            try { pago = !String.IsNullOrEmpty(DataBinder.Eval(e.Row.DataItem, "dataPagamento").ToString()); } catch (Exception ex) { }
            CheckBox ckbPago = (CheckBox)e.Row.FindControl("ckbPago");
            ckbPago.Checked = pago;
            if (pago)
                ckbPago.Enabled = false;
            else
                ckbPago.Enabled = true;

            Label lblDatasEntrega = (Label)e.Row.FindControl("lblDatasEntrega");
            int idPedidoFornecedorInt = Convert.ToInt32(idPedidoFornecedor);
            lblDatasEntrega.Text = datasEntrega(idPedidoFornecedorInt);
        }
    }
    protected string datasEntrega(int idPedidoFornecedor)
    {
        string retorno = "";
        var data = new dbCommerceDataContext();
        var listaDatas = (from c in data.tbPedidoFornecedorItems
                          where c.idPedidoFornecedor == idPedidoFornecedor && c.entregue && c.dataEntrega != null
                          select c.dataEntrega).ToList().OrderBy(x => x).Select(x => ((DateTime)x).ToString("MM/yyyy")).Distinct();
        retorno = String.Join(", ", listaDatas);
        return retorno;
    }
    protected void btnExportarPlanilha_Click(object sender, EventArgs e)
    {
        if (validarcampos())
        {
            List<pedidoFornecedor> itens = getDados();
            SLDocument sl = new SLDocument();
            SLStyle style1 = sl.CreateStyle();
            style1.Font.Bold = true;
            sl.SetRowStyle(1, style1);

            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("Content-Disposition", "attachment; filename=relatorio.xlsx");

            sl.SetCellValue("A1", "idPedidoFornecedor");
            sl.SetCellValue("B1", "Fornecedor Nome");
            sl.SetCellValue("C1", "Custo");
            sl.SetCellValue("D1", "Data do Pedido");
            sl.SetCellValue("E1", "Datas De Entrega");
            sl.SetCellValue("F1", "Data de Pagamento");
            sl.SetCellValue("G1", "Pago");
            int linha = 2;
            foreach (var item in itens)
            {
                sl.SetCellValue(linha, 1, item.idPedidoFornecedor.ToString());
                sl.SetCellValue(linha, 2, item.fornecedorNome);
                sl.SetCellValue(linha, 3, Convert.ToDecimal(item.custo).ToString("C"));
                sl.SetCellValue(linha, 4, Convert.ToDateTime(item.dataPedido).ToShortDateString());
                sl.SetCellValue(linha, 5, datasEntrega(item.idPedidoFornecedor));
                sl.SetCellValue(linha, 6, Convert.ToDateTime(item.dataPagamento).ToShortDateString());
                sl.SetCellValue(linha, 7, item.pago.ToString());
                linha++;
            }
            sl.SetRowStyle(linha, style1);
            sl.SetCellValue(linha, 1, itens.Count.ToString());
            sl.SetCellValue(linha, 3, Convert.ToDecimal(itens.Sum(x => x.custo)).ToString("C"));

            sl.SaveAs(Response.OutputStream);
            Response.End();
        }

    }




    protected void imgSalvar_Click(object sender, ImageClickEventArgs e)
    {
        var data = new dbCommerceDataContext();
        foreach (GridViewRow row in GridView1.Rows)
        {
            //string idPedidoFornecedor = DataBinder.Eval(e.Row.DataItem, "idPedidoFornecedor").ToString();
            HiddenField hdidPedidoFornecedor = (HiddenField)row.FindControl("hdidPedidoFornecedor");
            int idPedidoFornecedor = Convert.ToInt32(hdidPedidoFornecedor.Value);

            HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
            usuarioLogadoId = Request.Cookies["usuarioLogadoId"];

            string interacao = "informada data de pagamento da cobrança";

            if (((CheckBox)row.FindControl("ckbPago")).Checked)
            {
                var pedidoFornecedor =
                       (from c in data.tbPedidoFornecedors where c.idPedidoFornecedor == idPedidoFornecedor select c)
                           .First();
                if (pedidoFornecedor != null)
                {
                    if (pedidoFornecedor.dataPagamento == null)
                    {
                        pedidoFornecedor.dataPagamento = DateTime.Now;
                        var interacaoObj = new tbPedidoFornecedorInteracao();
                        interacaoObj.data = DateTime.Now;
                        interacaoObj.idPedidoFornecedor = idPedidoFornecedor;
                        interacaoObj.interacao = interacao;
                        interacaoObj.usuario = rnUsuarios.usuarioSeleciona_PorUsuarioId(int.Parse(usuarioLogadoId.Value.ToString())).Tables[0].Rows[0]["usuarioNome"].ToString();
                        data.tbPedidoFornecedorInteracaos.InsertOnSubmit(interacaoObj);
                    }
                }

            }

        }
        data.SubmitChanges();
        carregaGrid();
    }
}