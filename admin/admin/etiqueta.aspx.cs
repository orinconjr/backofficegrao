﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class admin_etiqueta : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
        usuarioLogadoId = Request.Cookies["usuarioLogadoId"];
        if (usuarioLogadoId == null)
        {
            Response.Redirect("../../Default.aspx");
        }

        if (!Page.IsPostBack)
        {

            int pedidoId = Convert.ToInt32(Request.QueryString["pedidoId"]);
            var pedidosDc = new dbCommerceDataContext();

            lblPedidoIdInterno.Text = rnFuncoes.retornaIdCliente(pedidoId).ToString();
            imgCodigoDeBarras.ImageUrl = string.Format("../BarCode.ashx?code={0}&ShowText={1}",
                                            rnFuncoes.retornaIdCliente(pedidoId).ToString(),
                                            0);

            /*tbItensPedido.itemPedidoId, tbItensPedido.pedidoId, tbProdutos.produtoNome, tbItensPedido.produtoId, tbItensPedido.itemQuantidade, tbItensPedido.itemPresente, tbItensPedido.valorCusto, 
                    tbItensPedido.garantiaId, tbItensPedido.ItemMensagemDoCartao, tbItensPedido.itemValor, tbProdutos.produtoId AS Expr1, 
                    tbProdutos.produtoIdDaEmpresa, tbProdutos.produtoCodDeBarras, tbProdutoFornecedor.fornecedorNome, tbProdutos.produtoPrecoDeCusto, tbItensPedido.entreguePeloFornecedor, tbItensPedido.prazoDeFabricacao,
tbProdutos.ncm, tbProdutos.cfop, tbProdutos.complementoIdDaEmpresa*/
            sqlItensPedido.SelectParameters.Add("pedidoId", pedidoId.ToString());
            sqlInteracaoes.SelectParameters.Add("pedidoId", pedidoId.ToString());

            dtlItensPedido.DataSource = rnPedidos.retornaProdutosEtiqueta(pedidoId);
            dtlItensPedido.DataBind();
        }
    }


    protected void DataList1_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            int produtoId = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "produtoId"));
            string codigoDeBarras = rnProdutos.produtoSeleciona_PorProdutoId(produtoId).Tables[0].Rows[0]["produtoCodDeBarras"].ToString();
            decimal produtoPreco = Convert.ToDecimal(rnProdutos.produtoSeleciona_PorProdutoId(produtoId).Tables[0].Rows[0]["produtoPreco"].ToString());
            decimal produtoPrecoDeCusto = Convert.ToDecimal(rnProdutos.produtoSeleciona_PorProdutoId(produtoId).Tables[0].Rows[0]["produtoPrecoDeCusto"].ToString());
            decimal produtoPrecoPromocional = Convert.ToDecimal(rnProdutos.produtoSeleciona_PorProdutoId(produtoId).Tables[0].Rows[0]["produtoPrecoPromocional"].ToString());
            decimal valorDeVenda = produtoPrecoPromocional > 0 ? produtoPrecoPromocional : produtoPreco;
            Label lblPrecoDeCusto = (Label)e.Item.FindControl("lblPrecoDeCusto");
            Label lblPrecoDeVenda = (Label)e.Item.FindControl("lblPrecoDeVenda");
            Label lblMargem = (Label)e.Item.FindControl("lblMargem");
            Label lblCodigoDeBarras = (Label)e.Item.FindControl("lblCodigoDeBarras");
            ListView lstCombo = (ListView)e.Item.FindControl("lstCombo");
            lblCodigoDeBarras.Text = codigoDeBarras;

            lstCombo.DataSource = rnProdutos.produtoSelecionaCombo_porProdutoId(produtoId);
            lstCombo.DataBind();

            //lblPrecoDeCusto.Text = "R$ " + produtoPrecoDeCusto.ToString("0.00");
            //lblPrecoDeVenda.Text = "R$ " + valorDeVenda.ToString("0.00");
            //lblMargem.Text = (((valorDeVenda*100)/produtoPrecoDeCusto) - 100).ToString("0.##") + "%";


            /*Label lblQuantidade = (Label)e.Item.FindControl("lblQuantidade");
            Label lblPreco = (Label)e.Item.FindControl("lblPreco");

            decimal itemQuantidade = decimal.Parse(lblQuantidade.Text);

            lblPrecoTotal.Text = String.Format("{0:c}", decimal.Parse((produtoPreco * itemQuantidade).ToString()));*/
        }
    }

    protected void btnImpressaoConcluida_OnClick(object sender, EventArgs e)
    {
        Response.Redirect("~/envio/Default.aspx");
    }


}