﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_produtosAguardandoEnderecamento : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //fillGrid(false);
    }

    private void fillGrid(bool rebind)
    {
        //grd.DataSource = sql;
        var data = new dbCommerceDataContext();

        var carrinhos = (from c in data.tbProdutoEstoques
                         join d in data.tbTransferenciaEnderecoProdutos on c.idPedidoFornecedorItem equals d.idPedidoFornecedorItem into carrinho
                         where c.enviado == false && (c.liberado ?? false) == false
                         
                         orderby c.dataEntrada
                         select new
                         {
                             c.idPedidoFornecedorItem,
                             c.tbProduto.produtoNome,
                             c.produtoId,
                             c.tbPedidoFornecedorItem.idPedidoFornecedor,
                             c.dataEntrada,
                             carrinhoAtual = carrinho.DefaultIfEmpty().FirstOrDefault().idTransferenciaEndereco
                             //carrinhoAtual =  ""
                         }).ToList();
        var idsCarrinhosLista = carrinhos.Select(x => x.carrinhoAtual).Distinct().ToList();
        var carrinhosAbertos = (from c in data.tbTransferenciaEnderecos
                                where idsCarrinhosLista.Contains(c.idTransferenciaEndereco)
                                select new
                                {
                                    c.idTransferenciaEndereco,
                                    c.tbEnderecamentoTransferencia.enderecamentoTransferencia
                                }).ToList();

        grd.DataSource = carrinhos;
        if ((!Page.IsPostBack && !Page.IsCallback) | rebind) grd.DataBind();
    }

    protected void rdbAberto_OnCheckedChanged(object sender, EventArgs e)
    {
        //fillGrid(true);
    }


    protected void sqlCarinhos_Selecting(object sender, DevExpress.Data.Linq.LinqServerModeDataSourceSelectEventArgs e)
    {

        var data = new dbCommerceDataContext();
        e.KeyExpression = "idPedidoFornecedorItem";
    }
}