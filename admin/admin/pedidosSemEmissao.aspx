﻿<%@ Page Title="" Theme="Glass" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="pedidosSemEmissao.aspx.cs" Inherits="admin_pedidosSemEmissao" %>

<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxTimer" TagPrefix="dxt" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxPopupControl" Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxEditors" Assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>

<%@ Register assembly="DevExpress.Web.v11.1" namespace="DevExpress.Web.ASPxPopupControl" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" runat="Server">
    <dx:ASPxPopupControl ID="popEnderecamento" runat="server" CloseAction="CloseButton" Modal="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
        ClientInstanceName="popEnderecamento" HeaderText="Histótico de Endereçamento" AllowDragging="True" PopupAnimationType="None" EnableViewState="True" Height="300" AutoUpdatePosition="True">
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl6" runat="server">
                <div style="width: 800px; height: 600px;">
                    <table width="100%">
                        <tr class="rotulos" align="center">
                            <td class="rotulos">
                                <asp:GridView ID="grdHistoricoEndereco" runat="server" Width="800px" Border="0" BorderWidth="0" BorderStyle="None" EmptyDataText="Nenhum registro encontrado" DataKeyNames="idPedidoPacote" AutoGenerateColumns="false">
                                    
                                    <HeaderStyle  BorderWidth="0" BorderStyle="None"  />
                                    <AlternatingRowStyle BackColor="#c0c0c0" />
                                    <Columns>
                                        <asp:BoundField HeaderText="Data Hora" DataField="dataHora" ItemStyle-BorderWidth="0" ItemStyle-Width="150px" />
                                        <asp:BoundField HeaderText="Usuário" DataField="nome" ItemStyle-Width="150px" ItemStyle-BorderWidth="0" />
                                        <asp:BoundField HeaderText="Endereço" DataField="enderecoPacote" ItemStyle-BorderWidth="0" ItemStyle-Width="500px" />
                                        <asp:BoundField HeaderText="Volume" DataField="numeroVolume" ItemStyle-BorderWidth="0" ItemStyle-Width="50px" />
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </div>
            </dx:PopupControlContentControl>
        </ContentCollection>
        <ContentStyle>
            <Paddings PaddingBottom="5px" />
        </ContentStyle>
    </dx:ASPxPopupControl>
    <table cellpadding="0" cellspacing="0" style="width: 920px">
        <tr>
            <td class="tituloPaginas" valign="top">
                <asp:Label ID="lblAcao" runat="server">Pedidos Sem Emissão</asp:Label>
            </td>
        </tr>
        <tr>
            <td>

                <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
                    <tr>
                        <td>
                            <dxwgv:ASPxGridView ID="grd" runat="server" AutoGenerateColumns="False" Width="834px" Cursor="auto" OnHtmlRowCreated="gridClientes_HtmlRowCreated"
                                OnCustomUnboundColumnData="grd_OnCustomUnboundColumnData" KeyFieldName="pedidoId" EnableViewState="false">
                                <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                                <Styles>
                                    <Footer Font-Bold="True">
                                    </Footer>
                                </Styles>
                                <SettingsEditing EditFormColumnCount="4"
                                    PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1"
                                    PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter"
                                    PopupEditFormWidth="700px" />
                                <SettingsText ConfirmDelete="Tem certeza que deseja excluir?" EmptyDataRow="Nenhum registro encontrado." />
                                <SettingsPager Position="TopAndBottom" PageSize="100" ShowDisabledButtons="False" AlwaysShowPager="True">
                                    <Summary AllPagesText="Páginas: {0} - {1} ({2} items)" Text="Página {0} de {1} ({2} registros encontrados)" />
                                </SettingsPager>
                                <Settings ShowFilterRow="True" ShowGroupButtons="False" ShowHeaderFilterButton="True" ShowFooter="True" />
                                <SettingsEditing EditFormColumnCount="4" PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1" PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter" PopupEditFormWidth="700px" />
                                <Columns>
                                    <dxwgv:GridViewDataTextColumn Caption="#" VisibleIndex="0" Width="50">
                                        <DataItemTemplate>
                                            <%# Container.VisibleIndex + 1 %>
                                        </DataItemTemplate>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="ID" FieldName="pedidoId" VisibleIndex="0" Width="50">
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataDateColumn Caption="Data Pedido" FieldName="dataHoraDoPedido" VisibleIndex="0">
                                        <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy hh:mm:ss">
                                        </PropertiesDateEdit>
                                    </dxwgv:GridViewDataDateColumn>
                                    <dxwgv:GridViewDataDateColumn Caption="Data Embalagem" FieldName="dataFimEmbalagem" VisibleIndex="0">
                                        <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy hh:mm:ss">
                                        </PropertiesDateEdit>
                                    </dxwgv:GridViewDataDateColumn>
                                    <dxwgv:GridViewDataDateColumn Caption="Data Atualizada" FieldName="prazoMaximoPostagemAtualizado" VisibleIndex="0">
                                        <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy hh:mm:ss">
                                        </PropertiesDateEdit>
                                    </dxwgv:GridViewDataDateColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Transportadora" FieldName="transportadora" VisibleIndex="0">
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataColumn Caption="Volumes" FieldName="numeroVolume" VisibleIndex="0"></dxwgv:GridViewDataColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Tempo" FieldName="dif" VisibleIndex="0">
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataHyperLinkColumn Caption="Editar" FieldName="pedidoId"
                                        VisibleIndex="7" Width="30px">
                                        <PropertiesHyperLinkEdit ImageUrl="~/admin/images/btEditar.jpg"
                                            NavigateUrlFormatString="pedido.aspx?pedidoId={0}" Target="_blank">
                                        </PropertiesHyperLinkEdit>
                                        <Settings AllowAutoFilter="False" />
                                        <HeaderTemplate>
                                            <img alt="" src="images/legendaEditar.jpg" />
                                        </HeaderTemplate>
                                    </dxwgv:GridViewDataHyperLinkColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Endereçamento" Name="Enderecamento" VisibleIndex="0">
                                        <DataItemTemplate>
                                            <asp:LinkButton ID="lnkEnderecamento" CommandArgument='<%# Bind("pedidoId") %>' OnCommand="lnkEnderecamento_Command" runat="server" Text="Endereçamento" ></asp:LinkButton>
                                        </DataItemTemplate>
                                    </dxwgv:GridViewDataTextColumn>
                                </Columns>
                                <StylesEditors>
                                    <Label Font-Bold="True">
                                    </Label>
                                </StylesEditors>
                            </dxwgv:ASPxGridView>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
