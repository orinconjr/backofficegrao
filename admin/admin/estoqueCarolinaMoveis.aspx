﻿<%@ Page Title="" Language="C#" Theme="Glass" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="estoqueCarolinaMoveis.aspx.cs" Inherits="admin_estoqueCarolinaMoveis" %>

<%@ Register TagPrefix="dxwgv" Namespace="DevExpress.Web.ASPxGridView" Assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>
<%@ Register TagPrefix="dxwgv" Namespace="DevExpress.Web.ASPxGridView.Export" Assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" runat="Server">

    <table cellpadding="0" cellspacing="0" style="width: 920px">
        <tr>
            <td class="tituloPaginas" valign="top">
                <asp:Label ID="lblAcao" runat="server">Produtos em Estoque Carolina Movéis</asp:Label>
            </td>
        </tr>

        <tr>
            <td>

                <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="right">
                            <table cellpadding="0" cellspacing="0" style="width: 834px">
                                <tr>
                                    <td align="right">&nbsp;</td>
                                    <td height="38"
                                        style="background-image: url('images/bkExportar.jpg'); padding-bottom: 1px;"
                                        valign="bottom" width="231">
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 97px">&nbsp;</td>
                                                <td style="width: 33px">
                                                    <asp:ImageButton ID="btPdf" Visible="False" runat="server" ImageUrl="~/admin/images/btPdf.jpg" OnClick="btPdf_Click"
                                                        Style="margin-left: 5px" />
                                                </td>
                                                <td style="width: 31px">
                                                    <asp:ImageButton ID="btXsl" runat="server" ImageUrl="~/admin/images/btXls.jpg" OnClick="btXsl_Click"
                                                        Style="margin-left: 5px" />
                                                </td>
                                                <td style="width: 36px">
                                                    <asp:ImageButton ID="btRtf" Visible="False" runat="server" ImageUrl="~/admin/images/btRtf.jpg" OnClick="btRtf_Click"
                                                        Style="margin-left: 5px" />
                                                </td>
                                                <td valign="bottom">
                                                    <asp:ImageButton ID="btCsv" Visible="False" runat="server" ImageUrl="~/admin/images/btCsv.jpg" OnClick="btCsv_Click"
                                                        Style="margin-left: 5px" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <dxwgv:ASPxGridView Settings-ShowHorizontalScrollBar="True" DataSourceID="sql" ID="grd" runat="server" AutoGenerateColumns="False" Width="840px" Cursor="auto"
                                KeyFieldName="produtoId" OnCustomUnboundColumnData="grd1_OnCustomUnboundColumnData" Settings-ShowVerticalScrollBar="True" Settings-VerticalScrollableHeight="400">
                                <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                                <Styles>
                                    <Footer Font-Bold="True">
                                    </Footer>
                                </Styles>
                                <SettingsText ConfirmDelete="Tem certeza que deseja excluir?" EmptyDataRow="Nenhum registro encontrado." />
                                <SettingsPager Position="TopAndBottom" PageSize="50" ShowDisabledButtons="False" AlwaysShowPager="True">
                                    <Summary AllPagesText="Páginas: {0} - {1} ({2} items)" Text="Página {0} de {1} ({2} registros encontrados)"></Summary>
                                </SettingsPager>
                                <Settings ShowFilterRow="True" ShowGroupButtons="False" ShowHeaderFilterButton="True" ShowFooter="True" />
                                <SettingsEditing EditFormColumnCount="4" PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1" PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter" PopupEditFormWidth="700px" />
                                <Columns>
                                    <dxwgv:GridViewDataTextColumn Caption="ID" FieldName="produtoId" VisibleIndex="0" Width="50">
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Produto" FieldName="produtoNome" VisibleIndex="0" Width="250">
                                        <Settings AutoFilterCondition="Contains" />
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="ID da Empresa" FieldName="produtoIdDaEmpresa" VisibleIndex="0" Width="100">
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Complemento ID" FieldName="complementoIdDaEmpresa" VisibleIndex="0" Width="100" Visible="False">
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Pedidos" FieldName="itensPedidoNaoEnviados" VisibleIndex="0" Visible="False" Width="100px">
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Disponível" FieldName="estoqueLivre" VisibleIndex="0" Width="100px">
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Encomendas" FieldName="encomendas" VisibleIndex="0" Width="100px" Visible="False">
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Estoque Real" FieldName="estoqueReal" VisibleIndex="0" Width="100px" Visible="False">
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Estoque Previsto" FieldName="estoquePrevisto" VisibleIndex="0" Width="100px" Visible="False">
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Aguardando Pagamento" FieldName="aguardandoConfirmacaoPagamento" VisibleIndex="0" Width="160px">
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Em Analise ClearSale" FieldName="pedidosEmAnalise" VisibleIndex="0" Width="140px">
                                    </dxwgv:GridViewDataTextColumn>
                                </Columns>
                                <Templates>
                                    <DetailRow>
                                        <dxwgv:ASPxGridView ID="grdDetalhes" runat="server" AutoGenerateColumns="False" DataSourceID="sqlDetalhes" Width="900px" OnCustomUnboundColumnData="grd_OnCustomUnboundColumnData"
                                            Cursor="auto" EnableCallBacks="False" OnBeforePerformDataSelect="grdDetalhes_BeforePerformDataSelect">
                                            <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                                            <SettingsText ConfirmDelete="Tem certeza que deseja excluir?"
                                                EmptyDataRow="Nenhum registro encontrado." />
                                            <SettingsPager Position="TopAndBottom" PageSize="50"
                                                ShowDisabledButtons="False" AlwaysShowPager="True">
                                                <Summary AllPagesText="Páginas: {0} - {1} ({2} items)"
                                                    Text="Página {0} de {1} ({2} registros encontrados)" />
                                            </SettingsPager>
                                            <Settings ShowFilterRow="True" ShowGroupButtons="False" />
                                            <SettingsEditing EditFormColumnCount="4"
                                                PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1"
                                                PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter"
                                                PopupEditFormWidth="700px" />
                                            <Columns>
                                                <dxwgv:GridViewDataTextColumn Caption="Id Cliente" FieldName="pedidoIdCliente" VisibleIndex="0" UnboundType="Integer">
                                                    <EditFormSettings Visible="False" />
                                                </dxwgv:GridViewDataTextColumn>
                                                <dxwgv:GridViewDataTextColumn Caption="Id do Pedido" FieldName="pedidoId"
                                                    VisibleIndex="0">
                                                    <EditFormSettings Visible="False" />
                                                </dxwgv:GridViewDataTextColumn>
                                                <dxwgv:GridViewDataTextColumn Caption="Nome" FieldName="clienteNome"
                                                    VisibleIndex="1" Settings-AutoFilterCondition="Contains">
                                                    <PropertiesTextEdit>
                                                        <ValidationSettings>
                                                            <RequiredField ErrorText="Preencha o nome." IsRequired="True" />
                                                        </ValidationSettings>
                                                    </PropertiesTextEdit>
                                                    <Settings AutoFilterCondition="Contains" />
                                                    <EditFormSettings CaptionLocation="Top" />
                                                </dxwgv:GridViewDataTextColumn>
                                                <dxwgv:GridViewDataTextColumn Caption="Valor" FieldName="valorCobrado"
                                                    VisibleIndex="2" Settings-AutoFilterCondition="Contains" Settings-FilterMode="DisplayText">
                                                    <PropertiesTextEdit DisplayFormatString="c">
                                                    </PropertiesTextEdit>
                                                    <EditFormSettings Visible="False" />
                                                </dxwgv:GridViewDataTextColumn>
                                                <dxwgv:GridViewDataDateColumn Caption="Data" FieldName="dataHoraDoPedido"
                                                    UnboundType="DateTime" VisibleIndex="3">
                                                    <PropertiesDateEdit DisplayFormatString="" EditFormat="DateTime">
                                                        <CalendarProperties ClearButtonText="Apagar" TodayButtonText="Hoje">
                                                        </CalendarProperties>
                                                    </PropertiesDateEdit>
                                                    <Settings GroupInterval="Date" />
                                                </dxwgv:GridViewDataDateColumn>
                                                <dxwgv:GridViewDataTextColumn Caption="Situação" FieldName="situacao" VisibleIndex="3">
                                                </dxwgv:GridViewDataTextColumn>
                                                <dxwgv:GridViewDataComboBoxColumn Caption="Pagamento" VisibleIndex="3" Width="55px" FieldName="condicaoNome">
                                                    <PropertiesComboBox DataSourceID="sqlPagamentos" TextField="condicaoNome" ValueField="condicaoId" ValueType="System.String">
                                                    </PropertiesComboBox>
                                                    <Settings AllowAutoFilter="True" AllowAutoFilterTextInputTimer="True" FilterMode="DisplayText" />
                                                    <DataItemTemplate>
                                                        <asp:Image ID="imgPagamento" runat="server" Height="35px" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings["caminhoVirtual"] + Eval("imagem") %>' Width="50px" />
                                                    </DataItemTemplate>
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dxwgv:GridViewDataComboBoxColumn>
                                                <dxwgv:GridViewDataTextColumn Caption="Parcelas pagas" FieldName="parcelasPagas" VisibleIndex="3">
                                                </dxwgv:GridViewDataTextColumn>
                                                <dxwgv:GridViewDataTextColumn Caption="Parcelas pendentes" FieldName="parcelasPendentes" VisibleIndex="3">
                                                </dxwgv:GridViewDataTextColumn>
                                                <dxwgv:GridViewDataTextColumn Caption="Pedido Fornecedor" FieldName="idPedidoFornecedor" UnboundType="String" VisibleIndex="3">
                                                </dxwgv:GridViewDataTextColumn>
                                                <dxwgv:GridViewDataTextColumn Caption="Reserva" FieldName="reserva" UnboundType="String" VisibleIndex="3">
                                                </dxwgv:GridViewDataTextColumn>
                                                <dxwgv:GridViewDataHyperLinkColumn Caption="Editar" FieldName="pedidoId"
                                                    VisibleIndex="7" Width="30px">
                                                    <PropertiesHyperLinkEdit ImageUrl="~/admin/images/btEditar.jpg"
                                                        NavigateUrlFormatString="pedido.aspx?pedidoId={0}" Target="_blank">
                                                    </PropertiesHyperLinkEdit>
                                                    <Settings AllowAutoFilter="False" />
                                                    <HeaderTemplate>
                                                        <img alt="" src="images/legendaEditar.jpg" />
                                                    </HeaderTemplate>
                                                </dxwgv:GridViewDataHyperLinkColumn>
                                            </Columns>
                                            <StylesEditors>
                                                <Label Font-Bold="True">
                                                </Label>
                                            </StylesEditors>
                                        </dxwgv:ASPxGridView>
                                    </DetailRow>
                                </Templates>
                                <SettingsDetail AllowOnlyOneMasterRowExpanded="True" ShowDetailRow="True" />
                                <StylesEditors>
                                    <Label Font-Bold="True">
                                    </Label>
                                </StylesEditors>
                            </dxwgv:ASPxGridView>
                            <asp:ObjectDataSource ID="sql" runat="server" SelectMethod="produtoEstoqueReal" TypeName="rnProdutos"></asp:ObjectDataSource>
                            <asp:SqlDataSource runat="server" ID="sqlDetalhes" ConnectionString="<%$ ConnectionStrings:connectionString %>" SelectCommand="SELECT     tbPedidos.pedidoId, tbPedidos.condDePagamentoId, tbPedidos.valorCobrado, tbPedidos.dataHoraDoPedido, tbClientes.clienteNome, 
                                                          tbClientes.clienteNomeDaEmpresa, tbClientes.clienteEmail, tbClientes.clienteCPFCNPJ, tbClientes.clienteRGIE, tbClientes.clienteDataNascimento, 
                                                          tbClientes.clienteFoneResidencial, tbClientes.clienteFoneComercial, tbClientes.clienteFoneCelular, tbClientes.clienteCep, tbClientes.clienteRua, 
                                                          tbClientes.clienteBairro, tbClientes.clienteNumero, tbClientes.clienteComplemento, tbClientes.clienteCidade, tbClientes.clienteEstado, tbClientes.clientePais, 
                                                          tbClientes.clienteReferenciaParaEntrega, tbPedidos.tipoDeEntregaId, tbPedidos.statusDoPedido, tbPedidoSituacao.situacao, tbCondicoesDePagamento.imagem, 
                                                          tbTipoDeEntrega.tipoDeEntregaNome, tbCondicoesDePagamento.condicaoNome, tbCondicoesDePagamento.imagemMini, tbPedidos.moipStatus, tbPedidos.statusInternoPedido as situacaoInternaId,
                                                          (select count (idPedidoPagamento) from tbPedidoPagamento where pago = 1 and cancelado = 0 and pedidoId = tbPedidos.pedidoId) as parcelasPagas,
                                                          (select count (idPedidoPagamento) from tbPedidoPagamento where pago = 0 and cancelado = 0 and pedidoId = tbPedidos.pedidoId) as parcelasPendentes
                                    FROM         tbPedidos INNER JOIN
                                                          tbClientes ON tbPedidos.clienteId = tbClientes.clienteId INNER JOIN
                                                          tbPedidoSituacao ON tbPedidos.statusDoPedido = tbPedidoSituacao.situacaoId INNER JOIN
                                                          tbCondicoesDePagamento ON tbPedidos.condDePagamentoId = tbCondicoesDePagamento.condicaoId INNER JOIN
                                                          tbTipoDeEntrega ON tbPedidos.tipoDeEntregaId = tbTipoDeEntrega.tipoDeEntregaId
                                    where pedidoId in 
                                    (select tbPedidos.pedidoId from tbPedidos
                                    join tbItensPedido on tbPedidos.pedidoId = tbItensPedido.pedidoId and tbPedidos.statusDoPedido = 2
                                    join tbItemPedidoEstoque on tbItemPedidoEstoque.itemPedidoId = tbItensPedido.itemPedidoId
                                    where tbItemPedidoEstoque.produtoId = @produtoId and tbItemPedidoEstoque.cancelado = 0 and tbItemPedidoEstoque.enviado = 0)">
                                <SelectParameters>
                                    <asp:SessionParameter Name="produtoId" SessionField="produtoId" Type="Int32" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                            <asp:ObjectDataSource ID="sqlPagamentos" runat="server" SelectMethod="condicaoDePagamentoSeleciona"
                                TypeName="rnCondicoesDePagamento"></asp:ObjectDataSource>
                            <asp:LinqDataSource ID="sqlFornecedores" runat="server" ContextTypeName="dbCommerceDataContext" TableName="tbProdutoFornecedors">
                            </asp:LinqDataSource>
                            <dxwgv:ASPxGridViewExporter ID="grdEx" runat="server" FileName="pedidosCarolinaMoveis"
                                GridViewID="grd" Landscape="True" PreserveGroupRowStates="True">
                            </dxwgv:ASPxGridViewExporter>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

</asp:Content>

