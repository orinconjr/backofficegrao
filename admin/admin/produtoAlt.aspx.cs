﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using Amazon.S3;
using Amazon.S3.Model;
using DevExpress.Web.ASPxGridView;

public partial class admin_produtoAlt : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {

            //tenho bindar os dados no drop antes do montar form para poder selecionar o item
            var data = new dbCommerceDataContext();

            drpFornecedor.DataBind();
            drpMarca.DataBind();
            dtlEspecificacoes.DataBind();

            int produtoId;
            if (!string.IsNullOrEmpty(Request.QueryString["produtoId"]) && !string.IsNullOrEmpty(Request.QueryString["produtoPaiId"]))
            {


                produtoId = int.Parse((Request.QueryString["produtoId"].ToString()));
                montaForm(produtoId);
                fillRelacionados();
                fillPersonalizadosRelacionados();

                var produtoPaiId = Convert.ToInt32(lblIdProdutoPai.Text);
                var especificacoesAtuais = (from c in data.tbJuncaoProdutoEspecificacaos
                                            where c.produtoId == produtoId | c.produtoId == produtoPaiId
                                            select c.especificacaoId).Distinct().ToList();
                var especificacoes = (from c in data.tbEspecificacaos
                                      where !especificacoesAtuais.Contains(c.especificacaoId)
                                      orderby c.especificacaoNomeInterno
                                      select c);
                ddlEspecificacaoGrupo.DataSource = especificacoes;
                ddlEspecificacaoGrupo.DataBind();
            }
            else
            {
                Response.Write("<script>alert('Selecione algum produto.');</script>");
                Response.Write("<script>history.back();</script>");
            }

            exibeOcultaObjeto();
            verificaAcao();
            PopulateRootLevel();
            atribuiJs();

            treeCategorias.CollapseAll();
            treeCategoriasComFilhos.CollapseAll();

            var usuarioLogadoId = Request.Cookies["usuarioLogadoId"];
            if (usuarioLogadoId != null)
            {
                txtProdutoPrecoDeCusto.ReadOnly = rnUsuarios.usuarioPaginasPermitidaSeleciona_PorUsuarioIdPaginaPermitidaNome(int.Parse(usuarioLogadoId.Value), "alterarprecodecustodoproduto").Tables[0].Rows.Count == 0;
                btnExcluirProduto.Visible = rnUsuarios.usuarioPaginasPermitidaSeleciona_PorUsuarioIdPaginaPermitidaNome(int.Parse(usuarioLogadoId.Value), "excluirproduto").Tables[0].Rows.Count > 0;
            }

            CategoriasVinculasNomes();

        }
    }

    public void montaForm(int produtoId)
    {
        DataTable dtt = rnProdutos.produtoSeleciona_PorProdutoId(produtoId).Tables[0];

        lblIdProdutoPai.Text = dtt.Rows[0]["produtoPaiId"].ToString();
        txtProdutoIdDaEmpresa.Text = dtt.Rows[0]["produtoIdDaEmpresa"].ToString();
        txtComplementoId.Text = dtt.Rows[0]["complementoIdDaEmpresa"].ToString();
        txtProdutoCodDeBarras.Text = dtt.Rows[0]["produtoCodDeBarras"].ToString();
        txtProdutoNome.Text = dtt.Rows[0]["produtoNome"].ToString();
        txtProdutoNomeTitle.Text = dtt.Rows[0]["produtoNomeTitle"].ToString();
        txtProdutoNomeShopping.Text = dtt.Rows[0]["produtoNomeShopping"].ToString();
        txtProdutoNomeNovo.Text = dtt.Rows[0]["produtoNomeNovo"].ToString();
        txtProdutoTags.Text = dtt.Rows[0]["produtoTags"].ToString();
        txtNcm.Text = dtt.Rows[0]["ncm"].ToString();
        drpFornecedor.Items.FindByValue(dtt.Rows[0]["produtoFornecedor"].ToString()).Selected = true;
        drpMarca.Items.FindByValue(dtt.Rows[0]["produtoMarca"].ToString()).Selected = true;
        ckbAtivo.Checked = Convert.ToBoolean(dtt.Rows[0]["produtoAtivo"].ToString());
        //if (Request.QueryString["acao"].ToString() == "alterar")
        //    ckbPrincipal.Checked = Convert.ToBoolean(dtt.Rows[0]["produtoPrincipal"].ToString());
        ckbFreteGratis.Checked = Convert.ToBoolean(dtt.Rows[0]["produtoFreteGratis"].ToString());
        ckbLancamento.Checked = Convert.ToBoolean(dtt.Rows[0]["produtoLancamento"].ToString());
        ckbPreVenda.Checked = Convert.ToBoolean(dtt.Rows[0]["preVenda"].ToString());
        ckbPromocao.Checked = Convert.ToBoolean(dtt.Rows[0]["produtoPromocao"].ToString());
        ckbDestaque1.Checked = Convert.ToBoolean(dtt.Rows[0]["destaque1"].ToString());
        ckbDestaque2.Checked = Convert.ToBoolean(dtt.Rows[0]["destaque2"].ToString());
        ckbDestaque3.Checked = Convert.ToBoolean(dtt.Rows[0]["destaque3"].ToString());
        ckbMarketplaceCadastrar.Checked = Convert.ToBoolean(dtt.Rows[0]["marketplaceCadastrar"].ToString());
        ckbMarketplaceEnviarMarca.Checked = Convert.ToBoolean(dtt.Rows[0]["marketplaceEnviarMarca"].ToString());
        chkExclusivo.Checked = Convert.ToBoolean(dtt.Rows[0]["produtoExclusivo"].ToString());
        ddlSite.SelectedValue = dtt.Rows[0]["siteId"].ToString();
        txtIdProdutoAlternativo.Text = dtt.Rows[0]["produtoAlternativoId"].ToString();
        ckbBloqueado.Checked = Convert.ToBoolean(dtt.Rows[0]["bloqueado"].ToString());
        ckbProntaEntrega.Checked = Convert.ToBoolean(dtt.Rows[0]["prontaEntrega"].ToString());
        ckbForaDeLinha.Checked = Convert.ToBoolean(dtt.Rows[0]["foraDeLinha"].ToString());
        ckbCriacaoPropria.Checked = Convert.ToBoolean(dtt.Rows[0]["criacaoPropria"].ToString());
        ckbOcultarBusca.Checked = Convert.ToBoolean(dtt.Rows[0]["ocultarBusca"].ToString());
        ckbOcultarLista.Checked = Convert.ToBoolean(dtt.Rows[0]["ocultarLista"].ToString());
        ckbAtivoMercadoLivre.Checked = Convert.ToBoolean(dtt.Rows[0]["ativoMercadoLivre"].ToString());
        ckbAtivoB2w.Checked = Convert.ToBoolean(dtt.Rows[0]["ativoB2w"].ToString());
        ckbAtivoCnova.Checked = Convert.ToBoolean(dtt.Rows[0]["ativoCnova"].ToString());
        ckbAtivoWalmart.Checked = Convert.ToBoolean(dtt.Rows[0]["ativoWalmart"].ToString());
        ckbAtivoMagazineLuiza.Checked = Convert.ToBoolean(dtt.Rows[0]["ativoMagazineLuiza"].ToString());
        txtFoto360.Text = dtt.Rows[0]["foto360"].ToString();


        
        hdnUrlFoto360Mini.Value = System.Configuration.ConfigurationManager.AppSettings["caminhoCDN"] + "fotos/" + dtt.Rows[0]["produtoId"].ToString() + "/imagem360.jpg";
        btnAdd360Position.Visible = (txtFoto360.Text.Length > 0);

        carregaProdutoPai();




        bool exibirDiferencaCombo = false;
        bool.TryParse(dtt.Rows[0]["exibirDiferencaCombo"].ToString(), out exibirDiferencaCombo);
        ckbExibirDiferencaCombo.Checked = exibirDiferencaCombo;

        bool descontoProgressivoFrete = false;
        bool.TryParse(dtt.Rows[0]["descontoProgressivoFrete"].ToString(), out descontoProgressivoFrete);
        chbDescontoProgressivoFrete.Checked = descontoProgressivoFrete;

        bool cubagem = false;
        bool.TryParse(dtt.Rows[0]["cubagem"].ToString(), out cubagem);
        ckbCubagem.Checked = cubagem;

        txtProdutoDescricao.Text = dtt.Rows[0]["produtoDescricao"].ToString();
        txtProdutoEstoqueAtual.Text = dtt.Rows[0]["produtoEstoqueAtual"].ToString();
        txtProdutoEstoqueMinimo.Text = dtt.Rows[0]["produtoEstoqueMinimo"].ToString();
        txtProdutoPeso.Text = dtt.Rows[0]["produtoPeso"].ToString();
        txtProdutoPrecoDeCusto.Text = String.Format("{0:c}", dtt.Rows[0]["produtoPrecoDeCusto"]).ToString().Replace("R$", "").Trim();
        txtProdutoPreco.Text = String.Format("{0:c}", dtt.Rows[0]["produtoPreco"]).ToString().Replace("R$", "").Trim();
        txtProdutoPrecoPromocional.Text = String.Format("{0:c}", dtt.Rows[0]["produtoPrecoPromocional"]).ToString().Replace("R$", "").Trim();
        txtProdutoPrecoAtacado.Text = String.Format("{0:c}", dtt.Rows[0]["produtoPrecoAtacado"]).ToString().Replace("R$", "").Trim();
        txtProdutoQuantidadeMininaParaAtacado.Text = dtt.Rows[0]["produtoQuantidadeMininaParaAtacado"].ToString();
        txtProdutoLegendaAtacado.Text = dtt.Rows[0]["produtoLegendaAtacado"].ToString();
        txtDisponibilidadeEmEstoque.Text = dtt.Rows[0]["disponibilidadeEmEstoque"].ToString();
        drpComposicao.Items.FindByValue(dtt.Rows[0]["produtoComposicao"].ToString()).Selected = true;
        txtFios.Text = dtt.Rows[0]["produtoFios"].ToString();
        txtPecas.Text = dtt.Rows[0]["produtoPecas"].ToString();
        txtBrindes.Text = dtt.Rows[0]["produtoBrindes"].ToString();
        txtRelevancia.Text = dtt.Rows[0]["relevanciaManual"].ToString();
        txtAltura.Text = dtt.Rows[0]["altura"].ToString();
        txtLargura.Text = dtt.Rows[0]["largura"].ToString();
        txtProfundidade.Text = dtt.Rows[0]["profundidade"].ToString();
        txtIcms.Text = dtt.Rows[0]["icms"].ToString();
        ddlCd.SelectedValue = dtt.Rows[0]["idCentroDistribuicao"].ToString();

        DataTable dttGarantias = rnGarantiaEstendida.produtoAdminSeleciona_PorProdutoId(produtoId).Tables[0];
    }

    protected void verificaAcao()
    {
        if (Request.QueryString["acao"].ToString() == "incluir")
        {
            //Aqui é para duplicar
            lblAcao.Text = "Incluir produto";
            treeCategorias.Enabled = true;
            treeCategoriasComFilhos.Enabled = true;
            btn.Text = "Incluir";
            ckbPrincipal.Visible = true;
            btnGravarFotos.Visible = false;
        }
        if (Request.QueryString["acao"].ToString() == "duplicar")
        {
            //Aqui é para duplicar
            lblAcao.Text = "Duplicar produto (inclui dentro do grupo)";
            treeCategorias.Enabled = false;
            treeCategoriasComFilhos.Enabled = false;
            btn.Text = "Duplicar";
            ckbPrincipal.Checked = false;
            btnGravarFotos.Visible = false;
            txtNcm.Text = "";
            linkListaDb(Convert.ToInt32(Request.QueryString["produtoId"].ToString()));
        }
        if (Request.QueryString["acao"].ToString() == "alterar")
        {
            //Aqui é para alterar
            trFotos.Visible = true;
            lblAcao.Text = "Alterar produto";
            btn.Text = "Alterar";
            fillComentariosFace();
            //pnComentariosFacebook.Visible = true;
            linkListaDb(Convert.ToInt32(Request.QueryString["produtoId"].ToString()));
        }

        if (Request.QueryString["acao"].ToString() != "alterar")
            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "$('#trUpload').hide();", true);
    }

    #region Exibe e oculta
    protected void exibeOcultaObjeto()
    {
        if (ConfigurationManager.AppSettings["tipoDeLoja"] == "full")
        {
            //trEspecificacoes.Visible = true;
            trGarantiaEstendida.Visible = true;
        }
        else
        {
            //trEspecificacoes.Visible = false;
            trGarantiaEstendida.Visible = false;
        }
        if (string.IsNullOrEmpty(Request.QueryString["produtoPaiId"]))
        {
            trFotos.Visible = true;
        }
    }
    #endregion

    #region categorias
    private void PopulateRootLevel()
    {
        treeCategorias.Nodes.Clear();
        treeCategoriasComFilhos.Nodes.Clear();
        int idSite = 0;
        int.TryParse(ddlSite.SelectedValue, out idSite);
        if (idSite == 0) idSite = 1;

        PopulateNodes(rnCategorias.categoriasSelecionaNivelPaiPorSiteId(idSite).Tables[0].Select("childnodecount = 0 OR exibirSite = 1").CopyToDataTable(), treeCategorias.Nodes, false);
        PopulateNodes(rnCategorias.categoriasSelecionaNivelPaiPorSiteId(idSite).Tables[0].Select("childnodecount > 0", "categoriaNomeExibicao").CopyToDataTable(), treeCategoriasComFilhos.Nodes);
        //PopulateNodes(rnCategorias.categoriasSelecionaNivelPai().Tables[0], treeCategorias.Nodes);
    }

    private void PopulateSubLevel(int categoriaPaiId, TreeNode parentNode)
    {
        PopulateNodes(rnCategorias.categoriasSelecionaNivelFilho(categoriaPaiId).Tables[0], parentNode.ChildNodes);
    }

    protected void TreeView1_TreeNodePopulate(object sender, TreeNodeEventArgs e)
    {
        PopulateSubLevel(Int32.Parse(e.Node.Value), e.Node);
    }

    private void PopulateNodes(DataTable dt, TreeNodeCollection nodes, bool preencherSubs = true)
    {

        foreach (DataRow dr in dt.Rows)
        {
            TreeNode tn = new TreeNode();
            tn.Text = dr["categoriaNomeExibicao"].ToString();
            tn.Value = dr["categoriaId"].ToString();

            //desabilita os links do treeview
            tn.SelectAction = TreeNodeSelectAction.None;

            //checo as categorias que o produto esta relacionado
            if (
                rnCategorias.juncaoProdutoCategoriaSeleciona_PorCategoriaId(int.Parse(tn.Value),
                    int.Parse(Request.QueryString["produtoId"])).Tables[0].Rows.Count > 0)
            {

                if (tn.Value == rnCategorias.juncaoProdutoCategoriaSeleciona_PorCategoriaId(int.Parse(tn.Value), int.Parse(Request.QueryString["produtoId"])).Tables[0].Rows[0]["categoriaId"].ToString())
                    tn.Checked = true;

            }

            nodes.Add(tn);

            //If node has child nodes, then enable on-demand populating
            if (preencherSubs) tn.PopulateOnDemand = ((int)(dr["childnodecount"]) > 0);
        }
    }
    #endregion

    protected void atribuiJs()
    {
        txtProdutoEstoqueAtual.Attributes.Add("onkeypress", "return soNumero(event);");
        txtProdutoEstoqueMinimo.Attributes.Add("onkeypress", "return soNumero(event);");
        txtProdutoPeso.Attributes.Add("onkeypress", "return soNumero(event);");
        txtProdutoPrecoDeCusto.Attributes.Add("onkeyPress", "return moeda(this,'.',',',event);");
        txtProdutoPreco.Attributes.Add("onkeyPress", "return moeda(this,'.',',',event);");
        txtProdutoPrecoPromocional.Attributes.Add("onkeyPress", "return moeda(this,'.',',',event);");
        txtProdutoPrecoAtacado.Attributes.Add("onkeyPress", "return moeda(this,'.',',',event);");
        txtProdutoQuantidadeMininaParaAtacado.Attributes.Add("onkeypress", "return soNumero(event);");
        txtFios.Attributes.Add("onkeypress", "return soNumero(event);");
        txtPecas.Attributes.Add("onkeypress", "return soNumero(event);");
        txtBrindes.Attributes.Add("onkeypress", "return soNumero(event);");
        txtRelevancia.Attributes.Add("onkeypress", "return soNumero(event);");
        txtProdutoRelacionadoDesconto.Attributes.Add("onkeypress", "return soNumero(event);");
        txtIcms.Attributes.Add("onkeypress", "return soNumero(event);");

        txtMargem.Attributes.Add("onkeyup", "javascript:return CalculoMargemVenda('" + txtMargem.ClientID + "', '" + lblPrecoPromocional.ClientID + "'," + "'" + lblPrecoVenda.ClientID + "', '" + txtProdutoPrecoDeCusto.ClientID + "');");
    }

    protected void dtlEspecificacoes_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            DropDownList drpEspecificacoes = (DropDownList)e.Item.FindControl("drpEspecificacoes");

            drpEspecificacoes.Items.Clear();
            drpEspecificacoes.Items.Add(new ListItem("Selecione", ""));

            foreach (DataRow dr in rnEspecificacao.especificacaoItemSeleciona_PorEspecificacaoId(int.Parse(DataBinder.Eval(e.Item.DataItem, "especificacaoId").ToString())).Tables[0].Rows)
            {
                drpEspecificacoes.Items.Add(new ListItem(dr["especificacaoItenNome"].ToString(), dr["especificacaoItensId"].ToString()));
            }

            if (rnProdutos.produtoItemEspecificacaoSeleciona_PorProdutoIdEProdutoEspecificacaoId(int.Parse(Request.QueryString["produtoId"].ToString()), int.Parse(DataBinder.Eval(e.Item.DataItem, "especificacaoId").ToString())).Tables[0].Rows.Count > 0)
                drpEspecificacoes.Items.FindByValue(rnProdutos.produtoItemEspecificacaoSeleciona_PorProdutoIdEProdutoEspecificacaoId(int.Parse(Request.QueryString["produtoId"].ToString()), int.Parse(DataBinder.Eval(e.Item.DataItem, "especificacaoId").ToString())).Tables[0].Rows[0]["produtoItemEspecificacaoId"].ToString()).Selected = true;
        }
    }

    protected void dtlGarantias_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        TextBox txtValor = (TextBox)e.Item.FindControl("txtValor");
        txtValor.Attributes.Add("onkeyPress", "return moeda(this,'.',',',event);");
    }

    protected int verificaSeExisteProdutoPaiIdNaQueryString()
    {
        if (!string.IsNullOrEmpty(Request.QueryString["produtoPaiId"]))
        {
            return int.Parse(Request.QueryString["produtoPaiId"].ToString());
        }
        else
        {
            return 0;
        }
    }

    protected void btn_Click(object sender, EventArgs e)
    {
        if (btn.Text == "Incluir")
        {
            /*var idExiste = rnProdutos.produtoSelecionaIdDoProdutoPrincipal_PorProdutoIdDaEmpresa(txtProdutoIdDaEmpresa.Text);
            if (idExiste.Tables[0].Rows.Count > 0)
            {
                Response.Write("<script>alert('ID da Empresa já cadastrado.');</script>");
                return;
            }*/

            treeCategoriasComFilhos.ExpandAll();//previni que não salve nenhum item que foi selecionado nesta lista

            incluir();

            treeCategoriasComFilhos.CollapseAll();
        }
        if (btn.Text == "Duplicar")
        {
            /*var idExiste = rnProdutos.produtoSelecionaIdDoProdutoPrincipal_PorProdutoIdDaEmpresa(txtProdutoIdDaEmpresa.Text);
            if (idExiste.Tables[0].Rows.Count > 0)
            {
                Response.Write("<script>alert('ID da Empresa já cadastrado.');</script>");
                return;
            }*/

            treeCategoriasComFilhos.ExpandAll();//previni que não salve nenhum item que foi selecionado nesta lista

            duplicar();

            treeCategoriasComFilhos.CollapseAll();
        }
        if (btn.Text == "Alterar")
        {
            treeCategoriasComFilhos.ExpandAll();//previni que não salve nenhum item que foi selecionado nesta lista

            if (treeCategorias.CheckedNodes.Count > 0 || treeCategoriasComFilhos.CheckedNodes.Count > 0)
            {
                alterar();
            }
            else
            {
                Response.Write("<script>alert('Selecione pelo menos uma categoria.');</script>");
            }

            treeCategoriasComFilhos.CollapseAll();
        }
    }

    private int incluir()
    {
        return incluir(verificaSeExisteProdutoPaiIdNaQueryString(), true);
    }

    private int incluir(int produtoPai, bool herdarEspecificacoes)
    {
        treeCategoriasComFilhos.ExpandAll();//previni que não salve nenhum item que foi selecionado nesta lista

        try
        {
            int produtoId;
            int produtoPaiId = produtoPai;
            string produtoMetaDescription = txtProdutoNome.Text + " " + txtProdutoDescricao.Text;
            string produtoMetakeywords = txtProdutoNome.Text + " " + txtProdutoDescricao.Text;
            string produtoNomeParaUrl = txtProdutoNomeTitle.Text;
            if (string.IsNullOrEmpty(produtoNomeParaUrl)) produtoNomeParaUrl = txtProdutoNome.Text;
            string produtoUrl = rnProdutos.GerarPermalinkProduto(0, produtoNomeParaUrl).Trim().ToLower();
            int produtoAlternativoId = 0;
            if (!string.IsNullOrEmpty(txtIdProdutoAlternativo.Text)) int.TryParse(txtIdProdutoAlternativo.Text, out produtoAlternativoId);
            #region Define valores dos checkboxs
            string produtoAtivo = "False";
            string produtoPrincipal = ckbPrincipal.Checked == true ? "True" : "False";
            string produtoFreteGratis = ckbFreteGratis.Checked == true ? "True" : "False";
            string produtoLancamento = ckbLancamento.Checked == true ? "True" : "False";
            string preVenda = ckbPreVenda.Checked == true ? "True" : "False";
            string produtoPromocao = ckbPromocao.Checked == true ? "True" : "False";
            string destaque1 = ckbDestaque1.Checked == true ? "True" : "False";
            string destaque2 = ckbDestaque2.Checked == true ? "True" : "False";
            string destaque3 = ckbDestaque3.Checked == true ? "True" : "False";
            string exclusivo = chkExclusivo.Checked == true ? "True" : "False";
            string marketplaceCadastrar = ckbMarketplaceCadastrar.Checked == true ? "True" : "False";
            string marketplaceEnviarMarca = ckbMarketplaceEnviarMarca.Checked == true ? "True" : "False";
            string descontoProgressivoFrete = chbDescontoProgressivoFrete.Checked == true ? "True" : "False";
            string cubagem = ckbCubagem.Checked == true ? "True" : "False";
            string exibirDiferencaCombo = ckbExibirDiferencaCombo.Checked == true ? "True" : "False";
            #endregion

            produtoId = rnProdutos.produtoInclui(produtoPaiId, produtoPrincipal, txtProdutoIdDaEmpresa.Text, txtProdutoCodDeBarras.Text, txtProdutoNome.Text,
                                        produtoUrl, txtProdutoDescricao.Text, int.Parse(drpFornecedor.SelectedItem.Value), int.Parse(drpMarca.SelectedItem.Value),
                                        decimal.Parse(txtProdutoPreco.Text), decimal.Parse(txtProdutoPrecoDeCusto.Text),
                                        decimal.Parse(txtProdutoPrecoPromocional.Text), decimal.Parse(txtProdutoPrecoAtacado.Text),
                                        int.Parse(txtProdutoQuantidadeMininaParaAtacado.Text), txtProdutoLegendaAtacado.Text,
                                        int.Parse(txtProdutoEstoqueAtual.Text), int.Parse(txtProdutoEstoqueMinimo.Text),
                                        decimal.Parse(txtProdutoPeso.Text), produtoAtivo, produtoFreteGratis, produtoLancamento,
                                        produtoPromocao, produtoMetaDescription, produtoMetakeywords, destaque1, destaque2, destaque3, txtDisponibilidadeEmEstoque.Text, preVenda,
                                        drpComposicao.SelectedValue, txtFios.Text, txtPecas.Text, txtBrindes.Text, txtProdutoTags.Text, txtNcm.Text, marketplaceCadastrar, marketplaceEnviarMarca,
                                        exclusivo, int.Parse(txtAltura.Text), int.Parse(txtLargura.Text), int.Parse(txtProfundidade.Text), txtComplementoId.Text, descontoProgressivoFrete,
                                        cubagem, txtProdutoNomeNovo.Text, exibirDiferencaCombo, Convert.ToInt32(ddlSite.SelectedValue), produtoAlternativoId, ckbBloqueado.Checked,
                                        Convert.ToInt32(txtRelevancia.Text), ckbProntaEntrega.Checked, ckbForaDeLinha.Checked, Convert.ToInt32(txtIcms.Text), "", txtProdutoNomeTitle.Text, txtProdutoNomeShopping.Text, txtFoto360.Text,
                                        ckbEstoqueReal.Checked, ckbEstoqueVirtual.Checked, Convert.ToInt32(ddlCd.SelectedValue), ckbCriacaoPropria.Checked, ckbOcultarLista.Checked, ckbOcultarBusca.Checked,
                                        ckbAtivoMercadoLivre.Checked, ckbAtivoB2w.Checked, ckbAtivoCnova.Checked, ckbAtivoWalmart.Checked, ckbAtivoMagazineLuiza.Checked);

            if (produtoId > 0)
            {
                //produtoId = recuperaProdutoId();


                categoriaInclui(produtoId, produtoPaiId);

                colecaoInclui(produtoId);

                //processoFabricaInclui(produtoId);

                tabelaDescricaoInclui(produtoId);

                if (herdarEspecificacoes) especificacaoInclui(produtoPaiId, produtoId);

                garantiaEstendidaInclui(produtoId);

                criaDiretorioDeFotos(produtoId);

                fotoInclui(produtoId);

                informacoesAdcionaisInclui(produtoId);

                linkVideoInclui(produtoId);

                var data = new dbCommerceDataContext();
                var produtoAlt = (from c in data.tbProdutos where c.produtoId == produtoId select c).First();
                produtoNomeParaUrl = txtProdutoNomeTitle.Text;
                if (string.IsNullOrEmpty(produtoNomeParaUrl)) produtoNomeParaUrl = txtProdutoNome.Text;
                produtoUrl = rnProdutos.GerarPermalinkProduto(produtoId, produtoNomeParaUrl).Trim().ToLower();
                produtoAlt.produtoUrl = produtoUrl;
                data.SubmitChanges();

                //rnProdutos.AtualizaDescontoCombo(produtoId);

                //rnBuscaCloudSearch.AtualizarProduto(produtoId);
            }
            Response.Write("<script>alert('Produto incluido com sucesso.');</script>");
            treeCategoriasComFilhos.CollapseAll();
            return produtoId;
        }
        catch (Exception e)
        {
            return 0;
            //throw new Exception(e.Message);
        }

    }

    protected bool duplicar()
    {
        try
        {
            int produtoId;
            int produtoPaiId = Convert.ToInt32(lblIdProdutoPai.Text);
            string produtoMetaDescription = txtProdutoNome.Text + " " + txtProdutoDescricao.Text;
            string produtoMetakeywords = txtProdutoNome.Text + " " + txtProdutoDescricao.Text;
            //string produtoUrl = rnProdutos.GerarPermalinkProduto(txtProdutoNome.Text).Trim().ToLower();
            string produtoNomeParaUrl = txtProdutoNomeTitle.Text;
            if (string.IsNullOrEmpty(produtoNomeParaUrl)) produtoNomeParaUrl = txtProdutoNome.Text;
            string produtoUrl = rnProdutos.GerarPermalinkProduto(0, produtoNomeParaUrl).Trim().ToLower();

            int produtoAlternativoId = 0;
            if (!string.IsNullOrEmpty(txtIdProdutoAlternativo.Text)) int.TryParse(txtIdProdutoAlternativo.Text, out produtoAlternativoId);

            #region Define valores dos checkboxs
            string produtoAtivo;
            if (ckbAtivo.Checked == true)
                produtoAtivo = "True";
            else
                produtoAtivo = "False";

            string produtoPrincipal;
            if (ckbPrincipal.Checked == true)
                produtoPrincipal = "True";
            else
                produtoPrincipal = "False";

            string produtoFreteGratis;
            if (ckbFreteGratis.Checked == true)
                produtoFreteGratis = "True";
            else
                produtoFreteGratis = "False";

            string produtoLancamento;
            if (ckbLancamento.Checked == true)
                produtoLancamento = "True";
            else
                produtoLancamento = "False";

            string preVenda;
            if (ckbPreVenda.Checked == true)
                preVenda = "True";
            else
                preVenda = "False";

            string produtoPromocao;
            if (ckbPromocao.Checked == true)
                produtoPromocao = "True";
            else
                produtoPromocao = "False";

            string destaque1;
            if (ckbDestaque1.Checked == true)
                destaque1 = "True";
            else
                destaque1 = "False";

            string destaque2;
            if (ckbDestaque2.Checked == true)
                destaque2 = "True";
            else
                destaque2 = "False";

            string destaque3;
            if (ckbDestaque3.Checked == true)
                destaque3 = "True";
            else
                destaque3 = "False";


            string exclusivo;
            if (chkExclusivo.Checked == true)
                exclusivo = "True";
            else
                exclusivo = "False";

            string marketplaceCadastrar;
            if (ckbMarketplaceCadastrar.Checked == true)
                marketplaceCadastrar = "True";
            else
                marketplaceCadastrar = "False";

            string marketplaceEnviarMarca;
            if (ckbMarketplaceEnviarMarca.Checked == true)
                marketplaceEnviarMarca = "True";
            else
                marketplaceEnviarMarca = "False";

            string descontoProgressivoFrete;
            if (chbDescontoProgressivoFrete.Checked == true)
                descontoProgressivoFrete = "True";
            else
                descontoProgressivoFrete = "False";

            string cubagem;
            if (ckbCubagem.Checked == true)
                cubagem = "True";
            else
                cubagem = "False";


            string exibirDiferencaCombo = ckbExibirDiferencaCombo.Checked == true ? "True" : "False";
            #endregion

            produtoId = rnProdutos.produtoInclui(produtoPaiId, produtoPrincipal, txtProdutoIdDaEmpresa.Text, txtProdutoCodDeBarras.Text, txtProdutoNome.Text,
                                        produtoUrl, txtProdutoDescricao.Text, int.Parse(drpFornecedor.SelectedItem.Value), int.Parse(drpMarca.SelectedItem.Value),
                                        decimal.Parse(txtProdutoPreco.Text), decimal.Parse(txtProdutoPrecoDeCusto.Text),
                                        decimal.Parse(txtProdutoPrecoPromocional.Text), decimal.Parse(txtProdutoPrecoAtacado.Text),
                                        int.Parse(txtProdutoQuantidadeMininaParaAtacado.Text), txtProdutoLegendaAtacado.Text,
                                        int.Parse(txtProdutoEstoqueAtual.Text), int.Parse(txtProdutoEstoqueMinimo.Text),
                                        decimal.Parse(txtProdutoPeso.Text), produtoAtivo, produtoFreteGratis, produtoLancamento,
                                        produtoPromocao, produtoMetaDescription, produtoMetakeywords, destaque1, destaque2, destaque3, txtDisponibilidadeEmEstoque.Text, preVenda,
                                        drpComposicao.SelectedValue, txtFios.Text, txtPecas.Text, txtBrindes.Text, txtProdutoTags.Text, txtNcm.Text,
                                        marketplaceCadastrar, marketplaceEnviarMarca, exclusivo, int.Parse(txtAltura.Text), int.Parse(txtLargura.Text), int.Parse(txtProfundidade.Text),
                                        txtComplementoId.Text, descontoProgressivoFrete, cubagem, txtProdutoNomeNovo.Text, exibirDiferencaCombo, Convert.ToInt32(ddlSite.SelectedValue),
                                        produtoAlternativoId, ckbBloqueado.Checked, Convert.ToInt32(txtRelevancia.Text), ckbProntaEntrega.Checked, ckbForaDeLinha.Checked, Convert.ToInt32(txtIcms.Text),
                                        "", txtProdutoNomeTitle.Text, txtProdutoNomeShopping.Text, txtFoto360.Text, ckbEstoqueReal.Checked, ckbEstoqueVirtual.Checked, Convert.ToInt32(ddlCd.SelectedValue),
                                        ckbCriacaoPropria.Checked, ckbOcultarLista.Checked, ckbOcultarBusca.Checked,
                                        ckbAtivoMercadoLivre.Checked, ckbAtivoB2w.Checked, ckbAtivoCnova.Checked, ckbAtivoWalmart.Checked, ckbAtivoMagazineLuiza.Checked);

            if (produtoId > 0)
            {
                //produtoId = recuperaProdutoId();

                if (produtoPrincipal == "True")
                    alteraPrincipal(produtoPaiId, produtoId);

                categoriaInclui(produtoId, produtoPaiId);

                colecaoInclui(produtoId);

                //processoFabricaInclui(produtoId);

                tabelaDescricaoInclui(produtoId);

                especificacaoInclui(produtoPaiId, produtoId);

                garantiaEstendidaInclui(produtoId);

                criaDiretorioDeFotos(produtoId);

                fotoInclui(produtoId);

                informacoesAdcionaisInclui(produtoId);

                linkVideoInclui(produtoId);

                var data = new dbCommerceDataContext();
                var produtoAlt = (from c in data.tbProdutos where c.produtoId == produtoId select c).First();
                produtoNomeParaUrl = txtProdutoNomeTitle.Text;
                if (string.IsNullOrEmpty(produtoNomeParaUrl)) produtoNomeParaUrl = txtProdutoNome.Text;
                produtoUrl = rnProdutos.GerarPermalinkProduto(produtoId, produtoNomeParaUrl).Trim().ToLower();
                produtoAlt.produtoUrl = produtoUrl;
                data.SubmitChanges();


                //rnProdutos.AtualizaDescontoCombo(produtoId);

                rnBuscaCloudSearch.AtualizarProduto(produtoId);
            }
            Response.Write("<script>alert('Produto duplicado com sucesso.');</script>");
            return true;
        }
        catch (Exception e)
        {
            throw new Exception(e.Message);
        }
    }

    protected bool alterar()
    {
        try
        {
            int produtoId = int.Parse(Request.QueryString["produtoId"]);
            int produtoPaiId = Convert.ToInt32(lblIdProdutoPai.Text);
            string produtoMetaDescription = txtProdutoNome.Text + " " + txtProdutoDescricao.Text;
            string produtoMetakeywords = txtProdutoNome.Text + " " + txtProdutoDescricao.Text;
            string estoqueAtual = rnProdutos.produtoSeleciona_PorProdutoId(produtoId).Tables[0].Rows[0]["produtoEstoqueAtual"].ToString();
            string produtoNomeParaUrl = txtProdutoNomeTitle.Text;
            if (string.IsNullOrEmpty(produtoNomeParaUrl)) produtoNomeParaUrl = txtProdutoNome.Text;
            string produtoUrl = rnProdutos.GerarPermalinkProduto(produtoId, produtoNomeParaUrl).Trim().ToLower();
            decimal valorDiferencaCombo = 0;
            decimal valorTotalCombo = 0;
            var data = new dbCommerceDataContext();

            //rnProdutos.AtualizaDescontoCombo(produtoId);

            int produtoAlternativoId = 0;
            if (!string.IsNullOrEmpty(txtIdProdutoAlternativo.Text)) int.TryParse(txtIdProdutoAlternativo.Text, out produtoAlternativoId);

            #region Define valores dos checkboxs
            string produtoAtivo;
            if (ckbAtivo.Checked == true)
                produtoAtivo = "True";
            else
                produtoAtivo = "False";

            string produtoPrincipal;
            if (ckbPrincipal.Checked == true)
                produtoPrincipal = "True";
            else
                produtoPrincipal = "False";

            string produtoFreteGratis;
            if (ckbFreteGratis.Checked == true)
                produtoFreteGratis = "True";
            else
                produtoFreteGratis = "False";

            string produtoLancamento;
            if (ckbLancamento.Checked == true)
                produtoLancamento = "True";
            else
                produtoLancamento = "False";

            string preVenda;
            if (ckbPreVenda.Checked == true)
                preVenda = "True";
            else
                preVenda = "False";

            string produtoPromocao;
            if (ckbPromocao.Checked == true)
                produtoPromocao = "True";
            else
                produtoPromocao = "False";

            string destaque1;
            if (ckbDestaque1.Checked == true)
                destaque1 = "True";
            else
                destaque1 = "False";

            string destaque2;
            if (ckbDestaque2.Checked == true)
                destaque2 = "True";
            else
                destaque2 = "False";

            string destaque3;
            if (ckbDestaque3.Checked == true)
                destaque3 = "True";
            else
                destaque3 = "False";


            string exclusivo;
            if (chkExclusivo.Checked == true)
                exclusivo = "True";
            else
                exclusivo = "False";

            string marketplaceCadastrar;
            if (ckbMarketplaceCadastrar.Checked == true)
                marketplaceCadastrar = "True";
            else
                marketplaceCadastrar = "False";

            string marketplaceEnviarMarca;
            if (ckbMarketplaceEnviarMarca.Checked == true)
                marketplaceEnviarMarca = "True";
            else
                marketplaceEnviarMarca = "False";

            string descontoProgressivoFrete;
            if (chbDescontoProgressivoFrete.Checked == true)
                descontoProgressivoFrete = "True";
            else
                descontoProgressivoFrete = "False";

            string cubagem;
            if (ckbCubagem.Checked == true)
                cubagem = "True";
            else
                cubagem = "False";

            string exibirDiferencaCombo = ckbExibirDiferencaCombo.Checked == true ? "True" : "False";
            #endregion

            if (rnProdutos.produtoAlterar(produtoPaiId, txtProdutoIdDaEmpresa.Text, txtProdutoCodDeBarras.Text, txtProdutoNome.Text, produtoUrl,
                                        txtProdutoDescricao.Text, int.Parse(drpFornecedor.SelectedItem.Value), int.Parse(drpMarca.SelectedItem.Value),
                                        decimal.Parse(txtProdutoPreco.Text), decimal.Parse(txtProdutoPrecoDeCusto.Text),
                                        decimal.Parse(txtProdutoPrecoPromocional.Text), decimal.Parse(txtProdutoPrecoAtacado.Text),
                                        int.Parse(txtProdutoQuantidadeMininaParaAtacado.Text), txtProdutoLegendaAtacado.Text,
                                        int.Parse(txtProdutoEstoqueAtual.Text), int.Parse(txtProdutoEstoqueMinimo.Text),
                                        decimal.Parse(txtProdutoPeso.Text), produtoAtivo, produtoFreteGratis, produtoLancamento,
                                        produtoPromocao, produtoMetaDescription, produtoMetakeywords, destaque1, destaque2, destaque3, produtoId, txtDisponibilidadeEmEstoque.Text, preVenda,
                                        drpComposicao.SelectedValue, txtFios.Text, txtPecas.Text, txtBrindes.Text, txtProdutoTags.Text, txtNcm.Text,
                                        marketplaceCadastrar, marketplaceEnviarMarca, exclusivo, int.Parse(txtAltura.Text), int.Parse(txtLargura.Text), int.Parse(txtProfundidade.Text),
                                        txtComplementoId.Text, descontoProgressivoFrete, cubagem, txtProdutoNomeNovo.Text, exibirDiferencaCombo, valorDiferencaCombo, Convert.ToInt32(ddlSite.SelectedValue),
                                        produtoAlternativoId, ckbBloqueado.Checked, Convert.ToInt32(txtRelevancia.Text), ckbProntaEntrega.Checked, ckbForaDeLinha.Checked, Convert.ToInt32(txtIcms.Text),
                                        txtProdutoNomeTitle.Text, txtProdutoNomeShopping.Text, txtFoto360.Text, ckbEstoqueReal.Checked, ckbEstoqueVirtual.Checked, Convert.ToInt32(ddlCd.SelectedValue),
                                        ckbCriacaoPropria.Checked, ckbOcultarLista.Checked, ckbOcultarBusca.Checked,
                                        ckbAtivoMercadoLivre.Checked, ckbAtivoB2w.Checked, ckbAtivoCnova.Checked, ckbAtivoWalmart.Checked, 
                                        ckbAtivoMagazineLuiza.Checked))
            {
                //if (produtoPrincipal == "True") alteraPrincipal(produtoPaiId, produtoId);

                categoriaExclui(produtoId, produtoPaiId);
                categoriaIncluiNoGrupoDeProdutos(produtoId, produtoPaiId);

                colecaoInclui(produtoId);

                //processoFabricaInclui(produtoId);

                tabelaDescricaoInclui(produtoId);

                especificacaoExclui(produtoId);
                especificacaoInclui(produtoPaiId, produtoId);

                garantiaEstendidaExclui(produtoId);
                garantiaEstendidaInclui(produtoId);

                fotoExclui(produtoId);
                fotoAltera();
                fotoDestaqueAltera(produtoId);
                fotoInclui(produtoId);

                informacoesAdcionaisExclui(produtoId);
                informacoesAdcionaisInclui(produtoId);

                linkVideoInclui(produtoId);

                verificaVinculoComBanner(produtoId);

                //aviso de atualizacao de estoque
                if (estoqueAtual != txtProdutoEstoqueAtual.Text)
                    if (rnProdutoEmEspera.selecionaClientesEsperandoProdutos_PorProdutoId(produtoId))
                        rnProdutoEmEspera.produtoEmEsperaExclui(produtoId);

                //rnProdutos.AtualizaDescontoCombo(produtoId);

                //rnBuscaCloudSearch.AtualizarProduto(produtoId);
            }
            Response.Write("<script>alert('Produto alterado com sucesso.');</script>");
            Response.Write("<script>window.location=('" + Request.Url.ToString() + "');</script>");
            return true;
        }
        catch (Exception e)
        {
            Response.Write("<script>alert('" + e.Message + "');</script>");
            return false;
            //throw new Exception(e.Message);
        }
    }

    private int recuperaProdutoId()
    {
        return int.Parse(rnProdutos.produtoSelecionaUltimoId().Tables[0].Rows[0]["produtoId"].ToString());
    }

    private void alteraPrincipal(int produtoPaiId, int produtoId)
    {
        rnProdutos.alteraProdutoPrincipal(produtoPaiId, produtoId);
    }

    private void colecaoInclui(int produtoId)
    {
        var produtoDc = new dbCommerceDataContext();
        var colecoesAtuais = (from c in produtoDc.tbJuncaoProdutoColecaos where c.produtoId == produtoId select c).ToList();


        foreach (ListItem colecaoItem in chkColecoes.Items)
        {
            int colecaoId = Convert.ToInt32(colecaoItem.Value);
            var possuiAtualmente = colecoesAtuais.Any(x => x.colecaoId == colecaoId);
            if (colecaoItem.Selected && possuiAtualmente == false)
            {
                var colecaoAdicionar = new tbJuncaoProdutoColecao();
                colecaoAdicionar.produtoId = produtoId;
                colecaoAdicionar.colecaoId = colecaoId;
                colecaoAdicionar.dataDaCriacao = DateTime.Now;
                produtoDc.tbJuncaoProdutoColecaos.InsertOnSubmit(colecaoAdicionar);

                var queueUpdateFoto = new tbQueue()
                {
                    agendamento = DateTime.Now,
                    andamento = false,
                    concluido = false,
                    idRelacionado = produtoId,
                    mensagem = "1",
                    tipoQueue = 26
                };
                produtoDc.tbQueues.InsertOnSubmit(queueUpdateFoto);
            }
            else if (colecaoItem.Selected == false && possuiAtualmente)
            {
                var colecaoExcluir =
                    (from c in produtoDc.tbJuncaoProdutoColecaos
                     where c.produtoId == produtoId && c.colecaoId == colecaoId
                     select c).First();
                produtoDc.tbJuncaoProdutoColecaos.DeleteOnSubmit(colecaoExcluir);
            }
        }
        produtoDc.SubmitChanges();
    }
    //private void processoFabricaInclui(int produtoId)
    //{

    //    var produtoDc = new dbCommerceDataContext();
    //    var processosAtuais = (from c in produtoDc.tbJuncaoProdutoProcessoFabricas where c.idProduto == produtoId select c);
    //    foreach (ListItem processoItem in chkProcessosFabrica.Items)
    //    {
    //        int processoId = Convert.ToInt32(processoItem.Value);
    //        var possuiAtualmente = processosAtuais.Any(x => x.idProcessoFabrica == processoId);
    //        if (processoItem.Selected && possuiAtualmente == false)
    //        {
    //            var processoAdicionar = new tbJuncaoProdutoProcessoFabrica();
    //            processoAdicionar.idProduto = produtoId;
    //            processoAdicionar.idProcessoFabrica = processoId;
    //            produtoDc.tbJuncaoProdutoProcessoFabricas.InsertOnSubmit(processoAdicionar);
    //            produtoDc.SubmitChanges();
    //        }
    //        else if (processoItem.Selected == false && possuiAtualmente)
    //        {
    //            var processoExcluir =
    //                (from c in produtoDc.tbJuncaoProdutoProcessoFabricas
    //                 where c.idProduto == produtoId && c.idProcessoFabrica == processoId
    //                 select c).First();
    //            produtoDc.tbJuncaoProdutoProcessoFabricas.DeleteOnSubmit(processoExcluir);
    //            produtoDc.SubmitChanges();
    //        }
    //    }
    //}
    private void categoriaExclui(int produtoId, int produtoPaiId)
    {
        /*if (produtoPaiId == 0)
            rnCategorias.juncaoProdutoCategoriaExclui_PorProdutoId(produtoId);
        else
            rnCategorias.juncaoProdutoCategoriaExclui_PorProdutoPaiId(produtoPaiId);*/
    }
    private void categoriaInclui(int produtoId, int produtoPaiId)
    {
        /*if (treeCategorias.CheckedNodes.Count > 0)
        {
            foreach (TreeNode node in treeCategorias.CheckedNodes)
            {
                rnCategorias.juncaoProdutoCategoriaInclui(produtoId, Convert.ToInt32(node.Value), produtoPaiId);
            }
        }*/


        var produtoDc = new dbCommerceDataContext();
        var categoriasAtuais = (from c in produtoDc.tbJuncaoProdutoCategorias where c.produtoId == produtoId select c).ToList();
        List<int> categoriasChecadas = new List<int>();

        foreach (TreeNode categoriaNode in treeCategorias.CheckedNodes)
        {
            categoriasChecadas.Add(Convert.ToInt32(categoriaNode.Value));
        }

        foreach (TreeNode categoriaNode in treeCategoriasComFilhos.CheckedNodes)
        {
            categoriasChecadas.Add(Convert.ToInt32(categoriaNode.Value));
        }


        foreach (var categoriaChecada in categoriasChecadas)
        {
            var possuiAtualmente = categoriasAtuais.Any(x => x.categoriaId == categoriaChecada);
            if (possuiAtualmente == false)
            {
                var categoriaAdicionar = new tbJuncaoProdutoCategoria();
                categoriaAdicionar.produtoId = produtoId;
                categoriaAdicionar.categoriaId = categoriaChecada;
                categoriaAdicionar.produtoPaiId = produtoPaiId;
                categoriaAdicionar.dataDaCriacao = DateTime.Now;
                produtoDc.tbJuncaoProdutoCategorias.InsertOnSubmit(categoriaAdicionar);
            }
        }

        foreach (var categoriaAtual in categoriasAtuais)
        {
            var possuiAtualmente = categoriasChecadas.Any(x => x == categoriaAtual.categoriaId);
            if (!possuiAtualmente)
            {
                var categoriaExcluir =
                    (from c in produtoDc.tbJuncaoProdutoCategorias
                     where c.produtoId == produtoId && c.categoriaId == categoriaAtual.categoriaId
                     select c).FirstOrDefault();
                if (categoriaExcluir != null)
                {
                    produtoDc.tbJuncaoProdutoCategorias.DeleteOnSubmit(categoriaExcluir);
                }
            }
        }
        produtoDc.SubmitChanges();

    }
    private void categoriaIncluiNoGrupoDeProdutos(int produtoId, int produtoPaiId)
    {
        //if (treeCategorias.CheckedNodes.Count > 0)
        //{
        //    if (produtoPaiId == 0)
        //    {
        //        foreach (TreeNode node in treeCategorias.CheckedNodes)
        //        {
        //            rnCategorias.juncaoProdutoCategoriaInclui(produtoId, Convert.ToInt32(node.Value), produtoPaiId);
        //        }
        //    }
        //    else
        //    {
        //        foreach (DataRow row in rnProdutos.produtoSeleciona_PorProdutoPaiId(produtoPaiId).Tables[0].Rows)
        //        {
        //            produtoId = int.Parse(row["produtoId"].ToString());
        //            foreach (TreeNode node in treeCategorias.CheckedNodes)
        //            {
        //                rnCategorias.juncaoProdutoCategoriaInclui(produtoId, Convert.ToInt32(node.Value), produtoPaiId);
        //            }
        //        }
        //    }
        //}


        var produtoDc = new dbCommerceDataContext();
        var categoriasAtuais = (from c in produtoDc.tbJuncaoProdutoCategorias where c.produtoId == produtoId select c).ToList();
        List<int> categoriasChecadas = new List<int>();

        foreach (TreeNode categoriaNode in treeCategorias.CheckedNodes)
        {
            categoriasChecadas.Add(Convert.ToInt32(categoriaNode.Value));
        }

        foreach (TreeNode categoriaNode in treeCategoriasComFilhos.CheckedNodes)
        {
            categoriasChecadas.Add(Convert.ToInt32(categoriaNode.Value));
        }

        foreach (var categoriaChecada in categoriasChecadas)
        {
            var possuiAtualmente = categoriasAtuais.Any(x => x.categoriaId == categoriaChecada);
            if (possuiAtualmente == false)
            {
                var categoriaAdicionar = new tbJuncaoProdutoCategoria();
                categoriaAdicionar.produtoId = produtoId;
                categoriaAdicionar.categoriaId = categoriaChecada;
                categoriaAdicionar.produtoPaiId = produtoPaiId;
                categoriaAdicionar.dataDaCriacao = DateTime.Now;
                produtoDc.tbJuncaoProdutoCategorias.InsertOnSubmit(categoriaAdicionar);
            }
        }

        foreach (var categoriaAtual in categoriasAtuais)
        {
            var possuiAtualmente = categoriasChecadas.Any(x => x == categoriaAtual.categoriaId);
            if (!possuiAtualmente)
            {
                var categoriaExcluir =
                    (from c in produtoDc.tbJuncaoProdutoCategorias
                     where c.produtoId == produtoId && c.categoriaId == categoriaAtual.categoriaId
                     select c).First();
                produtoDc.tbJuncaoProdutoCategorias.DeleteOnSubmit(categoriaExcluir);
            }
        }
        produtoDc.SubmitChanges();
    }
    private void especificacaoExclui(int produtoId)
    {
        //rnEspecificacao.produtoItensEspecificacaoExclui(produtoId);
    }
    private void especificacaoInclui(int produtoPaiId, int produtoId)
    {
        var data = new dbCommerceDataContext();

        if (produtoPaiId > 0)
        {
            var especificacoesPai = (from c in data.tbJuncaoProdutoEspecificacaos where c.produtoId == produtoPaiId select c).ToList();
            foreach (DataListItem item1 in dtlEspecificacoes.Items)
            {
                if (item1.ItemType == ListItemType.Item || item1.ItemType == ListItemType.AlternatingItem)
                {
                    Label lblEspecificacaoId = (Label)item1.FindControl("lblEspecificacaoId");
                    int especificacaoId = int.Parse(lblEspecificacaoId.Text);
                    if (!especificacoesPai.Any(x => x.especificacaoId == especificacaoId))
                    {
                        var itemEspecificacao = (from c in data.tbProdutoItemEspecificacaos where c.produtoId == produtoId && c.especificacaoId == especificacaoId select c).FirstOrDefault();
                        if (itemEspecificacao != null)
                        {
                            data.tbProdutoItemEspecificacaos.DeleteOnSubmit(itemEspecificacao);
                        }

                        var especificacoFilho = (from c in data.tbJuncaoProdutoEspecificacaos where c.produtoId == produtoId && c.especificacaoId == especificacaoId select c).FirstOrDefault();
                        if (especificacoFilho != null)
                        {
                            data.tbJuncaoProdutoEspecificacaos.DeleteOnSubmit(especificacoFilho);
                        }
                        data.SubmitChanges();
                    }
                }
            }


            foreach (var especificacaoPai in especificacoesPai)
            {
                var possuiEspecificacoFilho = (from c in data.tbJuncaoProdutoEspecificacaos where c.produtoId == produtoId && c.especificacaoId == especificacaoPai.especificacaoId select c).Any();
                if (possuiEspecificacoFilho == false)
                {
                    var especificacoFilho = new tbJuncaoProdutoEspecificacao()
                    {
                        dataDaCriacao = DateTime.Now,
                        especificacaoId = especificacaoPai.especificacaoId,
                        produtoId = produtoId
                    };
                    data.tbJuncaoProdutoEspecificacaos.InsertOnSubmit(especificacoFilho);
                    data.SubmitChanges();
                }

            }
        }


        foreach (DataListItem item1 in dtlEspecificacoes.Items)
        {
            if (item1.ItemType == ListItemType.Item || item1.ItemType == ListItemType.AlternatingItem)
            {
                Label lblEspecificacaoId = (Label)item1.FindControl("lblEspecificacaoId");
                DropDownList drpEspecificacoes = (DropDownList)item1.FindControl("drpEspecificacoes");

                int especificacaoId = int.Parse(lblEspecificacaoId.Text);

                #region inclui produtoId e especificacao na tabela tbJuncaoProdutoEspecificacao. antes de incluir verifico se já existe na procedure
                rnEspecificacao.juncaoProdutoEspecificacaoInclui(produtoId, especificacaoId);
                #endregion

                int? produtoItemEspecificacaoId = null;
                if (drpEspecificacoes.SelectedItem.Value != "")
                {
                    produtoItemEspecificacaoId = int.Parse(drpEspecificacoes.SelectedItem.Value);
                }

                var itemEspecificacaoAlterar = (from c in data.tbProdutoItemEspecificacaos where c.produtoId == produtoId && c.especificacaoId == especificacaoId select c).FirstOrDefault();
                if (itemEspecificacaoAlterar != null)
                {
                    itemEspecificacaoAlterar.produtoPaiId = produtoPaiId;
                    itemEspecificacaoAlterar.produtoItemEspecificacaoId = produtoItemEspecificacaoId;
                    data.SubmitChanges();
                }
                else
                {
                    var itemEspecificacao = new tbProdutoItemEspecificacao()
                    {
                        produtoId = produtoId,
                        dataDaCriacao = DateTime.Now,
                        especificacaoId = especificacaoId,
                        produtoItemEspecificacaoId = produtoItemEspecificacaoId,
                        produtoPaiId = produtoPaiId
                    };
                    data.tbProdutoItemEspecificacaos.InsertOnSubmit(itemEspecificacao);
                    data.SubmitChanges();
                }
                #region incluo as especificacoes selecionadas na tabela tbProdutoItemEspecificacao
                //rnEspecificacao.produtoItensEspecificacaoInclui(produtoId, produtoPaiId, especificacaoId, produtoItemEspecificacaoId);
                #endregion
            }
        }
    }

    private void garantiaEstendidaExclui(int produtoId)
    {
        rnGarantiaEstendida.garantiaEstendidaExclui(produtoId);
    }
    private void garantiaEstendidaInclui(int produtoId)
    {
        foreach (DataListItem itemGarantia in dtlGarantias.Items)
        {
            TextBox txtTempo = (TextBox)itemGarantia.FindControl("txtTempo");
            TextBox txtValor = (TextBox)itemGarantia.FindControl("txtValor");

            if (txtTempo.Text != string.Empty && txtValor.Text != string.Empty)
                rnGarantiaEstendida.garantiaEstendidaInclui(produtoId, txtTempo.Text, Convert.ToDecimal(txtValor.Text));
        }
    }

    private void criaDiretorioDeFotos(int produtoId)
    {
        Directory.CreateDirectory(ConfigurationManager.AppSettings["caminhoFisico"] + "fotos\\" + produtoId);
    }

    private void fotoAltera()
    {
        foreach (DataListItem itemFoto in dtlFotos.Items)
        {
            Image imgFotos = (Image)itemFoto.FindControl("imgFotos");
            TextBox txtFotoDescricao = (TextBox)itemFoto.FindControl("txtFotoDescricao");
            TextBox txtOrdenacaoFoto = (TextBox)itemFoto.FindControl("txtOrdenacaoFoto");
            int ordenacao = 0;
            int.TryParse(txtOrdenacaoFoto.Text, out ordenacao);

            if (imgFotos.ToolTip != "destaquecategoria.jpg") rnFotos.produtoFotoAltera(txtFotoDescricao.Text, int.Parse(imgFotos.ToolTip), ordenacao);
        }

    }
    private void fotoDestaqueAltera(int produtoId)
    {
        foreach (DataListItem itemFoto in dtlFotos.Items)
        {
            Image imgFotos = (Image)itemFoto.FindControl("imgFotos");
            RadioButton rdbDestaque = (RadioButton)itemFoto.FindControl("rdbDestaque");
            CheckBox ckbExcluirFoto = (CheckBox)itemFoto.FindControl("ckbExcluirFoto");

            if (rdbDestaque.Checked == true && ckbExcluirFoto.Checked == false)
            {
                rnFotos.produtoFotoDestaqueAltera(produtoId, imgFotos.ToolTip);
                var produtoData = new dbCommerceDataContext();
                var produto = (from c in produtoData.tbProdutos where c.produtoId == produtoId select c).FirstOrDefault();
                int fotoDestaque = Convert.ToInt32(imgFotos.ToolTip);
                int idProdutoFoto = Convert.ToInt32(imgFotos.ToolTip);
                var foto = (from c in produtoData.tbProdutoFotos where c.produtoFotoId == idProdutoFoto select c).FirstOrDefault();
                if (foto != null)
                {
                    foto.produtoFotoDestaque = "True";
                    produto.fotoDestaque = foto.produtoFoto;
                    produtoData.SubmitChanges();
                }
            }

        }
    }
    private void fotoExclui(int produtoId)
    {
        foreach (DataListItem itemFoto in dtlFotos.Items)
        {
            Image imgFotos = (Image)itemFoto.FindControl("imgFotos");
            RadioButton rdbDestaque = (RadioButton)itemFoto.FindControl("rdbDestaque");
            CheckBox ckbExcluirFoto = (CheckBox)itemFoto.FindControl("ckbExcluirFoto");

            var produtoData = new dbCommerceDataContext();
            var produto = (from c in produtoData.tbProdutos where c.produtoId == produtoId select c).FirstOrDefault();

            if (ckbExcluirFoto.Checked == true)
            {
                if (rdbDestaque.Checked == false)
                {
                    string produtoFotoId = imgFotos.ToolTip;
                    int fotoId = Convert.ToInt32(produtoFotoId);
                    var fotoDetalhe = (from c in produtoData.tbProdutoFotos where c.produtoFotoId == fotoId select c).FirstOrDefault();
                    rnFotos.produtoFotoExclui(fotoDetalhe.produtoFoto);
                    if (fotoDetalhe != null)
                    {
                        if (File.Exists(ConfigurationManager.AppSettings["caminhoFisico"] + "fotos\\" + produtoId + "\\pequena_" + fotoDetalhe.produtoFoto + ".jpg"))
                        {
                            File.Delete(ConfigurationManager.AppSettings["caminhoFisico"] + "fotos\\" + produtoId + "\\pequena_" + fotoDetalhe.produtoFoto + ".jpg");
                        }
                        if (File.Exists(ConfigurationManager.AppSettings["caminhoFisico"] + "fotos\\" + produtoId + "\\media_" + fotoDetalhe.produtoFoto + ".jpg"))
                        {
                            File.Delete(ConfigurationManager.AppSettings["caminhoFisico"] + "fotos\\" + produtoId + "\\media_" + fotoDetalhe.produtoFoto + ".jpg");
                        }
                        if (File.Exists(ConfigurationManager.AppSettings["caminhoFisico"] + "fotos\\" + produtoId + "\\" + fotoDetalhe.produtoFoto + ".jpg"))
                        {
                            File.Delete(ConfigurationManager.AppSettings["caminhoFisico"] + "fotos\\" + produtoId + "\\" + fotoDetalhe.produtoFoto + ".jpg");
                        }


                        string fotoPequena = produtoId + "/pequena_" + fotoDetalhe.produtoFoto + ".jpg";
                        //rnFotos.fotoExcluiAzure(fotoPequena, "fotos");

                        string fotoMedia = produtoId + "/media_" + fotoDetalhe.produtoFoto + ".jpg";
                        //rnFotos.fotoExcluiAzure(fotoMedia, "fotos");

                        string fotoNormal = produtoId + "/" + fotoDetalhe.produtoFoto + ".jpg";
                        //rnFotos.fotoExcluiAzure(fotoNormal, "fotos");

                        if (produtoFotoId == produto.fotoDestaque)
                        {
                            var fotosData = new dbCommerceDataContext();
                            var fotos = (from c in fotosData.tbProdutoFotos where c.produtoId == produtoId select c).FirstOrDefault();
                            if (fotos != null)
                            {
                                produto.fotoDestaque = fotos.produtoFoto;
                                produtoData.SubmitChanges();
                            }
                            else
                            {
                                produto.fotoDestaque = "";
                                produtoData.SubmitChanges();
                            }
                        }
                    }
                }
                else
                    Response.Write("<script>alert('Antes de excluir a foto destaque selecione outra como destaque.');</script>");
            }
        }
    }
    private void fotoInclui(int produtoId)
    {
        int i = 1;
        string Location;
        string Target;

        try
        {
            #region Fotos Galeria
            for (i = 1; i <= 3; i++)
            {
                FileUpload upl = (FileUpload)pnlFotos.FindControl("upl" + i.ToString());
                TextBox txtFotoDescricao = (TextBox)pnlFotos.FindControl("txtFotoDescricao" + i.ToString());

                if (upl.HasFile == true)
                {

                    FileInfo file = new FileInfo(upl.PostedFile.FileName);

                    #region Recupero o produtoFotoId
                    int fotoId;
                    if (rnFotos.produtoFotoSelecionaUltimoId().Tables[0].Rows.Count > 0)
                        fotoId = int.Parse(rnFotos.produtoFotoSelecionaUltimoId().Tables[0].Rows[0]["produtoFotoId"].ToString()) + 1;
                    else
                        fotoId = 0;
                    #endregion

                    //string fotoRenomeada = fotoId + ".jpg";
                    string fotoRenomeada = rnFuncoes.limpaString(txtProdutoNome.Text.Trim()).ToLower() + "-" + fotoId;
                    string fotoNomeProduto = fotoRenomeada + ".jpg";

                    //Salva foto no banco
                    rnFotos.fotosInclui(produtoId, fotoRenomeada.ToString(), txtFotoDescricao.Text);

                    if (!Directory.Exists(ConfigurationManager.AppSettings["caminhoFisico"] + "fotos\\" + produtoId + "\\"))//se não existir o diretório então cria
                        Directory.CreateDirectory(ConfigurationManager.AppSettings["caminhoFisico"] + "fotos\\" + produtoId + "\\");

                    //Salva foto no disco
                    string caminhoFoto = ConfigurationManager.AppSettings["caminhoFisico"] + "fotos\\" + produtoId + "\\";
                    upl.PostedFile.SaveAs(caminhoFoto + "original_" + fotoNomeProduto);

                    var agora = DateTime.Now;
                    using (var data = new dbCommerceDataContext()) // Service do Kraken
                    {
                        var queueCompactarFoto = new tbQueue
                        {
                            tipoQueue = 26,
                            agendamento = agora,
                            idRelacionado = produtoId,
                            mensagem = "",
                            concluido = false,
                            andamento = false
                        };
                        data.tbQueues.InsertOnSubmit(queueCompactarFoto);
                        data.SubmitChanges();
                    }


                    /*var tinify = new Tinify();
                    var fotoOtimizada = tinify.Shrink(caminhoFoto + "original_" + fotoNomeProduto, caminhoFoto + fotoNomeProduto);


                    gerarFotoWebP(caminhoFoto, fotoNomeProduto, produtoId, 90);

                    Location = caminhoFoto + fotoNomeProduto;
                    uploadImagemAmazonS3(fotoNomeProduto, produtoId);

                    //Cria a foto média
                    tinify.Cover(fotoOtimizada, int.Parse(ConfigurationManager.AppSettings["fotoMedia"]), int.Parse(ConfigurationManager.AppSettings["fotoMedia"]), caminhoFoto + "media_" + fotoNomeProduto);
                    uploadImagemAmazonS3("media_" + fotoNomeProduto, produtoId);
                    /*Target = caminhoFoto + "media_" + fotoNomeProduto;
                    System.Threading.Thread.Sleep(500);
                    rnFotos.ResizeImageFile(int.Parse(ConfigurationManager.AppSettings["fotoMedia"]), Location, Target, ImageFormat.Jpeg);*/
                    /*gerarFotoWebP(caminhoFoto, "media_" + fotoNomeProduto, produtoId);

                    //Cria a foto pequena
                    tinify.Cover(fotoOtimizada, int.Parse(ConfigurationManager.AppSettings["fotoPequena"]), int.Parse(ConfigurationManager.AppSettings["fotoPequena"]), caminhoFoto + "pequena_" + fotoNomeProduto);
                    uploadImagemAmazonS3("pequena_" + fotoNomeProduto, produtoId);
                    /*Target = caminhoFoto + "pequena_" + fotoNomeProduto;
                    System.Threading.Thread.Sleep(500);
                    rnFotos.ResizeImageFile(int.Parse(ConfigurationManager.AppSettings["fotoPequena"]), Location, Target, ImageFormat.Jpeg);*/
                    /*gerarFotoWebP(caminhoFoto, "pequena_" + fotoNomeProduto, produtoId);

                    //Cria a foto 500 mobile
                    tinify.Cover(fotoOtimizada, 500, 500, caminhoFoto + "500_" + fotoNomeProduto);
                    uploadImagemAmazonS3("500_" + fotoNomeProduto, produtoId);
                    /*Target = caminhoFoto + "500_" + fotoNomeProduto;
                    System.Threading.Thread.Sleep(500);
                    rnFotos.ResizeImageFile(500, Location, Target, ImageFormat.Jpeg);*/
                    /*gerarFotoWebP(caminhoFoto, "500_" + fotoNomeProduto, produtoId);

                    //Cria a foto 125 mobile
                    tinify.Cover(fotoOtimizada, 125, 125, caminhoFoto + "125_" + fotoNomeProduto);
                    uploadImagemAmazonS3("125_" + fotoNomeProduto, produtoId);
                    /*Target = caminhoFoto + "125_" + fotoNomeProduto;
                    System.Threading.Thread.Sleep(500);
                    rnFotos.ResizeImageFile(125, Location, Target, ImageFormat.Jpeg);*/
                    /*gerarFotoWebP(caminhoFoto, "125_" + fotoNomeProduto, produtoId);

                    //Cria a foto 70 mobile
                    tinify.Cover(fotoOtimizada, 70, 70, caminhoFoto + "70_" + fotoNomeProduto);
                    uploadImagemAmazonS3("70_" + fotoNomeProduto, produtoId);
                    /*Target = caminhoFoto + "125_" + fotoNomeProduto;
                    System.Threading.Thread.Sleep(500);
                    rnFotos.ResizeImageFile(125, Location, Target, ImageFormat.Jpeg);*/
                    /*gerarFotoWebP(caminhoFoto, "70_" + fotoNomeProduto, produtoId);*/


                    if (i == 1)
                    {
                        var produtoData = new dbCommerceDataContext();
                        var produto = (from c in produtoData.tbProdutos where c.produtoId == produtoId select c).FirstOrDefault();
                        if (string.IsNullOrEmpty(produto.fotoDestaque))
                        {
                            produto.fotoDestaque = fotoNomeProduto;
                            produtoData.SubmitChanges();
                        }
                    }

                    /*using (var data = new dbCommerceDataContext()) // Service do Kraken
                    {
                        var queueCompactarFoto = new tbQueue
                        {
                            tipoQueue = 26,
                            agendamento = DateTime.Now.AddMinutes(5),
                            idRelacionado = fotoId,
                            mensagem = "",
                            concluido = false,
                            andamento = false
                        };
                        data.tbQueues.InsertOnSubmit(queueCompactarFoto);
                        data.SubmitChanges();
                    }*/
                }
            }
            #endregion Fotos Galeria

            #region Foto Produto Destaque Categoria

            try
            {
                FileUpload uplDestaqueCategoria = (FileUpload)pnlFotos.FindControl("upl4");

                if (uplDestaqueCategoria.HasFile)
                {
                    rnFotos.fotosInclui(produtoId, "destaquecategoria", "");
                    uplDestaqueCategoria.PostedFile.SaveAs(ConfigurationManager.AppSettings["caminhoFisico"] + "fotos\\" + produtoId + "\\" + "destaquecategoria.jpg");
                    uploadImagemAmazonS3("destaquecategoria.jpg", produtoId);
                }

            }
            catch (Exception)
            {


            }


            #endregion Foto Produto Destaque Categoria

        }
        catch (Exception ex)
        {
            Response.Write("<script>alert('" + ex.StackTrace + "');</script>");
        }

    }

    private void informacoesAdcionaisExclui(int produtoId)
    {
        rnInformacoesAdcionais.informacoesAdcionaisExclui(produtoId);
    }
    private void informacoesAdcionaisInclui(int produtoId)
    {
        foreach (DataListItem itemInfo in dtlInformacoesAdcionais.Items)
        {
            TextBox txtInformacaoAdcionalNome = (TextBox)itemInfo.FindControl("txtInformacaoAdcionalNome");
            CuteEditor.Editor txtInformacaoAdcional = (CuteEditor.Editor)itemInfo.FindControl("txtInformacaoAdcional");

            rnInformacoesAdcionais.produtoInformacoesAdcionaisInclui(produtoId, txtInformacaoAdcionalNome.Text, txtInformacaoAdcional.Text);
        }
    }

    private void linkVideoInclui(int produtoId)
    {
        using (var data = new dbCommerceDataContext())
        {
            var linksExcluir = (from v in data.tbProdutoVideos where v.produtoId == produtoId select v).ToList();

            foreach (var link in linksExcluir)
            {
                data.tbProdutoVideos.DeleteOnSubmit(link);
                data.SubmitChanges();
            }

            foreach (var link in ListaLinks)
            {
                var adicionaLink = new tbProdutoVideo();
                adicionaLink.linkVideo = link.Link;
                adicionaLink.produtoId = produtoId;
                adicionaLink.dataCadastro = DateTime.Now;
                data.tbProdutoVideos.InsertOnSubmit(adicionaLink);
                data.SubmitChanges();
            }
        }
    }

    private void linkListaDb(int produtoId)
    {
        using (var data = new dbCommerceDataContext())
        {
            var linksCadastrados = (from v in data.tbProdutoVideos where v.produtoId == produtoId select v).ToList();

            try
            {
                List<Links> linksListaDb = new List<Links>();

                foreach (var item in linksCadastrados)
                {
                    Links _link = new Links();
                    _link.Link = item.linkVideo;

                    linksListaDb.Add(_link);
                }

                ListaLinks = linksListaDb;

                CarregarLinks();
            }
            catch (Exception ex)
            {

            }
        }
    }

    protected void btnProdutoRelacionadoAdicionar_OnClick(object sender, EventArgs e)
    {
        int produtoPai = int.Parse((Request.QueryString["produtoId"].ToString()));
        var produtoDc = new dbCommerceDataContext();
        int desconto = 0;
        int.TryParse(txtProdutoRelacionadoDesconto.Text, out desconto);

        var ids = txtProdutoRelacionadoId.Text.Split(';');
        var listaMultiplasOpcoes = new List<string>();

        int quantidade = 1;
        if (!string.IsNullOrEmpty(txtProdutoRelacionadoQuantidade.Text))
        {
            int.TryParse(txtProdutoRelacionadoQuantidade.Text, out quantidade);
        }
        if (quantidade == 0) quantidade = 1;

        if (lstProdutoRelacionadoComplemento.Visible == false)
        {
            foreach (var id in ids)
            {
                int produtoIdCombo = Convert.ToInt32(id);
                var produtos = (from c in produtoDc.tbProdutos where c.produtoId == produtoIdCombo select c);
                if (produtos.Count() > 1)
                {
                    listaMultiplasOpcoes.Add(id);
                    lstProdutoRelacionadoComplemento.Visible = true;
                }
                else if (produtos.Count() == 1)
                {
                    int produtoId = produtos.First().produtoId;
                    var produtosRelacionados = (from c in produtoDc.tbProdutoRelacionados where c.idProdutoPai == produtoId select c);
                    if (produtosRelacionados.Any())
                    {
                        foreach (var relacionado in produtosRelacionados)
                        {
                            var possuiEsteProduto = validaSePossuiProdutoRelacionado(relacionado.idProdutoFilho);
                            if (chkProdutoRelacionadoValidar.Checked == false) possuiEsteProduto = false;
                            //Response.Write(produtoId + "<br>" + possuiEsteProduto);
                            if (!possuiEsteProduto)
                            {
                                for (int i = 1; i <= quantidade; i++)
                                {
                                    var produtoRelacionado = new tbProdutoRelacionado();
                                    produtoRelacionado.idProdutoPai = produtoPai;
                                    produtoRelacionado.idProdutoFilho = relacionado.idProdutoFilho;
                                    produtoRelacionado.desconto = desconto;
                                    produtoDc.tbProdutoRelacionados.InsertOnSubmit(produtoRelacionado);
                                    produtoDc.SubmitChanges();
                                }
                                txtProdutoRelacionadoId.Text = "";
                                txtProdutoRelacionadoDesconto.Text = "0";
                                fillRelacionados();
                            }
                        }
                    }
                    else
                    {
                        var possuiEsteProduto = validaSePossuiProdutoRelacionado(produtoId);
                        if (chkProdutoRelacionadoValidar.Checked == false) possuiEsteProduto = false;
                        //Response.Write(produtoId + "<br>" + possuiEsteProduto);
                        if (!possuiEsteProduto)
                        {
                            for (int i = 1; i <= quantidade; i++)
                            {
                                var produtoRelacionado = new tbProdutoRelacionado();
                                produtoRelacionado.idProdutoPai = produtoPai;
                                produtoRelacionado.idProdutoFilho = produtoId;
                                produtoRelacionado.desconto = desconto;
                                produtoDc.tbProdutoRelacionados.InsertOnSubmit(produtoRelacionado);
                                produtoDc.SubmitChanges();
                            }
                            txtProdutoRelacionadoId.Text = "";
                            txtProdutoRelacionadoDesconto.Text = "0";
                            fillRelacionados();
                        }
                        else
                        {
                            Response.Write("<script>alert('Produto relacionado já cadastrado.');</script>");
                        }
                    }

                }
                else
                {
                    Response.Write("<script>alert('Produto não localizado.');</script>");
                }
            }
            if (listaMultiplasOpcoes.Count > 0)
            {
                Response.Write("<script>alert('Produto com múltiplas opções. Favor selecionar da lista e adicionar.');</script>");
                lstProdutoRelacionadoComplemento.DataSource = listaMultiplasOpcoes;
                lstProdutoRelacionadoComplemento.DataBind();
            }
        }
        else
        {
            foreach (var item in lstProdutoRelacionadoComplemento.Items)
            {
                var ddlProdutoRelacionadoComplemento = (DropDownList)item.FindControl("ddlProdutoRelacionadoComplemento");
                int produtoId = Convert.ToInt32(ddlProdutoRelacionadoComplemento.SelectedValue);
                var produtosRelacionados = (from c in produtoDc.tbProdutoRelacionados where c.idProdutoPai == produtoId select c);
                if (produtosRelacionados.Any())
                {
                    foreach (var relacionado in produtosRelacionados)
                    {
                        var possuiEsteProduto = validaSePossuiProdutoRelacionado(relacionado.idProdutoFilho);
                        if (chkProdutoRelacionadoValidar.Checked == false) possuiEsteProduto = false;
                        //Response.Write(produtoId + "<br>" + possuiEsteProduto);
                        if (!possuiEsteProduto)
                        {
                            for (int i = 1; i <= quantidade; i++)
                            {
                                var produtoRelacionado = new tbProdutoRelacionado();
                                produtoRelacionado.idProdutoPai = produtoPai;
                                produtoRelacionado.idProdutoFilho = relacionado.idProdutoFilho;
                                produtoRelacionado.desconto = desconto;
                                produtoDc.tbProdutoRelacionados.InsertOnSubmit(produtoRelacionado);
                                produtoDc.SubmitChanges();
                            }
                            txtProdutoRelacionadoId.Text = "";
                            txtProdutoRelacionadoDesconto.Text = "0";
                            fillRelacionados();
                        }
                    }
                }
                else
                {
                    bool validaSePossuiProduto = validaSePossuiProdutoRelacionado(produtoId);

                    if (chkProdutoRelacionadoValidar.Checked == false) validaSePossuiProduto = false;
                    if (!validaSePossuiProduto)
                    {
                        for (int i = 1; i <= quantidade; i++)
                        {
                            var produtoRelacionado = new tbProdutoRelacionado();
                            produtoRelacionado.idProdutoPai = produtoPai;
                            produtoRelacionado.idProdutoFilho = produtoId;
                            produtoRelacionado.desconto = desconto;
                            produtoDc.tbProdutoRelacionados.InsertOnSubmit(produtoRelacionado);
                            produtoDc.SubmitChanges();
                        }
                        txtProdutoRelacionadoId.Text = "";
                        ddlProdutoRelacionadoComplemento.Items.Clear();
                        Response.Write("<script>alert('Produto relacionado inserido com sucesso.');</script>");
                        txtProdutoRelacionadoDesconto.Text = "0";
                        fillRelacionados();
                    }
                    else
                    {
                        Response.Write("<script>alert('Produto relacionado já cadastrado.');</script>");
                    }
                }
            }
            lstProdutoRelacionadoComplemento.Visible = false;
        }


        //rnProdutos.AtualizaDescontoCombo(produtoPai);
        //rnBuscaCloudSearch.AtualizarProduto(produtoPai);

        btn.Focus();
    }

    protected void btnProdutoPersonalizadoAdicionar_OnClick(object sender, EventArgs e)
    {
        int produtoPai = int.Parse((Request.QueryString["produtoId"]));
        int produtoPersonalizadoFilho = int.Parse((txtIdProdutoPersonalizacao.Text == "" ? "0" : txtIdProdutoPersonalizacao.Text));
        string produtoIdEmpresa = txtIdEmpresaPersonalizacao.Text;
        string tabProdutoPersonalizado = txtTagProdutoPersonalizacao.Text;
        List<int> idsProdutos = new List<int>();

        var produtoDc = new dbCommerceDataContext();

        if (lstProdutoPersonalizadoRelacionado.Items.Count == 0)
        {

            if (!string.IsNullOrEmpty(produtoIdEmpresa))
            {
                var produtosPorIdEmpresa = (from c in produtoDc.tbProdutos select c).Where(x => x.produtoIdDaEmpresa.StartsWith(produtoIdEmpresa)).ToList();

                idsProdutos.AddRange(produtosPorIdEmpresa.Select(produto => produto.produtoId));

                foreach (var produto in idsProdutos)
                {
                    var produtoPersonalizadoRelacionado = new tbProdutoPersonalizacao
                    {
                        idProdutoPai = produtoPai,
                        idProdutoFilhoPersonalizacao = produto,
                        tagProdutoPersonalizado = tabProdutoPersonalizado,
                        dataCadastro = DateTime.Now
                    };
                    produtoDc.tbProdutoPersonalizacaos.InsertOnSubmit(produtoPersonalizadoRelacionado);
                    produtoDc.SubmitChanges();
                }
            }
            else
            {
                var produtoPersonalizadoRelacionado = new tbProdutoPersonalizacao
                {
                    idProdutoPai = produtoPai,
                    idProdutoFilhoPersonalizacao = produtoPersonalizadoFilho,
                    tagProdutoPersonalizado = tabProdutoPersonalizado,
                    dataCadastro = DateTime.Now
                };
                produtoDc.tbProdutoPersonalizacaos.InsertOnSubmit(produtoPersonalizadoRelacionado);
                produtoDc.SubmitChanges();
            }

            fillPersonalizadosRelacionados();
            Response.Write("<script>alert('Produto de personalização inserido com sucesso.');</script>");
        }
        else
        {

            var produtosPersonalizacaoRelacionados = (from c in produtoDc.tbProdutoPersonalizacaos where c.idProdutoPai == produtoPai select c);
            if (produtosPersonalizacaoRelacionados.Any())
            {
                bool possuiEsteProduto;

                if (!string.IsNullOrEmpty(produtoIdEmpresa))
                {
                    var produtosPorIdEmpresa = (from c in produtoDc.tbProdutos select c).Where(x => x.produtoIdDaEmpresa.StartsWith(produtoIdEmpresa)).ToList();

                    idsProdutos.AddRange(produtosPorIdEmpresa.Select(produto => produto.produtoId));
                }

                if (idsProdutos.Count > 0)
                {
                    foreach (var produto in idsProdutos)
                    {
                        possuiEsteProduto = validaSePossuiProdutoPersonalizadoRelacionado(produto);

                        if (!possuiEsteProduto)
                        {
                            var produtoPersonalizadoRelacionado = new tbProdutoPersonalizacao
                            {
                                idProdutoPai = produtoPai,
                                idProdutoFilhoPersonalizacao = produto,
                                tagProdutoPersonalizado = tabProdutoPersonalizado,
                                dataCadastro = DateTime.Now
                            };
                            produtoDc.tbProdutoPersonalizacaos.InsertOnSubmit(produtoPersonalizadoRelacionado);
                            produtoDc.SubmitChanges();



                        }
                    }
                    fillPersonalizadosRelacionados();
                    Response.Write("<script>alert('Produto de personalização inserido com sucesso.');</script>");
                }
                else
                {
                    possuiEsteProduto = validaSePossuiProdutoPersonalizadoRelacionado(produtoPersonalizadoFilho);

                    if (!possuiEsteProduto)
                    {
                        var produtoPersonalizadoRelacionado = new tbProdutoPersonalizacao
                        {
                            idProdutoPai = produtoPai,
                            idProdutoFilhoPersonalizacao = produtoPersonalizadoFilho,
                            tagProdutoPersonalizado = tabProdutoPersonalizado,
                            dataCadastro = DateTime.Now
                        };
                        produtoDc.tbProdutoPersonalizacaos.InsertOnSubmit(produtoPersonalizadoRelacionado);
                        produtoDc.SubmitChanges();


                        fillPersonalizadosRelacionados();
                        Response.Write("<script>alert('Produto de personalização inserido com sucesso.');</script>");
                    }
                }
            }
            else
            {
                bool validaSePossuiProduto = validaSePossuiProdutoPersonalizadoRelacionado(produtoPersonalizadoFilho);

                //if (chkProdutoRelacionadoValidar.Checked == false) validaSePossuiProduto = false;
                if (!validaSePossuiProduto)
                {

                    var produtoPersonalizadoRelacionado = new tbProdutoPersonalizacao
                    {
                        idProdutoPai = produtoPai,
                        idProdutoFilhoPersonalizacao = produtoPersonalizadoFilho,
                        tagProdutoPersonalizado = tabProdutoPersonalizado,
                        dataCadastro = DateTime.Now
                    };
                    produtoDc.tbProdutoPersonalizacaos.InsertOnSubmit(produtoPersonalizadoRelacionado);
                    produtoDc.SubmitChanges();

                    Response.Write("<script>alert('Produto de personalização inserido com sucesso.');</script>");
                    fillPersonalizadosRelacionados();
                }
                else
                {
                    Response.Write("<script>alert('Produto personalizado já cadastrado.');</script>");
                }
            }

        }
    }

    private bool validaSePossuiProdutoRelacionado(int produtoId)
    {
        int produtoPai = int.Parse((Request.QueryString["produtoId"].ToString()));
        var data = new dbCommerceDataContext();
        int quantidade =
            (from c in data.tbProdutoRelacionados
             where c.idProdutoFilho == produtoId && c.idProdutoPai == produtoPai
             select c).Count();
        return (quantidade > 0);

    }

    private bool validaSePossuiProdutoPersonalizadoRelacionado(int produtoId)
    {
        int produtoPai = int.Parse((Request.QueryString["produtoId"].ToString()));
        var data = new dbCommerceDataContext();
        int quantidade =
            (from c in data.tbProdutoPersonalizacaos
             where c.idProdutoFilhoPersonalizacao == produtoId && c.idProdutoPai == produtoPai
             select c).Count();
        return (quantidade > 0);

    }

    protected void btnRemoverProdutoRelacionado_OnCommand(object sender, CommandEventArgs e)
    {
        int produtoPai = int.Parse((Request.QueryString["produtoId"].ToString()));
        int produtoRelacionadoId = Convert.ToInt32(e.CommandArgument);
        var data = new dbCommerceDataContext();
        var produtoRelacionado =
            (from c in data.tbProdutoRelacionados where c.idProdutoRelacionado == produtoRelacionadoId select c)
                .FirstOrDefault();
        if (produtoRelacionado != null)
        {
            data.tbProdutoRelacionados.DeleteOnSubmit(produtoRelacionado);
            data.SubmitChanges();
        }
        Response.Write("<script>alert('Produto relacionado removido com sucesso.');</script>");
        fillRelacionados();

        //rnProdutos.AtualizaDescontoCombo(produtoPai);
        //rnBuscaCloudSearch.AtualizarProduto(produtoPai);


        btn.Focus();
    }

    protected void btnRemoverProdutoPersonalizacao_OnCommand(object sender, CommandEventArgs e)
    {
        int produtoPersonalizacaoId = Convert.ToInt32(e.CommandArgument);
        using (var data = new dbCommerceDataContext())
        {
            var produtoPersonalizacao = (from c in data.tbProdutoPersonalizacaos where c.idPersonalizacao == produtoPersonalizacaoId select c).FirstOrDefault();

            if (produtoPersonalizacao != null)
            {
                data.tbProdutoPersonalizacaos.DeleteOnSubmit(produtoPersonalizacao);
                data.SubmitChanges();
            }
        }

        Response.Write("<script>alert('Produto de personalização removido com sucesso.');</script>");
        fillPersonalizadosRelacionados();
    }

    protected void btnSalvarDadosTags_OnClick(object sender, EventArgs e)
    {
        try
        {
            foreach (var item in lstProdutoPersonalizadoRelacionado.Items)
            {
                TextBox txtTagProdutoPersonalizado = (TextBox)item.FindControl("txtTagProdutoPersonalizado");
                HiddenField hfIdPersonalizacao = (HiddenField)item.FindControl("hfIdPersonalizacao");
                int idPersonalizacao = Convert.ToInt32(hfIdPersonalizacao.Value);

                using (var data = new dbCommerceDataContext())
                {
                    var produtoPersonalizado = (from c in data.tbProdutoPersonalizacaos where c.idPersonalizacao == idPersonalizacao select c).First();
                    produtoPersonalizado.tagProdutoPersonalizado = txtTagProdutoPersonalizado.Text;
                    data.SubmitChanges();
                }

            }

            Response.Write("<script>alert('Informações de tags salvas com sucesso.');</script>");
        }
        catch (Exception ex)
        {


        }

    }

    protected void btnRemoverPersonalizacaoEmLote_OnClick(object sender, EventArgs e)
    {
        try
        {
            foreach (var item in lstProdutoPersonalizadoRelacionado.Items)
            {
                CheckBox ckbRemoverPersonalizacaoEmLote = (CheckBox)item.FindControl("ckbRemoverPersonalizacaoEmLote");
                HiddenField hfIdPersonalizacao = (HiddenField)item.FindControl("hfIdPersonalizacao");
                int idPersonalizacao = Convert.ToInt32(hfIdPersonalizacao.Value);

                if (ckbRemoverPersonalizacaoEmLote.Checked)
                {
                    using (var data = new dbCommerceDataContext())
                    {
                        var produtoPersonalizado = (from c in data.tbProdutoPersonalizacaos where c.idPersonalizacao == idPersonalizacao select c).First();
                        data.tbProdutoPersonalizacaos.DeleteOnSubmit(produtoPersonalizado);
                        data.SubmitChanges();
                    }
                }


            }

            fillPersonalizadosRelacionados();

            Response.Write("<script>alert('Personalizações removidas com sucesso.');</script>");
        }
        catch (Exception ex)
        {


        }
    }

    private void fillRelacionados()
    {
        int produtoPai = int.Parse((Request.QueryString["produtoId"].ToString()));
        var data = new dbCommerceDataContext();
        var relacionados = (from c in data.tbProdutoRelacionados
                            join d in data.tbProdutos on c.idProdutoFilho equals d.produtoId
                            where c.idProdutoPai == produtoPai
                            select
                                new
                                {
                                    d.produtoIdDaEmpresa,
                                    d.complementoIdDaEmpresa,
                                    d.produtoId,
                                    d.fotoDestaque,
                                    d.produtoNome,
                                    c.idProdutoRelacionado,
                                    c.desconto,
                                    d.produtoPrecoDeCusto,
                                    d.produtoPeso,
                                    d.produtoPreco,
                                    d.produtoPrecoPromocional
                                });
        lstProdutosRelacionados.DataSource = relacionados;
        lstProdutosRelacionados.DataBind();
        litProdutosRelacionados.Text = relacionados.Count().ToString();
        if (relacionados.Any())
        {
            decimal totalCusto = 0;
            decimal.TryParse(relacionados.Where(x => x.desconto == 0).Sum(x => x.produtoPrecoDeCusto).ToString(), out totalCusto);
            decimal totalVenda = relacionados.Sum(x => (x.produtoPrecoPromocional ?? 0) > 0 ? (decimal)x.produtoPrecoPromocional : x.produtoPreco);
            decimal totalGeral = relacionados.Sum(x => x.produtoPreco);

            litCustoCombo.Text = totalCusto.ToString("C");
            litValorTotalGeral.Text = totalGeral.ToString("C");
            litValorTotalVenda.Text = totalVenda.ToString("C");

            double pesoComparar = (relacionados.Sum(x => (x.produtoPeso == 0 ? 1 : x.produtoPeso)));

            double pesoAtual;
            double.TryParse(txtProdutoPeso.Text, out pesoAtual);

            if (pesoAtual < pesoComparar)
            {
                litPesoCombo.Text = pesoComparar.ToString();
            }
            else
            {
                litPesoCombo.Text = "Peso OK - " + pesoAtual;
            }
        }
    }

    private void fillPersonalizadosRelacionados()
    {
        int produtoPai = int.Parse((Request.QueryString["produtoId"].ToString()));
        using (var data = new dbCommerceDataContext())
        {
            var relacionados = (from c in data.tbProdutoPersonalizacaos
                                join d in data.tbProdutos on c.idProdutoFilhoPersonalizacao equals d.produtoId
                                where c.idProdutoPai == produtoPai
                                select
                                    new
                                    {
                                        d.produtoIdDaEmpresa,
                                        d.complementoIdDaEmpresa,
                                        d.produtoId,
                                        d.fotoDestaque,
                                        d.produtoNome,
                                        c.idPersonalizacao,
                                        d.produtoPrecoDeCusto,
                                        d.produtoPeso,
                                        c.tagProdutoPersonalizado
                                    });

            lstProdutoPersonalizadoRelacionado.DataSource = relacionados;
            lstProdutoPersonalizadoRelacionado.DataBind();
            litQtdProdutoPersonalizacao.Text = relacionados.Count().ToString();

            btnSalvarDadosTags.Visible = btnRemoverPersonalizacaoEmLote.Visible = lstProdutoPersonalizadoRelacionado.Items.Count > 0;
        }


    }

    protected void lstProdutoRelacionadoComplemento_OnItemDataBound(object sender, ListViewItemEventArgs e)
    {
        var litProdutoRelacionadoId = (Literal)e.Item.FindControl("litProdutoRelacionadoId");
        var ddlProdutoRelacionadoComplemento = (DropDownList)e.Item.FindControl("ddlProdutoRelacionadoComplemento");

        var produtoDc = new dbCommerceDataContext();
        var produtos = (from c in produtoDc.tbProdutos where c.produtoIdDaEmpresa == litProdutoRelacionadoId.Text select c);
        foreach (var produto in produtos)
        {
            ddlProdutoRelacionadoComplemento.Items.Add(new ListItem(produto.complementoIdDaEmpresa, produto.produtoId.ToString()));
        }
    }
    protected void chkColecoes_OnDataBound(object sender, EventArgs e)
    {
        litColecoesMarcadas.Text = "";
        var produtosDc = new dbCommerceDataContext();
        int produtoId = int.Parse(Request.QueryString["produtoId"]);
        var categoriasProduto = (from c in produtosDc.tbJuncaoProdutoColecaos where c.produtoId == produtoId select c).ToList();
        foreach (var colecao in categoriasProduto)
        {
            var itemCheck = chkColecoes.Items.FindByValue(colecao.colecaoId.ToString());
            if (itemCheck != null)
            {
                itemCheck.Selected = true;
                litColecoesMarcadas.Text += colecao.tbColecao.colecaoNome + " | ";
            }

        }

        if (litColecoesMarcadas.Text.Length > 777)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "document.getElementById('divListaColecoesSelecionadas').style.height = '86px';" +
                                                                                            "document.getElementById('divListaColecoesSelecionadas').style.overflow = 'auto';", true);
        }
    }
    protected void btnGravarFotos_Click(object sender, EventArgs e)
    {
        int produtoId = int.Parse(Request.QueryString["produtoId"]);
        fotoExclui(produtoId);
        fotoAltera();
        fotoDestaqueAltera(produtoId);
        fotoInclui(produtoId);
        //rnBuscaCloudSearch.AtualizarProduto(produtoId);
        Response.Write("<script>alert('Fotos alteradas com sucesso.');</script>");
        Response.Write("<script>window.location=('" + Request.Url.ToString() + "');</script>");
    }
    protected void btnAdicionarComentariosFace_OnClick(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(txtComentariosFaceIdPost.Text))
        {
            var data = new dbCommerceDataContext();
            var comentarios = new tbProdutoComentariosFacebook();
            comentarios.idPostagem = txtComentariosFaceIdPost.Text;
            comentarios.idProduto = int.Parse(Request.QueryString["produtoId"]);
            data.tbProdutoComentariosFacebooks.InsertOnSubmit(comentarios);
            data.SubmitChanges();
            fillComentariosFace();
        }
    }
    protected void btnRemoverComentarioFace_OnCommand(object sender, CommandEventArgs e)
    {
        int idComentarioFace = Convert.ToInt32(e.CommandArgument);
        var data = new dbCommerceDataContext();
        var comentario =
            (from c in data.tbProdutoComentariosFacebooks
             where c.idProdutoComentarioFacebook == idComentarioFace
             select c).First();
        data.tbProdutoComentariosFacebooks.DeleteOnSubmit(comentario);
        data.SubmitChanges();
        fillComentariosFace();
    }
    private void fillComentariosFace()
    {
        int produtoId = int.Parse(Request.QueryString["produtoId"]);
        var data = new dbCommerceDataContext();
        var comentarios = (from c in data.tbProdutoComentariosFacebooks where c.idProduto == produtoId select c);
        lstComentariosFace.DataSource = comentarios;
        lstComentariosFace.DataBind();
    }

    private void verificaVinculoComBanner(int produtoId)
    {

        try
        {
            using (var data = new dbCommerceDataContext())
            {

                var banners = (from b in data.tbBanners
                               join p in data.tbProdutos on b.idProduto equals p.produtoId
                               where b.idProduto == produtoId && b.ativo == true
                               select new { b.linkProduto, b.foto, b.local, p.produtoNome });

                if (banners != null)
                {
                    int qtdBanners = banners.Count();

                    string msg = "";
                    msg += "Houve alteração no produto <b>" + banners.SingleOrDefault().produtoNome + "</b><br>";
                    msg += "Link do produto: " + banners.SingleOrDefault().linkProduto + "<br>";
                    msg += (qtdBanners > 1 ? "Existem " + qtdBanners.ToString() + " banners vinculados" : "Existe 1 banner vinculado") + " ao produto, favor verificar se há necessidade de alterar o banner.<br>";

                    foreach (var banner in banners)
                    {
                        msg += banner.local == 1 ? ConfigurationManager.AppSettings["caminhoVirtual"] + "banners\\home\\" + banner.foto : ConfigurationManager.AppSettings["caminhoVirtual"] + "banners\\" + banner.foto + "<br>";
                    }

                    rnEmails.EnviaEmail("", "atendimento01@bark.com.br", "", "renato@bark.com.br", "", msg, "Verificar necessidade de alterar banner");
                }
            }
        }
        catch (Exception)
        {


        }

    }

    #region tabela descricao
    private void tabelaDescricaoInclui(int produtoId)
    {
        var produtoDc = new dbCommerceDataContext();
        var tabelasAtuais = (from c in produtoDc.tbProdutoDescricaoTabelaGrupos where c.produtoId == produtoId select c);
        foreach (ListItem tabelaItem in chkTabelaDescricoes.Items)
        {
            int idDescricaoTabelaProdutoGrupo = Convert.ToInt32(tabelaItem.Value);
            var possuiAtualmente = tabelasAtuais.Any(x => x.idDescricaoTabelaGrupo == idDescricaoTabelaProdutoGrupo);
            if (tabelaItem.Selected && possuiAtualmente == false)
            {
                var tabelaAdicionar = new tbProdutoDescricaoTabelaGrupo();
                tabelaAdicionar.produtoId = produtoId;
                tabelaAdicionar.idDescricaoTabelaGrupo = idDescricaoTabelaProdutoGrupo;
                produtoDc.tbProdutoDescricaoTabelaGrupos.InsertOnSubmit(tabelaAdicionar);
                produtoDc.SubmitChanges();
            }
            else if (tabelaItem.Selected == false && possuiAtualmente)
            {
                var tabelaExcluir =
                    (from c in produtoDc.tbProdutoDescricaoTabelaGrupos
                     where c.produtoId == produtoId && c.idDescricaoTabelaGrupo == idDescricaoTabelaProdutoGrupo
                     select c).First();
                produtoDc.tbProdutoDescricaoTabelaGrupos.DeleteOnSubmit(tabelaExcluir);
                produtoDc.SubmitChanges();
            }
        }
    }
    protected void chkTabelaDescricoes_OnDataBound(object sender, EventArgs e)
    {
        var produtosDc = new dbCommerceDataContext();
        int produtoId = int.Parse(Request.QueryString["produtoId"]);
        var tabelasProduto = (from c in produtosDc.tbProdutoDescricaoTabelaGrupos where c.produtoId == produtoId select c);
        foreach (var tabelaProduto in tabelasProduto)
        {
            var itemCheck = chkTabelaDescricoes.Items.FindByValue(tabelaProduto.idDescricaoTabelaGrupo.ToString());
            if (itemCheck != null) itemCheck.Selected = true;
        }
        fillTabelaDescricoes();
    }

    protected void lstTabelasDescricao_OnItemDataBound(object sender, ListViewItemEventArgs e)
    {
        ListView lstProdutosSimulacao = (ListView)e.Item.FindControl("lstProdutosSimulacao");
        var dataItem = (tbDescricaoTabelaProdutoGrupo)e.Item.DataItem;
        var data = new dbCommerceDataContext();
        var itens = (from c in data.tbDescricaoTabelaProdutoGrupoItems
                     where c.idDescricaoTabelaProdutoGrupo == dataItem.idDescricaoTabelaProdutoGrupo
                     select c);
        lstProdutosSimulacao.DataSource = itens;
        lstProdutosSimulacao.DataBind();
    }

    protected void lstProdutosSimulacao_OnItemDataBound(object sender, ListViewItemEventArgs e)
    {
        Literal litNomeAmigavel = (Literal)e.Item.FindControl("litNomeAmigavel");
        ListView lstProdutosSimulacaoItensQuantidades = (ListView)e.Item.FindControl("lstProdutosSimulacaoItensQuantidades");
        ListView lstProdutosSimulacaoItensDescricoes = (ListView)e.Item.FindControl("lstProdutosSimulacaoItensDescricoes");
        var item = (tbDescricaoTabelaProdutoGrupoItem)e.Item.DataItem;
        var data = new dbCommerceDataContext();
        var produto =
            (from c in data.tbDescricaoTabelaProdutos
             where c.idDescricaoTabelaProduto == item.idDescricaoTabelaProduto
             select c).FirstOrDefault();
        if (produto != null)
        {
            litNomeAmigavel.Text = produto.nomeAmigavel;
        }
        var itens =
            (from c in data.tbDescricaoTabelaProdutoItems
             where c.idDescricaoTabelaProduto == item.idDescricaoTabelaProduto
             select c);
        lstProdutosSimulacaoItensQuantidades.DataSource = itens;
        lstProdutosSimulacaoItensQuantidades.DataBind();
        lstProdutosSimulacaoItensDescricoes.DataSource = itens;
        lstProdutosSimulacaoItensDescricoes.DataBind();
    }

    protected void chkTabelaDescricoes_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        fillTabelaDescricoes();
        pnlTabelaDescricao.Focus();
    }

    private void fillTabelaDescricoes()
    {
        List<int> itensSelecionados = new List<int>();
        foreach (ListItem tabelaItem in chkTabelaDescricoes.Items)
        {
            if (tabelaItem.Selected)
            {
                itensSelecionados.Add(Convert.ToInt32(tabelaItem.Value));
            }
        }

        var data = new dbCommerceDataContext();
        var grupos =
            (from c in data.tbDescricaoTabelaProdutoGrupos
             where itensSelecionados.Contains(c.idDescricaoTabelaProdutoGrupo)
             select c);
        lstTabelasDescricao.DataSource = grupos;
        lstTabelasDescricao.DataBind();
    }
    #endregion

    protected void ddlSite_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        PopulateRootLevel();
        //treeCategorias.ExpandAll();
        treeCategorias.CollapseAll();
        treeCategoriasComFilhos.CollapseAll();
    }
    protected void btnAddLink_Click(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(txtLink.Text))
        {
            Response.Write("<script>alert('Informe a url do video!\\nMantenham a estrutura da url!\\nSiga o formato: www.youtube.com/watch?v=AEd6USvIPNc');</script>");
            return;
        }

        if (txtLink.Text.IndexOf("watch?v") == -1)
        {
            Response.Write("<script>alert('Mantenham a estrutura da url!\\nSiga o formato: www.youtube.com/watch?v=AEd6USvIPNc');</script>");
            return;
        }

        SalvarLinksNaLista();
        AlterarQuantidade();
    }

    /// <summary>
    /// Método responsável por salvar o que foi digitado na lista de links
    /// </summary>
    private void SalvarLinksNaLista()
    {
        try
        {
            List<Links> novaLista = new List<Links>();

            foreach (RepeaterItem item in rptLinks.Items)
            {
                TextBox txtLink = (TextBox)item.FindControl("txtLink");
                //TextBox txtEmail = (TextBox)item.FindControl("txtEmail");

                if (txtLink != null)
                {
                    Links _link = new Links();
                    _link.Link = txtLink.Text;

                    novaLista.Add(_link);
                }
            }

            ListaLinks = novaLista;

        }
        catch (Exception ex)
        {

        }
    }

    /// <summary>
    /// Método responsável por alterar a quantidade de links
    /// </summary>
    private void AlterarQuantidade()
    {
        try
        {
            int quantidade = rptLinks.Items.Count + 1;
            List<Links> novaLista = new List<Links>();

            if (quantidade < 1)
            {
                quantidade = 1;
            }

            //redimensiona a lista de checklists
            if (quantidade > ListaLinks.Count)
            {
                //se a quantidade for superior a lista atual, 
                //copia os primeiras X checklists (baseado na quantidade)
                for (int i = 0; i < ListaLinks.Count; i++)
                {
                    novaLista.Add(ListaLinks[i]);
                }

                //como a quantidade escolhida é superior a lista atual, acrescenta a diferença em branco
                for (int i = ListaLinks.Count; i < quantidade; i++)
                {
                    novaLista.Add(new Links() { Link = txtLink.Text });
                }

                txtLink.Text = string.Empty;
            }
            else
            {
                //se a quantidade for inferior a lista atual, copia as primeiras apenas
                for (int i = 0; i < quantidade; i++)
                {
                    novaLista.Add(ListaLinks[i]);
                }
            }

            ListaLinks = novaLista;
            CarregarLinks();
        }
        catch (Exception ex)
        {

        }
    }

    /// <summary>
    /// Método responsável por carregar os dados dos links
    /// </summary>
    private void CarregarLinks()
    {
        //if (ListaLinks.Count.Equals(0))
        //{
        //    ListaLinks.Add(new Links());
        //}

        //txtQuantidade.Text = ListaLinks.Count.ToString();

        rptLinks.DataSource = ListaLinks;
        rptLinks.DataBind();
    }

    /// <summary>
    /// Define a estrutura dos links
    /// </summary>
    [Serializable]
    private class Links
    {
        public string Link { get; set; }

        public Links()
        {
            this.Link = string.Empty;
        }
    }

    private List<Links> ListaLinks
    {
        get
        {
            List<Links> _listaLinks = (List<Links>)ViewState["ListaLinks"];

            if (_listaLinks == null)
            {
                _listaLinks = new List<Links>();
                ListaLinks = _listaLinks;
            }
            return _listaLinks;
        }
        set
        {
            ViewState["ListaLinks"] = value;
        }
    }
    protected void btnRemoverLink_Command(object sender, CommandEventArgs e)
    {
        var index = Convert.ToInt32(e.CommandArgument);
        ListaLinks.RemoveAt(index);
        CarregarLinks();
    }

    //protected void chkProcessosFabrica_OnDataBound(object sender, EventArgs e)
    //{
    //    var produtosDc = new dbCommerceDataContext();
    //    int produtoId = int.Parse(Request.QueryString["produtoId"]);
    //    var categoriasProduto = (from c in produtosDc.tbJuncaoProdutoProcessoFabricas where c.idProduto == produtoId select c);
    //    foreach (var processo in categoriasProduto)
    //    {
    //        var itemCheck = chkProcessosFabrica.Items.FindByValue(processo.idProcessoFabrica.ToString());
    //        if (itemCheck != null) itemCheck.Selected = true;
    //    }
    //}

    private void uploadImagemAmazonS3(string foto, int produtoId)
    {
        string s3ServiceUrl = "s3-sa-east-1.amazonaws.com";
        string bucketName = "cdn2.graodegente.com.br";
        string filePath = ConfigurationManager.AppSettings["caminhoFisico"] + "fotos\\" + produtoId + "\\" + foto;
        string fileContentType = "image/jpg";
        S3CannedACL fileCannedACL = S3CannedACL.PublicReadWrite;

        string accessKey = "AKIAIP7IE6WVNK2K6TIQ";
        string secretAccessKey = "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";

        try
        {
            AmazonS3Config config = new AmazonS3Config();
            config.ServiceURL = s3ServiceUrl;
            config.CommunicationProtocol = Protocol.HTTP;

            using (var s3Client = Amazon.AWSClientFactory.CreateAmazonS3Client(accessKey, secretAccessKey, config))
            {
                PutObjectRequest request = new PutObjectRequest
                {
                    BucketName = bucketName,
                    CannedACL = fileCannedACL,
                    Key = "fotos/" + produtoId + "/" + foto,
                    FilePath = filePath,
                    ContentType = fileContentType
                };
                request.AddHeader("expires", "Thu, 21 Mar 2042 08:16:32 GMT");
                PutObjectResponse response = s3Client.PutObject(request);

            }

        }
        catch (AmazonS3Exception s3Exception)
        {
            Response.Write(s3Exception.Message + " - " + s3Exception.InnerException);

            Console.WriteLine(s3Exception.Message, s3Exception.InnerException);

            Console.ReadKey();
        }
    }
    private void gerarFotoWebP(string caminhoFoto, string nomeArquivo, int produtoId)
    {
        gerarFotoWebP(caminhoFoto, nomeArquivo, produtoId, 70);
    }

    private void gerarFotoWebP(string caminhoFoto, string nomeArquivo, int produtoId, int resolucao)
    {
        bool webpCarregado = true;
        try
        {
            Imazen.WebP.Extern.LoadLibrary.LoadWebPOrFail();
        }
        catch (Exception ex)
        {
            webpCarregado = false;
        }

        if (webpCarregado)
        {
            try
            {
                string nomeArquivoWebp = nomeArquivo.Split('.')[0] + ".webp";
                System.Drawing.Bitmap mBitmap;
                FileStream outStream = new FileStream(caminhoFoto + nomeArquivoWebp, FileMode.Create);
                using (Stream BitmapStream = System.IO.File.Open(caminhoFoto + nomeArquivo, System.IO.FileMode.Open))
                {
                    System.Drawing.Image img = System.Drawing.Image.FromStream(BitmapStream);

                    mBitmap = new System.Drawing.Bitmap(img);
                    var encoder = new Imazen.WebP.SimpleEncoder();
                    encoder.Encode(mBitmap, outStream, resolucao);
                    outStream.Close();
                    uploadImagemAmazonS3(nomeArquivoWebp, produtoId);
                }
            }
            catch (Exception ex) { }
        }
    }

    protected void btnExcluirProduto_OnClick(object sender, EventArgs e)
    {
        try
        {
            int produtoId = Convert.ToInt32(Request.QueryString["produtoId"]);

            if (rnPedidos.itensPedidoSeleciona_PorProdutoId(produtoId).Tables[0].Rows.Count > 0)
            {
                Response.Write("<script>alert('O produto: " + txtProdutoNome.Text + ", não pode ser excluido por que existe pedido relacionado a ele.');</script>");
            }
            else
            {
                rnProdutos.produtoExclui(produtoId);
                rnBuscaCloudSearch.ExcluirProduto(produtoId);
                if (Directory.Exists(ConfigurationManager.AppSettings["caminhoFisico"] + "fotos\\" + produtoId))
                {
                    Directory.Delete(ConfigurationManager.AppSettings["caminhoFisico"] + "fotos\\" + produtoId, true);
                }
            }

            Response.Write("<script>alert('Produto excluído com sucesso!');window.location=('produtos.aspx');</script>");

        }
        catch (Exception)
        {

            throw;
        }

    }

    private void CategoriasVinculasNomes()
    {
        try
        {
            lblCategoriasSelecionadas.Text = "";

            int produtoId = Convert.ToInt32(Request.QueryString["produtoId"]);
            using (var data = new dbCommerceDataContext())
            {
                var categoriasPrincipais = (from c in data.tbJuncaoProdutoCategorias
                                            where c.produtoId == produtoId && c.tbProdutoCategoria.categoriaPaiId == 0
                                            select new { c.tbProdutoCategoria.categoriaNomeExibicao }).Distinct().OrderBy(x => x.categoriaNomeExibicao).ToList();

                var filtros = (from c in data.tbJuncaoProdutoCategorias
                               where c.produtoId == produtoId && c.tbProdutoCategoria.categoriaPaiId != 0
                               select new
                               {
                                   c.tbProdutoCategoria.categoriaNomeExibicao,
                                   categoriaPaiNome = (from d in data.tbProdutoCategorias where d.categoriaId == c.tbProdutoCategoria.categoriaPaiId select new { d.categoriaNomeExibicao }).FirstOrDefault().categoriaNomeExibicao
                               }).Distinct().OrderBy(x => x.categoriaPaiNome).ToList();

                foreach (var cat in categoriasPrincipais)
                {
                    lblCategoriasSelecionadas.Text += cat.categoriaNomeExibicao + "</br>";
                }

                foreach (var cat in filtros)
                {
                    lblCategoriasSelecionadas.Text += cat.categoriaPaiNome + "/" + cat.categoriaNomeExibicao + "</br>";
                }
            }
        }
        catch (Exception)
        {

            throw;
        }




    }

    protected void txtIdProdutoPai_TextChanged(object sender, EventArgs e)
    {
        carregaProdutoPai();
    }

    private void carregaProdutoPai()
    {
        try
        {
            int idProdutoPai = 0;
            int.TryParse(lblIdProdutoPai.Text, out idProdutoPai);
            var data = new dbCommerceDataContext();
            var produtoPai = (from c in data.tbProdutos where c.produtoId == idProdutoPai select new { c.produtoNome, c.produtoId, c.fotoDestaque });
            lstProdutoPai.DataSource = produtoPai;
            lstProdutoPai.DataBind();

            if (idProdutoPai > 0 && produtoPai.Count() == 0)
            {
                AlertShow("Produto pai não localizado");
            }
            else if (idProdutoPai > 0)
            {
                lnkCadastrarProdutosFilho.Visible = true;
                var produtos = (from c in data.tbProdutoItemEspecificacaos
                                join d in data.tbProdutos on c.produtoId equals d.produtoId
                                join e in data.tbEspecificacaoItens on c.produtoItemEspecificacaoId equals e.especificacaoItensId
                                join f in data.tbEspecificacaos on e.especificacaoId equals f.especificacaoId
                                where c.produtoPaiId == idProdutoPai
                                select new
                                {
                                    c.produtoId,
                                    d.produtoNome,
                                    f.especificacaoNomeInterno,
                                    e.especificacaoItenNome,
                                    d.produtoAtivo,
                                    d.produtoPreco,
                                    d.produtoPrecoDeCusto,
                                    d.produtoPrecoPromocional,
                                    d.produtoFornecedor,
                                    d.produtoMarca
                                }).ToList();
                var especificacoes = produtos.Select(x => x.especificacaoNomeInterno).Distinct().OrderBy(x => x);

                var dataTable = new DataTable();
                foreach (var especificacao in especificacoes)
                {
                    dataTable.Columns.Add(new DataColumn(especificacao, typeof(string)));


                    var coluna = new GridViewDataTextColumn();
                    coluna.Caption = especificacao;
                    coluna.FieldName = especificacao;
                    coluna.VisibleIndex = 99;
                    grdProdutosRelacionados.Columns.Add(coluna);
                }
                dataTable.Columns.Add(new DataColumn("produtoId", typeof(int)));
                dataTable.Columns.Add(new DataColumn("produtoNome", typeof(string)));
                dataTable.Columns.Add(new DataColumn("produtoPreco", typeof(decimal)));
                dataTable.Columns.Add(new DataColumn("produtoPrecoDeCusto", typeof(decimal)));
                dataTable.Columns.Add(new DataColumn("produtoPrecoPromocional", typeof(decimal)));
                dataTable.Columns.Add(new DataColumn("produtoAtivo", typeof(string)));
                dataTable.Columns.Add(new DataColumn("produtoFornecedor", typeof(int)));
                dataTable.Columns.Add(new DataColumn("produtoMarca", typeof(int)));


                var produtosDistintos = (from c in produtos select new { c.produtoId, c.produtoNome, c.produtoPreco, c.produtoPrecoDeCusto, c.produtoPrecoPromocional, c.produtoAtivo, c.produtoFornecedor, c.produtoMarca }).Distinct();
                foreach (var produto in produtosDistintos)
                {
                    var linha = dataTable.NewRow();
                    for (int i = 0; i < especificacoes.Count(); i++)
                    {
                        var especificacao = especificacoes.ToList()[i];
                        var especificacaoDetalhe = produtos.Where(x => x.produtoId == produto.produtoId && x.especificacaoNomeInterno == especificacao).FirstOrDefault();
                        if (especificacaoDetalhe != null)
                        {
                            linha[i] = especificacaoDetalhe.especificacaoItenNome;
                        }
                    }
                    linha["produtoId"] = produto.produtoId;
                    linha["produtoNome"] = produto.produtoNome;
                    linha["produtoPreco"] = produto.produtoPreco;
                    linha["produtoPrecoDeCusto"] = produto.produtoPrecoDeCusto;
                    linha["produtoPrecoPromocional"] = produto.produtoPrecoPromocional;
                    linha["produtoAtivo"] = produto.produtoAtivo;
                    linha["produtoFornecedor"] = produto.produtoFornecedor;
                    linha["produtoMarca"] = produto.produtoMarca;
                    dataTable.Rows.Add(linha);
                }

                grdProdutosRelacionados.DataSource = dataTable;
                grdProdutosRelacionados.DataBind();

            }
        }
        catch (Exception ex) { }
    }


    public void AlertShow(string message)
    {
        base.ClientScript.RegisterStartupScript(base.GetType(), "myalert", "alert('" + message + "'); ", true);
    }


    protected void ddlEspecificacaoGrupo_SelectedIndexChanged(object sender, EventArgs e)
    {
        int especificacaoId = Convert.ToInt32(ddlEspecificacaoGrupo.SelectedValue);
        var data = new dbCommerceDataContext();
        var itens = (from c in data.tbEspecificacaoItens where c.especificacaoId == especificacaoId orderby c.especificacaoItenNome select c);
        ddlEspecificacao.DataSource = itens;
        ddlEspecificacao.DataBind();
    }

    protected void btnAlterarProdutoPai_Click(object sender, EventArgs e)
    {
        if (txtProdutoPai.Visible == false)
        {
            txtProdutoPai.Visible = true;
            txtProdutoPai.Text = lblIdProdutoPai.Text;
            lblIdProdutoPai.Visible = false;
            btnAlterarProdutoPai.Text = "Gravar";
        }
        else
        {
            txtProdutoPai.Visible = false;
            lblIdProdutoPai.Text = txtProdutoPai.Text;
            lblIdProdutoPai.Visible = true;
            btnAlterarProdutoPai.Text = "x Alterar Produto Pai";
            carregaProdutoPai();
        }
    }

    protected void lkbAdicionarEspecificacao_Click(object sender, EventArgs e)
    {
        popAdicionarEspecificacao.ShowOnPageLoad = true;
        var data = new dbCommerceDataContext();
        var itensNoGrupo = (from c in data.tbProdutos
                            where c.produtoPaiId == Convert.ToInt32(lblIdProdutoPai.Text)
                            select new
                            {
                                c.produtoId,
                                c.produtoNome
                            });
        if (itensNoGrupo.Count() > 1)
        {
            ddlEspecificacao.Visible = false;
        }
    }

    protected void lnkCadastrarProdutosFilho_Click(object sender, EventArgs e)
    {
        int idProdutoPai = 0;
        int.TryParse(lblIdProdutoPai.Text, out idProdutoPai);
        popAdicionarProduto.ShowOnPageLoad = true;
        var data = new dbCommerceDataContext();
        var especificacaoesDisponiveis = (from c in data.tbJuncaoProdutoEspecificacaos where c.produtoId == idProdutoPai orderby c.tbEspecificacao.especificacaoNomeInterno select c.tbEspecificacao).Distinct().ToList();
        lstEspecificacoesAdicionarProdutos.DataSource = especificacaoesDisponiveis;
        lstEspecificacoesAdicionarProdutos.DataBind();
    }

    protected void lstEspecificacoesAdicionarProdutos_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        CheckBoxList cklEspecificacaoItens = (CheckBoxList)e.Item.FindControl("cklEspecificacaoItens");
        HiddenField hdfEspecificacaoId = (HiddenField)e.Item.FindControl("hdfEspecificacaoId");

        cklEspecificacaoItens.Items.Add(new ListItem("Selecionar", "0"));

        int especificacaoId = Convert.ToInt32(hdfEspecificacaoId.Value);
        var data = new dbCommerceDataContext();
        var itensEspecificacao = (from c in data.tbEspecificacaoItens where c.especificacaoId == especificacaoId orderby c.especificacaoItenNome select c).ToList();
        cklEspecificacaoItens.DataSource = itensEspecificacao;
        cklEspecificacaoItens.DataBind();

    }

    protected void btnAdicionarVariacaoProdutos_Click(object sender, EventArgs e)
    {
        var data = new dbCommerceDataContext();
        int idProdutoPai = 0;
        int.TryParse(lblIdProdutoPai.Text, out idProdutoPai);

        Dictionary<int, List<string>> variacoes = new Dictionary<int, List<string>>();
        int indiceAtual = 1;

        bool todosGruposPossuemCadastro = true;
        foreach (var especificacoes in lstEspecificacoesAdicionarProdutos.Items)
        {
            var cklEspecificacaoItens = (CheckBoxList)especificacoes.FindControl("cklEspecificacaoItens");
            var itensSelecionados = new List<string>();

            foreach (ListItem checkItem in cklEspecificacaoItens.Items)
            {
                if (checkItem.Selected)
                {
                    itensSelecionados.Add(checkItem.Value.ToString());
                }
            }

            if (itensSelecionados.Any())
            {
                variacoes.Add(indiceAtual, itensSelecionados);
                indiceAtual++;
            }
            else
            {
                todosGruposPossuemCadastro = false;
            }
        }
        if (variacoes.Count == 0) todosGruposPossuemCadastro = false;

        if (!todosGruposPossuemCadastro)
        {
            AlertShow("Favor selecionar pelo menos uma especificação em cada grupo.");
            return;
        }
        var listaVariacoes = GetCombos(variacoes);
        var produtosFilhos = (from c in data.tbProdutos where c.produtoPaiId == idProdutoPai select c.produtoId).ToList();
        foreach (var itemVariacao in listaVariacoes)
        {
            List<int?> idsVariacao = new List<int?>();
            var listVariacaoStrings = itemVariacao.Split(';');
            foreach (var listVariacaoString in listVariacaoStrings)
            {
                if (listVariacaoString != "0")
                {
                    idsVariacao.Add(Convert.ToInt32(listVariacaoString));
                }
                else
                {
                    idsVariacao.Add(null);
                }

            }
            bool variacaoCadastrada = false;
            var itensEspecificacao = (from c in data.tbProdutoItemEspecificacaos
                                      where produtosFilhos.Contains(c.produtoId) && idsVariacao.Contains(c.produtoItemEspecificacaoId)
                                      group c by c.produtoId into grupos
                                      select new
                                      {
                                          produtoId = grupos.Key,
                                          quantidade = grupos.Select(x => x.produtoItemEspecificacaoId).Distinct().Count()
                                      });
            if (itensEspecificacao.Where(x => x.quantidade == idsVariacao.Count).Count() > 0)
            {
                variacaoCadastrada = true;
            }
            if (!variacaoCadastrada)
            {
                var produtoIdCadastrado = incluir(Convert.ToInt32(lblIdProdutoPai.Text), false);
                foreach (var idVariacao in idsVariacao)
                {
                    var dataCad = new dbCommerceDataContext();
                    var especificacaoItem = new tbProdutoItemEspecificacao();
                    especificacaoItem.dataDaCriacao = DateTime.Now;
                    especificacaoItem.especificacaoId = (from c in data.tbEspecificacaoItens where c.especificacaoItensId == idVariacao select c.especificacaoId).First();
                    especificacaoItem.produtoId = produtoIdCadastrado;
                    especificacaoItem.produtoPaiId = Convert.ToInt32(lblIdProdutoPai.Text);
                    especificacaoItem.produtoItemEspecificacaoId = idVariacao;
                    dataCad.tbProdutoItemEspecificacaos.InsertOnSubmit(especificacaoItem);
                    dataCad.SubmitChanges();


                    var juncao = new tbJuncaoProdutoEspecificacao();
                    juncao.dataDaCriacao = DateTime.Now;
                    juncao.especificacaoId = especificacaoItem.especificacaoId;
                    juncao.produtoId = produtoIdCadastrado;
                    dataCad.tbJuncaoProdutoEspecificacaos.InsertOnSubmit(juncao);
                    dataCad.SubmitChanges();
                }
            }
        }

        popAdicionarProduto.ShowOnPageLoad = false;
        carregaProdutoPai();
    }

    List<string> GetCombos(IEnumerable<KeyValuePair<int, List<string>>> remainingTags)
    {
        if (remainingTags.Count() == 1)
        {
            return remainingTags.First().Value;
        }
        else
        {
            var current = remainingTags.First();
            List<string> outputs = new List<string>();
            List<string> combos = GetCombos(remainingTags.Where(tag => tag.Key != current.Key));

            foreach (var tagPart in current.Value)
            {
                foreach (var combo in combos)
                {
                    outputs.Add(tagPart + ";" + combo);
                }
            }

            return outputs;
        }


    }

    protected void btnGravarEspecificacao_Click(object sender, EventArgs e)
    {
        if (ddlEspecificacaoGrupo.SelectedValue == "0")
        {
            AlertShow("Favor selecionar a especificação corretamente.");
            popAdicionarEspecificacao.ShowOnPageLoad = false;
            return;
        }

        int especificacaoId = Convert.ToInt32(ddlEspecificacaoGrupo.SelectedValue);
        int itemEspecificacaoId = Convert.ToInt32(ddlEspecificacao.SelectedValue);
        int produtoId = Convert.ToInt32(Request.QueryString["produtoId"]);
        var data = new dbCommerceDataContext();

        var itensNoGrupo = (from c in data.tbProdutos where c.produtoPaiId == Convert.ToInt32(lblIdProdutoPai.Text) select new {
            c.produtoId,
            c.produtoNome
        });
        if (itensNoGrupo.Count() > 1)
        {
            pnAdicionarEspecificacaoProdutos.Visible = true;
            pnAdicionarEspecificacao.Visible = false;
            lstAdicionaEspecificacaoProdutos.DataSource = itensNoGrupo;
            lstAdicionaEspecificacaoProdutos.DataBind();
            popAdicionarEspecificacao.Width = 900;
            popAdicionarEspecificacao.Height = 500;
        }
        else
        {
            if (ddlEspecificacao.SelectedValue == "0")
            {
                AlertShow("Favor selecionar a especificação corretamente.");
                popAdicionarEspecificacao.ShowOnPageLoad = false;
                return;
            }

            var especificacaoItem = new tbProdutoItemEspecificacao();
            especificacaoItem.dataDaCriacao = DateTime.Now;
            especificacaoItem.especificacaoId = especificacaoId;
            especificacaoItem.produtoId = produtoId;
            especificacaoItem.produtoPaiId = Convert.ToInt32(lblIdProdutoPai.Text);
            especificacaoItem.produtoItemEspecificacaoId = itemEspecificacaoId;
            data.tbProdutoItemEspecificacaos.InsertOnSubmit(especificacaoItem);
            data.SubmitChanges();

            var juncao = new tbJuncaoProdutoEspecificacao();
            juncao.dataDaCriacao = DateTime.Now;
            juncao.especificacaoId = especificacaoId;
            juncao.produtoId = produtoId;
            data.tbJuncaoProdutoEspecificacaos.InsertOnSubmit(juncao);
            data.SubmitChanges();
            popAdicionarEspecificacao.ShowOnPageLoad = false;

            dtlEspecificacoes.DataBind();
            carregaProdutoPai();
        }

    }
    protected void btnGravarEspecificacaoProdutos_Click(object sender, EventArgs e)
    {
        foreach (var item in lstAdicionaEspecificacaoProdutos.Items)
        {
            var ddlEspecificacaoItem = (DropDownList)item.FindControl("ddlEspecificacaoItem");
            if (ddlEspecificacaoItem.SelectedValue == "0")
            {
                //AlertShow("Valor Selecionar a especificacao corretamente");
                //return;
            }
        }
        foreach (var item in lstAdicionaEspecificacaoProdutos.Items)
        {
            var hdfProdutoid = (HiddenField)item.FindControl("hdfProdutoid");
            int produtoId = Convert.ToInt32(hdfProdutoid.Value);
            var lstEspecificacaoAtual = (ListView)item.FindControl("lstEspecificacaoAtual");
            var ddlEspecificacaoItem = (DropDownList)item.FindControl("ddlEspecificacaoItem");

            var data = new dbCommerceDataContext();
            int especificacaoId = Convert.ToInt32(ddlEspecificacaoGrupo.SelectedValue);
            int? itemEspecificacaoId = Convert.ToInt32(ddlEspecificacaoItem.SelectedValue);

            if (itemEspecificacaoId == 0) itemEspecificacaoId = null;

            var especificacaoCheck = (from c in data.tbProdutoItemEspecificacaos where c.produtoId == produtoId && c.especificacaoId == especificacaoId select c).FirstOrDefault();
            if (especificacaoCheck == null)
            {
                var especificacaoItem = new tbProdutoItemEspecificacao();
                especificacaoItem.dataDaCriacao = DateTime.Now;
                especificacaoItem.especificacaoId = especificacaoId;
                especificacaoItem.produtoId = produtoId;
                especificacaoItem.produtoPaiId = Convert.ToInt32(lblIdProdutoPai.Text);
                especificacaoItem.produtoItemEspecificacaoId = itemEspecificacaoId;
                data.tbProdutoItemEspecificacaos.InsertOnSubmit(especificacaoItem);
                data.SubmitChanges();
            }
            else
            {
                especificacaoCheck.produtoId = produtoId;
                especificacaoCheck.produtoPaiId = Convert.ToInt32(lblIdProdutoPai.Text);
                especificacaoCheck.produtoItemEspecificacaoId = itemEspecificacaoId;
                data.SubmitChanges();
            }

            var juncaoCheck = (from c in data.tbJuncaoProdutoEspecificacaos where c.produtoId == produtoId && c.especificacaoId == especificacaoId select c).FirstOrDefault();
            if (juncaoCheck == null)
            {
                var juncao = new tbJuncaoProdutoEspecificacao();
                juncao.dataDaCriacao = DateTime.Now;
                juncao.especificacaoId = especificacaoId;
                juncao.produtoId = produtoId;
                data.tbJuncaoProdutoEspecificacaos.InsertOnSubmit(juncao);
                data.SubmitChanges();
            }
            popAdicionarEspecificacao.ShowOnPageLoad = false;

            dtlEspecificacoes.DataBind();
        }

        pnAdicionarEspecificacaoProdutos.Visible = false;
        pnAdicionarEspecificacao.Visible = true;

        dtlEspecificacoes.DataBind();
        carregaProdutoPai();

    }

    protected void lstAdicionaEspecificacaoProdutos_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        var hdfProdutoid = (HiddenField)e.Item.FindControl("hdfProdutoid");
        int produtoId = Convert.ToInt32(hdfProdutoid.Value);
        var lstEspecificacaoAtual = (ListView)e.Item.FindControl("lstEspecificacaoAtual");
        var ddlEspecificacaoItem = (DropDownList)e.Item.FindControl("ddlEspecificacaoItem");

        int especificacaoId = Convert.ToInt32(ddlEspecificacaoGrupo.SelectedValue);
        var data = new dbCommerceDataContext();
        var itens = (from c in data.tbEspecificacaoItens where c.especificacaoId == especificacaoId orderby c.especificacaoItenNome select c);
        ddlEspecificacaoItem.DataSource = itens;
        ddlEspecificacaoItem.DataBind();

        var itensAtuais = (from c in data.tbProdutoItemEspecificacaos where c.produtoId == produtoId select new { c.tbEspecificacaoIten.especificacaoItenNome, c.tbEspecificacaoIten.tbEspecificacao.especificacaoNomeInterno });
        lstEspecificacaoAtual.DataSource = itensAtuais;
        lstEspecificacaoAtual.DataBind();

    }

    protected void btnExcluirEspecificao_Command(object sender, CommandEventArgs e)
    {
        int especificacaoId = Convert.ToInt32(e.CommandArgument);
        int produtoId = Convert.ToInt32(Request.QueryString["produtoId"]);
        var data = new dbCommerceDataContext();
        var itemEspecificacao = (from c in data.tbProdutoItemEspecificacaos where c.produtoId == produtoId && c.especificacaoId == especificacaoId select c).FirstOrDefault();
        if (itemEspecificacao != null)
        {
            data.tbProdutoItemEspecificacaos.DeleteOnSubmit(itemEspecificacao);
        }
        var especificacao = (from c in data.tbJuncaoProdutoEspecificacaos where c.produtoId == produtoId && c.especificacaoId == especificacaoId select c).FirstOrDefault();
        if (especificacao != null)
        {
            data.tbJuncaoProdutoEspecificacaos.DeleteOnSubmit(especificacao);
        }
        var queue = new tbQueue()
        {
            agendamento = DateTime.Now,
            andamento = false,
            concluido = false,
            idRelacionado = produtoId,
            mensagem = "1",
            tipoQueue = 15
        };
        data.tbQueues.InsertOnSubmit(queue);
        data.SubmitChanges();

        dtlEspecificacoes.DataBind();
        carregaProdutoPai();

    }

    protected void btnAdd360Position_Click(object sender, EventArgs e)
    {
        popPanorama360.ShowOnPageLoad = true;
    }

}
