﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adicionafilapagamento : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btn_onclick(object sender, EventArgs e)
    {
        var id = Convert.ToInt32(txtId.Text);
        var data = new dbCommerceDataContext();
        var queue = new tbQueue();
        queue.tipoQueue = 10;
        queue.agendamento = DateTime.Now;
        queue.idRelacionado = id;
        queue.mensagem = "";
        queue.concluido = false;
        queue.andamento = false;
        data.tbQueues.InsertOnSubmit(queue);
        data.SubmitChanges();

        txtId.Text = "";
    }
}