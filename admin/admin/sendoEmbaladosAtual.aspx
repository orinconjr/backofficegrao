﻿<%@ Page Title="" Language="C#" Theme="Glass" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="sendoEmbaladosAtual.aspx.cs" Inherits="admin_sendoEmbaladosAtual" %>

<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v11.1" Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Namespace="Controls" TagPrefix="uc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" runat="Server">
    <table cellpadding="0" cellspacing="0" style="width: 920px">
        <tr>
            <td class="tituloPaginas" valign="top">
                <asp:Label ID="lblAcao" runat="server">Pedidos em Separação e Embalagem</asp:Label>
            </td>
        </tr>
        <tr>
            <td>

                <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
                    <tr>
                        <td align="right">
                            <table cellpadding="0" cellspacing="0" style="width: 834px">
                                <tr>
                                    <td align="right">&nbsp;</td>
                                    <td height="38"
                                        style="background-image: url('images/bkExportar.jpg'); padding-bottom: 1px;"
                                        valign="bottom" width="231">
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 97px">&nbsp;</td>
                                                <td style="width: 33px">
                                                    <asp:ImageButton ID="btPdf" runat="server" ImageUrl="~/admin/images/btPdf.jpg"
                                                        Style="margin-left: 5px" OnClick="btPdf_Click" />
                                                </td>
                                                <td style="width: 31px">
                                                    <asp:ImageButton ID="btXsl" runat="server" ImageUrl="~/admin/images/btXls.jpg"
                                                        Style="margin-left: 5px" OnClick="btXsl_Click" />
                                                </td>
                                                <td style="width: 36px">
                                                    <asp:ImageButton ID="btRtf" runat="server" ImageUrl="~/admin/images/btRtf.jpg"
                                                        Style="margin-left: 5px" OnClick="btRtf_Click" />
                                                </td>
                                                <td valign="bottom">
                                                    <asp:ImageButton ID="btCsv" runat="server" ImageUrl="~/admin/images/btCsv.jpg"
                                                        Style="margin-left: 5px" OnClick="btCsv_Click" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="rotulos" style="padding-bottom: 20px;">
                            Centro de Distribuição: 
                            <asp:RadioButton runat="server" ID="rdbCd1" Checked="true" Text="CD 1" GroupName="groupdCds" AutoPostBack="true"  OnCheckedChanged="rdbCd1_CheckedChanged"/>
                            <asp:RadioButton runat="server" ID="rdbCd2" Text="CD 2" GroupName="groupdCds" AutoPostBack="true"  OnCheckedChanged="rdbCd2_CheckedChanged" />
                            <asp:RadioButton runat="server" ID="rdbCd3" Text="CD 3" GroupName="groupdCds" AutoPostBack="true"  OnCheckedChanged="rdbCd3_CheckedChanged" />
                            <asp:RadioButton runat="server" ID="rdbCd4" Text="CD 4" GroupName="groupdCds" AutoPostBack="true"  OnCheckedChanged="rdbCd4_CheckedChanged" />
                            <asp:RadioButton runat="server" ID="rdbCd5" Text="CD 5" GroupName="groupdCds" AutoPostBack="true"  OnCheckedChanged="rdbCd4_CheckedChanged" />
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td class="rotulos">
                            <b>Pedidos sendo separados</b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <dxwgv:aspxgridview id="grdSendoSeparados" runat="server" autogeneratecolumns="False" keyfieldname="pedidoId" width="834px" enablecallbacks="False"
                                cursor="auto" oncustomunboundcolumndata="grdSendoSeparados_OnCustomUnboundColumnData">
                    <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                    <Styles>
                        <Footer Font-Bold="True">
                        </Footer>
                    </Styles>
                    <SettingsText ConfirmDelete="Tem certeza que deseja excluir?" 
                        emptydatarow="Nenhum registro encontrado." />
                    <SettingsPager Position="TopAndBottom" PageSize="50" 
                        ShowDisabledButtons="False" AlwaysShowPager="True">
                        <Summary AllPagesText="Páginas: {0} - {1} ({2} items)" 
                            Text="Página {0} de {1} ({2} registros encontrados)" />
                    </SettingsPager>
                    <settings showfilterrow="True" ShowGroupButtons="False" 
                        ShowHeaderFilterButton="True" ShowFooter="True" />
                    <TotalSummary>
                        <dxwgv:ASPxSummaryItem DisplayFormat="c" FieldName="valorCobrado" 
                            ShowInColumn="Valor" ShowInGroupFooterColumn="Valor" SummaryType="Sum" />
                    </TotalSummary>
                    <SettingsEditing EditFormColumnCount="4" 
                        PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1" 
                        PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter" 
                        PopupEditFormWidth="700px" />
                    <Columns>
                        <dxwgv:GridViewDataTextColumn Caption="Pedido" FieldName="pedidoIdCliente" VisibleIndex="0" UnboundType="Integer">
                            <EditFormSettings Visible="False" />
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Id do Pedido" FieldName="pedidoId" VisibleIndex="0">
                            <EditFormSettings Visible="False" />
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Usuário Separacao" FieldName="nome" VisibleIndex="0">
                            <EditFormSettings Visible="False" />
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataDateColumn Caption="Inicio Separacao" FieldName="dataInicioSeparacao" UnboundType="DateTime" VisibleIndex="3" Width="120px">
                            <PropertiesDateEdit DisplayFormatString="" EditFormat="DateTime">
                                <CalendarProperties ClearButtonText="Apagar" TodayButtonText="Hoje">
                                </CalendarProperties>
                            </PropertiesDateEdit>
                            <Settings GroupInterval="Date" />
                        </dxwgv:GridViewDataDateColumn>
                        <dxwgv:GridViewDataTextColumn VisibleIndex="8" Width="80">
                            <DataItemTemplate>
                                <asp:Button runat="server" ID="btnCancelar" Text="Cancelar Separacao" OnCommand="btnCancelar_OnCommand" CommandArgument='<%# Eval("pedidoId") %>' />
                            </DataItemTemplate>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataHyperLinkColumn Caption="Editar" FieldName="pedidoId"
                            VisibleIndex="8" Width="30px">
                            <PropertiesHyperLinkEdit ImageUrl="~/admin/images/btEditar.jpg"
                                NavigateUrlFormatString="pedido.aspx?pedidoId={0}" Target="_blank">
                            </PropertiesHyperLinkEdit>
                            <Settings AllowAutoFilter="False" />
                            <HeaderTemplate>
                                <img alt="" src="images/legendaEditar.jpg" />
                            </HeaderTemplate>
                        </dxwgv:GridViewDataHyperLinkColumn>
                    </Columns>
                    <StylesEditors>
                        <Label Font-Bold="True">
                        </Label>
                    </StylesEditors>
                </dxwgv:aspxgridview>
                            <dxwgv:aspxgridviewexporter id="grdEx" runat="server" filename="fornecedores"
                                gridviewid="grdSendoSeparados" landscape="True" preservegrouprowstates="True">
                </dxwgv:aspxgridviewexporter>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td class="rotulos">
                            <b>Pedidos sendo Embalados</b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <dxwgv:aspxgridview id="grdSendoEmbalados" runat="server" autogeneratecolumns="False" keyfieldname="pedidoId" width="834px" enablecallbacks="False"
                                cursor="auto" oncustomunboundcolumndata="grdSendoEmbalados_OnCustomUnboundColumnData">
                    <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                    <Styles>
                        <Footer Font-Bold="True">
                        </Footer>
                    </Styles>
                    <SettingsText ConfirmDelete="Tem certeza que deseja excluir?" 
                        emptydatarow="Nenhum registro encontrado." />
                    <SettingsPager Position="TopAndBottom" PageSize="50" 
                        ShowDisabledButtons="False" AlwaysShowPager="True">
                        <Summary AllPagesText="Páginas: {0} - {1} ({2} items)" 
                            Text="Página {0} de {1} ({2} registros encontrados)" />
                    </SettingsPager>
                    <settings showfilterrow="True" ShowGroupButtons="False" 
                        ShowHeaderFilterButton="True" ShowFooter="True" />
                    <TotalSummary>
                        <dxwgv:ASPxSummaryItem DisplayFormat="c" FieldName="valorCobrado" 
                            ShowInColumn="Valor" ShowInGroupFooterColumn="Valor" SummaryType="Sum" />
                    </TotalSummary>
                    <SettingsEditing EditFormColumnCount="4" 
                        PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1" 
                        PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter" 
                        PopupEditFormWidth="700px" />
                    <Columns>
                        <dxwgv:GridViewDataTextColumn Caption="Pedido" FieldName="pedidoIdCliente" VisibleIndex="0" UnboundType="Integer">
                            <EditFormSettings Visible="False" />
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Id do Pedido" FieldName="pedidoId" VisibleIndex="0">
                            <EditFormSettings Visible="False" />
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Usuário Embalagem" FieldName="nome" VisibleIndex="0">
                            <EditFormSettings Visible="False" />
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataDateColumn Caption="Inicio Embalagem" FieldName="dataInicioEmbalagem" UnboundType="DateTime" VisibleIndex="3" Width="120px">
                            <PropertiesDateEdit DisplayFormatString="" EditFormat="DateTime">
                                <CalendarProperties ClearButtonText="Apagar" TodayButtonText="Hoje">
                                </CalendarProperties>
                            </PropertiesDateEdit>
                            <Settings GroupInterval="Date" />
                        </dxwgv:GridViewDataDateColumn>
                        <dxwgv:GridViewDataTextColumn VisibleIndex="8" Width="80">
                            <DataItemTemplate>
                                <asp:Button runat="server" ID="btnCancelar" Text="Cancelar Embalagem" OnCommand="btnCancelar_OnCommand" CommandArgument='<%# Eval("pedidoId") %>' />
                            </DataItemTemplate>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataHyperLinkColumn Caption="Editar" FieldName="pedidoId"
                            VisibleIndex="8" Width="30px">
                            <PropertiesHyperLinkEdit ImageUrl="~/admin/images/btEditar.jpg"
                                NavigateUrlFormatString="pedido.aspx?pedidoId={0}" Target="_blank">
                            </PropertiesHyperLinkEdit>
                            <Settings AllowAutoFilter="False" />
                            <HeaderTemplate>
                                <img alt="" src="images/legendaEditar.jpg" />
                            </HeaderTemplate>
                        </dxwgv:GridViewDataHyperLinkColumn>
                    </Columns>
                    <StylesEditors>
                        <Label Font-Bold="True">
                        </Label>
                    </StylesEditors>
                </dxwgv:aspxgridview>
                            <dxwgv:aspxgridviewexporter id="ASPxGridViewExporter1" runat="server" filename="fornecedores"
                                gridviewid="grdSendoSeparados" landscape="True" preservegrouprowstates="True">
                </dxwgv:aspxgridviewexporter>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;
                        </td>
                    </tr>
                    <asp:Panel runat="server" ID="pnlGridRastreioDuplicado" Visible="false">
                        <tr>
                            <td class="rotulos">
                                <b>Pedidos Embalados - Qtd Volume Informado Errado</b>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox runat="server" ID="txtPedidoVolumeDuplicado"></asp:TextBox>
                                <asp:Button runat="server" ID="btnPesquisarVolumeDuplicado" Text="pesquisar" OnClick="btnPesquisarVolumeDuplicado_Click" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <dxwgv:aspxgridview id="grdVolumeErrado" runat="server" autogeneratecolumns="False" keyfieldname="idPedido" width="834px" enablecallbacks="False"
                                    cursor="auto" oncustomunboundcolumndata="grdVolumeErrado_OnCustomUnboundColumnData">
                    <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                    <Styles>
                        <Footer Font-Bold="True">
                        </Footer>
                    </Styles>
                    <SettingsText ConfirmDelete="Tem certeza que deseja excluir?" 
                        emptydatarow="Nenhum registro encontrado." />
                    <SettingsPager Position="TopAndBottom" PageSize="50" 
                        ShowDisabledButtons="False" AlwaysShowPager="True">
                        <Summary AllPagesText="Páginas: {0} - {1} ({2} items)" 
                            Text="Página {0} de {1} ({2} registros encontrados)" />
                    </SettingsPager>
                    <settings showfilterrow="True" ShowGroupButtons="False" 
                        ShowHeaderFilterButton="True" ShowFooter="True" />
                    <TotalSummary>
                        <dxwgv:ASPxSummaryItem DisplayFormat="c" FieldName="valorCobrado" 
                            ShowInColumn="Valor" ShowInGroupFooterColumn="Valor" SummaryType="Sum" />
                    </TotalSummary>
                    <SettingsEditing EditFormColumnCount="4" 
                        PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1" 
                        PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter" 
                        PopupEditFormWidth="700px" />
                    <Columns>
                        <dxwgv:GridViewDataTextColumn Caption="Pedido" FieldName="pedidoIdCliente" VisibleIndex="0" UnboundType="Integer">
                            <EditFormSettings Visible="False" />
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Id do Pedido" FieldName="idPedido" VisibleIndex="0">
                            <EditFormSettings Visible="False" />
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Volume" FieldName="numeroVolume" VisibleIndex="0">
                            <EditFormSettings Visible="False" />
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Peso" FieldName="peso" VisibleIndex="0">
                            <EditFormSettings Visible="False" />
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Envio" FieldName="formaDeEnvio" VisibleIndex="0">
                            <EditFormSettings Visible="False" />
                        </dxwgv:GridViewDataTextColumn>
                        <%--<dxwgv:GridViewDataDateColumn Caption="Inicio Embalagem" FieldName="dataSendoEmbalado" UnboundType="DateTime" VisibleIndex="3" Width="120px">
                            <PropertiesDateEdit DisplayFormatString="" EditFormat="DateTime">
                                <CalendarProperties ClearButtonText="Apagar" TodayButtonText="Hoje">
                                </CalendarProperties>
                            </PropertiesDateEdit>
                            <Settings GroupInterval="Date" />
                        </dxwgv:GridViewDataDateColumn>--%>
                        <dxwgv:GridViewDataTextColumn VisibleIndex="8" Width="80">
                            <DataItemTemplate>
                                <asp:Button runat="server" ID="btnExcluirRastreio" Text="Excluir Rastreio" OnCommand="btnExcluirRastreio_OnCommand" OnClientClick="return confirm('Confirma exclusão do rastreio?')" CommandArgument='<%# Eval("idPedidoPacote") %>' />
                            </DataItemTemplate>
                        </dxwgv:GridViewDataTextColumn>
                    </Columns>
                    <StylesEditors>
                        <Label Font-Bold="True">
                        </Label>
                    </StylesEditors>
                </dxwgv:aspxgridview>
                            </td>
                        </tr>
                    </asp:Panel>

                    <asp:Panel runat="server" ID="pnlGridVolumesIncorretos" Visible="false">
                        <tr>
                            <td class="rotulos">
                                <b>Pedidos Embalados - Qtd Volume Informado Errado (Alterar)</b>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox runat="server" ID="txtIdPedidoPesquisa" MaxLength="10"></asp:TextBox>
                                <asp:Label runat="server" ID="lblQtdVolumesPedidoPesquisa"></asp:Label>
                                <asp:Button runat="server" ID="btnPesquisarQtdVolumesInformadoErrado" Text="pesquisar" OnClick="btnPesquisarQtdVolumesInformadoErrado_OnClick" />
                                <br/>
                                <asp:TextBox runat="server" ID="txtNovaQtdVolumes" MaxLength="2" Visible="False"></asp:TextBox>
                                <asp:Button runat="server" ID="btnAlterarQtdVolumes" Text="Acrestar Volume" OnClick="btnAlterarQtdVolumes_OnClick" OnClientClick="return confirm('Confirma o acrescimo na quantidade de volumes do pedido?')" Visible="False"/>
                            </td>
                        </tr>
                    </asp:Panel>
                    <tr>
                        <td>&nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <dx:aspxpopupcontrol id="pcCancelarEmbalagem" runat="server" closeaction="CloseButton" modal="True"
        popuphorizontalalign="WindowCenter" popupverticalalign="TopSides" clientinstancename="pcCancelarEmbalagem"
        headertext="Cancelar Embalagem" allowdragging="True" popupanimationtype="None" enableviewstate="True">
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server">
                <asp:HiddenField runat="server" ID="hdfPedidoId" />
                <div style="width: 1000px; height: 500px; overflow: scroll;">
                    <div>
                        <table style="width: 100%">
                            <tr class="rotulos">
                                <td colspan="3">
                                    Motivo para voltar o pedido para separação:
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <asp:TextBox CssClass="campos" runat="server" ID="txtMotivo" Width="90%"></asp:TextBox>
                                </td>
                            </tr>
                            <tr class="rotulos">
                                <td colspan="3">
                                    Priorizar:
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <asp:TextBox CssClass="campos" runat="server" ID="txtPriorizar" Width="50px"></asp:TextBox>
                                </td>
                            </tr>
                            <asp:Panel runat="server" ID="pnProdutosCancelar">
                                <tr class="rotulos">
                                    <td style="width: 90%; font-weight: bold; padding-top: 30px;"  colspan="2">
                                        Produto
                                    </td>
                                    <td style="padding-left: 20px; font-weight: bold;">
                                        Faltando
                                    </td>
                                </tr>
                                <asp:ListView runat="server" ID="lstItensPedido" OnItemDataBound="lstItensPedido_ItemDataBound">
                                    <ItemTemplate>
                                        <div style="font-weight: bold;">
                                            <asp:HiddenField runat="server" ID="hdfProdutoId" Value='<%# Eval("produtoId") %>'/>
                                            <asp:HiddenField runat="server" ID="hdfItemPedidoId" Value='<%# Eval("itemPedidoId") %>'/>
                                            <asp:HiddenField runat="server" ID="hdfItemPedidoEstoque" Value='<%# Eval("idItemPedidoEstoque") %>'/>
                                                <tr>
                                                    <td colspan="2">
                                                        <%# Eval("produtoNome") %>
                                                    </td>
                                                    <td style="padding-left: 20px;">
                                                        <asp:CheckBox runat="server" ID="chkCancelar" />
                                                    </td>
                                                </tr>                                    
                                        </div>
                                        <tr>
                                            <td colspan="3">
                                                <hr color="#7EACB1" size="1" />
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:ListView>
                            </asp:Panel>
                            <asp:Panel runat="server" ID="pnEtiquetasCancelar" Visible="false">
                                <tr class="rotulos">
                                    <td style="width: 90%; font-weight: bold; padding-top: 30px;">
                                        Etiqueta
                                    </td>
                                    <td style="width: 90%; font-weight: bold; padding-top: 30px;">
                                        Produto
                                    </td>
                                    <td style="padding-left: 20px; font-weight: bold;">
                                        Faltando
                                    </td>
                                </tr>
                                <asp:ListView runat="server" ID="lstEtiquetasCancelar">
                                    <ItemTemplate>
                                        <div style="font-weight: bold;">
                                            <asp:HiddenField runat="server" ID="hdfIdPedidoFornecedorItem" Value='<%# Eval("idPedidoFornecedorItem") %>'/>
                                                <tr>
                                                    <td>
                                                        <%# Eval("idPedidoFornecedorItem") %>
                                                    </td>
                                                    <td>
                                                        <%# Eval("produtoNome") %>
                                                    </td>
                                                    <td style="padding-left: 20px;">
                                                        <asp:CheckBox runat="server" ID="chkCancelar" />
                                                    </td>
                                                </tr>                                    
                                        </div>
                                        <tr>
                                            <td colspan="3">
                                                <hr color="#7EACB1" size="1" />
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:ListView>
                            </asp:Panel>
                            <tr class="rotulos">
                                <td style="width: 80%; font-weight: bold;">
                                    &nbsp;
                                </td>
                                <td style="padding-left: 20px; font-weight: bold;">
                                    <uc:OneClickButton ID="btnCancelarEmbalagem" runat="server" Text="Cancelar Embalagem" ReplaceTitleTo="Aguarde..." onclick="btnCancelarEmbalagem_OnClick" />
                                </td>
                            </tr>
                            </table>
                    </div>
                </div>
            </dx:PopupControlContentControl>
        </ContentCollection>
        <ContentStyle>
            <Paddings PaddingBottom="5px" />
        </ContentStyle>
    </dx:aspxpopupcontrol>
</asp:Content>
