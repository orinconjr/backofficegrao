﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="alterarValorItemPedido.aspx.cs" Inherits="admin_alterarValorItemPedido" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" runat="Server">
    <style>
        .tabrotulos {
            width: 848px;
            margin: auto;
        }

        .rotulos td {
            font: bold 12px tahoma;
            padding-top: 20px;
            color: rgb(65, 105, 126);
            padding-left: 5px;
            padding-bottom: 5px;
        }

        .meubotao {
            cursor: pointer;
            font: bold 14px tahoma;
        }

        .fieldsetatualizarprecos {
            width: 760px;
            float: left;
            clear: left;
            border-radius: 5px;
            margin-left: 25px;
        }

            .fieldsetatualizarprecos input[type=checkbox] {
                margin-top: 15px;
            }

            .fieldsetatualizarprecos label {
                margin-left: 5px;
                margin-right: 20px;
            }

        .meugrid {
            background-color: #F4FAFB;
            border: 0px none;
            /* border-collapse: separate !important; */
            overflow: hidden;
            font: 9pt Tahoma;
            color: #000;
            cursor: pointer;
            /* white-space: nowrap;*/
            font-weight: normal;
            text-align: left;
            width: 834px;
            border-collapse: collapse;
            float: left;
            clear: left;
            margin-top: 10px;
            margin-bottom: 10px;
            min-height: 500px;
            margin-left: 25px;
        }

            .meugrid th {
                background-image: url(../../App_Themes/Glass/GridView/gvHeaderBackground.gif);
                background-position: center top;
                background-repeat: repeat-x;
                background-color: #C0DDE0;
                overflow: hidden;
                height: 23px;
                line-height: 23px;
                padding: 4px 6px 5px;
                font-weight: normal;
                border: 1px solid #7EACB1;
                /* border-collapse: separate;*/
            }

            .meugrid td {
                overflow: hidden;
                height: 23px;
                line-height: 23px;
                padding: 4px 6px 5px;
                border: 1px solid #7EACB1;
            }

            .meugrid tr td:nth-child(4) {
                text-align: center;
            }

            .meugrid tr td:last-of-type {
                text-align: center;
            }
    </style>

    <div class="tituloPaginas" valign="top">
        Alterar Valor de Item de Pedido
    </div>
    <table class="tabrotulos">
        <tr class="rotulos">


            <td>Id do Pedido:<br />
                <asp:TextBox runat="server" ID="txtPedidoId"></asp:TextBox>
            </td>

        </tr>
        <tr class="rotulos">

            <td>Id do Item:<br />
                <asp:TextBox runat="server" ID="txtitemPedidoId"></asp:TextBox>
            </td>

        </tr>

        <tr class="rotulos">

            <td>Valor:<br />
                <asp:TextBox runat="server" ID="txtItemValor"></asp:TextBox>
            </td>

        </tr>
        <tr class="rotulos">

            <td>
                <br />
                <asp:Button runat="server" ID="btnSalvar" CssClass="meubotao" Text="Salvar" OnClick="btnSalvar_Click" />
            </td>

        </tr>




    </table>


</asp:Content>

