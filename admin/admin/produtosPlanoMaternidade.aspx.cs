﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Data.Filtering;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;

public partial class admin_produtosPlanoMaternidade : System.Web.UI.Page
{
    DateTemplate dateTemplate = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        fillGrid(false);
    }

    private class pedidosLista
    {
        public int pedidoId { get; set; }
        public int produtoId { get; set; }
        public int itemPedidoId { get; set; }
        public string produtoNome { get; set; }
        public DateTime dataDoPedido { get; set; }
        public DateTime vencimentoBoleto { get; set; }
        public int prazoFornecedor { get; set; }
        public string fornecedor { get; set; }
        public DateTime dataPedir { get; set; }
        public bool pedidoFornecedor { get; set; }
    }
    private void fillGrid(bool rebind)
    {
        var pedidosLista = new List<pedidosLista>();
        var data = new dbCommerceDataContext();
        var itensPedidoPlano = (from c in data.tbItensPedidos
            where c.tbPedido.tipoDePagamentoId == 11
            join d in data.tbPedidoPagamentos on c.pedidoId equals d.pedidoId into pagamentos
            join e in data.tbItensPedidoCombos on c.itemPedidoId equals e.idItemPedido into produtosCombo
            where pagamentos.Where(x => x.pago).Any() && produtosCombo.Count() == 0
            select new
            {
                c.pedidoId,
                c.itemPedidoId,
                c.tbProduto.produtoNome,
                c.tbPedido.dataHoraDoPedido,
                c.tbProduto.tbProdutoFornecedor.fornecedorNome,
                c.tbProduto.tbProdutoFornecedor.fornecedorPrazoPedidos,
                c.tbProduto.tbProdutoFornecedor.fornecedorPrazoDeFabricacao,
                c.tbPedido.statusDoPedido,
                c.tbProduto.tbProdutoFornecedor.pedidoDomingo,
                c.tbProduto.tbProdutoFornecedor.pedidoSegunda,
                c.tbProduto.tbProdutoFornecedor.pedidoTerca,
                c.tbProduto.tbProdutoFornecedor.pedidoQuarta,
                c.tbProduto.tbProdutoFornecedor.pedidoQuinta,
                c.tbProduto.tbProdutoFornecedor.pedidoSexta,
                c.tbProduto.tbProdutoFornecedor.pedidoSabado,
                c.produtoId
            });
        var itensPedidoPlanoCombo = (from c in data.tbItensPedidoCombos
            where c.tbItensPedido.tbPedido.tipoDePagamentoId == 11
            join d in data.tbPedidoPagamentos on c.tbItensPedido.pedidoId equals d.pedidoId into pagamentos
            where pagamentos.Where(x => x.pago).Any()
            select new
            {
                c.tbItensPedido.pedidoId,
                c.tbItensPedido.itemPedidoId,
                c.tbProduto.produtoNome,
                c.tbItensPedido.tbPedido.dataHoraDoPedido,
                c.tbProduto.tbProdutoFornecedor.fornecedorNome,
                c.tbProduto.tbProdutoFornecedor.fornecedorPrazoPedidos,
                c.tbProduto.tbProdutoFornecedor.fornecedorPrazoDeFabricacao,
                c.tbItensPedido.tbPedido.statusDoPedido,
                c.tbProduto.tbProdutoFornecedor.pedidoDomingo,
                c.tbProduto.tbProdutoFornecedor.pedidoSegunda,
                c.tbProduto.tbProdutoFornecedor.pedidoTerca,
                c.tbProduto.tbProdutoFornecedor.pedidoQuarta,
                c.tbProduto.tbProdutoFornecedor.pedidoQuinta,
                c.tbProduto.tbProdutoFornecedor.pedidoSexta,
                c.tbProduto.tbProdutoFornecedor.pedidoSabado,
                c.produtoId
            });
        var itensCompleto = itensPedidoPlano.Union(itensPedidoPlanoCombo);
        foreach (var itensPedido in itensCompleto)
        {
            var boletos = (from c in data.tbPedidoPagamentos
                where c.pedidoId == itensPedido.pedidoId && c.cancelado == false
                orderby c.dataVencimento descending
                select c).ToList();
            var ultimoBoleto = (from c in boletos
                where c.pedidoId == itensPedido.pedidoId && c.cancelado == false
                orderby c.dataVencimento descending
                select c).FirstOrDefault();
            
            var pedidoLista = new pedidosLista();
            pedidoLista.pedidoId = itensPedido.pedidoId;
            pedidoLista.produtoId = itensPedido.produtoId;
            pedidoLista.itemPedidoId = itensPedido.itemPedidoId;
            pedidoLista.produtoNome = itensPedido.produtoNome;
            pedidoLista.dataDoPedido = Convert.ToDateTime(itensPedido.dataHoraDoPedido);
            pedidoLista.vencimentoBoleto = ultimoBoleto.dataVencimento;
            pedidoLista.fornecedor = itensPedido.fornecedorNome;
            int prazoFornecedor = itensPedido.fornecedorPrazoPedidos == null ? itensPedido.fornecedorPrazoDeFabricacao == null ? 1 : (int)itensPedido.fornecedorPrazoDeFabricacao : (int)itensPedido.fornecedorPrazoPedidos;
            pedidoLista.prazoFornecedor = prazoFornecedor;
            if (itensPedido.statusDoPedido == 3)
            {
                pedidoLista.dataPedir =
                    Convert.ToDateTime(
                        boletos.Where(x => x.pago).OrderByDescending(x => x.dataPagamento).First().dataPagamento);
            }
            else
            {
                var dataPedidoFonecedor = ultimoBoleto.dataVencimento.AddDays(prazoFornecedor*(-1));
                bool pedirNoDia = false;
                while (pedirNoDia == false)
                {
                    var diaDaSemana = (int) dataPedidoFonecedor.DayOfWeek;
                    if (diaDaSemana == 0 && itensPedido.pedidoDomingo == false) dataPedidoFonecedor = dataPedidoFonecedor.AddDays(-1);
                    if (diaDaSemana == 1 && itensPedido.pedidoSegunda == false) dataPedidoFonecedor = dataPedidoFonecedor.AddDays(-1);
                    if (diaDaSemana == 2 && itensPedido.pedidoTerca == false) dataPedidoFonecedor = dataPedidoFonecedor.AddDays(-1);
                    if (diaDaSemana == 3 && itensPedido.pedidoQuarta == false) dataPedidoFonecedor = dataPedidoFonecedor.AddDays(-1);
                    if (diaDaSemana == 4 && itensPedido.pedidoQuinta == false) dataPedidoFonecedor = dataPedidoFonecedor.AddDays(-1);
                    if (diaDaSemana == 5 && itensPedido.pedidoSexta == false) dataPedidoFonecedor = dataPedidoFonecedor.AddDays(-1);
                    if (diaDaSemana == 6 && itensPedido.pedidoSabado == false) dataPedidoFonecedor = dataPedidoFonecedor.AddDays(-1);
                    else pedirNoDia = true;
                }
                pedidoLista.dataPedir = dataPedidoFonecedor;
            }
            var pedidosFornecedor = (from c in data.tbPedidoFornecedorItems
                where
                    c.idPedido == itensPedido.pedidoId && c.idProduto == itensPedido.produtoId &&
                    c.idItemPedido == itensPedido.itemPedidoId
                select c).Any();
            var reservas = (from c in data.tbProdutoReservaEstoques
                where
                    c.idProduto == itensPedido.produtoId && c.idItemPedido == itensPedido.itemPedidoId &&
                    c.idPedido == itensPedido.pedidoId
                select c).Any();
            pedidoLista.pedidoFornecedor = pedidosFornecedor | reservas;
            pedidosLista.Add(pedidoLista);
        }

        grd.DataSource = pedidosLista;
        if ((!Page.IsPostBack && !Page.IsCallback) | rebind)
        {
            grd.DataBind();
        }

    }
    protected void btPdf_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WritePdfToResponse();
    }
    protected void btXsl_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteXlsToResponse();
    }
    protected void btRtf_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteRtfToResponse();
    }
    protected void btCsv_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteCsvToResponse();
    }

    protected void grid_AutoFilterCellEditorCreate(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewEditorCreateEventArgs e)
    {
        ASPxGridView grid = sender as ASPxGridView;
        if (e.Column.FieldName == "dataDoPedido")
        {
            DropDownEditProperties dde = new DropDownEditProperties();
            dde.EnableClientSideAPI = true;
            dateTemplate = new DateTemplate();
            dde.DropDownWindowTemplate = dateTemplate;
            e.EditorProperties = dde;
        }
        if (e.Column.FieldName == "vencimentoBoleto")
        {
            DropDownEditProperties dde = new DropDownEditProperties();
            dde.EnableClientSideAPI = true;
            dateTemplate = new DateTemplate();
            dde.DropDownWindowTemplate = dateTemplate;
            e.EditorProperties = dde;
        }
        if (e.Column.FieldName == "dataPedir")
        {
            DropDownEditProperties dde = new DropDownEditProperties();
            dde.EnableClientSideAPI = true;
            dateTemplate = new DateTemplate();
            dde.DropDownWindowTemplate = dateTemplate;
            e.EditorProperties = dde;
        }
    }
    protected void grid_AutoFilterCellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
    {
        ASPxGridView grid = sender as ASPxGridView;
        if (e.Column.FieldName == "dataDoPedido")
        {
            ASPxDropDownEdit dde = e.Editor as ASPxDropDownEdit;
            dde.ClientSideEvents.CloseUp = String.Format("function (s, e) {{ ApplyFilter(s, {0}, {1}, 'dataDoPedido'); }}", dateTemplate.cIdFrom, dateTemplate.cIdTo);
            dde.ReadOnly = true;
        }
        if (e.Column.FieldName == "vencimentoBoleto")
        {
            ASPxDropDownEdit dde = e.Editor as ASPxDropDownEdit;
            dde.ClientSideEvents.CloseUp = String.Format("function (s, e) {{ ApplyFilter(s, {0}, {1}, 'vencimentoBoleto'); }}", dateTemplate.cIdFrom, dateTemplate.cIdTo);
            dde.ReadOnly = true;
        }
        if (e.Column.FieldName == "dataPedir")
        {
            ASPxDropDownEdit dde = e.Editor as ASPxDropDownEdit;
            dde.ClientSideEvents.CloseUp = String.Format("function (s, e) {{ ApplyFilter(s, {0}, {1}, 'dataPedir'); }}", dateTemplate.cIdFrom, dateTemplate.cIdTo);
            dde.ReadOnly = true;
        }
    }

    protected void grid_ProcessColumnAutoFilter(object sender, ASPxGridViewAutoFilterEventArgs e)
    {
        if (e.Value == "|") return;
        if (e.Column.FieldName == "dataDoPedido")
        {
            if (e.Kind == GridViewAutoFilterEventKind.CreateCriteria)
            {
                Session["dataDoPedido"] = e.Value;
                String[] dates = e.Value.Split('|');
                DateTime dateFrom = Convert.ToDateTime(dates[0]),
                    dateTo = Convert.ToDateTime(dates[1]);
                e.Criteria = (new OperandProperty("dataDoPedido") >= dateFrom) &
                             (new OperandProperty("dataDoPedido") <= dateTo);
            }
            else
            {
                if (Session["dataDoPedido"] != null)
                    e.Value = Session["dataDoPedido"].ToString();
            }
        }
        if (e.Column.FieldName == "vencimentoBoleto")
        {
            if (e.Kind == GridViewAutoFilterEventKind.CreateCriteria)
            {
                Session["vencimentoBoleto"] = e.Value;
                String[] dates = e.Value.Split('|');
                DateTime dateFrom = Convert.ToDateTime(dates[0]),
                    dateTo = Convert.ToDateTime(dates[1]);
                e.Criteria = (new OperandProperty("vencimentoBoleto") >= dateFrom) &
                             (new OperandProperty("vencimentoBoleto") <= dateTo);
            }
            else
            {
                if (Session["vencimentoBoleto"] != null)
                    e.Value = Session["vencimentoBoleto"].ToString();
            }
        }
        if (e.Column.FieldName == "dataPedir")
        {
            if (e.Kind == GridViewAutoFilterEventKind.CreateCriteria)
            {
                Session["dataPedir"] = e.Value;
                String[] dates = e.Value.Split('|');
                DateTime dateFrom = Convert.ToDateTime(dates[0]),
                    dateTo = Convert.ToDateTime(dates[1]);
                e.Criteria = (new OperandProperty("dataPedir") >= dateFrom) &
                             (new OperandProperty("dataPedir") <= dateTo);
            }
            else
            {
                if (Session["dataPedir"] != null)
                    e.Value = Session["dataPedir"].ToString();
            }
        }
    }


    protected void grd_OnCustomUnboundColumnData(object sender, ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "pedidoIdCliente")
        {
            int pedidoId = Convert.ToInt32(e.GetListSourceFieldValue("pedidoId"));
            e.Value = rnFuncoes.retornaIdCliente(pedidoId);
        }
    }

    protected void btnFiltrar_OnClick(object sender, EventArgs e)
    {
        fillGrid(true);
    }
}
