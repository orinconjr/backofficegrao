﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;

public partial class admin_geraXml : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            FileInfo file = new FileInfo(ConfigurationManager.AppSettings["caminhoFisico"].ToString() + "xml/produtos.xml");

            if (file.Exists)
            {
                file.Delete();
            }

            StreamWriter sw = file.CreateText();
            sw.WriteLine("<RDC>");

            foreach (DataRow dr in rnProdutos.selecionaParaGerarXML().Tables[0].Rows)
            {
                sw.WriteLine("<Produto>");
                sw.WriteLine("<id_produto>" + dr["produtoId"].ToString() + "</id_produto>");
                sw.WriteLine("<link_produto>http://www.rdccomercial.com.br/" + dr["produtoUrl"].ToString() + "/produto/" + dr["produtoId"].ToString() + ".aspx</link_produto>");
                sw.WriteLine("<descricao>" + dr["produtoNome"].ToString().Replace("&","") + "</descricao>");
                sw.WriteLine("<precode>" + double.Parse(dr["produtoPreco"].ToString()) + "</precode>");
                sw.WriteLine("<preco_normal>" + double.Parse(dr["produtoPrecoAtacado"].ToString()) + "</preco_normal>");
                sw.WriteLine("<parcelamento></parcelamento>");
                sw.WriteLine("<imagem>http://www.rdccomercial.com.br/fotos" + "/" + dr["produtoId"].ToString() + "/pequena_" + dr["produtoFoto"].ToString() + ".jpg</imagem>");
                sw.WriteLine("<categoria>" + dr["categoriaNome"].ToString().Replace("&", "") + "</categoria>");
                sw.WriteLine("</Produto>");
            }

            sw.WriteLine("</RDC>");


            sw.WriteLine();
            sw.WriteLine();
            sw.Close();
        }
    }
}
