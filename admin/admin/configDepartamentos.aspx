﻿<%@ Page Title="" Theme="Glass" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="configDepartamentos.aspx.cs" Inherits="admin_configDepartamentos" %>

<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxTimer" TagPrefix="dxt" %>
<%@ Register TagPrefix="dxe" Namespace="DevExpress.Web.ASPxMenu" Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxPopupControl" Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxEditors" Assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>
<%@ Register Assembly="CuteEditor" Namespace="CuteEditor" TagPrefix="CE" %>


<%@ Register Assembly="DevExpress.Web.v11.1" Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v11.1" Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>


<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" runat="Server">
    <asp:ScriptManager ID="scriptManager" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="updPanel" runat="server">
        <ContentTemplate>
            <dx:ASPxPopupControl ID="popAdicionarTarefa" runat="server" CloseAction="CloseButton" Modal="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
                ClientInstanceName="popAdicionarTarefa" HeaderText="Edição de tarefa" AllowDragging="True" PopupAnimationType="None" EnableViewState="True" Height="500"
                AutoUpdatePosition="True">
                <ContentCollection>
                    <dx:PopupControlContentControl ID="PopupControlContentControl3" runat="server">
                        <asp:HiddenField runat="server" ID="hdfTarefaId" />
                        <div style="width: 800px; height: 300px;">
                            <table width="100%">
                                <tr>
                                    <td colspan="2">
                                        <asp:Label ID="lblMensagemTarefa" runat="server" ForeColor="Red"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td><b>Nome:</b><br />
                                        <asp:TextBox ID="txtTarefa" runat="server" MaxLength="50"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <div style="height: 300px; overflow-x: auto;">
                                            <asp:GridView ID="gvTarefa" Width="100%" DataKeyNames="idChamadoTarefa" AutoGenerateColumns="false" runat="server"
                                                EmptyDataText="Registro não encontrado">
                                                <Columns>
                                                    <asp:BoundField DataField="idChamadoTarefa" HeaderText="ID" ItemStyle-Width="10px" />
                                                    <asp:BoundField DataField="nomeChamadoTarefa" HeaderText="Tarefa" ItemStyle-Width="500px" />
                                                    <asp:TemplateField ItemStyle-Width="100px">
                                                        <ItemTemplate>
                                                            <asp:Button ID="btnAlterarTarefa" OnCommand="btnAlterarTarefa_Command" runat="server" Text="Alterar" CommandArgument='<%# Eval("idChamadoTarefa") %>' CommandName="AlterarTarefa"  />
                                                            <%--<asp:ImageButton ID="btnExcluirTarefa" runat="server" OnCommand="btnExcluirTarefa_Command"
                                                                CommandArgument='<%# Eval("idChamadoTarefa") %>' CommandName="ExcluirTarefa" ImageUrl="~/admin/images/btExcluir.jpg" />--%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>

                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" colspan="2">
                                        <asp:Button ID="btnAddTarefa" OnClick="btnAddTarefa_Click" runat="server" Text="Gravar" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">&nbsp;</td>
                                </tr>
                            </table>
                        </div>
                    </dx:PopupControlContentControl>
                </ContentCollection>
                <ContentStyle>
                    <Paddings PaddingBottom="5px" />
                </ContentStyle>
            </dx:ASPxPopupControl>
            <!-- -->
            <dx:ASPxPopupControl ID="popAdicionarAssunto" runat="server" CloseAction="CloseButton" Modal="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
                ClientInstanceName="popAdicionarAssunto" HeaderText="Edição de Assunto" AllowDragging="True" PopupAnimationType="None" EnableViewState="True" Height="500"
                AutoUpdatePosition="True">
                <ContentCollection>
                    <dx:PopupControlContentControl ID="PopupControlContentControl2" runat="server">
                        <asp:HiddenField runat="server" ID="hdfAssuntoId" />
                        <div style="width: 800px; height: 300px;">
                            <table width="100%">
                                <tr>
                                    <td colspan="2">
                                        <asp:Label ID="lblMensagemAssunto" runat="server" ForeColor="Red"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td><b>Nome:</b><br />
                                        <asp:TextBox ID="txtAssunto" runat="server" MaxLength="50"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <div style="height: 300px; overflow-x: auto;">
                                            <asp:GridView ID="gvAssunto" Width="100%" DataKeyNames="idChamadoAssunto" AutoGenerateColumns="false" runat="server"
                                                EmptyDataText="Registro não encontrado" >
                                                <Columns>
                                                    <asp:BoundField DataField="idChamadoAssunto" HeaderText="ID" ItemStyle-Width="10px" />
                                                    <asp:BoundField DataField="nomeChamadoAssunto" HeaderText="Assunto" ItemStyle-Width="500px" />
                                                    <asp:TemplateField ItemStyle-Width="100px">
                                                        <ItemTemplate>
                                                            <asp:Button ID="btnAlterarAssunto" OnCommand="btnAlterarAssunto_Command" runat="server" Text="Alterar" CommandArgument='<%# Eval("idChamadoAssunto") %>' CommandName="AlterarAssunto"  />
                                                            <%--<asp:ImageButton ID="btnExcluir" runat="server" OnCommand="btnExcluir_Command"
                                                                CommandArgument='<%# Eval("idChamadoAssunto") %>' CommandName="ExcluirAssunto" ImageUrl="~/admin/images/btExcluir.jpg" />--%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>

                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" colspan="2">
                                        <asp:Button ID="btnAddAssunto" OnClick="btnAddAssunto_Click" runat="server" Text="Gravar" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">&nbsp;</td>
                                </tr>
                            </table>
                        </div>
                    </dx:PopupControlContentControl>
                </ContentCollection>
                <ContentStyle>
                    <Paddings PaddingBottom="5px" />
                </ContentStyle>
            </dx:ASPxPopupControl>
            <!-- -->
            <dx:ASPxPopupControl ID="popAdicionarDepartamento" runat="server" CloseAction="CloseButton" Modal="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
                ClientInstanceName="popAdicionarDepartamento" HeaderText="Edição de Departamento" AllowDragging="True" PopupAnimationType="None" EnableViewState="True" Height="500"
                AutoUpdatePosition="True">
                <ContentCollection>
                    <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server">
                        <asp:HiddenField runat="server" ID="HiddenField1" />
                        <div style="width: 800px; height: 300px;">
                            <table width="100%">
                                <tr>
                                    <td colspan="2">
                                        <asp:Label ID="lblMensagemAdd" runat="server" ForeColor="Red"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td><b>Nome:</b><br />
                                        <asp:TextBox ID="txtNomeDepartamento" runat="server" MaxLength="50"></asp:TextBox>
                                    </td>
                                    <td><b>Ordem:</b><br />
                                        <asp:TextBox ID="txtOrdemDepartamento" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <table width="100%">
                                            <tr>
                                                <td><b>Assunto:</b><br />
                                                    <dx:ASPxComboBox ID="cboAssunto" AutoPostBack="true" OnSelectedIndexChanged="cboAssunto_SelectedIndexChanged" runat="server" Width="250px"></dx:ASPxComboBox>
                                                </td>
                                                <td>
                                                    <b>Tarefa:</b><br />
                                                    <dx:ASPxComboBox ID="cboTarefa" Width="250px" runat="server"></dx:ASPxComboBox>
                                                </td>
                                                <td align="right">
                                                    <br />
                                                    <asp:Button ID="btnAdicionarAssuntoTarefa" runat="server" OnClick="btnAdicionarAssuntoTarefa_Click" Text="Adicionar" />
                                                </td>
                                            </tr>
                                        </table>
                                        <br />
                                        <div style="height: 300px; overflow-x: auto;">
                                            <asp:GridView ID="gvAssuntosTarefas" Width="100%" DataKeyNames="idDepartamentAssuntoTarefa,idAssunto,idTarefa" AutoGenerateColumns="false" runat="server"
                                                EmptyDataText="Registro não encontrado" OnRowDataBound="gvAssuntosTarefas_RowDataBound">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Assunto">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblAssunto" runat="server" Text='<%# Eval("idAssunto") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Tarefa">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblTarefa" runat="server" Text='<%# Eval("idTarefa") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField ItemStyle-Width="20px">
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="btnExcluir" runat="server" OnCommand="btnExcluir_Command"
                                                                CommandArgument='<%# Container.DataItemIndex  %>' CommandName="ExcluirAssuntoTarefa" ImageUrl="~/admin/images/btExcluir.jpg" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>

                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" colspan="2">
                                        <asp:Button ID="btnAddDepartamento" OnClick="btnAddDepartamento_Click" runat="server" Text="Gravar" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">&nbsp;</td>
                                </tr>
                            </table>
                        </div>
                    </dx:PopupControlContentControl>
                </ContentCollection>
                <ContentStyle>
                    <Paddings PaddingBottom="5px" />
                </ContentStyle>
            </dx:ASPxPopupControl>

            <dx:ASPxPopupControl ID="popManutencaoDepartamento" runat="server" CloseAction="CloseButton" Modal="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
                ClientInstanceName="popManutencaoDepartamento" HeaderText="Manutenção de Departamento" AllowDragging="True" PopupAnimationType="None" EnableViewState="True" Height="500"
                AutoUpdatePosition="True">
                <ContentCollection>
                    <dx:PopupControlContentControl ID="PopupControlContentControl6" runat="server">
                        <asp:HiddenField runat="server" ID="hdfIdChamadoDepartamento" />
                        <div style="width: 800px; height: 500px;">
                            <table width="100%">
                                <tr>
                                    <td>
                                        <h2>
                                            <asp:Label ID="lblDepartamentoSelecionado" runat="server"></asp:Label></h2>
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>
                                        <table width="100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td align="center" style="width: 45%"><b>Usuários sem acesso</b></td>
                                                <td align="center" style="width: 10%"><b>Ação</b></td>
                                                <td align="center" style="width: 45%"><b>Usuários com acesso</b></td>
                                            </tr>
                                            <tr>
                                                <td align="center" style="width: 45%">
                                                    <asp:ListBox ID="lstUsuarioSemAcesso" runat="server" Width="100%" Height="300px" SelectionMode="Multiple" Rows="20"></asp:ListBox>
                                                </td>
                                                <td align="center" style="width: 10%">
                                                    <table width="100%" class="rotulos" border="0">
                                                        <tr>
                                                            <td align="center">
                                                                <asp:Button ID="btnDarAcesso" runat="server" Text=">>" OnClick="btnDarAcesso_Click" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center">
                                                                <asp:Button ID="btnTirarAcesso" runat="server" Text="<<" OnClick="btnTirarAcesso_Click" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td align="center" style="width: 45%">
                                                    <asp:ListBox ID="lstUsuarioComAcesso" runat="server" Width="100%" Height="300px" SelectionMode="Multiple" Rows="20"></asp:ListBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </dx:PopupControlContentControl>
                </ContentCollection>
                <ContentStyle>
                    <Paddings PaddingBottom="5px" />
                </ContentStyle>
            </dx:ASPxPopupControl>

            <table cellpadding="0" cellspacing="0" style="width: 920px">
                <tr>
                    <td class="tituloPaginas" valign="top">
                        <asp:Label ID="lblAcao" runat="server">Manutenção de Departamentos</asp:Label>
                    </td>
                </tr>
                <tr>
                    <td style="padding-left: 20px">
                        <asp:Button runat="server" ID="btnAdicionarDepartamento" Text="Adicionar Departamento" OnClick="btnAdicionarDepartamento_Click" />
                        &nbsp;
                        <asp:Button runat="server" ID="btnAdicionarAssunto" Text="Adicionar Assunto" OnClick="btnAdicionarAssunto_Click" />
                        &nbsp;
                        <asp:Button runat="server" ID="btnAdicionarTarefa" Text="Adicionar Tarefa" OnClick="btnAdicionarTarefa_Click" />
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblMensagem" runat="server" ForeColor="Red"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td style="padding-left: 20px">
                        <dxwgv:ASPxGridView ID="grd" ClientInstanceName="grd" runat="server" AutoGenerateColumns="False"
                            KeyFieldName="idChamadoDepartamento" Width="834px" Settings-ShowFilterRow="True" Settings-ShowFilterBar="Visible"
                            Cursor="auto" OnHtmlRowCreated="grd_HtmlRowCreated" EnableViewState="False">
                            <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                            <Styles>
                                <Footer Font-Bold="True">
                                </Footer>
                            </Styles>
                            <SettingsText ConfirmDelete="Tem certeza que deseja excluir?" EmptyDataRow="Nenhum registro encontrado." />
                            <SettingsPager Position="TopAndBottom" PageSize="500"
                                ShowDisabledButtons="False" AlwaysShowPager="True">
                                <Summary AllPagesText="Páginas: {0} - {1} ({2} items)"
                                    Text="Página {0} de {1} ({2} registros encontrados)" />
                            </SettingsPager>
                            <Settings ShowFilterRow="True" ShowGroupButtons="False" ShowFooter="True" />
                            <SettingsEditing EditFormColumnCount="4"
                                PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1"
                                PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter"
                                PopupEditFormWidth="700px" />
                            <Columns>
                                <dxwgv:GridViewDataTextColumn Caption="Id" FieldName="idChamadoDepartamento" Width="50px" VisibleIndex="0">
                                    <Settings AutoFilterCondition="Contains" />
                                    <EditFormSettings Visible="False" />
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataTextColumn Caption="Departamento" FieldName="nomeChamadoDepartamento" VisibleIndex="0">
                                    <Settings AutoFilterCondition="Contains" />
                                    <EditFormSettings Visible="False" />
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataTextColumn Caption="Ordem" FieldName="ordem" Width="50px" VisibleIndex="0">
                                    <Settings AutoFilterCondition="Contains" />
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataTextColumn Caption="Alterar" Width="100px" Name="alterar" VisibleIndex="0">
                                    <DataItemTemplate>
                                        <asp:ImageButton ID="btnAlterarDepartamento" CommandArgument='<%# Eval("idChamadoDepartamento") %>'
                                            OnCommand="btnAlterarDepartamento_Command" CommandName="Alterar" runat="server" ImageAlign="AbsMiddle" ImageUrl="images/btAlterar.jpg" />

                                        <asp:Button ID="btnVincularUsuario" runat="server" Text="Vincular usuários" OnCommand="btnAlterarDepartamento_Command"
                                            CommandName="VincularUsuario" CommandArgument='<%# Eval("idChamadoDepartamento") %>' />
                                    </DataItemTemplate>
                                </dxwgv:GridViewDataTextColumn>
                            </Columns>
                            <StylesEditors>
                                <Label Font-Bold="True">
                                </Label>
                            </StylesEditors>
                        </dxwgv:ASPxGridView>
                    </td>
                </tr>
                <tr class="rotulos">
                    <td style="padding-top: 30px; font-size: 20px;">&nbsp;
                    </td>
                </tr>

            </table>

        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

