﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="vendasDoDia.aspx.cs" Inherits="admin_relatorios_vendasDoDia" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Relátorios</title>
    <script src="../js/jquery.min.js"></script>
    <script src="../js/highcharts.js"></script>
    <script src="../js/exporting.js"></script>
    <script src="../js/pubsub.js"></script>
    <link rel="stylesheet" href="../css/vendasDoDia.css" type="text/css" />

    <!--Load the AJAX API-->
    <script type="text/javascript">

        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1;
        var yyyy = today.getFullYear();

        var dataHojeCompleta = dd + "/" + mm + "/" + yyyy;

        var ontem = new Date();
        ontem.setDate(ontem.getDate()-1);
        var dataOntemCompleta = ontem.getDate() + '/' + (ontem.getMonth()+1) + '/' + ontem.getFullYear();

        ////////////////////////////////////////////////////////////////////////////
        /////////////////// Functions
        ////////////////////////////////////////////////////////////////////////////

        console.log("Nome: Load - Hora:" + new Date().getHours() + ":" + new Date().getMinutes() + ":" + new Date().getSeconds());

        Number.prototype.formatMoney = function (c, d, t) {
            var n = this, c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "," : d, t = t == undefined ? "." : t, s = n < 0 ? "-" : "", i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
            return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
        };

        Array.prototype.max = function () {
            return Math.max.apply(null, this);
        };

        var isMobile = {
            Android: function () {
                return navigator.userAgent.match(/Android/i);
            },
            BlackBerry: function () {
                return navigator.userAgent.match(/BlackBerry/i);
            },
            iOS: function () {
                return navigator.userAgent.match(/iPhone|iPod/i);
            },
            Opera: function () {
                return navigator.userAgent.match(/Opera Mini/i);
            },
            Windows: function () {
                return navigator.userAgent.match(/IEMobile/i);
            },
            any: function () {
                return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
            }
        };

        function GraficoType() {
            width = $(window).width();

            if (isMobile.any()) {
                $("#relatorioTotal").hide();
                $("#relatorioTotalBarLegenda").removeClass("column");
                $("#relatorioTotalBar").show();
            } else {
                $("#relatorioTotal").show();
                $("#relatorioTotalBarLegenda").addClass("column");
                $("#relatorioTotalBar").hide();
            }
        }

        ////////////////////////////////////////////////////////////////////////////
        /////////////////// Cria variaveis
        ////////////////////////////////////////////////////////////////////////////

        // Arrays de Hoje
        var estatisticasValorHoje = new Array();
        var estatisticasPedidoHoje = new Array();
        var estatisticasMedioHoje = new Array();
        var valorHoje = 0;
        var pedidosHoje = 0;
        var tickerHoje = 0;

        // Arrays Todos
        var vetorTemp = new Array();
        var estatisticasDia = new Array();
        var estatisticasValor = new Array();
        var estatisticasValorColor = new Array();
        var estatisticasValorMaior = 0;
        var estatisticasPedido = new Array();
        var estatisticasPedidoColor = new Array();
        var estatisticasMedio = new Array();
        var estatisticasMedioColor = new Array();
        var estatisticasPedidoMaior = 0;

        // Porcentagens
        var porcentagemReceita = new Array();
        var porcentagemReceitaCal = new Array();
        var porcentagemPedidos = new Array();
        var porcentagemTicket = new Array();

        var colorReceita = "#000097";
        var colorPedido = "#2828FF";
        var colorTicket = "#6F6FFF";

        var ts = new Date().getTime();
        var horaDia = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23];
        var optionsBar;
        var optionsColumn;
        
        ////////////////////////////////////////////////////////////////////////////
        /////////////////// Cria Vetor
        ////////////////////////////////////////////////////////////////////////////

        var GraficoObject = {
            init: function(){
                GraficoObject.relatorioTotal();
            },
            relatorioTotal: function(){
                var ts = new Date().getTime();
                var enderecoTotal = "../relatorioServices/vendasDoDia.aspx";

                $.get(enderecoTotal,
                {
                    nocache: ts
                }, function (data) {
                    var objects = data;

                    GraficoObject.consoleLog("Gráfico Todo:", data);

                    var gsDayNames = new Array(
                        'Seg',
                        'Ter',
                        'Qua',
                        'Qui',
                        'Sex',
                        'Sab',
                        'Dom'
                    );
                    
                    for (var $i = 0; $i < objects['Table'].length; $i++) {
                        
                        // Hoje Dados
                        valorHoje = objects['Table'][0]['valor'];
                        pedidosHoje = objects['Table'][0]['quantidadePedidos'];
                        tickerHoje = (valorHoje / pedidosHoje);

                        // Valor Comparado
                        var data = String(String(objects['Table'][$i]['data'].split("T")).split("-")).split(",");

                        var d = new Date(data[0] + "-" + data[1] + "-" + data[2]);
                        var dayName = gsDayNames[d.getDay()];

                        var dia = objects['Table'][$i]['dia'] + " - " + data[2] + "/" + data[1] + " - " + dayName;
                        var valor = objects['Table'][$i]['valor'];
                        var pedidos = objects['Table'][$i]['quantidadePedidos'];

                        // Porcetagem Calculada
                        var porcentagemReceitaValue = (valorHoje * 100 / valor) - 100;
                        var porcentagemPedidosValue = (pedidosHoje * 100 / pedidos) - 100;
                        var porcentagemTicketValue = ((valorHoje / pedidosHoje) * 100 / (valor / pedidos)) - 100;
                        
                        porcentagemReceita[$i] = porcentagemReceitaValue;
                        porcentagemReceitaCal[$i] = [valorHoje.formatMoney(0, ',', '.'), valor.formatMoney(0, ',', '.')];
                        porcentagemPedidos[$i] = porcentagemPedidosValue;
                        porcentagemTicket[$i] = porcentagemTicketValue;

                        // VERIFICAÇÃO DAS CORES (VERMELHO, VERDE OU AZUL - DIA DE HOJE )

                        if (porcentagemReceitaValue > 0) {
                            colorReceita = "#204000";
                        } else if (porcentagemReceitaValue < 0) {
                            colorReceita = "#AE0000";
                        } else {
                            colorReceita = "#000097";
                        }

                        if (porcentagemPedidosValue > 0) {
                            colorPedido = "#73E600";
                        } else if (porcentagemPedidosValue < 0) {
                            colorPedido = "#FF6262";
                        } else {
                            colorPedido = "#2828FF";
                        }

                        if (porcentagemTicketValue > 0) {
                            colorTicket = "#468C00";
                        } else if (porcentagemTicketValue < 0) {
                            colorTicket = "#FF2B2B";
                        } else {
                            colorTicket = "#6F6FFF";
                        }

                        // VALORES DE HOJE NO ARRAY -- REPETINDO O NUMERO DO ARRAY PARA FAZER UMA LINHA RETA
                        if ($i == 0) {
                            for (var $b = 0; $b < objects['Table'].length; $b++) {
                                estatisticasValorHoje[$b] = [valor];
                                estatisticasPedidoHoje[$b] = [pedidos];
                                estatisticasMedioHoje[$b] = [Number((valor / pedidos).toFixed(2))];
                            }

                            colorReceita = "#000097";
                            colorPedido = "#2828FF";
                            colorTicket = "#6F6FFF";
                        }

                        // INSERINDO OS VALORES DA CONSULTA
                        estatisticasDia[$i] = [dia];

                        estatisticasValor[$i] = [valor];
                        estatisticasValorColor[$i] = [$i, colorReceita];
                        estatisticasValorMaior = estatisticasValor.max();

                        estatisticasPedido[$i] = [pedidos];
                        estatisticasPedidoColor[$i] = [$i, colorPedido];

                        estatisticasMedio[$i] = [Number((valor / pedidos).toFixed(2))];
                        estatisticasMedioColor[$i] = [$i, colorTicket];

                        if (estatisticasPedido.max() > estatisticasMedio.max()) {
                            estatisticasPedidoMaior = estatisticasPedido.max();
                        } else {
                            estatisticasPedidoMaior = estatisticasMedio.max();
                        }
                    }

                    ////////////////////////////////////////////////////////////////////////////
                    /////////////////// CONFIGURAÇÃO DO GRÁFICO DE COLUNA
                    ////////////////////////////////////////////////////////////////////////////

                    GraficoObject.constructColuna();

                    ////////////////////////////////////////////////////////////////////////////
                    /////////////////// CONFIGURAÇÃO DO GRÁFICO DE BARRA
                    ////////////////////////////////////////////////////////////////////////////

                    GraficoObject.constructBarra();

                    ////////////////////////////////////////////////////////////////////////////
                    /////////////////// START NO GRÁFICO DE BARRA E COLUNA
                    ////////////////////////////////////////////////////////////////////////////

                    $("#relatorioTotal").highcharts(optionsColumn);
                    $("#relatorioTotalBar").highcharts(optionsBar);

                    // HOVER FALSO PARA ARRUMAR UM BUG DA COR DEFAULT DO GRÁFICO -- FICA PRETO SEM ISSO E SÓ FUNCIONA AS CORES CERTAS NO HOVER
                    $("#relatorioTotal *, #relatorioTotalBar *").trigger('mouseover');

                    GraficoObject.relatorioHoraHoje();
                }).fail(function (data) {
                    GraficoObject.consoleLog("error", data);
                });
            },
            constructColuna: function(){
                optionsColumn = {
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: 'Relátorio de Vendas - Total'
                    },
                    xAxis: {
                        categories: estatisticasDia
                    },
                    yAxis: [{
                        title: {
                            text: 'Receita'
                        },
                        min: 0,
                        max: estatisticasValorMaior,
                        tickInterval: 1,
                        endOnTick: false
                    }, {
                        title: {
                            text: 'Pedidos'
                        },
                        min: 0,
                        max: estatisticasPedidoMaior,
                        tickInterval: 1,
                        endOnTick: false,
                        opposite: true
                    }],
                    legend: {
                        shadow: false
                    },
                    tooltip: {
                        shared: true
                    },
                    plotOptions: {
                        column: {
                            grouping: false,
                            borderWidth: 0,
                            colorByPoint: true,
                            dataLabels: {
                                enabled: true,
                                shape: 'callout',
                                color: 'white',
                                style: {
                                    fontSize: '10px',
                                    textShadow: '0 0 6px black, 0 0 3px black'
                                },
                            }
                        }
                    },
                    series: [{
                        name: 'Receita',
                        data: estatisticasValor,
                        colors: estatisticasValorColor,
                        pointPadding: 0,
                        pointPlacement: 0,
                        dataLabels: {
                            verticalAlign: 'bottom',
                            formatter: function () {
                                var cal = ((valorHoje * 100 / this.y) - 100).toFixed(2);

                                if (cal == 0) {
                                    return "R$ " + Math.ceil(this.y).formatMoney(0, ',', '.');
                                } else {
                                    return "R$ " + Math.ceil(this.y).formatMoney(0, ',', '.') + " | " + cal + "%";
                                }
                            }
                        }
                    }, {
                        name: 'Pedidos',
                        data: estatisticasPedido,
                        colors: estatisticasPedidoColor,
                        pointPadding: 0.2,
                        pointPlacement: 0,
                        yAxis: 1,
                        dataLabels: {
                            formatter: function () {
                                var cal = ((pedidosHoje * 100 / this.y) - 100).toFixed(2);

                                if (cal == 0) {
                                    return Math.ceil(this.y);
                                } else {
                                    return Math.ceil(this.y) + " | " + cal + "%";
                                }
                            }
                        }
                    }, {
                        name: 'Ticket Médio',
                        color: colorPedido,
                        data: estatisticasMedio,
                        colors: estatisticasMedioColor,
                        pointPadding: 0.4,
                        pointPlacement: 0,
                        yAxis: 1,
                        dataLabels: {
                            formatter: function () {
                                var cal = ((tickerHoje * 100 / this.y) - 100).toFixed(2);

                                if (cal == 0) {
                                    return Math.ceil(this.y);
                                } else {
                                    return Math.ceil(this.y) + " | " + cal + "%";
                                }
                            }
                        }
                    }, {
                        type: 'line',
                        name: 'Receita - Hoje',
                        color: '#0000D9',
                        data: estatisticasValorHoje,
                        pointPadding: 0,
                        pointPlacement: 0
                    }, {
                        type: 'line',
                        name: 'Ticket Médio - Hoje',
                        color: '#2693FF',
                        data: estatisticasMedioHoje,
                        pointPadding: 0.2,
                        pointPlacement: 0,
                        yAxis: 1
                    }, {
                        type: 'line',
                        name: 'Pedidos - Hoje',
                        color: '#003040',
                        data: estatisticasPedidoHoje,
                        pointPadding: 0.3,
                        pointPlacement: 0,
                        yAxis: 1
                    }]
                }
            },
            constructBarra: function(){
                optionsBar = {
                    chart: {
                        type: 'bar'
                    },
                    title: {
                        text: 'Relátorio de Vendas - Total'
                    },
                    xAxis: {
                        categories: estatisticasDia
                    },
                    yAxis: [{
                        title: {
                            text: 'Receita'
                        },
                        min: 0,
                        max: estatisticasValorMaior,
                        tickInterval: 1,
                        endOnTick: false
                    }, {
                        title: {
                            text: 'Pedidos'
                        },
                        min: 0,
                        tickInterval: 1,
                        max: estatisticasPedidoMaior,
                        opposite: true,
                        endOnTick: false
                    }],
                    legend: {
                        shadow: false
                    },
                    tooltip: {
                        shared: true,
                        headerFormat: '<span style="font-size: 10px;text-transform:uppercase;">{point.key}</span><br/>',
                        pointFormat: '{series.name}: <b>{point.y}</b><br/>'
                    },
                    plotOptions: {
                        bar: {
                            grouping: false,
                            borderWidth: 0,
                            colorByPoint: true,
                            dataLabels: {
                                enabled: true,
                                inside: true,
                                shape: 'callout',
                                color: 'white',
                                allowOverlap: true,
                                align: 'left',
                                padding: 0,
                                style: {
                                    fontSize: '10px',
                                    textShadow: '0 0 6px black, 0 0 3px black'
                                },
                                x: 10
                            }
                        }
                    },
                    series: [{
                        name: 'Receita',
                        data: estatisticasValor,
                        colors: estatisticasValorColor,
                        pointPadding: 0,
                        pointPlacement: 0,
                        dataLabels: {
                            formatter: function () {
                                var cal = ((valorHoje * 100 / this.y) - 100).toFixed(2);

                                if (cal == 0) {
                                    return "R$ " + Math.ceil(this.y).formatMoney(0, ',', '.');
                                } else {
                                    return "R$ " + Math.ceil(this.y).formatMoney(0, ',', '.') + " | " + ((valorHoje * 100 / this.y) - 100).toFixed(2) + "%";
                                }
                            },
                            y: -29
                        }
                    }, {
                        name: 'Pedidos',
                        data: estatisticasPedido,
                        colors: estatisticasPedidoColor,
                        pointPadding: 0.2,
                        yAxis: 1,
                        dataLabels: {
                            formatter: function () {
                                var cal = ((pedidosHoje * 100 / this.y) - 100).toFixed(2);

                                if (cal == 0) {
                                    return Math.ceil(this.y);
                                } else {
                                    return Math.ceil(this.y) + " | " + ((pedidosHoje * 100 / this.y) - 100).toFixed(2) + "%";
                                }
                            },
                            zIndex: 1000,
                            y: -14
                        }
                    }, {
                        name: 'Ticket Médio',
                        color: colorPedido,
                        data: estatisticasMedio,
                        colors: estatisticasMedioColor,
                        pointPadding: 0.4,
                        yAxis: 1,
                        dataLabels: {
                            formatter: function () {
                                var cal = ((tickerHoje * 100 / this.y) - 100).toFixed(2);

                                if (cal == 0) {
                                    return Math.ceil(this.y);
                                } else {
                                    return Math.ceil(this.y) + " | " + ((tickerHoje * 100 / this.y) - 100).toFixed(2) + "%";
                                }
                            },
                            y: 1
                        }
                    }, {
                        type: 'line',
                        name: 'Receita - Hoje',
                        color: '#0000D9',
                        data: estatisticasValorHoje,
                        pointPadding: 0,
                        pointPlacement: 0
                    }, {
                        type: 'line',
                        name: 'Ticket Médio - Hoje',
                        color: '#2693FF',
                        data: estatisticasMedioHoje,
                        pointPadding: 0.2,
                        pointPlacement: 0,
                        yAxis: 1
                    }, {
                        type: 'line',
                        name: 'Pedidos - Hoje',
                        color: '#003040',
                        data: estatisticasPedidoHoje,
                        pointPadding: 0.3,
                        pointPlacement: 0,
                        yAxis: 1
                    }]
                }
            },
            relatorioHoraHoje: function(){
                var ConsultaHojeValor = new Array();
                var ConsultaHojeHora = new Array();
                var ConsultaHojeValorNew = new Array();
                var biggest = 0;
                var enderecoHoje = "../relatorioServices/relatorioVendasPorData.aspx?data=" + dataHojeCompleta;

                $.get(enderecoHoje,
                {
                    nocache: ts
                }, function (data) {
                    var objects = data;
                    
                    GraficoObject.consoleLog("Relátorio Hoje:", data);

                    vetorTemp = GraficoObject.processaVetorHora(objects);

                    ConsultaHojeValor = vetorTemp[0];
                    ConsultaHojeHora = vetorTemp[1];
                    ConsultaHojeValorNew = vetorTemp[2];

                    GraficoObject.relatorioHoraOntem(ConsultaHojeValorNew);

                    GraficoObject.relatorioHoraUmaSemana(ConsultaHojeValorNew);

                    GraficoObject.relatorioHoraUmMes(ConsultaHojeValorNew);

                    GraficoObject.relatorioHoraMediaUmaSemana(ConsultaHojeValorNew);

                    GraficoObject.relatorioHoraMedia30Dias(ConsultaHojeValorNew);

                    GraficoObject.relatorioHoraMediaMes(ConsultaHojeValorNew);

                    GraficoObject.relatorioHoraMelhorDiaDoMes(ConsultaHojeValorNew);

                }).fail(function (data) {
                    console.log("Relátorio Hoje Error");
                    console.log(data);
                });
            },
            processaVetorHora: function (objeto) {
                var vetor = new Array();
                var vetorValor = new Array();
                var vetorHora = new Array();
                var vetorValorNew = new Array();

                for (var $i = 0; $i < objeto['Table'].length; $i++) {

                    var valor = objeto['Table'][$i]['valor'];
                    var hora = objeto['Table'][$i]['hora'];

                    vetorValor[$i] = valor;
                    vetorHora[$i] = hora;
                }

                for (var $i = 0, $iNew = 0; $i < vetorValor.length; $i++) {
                    if (vetorHora[$i] == $iNew) {
                        vetorValorNew[$iNew] = vetorValor[$i];
                        $iNew++;
                    } else {
                        while ($iNew < vetorHora[$i]) {
                            vetorValorNew[$iNew] = 0;
                            $iNew++;
                        }

                        $i--;
                    }
                }

                vetor[0] = vetorValor;
                vetor[1] = vetorHora;
                vetor[2] = vetorValorNew;

                return vetor;
            },
            verificaMaxValor: function (vetorHoje, vetorCorrent) {
                var max = 0;

                if (vetorHoje.max() > vetorCorrent.max()) {
                    max = vetorHoje.max();
                } else {
                    max = vetorCorrent.max();
                }

                return max;
            },
            consoleLog: function (text, data) {
                var date = new Date;
                var hours = date.getHours();
                var minutes = date.getMinutes();
                var seconds = date.getSeconds();

                console.log("Nome:" + text + " - Hora:" + hours + ":" + minutes + ":" + seconds);
                console.log("Vetor:" + data);
            },
            relatorioHoraOntem: function(HojeValorNew){
                var ConsultaOntemValor = new Array();
                var ConsultaOntemHora = new Array();
                var ConsultaOntemValorNew = new Array();

                var ts = new Date().getTime();
                var enderecoOntem = "../relatorioServices/relatorioVendasPorData.aspx?data=" + dataOntemCompleta;
                //var enderecoOntem = "../relatorioServices/relatorioVendasOntemHoraAtual.aspx";

                $.get(enderecoOntem,
                {
                    nocache: ts
                }, function (data) {
                    var objects = data;
                    
                    GraficoObject.consoleLog("Relátorio Ontem:", data);

                    vetorTemp = GraficoObject.processaVetorHora(objects);

                    ConsultaOntemValor = vetorTemp[0];
                    ConsultaOntemHora = vetorTemp[1];
                    ConsultaOntemValorNew = vetorTemp[2];

                    biggest = GraficoObject.verificaMaxValor(HojeValorNew, ConsultaOntemValorNew);

                    GraficoObject.constructHora(HojeValorNew, ConsultaOntemValorNew, porcentagemReceita[1], porcentagemReceitaCal[1], 'Ontem', 'Ontem', biggest);
                }).fail(function (data) {
                    GraficoObject.consoleLog("Relátorio Ontem Error", data);
                });
            },
            relatorioHoraUmaSemana: function(HojeValorNew){
                var ConsultaUmaSemanaValor = new Array();
                var ConsultaUmaSemanaHora = new Array();
                var ConsultaUmaSemanaValorNew = new Array();

                var enderecoUmaSemana = "../relatorioServices/relatorioVendasUmaSemanaHoraAtual.aspx";

                $.get(enderecoUmaSemana,
                {
                    nocache: ts
                }, function (data) {
                    var objects = data;

                    GraficoObject.consoleLog("Relátorio Uma Semana:", data);

                    vetorTemp = GraficoObject.processaVetorHora(objects);

                    ConsultaUmaSemanaValor = vetorTemp[0];
                    ConsultaUmaSemanaHora = vetorTemp[1];
                    ConsultaUmaSemanaValorNew = vetorTemp[2];

                    biggest = GraficoObject.verificaMaxValor(HojeValorNew, ConsultaUmaSemanaValorNew);

                    GraficoObject.constructHora(HojeValorNew, ConsultaUmaSemanaValorNew, porcentagemReceita[2], porcentagemReceitaCal[2], 'UmaSemana', '1 Semana', biggest);
                }).fail(function (data) {
                    GraficoObject.consoleLog("Relátorio Uma Semana Error", data);
                });
            },
            relatorioHoraUmMes: function(HojeValorNew){
                var ConsultaUmaMesValor = new Array();
                var ConsultaUmaMesHora = new Array();
                var ConsultaUmaMesValorNew = new Array();

                var enderecoUmMes = "../relatorioServices/relatorioVendasUmMesHoraAtual.aspx";

                $.get(enderecoUmMes,
                {
                    nocache: ts
                }, function (data) {
                    var objects = data;

                    GraficoObject.consoleLog("Relátorio Um Mês:", data);

                    vetorTemp = GraficoObject.processaVetorHora(objects);

                    ConsultaUmaMesValor = vetorTemp[0];
                    ConsultaUmaMesHora = vetorTemp[1];
                    ConsultaUmaMesValorNew = vetorTemp[2];

                    biggest = GraficoObject.verificaMaxValor(HojeValorNew, ConsultaUmaMesValorNew);

                    GraficoObject.constructHora(HojeValorNew, ConsultaUmaMesValorNew, porcentagemReceita[3], porcentagemReceitaCal[3], 'UmMes', '1 Mês', biggest);
                }).fail(function (data) {
                    GraficoObject.consoleLog("Relátorio Um Mês Error", data);
                });
            },
            relatorioHoraMediaUmaSemana: function(HojeValorNew){
                var ConsultaMedia7DiasValor = new Array();
                var ConsultaMedia7DiasHora = new Array();
                var ConsultaMedia7DiasValorNew = new Array();

                var enderecoMedia7 = "../relatorioServices/relatorioVendasMediaUmaSemanaHoraAtual.aspx";

                $.get(enderecoMedia7,
                {
                    nocache: ts
                }, function (data) {
                    var objects = data;

                    GraficoObject.consoleLog("Relátorio Um Média 7 dias:", data);

                    vetorTemp = GraficoObject.processaVetorHora(objects);

                    ConsultaMedia7DiasValor = vetorTemp[0];
                    ConsultaMedia7DiasHora = vetorTemp[1];
                    ConsultaMedia7DiasValorNew = vetorTemp[2];

                    biggest = GraficoObject.verificaMaxValor(HojeValorNew, ConsultaMedia7DiasValorNew);

                    GraficoObject.constructHora(HojeValorNew, ConsultaMedia7DiasValorNew, porcentagemReceita[4], porcentagemReceitaCal[4], 'Media7', 'Media 7 Dias', biggest);
                }).fail(function (data) {
                    GraficoObject.consoleLog("Relátorio Um Média 7 dias Error", data);
                });
            },
            relatorioHoraMedia30Dias: function(HojeValorNew){
                var ConsultaMedia30DiasValor = new Array();
                var ConsultaMedia30DiasHora = new Array();
                var ConsultaMedia30DiasValorNew = new Array();

                var enderecoMedia30 = "../relatorioServices/relatorioVendasMedia30DiasHoraAtual.aspx";

                $.get(enderecoMedia30,
                {
                    nocache: ts
                }, function (data) {
                    var objects = data;

                    GraficoObject.consoleLog("Relátorio Um Média 30 dias:", data);

                    vetorTemp = GraficoObject.processaVetorHora(objects);

                    ConsultaMedia30DiasValor = vetorTemp[0];
                    ConsultaMedia30DiasHora = vetorTemp[1];
                    ConsultaMedia30DiasValorNew = vetorTemp[2];

                    biggest = GraficoObject.verificaMaxValor(HojeValorNew, ConsultaMedia30DiasValorNew);

                    GraficoObject.constructHora(HojeValorNew, ConsultaMedia30DiasValorNew, porcentagemReceita[5], porcentagemReceitaCal[5], 'Media30', 'Media 30 Dias', biggest);
                }).fail(function (data) {
                    GraficoObject.consoleLog("Relátorio Um Média 30 dias Error", data);
                });
            },
            relatorioHoraMediaMes: function(HojeValorNew){
                var ConsultaMediaMesDiasValor = new Array();
                var ConsultaMediaMesDiasHora = new Array();
                var ConsultaMediaMesDiasValorNew = new Array();

                var enderecoMediaMes = "../relatorioServices/relatorioVendasMediaMesHoraAtual.aspx";

                $.get(enderecoMediaMes,
                {
                    nocache: ts
                }, function (data) {
                    var objects = data;

                    GraficoObject.consoleLog("Relátorio Um Média Mês:", data);

                    vetorTemp = GraficoObject.processaVetorHora(objects);

                    ConsultaMediaMesDiasValor = vetorTemp[0];
                    ConsultaMediaMesDiasHora = vetorTemp[1];
                    ConsultaMediaMesDiasValorNew = vetorTemp[2];

                    biggest = GraficoObject.verificaMaxValor(HojeValorNew, ConsultaMediaMesDiasValorNew);

                    GraficoObject.constructHora(HojeValorNew, ConsultaMediaMesDiasValorNew, porcentagemReceita[6], porcentagemReceitaCal[6], 'MediaMes', 'Media Mês', biggest);
                }).fail(function (data) {
                    GraficoObject.consoleLog("Relátorio Um Média Mês Error", data);
                });
            },
            relatorioHoraMelhorDiaDoMes: function(HojeValorNew){
                var ConsultaMelhorDiaDoMesValor = new Array();
                var ConsultaMelhorDiaDoMesHora = new Array();
                var ConsultaMelhorDiaDoMesValorNew = new Array();

                var enderecoMelhorDiaDoMes = "../relatorioServices/relatorioVendasMelhorDiaMesHoraAtual.aspx";

                $.get(enderecoMelhorDiaDoMes,
                {
                    nocache: ts
                }, function (data) {
                    var objects = data;

                    GraficoObject.consoleLog("Relátorio Melhor Dia do Mês:", data);

                    vetorTemp = GraficoObject.processaVetorHora(objects);

                    ConsultaMelhorDiaDoMesValor = vetorTemp[0];
                    ConsultaMelhorDiaDoMesHora = vetorTemp[1];
                    ConsultaMelhorDiaDoMesValorNew = vetorTemp[2];

                    biggest = GraficoObject.verificaMaxValor(HojeValorNew, ConsultaMelhorDiaDoMesValorNew);

                    GraficoObject.constructHora(HojeValorNew, ConsultaMelhorDiaDoMesValorNew, porcentagemReceita[7], porcentagemReceitaCal[7], 'MelhorDiaDoMes', 'Melhor Dia do Mês', biggest);
                }).fail(function (data) {
                    GraficoObject.consoleLog("Relátorio Melhor Dia do Mês Error", data);
                });
            },
            constructHora: function(ArrayValorHoje, ArrayValor, porcentagem, porcentagemCal, name, title, biggest){
                var css = (porcentagem == 0) ? "blue" : (porcentagem > 0) ? "green" : "red";

                var prefix;

                if(porcentagem.toFixed(2) < 0 || porcentagem.toFixed(2) == 0 ) {
                    prefix = "";
                } else {
                    prefix = "+";
                }

                var totalDia = 0;
                
                for (var i = 0; i < ArrayValor.length; i++) {
                    totalDia = totalDia + ArrayValor[i];
                };

                var html = "<span style='font-size: 20px;color: " + css + "'>" + prefix + " " + porcentagem.toFixed(2) + " %</span> | <span style='font-size:18px;'> Total " + title + ": " + totalDia.formatMoney(0, ',', '.') + " / Hora Atual: " + porcentagemCal[0] + "/" + porcentagemCal[1] + "</span>";

                $('#relatorioHoje' + name).highcharts({
                    chart: {
                        type: 'area'
                    },
                    title: {
                        text: 'Relátorio de Vendas - Hoje/' + title
                    },
                    xAxis: {
                        categories: horaDia
                    },
                    yAxis: {
                        title: {
                            text: 'Receita'
                        },
                        min: 0,
                        max: biggest
                    },
                    tooltip: {
                        headerFormat: "<span style='font-size: 10px'>{point.key} hr's</span><br/>",
                        pointFormat: "Receita: R$ {point.y}</b><br/>"
                    },
                    subtitle: {
                        align: 'center',
                        text: html,
                        useHTML: true
                    },
                    plotOptions: {
                        area: {
                            marker: {
                                enabled: false,
                                symbol: 'circle',
                                radius: 2,
                                states: {
                                    hover: {
                                        enabled: true
                                    }
                                }
                            }
                        }
                    },
                    series: [{
                        type: 'area',
                        name: title,
                        data: ArrayValor,
                        pointPadding: 0,
                        pointPlacement: 0
                    }, {
                        type: 'area',
                        name: 'Hoje',
                        data: ArrayValorHoje,
                        pointPadding: 0,
                        pointPlacement: 0,
                        dataLabels: {
                            enabled: true,
                            inside: true,
                            shape: 'callout',
                            color: 'white',
                            allowOverlap: true,
                            align: 'left',
                            padding: 0,
                            useHTML: true,
                            zIndex: 999999,
                            formatter: function () {
                                if( ArrayValor[this.x] != 0 && this.y != 0 ){
                                    var porcentagemHora = (this.y * 100 / ArrayValor[this.x]) - 100;
                                    var css = (porcentagemHora == 0) ? "blue" : (porcentagemHora > 0) ? "green" : "red";
                                    var prefix;

                                    if(porcentagemHora.toFixed(2) < 0 || porcentagemHora.toFixed(2) == 0 ) {
                                        prefix = "";
                                    } else {
                                        prefix = "+";
                                    }

                                    return "<span style='color: " + css + "'>" + prefix + " " + porcentagemHora.toFixed(2) + "%</span>";
                                } else {
                                    return "<span style='color: green;margin-left: 2px;'>+ 100%</span>";
                                }
                            },
                            x: -25,
                            y: -10
                        }
                    }]
                });
            }
        }

        ////////////////////////////////////////////////////////////////////////////
        /////////////////// RELATORIO DE VENDAS TOTAL
        ////////////////////////////////////////////////////////////////////////////

        $(document).ready(function () {

            var width;
            
            GraficoType();

            $(window).resize(function () {
                GraficoType();
            });

            GraficoObject.init();

            ////////////////////////////////////////////////////////////////////////////
            /////////////////// RELATORIO DE VENDAS TOTAL
            ////////////////////////////////////////////////////////////////////////////


            // ALTERAR GRÁFICO DE COLUNA E BARRA
            $('#button').click(function () {
                $("#relatorioTotal").toggle();
                $("#relatorioTotalBar").toggle();

                if ($("#relatorioTotal").is(":visible")) {
                    $("#relatorioTotalBarLegenda").addClass("column");
                } else {
                    $("#relatorioTotalBarLegenda").removeClass("column");
                }
            });
        });
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <a id="button">Mudar Gráfico</a>

        <!-- RELATORIO TOTAL -->
        <div id="relatorioTotal" class="mapas" style="display: none;"></div>
        <div id="relatorioTotalBarLegenda">
            <span class="receita">
                Receita
            </span>
            <span class="pedidos">
                Pedidos
            </span>
            <span class="ticket">
                Ticket Médio
            </span>
        </div>
        <div id="relatorioTotalBar" class="mapas" style="height: 1200px;"></div>

        <!-- RELATORIO HOJE / ONTEM -->
        <div id="relatorioHojeOntem" class="mapas"></div>

        <!-- RELATORIO HOJE / 1 SEMANA -->
        <div id="relatorioHojeUmaSemana" class="mapas"></div>

        <!-- RELATORIO HOJE / 1 MÊS -->
        <div id="relatorioHojeUmMes" class="mapas"></div>

        <!-- RELATORIO HOJE / MÉDIA 7 DIAS -->
        <div id="relatorioHojeMedia7" class="mapas"></div>

        <!-- RELATORIO HOJE / MÉDIA 30 DIAS -->
        <div id="relatorioHojeMedia30" class="mapas"></div>

        <!-- RELATORIO HOJE / MÉDIA MÊS -->
        <div id="relatorioHojeMediaMes" class="mapas" ></div>

        <!-- RELATORIO HOJE / MELHOR DIA DO MÊS -->
        <div id="relatorioHojeMelhorDiaDoMes" class="mapas"></div>
    </form>
</body>
</html>
