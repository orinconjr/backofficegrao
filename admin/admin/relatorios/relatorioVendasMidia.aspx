﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="relatorioVendasMidia.aspx.cs" Inherits="admin_relatorios_relatorioVendasMidia" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Relátorios</title>
    <script src="../js/jquery.min.js"></script>
    <script src="../js/highcharts.js"></script>
    <script src="../js/exporting.js"></script>
    <script src="../js/pubsub.js"></script>
    <script src="../js/jquery.mask.min.js"></script>
    <link rel="stylesheet" href="../css/vendasDoDia.css" type="text/css" />

    <!--Load the AJAX API-->
    <script type="text/javascript">

        $('#data').mask('00/00/0000');

        ////////////////////////////////////////////////////////////////////////////
        /////////////////// Criando as datas que irão exibir no gráfico inicial
        ////////////////////////////////////////////////////////////////////////////

        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1;
        var yyyy = today.getFullYear();

        var dataHojeCompleta = dd + "/" + mm + "/" + yyyy;
        var dataOntemCompleta = (dd - 1) + "/" + mm + "/" + yyyy;
        //var dataQuarta = (dd - 2) + "/" + mm + "/" + yyyy;
        //var dataTerca = (dd - 3) + "/" + mm + "/" + yyyy;
        //var dataSegunda = (dd - 4) + "/" + mm + "/" + yyyy;

        ////////////////////////////////////////////////////////////////////////////
        /////////////////// Formatar data de hoje para comparação
        ////////////////////////////////////////////////////////////////////////////

        var splitHoje = dataHojeCompleta.split("/");

        if (splitHoje[0] < 10) {
            splitHoje[0] = "0" + splitHoje[0];
        }

        if (splitHoje[1] < 10) {
            splitHoje[1] = "0" + splitHoje[1];
        }

        var newDateHoje = splitHoje[0] + "/" + splitHoje[1] + "/" + splitHoje[2];

        ////////////////////////////////////////////////////////////////////////////
        /////////////////// Set dates in array
        ////////////////////////////////////////////////////////////////////////////

        var date = [newDateHoje, dataOntemCompleta];
        var dateName = ["Hoje", "Ontem"];
        //var dateName = ["Hoje", "Ontem", "Quarta", "Terça", "Segunda"];

        ////////////////////////////////////////////////////////////////////////////
        /////////////////// Functions
        ////////////////////////////////////////////////////////////////////////////

        Number.prototype.formatMoney = function (c, d, t) {
            var n = this, c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "," : d, t = t == undefined ? "." : t, s = n < 0 ? "-" : "", i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
            return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
        };

        Array.prototype.max = function () {
            return Math.max.apply(null, this);
        };

        var isMobile = {
            Android: function () {
                return navigator.userAgent.match(/Android/i);
            },
            BlackBerry: function () {
                return navigator.userAgent.match(/BlackBerry/i);
            },
            iOS: function () {
                return navigator.userAgent.match(/iPhone|iPod/i);
            },
            Opera: function () {
                return navigator.userAgent.match(/Opera Mini/i);
            },
            Windows: function () {
                return navigator.userAgent.match(/IEMobile/i);
            },
            any: function () {
                return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
            }
        };

        function GraficoType() {
            width = $(window).width();

            if (isMobile.any()) {
                $("#relatorioTotal").hide();
                $("#relatorioTotalBarLegenda").removeClass("column");
                $("#relatorioTotalBar").show();
            } else {
                $("#relatorioTotal").show();
                $("#relatorioTotalBarLegenda").addClass("column");
                $("#relatorioTotalBar").hide();
            }
        }

        ////////////////////////////////////////////////////////////////////////////
        /////////////////// Cria variaveis
        ////////////////////////////////////////////////////////////////////////////

        // Arrays
        var midias = new Array();
        var midiasHora = new Array();

        var midiasBC = new Array();
            midiasBC["pedidos"] = new Array(34);
            midiasBC["pedidosColor"] = new Array(34);
            midiasBC["pedidosHoje"] = new Array(34);

            midiasBC["sessoes"] = new Array(34);
            midiasBC["sessoesColor"] = new Array(34);
            midiasBC["sessoesHoje"] = new Array(34);

        var midiasHoraBC = new Array();
            midiasHoraBC["pedidos"] = new Array(34);
            midiasHoraBC["sessoes"] = new Array(34);
            midiasHoraBC["taxa"] = new Array(34);
            midiasHoraBC["taxaDia"] = new Array(34);

        var allMidias = new Array();
        var selectedMidias = new Array();

        var ts = new Date().getTime();
        var horaDia = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23];
        var optionsBar;
        var optionsColumn;
        var countMidias = 0;
        var checkRequest = 0;
        var checkRequestHour = 0;
        var fullDay = true;
        
        ////////////////////////////////////////////////////////////////////////////
        /////////////////// Cria Vetor
        ////////////////////////////////////////////////////////////////////////////

        var GraficoObject = {
            init: function (dates) {

                ////////////////////////////////////////////////////////////////////////////
                /////////////////// Reset values
                ////////////////////////////////////////////////////////////////////////////

                checkRequest = 0;
                checkRequestHour = 0;
                GraficoObject.resetArrays(dates.length);

                ////////////////////////////////////////////////////////////////////////////
                /////////////////// Create checkbox for each midia
                ////////////////////////////////////////////////////////////////////////////

                var enderecoTotal = "../relatorioServices/analyticsVisitas.aspx?listamidias=1";

                GraficoObject.requestMidia(enderecoTotal, dates);
            },
            initRequests: function(dates){
                ////////////////////////////////////////////////////////////////////////////
                /////////////////// Create graphic of hour for each day's
                ////////////////////////////////////////////////////////////////////////////

                var html = "";

                for (var i = 1; i < date.length; i++) {
                    html += "<p class='titleComparacao'>" + dateName[i] + "</p>";
                    html += "<div id='relatorio" + dateName[i] + "Taxa' class='mapas horaTotal'></div>";
                    html += "<div id='relatorio" + dateName[i] + "Sessao' class='mapas hora'></div>";
                    html += "<div id='relatorio" + dateName[i] + "Pedidos' class='mapas hora'></div>";
                }

                $(".horarios").html(html);

                ////////////////////////////////////////////////////////////////////////////
                /////////////////// Create graphics of conversion for each media
                ////////////////////////////////////////////////////////////////////////////

                var htmlMidias = "";

                for (var i = 0; i < allMidias.length; i++) {
                    htmlMidias += "<div id='relatorio" + allMidias[i] + "Midia' class='mapas midiaGraphic'></div>";
                }

                $(".graficoMidias").html(htmlMidias);

                ////////////////////////////////////////////////////////////////////////////
                /////////////////// Request's - total
                ////////////////////////////////////////////////////////////////////////////

                for (var d = 0; d < dates.length; d++) {
                    var ts = new Date().getTime();
                    var dateSplit = dates[d].split("/");
                    var enderecoTotal;

                    if (fullDay) {
                        enderecoTotal = "../relatorioServices/analyticsVisitas.aspx?midiasdia=1&dia=" + dateSplit[0] + "&mes=" + dateSplit[1] + "&ano=" + dateSplit[2];
                    } else {
                        enderecoTotal = "../relatorioServices/analyticsVisitas.aspx?midiasdia=1&dia=" + dateSplit[0] + "&mes=" + dateSplit[1] + "&ano=" + dateSplit[2] + "&hora=25";
                    }

                    console.log(enderecoTotal);

                    GraficoObject.requestObject(enderecoTotal, d, dates.length);
                }

                ////////////////////////////////////////////////////////////////////////////
                /////////////////// Request's - hour
                ////////////////////////////////////////////////////////////////////////////

                for (var d = 0; d < dates.length; d++) {
                    var ts = new Date().getTime();
                    var dateSplit = dates[d].split("/");
                    var enderecoTotal = "../relatorioServices/analyticsVisitas.aspx?midiasporhora=1&dia=" + dateSplit[0] + "&mes=" + dateSplit[1] + "&ano=" + dateSplit[2] + "&hora=25";

                    GraficoObject.requestObjectHora(enderecoTotal, d);
                }
            },
            resetArrays: function(day){
                for (var d = 0; d < day; d++) {
                    for (var i = 0; i < midias.length; i++) {
                        midias[i]["pedidosOrigem"][d] = 0;
                        midias[i]["pedidosUltima"][d] = 0;
                        midias[i]["sessoes"][d] = 0;
                    }

                    for (var i = 0; i < midiasHora.length; i++) {
                        for (var j = 0; j < midiasHora[i]["pedidosOrigem"][d].length; j++) {
                            midiasHora[i]["pedidosOrigem"][d][objects[j].hora] = 0;
                            midiasHora[i]["pedidosUltima"][d][objects[j].hora] = 0;
                            midiasHora[i]["sessoes"][d][objects[j].hora] = 0;
                            midiasHora[i]["taxa"][d][objects[j].hora] = 0;
                        }
                    }

                    midiasBC["pedidos"][d] = 0;
                    midiasBC["sessoes"][d] = 0;
                }
            },
            createArrays: function(d, obj){
                for (var i = 0; i < obj.length; i++) {

                    if (typeof (midias[obj[i].origem]) == "undefined") {
                        midias[obj[i].origem] = new Array();
                    }

                    if (typeof (midias[obj[i].origem]["pedidosOrigem"]) == "undefined") {
                        midias[obj[i].origem]["pedidosOrigem"] = new Array(obj.length);
                    }

                    if (typeof (midias[obj[i].origem]["pedidosUltima"]) == "undefined") {
                        midias[obj[i].origem]["pedidosUltima"] = new Array(obj.length);
                    }

                    if (typeof (midias[obj[i].origem]["sessoes"]) == "undefined") {
                        midias[obj[i].origem]["sessoes"] = new Array(obj.length);
                    }

                    if (typeof (midiasHora[obj[i].origem]) == "undefined") {
                        midiasHora[obj[i].origem] = new Array();
                    }

                    if (typeof (midiasHora[obj[i].origem]["pedidosOrigem"]) == "undefined") {
                        midiasHora[obj[i].origem]["pedidosOrigem"] = new Array(obj.length);
                    }

                    if (typeof (midiasHora[obj[i].origem]["pedidosUltima"]) == "undefined") {
                        midiasHora[obj[i].origem]["pedidosUltima"] = new Array(obj.length);
                    }
                    
                    if (typeof (midiasHora[obj[i].origem]["sessoes"]) == "undefined") {
                        midiasHora[obj[i].origem]["sessoes"] = new Array(obj.length);
                    }

                    if (typeof (midiasHora[obj[i].origem]["taxaOrigem"]) == "undefined") {
                        midiasHora[obj[i].origem]["taxaOrigem"] = new Array(obj.length);
                    }

                    if (typeof (midiasHora[obj[i].origem]["taxaUltima"]) == "undefined") {
                        midiasHora[obj[i].origem]["taxaUltima"] = new Array(obj.length);
                    }

                    for (var j = 0; j < d.length; j++) {
                        if (typeof (midiasHora[obj[i].origem]["pedidosUltima"][j]) == "undefined") {
                            midiasHora[obj[i].origem]["pedidosUltima"][j] = new Array(d.length);
                        }

                        if (typeof (midiasHora[obj[i].origem]["pedidosOrigem"][j]) == "undefined") {
                            midiasHora[obj[i].origem]["pedidosOrigem"][j] = new Array(d.length);
                        }

                        if (typeof (midiasHora[obj[i].origem]["sessoes"][j]) == "undefined") {
                            midiasHora[obj[i].origem]["sessoes"][j] = new Array(d.length);
                        }

                        if (typeof (midiasHora[obj[i].origem]["taxaOrigem"][j]) == "undefined") {
                            midiasHora[obj[i].origem]["taxaOrigem"][j] = new Array(d.length);
                        }

                        if (typeof (midiasHora[obj[i].origem]["taxaUltima"][j]) == "undefined") {
                            midiasHora[obj[i].origem]["taxaUltima"][j] = new Array(d.length);
                        }

                        for (var k = 0; k < 24; k++) {
                            if (typeof (midiasHora[obj[i].origem]["pedidosUltima"][j][k]) == "undefined") {
                                midiasHora[obj[i].origem]["pedidosUltima"][j][k] = 0;
                            }

                            if (typeof (midiasHora[obj[i].origem]["pedidosOrigem"][j][k]) == "undefined") {
                                midiasHora[obj[i].origem]["pedidosOrigem"][j][k] = 0;
                            }

                            if (typeof (midiasHora[obj[i].origem]["sessoes"][j][k]) == "undefined") {
                                midiasHora[obj[i].origem]["sessoes"][j][k] = 0;
                            }

                            if (typeof (midiasHora[obj[i].origem]["taxaOrigem"][j][k]) == "undefined") {
                                midiasHora[obj[i].origem]["taxaOrigem"][j][k] = 0;
                            }

                            if (typeof (midiasHora[obj[i].origem]["taxaUltima"][j][k]) == "undefined") {
                                midiasHora[obj[i].origem]["taxaUltima"][j][k] = 0;
                            }
                        }
                    }
                }
            },
            requestObject: function (get, day, dateCount) {
                $.get(get,
                {
                    nocache: ts
                }, function (data) {
                    var objects = data.visits;
                    console.log("Total Object");
                    console.log(objects);

                    /////////////////////////////////////////////////
                    //////////////// INSERT
                    /////////////////////////////////////////////////

                    GraficoObject.createArrays(day, objects);

                    for (var i = 0; i < objects.length; i++) {
                        if (typeof (objects[i].pedidosorigemsessao) != "undefined") {
                            midias[objects[i].origem]["pedidosOrigem"][day] = objects[i].pedidosorigemsessao;
                        } else {
                            midias[objects[i].origem]["pedidosOrigem"][day] = 0;
                        }

                        if (typeof (objects[i].pedidosultimasessao) != "undefined") {
                            midias[objects[i].origem]["pedidosUltima"][day] = objects[i].pedidosultimasessao;
                        } else {
                            midias[objects[i].origem]["pedidosUltima"][day] = 0;
                        }

                        if (typeof (objects[i].sessoes) != "undefined") {
                            midias[objects[i].origem]["sessoes"][day] = objects[i].sessoes;
                        } else {
                            midias[objects[i].origem]["sessoes"][day] = 0;
                        }
                    }

                }).fail(function (data) {
                    GraficoObject.consoleLog("error", data);
                }).success(function (data) {
                    if (checkRequest == (date.length - 1)) {
                        GraficoObject.loadBarColumn();
                    }

                    checkRequest++;
                });
            },
            requestObjectHora: function (get, day) {
                $.get(get,
                {
                    nocache: ts
                }, function (data) {
                    var objects = data.visits;
                    console.log("Hora Object" + day);
                    console.log(objects);

                    /////////////////////////////////////////////////
                    //////////////// INSERT
                    /////////////////////////////////////////////////

                    GraficoObject.createArrays(day, objects);

                    for (var i = 0; i < objects.length; i++) {
                        if (typeof (objects[i].pedidosorigemsessao) != "undefined") {

                            //console.log("----------------------------------------------------------------");
                            //console.log(midiasHora);
                            //console.log("day" + day);
                            //console.log("i" + i);
                            //console.log("objects[i].origem" + objects[i].origem);
                            //console.log("objects[i].hora" + objects[i].hora);
                            //console.log(objects[i].pedidosorigemsessao);
                            //console.log(midiasHora[objects[i].origem]);
                            //console.log(midiasHora[objects[i].origem]["pedidosOrigem"]);
                            //console.log(midiasHora[objects[i].origem]["pedidosOrigem"][day]);
                            //console.log(midiasHora[objects[i].origem]["pedidosOrigem"][day][objects[i].hora]);
                            
                            midiasHora[objects[i].origem]["pedidosOrigem"][day][objects[i].hora] = objects[i].pedidosorigemsessao;

                            //console.log(midiasHora[objects[i].origem]["pedidosOrigem"][day][objects[i].hora]);
                        } else {
                            midiasHora[objects[i].origem]["pedidosOrigem"][day][objects[i].hora] = 0;
                        }

                        if (typeof (objects[i].pedidosultimasessao) != "undefined") {
                            midiasHora[objects[i].origem]["pedidosUltima"][day][objects[i].hora] = objects[i].pedidosultimasessao;
                        } else {
                            midiasHora[objects[i].origem]["pedidosUltima"][day][objects[i].hora] = 0;
                        }

                        if (typeof (objects[i].sessoes) != "undefined") {
                            midiasHora[objects[i].origem]["sessoes"][day][objects[i].hora] = objects[i].sessoes;
                        } else {
                            midiasHora[objects[i].origem]["sessoes"][day][objects[i].hora] = 0;
                        }

                        if (typeof (objects[i].sessoes) != "undefined") {
                            midiasHora[objects[i].origem]["taxaOrigem"][day][objects[i].hora] = (objects[i].pedidosorigemsessao * 100) / objects[i].sessoes;
                            midiasHora[objects[i].origem]["taxaUltima"][day][objects[i].hora] = (objects[i].pedidosultimasessao * 100) / objects[i].sessoes;
                        } else {
                            midiasHora[objects[i].origem]["taxaOrigem"][day][objects[i].hora] = 0;
                            midiasHora[objects[i].origem]["taxaUltima"][day][objects[i].hora] = 0;
                        }
                    }

                }).fail(function (data) {
                    GraficoObject.consoleLog("error", data);
                }).success(function (data) {
                    if (checkRequestHour == (date.length - 1)) {
                        GraficoObject.loadHour();
                    }

                    checkRequestHour++;
                });
            },
            requestMidia: function (get, day) {
                $.get(get,
                {
                    nocache: ts
                }, function (data) {
                    var objects = data.visits;
                    var html = "";

                    for (var i = 0; i < objects.length; i++) {
                        if ($(".modalMidias .checkboxs").html() == ""){
                            html += "<label>";
                            html += "<input type='checkbox' name='midias' id='" + objects[i].origem + "' checked='true' />";
                            html += "<span>" + objects[i].origem + "</span>";
                            html += "</label>";

                            countMidias++;
                            allMidias.push(objects[i].origem);
                            selectedMidias.push(objects[i].origem);
                        }
                    }

                    GraficoObject.createArrays(day, objects);

                    if ($(".modalMidias .checkboxs").html() == "") {
                        $(".modalMidias .checkboxs").html(html);
                    }
                }).fail(function (data) {
                    GraficoObject.consoleLog("error", data);
                }).success(function (data) {
                    GraficoObject.initRequests(day);
                });
            },
            loadBarColumn: function () {
                var totalSesssoes = 0;
                var totalPedidosOrigem = 0;
                var totalPedidosUltima = 0;
                
                console.log("TESTE");
                console.log(midias);

                for (var d = 0; d < date.length; d++) {
                    totalSesssoes = 0;
                    totalPedidosOrigem = 0;
                    totalPedidosUltima = 0;

                    for (var j = 0; j < countMidias; j++) {
                        //console.log(selectedMidias.indexOf(allMidias[j]) > -1);

                        if (selectedMidias.indexOf(allMidias[j]) > -1) {
                            if (typeof (midias[allMidias[j]]["sessoes"][d]) != "undefined") {
                                totalSesssoes = totalSesssoes + midias[allMidias[j]]["sessoes"][d];
                            }

                            if (typeof (midias[allMidias[j]]["pedidosOrigem"][d]) != "undefined") {
                                totalPedidosOrigem = totalPedidosOrigem + midias[allMidias[j]]["pedidosOrigem"][d];
                                //console.log("totalPedidosOrigem" + totalPedidosOrigem);
                            }

                            if (typeof (midias[allMidias[j]]["pedidosUltima"][d]) != "undefined") {
                                totalPedidosUltima = totalPedidosUltima + midias[allMidias[j]]["pedidosUltima"][d];
                                //console.log("totalPedidosUltima" + totalPedidosUltima);
                            }
                        }
                    }

                    var pedidos = 0;

                    if ($("input:radio[name=tipoSessao]:checked").val() == "origem") {
                        pedidos = totalPedidosOrigem;
                    } else {
                        pedidos = totalPedidosUltima;
                    }

                    midiasBC["pedidos"][d] = pedidos;
                    midiasBC["sessoes"][d] = totalSesssoes;

                    if (d == 0) {
                        for (var k = 0; k < date.length; k++) {
                            midiasBC["pedidosHoje"][k] = pedidos;
                            midiasBC["sessoesHoje"][k] = totalSesssoes;
                        }
                    }
                }

                console.log("TESTE2");
                console.log(midiasBC);

                ////////////////////////////////////
                ///////// CONSTUCT MIDIAS
                ////////////////////////////////////
                
                var sessionHighest = 0
                var pedidoHighest = 0;

                for (var i = 0; i < date.length; i++) {
                    if (midiasBC["pedidos"][i] > pedidoHighest) {
                        pedidoHighest = midiasBC["pedidos"][i];
                    }

                    if (midiasBC["sessoes"][i] > sessionHighest) {
                        sessionHighest = midiasBC["sessoes"][i];
                    }

                    if (midiasBC["pedidos"][i] > midiasBC["pedidos"][0]) {
                        midiasBC["pedidosColor"][i] = "#AE0000";
                    } else if (midiasBC["pedidos"][i] < midiasBC["pedidos"][0]) {
                        midiasBC["pedidosColor"][i] = "#204000";
                    } else {
                        midiasBC["pedidosColor"][i] = "#000097";
                    }

                    if (midiasBC["sessoes"][i] > midiasBC["sessoes"][0]) {
                        midiasBC["sessoesColor"][i] = "#FF6262";
                    } else if (midiasBC["sessoes"][i] < midiasBC["sessoes"][0]) {
                        midiasBC["sessoesColor"][i] = "#73E600";
                    } else {
                        midiasBC["sessoesColor"][i] = "#2828FF";
                    }
                }

                setTimeout(function () {
                    GraficoObject.constructColuna(midiasBC, "Total", sessionHighest, pedidoHighest);
                    GraficoObject.constructBarra(midiasBC, "Total", sessionHighest, pedidoHighest);

                    $("#relatorioTotal").highcharts(optionsColumn);
                    $("#relatorioTotalBar").highcharts(optionsBar);

                    $(".loading").hide();
                }, 1000);

                // HOVER FALSO PARA ARRUMAR UM BUG DA COR DEFAULT DO GRÁFICO -- FICA PRETO SEM ISSO E SÓ FUNCIONA AS CORES CERTAS NO HOVER
                $("#relatorioTotal *, #relatorioTotalBar *").trigger('mouseover');
            },
            loadHour: function () {
                for (var d = 0; d < date.length; d++) {
                    var totalPedidosOrigem = 0;
                    var totalPedidosUltima = 0;
                    var pedidosTotalDiaOrigem = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
                    var pedidosTotalDiaUltima = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

                    var totalSessao = 0;
                    var sessaoTotalDia = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

                    var taxaTotalDiaOrigem = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
                    var taxaTotalDiaUltima = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

                    for (var j = 0; j < countMidias; j++) {
                        if (selectedMidias.indexOf(allMidias[j]) > -1) {
                            for (var k = 0; k < midiasHora[allMidias[j]]["sessoes"][d].length; k++) {

                                if (typeof (midiasHora[allMidias[j]]["sessoes"][d][k]) != "undefined") {
                                    sessaoTotalDia[k] = sessaoTotalDia[k] + midiasHora[allMidias[j]]["sessoes"][d][k];
                                    totalSessao = totalSessao + midiasHora[allMidias[j]]["sessoes"][d][k];
                                }

                                if (typeof (midiasHora[allMidias[j]]["pedidosOrigem"][d][k]) != "undefined") {
                                    pedidosTotalDiaOrigem[k] = pedidosTotalDiaOrigem[k] + midiasHora[allMidias[j]]["pedidosOrigem"][d][k];
                                    totalPedidosOrigem = totalPedidosOrigem + midiasHora[allMidias[j]]["pedidosOrigem"][d][k];
                                }

                                if (typeof (midiasHora[allMidias[j]]["pedidosUltima"][d][k]) != "undefined") {
                                    pedidosTotalDiaUltima[k] = pedidosTotalDiaUltima[k] + midiasHora[allMidias[j]]["pedidosUltima"][d][k];
                                    totalPedidosUltima = totalPedidosUltima + midiasHora[allMidias[j]]["pedidosUltima"][d][k];
                                }

                                if (typeof (midiasHora[allMidias[j]]["taxaOrigem"][d][k]) != "undefined") {
                                    taxaTotalDiaOrigem[k] = taxaTotalDiaOrigem[k] + midiasHora[allMidias[j]]["taxaOrigem"][d][k];
                                }

                                if (typeof (midiasHora[allMidias[j]]["taxaUltima"][d][k]) != "undefined") {
                                    taxaTotalDiaUltima[k] = taxaTotalDiaUltima[k] + midiasHora[allMidias[j]]["taxaUltima"][d][k];
                                }
                            }
                        }
                    }

                    var pedidos = 0;
                    var pedidosTotal = 0;
                    var taxa = 0;

                    if ($("input:radio[name=tipoSessao]:checked").val() == "origem") {
                        pedidos = pedidosTotalDiaOrigem;
                        pedidosTotal = totalPedidosOrigem;
                        taxa = taxaTotalDiaOrigem;
                    } else {
                        pedidos = pedidosTotalDiaUltima;
                        pedidosTotal = totalPedidosUltima;
                        taxa = taxaTotalDiaUltima;
                    }

                    midiasHoraBC["pedidos"][d] = pedidos;
                    midiasHoraBC["sessoes"][d] = sessaoTotalDia;
                    midiasHoraBC["taxa"][d] = taxa;
                    midiasHoraBC["taxaDia"][d] = (pedidosTotal * 100) / totalSessao;
                }

                ////////////////////////////////////
                ///////// CONSTUCT HOURS BY MIDIA
                ////////////////////////////////////

                var pedidoHighestHora = 0;
                var sessionHighestHora = 0;
                var taxaHighestHora = 0;
				
                ////////////////////////////////////
                ///////// Pedidos Highest Hoje
                ////////////////////////////////////

                for (var i = 0; i < midiasHoraBC["pedidos"][0].length; i++) {
                    if (midiasHoraBC["pedidos"][0][i] > pedidoHighestHora) {
                        pedidoHighestHora = midiasHoraBC["pedidos"][0][i];
                    }
					
                    if (midiasHoraBC["sessoes"][0][i] > sessionHighestHora) {
                        sessionHighestHora = midiasHoraBC["sessoes"][0][i];
                    }

                    if (midiasHoraBC["taxa"][0][i] > taxaHighestHora) {
                        taxaHighestHora = midiasHoraBC["taxa"][0][i];
                    }
                }
				
                ////////////////////////////////////
                ///////// Send to construct
                ////////////////////////////////////
				
                for (var i = 1; i < date.length; i++) {
                    for (var j = 0; j < midiasHoraBC["pedidos"][i].length; j++) {
                        if (midiasHoraBC["pedidos"][i][j] > pedidoHighestHora) {
                            pedidoHighestHora = midiasHoraBC["pedidos"][i][j];
                        }

                        if (midiasHoraBC["sessoes"][i][j] > sessionHighestHora) {
                            sessionHighestHora = midiasHoraBC["sessoes"][i][j];
                        }

                        if (midiasHoraBC["taxa"][i][j] > taxaHighestHora) {
                            taxaHighestHora = midiasHoraBC["taxa"][i][j];
                        }
                    }

                    GraficoObject.constructHora(
                        midiasHoraBC["pedidos"][0],
                        midiasHoraBC["sessoes"][0],
                        midiasHoraBC["taxa"][0],
                        midiasHoraBC["taxaDia"][0],
                        midiasHoraBC["pedidos"][i],
                        midiasHoraBC["sessoes"][i],
                        midiasHoraBC["taxa"][i],
                        midiasHoraBC["taxaDia"][i],
                        dateName[0],
                        dateName[i],
                        sessionHighestHora,
                        pedidoHighestHora,
                        taxaHighestHora
                    );
                }

                for (var j = 0; j < countMidias; j++) {
                    GraficoObject.constructMidias(allMidias[j]);
                }
            },
            constructColuna: function (objetoMidia, name, sessionHigh, pedidoHigh) {
                optionsColumn = {
                    chart: {
                        "type": "column"
                    },
                    title: {
                        text: 'Relátorio de Midia - ' + name
                    },
                    xAxis: {
                        categories: dateName
                    },
                    yAxis: [{
                        title: {
                            text: 'Sessões'
                        },
                        min: 0,
                        max: sessionHigh,
                        tickInterval: 1,
                        endOnTick: false
                    }, {
                        title: {
                            text: 'Pedidos'
                        },
                        min: 0,
                        tickInterval: 1,
                        max: pedidoHigh,
                        opposite: true,
                        endOnTick: false
                    }],
                    legend: {
                        shadow: false
                    },
                    tooltip: {
                        shared: true
                    },
                    plotOptions: {
                        column: {
                            grouping: false,
                            borderWidth: 0,
                            colorByPoint: true,
                            dataLabels: {
                                enabled: true,
                                shape: 'callout',
                                color: 'white',
                                style: {
                                    fontSize: '10px',
                                    textShadow: '0 0 6px black, 0 0 3px black'
                                },
                            }
                        }
                    },
                    series: [
                        {
                            name: "Sessões",
                            data: objetoMidia["sessoes"],
                            colors: objetoMidia["sessoesColor"],
                            pointPadding: 0,
                            pointPadding: 0,
                            pointPlacement: 0,
                            dataLabels: {
                                verticalAlign: 'bottom',
                                formatter: function () {
                                    var cal = ((objetoMidia["sessoes"][0] * 100 / this.y) - 100).toFixed(2);

                                    if (cal == 0) {
                                        return Math.ceil(this.y);
                                    } else {
                                        return Math.ceil(this.y) + " | " + cal + "%";
                                    }
                                }
                            }
                        }, {
                            name: "Pedidos",
                            data: objetoMidia["pedidos"],
                            colors: objetoMidia["pedidosColor"],
                            pointPadding: 0.2,
                            pointPlacement: 0,
                            yAxis: 1,
                            dataLabels: {
                                verticalAlign: 'bottom',
                                formatter: function () {
                                    var cal = ((objetoMidia["pedidos"][0] * 100 / this.y) - 100).toFixed(2);

                                    if (cal == 0) {
                                        return Math.ceil(this.y);
                                    } else {
                                        return Math.ceil(this.y) + " | " + cal + "%";
                                    }
                                }
                            }   
                        }, {
                            type: 'line',
                            name: 'Pedidos - Hoje',
                            color: '#2693FF',
                            data: objetoMidia["pedidosHoje"],
                            pointPadding: 0.2,
                            pointPlacement: 0,
                            yAxis: 1
                        }, {
                            type: 'line',
                            name: 'Sessões - Hoje',
                            color: '#003040',
                            data: objetoMidia["sessoesHoje"],
                            pointPadding: 0.3,
                            pointPlacement: 0
                        }
                    ]
                }
            },
            constructBarra: function (objetoMidia, name, sessionHigh, pedidoHigh) {
                optionsBar = {
                    chart: {
                        "type": "bar"
                    },
                    title: {
                        text: 'Relátorio de Midia - ' + name
                    },
                    xAxis: {
                        categories: dateName
                    },
                    yAxis: [{
                        title: {
                            text: 'Sessões'
                        },
                        min: 0,
                        max: sessionHigh,
                        tickInterval: 1,
                        endOnTick: false
                    }, {
                        title: {
                            text: 'Pedidos'
                        },
                        min: 0,
                        tickInterval: 1,
                        max: pedidoHigh,
                        opposite: true,
                        endOnTick: false
                    }],
                    legend: {
                        shadow: false
                    },
                    tooltip: {
                        shared: true
                    },
                    plotOptions: {
                        column: {
                            grouping: false,
                            borderWidth: 0,
                            colorByPoint: true,
                            dataLabels: {
                                enabled: true,
                                shape: 'callout',
                                color: 'white',
                                style: {
                                    fontSize: '10px',
                                    textShadow: '0 0 6px black, 0 0 3px black'
                                },
                            }
                        }
                    },
                    series: [
                        {
                            name: "Sessões",
                            data: objetoMidia["sessoes"],
                            colors: objetoMidia["sessoesColor"],
                            pointPadding: 0,
                            pointPadding: 0,
                            pointPlacement: 0
                        }, {
                            name: "Pedidos",
                            data: objetoMidia["pedidos"],
                            colors: objetoMidia["pedidosColor"],
                            pointPadding: 0.2,
                            pointPlacement: 0,
                            yAxis: 1
                        }, {
                            type: 'line',
                            name: 'Pedidos - Hoje',
                            color: '#2693FF',
                            data: objetoMidia["pedidosHoje"],
                            pointPadding: 0.2,
                            pointPlacement: 0,
                            yAxis: 1
                        }, {
                            type: 'line',
                            name: 'Sessões - Hoje',
                            color: '#003040',
                            data: objetoMidia["sessoesHoje"],
                            pointPadding: 0.3,
                            pointPlacement: 0
                        }
                    ]

                    //tooltip: {
                    //    shared: true,
                    //    headerFormat: '<span style="font-size: 10px;text-transform:uppercase;">{point.key}</span><br/>',
                    //    pointFormat: '{series.name}: <b>{point.y}</b><br/>'
                    //},
                    //plotOptions: {
                    //    bar: {
                    //        grouping: false,
                    //        borderWidth: 0,
                    //        colorByPoint: true,
                    //        dataLabels: {
                    //            enabled: true,
                    //            inside: true,
                    //            shape: 'callout',
                    //            color: 'white',
                    //            allowOverlap: true,
                    //            align: 'left',
                    //            padding: 0,
                    //            style: {
                    //                fontSize: '10px',
                    //                textShadow: '0 0 6px black, 0 0 3px black'
                    //            },
                    //            x: 10
                    //        }
                    //    }
                    //},
                    
                }
            },
            constructHora: function (objMidiaPedidos, objMidiaSessoes, objMidiaTaxa, objMidiaTaxaDiaTotal, objMidiaPedidosComparado, objMidiaSessoesComparado, objMidiaTaxaComparado, objMidiaTaxaDiaTotalComparado, name, nameComparado, sessionHigh, pedidoHigh, taxaHigh ) {

                //console.log("objMidiaPedidos" + objMidiaPedidos);
                //console.log("objMidiaSessoes" + objMidiaSessoes);
                //console.log("objMidiaTaxa" + objMidiaTaxa);
                //console.log("objMidiaTaxaDiaTotal");
                //console.log(objMidiaTaxaDiaTotal);
                //console.log("objMidiaPedidosComparado" + objMidiaPedidosComparado);
                //console.log("objMidiaSessoesComparado" + objMidiaSessoesComparado);
                //console.log("objMidiaTaxaComparado" + objMidiaTaxaComparado);
                //console.log("objMidiaTaxaDiaTotalComparado");
                //console.log(objMidiaTaxaDiaTotalComparado);
                //console.log("name" + name);
                //console.log("nameComparado" + nameComparado);
                //console.log("sessionHigh" + sessionHigh);
                //console.log("pedidoHigh" + pedidoHigh);
                //console.log("taxaHigh" + taxaHigh);


                $('#relatorio' + nameComparado + "Taxa").highcharts({
                    chart: {
                        type: 'area'
                    },
                    title: {
                        text: 'Relátorio - Taxa de conversão'
                    },
                    xAxis: {
                        categories: horaDia
                    },
                    yAxis: [
                    {
                        title: {
                            text: 'Taxa'
                        },
                        min: 0,
                        tickInterval: 1,
                        max: taxaHigh,
                        opposite: true,
                        endOnTick: false
                    }],
                    subtitle: {
                        align: 'center',
                        text: "Hoje/" + nameComparado + " - " + objMidiaTaxaDiaTotal.toFixed(4) + "/" + objMidiaTaxaDiaTotalComparado.toFixed(4),
                        useHTML: true
                    },
                    plotOptions: {
                        area: {
                            marker: {
                                enabled: false,
                                symbol: 'circle',
                                radius: 2,
                                states: {
                                    hover: {
                                        enabled: true
                                    }
                                }
                            }
                        }
                    },
                    series: [
                        {
                            type: 'area',
                            name: nameComparado,
                            data: objMidiaTaxaComparado,
                            pointPadding: 0,
                            pointPlacement: 0,
                            dataLabels: {
                                enabled: true,
                                inside: true,
                                shape: 'callout',
                                color: 'white',
                                allowOverlap: true,
                                align: 'left',
                                padding: 0,
                                useHTML: true,
                                zIndex: 999999,
                                formatter: function () {
                                    var css = (this.y == 0) ? "blue" : (this.y > objMidiaTaxa[this.x]) ? "green" : "red";

                                    return "<span style='color: " + css + "'>" + this.y.toFixed(3) + "</span>";
                                },
                                y: -10,
                                x: -15
                            }
                        }, {
                            type: 'area',
                            name: 'Hoje',
                            data: objMidiaTaxa,
                            pointPadding: 0,
                            pointPlacement: 0,
                            dataLabels: {
                                enabled: true,
                                inside: true,
                                shape: 'callout',
                                color: 'white',
                                allowOverlap: true,
                                align: 'left',
                                padding: 0,
                                useHTML: true,
                                zIndex: 999999,
                                formatter: function () {
                                    var css = (this.y == 0) ? "blue" : (this.y > objMidiaTaxaComparado[this.x]) ? "green" : "red";

                                    return "<span style='color: " + css + "'>" + this.y.toFixed(3) + "</span>";
                                },
                                x: -15
                            }
                        }
                    ]
                });

                $('#relatorio' + nameComparado + "Pedidos").highcharts({
                    chart: {
                        type: 'area'
                    },
                    title: {
                        text: 'Relátorio - Pedidos'
                    },
                    xAxis: {
                        categories: horaDia
                    },
                    yAxis: [
                    {
                        title: {
                            text: 'Pedidos'
                        },
                        min: 0,
                        tickInterval: 1,
                        max: pedidoHigh,
                        opposite: true,
                        endOnTick: false
                    }],
                    subtitle: {
                        align: 'center',
                        text: "Hoje/" + nameComparado,
                        useHTML: true
                    },
                    plotOptions: {
                        area: {
                            marker: {
                                enabled: false,
                                symbol: 'circle',
                                radius: 2,
                                states: {
                                    hover: {
                                        enabled: true
                                    }
                                }
                            }
                        }
                    },
                    series: [
                        {
                            type: 'area',
                            name: nameComparado,
                            data: objMidiaPedidosComparado,
                            pointPadding: 0,
                            pointPlacement: 0
                        }, {
                            type: 'area',
                            name: 'Hoje',
                            data: objMidiaPedidos,
                            pointPadding: 0,
                            pointPlacement: 0,
                            dataLabels: {
                                enabled: true,
                                inside: true,
                                shape: 'callout',
                                color: 'white',
                                allowOverlap: true,
                                align: 'left',
                                padding: 0,
                                useHTML: true,
                                zIndex: 999999,
                                formatter: function () {
                                    if (objMidiaPedidosComparado[this.x] != 0 && this.y != 0) {
                                        var porcentagemHora = (this.y * 100 / objMidiaPedidosComparado[this.x]) - 100;
                                        var css = (porcentagemHora == 0) ? "blue" : (porcentagemHora > 0) ? "green" : "red";
                                        var prefix;

                                        if (porcentagemHora.toFixed(2) < 0 || porcentagemHora.toFixed(2) == 0) {
                                            prefix = "";
                                        } else {
                                            prefix = "+";
                                        }

                                        return "<span style='color: " + css + "'>" + prefix + " " + porcentagemHora.toFixed(2) + "%</span>";
                                    } else {
                                        return "<span style='color: green;margin-left: 2px;'>+ 100%</span>";
                                    }
                                },
                                x: -25,
                                y: -10
                            }
                        }
                    ]
                });

                $('#relatorio' + nameComparado + "Sessao").highcharts({
                    chart: {
                        type: 'area'
                    },
                    title: {
                        text: 'Relátorio - Sessão'
                    },
                    xAxis: {
                        categories: horaDia
                    },
                    yAxis: [
                    {
                        title: {
                            text: 'Sessões'
                        },
                        min: 0,
                        max: sessionHigh,
                        tickInterval: 1,
                        endOnTick: false
                    }],
                    subtitle: {
                        align: 'center',
                        text: "Hoje/" + nameComparado,
                        useHTML: true
                    },
                    plotOptions: {
                        area: {
                            marker: {
                                enabled: false,
                                symbol: 'circle',
                                radius: 2,
                                states: {
                                    hover: {
                                        enabled: true
                                    }
                                }
                            }
                        }
                    },
                    series: [
                        {
                            type: 'area',
                            name: nameComparado,
                            data: objMidiaSessoesComparado,
                            pointPadding: 0,
                            pointPlacement: 0
                        }, {
                            type: 'area',
                            name: 'Hoje',
                            data: objMidiaSessoes,
                            pointPadding: 0,
                            pointPlacement: 0,
                            dataLabels: {
                                enabled: true,
                                inside: true,
                                shape: 'callout',
                                color: 'white',
                                allowOverlap: true,
                                align: 'left',
                                padding: 0,
                                useHTML: true,
                                zIndex: 999999,
                                formatter: function () {
                                    if (objMidiaSessoesComparado[this.x] != 0 && this.y != 0) {
                                        var porcentagemHora = (this.y * 100 / objMidiaSessoesComparado[this.x]) - 100;
                                        var css = (porcentagemHora == 0) ? "blue" : (porcentagemHora > 0) ? "green" : "red";
                                        var prefix;

                                        if (porcentagemHora.toFixed(2) < 0 || porcentagemHora.toFixed(2) == 0) {
                                            prefix = "";
                                        } else {
                                            prefix = "+";
                                        }

                                        return "<span style='color: " + css + "'>" + prefix + " " + porcentagemHora.toFixed(2) + "%</span>";
                                    } else {
                                        return "<span style='color: green;margin-left: 2px;'>+ 100%</span>";
                                    }
                                },
                                x: -25,
                                y: -10
                            }
                        }
                    ]
                });
            },
            constructMidias: function (nameMidia) {
                
                var optionsMidias = {
                    title: {
                        text: 'Relátorio - ' + nameMidia
                    },
                    xAxis: {
                        categories: horaDia
                    },
                    yAxis: [
                    {
                        title: {
                            text: 'Sessões'
                        },
                        min: 0,
                        tickInterval: 1,
                        endOnTick: false
                    }],
                    plotOptions: {
                        area: {
                            marker: {
                                enabled: false,
                                symbol: 'circle',
                                radius: 2,
                                states: {
                                    hover: {
                                        enabled: true
                                    }
                                }
                            }
                        }
                    },
                    series: []
                };
                
                function configureSerie(array, name, comparador) {
                    if (comparador) {
                        return {
                            type: 'area',
                            name: name,
                            data: array,
                            pointPadding: 0,
                            pointPlacement: 0
                        }
                    } else {
                        return {
                            name: name,
                            data: array,
                            pointPadding: 0,
                            pointPlacement: 0
                        }
                    }
                }
                var css = "blue";
                var hoje = false;
                    
                for (var d = 0; d < date.length; d++) {
                    console.log($("input:radio[name=tipoDadoMidia]:checked").val());
                    console.log($("input:radio[name=tipoSessao]:checked").val());

                    if (d == 0) {
                        hoje = true;
                    } else {
                        hoje = false;
                    }

                    if ($("input:radio[name=tipoDadoMidia]:checked").val() == "pedidos") {
                        if ($("input:radio[name=tipoSessao]:checked").val() == "origem") {
                            optionsMidias.series.push(configureSerie(midiasHora[nameMidia]["pedidosOrigem"][d], dateName[d], hoje));
                        } else {
                            optionsMidias.series.push(configureSerie(midiasHora[nameMidia]["pedidosUltima"][d], dateName[d], hoje));
                        }
                    } else if ($("input:radio[name=tipoDadoMidia]:checked").val() == "taxa") {
                        if ($("input:radio[name=tipoSessao]:checked").val() == "origem") {
                            optionsMidias.series.push(configureSerie(midiasHora[nameMidia]["taxaOrigem"][d], dateName[d], hoje));
                        } else {
                            optionsMidias.series.push(configureSerie(midiasHora[nameMidia]["taxaUltima"][d], dateName[d], hoje));
                        }
                    } else {
                        optionsMidias.series.push(configureSerie(midiasHora[nameMidia]["sessoes"][d], dateName[d], hoje));
                    }
                }

                $('#relatorio' + nameMidia + "Midia").highcharts(optionsMidias);
            },
            consoleLog: function (text, data) {
                var date = new Date;
                var hours = date.getHours();
                var minutes = date.getMinutes();
                var seconds = date.getSeconds();

                console.log("Nome:" + text + " - Hora:" + hours + ":" + minutes + ":" + seconds);
                console.log("Vetor:");
                console.log(data)
            }
        }

        ////////////////////////////////////////////////////////////////////////////
        /////////////////// RELATORIO DE VENDAS TOTAL
        ////////////////////////////////////////////////////////////////////////////

        $(document).ready(function () {
            GraficoType();

            $(window).resize(function () {
                GraficoType();
            });

            GraficoObject.init(date);

            ////////////////////////////////////////////////////////////////////////////
            /////////////////// Switch type
            ////////////////////////////////////////////////////////////////////////////

            $('#button').click(function () {
                $("#relatorioTotal").toggle();
                $("#relatorioTotalBar").toggle();

                if ($("#relatorioTotal").is(":visible")) {
                    $("#relatorioTotalBarLegenda").addClass("column");
                } else {
                    $("#relatorioTotalBarLegenda").removeClass("column");
                }
            });

            ////////////////////////////////////////////////////////////////////////////
            /////////////////// Modal - Watch checkbox's
            ////////////////////////////////////////////////////////////////////////////

            $("#buttonMidias").click(function () {
                $(".modalMidias, .shadow").show();
            });

            $("#buttonMidiaFilter").click(function (e) {
                e.preventDefault();
                selectedMidias = [];

                $("input:checkbox[name=midias]").each(function () {
                    if ($(this).attr("checked") == "checked") {
                        selectedMidias.push($(this).attr("id"));
                    }
                });
                
                GraficoObject.loadBarColumn();
                GraficoObject.loadHour();

                $(".loading").show();
                $(".modalMidias, .shadow").hide();
            });

            $("#buttonMidiaUncheck").click(function () {
                $("input:checkbox[name=midias]").each(function () {
                    $(this).attr("checked", false);
                });
            });

            $("#buttonMidiaCheck").click(function () {
                $("input:checkbox[name=midias]").each(function () {
                    $(this).attr("checked", true);
                });
            });

            ////////////////////////////////////////////////////////////////////////////
            /////////////////// Watch sessão
            ////////////////////////////////////////////////////////////////////////////

            $('input:radio[name=tipoSessao]').change(function () {
                GraficoObject.loadBarColumn();
                GraficoObject.loadHour();
            });

            ////////////////////////////////////////////////////////////////////////////
            /////////////////// Type of data
            ////////////////////////////////////////////////////////////////////////////

            $('input:radio[name=tipoDadoMidia]').change(function () {
                GraficoObject.loadHour();
            });

            ////////////////////////////////////////////////////////////////////////////
            /////////////////// Modal - Insert date
            ////////////////////////////////////////////////////////////////////////////

            $("#buttonData").click(function () {
                $(".modalData, .shadow").show();
            });

            $("#buttonDataAdd").click(function (e) {
                e.preventDefault();

                if ($("input#data").val() != "" && $("input#nomeData").val() != "") {
                    if ($("#dataComparacao").attr("checked") == "checked") {
                        date[0] = $("input#data").val();
                        dateName[0] = $("input#nomeData").val();
                    } else {
                        date[date.length] = $("input#data").val();
                        dateName[date.length - 1] = $("input#nomeData").val();
                    }

                    /////////////////////////////////////////////////////////////
                    //////////////// Tratamento para comparação de data
                    /////////////////////////////////////////////////////////////

                    console.log(date[0]);
                    console.log(newDateHoje);
                    console.log(date[0] == newDateHoje);

                    if (date[0] == newDateHoje) {
                        fullDay = true;
                    } else {
                        fullDay = false; 
                    }

                    $(".modalData, .shadow").hide();
                    $("input#data, input#nomeData").removeClass("erro");
                    $("input#data, input#nomeData").attr("value", "");
                    $("#dataComparacao").attr("checked", false);

                    $(".loading").show();
                    GraficoObject.init(date);
                } else {
                    if ($("input#data").val() == "") {
                        $("input#data").addClass("erro");
                    }

                    if ($("input#nomeData").val() == "") {
                        $("input#nomeData").addClass("erro");
                    }
                }
            });

            ////////////////////////////////////////////////////////////////////////////
            /////////////////// Shadow
            ////////////////////////////////////////////////////////////////////////////

            $("#buttonMidiasGrafico").click(function () {
                if ($(this).hasClass("btnHorario")) {
                    $(".horarios").hide();
                    $(".graficoMidias, .tipoDadoMidia").show();
                    $(this).removeClass("btnHorario").addClass("btnGraficoMidias").html("Gráfico por hora");
                } else {
                    $(".graficoMidias, .tipoDadoMidia").hide();
                    $(".horarios").show();
                    $(this).removeClass("btnGraficoMidias").addClass("btnHorario").html("Gráficos por mídias");
                }
                
            });

            ////////////////////////////////////////////////////////////////////////////
            /////////////////// Shadow
            ////////////////////////////////////////////////////////////////////////////

            $(".shadow").click(function(){
                $(".modalData, .shadow, .modalMidias").hide();
                $("input#data, input#nomeData").removeClass("erro");
            });
        });
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <div class="botoesMiddle">
            <a id="buttonData" class="button">Adicionar data</a>
            <a id="buttonMidias" class="button">Filtro por midias</a>
            <a id="buttonMidiasGrafico" class="button btnHorario">Gráficos por mídias</a>
        </div>
        <div class="tipoSessao">
            <div class="checkboxs">
                <label>
                    <input type='radio' name='tipoSessao' id='origem' checked='true' value="origem" />
                    <span>Origem Sessão</span>
                </label>
                <label>
                    <input type='radio' name='tipoSessao' id='ultima' value="ultima" />
                    <span>Última Sessão</span>
                </label>
            </div>
        </div>

        <div class="tipoDadoMidia">
            <div class="checkboxs">
                <label>
                    <input type='radio' name='tipoDadoMidia' id='pedidos' checked='true' value="pedidos" />
                    <span>Pedidos</span>
                </label>
                <label>
                    <input type='radio' name='tipoDadoMidia' id='sessao' value="sessao" />
                    <span>Sessão</span>
                </label>
                <label>
                    <input type='radio' name='tipoDadoMidia' id='taxa' value="taxa" />
                    <span>Taxa</span>
                </label>
            </div>
        </div>

        <div class="loading"></div>
        <div class="shadow"></div>
        <div class="modalData">
            <form action="">
                <input type="text" name="nomeData" id="nomeData" placeholder="Nome" />
                <input type="text" name="data" id="data" placeholder="Data - 20/1/1996" />
                <label class="checkboxLabel">
                    <input type='checkbox' name='dataComparacao' id='dataComparacao' value="origem" />
                    <span>Comparador</span>
                </label>
                <input type="submit" id="buttonDataAdd" class="buttonSend" value="Adicionar" />
            </form>
        </div>

        <div class="modalMidias">
            <button id="buttonMidiaUncheck" class="buttonCheck">Desmarcar todos</button>
            <button id="buttonMidiaCheck" class="buttonCheck">Marcar todos</button>
            <form action="">
                <span class="checkboxs"></span>
                <button id="buttonMidiaFilter" class="buttonSend">Aplicar Filtros</button>
            </form>
        </div>

        <!-- RELATORIO TOTAL -->
        <div id="relatorioTotal" class="mapas" style="display: none;"></div>
        <div id="relatorioTotalBarLegenda">
            <span class="receita">
                Pedidos
            </span>
            <span class="pedidos">
                Sessões
            </span>
        </div>

        <div id="relatorioTotalBar" class="mapas" style="height: 1200px;"></div>

        <div class="horarios"></div>
        <div class="graficoMidias"></div>
    </form>
</body>
</html>
