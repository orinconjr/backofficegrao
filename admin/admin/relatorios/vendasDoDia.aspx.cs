﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_relatorios_vendasDoDia : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
            usuarioLogadoId = Request.Cookies["usuarioLogadoId"];
            if (usuarioLogadoId != null)
            {
                string paginaSolicitada = Request.Path.Substring(Request.Path.LastIndexOf("/") + 1);
                if (rnUsuarios.usuarioPaginasPermitidaSeleciona_PorUsuarioIdPaginaPermitidaNome(int.Parse(usuarioLogadoId.Value.ToString()), paginaSolicitada).Tables[0].Rows.Count == 0)
                {
                    Response.Write("<script>alert('Você não tem acesso a essa área.');</script>");
                    Response.Redirect("../Default.aspx");
                }
            }
            else
            {
                Response.Write("<script>alert('Você não tem acesso a essa área.');</script>");
                Response.Redirect("../Default.aspx");
            }
        }
    }
}