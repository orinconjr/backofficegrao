﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" Theme="Glass" CodeFile="enderecamentoEmLote.aspx.cs" Inherits="admin_enderecamentoEmLote" %>

<%@ Register TagPrefix="dxwgv" Namespace="DevExpress.Web.ASPxGridView" Assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxClasses" Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxTabControl" Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" runat="Server">

    <asp:ScriptManager runat="server"></asp:ScriptManager>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <script type="text/javascript">
        function confirmarEnderecamento() {

            var area = $("#<%=ddlArea.ClientID%> option:selected").text();
            var rua = $("#<%=ddlRua.ClientID%> option:selected").text();
            var predio = $("#<%=ddlPredio.ClientID%> option:selected").text();
            var andar = $("#<%=ddlAndar.ClientID%> option:selected").text();

            var result;

            var result1 = confirm('Os produtos do romaneio serão endereçados em:\n' + 'Area: ' + area + ' - Rua: ' + rua + ' - Predio: ' + predio + ' - Andar: ' + andar + '\nConfirma?');

            try {

                if (result1 == true && $('#ctl00_conteudoPrincipal_tabsLotes_rblTipoEnderecamento_1').is(':checked')) {
                    result = confirm('Todos os produtos do carrinho informado serão endereçados em:\n' + 'Area: ' + area + ' - Rua: ' + rua + ' - Predio: ' + predio + ' - Andar: ' + andar + '\nConfirma?');
                } else {
                    result = true;
                }
            } catch (e) {
                return false;
            }


            return result;
        }
    </script>
    <table cellpadding="0" cellspacing="0" style="width: 920px">
        <tr>
            <td class="tituloPaginas" valign="top">
                <asp:Label ID="lblAcao" runat="server">Entrada/Endereçamento em Lote</asp:Label>
            </td>
        </tr>
        <tr>
            <td style="padding-left: 30px;">

                <dx:ASPxPageControl ID="tabsLotes" runat="server" ActiveTabIndex="1" Width="815px">
                    <TabPages>
                        <dx:TabPage Text="Entrada">
                            <ContentCollection>
                                <dx:ContentControl runat="server">

                                    <asp:UpdatePanel runat="server">
                                        <ContentTemplate>
                                            <div style="float: left; padding-left: 5px;">
                                                Informe o numero do romaneio:<br />
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:TextBox runat="server" ID="txtNumRomaneioEntrada" Width="80px"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:ImageButton ID="ibtnPesquisarItensRomaneioEntrada" runat="server" ImageUrl="~/admin/images/btPesquisar.jpg" OnClick="ibtnPesquisarItensRomaneioEntrada_OnClick" />
                                                        </td>
                                                        <td>
                                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                                                <asp:DropDownList runat="server" ID="ddlCarrinhosEntrada" />
                                                        </td>
                                                        <td>
                                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                                            <asp:Button runat="server" ID="btnRegistrarEntradaRomaneio" Text="Registrar Entrada" OnClientClick="return confirm('Confirma o registro da entrada de todos os itens do romaneio informado?');" Visible="False" OnClick="btnRegistrarEntradaRomaneio_OnClick" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div style="float: left; padding-left: 5px;">

                                                <dxwgv:ASPxGridView ID="grdItemPedidoFornecedor" ClientInstanceName="grdItemPedidoFornecedor" runat="server" AutoGenerateColumns="False"
                                                    KeyFieldName="idPedidoFornecedorItem" Width="834px" Settings-ShowFilterRow="True" Settings-ShowFilterBar="Visible"
                                                    Cursor="auto">
                                                    <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                                                    <Styles>
                                                        <Footer Font-Bold="True">
                                                        </Footer>
                                                    </Styles>
                                                    <SettingsText ConfirmDelete="Tem certeza que deseja excluir?" EmptyDataRow="Nenhum registro encontrado." />
                                                    <SettingsPager Position="TopAndBottom" PageSize="50"
                                                        ShowDisabledButtons="False" AlwaysShowPager="True">
                                                        <Summary AllPagesText="Páginas: {0} - {1} ({2} items)"
                                                            Text="Página {0} de {1} ({2} etiquetas sem entrada encontradas)" />
                                                    </SettingsPager>
                                                    <Settings ShowFilterRow="True" ShowGroupButtons="False" ShowFooter="True" />
                                                    <TotalSummary>
                                                        <dxwgv:ASPxSummaryItem DisplayFormat="c" FieldName="valorCobrado"
                                                            ShowInColumn="Valor" ShowInGroupFooterColumn="Valor" SummaryType="Sum" />
                                                    </TotalSummary>
                                                    <SettingsEditing EditFormColumnCount="4"
                                                        PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1"
                                                        PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter"
                                                        PopupEditFormWidth="700px" />
                                                    <Columns>
                                                        <dxwgv:GridViewDataTextColumn Caption="Etiqueta" FieldName="idPedidoFornecedorItem" VisibleIndex="0" UnboundType="Integer" Width="100">
                                                            <Settings AutoFilterCondition="Contains" />
                                                            <DataItemTemplate>
                                                                <asp:Label runat="server" ID="lblIdPedidoFornecedorItem" Text='<%# Bind("idPedidoFornecedorItem") %>'></asp:Label>
                                                                <asp:HiddenField ID="hfIdPedidoFornecedorItem" runat="server" Value='<%# Bind("idPedidoFornecedorItem") %>'></asp:HiddenField>
                                                            </DataItemTemplate>
                                                        </dxwgv:GridViewDataTextColumn>
                                                        <dxwgv:GridViewDataTextColumn Caption="Produto" FieldName="produto" VisibleIndex="1" Settings-AutoFilterCondition="Contains">
                                                            <Settings AutoFilterCondition="Contains" />
                                                        </dxwgv:GridViewDataTextColumn>
                                                    </Columns>
                                                    <StylesEditors>
                                                        <Label Font-Bold="True">
                                                        </Label>
                                                    </StylesEditors>
                                                </dxwgv:ASPxGridView>

                                            </div>

                                        </ContentTemplate>
                                    </asp:UpdatePanel>

                                </dx:ContentControl>
                            </ContentCollection>
                        </dx:TabPage>

                        <dx:TabPage Text="Endereçamento">
                            <ContentCollection>
                                <dx:ContentControl runat="server">

                                    <asp:UpdatePanel runat="server" ID="upd">
                                        <ContentTemplate>
                                            <div style="float: left; padding-left: 5px;">
                                                &nbsp;&nbsp;Endereçar pelo:
                                                <asp:RadioButtonList runat="server" ID="rblTipoEnderecamento" RepeatDirection="Horizontal" AutoPostBack="True" OnSelectedIndexChanged="rblTipoEnderecamento_OnSelectedIndexChanged">
                                                    <asp:ListItem Text="Romaneio" Value="1" Selected="True"></asp:ListItem>
                                                    <asp:ListItem Text="Carrinho" Value="2"></asp:ListItem>
                                                </asp:RadioButtonList>
                                                <br />
                                                <asp:Panel runat="server" ID="pnlEnderecamentoPorRomaneio">
                                                    <table style="float: left;">
                                                        <tr>
                                                            <td>Romaneio:
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtNumeroRomaneio" runat="server" MaxLength="10"></asp:TextBox>
                                                            </td>
                                                            <td>Etiqueta do Produto:
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtNumetoEtiqueta" runat="server" MaxLength="10"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:ImageButton ID="imbPesquisar" runat="server" ImageUrl="~/admin/images/btPesquisar.jpg" OnClick="imbPesquisar_OnClick" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                                <asp:Panel runat="server" ID="pnlEnderecamentoPorCarrinho" Visible="False">
                                                    <table style="float: left;">
                                                        <tr>
                                                            <td>Carrinho:
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList runat="server" ID="ddlCarrinhos" />
                                                            </td>
                                                            <td>
                                                                <asp:ImageButton ID="imbPesquisarPorCarrinho" runat="server" ImageUrl="~/admin/images/btPesquisar.jpg" OnClick="imbPesquisar_OnClick" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                                <asp:Panel runat="server" ID="pnlEndereco">
                                                    <table style="float: left;">
                                                        <tr>
                                                            <td>Area:
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList runat="server" ID="ddlArea" AutoPostBack="True" OnSelectedIndexChanged="ddlArea_OnSelectedIndexChanged" />
                                                            </td>
                                                            <td>Rua:
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList runat="server" ID="ddlRua" Enabled="False" AutoPostBack="True" OnSelectedIndexChanged="ddlRua_OnSelectedIndexChanged" />
                                                            </td>
                                                            <td>Predio:
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList runat="server" ID="ddlPredio" Enabled="False" AutoPostBack="True" OnSelectedIndexChanged="ddlPredio_OnSelectedIndexChanged" />
                                                            </td>
                                                            <td>Andar:
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList runat="server" ID="ddlAndar" Enabled="False" />
                                                            </td>
                                                            <td>
                                                                <asp:Button runat="server" ID="btnEnderecar" Text="Endereçar" OnClientClick="return confirmarEnderecamento()" OnClick="btnEnderecar_OnClick" Visible="False" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                            </div>

                                            <div style="float: left; padding-left: 5px;">

                                                <dxwgv:ASPxGridView ID="grd" ClientInstanceName="grd" runat="server" AutoGenerateColumns="False"
                                                    KeyFieldName="idPedidoFornecedorItem" Width="834px" Settings-ShowFilterRow="True" Settings-ShowFilterBar="Visible"
                                                    Cursor="auto" OnCustomCallback="grd_CustomCallback">
                                                    <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                                                    <Styles>
                                                        <Footer Font-Bold="True">
                                                        </Footer>
                                                    </Styles>
                                                    <SettingsText ConfirmDelete="Tem certeza que deseja excluir?" EmptyDataRow="Nenhum registro encontrado." />
                                                    <SettingsPager Position="TopAndBottom" PageSize="50"
                                                        ShowDisabledButtons="False" AlwaysShowPager="True">
                                                        <Summary AllPagesText="Páginas: {0} - {1} ({2} items)"
                                                            Text="Página {0} de {1} ({2} registros encontrados)" />
                                                    </SettingsPager>
                                                    <Settings ShowFilterRow="True" ShowGroupButtons="False" ShowFooter="True" />
                                                    <TotalSummary>
                                                        <dxwgv:ASPxSummaryItem DisplayFormat="c" FieldName="valorCobrado"
                                                            ShowInColumn="Valor" ShowInGroupFooterColumn="Valor" SummaryType="Sum" />
                                                    </TotalSummary>
                                                    <SettingsEditing EditFormColumnCount="4"
                                                        PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1"
                                                        PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter"
                                                        PopupEditFormWidth="700px" />
                                                    <Columns>
                                                        <dxwgv:GridViewDataTextColumn Caption="Etiqueta" FieldName="idPedidoFornecedorItem" VisibleIndex="0" UnboundType="Integer" Width="100">
                                                            <Settings AutoFilterCondition="Contains" />
                                                            <DataItemTemplate>
                                                                <asp:Label runat="server" ID="lblIdPedidoFornecedorItem" Text='<%# Bind("idPedidoFornecedorItem") %>'></asp:Label>
                                                                <asp:HiddenField ID="hfIdPedidoFornecedorItem" runat="server" Value='<%# Bind("idPedidoFornecedorItem") %>'></asp:HiddenField>
                                                            </DataItemTemplate>
                                                        </dxwgv:GridViewDataTextColumn>
                                                        <dxwgv:GridViewDataTextColumn Caption="Produto" FieldName="produto" VisibleIndex="1" Settings-AutoFilterCondition="Contains">
                                                            <Settings AutoFilterCondition="Contains" />
                                                        </dxwgv:GridViewDataTextColumn>
                                                    </Columns>
                                                    <StylesEditors>
                                                        <Label Font-Bold="True">
                                                        </Label>
                                                    </StylesEditors>
                                                </dxwgv:ASPxGridView>

                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>

                                </dx:ContentControl>
                            </ContentCollection>
                        </dx:TabPage>

                    </TabPages>
                </dx:ASPxPageControl>

                <asp:UpdateProgress ID="updateProgress" runat="server" DisplayAfter="1500">
                    <ProgressTemplate>
                        <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                            <div style="background-color: white; position: fixed; top: 44%; left: 40%; width: 238px; height: 50px;"><asp:Literal runat="server" ID="litInfoLoad" Text="Endereçando produtos"></asp:Literal></div>
                            <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="../admin/images/245.gif" AlternateText="Loading ..." ToolTip="Endereçando produtos..." Style="padding: 10px; position: fixed; top: 45%; left: 40%;" />
                        </div>
                    </ProgressTemplate>
                </asp:UpdateProgress>
            </td>
        </tr>
    </table>
</asp:Content>

