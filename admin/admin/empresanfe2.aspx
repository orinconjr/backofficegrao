﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="empresanfe2.aspx.cs" Theme="Glass" Inherits="admin_empresanfe2" %>

<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" runat="Server">
    <style>
        .tamanhoTxt {
            width: 90px;
        }
    </style>
    <table style="width: 920px">
        <tr>
            <td class="tituloPaginas">
                <asp:Label ID="lblAcao" runat="server">Empresas</asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <div style="width: 500px; margin: 0 auto; text-align: center; font-family: Tahoma; font-size: 21px; padding: 20px 0 0px 0;">
                    Mês:<asp:DropDownList runat="server" ID="ddlMes" Style="font-family: Tahoma; font-size: 21px;" OnSelectedIndexChanged="ddlMes_OnSelectedIndexChanged" AutoPostBack="True" />
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <table style="width: 834px; margin: 0 auto;">
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <div>
                                <dxwgv:ASPxGridView ID="grd" runat="server" AutoGenerateColumns="False" OnStartRowEditing="grd_OnStartRowEditing"
                                    DataSourceID="sqlEmpresa" KeyFieldName="idEmpresa" Width="834px" OnCustomUnboundColumnData="grd_OnCustomUnboundColumnData"
                                    Cursor="auto">
                                    <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                                    <SettingsText ConfirmDelete="Tem certeza que deseja excluir?"
                                        EmptyDataRow="Nenhum registro encontrado." />
                                    <SettingsPager Position="TopAndBottom" PageSize="50"
                                        ShowDisabledButtons="False" AlwaysShowPager="True">
                                        <Summary AllPagesText="Páginas: {0} - {1} ({2} items)"
                                            Text="Página {0} de {1} ({2} registros encontrados)"></Summary>
                                    </SettingsPager>
                                    <Settings ShowFilterRow="True" ShowGroupButtons="False" />
                                    <SettingsEditing EditFormColumnCount="4"
                                        PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1"
                                        PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter"
                                        PopupEditFormWidth="700px" />
                                    <Columns>
                                        <dxwgv:GridViewDataTextColumn Caption="Id"
                                            FieldName="idEmpresa" ReadOnly="True" VisibleIndex="0" Width="30px">
                                            <Settings AutoFilterCondition="Contains" />
                                            <EditFormSettings Visible="False" CaptionLocation="Top" ColumnSpan="2" />
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn Caption="Nome" FieldName="nome" Width="250"
                                            VisibleIndex="1">
                                            <PropertiesTextEdit>
                                                <ValidationSettings ErrorText="" SetFocusOnError="True">
                                                    <RequiredField ErrorText="Preencha o campo nome." IsRequired="True" />
                                                </ValidationSettings>
                                            </PropertiesTextEdit>
                                            <Settings AutoFilterCondition="Contains" />
                                            <EditFormSettings CaptionLocation="Top" ColumnSpan="2" Visible="True"
                                                VisibleIndex="1" />
                                            <EditItemTemplate>
                                                <dxe:ASPxTextBox runat="server" Text='<%# Eval("nome")%>' ReadOnly="True"></dxe:ASPxTextBox>
                                            </EditItemTemplate>
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn Caption="Nome Emitente" FieldName="nomeEmitente" Visible="False"
                                            VisibleIndex="3" Width="90px">
                                            <Settings AutoFilterCondition="Contains" />
                                            <EditFormSettings CaptionLocation="Top" Visible="True" VisibleIndex="4" />
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn Caption="CNPJ" FieldName="cnpj"
                                            Visible="True" VisibleIndex="3" UnboundType="Integer">
                                            <PropertiesTextEdit>
                                                <ValidationSettings SetFocusOnError="True">
                                                    <RegularExpression ErrorText="Preencha corretamente o CNPJ ou CPF."
                                                        ValidationExpression="(^(\d{2}.\d{3}.\d{3}/\d{4}-\d{2})|(\d{14})$)|(^(\d{3}.\d{3}.\d{3}-\d{2})|(\d{11})$)" />
                                                </ValidationSettings>
                                            </PropertiesTextEdit>
                                            <EditFormSettings CaptionLocation="Top" Visible="True"
                                                VisibleIndex="2" />
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn Caption="IE" FieldName="ieEmitente"
                                            Visible="True" VisibleIndex="3">
                                            <EditFormSettings CaptionLocation="Top" Visible="True"
                                                VisibleIndex="3" />
                                        </dxwgv:GridViewDataTextColumn>

                                        <dxwgv:GridViewDataTextColumn Caption="Regime Simples" FieldName="simples" Name="simples" Visible="False" VisibleIndex="4">
                                            <EditFormSettings CaptionLocation="Top" Visible="True"
                                                VisibleIndex="29" />
                                            <EditItemTemplate>
                                                <dxe:ASPxCheckBox runat="server" ID="chkSimples" Checked='<%# Convert.ToBoolean(Eval("simples")) %>' AutoPostBack="false">
                                                </dxe:ASPxCheckBox>
                                            </EditItemTemplate>
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn Caption="Ativo" FieldName="ativo" Visible="False" Name="ativo">
                                            <EditFormSettings CaptionLocation="Top" Visible="True"
                                                VisibleIndex="30" />
                                            <EditItemTemplate>
                                                <dxe:ASPxCheckBox ID="chkAtivo" runat="server" Checked='<%# Convert.ToBoolean(Eval("ativo"))%>' />
                                            </EditItemTemplate>
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn Caption="Emissões Mês" FieldName="emissoesMes" Name="emissoesMes" UnboundType="String" VisibleIndex="10" CellStyle-HorizontalAlign="Center">
                                            <EditFormSettings CaptionLocation="Top" Visible="True"
                                                VisibleIndex="31" />
                                        </dxwgv:GridViewDataTextColumn>

                                        <dxwgv:GridViewDataTextColumn Caption="Qtd Venda" FieldName="qtdvenda" Name="qtdvenda" UnboundType="String" VisibleIndex="10" CellStyle-HorizontalAlign="Center">
                                            <EditFormSettings CaptionLocation="Top" Visible="True"
                                                VisibleIndex="32" />
                                        </dxwgv:GridViewDataTextColumn>

                                        <dxwgv:GridViewDataTextColumn Caption="Vl Venda" FieldName="vlvenda" Name="vlvenda" UnboundType="String" VisibleIndex="10" CellStyle-HorizontalAlign="Center">
                                            <EditFormSettings CaptionLocation="Top" Visible="True"
                                                VisibleIndex="33" />
                                        </dxwgv:GridViewDataTextColumn>

                                        <dxwgv:GridViewDataTextColumn Caption="Qtd Devolucão" FieldName="qtddevolucao" Name="qtddevolucao" UnboundType="String" VisibleIndex="10" CellStyle-HorizontalAlign="Center">
                                            <EditFormSettings CaptionLocation="Top" Visible="True"
                                                VisibleIndex="33" />
                                        </dxwgv:GridViewDataTextColumn>

                                        <dxwgv:GridViewDataTextColumn Caption="Vl Devolução" FieldName="vldevolucao" Name="vldevolucao" UnboundType="String" VisibleIndex="10" CellStyle-HorizontalAlign="Center">
                                            <EditFormSettings CaptionLocation="Top" Visible="True"
                                                VisibleIndex="33" />
                                        </dxwgv:GridViewDataTextColumn>


                                        <dxwgv:GridViewDataTextColumn Caption="Total R$" FieldName="totalEmitido" Name="totalEmitido" UnboundType="String" VisibleIndex="10" CellStyle-HorizontalAlign="Center">
                                            <EditFormSettings CaptionLocation="Top" Visible="True"
                                                VisibleIndex="34" />
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn Caption="Limite R$" FieldName="limite" Name="limite" VisibleIndex="10" CellStyle-HorizontalAlign="Center" PropertiesTextEdit-DisplayFormatString="N">
                                            <EditFormSettings CaptionLocation="Top" Visible="True"
                                                VisibleIndex="35" />
                                        </dxwgv:GridViewDataTextColumn>
                                       
                                        <dxwgv:GridViewCommandColumn VisibleIndex="50" Width="50px" ButtonType="Image">
                                            <EditButton Visible="True" Text="Editar">
                                                <Image Url="~/admin/images/btEditar-v2.jpg" />
                                            </EditButton>
                                          <CancelButton Text="Cancelar">
                                                <Image Url="~/admin/images/btCancelar.jpg" />
                                            </CancelButton> 
                                            <UpdateButton Text="Salvar">
                                                <Image Url="~/admin/images/btSalvarPeq.jpg" />
                                            </UpdateButton>
                                            <ClearFilterButton Visible="True" Text="Limpar filtro">
                                                <Image Url="~/admin/images/btLimparFiltro.jpg" />
                                            </ClearFilterButton>
                                            <HeaderTemplate>
                                                <%--<img alt="" src="images/legendaIcones.jpg" />--%>
                                            Editar
                                            </HeaderTemplate>
                                        </dxwgv:GridViewCommandColumn>
                                    </Columns>
                                    <StylesEditors>
                                        <Label Font-Bold="True">
                                        </Label>
                                    </StylesEditors>
                                </dxwgv:ASPxGridView>
                            </div>

                            <asp:LinqDataSource ID="sqlEmpresa" runat="server" ContextTypeName="dbCommerceDataContext" EnableDelete="False" EnableInsert="True" EnableUpdate="True"
                                TableName="tbEmpresas" OnUpdating="sqlEmpresa_Updating" OnUpdated="sqlEmpresa_OnUpdated">
                            </asp:LinqDataSource>
                            <asp:LinqDataSource ID="sqlEstados" runat="server"
                                ContextTypeName="dbCommerceDataContext" TableName="tbEstados">
                            </asp:LinqDataSource>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table><br />
    <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
    <br />
    <asp:GridView ID="GridView1" runat="server"></asp:GridView>
</asp:Content>

