﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_relatorioChamado : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        FillGrid();

    }

    private void FillGrid()
    {
        var data = new dbCommerceDataContext();
        try
        {
            //var chamados = (from c in data.tbChamados
            //                where c.idChamadoDepartamento != null
            //                group c by new { c.idChamadoDepartamento, c.idUsuario, c.dataConclusao } into chamado

            //                select new
            //                {
            //                    usuario = chamado.FirstOrDefault().tbUsuario.usuarioNome,
            //                    concluidos = chamado.Count(x => x.concluido == true),
            //                    naoConcluidos = chamado.Count(x => x.concluido == false),
            //                    dataConclusao = (chamado.FirstOrDefault().dataConclusao.HasValue ? chamado.FirstOrDefault().dataConclusao.Value.Day.ToString() :string.Empty),
            //                    //dataConcluidos = (chamado.FirstOrDefault().dataConclusao == null ? "" : chamado.FirstOrDefault().dataConclusao.Value.ToString()), 
            //                    departamento = chamado.FirstOrDefault().tbChamadoDepartamento.nomeChamadoDepartamento
            //                }).ToList();

            var chamados = (from c in data.tbChamados
                            where c.idChamadoDepartamento != null
                            select new {
                                c.idChamadoDepartamento,
                                c.idUsuario,
                                dataConclusao = (c.dataConclusao == null ? null : (DateTime?)c.dataConclusao.Value.Date),
                                usuarioNome = c.tbUsuario == null ? "" : c.tbUsuario.usuarioNome,
                                c.concluido,
                                nomeChamadoDepartamento = c.tbChamadoDepartamento == null ? "" : c.tbChamadoDepartamento.nomeChamadoDepartamento,
                            }).ToList();

            var chamadosFinal = (from c in chamados
                                 group c by new { c.idChamadoDepartamento, c.idUsuario, c.dataConclusao } into chamado
                                 select new
                                 {
                                     usuario = chamado.FirstOrDefault().usuarioNome,
                                     concluidos = chamado.Count(x => x.concluido == true),
                                     naoConcluidos = chamado.Count(x => x.concluido == false),
                                     dataConcluidos = chamado.FirstOrDefault().dataConclusao == null ? "" : chamado.FirstOrDefault().dataConclusao.Value.ToShortDateString(),
                                     departamento = chamado.FirstOrDefault().nomeChamadoDepartamento
                                 }).ToList();

            
            grd.DataSource = chamadosFinal;
            if (!IsPostBack && !IsCallback)
            {
                grd.Fields["dataConcluidos"].CollapseAll();
                grd.Fields["departamento"].CollapseAll();
                grd.Fields["usuario"].CollapseAll();
                grd.DataBind();
            }
                

        }
        catch (Exception ex)
        {
            lblMensagem.Text = "Erro ao carregar o grid: " + ex.Message;
        }
    }

    protected void btRefresh_Click(object sender, ImageClickEventArgs e)
    {

    }


}