﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="grid.aspx.cs" Inherits="admin_grid" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="js/jquery.min.js"></script>
    <script type="text/javascript">
        function valida() {
           
            var query = $("#<%=txtselect.ClientID%>").val();
            var mensagem = "";
            var ok = true;
            var temwhere = false;
            if (query.trim() == "") {
                mensagem += "Digite a query\n";
                ok = false;
            }
            else
            {
              
                var posespaco = query.indexOf(" ");
                if (!isNaN(posespaco)) {
                    var primeirapalavra = query.substring(0, posespaco);
                    if (primeirapalavra.toLowerCase() == "insert") {
                        temwhere = true;
                      //  alert("insert");
                    } else {
                        if (query.toLowerCase().indexOf("where") < 0) {
                            mensagem += "Falta clausula where\n";
                            ok = false;
                        } else {
                            temwhere = true;
                        }
                    }
                    
                }
              
            }

            if (!ok) {
                alert(mensagem);
            }
            //alert(ok);
            return ok;

        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:TextBox ID="txtselect" runat="server" Height="169px" TextMode="MultiLine" Width="100%"></asp:TextBox>
        <br/><br/>
 
        <asp:Button ID="Button1" runat="server" Text="Rodar Select" OnClick="Button1_Click" OnClientClick="return valida()" /><br/><br/>
        <asp:Label ID="lblregistros" runat="server" Text=""></asp:Label><br/>
        <asp:GridView ID="GridView1" runat="server" AllowPaging="True" OnPageIndexChanging="GridView1_PageIndexChanging" PageSize="50"></asp:GridView>
    </div>
    </form>
</body>
</html>
