﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_relatorioMargemContribuicaoEstimada : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            txtDataInicial.Attributes.Add("onkeypress", "Mascaras('DATA', '" + txtDataInicial.ClientID + "');");
            txtDataFinal.Attributes.Add("onkeypress", "Mascaras('DATA', '" + txtDataFinal.ClientID + "');");
        }
    }

    protected void imbInsert_OnClick(object sender, ImageClickEventArgs e)
    {
        var dataInicial = DateTime.Now;
        var dataFinal = DateTime.Now;
        DateTime.TryParse(txtDataInicial.Text, out dataInicial);
        DateTime.TryParse(txtDataFinal.Text, out dataFinal);

        var data = new dbCommerceDataContext();
        var pedidos = (from c in data.tbPedidos
            where
                (c.statusDoPedido == 3 | c.statusDoPedido == 4 | c.statusDoPedido == 5 | c.statusDoPedido == 9 |
                 c.statusDoPedido == 11) && c.dataHoraDoPedido.Value.Date >= dataInicial.Date && c.dataHoraDoPedido.Value.Date <= dataFinal.Date
            select c).ToList();

        var receitaBruta = pedidos.Sum(x => x.valorTotalGeral);
        litReceitaBruta.Text = ((decimal) receitaBruta).ToString("C");

        var desconto = pedidos.Sum(x => x.valorDoDescontoDoPagamento);
        litDesconto.Text = ((decimal)desconto).ToString("C");


        var pedidosOutrosEstados = (from pedido in pedidos
            where
                (pedido.endEstado != "ES" && pedido.endEstado != "MG" && pedido.endEstado != "SP" &&
                 pedido.endEstado != "PR" && pedido.endEstado != "RS" && pedido.endEstado != "SC" &&
                 pedido.endEstado != "RJ") | pedido.tipoDePagamentoId == 10
            select pedido).ToList();
        var totalOutrosEstados = pedidosOutrosEstados.Sum(x => x.valorCobrado);
        var valorImpostos = (totalOutrosEstados / 100) * 9;
        litImposto.Text = ((decimal)valorImpostos).ToString("C");

        var cartoesElavon =
            (from c in pedidos
                where c.tipoDePagamentoId == 2 && (c.condDePagamentoId == 3 | c.condDePagamentoId == 5)
                select c).ToList();
        var cartoesElavonAVista = cartoesElavon.Where(x => x.numeroDeParcelas == 1);
        var cartoesElavon6 = cartoesElavon.Where(x => x.numeroDeParcelas >= 2 && x.numeroDeParcelas <= 6);
        var cartoesElavon12 = cartoesElavon.Where(x => x.numeroDeParcelas >= 7 && x.numeroDeParcelas <= 12);
        decimal totalElavonAVista = 0;
        decimal totalElavon6 = 0;
        decimal totalElavon12 = 0;
        if (cartoesElavonAVista.Any()) totalElavonAVista = (((decimal) cartoesElavonAVista.Sum(x => x.valorCobrado))/100)*Convert.ToDecimal("2,65");
        if (cartoesElavon6.Any()) totalElavon6 = (((decimal)cartoesElavon6.Sum(x => x.valorCobrado)) / 100) * Convert.ToDecimal("3,05");
        if (cartoesElavon12.Any()) totalElavon12 = (((decimal)cartoesElavon12.Sum(x => x.valorCobrado)) / 100) * Convert.ToDecimal("3,35");
        
        var cartoesCielo =
            (from c in pedidos
                where c.tipoDePagamentoId == 2 && c.condDePagamentoId != 3 && c.condDePagamentoId != 5 && c.condDePagamentoId != 14
                select c).ToList();
        var cartoesCieloAVista = cartoesCielo.Where(x => x.numeroDeParcelas == 1);
        var cartoesCielo6 = cartoesCielo.Where(x => x.numeroDeParcelas >= 2 && x.numeroDeParcelas <= 6);
        var cartoesCielo12 = cartoesCielo.Where(x => x.numeroDeParcelas >= 7 && x.numeroDeParcelas <= 12);
        decimal totalCieloAVista = 0;
        decimal totalCielo6 = 0;
        decimal totalCielo12 = 0;
        if (cartoesCieloAVista.Any()) totalCieloAVista = (((decimal)cartoesCieloAVista.Sum(x => x.valorCobrado)) / 100) * Convert.ToDecimal("2,65");
        if (cartoesCielo6.Any()) totalCielo6 = (((decimal)cartoesCielo6.Sum(x => x.valorCobrado)) / 100) * Convert.ToDecimal("3,05");
        if (cartoesCielo12.Any()) totalCielo12 = (((decimal)cartoesCielo12.Sum(x => x.valorCobrado)) / 100) * Convert.ToDecimal("3,35");

        var boletos =
            (from c in pedidos
             where c.tipoDePagamentoId == 1 select c).ToList();
        decimal totalBoletos = 0;
        if (boletos.Any()) totalBoletos = ((decimal)boletos.Count()) * Convert.ToDecimal("2,50");

        var debitos =
            (from c in pedidos
             where c.tipoDePagamentoId == 7 select c).ToList();
        decimal totalDebitos = 0;
        if (debitos.Any()) totalDebitos = (((decimal)debitos.Count()) * Convert.ToDecimal("0,39")) + ((((decimal)debitos.Sum(x => x.valorCobrado)) / 100) * Convert.ToDecimal("2,90"));

        var hipercard =
            (from c in pedidos
             where c.tipoDePagamentoId == 2 && c.condDePagamentoId == 14 select c).ToList();
        decimal totalHipercard = 0;
        if (hipercard.Any()) totalHipercard = (((decimal)hipercard.Count()) * Convert.ToDecimal("0,39")) + ((((decimal)hipercard.Sum(x => x.valorCobrado)) / 100) * Convert.ToDecimal("7,40"));
        foreach (var hiper in hipercard)
        {
            decimal totalComJuros = (decimal)hiper.valorCobrado;
            for (int i = 1; i <= hiper.numeroDeParcelas; i++)
            {
                if (1 > 1)
                {
                    totalComJuros += (totalComJuros/100)*2;
                }
            }
            totalHipercard += totalComJuros - (decimal) hiper.valorCobrado;
        }

        var totalDespesasPagamento = totalHipercard + totalDebitos + totalBoletos + totalCielo12 + totalCielo6 +
                                     totalCieloAVista + totalElavon12 + totalElavon6 + totalElavonAVista;
        litTarifasPagamento.Text = totalDespesasPagamento.ToString("C");


        var receitaLiquida = receitaBruta - desconto - totalDespesasPagamento;
        litReceitaLiquida.Text = ((decimal)receitaLiquida).ToString("C");


        var custoEnvio = (from c in data.tbPedidoEnvios
            where c.tbPedido.dataHoraDoPedido >= dataInicial && c.tbPedido.dataHoraDoPedido <= dataFinal
            select c).ToList();
        decimal totalCustoEnvio = 0;
        if (custoEnvio.Any()) totalCustoEnvio = ((decimal) custoEnvio.Sum(x => x.valorSelecionado));
        litFrete.Text = totalCustoEnvio.ToString("C");


        var custoMercadoria = (from c in data.tbItensPedidos
                               where c.tbPedido.dataHoraDoPedido >= dataInicial && c.tbPedido.dataHoraDoPedido <= dataFinal && (c.tbPedido.statusDoPedido == 3 | c.tbPedido.statusDoPedido == 4 | c.tbPedido.statusDoPedido == 5 | c.tbPedido.statusDoPedido == 9 |
                                    c.tbPedido.statusDoPedido == 11)
            select c).ToList();
        decimal totalCustoMercadoria = 0;
        if (custoMercadoria.Any()) totalCustoMercadoria = ((decimal)custoMercadoria.Sum(x => x.tbProduto.produtoPrecoDeCusto));
        litCustoMercadoria.Text = totalCustoMercadoria.ToString("C");

        var margemContribuicao = (receitaLiquida - valorImpostos - totalCustoEnvio -
                                  totalCustoMercadoria);
        litMargemContribuicao.Text = ((decimal) margemContribuicao).ToString("C");

    }
}