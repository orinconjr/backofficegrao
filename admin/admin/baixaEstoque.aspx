﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="baixaEstoque.aspx.cs" Inherits="admin_baixaEstoque" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxTimer" TagPrefix="dxt" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" Runat="Server">
    <asp:Panel runat="server" ID="pnRetirar" DefaultButton="btnBaixa">
    <table cellpadding="0" cellspacing="0" style="width: 920px">
        <tr>
            <td class="tituloPaginas" valign="top">
                <asp:Label ID="lblAcao" runat="server">Baixa no Estoque</asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                        
                    <asp:Panel runat="server" ID="pnNumeroEtiqueta">
                        <tr class="rotulos">
                            <td>
                                Número da Etiqueta
                            </td>
                        </tr>
                        <tr class="rotulos">
                            <td>
                                <asp:TextBox runat="server" id="txtNumeroEtiqueta"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="rotulos">
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr class="rotulos">
                            <td>
                                <asp:Button runat="server" id="btnBaixa" text="Retirar do Estoque" OnClick="btnBaixa_Click" />
                            </td>
                        </tr>
                    </asp:Panel>
                    <asp:Panel runat="server" ID="pnConfirmar" Visible="False">
                        <tr class="rotulos">
                            <td>
                                Deseja realmente retirar a o produto <asp:Literal runat="server" id="litProduto"></asp:Literal> do estoque?
                            </td>
                        </tr>
                        <tr class="rotulos">
                            <td>
                                <asp:Button runat="server" id="btnConfimar" text="Confirmar" OnClick="btnConfimar_OnClick" />
                            </td>
                        </tr>
                    </asp:Panel>  
                </table>
            </td>
        </tr>
    </table>
                </asp:Panel>
</asp:Content>
