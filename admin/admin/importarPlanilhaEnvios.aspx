﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="importarPlanilhaEnvios.aspx.cs" Inherits="admin_importarPlanilhaEnvios" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" runat="Server">

    <table width="100%">
        <tr>
            <td class="tituloPaginas" valign="top" colspan="5">Exportar Planilha de Envios da Transportadora
            </td>
        </tr>
        <tr>
            <td>
                <table style="margin-left: 37%;">
                    <tr>
                        <td colspan="2" style="text-align: center;">Envios no Período:
                        </td>
                    </tr>
                    <tr>
                        <td class="rotulos" style="width: 100px; text-align: center;">Data inicial<br />
                            <asp:TextBox ID="txtDataInicial" runat="server" CssClass="ll-skin-latoja campos"
                                Text=""
                                Width="90px" MaxLength="10"></asp:TextBox>
                            <b>
                                <asp:RegularExpressionValidator ID="rgeDataInicial" runat="server"
                                    ControlToValidate="txtDataInicial" Display="None"
                                    ErrorMessage="Por favor, preencha corretamente a data inicial."
                                    SetFocusOnError="True"
                                    ValidationExpression="^(([0-2]\d|[3][0-1])\/([0]\d|[1][0-2])\/[1-2][0-9]\d{2})$"></asp:RegularExpressionValidator>


                            </b>
                        </td>
                        <td class="rotulos" style="width: 100px; text-align: center;">Data final<br />
                            <asp:TextBox ID="txtDataFinal" runat="server" CssClass="ll-skin-latoja campos"
                                Text=""
                                Width="90px" MaxLength="10"></asp:TextBox>
                            <b>
                                <asp:RegularExpressionValidator ID="rgeDataInicial0" runat="server"
                                    ControlToValidate="txtDataFinal" Display="None"
                                    ErrorMessage="Por favor, preencha corretamente a data final"
                                    SetFocusOnError="True"
                                    ValidationExpression="^(([0-2]\d|[3][0-1])\/([0]\d|[1][0-2])\/[1-2][0-9]\d{2})$"></asp:RegularExpressionValidator>
                            </b>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align: center;">
                            <asp:Button runat="server" Text="Exportar" ID="btnExportar" OnClick="btnExportar_Click" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

</asp:Content>

