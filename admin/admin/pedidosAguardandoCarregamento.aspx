﻿<%@ Page Title="" Theme="Glass" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="pedidosAguardandoCarregamento.aspx.cs" Inherits="admin_pedidosAguardandoCarregamento" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxLoadingPanel" Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxCallback" Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxEditors" Assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" Runat="Server">
    <script language="javascript" type="text/javascript">
        function ApplyFilter(dde, dateFrom, dateTo) {
            var d1 = dateFrom.GetText();
            var d2 = dateTo.GetText();
            if (d1 == "" || d2 == "")
                return;
            dde.SetText(d1 + "|" + d2);
            //grd.ApplyFilter("[dataHoraDoPedido] >= '" + d1 + " 00:00:00' && [dataHoraDoPedido] <= '" + d2 + " 23:59:59'");
            grd.AutoFilterByColumn("dataSendoEmbalado", dde.GetText());
        }

        function OnDropDown(s, dateFrom, dateTo) {
            var str = s.GetText();
            if (str == "") {
                // default date (1950-1961 for demo)     
                dateFrom.SetDate(new Date(1950, 0, 1));
                dateTo.SetDate(new Date(1960, 11, 31));
                return;
            }
            var d = str.split("|");
            dateFrom.SetText(d[0]);
            dateTo.SetText(d[1]);
        }
    </script>
    <script type="text/javascript">
        function toggle() {
            var divs = document.querySelectorAll("[id]");
            for (var i = 0, len = divs.length; i < len; i++) {
                var div = divs[i];
                if (div.id.indexOf("chkEnviado") > -1) {
                    div.checked = true;
                }
            }
        }
    </script>
    <table cellpadding="0" cellspacing="0" style="width: 920px">
    <tr>
        <td class="tituloPaginas" valign="top">
            <asp:Label ID="lblAcao" runat="server">Pedidos Aguardando Carregamento</asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            
    <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
        <tr>
            <td align="right">
                <table cellpadding="0" cellspacing="0" style="width: 834px">
                    <tr>
                        <td align="right">
                            &nbsp;</td>
                        <td height="38" 
                            style="background-image: url('images/bkExportar.jpg'); padding-bottom: 1px;" 
                            valign="bottom" width="231">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="width: 97px">
                                        &nbsp;</td>
                                    <td style="width: 33px">
                                        <asp:ImageButton ID="btPdf" runat="server" ImageUrl="~/admin/images/btPdf.jpg"  OnClick="btPdf_Click"
                                            style="margin-left: 5px" />
                                    </td>
                                    <td style="width: 31px">
                                        <asp:ImageButton ID="btXsl" runat="server" ImageUrl="~/admin/images/btXls.jpg" OnClick="btXsl_Click"
                                            style="margin-left: 5px" />
                                    </td>
                                    <td style="width: 36px">
                                        <asp:ImageButton ID="btRtf" runat="server" ImageUrl="~/admin/images/btRtf.jpg" OnClick="btRtf_Click"
                                            style="margin-left: 5px" />
                                    </td>
                                    <td valign="bottom">
                                        <asp:ImageButton ID="btCsv" runat="server" ImageUrl="~/admin/images/btCsv.jpg" OnClick="btCsv_Click"
                                            style="margin-left: 5px" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td style="text-align: right;">
                <asp:Button runat="server" OnClientClick="toggle(); return false;" Text="Checar todos"  />
            </td>
        </tr>
        <tr>
            <td>
                <dxwgv:ASPxGridView ID="grd" ClientInstanceName="grd" runat="server" AutoGenerateColumns="False" 
                     OnAutoFilterCellEditorCreate="grid_AutoFilterCellEditorCreate" OnAutoFilterCellEditorInitialize="grid_AutoFilterCellEditorInitialize"
                OnProcessColumnAutoFilter="grid_ProcessColumnAutoFilter" KeyFieldName="pedidoId" Width="834px"  Settings-ShowFilterRow="True" Settings-ShowFilterBar="Visible"
                    Cursor="auto" OnCustomUnboundColumnData="grd_OnCustomUnboundColumnData">
                    <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                    <Styles>
                        <Footer Font-Bold="True">
                        </Footer>
                    </Styles>
                    <SettingsText ConfirmDelete="Tem certeza que deseja excluir?" emptydatarow="Nenhum registro encontrado." />
                    <SettingsPager Position="TopAndBottom" PageSize="500" 
                        ShowDisabledButtons="False" AlwaysShowPager="True">
                        <Summary AllPagesText="Páginas: {0} - {1} ({2} items)" 
                            Text="Página {0} de {1} ({2} registros encontrados)" />
                    </SettingsPager>
                    <TotalSummary>
                        <dxwgv:ASPxSummaryItem FieldName="volumesEmbalados" ShowInColumn="volumesEmbalados" ShowInGroupFooterColumn="volumesEmbalados" SummaryType="Sum" DisplayFormat="{0}" />
                    </TotalSummary>
                    <settings showfilterrow="True" ShowGroupButtons="False" ShowFooter="True" />
                    <SettingsEditing EditFormColumnCount="4" 
                        PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1" 
                        PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter" 
                        PopupEditFormWidth="700px" />
                    <Columns>
                        <dxwgv:GridViewDataTextColumn Caption="Id Cliente" FieldName="pedidoIdCliente" VisibleIndex="0" UnboundType="Integer">
                            <EditFormSettings Visible="False" />
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Id" FieldName="pedidoId" VisibleIndex="0">
                            <EditFormSettings Visible="False" />
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Cliente" FieldName="endNomeDoDestinatario" VisibleIndex="0">
                            <EditFormSettings Visible="False" />
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Envio" FieldName="formaDeEnvio" VisibleIndex="0">
                            <EditFormSettings Visible="False" />
                        </dxwgv:GridViewDataTextColumn>
                        <%--<dxwgv:GridViewDataComboBoxColumn Caption="Usuário" FieldName="idUsuarioEmbalado" VisibleIndex="3" Width="130px">
                            <PropertiesComboBox DataSourceID="sqlUsuarios" TextField="nome" ValueField="idUsuarioExpedicao" ValueType="System.String">
                            </PropertiesComboBox>
                        </dxwgv:GridViewDataComboBoxColumn>--%>
                        <dxwgv:GridViewDataTextColumn Caption="Rastreio" FieldName="rastreio" VisibleIndex="3">
                            <EditFormSettings Visible="False" />
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Carregado" Name="carregado" VisibleIndex="4">
                            <DataItemTemplate>
                                <asp:CheckBox runat="server" ID="chkEnviado" />
                            </DataItemTemplate>
                        </dxwgv:GridViewDataTextColumn>
                    </Columns>
                    <StylesEditors>
                        <Label Font-Bold="True">
                        </Label>
                    </StylesEditors>
                </dxwgv:ASPxGridView>
                <dxwgv:ASPxGridViewExporter ID="grdEx" runat="server" FileName="pedidos" 
                    GridViewID="grd" Landscape="True" PreserveGroupRowStates="True">
                </dxwgv:ASPxGridViewExporter>
                <asp:LinqDataSource ID="sqlUsuarios" runat="server" 
                    ContextTypeName="dbCommerceDataContext" EnableDelete="True" 
                    EnableInsert="True" EnableUpdate="True" TableName="tbUsuarioExpedicaos" EntityTypeName="">
                </asp:LinqDataSource>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td style="text-align: right;">
                <asp:Button Visible="False" runat="server" ID="btnGravar" OnClick="btnGravar_OnClick" Text="Gravar Carregamento" />
                <dx:ASPxButton ID="btnEnviarGravar" runat="server" Text="Gravar Rastreamento" AutoPostBack="False">
                    <ClientSideEvents Click="function(s, e) {
                        cbGravar.PerformCallback();
                        LoadingPanel.Show();
                    }" />
                </dx:ASPxButton>
            </td>
        </tr>
    </table>
        </td>
    </tr>
</table>
    <dx:ASPxLoadingPanel ID="LoadingPanel" runat="server" ClientInstanceName="LoadingPanel" Modal="True" Text="Carregando... Por favor Aguarde&hellip;">
    </dx:ASPxLoadingPanel>
    <dx:ASPxCallback OnCallback="cbGravar_OnCallback" ID="cbGravar" runat="server" ClientInstanceName="cbGravar">
        <ClientSideEvents CallbackComplete="function(s, e) { 
            LoadingPanel.Hide(); 
            grd.Refresh();
            }" />
    </dx:ASPxCallback>
</asp:Content>