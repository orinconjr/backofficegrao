﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;

public partial class admin_depoimentos : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    //protected void btPdf_Click(object sender, ImageClickEventArgs e)
    //{
    //    this.grdEx.WritePdfToResponse();
    //}
    //protected void btXsl_Click(object sender, ImageClickEventArgs e)
    //{
    //    this.grdEx.WriteXlsToResponse();
    //}
    //protected void btRtf_Click(object sender, ImageClickEventArgs e)
    //{
    //    this.grdEx.WriteRtfToResponse();
    //}
    //protected void btCsv_Click(object sender, ImageClickEventArgs e)
    //{
    //    this.grdEx.WriteCsvToResponse();
    //}

    protected void grd_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType != GridViewRowType.EditForm) return;
        {
           
        }
    }

    protected void grd_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        sqlCategorias.InsertParameters.Clear();

        ASPxTextBox txtURL = (ASPxTextBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["url"], "txtURL");
        TextBox txtPosicao = (TextBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["posicao"], "txtPosicao");

        sqlCategorias.InsertParameters.Add("url", txtURL.Text);

        if (!string.IsNullOrEmpty(txtPosicao.Text.ToString()))
        {
            sqlCategorias.InsertParameters.Add("posicao", txtPosicao.Text);
        }
        else
        {
            sqlCategorias.InsertParameters.Add("posicao", "0");
        }
    }

    protected void grd_RowInserted(object sender, DevExpress.Web.Data.ASPxDataInsertedEventArgs e)
    {
        if (e.Exception == null)
        {
            Response.Write("<script>window.location=('depoimentos.aspx');</script>");
        }
    }

    protected void grd_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        sqlCategorias.UpdateParameters.Clear();


        ASPxTextBox txtURL = (ASPxTextBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["url"], "txtURL");
        TextBox txtPosicao = (TextBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["posicao"], "txtPosicao");

        int categoriaId = Convert.ToInt32(e.Keys[0]);

        sqlCategorias.UpdateParameters.Add("url", txtURL.Text);

        if (!string.IsNullOrEmpty(txtPosicao.Text.ToString()))
        {
            sqlCategorias.UpdateParameters.Add("posicao", txtPosicao.Text);
        }
        else
        {
            sqlCategorias.UpdateParameters.Add("posicao", "0");
        }
    }

    protected void grd_RowUpdated(object sender, DevExpress.Web.Data.ASPxDataUpdatedEventArgs e)
    {
        if (e.Exception == null)
        {
            Response.Write("<script>window.location=('depoimentos.aspx');</script>");
        }
    }

    protected void grd_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
    {
        
    }

    protected void grd_RowDeleted(object sender, DevExpress.Web.Data.ASPxDataDeletedEventArgs e)
    {
        Response.Write("<script>window.location=('depoimentos.aspx');</script>");
    }
}
