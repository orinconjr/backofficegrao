﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxGridView;
using DevExpress.XtraPrinting.Native;

public partial class admin_pedidosSeparacaoCompletos2 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        fillGrid(false);
    }
    private void fillGrid(bool rebind)
    {
        grd.DataSource = sql;
        if ((!Page.IsPostBack && !Page.IsCallback) | rebind) grd.DataBind();
    }

    protected void gridClientes_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType != GridViewRowType.Data)
        {
            return;
        }
        bool completo = true;
        int pedidoId = 0;
        int.TryParse(grd.GetRowValues(e.VisibleIndex, new string[] { "pedidoId" }).ToString(), out pedidoId);
        TextBox txtEnviarPor = (TextBox)grd.FindRowCellTemplateControl(e.VisibleIndex, grd.Columns["enviarPor"] as GridViewDataColumn, "txtEnviarPor");
        var pedidosDc = new dbCommerceDataContext();
        var pedido = (from c in pedidosDc.tbPedidos where c.pedidoId == pedidoId select c).First();
        txtEnviarPor.Text = pedido.entregaEnviarPor;
    }

    protected void btnGravar_OnClick(object sender, ImageClickEventArgs e)
    {
        int inicio = 0;
        int fim = 0;

        if (grd.PageIndex == 0)
        {
            inicio = 0;
            fim = inicio + grd.GetCurrentPageRowValues("pedidoId").Count;
        }
        else
        {
            inicio += inicio + grd.SettingsPager.PageSize * grd.PageIndex;
            fim += inicio + grd.GetCurrentPageRowValues("pedidoId").Count;
        }

        for (int i = inicio; i < fim; i++)
        {
            var pedidoDc = new dbCommerceDataContext();
            TextBox txtEnviarPor = (TextBox)grd.FindRowCellTemplateControl(i, grd.Columns["enviarPor"] as GridViewDataColumn, "txtEnviarPor");
            int pedidoId = (int)grd.GetRowValues(i, new string[] { "pedidoId" });
            var pedido = (from c in pedidoDc.tbPedidos where c.pedidoId == pedidoId select c).First();
            if (pedido.entregaEnviarPor != txtEnviarPor.Text)
            {
                interacaoInclui(txtEnviarPor.Text, "False", pedido.pedidoId);
            }
            pedido.entregaEnviarPor = txtEnviarPor.Text;
            pedidoDc.SubmitChanges();
        }
        fillGrid(true);
        Response.Write("<script>alert('Métodos de Entrega gravados com sucesso.');</script>");
    }

    protected void btnAlterarStatus_OnClick(object sender, EventArgs e)
    {
        int inicio = 0;
        int fim = 0;

        if (grd.PageIndex == 0)
        {
            inicio = 0;
            fim = inicio + grd.GetCurrentPageRowValues("pedidoId").Count;
        }
        else
        {
            inicio += inicio + grd.SettingsPager.PageSize * grd.PageIndex;
            fim += inicio + grd.GetCurrentPageRowValues("pedidoId").Count;
        }

        for (int i = inicio; i < fim; i++)
        {
            var pedidoDc = new dbCommerceDataContext();
            int pedidoId = (int)grd.GetRowValues(i, new string[] { "pedidoId" });
            var pedido = (from c in pedidoDc.tbPedidos where c.pedidoId == pedidoId select c).First();
            pedido.statusDoPedido = 4;
            pedidoDc.SubmitChanges();

            rnPedidos.pedidoAlteraStatus(4, pedido.pedidoId); var clientesDc = new dbCommerceDataContext();
            var cliente = (from c in clientesDc.tbClientes where c.clienteId == pedido.clienteId select c).First();
            interacaoInclui("Seu pedido está sendo embalado", "True", pedido.pedidoId);
            rnEmails.enviaAlteracaoDeStatus(cliente.clienteNome, pedido.pedidoId.ToString(), "Seu pedido está sendo embalado", cliente.clienteEmail);

        }
        fillGrid(true);
        Response.Write("<script>alert('Pedidos alterados com sucesso.');</script>");
    }



    public void interacaoInclui(string interacao, string alteracaoDeEstatus, int pedidoId)
    {
        HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
        usuarioLogadoId = Request.Cookies["usuarioLogadoId"];
        if (usuarioLogadoId != null)
        {
            rnInteracoes.interacaoInclui(pedidoId, interacao, rnUsuarios.usuarioSeleciona_PorUsuarioId(int.Parse(usuarioLogadoId.Value.ToString())).Tables[0].Rows[0]["usuarioNome"].ToString(), alteracaoDeEstatus);
        }
    }
}