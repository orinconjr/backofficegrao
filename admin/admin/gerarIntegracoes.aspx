﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="gerarIntegracoes.aspx.cs" Inherits="admin_gerarIntegracoes" %>
<%@ Register Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxTimer" TagPrefix="dxt" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxCallback" TagPrefix="dxcb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" Runat="Server">
    <table cellpadding="0" cellspacing="0" style="width: 920px">
        <tr>
            <td class="tituloPaginas" valign="top">
                <asp:Label ID="lblAcao" runat="server">Atualizar Integrações</asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
                    <tr>
                        <td class="textoPreto">
                            <asp:CheckBox runat="server" ID="chkRakuten" Text="Rakuten"/>&nbsp;<asp:CheckBox runat="server" ID="chkRakutenIgnorarFotos" Text="Não enviar fotos (atualizar apenas produtos)"/><br/>
                            <asp:CheckBox runat="server" ID="chkExtra" Text="Extra"/>&nbsp;<asp:CheckBox runat="server" ID="chkExtraRecadastrar" Text="Recadastrar todos os produtos"/>&nbsp;<asp:CheckBox runat="server" ID="chkExtraEnviarApenasNaoCadastrados" Checked="False" Text="Enviar apenas produtos não cadastrados (não atualizar estoques e preços)"/><br/>
                        </td>
                    </tr>
                    <tr><td>&nbsp;</td></tr>
                    <tr>
                        <td class="textoPreto">
                            <dxcb:ASPxCallback ID="ASPxCallback1" runat="server" ClientInstanceName="myCallback1" OnCallback="ASPxCallback1_Callback">
                            <ClientSideEvents 
                                CallbackComplete="function(s, e) {
                                    myButton.SetEnabled(true);
                                    myTimer.SetEnabled(false);
                                    myLabel.SetText('Process completed');
                            }" />
                        </dxcb:ASPxCallback>
        

                        <dxe:ASPxButton ID="ASPxButton1" runat="server" AutoPostBack="False" Text="Atualizar Integradores" ClientInstanceName="myButton">
                            <ClientSideEvents 
                                Click="function(s, e) {
                                    s.SetEnabled(false);
	                                myCallback1.PerformCallback();
	                                myLabel.SetText('Process completion: 0% ');
	                                myLabel.SetClientVisible(true);
	                                myTimer.SetEnabled(true);
                                    barraProgresso.SetVisible(true);
                                }" />
                        </dxe:ASPxButton>

        
                        <dxcb:ASPxCallback ID="ASPxCallback2" runat="server" ClientInstanceName="myCallback2" OnCallback="ASPxCallback2_Callback">
                            <ClientSideEvents 
                                CallbackComplete="function(s, e) {
                                    var labelText = myLabel.GetText();
                                    if(labelText != 'Process completed')
                                    {
                                        barraProgresso.SetPosition(e.result);
	                                }
                                    else
                                    {
                                        barraProgresso.SetVisible(false);
                                    }
                                }" />
                        </dxcb:ASPxCallback>
        
                        <dxt:ASPxTimer ID="ASPxTimer1" runat="server" ClientInstanceName="myTimer" Enabled="False" Interval="1000">
                            <ClientSideEvents 
                                Tick="function(s, e) {
	                                myCallback2.PerformCallback();
                                }" />
                        </dxt:ASPxTimer>
                        <dxe:ASPxLabel ID="ASPxLabel1" runat="server" ClientInstanceName="myLabel" ClientVisible="False">
                        </dxe:ASPxLabel>
                        </td>
                        <tr><td>&nbsp;<asp:Button runat="server" ID="btnGerarExtra" OnClick="btnGerarExtra_Click" Visible="False"/></td></tr>
                        <tr><td>&nbsp;<asp:Button runat="server" ID="btnGerarRakuten" OnClick="btnGerarRakuten_Click" Visible="False"/></td></tr>
                        <tr>
                            <td>
                                <dxe:ASPxProgressBar ID="barraProgresso" runat="server" ForeColor="#3333CC" ClientInstanceName="barraProgresso"
                                    Height="21px" Minimum="0" Width="300px" ClientVisible="False">
                                </dxe:ASPxProgressBar>
                            </td>
                        </tr>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="textoPreto">
                            Importações do Extra Pendentes: <asp:Literal runat="server" ID="litImportacoesExtraPendentes"></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Button runat="server" ID="btnGerarSubmarino" OnClick="btnGerarSubmarino_OnClick" Text="Gerar Submarino"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Button runat="server" ID="btnGerarSubmarinoFrete" OnClick="btnGerarSubmarinoFrete_OnClick" Text="Gerar Submarino Frete"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Button runat="server" ID="btnGerarSubmarinoPreco" OnClick="btnGerarSubmarinoPreco_OnClick" Text="Gerar Submarino Preco"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Button runat="server" ID="btnGerarSubmarinoEstoque" OnClick="btnGerarSubmarinoEstoque_OnClick" Text="Gerar Submarino Estoque"/>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>



