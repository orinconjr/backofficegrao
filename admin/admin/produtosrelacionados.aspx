﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="produtosrelacionados.aspx.cs" Inherits="admin_produtosrelacionados" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" runat="Server">
    <style>
        .meugrid {
            background-color: #F4FAFB;
            border: 0px none;
            /* border-collapse: separate !important; */
            overflow: hidden;
            font: 9pt Tahoma;
            color: #000;
            cursor: pointer;
            white-space: nowrap;
            font-weight: normal;
            text-align: left;
            width: 834px;
            border-collapse: collapse;
        }

            .meugrid th {
                background-image: url(../../App_Themes/Glass/GridView/gvHeaderBackground.gif);
                background-position: center top;
                background-repeat: repeat-x;
                background-color: #C0DDE0;
                overflow: hidden;
                height: 23px;
                line-height: 23px;
                padding: 4px 6px 5px;
                font-weight: normal;
                border: 1px solid #7EACB1;
                /* border-collapse: separate;*/
            }

            .meugrid td {
                overflow: hidden;
                height: 23px;
                line-height: 23px;
                padding: 4px 6px 5px;
                border: 1px solid #7EACB1;
            }

            .meugrid tr td:nth-child(4) {
                text-align: center;
            }

            .meugrid tr td:last-of-type {
                text-align: center;
            }

        .tabrotulos {
            width: 848px;
            margin: auto;
        }

        .materiaprima {
            width: 250px;
        }

            .materiaprima ul {
                margin: 0px;
                padding-left: 10px;
            }

                .materiaprima ul li {
                    max-width: 230px;
                    white-space: pre-wrap;
                }

        .meugrid tr td:last-of-type {
            text-align: center;
        }

        .nome {
            width: 237px;
        }

            .nome div {
                /* max-width: 230px;*/
                white-space: pre-wrap;
            }

        .rotulos td {
            font: bold 12px tahoma;
            padding-top: 20px;
            color: rgb(65, 105, 126);
            padding-left: 5px;
            padding-bottom: 5px;
        }
    </style>
    <table cellpadding="0" cellspacing="0" style="width: 920px">
        <tr>
            <td class="tituloPaginas" valign="top">
                <asp:Label ID="lblAcao" runat="server">Pesquisa de Produtos Relacionados</asp:Label>
            </td>
        </tr>
        <tr>
            <td>

                <div style="width: 100%; height: auto; float: left; clear: left; margin-left: 45px; font: normal 12px tahoma; margin-bottom: 10px;">
                    <div style="float: left; clear: left; width: 340px; height: 30px;">
                        Insira o código do Produto Filho:
                        <asp:TextBox ID="txtBusca" runat="server" TextMode="MultiLine" Height="50px" />
                    </div>
                    <div style="float: left; width: 300px; height: 30px;">
                        <asp:ImageButton ID="btnPesquisar" runat="server" ImageUrl="~/admin/images/btPesquisar.jpg" OnClick="btnPesquisar_Click" ValidationGroup="a" />
                    </div>
                    <div style="float: left; clear: left; width: 340px; height: 30px;">
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Digite o código do produto" ValidationGroup="a" ControlToValidate="txtBusca"></asp:RequiredFieldValidator>
                    </div>


                </div>

            </td>
        </tr>
        <tr>
            <td>
                <div style="width: 100%; height: auto; float: left; clear: left;">
                    <div style="width: 100%; height: auto; float: left; clear: left; padding-left: 45px; font: normal 12px tahoma; margin-bottom: 10px;">
                        <asp:Label ID="lblprodutofilho" runat="server" Text=""></asp:Label>
                    </div>
                    <div id="containergrid" runat="server" style="float: left; clear: left; display: none;">
                        <table class="meugrid" align="center" id="meugrid">

                            <tr>
                                <th style="width: 50px;">Id Produto Pai
                                </th>
                                <th>Produto
                                </th>
                                <th style="width: 50px; text-align: center;">Editar
                                </th>

                            </tr>
                            <asp:ListView runat="server" ID="lstProdutos">
                                <ItemTemplate>
                                    <tr>
                                        <asp:HiddenField runat="server" ID="hdfProdutoId" Value='<%# Eval("produtoId") %>' />

                                        <td>
                                            <div><%# Eval("produtoId") %></div>
                                        </td>
                                        <td class="nome">
                                            <div><%# Eval("produtoNome") %></div>
                                        </td>


                                        <td align="center">
                                            <a class="dxeHyperlink_Glass" href="produtoalt.aspx?produtoId=<%# Eval("produtoId") %>&produtoPaiId=<%# Eval("produtoId") %>&acao=alterar" target="_blank">
                                                <img src="images/btEditar.png" />
                                            </a>
                                        </td>

                                    </tr>

                                </ItemTemplate>
                            </asp:ListView>
                            <tr>
                                <td colspan="3">                                    
                                    <div style="text-align: right;">
                                        <asp:Button ID="btnExportar" Text="Exportar" runat="server" OnClick="btnExportar_Click" />
                                    </div>
                                </td>
                            </tr>
                        </table>

                    </div>


                </div>


            </td>
        </tr>
        <tr>
            <td>&nbsp;
            </td>
        </tr>
        <tr>
            <td class="tituloPaginas" valign="top">
                <asp:Label ID="Label1" runat="server">Pesquisa de Categorias Relacionadas ao Produto</asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <div style="width: 100%; height: auto; float: left; clear: left; margin-left: 45px; font: normal 12px tahoma; margin-bottom: 10px;">
                    <div style="float: left; clear: left; width: 340px; height: 30px;">
                        Insira o ID do Produto:<br/>
                        <asp:TextBox ID="txtProdutoId" runat="server" />
                    </div>
                    <div style="float: left; width: 300px; height: 30px;">
                        <asp:ImageButton ID="btnPesquisarCategoriaProduto" runat="server" ImageUrl="~/admin/images/btPesquisar.jpg" OnClick="btnPesquisarCategoriaProduto_OnClick" ValidationGroup="b" />
                    </div>
                    <div style="float: left; clear: left; width: 340px; height: 30px;">
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Digite o id do produto" ValidationGroup="b" ControlToValidate="txtProdutoId"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="Digite apenas números" ControlToValidate="txtProdutoId" ValidationExpression="^([0-9]*)$" ValidationGroup="b"></asp:RegularExpressionValidator>
                    </div>
                    <div style="float: left; width: 100%;">
                        <asp:Literal runat="server" ID="litProdutoNome"></asp:Literal>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div style="width: 100%; height: auto; float: left; clear: left;">
                    <div id="containergrid2" runat="server" style="float: left; clear: left; display: none;">
                        <table class="meugrid" align="center" id="meugrid">

                            <tr>
                                <th style="width: 50px;">Categoria Id
                                </th>
                                <th style="width: 60px;">Categoria Pai Id
                                </th>
                                <th>Categoria Nome
                                </th>
                                <th style="width: 50px; text-align: center;">Editar
                                </th>
                            </tr>
                            <asp:ListView runat="server" ID="lstCategoriaProduto">
                                <ItemTemplate>
                                    <tr>
                                        <asp:HiddenField runat="server" ID="hdfProdutoId" Value='<%# Eval("produtoId") %>' />

                                        <td>
                                            <div><%# Eval("categoriaId") %></div>
                                        </td>
                                        <td>
                                            <div><%# Eval("categoriaPaiId") %></div>
                                        </td>
                                        <td class="nome">
                                            <div><%# Eval("categoriaNome") %></div>
                                        </td>
                                        <td align="center">
                                            <a class="dxeHyperlink_Glass" href="produtoalt.aspx?produtoId=<%# Eval("produtoId") %>&produtoPaiId=<%# Eval("produtoId") %>&acao=alterar" target="_blank">
                                                <img src="images/btEditar.png" />
                                            </a>
                                        </td>

                                    </tr>

                                </ItemTemplate>
                            </asp:ListView>
                        </table>

                    </div>
                </div>
            </td>
        </tr>
    </table>
</asp:Content>

