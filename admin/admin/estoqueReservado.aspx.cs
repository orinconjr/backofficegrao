﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxGridView;

public partial class admin_estoqueReservado : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack && Request.QueryString["produtoId"] != null)
        {
            txtProduto.Text = Request.QueryString["produtoId"].ToString();

            if (Request.QueryString["trocarEtiqueta"] == null)
            {
                fillGrid(true);
            }
            else
            {
                fillGrid2(true);
            }
        }
    }

    private void fillGrid(bool rebind)
    {
        int produtoId = 0;
        int.TryParse(txtProduto.Text, out produtoId);

        var data = new dbCommerceDataContext();
        var pedidos = (from c in data.tbProdutoEstoques
                       where c.produtoId == produtoId && c.enviado == false && c.liberado == true && c.pedidoIdReserva != null
                       join d in data.tbItemPedidoEstoques on c.pedidoIdReserva equals d.tbItensPedido.pedidoId into aguardando
                       select new
                       {
                           pedidoId = c.pedidoIdReserva,
                           itemPedidoIdReserva = c.itemPedidoIdReserva,
                           c.idPedidoFornecedorItem,
                           itensAguardando = aguardando.Count(x => x.reservado == false && x.enviado == false && x.cancelado == false),
                           prazoAguardando = aguardando.Count(x => x.reservado == false && x.enviado == false && x.cancelado == false) > 0 ? aguardando.OrderByDescending(x => x.dataLimite).First().dataLimite : null,
                           prazoFinalAtual = c.tbPedido.prazoMaximoPostagemAtualizado,
                           separado = c.pedidoId == null ? false : true
                       }).Take(300).ToList().OrderByDescending(x => x.prazoAguardando);
        grd.DataSource = pedidos;
        if ((!Page.IsPostBack && !Page.IsCallback) | rebind) grd.DataBind();

        grdTrocaEtiqueta.Visible = false;
        grd.Visible = true;
    }

    private void fillGrid2(bool rebind)
    {
        int produtoId = 0;
        int.TryParse(txtProduto.Text, out produtoId);

        var data = new dbCommerceDataContext();
        var pedidos = (from c in data.tbProdutoEstoques
                       join p in data.tbPedidos on c.pedidoIdReserva equals p.pedidoId
                       where c.produtoId == produtoId && c.enviado == false && c.liberado == true && c.pedidoIdReserva != null
                       join d in data.tbItemPedidoEstoques on c.pedidoIdReserva equals d.tbItensPedido.pedidoId into aguardando
                       select new
                       {
                           pedidoId = c.pedidoIdReserva,
                           itemPedidoIdReserva = c.itemPedidoIdReserva,
                           c.idPedidoFornecedorItem,
                           itensAguardando = aguardando.Count(x => x.reservado == false && x.enviado == false && x.cancelado == false),
                           prazoAguardando = aguardando.Count(x => x.reservado == false && x.enviado == false && x.cancelado == false) > 0 ? aguardando.OrderByDescending(x => x.dataLimite).First().dataLimite : null,
                           p.prazoMaximoPostagemAtualizado,
                           separado = c.pedidoId == null ? false : true
                       }).Take(20).ToList().OrderByDescending(x => x.prazoMaximoPostagemAtualizado);
        grdTrocaEtiqueta.DataSource = pedidos;
        if ((!Page.IsPostBack && !Page.IsCallback) | rebind) grdTrocaEtiqueta.DataBind();

        grdTrocaEtiqueta.Visible = true;
        grd.Visible = false;

    }


    protected void btnGravar_OnClick(object sender, ImageClickEventArgs e)
    {

    }

    public void interacaoInclui(string interacao, string alteracaoDeEstatus, int pedidoId)
    {
        HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
        usuarioLogadoId = Request.Cookies["usuarioLogadoId"];
        if (usuarioLogadoId != null)
        {
            rnInteracoes.interacaoInclui(pedidoId, interacao, rnUsuarios.usuarioSeleciona_PorUsuarioId(int.Parse(usuarioLogadoId.Value.ToString())).Tables[0].Rows[0]["usuarioNome"].ToString(), alteracaoDeEstatus);
        }
    }

    protected void grd_OnCustomUnboundColumnData(object sender, ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "pedidoIdCliente")
        {
            int pedidoId = Convert.ToInt32(e.GetListSourceFieldValue("pedidoId"));
            e.Value = rnFuncoes.retornaIdCliente(pedidoId);
        }
    }


    protected void btnFiltrar_OnClick(object sender, EventArgs e)
    {
        int produtoId = 0;
        int.TryParse(txtProduto.Text, out produtoId);
        fillGrid(true);
    }
    protected void btnCancelar_OnCommand(object sender, CommandEventArgs e)
    {
        int idPedidoFornecedorItem = Convert.ToInt32(e.CommandArgument);
        var data = new dbCommerceDataContext();
        var pedidos = (from c in data.tbProdutoEstoques where c.idPedidoFornecedorItem == idPedidoFornecedorItem select c).First();
        if(pedidos.pedidoId != null)
        {
            Response.Write("<script>alert('Não é possível cancelar a reserva de um produto separado')</script>");
            return;
        }
        var itemPedidoEstoque = (from c in data.tbItemPedidoEstoques where c.idItemPedidoEstoque == pedidos.idItemPedidoEstoqueReserva select c).First();
        itemPedidoEstoque.reservado = false;
        itemPedidoEstoque.dataReserva = null;
        pedidos.pedidoIdReserva = null;
        pedidos.idItemPedidoEstoqueReserva = null;
        pedidos.itemPedidoIdReserva = null;
        data.SubmitChanges();
        var queue = new tbQueue();
        queue.agendamento = DateTime.Now;
        queue.andamento = false;
        queue.concluido = false;
        queue.idRelacionado = pedidos.produtoId;
        queue.mensagem = "";
        queue.tipoQueue = 21;
        data.tbQueues.InsertOnSubmit(queue);
        data.SubmitChanges();

        fillGrid(true);
    }

    protected void btnTrocarEtiqueta_OnCommand(object sender, CommandEventArgs e)
    {
        int idPedidoFornecedorItem = Convert.ToInt32(e.CommandArgument.ToString().Split('#')[0]);
        int indexRow = Convert.ToInt32(e.CommandArgument.ToString().Split('#')[1]);
        TextBox txtItemPedidoId = (TextBox)grdTrocaEtiqueta.FindRowCellTemplateControl(indexRow, grdTrocaEtiqueta.Columns["trocar"] as GridViewDataColumn, "txtItemPedidoId");

        if (string.IsNullOrEmpty(txtItemPedidoId.Text))
        {
            Response.Write("<script>alert('informe o itemPedidoId')</script>");
            return;
        }
        int itemPedidoEstoqueEtiqueta = Convert.ToInt32(txtItemPedidoId.Text);
        int produtoId = Convert.ToInt32(Request.QueryString["produtoId"]);

        using (var data = new dbCommerceDataContext())
        {
            try
            {
                var validarItemPedido =
                    (from c in data.tbItemPedidoEstoques where c.itemPedidoId == itemPedidoEstoqueEtiqueta && c.produtoId == produtoId && c.dataReserva == null && c.reservado == false select c).FirstOrDefault();

                if (validarItemPedido == null)
                {
                    Response.Write("<script>alert('itemPedidoId inválido')</script>");
                    return;
                }

                var pedidos = (from c in data.tbProdutoEstoques where c.idPedidoFornecedorItem == idPedidoFornecedorItem select c).First();

                if (pedidos.pedidoId != null)
                {
                    Response.Write("<script>alert('Não é possível trocar a reserva de um produto separado')</script>");
                    return;
                }
                var itemPedidoEstoque =
                    (from c in data.tbItemPedidoEstoques
                     where c.idItemPedidoEstoque == pedidos.idItemPedidoEstoqueReserva
                     select c).First();
                var itemPedidoEstoqueTroca = (from c in data.tbItemPedidoEstoques
                                              where c.itemPedidoId == itemPedidoEstoqueEtiqueta && c.produtoId == produtoId
                                              select c).First();


                itemPedidoEstoque.reservado = false;
                itemPedidoEstoque.dataReserva = null;

                HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
                usuarioLogadoId = Request.Cookies["usuarioLogadoId"];
                if (usuarioLogadoId != null)
                {
                    rnInteracoes.interacaoInclui(itemPedidoEstoque.tbItensPedido.pedidoId, "Removida etiqueta reservada " + pedidos.idPedidoFornecedorItem + " - " + itemPedidoEstoque.tbItensPedido.tbProduto.produtoNome + " e repassada para o pedido " + itemPedidoEstoqueTroca.tbItensPedido.pedidoId, rnUsuarios.usuarioSeleciona_PorUsuarioId(int.Parse(usuarioLogadoId.Value)).Tables[0].Rows[0]["usuarioNome"].ToString(), "False");
                }

                pedidos.pedidoIdReserva = itemPedidoEstoqueTroca.tbItensPedido.pedidoId;
                pedidos.idItemPedidoEstoqueReserva = itemPedidoEstoqueTroca.idItemPedidoEstoque;
                pedidos.itemPedidoIdReserva = itemPedidoEstoqueTroca.itemPedidoId;

                itemPedidoEstoqueTroca.reservado = true;
                itemPedidoEstoqueTroca.dataReserva = DateTime.Now;

                if (usuarioLogadoId != null)
                {
                    rnInteracoes.interacaoInclui(itemPedidoEstoqueTroca.tbItensPedido.pedidoId, "Reservada etiqueta " + pedidos.idPedidoFornecedorItem + " - " + itemPedidoEstoque.tbItensPedido.tbProduto.produtoNome + " que foi removida do pedido " + itemPedidoEstoque.tbItensPedido.pedidoId, rnUsuarios.usuarioSeleciona_PorUsuarioId(int.Parse(usuarioLogadoId.Value)).Tables[0].Rows[0]["usuarioNome"].ToString(), "False");
                }

                #region Manda para tabela de Queue para poder verificar se coloca na fila de completos

                var queue = new tbQueue
                {
                    agendamento = DateTime.Now,
                    andamento = false,
                    concluido = false,
                    idRelacionado = itemPedidoEstoqueTroca.tbItensPedido.pedidoId,
                    tipoQueue = 8,
                    mensagem = ""
                };

                data.tbQueues.InsertOnSubmit(queue);
                data.SubmitChanges();
                #endregion

                Response.Write("<script>alert('Troca de etiqueta realizada com sucesso')</script>");

                System.Threading.Thread.Sleep(2000);

                //se completo prioriza como 1
                var priorizarPedido =
                    (from c in data.tbPedidos where c.pedidoId == itemPedidoEstoqueTroca.tbItensPedido.pedidoId select c)
                        .FirstOrDefault();

                if (priorizarPedido.envioLiberado && priorizarPedido.statusDoPedido == 11)
                {
                    priorizarPedido.prioridadeEnvio = 1;
                    data.SubmitChanges();
                }

            }
            catch (Exception ex)
            {

            }


        }

        fillGrid2(true);

    }

    protected void grdTrocaEtiqueta_OnHtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        //if (e.RowType != GridViewRowType.Data)
        //return;

        //Button btnExportarNota = (Button)grdTrocaEtiqueta.FindRowCellTemplateControl(e.VisibleIndex, grdTrocaEtiqueta.Columns["trocar"] as GridViewDataColumn, "btnTrocarEtiqueta");
        //TextBox txtItemPedidoId = (TextBox)grdTrocaEtiqueta.FindRowCellTemplateControl(e.VisibleIndex, grdTrocaEtiqueta.Columns["trocar"] as GridViewDataColumn, "txtItemPedidoId");

        //RequiredFieldValidator rqf1 = new RequiredFieldValidator { ControlToValidate = txtItemPedidoId.ClientID };

    }
}