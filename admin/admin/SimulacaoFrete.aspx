﻿<%@ Page Title="" Theme="Glass" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="simulacaoFrete.aspx.cs" Inherits="admin_simulacaoFrete" %>

<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxTimer" TagPrefix="dxt" %>
<%@ Register TagPrefix="dxe" Namespace="DevExpress.Web.ASPxMenu" Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxPopupControl" Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxEditors" Assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>
<%@ Register Assembly="CuteEditor" Namespace="CuteEditor" TagPrefix="CE" %>
<%@ Register Assembly="DevExpress.Web.ASPxPivotGrid.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPivotGrid" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v11.1" Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>

<%@ Register assembly="DevExpress.Web.ASPxPivotGrid.v11.1" namespace="DevExpress.Web.ASPxPivotGrid" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" runat="Server">
    <asp:ScriptManager ID="scriptManager" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="updPanel" runat="server">
        <ContentTemplate>
            <table cellpadding="0" cellspacing="0" style="width: 920px">
                <tr>
                    <td class="tituloPaginas" valign="top">
                        <asp:Label ID="lblAcao" runat="server">Simulação de Frete</asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblMensagem" runat="server" ForeColor="Red"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
                            <tr>
                                <td class="rotulos">
                                    <b>Data Início:</b><br />
                                    <dx:ASPxDateEdit ID="dataEditInicial" runat="server" OnInit="dataEditInicial_Init"></dx:ASPxDateEdit>
                                </td>
                                <td class="rotulos">
                                    <b>Data Final:</b><br />
                                    <dx:ASPxDateEdit ID="dataEditFinal" runat="server" OnInit="dataEditFinal_Init"></dx:ASPxDateEdit>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:Panel ID="pnlGrid" Visible="false" runat="server">
                                        <dxwgv:ASPxGridView ID="grdTransportadoras" ClientInstanceName="grdTransportadoras" runat="server" AutoGenerateColumns="False"
                                            KeyFieldName="tipoDeEntregaId" Width="834px" Settings-ShowFilterRow="True" Settings-ShowFilterBar="Visible"
                                            Cursor="auto" EnableViewState="False">
                                            <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                                            <Styles>
                                                <Footer Font-Bold="True">
                                                </Footer>
                                            </Styles>
                                            <SettingsText ConfirmDelete="Tem certeza que deseja excluir?" EmptyDataRow="Nenhum registro encontrado." />
                                            <SettingsPager Position="TopAndBottom" PageSize="500"
                                                ShowDisabledButtons="False" AlwaysShowPager="True">
                                                <Summary AllPagesText="Páginas: {0} - {1} ({2} items)"
                                                    Text="Página {0} de {1} ({2} registros encontrados)" />
                                            </SettingsPager>
                                            <Settings ShowFilterRow="false" ShowGroupButtons="False" ShowFooter="True" />
                                            <SettingsEditing EditFormColumnCount="4"
                                                PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1"
                                                PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter"
                                                PopupEditFormWidth="700px" />
                                            <Columns>
                                                <dxwgv:GridViewDataCheckColumn VisibleIndex="0" Width="10px">
                                                    <DataItemTemplate>
                                                        <dx:ASPxCheckBox ID="chk" runat="server" AllowGrayed="true" ValueType="System.Int32"
                                                            ValueChecked='<%# Eval("tipoDeEntregaId") %>' ValueUnchecked="" OnInit="chk_Init">
                                                        </dx:ASPxCheckBox>
                                                    </DataItemTemplate>
                                                    <EditFormSettings Visible="False" />
                                                </dxwgv:GridViewDataCheckColumn>
                                                <dxwgv:GridViewDataTextColumn Caption="Transportadora" Width="250px" FieldName="idTransportadoraString" VisibleIndex="0">
                                                    <Settings ShowInFilterControl="False" />
                                                    <EditFormSettings Visible="False" />
                                                </dxwgv:GridViewDataTextColumn>
                                                <dxwgv:GridViewDataTextColumn Caption="Tabela" Width="250px" Name="" FieldName="tipoDeEntregaNome" VisibleIndex="0">
                                                    <Settings ShowFilterRowMenu="False" />
                                                    <EditFormSettings Visible="False" />
                                                </dxwgv:GridViewDataTextColumn>
                                            </Columns>
                                            <StylesEditors>
                                                <Label Font-Bold="True">
                                                </Label>
                                            </StylesEditors>
                                        </dxwgv:ASPxGridView>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlResult" Visible="false" runat="server">
                                        <dx:ASPxPivotGrid OptionsData-DataFieldUnboundExpressionMode="UseSummaryValues" ID="grd" runat="server"
                                            CustomizationFieldsLeft="600" CustomizationFieldsTop="400" ClientInstanceName="pivotGrid" Width="100%">
                                            <Fields>
                                                <dx:PivotGridField Area="RowArea" FieldName="data"  Caption="Data" ID="Data" ValueFormat-FormatType="Custom"  CellFormat-FormatString="mm/YYYY" />
                                                <dx:PivotGridField Area="RowArea" FieldName="transportadora" Caption="Transportadora" ID="ColumTransportadora" />
                                                <dx:PivotGridField Area="RowArea" FieldName="valor" Caption="Valor" ID="ColumnValor" />
                                                <dx:PivotGridField Area="FilterArea" FieldName="Data"  Caption="Data" ID="ColumnData" ValueFormat-FormatType="DateTime" CellFormat-FormatString="d" />
                                                <dx:PivotGridField Area="DataArea" FieldName="valor" Caption="Valor" ID="QtdValor" />
                                                <dx:PivotGridField Area="DataArea" FieldName="volume" Caption="Volumes" ID="QtdVolume" />
                                            </Fields>
                                            <OptionsView ShowFilterHeaders="True" ShowHorizontalScrollBar="True" ShowColumnGrandTotals="False" />
                                            <OptionsCustomization AllowSortBySummary="True" AllowSort="True" AllowFilterInCustomizationForm="True"></OptionsCustomization>
                                            <OptionsPager RowsPerPage="100"></OptionsPager>
                                        </dx:ASPxPivotGrid>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2" align="right">
                                    <dx:ASPxButton ID="btnVerSimulacao" runat="server" Text="Ver Simulação" OnClick="btnVerSimulacao_Click"></dx:ASPxButton>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr class="rotulos">
                    <td style="padding-top: 30px; font-size: 20px;">&nbsp;
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>



</asp:Content>

