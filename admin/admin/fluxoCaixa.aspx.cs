﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxPivotGrid;
using DevExpress.XtraPivotGrid;

public partial class admin_fluxoCaixa : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (txtDataInicial.Text == "")
            {
                txtDataInicial.Text = DateTime.Now.ToShortDateString();
            }
            if (txtDataFinal.Text == "")
            {
                txtDataFinal.Text = DateTime.Now.AddMonths(1).ToShortDateString();
            }
        }
        fillGrid(false);
    }

    private void fillGrid(bool rebind)
    {
        var dataInicial = DateTime.Now;
        var dataFinal = DateTime.Now;
        DateTime.TryParse(txtDataInicial.Text, out dataInicial);
        DateTime.TryParse(txtDataFinal.Text, out dataFinal);

        var hoje = DateTime.Now;
        var data = new dbCommerceDataContext();
        //var ultimosAjustes = (from c in data.tbCaixaAjustes where c.data < dataInicial orderby c.data descending select new
        //{
        //    pago = true,
        //    dataVencimento = c.data,
        //    valor = c.valorNovo,
        //    agrupador = "Ajuste de Caixa"
        //}).ToList();

        //var ultimoAjuste = ultimosAjustes.FirstOrDefault();

        var recebimentosAnterioresCartoesPendentes = (from c in data.tbPedidoPagamentos
        join d in data.tbOperadorPagamentos on c.idOperadorPagamento equals d.idOperadorPagamento
        where c.cancelado == false && c.recebimentoParcelado == false && c.tbPedido.statusDoPedido != 6 && c.pago == false && c.idOperadorPagamento != 1
        && (c.dataPagamento ?? c.dataVencimento) < dataInicial && c.consolidado
        select new
        {
            c.pago,
            dataVencimento = c.dataPagamento ?? c.dataVencimento,
            c.valor,
            agrupador = d.nome
        }).ToList();
        decimal recebimentosAnterioresCartoesPendentesAcumulado = 0;
        if (recebimentosAnterioresCartoesPendentes.Any())
        {
            recebimentosAnterioresCartoesPendentesAcumulado = recebimentosAnterioresCartoesPendentes.Sum(x => x.valor);
        }
        var valorAcumuladoCartoesPendente = (from c in data.tbCaixaAjustes
                                        where c.data < dataInicial
                                        orderby c.data
                                        select new
                                        {
                                            pago = true,
                                            dataVencimento = dataInicial,
                                            valor = recebimentosAnterioresCartoesPendentesAcumulado,
                                            agrupador = "Recebimentos Anteriores Pendentes"
                                        }).Take(1).ToList();

        var recebimentosAnterioresCartoesPago = (from c in data.tbPedidoPagamentos
        join d in data.tbOperadorPagamentos on c.idOperadorPagamento equals d.idOperadorPagamento
                                                 where c.cancelado == false && c.consolidado && c.recebimentoParcelado == false && c.tbPedido.statusDoPedido != 6 && c.pago == true && c.idOperadorPagamento != 1
        && (c.dataPagamento ?? c.dataVencimento) < dataInicial
        select new
        {
            c.pago,
            dataVencimento = c.dataPagamento ?? c.dataVencimento,
            c.valor,
            agrupador = d.nome
        }).ToList();
        decimal recebimentosAnterioresCartoesPagoAcumulado = 0;
        if (recebimentosAnterioresCartoesPago.Any())
        {
            recebimentosAnterioresCartoesPagoAcumulado = recebimentosAnterioresCartoesPago.Sum(x => x.valor);
        }

        var valorAcumuladoCartoesPago = (from c in data.tbCaixaAjustes
                                             where c.data < dataInicial
                                             orderby c.data
                                             select new
                                             {
                                                 pago = true,
                                                 dataVencimento = dataInicial,
                                                 valor = recebimentosAnterioresCartoesPagoAcumulado,
                                                 agrupador = "Recebimentos Anteriores Pagos"
                                             }).Take(1).ToList();

        var recebimentosAnterioresBoletosPendentes = (from c in data.tbPedidoPagamentos
        join d in data.tbOperadorPagamentos on c.idOperadorPagamento equals d.idOperadorPagamento
        where c.cancelado == false && c.recebimentoParcelado == false && c.tbPedido.statusDoPedido != 6 && c.pago == false && c.idOperadorPagamento == 1
        && (c.dataPagamento ?? c.dataVencimento) < dataInicial
        select new
        {
            c.pago,
            dataVencimento = c.dataPagamento ?? c.dataVencimento,
            c.valor,
            agrupador = d.nome
        }).ToList();
        decimal recebimentosAnterioresBoletosPendentesAcumulado = 0;
        if (recebimentosAnterioresBoletosPendentes.Any())
        {
            recebimentosAnterioresBoletosPendentesAcumulado = recebimentosAnterioresBoletosPendentes.Sum(x => x.valor);
        }
        var valorAcumuladoBoletoPendente = (from c in data.tbCaixaAjustes
                                         where c.data < dataInicial
                                         orderby c.data
                                         select new
                                         {
                                             pago = true,
                                             dataVencimento = dataInicial,
                                             valor = recebimentosAnterioresBoletosPendentesAcumulado,
                                             agrupador = "Recebimentos Boleto Pendentes"
                                         }).Take(1).ToList();

        var recebimentosAnterioresBoletosPago = (from c in data.tbPedidoPagamentos
        join d in data.tbOperadorPagamentos on c.idOperadorPagamento equals d.idOperadorPagamento
                                                 where c.cancelado == false && c.recebimentoParcelado == false && c.tbPedido.statusDoPedido != 6 && c.pago == true && c.idOperadorPagamento == 1
        && (c.dataPagamento ?? c.dataVencimento) < dataInicial
        select new
        {
            c.pago,
            dataVencimento = c.dataPagamento ?? c.dataVencimento,
            c.valor,
            agrupador = d.nome
        }).ToList();
        decimal recebimentosAnterioresBoletosPagoAcumulado = 0;
        if (recebimentosAnterioresBoletosPendentes.Any())
        {
            recebimentosAnterioresBoletosPagoAcumulado = recebimentosAnterioresBoletosPago.Sum(x => x.valor);
        }
        var valorAcumuladoBoletoPago = (from c in data.tbCaixaAjustes
                                            where c.data < dataInicial
                                            orderby c.data
                                            select new
                                            {
                                                pago = true,
                                                dataVencimento = dataInicial,
                                                valor = recebimentosAnterioresBoletosPagoAcumulado,
                                                agrupador = "Recebimentos Boleto Pagos"
                                            }).Take(1).ToList();

        var despesasAnteriores = (from c in data.tbDespesas
        where (c.dataPagamento ?? c.vencimento) < dataInicial
        select new
        {
            c.pago,
            dataVencimento = c.dataPagamento ?? c.vencimento,
            valor = c.valor * (-1),
            agrupador = c.tbDespesaCategoria.despesaCategoria
        }).ToList();

        decimal despesasAnterioresAcumulado = 0;
        if (despesasAnteriores.Any())
        {
            despesasAnterioresAcumulado += despesasAnteriores.Sum(x => x.valor);
        }
        var valorAcumuladoDespesas = (from c in data.tbCaixaAjustes
                                        where c.data < dataInicial
                                        orderby c.data
                                        select new
                                        {
                                            pago = true,
                                            dataVencimento = dataInicial,
                                            valor = despesasAnterioresAcumulado,
                                            agrupador = "Despesas Anteriores"
                                        }).Take(1).ToList();

        var pedidosFornecedorAnteriores = (from c in data.tbPedidoFornecedorItems
                                           where c.tbPedidoFornecedor.dataVencimento < dataInicial && c.tbPedidoFornecedor.dataVencimento != null
                                           group c by c.tbPedidoFornecedor.dataEntrega into pedidos
        select new
        {
            pago = true,
            dataVencimento = (DateTime)pedidos.FirstOrDefault().tbPedidoFornecedor.dataVencimento,
            valor = pedidos.Sum(x => x.custo) * (-1),
            agrupador = "Custos de Produtos"
        }).ToList();

        decimal pedidosFornecedorAnterioresAcumulado = 0;
        if (pedidosFornecedorAnteriores.Any())
        {
            pedidosFornecedorAnterioresAcumulado -= pedidosFornecedorAnteriores.Sum(x => x.valor);
        }
        var valorAcumuladoFornecedor = (from c in data.tbCaixaAjustes
                                        where c.data < dataInicial
                                        orderby c.data
                                        select new
                                        {
                                            pago = true,
                                            dataVencimento = dataInicial,
                                            valor = pedidosFornecedorAnterioresAcumulado,
                                            agrupador = "Custo de Produtos Anteriores"
                                        }).Take(1).ToList();

        var recebimentosGeral = (from c in data.tbPedidoPagamentos
            join d in data.tbOperadorPagamentos on c.idOperadorPagamento equals d.idOperadorPagamento
                                 where c.cancelado == false && c.recebimentoParcelado == false && c.tbPedido.statusDoPedido != 6 && c.consolidado && c.idOperadorPagamento != 1
            && (c.dataPagamento ?? c.dataVencimento) >= dataInicial && (c.dataPagamento ?? c.dataVencimento) <= dataFinal
            select new
            {
                c.pago,
                dataVencimento = c.dataPagamento ?? c.dataVencimento,
                c.valor,
                agrupador = d.nome
            }).ToList();

        var recebimentosBoleto = (from c in data.tbPedidoPagamentos
            join d in data.tbOperadorPagamentos on c.idOperadorPagamento equals d.idOperadorPagamento
            where c.cancelado == false && c.recebimentoParcelado == false && c.tbPedido.statusDoPedido != 6 && c.idOperadorPagamento == 1
            && (c.dataPagamento ?? c.dataVencimento) >= dataInicial && (c.dataPagamento ?? c.dataVencimento) <= dataFinal
            select new
            {
                c.pago,
                dataVencimento = c.dataPagamento ?? c.dataVencimento,
                c.valor,
                agrupador = d.nome
            }).ToList();

        var despesasGeral = (from c in data.tbDespesas
                             where (c.dataPagamento ?? c.vencimento) >= dataInicial && (c.dataPagamento ?? c.vencimento) <= dataFinal
                             select new
                             {
                                 c.pago,
                                 dataVencimento = (DateTime)(c.dataPagamento == null ? c.vencimento == null ? c.data : c.vencimento : c.dataPagamento),
                                 valor = c.valor * (-1),
                                 agrupador = c.tbDespesaCategoria.despesaCategoria
                             }).ToList();

        var custosFornecedorGeral = (from c in data.tbPedidoFornecedorItems
                                     where c.tbPedidoFornecedor.dataVencimento >= dataInicial && c.tbPedidoFornecedor.dataVencimento <= dataFinal && c.tbPedidoFornecedor.dataVencimento != null
                                     group c by c.tbPedidoFornecedor.dataEntrega into pedidos
                                     select new
                                     {
                                         pago = true,
                                         dataVencimento = (DateTime)pedidos.FirstOrDefault().tbPedidoFornecedor.dataVencimento,
                                         valor = pedidos.Sum(x => x.custo) * (-1),
                                         agrupador = "Custos de Produtos"
                                     }).ToList();


        var resultadoFinal = recebimentosGeral.Union(valorAcumuladoCartoesPendente).ToList()
            .Union(recebimentosBoleto).ToList()
            .Union(valorAcumuladoCartoesPago).ToList()
            .Union(valorAcumuladoBoletoPendente).ToList()
            .Union(valorAcumuladoBoletoPago).ToList()
            .Union(valorAcumuladoDespesas).ToList()
            .Union(valorAcumuladoFornecedor).ToList()
            .Union(despesasGeral).ToList()
            .Union(custosFornecedorGeral).ToList();
        grd.DataSource = resultadoFinal;
        if ((!Page.IsPostBack && !Page.IsCallback) | rebind)
        {
            grd.DataBind();
        }
        
    }

    protected void imbInsert_OnClick(object sender, ImageClickEventArgs e)
    {
        fillGrid(true);
    }

    protected void grd_OnCustomCellValue(object sender, PivotCellValueEventArgs e)
    {
        var datafield = e.DataField;

        if (e.DataField.FieldName.ToLower() == "valor")
        {
            PivotDrillDownDataSource ds = e.CreateDrillDownDataSource();
            double value = 0;
            Dictionary<PivotGridField, object> rowFieldValues = new Dictionary<PivotGridField, object>();
            PivotGridField[] rowFields = e.GetRowFields();
            foreach (PivotGridField field in rowFields)
                rowFieldValues[field] = e.GetFieldValue(field);
            for (int i = 0; i < ds.RowCount; i++)
            {
                bool skip = false;
                foreach (PivotGridField field in rowFields)
                {
                    if (!Comparer.Equals(ds[i][field], rowFieldValues[field]))
                    {
                        skip = true;
                        break;
                    }
                }
                if (skip)
                    continue;
                double v1 = Convert.ToDouble(ds[i][e.DataField]);
                value += v1;
            }
            e.Value = value;
        }
    }
}