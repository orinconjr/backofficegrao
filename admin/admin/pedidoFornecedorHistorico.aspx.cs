﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CarlosAg.ExcelXmlWriter;
using DevExpress.Web.ASPxGridView;

public partial class admin_pedidoFornecedorHistorico : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        fillGrid(false);
    }


    private void fillGrid(bool rebind)
    {
        var fornecedorDc = new dbCommerceDataContext();
        var historicoAcesso = (from c in fornecedorDc.tbFornecedorUsuarioAcessos
            select new
            {
                data = c.dataHora,
                tipo = "Acesso",
                fornecedor = c.tbProdutoFornecedor.fornecedorNome,
                idPedido = 0,
                c.tbFornecedorUsuario.nome
            });
        var historicoExportacao = (from c in fornecedorDc.tbFornecedorExportacaos
            select new
            {
                data = c.dataHora,
                tipo = "Exportação",
                fornecedor = c.tbFornecedorUsuario.tbProdutoFornecedor.fornecedorNome,
                idPedido = c.idPedidoFornecedor,
                c.tbFornecedorUsuario.nome
            });
        var historico = historicoAcesso.Union(historicoExportacao).OrderByDescending(x => x.data);
        grd.DataSource = historico;
        if ((!Page.IsPostBack && !Page.IsCallback) | rebind) grd.DataBind();

        
    }
}