﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_comprasAlterarStatus : System.Web.UI.Page
{
    public class StatusCompra
    {
        public StatusCompra(int idStatusOrdemCompra,string statusOrdemCompra)
        {
            this.idStatusOrdemCompra = idStatusOrdemCompra;
            this.statusOrdemCompra = statusOrdemCompra;
        }
        public int idStatusOrdemCompra { get; set; }
        public string statusOrdemCompra { get; set; }
    }
    List<StatusCompra> statusCompras = new List<StatusCompra>{
            new StatusCompra(0,"Pendente"),
            new StatusCompra(1,"Aguardando Aprovação"),
            new StatusCompra(2,"Aprovada"),
            new StatusCompra(3,"Aguardando Financeiro"),
            new StatusCompra(4,"Finalizado"),
            new StatusCompra(5,"Cancelado"),
            new StatusCompra(6,"Aguardando Conferência de Entrega"),
        };
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnBuscarStatus_Click(object sender, EventArgs e)
    {
        var data = new dbCommerceDataContext();
        
         int idComprasOrdem = Convert.ToInt32(txtItem.Text);
         var compra = (from c in data.tbComprasOrdems where c.idComprasOrdem == idComprasOrdem select c).FirstOrDefault();
        if(compra != null)
        {
            try
            {
                StatusCompra statusAtual = (from c in statusCompras where c.idStatusOrdemCompra == compra.statusOrdemCompra select c).FirstOrDefault();
                txtStatusAtual.Text = statusAtual.idStatusOrdemCompra + " - " + statusAtual.statusOrdemCompra;
            }
            catch(Exception ex)
            {
                txtStatusAtual.Text = "Status atual: " + compra.statusOrdemCompra + " - Não identificado";
            }
           
        }

    }

    bool validarCampos()
    {
        bool ok = true;
        string mensagem = "";
        int idComprasOrdem = 0;
        try
        {
            idComprasOrdem = Convert.ToInt32(txtItem.Text);
        }
        catch (Exception) {
                   
        }

        if(idComprasOrdem == 0)
        {
            ok = false;
            mensagem += @"Id do item inválido\n";
        }

        if(ddlAlterarStatusPara.SelectedItem.Value == "")
        {
            ok = false;
            mensagem += "Selecione um Status válido";
        }
        if(!ok)
        {
            string meuscript = @"alert('" + mensagem + "');";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "MyScript", meuscript, true);
            
        }
        return ok;
       
    }
    protected void btnAlterar_Click(object sender, EventArgs e)
    {
        if (!validarCampos())
            return;

        var data = new dbCommerceDataContext();

        int idComprasOrdem = Convert.ToInt32(txtItem.Text);
        var compra = (from c in data.tbComprasOrdems where c.idComprasOrdem == idComprasOrdem select c).FirstOrDefault();
        if (compra != null)
        {
            string mensagem = "";
            try{
                compra.statusOrdemCompra = Convert.ToInt32(ddlAlterarStatusPara.SelectedItem.Value);
                data.SubmitChanges();
                mensagem = "Status alterado com sucesso";

            }
            catch (Exception ex)
            {
                mensagem = ex.Message;
            }
            string meuscript = @"alert('" + mensagem + "');";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "MyScript", meuscript, true);
        }
    }
}