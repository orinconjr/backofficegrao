﻿<%@ Page Title="" Theme="Glass" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="pedidosQueVaoAtrasar.aspx.cs" Inherits="admin_PedidosQueVaoAtrasar" %>

<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxTimer" TagPrefix="dxt" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" runat="Server">

    <table cellpadding="0" cellspacing="0" style="width: 920px">
        <tr>
            <td class="tituloPaginas" valign="top">
                <asp:Label ID="lblAcao" runat="server">Pedidos que vão atrasar</asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
                    <tr>
                        <td align="right">
                            <table cellpadding="0" cellspacing="0" style="width: 834px">
                                <tr>
                                    <td align="right">&nbsp;</td>
                                    <td height="38"
                                        style="background-image: url('images/bkExportar.jpg'); padding-bottom: 1px;"
                                        valign="bottom" width="231">
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 97px">&nbsp;</td>
                                                <td style="width: 33px">
                                                    <asp:ImageButton ID="btPdf" runat="server" ImageUrl="~/admin/images/btPdf.jpg" OnClick="btPdf_Click"
                                                        Style="margin-left: 5px" />
                                                </td>
                                                <td style="width: 31px">
                                                    <asp:ImageButton ID="btXsl" runat="server" ImageUrl="~/admin/images/btXls.jpg" OnClick="btXsl_Click"
                                                        Style="margin-left: 5px" />
                                                </td>
                                                <td style="width: 36px">
                                                    <asp:ImageButton ID="btRtf" runat="server" ImageUrl="~/admin/images/btRtf.jpg" OnClick="btRtf_Click"
                                                        Style="margin-left: 5px" />
                                                </td>
                                                <td valign="bottom">
                                                    <asp:ImageButton ID="btCsv" runat="server" ImageUrl="~/admin/images/btCsv.jpg" OnClick="btCsv_Click"
                                                        Style="margin-left: 5px" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>                        
                        <td>                                           
                            <asp:RadioButton runat="server" ID="rdbAtrasados" Text="Atrasados" Checked="True" GroupName="filtroAtraso" AutoPostBack="True" OnCheckedChanged="rdbAtrasados_CheckedChanged" />
                                        - 
                            <asp:RadioButton runat="server" ID="rdbTodos" Text="Todos" Checked="False" GroupName="filtroAtraso" AutoPostBack="True" OnCheckedChanged="rdbAtrasados_CheckedChanged" />                            
                        </td>
                    </tr>
                    <tr>
                        <td>
                            
                            <dxwgv:ASPxGridViewExporter ID="grdEx" runat="server" FileName="pedidos"
                                GridViewID="grd" Landscape="True" PreserveGroupRowStates="True">
                            </dxwgv:ASPxGridViewExporter>
                            <dxwgv:ASPxGridView ID="grd" runat="server" ClientInstanceName="grd" AutoGenerateColumns="False" Width="834px" Cursor="auto"
                                KeyFieldName="pedidoId">
                                <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                                <Styles>
                                    <Footer Font-Bold="True">
                                    </Footer>
                                </Styles>
                                <SettingsText ConfirmDelete="Tem certeza que deseja excluir?" EmptyDataRow="Nenhum registro encontrado." />
                                <SettingsPager Position="TopAndBottom" PageSize="500" ShowDisabledButtons="False" AlwaysShowPager="True">
                                    <Summary AllPagesText="Páginas: {0} - {1} ({2} items)" Text="Página {0} de {1} ({2} registros encontrados)" />
                                </SettingsPager>
                                <TotalSummary>
                                    <dxwgv:ASPxSummaryItem FieldName="sumarioPedidos" ShowInColumn="Pedido" ShowInGroupFooterColumn="Pedido" SummaryType="Sum" />
                                </TotalSummary>
                                <Settings ShowFilterRow="True" ShowGroupButtons="False" ShowHeaderFilterButton="True" ShowFooter="True" />
                                <SettingsEditing EditFormColumnCount="4" PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1" PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter" PopupEditFormWidth="700px" />
                                <Columns>
                                    <dxwgv:GridViewDataTextColumn Caption="#" VisibleIndex="0" Width="50">
                                        <DataItemTemplate>
                                            <%# Container.VisibleIndex + 1 %>
                                        </DataItemTemplate>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Pedido" FieldName="pedidoId" VisibleIndex="0" Width="50" Visible="true">
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="ID Produto" FieldName="produtoId" VisibleIndex="0" Visible="true" Width="50px">
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Nome do Produto" FieldName="nomeProduto" VisibleIndex="0" Visible="true" Width="120px">
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Fornecedor" FieldName="fornecedorNome" VisibleIndex="0">
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Data do Pedido" FieldName="dataDoPedido" VisibleIndex="0">
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Data Limite Produto" FieldName="dataLimiteProduto" VisibleIndex="0">
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataDateColumn Caption="Data Prevista Romaneio" FieldName="dataLimiteFornecedor" VisibleIndex="0">
                                    </dxwgv:GridViewDataDateColumn>
                                    <dxwgv:GridViewDataDateColumn Caption="Romaneio" FieldName="idPedidoFornecedor" VisibleIndex="0">
                                    </dxwgv:GridViewDataDateColumn>                                    
                                    <dxwgv:GridViewDataTextColumn Caption="Motivo Atraso" FieldName="motivoAtraso" VisibleIndex="0">
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Dias de atraso" FieldName="diaPrevistoAtraso" VisibleIndex="0">
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataHyperLinkColumn Caption="Editar" FieldName="pedidoId" VisibleIndex="7" Width="30px">
                                        <PropertiesHyperLinkEdit ImageUrl="~/admin/images/btEditar.jpg" NavigateUrlFormatString="pedido.aspx?pedidoId={0}" Target="_blank">
                                        </PropertiesHyperLinkEdit>
                                        <PropertiesHyperLinkEdit Text="" NavigateUrlFormatString="pedido.aspx?pedidoId={0}" Target="_blank">
                                        </PropertiesHyperLinkEdit>
                                        <Settings AllowAutoFilter="False" />
                                        <HeaderTemplate>
                                            <img alt="" src="images/legendaEditar.jpg" />
                                        </HeaderTemplate>
                                    </dxwgv:GridViewDataHyperLinkColumn>
                                    <dxwgv:GridViewDataHyperLinkColumn Caption="Editar" FieldName="produtoId" ToolTip="Fila de Estoque" VisibleIndex="8" Width="30px">
                                        <PropertiesHyperLinkEdit Text="Fila Estoque" NavigateUrlFormatString="filaEstoque.aspx?produtoId={0}" Target="_blank">
                                        </PropertiesHyperLinkEdit>
                                        <PropertiesHyperLinkEdit Text="Fila Estoque" NavigateUrlFormatString="filaEstoque.aspx?produtoId={0}" Target="_blank">
                                        </PropertiesHyperLinkEdit>
                                        <Settings AllowAutoFilter="False" />
                                        <HeaderTemplate>
                                            <img alt="" src="images/legendaEditar.jpg" />
                                        </HeaderTemplate>
                                    </dxwgv:GridViewDataHyperLinkColumn>
                                </Columns>
                                <StylesEditors>
                                    <Label Font-Bold="True">
                                    </Label>
                                </StylesEditors>
                            </dxwgv:ASPxGridView>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right;"></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
