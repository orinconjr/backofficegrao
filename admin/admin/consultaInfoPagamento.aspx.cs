﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_consultaInfoPagamento : System.Web.UI.Page
{
    public class pedidoPagamento
    {
        public int pedidoId {get;set;}
        public string numeroCobranca { get;set;}
        public decimal valor { get; set; }
        public string situacao{get;set;}
    }
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void FillGrid()
    {
        try
        {
            List<string> nsu = new List<string>(txtNsu.Text.TrimStart('0').Split(',').Select(n => Convert.ToInt64(n).ToString())).ToList();
 
            using (var data = new dbCommerceDataContext())
            {

                List<pedidoPagamento> pedidosPagamento = new List<pedidoPagamento>();

                foreach(var item in nsu)
                {

                    List<pedidoPagamento> pedpagamento = (from c in data.tbPedidoPagamentos
                                                           where c.numeroCobranca.EndsWith(item)
                                                           select new pedidoPagamento
                                                           {

                                                               pedidoId = c.pedidoId,
                                                               numeroCobranca = c.numeroCobranca,
                                                               valor = c.valor,
                                                               situacao = c.tbPedido.tbPedidoSituacao.situacao

                                                           }).ToList<pedidoPagamento>();
                    foreach(var item2 in pedpagamento)
                    {
                        pedidosPagamento.Add(item2);
                    }

                }
              //  var pedidos = (from c in data.tbPedidoPagamentos where nsu.Contains(c.numeroCobranca) select new { c.pedidoId, c.numeroCobranca, c.valor, c.tbPedido.tbPedidoSituacao.situacao }).ToList();

                grd.DataSource = pedidosPagamento;
                grd.DataBind();

                if (grd.Rows.Count != 0) return;
                grd.DataSource = (from c in data.tbPedidoPagamentoGateways
                                  join p in data.tbPedidoPagamentos on c.idPedidoPagamento equals p.idPedidoPagamento
                                  where nsu.Contains(c.processorReferenceNumber)
                                  select new { numeroCobranca = c.processorReferenceNumber, p.pedidoId, p.valor, p.tbPedido.tbPedidoSituacao.situacao }).ToList();
                grd.DataBind();

                if (grd.Rows.Count != 0) return;
                grd.DataSource = (from c in data.tbPedidoPagamentoGateways
                                  join p in data.tbPedidoPagamentos on c.idPedidoPagamento equals p.idPedidoPagamento
                                  where nsu.Contains(c.authorizationCode)
                                  select new { numeroCobranca = c.processorReferenceNumber, p.pedidoId, p.valor, p.tbPedido.tbPedidoSituacao.situacao }).ToList();
                grd.DataBind();

                if (grd.Rows.Count != 0) return;
                grd.DataSource = (from c in data.tbPedidoPagamentoGateways
                                  join p in data.tbPedidoPagamentos on c.idPedidoPagamento equals p.idPedidoPagamento
                                  where nsu.Contains(c.transactionId)
                                  select new { numeroCobranca = c.processorReferenceNumber, p.pedidoId, p.valor, p.tbPedido.tbPedidoSituacao.situacao }).ToList();
                grd.DataBind();

                ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('NSU não encontrado na base de dados!');", true);
            }
        }
        catch (Exception ex)
        {
            string erro = ex.Message;
            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('Falha na listagem dos dados, verifique os dados digitados');", true);
        }

    }

    private void FillGridMaxiPago()
    {
        try
        {
            string orderId = txtCodMaxiPago.Text.Trim();

            using (var data = new dbCommerceDataContext())
            {
                var pedidos = (from c in data.tbPedidoPagamentoGateways where c.orderId == orderId select new { c.tbPedidoPagamento.pedidoId, c.tbPedidoPagamento.numeroCobranca, c.tbPedidoPagamento.valor, c.tbPedidoPagamento.tbPedido.tbPedidoSituacao.situacao });

                grdCodMaxiPago.DataSource = pedidos;
                grdCodMaxiPago.DataBind();

                if (grdCodMaxiPago.Rows.Count == 0)
                    ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('Cod. Maxipago não encontrado na base de dados!');", true);
            }
        }
        catch (Exception)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('Falha na listagem dos dados, verifique os dados digitados')", true);
        }

    }

    private void FillGridNumeroPgto()
    {
        try
        {
            int idPedidoPagamento = Convert.ToInt32(txtNumeroPgto.Text.Trim());

            using (var data = new dbCommerceDataContext())
            {
                var pedidos = (from c in data.tbPedidoPagamentoGateways where c.idPedidoPagamento == idPedidoPagamento select new { c.tbPedidoPagamento.pedidoId, c.tbPedidoPagamento.numeroCobranca, c.tbPedidoPagamento.valor, c.tbPedidoPagamento.tbPedido.tbPedidoSituacao.situacao,c.authorizationCode });

                grdNumeroPgto.DataSource = pedidos;
                grdNumeroPgto.DataBind();

                if (grdNumeroPgto.Rows.Count == 0)
                    ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('Numero de pagamento não encontrado na base de dados!');", true);
            }
        }
        catch (Exception)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('Falha na listagem dos dados, verifique os dados digitados')", true);
        }

    }

    protected void btnPesquisar_Click(object sender, EventArgs e)
    {
        FillGrid();
        ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(),
               "$('#" + pnlConsultaNsu.ClientID + "').show();" +
               "$('#" + pnlConsultaNumeroBoleto.ClientID + "').hide();" +
               "$('#" + pnlConsultaCodMaxiPago.ClientID + "').hide();" +
               "$('#" + pnlConsultaNumeroPgto.ClientID + "').hide();" +
               "$('#divConteudo').width('305px'); ", true);
    }
    private void FillGridNumeroBoleto()
    {
        try
        {
            int idPedidoPagamento = Convert.ToInt32(txtNumeroBoleto.Text.Trim());

            using (var data = new dbCommerceDataContext())
            {
                var pedidoPagamento = (from pp in data.tbPedidoPagamentos
                                       where pp.idPedidoPagamento == idPedidoPagamento
                                       select new
                                       {
                                           pp.idPedidoPagamento,
                                           pp.pedidoId,
                                           pp.valorParcela,
                                           pp.valorDesconto,
                                           pp.valorTotal,
                                           pp.tbPedido.clienteId,
                                           cliente = (from cli in data.tbClientes where cli.clienteId == pp.tbPedido.clienteId select cli.clienteNome).FirstOrDefault()
                                       });
               // var pedidos = (from c in data.tbPedidoPagamento where c.idPedidoPagamento == idPedidoPagamento select new { c.tbPedidoPagamento.pedidoId, c.tbPedidoPagamento.numeroCobranca, c.tbPedidoPagamento.valor, c.tbPedidoPagamento.tbPedido.tbPedidoSituacao.situacao, c.authorizationCode });

                grdNumeroBoleto.DataSource = pedidoPagamento;
                grdNumeroBoleto.DataBind();
                //GridView1.DataSource = pedidoPagamento;
                //GridView1.DataBind();

                if (grdNumeroBoleto.Rows.Count == 0)
                    ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('Numero de boleto não encontrado na base de dados!');", true);
            }
        }
        catch (Exception)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('Falha na listagem dos dados, verifique os dados digitados')", true);
        }

    }
    protected void btnPesquisarCodMaxiPago_OnClick(object sender, EventArgs e)


    {
        FillGridMaxiPago();
        ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(),
               "$('#" + pnlConsultaCodMaxiPago.ClientID + "').show();" +
              "$('#" + pnlConsultaNumeroBoleto.ClientID + "').hide();" +
               "$('#" + pnlConsultaNsu.ClientID + "').hide();" +
               "$('#" + pnlConsultaNumeroPgto.ClientID + "').hide();" +
               "$('#divConteudo').width('405px');", true);
    }

    protected void btnPesquisarNumeroPgto_OnClick(object sender, EventArgs e)
    {
        FillGridNumeroPgto();
        ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(),
               "$('#" + pnlConsultaNumeroPgto.ClientID + "').show();" +
               "$('#" + pnlConsultaNumeroBoleto.ClientID + "').hide();" +
               "$('#" + pnlConsultaCodMaxiPago.ClientID + "').hide();" +
               "$('#" + pnlConsultaNsu.ClientID + "').hide();" +
               "$('#divConteudo').width('440px'); ", true);
    }

    protected void btnPesquisarNumeroBoleto_OnClick(object sender, EventArgs e)
    {
        FillGridNumeroBoleto();
        ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(),
               "$('#" + pnlConsultaNumeroPgto.ClientID + "').hide();" +
                "$('#" + pnlConsultaNumeroBoleto.ClientID + "').show();" +
               "$('#" + pnlConsultaCodMaxiPago.ClientID + "').hide();" +
               "$('#" + pnlConsultaNsu.ClientID + "').hide();" +
               "$('#divConteudo').width('440px'); ", true);
    }
}