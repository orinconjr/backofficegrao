﻿<%@ Page Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="pedidosMarketplace.aspx.cs" Inherits="admin_pedidosMarketplace" Theme="Glass" MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dxwgv" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxPopupControl" Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>
<%@ Register TagPrefix="dxe" Namespace="DevExpress.Web.ASPxMenu" Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Data.Linq" Assembly="DevExpress.Web.v11.1.Linq, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" runat="Server">

    <script language="javascript" type="text/javascript">
        function grid_ContextMenu(s, e) {
            if (e.objectType == "header")
                pmColumnMenu.ShowAtPos(ASPxClientUtils.GetEventX(e.htmlEvent), ASPxClientUtils.GetEventY(e.htmlEvent));
        }
    </script>

    <dxe:ASPxPopupMenu ID="pmColumnMenu" runat="server" ClientInstanceName="pmColumnMenu">
        <Items>
            <dxe:MenuItem Name="cmdShowCustomization" Text="Escolher colunas">
            </dxe:MenuItem>
        </Items>
        <ClientSideEvents ItemClick="function(s, e) { if(e.item.name == 'cmdShowCustomization') grd.ShowCustomizationWindow(); }" />
    </dxe:ASPxPopupMenu>

    <table cellpadding="0" cellspacing="0" style="width: 920px">
        <tr>
            <td class="tituloPaginas" valign="top">
                <asp:Label ID="lblAcao" runat="server">Pedidos</asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
                    <tr>
                        <td align="right">
                            <table cellpadding="4" cellspacing="0" style="width: 834px">
                                <tr>
                                    <td colspan="2">
                                       <strong>Marketplace:</strong>
                                        <asp:DropDownList ID="ddlMarketplace" AutoPostBack="true" runat="server">
                                             <asp:ListItem Value="1" Text="CNova" Selected="True"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                       <strong>Status da Integração:</strong>
                                        <asp:DropDownList ID="ddlStatusIntegracao" AutoPostBack="true" runat="server">
                                            <asp:ListItem Value="" Text="Todos" Selected="True"></asp:ListItem>
                                            <asp:ListItem Value="0" Text="Pendente"></asp:ListItem>
                                            <asp:ListItem Value="1" Text="Integrado"></asp:ListItem>
                                            <asp:ListItem Value="2" Text="Erro"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
               
                                    </td>
                                    <td height="38"
                                        style="background-image: url('images/bkExportar.jpg'); padding-bottom: 1px;"
                                        valign="bottom" width="231">
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 97px">&nbsp;</td>
                                                <td style="width: 33px">
                                                    <asp:ImageButton ID="btPdf" runat="server" ImageUrl="~/admin/images/btPdf.jpg"
                                                        Style="margin-left: 5px" OnClick="btPdf_Click" EnableViewState="true" ValidationGroup="exportar" />
                                                </td>
                                                <td style="width: 31px">
                                                    <asp:ImageButton ID="btXsl" runat="server" ImageUrl="~/admin/images/btXls.jpg"
                                                        Style="margin-left: 5px" OnClick="btXsl_Click" ValidationGroup="exportar" />
                                                </td>
                                                <td style="width: 36px">
                                                    <asp:ImageButton ID="btRtf" runat="server" ImageUrl="~/admin/images/btRtf.jpg"
                                                        Style="margin-left: 5px" OnClick="btRtf_Click" ValidationGroup="exportar" />
                                                </td>
                                                <td valign="bottom">
                                                    <asp:ImageButton ID="btCsv" runat="server" ImageUrl="~/admin/images/btCsv.jpg"
                                                        Style="margin-left: 5px" OnClick="btCsv_Click" ValidationGroup="exportar" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Panel ID="pnlGrid" runat="server" ScrollBars="Horizontal" Width="834px">
                                <dxwgv:ASPxGridView ID="grd" runat="server" AutoGenerateColumns="False"
                                    KeyFieldName="PedidoId" Settings-ShowFilterBar="Visible"
                                    Cursor="auto" ClientInstanceName="grd" EnableCallBacks="False">
                                    <SettingsBehavior AutoFilterRowInputDelay="10200" ConfirmDelete="True"
                                        AutoExpandAllGroups="True" ColumnResizeMode="Control" AllowGroup="False"
                                        AllowFocusedRow="True" />
                                    <SettingsText ConfirmDelete="Tem certeza que deseja excluir?"
                                        EmptyDataRow="Nenhum registro encontrado."
                                        GroupPanel="Arraste uma coluna aqui para agrupar." />
                                    <SettingsPager Position="TopAndBottom" PageSize="25"
                                        ShowDisabledButtons="False" AlwaysShowPager="True">
                                        <Summary AllPagesText="Páginas: {0} - {1} ({2} items)"
                                            Text="Página {0} de {1} ({2} registros encontrados)" />
                                    </SettingsPager>
                                    <Settings ShowFilterRow="True" ShowGroupedColumns="True" ShowFooter="True" />
                                    <TotalSummary>
                                        <dxwgv:ASPxSummaryItem DisplayFormat="0" FieldName="margem"
                                            ShowInColumn="Margem" ShowInGroupFooterColumn="Margem"
                                            SummaryType="Average" />
                                    </TotalSummary>
                                    <SettingsEditing EditFormColumnCount="4"
                                        PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1"
                                        PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter"
                                        PopupEditFormWidth="700px" Mode="Inline" />
                                    <Columns>
                                        <dxwgv:GridViewDataTextColumn Caption="ID Pedido" FieldName="PedidoId"
                                            ReadOnly="True" VisibleIndex="1" Width="50px">
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn Caption="Cliente" FieldName="Cliente"
                                            VisibleIndex="2" Width="150px">
                                            <Settings AutoFilterCondition="Contains" />
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn Caption="Site" FieldName="Site" Settings-ShowFilterRowMenu="False"
                                            ReadOnly="True" VisibleIndex="3" Width="70px">
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn Caption="ID Mktplace " FieldName="PedidoMarketplaceId"
                                            ReadOnly="True" VisibleIndex="4" Width="60px">
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn Caption="Total" Name="Total" VisibleIndex="5" Width="70px">
                                            <DataItemTemplate>
                                                <%# Eval("Total", "{0:f2}")%>
                                            </DataItemTemplate>
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn Caption="Status Pedido" Name="StatusPedido" VisibleIndex="6" Width="70px">
                                            <DataItemTemplate>
                                                <%# Eval("StatusPedido")%>
                                            </DataItemTemplate>
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn Caption="Status Integ." Name="StatusIntegracao" VisibleIndex="7" Width="70px">
                                            <DataItemTemplate>
                                                <%# Eval("StatusIntegracao")%>
                                            </DataItemTemplate>
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn Caption="Data Criação" Name="DataCriacao" VisibleIndex="8" Width="70px">
                                            <DataItemTemplate>
                                                <%# Eval("DataCriacao", "{0:dd/MM/yy HH:mm}")%>
                                            </DataItemTemplate>
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn Caption="#" Name="Enviar" VisibleIndex="9" Width="50px" Visible="true">
                                            <DataItemTemplate>
                                                <asp:Button runat="server" ID="btnEnviar" Enabled='<%# Eval("PermiteEnviar") %>'  Text="Enviar" CommandArgument='<%# Eval("PedidoId") %>' OnCommand="btnEnviarPedido_OnCommand" OnClientClick="return confirm('Deseja realmente enviar pedido?')">
                                                </asp:Button>
                                            </DataItemTemplate>
                                        </dxwgv:GridViewDataTextColumn>
                                    </Columns>
                                    <ClientSideEvents ContextMenu="grid_ContextMenu" />
                                    <SettingsCustomizationWindow Enabled="True" />
                                </dxwgv:ASPxGridView>
                            </asp:Panel>

                            <dxwgv:ASPxGridViewExporter ID="grdEx" runat="server" FileName="pedidos"
                                GridViewID="grd" Landscape="True" PreserveGroupRowStates="True">
                            </dxwgv:ASPxGridViewExporter>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Button ID="btnImportarPedidos" Text="Importar Pedidos" runat="server" OnClick="btnImportarPedidos_Click"/>&nbsp;
                        </td>
                    </tr>
                  
                </table>
            </td>
        </tr>
    </table>
    
</asp:Content>

