﻿<%@ Page Title="" Theme="Glass" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="relatorioGeralPedidos.aspx.cs" Inherits="admin_relatorioGeralPedidos" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxTimer" TagPrefix="dxt" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxPopupControl" Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxLoadingPanel" Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxCallback" Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxEditors" Assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" Runat="Server">  

    <table cellpadding="0" cellspacing="0" style="width: 920px">
    <tr>
        <td class="tituloPaginas" valign="top">
            <asp:Label ID="lblAcao" runat="server">Relatório de Pagamentos</asp:Label>
        </td>
    </tr>
        
    <tr>
        <td>
            
    <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">        
        <tr>
            <td>
                <table cellpadding="0" cellspacing="0" style="width: 834px">
                    <tr>
                        <td class="rotulos" style="width: 100px">Data inicial<br />
                            <asp:TextBox ID="txtDataInicial" runat="server" CssClass="campos"
                                Width="90px" MaxLength="10"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rqvDataInicial" runat="server"
                                ControlToValidate="txtDataInicial" Display="None"
                                ErrorMessage="Preencha a data inicial."
                                SetFocusOnError="True"></asp:RequiredFieldValidator>
                            <b>
                                <asp:RegularExpressionValidator ID="rgeDataInicial" runat="server"
                                    ControlToValidate="txtDataInicial" Display="None"
                                    ErrorMessage="Por favor, preencha corretamente a data inicial."
                                    SetFocusOnError="True"
                                    ValidationExpression="^(([0-2]\d|[3][0-1])\/([0]\d|[1][0-2])\/[1-2][0-9]\d{2})$"></asp:RegularExpressionValidator>
                            </b>
                        </td>
                        <td class="rotulos" style="width: 100px">Data final<br />
                            <asp:TextBox ID="txtDataFinal" runat="server" CssClass="campos" ValidationGroup="grpInsert"
                                Width="90px" MaxLength="10"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rqvDataFinal" runat="server"
                                ControlToValidate="txtDataFinal" Display="None"
                                ErrorMessage="Preencha a data final."
                                SetFocusOnError="True"></asp:RequiredFieldValidator>
                            <b>
                                <asp:RegularExpressionValidator ID="rgeDataInicial0" runat="server"
                                    ControlToValidate="txtDataFinal" Display="None"
                                    ErrorMessage="Por favor, preencha corretamente a data final"
                                    SetFocusOnError="True"
                                    ValidationExpression="^(([0-2]\d|[3][0-1])\/([0]\d|[1][0-2])\/[1-2][0-9]\d{2})$"></asp:RegularExpressionValidator>
                            </b>
                        </td>
                        <td valign="bottom">
                            <asp:ImageButton ID="imbInsert" runat="server" ImageUrl="~/admin/images/btPesquisar.jpg" OnClick="imbInsert_Click" />
                            <asp:ValidationSummary ID="llds" runat="server" DisplayMode="List" ShowMessageBox="True" ShowSummary="False" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>            
            <td align="right">
                    <table align="right" cellpadding="0" cellspacing="0" style="width: 395px">
                    <tr>
                        <td align="right">&nbsp;</td>
                        <td height="38"
                            style="background-image: url('images/bkExportar.jpg'); padding-bottom: 1px;"
                            valign="bottom" width="231">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="width: 97px">&nbsp;</td>
                                    <td style="width: 33px">
                                        <asp:ImageButton ID="btPdf" runat="server" ImageUrl="~/admin/images/btPdf.jpg" OnClick="btPdf_Click" Enabled="false"
                                            Style="margin-left: 5px" />
                                    </td>
                                    <td style="width: 31px">
                                        <asp:ImageButton ID="btXsl" runat="server" ImageUrl="~/admin/images/btXls.jpg" CausesValidation="false" OnClick="btXsl_Click"
                                            Style="margin-left: 5px" />
                                    </td>
                                    <td style="width: 36px">
                                        <asp:ImageButton ID="btRtf" runat="server" ImageUrl="~/admin/images/btRtf.jpg" OnClick="btRtf_Click" Enabled="false"
                                            Style="margin-left: 5px" />
                                    </td>
                                    <td valign="bottom">
                                        <asp:ImageButton ID="btCsv" runat="server" ImageUrl="~/admin/images/btCsv.jpg" OnClick="btCsv_Click" Enabled="false"
                                            Style="margin-left: 5px" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <dxwgv:ASPxGridViewExporter ID="grdEx" runat="server" FileName="relatorioPagamentos" GridViewID="grd" Landscape="True" PreserveGroupRowStates="True">
            </dxwgv:ASPxGridViewExporter>
                <dxwgv:ASPxGridView ID="grd" runat="server" AutoGenerateColumns="True" Width="834px" Cursor="auto" KeyFieldName="pedidoId" EnableCallBacks="false" EnableViewState="False">
                    <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                    <Styles>
                        <Footer Font-Bold="True">
                        </Footer>
                    </Styles>
                    <SettingsText ConfirmDelete="Tem certeza que deseja excluir?" emptydatarow="Nenhum registro encontrado." />
                    <SettingsPager Position="TopAndBottom" PageSize="50" ShowDisabledButtons="False" AlwaysShowPager="True">
                        <Summary AllPagesText="Páginas: {0} - {1} ({2} items)" Text="Página {0} de {1} ({2} registros encontrados)" />
                    </SettingsPager>
                    <TotalSummary>
                        <dxwgv:ASPxSummaryItem FieldName="valorCobrado" ShowInColumn="valorCobrado" ShowInGroupFooterColumn="valorCobrado" SummaryType="Sum" DisplayFormat="{0:C}" />
                        <dxwgv:ASPxSummaryItem FieldName="valor" ShowInColumn="valor" ShowInGroupFooterColumn="valor" SummaryType="Sum" DisplayFormat="{0:C}" />
                    </TotalSummary>
                    <settings showfilterrow="True" ShowGroupButtons="False" ShowHeaderFilterButton="True" ShowFooter="True" />
                    <SettingsEditing EditFormColumnCount="4" PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1" PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter" PopupEditFormWidth="700px" />
                    <%--<Columns>
                        <dxwgv:GridViewDataTextColumn Caption="ID" FieldName="pedidoId" VisibleIndex="0">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Valor Pedido" FieldName="valorCobrado" VisibleIndex="0">
                            <PropertiesTextEdit DisplayFormatString="C" />
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Valor Pagamento" FieldName="valor" VisibleIndex="0">
                            <PropertiesTextEdit DisplayFormatString="C" />
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Pago" FieldName="pago" VisibleIndex="0">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataDateColumn Caption="Data Limite" FieldName="dataHoraDoPedido" VisibleIndex="0">
                        </dxwgv:GridViewDataDateColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Situação" FieldName="situacao" VisibleIndex="0">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Condição" FieldName="condicaoNome" VisibleIndex="0">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Status Gateway" FieldName="statusGateway" VisibleIndex="0">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Status ClearSale" FieldName="statusClearsale" VisibleIndex="0">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Pedidos Apos" FieldName="pedidosApos" VisibleIndex="0">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Editar" Name="editar" VisibleIndex="0" Width="60">
                            <DataItemTemplate>
                                <a href="pedido.aspx?pedidoId=<%# Eval("pedidoId") %>" target="_blank">
                                    Abrir Pedido
                                </a>
                            </DataItemTemplate>
                        </dxwgv:GridViewDataTextColumn>
                    </Columns>--%>
                    <StylesEditors>
                        <Label Font-Bold="True">
                        </Label>
                    </StylesEditors>
                </dxwgv:ASPxGridView>        
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
    </table>
        </td>
    </tr>
</table>
</asp:Content>