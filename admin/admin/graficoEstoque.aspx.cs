﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

public partial class admin_graficoEstoque : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
    }

    private void fill()
    {
        var dataInicial = DateTime.Now;
        var dataFinal = DateTime.Now;
        int estoqueMinimo = 0;
        if(!string.IsNullOrEmpty(txtDataInicial.Text)) DateTime.TryParse(txtDataInicial.Text, out dataInicial);
        if (!string.IsNullOrEmpty(txtDataFinal.Text)) DateTime.TryParse(txtDataFinal.Text, out dataFinal);

        int.TryParse(txtEstoqueMinimo.Text, out estoqueMinimo);

        var data = new dbCommerceDataContext();
        var estoque = data.admin_produtosEmEstoque().Where(x => x.estoqueLivre >= estoqueMinimo);
        var estoqueIds = estoque.Select(x => x.produtoId);

        var pedidosFornecedor = (from c in data.tbPedidoFornecedorItems
            where
                estoqueIds.Contains(c.idProduto) && c.tbPedidoFornecedor.data >= dataInicial &&
                c.tbPedidoFornecedor.data <= dataFinal && c.motivo.ToLower() == "estoque" select new {c.idProduto}).Distinct().ToList();
        lstProdutosEstoque.DataSource = pedidosFornecedor;
        lstProdutosEstoque.DataBind();
    }

    private class retornoGrafico
    {
        public int idProduto { get; set; }
        public DateTime data { get; set; }
        public int compras { get; set; }
        public int pedidos { get; set; }
        public int entregas { get; set; }
        public int reservas { get; set; }
        public int cancelamentos { get; set; }
        public int estoqueLivre { get; set; }
    }
    protected void btnFiltrar_OnClick(object sender, EventArgs e)
    {
        fill();
    }

    protected void lstProdutosEstoque_OnItemDataBound(object sender, ListViewItemEventArgs e)
    {
        var dataInicial = DateTime.Now;
        var dataFinal = DateTime.Now;
        if (!string.IsNullOrEmpty(txtDataInicial.Text)) DateTime.TryParse(txtDataInicial.Text, out dataInicial);
        if (!string.IsNullOrEmpty(txtDataFinal.Text)) DateTime.TryParse(txtDataFinal.Text, out dataFinal);

        int estoqueMinimo = 0;
        int produtoId = 0;

        var data = new dbCommerceDataContext();
        var hdfProdutoId = (HiddenField)e.Item.FindControl("hdfProdutoId");
        int.TryParse(hdfProdutoId.Value, out produtoId);
        var produto = (from c in data.tbProdutos where c.produtoId == produtoId select c).FirstOrDefault();

        //var primeiroProduto = (tbPedidoFornecedorItem)e.Item.DataItem;
        if (produto != null)
        {
            int estoqueLivreAtual = (int)data.admin_produtoEmEstoque(produtoId).FirstOrDefault().estoqueLivre;

            var grafico = (RadHtmlChart)e.Item.FindControl("grafico");
            var litProdutoNome = (Literal)e.Item.FindControl("litProdutoNome");
            var litEstoque = (Literal)e.Item.FindControl("litEstoque");
            litEstoque.Text = estoqueLivreAtual.ToString();
            litProdutoNome.Text = produto.produtoNome;

            var pedidosFornecedorProduto = (from c in data.tbPedidoFornecedorItems
                                            where c.idProduto == produtoId && c.tbPedidoFornecedor.data >= dataInicial &&
                                                  c.tbPedidoFornecedor.data <= dataFinal && c.motivo.ToLower() == "estoque"
                                            group c by new
                                            {
                                                c.idProduto,
                                                c.idPedidoFornecedor
                                            }
                                                into pedidos
                                                select new
                                                {
                                                    tipo = "Pedido de Estoque",
                                                    idProduto = pedidos.FirstOrDefault().idProduto,
                                                    data = (DateTime)pedidos.FirstOrDefault().tbPedidoFornecedor.data,
                                                    quantidade = pedidos.Sum(x => x.quantidade)
                                                }).OrderBy(x => x.data).ToList();
            var primeiroPedido = pedidosFornecedorProduto.First().data;

            var pedidosFornecedorEntregue = (from c in data.tbPedidoFornecedorItems
                                             where c.idProduto == produtoId && c.tbPedidoFornecedor.data >= dataInicial &&
                                                   c.tbPedidoFornecedor.data <= dataFinal && c.motivo.ToLower() == "estoque" && c.dataEntrega != null
                                             group c by new
                                             {
                                                 c.idProduto,
                                                 c.idPedidoFornecedor
                                             }
                                                 into pedidos
                                                 select new
                                                 {
                                                     tipo = "Pedido de Estoque",
                                                     idProduto = pedidos.FirstOrDefault().idProduto,
                                                     data = (DateTime)pedidos.FirstOrDefault().dataEntrega,
                                                     quantidade = pedidos.Sum(x => x.quantidade)
                                                 }).OrderBy(x => x.data).ToList();

            var pedidosProdutoGeral = (from c in data.tbItensPedidos
                                       where c.produtoId == produtoId && (c.tbPedido.dataHoraDoPedido >= dataInicial | c.tbPedido.dataConfirmacaoPagamento >= dataInicial) &&
                                       (c.tbPedido.dataHoraDoPedido >= primeiroPedido | c.tbPedido.dataConfirmacaoPagamento >= primeiroPedido) &&
                                        (c.tbPedido.dataHoraDoPedido <= dataFinal | c.tbPedido.dataConfirmacaoPagamento <= dataFinal)
                                  select new
                                  {
                                      c.itemQuantidade,
                                      c.tbPedido.dataHoraDoPedido,
                                      c.tbPedido.dataConfirmacaoPagamento,
                                      c.tbPedido.pedidoId
                                  }).ToList();

            var pedidosProdutoComboGeral = (from c in data.tbItensPedidoCombos
                                            where c.produtoId == produtoId && (c.tbItensPedido.tbPedido.dataHoraDoPedido >= dataInicial | c.tbItensPedido.tbPedido.dataConfirmacaoPagamento >= dataInicial) &&
                                       (c.tbItensPedido.tbPedido.dataHoraDoPedido >= primeiroPedido | c.tbItensPedido.tbPedido.dataConfirmacaoPagamento >= primeiroPedido) &&
                                            c.tbItensPedido.tbPedido.dataHoraDoPedido <= dataFinal
                                       select new
                                        {
                                            c.tbItensPedido.itemQuantidade,
                                            c.tbItensPedido.tbPedido.dataHoraDoPedido,
                                            c.tbItensPedido.tbPedido.dataConfirmacaoPagamento,
                                            c.tbItensPedido.tbPedido.pedidoId
                                        }).ToList();
            var listaIdsPedidos = new List<int>();
            listaIdsPedidos.AddRange(pedidosProdutoGeral.Select(x => x.pedidoId));
            listaIdsPedidos.AddRange(pedidosProdutoComboGeral.Select(x => x.pedidoId));
            var reservasEstoqueGeral = (from c in data.tbProdutoReservaEstoques 
                                        where c.idProduto == produtoId && c.dataHora >= dataInicial && c.dataHora >= primeiroPedido &&
                                         c.dataHora <= dataFinal && !listaIdsPedidos.Contains(c.idPedido)
                                   select c).ToList();

            var pedidosProdutoCancelamentoGeral = (from c in data.tbItensPedidos
                                              join d in data.tbPedidoInteracoes on c.pedidoId equals d.pedidoId
                                                   where c.produtoId == produtoId && d.interacaoData >= dataInicial &&
                                        d.interacaoData <= dataFinal && c.tbPedido.statusDoPedido == 6
                                        && d.interacao.Contains("cancelado") && (c.tbPedido.dataHoraDoPedido >= dataInicial | c.tbPedido.dataConfirmacaoPagamento >= dataInicial)
                                        && (c.tbPedido.dataHoraDoPedido >= primeiroPedido | c.tbPedido.dataConfirmacaoPagamento >= primeiroPedido)
                                              select new{c.itemPedidoId, c.itemQuantidade, d.interacaoData}).Distinct().ToList();
            var pedidosProdutoComboCancelamentoGeral = (from c in data.tbItensPedidoCombos
                                                   join d in data.tbPedidoInteracoes on c.tbItensPedido.pedidoId equals d.pedidoId
                                                        where c.produtoId == produtoId && d.interacaoData >= dataInicial &&
                                                    d.interacaoData <= dataFinal && c.tbItensPedido.tbPedido.statusDoPedido == 6
                                        && d.interacao.Contains("cancelado") && 
                                        (c.tbItensPedido.tbPedido.dataHoraDoPedido >= dataInicial | c.tbItensPedido.tbPedido.dataConfirmacaoPagamento >= dataInicial) &&
                                        (c.tbItensPedido.tbPedido.dataHoraDoPedido >= primeiroPedido | c.tbItensPedido.tbPedido.dataConfirmacaoPagamento >= primeiroPedido)
                                                        select new { c.tbItensPedido.itemPedidoId, c.tbItensPedido.itemQuantidade, d.interacaoData }).Distinct().ToList();


            //var ultimaData = DateTime.Now;
            //var dataAtual = DateTime.Now;
            int contagem = 1;
            var listaPedidos = new List<retornoGrafico>();
            var listaDatas = new List<DateTime>();
            listaDatas.AddRange(pedidosFornecedorProduto.Select(x => x.data.Date));
            listaDatas.AddRange(pedidosFornecedorEntregue.Select(x => x.data.Date));
            listaDatas.AddRange(pedidosProdutoGeral.Select(x => ((DateTime)x.dataHoraDoPedido).Date));
            listaDatas.AddRange(pedidosProdutoComboGeral.Select(x => ((DateTime)x.dataHoraDoPedido).Date));
            listaDatas.AddRange(reservasEstoqueGeral.Select(x => ((DateTime)x.dataHora).Date));
            listaDatas.AddRange(pedidosProdutoCancelamentoGeral.Select(x => ((DateTime)x.interacaoData).Date));
            listaDatas.AddRange(pedidosProdutoComboCancelamentoGeral.Select(x => ((DateTime)x.interacaoData).Date));


            listaDatas.Add(DateTime.Now.Date);
            listaDatas = listaDatas.Distinct().ToList();

            //grafico.PlotArea.XAxis.Items.Clear();
            int estoqueInicial = 0;

            foreach (var listaData in listaDatas.OrderBy(x => x))
            {
                AxisItem newAxisItem = new AxisItem();
                //format the DateTime object
                string formattedLabelText = string.Format("{0:dd/M}", listaData);
                newAxisItem.LabelText = formattedLabelText;
                grafico.PlotArea.XAxis.Items.Add(newAxisItem);

                var pedidoFornecedor = pedidosFornecedorProduto.Where(x => x.data.Date == listaData.Date);
                var pedidoFornecedorEntregue = pedidosFornecedorEntregue.Where(x => x.data.Date == listaData.Date);
                var pedidos = new retornoGrafico();
                pedidos.data = listaData;
                pedidos.idProduto = produtoId;
                pedidos.compras = pedidoFornecedor.Any() ? pedidoFornecedor.Sum(x => x.quantidade) : 0;
                pedidos.entregas = pedidoFornecedorEntregue.Any() ? pedidoFornecedorEntregue.Sum(x => x.quantidade) : 0;

                

                #region pedidos
                var pedidosProduto = (from c in pedidosProdutoGeral
                                      where ((c.dataConfirmacaoPagamento ?? c.dataHoraDoPedido).Value.Date == listaData.Date)
                                      select c).ToList();

                int totalProdutos = 0;
                if (pedidosProduto.Any()) totalProdutos = Convert.ToInt32(pedidosProduto.Sum(x => x.itemQuantidade));
                var pedidosProdutoCombo = (from c in pedidosProdutoComboGeral
                                           where ((c.dataConfirmacaoPagamento ?? c.dataHoraDoPedido).Value.Date == listaData.Date)
                                           select c).ToList();
                int totalProdutosCombo = 0;
                if (pedidosProdutoCombo.Any()) totalProdutosCombo = Convert.ToInt32(pedidosProdutoCombo.Sum(x => x.itemQuantidade));
                pedidos.pedidos = totalProdutos + totalProdutosCombo;
                #endregion

                #region reservas
                var reservasEstoque = (from c in reservasEstoqueGeral
                                       where c.dataHora.Date == listaData.Date
                                            select c).ToList();
                int totalProdutosReserva = 0;
                if (reservasEstoque.Any()) totalProdutosReserva = Convert.ToInt32(reservasEstoque.Sum(x => x.quantidade));
                pedidos.reservas = totalProdutosReserva;
                #endregion

                #region cancelamentos
                var pedidosProdutoCancelamento = (from c in pedidosProdutoCancelamentoGeral
                                                  where c.interacaoData.Value.Date == listaData.Date
                                                  select c).Distinct().ToList();
                int totalProdutosCancelamento = 0;
                if (pedidosProdutoCancelamento.Any()) totalProdutosCancelamento = Convert.ToInt32(pedidosProdutoCancelamento.Sum(x => x.itemQuantidade));
                var pedidosProdutoComboCancelamento = (from c in pedidosProdutoComboCancelamentoGeral
                                                  where c.interacaoData.Value.Date == listaData.Date
                                                       select c).Distinct().ToList();
                int totalProdutosComboCancelamento = 0;
                if (pedidosProdutoComboCancelamento.Any()) totalProdutosComboCancelamento = Convert.ToInt32(pedidosProdutoComboCancelamento.Sum(x => x.itemQuantidade));
                pedidos.cancelamentos = totalProdutosCancelamento + totalProdutosComboCancelamento;
                #endregion

                estoqueInicial += pedidos.compras - pedidos.pedidos + pedidos.cancelamentos - pedidos.reservas;

                pedidos.estoqueLivre = estoqueInicial;

                var possuiDataAnterior = listaDatas.OrderByDescending(x => x).Where(x => x.Date < listaData.Date).Any();
                var dataAnterior = listaDatas.OrderByDescending(x => x).Where(x => x.Date < listaData.Date).FirstOrDefault();


                listaPedidos.Add(pedidos);
                contagem++;
            }

            grafico.DataSource = listaPedidos.OrderBy(x => x.data);
            grafico.DataBind();
        }
    }
}