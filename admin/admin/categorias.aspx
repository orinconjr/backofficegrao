﻿<%@ Page Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="categorias.aspx.cs" Inherits="admin_categorias"   Theme="Glass" MaintainScrollPositionOnPostback=true %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dxwgv" %>


<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>


<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" Runat="Server">
    <table cellpadding="0" cellspacing="0" style="width: 920px">
    <tr>
        <td class="tituloPaginas" valign="top">
            <asp:Label ID="lblAcao" runat="server">Categorias</asp:Label>
            </td>
    </tr>
    <tr>
        <td>
            
    <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
        <tr>
            <td align="right">
                <table cellpadding="0" cellspacing="0" style="width: 834px">
                    <tr>
                        <td align="right">
                            &nbsp;</td>
                        <td height="38" 
                            style="background-image: url('images/bkExportar.jpg'); padding-bottom: 1px;" 
                            valign="bottom" width="231">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="width: 97px">
                                        &nbsp;</td>
                                    <td style="width: 33px">
                                        <asp:ImageButton ID="btPdf" runat="server" ImageUrl="~/admin/images/btPdf.jpg" 
                                            style="margin-left: 5px" />
                                    </td>
                                    <td style="width: 31px">
                                        <asp:ImageButton ID="btXsl" runat="server" ImageUrl="~/admin/images/btXls.jpg" 
                                            style="margin-left: 5px" />
                                    </td>
                                    <td style="width: 36px">
                                        <asp:ImageButton ID="btRtf" runat="server" ImageUrl="~/admin/images/btRtf.jpg" 
                                            style="margin-left: 5px" />
                                    </td>
                                    <td valign="bottom">
                                        <asp:ImageButton ID="btCsv" runat="server" ImageUrl="~/admin/images/btCsv.jpg" 
                                            style="margin-left: 5px" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td style="font-weight: bold; padding-bottom: 15px; padding-left: 10px;" class="rotulos">
                Site:<br/>
                <asp:DropDownList runat="server" ID="ddlSite" DataTextField="nome" DataValueField="idSite" DataSourceID="sqlSite" AutoPostBack="True" OnSelectedIndexChanged="ddlSite_OnSelectedIndexChanged" />
            </td>
        </tr>
        <tr>
            <td>
                <table cellpadding="0" cellspacing="0" style="width: 834px">
                    <tr>
                        <td style="width: 220px" valign="top">
         <asp:TreeView ID="treeCategorias" runat="server" 
             ShowLines="True" ontreenodepopulate="treeCategorias_TreeNodePopulate" Font-Names="Tahoma" 
                                Font-Size="12px" ForeColor="Black" ExpandDepth="30" 
                                onprerender="treeCategorias_PreRender" ShowCheckBoxes="All" 
                                onselectednodechanged="treeCategorias_SelectedNodeChanged">
             <HoverNodeStyle Font-Underline="True" />
             <SelectedNodeStyle BackColor="#D2ECEC" />
             <Nodes>
                 <asp:TreeNode Text="Categorias" Value="0" Checked="True" Expanded="True" Selected="True"></asp:TreeNode>
             </Nodes>

</asp:TreeView>



                        </td>
                        <td valign="top">
                <dxwgv:ASPxGridView ID="grd" runat="server" AutoGenerateColumns="False" 
                    DataSourceID="sqlCategorias" KeyFieldName="categoriaId" Width="614px" 
                    Cursor="auto" onrowinserting="grd_RowInserting" EnableCallBacks="False" 
                    onhtmlrowcreated="grd_HtmlRowCreated" onrowupdating="grd_RowUpdating" 
                    onrowdeleting="grd_RowDeleting" onrowinserted="grd_RowInserted" 
                                onrowupdated="grd_RowUpdated" onrowdeleted="grd_RowDeleted">
                    <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                    <SettingsText ConfirmDelete="Tem certeza que deseja excluir?" 
                        emptydatarow="Nenhum registro encontrado." />
                    <SettingsPager Position="TopAndBottom" PageSize="50" 
                        ShowDisabledButtons="False" AlwaysShowPager="True">
                        <Summary AllPagesText="Páginas: {0} - {1} ({2} items)" 
                            Text="Página {0} de {1} ({2} registros encontrados)" />
                    </SettingsPager>
                    <settings showfilterrow="True" ShowGroupButtons="False" />
                    <SettingsEditing EditFormColumnCount="4" 
                        PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1" 
                        PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter" 
                        PopupEditFormWidth="700px" />
                    <Columns>
                        <dxwgv:GridViewDataTextColumn Caption="ID da categoria" 
                            FieldName="categoriaId" ReadOnly="True" VisibleIndex="0" Width="80px">
                            <Settings AutoFilterCondition="Contains" />
                            <editformsettings visible="False" CaptionLocation="Top" VisibleIndex="2" />
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="URL" 
                            FieldName="categoriaUrl" ReadOnly="True" VisibleIndex="0" Width="80px">
                            <Settings AutoFilterCondition="Contains" />
                            <editformsettings visible="False" CaptionLocation="Top" VisibleIndex="2" />
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Nome" FieldName="categoriaNome" 
                            VisibleIndex="1" Width="444px">
                            <PropertiesTextEdit>
                                
                                <ValidationSettings ErrorText="" SetFocusOnError="True">
                                    
                                    <RequiredField ErrorText="Preencha o campo nome." IsRequired="True" />
                                    
                                </ValidationSettings>
                                
                            </PropertiesTextEdit>
                            <Settings AutoFilterCondition="Contains" />
                            <editformsettings captionlocation="None" columnspan="2" visible="True" 
                                VisibleIndex="1" />
                            <EditItemTemplate>
                                <div class="rotulos">
                                    Nome<dxe:ASPxTextBox ID="txtNome" runat="server" 
                                        Text='<%# Eval("CategoriaNome") %>' Width="260px">
                                        <ValidationSettings ErrorDisplayMode="ImageWithTooltip">
                                            <RequiredField ErrorText="Preencha o campo nome." IsRequired="True" />
                                        </ValidationSettings>
                                    </dxe:ASPxTextBox>
                                </div>
                            </EditItemTemplate>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Exibicao" FieldName="categoriaNomeExibicao" VisibleIndex="1" Width="444px">
                            <PropertiesTextEdit>
                                <ValidationSettings ErrorText="" SetFocusOnError="True">
                                    <RequiredField ErrorText="Preencha o campo nome de exibicao." IsRequired="True" />
                                </ValidationSettings>
                            </PropertiesTextEdit>
                            <Settings AutoFilterCondition="Contains" />
                            <editformsettings captionlocation="None" columnspan="2" visible="True" 
                                VisibleIndex="1" />
                            <EditItemTemplate>
                                <div class="rotulos">
                                    Nome de Exibição<dxe:ASPxTextBox ID="txtCategoriaNomeExibicao" runat="server" 
                                        Text='<%# Eval("categoriaNomeExibicao") %>' Width="260px">
                                        <ValidationSettings ErrorDisplayMode="ImageWithTooltip">
                                            <RequiredField ErrorText="Preencha o campo nome de exibicao." IsRequired="True" />
                                        </ValidationSettings>
                                    </dxe:ASPxTextBox>
                                </div>
                            </EditItemTemplate>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Sigla" FieldName="codigoEtiqueta" VisibleIndex="1" Width="80px">
                            <PropertiesTextEdit>
                                <ValidationSettings ErrorText="" SetFocusOnError="True">
                                    <RequiredField ErrorText="Preencha o campo Sigla." IsRequired="True" />
                                </ValidationSettings>
                            </PropertiesTextEdit>
                            <Settings AutoFilterCondition="Contains" />
                            <editformsettings captionlocation="None" columnspan="2" visible="True" 
                                VisibleIndex="1" />
                            <EditItemTemplate>
                                <div class="rotulos">
                                    Sigla Etiqueta<dxe:ASPxTextBox ID="txtCategoriaSigla" runat="server" 
                                        Text='<%# Eval("codigoEtiqueta") %>' Width="260px">
                                        <ValidationSettings ErrorDisplayMode="ImageWithTooltip">
                                            <RequiredField ErrorText="Preencha o campo sigla." IsRequired="True" />
                                        </ValidationSettings>
                                    </dxe:ASPxTextBox>
                                </div>
                            </EditItemTemplate>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Tags" FieldName="categoriaTags" VisibleIndex="1" Width="444px">
                            <PropertiesTextEdit>
                                <ValidationSettings ErrorText="" SetFocusOnError="True">
                                    <RequiredField ErrorText="Preencha o campo nome de exibicao." IsRequired="True" />
                                </ValidationSettings>
                            </PropertiesTextEdit>
                            <Settings AutoFilterCondition="Contains" />
                            <editformsettings captionlocation="None" columnspan="2" visible="True" 
                                VisibleIndex="1" />
                            <EditItemTemplate>
                                <div class="rotulos">
                                    TAGs (separado por virgula)<dxe:ASPxTextBox ID="txtCategoriaTags" runat="server" 
                                        Text='<%# Eval("categoriaTags") %>' Width="260px">
                                        <ValidationSettings ErrorDisplayMode="ImageWithTooltip">
                                            <RequiredField ErrorText="Preencha o campo TAGs." IsRequired="True" />
                                        </ValidationSettings>
                                    </dxe:ASPxTextBox>
                                </div>
                            </EditItemTemplate>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Descrição" FieldName="categoriaDescricao" VisibleIndex="1" Width="444px">
                            <PropertiesTextEdit>
                                <ValidationSettings ErrorText="" SetFocusOnError="True">
                                    <RequiredField ErrorText="Preencha o campo nome de exibicao." IsRequired="True" />
                                </ValidationSettings>
                            </PropertiesTextEdit>
                            <Settings AutoFilterCondition="Contains" />
                            <editformsettings captionlocation="None" columnspan="2" visible="True" 
                                VisibleIndex="1" />
                            <EditItemTemplate>
                                <div class="rotulos">
                                    Descrição <asp:TextBox ID="txtCategoriaDescricao" runat="server" TextMode="MultiLine" Text='<%# Eval("categoriaDescricao") %>' Width="500px" Height="300px">
                                    </asp:TextBox>
                                </div>
                            </EditItemTemplate>
                            <DataItemTemplate>
                                
                            </DataItemTemplate>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Categoria ML" FieldName="categoriaMl" VisibleIndex="1" Width="444px">
                            <PropertiesTextEdit>
                                <ValidationSettings ErrorText="" SetFocusOnError="True">
                                    <RequiredField ErrorText="Preencha o campo categoria ML." IsRequired="False" />
                                </ValidationSettings>
                            </PropertiesTextEdit>
                            <Settings AutoFilterCondition="Contains" />
                            <editformsettings captionlocation="None" columnspan="1" visible="True" 
                                VisibleIndex="1" />
                            <EditItemTemplate>
                                <div class="rotulos">
                                    Categoria ML<dxe:ASPxTextBox ID="txtCategoriaMl" runat="server" 
                                        Text='<%# Eval("categoriaMl") %>' Width="260px">
                                        <ValidationSettings ErrorDisplayMode="ImageWithTooltip">
                                            <RequiredField ErrorText="Preencha o campo Categoria ML." IsRequired="False" />
                                        </ValidationSettings>
                                    </dxe:ASPxTextBox>
                                </div>
                            </EditItemTemplate>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Posição" FieldName="CategoriaOrdem" 
                            VisibleIndex="2" Name="CategoriaOrdem">
                            <EditFormSettings CaptionLocation="None" RowSpan="2" />
                            <EditItemTemplate>
                                <div class="rotulos">Posição</div>
                                <asp:TextBox ID="txtPosicao" runat="server" 
                                    Text='<%# Eval("CategoriaOrdem") %>' Width="40px" CssClass="campos"></asp:TextBox>
                            </EditItemTemplate>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Exibir no Site" FieldName="exibirSite" 
                            VisibleIndex="2" Name="exibirSite">
                            <EditFormSettings CaptionLocation="None" RowSpan="2" />
                            <EditItemTemplate>
                                <div class="rotulos">Exibir no Menu</div>
                                 <asp:CheckBox ID="chkExibirSite" runat="server" Checked='<%# (Eval("exibirSite") == null ? true : Eval("exibirSite")) %>' />
                            </EditItemTemplate>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Exibir no Filtro" FieldName="exibirNoFiltro"
                            VisibleIndex="2" Name="exibirNoFiltro">
                            <EditFormSettings CaptionLocation="None" RowSpan="2" />
                            <EditItemTemplate>
                                <div class="rotulos">Exibir no Filtro</div>
                                 <asp:CheckBox ID="chkExibirNoFiltro" runat="server" Checked='<%# Convert.ToBoolean(Eval("exibirNoFiltro")) %>' />
                            </EditItemTemplate>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Exibir Somente com 1" FieldName="exibirMesmoCom1Filtro" 
                            VisibleIndex="2" Name="exibirMesmoCom1Filtro">
                            <EditFormSettings CaptionLocation="None" RowSpan="2" />
                            <EditItemTemplate>
                                <div class="rotulos">Exibir Somente com 1</div>
                                 <asp:CheckBox ID="chkExibirMesmoCom1Filtro" runat="server" Checked='<%# Convert.ToBoolean(Eval("exibirMesmoCom1Filtro")) %>' />
                            </EditItemTemplate>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Visible="False" VisibleIndex="2" Name="imagens">
                            <EditFormSettings Visible="True" CaptionLocation="Top" columnspan="4" />
                            <EditCellStyle Font-Strikeout="False">
                            </EditCellStyle>
                            <EditItemTemplate>
                                <table cellpadding="0" cellspacing="0" style="width: 600px">
                                    <tr>
                                        <td class="rotulos" style="width: 415px">
                                            Imagem 1<br />
                                            <asp:FileUpload ID="flu1" runat="server" CssClass="campos" Width="400px" />
                                            <br />
                                            <asp:RegularExpressionValidator ID="rgeFlu1" runat="server" 
                                                ControlToValidate="flu1" ErrorMessage="A imagem precisa ser jpg." 
                                                ValidationExpression="(.*\.jpe?g|.*\.JPE?G)"></asp:RegularExpressionValidator>
                                        </td>
                                        <td style="width: 115px">
                                            <asp:Image ID="Imagem1" runat="server" 
                                                ImageUrl='<%# "../categorias/" + Eval("categoriaId") + "/" + Eval("imagem1") %>' Height="100px" 
                                                Width="100px" />
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="ckbExcluir1" runat="server" CssClass="rotulos" 
                                                Text="Excluir" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 415px">
                                            &nbsp;</td>
                                        <td style="width: 115px">
                                            &nbsp;</td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="rotulos" style="width: 415px">
                                            Imagem 2<asp:FileUpload ID="flu2" runat="server" CssClass="campos" 
                                                Width="400px" />
                                            <br />
                                            <asp:RegularExpressionValidator ID="rgeFlu2" runat="server" 
                                                ControlToValidate="flu2" ErrorMessage="A imagem precisa ser jpg." 
                                                ValidationExpression="(.*\.jpe?g|.*\.JPE?G)"></asp:RegularExpressionValidator>
                                        </td>
                                        <td style="width: 115px">
                                            <asp:Image ID="Imagem2" runat="server" 
                                                ImageUrl='<%# "../categorias/" + Eval("categoriaId") + "/" + Eval("imagem2") %>' Height="100px" 
                                                Width="100px" />
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="ckbExcluir2" runat="server" CssClass="rotulos" 
                                                Text="Excluir" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 415px">
                                            &nbsp;</td>
                                        <td style="width: 115px">
                                            &nbsp;</td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="rotulos" style="width: 415px">
                                            Imagem 3<asp:FileUpload ID="flu3" runat="server" CssClass="campos" 
                                                Width="400px" />
                                            <br />
                                            <asp:RegularExpressionValidator ID="rgeFlu3" runat="server" 
                                                ControlToValidate="flu3" ErrorMessage="A imagem precisa ser jpg." 
                                                ValidationExpression="(.*\.jpe?g|.*\.JPE?G)"></asp:RegularExpressionValidator>
                                        </td>
                                        <td style="width: 115px">
                                            <asp:Image ID="Imagem3" runat="server" 
                                                ImageUrl='<%# "../categorias/" + Eval("categoriaId") + "/" + Eval("imagem3") %>' Height="100px" 
                                                Width="100px" />
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="ckbExcluir3" runat="server" CssClass="rotulos" 
                                                Text="Excluir" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 415px">
                                            &nbsp;</td>
                                        <td style="width: 115px">
                                            &nbsp;</td>
                                    </tr>
                                </table>
                            </EditItemTemplate>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewCommandColumn VisibleIndex="3" Width="90px" ButtonType="Image">
                            <editbutton visible="True" Text="Editar">
                                <Image Url="~/admin/images/btEditar.jpg" />
                            </editbutton>
                            <NewButton Visible="True" Text="Novo">
                                <Image Url="~/admin/images/btNovo.jpg" />
                            </NewButton>
                            <deletebutton visible="True" Text="Excluir">
                                <Image Url="~/admin/images/btExcluir.jpg" />
                            </deletebutton>
                            <CancelButton Text="Cancelar">
                                <Image Url="~/admin/images/btCancelar.jpg" />
                            </CancelButton>
                            <UpdateButton Text="Salvar">
                                <Image Url="~/admin/images/btSalvarPeq.jpg" />
                            </UpdateButton>
                            <ClearFilterButton Visible="True" text="Limpar filtro">
                                <Image Url="~/admin/images/btLimparFiltro.jpg" />
                            </ClearFilterButton>
                            <HeaderTemplate>
                                <img alt="" src="images/legendaIcones.jpg" />
                            </HeaderTemplate>
                        </dxwgv:GridViewCommandColumn>
                    </Columns>
                    <StylesEditors>
                        <Label Font-Bold="True">
                        </Label>
                    </StylesEditors>
                </dxwgv:ASPxGridView>
                        </td>
                    </tr>
                </table>
<%--                <dxwgv:ASPxGridViewExporter ID="grdEx" runat="server" FileName="fornecedores" 
                    GridViewID="grd" Landscape="True" PreserveGroupRowStates="True">
                </dxwgv:ASPxGridViewExporter>--%>
                <asp:SqlDataSource ID="sqlCategorias" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:connectionString %>" 
                    DeleteCommand="DELETE FROM tbProdutoCategoria WHERE (categoriaId = @categoriaId)" 
                    
                    InsertCommand="INSERT INTO tbProdutoCategoria(categoriaPaiId, categoriaNome, categoriaUrl, imagem1, imagem2, imagem3, CategoriaOrdem, categoriaNomeExibicao, exibirSite, categoriaTags, categoriaMl, categoriaDescricao, codigoEtiqueta, exibirNoFiltro, exibirMesmoCom1Filtro) VALUES (@categoriaPaiId, @categoriaNome, @categoriaUrl, @imagem1, @imagem2, @imagem3, @CategoriaOrdem, @categoriaNomeExibicao, @exibirSite, @categoriaTags, @categoriaMl, @categoriaDescricao, @codigoEtiqueta, @exibirNoFiltro, @exibirMesmoCom1Filtro)"
                    
                    SelectCommand="SELECT categoriaId, categoriaNome, categoriaUrl, imagem1, imagem2, imagem3, CategoriaOrdem, categoriaPaiId, categoriaNomeExibicao, exibirSite, categoriaTags, categoriaMl, categoriaDescricao, codigoEtiqueta, ISNULL(exibirNoFiltro, 'False') as exibirNoFiltro, ISNULL(exibirMesmoCom1Filtro, 'False') as exibirMesmoCom1Filtro FROM tbProdutoCategoria WHERE (categoriaPaiId = @categoriaPaiId AND idSite = @idSite)" 
                    
                    UpdateCommand="UPDATE tbProdutoCategoria SET categoriaPaiId = @categoriaPaiId, categoriaNome = @categoriaNome, categoriaUrl = @categoriaUrl, imagem1 = @imagem1, imagem2 = @imagem2, imagem3 = @imagem3, CategoriaOrdem = @CategoriaOrdem, categoriaNomeExibicao = @categoriaNomeExibicao, exibirSite = @exibirSite, categoriaTags = @categoriaTags, categoriaMl = @categoriaMl, categoriaDescricao = @categoriaDescricao, codigoEtiqueta = @codigoEtiqueta, exibirNoFiltro = @exibirNoFiltro, exibirMesmoCom1Filtro = @exibirMesmoCom1Filtro WHERE (categoriaId = @categoriaId)">
                    <DeleteParameters>
                        <asp:Parameter Name="categoriaId" />
                    </DeleteParameters>
                    <InsertParameters>
                        <asp:Parameter Name="categoriaPaiId" />
                        <asp:Parameter Name="categoriaNome" />
                        <asp:Parameter Name="categoriaNomeExibicao" />
                        <asp:Parameter Name="categoriaMl" />
                        <asp:Parameter Name="categoriaUrl" />
                        <asp:Parameter Name="imagem1" />
                        <asp:Parameter Name="imagem2" />
                        <asp:Parameter Name="imagem3" />
                        <asp:Parameter Name="CategoriaOrdem" />
                        <asp:Parameter Name="exibirSite" />
                        <asp:Parameter Name="categoriaTags" />
                        <asp:Parameter Name="categoriaDescricao" />
                        <asp:Parameter Name="codigoEtiqueta" />
                        <asp:Parameter Name="exibirNoFiltro" />
                        <asp:Parameter Name="exibirMesmoCom1Filtro" />
                    </InsertParameters>
                    <SelectParameters>
                        <asp:ControlParameter ControlID="treeCategorias" Name="categoriaPaiId" PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="ddlSite" Name="idSite" PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <asp:LinqDataSource ID="sqlSite" runat="server" 
                    ContextTypeName="dbCommerceDataContext" EnableDelete="True" 
                    EnableInsert="True" EnableUpdate="True" TableName="tbSites">
                </asp:LinqDataSource>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
    </table>
        </td>
    </tr>
</table>
    </asp:Content>
