﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="pedidosProdutosFaltando.aspx.cs" Inherits="admin_pedidosProdutosFaltando" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxTimer" TagPrefix="dxt" %>
<%@ Register assembly="CuteEditor" namespace="CuteEditor" tagprefix="CE" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" Runat="Server">
    <table cellpadding="0" cellspacing="0" style="width: 920px">
    <tr>
        <td class="tituloPaginas" valign="top">
            <asp:Label ID="lblAcao" runat="server">Produtos Faltando</asp:Label>
        </td>
    </tr>
        
    <tr>
        <td>
            
    <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                <table style="width: 100%;">
                    <tr class="rotulos" style="text-align: center; font-weight: bold;">
                        <td>
                            ID Pedido
                        </td>
                        <td>
                            ID Interno
                        </td>
                        <td>
                            ID da Empresa
                        </td>
                        <td>
                            Fornecedor
                        </td>
                        <td>
                            Romaneios
                        </td>
                        <td>
                            Complemento ID
                        </td>
                        <td>
                            Nome
                        </td>
                        <td>
                            Entregue
                        </td>
                        <td>
                            Gerar Pedido
                        </td>
                    </tr>
                    <asp:ListView runat="server" ID="lstProdutos" OnItemDataBound="lstProdutos_OnItemDataBound">
                        <ItemTemplate>
                            <tr class="rotulos" style="text-align: center">
                                <td>
                                    <asp:HiddenField runat="server" ID="hdfIdPedido" Value='<%# Eval("pedidoId") %>' />
                                    <asp:HiddenField runat="server" ID="hdfIdProduto" Value='<%# Eval("produtoId") %>' />
                                    <asp:HiddenField runat="server" ID="hdfItemFaltando" Value='<%# Eval("idPedidoProdutoFaltando") %>' />
                                    <asp:HiddenField runat="server" ID="hdfProdutoFornecedor" Value='<%# Eval("produtoFornecedor") %>' />
                                    <asp:Literal runat="server" ID="litIdPedidoCliente"></asp:Literal>
                                </td>
                                <td>
                                    <%# Eval("pedidoId") %>
                                </td>
                                <td>
                                    <%# Eval("produtoIdDaEmpresa") %>
                                </td>
                                <td>
                                   <asp:Literal runat="server" ID="litFornecedor"></asp:Literal>
                                </td>
                                <td>
                                   <asp:Literal runat="server" ID="litRomaneios"></asp:Literal>
                                </td>
                                <td>
                                    <%# Eval("complementoIdDaEmpresa") %>
                                </td>
                                <td style="padding-bottom: 30px;">
                                    <%# Eval("produtoNome") %><br/>
                                </td>
                                <td>
                                    <asp:CheckBox runat="server" ID="chkEntregue" /> 
                                </td>
                                <td>
                                    <asp:Button runat="server" ID="btnGerarPedido" Text="Gerar Pedido" CommandArgument='<%# Eval("idPedidoProdutoFaltando") %>' OnCommand="btnGerarPedido_OnCommand"/>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:ListView>    
                    
                    <tr class="rotulos" style="text-align: center; font-weight: bold;">
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            <asp:Button runat="server" ID="btnGravarEntregue" Text="Gravar" OnClick="btnGravarEntregue_OnClick"/>
                        </td>
                    </tr>      
                </table>        
            </td>
        </tr>
        <tr>
            <td style="text-align: right;">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td style="padding-top: 100px;">
                &nbsp;
            </td>
        </tr>
    </table>
        </td>
    </tr>
</table>
</asp:Content>