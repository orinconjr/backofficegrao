﻿<%@ Page Title="" Theme="Glass" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="comprasCondicaoPagamentoCadastro.aspx.cs" Inherits="admin_comprasCondicaoPagamentoCadastro" %>

<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxTimer" TagPrefix="dxt" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxMenu" Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxPopupControl" Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>

<%@ Register Assembly="CuteEditor" Namespace="CuteEditor" TagPrefix="CE" %>


<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" runat="Server">
    <table cellpadding="0" cellspacing="0" style="width: 920px">
        <tr>
            <td class="tituloPaginas" valign="top">
                <asp:Label ID="lblAcao" runat="server">Compras - Cadastro de Condição de Pagamento</asp:Label>
            </td>
        </tr>

        <tr>
            <td>

                <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
                    <tr>
                        <td>
                            <asp:TextBox runat="server" ID="hdfLista" Visible="False" />
                            <asp:TextBox runat="server" ID="hdfIdCondicao" Visible="False" />
                            &nbsp;
                        </td>
                    </tr>
                    <tr class="campos">
                        <td>Nome da Condição de Pagamento:<br />
                            <asp:TextBox runat="server" ID="txtNome" Width="300"></asp:TextBox>&nbsp;
                        </td>
                    </tr>
                    <tr class="campos">
                        <td>Tipo de condição de pagamento:<br />
                            <asp:RadioButton runat="server" ID="rdbTipoDia" Text="Prazo em Dias" Checked="true" GroupName="tipo" />&nbsp;&nbsp;&nbsp;
                <asp:RadioButton runat="server" ID="rdbTipoFechamento" Text="Fechamento com dia fixo" GroupName="tipo" />
                        </td>
                    </tr>
                    <tr class="campos">
                        <td>Prazo à partir de:<br />
                            <asp:RadioButton runat="server" ID="rdbPrazoPedido" Text="Data do Pedido" Checked="true" GroupName="prazo" />&nbsp;&nbsp;&nbsp;
                <asp:RadioButton runat="server" ID="rdbPrazoEntrega" Text="Entrega do Pedido" GroupName="prazo" />
                        </td>
                    </tr>
                    <tr class="campos">
                        <td>
                            <asp:CheckBox ID="chkIgnorarMes" runat="server" Text="Ignorar mês atual" />
                        </td>
                    </tr>
                    <tr class="rotulos">
                        <td style="font-weight: bold; padding-top: 30px;">Prazos:
                        </td>
                    </tr>
                    <tr class="campos">
                        <td>
                            <table style="width: 100%">
                                <tr>
                                    <td>Dias:<br />
                                        <dxe:ASPxTextBox ID="txtDias" runat="server" Width="100">
                                            <MaskSettings Mask="<0..999999g>" IncludeLiterals="None" />
                                            <ValidationSettings ErrorDisplayMode="ImageWithTooltip" />
                                        </dxe:ASPxTextBox>
                                        <asp:RequiredFieldValidator ID="rqvpreco" ValidationGroup="vgFornecedor" runat="server" ControlToValidate="txtDias" Display="Dynamic" ErrorMessage="Preencha a quantidade de dias" Font-Size="10px" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                    </td>
                                    <td style="text-align: right">&nbsp;<br />
                                        <asp:Button runat="server" ID="btnGravarPrazo" Text="Adicionar Prazo" OnClick="btnGravarPrazo_OnClick" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <dxwgv:ASPxGridView ID="grdPrazos" runat="server" AutoGenerateColumns="False" Width="834px" Cursor="auto" KeyFieldName="idComprasCondicaoPagamentoDias">
                                <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                                <Styles>
                                    <Footer Font-Bold="True">
                                    </Footer>
                                </Styles>
                                <SettingsText EmptyDataRow="Nenhum registro encontrado." />
                                <SettingsPager Position="Bottom" PageSize="50" ShowDisabledButtons="False" AlwaysShowPager="True">
                                    <Summary AllPagesText="Páginas: {0} - {1} ({2} items)" Text="Página {0} de {1} ({2} registros encontrados)" />
                                </SettingsPager>
                                <Settings ShowFilterRow="True" ShowGroupButtons="False" ShowHeaderFilterButton="True" ShowFooter="True" />
                                <SettingsEditing EditFormColumnCount="4" PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1" PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter" PopupEditFormWidth="700px" />
                                <Columns>
                                    <dxwgv:GridViewDataTextColumn Caption="ID" FieldName="idComprasFornecedor" Visible="False" VisibleIndex="0">
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Prazo" FieldName="dia">
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Remover" Name="remover" Width="80">
                                        <DataItemTemplate>
                                            <asp:ImageButton ImageUrl="~/admin/images/btExcluir.jpg" runat="server" OnCommand="btnRemover_OnCommand" ID="btnRemover" CommandArgument='<%# Eval("dia") %>' EnableViewState="False"></asp:ImageButton>
                                        </DataItemTemplate>
                                    </dxwgv:GridViewDataTextColumn>
                                </Columns>
                                <StylesEditors>
                                    <Label Font-Bold="True">
                                    </Label>
                                </StylesEditors>
                            </dxwgv:ASPxGridView>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right;">&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right;">
                            <asp:Button runat="server" ID="btnGravar" Text="Gravar Condição de Pagamento" OnClick="btnGravar_OnClick" />
                        </td>
                    </tr>

                </table>
            </td>
        </tr>
    </table>

    <asp:LinqDataSource ID="sqlComprasProduto" runat="server" ContextTypeName="dbCommerceDataContext" OrderBy="produto" TableName="tbComprasProdutos">
    </asp:LinqDataSource>
</asp:Content>
