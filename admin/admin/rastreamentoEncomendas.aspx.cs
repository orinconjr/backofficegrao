﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxCallback;
using DevExpress.Web.ASPxGridView;

public partial class admin_rastreamentoEncomendas : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack && !Page.IsCallback)
        {
            //atualizaRastreio();
        }
        fillGrid(false);
    }

    private void atualizaRastreio()
    {
        checaPedidosPendentesTnt();
        rnPedidos.atualizaTodosStatusRastreio();
    }

    public static void checaPedidosPendentesTnt()
    {
        var pedidosDc = new dbCommerceDataContext();
        var pedidosTnt = (from c in pedidosDc.tbPedidoEnvios
                          join d in pedidosDc.tbPedidoPacotes on c.idPedidoEnvio equals d.idPedidoEnvio into pacotes
                          where c.dataFimEmbalagem > Convert.ToDateTime("19/12/2015") && c.nfeStatus == 2 && pacotes.Any(x => (x.rastreio ?? "") == "")
                          orderby c.idPedidoEnvio descending
                          select c);
        foreach (var pedido in pedidosTnt)
        {
            string numeroTracking = checaRastreioTnt((int)pedido.idPedidoEnvio);
            if (!string.IsNullOrEmpty(numeroTracking))
            {
                var itensCodigo = (from c in pedidosDc.tbPedidoPacotes where c.idPedidoEnvio == pedido.idPedidoEnvio select c);
                foreach (var pacote in itensCodigo)
                {
                    pacote.rastreio = numeroTracking;
                }
                pedidosDc.SubmitChanges();
            }
        }
    }
    private static string checaRastreioTnt(int idPedidoEnvio)
    {
        var data = new dbCommerceDataContext();
        var nota = (from c in data.tbNotaFiscals where c.idPedidoEnvio == idPedidoEnvio select c).First();
        string numeroTracking = "";
        var parametros = new serviceTntLocalizacao.LocalizacaoIn();
        parametros.usuario = "atendimento@bark.com.br";
        parametros.nf = nota.numeroNota;
        parametros.nfSpecified = true;
        parametros.nfSerie = "1";
        parametros.cnpj = nota.tbEmpresa.cnpj;

        try
        {
            var consulta = new serviceTntLocalizacao.Localizacao();
            var retorno = consulta.localizaMercadoria(parametros);
            numeroTracking = retorno.conhecimento;

            if (!string.IsNullOrEmpty(numeroTracking))
            {
                var pacotes = (from c in data.tbPedidoPacotes where c.idPedidoEnvio == idPedidoEnvio select c);
                foreach (var pacote in pacotes)
                {
                    pacote.statusAtual = retorno.localizacao;
                    if (pacote.statusAtual.ToLower() == "entrega realizada")
                    {
                        pacote.rastreamentoConcluido = true;
                    }
                    data.SubmitChanges();
                }
            }
        }
        catch (Exception)
        {

        }
        //retorno.localizacao;
        //retorno.conhecimento;
        //http://app.tntbrasil.com.br/LocalizacaoSimplificada/resultado_localizacao.asp?cnpj=01404654135&nota=7858

        string teste = "";
        return numeroTracking;
    }
    private void fillGrid(bool rebind)
    {
        //grd.DataSource = sql;
        var data = new dbCommerceDataContext();
        var pedidos = (from c in data.tbPedidoPacotes
                       join d in data.tbPedidoEnvios on c.idPedido equals d.idPedido into envio
                       where c.prazoFinalEntrega > Convert.ToDateTime("19/12/2015") && c.despachado == true && c.rastreio != ""
                       orderby c.prazoFinalEntrega
            select new
            {
                c.idPedidoPacote,
                pedidoId = c.idPedido,
                c.rastreio,
                c.tbPedido.prazoFinalPedido,
                c.prazoFinalEntrega,
                c.formaDeEnvio,
                c.statusAtual,
                c.observacao,
                c.rastreamentoConcluido
            });
        if (rdbAberto.Checked) pedidos = pedidos.Where(x => (x.rastreamentoConcluido ?? false) == false);
        grd.DataSource = pedidos;
        if ((!Page.IsPostBack && !Page.IsCallback) | rebind) grd.DataBind();
    }

    protected void gridClientes_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType != GridViewRowType.Data)
        {
            return;
        }
        int pedidoId = 0;
        int.TryParse(grd.GetRowValues(e.VisibleIndex, new string[] { "pedidoId" }).ToString(), out pedidoId);

        var prazoFinalEntrega = grd.GetRowValues(e.VisibleIndex, new string[] { "prazoFinalEntrega" });
        if (prazoFinalEntrega != null)
        {
            DateTime dataLimite = DateTime.Now;
            DateTime.TryParse(prazoFinalEntrega.ToString(), out dataLimite);
            if (dataLimite.Date < DateTime.Now.Date)
            {
                int i = 0;
                while ((i < e.Row.Cells.Count))
                {
                    e.Row.Cells[i].Style.Add("background", "#FFC6C7");
                    i++;
                }
            }
        }

    }

    protected void grd_OnCustomUnboundColumnData(object sender, ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "pedidoIdCliente")
        {
            int pedidoId = Convert.ToInt32(e.GetListSourceFieldValue("pedidoId"));
            e.Value = rnFuncoes.retornaIdCliente(pedidoId);
        }
        /*if (e.Column.FieldName == "melhorFormaEntrega")
        {
            int pedidoId = Convert.ToInt32(e.GetListSourceFieldValue("pedidoId"));
            e.Value = determinaEntrega(pedidoId);
        }
        if (e.Column.FieldName == "dataLimitePostagem")
        {
            int pedidoId = Convert.ToInt32(e.GetListSourceFieldValue("pedidoId"));
            var data = new dbCommerceDataContext();
            DateTime dataRetorno = DateTime.Now;

            var dataLimite =
                (from c in data.tbPedidoFornecedorItems
                    where c.idPedido == pedidoId
                    orderby c.tbPedidoFornecedor.dataLimite descending
                    select c).FirstOrDefault();
            if (dataLimite != null)
            {
                dataRetorno = Convert.ToDateTime(dataLimite.tbPedidoFornecedor.dataLimite);
            }

            var itensPedidoDc = new dbCommerceDataContext();
            var itens = (from c in itensPedidoDc.tbItensPedidos
                         where c.pedidoId == pedidoId && c.prazoDeFabricacao != null
                         orderby c.prazoDeFabricacao descending
                         select c).FirstOrDefault();
            if (itens != null)
            {
                if (itens.prazoDeFabricacao > dataRetorno)
                {
                    dataRetorno = Convert.ToDateTime(itens.prazoDeFabricacao);
                }
            }

            var limitePedido = (from c in data.tbPedidos where c.pedidoId == pedidoId select c).First();
            if (limitePedido.prazoFinalPedido != null)
            {
                dataRetorno = Convert.ToDateTime(limitePedido.prazoFinalPedido);
            }

            e.Value = dataRetorno; 
        }*/
    }

    protected void btnAtualizarTodos_OnClick(object sender, EventArgs e)
    {
        atualizaRastreio();
        fillGrid(true);
    }

    protected void atualizarRastreioPedido(object sender, CommandEventArgs e)
    {
        var pedidoId = 0;
        int.TryParse(e.CommandArgument.ToString(), out pedidoId);
        if (pedidoId > 0)
        {
            rnPedidos.atualizaStatusRastreio(pedidoId);
            fillGrid(true);
        }
    }
    protected void rdbAberto_OnCheckedChanged(object sender, EventArgs e)
    {
        fillGrid(true);
    }

    protected void cbGravar_OnCallback(object source, CallbackEventArgs e)
    {
        atualizaRastreio();
        fillGrid(true);
    }
}