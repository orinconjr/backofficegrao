﻿<%@ Page Title="" Language="C#" Theme="Glass" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="carregamentoCaminhao.aspx.cs" Inherits="admin_carregamentoCaminhao" %>

<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxTimer" TagPrefix="dxt" %>
<%@ Register TagPrefix="dxe" Namespace="DevExpress.Web.ASPxMenu" Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxPopupControl" Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxEditors" Assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>
<%@ Register Assembly="CuteEditor" Namespace="CuteEditor" TagPrefix="CE" %>

<%@ Register Assembly="DevExpress.Web.v11.1" Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" runat="Server">
    <asp:ScriptManager ID="scriptManager" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="updPanel" runat="server">
        <ContentTemplate>
            <dx:ASPxPopupControl ID="popCarregamentoCaminhao" EnableCallBacks="false" runat="server" CloseAction="CloseButton" Modal="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
                ClientInstanceName="popCarregamentoCaminhao" HeaderText="Adicionar Chamado" AllowDragging="True" PopupAnimationType="None" EnableViewState="True" Height="500" AutoUpdatePosition="True">
                <ContentCollection>
                    <dx:PopupControlContentControl ID="PopupControlContentControl6" runat="server">
                        <asp:HiddenField runat="server" ID="hdfIdCarregamentoCaminhao" />

                        <div style="width: 800px; height: 600px;" id="divAbertura" runat="server" visible="false">
                            <table width="100%">
                                <tr class="rotulos" runat="server" id="trTransportadora" visible="true">
                                    <td colspan="5">
                                        <b>Transportadora:</b><br />
                                        <asp:CheckBoxList ID="chkTransportadora" RepeatLayout="Table" runat="server" OnSelectedIndexChanged="rdb_OnCheckedChanged"></asp:CheckBoxList>
                                        <%--<asp:DropDownList ID="ddlTransportadora" AutoPostBack="true" OnSelectedIndexChanged="ddlTransportadora_SelectedIndexChanged" runat="server"></asp:DropDownList>--%>
                                        <dx:ASPxButton ID="btnCriarCarregamentoTransportadora" runat="server" OnInit="btnAbrirCarregamento_Init" Text="Abertura Transportadora" OnClick="btnCriarCarregamentoTransportadora_Click" />
                                        <asp:Label ID="lblMensagem" runat="server" ForeColor="Red"></asp:Label>
                                    </td>
                                </tr>
                                <tr class="rotulos" id="trCabecalho" runat="server" visible="false">
                                    <td>
                                        <asp:Literal ID="litCabecalho" runat="server"></asp:Literal>
                                    </td>
                                </tr>
                                <tr class="rotulos" id="trAbertura" runat="server" visible="false">
                                    <td><b>Tipo de Caminhão:</b><br />
                                        <asp:DropDownList ID="ddlTipoCaminhao" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlTipoCaminhao_SelectedIndexChanged"></asp:DropDownList>
                                    </td>
                                    <td id="tdCaminhao" runat="server">
                                        <b>Caminhão:</b><br />
                                        <asp:DropDownList ID="ddlCaminhao" runat="server"></asp:DropDownList>
                                    </td>
                                    <td id="tdPlaca" runat="server"><b>Placa:</b><br />
                                        <asp:TextBox ID="txtPlaca" runat="server" MaxLength="7"></asp:TextBox>
                                    </td>
                                    <td id="tdMotorista" runat="server">
                                        <b>Motorista:</b><br />
                                        <asp:TextBox ID="txtMotorista" runat="server" MaxLength="50"></asp:TextBox>
                                    </td>
                                    <td id="tdCusto" runat="server">
                                        <b>Custo:</b><br />
                                        <asp:TextBox ID="txtCusto" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr id="trSalvar" runat="server">
                                    <td class="rotulos" colspan="5" style="text-align: right">
                                        <asp:Button runat="server" ID="btnGravarAberturaCarregamento" OnClick="btnGravarAberturaCarregamento_Click" Text="Gravar" />
                                    </td>
                                </tr>
                                <tr id="trGrid" runat="server" visible="false">
                                    <td colspan="5">
                                        <asp:GridView Width="100%" OnRowDataBound="gridCarregamentoTransportadora_RowDataBound"
                                            DataKeyNames="idCarregamentoCaminhao" ID="gridCarregamentoTransportadora" OnRowCommand="gridCarregamentoTransportadora_RowCommand"
                                            EmptyDataText="Nenhum carregamento de transportadora aberto" AutoGenerateColumns="false" runat="server">
                                            <Columns>
                                                <asp:BoundField DataField="idCarregamentoCaminhaoTransportadora" HeaderText="ID" />
                                                <asp:TemplateField HeaderText="Transportadora">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbTrasportadora" runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Usuário Abertura">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblUsuarioAbertura" runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="dataAbertura" HeaderText="Data Abertura" />
                                                <asp:TemplateField HeaderText="Usuário Fechamento">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblUsuarioFechamento" runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="dataFechamento" HeaderText="Data Fechamento" />
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr id="trFechamento" runat="server" visible="false">
                                    <td>
                                        <asp:Button ID="btnFecharCarregamentoCaminhao" runat="server" OnClick="btnFecharCarregamentoCaminhao_Click" Text="Fechar carregamento" />
                                        <asp:Label ID="lblMensagemFechamento" runat="server" ForeColor="Red"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </dx:PopupControlContentControl>
                </ContentCollection>
                <ContentStyle>
                    <Paddings PaddingBottom="5px" />
                </ContentStyle>
            </dx:ASPxPopupControl>

            <table cellpadding="0" cellspacing="0" style="width: 920px">
                <tr>
                    <td class="tituloPaginas" valign="top">
                        <asp:Label ID="lblAcao" runat="server">Carregamento de Caminhão</asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="rotulos" style="padding-bottom: 10px; padding-left: 10px;">Status:
                                    <asp:RadioButton runat="server" ID="rdbAbertos" Text="Carregamentos em aberto" Checked="true" GroupName="filtroChamado" AutoPostBack="True" OnCheckedChanged="rdb_OnCheckedChanged" />
                                    <asp:RadioButton runat="server" ID="rdbFinalizados" Text="Carregamentos finalizados" Checked="False" GroupName="filtroChamado" AutoPostBack="True" OnCheckedChanged="rdb_OnCheckedChanged" />
                                    <asp:RadioButton runat="server" ID="rdoTodos" Text="Todos" Checked="false" GroupName="filtroChamado" AutoPostBack="True" OnCheckedChanged="rdb_OnCheckedChanged" />
                                </td>
                            </tr>
                            <tr align="left">
                                <td>
                                    <dx:ASPxButton ID="btnAbrirCarregamento" OnClick="btnAbrirCarregamento_Click" ClientInstanceName="btnAbrirCarregamento" runat="server" Text="Abrir Novo Carregamento" Width="192px"></dx:ASPxButton>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    <dxwgv:ASPxGridView ID="grd" ClientInstanceName="grd" runat="server" AutoGenerateColumns="False"
                                        KeyFieldName="idCarregamentoCaminhao" Width="834px" Settings-ShowFilterRow="True" Settings-ShowFilterBar="Visible"
                                        Cursor="auto" OnHtmlRowCreated="grd_HtmlRowCreated" EnableViewState="False">
                                        <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                                        <Styles>
                                            <Footer Font-Bold="True">
                                            </Footer>
                                        </Styles>
                                        <SettingsText ConfirmDelete="Tem certeza que deseja excluir?" EmptyDataRow="Nenhum registro encontrado." />
                                        <SettingsPager Position="TopAndBottom" PageSize="500"
                                            ShowDisabledButtons="False" AlwaysShowPager="True">
                                            <Summary AllPagesText="Páginas: {0} - {1} ({2} items)"
                                                Text="Página {0} de {1} ({2} registros encontrados)" />
                                        </SettingsPager>
                                        <Settings ShowFilterRow="True" ShowGroupButtons="False" ShowFooter="True" />
                                        <SettingsEditing EditFormColumnCount="4"
                                            PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1"
                                            PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter"
                                            PopupEditFormWidth="700px" />
                                        <Columns>
                                            <dxwgv:GridViewDataTextColumn Caption="Id" FieldName="idCarregamentoCaminhao" VisibleIndex="0">
                                                <EditFormSettings Visible="False" />
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn Caption="Data Abertura" FieldName="dataAbertura" Name="dataAbertura" VisibleIndex="0">
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn Caption="Usuário Abertura" FieldName="idUsuarioAbertura" VisibleIndex="0">
                                                <DataItemTemplate>
                                                    <asp:Label ID="lblUsuarioAbertura" runat="server"></asp:Label>
                                                </DataItemTemplate>
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn Caption="Data Fechamento" FieldName="dataFechamento" Name="dataFechamento" VisibleIndex="0">
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn Caption="Usuário Fechamento" FieldName="idUsuarioFechamento" VisibleIndex="0">
                                                <DataItemTemplate>
                                                    <asp:Label ID="lblUsuarioFechamento" runat="server"></asp:Label>
                                                </DataItemTemplate>
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn Caption="Motorista" FieldName="nomeMotorista" Name="nomeMotorista" VisibleIndex="0">
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn Caption="Placa Caminhão" FieldName="placaCaminhao" Name="placaCaminhao" VisibleIndex="0">
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn Caption="Tipo" FieldName="idCarregamentoCaminhaoTipo" Name="idCarregamentoCaminhaoTipo" VisibleIndex="0">
                                                <DataItemTemplate>
                                                    <asp:Label ID="lblTipoCarregamento" runat="server"></asp:Label>
                                                </DataItemTemplate>
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn Caption="Caminhão" FieldName="idCaminhao" Name="idCaminhao" VisibleIndex="0">
                                                <DataItemTemplate>
                                                    <asp:Label ID="lblCaminhao" runat="server"></asp:Label>
                                                </DataItemTemplate>
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn Caption="transportadora" FieldName="nomeTransportadora" Name="nomeTrasportadora" VisibleIndex="0">
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn Caption="Custo" FieldName="custoTransporte" Name="custoTransporte" VisibleIndex="0">
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn Caption="Alterar" Name="alterar" VisibleIndex="0">
                                                <DataItemTemplate>
                                                    <asp:ImageButton ID="btnAlterar" CommandArgument='<%# Eval("idCarregamentoCaminhao") %>'
                                                        OnCommand="btnAlterar_Command" runat="server" ImageAlign="AbsMiddle" ImageUrl="images/btAlterar.jpg" />

                                                    <asp:HyperLink ID="linkImprimir" runat="server" Text="Imprimir" Target="_blank" Visible="false"></asp:HyperLink>

                                                </DataItemTemplate>
                                            </dxwgv:GridViewDataTextColumn>
                                        </Columns>
                                        <StylesEditors>
                                            <Label Font-Bold="True">
                                            </Label>
                                        </StylesEditors>
                                    </dxwgv:ASPxGridView>
                                </td>
                            </tr>
                            <tr class="rotulos">
                                <td style="padding-top: 30px; font-size: 20px;">&nbsp;
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>



</asp:Content>
