﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_comprasCondicaoPagamentoLista : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        fillGrid(false);
    }


    private void fillGrid(bool rebind)
    {
        var data = new dbCommerceDataContext();
        var produtos = (from c in data.tbComprasCondicaoPagamentos select c);
        grd.DataSource = produtos;
        if ((!Page.IsPostBack && !Page.IsCallback) | rebind) grd.DataBind();
    }


    protected void btPdf_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WritePdfToResponse();
    }
    protected void btXsl_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteXlsToResponse();
    }
    protected void btRtf_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteRtfToResponse();
    }
    protected void btCsv_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteCsvToResponse();
    }
}