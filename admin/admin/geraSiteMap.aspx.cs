﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;


public partial class admin_geraSiteMap : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            FileInfo file = new FileInfo(ConfigurationManager.AppSettings["caminhoFisico"].ToString() + "sitemap.xml");

            if (file.Exists)
            {
                file.Delete();
            }

            StreamWriter sw = file.CreateText();
            sw.WriteLine("<?xml version=\"1.0\" encoding=\"UTF-8\" ?> ");
            sw.WriteLine("<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd\">");

            var data = new dbCommerceDataContext();
            var categorias = (from c in data.tbProdutoCategorias where c.exibirSite == true select c).ToList();
            
            sw.WriteLine("<url>");
            sw.WriteLine("<loc>" + ConfigurationManager.AppSettings["caminhoVirtual"].ToString() + "</loc>");
            sw.WriteLine("<priority>1.00</priority>");
            sw.WriteLine("<changefreq>daily</changefreq>");
            sw.WriteLine("</url>");

            foreach (var categoria in categorias)
            {
                sw.WriteLine("<url>");
                sw.WriteLine("<loc>" + ConfigurationManager.AppSettings["caminhoVirtual"].ToString() + categoria.categoriaUrl + "/</loc>");
                sw.WriteLine("<priority>1.00</priority>");
                sw.WriteLine("<changefreq>daily</changefreq>");
                sw.WriteLine("</url>");
            }

            var produtos = (from c in data.tbProdutos where c.produtoAtivo.ToLower() == "true" select c).ToList();

            foreach (var produto in produtos)
            {
                var categoriasProduto = (from c in data.tbJuncaoProdutoCategorias where c.produtoId == produto.produtoId select c).ToList();
                var categoriaRaiz = categoriasProduto.FirstOrDefault(x => x.tbProdutoCategoria.categoriaPaiId == 0 && x.tbProdutoCategoria.exibirSite == true);
                if (categoriaRaiz != null)
                {
                    sw.WriteLine("<url>");
                    sw.WriteLine("<loc>" + ConfigurationManager.AppSettings["caminhoVirtual"].ToString() +
                                 categoriaRaiz.tbProdutoCategoria.categoriaUrl + "/" + produto.produtoUrl +
                                 "/</loc>");
                    sw.WriteLine("<priority>1.00</priority>");
                    sw.WriteLine("<changefreq>daily</changefreq>");
                    sw.WriteLine("</url>");
                }
            }

            /*var corData = new dbCommerceDataContext();
            var cores = (from c in corData.tbCores select c);
            foreach (var cor in cores)
            {
                sw.WriteLine("<url>");
                sw.WriteLine("<loc>" + cor.corUrl + "</loc>");
                sw.WriteLine("<priority>1.00</priority>");
                sw.WriteLine("<changefreq>daily</changefreq>");
                sw.WriteLine("</url>");
            }*/

            sw.WriteLine("</urlset>");


            sw.WriteLine();
            sw.WriteLine();
            sw.Close();
        }
    }
}
