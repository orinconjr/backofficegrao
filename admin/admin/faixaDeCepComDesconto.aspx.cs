﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxTabControl;
using DevExpress.Web.ASPxEditors;
using DevExpress.Data;
using DevExpress.Web.ASPxUploadControl;
using System.IO;
using System.Collections.Generic;

public partial class admin_faixaDeCepComDesconto : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    //protected void btPdf_Click(object sender, ImageClickEventArgs e)
    //{
    //    this.grdEx.WritePdfToResponse();
    //}
    //protected void btXsl_Click(object sender, ImageClickEventArgs e)
    //{
    //    this.grdEx.WriteXlsToResponse();
    //}
    //protected void btRtf_Click(object sender, ImageClickEventArgs e)
    //{
    //    this.grdEx.WriteRtfToResponse();
    //}
    //protected void btCsv_Click(object sender, ImageClickEventArgs e)
    //{
    //    this.grdEx.WriteCsvToResponse();
    //}

    protected void grd_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType != GridViewRowType.EditForm) return;
        {
            TextBox txtValorInicial = (TextBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["valorInicial"], "txtValorInicial");
            txtValorInicial.Attributes.Add("onkeyPress", "return moeda(this,'.',',',event);");

            TextBox txtValorFinal = (TextBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["valorFinal"], "txtValorFinal");
            txtValorFinal.Attributes.Add("onkeyPress", "return moeda(this,'.',',',event);");
        }
    }

    protected void grd_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        TextBox txtValorInicial = (TextBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["valorInicial"], "txtValorInicial");
        e.NewValues["valorInicial"] = txtValorInicial.Text;

        TextBox txtValorFinal = (TextBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["valorFinal"], "txtValorFinal");
        e.NewValues["valorFinal"] = txtValorFinal.Text;
    }

    protected void grd_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        TextBox txtValorInicial = (TextBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["valorInicial"], "txtValorInicial");
        e.NewValues["valorInicial"] = txtValorInicial.Text;

        TextBox txtValorFinal = (TextBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["valorFinal"], "txtValorFinal");
        e.NewValues["valorFinal"] = txtValorFinal.Text;
    }
 }
