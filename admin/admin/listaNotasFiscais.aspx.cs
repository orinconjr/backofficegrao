﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Data.Filtering;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxHtmlEditor.Internal;
using SpreadsheetLight;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

public partial class admin_listaNotasFiscais : System.Web.UI.Page
{
    DateTemplate dateTemplate = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            txtDtInicial.Attributes.Add("onkeypress", "Mascaras('DATA', '" + txtDtInicial.ClientID + "');");
            txtDtFinal.Attributes.Add("onkeypress", "Mascaras('DATA', '" + txtDtFinal.ClientID + "');");
        }

        txtValorDiferenca.Attributes.Add("onkeyPress", "return moeda(this,'.',',',event);");
        txtValorReversa.Attributes.Add("onkeyPress", "return moeda(this,'.',',',event);");
        txtValorCobrancaAdicional.Attributes.Add("onkeyPress", "return moeda(this,'.',',',event);");
        txtDataFatura.Attributes.Add("onkeypress", "Mascaras('DATA', '" + txtDataFatura.ClientID + "');");
        txtDataPagamentoFatura.Attributes.Add("onkeypress", "Mascaras('DATA', '" + txtDataFatura.ClientID + "');");
        fillGrid(false);
    }

    private void fillGrid(bool rebind)
    {
        var data = new dbCommerceDataContext();


        using (
            var txn = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required,
                new System.Transactions.TransactionOptions
                {
                    IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                }))
        {
            string connectionString = ConfigurationManager.ConnectionStrings["connectionString"].ToString();

            using (
                    SqlConnection conn =
                        new SqlConnection(connectionString))
            {
                try
                {
                    var dataFiltro = new DateTime(2017, 6, 1);
                    SqlCommand command = conn.CreateCommand();
                    command.Connection.Open();
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "listaNotasFiscais";
                    command.Parameters.AddWithValue("@dataFiltro", dataFiltro);
                    SqlDataReader reader = command.ExecuteReader(CommandBehavior.CloseConnection);

                    DataTable pedidos = new DataTable();

                    pedidos.Load(reader);
                    grd.DataSource = pedidos;
                }
                catch (Exception ex)
                {
                    Response.Write(ex.Message);
                }
            }

           
        }
        if ((!Page.IsPostBack && !Page.IsCallback) | rebind) grd.DataBind();
    }
    protected void btPdf_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WritePdfToResponse();
    }
    protected void btXsl_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteXlsToResponse();

    }
    protected void btRtf_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteRtfToResponse();
    }
    protected void btCsv_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteCsvToResponse();
    }

    protected void grid_AutoFilterCellEditorCreate(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewEditorCreateEventArgs e)
    {
        ASPxGridView grid = sender as ASPxGridView;
        if (e.Column.FieldName == "dataSendoEmbalado")
        {
            DropDownEditProperties dde = new DropDownEditProperties();
            dde.EnableClientSideAPI = true;

            dateTemplate = new DateTemplate();

            dde.DropDownWindowTemplate = dateTemplate;
            e.EditorProperties = dde;
        }
    }
    protected void grid_AutoFilterCellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
    {
        ASPxGridView grid = sender as ASPxGridView;
        if (e.Column.FieldName == "dataSendoEmbalado")
        {
            ASPxDropDownEdit dde = e.Editor as ASPxDropDownEdit;

            dde.ClientSideEvents.CloseUp = String.Format("function (s, e) {{ ApplyFilter(s, {0}, {1}); }}", dateTemplate.cIdFrom, dateTemplate.cIdTo);
            //dde.ClientSideEvents.DropDown = String.Format("function (s, e) {{ OnDropDown(s, {0}, {1}); }}", dateTemplate.cIdFrom, dateTemplate.cIdTo);
            dde.ReadOnly = true;
        }
    }

    protected void grid_ProcessColumnAutoFilter(object sender, ASPxGridViewAutoFilterEventArgs e)
    {
        if (e.Value == "|") return;
        if (e.Column.FieldName == "dataSendoEmbalado")
        {
            if (e.Kind == GridViewAutoFilterEventKind.CreateCriteria)
            {
                Session["dataSendoEmbalado"] = e.Value;
                String[] dates = e.Value.Split('|');
                DateTime dateFrom = Convert.ToDateTime(dates[0]),
                    dateTo = Convert.ToDateTime(dates[1]);
                e.Criteria = (new OperandProperty("dataSendoEmbalado") >= dateFrom) &
                             (new OperandProperty("dataSendoEmbalado") <= dateTo);
            }
            else
            {
                if (Session["dataSendoEmbalado"] != null)
                    e.Value = Session["dataSendoEmbalado"].ToString();
            }
        }
    }

    protected void btnChecarPedidosMl_OnClick(object sender, EventArgs e)
    {
        //var meli = mlIntegracao.verificaAutenticacao("pedidos.aspx", Request.QueryString["code"]);
        mlIntegracao.checaPedidosML();
        rnIntegracoes.checaPedidosExtra(true);
        grd.DataBind();
    }

    protected void grd_OnCustomUnboundColumnData(object sender, ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "pedidoIdCliente")
        {
            int pedidoId = Convert.ToInt32(e.GetListSourceFieldValue("pedidoId"));
            e.Value = rnFuncoes.retornaIdCliente(pedidoId);
        }

    }

    protected void btnEditarValor_OnCommand(object sender, CommandEventArgs e)
    {
        hdfIdPedidoEnvio.Value = e.CommandArgument.ToString();
        int idPedidoEnvio = Convert.ToInt32(e.CommandArgument);
        var pedidoDc = new dbCommerceDataContext();
        var pedido = (from c in pedidoDc.tbPedidoEnvios where c.idPedidoEnvio == idPedidoEnvio select c).FirstOrDefault();

        if (pedido.valorCobradoTransportadora > 0)
        {
            txtValorDiferenca.Text = pedido.valorCobradoTransportadora.ToString();
        }
        else
        {
            txtValorDiferenca.Text = "0";
        }

        if (pedido.dataCobrancaTransportadora != null)
        {
            txtDataFatura.Text = pedido.dataCobrancaTransportadora.Value.ToShortDateString();
        }
        else
        {
            txtDataFatura.Text = DateTime.Now.ToShortDateString();
        }

        if (pedido.dataPagamentoFatura != null)
        {
            txtDataPagamentoFatura.Text = pedido.dataPagamentoFatura.Value.ToShortDateString();
        }
        else
        {
            //txtDataFatura.Text = DateTime.Now.ToShortDateString();
        }

        if (pedido.valorReversa != null)
        {
            txtValorReversa.Text = pedido.valorReversa.ToString();
        }
        else
        {
            txtValorReversa.Text = "0";
        }

        if (pedido.valorCobrancaAdicional != null)
        {
            txtValorCobrancaAdicional.Text = pedido.valorCobrancaAdicional.ToString();
        }
        else
        {
            txtValorCobrancaAdicional.Text = "0";
        }

        if (pedido.comentariosCobranca != null)
        {
            txtObservacoes.Text = pedido.comentariosCobranca;
        }
        else
        {
            txtObservacoes.Text = "";
        }

        litPedidoIdDiferenca.Text = pedido.idPedido.ToString();
        pnGridEditar.Visible = true;
        pnGridVerPedidos.Visible = false;
    }

    protected void btnGravarDiferenca_onClick(object sender, EventArgs e)
    {
        int idPedidoEnvio = Convert.ToInt32(hdfIdPedidoEnvio.Value);
        var pedidoDc = new dbCommerceDataContext();
        var pedido = (from c in pedidoDc.tbPedidoEnvios where c.idPedidoEnvio == idPedidoEnvio select c).FirstOrDefault();
        pedido.dataCobrancaTransportadora = Convert.ToDateTime(txtDataFatura.Text);
        if (!string.IsNullOrEmpty(txtDataPagamentoFatura.Text))
            pedido.dataPagamentoFatura = Convert.ToDateTime(txtDataPagamentoFatura.Text);
        pedido.valorCobradoTransportadora = Convert.ToDecimal(txtValorDiferenca.Text);
        pedido.valorReversa = Convert.ToDecimal(txtValorReversa.Text);
        pedido.valorCobrancaAdicional = Convert.ToDecimal(txtValorCobrancaAdicional.Text);
        pedido.comentariosCobranca = txtObservacoes.Text;
        pedidoDc.SubmitChanges();
        txtValorDiferenca.Text = "0";
        pnGridEditar.Visible = false;
        pnGridVerPedidos.Visible = true;
        fillGrid(true);
    }

    protected void btnEnviarXml_OnCommand(object sender, CommandEventArgs e)
    {
        try
        {
            string[] dados = e.CommandArgument.ToString().Split('#');
            int numeroNota = Convert.ToInt32(dados[0]);
            int idPedidoEnvio = Convert.ToInt32(dados[2]);
            tbNotaFiscal nota;

            using (var data = new dbCommerceDataContext())
            {
                nota = (from c in data.tbNotaFiscals where c.idPedidoEnvio == idPedidoEnvio && c.numeroNota == numeroNota select c).FirstOrDefault();
            }

            if (nota == null) return;

            tbQueue queueNota;

            string xmlNota = "";

            if (string.IsNullOrEmpty(nota.xmlBase64))
            {
                xmlNota = rnNotaFiscal.retornaXmlNota(nota.numeroNota, nota.idNotaFiscal);
            }
            else
            {
                xmlNota = rnNotaFiscal.retornaXmlNotaFromBase64(nota.xmlBase64);
            }

            if (dados[1] == "belle")
            {
                queueNota = new tbQueue
                {
                    agendamento = DateTime.Now,
                    idRelacionado = numeroNota,
                    tipoQueue = 27,
                    concluido = false,
                    andamento = false,
                    mensagem = xmlNota
                };
            }
            else
            {
                queueNota = new tbQueue
                {
                    agendamento = DateTime.Now,
                    idRelacionado = numeroNota,
                    tipoQueue = 3,
                    concluido = false,
                    andamento = false,
                    mensagem = xmlNota
                };

            }

            using (var data = new dbCommerceDataContext())
            {
                data.tbQueues.InsertOnSubmit(queueNota);
                data.SubmitChanges();
            }

            Response.Write("<script>alert('Xml enviado com sucesso!')</script>");
        }
        catch (Exception)
        {

        }


    }

    protected void btnInclusaoDefeitoTransp_InclusaoSinistro_OnClick(object sender, EventArgs e)
    {
        DateTime dtInicial = Convert.ToDateTime(txtDtInicial.Text);
        DateTime dtFinal = Convert.ToDateTime(txtDtFinal.Text);

        SLDocument sl = new SLDocument();
        SLStyle style1 = sl.CreateStyle();
        style1.Font.Bold = true;
        sl.SetRowStyle(1, style1);

        if (rblMotivoInclusao.SelectedIndex == -1)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('Selecione uma opção de relatório!');", true);
            return;
        }

        try
        {
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("Content-Disposition", "attachment; filename=" + (rblMotivoInclusao.SelectedIndex == 0 ? "relatorio_Inclusao_por_defeito_transporte.xlsx" : "relatorio_Inclusao_por_sinistro.xlsx"));

            sl.SetCellValue("A1", "Data");
            sl.SetCellValue("B1", "Pedido");
            sl.SetCellValue("C1", "Rastreio");
            sl.SetCellValue("D1", "Nota");
            sl.SetCellValue("E1", "Valor");
            sl.SetCellValue("F1", "Transportadora");

            using (var data = new dbCommerceDataContext())
            {
                string motivoInclusao = rblMotivoInclusao.SelectedValue;

                var pedidos =
                    (from c in data.tbItensPedidos
                     join pe in data.tbProdutoEstoques on c.pedidoId equals pe.pedidoId
                     where c.motivoAdicao == motivoInclusao
                     && (c.dataDaCriacao.Date >= dtInicial.Date && c.dataDaCriacao <= dtFinal.Date)
                     orderby c.dataDaCriacao
                     select new
                     {
                         c.dataDaCriacao,
                         c.pedidoId,
                         pe.tbPedidoEnvio.tbPedidoPacotes.FirstOrDefault().rastreio,
                         nota = pe.tbPedidoEnvio.nfeNumero,
                         c.itemValor,
                         transportadora = pe.tbPedidoEnvio.formaDeEnvio
                     }).ToList().Distinct();

                int linha = 2;
                foreach (var pedido in pedidos)
                {
                    sl.SetCellValue(linha, 1, pedido.dataDaCriacao.ToShortDateString());
                    sl.SetCellValue(linha, 2, rnFuncoes.retornaIdCliente(pedido.pedidoId));
                    sl.SetCellValue(linha, 3, pedido.rastreio);
                    sl.SetCellValue(linha, 4, pedido.nota.ToString());
                    sl.SetCellValue(linha, 5, pedido.itemValor.ToString("N"));
                    sl.SetCellValue(linha, 6, pedido.transportadora);
                    linha++;
                }
            }

            //Exporta 
            sl.SaveAs(Response.OutputStream);
            Response.End();
        }
        catch (Exception ex)
        {

        }
    }
}
