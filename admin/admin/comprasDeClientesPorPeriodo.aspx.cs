﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxTabControl;
using DevExpress.Web.ASPxEditors;
using DevExpress.Data;
using DevExpress.Web.ASPxUploadControl;
using System.IO;
using System.Collections.Generic;

public partial class admin_comprasDeClientesPorPeriodo : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            txtDataInicial.Attributes.Add("onkeypress", "Mascaras('DATA', '" + txtDataInicial.ClientID + "');");
            txtDataFinal.Attributes.Add("onkeypress", "Mascaras('DATA', '" + txtDataFinal.ClientID + "');");
        }
    }

    protected void grd_HtmlRowCreated(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType != GridViewRowType.Data) return;
        {
            try
            {
                Label lblQuantidadeComprada = (Label)grd.FindRowCellTemplateControl(e.VisibleIndex, (GridViewDataColumn)grd.Columns["quantidadeComprada"], "lblQuantidadeComprada");
                Label lblValorComprado = (Label)grd.FindRowCellTemplateControl(e.VisibleIndex, (GridViewDataColumn)grd.Columns["valorComprado"], "lblValorComprado");
                Label lblPedidoMedio = (Label)grd.FindRowCellTemplateControl(e.VisibleIndex, (GridViewDataColumn)grd.Columns["pedidoMedio"], "lblPedidoMedio");
                try
                {
                    decimal valorComprado = Convert.ToDecimal(lblValorComprado.Text);
                }
                catch (Exception)
                {
                    
                } 
                int quantidadeComprada = Convert.ToInt32(lblQuantidadeComprada.Text);
                decimal pedidoMedio = Convert.ToDecimal(lblValorComprado.Text.Replace("R$", "").Replace(" ", "")) / Convert.ToInt32(lblQuantidadeComprada.Text);
                lblPedidoMedio.Text = String.Format("{0:c}", decimal.Parse(pedidoMedio.ToString()));
            }
            catch (Exception)
            {
                
            }
        }
    }

    protected void btPdf_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WritePdfToResponse();
    }
    protected void btXsl_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteXlsToResponse();
    }
    protected void btRtf_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteRtfToResponse();
    }
    protected void btCsv_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteCsvToResponse();
    }
}
