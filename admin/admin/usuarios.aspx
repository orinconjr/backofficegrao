﻿<%@ Page Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="usuarios.aspx.cs" Inherits="admin_usuarios" Theme="Glass" MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" runat="Server">
    <table cellpadding="0" cellspacing="0" style="width: 920px"> 
        <tr>
            <td class="tituloPaginas" valign="top">
                <asp:Label ID="lblAcao" runat="server">Usuários</asp:Label>
            </td>
        </tr>
        <tr>
            <td>

                <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
                    <tr>
                        <td align="right">
                            <table cellpadding="0" cellspacing="0" style="width: 834px">
                                <tr>
                                    <td align="right">&nbsp;</td>
                                    <td height="38"
                                        style="background-image: url('images/bkExportar.jpg'); padding-bottom: 1px;"
                                        valign="bottom" width="231">
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 97px">&nbsp;</td>
                                                <td style="width: 33px">
                                                    <asp:ImageButton ID="btPdf" runat="server" ImageUrl="~/admin/images/btPdf.jpg"
                                                        Style="margin-left: 5px" />
                                                </td>
                                                <td style="width: 31px">
                                                    <asp:ImageButton ID="btXsl" runat="server" ImageUrl="~/admin/images/btXls.jpg"
                                                        Style="margin-left: 5px" />
                                                </td>
                                                <td style="width: 36px">
                                                    <asp:ImageButton ID="btRtf" runat="server" ImageUrl="~/admin/images/btRtf.jpg"
                                                        Style="margin-left: 5px" />
                                                </td>
                                                <td valign="bottom">
                                                    <asp:ImageButton ID="btCsv" runat="server" ImageUrl="~/admin/images/btCsv.jpg"
                                                        Style="margin-left: 5px" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <dxwgv:ASPxGridView ID="grd" runat="server" AutoGenerateColumns="False"
                                DataSourceID="sqlUsuarios" KeyFieldName="usuarioId" Width="834px" OnRowUpdating="grd_OnRowUpdating"
                                Cursor="auto" EnableCallBacks="False"
                                OnHtmlRowCreated="grd_HtmlRowCreated" OnRowInserted="grd_RowInserted"
                                OnRowUpdated="grd_RowUpdated">
                                <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                                <SettingsText ConfirmDelete="Tem certeza que deseja excluir?"
                                    EmptyDataRow="Nenhum registro encontrado." />
                                <SettingsPager Position="TopAndBottom" PageSize="50"
                                    ShowDisabledButtons="False" AlwaysShowPager="True">
                                    <Summary AllPagesText="Páginas: {0} - {1} ({2} items)"
                                        Text="Página {0} de {1} ({2} registros encontrados)" />
                                </SettingsPager>
                                <Settings ShowFilterRow="True" ShowGroupButtons="False" />
                                <SettingsEditing EditFormColumnCount="4"
                                    PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1"
                                    PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter"
                                    PopupEditFormWidth="700px" />
                                <Columns>
                                    <dxwgv:GridViewDataTextColumn Caption="ID do usuário" Width="100px"
                                        FieldName="usuarioId" ReadOnly="True" VisibleIndex="0">
                                        <Settings AutoFilterCondition="Contains" />
                                        <EditFormSettings Visible="False" CaptionLocation="Top" ColumnSpan="2" />
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Departamento" FieldName="idDepartamento"
                                        VisibleIndex="1" Width="444px" Visible="false" Name="Departamento" >
                                        <EditFormSettings CaptionLocation="None" Visible="True" VisibleIndex="1" />
                                        <EditItemTemplate>
                                            <div class="rotulos">
                                                Departamento
                                                    <asp:DropDownList runat="server" OnInit="ddlDepartamento_Init"  ID="ddlDepartamento" >
                                                    </asp:DropDownList>
                                            </div>
                                        </EditItemTemplate>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Nome" FieldName="usuarioNome"
                                        VisibleIndex="1">
                                        <PropertiesTextEdit>
                                            <ValidationSettings ErrorText="" SetFocusOnError="True">
                                                <RequiredField ErrorText="Preencha o campo nome." IsRequired="True" />
                                            </ValidationSettings>
                                        </PropertiesTextEdit>
                                        <Settings AutoFilterCondition="Contains" />
                                        <EditFormSettings CaptionLocation="Top" Visible="True"
                                            VisibleIndex="1" />
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn VisibleIndex="2" Visible="False" Caption="Senha" Width="250px" FieldName="usuarioSenha" PropertiesTextEdit-Password="True">
                                        <EditFormSettings CaptionLocation="Top" Visible="True" />
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataColumn Name="paginasPermitidas" Visible="False"
                                        VisibleIndex="2">
                                        <EditFormSettings CaptionLocation="Top" ColumnSpan="4" Visible="True" />
                                        <EditCellStyle Font-Strikeout="False">
                                        </EditCellStyle>
                                        <EditItemTemplate>
                                            <asp:CheckBoxList ID="ckbPaginasPermitidas" runat="server" CssClass="rotulos" RepeatColumns="4" Width="100%">
                                                <asp:ListItem Value="bemvindo.aspx">Bem vindo</asp:ListItem>
                                                <asp:ListItem Value="pedidos.aspx">Pedidos</asp:ListItem>
                                                <asp:ListItem Value="pedido.aspx">Pedido</asp:ListItem>
                                                <asp:ListItem Value="popCartao.aspx">Dados do cartão</asp:ListItem>
                                                <asp:ListItem Value="categoriacad.aspx">Categorias Novo</asp:ListItem>
                                                <asp:ListItem Value="categorias.aspx">Categorias</asp:ListItem>
                                                <asp:ListItem Value="clientes.aspx">Clientes</asp:ListItem>
                                                <asp:ListItem Value="historicoDeCliente.aspx">Histórico de cliente</asp:ListItem>
                                                <asp:ListItem Value="comentarios.aspx">Comentários</asp:ListItem>
                                                <asp:ListItem Value="compreJunto.aspx">Compre junto</asp:ListItem>
                                                <asp:ListItem Value="compreJuntoItens.aspx">Compre junto itens</asp:ListItem>
                                                <asp:ListItem Value="especificacaoItens.aspx">Itens das especificações</asp:ListItem>
                                                <asp:ListItem Value="especificacoes.aspx">Especificações</asp:ListItem>
                                                <asp:ListItem Value="faixaDeCep.aspx">Faixa de cep</asp:ListItem>
                                                <asp:ListItem Value="faixaDeCepPesosPrecos.aspx">Faixa de cep pesos e preços</asp:ListItem>
                                                <asp:ListItem Value="fornecedores.aspx">Fornecedores</asp:ListItem>
                                                <asp:ListItem Value="marcas.aspx">Marcas</asp:ListItem>
                                                <asp:ListItem Value="produtoAlt.aspx">Alterar produtos</asp:ListItem>
                                                <asp:ListItem Value="produtoCad.aspx">Cadastrar produtos</asp:ListItem>
                                                <asp:ListItem Value="produtos.aspx">Lista de produtos</asp:ListItem>
                                                <asp:ListItem Value="destaques.aspx">Produtos em destaque</asp:ListItem>
                                                <asp:ListItem Value="vendasPorPeriodo.aspx">Relatório de vendas por período</asp:ListItem>
                                                <asp:ListItem Value="vendasPorPeriodoUf.aspx">Relatório de vendas por período (UF)</asp:ListItem>
                                                <asp:ListItem Value="produtosPorPeriodo.aspx">Relatório de produtos por período</asp:ListItem>
                                                <asp:ListItem Value="comprasDeClientesPorPeriodo.aspx">Relatório de compras de cliente por período</asp:ListItem>
                                                <asp:ListItem Value="relatorioGeral.aspx">Relatório geral</asp:ListItem>
                                                <asp:ListItem Value="palavrasPesquisadas.aspx">Relatório de palavras pesquisadas</asp:ListItem>
                                                <asp:ListItem Value="produtosEmEspera.aspx">Produtos em espera</asp:ListItem>
                                                <asp:ListItem Value="condicoesDePagamento.aspx">Condições de pagamento</asp:ListItem>
                                                <asp:ListItem Value="contasParaDeposito.aspx">Contas para depósito</asp:ListItem>
                                                <asp:ListItem Value="tipoDeEntrega.aspx">Tipos de entregas</asp:ListItem>
                                                <asp:ListItem Value="usuarios.aspx">Usuários</asp:ListItem>
                                                <asp:ListItem Value="usuarioDuplicarPermissao.aspx">Usuário - Duplicar Permissões</asp:ListItem>
                                                <asp:ListItem Value="usuarioExpedicao.aspx">Usuários Expedição</asp:ListItem>
                                                <asp:ListItem Value="paises.aspx">Entrega internacional</asp:ListItem>
                                                <asp:ListItem Value="paisesPrecosEPesos.aspx">Preços de entregas internacional</asp:ListItem>
                                                <asp:ListItem Value="etiqueta.aspx">Etiqueta</asp:ListItem>
                                                <asp:ListItem Value="relacionarProdutosComCategoria.aspx">Relacionar produtos com categorias</asp:ListItem>
                                                <asp:ListItem Value="faixaDeCepComDesconto.aspx">Faixa de cep com descontos</asp:ListItem>
                                                <asp:ListItem Value="geraXml.aspx">Gerar xml</asp:ListItem>
                                                <asp:ListItem Value="geraSiteMap.aspx">Gerar sitemap</asp:ListItem>
                                                <asp:ListItem Value="pedidosEmFabricacao.aspx">Pedidos em Fabricação</asp:ListItem>
                                                <asp:ListItem Value="produtosEmFabricacao.aspx">Produtos em Fabricação</asp:ListItem>
                                                <asp:ListItem Value="despesaCategoria.aspx">Categoria de Despesa</asp:ListItem>
                                                <asp:ListItem Value="despesa.aspx">Despesas</asp:ListItem>
                                                <asp:ListItem Value="pedidosEmbalado.aspx">Pedidos sendo Embalados</asp:ListItem>
                                                <asp:ListItem Value="sendoEnderecadoAtual.aspx">Produtos sendo endereçados</asp:ListItem>
                                                <asp:ListItem Value="produtosEditarCategoria.aspx">Editar Categorias</asp:ListItem>
                                                <asp:ListItem Value="gerarIntegracoes.aspx">Gerar Integracoes</asp:ListItem>
                                                <asp:ListItem Value="relacionarProdutosComCategoriasEmLote.aspx">Relacionar produtos com categorias</asp:ListItem>
                                                <asp:ListItem Value="gerarNotasPorData.aspx">Gerar notas fiscais</asp:ListItem>
                                                <asp:ListItem Value="inutilizarNota.aspx">Inutilizar Nota Fiscal</asp:ListItem>
                                                <asp:ListItem Value="mlIntegracaoProdutos.aspx">Integração Mercado Livre</asp:ListItem>
                                                <asp:ListItem Value="pedidoFornecedor.aspx">Pedidos ao Fornecedor</asp:ListItem>
                                                <asp:ListItem Value="pedidoFornecedorAvulso.aspx">Pedidos ao Fornecedor Avulsos</asp:ListItem>
                                                <asp:ListItem Value="pedidoFornecedorEditar.aspx">Editar Pedidos ao Fornecedor</asp:ListItem>
                                                <asp:ListItem Value="pedidoFornecedorVisualizar.aspx">Visualizar Pedidos ao Fornecedor</asp:ListItem>
                                                <asp:ListItem Value="pedidosJadlog.aspx">Pedidos Jadlog</asp:ListItem>
                                                <asp:ListItem Value="depoimentos.aspx">Depoimentos</asp:ListItem>
                                                <asp:ListItem Value="pedidosSeparacaoCompleto.aspx">Pedidos Completos</asp:ListItem>
                                                <asp:ListItem Value="etiquetaLote.aspx">Gerar etiquetas</asp:ListItem>
                                                <asp:ListItem Value="pedidosSeparacaoCompletos2.aspx">Pedidos Completos (individual)</asp:ListItem>
                                                <asp:ListItem Value="etiquetaLote2.aspx">Gerar etiquetas</asp:ListItem>
                                                <asp:ListItem Value="pedidoFornecedorAvulsoNovo.aspx">Cadastrar Pedido Fornecedor Avulso</asp:ListItem>
                                                <asp:ListItem Value="colecoes.aspx">Coleções</asp:ListItem>
                                                <asp:ListItem Value="relacionarProdutosComColecoes.aspx">Relacionar produtos com Coleções</asp:ListItem>
                                                <asp:ListItem Value="produtoFreteDesconto.aspx">Produtos com Desconto no Frete</asp:ListItem>
                                                <asp:ListItem Value="produtosEmEstoque.aspx">Produtos em Estoque</asp:ListItem>
                                                <asp:ListItem Value="estoqueReal.aspx">Estoque Real</asp:ListItem>
                                                <asp:ListItem Value="pedidosFornecedorPendente.aspx">Pedidos ao Fornecedor Pendentes</asp:ListItem>
                                                <asp:ListItem Value="produtoAjusteEstoque.aspx">Ajuste de Estoque</asp:ListItem>
                                                <asp:ListItem Value="produtosPeriodoCombo.aspx">Relatório de Produtos por Período Combo</asp:ListItem>
                                                <asp:ListItem Value="pedidoFornecedorHistorico.aspx">Histórico de Pedidos ao Fornecedor</asp:ListItem>
                                                <asp:ListItem Value="usuariosFornecedor.aspx">Usuários do Fornecedor</asp:ListItem>
                                                <asp:ListItem Value="pedidosCompletosFila.aspx">Prioridade de Pedidos Completos</asp:ListItem>
                                                <asp:ListItem Value="gerarNotasDoDia.aspx">Gerar Notas do Dia</asp:ListItem>
                                                <asp:ListItem Value="enviarDanfeNotas.aspx">Enviar Danfe Notas Fiscais</asp:ListItem>
                                                <asp:ListItem Value="listanotasfiscais.aspx">Consulta de Pedido por Nota Fiscal</asp:ListItem>
                                                <asp:ListItem Value="pedidosCompletosPrevisao.aspx">Previsão de Expedição</asp:ListItem>
                                                <asp:ListItem Value="sendoEmbaladosAtual.aspx">Sendo Embalados</asp:ListItem>
                                                <asp:ListItem Value="pedidosProdutosFaltando.aspx">Produtos Faltando</asp:ListItem>
                                                <asp:ListItem Value="relatorioTransportadora.aspx">Relatório Transportadora</asp:ListItem>
                                                <asp:ListItem Value="pedidosSemVinculoFornecedor.aspx">Pedidos Sem Vinculo com Fornecedor</asp:ListItem>
                                                <asp:ListItem Value="pedidosPagamentoConfirmado.aspx">Pedidos em Pagamento Confirmado</asp:ListItem>
                                                <asp:ListItem Value="pedidosEmbaladosPorData.aspx">Pedidos Embalados por Data</asp:ListItem>
                                                <asp:ListItem Value="pedidosAguardandoCarregamento.aspx">Pedidos Aguardando Carregamento</asp:ListItem>
                                                <asp:ListItem Value="pedidosFornecedorPossuemEstoque.aspx">Utilização de Estoque</asp:ListItem>
                                                <asp:ListItem Value="rastreamentoEncomendas.aspx">Rastreamento de Encomendas</asp:ListItem>
                                                <asp:ListItem Value="chamados.aspx">Chamados e Agendamentos</asp:ListItem>
                                                <asp:ListItem Value="produtosEntregues.aspx">Produtos Entregues</asp:ListItem>
                                                <asp:ListItem Value="imprimiretiquetasromaneio">Imprimir etiquetas de Romaneio</asp:ListItem>
                                                <asp:ListItem Value="imprimiretiquetasromaneioavulso">Imprimir etiquetas de Romaneio Avulsa</asp:ListItem>
                                                <asp:ListItem Value="pedidoFornecedorNovo.aspx">Gerar Pedido ao Fornecedor</asp:ListItem>
                                                <asp:ListItem Value="boletosConfirmar.aspx">Confirmar Boletos</asp:ListItem>
                                                <asp:ListItem Value="boletosConfirmadosComBanco.aspx">Boletos Confirmados com Banco</asp:ListItem>
                                                <asp:ListItem Value="relatorioProdutosVendidos.aspx">Relatório de Produtos Vendidos</asp:ListItem>
                                                <asp:ListItem Value="produtosEstoqueEtiquetas.aspx">Etiquetas dos Produtos</asp:ListItem>
                                                <asp:ListItem Value="atualizarProdutosBusca.aspx">Atualizar Produtos Busca</asp:ListItem>
                                                <asp:ListItem Value="listaNotasFiscais.aspx">Lista de Notas Fiscais</asp:ListItem>
                                                <asp:ListItem Value="excluiritemromaneio">Excluir Itens de Romaneio</asp:ListItem>
                                                <asp:ListItem Value="desmarcaritemromaneio">Desmarcar Itens de Romaneio</asp:ListItem>
                                                <asp:ListItem Value="relatorioMargemContribuicaoEstimada.aspx">Margem de Contribuição Estimada</asp:ListItem>
                                                <asp:ListItem Value="relatorioMargemContribuicao.aspx">Margem de Contribuição</asp:ListItem>
                                                <asp:ListItem Value="pedidosAtrasados.aspx">Pedidos Atrasados</asp:ListItem>
                                                <asp:ListItem Value="baixaEstoque.aspx">Retirar produtos do Estoque</asp:ListItem>
                                                <asp:ListItem Value="relatorioPlanoMaternidade.aspx">Relatório Plano Maternidade</asp:ListItem>
                                                <asp:ListItem Value="pedidosAguardandoEstoque.aspx">Pedidos Aguardando Estoque</asp:ListItem>
                                                <asp:ListItem Value="fluxoCaixa.aspx">Fluxo de Caixa</asp:ListItem>
                                                <asp:ListItem Value="despesaFornecedores.aspx">Fornecedores de Despesas</asp:ListItem>
                                                <asp:ListItem Value="financeiroImportarCartao.aspx">Importar extrato Cartões</asp:ListItem>
                                                <asp:ListItem Value="descricaoTabelaProduto.aspx">Tabela de Descrição - Produtos</asp:ListItem>
                                                <asp:ListItem Value="descricaoTabelaGrupo.aspx">Tabela de Descrição - Grupos</asp:ListItem>
                                                <asp:ListItem Value="gerarRelevancia.aspx">Relevância</asp:ListItem>
                                                <asp:ListItem Value="alterarstatuspedidos">Alterar Status Pedidos</asp:ListItem>
                                                <asp:ListItem Value="produtosPlanoMaternidade.aspx">Relatorio Plano Maternidade</asp:ListItem>
                                                <asp:ListItem Value="pedidosDiferencaCombo.aspx">Diferenças no Combo</asp:ListItem>
                                                <asp:ListItem Value="graficoEstoque.aspx">Grafico Estoque</asp:ListItem>
                                                <asp:ListItem Value="atualizarTabelaJadlog.aspx">Atualizar Tabela Jadlog</asp:ListItem>
                                                <asp:ListItem Value="relatorioClientesComCompras.aspx">Relatório Clientes com Compras</asp:ListItem>
                                                <asp:ListItem Value="voltarEstoque.aspx">Voltar produtos do estoque</asp:ListItem>
                                                <asp:ListItem Value="transportadorasLiberadas.aspx">Transportadoras Liberadas</asp:ListItem>
                                                <asp:ListItem Value="transferenciaProdutos.aspx">Produtos Aguardando Transferência de Galpão</asp:ListItem>
                                                <asp:ListItem Value="cronogramaEntrega.aspx">Cronograma de Entrega</asp:ListItem>
                                                <asp:ListItem Value="confirmarpagamento">Confirmar Pagamento</asp:ListItem>
                                                <asp:ListItem Value="reativarpagamento">Reativar Pagamento Não Autorizado</asp:ListItem>
                                                <asp:ListItem Value="banners.aspx">Listar banners</asp:ListItem>
                                                <asp:ListItem Value="bannerCad.aspx">Cadastrar banners banners</asp:ListItem>
                                                <asp:ListItem Value="bannerAlt.aspx">Alterar banners banners</asp:ListItem>
                                                <asp:ListItem Value="autorizarml">Autorizar Mercado Livre</asp:ListItem>
                                                <asp:ListItem Value="gerarnovanota">Gerar nova nota</asp:ListItem>
                                                <asp:ListItem Value="gerarnotadevolucao">Gerar nota de devolução</asp:ListItem>
                                                <asp:ListItem Value="vendasDoDia.aspx">Relatório de Vendas do Dia</asp:ListItem>
                                                <asp:ListItem Value="entradasProdutosPorPeriodo.aspx">Relatório Entrada de Produtos</asp:ListItem>
                                                <asp:ListItem Value="biProdutos.aspx">BI - Itens do Pedido</asp:ListItem>
                                                <asp:ListItem Value="produtosNecessitandoAntecipacao.aspx">Produtos Necessitando Antecipação</asp:ListItem>
                                                <asp:ListItem Value="filaEstoque.aspx">Fila do Estoque</asp:ListItem>
                                                <asp:ListItem Value="comprasFornecedores.aspx">Compras - Fornecedores</asp:ListItem>
                                                <asp:ListItem Value="comprasUnidadesDeMedida.aspx">Compras - Unidades de Medida</asp:ListItem>
                                                <asp:ListItem Value="comprasProdutosLista.aspx">Compras - Produtos</asp:ListItem>
                                                <asp:ListItem Value="comprasProdutosCadastro.aspx">Compras - Cadastrar Produtos</asp:ListItem>
                                                <asp:ListItem Value="comprasSolicitacao.aspx">Compras - Solicitação de Compra</asp:ListItem>
                                                <asp:ListItem Value="comprasMinhasSolicitacoes.aspx">Compras - Minhas Solicitações</asp:ListItem>
                                                <asp:ListItem Value="comprasSolicitacoesPendentes.aspx">Compras - Solicitações Pendentes</asp:ListItem>
                                                <asp:ListItem Value="compraOrdensPendentes.aspx">Compras - Ordens Pendentes</asp:ListItem>
                                                <asp:ListItem Value="comprasOrdemPendenteEditar.aspx">Compras - Editar Ordens Pendentes</asp:ListItem>
                                                <asp:ListItem Value="comprasOrdensAguardandoAprovacao.aspx">Compras - Ordens aguardando aprovação</asp:ListItem>
                                                <asp:ListItem Value="comprasOrdensAguardandoAprovacaoAprovar.aspx">Compras - Aprovar Ordens</asp:ListItem>
                                                <asp:ListItem Value="comprasOrdensAprovadas.aspx">Compras - Ordens Aprovadas</asp:ListItem>
                                                <asp:ListItem Value="comprasOrdensAguardandoFinanceiro.aspx">Compras - Ordens Aguardando Financeiro</asp:ListItem>
                                                <asp:ListItem Value="comprasOrdensHistorico.aspx">Compras - Histórico de Ordens de Compra</asp:ListItem>
                                                <asp:ListItem Value="comprasOrdemDetalhe.aspx">Compras - Exportar Ordem</asp:ListItem>
                                                <asp:ListItem Value="comprasCondicaoPagamentoLista.aspx">Compras - Condições de Pagamento</asp:ListItem>
                                                <asp:ListItem Value="comprasCondicaoPagamentoCadastro.aspx">Compras - Editar Condições de Pagamento</asp:ListItem>
                                                <asp:ListItem Value="comprasFornecedorCadastro.aspx">Compras - Cadastro de Fornecedores</asp:ListItem>
                                                <asp:ListItem Value="comprasAlterarStatus.aspx">Compras - Alterar Status</asp:ListItem>
                                                <asp:ListItem Value="relatorioPedidosFornecedor.aspx">Relatório de Pedidos ao Fornecedor</asp:ListItem>
                                                <asp:ListItem Value="estoqueReservado.aspx">Estoque Reservado</asp:ListItem>
                                                <asp:ListItem Value="fabricaAndamento.aspx">Andamento da Fábrica</asp:ListItem>
                                                <asp:ListItem Value="produtosCatalogo.aspx">Produtos Catálogo</asp:ListItem>
                                                <asp:ListItem Value="relatorioVendasPedidoFornecedor.aspx">Relatorio Vendas Pedidos Fornecedor</asp:ListItem>
                                                <asp:ListItem Value="entradaCd2.aspx">Entrada CD2</asp:ListItem>
                                                <asp:ListItem Value="pedidosCompletosCd2.aspx">Pedidos Completos CD2</asp:ListItem>
                                                <asp:ListItem Value="etiquetaduplicadaenderecamento.aspx">Etiquetas - Endereçamento</asp:ListItem>
                                                <asp:ListItem Value="comprasOrdensAguardandoConferenciaEntrega.aspx">Ordem Aguardando Conferência de Entrega</asp:ListItem>
                                                <asp:ListItem Value="comprasOrdemConferirEntrega.aspx">Compras - Conferir Entrega</asp:ListItem>
                                                <asp:ListItem Value="comprasORdensHistoricoProduto.aspx">Compras - Historico de Produtos</asp:ListItem>
                                                <asp:ListItem Value="emissao">Emissão</asp:ListItem>
                                                <asp:ListItem Value="priorizarpedido">Priorizar Pedido</asp:ListItem>
                                                <asp:ListItem Value="produtosReservadoCD2.aspx">Produtos Reservados CD2</asp:ListItem>
                                                <asp:ListItem Value="cadastroProcessosFabrica.aspx">Cadastro de Processos Fábrica</asp:ListItem>
                                                <asp:ListItem Value="custoProdutosFabrica.aspx">Custo Processos Fabrica</asp:ListItem>
                                                <asp:ListItem Value="listaDeProdutosFabrica.aspx">Lista Produtos Fábrica</asp:ListItem>
                                                <asp:ListItem Value="processosinternos.aspx">Processos Internos Fábrica</asp:ListItem>
                                                <asp:ListItem Value="materiaPrimaProduto.aspx">Matéria Prima Fábrica</asp:ListItem>
                                                <asp:ListItem Value="markup.aspx">Markup Fábrica</asp:ListItem>
                                                <asp:ListItem Value="produtosrelacionados.aspx">Produtos Relacionados</asp:ListItem>
                                                <asp:ListItem Value="acessotransportadora">Acesso Transportadora</asp:ListItem>
                                                <asp:ListItem Value="filaEstoqueFornecedor.aspx">Fila Estoque Fornecedor</asp:ListItem>
                                                <asp:ListItem Value="cadastroMateriaPrimaFabrica.aspx">Cadastro Matéria Prima Fábrica</asp:ListItem>
                                                <asp:ListItem Value="retornoSispag.aspx">Enviar Retorno Sispag</asp:ListItem>
                                                <asp:ListItem Value="baixarLotesSispag.aspx">Baixar Lotes Sispag</asp:ListItem>
                                                <asp:ListItem Value="gerarLoteCorreios.aspx">Gerar Lote Correios</asp:ListItem>
                                                <asp:ListItem Value="siteseguroadmin.aspx">Administrar pagina Site Seguro</asp:ListItem>
                                                <asp:ListItem Value="gerenciarCampanha.aspx">Administrar paginas Campanhas</asp:ListItem>
                                                <asp:ListItem Value="relatorioLetsFamily.aspx">Lets Family - Cupons Usados</asp:ListItem>
                                                <asp:ListItem Value="pesar">Pesar</asp:ListItem>
                                                <asp:ListItem Value="panoramaGeralPedidos.aspx">Panorâma Geral Pedidos</asp:ListItem>
                                                <asp:ListItem Value="acompanhamentoDePrazos.aspx">Produtos cam Prazo Divergente</asp:ListItem>
                                                <asp:ListItem Value="produtosDestaque.aspx">Gerenciar Produtos Destaque</asp:ListItem>
                                                <asp:ListItem Value="gerarcreditonopedido">Adicionar Crédito ao Pedido</asp:ListItem>
                                                <asp:ListItem Value="relatorioAtendimento.aspx">Relatorio de Atendimento</asp:ListItem>
                                                <asp:ListItem Value="pedidoNaoAutorizado.aspx">Pedidos Nao Autorizados</asp:ListItem>
                                                <asp:ListItem Value="pagamentosEstornados.aspx">Pagamentos Estornados</asp:ListItem>
                                                <asp:ListItem Value="autorizaestorno">Autorizar Estorno</asp:ListItem>
                                                <asp:ListItem Value="autorizaestornovalordiferente">Autorizar Estorno c/ Valor Diferente</asp:ListItem>
                                                <asp:ListItem Value="gerarNewsLetter.aspx">Gerar Pág. Newsletter</asp:ListItem>
                                                <asp:ListItem Value="landingpage">Cadastrar Landing Page</asp:ListItem>
                                                <asp:ListItem Value="dashboardProdutos.aspx">Dashboard de Produtos</asp:ListItem>
                                                <asp:ListItem Value="dashboardPagamentos.aspx">Dashboard de Pagamentos</asp:ListItem>
                                                <asp:ListItem Value="exportarNewsLetterCsv.aspx">Lista de NewsLetter</asp:ListItem>
                                                <asp:ListItem Value="despesasGerais.aspx">Cadastro Despesas Gerais</asp:ListItem>
                                                <asp:ListItem Value="relatorioPagamentosPedidos.aspx">Relatório de Pagamentos dos Pedidos</asp:ListItem>
                                                <asp:ListItem Value="relatorioGeralPedidos.aspx">Relatório Geral dos Pedidos</asp:ListItem>
                                                <asp:ListItem Value="estoqueCarolinaMoveis.aspx">Estoque Carolina Móveis</asp:ListItem>
                                                <asp:ListItem Value="importarPlanilhaEnvios.aspx">Importar Planilha Custo Transportadora (xls)</asp:ListItem>
                                                <asp:ListItem Value="informarTaxaExtraEnvio">Informar Taxa Extra de Envio</asp:ListItem>
                                                <asp:ListItem Value="solicitarenvioparcial">Solicitar Envio Parcial</asp:ListItem>
                                                <asp:ListItem Value="landingpage">Landing page</asp:ListItem>
                                                <asp:ListItem Value="gerenciarCampanha.aspx">Gerenciar Campanhas</asp:ListItem>
                                                <asp:ListItem Value="enderecamentoemlote.aspx ">Endereçar em Lote</asp:ListItem>
                                                <asp:ListItem Value="atualizarprecosimportarplanilha.aspx">Exportar/Importar Planilha de Preços</asp:ListItem>
                                                <asp:ListItem Value="biProdutosPeriodo.aspx">Bi Produtos Periodo</asp:ListItem>
                                                <asp:ListItem Value="custodopedido">Custo Final no Pedido</asp:ListItem>
                                                <asp:ListItem Value="biProdutosCategoria.aspx">BI - Vendas por Categoria</asp:ListItem>
                                                <asp:ListItem Value="consultalog.aspx">Logs Alterações no Produto</asp:ListItem>
                                                <asp:ListItem Value="reativarprodutocanceladonopedido">Reativar Produto Cancelado no Pedido</asp:ListItem>
                                                <asp:ListItem Value="trocaretiqueta">Trocar Etiqueta no Pedido</asp:ListItem>
                                                <asp:ListItem Value="consultaInfoPagamento.aspx">Consultar Pedidos pelo Pagamento</asp:ListItem>
                                                <asp:ListItem Value="postPadrao.aspx">Post Padrão</asp:ListItem>
                                                <asp:ListItem Value="marcarprodutocomoenviadonopedido">Marcar Produto como Enviado no Pedido</asp:ListItem>
                                                <asp:ListItem Value="priorizarmoveiscarolinaplanet">Priorizar pedidos de móveis (completos)</asp:ListItem>
                                                <asp:ListItem Value="priorizarmoveisem1cd2">Priorizar pedidos com móveis no CD2 (completos)</asp:ListItem>
                                                <asp:ListItem Value="carregamentocaminhaojadlog">Carregamento Caminhão Jadlog</asp:ListItem>
                                                <asp:ListItem Value="carregamentocaminhaocorreios">Carregamento Caminhão Correios</asp:ListItem>
                                                <asp:ListItem Value="carregamentocaminhaotnt">Carregamento Caminhão TNT</asp:ListItem>
                                                <asp:ListItem Value="carregamentocaminhaobelle">Carregamento Caminhão Belle</asp:ListItem>
                                                <asp:ListItem Value="carregamentocaminhaoplimor">Carregamento Caminhão Plimor</asp:ListItem>
                                                <asp:ListItem Value="simulacaoEstoque.aspx">Simulação de Estoque</asp:ListItem>
                                                <asp:ListItem Value="empresanfe.aspx">Empresas - Nfe</asp:ListItem>
                                                <asp:ListItem Value="alterarprecodecustodoproduto">Alterar preço de custo do produto</asp:ListItem>
                                                <asp:ListItem Value="enderecamentoArea.aspx">Endereçamento Area</asp:ListItem>
                                                <asp:ListItem Value="enderecamentoRua.aspx">Endereçamento Rua</asp:ListItem>
                                                <asp:ListItem Value="enderecamentoLado.aspx">Endereçamento Lado</asp:ListItem>
                                                <asp:ListItem Value="enderecamentoPredio.aspx">Endereçamento Prédio</asp:ListItem>
                                                <asp:ListItem Value="enderecamentoAndar.aspx">Endereçamento Andar</asp:ListItem>
                                                <asp:ListItem Value="enderecamentoApartamento.aspx">Endereçamento Apartamento</asp:ListItem>
                                                <asp:ListItem Value="logPrazoPedido.aspx">Log Prazos de Pedido</asp:ListItem>
                                                <asp:ListItem Value="produtosv2.aspx">Lista de Produtos com Todas as Colunas</asp:ListItem>
                                                <asp:ListItem Value="excluirproduto">Excluir Produto</asp:ListItem>
                                                <asp:ListItem Value="listadecolunasprodutos">Visualizar todas as colunas na lista de produtos</asp:ListItem>
                                                <asp:ListItem Value="pedidoFornecedorSemCusto.aspx">Pedidos ao Fornecedor sem Custo - (Defeito)</asp:ListItem>
                                                <asp:ListItem Value="selecionarprodutogerarnota">Selecionar Produto para Gerar Nfe Manualmente</asp:ListItem>
                                                <asp:ListItem Value="pedidosAguardandoPersonalizacao.aspx">Pedidos Aguardando Personalização</asp:ListItem>
                                                <asp:ListItem Value="etiquetasDisponiveisNoEstoque.aspx">Etiquetas Disponiveis no Estoque</asp:ListItem>
                                                <asp:ListItem Value="etiquetasCDDivergente.aspx">Etiquetas CD divergentes</asp:ListItem>
                                                <asp:ListItem Value="relatorioCustoPedido.aspx">Relatorio Custo do Pedido</asp:ListItem>
                                                <asp:ListItem Value="relatorioBiCustoPedido.aspx">BI Custo dos Itens do Pedido</asp:ListItem>
                                                <asp:ListItem Value="seloPromocao.aspx">Produtos com Selo Promoção</asp:ListItem>
                                                <asp:ListItem Value="produtoContador.aspx">Produto Contador</asp:ListItem>
                                                <asp:ListItem Value="pedidosPagamentosPendentes.aspx">Pagamentos Pendentes</asp:ListItem>
                                                <asp:ListItem Value="atualizarPrazosTNT.aspx">Atualizar Prazos de Entrega de Transportadora (Importar Planiha)</asp:ListItem>
                                                <asp:ListItem Value="carrinhosEnderecamento.aspx">Carrinho Endereçamento</asp:ListItem>
                                                <asp:ListItem Value="carrinhoEnderecamentoDetalhe.aspx">Carrinho Endereçamento Detalhe</asp:ListItem>
                                                <asp:ListItem Value="enderecamentoemlote.aspx">Endereçamento em Lote</asp:ListItem>
                                                <asp:ListItem Value="relatorioProdutosAdicionadosManualmente.aspx">Produtos Adicionados Manualmente</asp:ListItem>
                                                <asp:ListItem Value="visualizartotais">Visualizar Totais</asp:ListItem>
                                                <asp:ListItem Value="notaFiscalFabrica.aspx">Gerar Nota Fiscal Fábrica</asp:ListItem>
                                                <asp:ListItem Value="produtosnaovendidos.aspx">Produtos Não Vendidos por Período</asp:ListItem>
                                                <asp:ListItem Value="boletosemfrete">Boleto sem cálculo de frete no desconto</asp:ListItem>
                                                <asp:ListItem Value="biQuebraPorData.aspx">Quebra por Data</asp:ListItem>
                                                <asp:ListItem Value="atribuiretiqueta">Atribuir Etiqueta ao Pedido</asp:ListItem>
                                                <asp:ListItem Value="reiniciarpesagem">Reiniciar Pesagem</asp:ListItem>
                                                <asp:ListItem Value="produtosAguardandoEnderecamento.aspx">Produtos aguardando endereçamento</asp:ListItem>
                                                <asp:ListItem Value="configuracoesapp">Configurar Aplicativo Expedição</asp:ListItem>
                                                <asp:ListItem Value="transferircarrinho">Transferir itens de carrinho</asp:ListItem>
                                                <asp:ListItem Value="cadastrarCupomDesconto.aspx">Cadastrar Cupom de Desconto</asp:ListItem>
                                                <asp:ListItem Value="simularFrete.aspx">Simular Frete</asp:ListItem>
                                                <asp:ListItem Value="logEtiqueta.aspx">Log de Etiqueta</asp:ListItem>
                                                <asp:ListItem Value="vincularNotaPedido.aspx">Vincular NFe a Pedido</asp:ListItem>
                                                <asp:ListItem Value="importarPlanilhaTransportadora.aspx">Importar Planilha Transportadoras</asp:ListItem>
                                                <asp:ListItem Value="pedidosClientesRomaneio.aspx">Pedidos por Romaneio</asp:ListItem>
                                                <asp:ListItem Value="biConciliacaoFrete.aspx">BI Conciliação de Frete</asp:ListItem>
                                                <asp:ListItem Value="pedidosQueVaoAtrasar.aspx">Pedidos Que Vão Atrasar</asp:ListItem>
                                                <asp:ListItem Value="pedidosEmbaladosNaoPesados.aspx">Pedidos Embalados Não Pesados</asp:ListItem>
                                                <asp:ListItem Value="pedidosMotivosInclusaoItem.aspx">Pedidos com Itens Inclusos</asp:ListItem>
                                                <asp:ListItem Value="pedidosSemEmissao.aspx">Pedidos Sem Emissão Transportadora</asp:ListItem>
                                                <asp:ListItem Value="pedidosEmbaladosAguardeEnvio.aspx">Pedidos Embalados Aguardando Envio</asp:ListItem>  
                                                <asp:ListItem Value="adminchamado">Administrador Geral Chamados</asp:ListItem>    
                                                <asp:ListItem Value="gestordepartamento">Gestor de Departamento de Usuários</asp:ListItem>                                                  
                                                <asp:ListItem Value="consultarnfgrao.aspx">Consultar NFes - Contabilidade</asp:ListItem>     
                                                <asp:ListItem Value="relatorioFraude.aspx">Relatório de Fraude</asp:ListItem>
                                                <asp:ListItem Value="relatorioChamado.aspx">Relatório Gerencial de Chamados</asp:ListItem>
                                                <asp:ListItem Value="carregamentoCaminhao.aspx">Carregamento de Caminhão</asp:ListItem>
                                                <asp:ListItem Value="configDepartamentos.aspx">Manutenção de Departamentos</asp:ListItem>
                                                <asp:ListItem Value="alterarValorItemPedido.aspx">Alterar Valor Item Pedido</asp:ListItem>
                                                <asp:ListItem Value="simulacaoFrete.aspx">Simulação de Frete</asp:ListItem>
                                                <asp:ListItem Value="ReanaliseClearSale">Reanalise ClearSale</asp:ListItem>
                                                <asp:ListItem Value="gerarsimulacoestransportadoras.aspx">Gerar Simulações de Transportadoras</asp:ListItem>
                                                <asp:ListItem Value="unificarCadastroCategoria.aspx">Unificar Cadastro Categoria</asp:ListItem>
                                            </asp:CheckBoxList>
                                        </EditItemTemplate>
                                    </dxwgv:GridViewDataColumn>
                                    <dxwgv:GridViewCommandColumn VisibleIndex="2" Width="90px" ButtonType="Image">
                                        <EditButton Visible="True" Text="Editar">
                                            <Image Url="~/admin/images/btEditar.jpg" />
                                        </EditButton>
                                        <NewButton Visible="True" Text="Novo">
                                            <Image Url="~/admin/images/btNovo.jpg" />
                                        </NewButton>
                                        <DeleteButton Visible="True" Text="Excluir">
                                            <Image Url="~/admin/images/btExcluir.jpg" />
                                        </DeleteButton>
                                        <CancelButton Text="Cancelar">
                                            <Image Url="~/admin/images/btCancelar.jpg" />
                                        </CancelButton>
                                        <UpdateButton Text="Salvar">
                                            <Image Url="~/admin/images/btSalvarPeq.jpg" />
                                        </UpdateButton>
                                        <ClearFilterButton Visible="True" Text="Limpar filtro">
                                            <Image Url="~/admin/images/btLimparFiltro.jpg" />
                                        </ClearFilterButton>
                                        <HeaderTemplate>
                                            <img alt="" src="images/legendaIcones.jpg" />
                                        </HeaderTemplate>
                                    </dxwgv:GridViewCommandColumn>
                                </Columns>
                                <StylesEditors>
                                    <Label Font-Bold="True">
                                    </Label>
                                </StylesEditors>
                            </dxwgv:ASPxGridView>
                            
                            <asp:LinqDataSource ID="sqlUsuarios" runat="server" OnUpdating="sqlUsuarios_OnUpdating"
                                ContextTypeName="dbCommerceDataContext" EnableDelete="True"
                                EnableInsert="True" EnableUpdate="True" TableName="tbUsuarios" EntityTypeName="">
                                <InsertParameters>
                                    <asp:FormParameter FormField="ddlDepartamento" Name="idDepartamento" />
                                </InsertParameters>
                            </asp:LinqDataSource>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>

