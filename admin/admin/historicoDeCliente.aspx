﻿<%@ Page Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="historicoDeCliente.aspx.cs" Inherits="admin_historicoDeCliente"   Theme="Glass" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dxwgv" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" Runat="Server">
    <table cellpadding="0" cellspacing="0" style="width: 920px">
    <tr>
        <td class="tituloPaginas" valign="top">
            Histórico de:
            <asp:Label ID="lblAcao" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            
    <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
        <tr>
            <td align="right">
                <table cellpadding="0" cellspacing="0" style="width: 834px">
                    <tr>
                        <td align="right">
                            &nbsp;</td>
                        <td height="38" 
                            style="background-image: url('images/bkExportar.jpg'); padding-bottom: 1px;" 
                            valign="bottom" width="231">
                            <table cellpadding="0" cellspacing="0" style="2">
                                <tr>
                                    <td style="width: 97px">
                                        &nbsp;</td>
                                    <td style="width: 33px">
                                        <asp:ImageButton ID="btPdf" runat="server" ImageUrl="~/admin/images/btPdf.jpg" 
                                            style="margin-left: 5px" />
                                    </td>
                                    <td style="width: 31px">
                                        <asp:ImageButton ID="btXsl" runat="server" ImageUrl="~/admin/images/btXls.jpg" 
                                            style="margin-left: 5px" />
                                    </td>
                                    <td style="width: 36px">
                                        <asp:ImageButton ID="btRtf" runat="server" ImageUrl="~/admin/images/btRtf.jpg" 
                                            style="margin-left: 5px" />
                                    </td>
                                    <td valign="bottom">
                                        <asp:ImageButton ID="btCsv" runat="server" ImageUrl="~/admin/images/btCsv.jpg" 
                                            style="margin-left: 5px" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                <dxwgv:ASPxGridView ID="grd" runat="server" AutoGenerateColumns="False" 
                    DataSourceID="sqlPedidos" KeyFieldName="pedidoId" Width="834px" 
                    Cursor="auto">
                    <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                    <SettingsText ConfirmDelete="Tem certeza que deseja excluir?" 
                        emptydatarow="Nenhum registro encontrado." />
                    <SettingsPager Position="TopAndBottom" PageSize="50" 
                        ShowDisabledButtons="False" AlwaysShowPager="True">
                        <Summary AllPagesText="Páginas: {0} - {1} ({2} items)" 
                            Text="Página {0} de {1} ({2} registros encontrados)" />
                    </SettingsPager>
                    <settings showfilterrow="True" ShowGroupButtons="False" ShowFooter="True" />
                    <TotalSummary>
                        <dxwgv:ASPxSummaryItem DisplayFormat="C" FieldName="valorDosItens" 
                            ShowInColumn="Valor dos itens" ShowInGroupFooterColumn="Valor dos itens" 
                            SummaryType="Sum" />
                        <dxwgv:ASPxSummaryItem DisplayFormat="C" FieldName="valorDoFrete" 
                            ShowInColumn="Valor do frete" ShowInGroupFooterColumn="Valor do frete" 
                            SummaryType="Sum" />
                        <dxwgv:ASPxSummaryItem DisplayFormat="C" FieldName="valorCobrado" 
                            ShowInColumn="Valor total" ShowInGroupFooterColumn="Valor total" 
                            SummaryType="Sum" />
                    </TotalSummary>
                    <SettingsEditing EditFormColumnCount="4" 
                        PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1" 
                        PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter" 
                        PopupEditFormWidth="700px" />
                    <Columns>
                        <dxwgv:GridViewDataTextColumn Caption="Id do Pedido" FieldName="pedidoId" 
                            VisibleIndex="0">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Data" FieldName="dataHoraDoPedido" 
                            VisibleIndex="1">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Valor dos itens" FieldName="valorDosItens" 
                            VisibleIndex="2">
                            <PropertiesTextEdit DisplayFormatString="C">
                            </PropertiesTextEdit>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Valor do frete" FieldName="valorDoFrete" 
                            VisibleIndex="3">
                            <PropertiesTextEdit DisplayFormatString="C">
                            </PropertiesTextEdit>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Valor total" FieldName="valorCobrado" 
                            VisibleIndex="4">
                            <PropertiesTextEdit DisplayFormatString="C">
                            </PropertiesTextEdit>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Situação" FieldName="situacao" 
                            VisibleIndex="5">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Detalhes" VisibleIndex="6">
                            <DataItemTemplate>
                                <asp:HyperLink ID="hplHistorico" runat="server" 
                                    ImageUrl="~/admin/images/btNovo.jpg" 
                                    NavigateUrl='<%# "pedido.aspx?pedidoId=" + Eval("pedidoId") %>'></asp:HyperLink>
                            </DataItemTemplate>
                        </dxwgv:GridViewDataTextColumn>
                    </Columns>
                    <StylesEditors>
                        <Label Font-Bold="True">
                        </Label>
                    </StylesEditors>
                </dxwgv:ASPxGridView>
<%--                <dxwgv:ASPxGridViewExporter ID="grdEx" runat="server" FileName="fornecedores" 
                    GridViewID="grd" Landscape="True" PreserveGroupRowStates="True">
                </dxwgv:ASPxGridViewExporter>--%>
                <asp:ObjectDataSource ID="sqlPedidos" runat="server" SelectMethod="pedidoSelecionaAdmin_PorClienteId" 
                    TypeName="rnPedidos">
                    <SelectParameters>
                        <asp:QueryStringParameter Name="clienteId" QueryStringField="clienteId" 
                            Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="sqlEstado" runat="server" 
                    SelectMethod="estadoSeleciona" TypeName="rnEstados">
                </asp:ObjectDataSource>
                    <asp:ObjectDataSource ID="sqlPaises" runat="server" 
                                            SelectMethod="paisesSeleciona" TypeName="rnPais">
                    </asp:ObjectDataSource>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
    </table>
        </td>
    </tr>
</table>
    </asp:Content>
