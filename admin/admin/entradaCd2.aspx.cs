﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_entradaCd2 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
    }

    private void fillLista()
    {
        var data = new dbCommerceDataContext();
        int idPedidoFornecedor = Convert.ToInt32("0" + txtRomaneio.Text);

        lstProdutos.DataSource = (from c in data.tbPedidoFornecedorItems where c.idPedidoFornecedor == idPedidoFornecedor select new
        {
            c.idPedidoFornecedorItem,
            c.tbProduto.produtoIdDaEmpresa,
            c.tbProduto.complementoIdDaEmpresa,
            c.tbProduto.produtoNome,
            c.entregue
        });
        lstProdutos.DataBind();

    }
    protected void btnGravar_OnClick(object sender, EventArgs e)
    {
        StringBuilder interacao = new StringBuilder();
        bool alteradoGeral = false;
        foreach (var item in lstProdutos.Items)
        {
            bool alterado = false;
            var hdfIdPedidoFornecedorItem = (HiddenField)item.FindControl("hdfIdPedidoFornecedorItem");
            var chkEntregue = (CheckBox)item.FindControl("chkEntregue");
            int idPedidoFornecedorItem = Convert.ToInt32(hdfIdPedidoFornecedorItem.Value);

            if (chkEntregue.Checked)
            {
                var data = new dbCommerceDataContext();
                var pedidoItem =
                    (from c in data.tbPedidoFornecedorItems
                        where c.idPedidoFornecedorItem == idPedidoFornecedorItem
                        select c).First();
                if (pedidoItem.entregue == false)
                {
                    var estoqueCheck = (from c in data.tbProdutoEstoques where c.idPedidoFornecedorItem == idPedidoFornecedorItem select c).Any();
                    if (!estoqueCheck)
                    {
                        pedidoItem.entregue = true;
                        pedidoItem.liberado = true;
                        pedidoItem.dataEntrega = DateTime.Now;

                        var estoque = new tbProdutoEstoque();
                        estoque.idPedidoFornecedorItem = idPedidoFornecedorItem;
                        estoque.produtoId = pedidoItem.idProduto;
                        estoque.dataEntrada = DateTime.Now;
                        estoque.enviado = false;
                        estoque.liberado = true;
                        estoque.idCentroDistribuicao = 1;
                        estoque.idEnderecamentoArea = 7;
                        estoque.idEnderecamentoRua = 58;
                        estoque.idEnderecamentoPredio = 271;
                        estoque.idEnderecamentoAndar = 1080;
                        data.tbProdutoEstoques.InsertOnSubmit(estoque);

                        var queue = new tbQueue();
                        queue.agendamento = DateTime.Now;
                        queue.andamento = false;
                        queue.concluido = false;
                        queue.idRelacionado = pedidoItem.idProduto;
                        queue.mensagem = "";
                        queue.tipoQueue = 21;
                        data.tbQueues.InsertOnSubmit(queue);
                        data.SubmitChanges();
                        data.Dispose();
                    }

                }

            }
        }

        Response.Write("<script>alert('Pedido alterado com sucesso.');</script>");

        fillLista();
    }
    protected void btnCarregarRomaneio_Click(object sender, EventArgs e)
    {
        fillLista();
    }
}