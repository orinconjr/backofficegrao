﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="produtosEditarCategoria.aspx.cs" Inherits="admin_produtosEditarCategoria" Theme="Glass" %>
<%@ Register TagPrefix="dxwgv" Namespace="DevExpress.Web.ASPxGridView" Assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>
<%@ Register TagPrefix="dxe" Namespace="DevExpress.Web.ASPxEditors" Assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>

<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" runat="Server">    
    
    <table cellpadding="0" cellspacing="0" style="width: 920px">
        <tr>
            <td class="tituloPaginas" valign="top">
                <asp:Label ID="lblAcao" runat="server">Produtos</asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
                    <tr>
                        <td class="rotulos">
                            <div style="display: none;">
                                <asp:TreeView ID="treeCategorias" runat="server" ShowLines="True" 
                                    OnTreeNodePopulate="TreeView1_TreeNodePopulate" ExpandDepth="0"
                                    Font-Names="Tahoma" Font-Size="12px" ForeColor="Black" OnDataBound="treeCategorias_databound">
                                    <SelectedNodeStyle BackColor="#D7EAEE" />
                                    <Nodes>
                                        <asp:TreeNode Selected="True" Text="Categorias" Value="0"></asp:TreeNode>
                                    </Nodes>
                                </asp:TreeView>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Panel ID="pnlGrid" runat="server" ScrollBars="Horizontal" Width="834px">
                                <dxwgv:ASPxGridView ID="grd" runat="server" AutoGenerateColumns="False"
                                    DataSourceID="sqlProdutos" KeyFieldName="produtoId"
                                    Cursor="auto" OnHtmlRowCreated="grd_HtmlRowCreated" OnCustomCallback="grd_CustomCallback"
                                    ClientInstanceName="grd" EnableCallBacks="False">
                                    <SettingsBehavior AutoFilterRowInputDelay="10200" ConfirmDelete="True"
                                        AutoExpandAllGroups="True" ColumnResizeMode="Control" AllowGroup="False"
                                        AllowFocusedRow="True" />
                                    <SettingsText ConfirmDelete="Tem certeza que deseja excluir?"
                                        EmptyDataRow="Nenhum registro encontrado."
                                        GroupPanel="Arraste uma coluna aqui para agrupar." />
                                    <SettingsPager Position="TopAndBottom" PageSize="30"
                                        ShowDisabledButtons="False" AlwaysShowPager="True">
                                        <Summary AllPagesText="Páginas: {0} - {1} ({2} items)"
                                            Text="Página {0} de {1} ({2} registros encontrados)" />
                                    </SettingsPager>
                                    <Settings ShowFilterRow="True" ShowGroupedColumns="True" ShowFilterBar="Visible" ShowFilterRowMenu="True" />
                                    <SettingsEditing EditFormColumnCount="4"
                                        PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1"
                                        PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter"
                                        PopupEditFormWidth="700px" Mode="Inline" />
                                    <Columns>
   <%--                                     <dxwgv:GridViewDataTextColumn Caption="ID do Produto" FieldName="produtoId"
                                            ReadOnly="True" VisibleIndex="8" Width="100px">
                                            <EditFormSettings Visible="False" />
                                            <DataItemTemplate>
                                                <asp:TextBox ID="txtProdutoId" runat="server" BorderStyle="None"
                                                    CssClass="campos" ReadOnly="True" Text='<%# Bind("produtoId") %>' Width="82px"></asp:TextBox>
                                            </DataItemTemplate>
                                        </dxwgv:GridViewDataTextColumn>--%>

                                        <dxwgv:GridViewDataImageColumn Caption="Foto"
                                            FieldName="fotoDestaque" Name="foto" VisibleIndex="1" Width="100px">
                                            <DataItemTemplate>
                                                <asp:Image ID="imgFotos" runat="server"                                                                   
                                                                    ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings["caminhoVirtual"] + "/fotos/" + Eval("produtoId") + "/pequena_" + Eval("fotoDestaque") + ".jpg" %>' 
                                                                    ToolTip='<%# Eval("fotoDestaque") %>' Height="80px" Width="80px"></asp:Image>
                                            </DataItemTemplate>
                                        </dxwgv:GridViewDataImageColumn>

                                        <dxwgv:GridViewDataTextColumn VisibleIndex="1" Width="200px">
                                            <DataItemTemplate>
                                                <dx:ASPxDropDownEdit ID="ddlCategorias" OnPreRender="ddlCategorias_OnPreRender" runat="server"
                                                    ClientInstanceName="ddlCategorias" Width="180px" ToolTip='<%# Bind("produtoId") %>' >
                                                    <DropDownWindowTemplate>
                                                        <div style="height: 400px; width: 275px; overflow: auto;">
                                                            <asp:TreeView ID="treeCategoriasProduto" runat="server" ShowCheckBoxes="All"
                                                                ShowLines="True" OnTreeNodePopulate="treeCategoriasProduto_TreeNodePopulate" ExpandDepth="0"
                                                                Font-Names="Tahoma" Font-Size="12px" ForeColor="Black">
                                                                <SelectedNodeStyle BackColor="#D7EAEE" />
                                                                <Nodes>
                                                                </Nodes>
                                                            </asp:TreeView>
                                                        </div>
<%--                                                        <asp:TextBox ID="txtProdutoId" runat="server" BorderStyle="None" Visible="False"
                                                            CssClass="campos" ReadOnly="True" Text='<%# Bind("produtoId") %>' Width="82px"></asp:TextBox>--%>
                                                    </DropDownWindowTemplate>
                                                </dx:ASPxDropDownEdit>
                                            </DataItemTemplate>
                                        </dxwgv:GridViewDataTextColumn>

                                        <dxwgv:GridViewDataTextColumn Caption="ID da Empresa"
                                            FieldName="produtoIdDaEmpresa" Name="idDaEmpresa" VisibleIndex="3" Width="100px">
                                            <DataItemTemplate>
                                                <asp:TextBox ID="txtIdDaEmpresa" runat="server" BorderStyle="None"
                                                    CssClass="campos" Text='<%# Bind("produtoIdDaEmpresa") %>' Width="100%"></asp:TextBox>
                                            </DataItemTemplate>
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn Caption="ID do Produto" FieldName="produtoId" 
                                            ReadOnly="True" VisibleIndex="8" Width="100px">
                                            <EditFormSettings Visible="False" />
                                            <DataItemTemplate>
                                                <asp:TextBox ID="txtProdutoId" runat="server" BorderStyle="None" 
                                                    CssClass="campos" ReadOnly="True" Text='<%# Bind("produtoId") %>' Width="82px"></asp:TextBox>
                                            </DataItemTemplate>
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn Caption="Nome" FieldName="produtoNome"
                                            VisibleIndex="4" Width="250px">
                                            <Settings AutoFilterCondition="Contains" />
                                            <DataItemTemplate>
                                                <asp:TextBox ID="txtNome" runat="server" BorderStyle="None" CssClass="campos"
                                                    Text='<%# Bind("produtoNome") %>' Width="100%"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rqvNome" runat="server"
                                                    ControlToValidate="txtNome" Display="Dynamic" ErrorMessage="Preencha o nome."
                                                    Font-Size="10px" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                            </DataItemTemplate>
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn Caption="Fornecedor" FieldName="fornecedorNome"
                                            VisibleIndex="10" Width="160px">
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn Caption="ID do Produto Pai" 
                                            FieldName="produtoPaiId" VisibleIndex="9" Width="110px">
                                            <DataItemTemplate>
                                                <asp:TextBox ID="txtProdutoPaiId" runat="server" BorderStyle="None" 
                                                    CssClass="rotulos" Text='<%# Bind("produtoPaiId") %>'></asp:TextBox>
                                            </DataItemTemplate>
                                        </dxwgv:GridViewDataTextColumn>
                                    </Columns>
                                </dxwgv:ASPxGridView>
                            </asp:Panel>
                            <asp:ObjectDataSource ID="sqlProdutos" runat="server"
                                SelectMethod="produtoAdminSelecionaAtivos" TypeName="rnProdutos"
                                DeleteMethod="produtoExclui">
                                <DeleteParameters>
                                    <asp:Parameter Name="produtoId" Type="String" />
                                </DeleteParameters>
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="treeCategorias" Name="categoriaId"
                                        PropertyName="SelectedValue" Type="String" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                    <tr>
                        <td align="right">
                            <img src="images/btSalvar.jpg" onclick="grd.PerformCallback(this.value);" /></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>