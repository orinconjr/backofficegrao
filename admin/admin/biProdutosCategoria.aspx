﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="biProdutosCategoria.aspx.cs" Inherits="admin_biProdutosCategoria" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.ASPxPivotGrid.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPivotGrid" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" Runat="Server">
    <table cellpadding="0" cellspacing="0" style="width: 920px">
        <tr>
            <td class="tituloPaginas" valign="top">
                <asp:Label ID="lblAcao" runat="server">BI - Vendas por Categoria</asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
                    <tr>
                        <td align="right">
                            <table cellpadding="0" cellspacing="0" style="width: 834px">
                                <tr>
                                    <td>
                                        <table cellpadding="0" cellspacing="0" style="width: 834px">
                                            <tr>
                                                <td>                                    
                                                    <asp:Panel runat="server" ID="pnCategorias" Height="200" ScrollBars="Vertical">
                                                        <asp:TreeView ID="treeCategorias" runat="server"
                                                            ShowLines="True" OnTreeNodePopulate="TreeView1_TreeNodePopulate" ExpandDepth="0"
                                                            Font-Names="Tahoma" Font-Size="12px" ForeColor="Black">
                                                            <SelectedNodeStyle BackColor="#D7EAEE" />
                                                            <Nodes>
                                                                <asp:TreeNode Selected="True" Text="Categorias" Value="0"></asp:TreeNode>
                                                            </Nodes>
                                                        </asp:TreeView>
                                                    </asp:Panel>
                                                </td>
                                                <td style="padding-left: 20px; vertical-align: top;">
                                                    <table width="100%">                                        
                                                       <tr>
                                                <td class="rotulos" style="width: 100px">
                                                    Data inicial 1<br />
                                                                <asp:TextBox ID="txtDataInicial" runat="server" CssClass="campos" 
                                                                                    Width="90px" MaxLength="10"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="rqvDataInicial" runat="server" 
                                                                                    ControlToValidate="txtDataInicial" Display="None" 
                                                                                    ErrorMessage="Preencha a data inicial." 
                                                        SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="rgeDataInicial" runat="server" 
                                                                    ControlToValidate="txtDataInicial" Display="None" 
                                                                    ErrorMessage="Por favor, preencha corretamente a data inicial." 
                                                                    SetFocusOnError="True" 
                                                        ValidationExpression="^(([0-2]\d|[3][0-1])\/([0]\d|[1][0-2])\/[1-2][0-9]\d{2})$"></asp:RegularExpressionValidator>
                            
                                                </td>
                                                <td class="rotulos" style="width: 100px">
                                                    Data final 1<br  />
                                                    <asp:TextBox ID="txtDataFinal" runat="server" CssClass="campos" 
                                                                        Width="90px" MaxLength="10"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="rqvDataFinal" runat="server" 
                                                                        ControlToValidate="txtDataFinal" Display="None" 
                                                                        ErrorMessage="Preencha a data final." 
                                                        SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                    <b>
                                                    <asp:RegularExpressionValidator ID="rgeDataInicial0" runat="server" 
                                                                    ControlToValidate="txtDataFinal" Display="None" 
                                                                    ErrorMessage="Por favor, preencha corretamente a data final" 
                                                                    SetFocusOnError="True" 
                                            
                                            
                                                        ValidationExpression="^(([0-2]\d|[3][0-1])\/([0]\d|[1][0-2])\/[1-2][0-9]\d{2})$"></asp:RegularExpressionValidator>
                                                    </b>
                                                </td>
                                                <td valign="bottom">
                                                    <asp:ImageButton ID="btnPesquisar" runat="server" ImageUrl="~/admin/images/btPesquisar.jpg" OnClick="btnPequisar_Click" />
                                                    <asp:ValidationSummary ID="llds" runat="server" DisplayMode="List" ShowMessageBox="True" ShowSummary="False" />
                                                </td>
                                            </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">
                                        &nbsp;</td>
                                    <td height="38" 
                                        style="background-image: url('images/bkExportar.jpg'); padding-bottom: 1px;" 
                                        valign="bottom" width="231">
                                        <%--<table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 97px">
                                                    &nbsp;</td>
                                                <td style="width: 33px">
                                                    <asp:ImageButton ID="btPdf" runat="server" ImageUrl="~/admin/images/btPdf.jpg"  OnClick="btPdf_Click"
                                                        style="margin-left: 5px" />
                                                </td>
                                                <td style="width: 31px">
                                                    <asp:ImageButton ID="btXsl" runat="server" ImageUrl="~/admin/images/btXls.jpg" OnClick="btXsl_Click"
                                                        style="margin-left: 5px" />
                                                </td>
                                                <td style="width: 36px">
                                                    <asp:ImageButton ID="btRtf" runat="server" ImageUrl="~/admin/images/btRtf.jpg" OnClick="btRtf_Click"
                                                        style="margin-left: 5px" />
                                                </td>
                                                <td valign="bottom">
                                                    <asp:ImageButton ID="btCsv" runat="server" ImageUrl="~/admin/images/btCsv.jpg" OnClick="btCsv_Click"
                                                        style="margin-left: 5px" />
                                                </td>
                                            </tr>
                                        </table>--%>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <dx:ASPxPivotGrid OptionsData-DataFieldUnboundExpressionMode="UseSummaryValues" OptionsView-ShowDataHeaders="False" CustomizationFieldsLeft="300" CustomizationFieldsTop="400" OnCustomUnboundFieldData="grd_CustomUnboundFieldData" ID="grd" runat="server"  ClientInstanceName="pivotGrid" Width="1300px">
                                <Fields>

                                    
                                    <dx:PivotGridField Area="FilterArea" FieldName="dias" Caption="Dias" ID="Dias" />

                                    <dx:PivotGridField Area="RowArea" FieldName="categoria" Caption="Categoria" ID="Categoria"  /> 
                                    
                                                                       
                                    
                                    <dx:PivotGridField Area="DataArea" FieldName="totalCategoriaSelecionada" Caption="Clientes Categoria Selecionada" ID="TotalCategoriaSelecionada"  />
                                    <dx:PivotGridField Area="DataArea" FieldName="totalCategoria" Caption="Qtd Total" ID="TotalCategoria"  />                        
                                    <dx:PivotGridField Area="DataArea" Caption="Potencial Vendas" ID="PotencialVendas" UnboundExpression="[TotalCategoriaSelecionada] - [TotalCategoria]" FieldName="potencialVendas" UnboundType="Decimal" UnboundFieldName="potencialVendas" CellFormat-FormatString="{0:0.##}"  CellFormat-FormatType="Numeric" />
                                    <dx:PivotGridField Area="DataArea" FieldName="quantidadeAntes" Caption="Qtd Antes" ID="QuantidadeAntes"  />
                                    <dx:PivotGridField Area="DataArea" FieldName="quantidadeMesmoPedido" Caption="Qtd Mesmo" ID="QuantidadeMesmoPedido"  />
                                    <dx:PivotGridField Area="DataArea" FieldName="quantidadeDepois" Caption="Qtd Depois" ID="QuantidadeDepois"  />

                                    <dx:PivotGridField Area="DataArea" FieldName="valorAntes" Caption="Valor Antes" ID="ValorAntes"  />
                                    <dx:PivotGridField Area="DataArea" FieldName="valorMesmoPedido" Caption="Valor Mesmo" ID="ValorMesmoPedido"  />
                                    <dx:PivotGridField Area="DataArea" FieldName="valorDepois" Caption="Valor Depois" ID="ValorDepois"  />

                                </Fields>
                                <OptionsView ShowFilterHeaders="True" ShowHorizontalScrollBar="True" ShowColumnGrandTotals="False"  ShowRowGrandTotals="False" ShowRowTotals="False" />
                                <OptionsCustomization AllowSortBySummary="True" AllowSort="True" AllowFilterInCustomizationForm="True"></OptionsCustomization>
                                <OptionsPager RowsPerPage="100"></OptionsPager>
                            </dx:ASPxPivotGrid>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>