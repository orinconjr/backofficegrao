﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" Theme="Glass" AutoEventWireup="true" CodeFile="pedidoFornecedorNovo.aspx.cs" Inherits="admin_pedidoFornecedorNovo" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxTimer" TagPrefix="dxt" %>
<%@ Register TagPrefix="dxe" Namespace="DevExpress.Web.ASPxMenu" Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" Runat="Server">
    <script language="javascript" type="text/javascript">
        function OnDropDownDataPedido(s, dateFrom, dateTo) {
            var str = s.GetText();
            if (str == "") {
                dateFrom.SetDate(new Date(1950, 0, 1));
                dateTo.SetDate(new Date(1960, 11, 31));
                return;
            }
            var d = str.split("|");
            dateFrom.SetText(d[0]);
            dateTo.SetText(d[1]);
        }

        function ApplyFilter(dde, dateFrom, dateTo, campo) {
            var colunaSplit = dde.name.split('_');
            var coluna = colunaSplit[colunaSplit.length - 1].replace("DXFREditorcol", "");
            var colunaFiltro = "";
            if (coluna == "4") colunaFiltro = "dataLimite";
            if (coluna == "3") colunaFiltro = "data";

            var d1 = dateFrom.GetText();
            var d2 = dateTo.GetText();
            if (d1 == "" || d2 == "") return;
            dde.SetText(d1 + "|" + d2);
            grd.AutoFilterByColumn(colunaFiltro, dde.GetText());
        }
        function OnDropDown(s, dateFrom, dateTo) {
            var str = s.GetText();
            if (str == "") {
                dateFrom.SetDate(new Date(1950, 0, 1));
                dateTo.SetDate(new Date(1960, 11, 31));
                return;
            }
            var d = str.split("|");
            dateFrom.SetText(d[0]);
            dateTo.SetText(d[1]);
        }
        function grid_ContextMenu(s, e) {
            if (e.objectType == "header")
                pmColumnMenu.ShowAtPos(ASPxClientUtils.GetEventX(e.htmlEvent), ASPxClientUtils.GetEventY(e.htmlEvent));
        }
    </script>
    
    <dxe:ASPxPopupMenu ID="pmColumnMenu" runat="server" ClientInstanceName="pmColumnMenu">
        <Items>
            <dxe:MenuItem Name="cmdShowCustomization" Text="Escolher colunas">
            </dxe:MenuItem>
        </Items>
        <ClientSideEvents ItemClick="function(s, e) { if(e.item.name == 'cmdShowCustomization') grd.ShowCustomizationWindow(); }" />
    </dxe:ASPxPopupMenu>

    <table cellpadding="0" cellspacing="0" style="width: 920px">
    <tr>
        <td class="tituloPaginas" valign="top">
            <asp:Label ID="lblAcao" runat="server">Pedidos ao Fornecedor</asp:Label>
        </td>
    </tr>
        
    <tr>
        <td>
            
    <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                <dxt:ASPxTimer ID="timer1" runat="server" ClientInstanceName="myTimer" Enabled="False" Interval="1000" OnTick="timer1_OnTick">
                </dxt:ASPxTimer>
                <asp:HiddenField runat="server" ID="hiddenExportarId" />
                <dxwgv:ASPxGridView EnableCallBacks="False" ID="grd" ClientInstanceName="grd" runat="server" AutoGenerateColumns="False" Width="834px" Cursor="auto" OnHtmlRowCreated="gridClientes_HtmlRowCreated" KeyFieldName="fornecedorId"
                     OnCustomUnboundColumnData="grd_OnCustomUnboundColumnData" OnAutoFilterCellEditorCreate="grid_AutoFilterCellEditorCreate" OnAutoFilterCellEditorInitialize="grid_AutoFilterCellEditorInitialize"
                OnProcessColumnAutoFilter="grid_ProcessColumnAutoFilter" Settings-ShowFilterRow="True" Settings-ShowFilterBar="Visible" OnDataBound="grd_OnDataBound">
                    <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                    <Styles>
                        <Footer Font-Bold="True">
                        </Footer>
                    </Styles>
                    <SettingsText ConfirmDelete="Tem certeza que deseja excluir?" emptydatarow="Nenhum registro encontrado." />
                    <SettingsPager Position="TopAndBottom" PageSize="40" ShowDisabledButtons="False" AlwaysShowPager="True">
                        <Summary AllPagesText="Páginas: {0} - {1} ({2} items)" Text="Página {0} de {1} ({2} registros encontrados)" />
                    </SettingsPager>
                    <TotalSummary>
                        <dxwgv:ASPxSummaryItem FieldName="qtdProdutos" ShowInColumn="qtdProdutos" ShowInGroupFooterColumn="qtdProdutos" SummaryType="Sum" DisplayFormat="{0}" />
                        <dxwgv:ASPxSummaryItem FieldName="custo" ShowInColumn="custo" ShowInGroupFooterColumn="custo" SummaryType="Sum" DisplayFormat="{0:C}" />
                        <dxwgv:ASPxSummaryItem FieldName="diferenca" ShowInColumn="diferenca" ShowInGroupFooterColumn="diferenca" SummaryType="Sum" DisplayFormat="{0:C}" />
                    </TotalSummary>
                    <settings showfilterrow="True" ShowGroupButtons="False" ShowHeaderFilterButton="True" ShowFooter="True" />
                    <SettingsEditing EditFormColumnCount="4" PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1" PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter" PopupEditFormWidth="700px" />
                    <Columns>
                        <dxwgv:GridViewDataTextColumn Caption="#" Name="selecionar" VisibleIndex="0" Width="30px">
                            <DataItemTemplate>
                                <dxe:ASPxCheckBox ID="chkSelecionar" runat="server" ValueChecked="True"  ValueType="System.String" ValueUnchecked="False">
                                </dxe:ASPxCheckBox>
                            </DataItemTemplate>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Fornecedor" FieldName="fornecedorNome" VisibleIndex="0">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Fornecedor ID" FieldName="fornecedorId" VisibleIndex="0" Visible="False">
                        </dxwgv:GridViewDataTextColumn>
                    </Columns>
                    <StylesEditors>
                        <Label Font-Bold="True">
                        </Label>
                    </StylesEditors>
                    <ClientSideEvents ContextMenu="grid_ContextMenu" />
                    <SettingsCustomizationWindow Enabled="True" />     
                </dxwgv:ASPxGridView>                
                <asp:ObjectDataSource ID="sql" runat="server" SelectMethod="fornecedoresComVendas" TypeName="rnPedidos">
                </asp:ObjectDataSource>
            </td>
        </tr>
        <tr class="rotulos">
            <td style="padding-top: 30px; font-size: 20px;">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td style="text-align: right; padding-top: 15px;">
                <asp:Button runat="server" ID="btnGerarFaturas" Text="Gerar Faturas" OnClick="btnGerarFaturas_OnClick"/>
            </td>
        </tr>
    </table>
        </td>
    </tr>
</table>
    </asp:Content>