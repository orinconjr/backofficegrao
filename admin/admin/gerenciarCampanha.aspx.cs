﻿using System;
using System.Activities.Expressions;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.Data.Extensions;

public partial class admin_gerenciarCampanha : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            FillCampanhas();
            FillFornecedores();
        }
    }

    protected void FillCampanhas()
    {
        using (var data = new dbCommerceDataContext())
        {
            var campanhas = (from c in data.tbProdutoCategorias where c.categoriaPaiId == 774 | c.categoriaPaiId == 1433 orderby c.categoriaNome select c).ToList();
            ddlCampanhas.DataSource = campanhas;
            ddlCampanhas.DataBind();

            ddlLimparCampanha.DataSource = campanhas;
            ddlLimparCampanha.DataBind();

            ddlCampanhaFaixaDesconto.DataSource = campanhas;
            ddlCampanhaFaixaDesconto.DataBind();

            int categoria = Convert.ToInt32(ddlCampanhas.SelectedValue);
            var url = (from c in data.tbProdutoCategorias where c.categoriaId == categoria select new { c.categoriaUrl }).FirstOrDefault();
            lblUrlCategoria.Text = url.categoriaUrl;
        }


    }

    protected void FillFornecedores()
    {
        using (var data = new dbCommerceDataContext())
        {
            var fornecedores = (from c in data.tbProdutoFornecedors where c.gerarPedido orderby c.fornecedorNome select c).ToList();
            ddlFornecedores.DataSource = fornecedores;
            ddlFornecedores.DataBind();
        }
    }

    protected void OnSelectedIndexChanged(object sender, EventArgs e)
    {

        if (rblTipo.SelectedValue == "0")
        {
            pnlPorProduto.Visible = true;
            pnlPorFornecedor.Visible = false;
        }
        else
        {
            pnlPorProduto.Visible = false;
            pnlPorFornecedor.Visible = true;
        }

    }

    protected void ddlCampanhas_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        using (var data = new dbCommerceDataContext())
        {
            int categoria = Convert.ToInt32(ddlCampanhas.SelectedValue);
            var url = (from c in data.tbProdutoCategorias where c.categoriaId == categoria select new { c.categoriaUrl }).FirstOrDefault();
            lblUrlCategoria.Text = url.categoriaUrl;
        }

        hplCampanha.Visible = false;
    }

    protected void btnSalvar_Click(object sender, EventArgs e)
    {

        try
        {
            int categoria = Convert.ToInt32(ddlCampanhas.SelectedValue);

            if (rblTipo.SelectedValue == "0")
            {

                if (string.IsNullOrEmpty(txtIdsProdutos.Text))
                {
                    Response.Write("<script>alert('Informe os produtos!')</script>");
                    return;
                }

                List<int> idsProdutosSplit = txtIdsProdutos.Text.Split(',').Select(x => Convert.ToInt32(x.Trim())).ToList();
                using (var data = new dbCommerceDataContext())
                {
                    foreach (var produto in idsProdutosSplit)
                    {
                        var produtoPromocao = new tbJuncaoProdutoCategoria
                        {
                            produtoId = produto,
                            categoriaId = categoria,
                            produtoPaiId = 0,
                            dataDaCriacao = DateTime.Now
                        };

                        data.tbJuncaoProdutoCategorias.InsertOnSubmit(produtoPromocao);
                        data.SubmitChanges();

                        if (ckbAddColecao.Checked)
                        {
                            var colecao = (from c in data.tbJuncaoProdutoColecaos where c.produtoId == produto select c).Distinct().ToList();

                            if (colecao.Count() == 1)
                            {

                                var produtosDaColecao =
                                      (from c in data.tbJuncaoProdutoColecaos
                                       where c.colecaoId == colecao.FirstOrDefault().colecaoId && c.produtoId != produto
                                       select c);

                                foreach (var produtoColecao in produtosDaColecao)
                                {

                                    var produtoPromocaoColecao = new tbJuncaoProdutoCategoria
                                    {
                                        produtoId = produtoColecao.produtoId,
                                        categoriaId = categoria,
                                        produtoPaiId = 0,
                                        dataDaCriacao = DateTime.Now
                                    };

                                    data.tbJuncaoProdutoCategorias.InsertOnSubmit(produtoPromocaoColecao);
                                    data.SubmitChanges();
                                }
                            }
                        }
                    }

                    var queue = new tbQueue
                    {
                        tipoQueue = 16,
                        agendamento = DateTime.Now,
                        idRelacionado = categoria,
                        mensagem = "",
                        concluido = false,
                        andamento = false
                    };

                    data.tbQueues.InsertOnSubmit(queue);
                    data.SubmitChanges();
                }


            }
            else
            {
                int fornecedor = Convert.ToInt32(ddlFornecedores.SelectedValue);
                using (var data = new dbCommerceDataContext())
                {
                    var produtos = (from c in data.tbProdutos where c.produtoFornecedor == fornecedor && c.produtoAtivo == "True" select new { c.produtoId }).ToList();

                    foreach (var produto in produtos)
                    {
                        var produtoPromocao = new tbJuncaoProdutoCategoria
                        {
                            produtoId = produto.produtoId,
                            categoriaId = categoria,
                            produtoPaiId = 0,
                            dataDaCriacao = DateTime.Now
                        };

                        data.tbJuncaoProdutoCategorias.InsertOnSubmit(produtoPromocao);
                        data.SubmitChanges();
                    }
                }
            }

            Response.Write("<script>alert('Campanha criada com sucesso, aguarde alguns segundos e acesse o link exibido mais abaixo!')</script>");

            hplCampanha.NavigateUrl = "http://graodegente.com.br/" + lblUrlCategoria.Text;
            hplCampanha.Visible = true;
        }
        catch (Exception ex)
        {
            Response.Write("<script>alert('ERRO: " + ex.Message + "')</script>");

        }

    }


    protected void btnLimparCampanha_OnClick(object sender, EventArgs e)
    {
        int categoriaId = Convert.ToInt32(ddlLimparCampanha.SelectedValue);
        try
        {
            using (var data = new dbCommerceDataContext())
            {
                var produtosDaCategoria =
                    (from c in data.tbJuncaoProdutoCategorias where c.categoriaId == categoriaId select c);

                foreach (var produtoCategoria in produtosDaCategoria)
                {
                    data.tbJuncaoProdutoCategorias.DeleteOnSubmit(produtoCategoria);
                    data.SubmitChanges();
                }

                var queue = new tbQueue
                {
                    tipoQueue = 16,
                    agendamento = DateTime.Now,
                    idRelacionado = categoriaId,
                    mensagem = "",
                    concluido = false,
                    andamento = false
                };

                data.tbQueues.InsertOnSubmit(queue);
                data.SubmitChanges();
            }

            Response.Write("<script>alert('Campanha encerrada com sucesso!')</script>");
        }
        catch (Exception ex)
        {
            Response.Write("<script>alert('ERRO: " + ex.Message + "')</script>");
        }
    }

    protected void btnCadastrarCampanhaFaixaDesconto_OnClick(object sender, EventArgs e)
    {

        try
        {

            if (string.IsNullOrEmpty(txtDescontoInicial.Text) | string.IsNullOrEmpty(txtDescontoFinal.Text))
            {
                Response.Write("<script>alert('Informe os valores de porcentagem corretamente!')</script>");
                return;
            }

            int descontoInicial = Convert.ToInt32(txtDescontoInicial.Text);
            int descontoFinal = Convert.ToInt32(txtDescontoFinal.Text);
            int categoriaId = Convert.ToInt32(ddlCampanhaFaixaDesconto.SelectedValue);

            if (descontoInicial > descontoFinal)
            {
                Response.Write("<script>alert('Informe os valores de porcentagem corretamente!')</script>");
                return;
            }

            using (var data = new dbCommerceDataContext())
            {
                var produtosInserirCampanha = data.admin_produtosPorFaixaDeDesconto(descontoInicial, descontoFinal);

                foreach (var produto in produtosInserirCampanha)
                {
                    var produtoPromocao = new tbJuncaoProdutoCategoria
                    {
                        produtoId = produto.produtoId,
                        categoriaId = categoriaId,
                        produtoPaiId = 0,
                        dataDaCriacao = DateTime.Now
                    };

                    data.tbJuncaoProdutoCategorias.InsertOnSubmit(produtoPromocao);
                    data.SubmitChanges();
                }

                var queue = new tbQueue
                {
                    tipoQueue = 16,
                    agendamento = DateTime.Now,
                    idRelacionado = categoriaId,
                    mensagem = "",
                    concluido = false,
                    andamento = false
                };

                data.tbQueues.InsertOnSubmit(queue);
                data.SubmitChanges();
            }

            btnCadastrarCampanhaFaixaDesconto.Visible = false;

            Response.Write("<script>alert('Campanha por faixa de desconto criada com sucesso!')</script>");
        }
        catch (Exception ex)
        {
            Response.Write("<script>alert('ERRO: " + ex.Message + "')</script>");
        }
    }

    protected void btnQtdProdutosFaixaDesconto_OnClick(object sender, EventArgs e)
    {

        try
        {
            if (string.IsNullOrEmpty(txtDescontoInicial.Text) | string.IsNullOrEmpty(txtDescontoFinal.Text))
            {
                Response.Write("<script>alert('Informe os valores de porcentagem corretamente!')</script>");
                return;
            }

            int descontoInicial = Convert.ToInt32(txtDescontoInicial.Text);
            int descontoFinal = Convert.ToInt32(txtDescontoFinal.Text);

            if (descontoInicial > descontoFinal)
            {
                Response.Write("<script>alert('Informe os valores de porcentagem corretamente!')</script>");
                return;
            }

            using (var data = new dbCommerceDataContext())
            {
                int qtd = data.admin_produtosPorFaixaDeDesconto(descontoInicial, descontoFinal).Count();

                btnCadastrarCampanhaFaixaDesconto.Visible = qtd > 0;

                lblQtdPRodutosFaixaDesconto.Text = qtd + " itens encontrados";

            }
        }
        catch (Exception ex)
        {
            Response.Write("<script>alert('ERRO: " + ex.Message + "')</script>");
        }
    }
}