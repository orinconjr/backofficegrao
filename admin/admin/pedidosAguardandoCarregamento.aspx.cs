﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AmazingCloudSearch.Enum;
using DevExpress.Data.Filtering;
using DevExpress.Web.ASPxCallback;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;

public partial class admin_pedidosAguardandoCarregamento : System.Web.UI.Page
{
    DateTemplate dateTemplate = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        fillGrid(false);
        if (Request.QueryString["nfe"] != null)
        {
            EnviaNotaTnt(Convert.ToInt32(Request.QueryString["nfe"]));
        }
    }

    private void fillGrid(bool rebind)
    {
        var data = new dbCommerceDataContext();
        var pedidosIdsEnvio = (from c in data.tbPedidoPacotes where (c.despachado ?? false) == false && c.formaDeEnvio.ToLower() == "pac" && c.rastreio != "" select c.idPedido).Distinct();

        //var pedidos = (from c in data.tbPedidos
        //               join d in data.tbPedidoPacotes on c.pedidoId equals d.idPedido into pacotes
        //               join e in data.tbPedidoEnvios on c.pedidoId equals e.idPedido into envios
        //               where pedidosIdsEnvio.Contains(c.pedidoId) && (pacotes.Count(x => (x.despachado ?? false) == false)) > 0 && c.statusDoPedido != 6 && (pacotes.Count(x => x.formaDeEnvio.ToLower() == "pac") > 0)  && ((pacotes.Count(x => x.rastreio != "") > 0) | (envios.Count(x => x.nfeImpressa == true && x.formaDeEnvio == "tnt") > 0))
        //               select new
        //               {
        //                   c.pedidoId,
        //                   c.endNomeDoDestinatario,
        //                   c.idUsuarioEmbalado,
        //                   c.dataEmbaladoFim,
        //                   volumesEmbalados = pacotes.Count(x => (x.despachado ?? false) == false),
        //                   formaDeEnvio = pacotes.FirstOrDefault(x => (x.despachado ?? false) == false).formaDeEnvio
        //               }).OrderBy(x => x.dataEmbaladoFim);


        var pedidos = (from c in data.tbPedidos
                       join d in data.tbPedidoPacotes on c.pedidoId equals d.idPedido
                       join e in data.tbPedidoEnvios on c.pedidoId equals e.idPedido into envios
                       where pedidosIdsEnvio.Contains(c.pedidoId) && c.statusDoPedido != 6 && d.formaDeEnvio.ToLower() == "pac"
                       select new
                       {
                           c.pedidoId,
                           c.endNomeDoDestinatario,
                           c.idUsuarioEmbalado,
                           c.dataEmbaladoFim,
                           d.rastreio,
                           d.formaDeEnvio
                       }).OrderBy(x => x.dataEmbaladoFim);

        grd.DataSource = pedidos;
        if ((!Page.IsPostBack && !Page.IsCallback) | rebind) grd.DataBind();
    }
    protected void btPdf_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WritePdfToResponse();
    }
    protected void btXsl_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteXlsToResponse();
    }
    protected void btRtf_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteRtfToResponse();
    }
    protected void btCsv_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteCsvToResponse();
    }

    protected void grid_AutoFilterCellEditorCreate(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewEditorCreateEventArgs e)
    {
        ASPxGridView grid = sender as ASPxGridView;
        if (e.Column.FieldName == "dataSendoEmbalado")
        {
            DropDownEditProperties dde = new DropDownEditProperties();
            dde.EnableClientSideAPI = true;

            dateTemplate = new DateTemplate();

            dde.DropDownWindowTemplate = dateTemplate;
            e.EditorProperties = dde;
        }
    }
    protected void grid_AutoFilterCellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
    {
        ASPxGridView grid = sender as ASPxGridView;
        if (e.Column.FieldName == "dataSendoEmbalado")
        {
            ASPxDropDownEdit dde = e.Editor as ASPxDropDownEdit;

            dde.ClientSideEvents.CloseUp = String.Format("function (s, e) {{ ApplyFilter(s, {0}, {1}); }}", dateTemplate.cIdFrom, dateTemplate.cIdTo);
            //dde.ClientSideEvents.DropDown = String.Format("function (s, e) {{ OnDropDown(s, {0}, {1}); }}", dateTemplate.cIdFrom, dateTemplate.cIdTo);
            dde.ReadOnly = true;
        }
    }

    protected void grid_ProcessColumnAutoFilter(object sender, ASPxGridViewAutoFilterEventArgs e)
    {
        if (e.Value == "|") return;
        if (e.Column.FieldName == "dataSendoEmbalado")
        {
            if (e.Kind == GridViewAutoFilterEventKind.CreateCriteria)
            {
                Session["dataSendoEmbalado"] = e.Value;
                String[] dates = e.Value.Split('|');
                DateTime dateFrom = Convert.ToDateTime(dates[0]),
                    dateTo = Convert.ToDateTime(dates[1]);
                e.Criteria = (new OperandProperty("dataSendoEmbalado") >= dateFrom) &
                             (new OperandProperty("dataSendoEmbalado") <= dateTo);
            }
            else
            {
                if (Session["dataSendoEmbalado"] != null)
                    e.Value = Session["dataSendoEmbalado"].ToString();
            }
        }
    }

    protected void btnChecarPedidosMl_OnClick(object sender, EventArgs e)
    {
        //var meli = mlIntegracao.verificaAutenticacao("pedidos.aspx", Request.QueryString["code"]);
        mlIntegracao.checaPedidosML();
        rnIntegracoes.checaPedidosExtra(true);
        grd.DataBind();
    }

    protected void grd_OnCustomUnboundColumnData(object sender, ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "pedidoIdCliente")
        {
            int pedidoId = Convert.ToInt32(e.GetListSourceFieldValue("pedidoId"));
            e.Value = rnFuncoes.retornaIdCliente(pedidoId);
        }
    }

    protected void btnGravar_OnClick(object sender, EventArgs e)
    {
        /*int inicio = 0;
        int fim = 0;

        if (grd.PageIndex == 0)
        {
            inicio = 0;
            fim = inicio + grd.GetCurrentPageRowValues("pedidoId").Count;
        }
        else
        {
            inicio += inicio + grd.SettingsPager.PageSize * grd.PageIndex;
            fim += inicio + grd.GetCurrentPageRowValues("pedidoId").Count;
        }

        var data = new dbCommerceDataContext();
        for (int i = inicio; i < fim; i++)
        {
            CheckBox chkEnviado = (CheckBox)grd.FindRowCellTemplateControl(i, grd.Columns["carregado"] as GridViewDataColumn, "chkEnviado");
            int pedidoId = (int)grd.GetRowValues(i, new string[] { "pedidoId" });

            if (chkEnviado.Checked)
            {
                int idPedidoEnvio = 0;
                bool tnt = false;
                bool jadlog = false;
                var pedidoPacote = (from c in data.tbPedidoPacotes where c.idPedido == pedidoId && c.idPedidoEnvio != null orderby c.idPedidoEnvio descending select c).ToList();
                string rastreio = "";
                foreach (var pacote in pedidoPacote)
                {
                    try
                    {
                        pacote.despachado = true;
                        //data.SubmitChanges();
                        idPedidoEnvio = (int)pacote.idPedidoEnvio;
                        if (pacote.formaDeEnvio.ToLower() == "tnt") tnt = true;
                        if (pacote.formaDeEnvio.ToLower() == "jadlog" | pacote.formaDeEnvio.ToLower() == "jadlogexpressa")
                        {
                            jadlog = true;
                            rastreio = pacote.rastreio;
                        }
                    }
                    catch (Exception ex)
                    {
                        
                    }
                }
                try
                {
                    if (tnt)
                    {
                        //var retorno = rnEmails.enviaCodigoDoTrakingTnt(idPedidoEnvio);
                    }
                    if (jadlog)
                    {
                        //var retorno = rnEmails.enviaCodigoDoTrakingJadlog(pedidoId, rastreio);
                    }
                }
                catch (Exception)
                {

                }
                interacaoInclui("Pedido marcado como carregado", "False", pedidoId);
            }
        }

        fillGrid(true);*/
    }

    public void interacaoInclui(string interacao, string alteracaoDeEstatus, int pedidoId)
    {
        HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
        usuarioLogadoId = Request.Cookies["usuarioLogadoId"];
        if (usuarioLogadoId != null)
        {
            rnInteracoes.interacaoInclui(pedidoId, interacao, rnUsuarios.usuarioSeleciona_PorUsuarioId(int.Parse(usuarioLogadoId.Value.ToString())).Tables[0].Rows[0]["usuarioNome"].ToString(), alteracaoDeEstatus);
        }
    }

    protected void cbGravar_OnCallback(object source, CallbackEventArgs e)
    {
        int inicio = 0;
        int fim = 0;

        if (grd.PageIndex == 0)
        {
            inicio = 0;
            fim = inicio + grd.GetCurrentPageRowValues("pedidoId").Count;
        }
        else
        {
            inicio += inicio + grd.SettingsPager.PageSize * grd.PageIndex;
            fim += inicio + grd.GetCurrentPageRowValues("pedidoId").Count;
        }

        var listaIds = new List<int>();
        var data = new dbCommerceDataContext();
        for (int i = inicio; i < fim; i++)
        {
            CheckBox chkEnviado = (CheckBox)grd.FindRowCellTemplateControl(i, grd.Columns["carregado"] as GridViewDataColumn, "chkEnviado");
            int pedidoId = (int)grd.GetRowValues(i, new string[] { "pedidoId" });

            if (chkEnviado.Checked)
            {
                int idPedidoEnvio = 0;
                bool tnt = false;
                bool jadlog = false;
                var pedidoPacote = (from c in data.tbPedidoPacotes where c.idPedido == pedidoId && c.idPedidoEnvio != null orderby c.idPedidoEnvio descending select c);
                string rastreio = "";
                foreach (var pacote in pedidoPacote)
                {
                    try
                    {
                        pacote.despachado = true;
                        pacote.dataDeCarregamento = DateTime.Now;
                        data.SubmitChanges();
                        idPedidoEnvio = (int)pacote.idPedidoEnvio;
                        if (pacote.formaDeEnvio.ToLower() == "tnt") tnt = true;
                        if (pacote.formaDeEnvio.ToLower() == "jadlog" | pacote.formaDeEnvio.ToLower() == "jadlogexpressa")
                        {
                            jadlog = true;
                            rastreio = pacote.rastreio;
                        }
                    }
                    catch (Exception)
                    {
                        
                    }
                }
                var queue = new tbQueue();
                queue.agendamento = DateTime.Now.AddHours(3);
                queue.idRelacionado = idPedidoEnvio;
                queue.tipoQueue = 2;
                queue.concluido = false;
                queue.andamento = false;
                queue.mensagem = "";
                data.tbQueues.InsertOnSubmit(queue);
                data.SubmitChanges();

                //try
                //{
                //    if (tnt)
                //    {
                //        rnEmails.enviaCodigoDoTrakingTnt(idPedidoEnvio, "");
                //    }
                //    if (jadlog)
                //    {
                //        var retorno = rnEmails.enviaCodigoDoTrakingJadlog(pedidoId, rastreio);
                //    }
                //}
                //catch (Exception)
                //{
                    
                //}
                interacaoInclui("Pedido marcado como carregado", "False", pedidoId);
            }
        }

        fillGrid(true);
    }
    private void EnviaNotaTnt(int nfeNumero)
    {
        rnEmails.EnviaNotaTnt(nfeNumero);
    }
}
