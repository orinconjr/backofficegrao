﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="relatorioLetsFamily.aspx.cs" Inherits="admin_relatorioLetsFamily" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" runat="Server">
    <script src="js/jquery.min.js"></script>
    <style>
        .meubotao{
            cursor:pointer;
            font:bold 14px tahoma;
        }
        .fieldsetatualizarprecos {
            width: 848px;
            float: left;
            clear: left;
            border-radius: 5px;
            margin-left: 25px;
            font:bold 14px tahoma;
        }

            .fieldsetatualizarprecos input[type=checkbox] {
                margin-top: 15px;
            }

            .fieldsetatualizarprecos label {
                margin-left: 5px;
                margin-right: 20px;
            }

        .meugrid {
            background-color: #F4FAFB;
            border: 0px none;
            /* border-collapse: separate !important; */
            overflow: hidden;
            font: 9pt Tahoma;
            color: #000;
            cursor: pointer;
            white-space: nowrap;
            font-weight: normal;
            text-align: left;
            width: 834px;
            border-collapse: collapse;
            float: left;
            clear: left;
            margin-top: 10px;
            margin-bottom: 10px;
            min-height: 500px;
            margin-left: 25px;
        }

            .meugrid th {
                background-image: url(../../App_Themes/Glass/GridView/gvHeaderBackground.gif);
                background-position: center top;
                background-repeat: repeat-x;
                background-color: #C0DDE0;
                overflow: hidden;
                height: 23px !important;
                line-height: 23px !important;
                padding: 4px 6px 5px;
                font-weight: normal;
                border: 1px solid #7EACB1;
                /* border-collapse: separate;*/
            }

            .meugrid td {
                overflow: hidden;
                height: 23px !important;
                line-height: 23px !important;
                padding: 4px 6px 5px;
                border: 1px solid #7EACB1;
            }

            .meugrid tr td:last-of-type {
                text-align: center;
            }

        .tabrotulos {
            width: 848px;
            margin: auto;
        }

        .materiaprima {
            width: 250px;
        }

            .materiaprima ul {
                margin: 0px;
                padding-left: 10px;
            }

                .materiaprima ul li {
                    max-width: 230px;
                    white-space: pre-wrap;
                }

        .meugrid tr td:last-of-type {
            text-align: center;
        }

        .nome {
            width: 237px;
        }

            .nome div {
                max-width: 230px;
                white-space: pre-wrap;
            }

        .rotulos td {
            font: bold 12px tahoma;
            /*padding-top: 20px; */
            color: rgb(65, 105, 126);
            padding-left: 5px;
            /*padding-bottom: 5px;*/
        }

    </style>
    <div class="tituloPaginas" valign="top">
        Relatório de Campanhas Lets Family - Cupons Usados
    </div>
    <div>
        <fieldset class="fieldsetatualizarprecos">
            <legend style="font-weight: bold;">Filtro</legend>

            <table class="tabrotulos">
                <tr class="rotulos">
                    <td>Num. Pedido:<br />
                        <asp:TextBox runat="server" ID="txtNumPedido"></asp:TextBox>
                    </td>

                    <td>&nbsp;
                    </td>

                    <td>Campanha:<br />
                        <asp:DropDownList runat="server" ID="ddlCampanha" Width="370px" CssClass="campos">
                            <Items>
                                <asp:ListItem Text="Todas" Value="0"></asp:ListItem>
                                <asp:ListItem Text="LetsFamily-Especial" Value="2"></asp:ListItem>
                                <asp:ListItem Text="LetsFamily-Mamy" Value="3"></asp:ListItem>
                                <asp:ListItem Text="LetsFamily-Plus" Value="4"></asp:ListItem>
                                <asp:ListItem Text="LetsFamily-Premium" Value="5"></asp:ListItem>
                                <asp:ListItem Text="LetsFamily-PreNatal" Value="6"></asp:ListItem>
                           
                            </Items>
                        </asp:DropDownList>
                    </td>

                </tr>
                <tr class="rotulos">

                    <td>Data Inicial:<br />
                        <asp:TextBox runat="server" ID="txtDataInicial"></asp:TextBox>
                    </td>
                    <td>Data Final:<br />
                        <asp:TextBox runat="server" ID="txtDataFinal"></asp:TextBox>
                    </td>
                    <td >
                        Ordenar por:<br />
                        <asp:DropDownList runat="server" ID="ddlordenarpor" Width="370px"  CssClass="campos">
                            <Items>
                                <asp:ListItem Text="Id do Pedido" Value="pedidoId"></asp:ListItem>
                                <asp:ListItem Text="Data" Value="pedidoData"></asp:ListItem>
                                <asp:ListItem Text="Campanha" Value="campanhaNome"></asp:ListItem>
                            </Items>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:RadioButton ID="rbPagos" runat="server" Checked="true" GroupName="a" />Pagos&nbsp;&nbsp;
                        <asp:RadioButton ID="rbNaoPagos" runat="server" GroupName="a" />Não Pagos&nbsp;&nbsp;
                        <asp:RadioButton ID="rbTodos" runat="server" GroupName="a" />Todos
                    </td>
                    
                    <td style="padding-top: 15px;text-align:right;">
                        <asp:Button runat="server" ID="btnFiltrar" CssClass="meubotao" Text="Filtrar" OnClick="btnFiltrar_Click" />
                    </td>
                </tr>

            </table>
        </fieldset>
       
        <div style="float:left;clear:left;width:868px;text-align:right;margin-left: 25px;margin-top:15px;">
              <asp:Button runat="server" ID="btnExportarPlanilha"  CssClass="meubotao"   Text="Exportar Planilha" OnClick="btnExportarPlanilha_Click" />
        </div>
    </div>
    <div align="center" style="min-height: 500px;">

        <asp:GridView ID="GridView1" CssClass="meugrid" runat="server" DataKeyNames="IdCupom" Width="834px" AutoGenerateColumns="False" AllowPaging="True" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowDataBound="GridView1_RowDataBound" PageSize="20">
            <Columns>
                <asp:BoundField DataField="pedidoId" HeaderText="Id Pedido" />
                <asp:BoundField DataField="pedidoData" HeaderText="Data" />
                <asp:BoundField DataField="situacao" HeaderText="Status" />
                <asp:BoundField DataField="valorTotalGeral" DataFormatString=" {0:C}" HeaderText="Vl do Pedido" />
                <asp:BoundField DataField="idCupom"  HeaderText="Cupom" />
                <asp:BoundField DataField="campanhaNome"  HeaderText="Campanha" />
                <asp:BoundField DataField="clienteNome"  HeaderText="Cliente" />
                <asp:BoundField DataField="clienteEmail"  HeaderText="E-mail" />



                <asp:TemplateField HeaderText="Editar" ShowHeader="False">
                    <ItemTemplate>
                        <a id="linkeditar" runat="server" target="_blank">
                            <img src="images/btEditar.jpg" alt="Editar" style="border-style: none;" />
                        </a>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>

    </div>
    <div style="float: left; clear: left; width: 868px;margin-left: 25px; margin-top: 15px;">
        <strong>Quantidade de itens encontrados nesta busca:
        <span style="text-decoration: underline">
            <asp:Label ID="lblitensencontrados" runat="server" Text=""></asp:Label></span>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
           Valor total nesta busca: <span style="text-decoration: underline"><asp:Label ID="lblvalortotal" runat="server" Text=""></asp:Label></span>
        </strong>
      
    </div>
</asp:Content>

