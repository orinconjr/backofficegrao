﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="entradasProdutosPorPeriodo.aspx.cs" Inherits="admin_entradasProdutosPorPeriodo" Theme="Glass" %>

<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" runat="Server">

    <asp:ScriptManager runat="server"></asp:ScriptManager>


    <asp:UpdatePanel runat="server">
        <ContentTemplate>



            <table cellpadding="0" cellspacing="0" style="width: 920px">
                <tr>
                    <td class="tituloPaginas" valign="top">
                        <asp:Label ID="lblAcao" runat="server">Entradas de produtos por período</asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>

                        <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
                            <tr>
                                <td align="right">
                                    <table cellpadding="0" cellspacing="0" style="width: 834px">
                                        <tr>
                                            <td align="right">&nbsp;</td>
                                            <td height="38"
                                                style="background-image: url('images/bkExportar.jpg'); padding-bottom: 1px;"
                                                valign="bottom" width="231">
                                                <table cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td style="width: 97px">&nbsp;</td>
                                                        <td style="width: 33px">
                                                            <asp:ImageButton ID="btPdf" runat="server" ImageUrl="~/admin/images/btPdf.jpg"
                                                                Style="margin-left: 5px" OnClick="btPdf_Click" />
                                                        </td>
                                                        <td style="width: 31px">
                                                            <asp:ImageButton ID="btXsl" runat="server" ImageUrl="~/admin/images/btXls.jpg"
                                                                Style="margin-left: 5px" OnClick="btXsl_Click" />
                                                        </td>
                                                        <td style="width: 36px">
                                                            <asp:ImageButton ID="btRtf" runat="server" ImageUrl="~/admin/images/btRtf.jpg"
                                                                Style="margin-left: 5px" OnClick="btRtf_Click" />
                                                        </td>
                                                        <td valign="bottom">
                                                            <asp:ImageButton ID="btCsv" runat="server" ImageUrl="~/admin/images/btCsv.jpg"
                                                                Style="margin-left: 5px" OnClick="btCsv_Click" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    <table cellpadding="0" cellspacing="0" style="width: 490px">
                                        <tr>
                                            <td class="rotulos" style="width: 100px">Data inicial<br __designer:mapid="3fc" />
                                                <asp:TextBox ID="txtDataInicial" runat="server" CssClass="campos"
                                                    Text='<%# Bind("faixaDeCepPesoInicial") %>'
                                                    Width="90px" MaxLength="10"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rqvDataInicial" runat="server"
                                                    ControlToValidate="txtDataInicial" Display="None"
                                                    ErrorMessage="Preencha a data inicial."
                                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                <b __designer:mapid="27df">
                                                    <asp:RegularExpressionValidator ID="rgeDataInicial" runat="server"
                                                        ControlToValidate="txtDataInicial" Display="None"
                                                        ErrorMessage="Por favor, preencha corretamente a data inicial."
                                                        SetFocusOnError="True"
                                                        ValidationExpression="^(([0-2]\d|[3][0-1])\/([0]\d|[1][0-2])\/[1-2][0-9]\d{2})$"></asp:RegularExpressionValidator>
                                                </b>
                                            </td>
                                            <td class="rotulos" style="width: 100px">Data final<br __designer:mapid="401" />
                                                <asp:TextBox ID="txtDataFinal" runat="server" CssClass="campos"
                                                    Text='<%# Bind("faixaDeCepPesoFinal") %>' ValidationGroup="grpInsert"
                                                    Width="90px" MaxLength="10"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rqvDataFinal" runat="server"
                                                    ControlToValidate="txtDataFinal" Display="None"
                                                    ErrorMessage="Preencha a data final."
                                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                <b __designer:mapid="27df">
                                                    <asp:RegularExpressionValidator ID="rgeDataInicial0" runat="server"
                                                        ControlToValidate="txtDataFinal" Display="None"
                                                        ErrorMessage="Por favor, preencha corretamente a data final"
                                                        SetFocusOnError="True"
                                                        ValidationExpression="^(([0-2]\d|[3][0-1])\/([0]\d|[1][0-2])\/[1-2][0-9]\d{2})$"></asp:RegularExpressionValidator>
                                                </b>
                                            </td>
                                            <td class="rotulos">Fornecedor<br __designer:mapid="3fc" />
                                                <asp:DropDownList ID="ddlFornecedores" runat="server" AppendDataBoundItems="True" Width="175" Height="26"
                                                    DataSourceID="sqlFornecedores" DataTextField="fornecedorNome" DataValueField="fornecedorId">
                                                </asp:DropDownList>
                                            </td>
                                            <td valign="middle">
                                                <asp:ImageButton ID="imbInsert" runat="server" Style="margin-top: 14px;"
                                                    ImageUrl="~/admin/images/btPesquisar.jpg" />
                                                <asp:ValidationSummary ID="llds" runat="server" DisplayMode="List"
                                                    ShowMessageBox="True" ShowSummary="False" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td valign="top">
                                                <dxwgv:ASPxGridView ID="grd" runat="server" AutoGenerateColumns="False"
                                                    Width="400px" KeyFieldName="DataPedido" OnCustomSummaryCalculate="grd_CustomSummaryCalculate"
                                                    Cursor="auto" DataSourceID="sql">
                                                    <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                                                    <Styles>
                                                        <Footer Font-Bold="True">
                                                        </Footer>
                                                    </Styles>
                                                    <SettingsText ConfirmDelete="Tem certeza que deseja excluir?"
                                                        EmptyDataRow="Nenhum registro encontrado." />
                                                    <SettingsPager Position="TopAndBottom" PageSize="50"
                                                        ShowDisabledButtons="False" AlwaysShowPager="False" Visible="True">
                                                        <Summary AllPagesText="Páginas: {0} - {1} ({2} items)"
                                                            Text="Página {0} de {1} ({2} registros encontrados)" />
                                                    </SettingsPager>
                                                    <Settings ShowFilterRow="True" ShowGroupButtons="False" ShowFooter="True" />
                                                    <TotalSummary>
                                                        <dxwgv:ASPxSummaryItem FieldName="QtdSolicitados" DisplayFormat="Qtd. Solicitados {0}"
                                                            ShowInColumn="Qtd. Solicitados" ShowInGroupFooterColumn="Qtd. Solicitados"
                                                            SummaryType="Sum" />
                                                        <dxwgv:ASPxSummaryItem FieldName="QtdEntregues" DisplayFormat="Qtd. Entregues {0}"
                                                            ShowInColumn="Qtd. Entregues" ShowInGroupFooterColumn="Qtd. Entregues"
                                                            SummaryType="Sum" />
                                                        <dxwgv:ASPxSummaryItem FieldName="QtdEntregaPrevista" DisplayFormat="Qtd. Entrega Prevista {0}"
                                                            ShowInColumn="Qtd. Entrega Prevista" ShowInGroupFooterColumn="Qtd. Entrega Prevista"
                                                            SummaryType="Sum" />
                                                        <%--<dxwgv:ASPxSummaryItem FieldName="QtdEntregaPrevista"
                                                            ShowInColumn="Qtd. Entrega Prevista" ShowInGroupFooterColumn="Qtd. Faltando"
                                                            SummaryType="Custom" />--%>
                                                        <dxwgv:ASPxSummaryItem FieldName="QtdFaltando" DisplayFormat="Qtd. Faltando {0}"
                                                            ShowInColumn="Qtd. Faltando" ShowInGroupFooterColumn="Qtd. Faltando"
                                                            SummaryType="Sum" />
                                                    </TotalSummary>
                                                    <SettingsEditing EditFormColumnCount="4"
                                                        PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1"
                                                        PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter"
                                                        PopupEditFormWidth="700px" Mode="Inline" />
                                                    <Columns>
                                                        <dxwgv:GridViewDataTextColumn Caption="Data do Pedido" FieldName="DataPedido"
                                                            VisibleIndex="0" PropertiesTextEdit-DisplayFormatString="{0:d}">
                                                        </dxwgv:GridViewDataTextColumn>
                                                        <dxwgv:GridViewDataTextColumn Caption="Qtd. Solicitados" FieldName="QtdSolicitados"
                                                            VisibleIndex="1">
                                                            <Settings AutoFilterCondition="Contains" />
                                                        </dxwgv:GridViewDataTextColumn>
                                                        <dxwgv:GridViewDataTextColumn Caption="Qtd. Entregues" FieldName="QtdEntregues"
                                                            VisibleIndex="1">
                                                            <Settings AutoFilterCondition="Contains" />
                                                        </dxwgv:GridViewDataTextColumn>
                                                        <dxwgv:GridViewDataTextColumn Caption="Qtd. Entrega Prevista" FieldName="QtdEntregaPrevista"
                                                            VisibleIndex="1">
                                                            <Settings AutoFilterCondition="Contains" />
                                                        </dxwgv:GridViewDataTextColumn>
                                                        <dxwgv:GridViewDataTextColumn Caption="Qtd. Faltando" FieldName="QtdFaltando"
                                                            VisibleIndex="1">
                                                            <Settings AutoFilterCondition="Contains" />
                                                        </dxwgv:GridViewDataTextColumn>
                                                        <dxwgv:GridViewDataHyperLinkColumn Caption="Visualizar" VisibleIndex="25"
                                                            Width="52px">
                                                            <DataItemTemplate>
                                                                <asp:LinkButton ID="lbtDetalhesData" runat="server" Text="visualizar" CommandArgument='<%# Bind("DataPedido") %>'
                                                                    CommandName="Visualizar" OnCommand="lbtDetalhesData_Command" ToolTip=""></asp:LinkButton>
                                                            </DataItemTemplate>
                                                            <CellStyle HorizontalAlign="Center">
                                                            </CellStyle>
                                                        </dxwgv:GridViewDataHyperLinkColumn>
                                                    </Columns>
                                                    <StylesEditors>
                                                        <Label Font-Bold="True">
                                                        </Label>
                                                    </StylesEditors>
                                                </dxwgv:ASPxGridView>
                                            </td>
                                        </tr>
                                        <tr runat="server" id="linhaDetalhes" visible="false">
                                            <td>
                                                <asp:Button ID="btnVoltar" runat="server" Text="<< Voltar" OnClick="btnVoltar_Click" />
                                                <dxwgv:ASPxGridView ID="grdDetalhes" runat="server" AutoGenerateColumns="False"
                                                    Width="400px" KeyFieldName="produtoId"
                                                    Cursor="auto">
                                                    <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                                                    <Styles>
                                                        <Footer Font-Bold="True">
                                                        </Footer>
                                                    </Styles>
                                                    <SettingsText ConfirmDelete="Tem certeza que deseja excluir?"
                                                        EmptyDataRow="Nenhum registro encontrado." />
                                                    <SettingsPager Position="TopAndBottom" PageSize="50"
                                                        ShowDisabledButtons="False" AlwaysShowPager="False" Visible="True">
                                                        <Summary AllPagesText="Páginas: {0} - {1} ({2} items)"
                                                            Text="Página {0} de {1} ({2} registros encontrados)" />
                                                    </SettingsPager>
                                                    <Settings ShowFilterRow="True" ShowGroupButtons="False" ShowFooter="True" />
                                                    <TotalSummary>
                                                        <dxwgv:ASPxSummaryItem FieldName="QtdPedidos"
                                                            ShowInColumn="Qtd. Pedidos" ShowInGroupFooterColumn="Qtd. Pedidos"
                                                            SummaryType="Sum" />
                                                        <dxwgv:ASPxSummaryItem FieldName="QtdEntregues"
                                                            ShowInColumn="Qtd. Entradas" ShowInGroupFooterColumn="Qtd. Entradas"
                                                            SummaryType="Sum" />
                                                        <dxwgv:ASPxSummaryItem FieldName="QtdEntregaPrevista"
                                                            ShowInColumn="Qtd. Prevista" ShowInGroupFooterColumn="Qtd. Prevista"
                                                            SummaryType="Sum" />
                                                        <dxwgv:ASPxSummaryItem FieldName="QtdFaltando"
                                                            ShowInColumn="Qtd. Faltando" ShowInGroupFooterColumn="Qtd. Faltando"
                                                            SummaryType="Sum" />
                                                    </TotalSummary>
                                                    <SettingsEditing EditFormColumnCount="4"
                                                        PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1"
                                                        PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter"
                                                        PopupEditFormWidth="700px" Mode="Inline" />
                                                    <Columns>
                                                        <dxwgv:GridViewDataTextColumn Caption="ProdutoId" FieldName="produtoId"
                                                            VisibleIndex="0">
                                                        </dxwgv:GridViewDataTextColumn>
                                                        <dxwgv:GridViewDataTextColumn Caption="Produto Nome" FieldName="produtoNome"
                                                            VisibleIndex="0">
                                                        </dxwgv:GridViewDataTextColumn>
                                                        <dxwgv:GridViewDataTextColumn Caption="Data do Pedido" FieldName="DataPedido"
                                                            VisibleIndex="0" PropertiesTextEdit-DisplayFormatString="{0:d}">
                                                        </dxwgv:GridViewDataTextColumn>
                                                        <dxwgv:GridViewDataTextColumn Caption="Qtd. Pedidos" FieldName="QtdPedidos"
                                                            VisibleIndex="1">
                                                            <Settings AutoFilterCondition="Contains" />
                                                        </dxwgv:GridViewDataTextColumn>
                                                        <dxwgv:GridViewDataTextColumn Caption="Qtd. Entradas" FieldName="QtdEntregues"
                                                            VisibleIndex="1">
                                                            <Settings AutoFilterCondition="Contains" />
                                                        </dxwgv:GridViewDataTextColumn>
                                                         <dxwgv:GridViewDataTextColumn Caption="Qtd. Prevista" FieldName="QtdEntregaPrevista"
                                                            VisibleIndex="1">
                                                            <Settings AutoFilterCondition="Contains" />
                                                        </dxwgv:GridViewDataTextColumn>
                                                        <dxwgv:GridViewDataTextColumn Caption="Qtd. Faltando" FieldName="QtdFaltando"
                                                            VisibleIndex="1">
                                                            <Settings AutoFilterCondition="Contains" />
                                                        </dxwgv:GridViewDataTextColumn>
                                                    </Columns>
                                                    <StylesEditors>
                                                        <Label Font-Bold="True">
                                                        </Label>
                                                    </StylesEditors>
                                                </dxwgv:ASPxGridView>
                                            </td>
                                        </tr>
                                    </table>




                                    <dxwgv:ASPxGridViewExporter ID="grdEx" runat="server" FileName="fornecedores"
                                        GridViewID="grd1" Landscape="True" PreserveGroupRowStates="True">
                                    </dxwgv:ASPxGridViewExporter>


                                    <asp:ObjectDataSource ID="sql" runat="server"
                                        SelectMethod="entradaDeProdutoPorPeriodo" TypeName="rnRelatorios">
                                        <SelectParameters>
                                            <asp:ControlParameter ControlID="txtDataInicial" Name="dataInicial"
                                                PropertyName="Text" Type="String" />
                                            <asp:ControlParameter ControlID="txtDataFinal" Name="dataFinal"
                                                PropertyName="Text" Type="String" />
                                            <asp:ControlParameter ControlID="ddlFornecedores" Name="fornecedorId"
                                                PropertyName="Text" Type="String" />
                                        </SelectParameters>
                                    </asp:ObjectDataSource>


                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>

            <asp:SqlDataSource ID="sqlFornecedores" runat="server"
                ConnectionString="<%$ ConnectionStrings:connectionString %>"
                SelectCommand="SELECT [fornecedorId], [fornecedorNome] FROM [tbProdutoFornecedor] ORDER BY [fornecedorNome]"></asp:SqlDataSource>

        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>

