﻿<%@ Page Title="" Theme="Glass" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="chamados.aspx.cs" Inherits="admin_chamados" %>

<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxTimer" TagPrefix="dxt" %>
<%@ Register TagPrefix="dxe" Namespace="DevExpress.Web.ASPxMenu" Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxPopupControl" Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxEditors" Assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>
<%@ Register Assembly="CuteEditor" Namespace="CuteEditor" TagPrefix="CE" %>

<%@ Register Assembly="DevExpress.Web.v11.1" Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" runat="Server">
    <script type="text/javascript">
        function validaChamado() {
            var depto = document.getElementById("<%=ddlDepartamento.ClientID%>");
            var tarefa = document.getElementById("<%=ddlTarefa.ClientID%>");
            var categoria = document.getElementById("<%=ddlAssunto.ClientID%>");

            //if (depto.value == null || depto.value == "0") {
            //    alert("Favor selecionar o campo departamento");
            //    depto.focus();
            //    return false;
            //}

            //if (tarefa.value == null || tarefa.value == "0") {
            //    alert("Favor selecionar o campo tarefa");
            //    tarefa.focus();
            //    return false;
            //}

            //if (categoria.value == null || categoria.value == "0") {
            //    alert("Favor selecionar o campo categoria");
            //    categoria.focus();
            //    return false;
            //}

            return true;
        }
    </script>
    <asp:ScriptManager ID="scriptManager" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="updPanel" runat="server">
        <ContentTemplate>
            <dx:ASPxPopupControl ID="popAdicionarChamado" runat="server" CloseAction="CloseButton" Modal="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
                ClientInstanceName="popAdicionarChamado" HeaderText="Adicionar Chamado" AllowDragging="True" PopupAnimationType="None" EnableViewState="True" Height="500" AutoUpdatePosition="True">
                <ContentCollection>
                    <dx:PopupControlContentControl ID="PopupControlContentControl6" runat="server">
                        <asp:HiddenField runat="server" ID="hdfIdAgendamento" />
                        <div style="width: 800px; height: 600px;">
                            <table width="100%">
                                <tr class="rotulos">
                                    <td>
                                        <asp:CheckBox runat="server" ID="chkChamadoConcluido" Text="Concluído" />
                                        <asp:CheckBox runat="server" ID="chkRetorno" Text="Retornar chamado" AutoPostBack="true" OnCheckedChanged="chkRetorno_CheckedChanged" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table width="100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 33%">
                                                    <b>Departamento:</b><br />
                                                    <asp:DropDownList ID="ddlDepartamento" runat="server" OnSelectedIndexChanged="ddlDepartamento_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList>
                                                </td>
                                                <td style="width: 33%" runat="server" id="trAssunto">
                                                    <b>Assunto:</b><br />
                                                    <asp:DropDownList ID="ddlAssunto" runat="server" Enabled="false" OnSelectedIndexChanged="ddlAssunto_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList>
                                                </td>
                                                <td style="width: 33%" runat="server" id="trTarefa">
                                                    <b>Tarefa:</b><br />
                                                    <asp:DropDownList ID="ddlTarefa" runat="server" Enabled="false" OnSelectedIndexChanged="ddlTarefa_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList>
                                                </td>
                                                <td style="width: *%">&nbsp;</td>
                                            </tr>
                                        </table>

                                    </td>
                                </tr>
                                <tr>
                                    <td class="rotulos">
                                        <table width="100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td>
                                                    <b>Usuário Responsável:<br />
                                                        <asp:DropDownList ID="ddlUsuarioDepartamento" runat="server"></asp:DropDownList>
                                                </td>
                                                <td>
                                                    <b>Prioridade:</b><br />
                                                    <asp:DropDownList ID="ddlPrioridade" runat="server">
                                                        <asp:ListItem Text="Muito Baixo" Value="1"></asp:ListItem>
                                                        <asp:ListItem Text="Baixo" Value="2"></asp:ListItem>
                                                        <asp:ListItem Text="Normal" Value="3"></asp:ListItem>
                                                        <asp:ListItem Text="Alta" Value="4"></asp:ListItem>
                                                        <asp:ListItem Text="Urgente" Value="5"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                        </table>

                                    </td>
                                </tr>
                                <tr class="rotulos">
                                    <td>Título
                                    </td>
                                </tr>
                                <tr class="rotulos">
                                    <td>
                                        <asp:TextBox runat="server" ID="txtChamadoTitulo" Width="50%" ReadOnly="True"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr class="rotulos">
                                    <td class="rotulos">
                                        <table>
                                            <tr>
                                                <td>Data</td>
                                                <td>Hora</td>
                                                <%--<td>Responsável</td>--%>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="txtChamadoData" runat="server" Width="100px"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="txtChamadoHora" runat="server" Width="50px"></asp:Label>
                                                </td>
                                                <%--                                                <td class="rotulos">--%>
                                                <asp:DropDownList runat="server" Visible="false" DataValueField="usuarioId" DataTextField="usuarioNome" ID="ddlChamadoUsuario" />
                                                <%--</td>--%>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="rotulos">Descrição
                                    </td>
                                </tr>
                                <tr>
                                    <td class="rotulos">
                                        <ce:editor id="txtChamado" runat="server"
                                            autoconfigure="Minimal" contextmenumode="Simple"
                                            editorwysiwygmodecss="" filespath="" height="200px"
                                            urltype="Default" userelativelinks="False"
                                            width="782px"
                                            removeservernamesfromurl="False">
                                    <TextAreaStyle CssClass="campos" />
                                  <%--  <FrameStyle BackColor="White" BorderColor="#DDDDDD" BorderStyle="Solid" 
                                        BorderWidth="1px" CssClass="CuteEditorFrame" Height="100%" Width="100%" />--%>
                                </ce:editor>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="rotulos" colspan="2" style="text-align: right">
                                        <asp:Button runat="server" ID="btnGravarChamado" OnClientClick="javascript: return validaChamado();" OnClick="btnGravarChamado_OnClick" Text="Gravar" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div style="overflow: scroll; height: 200px;">
                                            <asp:DataList ID="dtlChamadoInteracoes" runat="server" CellPadding="0" DataSourceID="sqlChamadoInteracao">
                                                <ItemTemplate>
                                                    <table cellpadding="0" cellspacing="0" class="rotulos" style="width: 782px">
                                                        <tr>
                                                            <td height="25" style="padding-left: 20px; width: 140px; border-top-style: solid; border-top-width: 1px; border-top-color: #7EACB1;">
                                                                <asp:Label ID="lblData" runat="server" Text='<%# Bind("dataHora") %>'></asp:Label>
                                                            </td>
                                                            <td style="width: 200px; border-top-style: solid; border-top-width: 1px; border-top-color: #7EACB1;">
                                                                <asp:Label ID="lblusuario" runat="server" Text='<%# Bind("usuarioNome") %>'></asp:Label>
                                                            </td>
                                                            <td style="border-top-style: solid; border-width: 1px; border-color: #7EACB1">
                                                                <asp:Literal ID="ltrTitulo" runat="server" Text='<%# Bind("interacao") %>'></asp:Literal>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ItemTemplate>
                                                <HeaderTemplate>
                                                    <table cellpadding="0" cellspacing="0" class="rotulos" style="width: 782px">
                                                        <tr bgcolor="#D4E3E6">
                                                            <td height="25" style="padding-left: 20px; width: 140px;">
                                                                <b>Data:</b>
                                                            </td>
                                                            <td style="width: 200px">
                                                                <b>Responsável:</b>
                                                            </td>
                                                            <td>
                                                                <b>Chamado:</b>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </HeaderTemplate>
                                            </asp:DataList>
                                        </div>
                                        <asp:SqlDataSource ID="sqlChamadoInteracao" runat="server" ConnectionString="<%$ ConnectionStrings:connectionString %>"
                                            SelectCommand="SELECT tbChamadoInteracao.dataHora, tbChamadoInteracao.interacao, tbUsuario.usuarioNome
                                    FROM [tbChamadoInteracao] join [tbUsuario] on [tbChamadoInteracao].idUsuario = [tbUsuario].usuarioId
                                    WHERE ([idChamado] = @idChamado) order by tbChamadoInteracao.dataHora DESC">
                                            <SelectParameters>
                                                <asp:Parameter Name="idChamado" DefaultValue="0" Type="Int32" />
                                            </SelectParameters>
                                        </asp:SqlDataSource>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </dx:PopupControlContentControl>
                </ContentCollection>
                <ContentStyle>
                    <Paddings PaddingBottom="5px" />
                </ContentStyle>
            </dx:ASPxPopupControl>

            <table cellpadding="0" cellspacing="0" style="width: 920px">
                <tr>
                    <td class="tituloPaginas" valign="top">
                        <asp:Label ID="lblAcao" runat="server">Chamados e Agendamentos</asp:Label>
                    </td>
                </tr>

                <tr>
                    <td>

                        <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="rotulos" style="padding-bottom: 10px; padding-left: 10px;">Filtrar:
                <asp:RadioButton runat="server" ID="rdbMeus" Text="Meus Chamados" Checked="false" GroupName="filtroEntregue" AutoPostBack="True" OnCheckedChanged="rdb_OnCheckedChanged" />
                                    - 
                <asp:RadioButton runat="server" ID="rdbTodos" Text="Chamados do departamento" Checked="true" GroupName="filtroEntregue" AutoPostBack="True" OnCheckedChanged="rdb_OnCheckedChanged" />
                                </td>
                            </tr>
                            <tr>
                                <td class="rotulos" style="padding-bottom: 10px; padding-left: 10px;">Status:
                                    <asp:RadioButton runat="server" ID="rdbAbertos" Text="Chamados em aberto" Checked="True" GroupName="filtroChamado" AutoPostBack="True" OnCheckedChanged="rdb_OnCheckedChanged" />
                                    - 
                                    <asp:RadioButton runat="server" ID="rdbFinalizados" Text="Chamados finalizados" Checked="False" GroupName="filtroChamado" AutoPostBack="True" OnCheckedChanged="rdb_OnCheckedChanged" /><br />
                                    <br />
                                    <b>Seus departamentos:</b><br />
                                    <asp:Label ID="lblSeusDepartamentos" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="rotulos" style="padding-bottom: 10px; padding-left: 10px;"><b>Legenda:</b><br />
                                    <table>
                                        <tr>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td style="background-color: #ff0000; width: 15px"></td>
                                                        <td>Crise</td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td style="background-color: #ffcc99; width: 15px"></td>
                                                        <td>Cancelamento</td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td style="background-color: #0099e6; width: 15px"></td>
                                                        <td>Pós Vendas</td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td>
                                                <table>
                                                    <tr>
                                                        <td style="background-color: #C39EBA; width: 15px"></td>
                                                        <td>Reclame Aqui</td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <tr>
                                    <td></td>
                                </tr>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <dxwgv:ASPxGridView ID="grd" ClientInstanceName="grd" runat="server" AutoGenerateColumns="False"
                            KeyFieldName="idChamado" Width="834px" Settings-ShowFilterRow="True" Settings-ShowFilterBar="Visible"
                            Cursor="auto" OnHtmlRowCreated="grd_HtmlRowCreated" EnableViewState="False">
                            <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                            <Styles>
                                <Footer Font-Bold="True">
                                </Footer>
                            </Styles>
                            <SettingsText ConfirmDelete="Tem certeza que deseja excluir?" EmptyDataRow="Nenhum registro encontrado." />
                            <SettingsPager Position="TopAndBottom" PageSize="500"
                                ShowDisabledButtons="False" AlwaysShowPager="True">
                                <Summary AllPagesText="Páginas: {0} - {1} ({2} items)"
                                    Text="Página {0} de {1} ({2} registros encontrados)" />
                            </SettingsPager>
                            <Settings ShowFilterRow="True" ShowGroupButtons="False" ShowFooter="True" />
                            <SettingsEditing EditFormColumnCount="4"
                                PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1"
                                PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter"
                                PopupEditFormWidth="700px" />
                            <Columns>
                                <dxwgv:GridViewDataTextColumn Caption="Departamento Origem" FieldName="idDeparetamentoAbertura" Visible="false" VisibleIndex="0">
                                    <EditFormSettings Visible="False" />
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataTextColumn Caption="Id" FieldName="idPedido" VisibleIndex="0">
                                    <Settings AutoFilterCondition="Contains" />
                                    <EditFormSettings Visible="False" />
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataTextColumn Caption="Prioridade" Name="prioridade" FieldName="prioridade" VisibleIndex="0">
                                    <DataItemTemplate>
                                        <asp:Label ID="lblPrioridade" runat="server"></asp:Label>
                                    </DataItemTemplate>
                                    <EditFormSettings Visible="False" />
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataTextColumn Caption="Data" FieldName="dataLimite" Name="dataLimite" VisibleIndex="0">
                                    <Settings AutoFilterCondition="Contains" />
                                    <DataItemTemplate>
                                        <asp:Label ID="hdfConcluido" runat="server" Text='<%# Bind("concluido") %>' Visible="False"></asp:Label>
                                        <asp:Label ID="hdfPedido" runat="server" Text='<%# Bind("idPedido") %>' Visible="False"></asp:Label>
                                        <asp:Label ID="lblData" runat="server" Text='<%# Bind("dataLimite") %>'></asp:Label>
                                    </DataItemTemplate>
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataTextColumn Caption="Usuário" FieldName="usuarioNome" VisibleIndex="0">
                                    <Settings AutoFilterCondition="Contains" />
                                    <EditFormSettings Visible="False" />
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataTextColumn Caption="Título" FieldName="titulo" VisibleIndex="0">
                                    <Settings AutoFilterCondition="Contains" />
                                    <DataItemTemplate>
                                        <asp:Literal ID="ltrTitulo" runat="server" Text='<%# Bind("titulo") %>'></asp:Literal>
                                    </DataItemTemplate>
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataTextColumn Caption="Ver Pedido" Name="verPedido" VisibleIndex="0">
                                    <DataItemTemplate>
                                        <asp:HyperLink runat="server" ID="hplPedido" NavigateUrl='pedido.aspx?idPedido=<%# Eval("idPedido") %>' Target="_blank">
                                        Ver<br />Pedido
                                        </asp:HyperLink>
                                    </DataItemTemplate>
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataTextColumn Caption="Alterar" Name="alterar" VisibleIndex="0">
                                    <DataItemTemplate>
                                        <asp:ImageButton ID="btnAlterarChamado" CommandArgument='<%# Eval("idChamado") %>'
                                            OnCommand="btnAlterarChamado_OnCommand" runat="server" ImageAlign="AbsMiddle" ImageUrl="images/btAlterar.jpg" />
                                    </DataItemTemplate>
                                </dxwgv:GridViewDataTextColumn>
                            </Columns>
                            <StylesEditors>
                                <Label Font-Bold="True">
                                </Label>
                            </StylesEditors>
                        </dxwgv:ASPxGridView>
                    </td>
                </tr>
                <tr class="rotulos">
                    <td style="padding-top: 30px; font-size: 20px;">&nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="text-align: right; padding-top: 20px;">
                        <asp:Button runat="server" ID="btnAdicionarChamado" Visible="false" Text="Adicionar Chamado" OnClick="btnAdicionarChamado_OnClick" />
                    </td>
                </tr>
            </table>
            </td>
                </tr>
            </table>
            <asp:LinqDataSource ID="sqlUsuario" runat="server"
                ContextTypeName="dbCommerceDataContext" EnableDelete="True"
                EnableInsert="True" EnableUpdate="True" TableName="tbUsuarios">
            </asp:LinqDataSource>
        </ContentTemplate>
    </asp:UpdatePanel>


</asp:Content>
