﻿<%@ Page Title="" Language="C#" Theme="Glass" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="retornoSispag.aspx.cs" Inherits="admin_retornoSispag" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxTimer" TagPrefix="dxt" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" Runat="Server">
    <table cellpadding="0" cellspacing="0" style="width: 920px">
        <tr>
            <td class="tituloPaginas" valign="top">
                <asp:Label ID="lblAcao" runat="server">Enviar arquivo Sispag</asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                        
                    <asp:Panel runat="server" ID="pnAtualizar">
                        <tr class="rotulos">
                            <td>
                                Arquivo:
                            </td>
                        </tr>
                        <tr class="rotulos">
                            <td>
                                <asp:FileUpload ID="fluArquivo" runat="server" />
                            </td>
                        </tr>
                        <tr class="rotulos">
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr class="rotulos">
                            <td>
                                <asp:ImageButton ID="btnSalvar" runat="server" ImageAlign="AbsMiddle" ImageUrl="images/btSalvarPeq1.jpg" onclick="btnSalvar_Click" />
                            </td>
                        </tr>
                    </asp:Panel>
                    <asp:Panel runat="server" ID="pnStatus" Visible="False">
                        <tr>
                            <td>
                                Arquivo de Retorno processado com sucesso!<br />
                                <asp:Literal runat="server" id="litBoletos"></asp:Literal>
                            </td>
                        </tr>
                    </asp:Panel>  
                </table>
            </td>
        </tr>
    </table>
</asp:Content>