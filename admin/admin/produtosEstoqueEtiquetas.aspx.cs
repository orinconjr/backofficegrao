﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Serialization;
using DevExpress.Web.ASPxGridView;

public partial class admin_produtosEstoqueEtiquetas : System.Web.UI.Page
{

    private static int produtoId;
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Request.QueryString["etiqueta"] != null)
        {
            fillGrid(Convert.ToInt32(Request.QueryString["etiqueta"]), 0);
            txtEtiqueta.Focus();
        }
        else if (Request.QueryString["pedido"] != null)
        {
            fillGrid(0, Convert.ToInt32(Request.QueryString["pedido"]));
            txtEtiqueta.Focus();
        }
        else
        {
            fillGrid();
        }
    }

    private void fillGrid()
    {
        fillGrid(0, 0);
    }

    private void fillGrid(int etiqueta, int pedido)
    {
        var data = new dbCommerceDataContext();
        if(etiqueta > 0)
        {
            var estoque = (from c in data.tbProdutoEstoques
                           orderby c.enviado, c.dataEntrada
                           where c.idPedidoFornecedorItem == etiqueta
                           select new
                           {
                               c.produtoId,
                               c.pedidoId,
                               c.itemPedidoId,
                               enviado = c.enviado ? "Sim" : "Não",
                               c.idPedidoFornecedorItem,
                               c.tbProduto.produtoNome,
                               c.tbProduto.produtoIdDaEmpresa,
                               c.tbProduto.complementoIdDaEmpresa,
                               c.tbProduto.tbProdutoFornecedor.fornecedorNome,
                               c.dataEntrada,
                               c.dataEnvio,
                               c.tbPedidoFornecedorItem.idPedidoFornecedor,
                               c.tbPedidoFornecedorItem.tbPedidoFornecedor.dataLimite,
                               area = c.idEnderecamentoArea == null ? "" : c.tbEnderecamentoArea.area,
                               rua = c.idEnderecamentoRua == null ? "" : c.tbEnderecamentoRua.rua,
                               predio = c.idEnderecamentoPredio == null ? "" : c.tbEnderecamentoPredio.predio,
                               lado = c.idEnderecamentoPredio == null ? "" : c.tbEnderecamentoPredio.tbEnderecamentoLado.lado,
                               andar = c.idEnderecamentoAndar == null ? "" : c.tbEnderecamentoAndar.andar
                           });
            int produtoId = 0;
            if (Request.QueryString["produtoId"] != null)
            {
                int.TryParse(Request.QueryString["produtoId"], out produtoId);
                estoque =
                    estoque.OrderBy(x => x.produtoId != produtoId).ThenBy(x => x.enviado).ThenBy(x => x.dataEntrada);
            }
            grd.DataSource = estoque;
            if (!Page.IsPostBack && !Page.IsCallback) grd.DataBind();
            
        }
        else if(pedido > 0)
        {
            var estoque = (from c in data.tbProdutoEstoques
                           orderby c.enviado, c.dataEntrada
                           where c.pedidoId == pedido
                           select new
                           {
                               c.produtoId,
                               c.pedidoId,
                               c.itemPedidoId,
                               enviado = c.enviado ? "Sim" : "Não",
                               c.idPedidoFornecedorItem,
                               c.tbProduto.produtoNome,
                               c.tbProduto.produtoIdDaEmpresa,
                               c.tbProduto.complementoIdDaEmpresa,
                               c.tbProduto.tbProdutoFornecedor.fornecedorNome,
                               c.dataEntrada,
                               c.dataEnvio,
                               c.tbPedidoFornecedorItem.idPedidoFornecedor,
                               c.tbPedidoFornecedorItem.tbPedidoFornecedor.dataLimite,
                               area = c.idEnderecamentoArea == null ? "" : c.tbEnderecamentoArea.area,
                               rua = c.idEnderecamentoRua == null ? "" : c.tbEnderecamentoRua.rua,
                               predio = c.idEnderecamentoPredio == null ? "" : c.tbEnderecamentoPredio.predio,
                               lado = c.idEnderecamentoPredio == null ? "" : c.tbEnderecamentoPredio.tbEnderecamentoLado.lado,
                               andar = c.idEnderecamentoAndar == null ? "" : c.tbEnderecamentoAndar.andar
                           });
            grd.DataSource = estoque;
            if (!Page.IsPostBack && !Page.IsCallback) grd.DataBind();
        }
        else
        {
            var estoque = (from c in data.tbProdutoEstoques
                           orderby c.enviado, c.dataEntrada
                           where c.enviado == false
                           select new
                           {
                               c.produtoId,
                               c.pedidoId,
                               c.itemPedidoId,
                               enviado = c.enviado ? "Sim" : "Não",
                               c.idPedidoFornecedorItem,
                               c.tbProduto.produtoNome,
                               c.tbProduto.produtoIdDaEmpresa,
                               c.tbProduto.complementoIdDaEmpresa,
                               c.tbProduto.tbProdutoFornecedor.fornecedorNome,
                               c.dataEntrada,
                               c.dataEnvio,
                               c.tbPedidoFornecedorItem.idPedidoFornecedor,
                               c.tbPedidoFornecedorItem.tbPedidoFornecedor.dataLimite,
                               area = c.idEnderecamentoArea == null ? "" : c.tbEnderecamentoArea.area,
                               rua = c.idEnderecamentoRua == null ? "" : c.tbEnderecamentoRua.rua,
                               predio = c.idEnderecamentoPredio == null ? "" : c.tbEnderecamentoPredio.predio,
                               lado = c.idEnderecamentoPredio == null ? "" : c.tbEnderecamentoPredio.tbEnderecamentoLado.lado,
                               andar = c.idEnderecamentoAndar == null ? "" : c.tbEnderecamentoAndar.andar
                           });
            int produtoId = 0;
            if (Request.QueryString["produtoId"] != null)
            {
                int.TryParse(Request.QueryString["produtoId"], out produtoId);
                estoque =
                    estoque.OrderBy(x => x.produtoId != produtoId).ThenBy(x => x.enviado).ThenBy(x => x.dataEntrada);
            }
            grd.DataSource = estoque;
            if (!Page.IsPostBack && !Page.IsCallback) grd.DataBind();
        }
    }

    protected void grdDetalhes_BeforePerformDataSelect(object sender, EventArgs e)
    {
        Session["produtoId"] = (sender as ASPxGridView).GetMasterRowKeyValue();
        produtoId = Convert.ToInt32((sender as ASPxGridView).GetMasterRowKeyValue());
    }

    protected void grd_OnCustomUnboundColumnData(object sender, ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "pedidoIdCliente")
        {
            int pedidoId = Convert.ToInt32(e.GetListSourceFieldValue("pedidoId"));
            e.Value = rnFuncoes.retornaIdCliente(pedidoId);
        }
        if (e.Column.FieldName == "idPedidoFornecedor")
        {
            int pedidoId = Convert.ToInt32(e.GetListSourceFieldValue("pedidoId"));
            int produtoId = Convert.ToInt32(Session["produtoId"]);
            var pedidoDc = new dbCommerceDataContext();
            var pedidoFornecedor =
                (from c in pedidoDc.tbPedidoFornecedorItems
                 where c.idPedido == pedidoId && c.idProduto == produtoId
                 select c).FirstOrDefault();
            if (pedidoFornecedor != null)
            {
                e.Value = pedidoFornecedor.idPedidoFornecedor;
                if (!pedidoFornecedor.entregue && (pedidoFornecedor.tbPedidoFornecedor.dataLimite.Date <= DateTime.Now.Date))
                {
                    e.Value = pedidoFornecedor.idPedidoFornecedor + " - Atrasado";
                }
                if (pedidoFornecedor.entregue)
                {
                    e.Value = pedidoFornecedor.idPedidoFornecedor + " - Entregue";
                }
                if (pedidoFornecedor.tbPedidoFornecedor.confirmado == null | pedidoFornecedor.tbPedidoFornecedor.confirmado == false)
                {
                    e.Value = pedidoFornecedor.idPedidoFornecedor + " - Não confirmado";
                }
            }
        }
        if (e.Column.FieldName == "reserva")
        {
            int pedidoId = Convert.ToInt32(e.GetListSourceFieldValue("pedidoId"));
            int produtoId = Convert.ToInt32(Session["produtoId"]);
            var pedidoDc = new dbCommerceDataContext();
            var reserva =
                (from c in pedidoDc.tbProdutoReservaEstoques
                 where c.idPedido == pedidoId && c.idProduto == produtoId
                 select c).FirstOrDefault();
            if (reserva != null)
            {
                e.Value = reserva.idProdutoReservaEstoque;
            }

        }
    }

    protected void grd1_OnCustomUnboundColumnData(object sender, ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "ultimoPedido")
        {
            int produtoId = Convert.ToInt32(e.GetListSourceFieldValue("produtoId"));
            if (rnProdutos.dataUltimoPedidoDoProduto(produtoId).Tables[0].Rows.Count > 0)
            {
                e.Value = rnProdutos.dataUltimoPedidoDoProduto(produtoId).Tables[0].Rows[0]["dataDaCriacao"];
            }
            else
            {
                e.Value = "";
            }
        }
        if (e.Column.FieldName == "ultimaCompra")
        {
            int produtoId = Convert.ToInt32(e.GetListSourceFieldValue("produtoId"));
            if (rnProdutos.dataUltimaCompraDoProduto(produtoId).Tables[0].Rows.Count > 0)
            {
                e.Value = rnProdutos.dataUltimaCompraDoProduto(produtoId).Tables[0].Rows[0]["dataPedido"];
            }
            else
            {
                e.Value = "";
            }
        }
        if (e.Column.FieldName == "diasEstocado")
        {
            bool possuiValores = true;
            int produtoId = Convert.ToInt32(e.GetListSourceFieldValue("produtoId"));
            if (rnProdutos.dataUltimoPedidoDoProduto(produtoId).Tables[0].Rows.Count > 0)
            {
                try
                {
                    var dataPedido = Convert.ToDateTime(rnProdutos.dataUltimoPedidoDoProduto(produtoId).Tables[0].Rows[0]["dataDaCriacao"]);
                    if (rnProdutos.dataUltimaCompraDoProduto(produtoId).Tables[0].Rows.Count > 0)
                    {
                        try
                        {
                            var dataCompra = Convert.ToDateTime(rnProdutos.dataUltimaCompraDoProduto(produtoId).Tables[0].Rows[0]["dataPedido"]);
                            if (dataCompra > dataPedido)
                            {
                                e.Value = (DateTime.Now - dataCompra).Days;
                            }
                            else
                            {
                                e.Value = (DateTime.Now - dataPedido).Days;
                            }
                        }
                        catch (Exception)
                        {
                            possuiValores = false;
                        }
                    }
                }
                catch (Exception)
                {
                    possuiValores = false;
                }
            }
        }
    }






    #region Adicionar Produto ao Estoque

    public class pedidoFornecedorTemporario
    {
        public int pedidoId { get; set; }
        public int produtoId { get; set; }
        public int itemId { get; set; }
        public int fornecedorId { get; set; }
        public string produtoIdDaEmpresa { get; set; }
        public string complementoIdDaEmpresa { get; set; }
        public string produtoNome { get; set; }
        public int quantidade { get; set; }
        public string motivo { get; set; }
    }

    protected void btnAdicionar_OnClick(object sender, EventArgs e)
    {
        
    }

    private void fillGridAdicionarProduto()
    {
        var lista = new List<pedidoFornecedorTemporario>();
        if (!string.IsNullOrEmpty(hdfLista.Text))
        {
            lista = Deserialize(hdfLista.Text);
        }
        //if (Session["pedidoFornecedorTemporario"] == null)
        //{
        //    Session["pedidoFornecedorTemporario"] = new List<pedidoFornecedorTemporario>();
        //}
        //var lista = (List<pedidoFornecedorTemporario>)Session["pedidoFornecedorTemporario"];
        lstProdutos.DataSource = lista;
        lstProdutos.DataBind();
    }
    private void adicionaItemListaTemporaria(pedidoFornecedorTemporario item)
    {
        var lista = new List<pedidoFornecedorTemporario>();
        if (!string.IsNullOrEmpty(hdfLista.Text))
        {
            lista = Deserialize(hdfLista.Text);
        }
        //if (Session["pedidoFornecedorTemporario"] == null)
        //{
        //    Session["pedidoFornecedorTemporario"] = new List<pedidoFornecedorTemporario>();
        //}
        //var lista = (List<pedidoFornecedorTemporario>)Session["pedidoFornecedorTemporario"];
        lista.Add(item);
        //Session["pedidoFornecedorTemporario"] = lista;
        hdfLista.Text = Serialize(lista);
        fillGridAdicionarProduto();
    }

    protected void lstProdutos_OnItemDataBound(object sender, ListViewItemEventArgs e)
    {

    }

    protected void btnGravar_OnClick(object sender, EventArgs e)
    {
        var listaIds = new List<int>();
        HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
        usuarioLogadoId = Request.Cookies["usuarioLogadoId"];
        if (usuarioLogadoId == null)
        {
            return;
        }

        var lista = new List<pedidoFornecedorTemporario>();
        if (!string.IsNullOrEmpty(hdfLista.Text))
        {
            lista = Deserialize(hdfLista.Text);
        }
        if (!lista.Any())
        {
            Response.Write("<script>alert('Favor adicionar algum produto à lista.');</script>");
            return;
        }
        //if (Session["pedidoFornecedorTemporario"] == null)
        //{
        //    Session["pedidoFornecedorTemporario"] = new List<pedidoFornecedorTemporario>();
        //}
        //var lista = (List<pedidoFornecedorTemporario>)Session["pedidoFornecedorTemporario"];

        var listaFornecedores = new List<int>();
        bool pedidoEstoque = true;
        if (pedidoEstoque)
        {
            listaFornecedores.Add(71);
        }
        else
        {
            listaFornecedores = lista.Select(x => x.fornecedorId).Distinct().ToList();
        }
        foreach (var listaFornecedor in listaFornecedores)
        {
            int fornecedorId = listaFornecedor;
            var fornecedorDc = new dbCommerceDataContext();
            var fornecedor = (from c in fornecedorDc.tbProdutoFornecedors where c.fornecedorId == fornecedorId select c).First();
            var pedidosDc = new dbCommerceDataContext();
            var pedidoFornecedorCheck = (from c in pedidosDc.tbPedidoFornecedors where c.idFornecedor == fornecedorId && c.pendente == true select c).FirstOrDefault();
            var pedidoFornecedor = pedidoFornecedorCheck == null ? new tbPedidoFornecedor() : pedidoFornecedorCheck;
            if (pedidoEstoque) pedidoFornecedor = new tbPedidoFornecedor();
            pedidoFornecedor.data = DateTime.Now;
            pedidoFornecedor.idFornecedor = fornecedorId;
            pedidoFornecedor.confirmado = false;
            pedidoFornecedor.avulso = true;
            pedidoFornecedor.pendente = true;
            if (pedidoEstoque) pedidoFornecedor.pendente = false;
            if (fornecedor.fornecedorPrazoPedidos > 0)
            {
                int diasPrazo = Convert.ToInt32(fornecedor.fornecedorPrazoPedidos);
                if ((fornecedor.prazoFornecedorDiasUteis ?? false) == true)
                {
                    int diasPrazoFinal = rnFuncoes.retornaPrazoDiasUteis(diasPrazo, 0);
                    diasPrazo = diasPrazoFinal;
                }
                pedidoFornecedor.dataLimite = DateTime.Now.AddDays(diasPrazo);
            }
            else
            {
                pedidoFornecedor.dataLimite = DateTime.Now.AddDays(1);
            }
            pedidoFornecedor.usuario = "";
            if (usuarioLogadoId != null)
            {
                pedidoFornecedor.usuario = rnUsuarios.usuarioSeleciona_PorUsuarioId(int.Parse(usuarioLogadoId.Value.ToString())).Tables[0].Rows[0]["usuarioNome"].ToString();
            }
            if (pedidoFornecedorCheck == null | pedidoEstoque) pedidosDc.tbPedidoFornecedors.InsertOnSubmit(pedidoFornecedor);
            pedidosDc.SubmitChanges();

            int idPedidoFornecedor = pedidoFornecedor.idPedidoFornecedor;

            foreach (var pedido in lista)
            {
                int totalItens = 0;
                var itensPedidoDc = new dbCommerceDataContext();
                totalItens++;
                var produtosDc = new dbCommerceDataContext();
                var produto = (from c in produtosDc.tbProdutos where c.produtoId == pedido.produtoId select c).First();
                if (produto.produtoFornecedor == fornecedorId | pedidoEstoque)
                {
                    var pedidoFornecedorItem = new tbPedidoFornecedorItem();
                    pedidoFornecedorItem.custo = (decimal)produto.produtoPrecoDeCusto;
                    pedidoFornecedorItem.brinde = false;
                    pedidoFornecedorItem.diferenca = 0;
                    pedidoFornecedorItem.idPedidoFornecedor = idPedidoFornecedor;
                    pedidoFornecedorItem.idProduto = produto.produtoId;
                    pedidoFornecedorItem.entregue = false;
                    pedidoFornecedorItem.quantidade = Convert.ToInt32(pedido.quantidade);
                    pedidoFornecedorItem.motivo = "";
                    if (pedido.itemId > 0) pedidoFornecedorItem.idItemPedido = pedido.itemId;
                    if (pedido.pedidoId > 0) pedidoFornecedorItem.idPedido = pedido.pedidoId;
                    pedidosDc.tbPedidoFornecedorItems.InsertOnSubmit(pedidoFornecedorItem);
                    pedidosDc.SubmitChanges();
                }
            }

            listaIds.Add(pedidoFornecedor.idPedidoFornecedor);

            if (pedidoEstoque)
            {
                Session["pedidoFornecedorTemporario"] = null;
                Response.Redirect("pedidoFornecedorEditar.aspx?idPedidoFornecedor=" + idPedidoFornecedor);
            }
        }
        Session["pedidoFornecedorTemporario"] = null;
        Response.Redirect("pedidoFornecedorAvulso.aspx");
    }


    private class fornecedorCusto
    {
        public decimal custo { get; set; }
        public int produtoId { get; set; }
    }

    protected void btnRemover_OnCommand(object sender, CommandEventArgs e)
    {
        var lista = new List<pedidoFornecedorTemporario>();
        if (!string.IsNullOrEmpty(hdfLista.Text))
        {
            lista = Deserialize(hdfLista.Text);
        }
        //if (Session["pedidoFornecedorTemporario"] == null)
        //{
        //    Session["pedidoFornecedorTemporario"] = new List<pedidoFornecedorTemporario>();
        //}
        //var lista = (List<pedidoFornecedorTemporario>)Session["pedidoFornecedorTemporario"];
        try
        {
            lista.Remove(lista[Convert.ToInt32(e.CommandArgument)]);
            hdfLista.Text = Serialize(lista);
        }
        catch (Exception)
        {

        }
        fillGridAdicionarProduto();
    }

    public static string Serialize(List<pedidoFornecedorTemporario> tData)
    {
        var serializer = new XmlSerializer(typeof(List<pedidoFornecedorTemporario>));

        TextWriter writer = new StringWriter();
        serializer.Serialize(writer, tData);

        return writer.ToString();
    }

    public static List<pedidoFornecedorTemporario> Deserialize(string tData)
    {
        var serializer = new XmlSerializer(typeof(List<pedidoFornecedorTemporario>));

        TextReader reader = new StringReader(tData);

        return (List<pedidoFornecedorTemporario>)serializer.Deserialize(reader);
    } 
    #endregion

    protected void btnAdicionarEstoque_OnCommand(object sender, CommandEventArgs e)
    {
        var pedidoDc = new dbCommerceDataContext();
        int pedidoId = 0;
        int itemId = 0;
        int quantidade = 1;
        if (quantidade == 0) quantidade = 1;

            var produtosDc = new dbCommerceDataContext();
            int produtoId = 0;
            int.TryParse(e.CommandArgument.ToString(), out produtoId);

            var produto = (from c in produtosDc.tbProdutos where c.produtoId == produtoId select c);
            var produtoItem = produto.First();
            var produtosFilho =
                        (from c in produtosDc.tbProdutoRelacionados
                            where c.idProdutoPai == produtoItem.produtoId
                            select c);
            if (produtosFilho.Any())
            {
                foreach (var produtoRelacionado in produtosFilho)
                {
                    var produtoRel =
                        (from c in produtosDc.tbProdutos
                            where c.produtoId == produtoRelacionado.idProdutoFilho
                            select c).First();
                    for (int i = 1; i <= quantidade; i++)
                    {
                        var item = new pedidoFornecedorTemporario();
                        item.produtoId = produtoRelacionado.idProdutoFilho;
                        item.fornecedorId = produtoRel.produtoFornecedor;
                        item.produtoIdDaEmpresa = produtoRel.produtoIdDaEmpresa;
                        item.complementoIdDaEmpresa = produtoRel.complementoIdDaEmpresa;
                        item.produtoNome = produtoRel.produtoNome;
                        item.quantidade = 1;
                        item.pedidoId = pedidoId;
                        item.motivo = "";
                        item.itemId = itemId;
                        adicionaItemListaTemporaria(item);
                    }
                }
            }
            else
            {
                for (int i = 1; i <= quantidade; i++)
                {
                    var item = new pedidoFornecedorTemporario();
                    item.produtoId = produtoItem.produtoId;
                    item.fornecedorId = produtoItem.produtoFornecedor;
                    item.produtoIdDaEmpresa = produtoItem.produtoIdDaEmpresa;
                    item.complementoIdDaEmpresa = produtoItem.complementoIdDaEmpresa;
                    item.produtoNome = produtoItem.produtoNome;
                    item.quantidade = 1;
                    item.pedidoId = pedidoId;
                    item.motivo = "";
                    item.itemId = itemId;
                    adicionaItemListaTemporaria(item);
                }
            }
    }

    protected void btnCarregarEtiqueta_OnClick(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(txtEtiqueta.Text))
        {
            Response.Redirect("produtosEstoqueEtiquetas.aspx?etiqueta=" + txtEtiqueta.Text);
        }
        if (!string.IsNullOrEmpty(txtPedido.Text))
        {
            Response.Redirect("produtosEstoqueEtiquetas.aspx?pedido=" + txtPedido.Text);
        }
    }
}