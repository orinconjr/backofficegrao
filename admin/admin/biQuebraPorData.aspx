﻿<%@ Page Title="" Language="C#" Theme="Glass" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="biQuebraPorData.aspx.cs" Inherits="admin_biQuebraPorData" %>


<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" runat="Server">
    <table cellpadding="0" cellspacing="0" style="width: 920px">
        <tr>
            <td class="tituloPaginas" valign="top">
                <asp:Label ID="lblAcao" runat="server">Vendas por período</asp:Label>
            </td>
        </tr>
        <tr>
            <td>

                <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
                    <tr>
                        <td align="right">
                            <table cellpadding="0" cellspacing="0" style="width: 834px">
                                <tr>
                                    <td align="right">&nbsp;</td>
                                    <td height="38"
                                        style="background-image: url('images/bkExportar.jpg'); padding-bottom: 1px;"
                                        valign="bottom" width="231">
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 97px">&nbsp;</td>
                                                <td style="width: 33px">
                                                    <asp:ImageButton ID="btPdf" runat="server" ImageUrl="~/admin/images/btPdf.jpg"
                                                        Style="margin-left: 5px" OnClick="btPdf_Click" />
                                                </td>
                                                <td style="width: 31px">
                                                    <asp:ImageButton ID="btXsl" runat="server" ImageUrl="~/admin/images/btXls.jpg"
                                                        Style="margin-left: 5px" OnClick="btXsl_Click" />
                                                </td>
                                                <td style="width: 36px">
                                                    <asp:ImageButton ID="btRtf" runat="server" ImageUrl="~/admin/images/btRtf.jpg"
                                                        Style="margin-left: 5px" OnClick="btRtf_Click" />
                                                </td>
                                                <td valign="bottom">
                                                    <asp:ImageButton ID="btCsv" runat="server" ImageUrl="~/admin/images/btCsv.jpg"
                                                        Style="margin-left: 5px" OnClick="btCsv_Click" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" style="width: 834px">
                                <tr>
                                    <td class="rotulos" style="width: 100px">Data inicial<br />
                                        <asp:TextBox ID="txtDataInicial" runat="server" CssClass="campos"
                                            Width="90px" MaxLength="10"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rqvDataInicial" runat="server"
                                            ControlToValidate="txtDataInicial" Display="None"
                                            ErrorMessage="Preencha a data inicial."
                                            SetFocusOnError="True"></asp:RequiredFieldValidator>
                                        <b>
                                            <asp:RegularExpressionValidator ID="rgeDataInicial" runat="server"
                                                ControlToValidate="txtDataInicial" Display="None"
                                                ErrorMessage="Por favor, preencha corretamente a data inicial."
                                                SetFocusOnError="True"
                                                ValidationExpression="^(([0-2]\d|[3][0-1])\/([0]\d|[1][0-2])\/[1-2][0-9]\d{2})$"></asp:RegularExpressionValidator>
                                        </b>
                                    </td>
                                    <td class="rotulos" style="width: 100px">&nbsp;<br />
                                        <asp:ImageButton ID="imbInsert" runat="server"
                                            ImageUrl="~/admin/images/btPesquisar.jpg" />
                                        <asp:ValidationSummary ID="llds" runat="server" DisplayMode="List"
                                            ShowMessageBox="True" ShowSummary="False" />                                        
                                    </td>
                                    <td valign="bottom">
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <dxwgv:ASPxGridView ID="grd" runat="server" AutoGenerateColumns="False"
                                KeyFieldName="dia" Width="834px" OnProcessColumnAutoFilter="grd_OnProcessColumnAutoFilter" OnCustomUnboundColumnData="grd_OnCustomUnboundColumnData"
                                Cursor="auto" DataSourceID="sql" Settings-ShowHorizontalScrollBar="true">
                                <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />

                                <Styles>
                                    <Footer Font-Bold="True">
                                    </Footer>
                                </Styles>
                                <SettingsText ConfirmDelete="Tem certeza que deseja excluir?"
                                    EmptyDataRow="Nenhum registro encontrado." />
                                <SettingsPager Position="TopAndBottom" PageSize="100"
                                    ShowDisabledButtons="False" AlwaysShowPager="True">
                                    <Summary AllPagesText="Páginas: {0} - {1} ({2} items)"
                                        Text="Página {0} de {1} ({2} registros encontrados)" />
                                </SettingsPager>
                                <Settings ShowFilterRow="True" ShowGroupButtons="False" ShowFooter="True" />
                                <TotalSummary>

                                </TotalSummary>
                                <SettingsEditing EditFormColumnCount="4"
                                    PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1"
                                    PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter"
                                    PopupEditFormWidth="700px" Mode="Inline" />
                                <Columns>
                                    <dxwgv:GridViewDataDateColumn Caption="Dia" FieldName="dia"
                                        VisibleIndex="0">
                                    </dxwgv:GridViewDataDateColumn>                                    
                                </Columns>
                                <Columns>
                                    <dxwgv:GridViewDataTextColumn Caption="Tot. Pedidos" FieldName="totaldepedidos"
                                        VisibleIndex="0">
                                    </dxwgv:GridViewDataTextColumn>   
                                    <dxwgv:GridViewDataTextColumn Caption="Tot. Aguardando" FieldName="totaisAguardandoDia"
                                        VisibleIndex="0">
                                    </dxwgv:GridViewDataTextColumn>     
                                    <dxwgv:GridViewDataTextColumn Caption="Tot. Pago" FieldName="totaisPagosDia"
                                        VisibleIndex="0">
                                    </dxwgv:GridViewDataTextColumn>     
                                    <dxwgv:GridViewDataTextColumn Caption="Tot. Enviado" FieldName="totalEnviadosDia"
                                        VisibleIndex="0">
                                    </dxwgv:GridViewDataTextColumn>   
                                    <dxwgv:GridViewDataTextColumn Caption="Tot. Cancelado" FieldName="totalCanceladosDia"
                                        VisibleIndex="0">
                                    </dxwgv:GridViewDataTextColumn>   
                                    <dxwgv:GridViewDataTextColumn Caption="Tot. Pago/Cancelado" FieldName="totalPagosCanceladosDia"
                                        VisibleIndex="0">
                                    </dxwgv:GridViewDataTextColumn>   
                                    
                                    <dxwgv:GridViewDataTextColumn Caption="Master Total" FieldName="totaldepedidosMaster"
                                        VisibleIndex="0">
                                    </dxwgv:GridViewDataTextColumn>  
                                    <dxwgv:GridViewDataTextColumn Caption="Master Aguardando" FieldName="totaisAguardandoMaster"
                                        VisibleIndex="0">
                                    </dxwgv:GridViewDataTextColumn>   
                                    <dxwgv:GridViewDataTextColumn Caption="Master Pago" FieldName="totaisPagosMaster"
                                        VisibleIndex="0">
                                    </dxwgv:GridViewDataTextColumn> 
                                    <dxwgv:GridViewDataTextColumn Caption="Master Cancelado" FieldName="totalCanceladosMaster"
                                        VisibleIndex="0">
                                    </dxwgv:GridViewDataTextColumn>  
                                    
                                    <dxwgv:GridViewDataTextColumn Caption="Visa Total" FieldName="totaldepedidosVisa"
                                        VisibleIndex="0">
                                    </dxwgv:GridViewDataTextColumn>  
                                    <dxwgv:GridViewDataTextColumn Caption="Visa Aguardando" FieldName="totaisAguardandoVisa"
                                        VisibleIndex="0">
                                    </dxwgv:GridViewDataTextColumn>   
                                    <dxwgv:GridViewDataTextColumn Caption="Visa Pago" FieldName="totaisPagosVisa"
                                        VisibleIndex="0">
                                    </dxwgv:GridViewDataTextColumn> 
                                    <dxwgv:GridViewDataTextColumn Caption="Visa Cancelado" FieldName="totalCanceladosVisa"
                                        VisibleIndex="0">
                                    </dxwgv:GridViewDataTextColumn> 
                                    
                                    <dxwgv:GridViewDataTextColumn Caption="Boleto Total" FieldName="totaldepedidosBoleto"
                                        VisibleIndex="0">
                                    </dxwgv:GridViewDataTextColumn>  
                                    <dxwgv:GridViewDataTextColumn Caption="Boleto Aguardando" FieldName="totaisAguardandoBoleto"
                                        VisibleIndex="0">
                                    </dxwgv:GridViewDataTextColumn>   
                                    <dxwgv:GridViewDataTextColumn Caption="Boleto Pago" FieldName="totaisPagosBoleto"
                                        VisibleIndex="0">
                                    </dxwgv:GridViewDataTextColumn> 
                                    <dxwgv:GridViewDataTextColumn Caption="Boleto Cancelado" FieldName="totalCanceladosBoleto"
                                        VisibleIndex="0">
                                    </dxwgv:GridViewDataTextColumn> 
                                    
                                    <dxwgv:GridViewDataTextColumn Caption="Outros Total" FieldName="totaldepedidosOutros"
                                        VisibleIndex="0">
                                    </dxwgv:GridViewDataTextColumn>  
                                    <dxwgv:GridViewDataTextColumn Caption="Outros Aguardando" FieldName="totaisAguardandoOutros"
                                        VisibleIndex="0">
                                    </dxwgv:GridViewDataTextColumn>   
                                    <dxwgv:GridViewDataTextColumn Caption="Outros Pago" FieldName="totaisPagosOutros"
                                        VisibleIndex="0">
                                    </dxwgv:GridViewDataTextColumn> 
                                    <dxwgv:GridViewDataTextColumn Caption="Outros Cancelado" FieldName="totalCanceladosOutros"
                                        VisibleIndex="0">
                                    </dxwgv:GridViewDataTextColumn> 
                                    
                                </Columns>
                                <StylesEditors>
                                    <Label Font-Bold="True">
                                    </Label>
                                </StylesEditors>
                            </dxwgv:ASPxGridView>
                            <dxwgv:ASPxGridViewExporter ID="grdEx" runat="server" FileName="fornecedores"
                                GridViewID="grd" Landscape="True" PreserveGroupRowStates="True">
                            </dxwgv:ASPxGridViewExporter>
                            <asp:ObjectDataSource ID="sql" runat="server" SelectMethod="quebraPorData"
                                TypeName="rnRelatorios">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="txtDataInicial" Name="data" PropertyName="Text" Type="String" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>

