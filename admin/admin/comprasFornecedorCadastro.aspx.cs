﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Serialization;
using CarlosAg.ExcelXmlWriter;

public partial class admin_comprasFornecedorCadastro : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Request.QueryString["id"] != null)
            {
                hdfIdFornecedor.Value = Request.QueryString["id"].ToString();
            }
            FillForm();
        }
    }

    private void FillForm()
    {
        if (!string.IsNullOrEmpty(hdfIdFornecedor.Value))
        {
            int idComprasFornecedor = 0;
            int.TryParse(hdfIdFornecedor.Value, out idComprasFornecedor);
            var data = new dbCommerceDataContext();
            var comprasFornecedor = (from c in data.tbComprasFornecedors where c.idComprasFornecedor == idComprasFornecedor select c).FirstOrDefault();
            if (comprasFornecedor == null)
            {
                hdfIdFornecedor.Value = "";
                return;
            }
            txtFornecedor.Text = comprasFornecedor.fornecedor;
            txtContato.Text = comprasFornecedor.contato;
            txtEmail.Text = comprasFornecedor.email;
            txtTelefone.Text = comprasFornecedor.telefone;
            txtPrazoDeEntrega.Value = comprasFornecedor.prazo;
            txtfornecedorCPFCNPJ.Text = comprasFornecedor.fornecedorCPFCNPJ;
            FillGridCondicoes();
        }
    }

    private void FillGridCondicoes()
    {
        if (!string.IsNullOrEmpty(hdfIdFornecedor.Value))
        {
            int idComprasFornecedor = 0;
            int.TryParse(hdfIdFornecedor.Value, out idComprasFornecedor);

            var data = new dbCommerceDataContext();
            var condicaoDias = (from c in data.tbComprasFornecedorCondicaoPagamentos where c.idComprasFornecedor == idComprasFornecedor select c).ToList();
            grdCondicoes.DataSource = condicaoDias;
            grdCondicoes.DataBind();
        }
        else
        {
            var lista = new List<tbComprasFornecedorCondicaoPagamento>();
            if (!string.IsNullOrEmpty(hdfLista.Text))
            {
                lista = Deserialize(hdfLista.Text);
            }
            grdCondicoes.DataSource = lista;
            grdCondicoes.DataBind();
        }
    }


    private void AdicionaItemListaTemporariaCondicoes(tbComprasFornecedorCondicaoPagamento item)
    {
        var lista = new List<tbComprasFornecedorCondicaoPagamento>();
        if (!string.IsNullOrEmpty(hdfLista.Text))
        {
            lista = Deserialize(hdfLista.Text);
        }

        lista.RemoveAll(x => x.idComprasCondicaoPagamento == item.idComprasCondicaoPagamento);

        lista.Add(item);
        hdfLista.Text = Serialize(lista);
        FillGridCondicoes();
    }
    protected bool cpfCnpJaExiste(string fornecedorCPFCNPJ,int idFornecedor)
    {
        bool existe = false;
        string mensagem = "";
        var data = new dbCommerceDataContext();
        var cpfcnpjexistente = (from c in data.tbComprasFornecedors
                                where c.fornecedorCPFCNPJ == fornecedorCPFCNPJ
                                select c).FirstOrDefault();
        if (cpfcnpjexistente != null)
        {
            existe = true;
            if (idFornecedor == 0)
            {
                mensagem = "Este CPF ou CNPJ já está cadastrado para o fornecedor: " + cpfcnpjexistente.fornecedor;
            }
            else
            {
                if (cpfcnpjexistente.idComprasFornecedor != idFornecedor)
                {
                    mensagem = "Este CPF ou CNPJ já está cadastrado para o fornecedor: " + cpfcnpjexistente.fornecedor;
                }
                else
                {
                    existe = false;
                }
            }
        }
        if (existe)
        {
            Response.Write("<script>alert('" + mensagem + "');</script>");
        }
        return existe;
    }

    protected bool validaCPFCNPJ(string fornecedorCPFCNPJ)
    {
        bool ok = true;
        string mensagem = "";
        if(fornecedorCPFCNPJ.Length!= 11 && fornecedorCPFCNPJ.Length != 14)
        {
            ok = false;
            mensagem += "CPF ou CNPJ inválido";
        }
        else
        if (fornecedorCPFCNPJ.Length == 11)
        {
            if(!validaCPF(fornecedorCPFCNPJ))
            {
                ok = false;
                mensagem += "CPF inválido";
            }
        }
        else
        if (fornecedorCPFCNPJ.Length == 14)
        {
            if (!validaCNPJ(fornecedorCPFCNPJ))
            {
                ok = false;
                mensagem += "CNPJ inválido";
            }
        }


        if(!ok)
        {
            Response.Write("<script>alert('" + mensagem + "');</script>") ;
        }
        return ok;
    }

    public static bool validaCPF(string vrCPF)
    {
        string valor = vrCPF.Replace(".", "");
        valor = valor.Replace("-", "");
        if (valor.Length != 11)
            return false;

        bool igual = true;
        for (int i = 1; i < 11 && igual; i++)
            if (valor[i] != valor[0])
                igual = false;

        if (igual || valor == "12345678909")
            return false;

        int[] numeros = new int[11];

        for (int i = 0; i < 11; i++)
            numeros[i] = int.Parse(valor[i].ToString());

        int soma = 0;
        for (int i = 0; i < 9; i++)
            soma += (10 - i) * numeros[i];

        int resultado = soma % 11;
        if (resultado == 1 || resultado == 0)
        {
            if (numeros[9] != 0)
                return false;
        }
        else if (numeros[9] != 11 - resultado)
            return false;
        soma = 0;
        for (int i = 0; i < 10; i++)
            soma += (11 - i) * numeros[i];
        resultado = soma % 11;

        if (resultado == 1 || resultado == 0)
        {
            if (numeros[10] != 0)
                return false;
        }
        else
            if (numeros[10] != 11 - resultado)
            return false;

        return true;
     }

    public static bool validaCNPJ(string vrCNPJ)
    {
        string CNPJ = vrCNPJ.Replace(".", "");
        CNPJ = CNPJ.Replace("/", "");
        CNPJ = CNPJ.Replace("-", "");

        int[] digitos, soma, resultado;
        int nrDig;
        string ftmt;
        bool[] CNPJOk;

        ftmt = "6543298765432";
        digitos = new int[14];
        soma = new int[2];
        soma[0] = 0;
        soma[1] = 0;
        resultado = new int[2];
        resultado[0] = 0;
        resultado[1] = 0;
        CNPJOk = new bool[2];
        CNPJOk[0] = false;
        CNPJOk[1] = false;

        try
        {
            for (nrDig = 0; nrDig < 14; nrDig++)
            {
                digitos[nrDig] = int.Parse(
                    CNPJ.Substring(nrDig, 1));
                if (nrDig <= 11)
                    soma[0] += (digitos[nrDig] *
                      int.Parse(ftmt.Substring(
                      nrDig + 1, 1)));
                if (nrDig <= 12)
                    soma[1] += (digitos[nrDig] *
                      int.Parse(ftmt.Substring(
                      nrDig, 1)));
            }

            for (nrDig = 0; nrDig < 2; nrDig++)
            {
                resultado[nrDig] = (soma[nrDig] % 11);
                if ((resultado[nrDig] == 0) || (
                     resultado[nrDig] == 1))
                    CNPJOk[nrDig] = (
                    digitos[12 + nrDig] == 0);
                else
                    CNPJOk[nrDig] = (
                    digitos[12 + nrDig] == (
                    11 - resultado[nrDig]));
            }
            return (CNPJOk[0] && CNPJOk[1]);
        }
        catch
        {
            return false;
        }
    }


    protected void btnGravar_OnClick(object sender, EventArgs e)
    {
        var data = new dbCommerceDataContext();
        if (!string.IsNullOrEmpty(hdfIdFornecedor.Value))
        {
            if(!String.IsNullOrEmpty(txtfornecedorCPFCNPJ.Text))
            {
                if (!validaCPFCNPJ(txtfornecedorCPFCNPJ.Text.Replace(".", "").Replace("-", "").Replace("/", "")))
                    return;
                if (cpfCnpJaExiste(txtfornecedorCPFCNPJ.Text.Replace(".", "").Replace("-", "").Replace("/", ""), Convert.ToInt32(hdfIdFornecedor.Value)))
                    return;
            }


            int idComprasFornecedor = Convert.ToInt32(hdfIdFornecedor.Value);
            var comprasFornecedor = (from c in data.tbComprasFornecedors where c.idComprasFornecedor == idComprasFornecedor select c).First();

            comprasFornecedor.fornecedor = txtFornecedor.Text;
            comprasFornecedor.contato = txtContato.Text;
            comprasFornecedor.email = txtEmail.Text;
            comprasFornecedor.telefone = txtTelefone.Text;
            comprasFornecedor.prazo = Convert.ToInt32(txtPrazoDeEntrega.Value);
            comprasFornecedor.fornecedorCPFCNPJ = txtfornecedorCPFCNPJ.Text.Replace(".", "").Replace("-", "").Replace("/", "");
            
            data.SubmitChanges();
        }
        else
        {
            if (!String.IsNullOrEmpty(txtfornecedorCPFCNPJ.Text))
            {
                if (!validaCPFCNPJ(txtfornecedorCPFCNPJ.Text.Replace(".", "").Replace("-", "").Replace("/", "")))
                    return;
                if (cpfCnpJaExiste(txtfornecedorCPFCNPJ.Text.Replace(".", "").Replace("-", "").Replace("/", ""), 0))
                    return;
            }

            var comprasFornecedor = new tbComprasFornecedor();
            comprasFornecedor.fornecedor = txtFornecedor.Text;
            comprasFornecedor.contato = txtContato.Text;
            comprasFornecedor.email = txtEmail.Text;
            comprasFornecedor.telefone = txtTelefone.Text;
            comprasFornecedor.prazo = Convert.ToInt32(txtPrazoDeEntrega.Value);
            comprasFornecedor.fornecedorCPFCNPJ = txtfornecedorCPFCNPJ.Text.Replace(".", "").Replace("-", "").Replace("/", "");

            data.tbComprasFornecedors.InsertOnSubmit(comprasFornecedor);
            data.SubmitChanges();

            var lista = new List<tbComprasFornecedorCondicaoPagamento>();
            if (!string.IsNullOrEmpty(hdfLista.Text))
            {
                lista = Deserialize(hdfLista.Text);
            }

            foreach (var fornecedorCondicao in lista)
            {
                var condicao = new tbComprasFornecedorCondicaoPagamento
                {
                    acrescimo = fornecedorCondicao.acrescimo,
                    desconto = fornecedorCondicao.desconto,
                    idComprasFornecedor = comprasFornecedor.idComprasFornecedor,
                    idComprasCondicaoPagamento = fornecedorCondicao.idComprasCondicaoPagamento
                };
                data.tbComprasFornecedorCondicaoPagamentos.InsertOnSubmit(condicao);
                data.SubmitChanges();
            }
        }
        Response.Redirect("comprasFornecedores.aspx");
    }
    protected void btnGravarCondicao_OnClick(object sender, EventArgs e)
    {
        var data = new dbCommerceDataContext();
        int condicaoDePagamento = 0;
        int desconto = Convert.ToInt32(txtDesconto.Value);
        int acrescimo = Convert.ToInt32(txtAcrescimo.Value);
        int.TryParse(ddlCondicaoPagamento.Value.ToString(), out condicaoDePagamento);
        if (condicaoDePagamento > 0)
        {
            if (!string.IsNullOrEmpty(hdfIdFornecedor.Value))
            {
                int idComprasFornecedor = Convert.ToInt32(hdfIdFornecedor.Value);
                var condicaoCheck = (from c in data.tbComprasFornecedorCondicaoPagamentos where c.idComprasFornecedor == idComprasFornecedor && c.idComprasCondicaoPagamento == condicaoDePagamento select c).Any();
                if (!condicaoCheck)
                {
                    var condicao = new tbComprasFornecedorCondicaoPagamento();
                    condicao.idComprasCondicaoPagamento = condicaoDePagamento;
                    condicao.idComprasFornecedor = idComprasFornecedor;
                    condicao.acrescimo = acrescimo;
                    condicao.desconto = desconto;
                    data.tbComprasFornecedorCondicaoPagamentos.InsertOnSubmit(condicao);
                    data.SubmitChanges();
                }
            }
            else
            {
                var condicao = new tbComprasFornecedorCondicaoPagamento();
                condicao.idComprasCondicaoPagamento = condicaoDePagamento;
                condicao.idComprasFornecedor = 0;
                condicao.acrescimo = acrescimo;
                condicao.desconto = desconto;
                AdicionaItemListaTemporariaCondicoes(condicao);
            }
        }
        FillGridCondicoes();
    }

    private int RetornaIdUsuarioLogado()
    {
        HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
        usuarioLogadoId = Request.Cookies["usuarioLogadoId"];
        if (usuarioLogadoId == null)
        {
            return 0;
        }

        return Convert.ToInt32(usuarioLogadoId.Value);
    }

    public static string Serialize(List<tbComprasFornecedorCondicaoPagamento> tData)
    {
        var serializer = new XmlSerializer(typeof(List<tbComprasFornecedorCondicaoPagamento>));

        TextWriter writer = new StringWriter();
        serializer.Serialize(writer, tData);

        return writer.ToString();
    }

    public static List<tbComprasFornecedorCondicaoPagamento> Deserialize(string tData)
    {
        var serializer = new XmlSerializer(typeof(List<tbComprasFornecedorCondicaoPagamento>));

        TextReader reader = new StringReader(tData);

        return (List<tbComprasFornecedorCondicaoPagamento>)serializer.Deserialize(reader);
    }



    protected void btnRemover_OnCommand(object sender, CommandEventArgs e)
    {
        if (!string.IsNullOrEmpty(hdfIdFornecedor.Value))
        {
            var data = new dbCommerceDataContext();
            int idComprasCondicao = Convert.ToInt32(e.CommandArgument);
            int idComprasFornecedor = Convert.ToInt32(hdfIdFornecedor.Value);
            var comprasCheck = (from c in data.tbComprasFornecedorCondicaoPagamentos where c.idComprasFornecedor == idComprasFornecedor && c.idComprasCondicaoPagamento == idComprasCondicao select c).FirstOrDefault();
            if (comprasCheck != null)
            {
                data.tbComprasFornecedorCondicaoPagamentos.DeleteOnSubmit(comprasCheck);
                data.SubmitChanges();
            }
        }
        else
        {
            RemoveItemLista(Convert.ToInt32(e.CommandArgument));
        }
        FillGridCondicoes();
    }
    private void RemoveItemLista(int idComprasCondicaoPagamento)
    {
        var lista = new List<tbComprasFornecedorCondicaoPagamento>();
        if (!string.IsNullOrEmpty(hdfLista.Text))
        {
            lista = Deserialize(hdfLista.Text);
        }
        lista.RemoveAll(x => x.idComprasCondicaoPagamento == idComprasCondicaoPagamento);
        hdfLista.Text = Serialize(lista);
        FillGridCondicoes();
    }
}