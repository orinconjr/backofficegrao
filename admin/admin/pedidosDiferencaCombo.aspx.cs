﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_pedidosDiferencaCombo : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        var lista = new StringBuilder();

        var dataPedidos = new dbCommerceDataContext();
        var pedidos = (from c in dataPedidos.tbPedidos where c.statusDoPedido == 11 orderby c.dataConfirmacaoPagamento select c);
        foreach (var tbPedido in pedidos)
        {
            var itensPedidoDc = new dbCommerceDataContext();
            var itens = (from c in itensPedidoDc.tbItensPedidos where c.pedidoId == tbPedido.pedidoId select c);
            var produtosDc = new dbCommerceDataContext();
            foreach (var item in itens)
            {
                bool cancelado = item.cancelado == null ? false : (bool)item.cancelado;
                if (!cancelado)
                {
                    var produtosCombo = (from c in produtosDc.tbProdutoRelacionados where c.idProdutoPai == item.produtoId select c);
                    foreach (var produtoCombo in produtosCombo)
                    {
                        var produtosFilho = (from c in produtosDc.tbItensPedidoCombos where c.produtoId == produtoCombo.idProdutoFilho
                                                 && c.tbItensPedido.pedidoId == item.pedidoId select c).Any();
                        if (!produtosFilho)
                        {
                            lista.Append("Pedido: " + tbPedido.pedidoId + "<br>Produto: " + produtoCombo.idProdutoFilho + "<br>" + produtoCombo.tbProduto.produtoNome + "<br><br>");
                        }
                    }
                }

            }
        }

        litListaPedidos.Text = lista.ToString();
    }
}