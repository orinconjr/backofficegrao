﻿using DevExpress.Web.ASPxGridView;
using SpreadsheetLight;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_importarPlanilhaTransportadora : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        fillDrops();
        CarregarTipoEntregaGrid();

    }

    private void fillDrops()
    {
        if (!Page.IsPostBack)
        {
            var data = new dbCommerceDataContext();
            var transportadoras = (from c in data.tbTransportadoras select c).OrderBy(x => x.transportadora).ToList();
            ddlTransportadora.DataSource = transportadoras;
            ddlTransportadora.DataBind();

            ddlTransportadora.Items.Insert(0, new ListItem("Selecione", "-1"));
            ddlTransportadora.SelectedIndex = 0;
        }
    }
    protected void btnImportar_Click(object sender, EventArgs e)
    {
        try
        {
            lblMensagem.Text = string.Empty;
            if ((ddlTransportadora.SelectedIndex > 0) || (string.IsNullOrEmpty(ddlTabelaTransportadora.SelectedValue)))
            {
                if (ddlTransportadora.SelectedIndex == 0)
                {
                    lblMensagem.Text = "Favor selecionar a transportadora";
                    ddlTransportadora.Focus();
                    return;
                }

                if (string.IsNullOrEmpty(ddlTabelaTransportadora.SelectedValue))
                {
                    lblMensagem.Text = "Favor selecionar a tabela";
                    ddlTabelaTransportadora.Focus();
                    return;
                }
            }

            string caminho = MapPath("~/");

            if (File.Exists(caminho + "planilhaTransportadora" + Path.GetExtension(fupPlanilha.FileName).ToLower()))
                File.Delete(caminho + "planilhaTransportadora" + Path.GetExtension(fupPlanilha.FileName).ToLower());
            fupPlanilha.SaveAs(caminho + "planilhaTransportadora" + Path.GetExtension(fupPlanilha.FileName).ToLower());


            string caminhoPlanilha = caminho + "planilhaTransportadora" + Path.GetExtension(fupPlanilha.FileName).ToLower();

            int idTransportadora = Convert.ToInt32(ddlTransportadora.SelectedValue);
            int idTipoDeEntrega = Convert.ToInt32(ddlTabelaTransportadora.SelectedValue);
            var data = new dbCommerceDataContext();
            using (SLDocument sl = new SLDocument(caminhoPlanilha, "taxas"))
            {
                SLWorksheetStatistics stats = sl.GetWorksheetStatistics();
                int iStartColumnIndex = stats.StartColumnIndex;
                for (int row = stats.StartRowIndex + 1; row <= stats.EndRowIndex; ++row)
                {
                    try
                    {
                        string taxa = sl.GetCellValueAsString(row, 1).ToString().Trim();
                        decimal percentual = Convert.ToDecimal(sl.GetCellValueAsDecimal(row, 2).ToString().Trim());
                        decimal valor = Convert.ToDecimal(sl.GetCellValueAsDecimal(row, 3).ToString().Trim());
                        string tipo = sl.GetCellValueAsString(row, 4).ToString().Trim();
                        bool obrigatoria = sl.GetCellValueAsString(row, 5).ToString().Trim().ToLower() == "s" ? true : false;


                        var tarifa = (from c in data.tbTipoDeEntregaTarifas where c.idTipoDeEntrega == idTipoDeEntrega && c.nome.ToLower() == taxa.ToLower() select c).FirstOrDefault();
                        if (tarifa != null)
                        {
                            tarifa.percentual = percentual;
                            tarifa.valor = valor;
                            tarifa.valorSobreFrete = tipo.Contains("frete") ? true : false;
                            tarifa.valorSobreFreteDivisao = tipo.Contains("divisao") ? true : false;
                            data.SubmitChanges();
                        }
                        else
                        {
                            tarifa = new tbTipoDeEntregaTarifa();
                            tarifa.percentual = percentual;
                            tarifa.valor = valor;
                            tarifa.nome = taxa;
                            tarifa.valorSobreFrete = tipo.Contains("frete") ? true : false;
                            tarifa.valorSobreFreteDivisao = tipo.Contains("divisao") ? true : false;
                            tarifa.idTipoDeEntregaTarifa = (from c in data.tbTipoDeEntregaTarifas orderby c.idTipoDeEntregaTarifa descending select c.idTipoDeEntregaTarifa).First() + 1;
                            tarifa.idTipoDeEntrega = idTipoDeEntrega;
                            data.tbTipoDeEntregaTarifas.InsertOnSubmit(tarifa);
                            data.SubmitChanges();
                        }
                    }
                    catch (Exception ex)
                    {

                    }
                }
            }
            using (SLDocument sl = new SLDocument(caminhoPlanilha, "tabela"))
            {
                SLWorksheetStatistics stats = sl.GetWorksheetStatistics();
                int iStartColumnIndex = stats.StartColumnIndex;
                for (int row = stats.StartRowIndex + 1; row <= stats.EndRowIndex; ++row)
                {
                    try
                    {
                        string uf = sl.GetCellValueAsString(row, 1).ToString().Trim();
                        string tabela = sl.GetCellValueAsString(row, 2).ToString();
                        string kgAdicional = sl.GetCellValueAsDecimal(row, 3).ToString();
                        string adValorem = sl.GetCellValueAsDecimal(row, 4).ToString();
                        string cubagem = sl.GetCellValueAsString(row, 5).ToString();

                        int idTipoDeEntregaTabelaPreco = 0;
                        var tabelaValor = (from c in data.tbTipoDeEntregaTabelaPrecos where c.idTipoDeEntrega == idTipoDeEntrega && c.nome.ToLower() == (uf.ToLower() + " - " + tabela) select c).FirstOrDefault();
                        if (tabelaValor == null)
                        {
                            var tabelaAdicionar = new tbTipoDeEntregaTabelaPreco();
                            tabelaAdicionar.idTipoDeEntrega = idTipoDeEntrega;
                            tabelaAdicionar.nome = uf + " - " + tabela;
                            tabelaAdicionar.valorKgAdicional = Convert.ToDecimal(kgAdicional.Trim());
                            tabelaAdicionar.valorSeguro = Convert.ToDecimal(adValorem.Trim());
                            tabelaAdicionar.fatorCubagem = Convert.ToInt32(cubagem);
                            data.tbTipoDeEntregaTabelaPrecos.InsertOnSubmit(tabelaAdicionar);
                            data.SubmitChanges();
                            idTipoDeEntregaTabelaPreco = tabelaAdicionar.idTipoDeEntregaTabelaPreco;
                        }
                        else
                        {
                            tabelaValor.idTipoDeEntrega = idTipoDeEntrega;
                            tabelaValor.nome = uf + " - " + tabela;
                            tabelaValor.valorKgAdicional = Convert.ToDecimal(kgAdicional.Replace(".", ",").Trim());
                            tabelaValor.valorSeguro = Convert.ToDecimal(adValorem.Replace(".", ",").Trim().Trim());
                            tabelaValor.fatorCubagem = Convert.ToInt32(cubagem);
                            data.SubmitChanges();
                            idTipoDeEntregaTabelaPreco = tabelaValor.idTipoDeEntregaTabelaPreco;
                        }

                    }
                    catch (Exception ex)
                    {

                    }
                }
            }
            using (SLDocument sl = new SLDocument(caminhoPlanilha, "tabela_faixas"))
            {
                SLWorksheetStatistics stats = sl.GetWorksheetStatistics();
                int iStartColumnIndex = stats.StartColumnIndex;
                for (int row = stats.StartRowIndex + 1; row <= stats.EndRowIndex; ++row)
                {
                    try
                    {
                        string uf = sl.GetCellValueAsString(row, 1).ToString().Trim();
                        string tabela = sl.GetCellValueAsString(row, 2).ToString();
                        int pesoInicial = Convert.ToInt32(sl.GetCellValueAsDecimal(row, 3).ToString());
                        int pesoFinal = Convert.ToInt32(sl.GetCellValueAsDecimal(row, 4).ToString());
                        decimal valor = Convert.ToDecimal(sl.GetCellValueAsDecimal(row, 5).ToString().Replace(".", "."));

                        var tabelaValor = (from c in data.tbTipoDeEntregaTabelaPrecos where c.idTipoDeEntrega == idTipoDeEntrega && c.nome.ToLower() == (uf.ToLower() + " - " + tabela) select c).FirstOrDefault();
                        if (tabelaValor != null)
                        {
                            var tipoDeEntregaValor = (from c in data.tbTipoDeEntregaTabelaPrecoValors where c.idTipoDeEntregaTabelaPreco == tabelaValor.idTipoDeEntregaTabelaPreco && c.pesoInicial == pesoInicial && c.pesoFinal == pesoFinal select c).FirstOrDefault();
                            if (tipoDeEntregaValor != null)
                            {
                                tipoDeEntregaValor.valor = valor;
                                data.SubmitChanges();
                            }
                            else
                            {
                                tipoDeEntregaValor = new tbTipoDeEntregaTabelaPrecoValor();
                                tipoDeEntregaValor.idTipoDeEntregaTabelaPreco = tabelaValor.idTipoDeEntregaTabelaPreco;
                                tipoDeEntregaValor.pesoInicial = pesoInicial;
                                tipoDeEntregaValor.pesoFinal = pesoFinal;
                                tipoDeEntregaValor.valor = valor;
                                data.tbTipoDeEntregaTabelaPrecoValors.InsertOnSubmit(tipoDeEntregaValor);
                                data.SubmitChanges();
                            }
                        }


                    }
                    catch (Exception ex)
                    {

                    }
                }
            }
            using (SLDocument sl = new SLDocument(caminhoPlanilha, "faixas"))
            {
                SLWorksheetStatistics stats = sl.GetWorksheetStatistics();
                int iStartColumnIndex = stats.StartColumnIndex;
                for (int row = stats.StartRowIndex + 1; row <= stats.EndRowIndex; ++row)
                {
                    try
                    {
                        long LongCepInicial = 0;
                        long LongCepFinal = 0;

                        long.TryParse(sl.GetCellValueAsString(row, 1).ToString().Trim().Replace("-", ""), out LongCepInicial);
                        long.TryParse(sl.GetCellValueAsString(row, 2).ToString().Trim().Replace("-", ""), out LongCepFinal);

                        if (LongCepInicial == 0 || LongCepFinal == 0)
                            continue;

                        string cepInicial = LongCepInicial.ToString();
                        string cepFinal = LongCepFinal.ToString();
                        string uf = sl.GetCellValueAsString(row, 3).ToString().Trim();
                        string tabela = sl.GetCellValueAsString(row, 4).ToString().ToLower();
                        string cidade = sl.GetCellValueAsString(row, 5).ToString();
                        int prazo = Convert.ToInt32(sl.GetCellValueAsDecimal(row, 6).ToString());
                        string taxa1 = sl.GetCellValueAsString(row, 7).ToString();
                        string taxa2 = sl.GetCellValueAsString(row, 8).ToString();
                        string taxa3 = sl.GetCellValueAsString(row, 9).ToString();
                        string taxa4 = sl.GetCellValueAsString(row, 10).ToString();
                        string taxa5 = sl.GetCellValueAsString(row, 11).ToString();

                        int idFaixaCep = 0;
                        var tabelaValor = (from c in data.tbTipoDeEntregaTabelaPrecos where c.idTipoDeEntrega == idTipoDeEntrega && c.nome.ToLower() == (uf.ToLower() + " - " + tabela) select c).FirstOrDefault();
                        if (tabelaValor != null)
                        {
                            var faixaCep = (from c in data.tbFaixaDeCeps
                                            where c.tipodeEntregaId == idTipoDeEntrega && c.faixaInicial == cepInicial
               && c.faixaFinal == cepFinal
                                            select c).FirstOrDefault();
                            if (faixaCep != null)
                            {
                                faixaCep.idTipoDeEntregaTabelaPreco = tabelaValor.idTipoDeEntregaTabelaPreco;
                                faixaCep.prazo = prazo;
                                idFaixaCep = faixaCep.faixaDeCepId;
                                data.SubmitChanges();
                            }
                            else
                            {
                                faixaCep = new tbFaixaDeCep();
                                faixaCep.tipodeEntregaId = idTipoDeEntrega;
                                faixaCep.faixaDeCepDescricao = uf + " - " + cidade;
                                faixaCep.faixaInicial = cepInicial;
                                faixaCep.faixaFinal = cepFinal;
                                faixaCep.prazo = prazo;
                                faixaCep.porcentagemDeDesconto = 0;
                                faixaCep.porcentagemDoSeguro = 0;
                                faixaCep.dataDaCriacao = DateTime.Now;
                                faixaCep.ativo = true;
                                faixaCep.interiorizacao = false;
                                faixaCep.idTipoDeEntregaTabelaPreco = tabelaValor.idTipoDeEntregaTabelaPreco;
                                data.tbFaixaDeCeps.InsertOnSubmit(faixaCep);
                                data.SubmitChanges();
                                idFaixaCep = faixaCep.faixaDeCepId;

                            }
                        }

                        if (idFaixaCep > 0)
                        {
                            if (!string.IsNullOrEmpty(taxa1))
                            {
                                insereTarifa(idFaixaCep, taxa1);
                            }
                            if (!string.IsNullOrEmpty(taxa2))
                            {
                                insereTarifa(idFaixaCep, taxa2);
                            }
                            if (!string.IsNullOrEmpty(taxa3))
                            {
                                insereTarifa(idFaixaCep, taxa3);
                            }
                            if (!string.IsNullOrEmpty(taxa4))
                            {
                                insereTarifa(idFaixaCep, taxa4);
                            }
                            if (!string.IsNullOrEmpty(taxa5))
                            {
                                insereTarifa(idFaixaCep, taxa5);
                            }
                        }

                    }
                    catch (Exception ex)
                    {

                    }
                }
            }

            AlertShow("Tabela atualizada com sucesso");
        }
        catch (Exception ex)
        {
            AlertShow("Ocorreu um erro");
        }
    }

    private void insereTarifa(int idFaixaCep, string tarifa)
    {
        var data = new dbCommerceDataContext();
        var faixa = (from c in data.tbFaixaDeCeps where c.faixaDeCepId == idFaixaCep select c).First();
        var taxa = (from c in data.tbTipoDeEntregaTarifas where c.nome.ToLower() == tarifa.ToLower() select c).FirstOrDefault();
        if (taxa != null)
        {
            var faixaTarifa = (from c in data.tbFaixaDeCepTarifas where c.idFaixaDeCep == idFaixaCep && c.idTipoDeEntregaTarifa == taxa.idTipoDeEntregaTarifa select c).FirstOrDefault();
            if (faixaTarifa == null)
            {
                faixaTarifa = new tbFaixaDeCepTarifa();
                faixaTarifa.idFaixaDeCep = idFaixaCep;
                faixaTarifa.idTipoDeEntregaTarifa = taxa.idTipoDeEntregaTarifa;
                data.tbFaixaDeCepTarifas.InsertOnSubmit(faixaTarifa);
                data.SubmitChanges();
            }
        }
    }


    public void AlertShow(string message)
    {
        base.ClientScript.RegisterStartupScript(base.GetType(), "myalert", "alert('" + message + "'); ", true);
    }

    protected void btnCadastrarTransportadora_Click(object sender, EventArgs e)
    {
        try
        {
            hdfIdTransportadora.Value = string.Empty;
            txtNomeTransportadora.Text = string.Empty;
            CarregarTransportadoraGrid();
            popCadastrarTransportadora.ShowOnPageLoad = true;
        }
        catch (Exception ex)
        {
            lblMensagemTransportadora.Text = "Erro: " + ex.Message;
        }
    }

    private void CarregarTransportadoraGrid()
    {
        try
        {
            var data = new dbCommerceDataContext();

            var trasnportadoras = (from c in data.tbTransportadoras select c).ToList();

            gvTransportadora.DataSource = trasnportadoras;
            gvTransportadora.DataBind();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void btnCadastrarTabelaTransportadora_Click(object sender, EventArgs e)
    {
        try
        {
            lblMensagemTransportadora.Text = string.Empty;
            hdnTipoEntregaId.Value = string.Empty;
            txtNomeTipoEntrega.Text = string.Empty;
            CarregarTipoEntregaGrid();
            CarregarTransportadoraCombo();
            popCadastrarTabela.ShowOnPageLoad = true;
        }
        catch (Exception ex)
        {
            lblMensagemTransportadora.Text = "Erro: " + ex.Message;
        }
    }

    private void CarregarTransportadoraCombo()
    {
        try
        {
            var data = new dbCommerceDataContext();

            var transportadoras = (from c in data.tbTransportadoras select c).OrderBy(x => x.transportadora).ToList();

            ddlTransportadoraTabela.DataTextField = "transportadora";
            ddlTransportadoraTabela.DataValueField = "idTransportadora";
            ddlTransportadoraTabela.DataSource = transportadoras;
            ddlTransportadoraTabela.DataBind();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void CarregarTipoEntregaGrid()
    {
        try
        {
            var data = new dbCommerceDataContext();

            var tipoEntregas = (from c in data.tbTipoDeEntregas select c).OrderBy(x => x.tipoDeEntregaNome).ToList();

            gvTipoEntrega.DataSource = tipoEntregas;
            gvTipoEntrega.DataBind();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void btnAlterar_Command(object sender, CommandEventArgs e)
    {
        try
        {
            var data = new dbCommerceDataContext();
            if (e.CommandName == "AlterarTransportadora" && !string.IsNullOrEmpty(e.CommandArgument.ToString()))
            {
                hdfIdTransportadora.Value = e.CommandArgument.ToString();

                var transportadora = (from c in data.tbTransportadoras
                                      where c.idTransportadora == int.Parse(hdfIdTransportadora.Value)
                                      select c).FirstOrDefault();

                if (transportadora != null)
                    txtNomeTransportadora.Text = transportadora.transportadora;
            }
        }
        catch (Exception ex)
        {
            lblMensagemTransportadora.Text = "Erro ao alterar a transportadora: " + ex.Message;
        }
    }

    protected void btnGravar_Click(object sender, EventArgs e)
    {
        try
        {
            lblMensagemTransportadora.Text = "";
            var data = new dbCommerceDataContext();

            if (string.IsNullOrEmpty(txtNomeTransportadora.Text.Trim()))
            {
                lblMensagemTransportadora.Text = "Favor informar o nome da transportadora";
                txtNomeTransportadora.Text = string.Empty;
                txtNomeTransportadora.Focus();
                return;
            }


            if (!string.IsNullOrEmpty(hdfIdTransportadora.Value))
            {

                VerificaNomeTransportadora(int.Parse(hdfIdTransportadora.Value));

                var transportadora = (from c in data.tbTransportadoras
                                      where c.idTransportadora == int.Parse(hdfIdTransportadora.Value)
                                      select c).FirstOrDefault();

                transportadora.transportadora = txtNomeTransportadora.Text;
                data.SubmitChanges();
            }
            else
            {
                VerificaNomeTransportadora();

                var novo = new tbTransportadora();
                novo.idTransportadora = data.tbTransportadoras.Max(x => x.idTransportadora) + 1;
                novo.transportadora = txtNomeTransportadora.Text.Trim();
                data.tbTransportadoras.InsertOnSubmit(novo);
                data.SubmitChanges();
            }

            LimparCamposTransportadora();
            CarregarTransportadoraGrid();

        }
        catch (Exception ex)
        {
            lblMensagemTransportadora.Text = "Erro ao gravar a transportadora: " + ex.Message;
        }
    }

    private void  VerificaNomeTransportadora(int? pTransportadoraId = null)
    {
        try
        {
            var data = new dbCommerceDataContext();

            int existeNome = 0;

            if (pTransportadoraId.HasValue)
            {
                existeNome = (from c in data.tbTransportadoras
                              where c.transportadora.Trim().ToLower() == txtNomeTransportadora.Text.Trim().ToLower()
                              && c.idTransportadora != pTransportadoraId.Value
                              select c).Count();
            }
            else
            {
                existeNome = (from c in data.tbTransportadoras
                              where c.transportadora.Trim().ToLower() == txtNomeTransportadora.Text.Trim().ToLower()
                              select c).Count();
            }
            

            if (existeNome > 0)
            {
                lblMensagemTransportadora.Text = "O nome " + txtNomeTransportadora.Text + " já cadastrado. Favor informar outro nome";
                txtNomeTransportadora.Text = string.Empty;
                txtNomeTransportadora.Focus();
                return;
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void LimparCamposTransportadora()
    {
        hdfIdTransportadora.Value = string.Empty;
        txtNomeTransportadora.Text = string.Empty;

    }

    protected void btnGravarTipoDeEntrega_Click(object sender, EventArgs e)
    {
        try
        {
            var data = new dbCommerceDataContext();

            if (!string.IsNullOrEmpty(hdnTipoEntregaId.Value))
            {
                var tipoEntrega = (from c in data.tbTipoDeEntregas where c.tipoDeEntregaId == int.Parse(hdnTipoEntregaId.Value) select c).FirstOrDefault();

                tipoEntrega.tipoDeEntregaNome = txtNomeTipoEntrega.Text;
                tipoEntrega.idTransportadoraString = ddlTransportadoraTabela.SelectedItem.Text;
                tipoEntrega.idTransportadora = int.Parse(ddlTransportadoraTabela.SelectedValue);
                tipoEntrega.pesoAdicionalSomaPesoMaximo = chkPesoAdicional.Checked;
                tipoEntrega.cubagem = chkCubagem.Checked;
                tipoEntrega.habilitarExpedicao = chkExpedicao.Checked;
                tipoEntrega.habilitarSimulacao = chkSimulacao.Checked;
                if (!string.IsNullOrEmpty(txtFatorCubagem.Text))
                    tipoEntrega.divisaoCubagem = int.Parse(txtFatorCubagem.Text);
                if (!string.IsNullOrEmpty(txtLimitePesoa.Text))
                    tipoEntrega.limitePeso = int.Parse(txtLimitePesoa.Text);
                if (!string.IsNullOrEmpty(txtLimiteMaiorDimensao.Text))
                    tipoEntrega.limiteMaiorDimensao = int.Parse(txtLimiteMaiorDimensao.Text);
                if (!string.IsNullOrEmpty(txtLimiteSomaDimensao.Text))
                    tipoEntrega.limiteSomaDimensoes = int.Parse(txtLimiteSomaDimensao.Text);
                data.SubmitChanges();
            }
            else
            {
                var novo = new tbTipoDeEntrega();
                novo.freteProporcional = false;
                novo.idTransportadora = int.Parse(ddlTransportadoraTabela.SelectedValue);
                novo.idTransportadoraString = ddlTransportadoraTabela.SelectedItem.Text;
                novo.descontoFreteProporcional = 0;
                novo.descontoTotalPedidoFreteProporcional = 0;
                novo.pesoAdicionalSomaPesoMaximo = chkPesoAdicional.Checked;
                novo.cubagem = chkCubagem.Checked;
                novo.habilitarExpedicao = chkExpedicao.Checked;
                novo.habilitarSimulacao = chkSimulacao.Checked;
                if (!string.IsNullOrEmpty(txtFatorCubagem.Text))
                    novo.divisaoCubagem = int.Parse(txtFatorCubagem.Text);
                novo.grupoEntrega = 0;
                if (!string.IsNullOrEmpty(txtLimitePesoa.Text))
                    novo.limitePeso = int.Parse(txtLimitePesoa.Text);
                novo.cepAproximado = false;
                if (!string.IsNullOrEmpty(txtLimiteMaiorDimensao.Text))
                    novo.limiteMaiorDimensao = int.Parse(txtLimiteMaiorDimensao.Text);
                if (!string.IsNullOrEmpty(txtLimiteSomaDimensao.Text))
                    novo.limiteSomaDimensoes = int.Parse(txtLimiteSomaDimensao.Text);
                novo.tipoDeEntregaNome = txtNomeTipoEntrega.Text;
                novo.dataDaCriacao = System.DateTime.Now;
                novo.ativo = false;
                novo.idTipoEntregaTipoTabela = 2;
                data.tbTipoDeEntregas.InsertOnSubmit(novo);
                data.SubmitChanges();
            }
            hdnTipoEntregaId.Value = string.Empty;
            txtNomeTipoEntrega.Text = string.Empty;
            chkCubagem.Checked = false;
            chkPesoAdicional.Checked = false;
            txtFatorCubagem.Text = string.Empty;
            txtLimiteMaiorDimensao.Text = string.Empty;
            txtLimitePesoa.Text = string.Empty;
            txtLimiteSomaDimensao.Text = string.Empty;
            CarregarTipoEntregaGrid();
        }
        catch (Exception ex)
        {
            lblMensagemTipoEntrega.Text = "Erro: " + ex.Message;
        }
    }

    protected void btnAlterarTipoEntrega_Command(object sender, CommandEventArgs e)
    {
        try
        {
            chkExpedicao.Checked = false;
            chkSimulacao.Checked = false;

            var data = new dbCommerceDataContext();
            if (e.CommandName == "AlterarTabela" && !string.IsNullOrEmpty(e.CommandArgument.ToString()))
            {
                hdnTipoEntregaId.Value = e.CommandArgument.ToString();

                var tabela = (from c in data.tbTipoDeEntregas where c.tipoDeEntregaId == int.Parse(hdnTipoEntregaId.Value.ToString()) select c).FirstOrDefault();

                if (tabela != null)
                {
                    txtNomeTipoEntrega.Text = tabela.tipoDeEntregaNome;
                    ddlTransportadoraTabela.Text = tabela.idTransportadora.ToString();
                    txtFatorCubagem.Text = tabela.divisaoCubagem.ToString();
                    chkCubagem.Checked = tabela.cubagem;
                    chkPesoAdicional.Checked = tabela.pesoAdicionalSomaPesoMaximo;
                    chkSimulacao.Checked = bool.Parse(tabela.habilitarSimulacao.ToString());
                    chkExpedicao.Checked = bool.Parse(tabela.habilitarExpedicao.ToString());
                    txtLimitePesoa.Text = tabela.limitePeso.ToString();
                    txtLimiteMaiorDimensao.Text = tabela.limiteMaiorDimensao.ToString();
                    txtLimiteSomaDimensao.Text = tabela.limiteSomaDimensoes.ToString();
                }
            }
        }
        catch (Exception ex)
        {
            lblMensagemTransportadora.Text = "Erro ao alterar a transportadora: " + ex.Message;
        }
    }

    protected void ddlTransportadora_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlTransportadora.SelectedIndex > 0)
            {
                CarregarTabelaTRansportadora(int.Parse(ddlTransportadora.SelectedValue), ddlTransportadora.SelectedItem.Text);
            }
        }
        catch (Exception ex)
        {
            lblMensagemTipoEntrega.Text = "Erro: " + ex.Message;
        }
    }

    private void CarregarTabelaTRansportadora(int pTransportadoraID, string pTransportadoraString)
    {
        try
        {
            var data = new dbCommerceDataContext();

            var tabelas = (from c in data.tbTipoDeEntregas where (c.idTransportadora == pTransportadoraID || c.idTransportadoraString == pTransportadoraString && c.habilitarExpedicao == false) select c).ToList();

            ddlTabelaTransportadora.DataSource = tabelas;
            ddlTabelaTransportadora.DataBind();

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
}