﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_financeiroImportarCartao : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnSalvar_Click(object sender, ImageClickEventArgs e)
    {
        string caminho = MapPath("~/");
        fluArquivo.SaveAs(caminho + "extratocartoescielo.csv");
        var reader = new StreamReader(File.OpenRead(caminho + "extratocartoescielo.csv"));
        int linha = 0;
        int operador = 1;

        int totalConsolidado = 0;
        while (!reader.EndOfStream)
        {
            var line = reader.ReadLine();
            if (!string.IsNullOrEmpty(line))
            {
                linha++;
                var values = line.Split(';');
                if (linha >= 4)
                {
                    var autorizacao = values[6].Trim().Replace("\"", "");
                    var data = new dbCommerceDataContext();
                    var cobrancas =
                        (from c in data.tbPedidoPagamentos
                         where c.idOperadorPagamento == 3 && c.numeroCobranca == autorizacao
                         select c);
                    foreach (var cobranca in cobrancas)
                    {
                        if (cobranca.consolidado == false) totalConsolidado++;
                        cobranca.consolidado = true;
                    }
                    data.SubmitChanges();
                }
            }
        }
        reader.Dispose();
        Response.Write("<script>alert('" + totalConsolidado + " pedidos consolidados.');</script>");
    }
    protected void btnSalvarElavon_Click(object sender, ImageClickEventArgs e)
    {
        string caminho = MapPath("~/");
        fluArquivoElavon.SaveAs(caminho + "extratocartoeselavon.csv");
        var reader = new StreamReader(File.OpenRead(caminho + "extratocartoeselavon.csv"));
        int linha = 0;
        int operador = 1;

        int totalConsolidado = 0;
        while (!reader.EndOfStream)
        {
            var line = reader.ReadLine();
            if (!string.IsNullOrEmpty(line))
            {
                linha++;
                var values = line.Split(',');
                if (linha >= 4)
                {
                    var autorizacao = values[12].Trim().Replace("\"", "");
                    var data = new dbCommerceDataContext();
                    var cobrancas =
                        (from c in data.tbPedidoPagamentos
                         where c.idOperadorPagamento == 4 && c.numeroCobranca == autorizacao
                         select c);
                    foreach (var cobranca in cobrancas)
                    {
                        if (cobranca.consolidado == false) totalConsolidado++;
                        cobranca.consolidado = true;
                    }
                    data.SubmitChanges();
                }
            }
        }
        reader.Dispose();
        Response.Write("<script>alert('" + totalConsolidado + " pedidos consolidados.');</script>");
    }
}