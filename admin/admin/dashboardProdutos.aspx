﻿<%@ Page Title="" Theme="Glass" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="dashboardProdutos.aspx.cs" Inherits="admin_dashboard" EnableEventValidation="false" %>

<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dxwgv" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxTabControl" Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxClasses" Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" runat="Server">

    <style type="text/css">
        .width150 {
        }

        .height150 {
        }

        .height80 {
        }

        .btn {
            -webkit-border-radius: 7px;
            -moz-border-radius: 7px;
            border-radius: 2px;
            font-family: Arial;
            color: #ffffff;
            font-size: 14px;
            background: #63c269;
            padding: 5px 10px 5px 10px;
            text-decoration: none;
            cursor: pointer;
            border: 0px;
        }

            .btn:hover {
                background: #a4dbab;
                text-decoration: none;
                color: black;
                font-weight: bold;
            }


        .marginTop5porcento {
            margin: 6%;
            font-size: 15px;
        }

        .quadroPrincipal {
            float: left;
            width: 150px;
            height: 155px;
            text-align: center;
            position: relative;
            font-family: monospace;
            border: 1px solid #B1B1B1;
            margin: 0 10px 5px 10px;
            border-radius: 15px;
        }

        .quadroInteriorQtds {
            width: 150px;
            height: 80px;
            margin-top: 5%;
            text-align: center;
            font-size: 30px;
        }

        .AlinharAoRodapeCentralizar {
            position: absolute;
            bottom: 5px;
            text-align: center;
            width: 150px;
        }

        .loadingInformation {
            background-image: url(images/gear.svg);
            background-repeat: no-repeat;
            width: 60px;
            height: 60px;
            margin: 0 auto;
        }
    </style>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <script src="js/dashboardProdutos.js"></script>

    <asp:ScriptManager runat="server" ScriptMode="Release"></asp:ScriptManager>

    <asp:UpdatePanel runat="server">
        <ContentTemplate>

            <table cellpadding="0" cellspacing="0" style="width: 920px">
                <tr>
                    <td class="tituloPaginas" valign="top">
                        <asp:Label ID="lblAcao" runat="server">Status de Produtos</asp:Label>
                    </td>
                </tr>
                <tr>
                    <td style="font-family: monospace; font-size: 14px;">
                        <div style="float: left; padding-left: 18px; width: 90px;">Fornecedor:</div>
                        <asp:DropDownList runat="server" ID="ddlFornecedor" DataSourceID="sqlFornecedores"
                            AppendDataBoundItems="True" DataTextField="fornecedorNome" DataValueField="fornecedorId" OnSelectedIndexChanged="ddlFornecedor_OnSelectedIndexChanged" /><%--AutoPostBack="True"--%>
                    </td>
                </tr>
                <tr>
                    <td style="font-family: monospace; font-size: 14px;">
                        <div style="float: left; padding-left: 18px; width: 90px;">Marca:</div>
                        <asp:DropDownList runat="server" ID="ddlMarca"
                            AppendDataBoundItems="True" DataTextField="marcaNome" DataValueField="marcaId" AutoPostBack="True" OnSelectedIndexChanged="ddlMarca_OnSelectedIndexChanged" />

                    </td>
                </tr>
                <tr>
                    <td style="font-family: monospace; font-size: 14px;">
                        <div style="float: left; padding-left: 18px; width: 90px;">Categoria:</div>
                        <asp:DropDownList runat="server" ID="ddlCategoria" DataTextField="categoriaNome" 
                            DataValueField="categoriaId" AppendDataBoundItems="True" OnSelectedIndexChanged="ddlCategoria_OnSelectedIndexChanged" /><%--AutoPostBack="True"--%>
                        <asp:HiddenField runat="server" ID="hfCategoriaId"/>
                    </td>
                </tr>
                <tr>
                    <td style="padding-left: 16px;">

                        <dx:ASPxPageControl ID="tabsDashboard" runat="server" ActiveTabIndex="0" Width="888px">
                            <TabPages>
                                <dx:TabPage Text="Fora de Linha">
                                    <ContentCollection>
                                        <dx:ContentControl runat="server">

                                            <div class="quadroPrincipal">
                                                <div class="marginTop5porcento">FORA DE LINHA</div>
                                                <div class="quadroInteriorQtds">
                                                    <asp:Literal runat="server" ID="litForaDeLinha"></asp:Literal>
                                                    <div id="divForaDeLinha"></div>
                                                </div>
                                                <div class="AlinharAoRodapeCentralizar">
                                                    <asp:Button runat="server" ID="btnDetalharFL" Text="Detalhar" CssClass="btn" OnCommand="btnDetalharFL_OnCommand" />
                                                </div>
                                            </div>
                                            <div class="quadroPrincipal">
                                                <div class="marginTop5porcento">FORA DE LINHA ESTOQUE 0</div>
                                                <div class="quadroInteriorQtds">
                                                    <asp:Literal runat="server" ID="litForaDeLinhaEstoque0"></asp:Literal>
                                                    <div id="divForaDeLinhaEstoque0"></div>
                                                </div>
                                                <div class="AlinharAoRodapeCentralizar">
                                                    <asp:Button runat="server" ID="btnDetalharFLE0" Text="Detalhar" CssClass="btn" OnCommand="btnDetalharFLE0_OnCommand" />
                                                </div>
                                            </div>
                                            <div class="quadroPrincipal">
                                                <div class="marginTop5porcento">FORA DE LINHA ESTOQUE BAIXO (1-10)</div>
                                                <div class="quadroInteriorQtds">
                                                    <asp:Literal runat="server" ID="litForaDeLinhaEstoqueBaixo"></asp:Literal>
                                                    <div id="divForaDeLinhaEstoqueBaixo"></div>
                                                </div>
                                                <div class="AlinharAoRodapeCentralizar">
                                                    <asp:Button runat="server" ID="btnDetalharFLEB" Text="Detalhar" CssClass="btn" OnCommand="btnDetalharFLEB_OnCommand" />
                                                </div>
                                            </div>

                                        </dx:ContentControl>
                                    </ContentCollection>
                                </dx:TabPage>

                                <dx:TabPage Text="Estoque">
                                    <ContentCollection>
                                        <dx:ContentControl runat="server">

                                            <div class="quadroPrincipal">
                                                <div class="marginTop5porcento">ESTOQUE BAIXO (1-10)</div>
                                                <div class="quadroInteriorQtds">
                                                    <asp:Literal runat="server" ID="litEstoqueBaixo"></asp:Literal>
                                                    <div id="divEstoqueBaixo"></div>
                                                </div>
                                                <div class="AlinharAoRodapeCentralizar">
                                                    <asp:Button runat="server" ID="btnDetalharEB" Text="Detalhar" CssClass="btn" OnCommand="btnDetalharEB_OnCommand" />
                                                </div>
                                            </div>
                                            
                                            <div class="quadroPrincipal">
                                                <div class="marginTop5porcento">ESTOQUE 0 OU NEGATIVO</div>
                                                <div class="quadroInteriorQtds">
                                                    <asp:Literal runat="server" ID="litEstoque0OuNegativo"></asp:Literal>
                                                    <div id="divEstoque0OuNegativo"></div>
                                                </div>
                                                <div class="AlinharAoRodapeCentralizar">
                                                    <asp:Button runat="server" ID="btnDetalharE0N" Text="Detalhar" CssClass="btn" OnCommand="btnDetalharE0N_OnCommand" />
                                                </div>
                                            </div>

                                            <div class="quadroPrincipal">
                                                <div class="marginTop5porcento">PRODUTO DESATIVADO COM ESTOQUE MAIOR QUE 0</div>
                                                <div class="quadroInteriorQtds">
                                                    <asp:Literal runat="server" ID="litProdutosDesativadosComEstoqueMaiorQue0"></asp:Literal>
                                                    <div id="divProdutosDesativadosComEstoqueMaiorQue0"></div>
                                                </div>
                                                <div class="AlinharAoRodapeCentralizar">
                                                    <asp:Button runat="server" ID="brnDetalharPDEM0" Text="Detalhar" CssClass="btn" OnCommand="brnDetalharPDEM0_OnCommand" />
                                                </div>
                                            </div>

                                        </dx:ContentControl>
                                    </ContentCollection>
                                </dx:TabPage>

                                <dx:TabPage Text="Categoria">
                                    <ContentCollection>
                                        <dx:ContentControl runat="server">

                                            <div class="quadroPrincipal">
                                                <div class="marginTop5porcento">PRODUTOS ATIVOS SEM CATEGORIA</div>
                                                <div class="quadroInteriorQtds">
                                                    <asp:Literal runat="server" ID="litProdutoAtivoSemCategoria"></asp:Literal>
                                                    <div id="divProdutoAtivoSemCategoria"></div>
                                                </div>
                                                <div class="AlinharAoRodapeCentralizar">
                                                    <asp:Button runat="server" ID="btnDetalharPASC" Text="Detalhar" CssClass="btn" OnCommand="btnDetalharPASC_OnCommand" />
                                                </div>
                                            </div>

                                            <div class="quadroPrincipal">
                                                <div class="marginTop5porcento">CATEGORIA ATIVA SEM PRODUTO</div>
                                                <div class="quadroInteriorQtds">
                                                    <asp:Literal runat="server" ID="litCategoriaAtivaSemProduto"></asp:Literal>
                                                    <div id="divCategoriaAtivaSemProduto"></div>
                                                </div>
                                                <div class="AlinharAoRodapeCentralizar">
                                                    <asp:Button runat="server" ID="btnDetalharCASP" Text="Detalhar" CssClass="btn" OnCommand="btnDetalharCASP_OnCommand" />
                                                </div>
                                            </div>

                                            <div class="quadroPrincipal">
                                                <div class="marginTop5porcento">CATEGORIA ATIVA SEM DESCRIÇÃO</div>
                                                <div class="quadroInteriorQtds">
                                                    <asp:Literal runat="server" ID="litCategoriaAtivaSemDescricao"></asp:Literal>
                                                    <div id="divCategoriaAtivaSemDescricao"></div>
                                                </div>
                                                <div class="AlinharAoRodapeCentralizar">
                                                    <asp:Button runat="server" ID="btnDetalharCASD" Text="Detalhar" CssClass="btn" OnCommand="btnDetalharCASD_OnCommand" />
                                                </div>
                                            </div>

                                        </dx:ContentControl>
                                    </ContentCollection>
                                </dx:TabPage>

                                <dx:TabPage Text="Produto">
                                    <ContentCollection>
                                        <dx:ContentControl runat="server">



                                            <div class="quadroPrincipal">
                                                <div class="marginTop5porcento">PRODUTO ATIVO SEM FOTO</div>
                                                <div class="quadroInteriorQtds">
                                                    <asp:Literal runat="server" ID="litProdutoAtivoSemFoto"></asp:Literal>
                                                    <div id="divProdutoAtivoSemFoto"></div>
                                                </div>
                                                <div class="AlinharAoRodapeCentralizar">
                                                    <asp:Button runat="server" ID="btnDetalharPASF" Text="Detalhar" CssClass="btn" OnCommand="btnDetalharPASF_OnCommand" />
                                                </div>
                                            </div>

                                            <div class="quadroPrincipal">
                                                <div class="marginTop5porcento">PRODUTO MOSTRANDO COMO ESGOTADO</div>
                                                <div class="quadroInteriorQtds">
                                                    <asp:Literal runat="server" ID="litProdutosAtivosComEstoque0"></asp:Literal>
                                                    <div id="divProdutosAtivosComEstoque0"></div>
                                                </div>
                                                <div class="AlinharAoRodapeCentralizar">
                                                    <asp:Button runat="server" ID="btnDetalharPASE0" Text="Detalhar" CssClass="btn" OnCommand="btnDetalharPASE0_OnCommand" />
                                                </div>
                                            </div>

                                            <div class="quadroPrincipal">
                                                <div class="marginTop5porcento">PRODUTO ATIVO SEM DESCRIÇÃO</div>
                                                <div class="quadroInteriorQtds">
                                                    <asp:Literal runat="server" ID="litProdutoAtivoSemDescricao"></asp:Literal>
                                                    <div id="divProdutoAtivoSemDescricao"></div>
                                                </div>
                                                <div class="AlinharAoRodapeCentralizar">
                                                    <asp:Button runat="server" ID="btnDetalhePASD" Text="Detalhar" CssClass="btn" OnCommand="btnDetalhePASD_OnCommand" />
                                                </div>
                                            </div>

                                            <div class="quadroPrincipal">
                                                <div class="marginTop5porcento">PRODUTO ATIVO SEM COLEÇÃO</div>
                                                <div class="quadroInteriorQtds">
                                                    <asp:Literal runat="server" ID="litProdutoAtivoSemColecao"></asp:Literal>
                                                    <div id="divProdutoAtivoSemColecao"></div>
                                                </div>
                                                <div class="AlinharAoRodapeCentralizar">
                                                    <asp:Button runat="server" ID="btnDetalhePASCO" Text="Detalhar" CssClass="btn" OnCommand="btnDetalhePASCO_OnCommand" />
                                                </div>
                                            </div>

                                            <div class="quadroPrincipal">
                                                <div class="marginTop5porcento">PRODUTO ATIVO SEM DESCRIÇÃO/ COMPOSIÇÃO</div>
                                                <div class="quadroInteriorQtds">
                                                    <asp:Literal runat="server" ID="litProdutoAtivoSemComposicao"></asp:Literal>
                                                    <div id="divProdutoAtivoSemComposicao"></div>
                                                </div>
                                                <div class="AlinharAoRodapeCentralizar">
                                                    <asp:Button runat="server" ID="btnDetalhePASCOM" Text="Detalhar" CssClass="btn" OnCommand="btnDetalhePASCOM_OnCommand" />
                                                </div>
                                            </div>

                                            <div class="quadroPrincipal">
                                                <div class="marginTop5porcento">PRODUTO ATIVO EM PROMOÇÃO</div>
                                                <div class="quadroInteriorQtds">
                                                    <asp:Literal runat="server" ID="litProdutoAtivoEmPromocao"></asp:Literal>
                                                    <div id="divProdutoAtivoEmPromocao"></div>
                                                </div>
                                                <div class="AlinharAoRodapeCentralizar">
                                                    <asp:Button runat="server" ID="btnDetalhePAEP" Text="Detalhar" CssClass="btn" OnCommand="btnDetalhePAEP_OnCommand" />
                                                </div>
                                            </div>

                                            <div class="quadroPrincipal" style="display: none;">
                                                <div class="marginTop5porcento">PRODUTO ATIVO COM MARGEM MENOR QUE 133</div>
                                                <div class="quadroInteriorQtds">
                                                    <asp:Literal runat="server" ID="litProdutoAtivoComMargemMenor133"></asp:Literal>
                                                </div>
                                                <div class="AlinharAoRodapeCentralizar">
                                                    <asp:Button runat="server" ID="btnDetalharPAMN133" Text="Detalhar" CssClass="btn" OnCommand="btnDetalharPAMN133_OnCommand" />
                                                </div>
                                            </div>



                                        </dx:ContentControl>
                                    </ContentCollection>
                                </dx:TabPage>

                                <dx:TabPage Text="Filtro">
                                    <ContentCollection>
                                        <dx:ContentControl runat="server">

                                            <div class="quadroPrincipal">
                                                <div class="marginTop5porcento">FILTROS SEM PRODUTOS</div>
                                                <div class="quadroInteriorQtds">
                                                    <asp:Literal runat="server" ID="litFiltroSemProduto"></asp:Literal>
                                                    <div id="divFiltroSemProduto"></div>
                                                </div>
                                                <div class="AlinharAoRodapeCentralizar">
                                                    <asp:Button runat="server" ID="btnDetalharFSP" Text="Detalhar" CssClass="btn" OnCommand="btnDetalharFSP_OnCommand" />
                                                </div>
                                            </div>

                                        </dx:ContentControl>
                                    </ContentCollection>
                                </dx:TabPage>

                                <dx:TabPage Text="Preço">
                                    <ContentCollection>
                                        <dx:ContentControl runat="server">

                                            <div class="quadroPrincipal" style="display: none;">
                                                <div class="marginTop5porcento">PREÇO PROMOCIONAL MAIOR QUE PREÇO DE VENDA</div>
                                                <div class="quadroInteriorQtds">
                                                    <asp:Literal runat="server" ID="litPrecoPromcionalMaiorQuePrecoVenda"></asp:Literal>
                                                </div>
                                                <div class="AlinharAoRodapeCentralizar">
                                                    <asp:Button runat="server" ID="btnDetalharPPMPV" Text="Detalhar" CssClass="btn" OnCommand="btnDetalharPPMPV_OnCommand" />
                                                </div>
                                            </div>

                                            <div class="quadroPrincipal">
                                                <div class="marginTop5porcento">PREÇO CUSTO MAIOR QUE PREÇO DE VENDA</div>
                                                <div class="quadroInteriorQtds">
                                                    <asp:Literal runat="server" ID="litPrecoDeCustoMaiorQuePrecoVenda"></asp:Literal>
                                                    <div id="divPrecoDeCustoMaiorQuePrecoVenda"></div>
                                                </div>
                                                <div class="AlinharAoRodapeCentralizar">
                                                    <asp:Button runat="server" ID="btnDetalharPCMPV" Text="Detalhar" CssClass="btn" />
                                                </div>
                                            </div>

                                        </dx:ContentControl>
                                    </ContentCollection>
                                </dx:TabPage>

                            </TabPages>
                        </dx:ASPxPageControl>

                    </td>
                </tr>
                <tr>
                    <td>

                        <div style="padding-left: 10px; display: none;">

                            <div class="quadroPrincipal">
                                <div class="marginTop5porcento">COMBOS COM MARGEM MENOR QUE 122</div>
                                <div class="quadroInteriorQtds">
                                    <asp:Literal runat="server" ID="litComboComMargemMenor122"></asp:Literal>
                                </div>
                                <div class="AlinharAoRodapeCentralizar">
                                    <asp:Button runat="server" ID="btnDetalharCMN122" Text="Detalhar" CssClass="btn" OnCommand="btnDetalharCMN122_OnCommand" />
                                </div>
                            </div>

                        </div>
                        <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
                            <tr>
                                <td align="right"></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="tituloPaginas" valign="top">
                                    <asp:Label ID="lblTituloDetalhamento" runat="server" Visible="False">Detalhamento</asp:Label>
                                    <div style="float: right; margin: 8px 25px; font-family: monospace; font-size: 14px;">
                                        <div style="float: left; padding-top: 9px;">
                                            Exportar-
                                        </div>
                                        <asp:ImageButton ID="btXsl" runat="server" ImageUrl="~/admin/images/btXls.jpg" OnClick="btXsl_Click" Style="border: 1px solid #41697e; padding: 3px;" />
                                    </div>

                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <dxwgv:ASPxGridView ID="grd" runat="server" AutoGenerateColumns="False" Visible="False"
                                        KeyFieldName="produto" Width="834px" OnCustomUnboundColumnData="grd_OnCustomUnboundColumnData"
                                        Cursor="auto" ClientInstanceName="grd" EnableCallBacks="False">
                                        <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                                        <Styles>
                                            <Footer Font-Bold="True">
                                            </Footer>
                                        </Styles>
                                        <SettingsText ConfirmDelete="Tem certeza que deseja excluir?"
                                            EmptyDataRow="Nenhum registro encontrado." />
                                        <SettingsPager Position="TopAndBottom" PageSize="50" ShowDisabledButtons="False" AlwaysShowPager="True">
                                            <Summary AllPagesText="Páginas: {0} - {1} ({2} items)" Text="Página {0} de {1} ({2} registros encontrados)" />
                                        </SettingsPager>
                                        <Settings ShowFilterRow="True" ShowGroupButtons="False" ShowFooter="True" />
                                        <TotalSummary>
                                            <dxwgv:ASPxSummaryItem DisplayFormat="0" ShowInColumn="produto" ShowInGroupFooterColumn="produto" SummaryType="Count" />
                                            <dxwgv:ASPxSummaryItem FieldName="custo" ShowInColumn="custo" ShowInGroupFooterColumn="custo" SummaryType="Sum" DisplayFormat="{0:C}" />
                                        </TotalSummary>
                                        <SettingsEditing EditFormColumnCount="4"
                                            PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1"
                                            PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter"
                                            PopupEditFormWidth="700px" Mode="Inline" />
                                        <Columns>
                                            <dxwgv:GridViewDataTextColumn Caption="Produto ID" FieldName="produtoId">
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn Caption="Produto" FieldName="produto" Settings-AutoFilterCondition="Contains">
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn Caption="Ativo" FieldName="ativo">
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn Caption="Categoria" FieldName="categoria" UnboundType="String">
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn Caption="Estoque" FieldName="estoqueReal" UnboundType="String">
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn Caption="Etiqueta" FieldName="etiqueta" UnboundType="String">
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataHyperLinkColumn Caption="Alterar"
                                                Width="45px">
                                                <DataItemTemplate>
                                                    <asp:HyperLink ID="hplNovo" runat="server" ImageUrl="images/btEditar.jpg" Target="_blanck"
                                                        NavigateUrl='<%# "produtoAlt.aspx?produtoId=" + Eval("produtoId") + "&produtoPaiId=" + Eval("produtoPaiId") + "&acao=alterar" %>'>Alterar</asp:HyperLink>
                                                </DataItemTemplate>
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dxwgv:GridViewDataHyperLinkColumn>
                                        </Columns>
                                        <StylesEditors>
                                            <Label Font-Bold="True">
                                            </Label>
                                        </StylesEditors>
                                    </dxwgv:ASPxGridView>
                                    <dxwgv:ASPxGridViewExporter ID="grdEx" runat="server" FileName="relatorio_dashboardProdutos"
                                        GridViewID="grd" Landscape="True" PreserveGroupRowStates="True">
                                    </dxwgv:ASPxGridViewExporter>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>

                        </table>

                    </td>
                </tr>
            </table>

        </ContentTemplate>
    </asp:UpdatePanel>

    <table cellpadding="0" cellspacing="0" style="width: 920px">
        <tr>
            <td class="tituloPaginas" valign="top">
                <asp:Label ID="Label1" runat="server">Pesquisa de Margem</asp:Label>
                <div style="float: right; margin: 8px 25px; font-family: monospace; font-size: 14px;">
                    <div style="float: left; padding-top: 9px;">
                        Exportar-
                    </div>
                    <asp:ImageButton ID="btXlsPM" runat="server" ImageUrl="~/admin/images/btXls.jpg" OnClick="btXlsPM_OnClick" Style="border: 1px solid #41697e; padding: 3px;" />
                </div>
            </td>
        </tr>
        <tr>
            <td style="font-family: monospace; font-size: 14px; padding-left: 40px;">De:<asp:TextBox runat="server" ID="txtMargemIni" Width="70" MaxLength="3"></asp:TextBox>
                Até:<asp:TextBox runat="server" ID="txtMargemFim" Width="70" MaxLength="3"></asp:TextBox>
                <asp:Button runat="server" ID="btnPesquisarProdutosPelaMargem" Text="Pesquisar" OnClick="btnPesquisarProdutosPelaMargem_OnClick" />
            </td>
        </tr>
        <tr>
            <td>
                <div style="padding-left: 47px; width: 116px; float: left;">
                    <asp:RangeValidator ID="RangeValidator1" runat="server" ErrorMessage="apenas numeros" MinimumValue="1" MaximumValue="999"
                        ControlToValidate="txtMargemIni" Type="Integer"></asp:RangeValidator>
                </div>
                <div>
                    <asp:RangeValidator ID="RangeValidator2" runat="server" ErrorMessage="apenas numeros" MinimumValue="1" MaximumValue="999"
                        ControlToValidate="txtMargemFim" Type="Integer"></asp:RangeValidator>
                </div>
            </td>
        </tr>
        <tr>
            <td style="padding-left: 40px;">
                <dxwgv:ASPxGridView ID="grdProdutosPorMargem" runat="server" AutoGenerateColumns="False" Visible="False"
                    KeyFieldName="produtoId" Width="834px" DataSourceID="sqlProdutosPorMargem"
                    Cursor="auto" ClientInstanceName="grdProdutosPorMargem" EnableCallBacks="False">
                    <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                    <Styles>
                        <Footer Font-Bold="True">
                        </Footer>
                    </Styles>
                    <SettingsText ConfirmDelete="Tem certeza que deseja excluir?"
                        EmptyDataRow="Nenhum registro encontrado." />
                    <SettingsPager Position="TopAndBottom" PageSize="50" ShowDisabledButtons="False" AlwaysShowPager="True">
                        <Summary AllPagesText="Páginas: {0} - {1} ({2} items)" Text="Página {0} de {1} ({2} registros encontrados)" />
                    </SettingsPager>
                    <Settings ShowFilterRow="True" ShowGroupButtons="False" ShowFooter="True" />
                    <TotalSummary>
                        <dxwgv:ASPxSummaryItem DisplayFormat="0" ShowInColumn="produto" ShowInGroupFooterColumn="produto" SummaryType="Count" />
                        <dxwgv:ASPxSummaryItem FieldName="custo" ShowInColumn="custo" ShowInGroupFooterColumn="custo" SummaryType="Sum" DisplayFormat="{0:C}" />
                    </TotalSummary>
                    <SettingsEditing EditFormColumnCount="4"
                        PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1"
                        PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter"
                        PopupEditFormWidth="700px" Mode="Inline" />
                    <Columns>
                        <dxwgv:GridViewDataTextColumn Caption="Produto ID" FieldName="produtoId">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Produto" FieldName="produtoNome" Settings-AutoFilterCondition="Contains">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Margem" FieldName="margemDeLucro" Settings-AutoFilterCondition="Contains">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Ativo" FieldName="ativo" Settings-AutoFilterCondition="Contains">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataHyperLinkColumn Caption="Alterar"
                            Width="45px">
                            <DataItemTemplate>
                                <asp:HyperLink ID="hplNovo" runat="server" ImageUrl="images/btEditar.jpg" Target="_blanck"
                                    NavigateUrl='<%# "produtoAlt.aspx?produtoId=" + Eval("produtoId") + "&produtoPaiId=" + Eval("produtoPaiId") + "&acao=alterar" %>'>Alterar</asp:HyperLink>
                            </DataItemTemplate>
                            <CellStyle HorizontalAlign="Center">
                            </CellStyle>
                        </dxwgv:GridViewDataHyperLinkColumn>
                    </Columns>
                    <StylesEditors>
                        <Label Font-Bold="True">
                        </Label>
                    </StylesEditors>
                </dxwgv:ASPxGridView>

                <dxwgv:ASPxGridViewExporter ID="grdExGrdProdutosPorMargem" runat="server" FileName="relatorio_dashboardProdutosPorMargem"
                    GridViewID="grdProdutosPorMargem" Landscape="True" PreserveGroupRowStates="True">
                </dxwgv:ASPxGridViewExporter>
            </td>
        </tr>
        <tr>
            <td>&nbsp;
                
                <asp:SqlDataSource ID="sqlProdutosPorMargem" runat="server"
                    ConnectionString="<%$ ConnectionStrings:connectionString %>"
                    SelectCommand="SELECT produtoId, produtoNome, margemDeLucro, produtoPaiId, CASE WHEN produtoAtivo = 'True' THEN 'Sim' ELSE 'Nao' END As ativo FROM tbProdutos WHERE (margemDeLucro >= @margemIni AND margemDeLucro <= @margemFim)">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="txtMargemIni" Name="margemIni" DefaultValue="0" />
                        <asp:ControlParameter ControlID="txtMargemFim" Name="margemFim" DefaultValue="0" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
        </tr>
    </table>

    <table cellpadding="0" cellspacing="0" style="width: 920px">
        <tr>
            <td class="tituloPaginas" valign="top">
                <asp:Label ID="Label2" runat="server">Pesquisa de Filtro</asp:Label>
                <div style="float: right; margin: 8px 25px; font-family: monospace; font-size: 14px;">
                    <div style="float: left; padding-top: 9px;">
                        Exportar-
                    </div>
                    <asp:ImageButton ID="btXlsPF" runat="server" ImageUrl="~/admin/images/btXls.jpg" OnClick="btXlsPF_OnClick" Style="border: 1px solid #41697e; padding: 3px;" />
                </div>
            </td>
        </tr>
        <tr>
            <td style="font-family: monospace; font-size: 14px; padding-left: 40px;">Filtro:<asp:DropDownList runat="server" ID="ddlFiltros" DataSourceID="sqlFiltros" DataTextField="categoriaNome" DataValueField="categoriaId"
                AppendDataBoundItems="True" OnSelectedIndexChanged="ddlFiltros_OnSelectedIndexChanged" AutoPostBack="True" />
                Opções do Filtro:<asp:DropDownList runat="server" ID="ddlOpcoesFiltro" DataTextField="categoriaNome" DataValueField="categoriaId" AppendDataBoundItems="True" />
                <asp:Button runat="server" ID="btnPesquisaPorFiltro" Text="Pesquisar" OnClick="btnPesquisaPorFiltro_OnClick" />
            </td>
        </tr>
        <tr>
            <td style="font-family: monospace; font-size: 14px; padding: 20px 0 0 40px;">
                <asp:Label runat="server" ID="lblTituloPesquisaPorFiltro"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="padding-left: 40px;">
                <dxwgv:ASPxGridView ID="grdPesquisaPorFiltro" runat="server" AutoGenerateColumns="False" Visible="False"
                    KeyFieldName="produtoId" Width="834px"
                    Cursor="auto" ClientInstanceName="grdPesquisaPorFiltro" EnableCallBacks="False">
                    <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                    <Styles>
                        <Footer Font-Bold="True">
                        </Footer>
                    </Styles>
                    <SettingsText ConfirmDelete="Tem certeza que deseja excluir?"
                        EmptyDataRow="Nenhum registro encontrado." />
                    <SettingsPager Position="TopAndBottom" PageSize="50" ShowDisabledButtons="False" AlwaysShowPager="True">
                        <Summary AllPagesText="Páginas: {0} - {1} ({2} items)" Text="Página {0} de {1} ({2} registros encontrados)" />
                    </SettingsPager>
                    <Settings ShowFilterRow="True" ShowGroupButtons="False" ShowFooter="True" />
                    <TotalSummary>
                        <dxwgv:ASPxSummaryItem DisplayFormat="0" ShowInColumn="produto" ShowInGroupFooterColumn="produto" SummaryType="Count" />
                        <dxwgv:ASPxSummaryItem FieldName="custo" ShowInColumn="custo" ShowInGroupFooterColumn="custo" SummaryType="Sum" DisplayFormat="{0:C}" />
                    </TotalSummary>
                    <SettingsEditing EditFormColumnCount="4"
                        PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1"
                        PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter"
                        PopupEditFormWidth="700px" Mode="Inline" />
                    <Columns>
                        <dxwgv:GridViewDataTextColumn Caption="Produto ID" FieldName="produtoId">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Produto" FieldName="produtoNome" Settings-AutoFilterCondition="Contains">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Ativo" FieldName="ativo" Settings-AutoFilterCondition="Contains">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataHyperLinkColumn Caption="Alterar"
                            Width="45px">
                            <DataItemTemplate>
                                <asp:HyperLink ID="hplNovo" runat="server" ImageUrl="images/btEditar.jpg" Target="_blanck"
                                    NavigateUrl='<%# "produtoAlt.aspx?produtoId=" + Eval("produtoId") + "&produtoPaiId=" + Eval("produtoPaiId") + "&acao=alterar" %>'>Alterar</asp:HyperLink>
                            </DataItemTemplate>
                            <CellStyle HorizontalAlign="Center">
                            </CellStyle>
                        </dxwgv:GridViewDataHyperLinkColumn>
                    </Columns>
                    <StylesEditors>
                        <Label Font-Bold="True">
                        </Label>
                    </StylesEditors>
                </dxwgv:ASPxGridView>

                <dxwgv:ASPxGridViewExporter ID="grdExGrdPesquisaPorFiltro" runat="server" FileName="relatorio_dashboardProdutosPorFiltro"
                    GridViewID="grdPesquisaPorFiltro" Landscape="True" PreserveGroupRowStates="True">
                </dxwgv:ASPxGridViewExporter>
            </td>
        </tr>
        <tr>
            <td>&nbsp;
            </td>
        </tr>
    </table>

    <asp:SqlDataSource ID="sqlFornecedores" runat="server"
        ConnectionString="<%$ ConnectionStrings:connectionString %>"
        SelectCommand="SELECT [fornecedorId], [fornecedorNome] FROM [tbProdutoFornecedor] ORDER BY [fornecedorNome]"></asp:SqlDataSource>

    <asp:SqlDataSource ID="sqlFiltros" runat="server"
        ConnectionString="<%$ ConnectionStrings:connectionString %>"
        SelectCommand="SELECT categoriaId, categoriaNome, Filhos FROM (SELECT Cat.categoriaId, Cat.categoriaNome, (SELECT COUNT(categoriaId) FROM tbProdutoCategoria WHERE categoriaPaiId=Cat.categoriaId) As Filhos FROM [tbProdutoCategoria] As Cat Where Cat.categoriaPaiId=0 AND Cat.exibirSite=0 AND Cat.categoriaId <> 774) AS Tab WHERE Filhos > 0 ORDER BY categoriaNome"></asp:SqlDataSource>


    <asp:UpdateProgress ID="updateProgress" runat="server" DisplayAfter="1000">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <div style="background-color: white; position: fixed; top: 44%; left: 40%; width: 238px; height: 50px;">
                    <asp:Literal runat="server" ID="litInfoLoad" Text="Pesquisando produtos"></asp:Literal>
                </div>
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="../admin/images/245.gif" AlternateText="Loading ..." ToolTip="Pesquisando produtos..." Style="padding: 10px; position: fixed; top: 45%; left: 40%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

</asp:Content>

