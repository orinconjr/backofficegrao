﻿<%@ Page Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="pedidos.aspx.cs" Inherits="admin_pedidos" Theme="Glass" ValidateRequest="false" %>

<%@ Register Assembly="DevExpress.Web.v11.1.Linq, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Data.Linq" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" runat="Server">
    <div class="tituloPaginas">
        <asp:Label ID="lblAcao" runat="server"></asp:Label>
    </div>

    <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
        <tr>
            <td align="right">
                <table cellpadding="0" cellspacing="0" style="width: 834px">
                    <tr>
                        <td align="right">&nbsp;</td>
                        <td height="38"
                            style="background-image: url('images/bkExportar.jpg'); padding-bottom: 1px;"
                            valign="bottom" width="231">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="width: 97px">&nbsp;</td>
                                    <td style="width: 33px">
                                        <asp:ImageButton ID="btPdf" runat="server" ImageUrl="~/admin/images/btPdf.jpg" OnClick="btPdf_Click"
                                            Style="margin-left: 5px" />
                                    </td>
                                    <td style="width: 31px">
                                        <asp:ImageButton ID="btXsl" runat="server" ImageUrl="~/admin/images/btXls.jpg" OnClick="btXsl_Click"
                                            Style="margin-left: 5px" />
                                    </td>
                                    <td style="width: 36px">
                                        <asp:ImageButton ID="btRtf" runat="server" ImageUrl="~/admin/images/btRtf.jpg" OnClick="btRtf_Click"
                                            Style="margin-left: 5px" />
                                    </td>
                                    <td valign="bottom">
                                        <asp:ImageButton ID="btCsv" runat="server" ImageUrl="~/admin/images/btCsv.jpg" OnClick="btCsv_Click"
                                            Style="margin-left: 5px" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="right" style="padding-top: 10px;">
                <asp:Button runat="server" ID="Button1" OnClick="btnChecarPedidosMl_OnClick" Text="Checar Pedidos ML" Width="140" CssClass="btn-default" />
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <dxwgv:ASPxGridView ID="grd" runat="server" 
                                    ClientInstanceName="grd" 
                                    AutoGenerateColumns="False"
                                    OnAutoFilterCellEditorCreate="grid_AutoFilterCellEditorCreate" 
                                    OnAutoFilterCellEditorInitialize="grid_AutoFilterCellEditorInitialize"
                                    OnProcessColumnAutoFilter="grid_ProcessColumnAutoFilter" 
                                    DataSourceID="sqlPedidosServer" 
                                    KeyFieldName="pedidoId" 
                                    Width="834px" 
                                    Settings-ShowFilterRow="True" 
                                    Settings-ShowFilterBar="Visible"
                                    Cursor="auto" 
                                    OnCustomUnboundColumnData="grd_OnCustomUnboundColumnData">
                    <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                    <Styles>
                        <Footer Font-Bold="True">
                        </Footer>
                    </Styles>
                    <SettingsText ConfirmDelete="Tem certeza que deseja excluir?" EmptyDataRow="Nenhum registro encontrado." />
                    <SettingsPager Position="TopAndBottom" PageSize="50"
                        ShowDisabledButtons="False" AlwaysShowPager="True">
                        <Summary AllPagesText="Páginas: {0} - {1} ({2} items)"
                            Text="Página {0} de {1} ({2} registros encontrados)" />
                    </SettingsPager>
                    <Settings ShowFilterRow="True" ShowGroupButtons="False" ShowFooter="True" />
                    <TotalSummary>
                        <dxwgv:ASPxSummaryItem DisplayFormat="c" FieldName="valorCobrado"
                            ShowInColumn="Valor" ShowInGroupFooterColumn="Valor" SummaryType="Sum" />
                    </TotalSummary>
                    <SettingsEditing EditFormColumnCount="4"
                        PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1"
                        PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter"
                        PopupEditFormWidth="700px" />
                    <Columns>
<%--                        <dxwgv:GridViewDataTextColumn Caption="Id" FieldName="plataforma" CellStyle-HorizontalAlign="Center" VisibleIndex="0">
                            <EditFormSettings Visible="False" />
                            <DataItemTemplate>
                                <%# (Eval("plataforma")!=null) ? Eval("plataforma").ToString().Equals("1") ?
                                           "<img src='images/mobile_icon.png' width='20px' alt='1 - Pedido feito via celular'/>" :
                                           "<img src='images/desktop_icon.png' width='20px' alt='0 - Pedido feito via desktop'/>"
                                        : ""
                                %>
                            </DataItemTemplate>
                            <HeaderTemplate></HeaderTemplate>
                        </dxwgv:GridViewDataTextColumn>--%>

                        <dxwgv:GridViewDataTextColumn Caption="Id Cliente" FieldName="pedidoIdCliente" VisibleIndex="0" UnboundType="Integer">
                            <EditFormSettings Visible="False" />
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Id" FieldName="pedidoId" CellStyle-HorizontalAlign="Center" VisibleIndex="0">
                            <EditFormSettings Visible="False" />
                            <DataItemTemplate>
                                <%#Eval("pedidoId")%>
                            </DataItemTemplate>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Nome" FieldName="clienteNome"
                            VisibleIndex="1" Width="150px" Settings-AutoFilterCondition="Contains">
                            <PropertiesTextEdit>
                                <ValidationSettings>
                                    <RequiredField ErrorText="Preencha o nome." IsRequired="True" />
                                </ValidationSettings>
                            </PropertiesTextEdit>
                            <Settings AutoFilterCondition="Contains" />
                            <EditFormSettings CaptionLocation="Top" />
                        </dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataTextColumn Caption="Valor" FieldName="valorCobrado" Settings-AllowHeaderFilter="True"
                            VisibleIndex="2" Width="70px" Settings-AutoFilterCondition="Contains" Settings-FilterMode="DisplayText">
                            <PropertiesTextEdit DisplayFormatString="c">
                            </PropertiesTextEdit>
                            <Settings FilterMode="DisplayText" AutoFilterCondition="Contains"></Settings>
                            <EditFormSettings Visible="False" />
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataDateColumn Name="dataHoraDoPedido" Caption="Data" FieldName="dataHoraDoPedido" VisibleIndex="3" Width="120px">
                            <PropertiesDateEdit DisplayFormatString="g"></PropertiesDateEdit>
                        </dxwgv:GridViewDataDateColumn>
                        <dxwgv:GridViewDataComboBoxColumn Caption="Situação" FieldName="statusDoPedido" VisibleIndex="4" Width="130px">
                            <PropertiesComboBox DataSourceID="sqlSituacao" TextField="situacao"
                                ValueField="situacaoId" ValueType="System.String">
                            </PropertiesComboBox>
                        </dxwgv:GridViewDataComboBoxColumn>
                        <dxwgv:GridViewDataDateColumn Name="vencimentoParcela" Caption="Vencimento" FieldName="vencimentoParcela" VisibleIndex="6" Width="120px">
                            <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit>
                        </dxwgv:GridViewDataDateColumn>
                        <dxwgv:GridViewDataTextColumn Name="statusPagamento" Caption="Status Pagamento" FieldName="statusPagamento" VisibleIndex="6" Width="120px" Settings-AutoFilterCondition="Contains">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="StatusMoip" FieldName="moipStatus" VisibleIndex="4" Width="130px">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataComboBoxColumn Caption="Pagamento" VisibleIndex="6"
                            Width="55px" FieldName="condicaoNome">
                            <PropertiesComboBox DataSourceID="sqlPagamentos" TextField="condicaoNome"
                                ValueField="condicaoNome" ValueType="System.String">
                            </PropertiesComboBox>
                            <Settings AllowAutoFilter="True" AllowAutoFilterTextInputTimer="True"
                                FilterMode="DisplayText" />
                            <DataItemTemplate>
                                <asp:Image ID="imgPagamento" runat="server" Height="35px"
                                    ImageUrl='<%# "http://admin.graodegente.com.br/" + Eval("imagem") %>' Width="50px" />
                            </DataItemTemplate>
                            <CellStyle HorizontalAlign="Center">
                            </CellStyle>
                        </dxwgv:GridViewDataComboBoxColumn>
                        <dxwgv:GridViewDataHyperLinkColumn Caption="Editar" FieldName="pedidoId"
                            VisibleIndex="7" Width="30px">
                            <PropertiesHyperLinkEdit ImageUrl="~/admin/images/btEditar.jpg"
                                NavigateUrlFormatString="pedido.aspx?pedidoId={0}" Target="_blank">
                            </PropertiesHyperLinkEdit>
                            <Settings AllowAutoFilter="False" />
                            <HeaderTemplate>
                                <img alt="" src="images/legendaEditar.jpg" />
                            </HeaderTemplate>
                        </dxwgv:GridViewDataHyperLinkColumn>
                        <dxwgv:GridViewCommandColumn ButtonType="Image" VisibleIndex="8">
                            <ClearFilterButton Visible="True">
                                <Image Url="~/admin/images/btExcluir.jpg" />
                            </ClearFilterButton>
                        </dxwgv:GridViewCommandColumn>
                    </Columns>
                    <StylesEditors>
                        <Label Font-Bold="True">
                        </Label>
                    </StylesEditors>
                </dxwgv:ASPxGridView>
                <dxwgv:ASPxGridViewExporter ID="grdEx" runat="server" FileName="pedidos"
                    GridViewID="grd" Landscape="True" PreserveGroupRowStates="True">
                </dxwgv:ASPxGridViewExporter>
                <asp:ObjectDataSource ID="sqlPedidos" runat="server" SelectMethod="pedidoSelecionaAdmin"
                    TypeName="rnPedidos"></asp:ObjectDataSource>
                <asp:ObjectDataSource ID="sqlSituacao" runat="server" SelectMethod="situacaoSeleciona"
                    TypeName="rnSituacao"></asp:ObjectDataSource>
                <asp:ObjectDataSource ID="sqlSituacaoInterna" runat="server" SelectMethod="situacaoInternaSeleciona"
                    TypeName="rnSituacao"></asp:ObjectDataSource>
                <asp:ObjectDataSource ID="sqlPagamentos" runat="server" SelectMethod="condicaoDePagamentoSelecionaTodas"
                    TypeName="rnCondicoesDePagamento"></asp:ObjectDataSource>
                <asp:ObjectDataSource ID="sqlTipoDeEntrega" runat="server" SelectMethod="tipoDeEntregaSeleciona" TypeName="rnTipoDeEntrega"></asp:ObjectDataSource>
                <dx:LinqServerModeDataSource ID="sqlPedidosServer" runat="server" ContextTypeName="dbCommerceDataContext" TableName="tbPedidos" OnSelecting="sqlPedidosServer_Selecting" />

            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
    </table>
    
    <script language="javascript" type="text/javascript">
         function OnDropDownDataPedido(s, dateFrom, dateTo) {
             var str = s.GetText();
             if (str == "") {
                 dateFrom.SetDate(new Date(1950, 0, 1));
                 dateTo.SetDate(new Date(1960, 11, 31));
                 return;
             }
             var d = str.split("|");
             dateFrom.SetText(d[0]);
             dateTo.SetText(d[1]);
         }

         function ApplyFilter(dde, dateFrom, dateTo, campo) {
             var colunaSplit = dde.name.split('_');
             var coluna = colunaSplit[colunaSplit.length - 1].replace("DXFREditorcol", "");
             var colunaFiltro = "";
             if (coluna == "4") colunaFiltro = "dataHoraDoPedido";
             if (coluna == "6") colunaFiltro = "vencimentoParcela";

             var d1 = dateFrom.GetText();
             var d2 = dateTo.GetText();
             if (d1 == "" || d2 == "") return;
             dde.SetText(d1 + "|" + d2);
             grd.AutoFilterByColumn(colunaFiltro, d1 + "|" + d2);
         }
         function OnDropDown(s, dateFrom, dateTo) {
             var str = s.GetText();
             if (str == "") {
                 dateFrom.SetDate(new Date(1950, 0, 1));
                 dateTo.SetDate(new Date(1960, 11, 31));
                 return;
             }
             var d = str.split("|");
             dateFrom.SetText(d[0]);
             dateTo.SetText(d[1]);
         }
         function grid_ContextMenu(s, e) {
             if (e.objectType == "header")
                 pmColumnMenu.ShowAtPos(ASPxClientUtils.GetEventX(e.htmlEvent), ASPxClientUtils.GetEventY(e.htmlEvent));
         }
    </script>
</asp:Content>
