﻿<%@ Page Title="" Theme="Glass" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="comprasSolicitacao.aspx.cs" Inherits="admin_comprasSolicitacao" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxTimer" TagPrefix="dxt" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxMenu" Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxPopupControl" Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>

<%@ Register assembly="CuteEditor" namespace="CuteEditor" tagprefix="CE" %>



<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" Runat="Server">


    <table cellpadding="0" cellspacing="0" style="width: 920px">
    <tr>
        <td class="tituloPaginas" valign="top">
            <asp:Label ID="lblAcao" runat="server">Solicitação de Compra</asp:Label>
        </td>
    </tr>
        
    <tr>
        <td>
            
    <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
        <tr>
            <td>
                <asp:TextBox runat="server" ID="hdfLista" Visible="False" />
                &nbsp;
            </td>
        </tr>
        <tr class="campos">
            <td>
                Motivo da Compra:<br/>
                <asp:TextBox runat="server" ID="txtMotivo" Width="300"></asp:TextBox>&nbsp;
            </td>
        </tr>
        <tr class="rotulos">
            <td  style="font-weight: bold; padding-top: 30px;">
                Produtos:
            </td>
        </tr>
        <tr class="campos">
            <td>
                <table style="width: 100%">
                    <tr>
                        <td>
                            Produto:&nbsp;&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="vgFornecedor" runat="server" ControlToValidate="ddlProduto" Display="Dynamic" ErrorMessage="Preencha o Produto" Font-Size="10px" SetFocusOnError="True"></asp:RequiredFieldValidator>                   
                            <br/>
                                  
                              <dxe:ASPxComboBox ID="ddlProduto"  DropDownStyle="DropDown" runat="server" ValueField="idComprasProduto" TextField="produto" 
                                   TextFormatString="{1}" ValueType="System.Int32" Width="400px"
                                EnableCallbackMode="True" IncrementalFilteringMode="Contains" 
                                OnItemsRequestedByFilterCondition="ASPxComboBox_OnItemsRequestedByFilterCondition_SQL"
                                OnItemRequestedByValue="ASPxComboBox_OnItemRequestedByValue_SQL">
                            </dxe:ASPxComboBox>
                              
                       
                            <asp:SqlDataSource ID="SqlDataSource1"  ConnectionString="<%$ ConnectionStrings:connectionString %>" runat="server" />
                            <br/>
                        </td>
                        <td>
                            Quantidade:<br/>
                            <dxe:ASPxTextBox ID="txtQuantidade" runat="server" Width="100">
                                <MaskSettings Mask="<0..999999g>" IncludeLiterals="None" />
                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" />
                            </dxe:ASPxTextBox>
                            <asp:RequiredFieldValidator ID="rqvpreco" ValidationGroup="vgFornecedor" runat="server" ControlToValidate="txtQuantidade" Display="Dynamic" ErrorMessage="Preencha a quantidade" Font-Size="10px" SetFocusOnError="True"></asp:RequiredFieldValidator>                   
                             <br/>
                        </td>
                        <td>
                    
                            <asp:Button runat="server" ID="btnGravarProduto" Text="Adicionar Produto" OnClick="btnGravarProduto_OnClick" />
                             <br/>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <dxwgv:ASPxGridView ID="grdProdutos" runat="server" AutoGenerateColumns="False" Width="834px" Cursor="auto" KeyFieldName="idComprasSolicitacaoProduto">
                    <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                    <Styles>
                        <Footer Font-Bold="True">
                        </Footer>
                    </Styles>
                    <SettingsText emptydatarow="Nenhum registro encontrado." />
                    <SettingsPager Position="Bottom" PageSize="50" ShowDisabledButtons="False" AlwaysShowPager="True">
                        <Summary AllPagesText="Páginas: {0} - {1} ({2} items)" Text="Página {0} de {1} ({2} registros encontrados)" />
                    </SettingsPager>
                    <settings showfilterrow="True" ShowGroupButtons="False" ShowHeaderFilterButton="True" ShowFooter="True" />
                    <SettingsEditing EditFormColumnCount="4" PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1" PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter" PopupEditFormWidth="700px" />
                    <Columns>
                        <dxwgv:GridViewDataTextColumn Caption="ID" FieldName="idComprasFornecedor" Visible="False" VisibleIndex="0">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Produto" FieldName="produto">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Quantidade" FieldName="quantidade">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Remover" Name="remover" Width="80">
                            <DataItemTemplate>
                                <asp:ImageButton ImageUrl="~/admin/images/btExcluir.jpg" runat="server" OnCommand="btnRemover_OnCommand" ID="btnRemover" CommandArgument='<%# Eval("idComprasSolicitacaoProduto") %>' EnableViewState="False">
                                </asp:ImageButton>
                            </DataItemTemplate>
                        </dxwgv:GridViewDataTextColumn>
                    </Columns>
                    <StylesEditors>
                        <Label Font-Bold="True">
                        </Label>
                    </StylesEditors>
                </dxwgv:ASPxGridView> 
            </td>
        </tr>
        <tr>
            <td style="text-align: right;">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td style="text-align: right;">
                <asp:Button runat="server" ID="btnGravar" Text="Gravar Solicitação de Compra" OnClick="btnGravar_OnClick"/>
            </td>
        </tr>
        
    </table>
        </td>
    </tr>
</table>

   
</asp:Content>