﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_baixaEstoque : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnBaixa_Click(object sender, EventArgs e)
    {
        var listaEtiquetas = txtNumeroEtiqueta.Text.Split(',');
        string nome = "";
        foreach (var etiquetaa in listaEtiquetas)
        {
            int idEtiqueta = 0;
            int.TryParse(etiquetaa, out idEtiqueta);
            if (idEtiqueta == 0)
            {
                AlertShow("Número de etiqueta incorreta");
                return;
            }

            var data = new dbCommerceDataContext();
            var etiqueta =
                (from c in data.tbProdutoEstoques where c.idPedidoFornecedorItem == idEtiqueta select c).FirstOrDefault();
            if (etiqueta == null)
            {
                AlertShow("Produto não localizado");
                return;
            }

            nome += etiqueta.tbProduto.produtoNome + ", ";
        }
        litProduto.Text = nome; 
        pnConfirmar.Visible = true;
        pnNumeroEtiqueta.Visible = false;
    }

    public void AlertShow(string message)
    {
        base.ClientScript.RegisterStartupScript(base.GetType(), "myalert", "alert('" + message + "'); ", true);
    }

    protected void btnConfimar_OnClick(object sender, EventArgs e)
    {
        var listaEtiquetas = txtNumeroEtiqueta.Text.Split(',');
        foreach (var etiquetaa in listaEtiquetas)
        {
            int idEtiqueta = 0;
            int.TryParse(etiquetaa, out idEtiqueta);

            var data = new dbCommerceDataContext();
            var etiqueta = (from c in data.tbProdutoEstoques where c.idPedidoFornecedorItem == idEtiqueta select c).FirstOrDefault();
            etiqueta.enviado = true;
            etiqueta.dataEnvio = DateTime.Now;
            etiqueta.tbPedidoFornecedorItem.motivo = "Produto retirado do estoque por " + rnUsuarios.retornaNomeUsuarioLogado();

            if (etiqueta.idItemPedidoEstoqueReserva != null)
            {
                var itemPedido = (from c in data.tbItemPedidoEstoques where c.idItemPedidoEstoque == etiqueta.idItemPedidoEstoqueReserva && c.cancelado == false && c.enviado == false select c).FirstOrDefault();
                if (itemPedido != null)
                {
                    itemPedido.reservado = false;
                    itemPedido.dataReserva = null;
                    string interacao = itemPedido.tbProduto.produtoNome + " retirado do estoque. Produto retornado à lista de aguardando estoque.";
                    rnInteracoes.interacaoInclui(itemPedido.tbItensPedido.pedidoId, interacao, rnUsuarios.retornaNomeUsuarioLogado(), "False");
                }
                etiqueta.idItemPedidoEstoqueReserva = null;
                etiqueta.itemPedidoIdReserva = null;
                etiqueta.pedidoIdReserva = null;
                data.SubmitChanges();
            }

            var aguardandosEnderecar = (from c in data.tbTransferenciaEnderecoProdutos where c.idPedidoFornecedorItem == idEtiqueta && c.dataEnderecamento == null select c).ToList();
            foreach(var aguardandoEnderecar in aguardandosEnderecar)
            {
                aguardandoEnderecar.dataEnderecamento = DateTime.Now;
            }
            data.SubmitChanges();

            pnConfirmar.Visible = false;
            pnNumeroEtiqueta.Visible = true;
        }
        txtNumeroEtiqueta.Text = "";
        AlertShow("Produto retirado do estoque com sucesso.");
    }
}