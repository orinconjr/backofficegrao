﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using System.Data.OleDb;
using System.Reflection;
using SpreadsheetLight;

public partial class admin_atualizarPrecosImportarPlanilhaBack041215 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //txtIpi.Attributes.Add("onkeyPress", "return moeda(this,'.',',',event);");
            //txtFrete.Attributes.Add("onkeyPress", "return moeda(this,'.',',',event);");
            //txtDesconto.Attributes.Add("onkeyPress", "return moeda(this,'.',',',event);");
            txtDataInicial.Attributes.Add("onkeypress", "Mascaras('DATA', '" + txtDataInicial.ClientID + "');");
            txtDataFinal.Attributes.Add("onkeypress", "Mascaras('DATA', '" + txtDataFinal.ClientID + "');");

            ddlExportarPlanilha.Items.Insert(0, new ListItem("Todos Fornecedores", "0"));
        }

    }
    //protected void btnImportar_Click(object sender, EventArgs e)
    //{
    //    string caminho = MapPath("~/");

    //    if (File.Exists(caminho + "planilhaPrecosFornecedor" + Path.GetExtension(fupPlanilha.FileName).ToLower()))
    //        File.Delete(caminho + "planilhaPrecosFornecedor" + Path.GetExtension(fupPlanilha.FileName).ToLower());

    //    fupPlanilha.SaveAs(caminho + "planilhaPrecosFornecedor" + Path.GetExtension(fupPlanilha.FileName).ToLower());

    //    string connStr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + caminho + "planilhaPrecosFornecedor" + Path.GetExtension(fupPlanilha.FileName).ToLower() + ";Extended Properties=Excel 12.0;";

    //    OleDbConnection cnnExcel = new OleDbConnection(connStr);
    //    OleDbCommand cmmPlan = null;
    //    OleDbDataReader drPlan = null;

    //    System.Text.StringBuilder resultado = new System.Text.StringBuilder();

    //    resultado.Append("<table>");
    //    resultado.Append("<tr><td>produtoId</td><td>produtoIdEmpresa</td><td>produtoNome<td></td><td></td><td></td><td></td></tr>");

    //    try
    //    {
    //        cnnExcel.Open();
    //        cmmPlan = new OleDbCommand("select * from [Plan2$]", cnnExcel);
    //        drPlan = cmmPlan.ExecuteReader(CommandBehavior.CloseConnection);

    //        decimal valorIPI = decimal.TryParse(txtIpi.Text, out valorIPI) ? valorIPI : 0;
    //        decimal valorFrete = decimal.TryParse(txtFrete.Text, out valorFrete) ? valorFrete : 0;
    //        decimal valorDesconto = decimal.TryParse(txtDesconto.Text, out valorDesconto) ? valorDesconto : 0;
    //        int qtdProdutosAtualizados = 0;
    //        int qtdProdutosNaPlanilha = 0;
    //        string fornecedorId = ddlFornecedores.SelectedValue;

    //        var dataOriginal = new dbCommerceDataContext();

    //        using (var data = new dbCommerceDataContext())
    //        {

    //            while (drPlan.Read())
    //            {

    //                try
    //                {
    //                    if (!String.IsNullOrEmpty(drPlan[0].ToString()))
    //                    {
    //                        int produtoId = Int32.TryParse(drPlan[0].ToString(), out produtoId) ? produtoId : 0;
    //                        string produtoIdEmpresa = drPlan[1].ToString();
    //                        string produtoNome = drPlan[2].ToString();
    //                        string produtoPreco = drPlan[3].ToString().Replace("R$", "").Replace(".", "");
    //                        string produtoValorCusto = drPlan[4].ToString().Replace("R$", "").Replace(".", "");
    //                        decimal valorPlanilha = Convert.ToDecimal(produtoPreco.Replace(".", ","));
    //                        decimal valorFinal = Convert.ToDecimal(produtoValorCusto); //Convert.ToDecimal(produtoPreco.Replace(".", ","));

    //                        string produtoMargemDeLucro = drPlan[5].ToString().Replace("R$", "").Replace(".", "").Replace(",", ".");
    //                        decimal margemdeLucro = Convert.ToDecimal(produtoMargemDeLucro.Replace(".", ","));

    //                        if (!String.IsNullOrEmpty(produtoPreco))
    //                        {

    //                            qtdProdutosNaPlanilha++;

    //                            if (valorDesconto > 0)
    //                                valorFinal += (valorFinal * (valorDesconto / 100));

    //                            if (valorIPI > 0)
    //                                valorFinal += (valorFinal * (valorIPI / 100));

    //                            if (valorFrete > 0)
    //                                valorFinal += (valorFinal * (valorFrete / 100));

    //                            if (produtoId > 0)
    //                            {

    //                                var produto = (from c in data.tbProdutos where c.produtoId == produtoId select c).FirstOrDefault();

    //                                if (produto != null)
    //                                {
    //                                    var produtoOriginal = (from c in dataOriginal.tbProdutos where c.produtoId == produtoId select c).First();

    //                                    if (margemdeLucro > 0)
    //                                    {
    //                                        produto.produtoPrecoDeCusto = valorFinal;
    //                                        produto.margemDeLucro = margemdeLucro;
    //                                        produto.produtoPreco = Convert.ToDecimal(produtoPreco);
    //                                        //if (produtoId == 21974)
    //                                        //{

    //                                        data.SubmitChanges();
    //                                    }



    //                                    var dataAlterado = new dbCommerceDataContext();
    //                                    var produtoAlterado = (from c in dataAlterado.tbProdutos where c.produtoId == produtoId select c).First();
    //                                    var log = new rnLog();
    //                                    log.usuario = rnUsuarios.retornaNomeUsuarioLogado();
    //                                    PropertyInfo[] propriedadesOriginal = produtoOriginal.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);
    //                                    PropertyInfo[] propriedadesAlterado = produtoAlterado.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);
    //                                    foreach (var propriedadeOriginal in propriedadesOriginal)
    //                                    {
    //                                        if (propriedadeOriginal.PropertyType.BaseType != null)
    //                                        {
    //                                            var propriedadeAlterado = propriedadesAlterado.Where(x => x.Name == propriedadeOriginal.Name).FirstOrDefault();
    //                                            if (propriedadeAlterado != null)
    //                                            {
    //                                                try
    //                                                {
    //                                                    string valorPropriedadeOriginal = propriedadeOriginal.GetValue(produtoOriginal, null).ToString();
    //                                                    string valorPropriedadeAlterado = propriedadeAlterado.GetValue(produtoAlterado, null).ToString();
    //                                                    if (valorPropriedadeOriginal != valorPropriedadeAlterado)
    //                                                    {
    //                                                        log.descricoes.Add(propriedadeOriginal.Name + " alterado de " + propriedadeOriginal.GetValue(produtoOriginal, null) + " para " + propriedadeAlterado.GetValue(produtoAlterado, null));
    //                                                    }
    //                                                }
    //                                                catch (Exception)
    //                                                {

    //                                                }
    //                                            }
    //                                        }
    //                                    }
    //                                    var tipoRelacionado = new rnEnums.TipoRelacionadoIds() { idRegistroRelacionado = produtoId, tipoRelacionado = rnEnums.TipoRegistroRelacionado.Produto };
    //                                    log.tiposRelacionados.Add(tipoRelacionado);
    //                                    log.tiposOperacao.Add(rnEnums.TipoOperacao.Produto);
    //                                    log.InsereLog();
    //                                    //}

    //                                    resultado.Append("<tr><td align=\"center\">" + produtoId + "&nbsp;&nbsp;</td><td>" + produtoIdEmpresa + "&nbsp;&nbsp;</td><td>" + produtoNome + "<td>valor na planilha:</td><td>" + Convert.ToDecimal(valorPlanilha).ToString("C", System.Globalization.CultureInfo.CurrentCulture) + "</td><td>valor final:</td><td>" + valorFinal.ToString("C", System.Globalization.CultureInfo.CurrentCulture) + "</td></tr>");

    //                                    qtdProdutosAtualizados++;
    //                                }
    //                            }
    //                        }
    //                    }
    //                }
    //                catch (Exception ex)
    //                {
    //                    ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('" + ex.Message + "');", true);
    //                }
    //            }
    //        }

    //        resultado.Append("</table>");
    //        litProdutosAtualizados.Text = resultado.ToString();

    //        ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('Atualização realizada com sucesso!\\n" + qtdProdutosAtualizados.ToString() + "/" + qtdProdutosNaPlanilha.ToString() + " produtos do fornecedor " + ddlFornecedores.SelectedItem.Text + " foram atualizados!');", true);
    //    }
    //    catch (Exception ex)
    //    {

    //        ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('" + ex.Message + "');", true);
    //    }
    //    finally
    //    {
    //        cnnExcel.Close();
    //        cnnExcel.Dispose();
    //    }

    //}

    protected void btnImportar_Click(object sender, EventArgs e)
    {
        string caminho = MapPath("~/");

        if (File.Exists(caminho + "planilhaPrecosFornecedor" + Path.GetExtension(fupPlanilha.FileName).ToLower()))
            File.Delete(caminho + "planilhaPrecosFornecedor" + Path.GetExtension(fupPlanilha.FileName).ToLower());

        fupPlanilha.SaveAs(caminho + "planilhaPrecosFornecedor" + Path.GetExtension(fupPlanilha.FileName).ToLower());

        string connStr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + caminho + "planilhaPrecosFornecedor" + Path.GetExtension(fupPlanilha.FileName).ToLower() + ";Extended Properties=Excel 12.0;";

        OleDbConnection cnnExcel = new OleDbConnection(connStr);
        OleDbCommand cmmPlan = null;
        OleDbDataReader drPlan = null;

        System.Text.StringBuilder resultado = new System.Text.StringBuilder();

        resultado.Append("<table>");
        resultado.Append("<tr><td>produtoId</td><td>produtoIdEmpresa</td><td>produtoNome<td></td><td></td><td></td><td></td></tr>");

        try
        {
            cnnExcel.Open();
            cmmPlan = new OleDbCommand("select * from [Sheet1$]", cnnExcel);
            drPlan = cmmPlan.ExecuteReader(CommandBehavior.CloseConnection);

            int qtdProdutosAtualizados = 0;
            int qtdProdutosNaPlanilha = 0;
         
            var dataOriginal = new dbCommerceDataContext();

            using (var data = new dbCommerceDataContext())
            {

                while (drPlan.Read())
                {
     
                    try
                    {
                        if (!String.IsNullOrEmpty(drPlan[0].ToString()))
                        {
                            int produtoId = Int32.TryParse(drPlan["produtoId"].ToString(), out produtoId) ? produtoId : 0;

                            decimal preco = 0;
                            string produtoPreco = "";
                            bool tempreco = false;
                            try
                            {
                               
                                produtoPreco = drPlan["produtoPreco"].ToString().Replace("R$", "").Replace(".", "");
                                preco = Convert.ToDecimal(produtoPreco.Replace(".", ","));
                                preco = Math.Round(preco, 1);
                                tempreco = true;
                            }
                            catch (Exception)
                            {
                               
                            }
                     


                            decimal precoDeCusto = 0;
                            bool temprecoDeCusto = false;
                            try
                            {
                                string produtoPrecoDeCusto = drPlan["produtoPrecoDeCusto"].ToString().Replace("R$", "").Replace(".", "");
                                precoDeCusto = Convert.ToDecimal(produtoPrecoDeCusto.Replace(".", ","));
                                temprecoDeCusto = true;
                            }
                            catch (Exception)
                            {
                                
                                
                            }
                           
                          

                            decimal precoPromocional = 0;
                            bool temprecoPromocional = false;
                            try
                            {
                                string produtoPrecoPromocional = drPlan["produtoPrecoPromocional"].ToString().Replace("R$", "").Replace(".", "");
                                precoPromocional = Convert.ToDecimal(produtoPrecoPromocional.Replace(".", ","));
                                precoPromocional = Math.Round(precoPromocional, 1);
                                temprecoPromocional = true;
                            }
                            catch (Exception)
                            {


                            }

                          
                            decimal margemdeLucro = 0;
                            bool temmargemdeLucro = false;
                            try
                            {
                                string produtoMargemDeLucro = drPlan["margemDeLucro"].ToString().Replace("R$", "").Replace(".", "");
                                margemdeLucro = Convert.ToDecimal(produtoMargemDeLucro.Replace(".", ","));
                                temmargemdeLucro = true;
                            }
                            catch (Exception)
                            {
                               
                            }


                            string sprodutoAtivo = "";
                            bool temsprodutoAtivo = false;
                            try
                            {
                                sprodutoAtivo = drPlan["produtoAtivo"].ToString();
                                temsprodutoAtivo = true;
                            }
                            catch (Exception)
                            {
                                
                            }
                           
                      

                            if (!String.IsNullOrEmpty(produtoPreco))
                            {

                                qtdProdutosNaPlanilha++;

                                if (produtoId > 0)
                                {

                                    var produto = (from c in data.tbProdutos where c.produtoId == produtoId select c).FirstOrDefault();

                                    if (produto != null)
                                    {
                                        var produtoOriginal = (from c in dataOriginal.tbProdutos where c.produtoId == produtoId select c).First();

                                        if (ckbprodutoPreco.Checked && tempreco)
                                            produto.produtoPreco = preco;

                                        if (ckbprodutoPrecoDeCusto.Checked && temprecoDeCusto)
                                            produto.produtoPrecoDeCusto = precoDeCusto;

                                        if (ckbprodutoPrecoPromocional.Checked && temprecoPromocional)
                                            produto.produtoPrecoPromocional = precoPromocional;

                                        if (ckbmargemDeLucro.Checked && temmargemdeLucro)
                                            produto.margemDeLucro = margemdeLucro;

                                        if (ckbprodutoAtivo.Checked && temsprodutoAtivo)
                                            produto.produtoAtivo = sprodutoAtivo;

                                        data.SubmitChanges();

                                        var dataAlterado = new dbCommerceDataContext();
                                        var produtoAlterado = (from c in dataAlterado.tbProdutos where c.produtoId == produtoId select c).First();
                                        var log = new rnLog();
                                        log.usuario = rnUsuarios.retornaNomeUsuarioLogado();
                                        PropertyInfo[] propriedadesOriginal = produtoOriginal.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);
                                        PropertyInfo[] propriedadesAlterado = produtoAlterado.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);
                                        foreach (var propriedadeOriginal in propriedadesOriginal)
                                        {
                                            if (propriedadeOriginal.PropertyType.BaseType != null)
                                            {
                                                var propriedadeAlterado = propriedadesAlterado.Where(x => x.Name == propriedadeOriginal.Name).FirstOrDefault();
                                                if (propriedadeAlterado != null)
                                                {
                                                    try
                                                    {
                                                        string valorPropriedadeOriginal = propriedadeOriginal.GetValue(produtoOriginal, null).ToString();
                                                        string valorPropriedadeAlterado = propriedadeAlterado.GetValue(produtoAlterado, null).ToString();
                                                        if (valorPropriedadeOriginal != valorPropriedadeAlterado)
                                                        {
                                                            log.descricoes.Add(propriedadeOriginal.Name + " alterado de " + propriedadeOriginal.GetValue(produtoOriginal, null) + " para " + propriedadeAlterado.GetValue(produtoAlterado, null));
                                                        }
                                                    }
                                                    catch (Exception)
                                                    {

                                                    }
                                                }
                                            }
                                        }
                                        var tipoRelacionado = new rnEnums.TipoRelacionadoIds() { idRegistroRelacionado = produtoId, tipoRelacionado = rnEnums.TipoRegistroRelacionado.Produto };
                                        log.tiposRelacionados.Add(tipoRelacionado);
                                        log.tiposOperacao.Add(rnEnums.TipoOperacao.Produto);
                                        log.InsereLog();
                                        //}

                                        resultado.Append("<tr><td align=\"center\">" + produtoId + "&nbsp;&nbsp;</td><td>" + produto.produtoIdDaEmpresa + "&nbsp;&nbsp;</td><td>" + produto.produtoNome + "<td>valor na planilha:</td><td>" + Convert.ToDecimal(produtoOriginal.produtoPreco).ToString("C", System.Globalization.CultureInfo.CurrentCulture) + "</td><td>valor final:</td><td>" + produto.produtoPreco.ToString("C", System.Globalization.CultureInfo.CurrentCulture) + "</td></tr>");

                                        qtdProdutosAtualizados++;
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('" + ex.Message + "');", true);
                    }
                }
            }

            resultado.Append("</table>");
            litProdutosAtualizados.Text = resultado.ToString();

            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('Atualização realizada com sucesso!\\n" + qtdProdutosAtualizados.ToString() + "/" + qtdProdutosNaPlanilha.ToString() + " foram atualizados!');", true);
        }
        catch (Exception ex)
        {

            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('" + ex.Message + "');", true);
        }
        finally
        {
            cnnExcel.Close();
            cnnExcel.Dispose();
        }

    }
    //protected void btnExportar_Click(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        int fornecedor = Convert.ToInt32(ddlExportarPlanilha.SelectedValue);
    //        //Conteúdo do Response
    //        //Response.AddHeader("content-disposition", "attachment; filename=" + ddlExportarPlanilha.SelectedItem.Text.Replace(" ", "").Trim() + ".xls");
    //        //Response.Charset = "";
    //        //Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

    //        Response.AddHeader("content-disposition", "attachment; filename= " + ddlExportarPlanilha.SelectedItem.Text.Replace(" ", "").Trim() + ".xls");
    //        Response.Charset = "";
    //        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

    //        //Objetos
    //        StringWriter stringWrite = new System.IO.StringWriter();
    //        HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);

    //        //Inclui controles 
    //        //DataGrid dgDados = new DataGrid();
    //        GridView dgDados = new GridView();
    //        //this.form1.Controls.Add(dgDados);

    //        using (var data = new dbCommerceDataContext())
    //        {



    //            var dados = (from p in data.tbProdutos
    //                         select new
    //                         {
    //                             p.produtoId,
    //                             p.produtoIdDaEmpresa,
    //                             p.produtoNome,
    //                             p.produtoPreco,
    //                             p.produtoPrecoDeCusto,
    //                             p.produtoPrecoPromocional,
    //                             p.margemDeLucro,
    //                             p.produtoFornecedor,
    //                             p.produtoAtivo,
    //                             p.produtoDataDaCriacao,
    //                             p.complementoIdDaEmpresa
    //                         });

    //            if (fornecedor != 0)
    //            {
    //                dados = dados.Where(x => x.produtoFornecedor == fornecedor);
    //            }

    //            dados = ckbAtivo.Checked ? dados.Where(x => x.produtoAtivo.ToLower().Contains("true")) : dados.Where(x => x.produtoAtivo.ToLower().Contains("false"));

    //            if (txtDataInicial.Text != "" && txtDataFinal.Text != "")
    //            {
    //                var dtInicial = Convert.ToDateTime(txtDataInicial.Text);
    //                var dtFinal = Convert.ToDateTime(txtDataFinal.Text);

    //                dados =
    //                    dados.Where(x => x.produtoDataDaCriacao.Date >= dtInicial.Date && x.produtoDataDaCriacao.Date <= dtFinal.Date);
    //            }
    //            //Definição de cores
    //            dgDados.DataSource = dados;
    //            dgDados.DataBind();

    //            //Renderiza o DataGrid
    //            dgDados.RenderControl(htmlWrite);
    //        }

    //        //Exporta 
    //        Response.Write(stringWrite.ToString());
    //        //encerra
    //        Response.End();
    //    }
    //    catch (Exception ex)
    //    {

    //        throw;
    //    }
    //}

    protected void btnExportar_Click(object sender, EventArgs e)
    {
        try
        {
            int fornecedor = Convert.ToInt32(ddlExportarPlanilha.SelectedValue);
         
            SLDocument sl = new SLDocument();
            SLStyle style1 = sl.CreateStyle();
            style1.Font.Bold = true;
            sl.SetRowStyle(1, style1);

            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("Content-Disposition", "attachment; filename=" + ddlExportarPlanilha.SelectedItem.Text.Replace(" ", "").Trim() + ".xlsx");

            sl.SetCellValue("A1", "produtoId");
            sl.SetCellValue("B1", "produtoIdDaEmpresa");
            sl.SetCellValue("C1", "produtoNome");
            sl.SetCellValue("D1", "produtoPreco");
            sl.SetCellValue("E1", "produtoPrecoDeCusto");
            sl.SetCellValue("F1", "produtoPrecoPromocional");
            sl.SetCellValue("G1", "margemDeLucro");
            sl.SetCellValue("H1", "produtoFornecedor");
            sl.SetCellValue("I1", "produtoAtivo");
            sl.SetCellValue("J1", "produtoDataDaCriacao");
            sl.SetCellValue("K1", "complementoIdDaEmpresa");

            using (var data = new dbCommerceDataContext())
            {

                var dados = (from p in data.tbProdutos
                             select new
                             {
                                 p.produtoId,
                                 p.produtoIdDaEmpresa,
                                 p.produtoNome,
                                 p.produtoPreco,
                                 p.produtoPrecoDeCusto,
                                 p.produtoPrecoPromocional,
                                 p.margemDeLucro,
                                 p.produtoFornecedor,
                                 p.produtoAtivo,
                                 p.produtoDataDaCriacao,
                                 p.complementoIdDaEmpresa
                             });

                if (fornecedor != 0)
                {
                    dados = dados.Where(x => x.produtoFornecedor == fornecedor);
                }

                dados = ckbAtivo.Checked ? dados.Where(x => x.produtoAtivo.ToLower().Contains("true")) : dados.Where(x => x.produtoAtivo.ToLower().Contains("false"));

                if (txtDataInicial.Text != "" && txtDataFinal.Text != "")
                {
                    var dtInicial = Convert.ToDateTime(txtDataInicial.Text);
                    var dtFinal = Convert.ToDateTime(txtDataFinal.Text);

                    dados =
                        dados.Where(x => x.produtoDataDaCriacao.Date >= dtInicial.Date && x.produtoDataDaCriacao.Date <= dtFinal.Date);
                }
                int linha = 2;
                foreach (var item in dados)
                {
                    sl.SetCellValue(linha, 1, item.produtoId);
                    sl.SetCellValue(linha, 2, item.produtoIdDaEmpresa);
                    sl.SetCellValue(linha, 3, item.produtoNome);
                    sl.SetCellValue(linha, 4, item.produtoPreco);
                    sl.SetCellValue(linha, 5, item.produtoPrecoDeCusto.ToString());
                    sl.SetCellValue(linha, 6, item.produtoPrecoPromocional.ToString());
                    sl.SetCellValue(linha, 7, item.margemDeLucro);
                    sl.SetCellValue(linha, 8, item.produtoFornecedor);
                    sl.SetCellValue(linha, 9, item.produtoAtivo);
                    sl.SetCellValue(linha, 10, item.produtoDataDaCriacao.ToString());
                    sl.SetCellValue(linha, 11, item.complementoIdDaEmpresa);
                    linha++;
                }
            }

            //Exporta 
            sl.SaveAs(Response.OutputStream);
            Response.End();
        }
        catch (Exception ex)
        {

            throw;
        }
    }
}