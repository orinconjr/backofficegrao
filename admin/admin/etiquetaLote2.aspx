﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="etiquetaLote2.aspx.cs" Inherits="admin_etiquetaLote2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>
        <% =ConfigurationManager.AppSettings["nomeDoSite"]%></title>
    <link href="estilos/estilos.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .style1
        {
            width: 635px;
        }
        .tahoma-16-000000-b
        {
            font-family: Tahoma;
            font-size: 16px;
            color: #000000;
            text-decoration: none;
            font-weight: bold;
            width: 234px;
        }
        .tahoma-14-000000
        {
            font-family: Tahoma;
            font-size: 14px;
            color: #000000;
            text-decoration: none;
        }
        .style2
        {
            font-family: Tahoma;
            font-size: 16px;
            color: #000000;
            text-decoration: none;
            font-weight: bold;
        }
        .style3
        {
            font-family: Tahoma;
            font-size: 14px;
            color: #000000;
            text-decoration: none;
        }
        .style4
        {
            font-family: Tahoma;
            font-size: 16px;
            color: #000000;
            text-decoration: none;
            font-weight: bold;
            width: 73px;
        }
        .style5
        {
            font-family: Tahoma;
            font-size: 14px;
            color: #000000;
            text-decoration: none;
            width: 73px;
        }
        .style12
        {
            font-family: Tahoma;
            font-size: 16px;
            color: #000000;
            text-decoration: none;
            font-weight: bold;
            width: 73px;
        }
        .style13
        {
            font-family: Tahoma;
            font-size: 14px;
            color: #000000;
            text-decoration: none;
            width: 75px;
        }
        .style16
        {
            width: 635px;
        }
        .style15
        {
            font-family: Tahoma;
            font-size: 16px;
            color: #000000;
            text-decoration: none;
        }
        .style16
        {
            width: 635px;
        }
        .style17
        {
            width: 254px;
        }
        .style21
        {
            width: 665px;
        }
        .style26
        {
            font-family: Tahoma;
            font-size: 16px;
            color: #000000;
            text-decoration: none;
            font-weight: bold;
            width: 88px;
        }
        .style27
        {
            font-family: Tahoma;
            font-size: 16px;
            color: #000000;
            text-decoration: none;
            font-weight: bold;
            width: 78px;
        }
        .style28
        {
            font-family: Tahoma;
            font-size: 11px;
            color: #000000;
            width: 153px;
        }
        .break {
            page-break-after: always;
        }
    </style>
</head>
<body style="background-color: White" onload=window.print();>
    <form id="form1" runat="server">
    <div>       
                <asp:ObjectDataSource ID="sqlPedidos" runat="server" SelectMethod="admin_pedidosCompletosSeparacaoEstoque2" TypeName="rnPedidos">
                </asp:ObjectDataSource>    
        <asp:ListView runat="server" ID="lstPedidos" OnItemDataBound="lstPedidos_OnItemDataBound" DataSourceID="sqlPedidos">
            <ItemTemplate>

                 <table cellpadding="0" cellspacing="0" width="635" class="break">
        <tr>
            <td>
                <table cellpadding="0" cellspacing="0" width="635">
                    <tr>
                        <td style="font-weight: bold; font-size: 16px;">
                            <span class="style15"><asp:Label ID="lblEnviarPor" runat="server" Text='<%#Eval("entregaEnviarPor") %>'></asp:Label><br/>&nbsp;</span><br/>
                            <span class="style15">Pedido: <asp:Label ID="lblPedidoIdInterno" runat="server" Text='<%#Eval("pedidoId") %>'></asp:Label><br/>&nbsp;</span>
                        </td>
                    </tr>
                    <tr style="display: none;">
                        <td>
                            <span class="style15">Pedido Interno: <asp:Label ID="lblPedidoId" runat="server" Text='<%#Eval("pedidoId") %>'></asp:Label><br/>&nbsp;</span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span class="style15">
                                Tel. Residencial: <asp:Label ID="lblTelResidencial" runat="server"></asp:Label><br/>
                                Tel. Comercial: <asp:Label ID="lblTelComercial" runat="server"></asp:Label><br/>
                                Celular: <asp:Label ID="lblTelCelular" runat="server"></asp:Label><br/><br/>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td style="border: 1px solid #000000; padding-left: 10px;">
                            <span class="tahoma-16-000000-b">Destinatário:</span>
                        </td>
                    </tr>
                    <tr >
                        <td class="style16" valign="top" style="border: 1px solid #000000; border-top: 0;  padding-left: 10px;">
                            &nbsp;<br/>
                            <span class="style15"><asp:Label ID="lblNomeDoClienteEntrega1" runat="server"></asp:Label><br />
                                <asp:Label ID="lblRuaEntrega1" runat="server"></asp:Label> <asp:Label ID="lblNumeroEntrega1" runat="server"></asp:Label>, <asp:Label ID="lblComplementoEntrega1" runat="server"></asp:Label><br />
                                <asp:Label ID="lblBairroEntrega1" runat="server"></asp:Label> - <asp:Label ID="lblCidadeEntrega1" runat="server"></asp:Label> - <asp:Label ID="lblEstadoEntrega1" runat="server"></asp:Label><br />
                                <asp:Label ID="lblPaisEntrega1" runat="server" visible="false"></asp:Label>
                                <asp:Label ID="lblCepEntrega1" runat="server"></asp:Label><br />
                                <asp:Label ID="lblReferenciaParaEntregaEntrega1" runat="server"></asp:Label></span><br/>&nbsp;
                        </td>
                    </tr>
                    <tr><td>
                            &nbsp;<br/>
                            &nbsp;<br/>
                            &nbsp;<br/>
                        </td></tr>
                    <tr>
                        <td class="style16"  style="border: 1px solid #000000;">
                            <span class="style15">
                            &nbsp;<br/>Itens do Pedido:</span><br/>
                            <asp:DataList ID="dtlItensPedido" runat="server" CellPadding="0" Width="100%" OnItemDataBound="DataList1_ItemDataBound">
                                                        <ItemTemplate>
                                                            <table cellpadding="0" cellspacing="0" class="rotulos" style="width: 100%">
                                                                <tr>
                                                                    <td>
                                                                        <table cellpadding="0" cellspacing="0">
                                                                            <tr>
                                                                                <td class="textoPreto" style="padding-left: 10px">
                                                                                    <asp:HiddenField ID="txtItemPedidoId" runat="server" Value='<%# Bind("itemPedidoId") %>' />
                                                                                    <asp:Label ID="lblProdutoNome" runat="server" Text='<%# Bind("produtoNome") %>'></asp:Label>
                                                                                    <br />
                                                                                    <b>Id da empresa:</b>
                                                                                    <asp:Label ID="lblProdutoIdEmpresa" runat="server" 
                                                                                        Text='<%# Bind("produtoIdDaEmpresa") %>'></asp:Label>
                                                                                    <br />
                                                                                    <b>Código de Barras:</b>
                                                                                    <asp:Label ID="lblCodigoDeBarras" runat="server" 
                                                                                        Text='<%# Bind("produtoIdDaEmpresa") %>'></asp:Label>
                                                                                    <br />
                                                                                    <b>Fornecedor:</b>
                                                                                    <asp:Label ID="lblFornecedor" runat="server" Text='<%# Bind("fornecedorNome") %>'></asp:Label><br />
                                                                                    <b>Quantidade:</b> <asp:Label ID="lblQuantidade" runat="server" Text='<%# Bind("itemQuantidade") %>'></asp:Label><br/>
                                                                                    <%--<b>Preço de Custo:</b> <asp:Label ID="lblPrecoDeCusto" runat="server"></asp:Label> - 
                                                                                    <b>Preço de Venda:</b> <asp:Label ID="lblPrecoDeVenda" runat="server"></asp:Label> - 
                                                                                    <b>Margem:</b> <asp:Label ID="lblMargem" runat="server"></asp:Label>--%>
                                                                                    <div style="padding-left: 30px;">
                                                                                        <asp:ListView runat="server" ID="lstCombo">
                                                                                            <ItemTemplate><br/>
                                                                                                    <asp:Label ID="lblProdutoNome" runat="server" Text='<%# Bind("produtoNome") %>'></asp:Label>
                                                                                                    <br />
                                                                                                    <b>Id da empresa:</b>
                                                                                                    <asp:Label ID="lblProdutoIdEmpresa" runat="server" 
                                                                                                        Text='<%# Bind("produtoIdDaEmpresa") %>'></asp:Label>
                                                                                                    <br />
                                                                                                    <b>Código de Barras:</b>
                                                                                                    <asp:Label ID="lblCodigoDeBarras" runat="server" 
                                                                                                        Text='<%# Bind("produtoIdDaEmpresa") %>'></asp:Label>
                                                                                                    <br />
                                                                                                    <b>Fornecedor:</b>
                                                                                                    <asp:Label ID="lblFornecedor" runat="server" Text='<%# Bind("fornecedorNome") %>'></asp:Label><br />

                                                                                            </ItemTemplate>
                                                                                        </asp:ListView>
                                                                                   </div>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="6">
                                                                        <hr color="#7EACB1" size="1" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ItemTemplate>
                                                        <HeaderTemplate>
                                                            
                                                        </HeaderTemplate>
                                                    </asp:DataList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            
                                                    
                        <asp:ObjectDataSource ID="sqlItensPedido" runat="server" SelectMethod="itensPedidoSelecionaAdmin_PorPedidoId" TypeName="rnPedidos">
                        </asp:ObjectDataSource>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                
            </td>
        </tr>
    </table>
                

            </ItemTemplate>
        </asp:ListView>
    </div>
    </form>
</body>
</html>
