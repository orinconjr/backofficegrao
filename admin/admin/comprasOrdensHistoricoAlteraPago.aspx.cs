﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_comprasOrdensHistoricoAlteraPago : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            int idComprasOrdem = Convert.ToInt32(Request.QueryString["idComprasOrdem"]);
            bool checado = Convert.ToBoolean(Request.QueryString["checado"]);
            var data = new dbCommerceDataContext();
            var linha = (from c in data.tbComprasOrdems where c.idComprasOrdem == idComprasOrdem select c).First();
            linha.icPago = Convert.ToBoolean(checado);
            data.SubmitChanges();
            Response.Write("Ok");
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
            
        }
      
    
    }

}