﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_logEtiqueta : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnFiltrar_Click(object sender, EventArgs e)
    {

        if (String.IsNullOrEmpty(txtiditempedido.Text))
        {
            lblerrofiltro.Text = "Informe a Etiqueta";

        }
        else
        {
            lblerrofiltro.Text = "";
            carregaGrid();
        }

    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        carregaGrid();
    }
    void carregaGrid()
    {
        var data = new dbCommerceDataContext();
        int etiqueta = Convert.ToInt32(txtiditempedido.Text);
        var logs = (from lrr in data.tbLogRegistroRelacionados
                    join ld in data.tbLogDescricaos on lrr.tbLog.idLog equals ld.idLog
                    join relacionados in data.tbLogRegistroRelacionados on lrr.idLog equals relacionados.idLog
                    where lrr.idRegistroRelacionado == etiqueta &&
                    lrr.tipoRegistroRelacionado == 8
                    select new
                    {
                        lrr.idLogRegistroRelacionado,
                        lrr.tbLog.data,
                        lrr.tbLog.usuario,
                        ld.descricao,
                        lrr.tbLog.idLog,
                        registro = relacionados.tipoRegistroRelacionado == 8 ? 
                        "etiqueta " + relacionados.idRegistroRelacionado : relacionados.tipoRegistroRelacionado == 2 ? 
                        "Pedido " + relacionados.idRegistroRelacionado : ""
                    }).OrderBy(x => x.idLog).ToList();
        GridView1.DataSource = logs;
        GridView1.DataBind();
    }
}