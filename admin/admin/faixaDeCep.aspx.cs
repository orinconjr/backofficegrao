﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxTabControl;
using DevExpress.Web.ASPxEditors;
using DevExpress.Data;
using DevExpress.Web.ASPxUploadControl;
using System.IO;
using System.Collections.Generic;

public partial class admin_faixaDeCep : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    //protected void btPdf_Click(object sender, ImageClickEventArgs e)
    //{
    //    this.grdEx.WritePdfToResponse();
    //}
    //protected void btXsl_Click(object sender, ImageClickEventArgs e)
    //{
    //    this.grdEx.WriteXlsToResponse();
    //}
    //protected void btRtf_Click(object sender, ImageClickEventArgs e)
    //{
    //    this.grdEx.WriteRtfToResponse();
    //}
    //protected void btCsv_Click(object sender, ImageClickEventArgs e)
    //{
    //    this.grdEx.WriteCsvToResponse();
    //}

    protected void grd_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        //if (e.RowType == GridViewRowType.Data) return;
        //{
        //    ASPxTextBox txtPrazo = (ASPxTextBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["prazo"], "txtPrazo");
        //    txtPrazo.Attributes.Add("onkeypress", "return soNumero(event);");

        //    //(GridViewDataColumn)grd.Columns["prazo"]

        //    //e.Row.Cells[e.VisibleIndex].Attributes.Add("onkeypress", "return soNumero(event);");
        //}
    }

    protected void grd_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        if (rnFaixaDeCep.faixaDeCepSeleciona_PorTipoDeEntregaId_FaixaInicial_FaixaFinal(int.Parse(e.NewValues["tipodeEntregaId"].ToString()), int.Parse(e.NewValues["faixaInicial"].ToString()), int.Parse(e.NewValues["faixaFinal"].ToString())).Tables[0].Rows.Count > 0)
        {
            throw new InvalidOperationException("Já existe um cep inicial ou final para essa faixa.");
            e.Cancel = true;
        }
    }
    protected void grd_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        if (rnFaixaDeCep.faixaDeCepSeleciona_PorFaiaxaDeCepIdDiferenteTipoDeEntregaId_FaixaInicial_FaixaFinal(int.Parse(e.OldValues["faixaDeCepId"].ToString()), int.Parse(e.NewValues["tipodeEntregaId"].ToString()), int.Parse(e.NewValues["faixaInicial"].ToString()), int.Parse(e.NewValues["faixaFinal"].ToString())).Tables[0].Rows.Count > 0)
        {
            throw new InvalidOperationException("Já existe um cep inicial ou final para essa faixa.");
            e.Cancel = true;
        }
    }
}
