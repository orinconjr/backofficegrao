﻿<%@ Page Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="produtoCad.aspx.cs" Inherits="admin_produtoCad" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dxwgv" %>
<%@ Register assembly="CuteEditor" namespace="CuteEditor" tagprefix="CE" %>
<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" Runat="Server">
    <script type="text/javascript">
        function GetClientId(strid) {
            var count = document.forms[0].length;
            var i = 0;
            var eleName;
            for (i = 0; i < count; i++) {
                eleName = document.forms[0].elements[i].id;
                pos = eleName.indexOf(strid);
                if (pos >= 0) break;
            }
            return eleName;
        }

        function CalculoMargemConcorrencia(txtCustoConcorrencia, lblCustoConcorrencia, txtCusto) {
            var obj_txtCustoConcorrencia = document.getElementById(txtCustoConcorrencia);
            var obj_lblCustoConcorrencia = document.getElementById(lblCustoConcorrencia);
            var obj_txtCusto = document.getElementById(txtCusto);
            var valorConcorrencia = parseFloat(obj_txtCustoConcorrencia.value.replace(',', '.'));
            var valorCusto = parseFloat(obj_txtCusto.value.replace('.', '').replace(',', '.'));
            obj_lblCustoConcorrencia.innerHTML = (((valorConcorrencia * 100) / valorCusto) - 100).toFixed(1) + '%';
        }

        function CalculoMargemVenda(txtPrecoPromocional, lblPrecoPromocional, lblPrecoVenda, txtCusto) {
            var obj_txtPrecoPromocional = document.getElementById(txtPrecoPromocional);
            var obj_lblPrecoPromocional = document.getElementById(lblPrecoPromocional);
            var obj_lblPrecoVenda = document.getElementById(lblPrecoVenda);
            var obj_txtCusto = document.getElementById(txtCusto);
            var valorPrecoPromocional = parseInt(obj_txtPrecoPromocional.value);
            var valorCusto = parseFloat(obj_txtCusto.value.replace('.', '').replace(',', '.'));
            var valorPromocao = ((valorPrecoPromocional / 100) * valorCusto) + valorCusto;
            var valorVenda = (valorPromocao * 0.3) + valorPromocao;
            obj_lblPrecoPromocional.innerHTML = valorPromocao.toFixed(2);
            obj_lblPrecoVenda.innerHTML = valorVenda.toFixed(2);
        }

        function PrecoIgual(txtPrecoPromocional) {
            var obj_txtPrecoPromocional = document.getElementById(txtPrecoPromocional);
            for (i = 0; i < 100; i++) {
                var obj_txtPrecoPromocional2 = document.getElementById('grid_cell' + i + '_5_txtPrecoPromocional');
                document.getElementById('grid_cell' + i + '_5_txtPrecoPromocional').value = obj_txtPrecoPromocional.value;
            }
            return false;
        }
    </script>

    <table cellpadding="0" cellspacing="0" style="width: 920px">
    <tr>
        <td class="tituloPaginas" valign="top">
            <asp:Label ID="lblAcao" runat="server">Cadastro de Produtos</asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            
    <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
        <tr>
            <td>
    <asp:Wizard ID="wzProdCad" runat="server" ActiveStepIndex="0"
             SideBarStyle-BorderWidth="4" DisplaySideBar="False" SideBarStyle-BorderStyle="None"

          SideBarButtonStyle-Font-Size="Small" 
        SideBarButtonStyle-Font-Bold="true"  HeaderStyle-BackColor="AliceBlue"

          SideBarStyle-BackColor="AliceBlue" SideBarStyle-BorderColor="Red" 
        StepPreviousButtonType="Image" 
        onfinishbuttonclick="wzProdCad_FinishButtonClick" Width="834px" 
                    StartNextButtonType="Image" CancelButtonType="Image" 
                    FinishCompleteButtonImageUrl="~/admin/images/btSalvar.jpg" 
                    FinishCompleteButtonType="Image" 
                    FinishPreviousButtonImageUrl="~/admin/images/btAnterior.jpg" 
                    FinishPreviousButtonType="Image" 
                    StartNextButtonImageUrl="~/admin/images/btProxima.jpg" 
                    StepNextButtonImageUrl="~/admin/images/btProxima.jpg" 
                    StepNextButtonType="Image">

          <WizardSteps>

            <asp:WizardStep ID="Wizardstep1" runat="server" Title="passo1">

                <table cellpadding="0" cellspacing="0" style="width: 834px">
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" style="width: 834px">
                                <tr>
                                    <td style="width: 570px">
                                        <table cellpadding="0" cellspacing="0" style="width: 570px">
                                            <tr>
                                                <td>
                                                    <table cellpadding="0" cellspacing="0" style="width: 570px">
                                                        <tr  class="rotulos">
                                                            <td colspan="2">
                                                                Site:<br/>
                                                               <asp:DropDownList runat="server" ID="ddlSite">
                                                                   <Items>
                                                                       <asp:ListItem Value="1" Text="Grão de Gente"></asp:ListItem>
                                                                       <asp:ListItem Value="2" Text="Grão de Gente KIDS"></asp:ListItem>
                                                                   </Items>
                                                               </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr class="rotulos">
                                                            <td>
                                                                Nome do Produto<asp:RequiredFieldValidator ID="rqvProdutoNome" runat="server" 
                                                                    ControlToValidate="txtProdutoNome" Display="None" 
                                                                    ErrorMessage="Preencha o nome do produto." SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                <br />
                                                                <asp:TextBox ID="txtProdutoNome" runat="server" CssClass="campos" Width="570px"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr class="rotulos">
                                                            <td>
                                                                TAGs do Produto
                                                                <br />
                                                                <asp:TextBox ID="txtProdutoTags" runat="server" CssClass="campos" Width="570px"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                     </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table cellpadding="0" cellspacing="0" style="width: 570px">
                                                        <tr class="rotulos">
                                                            <td style="width: 155px">
                                                                ID da Empresa<br />
                                                                <asp:TextBox ID="txtProdutoIdDaEmpresa" runat="server" CssClass="campos" 
                                                                    Width="132px"></asp:TextBox>
                                                            </td>
                                                            <td style="width: 162px">
                                                                Complemento ID<br />
                                                                <asp:TextBox ID="txtComplementoId" runat="server" CssClass="campos" 
                                                                    Width="132px"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                Código de Barras<br />
                                                                <asp:TextBox ID="txtProdutoCodDeBarras" runat="server" CssClass="campos" 
                                                                    Width="142px"></asp:TextBox>
                                                                
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    &nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table cellpadding="0" cellspacing="0" style="width: 570px">
                                                        <tr>
                                                            <td class="rotulos" style="width: 236px" valign="bottom">
                                                                Fornecedor<asp:RequiredFieldValidator ID="rqvFornecedor" runat="server" 
                                                                    ControlToValidate="drpFornecedor" Display="None" 
                                                                    ErrorMessage="Selecione o fornecedor." SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                <br />
                                                                <asp:DropDownList ID="drpFornecedor" runat="server" AppendDataBoundItems="True" 
                                                                    CssClass="campos" DataSourceID="sqlFornecedor" DataTextField="fornecedorNome" 
                                                                    DataValueField="fornecedorId" Width="212px">
                                                                    <asp:ListItem Selected="True" Value="">Selecione</asp:ListItem>
                                                                </asp:DropDownList>
                                                                <asp:LinqDataSource ID="sqlFornecedor" runat="server" 
                                                                    ContextTypeName="dbCommerceDataContext" TableName="tbProdutoFornecedors">
                                                                </asp:LinqDataSource>
                                                            </td>
                                                            <td class="rotulos" style="width: 234px" valign="bottom">
                                                                Marca<asp:RequiredFieldValidator ID="rqvMarca" runat="server" 
                                                                    ControlToValidate="drpMarca" Display="None" ErrorMessage="Selecione a marca." 
                                                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                <br />
                                                                <asp:DropDownList ID="drpMarca" runat="server" AppendDataBoundItems="True" 
                                                                    CssClass="campos" DataSourceID="sqlMarca" DataTextField="marcaNome" 
                                                                    DataValueField="marcaId" Width="212px">
                                                                    <asp:ListItem Selected="True" Value="">Selecione</asp:ListItem>
                                                                </asp:DropDownList>
                                                                <asp:LinqDataSource ID="sqlMarca" runat="server" 
                                                                    ContextTypeName="dbCommerceDataContext" TableName="tbMarcas">
                                                                </asp:LinqDataSource>
                                                            </td>
                                                            <td rowspan="4" valign="top">
                                                                <asp:CheckBox ID="ckbAtivo" runat="server" CssClass="rotulos" Text="Ativo" Checked=true />
                                                                <br />
                                                                <asp:CheckBox ID="ckbPrincipal" runat="server" CssClass="rotulos" Checked=true
                                                                    Text="Principal" />
                                                                <br />
                                                                <asp:CheckBox ID="ckbFreteGratis" runat="server" CssClass="rotulos" 
                                                                    Text="Frete Gratís" />
                                                                <br />
                                                                <asp:CheckBox ID="ckbPreVenda" runat="server" CssClass="rotulos" 
                                                                    Text="Pré venda" />
                                                                <br />
                                                                <asp:CheckBox ID="ckbLancamento" runat="server" CssClass="rotulos" 
                                                                    Text="Lançamento" />
                                                                <br />
                                                                <asp:CheckBox ID="ckbPromocao" runat="server" CssClass="rotulos" 
                                                                    Text="Promoção" />
                                                                <br />
                                                                <asp:CheckBox ID="chkExclusivo" runat="server" CssClass="rotulos" 
                                                                    Text="Exclusivo" />
                                                                <br />
                                                                <asp:CheckBox ID="ckbDestaque1" runat="server" CssClass="rotulos" 
                                                                    Text="Destaque 1" />
                                                                <br />
                                                                <asp:CheckBox ID="ckbDestaque2" runat="server" CssClass="rotulos" 
                                                                    Text="Meninos" />
                                                                <br />
                                                                <asp:CheckBox ID="ckbDestaque3" runat="server" CssClass="rotulos" 
                                                                    Text="Meninas" />
                                                                <br />
                                                                <asp:CheckBox ID="ckbMarketplaceCadastrar" runat="server" CssClass="rotulos" 
                                                                    Text="Marketplace" />
                                                                <br />
                                                                <asp:CheckBox ID="ckbMarketplaceEnviarMarca" runat="server" CssClass="rotulos" 
                                                                    Text="Enviar Marca no Marketplace" />
                                                                <br />
                                                                <asp:CheckBox ID="ckbExibirDiferencaCombo" runat="server" CssClass="rotulos"  Text="Exibir diferença do combo" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                                        <td class="rotulos" style="width: 236px" valign="bottom">
                                                                            Composição<br />
                                                                            <asp:DropDownList runat="server" CssClass="campos"
                                                                                Width="212px" ID="drpComposicao">
                                                                                <asp:ListItem Value="">Selecione</asp:ListItem>
                                                                                <asp:ListItem Value="fibrasiliconada">Fibra Siliconada</asp:ListItem>
                                                                                <asp:ListItem Value="duplaface">Dupla Face</asp:ListItem>
                                                                                <asp:ListItem Value="felpuda">Felpuda</asp:ListItem>
                                                                                <asp:ListItem Value="cemalgodao">100% Algodão</asp:ListItem>
                                                                                <asp:ListItem Value="varaosimples">Varão Simples</asp:ListItem>
                                                                                <asp:ListItem Value="varaoduplo">Varão Duplo</asp:ListItem>
                                                                                <asp:ListItem Value="comforro">Com Forro</asp:ListItem>
                                                                                <asp:ListItem Value="fibradesilicone">Fibra de Silicone</asp:ListItem>
                                                                                <asp:ListItem Value="macio">Macio</asp:ListItem>
                                                                                <asp:ListItem Value="fiosegipsios">Fios Egípcios</asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                        <td class="rotulos" style="width: 234px" valign="bottom">
                                                                        Fios<br />
                                                                        <asp:TextBox runat="server" CssClass="campos" Width="85px" ID="txtFios"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                        <tr>
                                                            <td class="rotulos" style="width: 236px" valign="bottom">
                                                                Peças<br />
                                                                <asp:TextBox ID="txtPecas" runat="server" CssClass="campos" Width="85px"></asp:TextBox>
                                                            </td>
                                                            <td class="rotulos" style="width: 234px" valign="bottom">
                                                                Brindes<br />
                                                                <asp:TextBox ID="txtBrindes" runat="server" CssClass="campos" Width="85px"></asp:TextBox></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                &nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <table cellpadding="0" cellspacing="0" style="width: 322px">
                                                                    <tr class="rotulos">
                                                                        <td style="width: 109px" valign="bottom">
                                                                            Estoque<asp:RequiredFieldValidator ID="rqvEstoque" runat="server" 
                                                                                ControlToValidate="txtProdutoEstoqueAtual" Display="None" 
                                                                                ErrorMessage="Preencha o estoque." SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                            <br />
                                                                            <asp:TextBox ID="txtProdutoEstoqueAtual" runat="server" CssClass="campos" 
                                                                                Width="85px"></asp:TextBox>
                                                                        </td>
                                                                        <td style="width: 127px" valign="bottom">
                                                                            Estoque Mínimo<asp:RequiredFieldValidator ID="rqvEstoqueMinino" runat="server" 
                                                                                ControlToValidate="txtProdutoEstoqueMinimo" Display="None" 
                                                                                ErrorMessage="Preencha o estoque mínimo." SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                            <br />
                                                                            <asp:TextBox ID="txtProdutoEstoqueMinimo" runat="server" CssClass="campos" 
                                                                                Width="104px">0</asp:TextBox>
                                                                        </td>
                                                                        <td valign="bottom">
                                                                            Peso<asp:RequiredFieldValidator ID="rqvPeso" runat="server" 
                                                                                ControlToValidate="txtProdutoPeso" Display="None" 
                                                                                ErrorMessage="Preencha o peso." SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                            <br />
                                                                            <asp:TextBox ID="txtProdutoPeso" runat="server" CssClass="campos" Width="85px"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr class="rotulos">
                                                                        <td style="width: 109px" valign="bottom">
                                                                            Largura (cm)
                                                                            <asp:RequiredFieldValidator ID="rqvLargura" runat="server" 
                                                                                ControlToValidate="txtLargura" Display="None" 
                                                                                ErrorMessage="Preencha a largura." SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                            <br />
                                                                            <asp:TextBox ID="txtLargura" runat="server" CssClass="campos" 
                                                                                Width="85px"></asp:TextBox>
                                                                        </td>
                                                                        <td style="width: 127px" valign="bottom">
                                                                            Altura (cm)
                                                                            <asp:RequiredFieldValidator ID="rqvAltura" runat="server" 
                                                                                ControlToValidate="txtAltura" Display="None" 
                                                                                ErrorMessage="Preencha a altura." SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                            <br />
                                                                            <asp:TextBox ID="txtAltura" runat="server" CssClass="campos" 
                                                                                Width="104px">0</asp:TextBox>
                                                                        </td>
                                                                        <td valign="bottom">
                                                                            Profundidade
                                                                            <asp:RequiredFieldValidator ID="rqvProfundidade" runat="server" 
                                                                                ControlToValidate="txtProfundidade" Display="None" 
                                                                                ErrorMessage="Preencha a profundidade." SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                            <br />
                                                                            <asp:TextBox ID="txtProfundidade" runat="server" CssClass="campos" Width="85px"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td valign="top">
                                        <table align="right" cellpadding="0" cellspacing="0" class="rotulos" 
                                            style="width: 243px">
                                            <tr>
                                                <td>
                                                    Descrição do produto</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="txtProdutoDescricao" runat="server" CssClass="campos" 
                                                        Height="217px" TextMode="MultiLine" Width="243px"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <img alt="" src="images/separador.gif" /></td>
                    </tr>
                    <tr class="rotulos">
                        <td>
                            <table cellpadding="0" cellspacing="0" style="width: 834px">
                                
                                <tr class="rotulos">
                                    <td style="width: 213px">
                                        Preço de Custo<asp:RequiredFieldValidator ID="rqvPrecoDeCusto" runat="server" 
                                            ControlToValidate="txtProdutoPrecoDeCusto" Display="None" 
                                            ErrorMessage="Preencha o preço de custo." SetFocusOnError="True"></asp:RequiredFieldValidator>
                                        <br />
                                        <asp:TextBox ID="txtProdutoPrecoDeCusto" runat="server" CssClass="campos" 
                                            Width="190px">0</asp:TextBox>
                                    </td>
                                    <td style="width: 213px">
                                        Preço<asp:RequiredFieldValidator ID="rqvPreco" runat="server" 
                                            ControlToValidate="txtProdutoPreco" Display="None" 
                                            ErrorMessage="Preencha o preço." SetFocusOnError="True"></asp:RequiredFieldValidator>
                                        <br />
                                        <asp:TextBox ID="txtProdutoPreco" runat="server" CssClass="campos" 
                                            Width="190px"></asp:TextBox>
                                    </td>
                                    <td style="width: 213px">
                                        Preço Promocional<asp:RequiredFieldValidator ID="rqvPrecoPromocional" 
                                            runat="server" ControlToValidate="txtProdutoPrecoPromocional" Display="None" 
                                            ErrorMessage="Preencha o preço promocional." SetFocusOnError="True"></asp:RequiredFieldValidator>
                                        <br />
                                        <asp:TextBox ID="txtProdutoPrecoPromocional" runat="server" CssClass="campos" 
                                            Width="190px">0</asp:TextBox>
                                    </td>
                                    <td>
                                        Preço para Atacado<asp:RequiredFieldValidator ID="rqvPrecoAtacado" 
                                            runat="server" ControlToValidate="txtProdutoPrecoAtacado" Display="None" 
                                            ErrorMessage="Preencha o preço de atacado." SetFocusOnError="True"></asp:RequiredFieldValidator>
                                        <br />
                                        <asp:TextBox ID="txtProdutoPrecoAtacado" runat="server" CssClass="campos" 
                                            Width="191px">0</asp:TextBox>
                                    </td>
                                </tr>
                                <tr class="rotulos">
                                    <td style="width: 213px">
                                        Margem:<br /><asp:TextBox ID="txtMargem" runat="server" CssClass="campos" Width="190px">0</asp:TextBox>
                                    </td>
                                    <td style="width: 213px">
                                         Preço Venda: <br/><asp:Label ID="lblPrecoVenda" runat="server" Text=""></asp:Label>      
                                    </td>
                                    <td style="width: 213px">
                                        Preço Promocional: <br/><asp:Label ID="lblPrecoPromocional" runat="server" Text=""></asp:Label><br /> 
                                    </td>
                                    <td style="width: 213px">
                                        
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" style="width: 834px">
                                <tr class="rotulos">
                                    <td style="width: 213px">
                                        &nbsp;</td>
                                    <td style="width: 213px">
                                        Disponibilidade em estoque<br />
                                        <asp:TextBox ID="txtDisponibilidadeEmEstoque" runat="server" CssClass="campos" 
                                            Width="190px"></asp:TextBox>
                                    </td>
                                    <td style="width: 213px">
                                        Quantidade Mínima para Atacado<asp:RequiredFieldValidator ID="rqvQuantidadeMinimaAtacado" 
                                            runat="server" ControlToValidate="txtProdutoQuantidadeMininaParaAtacado" 
                                            Display="None" ErrorMessage="Preencha quantidade mínima para atacado.." 
                                            SetFocusOnError="True"></asp:RequiredFieldValidator>
                                        <br />
                                        <asp:TextBox ID="txtProdutoQuantidadeMininaParaAtacado" runat="server" 
                                            CssClass="campos" Width="54px">0</asp:TextBox>
                                    </td>
                                    <td>
                                        Legenda para Atacado<br />
                                        <asp:TextBox ID="txtProdutoLegendaAtacado" runat="server" CssClass="campos" 
                                            Height="34px" TextMode="MultiLine" Width="190px"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td id=trEspecificacoes runat=server visible=false>
                            <table cellpadding="0" cellspacing="0" style="width: 834px">
                                <tr>
                                    <td>
                                        <img alt="" src="images/separador.gif" /></td>
                                </tr>
                                <tr>
                                    <td class="rotulos">
                                        <b>Especificações</b><asp:CheckBoxList ID="ckbEspecificacoes" runat="server" 
                                            CssClass="rotulos" DataSourceID="sqlEspecificacoes" 
                                            DataTextField="especificacaoNome" DataValueField="especificacaoId" 
                                            RepeatColumns="8">
                                        </asp:CheckBoxList>
                                        <asp:ObjectDataSource ID="sqlEspecificacoes" runat="server" 
                                            SelectMethod="especificacaoSeleciona" TypeName="rnEspecificacao">
                                        </asp:ObjectDataSource>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <img alt="" src="images/separador.gif" /></td>
                    </tr>
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" style="width: 834px">
                                <tr id=trGarantiaEstendida runat=server>
                                    <td class="rotulos">
                                        <b>Garantia Estendida</b><asp:Panel ID="pnlGarantiaEstendida" runat="server">
                                            <table cellpadding="0" cellspacing="0" style="width: 834px">
                                                <tr>
                                                    <td style="width: 207px">
                                                        <table cellpadding="0" cellspacing="0" style="width: 202px">
                                                            <tr class="rotulos">
                                                                <td style="width: 148px">
                                                                    Tempo<br />
                                                                    <asp:TextBox ID="txtTempo1" runat="server" CssClass="campos" Width="143px"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    Valor<br />
                                                                    <asp:TextBox ID="txtValor1" runat="server" CssClass="campos" Width="48px"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td style="width: 209px">
                                                        <table cellpadding="0" cellspacing="0" style="width: 202px">
                                                            <tr class="rotulos">
                                                                <td style="width: 148px">
                                                                    Tempo<br />
                                                                    <asp:TextBox ID="txtTempo2" runat="server" CssClass="campos" Width="143px"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    Valor<br />
                                                                    <asp:TextBox ID="txtValor2" runat="server" CssClass="campos" Width="48px"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td style="width: 208px">
                                                        <table cellpadding="0" cellspacing="0" style="width: 202px">
                                                            <tr class="rotulos">
                                                                <td style="width: 148px">
                                                                    Tempo<br />
                                                                    <asp:TextBox ID="txtTempo3" runat="server" CssClass="campos" Width="143px"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    Valor<br />
                                                                    <asp:TextBox ID="txtValor3" runat="server" CssClass="campos" Width="48px"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td>
                                                        <table cellpadding="0" cellspacing="0" style="width: 202px">
                                                            <tr class="rotulos">
                                                                <td style="width: 148px">
                                                                    Tempo<br />
                                                                    <asp:TextBox ID="txtTempo4" runat="server" CssClass="campos" Width="143px"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    Valor<br />
                                                                    <asp:TextBox ID="txtValor4" runat="server" CssClass="campos" Width="48px"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <img alt="" src="images/separador.gif" /></td>
                    </tr>
                    <tr>
                        <td class="rotulos">
                            <b>Categorias</b><asp:TreeView ID="treeCategorias" runat="server" 
                                Font-Names="Tahoma" Font-Size="12px" 
                                OnTreeNodePopulate="TreeView1_TreeNodePopulate" ShowCheckBoxes="Leaf" 
                                ShowLines="True">
                            </asp:TreeView>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <img alt="" src="images/separador.gif" /></td>
                    </tr>
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" style="width: 834px">
                                <tr class="rotulos">
                                    <td style="width: 238px">
                                        Quantidade de Informação Adicional<br />
                                        <asp:TextBox ID="txtQuantidadeDeInformacoesAdcionais" runat="server" 
                                            CssClass="campos" Width="78px"></asp:TextBox>
                                    </td>
                                    <td>
                                        Quantidade de Produtos<br />
                                        <asp:TextBox ID="txtQuantidadeDeProdutos" runat="server" CssClass="campos" 
                                            Width="96px">1</asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                    </tr>
                </table>

            </asp:WizardStep>            

            <asp:WizardStep ID="WizardStep2" runat="server" Title="passo2" StepType="Finish"> 

                 <asp:DataList ID="dtlQuantidadeDeEspecificacoes" runat="server" 
                     OnItemDataBound="dtlQuantidadeDeEspecificacoes_ItemDataBound" 
                     Width="834px">
                     <ItemTemplate>
                         <table cellpadding="0" cellspacing="0" style="width: 834px">
                             <tr>
                                 <td>
                                     <asp:DataList ID="dtlEspecificacoes" runat="server" RepeatColumns="6" 
                                         RepeatDirection="Horizontal">
                                         <ItemTemplate>
                                             <div>
                                                 <table cellpadding="0" cellspacing="0" style="width: 100%; margin-right: 20px;">
                                                     <tr>
                                                         <td>
                                                             <div>
                                                                 <asp:Label ID="lblEspecificacaoNome" runat="server" CssClass="rotulos" 
                                                                     Text='<%# Bind("especificacaoNome") %>'></asp:Label>
                                                             </div>
                                                             <asp:Label ID="lblEspecificacaoId" runat="server" 
                                                                 Text='<%# Bind("especificacaoId") %>' Visible="False"></asp:Label>
                                                             <!--preciso dessa label pq recupero o valor dela que é a especificacaoId para usar no sqlEspecificacoesItens-->
                                                             <asp:DropDownList ID="drpEspecificacoes" runat="server" 
                                                                 AppendDataBoundItems="True" CssClass="campos" 
                                                                 DataSourceID="sqlEspecificacoesItens" DataTextField="especificacaoItenNome" 
                                                                 DataValueField="especificacaoItensId">
                                                                 <asp:ListItem Value="">Selecione</asp:ListItem>
                                                             </asp:DropDownList>
                                                             <asp:ObjectDataSource ID="sqlEspecificacoesItens" runat="server" 
                                                                 SelectMethod="especificacaoItemSeleciona_PorEspecificacaoId" 
                                                                 TypeName="rnEspecificacao">
                                                                 <SelectParameters>
                                                                     <asp:ControlParameter ControlID="lblEspecificacaoId" Name="especificacaoId" 
                                                                         PropertyName="Text" Type="Int32" />
                                                                 </SelectParameters>
                                                             </asp:ObjectDataSource>
                                                             <asp:RequiredFieldValidator ID="rqvEspecificacoes" runat="server" 
                                                                 ControlToValidate="drpEspecificacoes" 
                                                                 ErrorMessage="Selecione uma especificação." Display="None" 
                                                                 SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                         </td>
                                                     </tr>
                                                 </table>
                                             </div>
                                         </ItemTemplate>
                                     </asp:DataList>
                                 </td>
                             </tr>
                             <tr>
                                 <td>
                                     &nbsp;</td>
                             </tr>
                             <tr>
                                 <td>
                                     <table cellpadding="0" cellspacing="0" class="rotulos" style="width: 834px">
                                         <tr>
                                             <td colspan="4">
                                                 Nome do produto<br />
                                                 <asp:TextBox ID="txtProdutoNome1" runat="server" CssClass="campos" 
                                                     Width="575px"></asp:TextBox>
                                                 <asp:RequiredFieldValidator ID="rqvNome" runat="server" 
                                                     ControlToValidate="txtProdutoNome1" Display="None" 
                                                     ErrorMessage="Preencha o nome do produto." SetFocusOnError="True"></asp:RequiredFieldValidator>
                                             </td>
                                         </tr>
                                         <tr>
                                             <td colspan="4">
                                                 Descrição do produto<br />
                                                 <asp:TextBox ID="txtProdutoDescricao1" runat="server" CssClass="campos" 
                                                     Height="40px" TextMode="MultiLine" Width="834px"></asp:TextBox>
                                             </td>
                                         </tr>
                                         <tr>
                                             <td style="width: 140px">
                                                 Id da Empresa<br />
                                                 <asp:TextBox ID="txtProdutoIdDaEmpresa1" runat="server" CssClass="campos"></asp:TextBox>
                                             </td>
                                             <td style="width: 150px">
                                                 Cód de Barras<br />
                                                 <asp:TextBox ID="txtProdutoCodDeBarras1" runat="server" CssClass="campos"></asp:TextBox>
                                             </td>
                                             <td style="width: 143px">
                                                 Preço de Custo<asp:RequiredFieldValidator ID="rqvPrecoDeCusto1" runat="server" 
                                                     ControlToValidate="txtProdutoPrecoDeCusto1" Display="None" 
                                                     ErrorMessage="Preencha o preço de custo." SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                 <br />
                                                 <asp:TextBox ID="txtProdutoPrecoDeCusto1" runat="server" CssClass="campos"></asp:TextBox>
                                             </td>
                                             <td>
                                                 Preço<asp:RequiredFieldValidator ID="rqvPreco1" runat="server" 
                                                     ControlToValidate="txtProdutoPreco1" Display="None" 
                                                     ErrorMessage="Preencha o preço." SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                 <br />
                                                 <asp:TextBox ID="txtProdutoPreco1" runat="server" CssClass="campos"></asp:TextBox>
                                             </td>
                                         </tr>
                                         <tr>
                                             <td colspan="4">
                                                 &nbsp;</td>
                                         </tr>
                                         <tr>
                                             <td style="width: 140px">
                                                 Preço Promocional<asp:RequiredFieldValidator ID="rqvPrecoPromocional1" 
                                                     runat="server" ControlToValidate="txtProdutoPrecoPromocional1" Display="None" 
                                                     ErrorMessage="Preencha o preço promocional." SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                 <br />
                                                 <asp:TextBox ID="txtProdutoPrecoPromocional1" runat="server" CssClass="campos"></asp:TextBox>
                                             </td>
                                             <td style="width: 150px">
                                                 Preço de Atacado<asp:RequiredFieldValidator ID="rqvPrecoAtacado1" 
                                                     runat="server" ControlToValidate="txtProdutoPrecoAtacado1" Display="None" 
                                                     ErrorMessage="Preencha o preço de atacado." SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                 <br />
                                                 <asp:TextBox ID="txtProdutoPrecoAtacado1" runat="server" CssClass="campos"></asp:TextBox>
                                             </td>
                                             <td style="width: 143px">
                                                 qtd Mínina atacado<asp:RequiredFieldValidator ID="rqvQuantidadeMinimaAtacado1" 
                                                     runat="server" ControlToValidate="txtProdutoQuantidadeMininaParaAtacado1" 
                                                     Display="None" ErrorMessage="Preencha a quantidade mínima para atacado." 
                                                     SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                 <br />
                                                 <asp:TextBox ID="txtProdutoQuantidadeMininaParaAtacado1" runat="server" 
                                                     CssClass="campos"></asp:TextBox>
                                             </td>
                                             <td>
                                                 Estoque<asp:RequiredFieldValidator ID="rqvEstoque1" runat="server" 
                                                     ControlToValidate="txtProdutoEstoqueAtual1" Display="None" 
                                                     ErrorMessage="Preencha o estoque." SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                 <br />
                                                 <asp:TextBox ID="txtProdutoEstoqueAtual1" runat="server" CssClass="campos"></asp:TextBox>
                                             </td>
                                         </tr>
                                         <tr>
                                             <td colspan="4">
                                                 &nbsp;</td>
                                         </tr>
                                     </table>
                                 </td>
                             </tr>
                             <tr>
                                 <td>
                                     <table cellpadding="0" cellspacing="0" class="rotulos" style="width: 834px">
                                         <tr>
                                             <td>
                                                 <table cellpadding="0" cellspacing="0" style="width: 834px">
                                                     <tr>
                                                         <td class="rotulos" style="width: 291px">
                                                             Foto<asp:RegularExpressionValidator ID="rgeFlu1" runat="server" 
                                                                 ControlToValidate="upl1" Display="None" 
                                                                 ErrorMessage="A imagem precisa ser jpg." SetFocusOnError="True" 
                                                                 ValidationExpression="(.*\.jpe?g|.*\.JPE?G)"></asp:RegularExpressionValidator>
                                                             <br />
                                                             <asp:FileUpload ID="upl1" runat="server" CssClass="campos" Width="262px" />
                                                         </td>
                                                         <td class="rotulos" style="width: 292px">
                                                             Foto<asp:RegularExpressionValidator ID="rgeFlu2" runat="server" 
                                                                 ControlToValidate="upl2" Display="None" 
                                                                 ErrorMessage="A imagem precisa ser jpg." SetFocusOnError="True" 
                                                                 ValidationExpression="(.*\.jpe?g|.*\.JPE?G)"></asp:RegularExpressionValidator>
                                                             <br />
                                                             <asp:FileUpload ID="upl2" runat="server" CssClass="campos" Width="262px" />
                                                         </td>
                                                         <td class="rotulos">
                                                             Foto<asp:RegularExpressionValidator ID="rgeFlu3" runat="server" 
                                                                 ControlToValidate="upl3" Display="None" 
                                                                 ErrorMessage="A imagem precisa ser jpg." SetFocusOnError="True" 
                                                                 ValidationExpression="(.*\.jpe?g|.*\.JPE?G)"></asp:RegularExpressionValidator>
                                                             <br />
                                                             <asp:FileUpload ID="upl3" runat="server" CssClass="campos" Width="245px" />
                                                         </td>
                                                     </tr>
                                                     <tr>
                                                         <td class="rotulos" style="width: 291px">
                                                             Descrição da foto<br />
                                                             <asp:TextBox ID="txtFotoDescricao1" runat="server" CssClass="campos" 
                                                                 Width="262px"></asp:TextBox>
                                                         </td>
                                                         <td class="rotulos" style="width: 292px">
                                                             Descrição da foto<br />
                                                             <asp:TextBox ID="txtFotoDescricao2" runat="server" CssClass="campos" 
                                                                 Width="262px"></asp:TextBox>
                                                         </td>
                                                         <td class="rotulos">
                                                             Descrição da foto<br />
                                                             <asp:TextBox ID="txtFotoDescricao3" runat="server" CssClass="campos" 
                                                                 Width="245px"></asp:TextBox>
                                                         </td>
                                                     </tr>
                                                 </table>
                                             </td>
                                         </tr>
                                         <tr>
                                             <td>
                                                 <img alt="" src="images/separador.gif" /></td>
                                         </tr>
                                     </table>
                                 </td>
                             </tr>
                         </table>
                     </ItemTemplate>
                 </asp:DataList>
                 <asp:Panel ID="pnlInformacoesAdcionais" runat="server">
                 </asp:Panel>
            </asp:WizardStep>
          </WizardSteps>

<SideBarButtonStyle Font-Bold="True" Font-Size="Small"></SideBarButtonStyle>

<SideBarStyle BackColor="AliceBlue" BorderColor="Red" BorderWidth="4px" BorderStyle="None"></SideBarStyle>

<HeaderStyle BackColor="AliceBlue"></HeaderStyle>
         </asp:Wizard> 
            </td>
        </tr>
        <tr>
            <td>
                <asp:ValidationSummary ID="vlds" runat="server" ShowMessageBox="True" 
                    ShowSummary="False" DisplayMode="List" />
            </td>
        </tr>
    </table>
        </td>
    </tr>
</table>
    </asp:Content>

