﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="despesasGerais.aspx.cs" Inherits="admin_despesasGerais" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" runat="Server">
    <script src="js/jquery.min.js"></script>
    <style>
        .fieldsetatualizarprecos {
            width: 760px;
            float: left;
            clear: left;
            border-radius: 5px;
            margin-left: 25px;
        }

            .fieldsetatualizarprecos input[type=checkbox] {
                margin-top: 15px;
            }

            .fieldsetatualizarprecos label {
                margin-left: 5px;
                margin-right: 20px;
            }

        .meugrid {
            background-color: #F4FAFB;
            border: 0px none;
            /* border-collapse: separate !important; */
            overflow: hidden;
            font: 9pt Tahoma;
            color: #000;
            cursor: pointer;
            white-space: nowrap;
            font-weight: normal;
            text-align: left;
            width: 834px;
            border-collapse: collapse;
            float: left;
            clear: left;
            margin-top: 10px;
            margin-bottom: 10px;
            min-height: 500px;
            margin-left: 25px;
        }

            .meugrid th {
                background-image: url(../../App_Themes/Glass/GridView/gvHeaderBackground.gif);
                background-position: center top;
                background-repeat: repeat-x;
                background-color: #C0DDE0;
                overflow: hidden;
                height: 23px;
                line-height: 23px;
                padding: 4px 6px 5px;
                font-weight: normal;
                border: 1px solid #7EACB1;
                /* border-collapse: separate;*/
            }

            .meugrid td {
                overflow: hidden;
                height: 23px;
                line-height: 23px;
                padding: 4px 6px 5px;
                border: 1px solid #7EACB1;
            }

            .meugrid tr td:nth-child(4) {
                text-align: center;
            }

            .meugrid tr td:last-of-type {
                text-align: center;
            }

        .tabrotulos {
            width: 848px;
            margin: auto;
        }

        .materiaprima {
            width: 250px;
        }

            .materiaprima ul {
                margin: 0px;
                padding-left: 10px;
            }

                .materiaprima ul li {
                    max-width: 230px;
                    white-space: pre-wrap;
                }

        .meugrid tr td:last-of-type {
            text-align: center;
        }

        .nome {
            width: 237px;
        }

            .nome div {
                max-width: 230px;
                white-space: pre-wrap;
            }

        .rotulos td {
            font: bold 12px tahoma;
            padding-top: 20px;
            color: rgb(65, 105, 126);
            padding-left: 5px;
            padding-bottom: 5px;
        }
    </style>
    <div class="tituloPaginas" valign="top">
        Cadastro de Despesas Gerais
    </div>
    <div>
        <fieldset class="fieldsetatualizarprecos">
            <legend style="font-weight: bold;">Filtro</legend>

            <table class="tabrotulos">

                <tr class="rotulos">
                    <td>Id:<br />
                        <asp:TextBox runat="server" ID="txtiddespesasgerais"></asp:TextBox>
                    </td>
                    <td>Despesa Geral:<br />
                        <asp:TextBox runat="server" ID="txtnomedespesasgerais"></asp:TextBox>
                    </td>


                    <td>Porcentagem:<br />
                        <asp:TextBox runat="server" ID="txtporcentagemdespesasgerais"></asp:TextBox>
                    </td>
                    <td>&nbsp;<br />
                        <asp:Button runat="server" ID="btnFiltrar" Text="Filtrar" OnClick="btnFiltrar_Click" />

                    </td>

                </tr>
              

            </table>
        </fieldset>
         <table class="tabrotulos">
               <tr class="rotulos">
                    <td>
                        <asp:Button runat="server" ID="btnNovaDespesasGerais" Text="Nova Despesa Geral" OnClick="btnNovaDespesasGerais_Click" />
                    </td>
                    <td></td>

                    <td>&nbsp;&nbsp;
                        <asp:Button runat="server" ID="Button1" Text="Exportar Planilha" OnClick="btnExportarPlanilha_Click" />
                    </td>


                    <td></td>

                </tr>
              

            </table>
        
         <fieldset class="fieldsetatualizarprecos" runat="server" id="fieldsetnovaunidade"  Visible="False">
            <legend style="font-weight: bold;">Nova Despesas Geral</legend>

            <table class="tabrotulos">

                <tr class="rotulos">
                  
                    <td>Despesa Geral:<br />
                        <asp:TextBox runat="server" ID="txtnomedespesasgeraisinserir"></asp:TextBox>
                    </td>
                 

                    <td>Porcentagem:<br />
                        <asp:TextBox runat="server" ID="txtporcentagemdespesasgeraisinserir"></asp:TextBox>
                    </td>
                    <td>&nbsp;<br />
                        <asp:Button runat="server" ID="btnSalvar" Text="Salvar" OnClick="btnSalvar_Click"  />

                    </td>

                </tr>
              

            </table>
        </fieldset>
        
        <fieldset class="fieldsetatualizarprecos" runat="server" id="fieldseteditarunidade" Visible="False">
            <legend style="font-weight: bold;">Atualizar Despesa Geral</legend>

            <table class="tabrotulos">

                <tr class="rotulos">
                  <td>Id:<br />
                      <asp:Label ID="lblId" runat="server" Text=""></asp:Label>
                    </td>
                    <td>Despesa Geral:<br />
                        <asp:TextBox runat="server" ID="txtnomedespesasgeraiseditar"></asp:TextBox>
                    </td>
                   
                    <td>Porcentagem:<br />
                        <asp:TextBox runat="server" ID="txtporcentagemdespesasgeraiseditar"></asp:TextBox>
                    </td>
                    <td>&nbsp;<br />
                        <asp:Button runat="server" ID="btnAtualizar" Text="Atualizar" OnClick="btnAtualizar_Click"   />

                    </td>

                </tr>
              

            </table>
        </fieldset>
    </div>
    <div align="center" style="min-height: 500px;">
        <div class="meugrid" align="center">

            <asp:GridView ID="GridView1" CssClass="meugrid" runat="server"  OnRowDeleting="GridView1_RowDeleting" DataKeyNames="idDespesasGerais" AutoGenerateColumns="False" AllowPaging="True" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowDataBound="GridView1_RowDataBound" PageSize="10">
                <Columns>

                    <asp:BoundField DataField="idDespesasGerais" HeaderText="Id" />
                    <asp:BoundField DataField="nomeDespesasGerais" HeaderText="Despesa Geral" />
               
                     <asp:BoundField DataField="porcentagemDespesasGerais" HeaderText="Porcentagem" />

                    <asp:TemplateField HeaderText="Editar" ShowHeader="False">
                        <ItemTemplate>
                            <a id="linkeditar" runat="server">
                                <img src="images/btEditar.jpg" alt="Editar" style="border-style: none;" />
                            </a>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Excluir" ShowHeader="False">
                        <ItemTemplate>
                            <asp:LinkButton ID="cmdDelete" runat="server" CausesValidation="False" CommandName="Delete"
                                Text="Delete">
                            <img  src="images/btExcluir.jpg" alt="Excluir o banner" style="border-style:none;"/>
                            </asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                
            </asp:GridView>
            <div style="float: left; clear: left; width: 100%; font: bold 12px tahoma; margin-left: 25px;">
                Total de Despesas Gerais ( % ) :&nbsp;<asp:Label ID="lblTotalPorcentagem" runat="server"></asp:Label>
            </div>
        </div>
    </div>
</asp:Content>

