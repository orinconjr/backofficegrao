﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="entradaCd2.aspx.cs" Inherits="admin_entradaCd2" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxTimer" TagPrefix="dxt" %>
<%@ Register Assembly="CuteEditor" Namespace="CuteEditor" TagPrefix="CE" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" Runat="Server">
    <script type="text/javascript">
        function toggle() {
            var divs = document.querySelectorAll("[id]");
            for (var i = 0, len = divs.length; i < len; i++) {
                var div = divs[i];
                if (div.id.indexOf("chkEntregue") > -1) {
                    div.checked = true;
                }
            }
        }

        Number.prototype.formatMoney = function (c, d, t) {
            var n = this, c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "," : d, t = t == undefined ? "." : t, s = n < 0 ? "-" : "", i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
            return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
        };
    </script>

    <table cellpadding="0" cellspacing="0" style="width: 920px">

        <tr>
            <td class="tituloPaginas" valign="top">
                <asp:Label ID="lblAcao" runat="server">Pedidos ao Fornecedor</asp:Label>
            </td>
        </tr>

        <tr>
            <td>
                <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
                    <tr class="rotulos" style="font-size: 16px;">
                        <td>
                            Romaneio:<br />
                            <asp:TextBox runat="server" ID="txtRomaneio"></asp:TextBox><br />
                            <asp:Button runat="server" ID="btnCarregarRomaneio" Text="Carregar Romaneio" OnClick="btnCarregarRomaneio_Click" /><br /><br />
                        </td>
                    </tr>
                    <td>
                        &nbsp;
                    </td>
        </tr>
        <tr class="rotulos">
            <td></td>
        </tr>
        <tr class="rotulos">
            <td></td>
        </tr>
        <tr>
            <td style="height: 50px;">&nbsp;
            </td>
        </tr>
        <tr>
            <td>
                <table style="width: 100%;">
                    <tr class="rotulos" style="text-align: center; font-weight: bold;">
                        <td>&nbsp;
                        </td>
                        <td>&nbsp;
                        </td>
                        <td>&nbsp;
                        </td>
                        <td>&nbsp;
                        </td>
                        <td>&nbsp;
                        </td>
                        <td>&nbsp;
                        </td>
                        <td>&nbsp;
                        </td>
                        <td>&nbsp;
                        </td>
                        <td style="text-align: right;">
                            <asp:Button runat="server" OnClientClick="toggle(); return false;" Text="Checar todos" />
                        </td>
                    </tr>
                    <tr class="rotulos" style="text-align: center; font-weight: bold;">
                        <td>ID da Empresa
                        </td>
                        <td>Complemento ID
                        </td>
                        <td>Nome
                        </td>
                        <td>Entregue
                        </td>
                    </tr>
                    <asp:ListView runat="server" ID="lstProdutos">
                        <ItemTemplate>
                            <tr class="rotulos" style="text-align: center;">
                                <td style="padding-bottom: 3px; padding-top: 3px; border-bottom: 1px solid #b0e0e6;">
                                    <asp:HiddenField runat="server" ID="hdfIdPedidoFornecedorItem" Value='<%# Eval("idPedidoFornecedorItem") %>' />
                                    <asp:Literal runat="server" ID="litProdutoIdDaEmpresa" Text='<%# Eval("produtoIdDaEmpresa") %>'></asp:Literal>
                                </td>
                                <td style="padding-bottom: 3px; padding-top: 3px; border-bottom: 1px solid #b0e0e6;">
                                    <asp:Literal runat="server" ID="litComplementoIdDaEmpresa" Text='<%# Eval("complementoIdDaEmpresa") %>'></asp:Literal>&nbsp;
                                </td>
                                <td style="padding-bottom: 3px; padding-top: 3px; border-bottom: 1px solid #b0e0e6;">
                                    <%# Eval("produtoNome") %>
                                </td>
                                <td style="padding-bottom: 3px; padding-top: 3px; border-bottom: 1px solid #b0e0e6;">
                                    <asp:CheckBox runat="server" ID="chkEntregue" Checked='<%# Convert.ToBoolean(Eval("entregue")) %>' />
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:ListView>
                </table>
            </td>
        </tr>
        <tr>
            <td style="text-align: right;">&nbsp;
            </td>
        </tr>
        <tr>
            <td style="text-align: right;">
                <asp:Button runat="server" ID="btnGravar" Text="Gravar" OnClick="btnGravar_OnClick" />
            </td>
        </tr>
        <tr>
            <td style="padding-top: 100px;">&nbsp;
            </td>
        </tr>
    </table>
    </td>
    </tr>
</table>
</asp:Content>

