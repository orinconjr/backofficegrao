﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="bannerCad.aspx.cs" Theme="Glass" Inherits="admin_bannerCad" %>

<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxEditors" Assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" runat="Server">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script>

        function previewFileUpload(input) {

            var img = $("#ctl00_conteudoPrincipal_imgBanner");

            if (input.files && input.files[0]) {

                var reader = new FileReader();
                reader.onload = function (e) {
                    $("#ctl00_conteudoPrincipal_imgBanner").attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

    </script>
    
    <asp:RangeValidator ID="RangeValidator1" runat="server" MinimumValue="1" MaximumValue="9999" ControlToValidate="txtRelevancia"
        ErrorMessage="Valores de relevância devem estar entre 1 e 9999" ValidationGroup="cadastro"></asp:RangeValidator>
    <table cellpadding="0" cellspacing="0" style="width: 920px">
        <tr>
            <td class="tituloPaginas" valign="top">
                <asp:Label ID="lblAcao" runat="server">Banner Cadastro</asp:Label>
            </td>
        </tr>
        <tr class="rotulos">
            <td style="padding-left: 36px;">
                <asp:Image ID="imgBanner" runat="server" AlternateText="" Style="max-width: 860px;" /><br />
                Banner<br />
                <asp:FileUpload ID="FileUploadBanner" runat="server" onchange="previewFileUpload(this)" Width="97%" ValidationGroup="cadastro" /><br />
                <asp:RequiredFieldValidator ID="rfvArquivo" runat="server" ValidationGroup="cadastro"
                    ControlToValidate="FileUploadBanner" ErrorMessage="Informe o arquivo do banner!">
                </asp:RequiredFieldValidator>
            </td>
         </tr>
        <tr class="rotulos">
            <td style="padding-left: 36px; padding-top: 20px; padding-bottom: 10px;">
                <asp:CheckBox ID="ckbAtivo" Text="Ativo" runat="server" />&nbsp;&nbsp;&nbsp;<asp:CheckBox ID="ckbBannerMobile" Text="Mobile" runat="server" />
            </td>
        </tr>
        <tr class="rotulos">
            <td style="padding-left: 36px;">
                Link do Produto (opcional)<br />
                <asp:TextBox ID="txtLinkProduto" runat="server" Width="97%" MaxLength="300"></asp:TextBox>
            </td>
         </tr>
        <tr class="rotulos">
            <td style="padding-left: 36px;">
                <table style="border-spacing: 0;">
                    <tr class="rotulos">
                        <td>
                            Ordem<br />
                            <asp:TextBox ID="txtRelevancia" runat="server" MaxLength="3" Width="50px" Style="text-align: center"></asp:TextBox>
                        </td>
                        <td style="padding-left: 30px;">
                            Local<br />
                            <asp:DropDownList ID="ddlLocal" runat="server" Width="172" Height="22" AutoPostBack="true" OnSelectedIndexChanged="ddlLocal_SelectedIndexChanged">
                                <asp:ListItem Text="Home" Value="1" />
                                <asp:ListItem Text="Detalhes Produto" Value="2" />
                                <asp:ListItem Text="Busca" Value="3" />
                                <asp:ListItem Text="Lista Categoria" Value="4" />
                            </asp:DropDownList>
                        </td>
                        <td style="padding-left: 30px;">
                            Posição<br />
                            <asp:DropDownList ID="ddlPosicao" runat="server" Width="172" Height="22">
                                <asp:ListItem Text="Topo" Value="1" />
                                <asp:ListItem Text="Direita" Value="2" />
                                <asp:ListItem Text="Rodapé" Value="3" />
                                <asp:ListItem Text="Esquerda" Value="4" />
                                <asp:ListItem Text="Conteudo Home - Topo" Value="5" />
                                <asp:ListItem Text="Conteudo Home - Rodapé" Value="6" />
                                <asp:ListItem Text="Topo Split" Value="7" />
                                <asp:ListItem Text="Sub Banner Full" Value="8" />
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr class="rotulos">
            <td style="padding-left: 36px; padding-top: 20px;">
                <table>
                    <tr class="rotulos">
                        <td>
                            <asp:CheckBox ID="ckbAgendamentoAtivacao" Text="Agendar Ativação" runat="server" /><br />
                            <div style="width: 400px;">
                                <div style="float: left; width: 370px;">
                                    <div style="float: left; width: 186px;">Data:</div>
                                    <div style="float: left;">Hora:</div>
                                    <dx:ASPxDateEdit ID="txtAgendamentoDataAtivacao" runat="server" Style="float: left;">
                                    </dx:ASPxDateEdit>
                                    <dx:ASPxTimeEdit ID="txtAgendamentoHoraAtivacao" runat="server" Style="float: left; margin-left: 15px;">
                                    </dx:ASPxTimeEdit>
                                </div>
                            </div>
                        </td>
                        <td style="padding-left: 20px;">
                            <asp:CheckBox ID="ckbAgendamentoDesativacao" Text="Agendar Desativação" runat="server" /><br />
                            <div style="width: 400px;">
                                <div style="float: left; width: 370px;">
                                    <div style="float: left; width: 186px;">Data:</div>
                                    <div style="float: left;">Hora:</div>
                                    <dx:ASPxDateEdit ID="txtAgendamentoDataDesativacao" runat="server" Style="float: left;">
                                    </dx:ASPxDateEdit>
                                    <dx:ASPxTimeEdit ID="txtAgendamentoHoraDesativacao" runat="server" Style="float: left; margin-left: 15px;">
                                    </dx:ASPxTimeEdit>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr class="rotulos">
            <td style="padding-left: 36px; padding-top: 20px;">

                <table width="100%" border="0">
                    <tr class="rotulos">
                        <td style="width: 422px;">
                            <p><asp:CheckBox ID="ckbContador" Text="Contador" runat="server" /><br /></p>

                            <div style="float: left; width: 370px">
                                <div style="float: left; width: 186px;">Data Início:</div>
                                <div style="float: left;">Hora Início:</div>
                                <dx:ASPxDateEdit ID="txtInicioContadorData" runat="server" Style="float: left;">
                                </dx:ASPxDateEdit>
                                <dx:ASPxTimeEdit ID="txtInicioContadorHora" runat="server" Style="float: left; margin-left: 15px;">
                                </dx:ASPxTimeEdit>

                                <div style="float: left; width: 186px;">Data Fim:</div>
                                <div style="float: left;">Hora Fim:</div>
                                <dx:ASPxDateEdit ID="txtFimContadorData" runat="server" Style="float: left;">
                                </dx:ASPxDateEdit>
                                <dx:ASPxTimeEdit ID="txtFimContadorHora" runat="server" Style="float: left; margin-left: 15px;">
                                </dx:ASPxTimeEdit>
                            </div>

                        </td>
                        <td style="vertical-align: top;" class="produto-box" id="produtobox" runat="server">
                            <p> Mostrar apenas nos seguintes produtos</p>
                            <input type="text" id="txtBuscaProduto" name="txtBuscaProduto" maxlength="200" placeholder="Buscar Produto" style="width:99%"/>
                            <ul class="selectedProducts"></ul>
                        </td>
                        <td rowspan="3" style="vertical-align: top;" class="categoria-box" id="categoriabox" runat="server">
                            <p> Mostrar apenas nas seguintes categorias</p>
                            <p class="alert-mini">(deixar em branco para mostrar em TODAS)</p>
                            <input type="text" id="txtBusca" name="txtBusca" maxlength="200" placeholder="Buscar Categoria" style="width:99%"/>
                            <ul class="selectedCategories"></ul>
                        </td>
                    </tr>
                </table>

                
            </td>
        </tr>
        <tr class="rotulos">
            <td style="padding-left: 36px; padding-top: 20px;">
                <table>
                    <tr class="rotulos">
                        <td>
                            Layout:<br />
                            <asp:DropDownList runat="server" ID="ddlLayoutContador" DataTextField="nome" DataValueField="id" Style="float: left;" Width="306"/>

                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr class="rotulos">
            <td>
                <asp:ValidationSummary runat="server" ShowMessageBox="true" ValidationGroup="cadastro" ShowSummary="false" />
            </td>
        </tr>
        <tr>
            <td style="text-align: right; padding-right: 23px;">
                <asp:ImageButton ID="btnSalvar" runat="server" ImageUrl="images/btSalvar.jpg" AlternateText="Salvar" ValidationGroup="cadastro" OnClick="btnSalvar_Click" />
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="hdnCategorias" ClientIDMode="Static" runat="server"/>
    <asp:HiddenField ID="hdnProdutos" ClientIDMode="Static" runat="server"/>

    <!-- Dependencias do componente que vincula categoria e produto ao banner -->
    <script src="https://code.jquery.com/ui/1.9.2/jquery-ui.min.js" integrity="sha256-eEa1kEtgK9ZL6h60VXwDsJ2rxYCwfxi40VZ9E0XwoEA=" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">   
    <link rel="stylesheet" href="css/banner-categoria-component.css">   
    <link rel="stylesheet" href="css/banner-produto-component.css">  
    <script src="js/banner-categoria-component.js"></script>
    <script src="js/banner-produto-component.js"></script>

</asp:Content>

