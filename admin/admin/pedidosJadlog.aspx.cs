﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Data.Filtering;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;

public partial class admin_pedidosJadlog : System.Web.UI.Page
{
    DateTemplate dateTemplate = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            txtPedidoId.Focus();
            txtPedidoId.Attributes.Add("onkeypress", "return soNumero(event);");
            txtVol1.Attributes.Add("onkeypress", "return soNumero(event);");
            txtVol2.Attributes.Add("onkeypress", "return soNumero(event);");
            txtVol3.Attributes.Add("onkeypress", "return soNumero(event);");
            txtVol4.Attributes.Add("onkeypress", "return soNumero(event);");
            txtVol5.Attributes.Add("onkeypress", "return soNumero(event);");
            txtVol6.Attributes.Add("onkeypress", "return soNumero(event);");
            txtVol7.Attributes.Add("onkeypress", "return soNumero(event);");
            txtVol8.Attributes.Add("onkeypress", "return soNumero(event);");
            txtKg.Attributes.Add("onkeypress", "return soNumero(event);");
            //txtFaixa.Attributes.Add("onkeyPress", "return moeda(this,'.',',',event);");
            txtFrete.Attributes.Add("onkeyPress", "return moeda(this,'.',',',event);");
            txtInterior.Attributes.Add("onkeyPress", "return moeda(this,'.',',',event);");
            //txtTotal.Attributes.Add("onkeyPress", "return moeda(this,'.',',',event);");
            txtNota.Attributes.Add("onkeyPress", "return moeda(this,'.',',',event);");
            txtValorDiferenca.Attributes.Add("onkeyPress", "return moeda(this,'.',',',event);");
            txtValorAr.Attributes.Add("onkeyPress", "return moeda(this,'.',',',event);");
            txtValorMaoPropria.Attributes.Add("onkeyPress", "return moeda(this,'.',',',event);");
            txtValorSeguro.Attributes.Add("onkeyPress", "return moeda(this,'.',',',event);");

            txtVol1.Attributes.Add("onkeyup", "javascript:return CalculaKg();");
            txtVol2.Attributes.Add("onkeyup", "javascript:return CalculaKg();");
            txtVol3.Attributes.Add("onkeyup", "javascript:return CalculaKg();");
            txtVol4.Attributes.Add("onkeyup", "javascript:return CalculaKg();");
            txtVol5.Attributes.Add("onkeyup", "javascript:return CalculaKg();");
            txtVol6.Attributes.Add("onkeyup", "javascript:return CalculaKg();");
            txtVol7.Attributes.Add("onkeyup", "javascript:return CalculaKg();");
            txtVol8.Attributes.Add("onkeyup", "javascript:return CalculaKg();");
            txtFrete.Attributes.Add("onkeyup", "javascript:return CalculaKg();");
            txtInterior.Attributes.Add("onkeyup", "javascript:return CalculaKg();");
            txtValorAr.Attributes.Add("onkeyup", "javascript:return CalculaKg();");
            txtValorMaoPropria.Attributes.Add("onkeyup", "javascript:return CalculaKg();");
            txtNota.Attributes.Add("onkeyup", "javascript:return CalculaKg();");



            txtCorreiosVol1.Attributes.Add("onkeypress", "return soNumero(event);");
            txtCorreiosVol2.Attributes.Add("onkeypress", "return soNumero(event);");
            txtCorreiosVol3.Attributes.Add("onkeypress", "return soNumero(event);");
            txtCorreiosVol4.Attributes.Add("onkeypress", "return soNumero(event);");
            txtCorreiosCubagem1.Attributes.Add("onkeypress", "return soNumero(event);");
            txtCorreiosCubagem2.Attributes.Add("onkeypress", "return soNumero(event);");
            txtCorreiosCubagem3.Attributes.Add("onkeypress", "return soNumero(event);");
            txtCorreiosCubagem4.Attributes.Add("onkeypress", "return soNumero(event);");

            txtCorreiosAr1.Attributes.Add("onkeyPress", "return moeda(this,'.',',',event);");
            txtCorreiosAr2.Attributes.Add("onkeyPress", "return moeda(this,'.',',',event);");
            txtCorreiosAr3.Attributes.Add("onkeyPress", "return moeda(this,'.',',',event);");
            txtCorreiosAr4.Attributes.Add("onkeyPress", "return moeda(this,'.',',',event);");
            txtCorreiosSeguro1.Attributes.Add("onkeyPress", "return moeda(this,'.',',',event);");
            txtCorreiosSeguro2.Attributes.Add("onkeyPress", "return moeda(this,'.',',',event);");
            txtCorreiosSeguro3.Attributes.Add("onkeyPress", "return moeda(this,'.',',',event);");
            txtCorreiosSeguro4.Attributes.Add("onkeyPress", "return moeda(this,'.',',',event);");
            txtCorreiosMaoPropria1.Attributes.Add("onkeyPress", "return moeda(this,'.',',',event);");
            txtCorreiosMaoPropria2.Attributes.Add("onkeyPress", "return moeda(this,'.',',',event);");
            txtCorreiosMaoPropria3.Attributes.Add("onkeyPress", "return moeda(this,'.',',',event);");
            txtCorreiosMaoPropria4.Attributes.Add("onkeyPress", "return moeda(this,'.',',',event);");
            txtCorreiosValor1.Attributes.Add("onkeyPress", "return moeda(this,'.',',',event);");
            txtCorreiosValor2.Attributes.Add("onkeyPress", "return moeda(this,'.',',',event);");
            txtCorreiosValor3.Attributes.Add("onkeyPress", "return moeda(this,'.',',',event);");
            txtCorreiosValor4.Attributes.Add("onkeyPress", "return moeda(this,'.',',',event);");

            txtCorreiosValor1.Attributes.Add("onkeyup", "javascript:return CalculaTotalCorreios();");
            txtCorreiosValor2.Attributes.Add("onkeyup", "javascript:return CalculaTotalCorreios();");
            txtCorreiosValor3.Attributes.Add("onkeyup", "javascript:return CalculaTotalCorreios();");
            txtCorreiosValor4.Attributes.Add("onkeyup", "javascript:return CalculaTotalCorreios();");
            txtCorreiosAr1.Attributes.Add("onkeyup", "javascript:return CalculaTotalCorreios();");
            txtCorreiosAr2.Attributes.Add("onkeyup", "javascript:return CalculaTotalCorreios();");
            txtCorreiosAr3.Attributes.Add("onkeyup", "javascript:return CalculaTotalCorreios();");
            txtCorreiosAr4.Attributes.Add("onkeyup", "javascript:return CalculaTotalCorreios();");
            txtCorreiosSeguro1.Attributes.Add("onkeyup", "javascript:return CalculaTotalCorreios();");
            txtCorreiosSeguro2.Attributes.Add("onkeyup", "javascript:return CalculaTotalCorreios();");
            txtCorreiosSeguro3.Attributes.Add("onkeyup", "javascript:return CalculaTotalCorreios();");
            txtCorreiosSeguro4.Attributes.Add("onkeyup", "javascript:return CalculaTotalCorreios();");
            txtCorreiosMaoPropria1.Attributes.Add("onkeyup", "javascript:return CalculaTotalCorreios();");
            txtCorreiosMaoPropria2.Attributes.Add("onkeyup", "javascript:return CalculaTotalCorreios();");
            txtCorreiosMaoPropria3.Attributes.Add("onkeyup", "javascript:return CalculaTotalCorreios();");
            txtCorreiosMaoPropria4.Attributes.Add("onkeyup", "javascript:return CalculaTotalCorreios();");

        }

        fillGrid(false);
    }

    private void fillGrid(bool rebind)
    {
        var data = new dbCommerceDataContext();
        var pedidosJad = (from c in data.tbPedidoJadlogs orderby c.idPedido descending select c);
        grd.DataSource = pedidosJad;
        if((!Page.IsPostBack && !Page.IsCallback) | rebind) grd.DataBind();
    }

    protected void btnSelecionar_OnClick(object sender, EventArgs e)
    {
        int idPedido = 0;
        int.TryParse(txtPedidoId.Text, out idPedido);

        if (idPedido.ToString().Length != 5)
        {
            litIdPedido.Text = idPedido.ToString();
            idPedido = rnFuncoes.retornaIdInterno(idPedido);
        }
        else
        {
            litIdPedido.Text = idPedido.ToString();
        }


        var pedidoDc = new dbCommerceDataContext();
        var pedido = (from c in pedidoDc.tbPedidos where c.pedidoId == idPedido select c).FirstOrDefault();
        if (pedido != null)
        {
            if (pedido.statusDoPedido == 11 | pedido.statusDoPedido == 4)
            {
                if(!string.IsNullOrEmpty(pedido.danfe))txtNota.Text = (Convert.ToDecimal(pedido.valorCobrado) - Convert.ToDecimal(pedido.valorDoFrete)).ToString("0.00");
                var pedidoJadlog = (from c in pedidoDc.tbPedidoJadlogs where c.idPedido == idPedido && (c.cancelado != null) != true select c).FirstOrDefault();
                if (pedidoJadlog != null)
                {
                    pnMensagemEnviado.Visible = true;
                    litDataEnviado.Text = pedidoJadlog.data.ToShortDateString();
                    pnSelecionarPedido.Visible = false;
                    pnEnviarPedido.Visible = true;
                }
                else
                {
                    pnMensagemEnviado.Visible = false;
                    pnSelecionarPedido.Visible = false;
                    pnEnviarPedido.Visible = true;
                }
            }
            else
            {
                Response.Write("<script>alert('Pedido não pode ser enviado devido ao status atual.');</script>");
            }
        }
        else
        {
            Response.Write("<script>alert('Pedido não localizado');</script>");
        }
    }

    protected void btnEnviar_OnClick(object sender, EventArgs e)
    {
        pnSelecionarPedido.Visible = true;
        pnEnviarPedido.Visible = false;
        int idPedido = 0;
        int.TryParse(txtPedidoId.Text, out idPedido);
        if (idPedido.ToString().Length != 5)
        {
            idPedido = rnFuncoes.retornaIdInterno(idPedido);
        }

        var pedidoDc = new dbCommerceDataContext();
        int idPedidoJadlog = (from c in pedidoDc.tbPedidoJadlogs where c.idPedido == idPedido select c).FirstOrDefault() == null ? 0 : (from c in pedidoDc.tbPedidoJadlogs where c.idPedido == idPedido select c).FirstOrDefault().idPedidoJadlog;

        var pedidosDc = new dbCommerceDataContext();
        HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
        usuarioLogadoId = Request.Cookies["usuarioLogadoId"];
        if (usuarioLogadoId == null)
        {
            return;
        }


        if (ddlTransportadora.SelectedValue == "jadlog")
        {
            //var pedidoJadlog = idPedidoJadlog == 0 ? new tbPedidoJadlog() : (from c in pedidoDc.tbPedidoJadlogs where c.idPedido == idPedido select c).FirstOrDefault();
            var pedidoJadlog = new tbPedidoJadlog();
            pedidoJadlog.vol1 = Convert.ToInt32(txtVol1.Text);
            pedidoJadlog.vol2 = Convert.ToInt32(txtVol2.Text);
            pedidoJadlog.vol3 = Convert.ToInt32(txtVol3.Text);
            pedidoJadlog.vol4 = Convert.ToInt32(txtVol4.Text);
            pedidoJadlog.vol5 = Convert.ToInt32(txtVol5.Text);
            pedidoJadlog.vol6 = Convert.ToInt32(txtVol6.Text);
            pedidoJadlog.vol7 = Convert.ToInt32(txtVol7.Text);
            pedidoJadlog.vol8 = Convert.ToInt32(txtVol8.Text);
            pedidoJadlog.kg = pedidoJadlog.vol1 + pedidoJadlog.vol2 + pedidoJadlog.vol3 + pedidoJadlog.vol4 +
                              pedidoJadlog.vol5 + pedidoJadlog.vol6 + pedidoJadlog.vol7 + pedidoJadlog.vol8;
            pedidoJadlog.faixa = txtFaixa.Text;
            pedidoJadlog.frete = Convert.ToDecimal(txtFrete.Text);
            pedidoJadlog.interior = Convert.ToDecimal(txtInterior.Text);
            if (ddlTransportadora.SelectedValue == "jadlog")
            {
                if (Convert.ToDecimal(txtNota.Text) > 0)
                {
                    pedidoJadlog.seguro = Convert.ToDecimal(txtNota.Text)*Convert.ToDecimal("0,0066");
                }
                else
                {
                    pedidoJadlog.seguro = Convert.ToDecimal("0,33");
                }
            }
            else
            {
                pedidoJadlog.seguro = 0;
            }
            pedidoJadlog.nota = Convert.ToDecimal(txtNota.Text);
            pedidoJadlog.ar = Convert.ToDecimal(txtValorAr.Text);
            pedidoJadlog.maoPropria = Convert.ToDecimal(txtValorMaoPropria.Text);
            pedidoJadlog.total = pedidoJadlog.frete + pedidoJadlog.interior + (decimal) pedidoJadlog.seguro +
                                 (decimal) pedidoJadlog.ar + (decimal) pedidoJadlog.maoPropria;

            pedidoJadlog.idPedido = idPedido;
            pedidoJadlog.usuario =
                rnUsuarios.usuarioSeleciona_PorUsuarioId(int.Parse(usuarioLogadoId.Value.ToString())).Tables[0].Rows[0][
                    "usuarioNome"].ToString();
            pedidoJadlog.data = DateTime.Now;
            pedidoJadlog.transportadora = ddlTransportadora.SelectedValue;
            pedidoDc.tbPedidoJadlogs.InsertOnSubmit(pedidoJadlog);
            pedidoDc.SubmitChanges();


            var pedido = (from c in pedidoDc.tbPedidos where c.pedidoId == pedidoJadlog.idPedido select c).First();
            var clientesDc = new dbCommerceDataContext();
            var cliente = (from c in clientesDc.tbClientes where c.clienteId == pedido.clienteId select c).First();

            if (idPedidoJadlog == 0) rnPedidos.pedidoAlteraStatus(5, pedidoJadlog.idPedido);
            if (idPedidoJadlog == 0)
            {
                rnPedidos.alteraEstoqueProdutosPorPedido(idPedido);
                interacaoInclui("Pedido Enviado", "True", pedidoJadlog.idPedido);
            }
            else
            {
                interacaoInclui("Pedido reenviado à Jadlog", "False", pedidoJadlog.idPedido);
            }
            if (idPedidoJadlog == 0)
                rnEmails.enviaAlteracaoDeStatus(cliente.clienteNome, pedidoJadlog.idPedido.ToString(), "Pedido Enviado", cliente.clienteEmail);

            enviaEmailJadlog(pedidoJadlog.idPedidoJadlog, false);
            txtVol1.Text = "0";
            txtVol2.Text = "0";
            txtVol3.Text = "0";
            txtVol4.Text = "0";
            txtVol5.Text = "0";
            txtVol6.Text = "0";
            txtVol7.Text = "0";
            txtVol8.Text = "0";
            txtKg.Text = "0";
            txtFaixa.Text = "0";
            txtFrete.Text = "0";
            txtInterior.Text = "0";
            //txtTotal.Text = "0";
            txtNota.Text = "0";
            txtPedidoId.Text = "";
            pnSelecionarPedido.Visible = true;
            pnEnviarPedido.Visible = false;
            Response.Write("<script>alert('Pedido enviado com sucesso');</script>");
            fillGrid(true);
        }
        else
        {
            //var pedidoJadlog = idPedidoJadlog == 0 ? new tbPedidoJadlog() : (from c in pedidoDc.tbPedidoJadlogs where c.idPedido == idPedido select c).FirstOrDefault();
            var pedidoJadlog = new tbPedidoJadlog();
            pedidoJadlog.vol1 = Convert.ToInt32(txtCorreiosVol1.Text);
            pedidoJadlog.vol2 = Convert.ToInt32(txtCorreiosVol2.Text);
            pedidoJadlog.vol3 = Convert.ToInt32(txtCorreiosVol3.Text);
            pedidoJadlog.vol4 = Convert.ToInt32(txtCorreiosVol4.Text);
            pedidoJadlog.cubagem1 = Convert.ToInt32(txtCorreiosCubagem1.Text);
            pedidoJadlog.cubagem2 = Convert.ToInt32(txtCorreiosCubagem2.Text);
            pedidoJadlog.cubagem3 = Convert.ToInt32(txtCorreiosCubagem3.Text);
            pedidoJadlog.cubagem4 = Convert.ToInt32(txtCorreiosCubagem4.Text);
            pedidoJadlog.vol5 = 0;
            pedidoJadlog.vol6 = 0;
            pedidoJadlog.vol7 = 0;
            pedidoJadlog.vol8 = 0;
            pedidoJadlog.kg = pedidoJadlog.vol1 + pedidoJadlog.vol2 + pedidoJadlog.vol3 + pedidoJadlog.vol4 + pedidoJadlog.vol5 + pedidoJadlog.vol6 + pedidoJadlog.vol7 + pedidoJadlog.vol8;
            pedidoJadlog.faixa = txtCorreiosFaixa.Text;
            pedidoJadlog.frete = 0;
            pedidoJadlog.interior = 0;

            pedidoJadlog.seguro = Convert.ToDecimal(txtCorreiosSeguro1.Text);
            pedidoJadlog.seguro2 = Convert.ToDecimal(txtCorreiosSeguro2.Text);
            pedidoJadlog.seguro3 = Convert.ToDecimal(txtCorreiosSeguro3.Text);
            pedidoJadlog.seguro4 = Convert.ToDecimal(txtCorreiosSeguro4.Text);

            pedidoJadlog.ar = Convert.ToDecimal(txtCorreiosAr1.Text);
            pedidoJadlog.ar2 = Convert.ToDecimal(txtCorreiosAr2.Text);
            pedidoJadlog.ar3 = Convert.ToDecimal(txtCorreiosAr3.Text);
            pedidoJadlog.ar4 = Convert.ToDecimal(txtCorreiosAr4.Text);

            pedidoJadlog.maoPropria = Convert.ToDecimal(txtCorreiosMaoPropria1.Text);
            pedidoJadlog.maoPropria2 = Convert.ToDecimal(txtCorreiosMaoPropria2.Text);
            pedidoJadlog.maoPropria3 = Convert.ToDecimal(txtCorreiosMaoPropria3.Text);
            pedidoJadlog.maoPropria4 = Convert.ToDecimal(txtCorreiosMaoPropria4.Text);

            pedidoJadlog.valorVol1 = Convert.ToDecimal(txtCorreiosValor1.Text);
            pedidoJadlog.valorVol2 = Convert.ToDecimal(txtCorreiosValor2.Text);
            pedidoJadlog.valorVol3 = Convert.ToDecimal(txtCorreiosValor3.Text);
            pedidoJadlog.valorVol4 = Convert.ToDecimal(txtCorreiosValor4.Text);

            pedidoJadlog.rastreio1 = txtCorreiosRastreio1.Text;
            pedidoJadlog.rastreio2 = txtCorreiosRastreio2.Text;
            pedidoJadlog.rastreio3 = txtCorreiosRastreio3.Text;
            pedidoJadlog.rastreio4 = txtCorreiosRastreio4.Text;

            
            pedidoJadlog.nota = 0;
            pedidoJadlog.total = (decimal)pedidoJadlog.valorVol1 + (decimal)pedidoJadlog.valorVol2 + (decimal)pedidoJadlog.valorVol3 + (decimal)pedidoJadlog.valorVol4 + (decimal)pedidoJadlog.seguro + (decimal)pedidoJadlog.seguro2 + (decimal)pedidoJadlog.seguro3 + (decimal)pedidoJadlog.seguro4 + (decimal)pedidoJadlog.ar + (decimal)pedidoJadlog.ar2 + (decimal)pedidoJadlog.ar3 + (decimal)pedidoJadlog.ar4 + (decimal)pedidoJadlog.maoPropria + (decimal)pedidoJadlog.maoPropria2 + (decimal)pedidoJadlog.maoPropria3 + (decimal)pedidoJadlog.maoPropria4;

            pedidoJadlog.idPedido = idPedido;
            pedidoJadlog.usuario = rnUsuarios.usuarioSeleciona_PorUsuarioId(int.Parse(usuarioLogadoId.Value.ToString())).Tables[0].Rows[0]["usuarioNome"].ToString();
            pedidoJadlog.data = DateTime.Now;
            pedidoJadlog.transportadora = ddlTransportadora.SelectedValue;
            pedidoDc.tbPedidoJadlogs.InsertOnSubmit(pedidoJadlog);
            pedidoDc.SubmitChanges();


            var pedido = (from c in pedidoDc.tbPedidos where c.pedidoId == pedidoJadlog.idPedido select c).First();
            var clientesDc = new dbCommerceDataContext();
            var cliente = (from c in clientesDc.tbClientes where c.clienteId == pedido.clienteId select c).First();

            if (idPedidoJadlog == 0) rnPedidos.pedidoAlteraStatus(5, pedidoJadlog.idPedido);
            if (idPedidoJadlog == 0)
            {
                rnPedidos.alteraEstoqueProdutosPorPedido(idPedido);
                interacaoInclui("Pedido Enviado", "True", pedidoJadlog.idPedido);
            }
            else
            {
                interacaoInclui("Pedido reenviado", "False", pedidoJadlog.idPedido);
            }
            if (idPedidoJadlog == 0) rnEmails.enviaAlteracaoDeStatus(cliente.clienteNome, pedidoJadlog.idPedido.ToString(), "Pedido Enviado", cliente.clienteEmail);

            txtVol1.Text = "0";
            txtVol2.Text = "0";
            txtVol3.Text = "0";
            txtVol4.Text = "0";
            txtVol5.Text = "0";
            txtVol6.Text = "0";
            txtVol7.Text = "0";
            txtVol8.Text = "0";
            txtKg.Text = "0";
            txtFaixa.Text = "0";
            txtFrete.Text = "0";
            txtInterior.Text = "0";
            //txtTotal.Text = "0";
            txtNota.Text = "0";
            txtPedidoId.Text = "";
            txtCorreiosAr1.Text = "0";
            txtCorreiosAr2.Text = "0";
            txtCorreiosAr3.Text = "0";
            txtCorreiosAr4.Text = "0";
            txtCorreiosCubagem1.Text = "0";
            txtCorreiosCubagem2.Text = "0";
            txtCorreiosCubagem3.Text = "0";
            txtCorreiosCubagem4.Text = "0";
            txtCorreiosFaixa.Text = "";
            txtCorreiosMaoPropria1.Text = "0";
            txtCorreiosMaoPropria2.Text = "0";
            txtCorreiosMaoPropria3.Text = "0";
            txtCorreiosMaoPropria4.Text = "0";
            txtCorreiosSeguro1.Text = "0";
            txtCorreiosSeguro2.Text = "0";
            txtCorreiosSeguro3.Text = "0";
            txtCorreiosSeguro4.Text = "0";
            txtCorreiosValor1.Text = "0";
            txtCorreiosValor2.Text = "0";
            txtCorreiosValor3.Text = "0";
            txtCorreiosValor4.Text = "0";
            txtCorreiosVol1.Text = "0";
            txtCorreiosVol2.Text = "0";
            txtCorreiosVol3.Text = "0";
            txtCorreiosVol4.Text = "0";

            pnSelecionarPedido.Visible = true;
            pnEnviarPedido.Visible = false;
            Response.Write("<script>alert('Pedido enviado com sucesso');</script>");
            fillGrid(true);
            txtPedidoId.Focus();
        }
    }

    private void enviaEmailJadlog(int idPedidoJadlog, bool cancelado)
    {
        var pedidoDc = new dbCommerceDataContext();
        var pedidoJadlog = (from c in pedidoDc.tbPedidoJadlogs where c.idPedidoJadlog == idPedidoJadlog select c).FirstOrDefault();
        var pedido = (from c in pedidoDc.tbPedidos where c.pedidoId == pedidoJadlog.idPedido select c).First();
        var clientesDc = new dbCommerceDataContext();
        var cliente = (from c in clientesDc.tbClientes where c.clienteId == pedido.clienteId select c).First();

        var email = new StringBuilder();
        email.AppendFormat("Pedido: {0}<br><br>", rnFuncoes.retornaIdCliente(pedidoJadlog.idPedido));
        if (pedidoJadlog.vol1 > 0) email.AppendFormat("VOL 1: {0}<br>", pedidoJadlog.vol1);
        if (pedidoJadlog.vol2 > 0) email.AppendFormat("VOL 2: {0}<br>", pedidoJadlog.vol2);
        if (pedidoJadlog.vol3 > 0) email.AppendFormat("VOL 3: {0}<br>", pedidoJadlog.vol3);
        if (pedidoJadlog.vol4 > 0) email.AppendFormat("VOL 4: {0}<br>", pedidoJadlog.vol4);
        if (pedidoJadlog.vol5 > 0) email.AppendFormat("VOL 5: {0}<br>", pedidoJadlog.vol5);
        if (pedidoJadlog.vol6 > 0) email.AppendFormat("VOL 6: {0}<br>", pedidoJadlog.vol6);
        if (pedidoJadlog.vol7 > 0) email.AppendFormat("VOL 7: {0}<br>", pedidoJadlog.vol7);
        if (pedidoJadlog.vol8 > 0) email.AppendFormat("VOL 8: {0}<br>", pedidoJadlog.vol8);

        email.AppendFormat("KG: {0}<br>", pedidoJadlog.kg);
        email.AppendFormat("Faixa: {0}<br>", pedidoJadlog.faixa);
        email.AppendFormat("Frete: {0}<br>", pedidoJadlog.frete.ToString("C"));
        email.AppendFormat("Interior: {0}<br>", pedidoJadlog.interior.ToString("C"));
        email.AppendFormat("Seguro: {0}<br><br>", Convert.ToDecimal(pedidoJadlog.seguro).ToString("C"));
        email.AppendFormat("Total: {0}<br>", pedidoJadlog.total.ToString("C"));
        email.AppendFormat("Nota: {0}<br><br>", pedidoJadlog.nota.ToString("C"));

        email.AppendFormat("CPF: {0}<br>", cliente.clienteCPFCNPJ);
        email.AppendFormat("Tel. Residencial: {0}<br>", cliente.clienteFoneResidencial);
        email.AppendFormat("Tel. Comercial: {0}<br>", cliente.clienteFoneComercial);
        email.AppendFormat("Celular: {0}<br><br>", cliente.clienteFoneCelular);

        email.AppendFormat("<b>Destinatário:</b><br>{0}<br>", pedido.endNomeDoDestinatario);
        email.AppendFormat("{0} {1}, {2}<br>", pedido.endRua, pedido.endNumero, pedido.endComplemento);
        email.AppendFormat("{0} - {1} - {2}<br>", pedido.endBairro, pedido.endCidade, pedido.endEstado);
        email.AppendFormat("{0}<br>", pedido.endCep);
        email.AppendFormat("{0}<br>", pedido.endReferenciaParaEntrega);

        string assunto = "";
        if (cancelado)
        {
            assunto = "CANCELAR Pedido Grão de Gente " + rnFuncoes.retornaIdCliente(pedidoJadlog.idPedido);
        }
        else
        {
            assunto = "Pedido Grão de Gente " + rnFuncoes.retornaIdCliente(pedidoJadlog.idPedido);
        }
        if (pedidoJadlog.transportadora == "jadlog")
        {
            //rnEmails.EnviaEmail("compras@graodegente.com.br", "andre@bark.com.br", "", "", "", email.ToString(), assunto);
            MailMessage mailMessage = new MailMessage();
            mailMessage.From = new MailAddress("compras@graodegente.com.br");
            mailMessage.To.Add("callili.ibt@jadlog.com.br");
            mailMessage.To.Add("compras@graodegente.com.br");
            mailMessage.ReplyTo = new MailAddress("compras@graodegente.com.br");
            mailMessage.Subject = assunto;
            mailMessage.Body = email.ToString();
            mailMessage.IsBodyHtml = true;
            mailMessage.Priority = MailPriority.Normal;

            var smtpClient = new SmtpClient(ConfigurationManager.AppSettings["emailSmtp"]);
            smtpClient.EnableSsl = true;
            smtpClient.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["emailLogin"], ConfigurationManager.AppSettings["emailSenha"]);

            MemoryStream memoryStream = new MemoryStream();
            if (!string.IsNullOrEmpty(pedido.danfe))
            {
                using (FileStream fs = File.OpenRead(System.Configuration.ConfigurationManager.AppSettings["caminhoFisico"] + "\\admin\\notas\\danfes\\" + pedido.danfe))
                {
                    fs.CopyTo(memoryStream);
                }
                memoryStream.Seek(0, SeekOrigin.Begin);
                // Create attachment
                ContentType contentType = new ContentType();
                contentType.MediaType = "application/pdf";
                contentType.Name = pedido.danfe;
                contentType.CharSet = "UTF-8";
                Attachment attachment = new Attachment(memoryStream, contentType);

                // Add the attachment
                mailMessage.Attachments.Add(attachment);
            }
            smtpClient.Send(mailMessage);
            memoryStream.Dispose();
            //rnEmails.EnviaEmail("compras@graodegente.com.br", "callili.ibt@jadlog.com.br", "compras@graodegente.com.br", "", "", email.ToString(), assunto);
            //rnEmails.EnviaEmail("compras@graodegente.com.br", "atendimento@bark.com.br", "compras@graodegente.com.br", "andre@bark.com.br, atendimento@bark.com.br", "", email.ToString(), assunto);
        }
        else
        {
            //rnEmails.EnviaEmail("compras@graodegente.com.br", "atendimento@bark.com.br", "andre@bark.com.br", "", "", email.ToString(), "Pedido Grão de Gente " + pedidoJadlog.idPedido);
        }
    }

    protected void btnEnviarEmail_OnCommand(object sender, CommandEventArgs e)
    {
        enviaEmailJadlog(Convert.ToInt32(e.CommandArgument), false);
        Response.Write("<script>alert('Pedido enviado com sucesso');</script>");
    }

    protected void btnVerPedidos_OnClick(object sender, EventArgs e)
    {
        pnEnviarJadlog.Visible = false;
        pnVerEnvios.Visible = true;
    }

    protected void btnEnviarPedidos_OnClick(object sender, EventArgs e)
    {
        pnEnviarJadlog.Visible = true;
        pnVerEnvios.Visible = false;
    }

    protected void btnEditarValor_OnCommand(object sender, CommandEventArgs e)
    {
        hiddenIdPedidoJadlogEditar.Value = e.CommandArgument.ToString();
        int idPedidoJadlog = Convert.ToInt32(e.CommandArgument);
        var pedidoDc = new dbCommerceDataContext();
        var pedido = (from c in pedidoDc.tbPedidoJadlogs where c.idPedidoJadlog == idPedidoJadlog select c).FirstOrDefault();
        if (pedido.valorRecebido > 0)
        {
            txtValorDiferenca.Text = pedido.valorRecebido.ToString();
        }
        else
        {
            txtValorDiferenca.Text = "0";
        }
        litPedidoIdDiferenca.Text = pedido.idPedido.ToString();
        pnGridEditar.Visible = true;
        pnGridVerPedidos.Visible = false;
    }

    protected void btnGravarDiferenca_onClick(object sender, EventArgs e)
    {
        int idPedidoJadlog = Convert.ToInt32(hiddenIdPedidoJadlogEditar.Value);
        var pedidoDc = new dbCommerceDataContext();
        var pedido = (from c in pedidoDc.tbPedidoJadlogs where c.idPedidoJadlog == idPedidoJadlog select c).FirstOrDefault();
        pedido.valorRecebido = Convert.ToDecimal(txtValorDiferenca.Text);
        pedidoDc.SubmitChanges();
        txtValorDiferenca.Text = "0";
        pnGridEditar.Visible = false;
        pnGridVerPedidos.Visible = true;
        fillGrid(true);
    }

    public void interacaoInclui(string interacao, string alteracaoDeEstatus, int pedidoId)
    {
        HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
        usuarioLogadoId = Request.Cookies["usuarioLogadoId"];
        if (usuarioLogadoId != null)
        {
            rnInteracoes.interacaoInclui(pedidoId, interacao, rnUsuarios.usuarioSeleciona_PorUsuarioId(int.Parse(usuarioLogadoId.Value.ToString())).Tables[0].Rows[0]["usuarioNome"].ToString(), alteracaoDeEstatus);
        }
    }

    protected void btnCancelar_OnCommand(object sender, CommandEventArgs e)
    {
        int idPedidoJadlog = Convert.ToInt32(e.CommandArgument);
        var pedidoDc = new dbCommerceDataContext();
        var pedido = (from c in pedidoDc.tbPedidoJadlogs where c.idPedidoJadlog == idPedidoJadlog select c).FirstOrDefault();
        pedido.cancelado = true;
        pedidoDc.SubmitChanges();
        fillGrid(true);
        enviaEmailJadlog(idPedidoJadlog, true);
    }

    protected void ddlTransportadora_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlTransportadora.SelectedValue == "jadlog")
        {
            pnCadastroJadlog.Visible = true;
            pnCadastroCorreios.Visible = false;
        }
        else
        {
            pnCadastroJadlog.Visible = false;
            pnCadastroCorreios.Visible = true;
        }
    }

    protected void grd_OnDataBound(object sender, EventArgs e)
    {
        int inicio = 0;
        int fim = 0;

        if (grd.PageIndex == 0)
        {
            inicio = 0;
            fim = inicio + grd.GetCurrentPageRowValues("idPedido").Count;
        }
        else
        {
            inicio += inicio + grd.SettingsPager.PageSize * grd.PageIndex;
            fim += inicio + grd.GetCurrentPageRowValues("idPedido").Count;
        }

        decimal totalVol1 = 0;
        decimal totalVol2 = 0;
        decimal totalVol3 = 0;
        decimal totalVol4 = 0;
        decimal totalVol5 = 0;
        decimal totalVol6 = 0;
        decimal totalVol7 = 0;
        decimal totalVol8 = 0;
        decimal totalCubagem1 = 0;
        decimal totalCubagem2 = 0;
        decimal totalCubagem3 = 0;
        decimal totalCubagem4 = 0;

        decimal totalRastreio1 = 0;
        decimal totalRastreio2 = 0;
        decimal totalRastreio3 = 0;
        decimal totalRastreio4 = 0;

        decimal totalSeguro = 0;
        decimal totalSeguro2 = 0;
        decimal totalSeguro3 = 0;
        decimal totalSeguro4 = 0;

        decimal totalValorVol1 = 0;
        decimal totalValorVol2 = 0;
        decimal totalValorVol3 = 0;
        decimal totalValorVol4 = 0;

        decimal totalAr = 0;
        decimal totalAr2 = 0;
        decimal totalAr3 = 0;
        decimal totalAr4 = 0;

        decimal totalMaoPropria = 0;
        decimal totalMaoPropria2 = 0;
        decimal totalMaoPropria3 = 0;
        decimal totalMaoPropria4 = 0;


        for (int i = inicio; i < fim; i++)
        {
            totalVol1 += grd.GetRowValues(i, new string[] { "vol1" }) == null ? 0 : (int)grd.GetRowValues(i, new string[] { "vol1" });
            totalVol2 += grd.GetRowValues(i, new string[] { "vol2" }) == null ? 0 : (int)grd.GetRowValues(i, new string[] { "vol2" });
            totalVol3 += grd.GetRowValues(i, new string[] { "vol3" }) == null ? 0 : (int)grd.GetRowValues(i, new string[] { "vol3" });
            totalVol4 += grd.GetRowValues(i, new string[] { "vol4" }) == null ? 0 : (int)grd.GetRowValues(i, new string[] { "vol4" });
            totalVol5 += grd.GetRowValues(i, new string[] { "vol5" }) == null ? 0 : (int)grd.GetRowValues(i, new string[] { "vol5" });
            totalVol6 += grd.GetRowValues(i, new string[] { "vol6" }) == null ? 0 : (int)grd.GetRowValues(i, new string[] { "vol6" });
            totalVol7 += grd.GetRowValues(i, new string[] { "vol7" }) == null ? 0 : (int)grd.GetRowValues(i, new string[] { "vol7" });
            totalVol8 += grd.GetRowValues(i, new string[] { "vol8" }) == null ? 0 : (int)grd.GetRowValues(i, new string[] { "vol8" });
            var cubagem1 = grd.GetRowValues(i, new string[] {"cubagem1"});
            var cubagem2 = grd.GetRowValues(i, new string[] {"cubagem2"});
            var cubagem3 = grd.GetRowValues(i, new string[] {"cubagem3"});
            var cubagem4 = grd.GetRowValues(i, new string[] {"cubagem4"});
            totalCubagem1 += cubagem1 == null ? 0 : Convert.ToInt32(cubagem1);
            totalCubagem2 += cubagem1 == null ? 0 : Convert.ToInt32(cubagem2);
            totalCubagem3 += cubagem1 == null ? 0 : Convert.ToInt32(cubagem3);
            totalCubagem4 += cubagem1 == null ? 0 : Convert.ToInt32(cubagem4);
            var rastreio1 = grd.GetRowValues(i, new string[] { "rastreio1" });
            var rastreio2 = grd.GetRowValues(i, new string[] { "rastreio2" });
            var rastreio3 = grd.GetRowValues(i, new string[] { "rastreio3" });
            var rastreio4 = grd.GetRowValues(i, new string[] { "rastreio4" });
            totalRastreio1 += rastreio1 == null ? 0 : string.IsNullOrEmpty(rastreio1.ToString()) ? 0 : 1;
            totalRastreio2 += rastreio2 == null ? 0 : string.IsNullOrEmpty(rastreio2.ToString()) ? 0 : 1;
            totalRastreio3 += rastreio3 == null ? 0 : string.IsNullOrEmpty(rastreio3.ToString()) ? 0 : 1;
            totalRastreio4 += rastreio4 == null ? 0 : string.IsNullOrEmpty(rastreio3.ToString()) ? 0 : 1;
            var seguro = grd.GetRowValues(i, new string[] { "seguro" });
            var seguro2 = grd.GetRowValues(i, new string[] { "seguro2" });
            var seguro3 = grd.GetRowValues(i, new string[] { "seguro3" });
            var seguro4 = grd.GetRowValues(i, new string[] { "seguro4" });
            totalSeguro += seguro4 == null ? 0 : string.IsNullOrEmpty(seguro.ToString()) ? 0 : Convert.ToDecimal(seguro.ToString()) == 0 ? 0 : 1;
            totalSeguro2 += seguro4 == null ? 0 : string.IsNullOrEmpty(seguro2.ToString()) ? 0 : Convert.ToDecimal(seguro2.ToString()) == 0 ? 0 : 1;
            totalSeguro3 += seguro4 == null ? 0 : string.IsNullOrEmpty(seguro3.ToString()) ? 0 : Convert.ToDecimal(seguro3.ToString()) == 0 ? 0 : 1;
            totalSeguro4 += seguro4 == null ? 0 : string.IsNullOrEmpty(seguro4.ToString()) ? 0 : Convert.ToDecimal(seguro4.ToString()) == 0 ? 0 : 1;
            var valorVol1 = grd.GetRowValues(i, new string[] { "valorVol1" });
            var valorVol2 = grd.GetRowValues(i, new string[] { "valorVol2" });
            var valorVol3 = grd.GetRowValues(i, new string[] { "valorVol3" });
            var valorVol4 = grd.GetRowValues(i, new string[] { "valorVol4" });
            totalValorVol1 += valorVol4 == null ? 0 : string.IsNullOrEmpty(valorVol1.ToString()) ? 0 : Convert.ToDecimal(valorVol1.ToString()) == 0 ? 0 : 1;
            totalValorVol2 += valorVol4 == null ? 0 : string.IsNullOrEmpty(valorVol2.ToString()) ? 0 : Convert.ToDecimal(valorVol2.ToString()) == 0 ? 0 : 1;
            totalValorVol3 += valorVol4 == null ? 0 : string.IsNullOrEmpty(valorVol3.ToString()) ? 0 : Convert.ToDecimal(valorVol3.ToString()) == 0 ? 0 : 1;
            totalValorVol4 += valorVol4 == null ? 0 : string.IsNullOrEmpty(valorVol4.ToString()) ? 0 : Convert.ToDecimal(valorVol4.ToString()) == 0 ? 0 : 1;

            var ar = grd.GetRowValues(i, new string[] { "ar" });
            var ar2 = grd.GetRowValues(i, new string[] { "ar2" });
            var ar3 = grd.GetRowValues(i, new string[] { "ar3" });
            var ar4 = grd.GetRowValues(i, new string[] { "ar4" });
            totalAr += ar == null ? 0 : string.IsNullOrEmpty(ar.ToString()) ? 0 : Convert.ToDecimal(ar.ToString()) == 0 ? 0 : 1;
            totalAr2 += ar2 == null ? 0 : string.IsNullOrEmpty(ar2.ToString()) ? 0 : Convert.ToDecimal(ar2.ToString()) == 0 ? 0 : 1;
            totalAr3 += ar3 == null ? 0 : string.IsNullOrEmpty(ar3.ToString()) ? 0 : Convert.ToDecimal(ar3.ToString()) == 0 ? 0 : 1;
            totalAr4 += ar4 == null ? 0 : string.IsNullOrEmpty(ar4.ToString()) ? 0 : Convert.ToDecimal(ar4.ToString()) == 0 ? 0 : 1;

            var maoPropria = grd.GetRowValues(i, new string[] { "maoPropria" });
            var maoPropria2 = grd.GetRowValues(i, new string[] { "maoPropria2" });
            var maoPropria3 = grd.GetRowValues(i, new string[] { "maoPropria3" });
            var maoPropria4 = grd.GetRowValues(i, new string[] { "maoPropria4" });
            totalMaoPropria += maoPropria == null ? 0 : string.IsNullOrEmpty(maoPropria.ToString()) ? 0 : Convert.ToDecimal(maoPropria.ToString()) == 0 ? 0 : 1;
            totalMaoPropria2 += maoPropria2 == null ? 0 : string.IsNullOrEmpty(maoPropria2.ToString()) ? 0 : Convert.ToDecimal(maoPropria2.ToString()) == 0 ? 0 : 1;
            totalMaoPropria3 += maoPropria3 == null ? 0 : string.IsNullOrEmpty(maoPropria3.ToString()) ? 0 : Convert.ToDecimal(maoPropria3.ToString()) == 0 ? 0 : 1;
            totalMaoPropria4 += maoPropria4 == null ? 0 : string.IsNullOrEmpty(maoPropria4.ToString()) ? 0 : Convert.ToDecimal(maoPropria4.ToString()) == 0 ? 0 : 1;
        }

        if (totalVol1 == 0) grd.Columns["vol1"].Visible = false;
        if (totalVol2 == 0) grd.Columns["vol2"].Visible = false;
        if (totalVol3 == 0) grd.Columns["vol3"].Visible = false;
        if (totalVol4 == 0) grd.Columns["vol4"].Visible = false;
        if (totalVol5 == 0) grd.Columns["vol5"].Visible = false;
        if (totalVol6 == 0) grd.Columns["vol6"].Visible = false;
        if (totalVol7 == 0) grd.Columns["vol7"].Visible = false;
        if (totalVol8 == 0) grd.Columns["vol8"].Visible = false;
        if (totalCubagem1 == 0) grd.Columns["cubagem1"].Visible = false;
        if (totalCubagem2 == 0) grd.Columns["cubagem2"].Visible = false;
        if (totalCubagem3 == 0) grd.Columns["cubagem3"].Visible = false;
        if (totalCubagem4 == 0) grd.Columns["cubagem4"].Visible = false;
        if (totalRastreio1 == 0) grd.Columns["rastreio1"].Visible = false;
        if (totalRastreio2 == 0) grd.Columns["rastreio2"].Visible = false;
        if (totalRastreio3 == 0) grd.Columns["rastreio3"].Visible = false;
        if (totalRastreio4 == 0) grd.Columns["rastreio4"].Visible = false;
        if (totalSeguro == 0) grd.Columns["seguro"].Visible = false;
        if (totalSeguro2 == 0) grd.Columns["seguro2"].Visible = false;
        if (totalSeguro3 == 0) grd.Columns["seguro3"].Visible = false;
        if (totalSeguro4 == 0) grd.Columns["seguro4"].Visible = false;

        if (totalValorVol1 == 0) grd.Columns["valorVol1"].Visible = false;
        if (totalValorVol2 == 0) grd.Columns["valorVol2"].Visible = false;
        if (totalValorVol3 == 0) grd.Columns["valorVol3"].Visible = false;
        if (totalValorVol4 == 0) grd.Columns["valorVol4"].Visible = false;

        if (totalAr == 0) grd.Columns["ar"].Visible = false;
        if (totalAr2 == 0) grd.Columns["ar2"].Visible = false;
        if (totalAr3 == 0) grd.Columns["ar3"].Visible = false;
        if (totalAr4 == 0) grd.Columns["ar4"].Visible = false;

        if (totalMaoPropria == 0) grd.Columns["maoPropria"].Visible = false;
        if (totalMaoPropria2 == 0) grd.Columns["maoPropria2"].Visible = false;
        if (totalMaoPropria3 == 0) grd.Columns["maoPropria3"].Visible = false;
        if (totalMaoPropria4 == 0) grd.Columns["maoPropria4"].Visible = false;


    }

    protected void grd_OnCustomUnboundColumnData(object sender, ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "pedidoIdCliente")
        {
            int pedidoId = Convert.ToInt32(e.GetListSourceFieldValue("idPedido"));
            e.Value = rnFuncoes.retornaIdCliente(pedidoId);
        }
        if (e.Column.FieldName == "diferenca")
        {
            decimal valorRecebido = Convert.ToInt32(e.GetListSourceFieldValue("valorRecebido"));
            decimal total = Convert.ToInt32(e.GetListSourceFieldValue("total"));
            e.Value = total - valorRecebido;
        }
    }


    protected void grid_AutoFilterCellEditorCreate(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewEditorCreateEventArgs e)
    {
        ASPxGridView grid = sender as ASPxGridView;
        if (e.Column.FieldName == "data")
        {
            DropDownEditProperties dde = new DropDownEditProperties();
            dde.EnableClientSideAPI = true;

            dateTemplate = new DateTemplate();

            dde.DropDownWindowTemplate = dateTemplate;
            e.EditorProperties = dde;
        }
    }
    protected void grid_AutoFilterCellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
    {
        ASPxGridView grid = sender as ASPxGridView;
        if (e.Column.FieldName == "data")
        {
            ASPxDropDownEdit dde = e.Editor as ASPxDropDownEdit;
            dde.ClientSideEvents.CloseUp = String.Format("function (s, e) {{ ApplyFilter(s, {0}, {1}); }}", dateTemplate.cIdFrom, dateTemplate.cIdTo);
            //dde.ClientSideEvents.DropDown = String.Format("function (s, e) {{ OnDropDown(s, {0}, {1}); }}", dateTemplate.cIdFrom, dateTemplate.cIdTo);
            dde.ReadOnly = true;
        }
    }

    protected void grid_ProcessColumnAutoFilter(object sender, ASPxGridViewAutoFilterEventArgs e)
    {
        if (e.Value == "|")
            return;
        if (e.Column.FieldName == "data")
        {
            if (e.Kind == GridViewAutoFilterEventKind.CreateCriteria)
            {
                Session["data"] = e.Value;
                String[] dates = e.Value.Split('|');
                DateTime dateFrom = Convert.ToDateTime(dates[0]),
                    dateTo = Convert.ToDateTime(dates[1]);
                e.Criteria = (new OperandProperty("data") >= dateFrom) &
                             (new OperandProperty("data") <= dateTo);
            }
            else
            {
                if (Session["data"] != null)
                    e.Value = Session["data"].ToString();
            }
        }
    }

    protected void btPdf_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WritePdfToResponse();
    }
    protected void btXsl_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteXlsToResponse();
    }
    protected void btRtf_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteRtfToResponse();
    }
    protected void btCsv_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteCsvToResponse();
    }
}