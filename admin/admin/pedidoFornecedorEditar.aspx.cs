﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_pedidoFornecedorEditar : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            txtDataEntrega.Attributes.Add("onkeypress", "Mascaras('DATA', '" + txtDataEntrega.ClientID + "');");
            txtDataVencimento.Attributes.Add("onkeypress", "Mascaras('DATA', '" + txtDataVencimento.ClientID + "');");
            txtDataPagamento.Attributes.Add("onkeypress", "Mascaras('DATA', '" + txtDataPagamento.ClientID + "');");
            //txtDataPrevista.Attributes.Add("onkeypress", "Mascaras('DATA', '" + txtDataPrevista.ClientID + "');");

            fillMotivos();
            fillLista();
            fillInteracoes();
            fillTotais();

        }
    }

    private void fillMotivos()
    {
        var data = new dbCommerceDataContext();
        var motivos = (from c in data.tbPedidoFornecedorMotivoAtrasos select c).ToList();
        ddlMotivoAtraso.Items.Clear();
        ddlMotivoAtraso.Items.Add(new ListItem("Selecione", ""));
        foreach (var item in motivos)
        {
            ddlMotivoAtraso.Items.Add(new ListItem(item.motivo, item.IdPedidoFornecedorMotivoAtraso.ToString()));
        }
    }
    private void fillLista()
    {
        lstProdutos.DataSource = sql;
        lstProdutos.DataBind();

        int idPedidoFornecedor = Convert.ToInt32("0" + Request.QueryString["idPedidoFornecedor"]);
        var pedidoDc = new dbCommerceDataContext();


        var pedidoFornecedor =
            (from c in pedidoDc.tbPedidoFornecedors where c.idPedidoFornecedor == idPedidoFornecedor select c)
                .FirstOrDefault();

        if (pedidoFornecedor != null)
        {
            litDataPedido.Text = pedidoFornecedor.data.ToShortDateString();
            litDataEntrega.Text = pedidoFornecedor.dataLimite.ToShortDateString();
            litNumeroPedido.Text = idPedidoFornecedor.ToString();

            var fornecedorDc = new dbCommerceDataContext();
            var fornecedor =
                (from c in fornecedorDc.tbProdutoFornecedors
                 where c.fornecedorId == pedidoFornecedor.idFornecedor
                 select c).FirstOrDefault();
            if (fornecedor != null)
            {
                litFornecedor.Text = fornecedor.fornecedorNome;
            }

            if (pedidoFornecedor.dataEntrega != null)
            {
                txtDataEntrega.Text = pedidoFornecedor.dataEntrega.Value.ToShortDateString();
            }
            if (pedidoFornecedor.dataPagamento != null)
            {
                txtDataPagamento.Text = pedidoFornecedor.dataPagamento.Value.ToShortDateString();

                if (!String.IsNullOrEmpty(txtDataPagamento.Text))
                    txtDataPagamento.Enabled = false;

            }
            if (pedidoFornecedor.dataVencimento != null)
            {
                txtDataVencimento.Text = pedidoFornecedor.dataVencimento.Value.ToShortDateString();
            }
            if (pedidoFornecedor.dataPrevista != null)
            {
                txtDataPrevista.Value = pedidoFornecedor.dataPrevista.Value;
                txtHoraPrevista.Value = pedidoFornecedor.dataPrevista.Value;
                hdfDataPrevista.Value = pedidoFornecedor.dataPrevista.Value.ToString();
            }
            string motivoatraso = "";
            try
            {
                motivoatraso = pedidoFornecedor.tbPedidoFornecedorMotivoAtraso.motivo;
            }
            catch (Exception ex) { }
            if(!String.IsNullOrEmpty(motivoatraso))
            {
                hdfidPedidoFornecedorMotivoAtraso.Value = pedidoFornecedor.tbPedidoFornecedorMotivoAtraso.motivo.ToString();
            }
            if (!string.IsNullOrEmpty(pedidoFornecedor.numeroCobranca))
            {
                txtNumeroCobranca.Text = pedidoFornecedor.numeroCobranca;
            }
            if (pedidoFornecedor.IdPedidoFornecedorMotivoAtraso != null)
            {
                ddlMotivoAtraso.Items.FindByValue(pedidoFornecedor.IdPedidoFornecedorMotivoAtraso.ToString()).Selected = true;
            }
        }
    }
    private void fillTotais()
    {
        var pedidosDc = new dbCommerceDataContext();
        int idPedidoFornecedor = Convert.ToInt32(Request.QueryString["idPedidoFornecedor"]);
        var pedidoItens = (from c in pedidosDc.tbPedidoFornecedorItems
                           where c.idPedidoFornecedor == idPedidoFornecedor
                           select new
                           {
                               custo = c.quantidade * c.custo,
                               diferenca = c.quantidade * ((c.diferenca == 0 ? c.custo : c.diferenca) - c.custo),
                               c.quantidade,
                               c.entregue
                           });
        litTotalQuantidade.Text = pedidoItens.Sum(x => x.quantidade).ToString();
        litTotalCusto.Text = pedidoItens.Sum(x => x.custo).ToString("C");
        litTotalDiferenca.Text = pedidoItens.Sum(x => x.diferenca).ToString("C");
        litTotalGeral.Text = (pedidoItens.Sum(x => x.diferenca) + pedidoItens.Sum(x => x.custo)).ToString("C");
        litTotalFaltandoEntregar.Text = pedidoItens.Any(x => x.entregue == false) ? pedidoItens.Where(x => x.entregue == false).Sum(x => x.custo).ToString("C") : "R$ 0,00";

        hfTotalCamposCusto.Value = lstProdutos.Items.Count.ToString();
    }
    private void fillInteracoes()
    {
        var pedidosDc = new dbCommerceDataContext();
        int idPedidoFornecedor = Convert.ToInt32(Request.QueryString["idPedidoFornecedor"]);
        var interacoes =
            (from c in pedidosDc.tbPedidoFornecedorInteracaos where c.idPedidoFornecedor == idPedidoFornecedor orderby c.idPedidoFornecedorInteracao descending select c);
        dtlInteracao.DataSource = interacoes;
        dtlInteracao.DataBind();
    }
    protected void btnGravar_OnClick(object sender, EventArgs e)
    {
        StringBuilder interacao = new StringBuilder();
        bool alteradoGeral = false;
        foreach (var item in lstProdutos.Items)
        {
            bool alterado = false;

            var litProdutoIdDaEmpresa = (Literal)item.FindControl("litProdutoIdDaEmpresa");
            var litComplementoIdDaEmpresa = (Literal)item.FindControl("litComplementoIdDaEmpresa");
            var txtCusto = (TextBox)item.FindControl("txtCusto");
            var txtDiferenca = (TextBox)item.FindControl("txtDiferenca");
            //var chkEntregue = (CheckBox)item.FindControl("chkEntregue");
            var hiddenProdutoId = (HiddenField)item.FindControl("hiddenProdutoId");
            var hiddenIdPEdidoFornecedorItem = (HiddenField)item.FindControl("hiddenIdPEdidoFornecedorItem");
            var lstEntregas = (ListView)item.FindControl("lstEntregas");
            int produtoId = Convert.ToInt32(hiddenProdutoId.Value);

            int idPedidoFornecedor = Convert.ToInt32(Request.QueryString["idPedidoFornecedor"]);

            decimal custo = Convert.ToDecimal(txtCusto.Text);
            decimal diferenca = Convert.ToDecimal(txtDiferenca.Text);

            var pedidosDc = new dbCommerceDataContext();
            var itensPedido = (from c in pedidosDc.tbPedidoFornecedorItems where c.idProduto == produtoId select c);
            var itemPedido = (from c in pedidosDc.tbPedidoFornecedorItems where c.idProduto == produtoId && c.idPedidoFornecedor == idPedidoFornecedor select c).FirstOrDefault();
            var itemPedidoCustoDiferente = (from c in pedidosDc.tbPedidoFornecedorItems where c.idProduto == produtoId && c.idPedidoFornecedor == idPedidoFornecedor && c.custo != custo select c).FirstOrDefault();
            var itemPedidoDiferencaDiferente = (from c in pedidosDc.tbPedidoFornecedorItems where c.idProduto == produtoId && c.idPedidoFornecedor == idPedidoFornecedor && c.diferenca != diferenca select c).FirstOrDefault();

            if (custo != itemPedido.custo | itemPedidoCustoDiferente != null)
            {
                interacao.AppendFormat("{0} {1} - Custo alterado - de {2} para {3}<br>", litProdutoIdDaEmpresa.Text, litComplementoIdDaEmpresa.Text, itemPedido.custo.ToString("C"), custo.ToString("C"));

                foreach (var itemEditar in itensPedido.Where(x => x.idPedidoFornecedor == idPedidoFornecedor && x.brinde == false))
                {
                    var produtosDc = new dbCommerceDataContext();
                    itemEditar.custo = custo;
                    alterado = true;
                }
                //foreach (var itemEditar in itensPedido.Where(x => x.entregue != true && x.brinde == false))
                //{
                //    var produtosDc = new dbCommerceDataContext();
                //    var produto = (from c in produtosDc.tbProdutos where c.produtoId == itemPedido.idProduto select c).First();
                //    produto.produtoPrecoDeCusto = custo;
                //    produtosDc.SubmitChanges();
                //    itemEditar.custo = custo;
                //    alterado = true;
                //}
            }
            if (diferenca != itemPedido.diferenca | itemPedidoDiferencaDiferente != null)
            {
                interacao.AppendFormat("{0} {1} - Diferença alterada - de {2} para {3}<br>", litProdutoIdDaEmpresa.Text, litComplementoIdDaEmpresa.Text, itemPedido.diferenca.ToString("C"), diferenca.ToString("C"));

                foreach (var itemEditar in itensPedido.Where(x => x.idPedidoFornecedor == idPedidoFornecedor))
                {
                    itemEditar.diferenca = diferenca;
                    alterado = true;
                }
            }

            foreach (var entregaItem in lstEntregas.Items)
            {
                var hdfItemPedidoId = (HiddenField)entregaItem.FindControl("hdfItemPedidoId");
                var chkEntregue = (CheckBox)entregaItem.FindControl("chkEntregue");
                int idPedidoFornecedorItem = Convert.ToInt32(hdfItemPedidoId.Value);
                var itemEditar = itensPedido.First(x => x.idPedidoFornecedorItem == idPedidoFornecedorItem);
                if (chkEntregue.Checked != itemEditar.entregue && chkEntregue.Checked)
                {
                    itemEditar.entregue = true;
                    itemEditar.dataEntrega = DateTime.Now;
                    alterado = true;
                    var produtoDc = new dbCommerceDataContext();
                    var produto =
                        (from c in produtoDc.tbProdutos where c.produtoId == itemEditar.idProduto select c)
                            .FirstOrDefault();
                    if (produto != null)
                    {

                        if (produto.estoqueReal == null) produto.estoqueReal = 0;
                        var logEstoque = new tbProdutoLogEstoque();
                        logEstoque.idProduto = produto.produtoId;
                        logEstoque.idOperacao = idPedidoFornecedor;
                        logEstoque.estoqueAnterior = Convert.ToInt32(produto.estoqueReal);
                        produto.estoqueReal += itemEditar.quantidade;
                        produtoDc.SubmitChanges();

                        logEstoque.estoqueAlterado = Convert.ToInt32(produto.estoqueReal);
                        logEstoque.data = DateTime.Now;
                        logEstoque.descricao = "Pedido do fornecedor " + idPedidoFornecedor;
                        produtoDc.tbProdutoLogEstoques.InsertOnSubmit(logEstoque);
                        produtoDc.SubmitChanges();
                    }
                    for (int i = 1; i <= itemEditar.quantidade; i++)
                    {
                        var checaProdutoFaltando = (from c in produtoDc.tbPedidoProdutoFaltandos
                                                    where
                                                        c.pedidoId == itemEditar.idPedido && c.produtoId == itemEditar.idProduto &&
                                                        c.entregue == false
                                                    select c).FirstOrDefault();
                        if (checaProdutoFaltando != null)
                        {
                            checaProdutoFaltando.entregue = true;
                            produtoDc.SubmitChanges();
                        }
                    }
                }
            }
            if (alterado)
            {
                pedidosDc.SubmitChanges();
                alteradoGeral = true;
            }
        }

        if (interacao.ToString() != "" && alteradoGeral)
        {
            var pedidosDc = new dbCommerceDataContext();
            HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
            usuarioLogadoId = Request.Cookies["usuarioLogadoId"];
            if (usuarioLogadoId == null)
            {
                return;
            }
            var interacaoObj = new tbPedidoFornecedorInteracao();
            interacaoObj.data = DateTime.Now;
            interacaoObj.idPedidoFornecedor = Convert.ToInt32(Request.QueryString["idPedidoFornecedor"]);
            interacaoObj.interacao = interacao.ToString();
            interacaoObj.usuario = rnUsuarios.usuarioSeleciona_PorUsuarioId(int.Parse(usuarioLogadoId.Value.ToString())).Tables[0].Rows[0]["usuarioNome"].ToString();
            pedidosDc.tbPedidoFornecedorInteracaos.InsertOnSubmit(interacaoObj);
            pedidosDc.SubmitChanges();
            fillInteracoes();
        }

        Response.Write("<script>alert('Pedido alterado com sucesso.');</script>");

        fillLista();
        fillTotais();
    }
    protected void lstProdutos_OnItemDataBound(object sender, ListViewItemEventArgs e)
    {
        var txtCusto = (TextBox)e.Item.FindControl("txtCusto");
        var txtDiferenca = (TextBox)e.Item.FindControl("txtDiferenca");
        var lstEntregas = (ListView)e.Item.FindControl("lstEntregas");
        var hiddenProdutoId = (HiddenField)e.Item.FindControl("hiddenProdutoId");
        var hplPedidos = (HyperLink)e.Item.FindControl("hplPedidos");
        int produtoId = Convert.ToInt32(hiddenProdutoId.Value);
        int idPedidoFornecedor = Convert.ToInt32("0" + Request.QueryString["idPedidoFornecedor"]);
        hplPedidos.NavigateUrl = "pedidosClientesRomaneio.aspx?romaneio="+ idPedidoFornecedor+"&produtoId=" + produtoId;

        var pedidoDc = new dbCommerceDataContext();
        var itensPedido = (from c in pedidoDc.tbPedidoFornecedorItems
                           where c.idProduto == produtoId && c.idPedidoFornecedor == idPedidoFornecedor
                           select c);
        lstEntregas.DataSource = itensPedido;
        lstEntregas.DataBind();
        /*var chkEntregue = (CheckBox)e.Item.FindControl("chkEntregue");
        if (chkEntregue.Checked) chkEntregue.Enabled = false;*/

        txtCusto.Attributes.Add("onkeyPress", "return moeda(this,'.',',',event);");
        txtDiferenca.Attributes.Add("onkeyPress", "return moeda(this,'.',',',event);");


    }
    protected void btSalvarInteracao_Click(object sender, ImageClickEventArgs e)
    {
        var pedidosDc = new dbCommerceDataContext();
        HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
        usuarioLogadoId = Request.Cookies["usuarioLogadoId"];
        if (usuarioLogadoId == null)
        {
            return;
        }
        var interacaoObj = new tbPedidoFornecedorInteracao();
        interacaoObj.data = DateTime.Now;
        interacaoObj.idPedidoFornecedor = Convert.ToInt32(Request.QueryString["idPedidoFornecedor"]);
        interacaoObj.interacao = txtInteracao.Text;
        interacaoObj.usuario = rnUsuarios.usuarioSeleciona_PorUsuarioId(int.Parse(usuarioLogadoId.Value.ToString())).Tables[0].Rows[0]["usuarioNome"].ToString();
        pedidosDc.tbPedidoFornecedorInteracaos.InsertOnSubmit(interacaoObj);
        pedidosDc.SubmitChanges();
        fillInteracoes();
        txtInteracao.Text = "";
    }
    protected void lstEntregas_OnItemDataBound(object sender, ListViewItemEventArgs e)
    {
        CheckBox chkEntregue = (CheckBox)e.Item.FindControl("chkEntregue");
        if (chkEntregue.Checked) chkEntregue.Enabled = false;
    }
    protected void btnGravarAlteracoes_OnClick(object sender, EventArgs e)
    {
        int idPedidoFornecedor = Convert.ToInt32("0" + Request.QueryString["idPedidoFornecedor"]);
        var pedidoDc = new dbCommerceDataContext();
        var pedidoFornecedor =
            (from c in pedidoDc.tbPedidoFornecedors where c.idPedidoFornecedor == idPedidoFornecedor select c)
                .FirstOrDefault();
        if (pedidoFornecedor != null)
        {
            string interacao = "";
            if (!string.IsNullOrEmpty(txtDataEntrega.Text))
            {
                pedidoFornecedor.dataEntrega = Convert.ToDateTime(txtDataEntrega.Text);
                interacao += "Data de entrega alterado para: " + txtDataEntrega.Text + "<br>";
            }
            if (!string.IsNullOrEmpty(txtDataPagamento.Text))
            {
                pedidoFornecedor.dataPagamento = Convert.ToDateTime(txtDataPagamento.Text);
                interacao += "Data de pagamento alterado para: " + txtDataPagamento.Text + "<br>";
            }
            if (!string.IsNullOrEmpty(txtDataVencimento.Text))
            {
                pedidoFornecedor.dataVencimento = Convert.ToDateTime(txtDataVencimento.Text);
                interacao += "Data de vencimento alterado para: " + txtDataVencimento.Text + "<br>";
            }
            if (!string.IsNullOrEmpty(txtDataPrevista.Text))
            {
                //pedidoFornecedor.dataPrevista = Convert.ToDateTime(txtDataPrevista.Text);
                DateTime dataprev = Convert.ToDateTime(txtDataPrevista.Text);
                pedidoFornecedor.dataPrevista = Convert.ToDateTime(txtDataPrevista.Text + " " + txtHoraPrevista.Text);
                if (!String.IsNullOrEmpty(hdfDataPrevista.Value))
                {
                    DateTime hddataprev = Convert.ToDateTime(hdfDataPrevista.Value);
                    if (dataprev.Year != hddataprev.Year | dataprev.Month != hddataprev.Month | dataprev.Day != hddataprev.Day)
                    {
                        interacao += "Data prevista do fornecedor alterado de : " + hddataprev.ToShortDateString() + " para " + dataprev.ToShortDateString() + "<br>";
                    }
                }
                else
                {
                    interacao += "Data prevista do fornecedor alterado para " + dataprev.ToShortDateString() + "<br>";
                }


            }
            if (!string.IsNullOrEmpty(ddlMotivoAtraso.SelectedItem.Value))
            {
                pedidoFornecedor.IdPedidoFornecedorMotivoAtraso = Convert.ToInt32(ddlMotivoAtraso.SelectedItem.Value);
                interacao += "Motivo do atraso alterado para: " + ddlMotivoAtraso.SelectedItem.Text + "<br>";
            }
            else
            {
                if (!string.IsNullOrEmpty(hdfidPedidoFornecedorMotivoAtraso.Value))
                {
                    pedidoFornecedor.IdPedidoFornecedorMotivoAtraso = null;
                    interacao += "Foi retirado o motivo do atraso";
                }
            }
            pedidoFornecedor.numeroCobranca = txtNumeroCobranca.Text;
            if ((pedidoFornecedor.numeroCobranca ?? "") != txtNumeroCobranca.Text) interacao += "Número da cobrança alterado para: " + txtNumeroCobranca.Text + "<br>";
            pedidoDc.SubmitChanges();


            var pedidosDc = new dbCommerceDataContext();
            HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
            usuarioLogadoId = Request.Cookies["usuarioLogadoId"];
            if (usuarioLogadoId == null)
            {
                return;
            }
            var interacaoObj = new tbPedidoFornecedorInteracao();
            interacaoObj.data = DateTime.Now;
            interacaoObj.idPedidoFornecedor = Convert.ToInt32(Request.QueryString["idPedidoFornecedor"]);
            interacaoObj.interacao = interacao;
            interacaoObj.usuario = rnUsuarios.usuarioSeleciona_PorUsuarioId(int.Parse(usuarioLogadoId.Value.ToString())).Tables[0].Rows[0]["usuarioNome"].ToString();
            pedidosDc.tbPedidoFornecedorInteracaos.InsertOnSubmit(interacaoObj);
            pedidosDc.SubmitChanges();
        }

        Response.Write("<script>alert('Dados alterados com sucesso.');</script>");
    }
}