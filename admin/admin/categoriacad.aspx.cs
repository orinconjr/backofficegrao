﻿using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_categoriacad : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            CarregarGridInicial();
            if (!IsPostBack)
            {
                CarregarThreeViewCategorias();
                CarregarDropDownCategoriaPai();
            }
        }
        catch (Exception ex)
        {
            lblMensagem.Text = "Erro: " + ex.Message;
        }
    }

    private void CarregarGridInicial()
    {
        try
        {
            var data = new dbCommerceDataContext();

            var categoriaPai = (from c in data.tbCategorias where c.flagDelete == false && c.categoriaPaiId == 0 select c).ToList();

            gvInicial.DataSource = categoriaPai;
            if (!IsPostBack && !IsCallback)
                gvInicial.DataBind();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void CarregarDropDownCategoriaPai()
    {
        try
        {
            var data = new dbCommerceDataContext();
            var categorias = (from c in data.tbCategorias where c.flagDelete == false select c).OrderBy(x => x.nomeExibicao).ToList();

            ddlCategoriaPai.DataTextField = "nomeExibicao";
            ddlCategoriaPai.DataValueField = "categoriaId";
            ddlCategoriaPai.DataSource = categorias;
            ddlCategoriaPai.DataBind();

            ddlCategoriaPai.Items.Insert(0, new ListItem() { Text = "Não atribuido", Value = "0" });
            ddlCategoriaPai.SelectedIndex = 0;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void CarregarThreeViewCategorias()
    {
        try
        {
            pnlFormulario.Visible = false;
            var data = new dbCommerceDataContext();
            var categorias = (from c in data.tbCategorias where c.flagDelete == false select c).ToList();

            foreach (var categoria in categorias)
            {
                TreeNode child = new TreeNode()
                {
                    Text = categoria.nomeExibicao,
                    Value = categoria.categoriaId.ToString()
                };

                if (categoria.categoriaPaiId == 0)
                {
                    var categoriaFilho = (from c in data.tbCategorias where c.flagDelete == false && c.categoriaPaiId == categoria.categoriaId select c).ToList();

                    foreach (var item in categoriaFilho)
                    {
                        TreeNode node = new TreeNode()
                        {
                            Text = item.nomeExibicao,
                            Value = item.categoriaId.ToString()
                        };
                        child.ChildNodes.Add(node);
                    }
                    tvCategorias.Nodes.Add(child);
                }
            }
        }
        catch (Exception ex)
        {
            lblMensagem.Text = "Erro: " + ex.Message;
        }
    }

    protected void treeView_NodeDataBound(object source, DevExpress.Web.ASPxTreeView.TreeViewNodeEventArgs e)
    {

    }

    protected void tvCategorias_SelectedNodeChanged(object sender, EventArgs e)
    {
        try
        {
            pnlFormulario.Visible = true;
            pnlGrid.Visible = true;
            hdnAcao.Value = "Editar";
            hdnCategoriaPaiId.Value = tvCategorias.SelectedValue.ToString();
            hdnCategoriaId.Value = tvCategorias.SelectedValue.ToString();
            CarregarDados(int.Parse(hdnCategoriaPaiId.Value.ToString()));
        }
        catch (Exception ex)
        {
            lblMensagem.Text = "Erro: " + ex.Message;
        }
    }

    private void CarregarDados(int pCategoriaId)
    { 
        try
        {
            var data = new dbCommerceDataContext();

            var categoria = (from c in data.tbCategorias where c.flagDelete == false && c.categoriaId == pCategoriaId select c).FirstOrDefault();

            txtNomeExibicao.Text = categoria.nomeExibicao;
            txtDescricao.Text = categoria.descricao;
            txtPosicao.Text = categoria.posicao.ToString();
            ddlCategoriaPai.SelectedValue = categoria.categoriaPaiId.ToString();
            chkExibir.Checked = categoria.exibir;

            var categoriasFilhos = (from c in data.tbCategorias where c.flagDelete == false &&  c.categoriaPaiId == categoria.categoriaId select c).ToList();

            gvInicial.DataSource = categoriasFilhos;
            gvInicial.DataBind();

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void gvCategoria_DataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            var data = new dbCommerceDataContext();

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var lblCategoriaPai = (Label)e.Row.FindControl("lblCategoriaPai");
                var categoriaPai = (from c in data.tbCategorias where c.flagDelete == false && c.categoriaId == int.Parse(lblCategoriaPai.Text) select c).FirstOrDefault();
                lblCategoriaPai.Text = categoriaPai.nomeExibicao;
            }
        }
        catch (Exception ex)
        {
            lblMensagem.Text = "Erro: " + ex.Message;
        }
    }

    protected void btnNovo_Click(object sender, EventArgs e)
    {
        try
        {
            hdnAcao.Value = "Novo";
            hdnCategoriaId.Value = string.Empty;
            pnlFormulario.Visible = true;
            txtNomeExibicao.Text = string.Empty;
            ddlCategoriaPai.SelectedIndex = 0;
            txtPosicao.Text = string.Empty;
            chkExibir.Checked = false;
            pnlGrid.Visible = false;
        }
        catch (Exception ex)
        {
            lblMensagem.Text = ex.Message;
        }
    }

    protected void btnSalvar_Click(object sender, EventArgs e)
    {
        try
        {
            var data = new dbCommerceDataContext();

            if (string.IsNullOrEmpty(txtNomeExibicao.Text.Trim()))
            {
                lblMensagem.Text = "Favor informar o nome de exibição";
                txtNomeExibicao.Focus();
                return;
            }

            if (string.IsNullOrEmpty(txtDescricao.Text.Trim()))
            {
                lblMensagem.Text = "Favor unformar a descrição";
                txtDescricao.Focus();
                return;
            }

            if (hdnAcao.Value.ToString() == "Novo")
            {
                if (!string.IsNullOrEmpty(hdnCategoriaId.Value.ToString()))
                {
                    var categoria = (from c in data.tbCategorias where c.flagDelete == false && c.categoriaId == int.Parse(hdnCategoriaId.Value.ToString()) select c).FirstOrDefault();

                    categoria.nomeExibicao = txtNomeExibicao.Text.Trim();
                    categoria.descricao = txtDescricao.Text.Trim();
                    categoria.exibir = chkExibir.Checked;
                    categoria.posicao = int.Parse(txtPosicao.Text.Trim());
                    categoria.dataAlteracao = System.DateTime.Now;
                    categoria.flagDelete = false;

                    data.SubmitChanges();
                }
                else
                {
                    var novo = new tbCategoria();

                    if (string.IsNullOrEmpty(hdnCategoriaPaiId.Value))
                        novo.categoriaPaiId = 0;
                    else
                        novo.categoriaPaiId = int.Parse(hdnCategoriaPaiId.Value.ToString());

                    novo.dataCadastro = System.DateTime.Now;
                    novo.descricao = txtDescricao.Text;
                    novo.exibir = chkExibir.Checked;
                    novo.posicao = int.Parse(txtPosicao.Text);
                    novo.nomeExibicao = txtNomeExibicao.Text;
                    novo.flagDelete = false;
                    novo.dataAlteracao = null;

                    data.tbCategorias.InsertOnSubmit(novo);
                    data.SubmitChanges();
                }
            }
            else if (hdnAcao.Value.ToString() == "Editar")
            {
                if (!string.IsNullOrEmpty(hdnCategoriaId.Value.ToString()))
                {
                    var categoria = (from c in data.tbCategorias where c.flagDelete == false && c.categoriaId == int.Parse(hdnCategoriaId.Value.ToString()) select c).FirstOrDefault();

                    categoria.nomeExibicao = txtNomeExibicao.Text.Trim();
                    categoria.descricao = txtDescricao.Text.Trim();
                    categoria.exibir = chkExibir.Checked;
                    categoria.posicao = int.Parse(txtPosicao.Text.Trim());
                    categoria.dataAlteracao = System.DateTime.Now;
                    categoria.flagDelete = false;
                    
                    data.SubmitChanges();
                }
                else
                {
                    var novo = new tbCategoria();

                    novo.dataCadastro = System.DateTime.Now;
                    novo.descricao = txtDescricao.Text;
                    novo.exibir = chkExibir.Checked;
                    novo.posicao = int.Parse(txtPosicao.Text);
                    novo.nomeExibicao = txtNomeExibicao.Text;
                    novo.flagDelete = false;
                    novo.dataAlteracao = null;

                    if (string.IsNullOrEmpty(hdnCategoriaPaiId.Value))
                        novo.categoriaPaiId = 0;
                    else
                        novo.categoriaPaiId = int.Parse(hdnCategoriaPaiId.Value.ToString());

                    data.tbCategorias.InsertOnSubmit(novo);
                    data.SubmitChanges();
                }

                
            }




            Response.Redirect("categoriacad.aspx", true);
        }
        catch (Exception ex)
        {
            lblMensagem.Text = "Erro ao gravar: " + ex.Message;
        }
    }

    protected void btnAdicionar_Click(object sender, EventArgs e)
    {
        try
        {
            if (string.IsNullOrEmpty(txtNomeExibicao.Text.Trim()))
            {
                lblMensagem.Text = "Favor preencher o nome de exibição";
                txtNomeExibicao.Focus();
                return;
            }

        }
        catch (Exception ex)
        {
            lblMensagem.Text = ex.Message;
        }
    }

    protected void gvInicial_HtmlRowCreated(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs e)
    {
        try
        {
            var data = new dbCommerceDataContext();

            if (e.RowType == DevExpress.Web.ASPxGridView.GridViewRowType.Data)
            {
                bool exibir = (Boolean)e.GetValue("exibir");

                ASPxLabel lblExibir = (ASPxLabel)gvInicial.FindRowCellTemplateControl(e.VisibleIndex, gvInicial.Columns["exibir"] as GridViewDataColumn, "lblExibir");
                if (exibir)
                    lblExibir.Text = "Sim";
                else
                    lblExibir.Text = "Não";

            }
        }
        catch (Exception ex)
        {
            lblMensagem.Text = "Erro: " + ex.Message;
        }
    }

    protected void btnAlterar_Command(object sender, CommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Editar" && !string.IsNullOrEmpty(e.CommandArgument.ToString()))
            {
                hdnAcao.Value = "Editar";
                pnlFormulario.Visible = true;
                hdnCategoriaId.Value = e.CommandArgument.ToString();
                CarregarDados(int.Parse(e.CommandArgument.ToString()));
            }
        }
        catch (Exception ex)
        {
            lblMensagem.Text = "Erro: " + ex.Message;
        }
    }

    protected void btnApagar_Command(object sender, CommandEventArgs e)
    {
        try
        {
            var data = new dbCommerceDataContext();

            if (e.CommandName == "Apagar" && !string.IsNullOrEmpty(e.CommandArgument.ToString()))
            {
                var categoria = (from c in data.tbCategorias where c.flagDelete == false && c.categoriaId == int.Parse(e.CommandArgument.ToString()) select c).FirstOrDefault();
                categoria.flagDelete = true;

                data.SubmitChanges();

            }

            Response.Redirect("categoriacad.aspx", true);
        }
        catch (Exception ex)
        {
            lblMensagem.Text = "Erro: " + ex.Message;
        }
    }
}