﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="seloPromocao.aspx.cs" Theme="Glass" Inherits="admin_seloPromocao" %>

<%@ Register TagPrefix="dxwgv" Namespace="DevExpress.Web.ASPxGridView" Assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" runat="Server">
    <asp:ScriptManager runat="server"></asp:ScriptManager>

    <table cellpadding="0" cellspacing="0" style="width: 920px">
        <tr>
            <td class="tituloPaginas" valign="top">
                <asp:Label ID="lblAcao" runat="server">Produtos com Selo Promoção</asp:Label>
            </td>
        </tr>
        <tr>
            <td>

                <div style="margin: 0 auto; width: 506px; margin-bottom: 35px;">
                    Informar os Id's dos Produtos (ex.: 12345,67890,74185)<br />
                    <asp:TextBox ID="txtIdsProdutosDesmarcarPromocao" runat="server" TextMode="MultiLine" Height="65" Width="500" />
                    <asp:Button runat="server" ID="btnDesmarcarPromocao" OnClientClick="return confirm('Os Id\'s dos produtos indicados serão desmarcados como promoção, confirma?')"
                        OnClick="btnDesmarcarPromocao_OnClick" Text="Desmarcar Promoção" Style="float: right;" />
                </div>

                <div style="margin: 0 auto; width: 175px; margin-bottom: 35px;">
                    <asp:Button runat="server" ID="btnDesmarcarTodosPromocao" OnClientClick="return confirm('Todos os produtos que estiverem marcados como produtoPromocao=True serão colocados como produtoPromocao=False, confirma?')"
                        OnClick="btnDesmarcarTodosPromocao_OnClick" Text="Desmarcar Todos Promoção" Style="float: right;" />
                </div>
                <asp:UpdatePanel runat="server">
                    <ContentTemplate>
                        <asp:Panel ID="pnlGrid" runat="server" ScrollBars="Horizontal" Width="834px" Style="margin: 0 auto;" Visible="False">
                            <dxwgv:ASPxGridView ID="grd" runat="server" AutoGenerateColumns="False" Visible="False"
                                DataSourceID="sqlDS" KeyFieldName="produtoId" Cursor="auto" OnCustomCallback="grd_CustomCallback"
                                ClientInstanceName="grd" EnableCallBacks="False">
                                <SettingsBehavior AutoFilterRowInputDelay="10200" ConfirmDelete="True"
                                    AutoExpandAllGroups="True" ColumnResizeMode="Control" AllowGroup="False"
                                    AllowFocusedRow="True" />
                                <SettingsText ConfirmDelete="Tem certeza que deseja excluir?"
                                    EmptyDataRow="Nenhum registro encontrado."
                                    GroupPanel="Arraste uma coluna aqui para agrupar." />
                                <SettingsPager Position="TopAndBottom" PageSize="30"
                                    ShowDisabledButtons="False" AlwaysShowPager="True">
                                    <Summary AllPagesText="Páginas: {0} - {1} ({2} items)"
                                        Text="Página {0} de {1} ({2} registros encontrados)" />
                                </SettingsPager>
                                <Settings ShowFilterRow="True" ShowGroupedColumns="True" ShowFooter="True" />
                                <TotalSummary>
                                    <dxwgv:ASPxSummaryItem DisplayFormat="0" FieldName="margem"
                                        ShowInColumn="Margem" ShowInGroupFooterColumn="Margem"
                                        SummaryType="Average" />
                                </TotalSummary>
                                <SettingsEditing EditFormColumnCount="4"
                                    PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1"
                                    PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter"
                                    PopupEditFormWidth="700px" Mode="Inline" />
                                <Columns>
                                    <dxwgv:GridViewDataTextColumn Caption="ProdutoId"
                                        FieldName="produtoId" Name="produtoId" VisibleIndex="3"
                                        Width="100px">
                                        <Settings AutoFilterCondition="Contains" />
                                        <DataItemTemplate>
                                            <asp:TextBox ID="txtProdutoId" runat="server" BorderStyle="None"
                                                CssClass="campos" Text='<%# Bind("produtoId") %>' Width="100%"></asp:TextBox>

                                        </DataItemTemplate>
                                    </dxwgv:GridViewDataTextColumn>

                                    <dxwgv:GridViewDataTextColumn Caption="Produto"
                                        FieldName="produtoNome" Name="produtoNome" VisibleIndex="3"
                                        Width="75">
                                        <DataItemTemplate>
                                            <%#Eval("produtoNome") %>
                                        </DataItemTemplate>
                                    </dxwgv:GridViewDataTextColumn>

                                    <dxwgv:GridViewDataTextColumn Caption="produtoPromocao" FieldName="produtoPromocao" CellStyle-HorizontalAlign="Center"
                                        VisibleIndex="6" Width="90px">
                                        <DataItemTemplate>
                                            <asp:Label ID="lblVisualizacoes" runat="server" Text='<%# Eval("produtoPromocao") %>'></asp:Label>
                                        </DataItemTemplate>
                                    </dxwgv:GridViewDataTextColumn>

                                    <dxwgv:GridViewDataTextColumn Caption="Ativo" FieldName="produtoAtivo" CellStyle-HorizontalAlign="Center" Visible="False"
                                        UnboundType="String" VisibleIndex="2" Width="40px">
                                        <DataItemTemplate>
                                            <dxe:ASPxCheckBox ID="ckbAtivo" runat="server"
                                                Value='<%#Convert.ToBoolean(Eval("produtoAtivo")) %>' ValueChecked="True"
                                                ValueType="System.Boolean" ValueUnchecked="False">
                                            </dxe:ASPxCheckBox>
                                        </DataItemTemplate>
                                    </dxwgv:GridViewDataTextColumn>

                                    <%-- <dxwgv:GridViewDataHyperLinkColumn Caption="Alterar" VisibleIndex="1"
                                        Width="45px">
                                        <DataItemTemplate>
                                            <asp:HyperLink ID="HyperLink1" runat="server" ImageUrl="images/btEditar.jpg" ToolTip="Alterar"
                                                NavigateUrl='<%# "bannerAlt.aspx?bannerId=" + Eval("id") %>'>Alterar</asp:HyperLink>
                                            <asp:HiddenField ID="hfIdBanner" runat="server" Value='<%# Eval("id") %>' />
                                        </DataItemTemplate>
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dxwgv:GridViewDataHyperLinkColumn>--%>
                                </Columns>
                            </dxwgv:ASPxGridView>


                            <div style="float: right; margin-top: 20px;">
                                <img alt="Salvar" src="images/btSalvar.jpg"
                                    title="Irá marcar produtoPromocao=False para todos os produtos da lista"
                                    onclick="if(confirm('Irá marcar produtoPromocao=False para todos os produtos da lista, confirma?')){grd.PerformCallback(this.value);}" style="cursor: pointer;" /></td>
                            </div>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>

                <asp:SqlDataSource ConnectionString="<%$ ConnectionStrings:connectionString %>" runat="server" ID="sqlDS"
                    SelectCommand="SELECT produtoId, produtoNome, produtoPromocao, produtoAtivo FROM tbProdutos (NOLOCK) WHERE produtoPromocao = 'True' AND produtoAtivo = 'True'"></asp:SqlDataSource>

            </td>
        </tr>
        <tr>
            <td>&nbsp;
            </td>
        </tr>
        <tr>
            <td class="tituloPaginas" valign="top">
                <asp:Label ID="Label1" runat="server">Marcar Produtos com Selo Promoção</asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <div style="margin: 0 auto; width: 506px; margin-bottom: 35px;">
                    Informar os Id's dos Produtos (ex.: 12345,67890,74185)<br />
                    <asp:TextBox ID="txtIdsProdutos" runat="server" TextMode="MultiLine" Height="65" Width="500" />
                    <asp:Button runat="server" ID="btnListaProdutosMarcarComoPromocao" OnClick="btnListaProdutosMarcarComoPromocao_OnClick" Text="Listar" Style="float: right;" />
                </div>
            </td>
        </tr>
        <tr>
            <td>

                <asp:UpdatePanel runat="server">
                    <ContentTemplate>
                        <asp:Panel ID="Panel1" runat="server" ScrollBars="Auto" Width="834px" Height="400px" Style="margin: 0 auto;">
                            <dxwgv:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False"
                                KeyFieldName="produtoId" Cursor="auto" OnCustomCallback="ASPxGridView1_OnCustomCallback"
                                ClientInstanceName="ASPxGridView1" EnableCallBacks="False">
                                <SettingsBehavior AutoFilterRowInputDelay="10200" ConfirmDelete="True"
                                    AutoExpandAllGroups="True" ColumnResizeMode="Control" AllowGroup="False"
                                    AllowFocusedRow="True" />
                                <SettingsText ConfirmDelete="Tem certeza que deseja excluir?"
                                    EmptyDataRow="Nenhum registro encontrado."
                                    GroupPanel="Arraste uma coluna aqui para agrupar." />
                                <SettingsPager Position="TopAndBottom" PageSize="1000"
                                    ShowDisabledButtons="False" AlwaysShowPager="True">
                                    <Summary AllPagesText="Páginas: {0} - {1} ({2} items)"
                                        Text="Página {0} de {1} ({2} registros encontrados)" />
                                </SettingsPager>
                                <Settings ShowFilterRow="True" ShowGroupedColumns="True" ShowFooter="True" />
                                <TotalSummary>
                                    <dxwgv:ASPxSummaryItem DisplayFormat="0" FieldName="margem"
                                        ShowInColumn="Margem" ShowInGroupFooterColumn="Margem"
                                        SummaryType="Average" />
                                </TotalSummary>
                                <SettingsEditing EditFormColumnCount="4"
                                    PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1"
                                    PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter"
                                    PopupEditFormWidth="700px" Mode="Inline" />
                                <Columns>
                                    <dxwgv:GridViewDataTextColumn Caption="ProdutoId"
                                        FieldName="produtoId" Name="produtoId" VisibleIndex="3"
                                        Width="100px">
                                        <Settings AutoFilterCondition="Contains" />
                                        <DataItemTemplate>
                                            <asp:TextBox ID="txtProdutoId" runat="server" BorderStyle="None"
                                                CssClass="campos" Text='<%# Bind("produtoId") %>' Width="100%"></asp:TextBox>

                                        </DataItemTemplate>
                                    </dxwgv:GridViewDataTextColumn>

                                    <dxwgv:GridViewDataTextColumn Caption="Produto"
                                        FieldName="produtoNome" Name="produtoNome" VisibleIndex="3"
                                        Width="75">
                                        <DataItemTemplate>
                                            <%#Eval("produtoNome") %>
                                        </DataItemTemplate>
                                    </dxwgv:GridViewDataTextColumn>

                                    <dxwgv:GridViewDataTextColumn Caption="produtoPromocao" FieldName="produtoPromocao" CellStyle-HorizontalAlign="Center"
                                        VisibleIndex="6" Width="90px">
                                        <DataItemTemplate>
                                            <asp:Label ID="lblVisualizacoes" runat="server" Text='<%# Eval("produtoPromocao") %>'></asp:Label>
                                        </DataItemTemplate>
                                    </dxwgv:GridViewDataTextColumn>

                                    <dxwgv:GridViewDataTextColumn Caption="Ativo" FieldName="produtoAtivo" CellStyle-HorizontalAlign="Center"
                                        UnboundType="String" VisibleIndex="2" Width="40px">
                                        <DataItemTemplate>
                                            <dxe:ASPxCheckBox ID="ckbAtivo" runat="server"
                                                Value='<%#Convert.ToBoolean(Eval("produtoAtivo")) %>' ValueChecked="True"
                                                ValueType="System.Boolean" ValueUnchecked="False">
                                            </dxe:ASPxCheckBox>
                                        </DataItemTemplate>
                                    </dxwgv:GridViewDataTextColumn>
                                </Columns>
                            </dxwgv:ASPxGridView>


                            <div style="float: right; margin-top: 20px;">
                                <img alt="Salvar" src="images/btSalvar.jpg"
                                    title="Irá marcar produtoPromocao=True para todos os produtos da lista"
                                    onclick="if(confirm('Irá marcar produtoPromocao=True para todos os produtos da lista, confirma?')){ASPxGridView1.PerformCallback(this.value);}" style="cursor: pointer;" /></td>
                            </div>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>

            </td>
        </tr>
    </table>

</asp:Content>

