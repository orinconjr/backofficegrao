﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="pagamentosEstornados.aspx.cs" Inherits="admin_pagamentosEstornados" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" runat="Server">
    <script src="js/jquery.min.js"></script>
    <style>
        .fieldsetatualizarprecos {
            width: 760px;
            float: left;
            clear: left;
            border-radius: 5px;
            margin-left: 25px;
        }

            .fieldsetatualizarprecos input[type=checkbox] {
                margin-top: 15px;
            }

            .fieldsetatualizarprecos label {
                margin-left: 5px;
                margin-right: 20px;
            }

        .meugrid {
            background-color: #F4FAFB;
            border: 0px none;
            /* border-collapse: separate !important; */
            overflow: hidden;
            font: 9pt Tahoma;
            color: #000;
            cursor: pointer;
            white-space: nowrap;
            font-weight: normal;
            text-align: left;
            width: 834px;
            border-collapse: collapse;
            float: left;
            clear: left;
            margin-top: 10px;
            margin-bottom: 10px;
            min-height: 500px;
            margin-left: 25px;
        }

            .meugrid th {
                background-image: url(../../App_Themes/Glass/GridView/gvHeaderBackground.gif);
                background-position: center top;
                background-repeat: repeat-x;
                background-color: #C0DDE0;
                overflow: hidden;
                height: 23px !important;
                line-height: 23px !important;
                padding: 4px 6px 5px;
                font-weight: normal;
                border: 1px solid #7EACB1;
                /* border-collapse: separate;*/
            }

            .meugrid td {
                overflow: hidden;
                height: 23px !important;
                line-height: 23px !important;
                padding: 4px 6px 5px;
                border: 1px solid #7EACB1;
            }

            .meugrid tr td:nth-child(4) {
                text-align: center;
            }

            .meugrid tr td:last-of-type {
                text-align: center;
            }

        .tabrotulos {
            width: 848px;
            margin: auto;
        }

        .materiaprima {
            width: 250px;
        }

            .materiaprima ul {
                margin: 0px;
                padding-left: 10px;
            }

                .materiaprima ul li {
                    max-width: 230px;
                    white-space: pre-wrap;
                }

        .meugrid tr td:last-of-type {
            text-align: center;
        }

        .nome {
            width: 237px;
        }

            .nome div {
                max-width: 230px;
                white-space: pre-wrap;
            }

        .rotulos td {
            font: bold 12px tahoma;
            padding-top: 20px;
            color: rgb(65, 105, 126);
            padding-left: 5px;
            padding-bottom: 5px;
        }
    </style>
    <div class="tituloPaginas" valign="top">
        Lista de Estornos
    </div>
    <div>
        <fieldset class="fieldsetatualizarprecos">
            <legend style="font-weight: bold;">Filtro</legend>

            <table class="tabrotulos">
                <tr class="rotulos">
                    <td>&nbsp;<br />
                        <asp:CheckBox runat="server" ID="ckbPendentes" Text="Pendentes" />

                    </td>
                    <td>&nbsp;<br />
                        <asp:CheckBox runat="server" ID="ckbAutorizados" Text="Autorizados" />
                    </td>

                    <td>&nbsp;<br />
                        <asp:CheckBox runat="server" ID="ckbNegados" Text="Negados" />
                    </td>


                    <td>Num. Pedido:<br />
                        <asp:TextBox runat="server" ID="txtNumPedido"></asp:TextBox>
                    </td>

                </tr>
                <tr class="rotulos">
                    <td>Motivo:<br />
                        <asp:DropDownList runat="server" ID="ddlMotivoCancelarPedido" CssClass="campos">
                            <Items>
                                <asp:ListItem Text="Selecione" Value="0"></asp:ListItem>
                                <asp:ListItem Text="Atraso Transportadora" Value="1"></asp:ListItem>
                                <asp:ListItem Text="Duplicidade" Value="2"></asp:ListItem>
                                <asp:ListItem Text="Mudou de Idéia /Ganhou o que precisava" Value="3"></asp:ListItem>
                                <asp:ListItem Text="Não gostou da qualidade" Value="4"></asp:ListItem>
                                <asp:ListItem Text="Óbito Bebê" Value="5"></asp:ListItem>
                                <asp:ListItem Text="Prazo de Entrega - não se atentou" Value="6"></asp:ListItem>
                                <asp:ListItem Text="Produto não serviu (tamanho)" Value="7"></asp:ListItem>
                                <asp:ListItem Text="Recebeu avariado" Value="8"></asp:ListItem>
                                <asp:ListItem Text="Alteração de Forma de Pagamento" Value="9"></asp:ListItem>
                            </Items>
                        </asp:DropDownList>
                    </td>
                    <td>Data Inicial:<br />
                        <asp:TextBox runat="server" ID="txtDataInicial"></asp:TextBox>
                    </td>
                    <td>Data Final:<br />
                        <asp:TextBox runat="server" ID="txtDataFinal"></asp:TextBox>
                    </td>
                    <td>&nbsp;<br />
                        <asp:Button runat="server" ID="btnFiltrar" Text="Filtrar" OnClick="btnFiltrar_Click" />
                        &nbsp;&nbsp;
                        <asp:Button runat="server" ID="btnExportarPlanilha" Text="Exportar Planilha"   OnClick="btnExportarPlanilha_Click"/>
                    </td>

                </tr>

            </table>
        </fieldset>
    </div>
    <div align="center" style="min-height: 500px;">
        

            <asp:GridView ID="GridView1" CssClass="meugrid" runat="server" DataKeyNames="idPedidoPagamento" Width="834px" AutoGenerateColumns="False" AllowPaging="True" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowDataBound="GridView1_RowDataBound" PageSize="10">
                <Columns>
                    
                    <asp:BoundField DataField="pedidoId" HeaderText="Id do Pedido" />
                    <asp:BoundField DataField="idPedidoPagamento" HeaderText="Id do Pagamento" />
                    <asp:BoundField DataField="valor"  DataFormatString=" {0:C}" HeaderText="valor" />
 

                    <asp:CheckBoxField DataField="estornoAprovado" HeaderText="Aprovado" />
                    <asp:CheckBoxField DataField="estornoNegado" HeaderText="Negado" />
                    <asp:BoundField DataField="dataPedidoEstorno" HeaderText="Data" />
                    <asp:BoundField DataField="motivoEstorno" HeaderText="Motivo" />
                    <asp:BoundField DataField="solicitanteEstorno" HeaderText="Solicitado por" />
                    <asp:BoundField DataField="aprovadorEstorno" HeaderText="Aprovado/Negado por" />
                   <%-- <asp:BoundField DataField="statusAntesEstorno" HeaderText="status anterior" />--%>
                    <asp:TemplateField HeaderText="Editar" ShowHeader="False">
                        <ItemTemplate>
                            <a id="linkeditar" runat="server">
                                <img src="images/btEditar.jpg" alt="Editar" style="border-style: none;" />
                            </a>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
       
    </div>
</asp:Content>

