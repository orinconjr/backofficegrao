﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_comprasOrdemDetalhe : System.Web.UI.Page
{

    private int id;

    protected void Page_Load(object sender, EventArgs e)
    {

        id = Convert.ToInt32(Request.QueryString["id"]);

        if (!Page.IsPostBack)
        {
            HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
            usuarioLogadoId = Request.Cookies["usuarioLogadoId"];
            if (usuarioLogadoId != null)
            {
                string paginaSolicitada = Request.Path.Substring(Request.Path.LastIndexOf("/") + 1);
                if (
                 rnUsuarios.usuarioPaginasPermitidaSeleciona_PorUsuarioIdPaginaPermitidaNome(
                  int.Parse(usuarioLogadoId.Value.ToString()), paginaSolicitada).Tables[0].Rows.Count == 0)
                {
                    Response.Write("<script>alert('Você não tem acesso a essa área.');</script>");
                    Response.Write("<script>history.back();</script>");
                }
            }
            else
            {
                Response.Write("<script>alert('Você não tem acesso a essa área.');</script>");
                Response.Write("<script>window.location=('default.aspx');</script>");
            }

            if (Request.QueryString["id"] != null)
            {
                FillForm();
            }
        }
    }
    private void FillForm()
    {

        id = Convert.ToInt32(Request.QueryString["id"]);
        var data = new dbCommerceDataContext();

        var ordem = (from c in data.tbComprasOrdems where c.idComprasOrdem == id select c).FirstOrDefault();

        if (ordem != null)
        {
            litNumero.Text = ordem.idComprasOrdem.ToString();
            litUsuario.Text =
             (from c in data.tbUsuarios where c.usuarioId == ordem.idUsuarioCadastroOrdem select c).First()
              .usuarioNome;
            litData.Text = ordem.dataCriacao.ToShortDateString();
            litFornecedor.Text = ordem.tbComprasFornecedor.fornecedor;
            litEmpresa.Text = ordem.tbComprasEmpresa.empresa;

            var itens = (from c in data.tbComprasOrdemProdutos
                         where c.idComprasOrdem == id && c.status == 1
                         orderby c.tbComprasProduto.produto
                         select new
                         {
                             c.tbComprasProduto.produto,
                             quantidade = c.quantidadeRecebida ?? c.quantidade,
                             preco = c.precoRecebido ?? c.preco
                         });

            lstProdutos.DataSource = itens;
            lstProdutos.DataBind();

            litTotal.Text = itens.Any() ? itens.Sum(x => x.quantidade * x.preco).ToString("C") : 0.ToString("C");

            usrComprasOrdemCondicoes.FillCondicoes(true, true);

        }
    }
}