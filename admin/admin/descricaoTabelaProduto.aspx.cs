﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Serialization;

public partial class admin_descricaoTabelaProduto : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        fillGrid(false);
    }

    private void fillGrid(bool rebind)
    {
        var data = new dbCommerceDataContext();
        var produtos = (from c in data.tbDescricaoTabelaProdutos select c);
        grd.DataSource = produtos;
        if((!Page.IsPostBack && !Page.IsCallback) | rebind) grd.DataBind();
    }
    protected void btnAdicionar_OnClick(object sender, EventArgs e)
    {
        limpaCampos();
        fillGridDescricoes();
        hdfProdutoAlterar.Value = "";
        pnNovo.Visible = true;
        pnItens.Visible = false;
    }

    protected void btnAdicionarDescricao_OnClick(object sender, EventArgs e)
    {
        int quantidade = 1;
        int.TryParse(txtQuantidade.Text, out quantidade);
        if (quantidade == 0) quantidade = 1;

        if (string.IsNullOrEmpty(txtDescricao.Text))
        {
            AlertShow("Favor preencher a descrição");
            return;
        }

        if (string.IsNullOrEmpty(hdfProdutoAlterar.Value))
        {
            var lista = new List<tbDescricaoTabelaProdutoItem>();
            if (!string.IsNullOrEmpty(txtListaProdutos.Text))
            {
                lista = Deserialize(txtListaProdutos.Text);
            }

            var item = new tbDescricaoTabelaProdutoItem
            {
                descricao = txtDescricao.Text,
                quantidade = quantidade,
                idDescricaoTabelaProdutoItem = lista.Any() ? (lista.OrderByDescending(x => x.idDescricaoTabelaProdutoItem).FirstOrDefault().idDescricaoTabelaProdutoItem + 1) : 1
            };
            lista.Add(item);
            txtListaProdutos.Text = Serialize(lista);
            txtQuantidade.Text = "";
            txtDescricao.Text = "";
            txtQuantidade.Focus();
        }
        else
        {
            int id = Convert.ToInt32(hdfProdutoAlterar.Value);
            var data = new dbCommerceDataContext();
            var item = new tbDescricaoTabelaProdutoItem
            {
                descricao = txtDescricao.Text,
                quantidade = quantidade,
                idDescricaoTabelaProduto =  id
            };
            data.tbDescricaoTabelaProdutoItems.InsertOnSubmit(item);
            data.SubmitChanges();
        }
        fillGridDescricoes();
    }

    private void fillGridDescricoes()
    {
        if (string.IsNullOrEmpty(hdfProdutoAlterar.Value))
        {
            var lista = new List<tbDescricaoTabelaProdutoItem>();
            if (!string.IsNullOrEmpty(txtListaProdutos.Text))
            {
                lista = Deserialize(txtListaProdutos.Text);
            }

            lstItensAdicionar.DataSource = lista;
            lstItensAdicionar.DataBind();
        }
        else
        {
            var data = new dbCommerceDataContext();
            int id = Convert.ToInt32(hdfProdutoAlterar.Value);
            var itens = (from c in data.tbDescricaoTabelaProdutoItems where c.idDescricaoTabelaProduto == id select c);
            lstItensAdicionar.DataSource = itens;
            lstItensAdicionar.DataBind();
        }
    }
    public void AlertShow(string message)
    {
        base.ClientScript.RegisterStartupScript(base.GetType(), "myalert", "alert('" + message + "'); ", true);
    }
    public static string Serialize(List<tbDescricaoTabelaProdutoItem> tData)
    {
        var serializer = new XmlSerializer(typeof(List<tbDescricaoTabelaProdutoItem>));

        TextWriter writer = new StringWriter();
        serializer.Serialize(writer, tData);

        return writer.ToString();
    }

    public static List<tbDescricaoTabelaProdutoItem> Deserialize(string tData)
    {
        var serializer = new XmlSerializer(typeof(List<tbDescricaoTabelaProdutoItem>));

        TextReader reader = new StringReader(tData);

        return (List<tbDescricaoTabelaProdutoItem>)serializer.Deserialize(reader);
    }

    protected void btnRemoverProduto_OnCommand(object sender, CommandEventArgs e)
    {
        if (string.IsNullOrEmpty(hdfProdutoAlterar.Value))
        {
            int id = Convert.ToInt32(e.CommandArgument);
            var lista = new List<tbDescricaoTabelaProdutoItem>();
            if (!string.IsNullOrEmpty(txtListaProdutos.Text))
            {
                lista = Deserialize(txtListaProdutos.Text);
            }
            lista.RemoveAll(x => x.idDescricaoTabelaProduto == id);
            txtListaProdutos.Text = Serialize(lista);
        }
        else
        {
            var data = new dbCommerceDataContext();
            int id = Convert.ToInt32(e.CommandArgument);
            var item =
                (from c in data.tbDescricaoTabelaProdutoItems where c.idDescricaoTabelaProdutoItem == id select c).First
                    ();
            data.tbDescricaoTabelaProdutoItems.DeleteOnSubmit(item);
            data.SubmitChanges();
        }
        fillGridDescricoes();
    }

    protected void btnGravarProduto_OnClick(object sender, EventArgs e)
    {
        if (txtNomeInterno.Text == "")
        {
            AlertShow("Favor preencher o nome interno");
            txtNomeInterno.Focus();
            return;
        }
        if (txtNomeSite.Text == "")
        {
            AlertShow("Favor preencher o nome de exibição no site");
            txtNomeSite.Focus();
            return;
        }
        if (string.IsNullOrEmpty(hdfProdutoAlterar.Value))
        {
            var lista = new List<tbDescricaoTabelaProdutoItem>();
            if (!string.IsNullOrEmpty(txtListaProdutos.Text))
            {
                lista = Deserialize(txtListaProdutos.Text);
            }
            if (!lista.Any())
            {
                AlertShow("Favor adicionar itens à lista");
                return;
            }
            var data = new dbCommerceDataContext();
            var produto = new tbDescricaoTabelaProduto();
            produto.idSite = Convert.ToInt32(ddlSite.SelectedValue);
            produto.nomeInterno = txtNomeInterno.Text;
            produto.nomeAmigavel = txtNomeSite.Text;
            data.tbDescricaoTabelaProdutos.InsertOnSubmit(produto);
            data.SubmitChanges();

            foreach (var item in lista)
            {
                var descricaoItem = new tbDescricaoTabelaProdutoItem();
                descricaoItem.quantidade = item.quantidade;
                descricaoItem.descricao = item.descricao;
                descricaoItem.idDescricaoTabelaProduto = produto.idDescricaoTabelaProduto;
                data.tbDescricaoTabelaProdutoItems.InsertOnSubmit(descricaoItem);
            }
            data.SubmitChanges();
        }
        else
        {
            int id = Convert.ToInt32(hdfProdutoAlterar.Value);
            var data = new dbCommerceDataContext();
            var produto = (from c in data.tbDescricaoTabelaProdutos where c.idDescricaoTabelaProduto == id select c).FirstOrDefault();
            produto.idSite = Convert.ToInt32(ddlSite.SelectedValue);
            produto.nomeInterno = txtNomeInterno.Text;
            produto.nomeAmigavel = txtNomeSite.Text;
            data.SubmitChanges();
        }
        fillGrid(true);
        limpaCampos();
        AlertShow("Produto gravado com sucesso");
        pnItens.Visible = true;
        pnNovo.Visible = false;
    }

    private void limpaCampos()
    {
        hdfProdutoAlterar.Value = "";
        txtDescricao.Text = "";
        txtListaProdutos.Text = "";
        txtNomeInterno.Text = "";
        txtNomeSite.Text = "";
        txtQuantidade.Text = "";
    }

    protected void btnEditar_OnCommand(object sender, CommandEventArgs e)
    {
        limpaCampos();
        int id = Convert.ToInt32(e.CommandArgument);
        var data = new dbCommerceDataContext();
        var pedido = (from c in data.tbDescricaoTabelaProdutos where c.idDescricaoTabelaProduto == id select c).First();
        hdfProdutoAlterar.Value = id.ToString();
        txtNomeInterno.Text = pedido.nomeInterno;
        txtNomeSite.Text = pedido.nomeAmigavel;
        ddlSite.SelectedValue = pedido.idSite.ToString();
        fillGridDescricoes();
        pnItens.Visible = false;
        pnNovo.Visible = true;
    }
}