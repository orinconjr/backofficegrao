﻿using DevExpress.Web.ASPxGridView;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_carrinhoEnderecamentoDetalhe : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        HttpCookie usuarioLogadoId = Request.Cookies["usuarioLogadoId"];
        bool permissaoTransferir = rnUsuarios.usuarioPaginasPermitidaSeleciona_PorUsuarioIdPaginaPermitidaNome(int.Parse(usuarioLogadoId.Value), "transferircarrinho").Tables[0].Rows.Count == 1;
        if (!permissaoTransferir)
        {
            trTransferir.Visible = false;
        }
        fillGrid(false);
    }

    private void fillGrid(bool rebind)
    {
        int id = 0;
        int.TryParse(Request.QueryString["id"], out id);

        //grd.DataSource = sql;
        var data = new dbCommerceDataContext();
        var carrinhos = (from c in data.tbTransferenciaEnderecoProdutos
                         where c.idTransferenciaEndereco == id
                         select new
                         {
                             c.idTransferenciaEnderecoProduto,
                             c.idPedidoFornecedorItem,
                             c.tbPedidoFornecedorItem.tbProduto.produtoNome,
                             c.tbPedidoFornecedorItem.tbProduto.tbProdutoFornecedor.fornecedorNome,
                             c.dataEnderecamento,
                             c.tbPedidoFornecedorItem.idPedidoFornecedor,
                             c.tbTransferenciaEndereco.tbEnderecamentoTransferencia.idCentroDistribuicao
                         });
        if (rdbAberto.Checked) carrinhos = carrinhos.Where(x => x.dataEnderecamento == null);
        grd.DataSource = carrinhos;
        if ((!Page.IsPostBack && !Page.IsCallback) | rebind) grd.DataBind();

        if ((!Page.IsPostBack && !Page.IsCallback) | rebind)
        {
            var carrinhosAuxiliares = (from c in data.tbTransferenciaEnderecos
                             join d in data.tbTransferenciaEnderecoProdutos on c.idTransferenciaEndereco equals d.idTransferenciaEndereco into produtos
                             orderby c.idTransferenciaEndereco
                             select new
                             {
                                 c.idTransferenciaEndereco,
                                 c.tbEnderecamentoTransferencia.enderecamentoTransferencia,
                                 nomeExibicao = c.tbEnderecamentoTransferencia.enderecamentoTransferencia + " (" + produtos.Count() + ")",
                                 totalItens = produtos.Count(),
                                 c.dataFim,
                                 faltandoEnderecar = produtos.Count(x => x.dataEnderecamento == null)
                             }).Where(x => x.dataFim == null && x.faltandoEnderecar > 0 && x.idTransferenciaEndereco != id).OrderBy(x => x.nomeExibicao);
            ddlTransferirCarrinho.DataSource = carrinhosAuxiliares;
            ddlTransferirCarrinho.DataBind();
        }
    }

    protected void rdbAberto_OnCheckedChanged(object sender, EventArgs e)
    {
        fillGrid(true);
    }


    protected void btnTransferir_Click(object sender, EventArgs e)
    {
        int id = 0;
        int.TryParse(Request.QueryString["id"], out id);

        int inicio = 0;
        int fim = 0;

        if (grd.PageIndex == 0)
        {
            inicio = 0;
            fim = inicio + grd.GetCurrentPageRowValues("idTransferenciaEnderecoProduto").Count;
        }
        else
        {
            inicio += inicio + grd.SettingsPager.PageSize * grd.PageIndex;
            fim += inicio + grd.GetCurrentPageRowValues("idTransferenciaEnderecoProduto").Count;
        }

        var data = new dbCommerceDataContext();
        var itens = (from c in data.tbTransferenciaEnderecoProdutos where c.idTransferenciaEndereco == id select c).ToList();
        int idTransferenciaNova = Convert.ToInt32(ddlTransferirCarrinho.SelectedValue);
        for (int i = inicio; i < fim; i++)
        {

            TextBox txtIdTransferenciaEnderecoProduto = (TextBox)grd.FindRowCellTemplateControl(i, grd.Columns["idTransferenciaEnderecoProduto"] as GridViewDataColumn, "txtIdTransferenciaEnderecoProduto");
            CheckBox ckbSelecionar = (CheckBox)grd.FindRowCellTemplateControl(i, grd.Columns["selecionar"] as GridViewDataColumn, "ckbSelecionar");
            int idTransferenciaEnderecoProduto = Convert.ToInt32(txtIdTransferenciaEnderecoProduto.Text);

            if (ckbSelecionar.Checked)
            {
                var item = (from c in itens where c.idTransferenciaEnderecoProduto == idTransferenciaEnderecoProduto && c.dataEnderecamento == null select c).FirstOrDefault();
                if(item != null)
                {
                    item.idTransferenciaEndereco = idTransferenciaNova;
                }
            }
        }
        data.SubmitChanges();

        Response.Redirect("carrinhosEnderecamento.aspx");
    }
}