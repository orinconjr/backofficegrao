﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_relatorioVendasPedidoFornecedor : System.Web.UI.Page
{
    public class DetalhesPedidos
    {
        public string produtoNome { get; set; }
        public int produtoId { get; set; }
        public int quantidadeVendida { get; set; }
        public int quantidadePedidos { get; set; }
        public int quantidadeEstoque { get; set; }
        public int quantidadeEncomendados { get; set; }
        public int quantidadeSolicitados { get; set; }
        public int quantidadeAtenderPedidos { get; set; }
    }
         //   Response.Write(primeiroItem.tbProduto.produtoNome + " - " + totalProduto + " - " + totalPedidos + " - " + totalReservado + " - " + itensPedidos + " - " + totalSobrasRomaneio);
    protected void Page_Load(object sender, EventArgs e)
    {

       
        var listaDetalhes = new List<DetalhesPedidos>();
        //int idFornecedor = 82;
        int idPedidoFornecedor = 0;

        if (Request.QueryString["idPedidoFornecedor"] != null) idPedidoFornecedor = Convert.ToInt32(Request.QueryString["idPedidoFornecedor"]);

        if (idPedidoFornecedor == 0) return;
        DateTime dataInicio = DateTime.Now.AddDays(-1);
        DateTime dataFim = DateTime.Now;

        DateTime dataInicioPedidos = DateTime.Now.AddDays(-1);
        DateTime dataFimPedidos = DateTime.Now;


        var data = new dbCommerceDataContext();
        var romaneiosPeriodo = (from c in data.tbPedidoFornecedors where c.idPedidoFornecedor == idPedidoFornecedor orderby c.data select c).ToList();
        var primeiroRomaneio = romaneiosPeriodo.OrderBy(x => x.data).First();
        var ultimoRomaneio = romaneiosPeriodo.OrderByDescending(x => x.data).First();
        var romaneioAnterior = (from c in data.tbPedidoFornecedors
                                where c.idFornecedor == primeiroRomaneio.idFornecedor && c.data < primeiroRomaneio.data
                                orderby c.data descending
                                select c).FirstOrDefault();
        if (romaneioAnterior != null)
        {
            dataInicioPedidos = romaneioAnterior.data;
        }
        dataFimPedidos = ultimoRomaneio.data;
        

        var itensPedidoPeriodo = (from c in data.tbItemPedidoEstoques
            where
                c.tbItensPedido.tbPedido.dataConfirmacaoPagamento != null &&
                c.tbItensPedido.tbPedido.dataConfirmacaoPagamento >= dataInicioPedidos &&
                c.tbItensPedido.tbPedido.dataConfirmacaoPagamento <= dataFimPedidos && c.tbProduto.produtoFornecedor == primeiroRomaneio.idFornecedor
            select c).ToList();
        var itensPedidoPeriodoProdutos = (from c in itensPedidoPeriodo select new {c.produtoId, c.tbProduto.produtoNome}).Distinct().ToList();

        var itensRomaneioGeral = (from c in data.tbPedidoFornecedorItems
                                              where c.idPedidoFornecedor == idPedidoFornecedor
                                              orderby c.encomendasNaoEntregues
                                              select c).ToList();


        foreach (var itemPedidoPeriodo in itensPedidoPeriodoProdutos)
        {
            int totalProduto =
                (from c in itensPedidoPeriodo where c.produtoId == itemPedidoPeriodo.produtoId select c).Count();
            int totalPedidos =
                (from c in itensPedidoPeriodo
                 where c.produtoId == itemPedidoPeriodo.produtoId
                 select new { c.tbItensPedido.pedidoId }).Distinct().Count();
            int totalReservado =
                (from c in itensPedidoPeriodo
                 where c.produtoId == itemPedidoPeriodo.produtoId && c.dataReserva != null && c.dataReserva > dataInicioPedidos && c.dataReserva <= dataFimPedidos
                 select c).Count();

            var detalhesPrimeiroPedidoRomaneio = (from c in itensRomaneioGeral
                                                  where c.idProduto == itemPedidoPeriodo.produtoId && c.idPedidoFornecedor == idPedidoFornecedor
                                                  orderby c.encomendasNaoEntregues
                                                  select c).FirstOrDefault();

            var romaneiosIds = (from c in romaneiosPeriodo select c.idPedidoFornecedor).ToList();
            var itensPedidos = (from c in itensRomaneioGeral
                                where romaneiosIds.Contains(c.idPedidoFornecedor) && c.idProduto == itemPedidoPeriodo.produtoId
                                select c).Count();
            int totalNecessarioRomaneio = 0;
            int totalNecessarioRomaneioMenosPedidos = 0;
            int totalPendenteRomaneio = 0;
            int totalSobrasRomaneio = 0;
            if (detalhesPrimeiroPedidoRomaneio != null)
            {
                totalNecessarioRomaneio = detalhesPrimeiroPedidoRomaneio.pedidosPendentes ?? 0;
                totalPendenteRomaneio = (detalhesPrimeiroPedidoRomaneio.encomendasNaoEntregues ?? 0) + (detalhesPrimeiroPedidoRomaneio.produtosNaoEnderecados ?? 0);
            }
            totalNecessarioRomaneioMenosPedidos = totalNecessarioRomaneio - (totalProduto - totalReservado);
            totalSobrasRomaneio = totalPendenteRomaneio - totalNecessarioRomaneioMenosPedidos;
            if (totalSobrasRomaneio < 0) totalSobrasRomaneio = 0;

            var detalhes = new DetalhesPedidos()
            {
                produtoId = itemPedidoPeriodo.produtoId,
                produtoNome = itemPedidoPeriodo.produtoNome,
                quantidadeAtenderPedidos = totalSobrasRomaneio + totalReservado + itensPedidos,
                quantidadeEncomendados = totalSobrasRomaneio,
                quantidadeEstoque = totalReservado,
                quantidadePedidos = totalPedidos,
                quantidadeSolicitados = itensPedidos,
                quantidadeVendida = totalProduto
            };
            listaDetalhes.Add(detalhes);
        }
        listaDetalhes = listaDetalhes.OrderBy(x => x.produtoNome).ToList();

        grd.DataSource = listaDetalhes;
        if(!Page.IsPostBack && !Page.IsCallback) grd.DataBind();
    }
}