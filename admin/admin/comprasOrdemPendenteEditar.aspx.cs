﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxEditors;

public partial class admin_comprasOrdemPendenteEditar : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            FillListaPendentes();
        }
    }
    

    private void FillListaPendentes()
    {
        int id = 0;
        id = Convert.ToInt32(Request.QueryString["id"]);

        usrComprasOrdemCondicoes.IdComprasOrdem = id;
        usrComprasOrdemCondicoes.FillCondicoes();

        var data = new dbCommerceDataContext();
        var ordem = (from c in data.tbComprasOrdems where c.idComprasOrdem == id select c).FirstOrDefault();
        if(ordem == null) Response.Redirect("Default.aspx");
        if (ordem.statusOrdemCompra == 1) Response.Redirect("compraOrdensPendentes.aspx");
        if (ordem.statusOrdemCompra == 2)
        {
            btnGravarOrdemCompra.OnClientClick = "return confirm('Atenção, a ordem de compra será reenviada para aprovação. Deseja gravar alterações?')";
        }

        litFornecedor.Text = ordem.tbComprasFornecedor.fornecedor;
        litEmpresa.Text = ordem.tbComprasEmpresa.empresa;

        var produtosPendentes = (from c in data.tbComprasOrdemProdutos where c.idComprasOrdem == id
                                 select new
                                 {
                                     c.idComprasOrdemProduto,
                                     c.idComprasSolicitacaoProduto,
                                     c.idComprasProduto,
                                     c.tbComprasProduto.produto,
                                     c.tbComprasSolicitacaoProduto.idComprasSolicitacao,
                                     c.quantidade,
                                     c.preco,
                                     status = c.status == 1 ? "Aprovado" : c.status == 2 ? "Reprovado" : "Aguardando",
                                     c.comentario
                                 });
        lstProdutosSolicitacao.DataSource = produtosPendentes;
        lstProdutosSolicitacao.DataBind();

        litTotal.Text = produtosPendentes.Any()
            ? produtosPendentes.Sum(x => x.preco*x.quantidade).ToString("C")
            : 0.ToString("C");
    }
    

    protected void btnGravarOrdemCompra_OnClick(object sender, EventArgs e)
    {
        var data = new dbCommerceDataContext();
        foreach (var item in lstProdutosSolicitacao.Items)
        {
            var idComprasOrdemProduto = (HiddenField) item.FindControl("hdfIdComprasOrdemProduto");
            var txtQuantidade = (ASPxTextBox) item.FindControl("txtQuantidade");
            var txtValor = (ASPxTextBox)item.FindControl("txtValor");
            int quantidade = Convert.ToInt32(txtQuantidade.Value);
            decimal valor = Convert.ToDecimal(txtValor.Value);

            if (quantidade > 0 | valor > 0)
            {
                var compraOrdem =
                    (from c in data.tbComprasOrdemProdutos
                        where c.idComprasOrdemProduto == Convert.ToInt32(idComprasOrdemProduto.Value)
                        select c).First();
                if (quantidade > 0)
                {

                    var produtoHistorico = new tbComprasOrdemProdutoHistorico();
                    produtoHistorico.data = DateTime.Now.ToShortDateString();
                    produtoHistorico.usuario = rnUsuarios.retornaNomeUsuarioLogado();
                    produtoHistorico.idComprasOrdemProduto = compraOrdem.idComprasOrdemProduto;
                    produtoHistorico.descricao = "Quantidade alterada de " + compraOrdem.quantidade + " para " +
                                                 quantidade;
                    compraOrdem.quantidade = quantidade;

                    data.tbComprasOrdemProdutoHistoricos.InsertOnSubmit(produtoHistorico);
                    data.SubmitChanges();
                }
                if (valor > 0)
                {

                    var produtoHistorico = new tbComprasOrdemProdutoHistorico();
                    produtoHistorico.data = DateTime.Now.ToShortDateString();
                    produtoHistorico.usuario = rnUsuarios.retornaNomeUsuarioLogado();
                    produtoHistorico.idComprasOrdemProduto = compraOrdem.idComprasOrdemProduto;
                    produtoHistorico.descricao = "Valor alterado de " + compraOrdem.preco + " para " + valor;
                    compraOrdem.preco = valor;

                    var produtoFornecedorChecagem = (from c in data.tbComprasProdutoFornecedors
                                                     where
                                                         c.idComprasProduto == compraOrdem.idComprasProduto &&
                                                         c.idComprasFornecedor == compraOrdem.tbComprasOrdem.idComprasFornecedor
                                                     select c).FirstOrDefault();
                    if (produtoFornecedorChecagem != null)
                    {
                        produtoFornecedorChecagem.valor = valor;
                        produtoFornecedorChecagem.dataValor = DateTime.Now;

                        var fornecedorHistorico = new tbComprasProdutoFornecedorHistorico
                        {
                            idComprasProduto = produtoFornecedorChecagem.idComprasProduto,
                            idComprasFornecedor = produtoFornecedorChecagem.idComprasFornecedor,
                            idComprasUnidadeMedida = produtoFornecedorChecagem.idComprasUnidadeMedida,
                            valor = produtoFornecedorChecagem.valor,
                            data = DateTime.Now,
                            idUsuario = RetornaIdUsuarioLogado()
                        };
                        data.tbComprasProdutoFornecedorHistoricos.InsertOnSubmit(fornecedorHistorico);
                        data.SubmitChanges();
                    }

                    data.tbComprasOrdemProdutoHistoricos.InsertOnSubmit(produtoHistorico);
                    data.SubmitChanges();
                }

                compraOrdem.status = 0;
                data.SubmitChanges();
            }
        }

        int id = Convert.ToInt32(Request.QueryString["id"]);
        var ordemDeCompra = (from c in data.tbComprasOrdems where c.idComprasOrdem == id select c).First();
        if (ordemDeCompra.statusOrdemCompra == 2)
        {
            ordemDeCompra.statusOrdemCompra = 1;
            var pedidoHistorico = new tbComprasOrdemHistorico();
            pedidoHistorico.data = DateTime.Now;
            pedidoHistorico.usuario = rnUsuarios.retornaNomeUsuarioLogado();
            pedidoHistorico.idComprasOrdem = id;
            pedidoHistorico.descricao = "Pedido reenviado para aprovação";
            data.tbComprasOrdemHistoricos.InsertOnSubmit(pedidoHistorico);
            data.SubmitChanges();
        }

        FillListaPendentes();
        Response.Write("<script>alert('Ordem de compra alterada com sucesso.');</script>");
    }

    

    private int RetornaIdUsuarioLogado()
    {
        HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
        usuarioLogadoId = Request.Cookies["usuarioLogadoId"];
        if (usuarioLogadoId == null)
        {
            return 0;
        }

        return Convert.ToInt32(usuarioLogadoId.Value);
    }

    protected void btnRemover_OnCommand(object sender, CommandEventArgs e)
    {
        var data = new dbCommerceDataContext();
        int idComprasOrdemProduto = Convert.ToInt32(e.CommandArgument);
        var ordemProduto =
            (from c in data.tbComprasOrdemProdutos where c.idComprasOrdemProduto == idComprasOrdemProduto select c)
                .First();
        ordemProduto.status = 2;
        data.SubmitChanges();

        var produtoHistorico = new tbComprasOrdemProdutoHistorico();
        produtoHistorico.data = DateTime.Now.ToShortDateString();
        produtoHistorico.usuario = rnUsuarios.retornaNomeUsuarioLogado();
        produtoHistorico.idComprasOrdemProduto = ordemProduto.idComprasOrdemProduto;
        produtoHistorico.descricao = "Status alterado para reprovado";

        data.tbComprasOrdemProdutoHistoricos.InsertOnSubmit(produtoHistorico);
        data.SubmitChanges();

        FillListaPendentes();
    }
}