﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;

public partial class admin_seloPromocao : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void grd_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        try
        {

            int inicio = 0;
            int fim = 0;

            if (grd.PageIndex == 0)
            {
                inicio = 0;
                fim = inicio + grd.GetCurrentPageRowValues("produtoId").Count;
            }
            else
            {
                inicio += inicio + grd.SettingsPager.PageSize * grd.PageIndex;
                fim += inicio + grd.GetCurrentPageRowValues("produtoId").Count;
            }

            for (int i = inicio; i < fim; i++)
            {
                TextBox txtProtudoId = (TextBox)grd.FindRowCellTemplateControl(i, grd.Columns["produtoId"] as GridViewDataColumn, "txtProdutoId");

                int produtoId = Convert.ToInt32(txtProtudoId.Text);

                using (var data = new dbCommerceDataContext())
                {
                    var produto = (from p in data.tbProdutos where p.produtoId == produtoId select p).First();

                    produto.produtoPromocao = "False";
                    data.SubmitChanges();

                    var queue = new tbQueue
                    {
                        agendamento = DateTime.Now,
                        andamento = false,
                        concluido = false,
                        tipoQueue = 15,
                        idRelacionado = produtoId,
                        mensagem = ""
                    };

                    data.tbQueues.InsertOnSubmit(queue);
                    data.SubmitChanges();
                }
            }

            grd.DataBind();

            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('Informações atualizadas com sucesso!');", true);
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('ERRO:" + ex.Message + "');", true);
        }
    }

    protected void ASPxGridView1_OnCustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        try
        {

            int inicio = 0;
            int fim = 0;

            if (ASPxGridView1.PageIndex == 0)
            {
                inicio = 0;
                fim = inicio + ASPxGridView1.GetCurrentPageRowValues("produtoId").Count;
            }
            else
            {
                inicio += inicio + ASPxGridView1.SettingsPager.PageSize * ASPxGridView1.PageIndex;
                fim += inicio + ASPxGridView1.GetCurrentPageRowValues("produtoId").Count;
            }

            for (int i = inicio; i < fim; i++)
            {
                TextBox txtProtudoId = (TextBox)ASPxGridView1.FindRowCellTemplateControl(i, ASPxGridView1.Columns["produtoId"] as GridViewDataColumn, "txtProdutoId");

                int produtoId = Convert.ToInt32(txtProtudoId.Text);

                using (var data = new dbCommerceDataContext())
                {
                    var produto = (from p in data.tbProdutos where p.produtoId == produtoId select p).First();

                    produto.produtoPromocao = "True";
                    data.SubmitChanges();

                    var queue = new tbQueue
                    {
                        agendamento = DateTime.Now,
                        andamento = false,
                        concluido = false,
                        tipoQueue = 15,
                        idRelacionado = produtoId,
                        mensagem = ""
                    };

                    data.tbQueues.InsertOnSubmit(queue);
                    data.SubmitChanges();
                }
            }

            ASPxGridView1.DataBind();

            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('Informações atualizadas com sucesso!');", true);
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('ERRO:" + ex.Message + "');", true);
        }
    }

    private void Carrega_ASPxGridView1()
    {
        try
        {
            List<int> produtoIds = txtIdsProdutos.Text.Split(',').Select(x => Convert.ToInt32(x.Trim())).ToList();

            using (var data = new dbCommerceDataContext())
            {
                var dados = (from c in data.tbProdutos where produtoIds.Contains(c.produtoId) select c).ToList();

                ASPxGridView1.DataSource = dados;
                ASPxGridView1.DataBind();
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('ERRO:" + ex.Message + "');", true);
        }
    }

    protected void btnListaProdutosMarcarComoPromocao_OnClick(object sender, EventArgs e)
    {
        bool valido;

        if (string.IsNullOrEmpty(txtIdsProdutos.Text))
        {
            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('Verifique os Ids informados, processo cancelado!');", true);
            return;
        }

        List<int> produtoIds = new List<int>();
        try
        {
            produtoIds = txtIdsProdutos.Text.Split(',').Select(x => Convert.ToInt32(x.Trim())).ToList();
            valido = true;
        }
        catch (Exception ex)
        {
            valido = false;
        }

        if (valido)
        {
            using (var data = new dbCommerceDataContext())
            {
                var produtos = (from p in data.tbProdutos where produtoIds.Contains(p.produtoId) select p).ToList();
                ASPxGridView1.DataSource = produtos;
                ASPxGridView1.DataBind();
            }
        }

        else
            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('Verifique os Ids informados, processo cancelado!');", true);
    }

    protected void btnDesmarcarPromocao_OnClick(object sender, EventArgs e)
    {
        bool valido;
        List<int> produtoIds = new List<int>();
        try
        {
            produtoIds = txtIdsProdutosDesmarcarPromocao.Text.Split(',').Select(x => Convert.ToInt32(x.Trim())).ToList();
            valido = true;
        }
        catch (Exception ex)
        {
            valido = false;
        }

        if (valido)
        {
            using (var data = new dbCommerceDataContext())
            {
                var dados = (from c in data.tbProdutos where produtoIds.Contains(c.produtoId) select c).ToList();

                foreach (var p in dados)
                {
                    p.produtoPromocao = "False";
                    data.SubmitChanges();

                    var queue = new tbQueue
                    {
                        agendamento = DateTime.Now,
                        andamento = false,
                        concluido = false,
                        tipoQueue = 15,
                        idRelacionado = p.produtoId,
                        mensagem = ""
                    };

                    data.tbQueues.InsertOnSubmit(queue);
                    data.SubmitChanges();
                }
            }

            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('Informações atualizadas com sucesso!');", true);

        }//Carrega_ASPxGridView1();
        else
            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('Verifique os Ids informados, processo cancelado!');", true);
    }

    protected void btnDesmarcarTodosPromocao_OnClick(object sender, EventArgs e)
    {
        try
        {
            using (var data = new dbCommerceDataContext())
            {
                var dados = (from c in data.tbProdutos where c.produtoPromocao == "True" select c).ToList();

                foreach (var p in dados)
                {
                    p.produtoPromocao = "False";
                    data.SubmitChanges();

                    var queue = new tbQueue
                    {
                        agendamento = DateTime.Now,
                        andamento = false,
                        concluido = false,
                        tipoQueue = 15,
                        idRelacionado = p.produtoId,
                        mensagem = ""
                    };

                    data.tbQueues.InsertOnSubmit(queue);
                    data.SubmitChanges();
                }
            }

            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('Informações atualizadas com sucesso!');", true);
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('ERRO:" + ex.Message + "');", true);
        }
    }
}