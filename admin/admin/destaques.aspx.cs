﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxTabControl;
using DevExpress.Web.ASPxEditors;

public partial class admin_destaques : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void grd_HtmlRowCreated(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType != GridViewRowType.Data) return;
        {
            Label lblCategoria = (Label)grd.FindRowCellTemplateControl(e.VisibleIndex, (GridViewDataColumn)grd.Columns["Categoria"], "lblCategoria");

            if (lblCategoria.Text == string.Empty)
                e.Row.Cells[2].Text = "Nenhuma";
        }
    }

    //protected void btPdf_Click(object sender, ImageClickEventArgs e)
    //{
    //    this.grdEx.WritePdfToResponse();
    //}
    //protected void btXsl_Click(object sender, ImageClickEventArgs e)
    //{
    //    this.grdEx.WriteXlsToResponse();
    //}
    //protected void btRtf_Click(object sender, ImageClickEventArgs e)
    //{
    //    this.grdEx.WriteRtfToResponse();
    //}
    //protected void btCsv_Click(object sender, ImageClickEventArgs e)
    //{
    //    this.grdEx.WriteCsvToResponse();
    //}

    protected void grd_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        CuteEditor.Editor txtDestaque = (CuteEditor.Editor)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["Destaque"], "txtDestaque");
        DropDownList drpNomeDapagina = (DropDownList)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["Nome da página"], "drpNomeDapagina");
        DropDownList drpCategorias = (DropDownList)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["Categoria"], "drpCategorias");

        e.NewValues["destaque"] = txtDestaque.Text;
        e.NewValues["nomeDaPagina"] = drpNomeDapagina.SelectedItem.Value;
        e.NewValues["categoriaId"] = drpCategorias.SelectedItem.Value;
    }

    protected void grd_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        CuteEditor.Editor txtDestaque = (CuteEditor.Editor)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["Destaque"], "txtDestaque");
        DropDownList drpNomeDapagina = (DropDownList)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["Nome da página"], "drpNomeDapagina");
        DropDownList drpCategorias = (DropDownList)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["Categoria"], "drpCategorias");   

        e.NewValues["destaque"] = txtDestaque.Text;
        e.NewValues["nomeDaPagina"] = drpNomeDapagina.SelectedItem.Value;
        e.NewValues["categoriaId"] = drpCategorias.SelectedItem.Value;
    }
}