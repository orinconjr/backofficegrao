﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="vincularEtiqueta.aspx.cs" Inherits="admin_vincularEtiqueta" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="js/jquery.min.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            Etiqueta:<br />
            <asp:TextBox runat="server" ID="txtEtiqueta"></asp:TextBox>
        </div>
        <div>
            IdPedido:<br />
            <asp:TextBox runat="server" ID="txtPedidoId"></asp:TextBox>
        </div>
        <div>
            IdItemPedido:<br />
            <asp:TextBox runat="server" ID="txtIdItemPedido"></asp:TextBox>
        </div>
        <div>
            <asp:Button runat="server" ID="btnVincularEtiqueta" Text="Vincular Etiqueta" OnClick="btnVincularEtiqueta_OnClick" />
            <asp:Button runat="server" ID="btnAuxProcessoVincularEtiqueta" OnClick="btnAuxProcessoVincularEtiqueta_OnClick" Style="display: none;"/>
        </div>
    </form>
</body>
</html>
