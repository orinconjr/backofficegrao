﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="enviarDanfeNotas.aspx.cs" Inherits="admin_enviarDanfeNotas" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxTimer" TagPrefix="dxt" %>
<%@ Register assembly="CuteEditor" namespace="CuteEditor" tagprefix="CE" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" Runat="Server">
    <table cellpadding="0" cellspacing="0" style="width: 920px">
    <tr>
        <td class="tituloPaginas" valign="top">
            <asp:Label ID="lblAcao" runat="server">Enviar Danfes</asp:Label>
        </td>
    </tr>
        
    <tr>
        <td>
            
    <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                <table style="width: 100%;">
                    
                    <tr class="rotulos" style="text-align: center; font-weight: bold;">
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            
                        </td>
                        <td style="text-align: right">
                            <asp:Button runat="server" ID="btnGravarDanfes" OnClick="btnGravarDanfes_OnClick" Text="Gravar" />
                        </td>
                    </tr>
                    <tr class="rotulos" style="text-align: center; font-weight: bold;">
                        <td>
                            Pedido
                        </td>
                        <td>
                            Número da Nota
                        </td>
                        <td>
                            Arquivo
                        </td>
                    </tr>
                    <asp:ListView runat="server" ID="lstProdutos" OnItemDataBound="lstProdutos_OnItemDataBound">
                        <ItemTemplate>
                            <tr class="rotulos" style="text-align: center">
                                <td>
                                    <asp:HiddenField runat="server" Value='<%# Eval("pedidoId") %>' ID="hdfIdPedido" />
                                    <%# rnFuncoes.retornaIdCliente(Convert.ToInt32(Eval("pedidoId"))) %>
                                </td>
                                <td>
                                    <asp:Literal runat="server" ID="litNumeroNfe" Text='<%# Eval("nfeNumero") %>'></asp:Literal>
                                </td>
                                <td>
                                    <asp:FileUpload runat="server" ID="fluDanfe"/>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:ListView>          
                </table>        
            </td>
        </tr>
        <tr>
            <td style="text-align: right;">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td style="padding-top: 100px;">
                &nbsp;
            </td>
        </tr>
    </table>
        </td>
    </tr>
</table>
</asp:Content>

