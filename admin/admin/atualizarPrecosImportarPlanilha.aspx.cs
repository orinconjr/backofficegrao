﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Reflection;
using System.Text;
using SpreadsheetLight;

public partial class admin_atualizarPrecosImportarPlanilha : System.Web.UI.Page
{

    public class meusprodutos
    {
        public int produtoId { get; set; }
        public string produtoIdDaEmpresa { get; set; }
        public string produtoNome { get; set; }
        public decimal produtoPreco { get; set; }
        public decimal? produtoPrecoDeCusto { get; set; }
        public decimal? produtoPrecoPromocional { get; set; }
        public decimal margemDeLucro { get; set; }
        public int produtoFornecedor { get; set; }
        public string produtoAtivo { get; set; }
        public DateTime produtoDataDaCriacao { get; set; }
        public string complementoIdDaEmpresa { get; set; }
        public string incluidoDeCombo { get; set; }
        public double produtoPeso { get; set; }
        public string fornecedorNome { get; set; }
        public decimal totalVendaCombo { get; set; }



        internal bool Contains()
        {
            throw new NotImplementedException();
        }
    }
    public class meusprodutoscategoriaespecifica
    {
        public int produtoId { get; set; }
        public string produtoIdDaEmpresa { get; set; }
        public string produtoNome { get; set; }
        public decimal produtoPreco { get; set; }
        public decimal? produtoPrecoDeCusto { get; set; }
        public decimal? produtoPrecoPromocional { get; set; }
        public decimal margemDeLucro { get; set; }
        public int produtoFornecedor { get; set; }
        public string produtoAtivo { get; set; }
        public DateTime produtoDataDaCriacao { get; set; }
        public string complementoIdDaEmpresa { get; set; }
        public string incluidoDeCombo { get; set; }
        public double produtoPeso { get; set; }
        public string fornecedorNome { get; set; }
        public decimal? precoVigente { get; set; }
        public decimal? porcentagemDesconto { get; set; }
        public decimal totalVendaCombo { get; set; }

        internal bool Contains()
        {
            throw new NotImplementedException();
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            txtDataInicial.Attributes.Add("onkeypress", "Mascaras('DATA', '" + txtDataInicial.ClientID + "');");
            txtDataFinal.Attributes.Add("onkeypress", "Mascaras('DATA', '" + txtDataFinal.ClientID + "');");

            ddlExportarPlanilha.Items.Insert(0, new ListItem("Todos Fornecedores", "0"));
            ddlExportarProdutosPorFornecedor.Items.Insert(0, new ListItem("Todos Fornecedores", "0"));

            using (var data = new dbCommerceDataContext())
            {
                var categoriasFornecedor = (from c in data.tbProdutoCategorias
                                            where c.exibirSite == true
                                            select new { c.categoriaId, c.categoriaNome }).Distinct().OrderBy(x => x.categoriaNome);

                ddlExportarProdutosPorFornecedor_Categoria.Items.Clear();

                ddlExportarProdutosPorFornecedor_Categoria.DataSource = categoriasFornecedor;
                ddlExportarProdutosPorFornecedor_Categoria.DataBind();

                ddlExportarProdutosPorFornecedor_Categoria.Items.Insert(0, new ListItem("Todas as Categorias", "0"));
            }

            //  tbProdutosAtivos(112);
        }

    }

    protected void btnImportar_Click(object sender, EventArgs e)
    {
        string caminho = MapPath("~/");

        if (File.Exists(caminho + "planilhaPrecosFornecedor" + Path.GetExtension(fupPlanilha.FileName).ToLower()))
            File.Delete(caminho + "planilhaPrecosFornecedor" + Path.GetExtension(fupPlanilha.FileName).ToLower());
        fupPlanilha.SaveAs(caminho + "planilhaPrecosFornecedor" + Path.GetExtension(fupPlanilha.FileName).ToLower());


        string caminhoPlanilha = caminho + "planilhaPrecosFornecedor" +
                                 Path.GetExtension(fupPlanilha.FileName).ToLower();

        int qtdProdutosAtualizados = 0;
        int qtdProdutosNaPlanilha = 0;
        StringBuilder resultado = new StringBuilder();
        resultado.Append(
            "<div class='titAlteracoes'><strong>Alterações efetuadas:</strong></div>");

        List<int> linhasComErro = new List<int>();
        try
        {

            using (SLDocument sl = new SLDocument(caminhoPlanilha, "Sheet1"))
            {
                SLWorksheetStatistics stats = sl.GetWorksheetStatistics();
                int iStartColumnIndex = stats.StartColumnIndex;

                if (sl.GetCellValueAsString(1, 1) != "produtoId" || sl.GetCellValueAsString(1, 2) != "produtoIdDaEmpresa" ||
                    sl.GetCellValueAsString(1, 3) != "produtoNome" || sl.GetCellValueAsString(1, 4) != "produtoPreco" ||
                    sl.GetCellValueAsString(1, 5) != "produtoPrecoDeCusto" || sl.GetCellValueAsString(1, 6) != "produtoPrecoPromocional" ||
                    sl.GetCellValueAsString(1, 7) != "margemDeLucro" || sl.GetCellValueAsString(1, 8) != "produtoFornecedor" ||
                    sl.GetCellValueAsString(1, 9) != "produtoAtivo" || sl.GetCellValueAsString(1, 10) != "produtoDataDaCriacao" ||
                    sl.GetCellValueAsString(1, 11) != "complementoIdDaEmpresa" || sl.GetCellValueAsString(1, 12) != "incluidoDeCombo" ||
                    sl.GetCellValueAsString(1, 13) != "produtoPeso" || sl.GetCellValueAsString(1, 14) != "Fornecedor")
                {

                    ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('É necessário que a planilha tenha a estrutura de colunas: produtoId, produtoIdDaEmpresa, produtoNome, " +
                                                                                                    "produtoPreco, produtoPrecoDeCusto, produtoPrecoPromocional, margemDeLucro, produtoFornecedor, produtoAtivo, " +
                                                                                                    "produtoDataDaCriacao, complementoIdDaEmpresa, incluidoDeCombo, produtoPeso, Fornecedor. Para definir os dados " +
                                                                                                    "que serão atualizados, marque na caixa abaixo (Colunas a Atualizar)!');", true);

                    return;

                }


                qtdProdutosNaPlanilha = stats.EndRowIndex - 1;
                for (int row = stats.StartRowIndex + 1; row <= stats.EndRowIndex; ++row)
                {
                    try
                    {
                        var dataOriginal = new dbCommerceDataContext();
                        using (var data = new dbCommerceDataContext())
                        {
                            int produtoIdPlanilha = sl.GetCellValueAsInt32(row, iStartColumnIndex);
                            var produto =
                                (from c in data.tbProdutos
                                 where c.produtoId == produtoIdPlanilha
                                 select c).FirstOrDefault();
                            if (produto != null)
                            {
                                var produtoOriginal = (from c in dataOriginal.tbProdutos where c.produtoId == produtoIdPlanilha select c).First();
                                var log = new rnLog();
                                log.usuario = rnUsuarios.retornaNomeUsuarioLogado();
                                if (ckbprodutoPreco.Checked)
                                {
                                    try
                                    {
                                        produto.produtoPreco = 0;
                                        try
                                        {
                                            produto.produtoPreco = Math.Round(
                                            Convert.ToDecimal(sl.GetCellValueAsDecimal(row, iStartColumnIndex + 3)), 2);
                                        }
                                        catch (Exception) { }

                                        if (produto.produtoPreco == 0)
                                        {
                                            string textoAlteracao = "produtoId: " + produtoIdPlanilha +
                                                                    " - produtoPreco está zerado na planilha - ALTERAÇÃO NÃO REALIZADA ";

                                            resultado.Append("<div class='linhaAteracoes'><strong>" + textoAlteracao +
                                                             "</strong></div>");
                                        }
                                        else
                                        {
                                            if (produto.produtoPreco != produtoOriginal.produtoPreco)
                                            {
                                                string textoAlteracao = "produtoId: " + produtoIdPlanilha +
                                                                        " - produtoPreco alterado de " +
                                                                        produtoOriginal.produtoPreco +
                                                                        " para " + produto.produtoPreco;
                                                resultado.Append("<div class='linhaAteracoes'>" + textoAlteracao + "</div>");
                                                log.descricoes.Add(textoAlteracao);

                                            }
                                        }

                                    }
                                    catch (Exception ex)
                                    {
                                        string erro = ex.Message;
                                    }
                                }

                                if (ckbprodutoPrecoDeCusto.Checked)
                                {
                                    try
                                    {
                                        produto.produtoPrecoDeCusto = 0;
                                        try
                                        {
                                            //    produto.produtoPrecoDeCusto =
                                            //Math.Round(
                                            //    Convert.ToDecimal(sl.GetCellValueAsDecimal(row, iStartColumnIndex + 4)), 1);
                                            produto.produtoPrecoDeCusto = Convert.ToDecimal(sl.GetCellValueAsDecimal(row, iStartColumnIndex + 4));
                                        }
                                        catch (Exception)
                                        {

                                        }
                                        if (produto.produtoPrecoDeCusto == 0)
                                        {
                                            string textoAlteracao = "produtoId: " + produtoIdPlanilha +
                                                                    " - produtoPrecoDeCusto está zerado na planilha - ALTERAÇÃO NÃO REALIZADA ";

                                            resultado.Append("<div class='linhaAteracoes'><strong>" + textoAlteracao +
                                                             "</strong></div>");
                                        }
                                        else
                                        {
                                            if (produto.produtoPrecoDeCusto != produtoOriginal.produtoPrecoDeCusto)
                                            {
                                                string textoAlteracao = "produtoId: " + produtoIdPlanilha +
                                                                  " - produtoPrecoDeCusto alterado de " +
                                                                  produtoOriginal.produtoPrecoDeCusto +
                                                                  " para " + produto.produtoPrecoDeCusto;
                                                resultado.Append("<div class='linhaAteracoes'>" + textoAlteracao + "</div>");
                                                log.descricoes.Add(textoAlteracao);       
                                            }
                                        }


                                        var dataCombo = new dbCommerceDataContext();
                                        var relacionados = (from c in dataCombo.tbProdutoRelacionados where c.idProdutoFilho == produto.produtoId select c).ToList();
                                        foreach (var relacionado in relacionados)
                                        {
                                            if (relacionado.idProdutoPai != produto.produtoId)
                                            {
                                                var filhos = (from c in dataCombo.tbProdutoRelacionados
                                                              join d in dataCombo.tbProdutos on c.idProdutoFilho equals d.produtoId
                                                              where c.idProdutoPai == relacionado.idProdutoPai
                                                              select new { d.produtoId, produtoPrecoDeCusto = (d.produtoPrecoDeCusto ?? 0) }).ToList();
                                                var custoTotalCombo = filhos.Sum(x => x.produtoPrecoDeCusto);
                                                var produtoRelacionadoAlterar = (from c in dataCombo.tbProdutos where c.produtoId == relacionado.idProdutoPai select c).First();
                                                if (produtoRelacionadoAlterar.produtoPrecoDeCusto != custoTotalCombo)
                                                {
                                                    log.descricoes.Add("Custo total do combo alterado de " + produtoRelacionadoAlterar.produtoPrecoDeCusto.ToString() + " para " + custoTotalCombo.ToString());
                                                    log.tiposRelacionados.Add(new rnEnums.TipoRelacionadoIds() { idRegistroRelacionado = relacionado.idProdutoPai, tipoRelacionado = rnEnums.TipoRegistroRelacionado.Produto });

                                                    resultado.Append("<div class='linhaAteracoes'>" + relacionado.idProdutoPai + " - Custo total do combo alterado de " + produtoRelacionadoAlterar.produtoPrecoDeCusto.ToString() + " para " + custoTotalCombo.ToString() + "</div>");
                                                    produtoRelacionadoAlterar.produtoPrecoDeCusto = custoTotalCombo;
                                                    dataCombo.SubmitChanges();
                                                }
                                            }
                                        }

                                    }
                                    catch (Exception ex)
                                    {
                                        string erro = ex.Message;
                                    }
                                }

                                if (ckbprodutoPrecoPromocional.Checked)
                                {
                                    try
                                    {
                                        produto.produtoPrecoPromocional =
                                            Math.Round(Convert.ToDecimal(sl.GetCellValueAsDecimal(row, iStartColumnIndex + 5)), 2);
                                        if (produto.produtoPrecoPromocional != produtoOriginal.produtoPrecoPromocional)
                                        {
                                            string textoAlteracao = "produtoId: " + produtoIdPlanilha +
                                                                " - produtoPrecoPromocional alterado de " +
                                                                produtoOriginal.produtoPrecoPromocional +
                                                                " para " + produto.produtoPrecoPromocional;
                                            resultado.Append("<div class='linhaAteracoes'>" + textoAlteracao + "</div>");
                                            log.descricoes.Add(textoAlteracao);
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        string erro = ex.Message;
                                    }
                                }
                                if (ckbmargemDeLucro.Checked)
                                {
                                    try
                                    {
                                        produto.margemDeLucro =
                                            Math.Round(
                                                Convert.ToDecimal(sl.GetCellValueAsDecimal(row, iStartColumnIndex + 6)), 2);
                                        if (produto.margemDeLucro != produtoOriginal.margemDeLucro)
                                        {
                                            string textoAlteracao = "produtoId: " + produtoIdPlanilha +
                                                           " - margemDeLucro alterado de " +
                                                           produtoOriginal.margemDeLucro +
                                                           " para " + produto.margemDeLucro;
                                            resultado.Append("<div class='linhaAteracoes'>" + textoAlteracao + "</div>");
                                            log.descricoes.Add(textoAlteracao);
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        string erro = ex.Message;
                                    }

                                }

                                if (ckbprodutoAtivo.Checked)
                                {
                                    try
                                    {
                                        produto.produtoAtivo = sl.GetCellValueAsString(row, iStartColumnIndex + 8);
                                        if (produto.produtoAtivo != produtoOriginal.produtoAtivo)
                                        {
                                            string textoAlteracao = "produtoId: " + produtoIdPlanilha +
                                                         " - produtoAtivo alterado de " +
                                                         produtoOriginal.produtoAtivo +
                                                         " para " + produto.produtoAtivo;
                                            resultado.Append("<div class='linhaAteracoes'>" + textoAlteracao + "</div>");
                                            log.descricoes.Add(textoAlteracao);
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        string erro = ex.Message;
                                    }

                                }
                                if (ckbprodutoPeso.Checked)
                                {
                                    try
                                    {
                                        produto.produtoPeso = Convert.ToDouble(sl.GetCellValueAsDecimal(row, iStartColumnIndex + 12));
                                        if (produto.produtoPeso != produtoOriginal.produtoPeso)
                                        {
                                            string textoAlteracao = "produtoId: " + produtoIdPlanilha +
                                                           " - produtoPeso alterado de " +
                                                           produtoOriginal.produtoPeso +
                                                           " para " + produto.produtoPeso;
                                            resultado.Append("<div class='linhaAteracoes'>" + textoAlteracao + "</div>");
                                            log.descricoes.Add(textoAlteracao);
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        string erro = ex.Message;
                                    }

                                }
                                


                                data.SubmitChanges();
                                var tipoRelacionado = new rnEnums.TipoRelacionadoIds() { idRegistroRelacionado = produtoIdPlanilha, tipoRelacionado = rnEnums.TipoRegistroRelacionado.Produto };
                                log.tiposRelacionados.Add(tipoRelacionado);
                                log.tiposOperacao.Add(rnEnums.TipoOperacao.Produto);
                                log.InsereLog();

                                qtdProdutosAtualizados++;
                            }
                            else
                            {
                                linhasComErro.Add(row);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('" + ex.Message + "');", true);
                        linhasComErro.Add(row);
                    }

                }

                litProdutosAtualizados.Text = resultado.ToString();
                string mensagem = "Atualização realizada com sucesso!\\n" + qtdProdutosAtualizados.ToString() + " / " + qtdProdutosNaPlanilha.ToString() + " foram atualizados!";
                if(linhasComErro.Count > 0)
                {
                    mensagem += " Linhas com erro: " + string.Join(",", linhasComErro.ToArray());
                }
                  ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('" + mensagem + "');", true);
            }

        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('Falha no upload do arquivo verifique se a aba da planilha esta nomeada como, Sheet1, e a extensão do arquivo xlxs!');", true);
        }

    }


    protected void btnExportar_Click(object sender, EventArgs e)
    {
        try
        {
            int fornecedor = Convert.ToInt32(ddlExportarPlanilha.SelectedValue);

            SLDocument sl = new SLDocument();
            SLStyle style1 = sl.CreateStyle();
            style1.Font.Bold = true;
            sl.SetRowStyle(1, style1);

            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("Content-Disposition", "attachment; filename=" + ddlExportarPlanilha.SelectedItem.Text.Replace(" ", "").Trim() + ".xlsx");

            sl.SetCellValue("A1", "produtoId");
            sl.SetCellValue("B1", "produtoIdDaEmpresa");
            sl.SetCellValue("C1", "produtoNome");
            sl.SetCellValue("D1", "produtoPreco");
            sl.SetCellValue("E1", "produtoPrecoDeCusto");
            sl.SetCellValue("F1", "produtoPrecoPromocional");
            sl.SetCellValue("G1", "margemDeLucro");
            sl.SetCellValue("H1", "produtoFornecedor");
            sl.SetCellValue("I1", "produtoAtivo");
            sl.SetCellValue("J1", "produtoDataDaCriacao");
            sl.SetCellValue("K1", "complementoIdDaEmpresa");
            sl.SetCellValue("L1", "incluidoDeCombo");
            sl.SetCellValue("M1", "produtoPeso");
            sl.SetCellValue("N1", "totalVendaCombo");

            using (var data = new dbCommerceDataContext())
            {

                IEnumerable<meusprodutos> dados = null;
                if (fornecedor != 0)
                {
                    if (rbativosomenteinativos.Checked)
                    {

                        // Com fornecedor e só inativos:
                        dados = (from p in data.tbProdutos
                                 join combo in data.tbProdutoRelacionados on p.produtoId equals combo.idProdutoPai into combos
                                 where p.produtoFornecedor == fornecedor &&
                                 p.produtoAtivo == "False"
                                 select new meusprodutos
                                 {
                                     produtoId = p.produtoId,
                                     produtoIdDaEmpresa = p.produtoIdDaEmpresa,
                                     produtoNome = p.produtoNome,
                                     produtoPreco = p.produtoPreco,
                                     produtoPrecoDeCusto = p.produtoPrecoDeCusto,
                                     produtoPrecoPromocional = p.produtoPrecoPromocional,
                                     margemDeLucro = p.margemDeLucro,
                                     produtoFornecedor = p.produtoFornecedor,
                                     produtoAtivo = p.produtoAtivo,
                                     produtoDataDaCriacao = p.produtoDataDaCriacao,
                                     complementoIdDaEmpresa = p.complementoIdDaEmpresa,
                                     incluidoDeCombo = "False",
                                     produtoPeso = p.produtoPeso,
                                     totalVendaCombo = combos.Count() == 0 ? 0 : (combos.Sum(x => (decimal)x.tbProduto.produtoPrecoPromocional > 0 ? (decimal)x.tbProduto.produtoPrecoPromocional : x.tbProduto.produtoPreco))
                                 }).ToList<meusprodutos>();
                    }
                    if (rbativotodos.Checked)
                    {
                        // Com fornecedor ativos e inativos:
                        dados = (from p in data.tbProdutos
                                 join combo in data.tbProdutoRelacionados on p.produtoId equals combo.idProdutoPai into combos
                                 where (p.produtoFornecedor == fornecedor | (combos.Count(x => x.tbProduto.produtoFornecedor == fornecedor) > 0)) //&& p.produtoAtivo == "True"
                                 select new meusprodutos
                                 {
                                     produtoId = p.produtoId,
                                     produtoIdDaEmpresa = p.produtoIdDaEmpresa,
                                     produtoNome = p.produtoNome,
                                     produtoPreco = p.produtoPreco,
                                     produtoPrecoDeCusto = p.produtoPrecoDeCusto,
                                     produtoPrecoPromocional = p.produtoPrecoPromocional,
                                     margemDeLucro = p.margemDeLucro,
                                     produtoFornecedor = p.produtoFornecedor,
                                     produtoAtivo = p.produtoAtivo,
                                     produtoDataDaCriacao = p.produtoDataDaCriacao,
                                     complementoIdDaEmpresa = p.complementoIdDaEmpresa,
                                     incluidoDeCombo = "False",
                                     produtoPeso = p.produtoPeso,
                                     totalVendaCombo = combos.Count() == 0 ? 0 : (combos.Sum(x => (decimal)x.tbProduto.produtoPrecoPromocional > 0 ? (decimal)x.tbProduto.produtoPrecoPromocional : x.tbProduto.produtoPreco))
                                 }).ToList<meusprodutos>();
                    }
                    // Com fornecedor e só ativos:
                    if (rbativosomenteativos.Checked)
                    {
                        dados = (from p in data.tbProdutos
                                 join combo in data.tbProdutoRelacionados on p.produtoId equals combo.idProdutoPai into combos
                                 where (p.produtoFornecedor == fornecedor | (combos.Count(x => x.tbProduto.produtoFornecedor == fornecedor) > 0)) && p.produtoAtivo == "True"
                                 select new meusprodutos
                                 {
                                     produtoId = p.produtoId,
                                     produtoIdDaEmpresa = p.produtoIdDaEmpresa,
                                     produtoNome = p.produtoNome,
                                     produtoPreco = p.produtoPreco,
                                     produtoPrecoDeCusto = p.produtoPrecoDeCusto,
                                     produtoPrecoPromocional = p.produtoPrecoPromocional,
                                     margemDeLucro = p.margemDeLucro,
                                     produtoFornecedor = p.produtoFornecedor,
                                     produtoAtivo = p.produtoAtivo,
                                     produtoDataDaCriacao = p.produtoDataDaCriacao,
                                     complementoIdDaEmpresa = p.complementoIdDaEmpresa,
                                     incluidoDeCombo = "False",
                                     produtoPeso = p.produtoPeso,
                                     totalVendaCombo = combos.Count() == 0 ? 0 : (combos.Sum(x => (decimal)x.tbProduto.produtoPrecoPromocional > 0 ? (decimal)x.tbProduto.produtoPrecoPromocional : x.tbProduto.produtoPreco))
                                 }).ToList<meusprodutos>();
                    }


                }
                else // Se fornecedor nao fui escolhido
                {
                    dados = (from p in data.tbProdutos
                             join combo in data.tbProdutoRelacionados on p.produtoId equals combo.idProdutoPai into combos
                             select new meusprodutos
                             {
                                 produtoId = p.produtoId,
                                 produtoIdDaEmpresa = p.produtoIdDaEmpresa,
                                 produtoNome = p.produtoNome,
                                 produtoPreco = p.produtoPreco,
                                 produtoPrecoDeCusto = p.produtoPrecoDeCusto,
                                 produtoPrecoPromocional = p.produtoPrecoPromocional,
                                 margemDeLucro = p.margemDeLucro,
                                 produtoFornecedor = p.produtoFornecedor,
                                 produtoAtivo = p.produtoAtivo,
                                 produtoDataDaCriacao = p.produtoDataDaCriacao,
                                 complementoIdDaEmpresa = p.complementoIdDaEmpresa,
                                 incluidoDeCombo = "False",
                                 produtoPeso = p.produtoPeso,
                                 totalVendaCombo = combos.Count() == 0 ? 0 : (combos.Sum(x => (decimal)x.tbProduto.produtoPrecoPromocional > 0 ? (decimal)x.tbProduto.produtoPrecoPromocional : x.tbProduto.produtoPreco))
                             }).ToList<meusprodutos>();
                    if (rbativosomenteativos.Checked) // trocar por modelo com procedure
                    {
                        dados = dados.Where(x => x.produtoAtivo.ToLower().Contains("true"));

                    }

                    if (rbativosomenteinativos.Checked)
                    {
                        dados = dados.Where(x => x.produtoAtivo.ToLower().Contains("false"));

                    }
                }




                // dados = ckbAtivo.Checked ? dados.Where(x => x.produtoAtivo.ToLower().Contains("true")) : dados.Where(x => x.produtoAtivo.ToLower().Contains("false"));

                if (txtDataInicial.Text != "" && txtDataFinal.Text != "")
                {
                    var dtInicial = Convert.ToDateTime(txtDataInicial.Text);
                    var dtFinal = Convert.ToDateTime(txtDataFinal.Text);

                    dados =
                        dados.Where(x => x.produtoDataDaCriacao.Date >= dtInicial.Date && x.produtoDataDaCriacao.Date <= dtFinal.Date);
                }
                int linha = 2;
                foreach (var item in dados)
                {
                    sl.SetCellValue(linha, 1, item.produtoId);
                    sl.SetCellValue(linha, 2, item.produtoIdDaEmpresa);
                    sl.SetCellValue(linha, 3, item.produtoNome);
                    sl.SetCellValue(linha, 4, item.produtoPreco);
                    sl.SetCellValue(linha, 5, item.produtoPrecoDeCusto.ToString());
                    sl.SetCellValue(linha, 6, item.produtoPrecoPromocional.ToString());
                    sl.SetCellValue(linha, 7, item.margemDeLucro);
                    sl.SetCellValue(linha, 8, item.produtoFornecedor);
                    sl.SetCellValue(linha, 9, item.produtoAtivo);
                    sl.SetCellValue(linha, 10, item.produtoDataDaCriacao.ToString());
                    sl.SetCellValue(linha, 11, item.complementoIdDaEmpresa);
                    sl.SetCellValue(linha, 12, item.incluidoDeCombo);
                    sl.SetCellValue(linha, 13, item.produtoPeso);
                    sl.SetCellValue(linha, 14, item.totalVendaCombo);
                    linha++;
                }
                //GridView1.DataSource = dados;
                //GridView1.DataBind();
            }

            //Exporta 
            sl.SaveAs(Response.OutputStream);
            Response.End();
        }
        catch (Exception ex)
        {

            throw;
        }
    }

    

    protected void ddlExportarProdutosPorFornecedor_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlExportarProdutosPorFornecedor.SelectedValue != "0")
        {
            int fornecedorId = Convert.ToInt32(ddlExportarProdutosPorFornecedor.SelectedValue);

            using (var data = new dbCommerceDataContext())
            {
                var categoriasFornecedor = (from c in data.tbProdutoCategorias
                                            join pc in data.tbJuncaoProdutoCategorias on c.categoriaId equals pc.categoriaId
                                            join p in data.tbProdutos on pc.produtoId equals p.produtoId
                                            where p.produtoFornecedor == fornecedorId && c.exibirSite == true
                                            select new { c.categoriaId, c.categoriaNome }).Distinct().OrderBy(x => x.categoriaNome);

                ddlExportarProdutosPorFornecedor_Categoria.Items.Clear();

                ddlExportarProdutosPorFornecedor_Categoria.DataSource = categoriasFornecedor;
                ddlExportarProdutosPorFornecedor_Categoria.DataBind();

                ddlExportarProdutosPorFornecedor_Categoria.Items.Insert(0, new ListItem("Todas as Categorias", "0"));
            }

        }
        else
        {
            ddlExportarProdutosPorFornecedor_Categoria.Items.Clear();

            ddlExportarProdutosPorFornecedor_Categoria.Items.Insert(0, new ListItem("Todas as Categorias", "0"));
        }
    }

    protected void btnExportarProdutos_OnClick(object sender, EventArgs e)
    {

        if (rblExportarProdutos.SelectedIndex == -1)
        {
            Response.Write("<script>alert('Selecione entre Todos os Produto ou Produtos Ativos')</script>");
            return;
        }

        try
        {
            int fornecedor = Convert.ToInt32(ddlExportarProdutosPorFornecedor.SelectedValue);

            SLDocument sl = new SLDocument();
            SLStyle style1 = sl.CreateStyle();
            style1.Font.Bold = true;
            sl.SetRowStyle(1, style1);

            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("Content-Disposition", "attachment; filename=" + ddlExportarProdutosPorFornecedor.SelectedItem.Text.Replace(" ", "").Trim() + (ddlExportarProdutosPorFornecedor_Categoria.SelectedValue != "0" ? "--" + ddlExportarProdutosPorFornecedor_Categoria.SelectedItem.Text.Replace(" ", "") : "") + ".xlsx");

            sl.SetCellValue("A1", "produtoId");
            sl.SetCellValue("B1", "produtoIdDaEmpresa");
            sl.SetCellValue("C1", "produtoNome");
            sl.SetCellValue("D1", "produtoPreco");
            sl.SetCellValue("E1", "produtoPrecoDeCusto");
            sl.SetCellValue("F1", "produtoPrecoPromocional");
            sl.SetCellValue("G1", "margemDeLucro");
            sl.SetCellValue("H1", "produtoFornecedor");
            sl.SetCellValue("I1", "produtoAtivo");
            sl.SetCellValue("J1", "produtoDataDaCriacao");
            sl.SetCellValue("K1", "complementoIdDaEmpresa");
            sl.SetCellValue("L1", "incluidoDeCombo");
            sl.SetCellValue("M1", "produtoPeso");
            sl.SetCellValue("N1", "Fornecedor");
            sl.SetCellValue("O1", "totalVendaCombo");

            using (var data = new dbCommerceDataContext())
            {

                IEnumerable<meusprodutos> dados = null;
                if (fornecedor != 0)
                {
                    if (rblExportarProdutos.Items[0].Selected)
                    {

                        if (ddlExportarProdutosPorFornecedor_Categoria.SelectedValue != "0")
                        {
                            int categoriaId = Convert.ToInt32(ddlExportarProdutosPorFornecedor_Categoria.SelectedValue);

                            // Com fornecedor ativos e inativos:
                            dados = (from p in data.tbProdutos
                                     join pc in data.tbJuncaoProdutoCategorias on p.produtoId equals pc.produtoId
                                     join c in data.tbProdutoCategorias on pc.categoriaId equals c.categoriaId
                                     join combo in data.tbProdutoRelacionados on p.produtoId equals combo.idProdutoPai into combos
                                     where p.produtoFornecedor == fornecedor && c.categoriaId == categoriaId
                                     select new meusprodutos
                                     {
                                         produtoId = p.produtoId,
                                         produtoIdDaEmpresa = p.produtoIdDaEmpresa,
                                         produtoNome = p.produtoNome,
                                         produtoPreco = p.produtoPreco,
                                         produtoPrecoDeCusto = p.produtoPrecoDeCusto,
                                         produtoPrecoPromocional = p.produtoPrecoPromocional,
                                         margemDeLucro = ((((p.produtoPrecoPromocional > 0 ? p.produtoPrecoPromocional : p.produtoPreco) * 100) / (p.produtoPrecoDeCusto == 0 ? 1 : p.produtoPrecoDeCusto)) - 100) ?? 0,
                                         produtoFornecedor = p.produtoFornecedor,
                                         produtoAtivo = p.produtoAtivo,
                                         produtoDataDaCriacao = p.produtoDataDaCriacao,
                                         complementoIdDaEmpresa = p.complementoIdDaEmpresa,
                                         incluidoDeCombo = "False",
                                         produtoPeso = p.produtoPeso,
                                         fornecedorNome = p.tbProdutoFornecedor.fornecedorNome,
                                         totalVendaCombo = combos.Count() == 0 ? 0 : (combos.Sum(x => (decimal)x.tbProduto.produtoPrecoPromocional > 0 ? (decimal)x.tbProduto.produtoPrecoPromocional : x.tbProduto.produtoPreco))
                                     }).ToList<meusprodutos>();
                        }
                        else
                        {
                            // Com fornecedor ativos e inativos:
                            dados = (from p in data.tbProdutos
                                     join combo in data.tbProdutoRelacionados on p.produtoId equals combo.idProdutoPai into combos
                                     where p.produtoFornecedor == fornecedor
                                     select new meusprodutos
                                     {
                                         produtoId = p.produtoId,
                                         produtoIdDaEmpresa = p.produtoIdDaEmpresa,
                                         produtoNome = p.produtoNome,
                                         produtoPreco = p.produtoPreco,
                                         produtoPrecoDeCusto = p.produtoPrecoDeCusto,
                                         produtoPrecoPromocional = p.produtoPrecoPromocional,
                                         margemDeLucro = ((((p.produtoPrecoPromocional > 0 ? p.produtoPrecoPromocional : p.produtoPreco) * 100) / (p.produtoPrecoDeCusto == 0 ? 1 : p.produtoPrecoDeCusto)) - 100) ?? 0,
                                         produtoFornecedor = p.produtoFornecedor,
                                         produtoAtivo = p.produtoAtivo,
                                         produtoDataDaCriacao = p.produtoDataDaCriacao,
                                         complementoIdDaEmpresa = p.complementoIdDaEmpresa,
                                         incluidoDeCombo = "False",
                                         produtoPeso = p.produtoPeso,
                                         fornecedorNome = p.tbProdutoFornecedor.fornecedorNome,
                                         totalVendaCombo = combos.Count() == 0 ? 0 : (combos.Sum(x => (decimal)x.tbProduto.produtoPrecoPromocional > 0 ? (decimal)x.tbProduto.produtoPrecoPromocional : x.tbProduto.produtoPreco))
                                     }).ToList<meusprodutos>();
                        }


                    }
                    else
                    {

                        if (ddlExportarProdutosPorFornecedor_Categoria.SelectedValue != "0")
                        {

                            int categoriaId = Convert.ToInt32(ddlExportarProdutosPorFornecedor_Categoria.SelectedValue);

                            // Com fornecedor e só ativos:
                            dados = (from p in data.tbProdutos
                                     join pc in data.tbJuncaoProdutoCategorias on p.produtoId equals pc.produtoId
                                     join c in data.tbProdutoCategorias on pc.categoriaId equals c.categoriaId
                                     join combo in data.tbProdutoRelacionados on p.produtoId equals combo.idProdutoPai into combos
                                     where p.produtoFornecedor == fornecedor && p.produtoAtivo == "True" && c.categoriaId == categoriaId
                                     select new meusprodutos
                                     {
                                         produtoId = p.produtoId,
                                         produtoIdDaEmpresa = p.produtoIdDaEmpresa,
                                         produtoNome = p.produtoNome,
                                         produtoPreco = p.produtoPreco,
                                         produtoPrecoDeCusto = p.produtoPrecoDeCusto,
                                         produtoPrecoPromocional = p.produtoPrecoPromocional,
                                         margemDeLucro = ((((p.produtoPrecoPromocional > 0 ? p.produtoPrecoPromocional : p.produtoPreco) * 100) / (p.produtoPrecoDeCusto == 0 ? 1 : p.produtoPrecoDeCusto)) - 100) ?? 0,
                                         produtoFornecedor = p.produtoFornecedor,
                                         produtoAtivo = p.produtoAtivo,
                                         produtoDataDaCriacao = p.produtoDataDaCriacao,
                                         complementoIdDaEmpresa = p.complementoIdDaEmpresa,
                                         incluidoDeCombo = "False",
                                         produtoPeso = p.produtoPeso,
                                         fornecedorNome = p.tbProdutoFornecedor.fornecedorNome,
                                         totalVendaCombo = combos.Count() == 0 ? 0 : (combos.Sum(x => (decimal)x.tbProduto.produtoPrecoPromocional > 0 ? (decimal)x.tbProduto.produtoPrecoPromocional : x.tbProduto.produtoPreco))
                                     }).ToList<meusprodutos>();
                        }
                        else
                        {
                            // Com fornecedor e só ativos:
                            dados = (from p in data.tbProdutos
                                     join combo in data.tbProdutoRelacionados on p.produtoId equals combo.idProdutoPai into combos
                                     where p.produtoFornecedor == fornecedor &&
                                     p.produtoAtivo == "True"
                                     select new meusprodutos
                                     {
                                         produtoId = p.produtoId,
                                         produtoIdDaEmpresa = p.produtoIdDaEmpresa,
                                         produtoNome = p.produtoNome,
                                         produtoPreco = p.produtoPreco,
                                         produtoPrecoDeCusto = p.produtoPrecoDeCusto,
                                         produtoPrecoPromocional = p.produtoPrecoPromocional,
                                         margemDeLucro = ((((p.produtoPrecoPromocional > 0 ? p.produtoPrecoPromocional : p.produtoPreco) * 100) / (p.produtoPrecoDeCusto == 0 ? 1 : p.produtoPrecoDeCusto)) - 100) ?? 0,
                                         produtoFornecedor = p.produtoFornecedor,
                                         produtoAtivo = p.produtoAtivo,
                                         produtoDataDaCriacao = p.produtoDataDaCriacao,
                                         complementoIdDaEmpresa = p.complementoIdDaEmpresa,
                                         incluidoDeCombo = "False",
                                         produtoPeso = p.produtoPeso,
                                         fornecedorNome = p.tbProdutoFornecedor.fornecedorNome,
                                         totalVendaCombo = combos.Count() == 0 ? 0 : (combos.Sum(x => (decimal)x.tbProduto.produtoPrecoPromocional > 0 ? (decimal)x.tbProduto.produtoPrecoPromocional : x.tbProduto.produtoPreco))
                                     }).ToList<meusprodutos>();
                        }

                    }

                }
                else // Se fornecedor nao foi escolhido
                {
                    if (ddlExportarProdutosPorFornecedor_Categoria.SelectedValue != "0")// selecionou uma categoria
                    {
                        int categoriaId = Convert.ToInt32(ddlExportarProdutosPorFornecedor_Categoria.SelectedValue);

                        if (rblExportarProdutos.Items[0].Selected)
                        {
                            dados = (from p in data.tbProdutos
                                     join combo in data.tbProdutoRelacionados on p.produtoId equals combo.idProdutoPai into combos
                                     join pc in data.tbJuncaoProdutoCategorias on p.produtoId equals pc.produtoId
                                     join c in data.tbProdutoCategorias on pc.categoriaId equals c.categoriaId
                                     where c.categoriaId == categoriaId
                                     select new meusprodutos
                                     {
                                         produtoId = p.produtoId,
                                         produtoIdDaEmpresa = p.produtoIdDaEmpresa,
                                         produtoNome = p.produtoNome,
                                         produtoPreco = p.produtoPreco,
                                         produtoPrecoDeCusto = p.produtoPrecoDeCusto,
                                         produtoPrecoPromocional = p.produtoPrecoPromocional,
                                         margemDeLucro = ((((p.produtoPrecoPromocional > 0 ? p.produtoPrecoPromocional : p.produtoPreco) * 100) / (p.produtoPrecoDeCusto == 0 ? 1 : p.produtoPrecoDeCusto)) - 100) ?? 0,
                                         produtoFornecedor = p.produtoFornecedor,
                                         produtoAtivo = p.produtoAtivo,
                                         produtoDataDaCriacao = p.produtoDataDaCriacao,
                                         complementoIdDaEmpresa = p.complementoIdDaEmpresa,
                                         incluidoDeCombo = "False",
                                         produtoPeso = p.produtoPeso,
                                         fornecedorNome = p.tbProdutoFornecedor.fornecedorNome,
                                         totalVendaCombo = combos.Count() == 0 ? 0 : (combos.Sum(x => (decimal)x.tbProduto.produtoPrecoPromocional > 0 ? (decimal)x.tbProduto.produtoPrecoPromocional : x.tbProduto.produtoPreco))
                                     }).ToList<meusprodutos>();
                        }
                        else
                        {
                            dados = (from p in data.tbProdutos
                                     join combo in data.tbProdutoRelacionados on p.produtoId equals combo.idProdutoPai into combos
                                     join pc in data.tbJuncaoProdutoCategorias on p.produtoId equals pc.produtoId
                                     join c in data.tbProdutoCategorias on pc.categoriaId equals c.categoriaId
                                     where c.categoriaId == categoriaId && p.produtoAtivo == "True"
                                     select new meusprodutos
                                     {
                                         produtoId = p.produtoId,
                                         produtoIdDaEmpresa = p.produtoIdDaEmpresa,
                                         produtoNome = p.produtoNome,
                                         produtoPreco = p.produtoPreco,
                                         produtoPrecoDeCusto = p.produtoPrecoDeCusto,
                                         produtoPrecoPromocional = p.produtoPrecoPromocional,
                                         margemDeLucro = ((((p.produtoPrecoPromocional > 0 ? p.produtoPrecoPromocional : p.produtoPreco) * 100) / (p.produtoPrecoDeCusto == 0 ? 1 : p.produtoPrecoDeCusto)) - 100) ?? 0,
                                         produtoFornecedor = p.produtoFornecedor,
                                         produtoAtivo = p.produtoAtivo,
                                         produtoDataDaCriacao = p.produtoDataDaCriacao,
                                         complementoIdDaEmpresa = p.complementoIdDaEmpresa,
                                         incluidoDeCombo = "False",
                                         produtoPeso = p.produtoPeso,
                                         fornecedorNome = p.tbProdutoFornecedor.fornecedorNome,
                                         totalVendaCombo = combos.Count() == 0 ? 0 : (combos.Sum(x => (decimal)x.tbProduto.produtoPrecoPromocional > 0 ? (decimal)x.tbProduto.produtoPrecoPromocional : x.tbProduto.produtoPreco))
                                     }).ToList<meusprodutos>();
                        }


                    }
                    else
                    {
                        if (rblExportarProdutos.Items[0].Selected)
                        {
                            dados = (from p in data.tbProdutos
                                     join combo in data.tbProdutoRelacionados on p.produtoId equals combo.idProdutoPai into combos
                                     select new meusprodutos
                                     {
                                         produtoId = p.produtoId,
                                         produtoIdDaEmpresa = p.produtoIdDaEmpresa,
                                         produtoNome = p.produtoNome,
                                         produtoPreco = p.produtoPreco,
                                         produtoPrecoDeCusto = p.produtoPrecoDeCusto,
                                         produtoPrecoPromocional = p.produtoPrecoPromocional,
                                         margemDeLucro = ((((p.produtoPrecoPromocional > 0 ? p.produtoPrecoPromocional : p.produtoPreco) * 100) / (p.produtoPrecoDeCusto == 0 ? 1 : p.produtoPrecoDeCusto)) - 100) ?? 0,
                                         produtoFornecedor = p.produtoFornecedor,
                                         produtoAtivo = p.produtoAtivo,
                                         produtoDataDaCriacao = p.produtoDataDaCriacao,
                                         complementoIdDaEmpresa = p.complementoIdDaEmpresa,
                                         incluidoDeCombo = "False",
                                         produtoPeso = p.produtoPeso,
                                         fornecedorNome = p.tbProdutoFornecedor.fornecedorNome,
                                         totalVendaCombo = combos.Count() == 0 ? 0 : (combos.Sum(x => (decimal)x.tbProduto.produtoPrecoPromocional > 0 ? (decimal)x.tbProduto.produtoPrecoPromocional : x.tbProduto.produtoPreco))
                                     }).ToList<meusprodutos>();
                        }
                        else
                        {
                            dados = (from p in data.tbProdutos
                                     join combo in data.tbProdutoRelacionados on p.produtoId equals combo.idProdutoPai into combos
                                     where p.produtoAtivo == "True"
                                     select new meusprodutos
                                     {
                                         produtoId = p.produtoId,
                                         produtoIdDaEmpresa = p.produtoIdDaEmpresa,
                                         produtoNome = p.produtoNome,
                                         produtoPreco = p.produtoPreco,
                                         produtoPrecoDeCusto = p.produtoPrecoDeCusto,
                                         produtoPrecoPromocional = p.produtoPrecoPromocional,
                                         margemDeLucro = ((((p.produtoPrecoPromocional > 0 ? p.produtoPrecoPromocional : p.produtoPreco) * 100) / (p.produtoPrecoDeCusto == 0 ? 1 : p.produtoPrecoDeCusto)) - 100) ?? 0,
                                         produtoFornecedor = p.produtoFornecedor,
                                         produtoAtivo = p.produtoAtivo,
                                         produtoDataDaCriacao = p.produtoDataDaCriacao,
                                         complementoIdDaEmpresa = p.complementoIdDaEmpresa,
                                         incluidoDeCombo = "False",
                                         produtoPeso = p.produtoPeso,
                                         fornecedorNome = p.tbProdutoFornecedor.fornecedorNome,
                                         totalVendaCombo = combos.Count() == 0 ? 0 : (combos.Sum(x => (decimal)x.tbProduto.produtoPrecoPromocional > 0 ? (decimal)x.tbProduto.produtoPrecoPromocional : x.tbProduto.produtoPreco))
                                     }).ToList<meusprodutos>();
                        }

                    }

                }


                int linha = 2;
                foreach (var item in dados)
                {
                    sl.SetCellValue(linha, 1, item.produtoId);
                    sl.SetCellValue(linha, 2, item.produtoIdDaEmpresa);
                    sl.SetCellValue(linha, 3, item.produtoNome);
                    sl.SetCellValue(linha, 4, item.produtoPreco);
                    sl.SetCellValue(linha, 5, item.produtoPrecoDeCusto.ToString());
                    sl.SetCellValue(linha, 6, item.produtoPrecoPromocional.ToString());
                    sl.SetCellValue(linha, 7, item.margemDeLucro);
                    sl.SetCellValue(linha, 8, item.produtoFornecedor);
                    sl.SetCellValue(linha, 9, item.produtoAtivo);
                    sl.SetCellValue(linha, 10, item.produtoDataDaCriacao.ToString());
                    sl.SetCellValue(linha, 11, item.complementoIdDaEmpresa);
                    sl.SetCellValue(linha, 12, item.incluidoDeCombo);
                    sl.SetCellValue(linha, 13, item.produtoPeso);
                    sl.SetCellValue(linha, 14, item.fornecedorNome);
                    sl.SetCellValue(linha, 15, item.totalVendaCombo);
                    linha++;
                }

            }

            //Exporta 
            sl.SaveAs(Response.OutputStream);
            Response.End();
        }
        catch (Exception ex)
        {

        }
    }


    protected void btnExportaProdutoPorCategoriaEspecifica_OnClick(object sender, EventArgs e)
    {
        try
        {
            int categoria = Convert.ToInt32(ddlCategoriaEspecifica.SelectedValue);

            SLDocument sl = new SLDocument();
            SLStyle style1 = sl.CreateStyle();
            style1.Font.Bold = true;
            sl.SetRowStyle(1, style1);

            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("Content-Disposition", "attachment; filename=" + ddlCategoriaEspecifica.SelectedItem.Text.Replace(" ", "").Trim() + ".xlsx");

            sl.SetCellValue("A1", "produtoId");
            sl.SetCellValue("B1", "produtoIdDaEmpresa");
            sl.SetCellValue("C1", "produtoNome");
            sl.SetCellValue("D1", "produtoPreco");
            sl.SetCellValue("E1", "produtoPrecoDeCusto");
            sl.SetCellValue("F1", "produtoPrecoPromocional");
            sl.SetCellValue("G1", "margemDeLucro");
            sl.SetCellValue("H1", "produtoFornecedor");
            sl.SetCellValue("I1", "produtoAtivo");
            sl.SetCellValue("J1", "produtoDataDaCriacao");
            sl.SetCellValue("K1", "complementoIdDaEmpresa");
            sl.SetCellValue("L1", "incluidoDeCombo");
            sl.SetCellValue("M1", "produtoPeso");
            sl.SetCellValue("N1", "precoVigente");
            sl.SetCellValue("O1", "porcetagemDesconto");
            sl.SetCellValue("P1", "totalVendaCombo");

            using (var data = new dbCommerceDataContext())
            {

                IEnumerable<meusprodutoscategoriaespecifica> dados = null;
                if (categoria != 0)
                {
                    if (rbtPESI.Checked)
                    {

                        // Produtos especificos só inativos:

                        using (
                            var txn =
                                new System.Transactions.TransactionScope(
                                    System.Transactions.TransactionScopeOption.Required,
                                    new System.Transactions.TransactionOptions
                                    {
                                        IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                                    }))

                        {

                            dados = (from p in data.tbProdutos
                                     join combo in data.tbProdutoRelacionados on p.produtoId equals combo.idProdutoPai into combos
                                     join jc in data.tbJuncaoProdutoCategorias on p.produtoId equals jc.produtoId
                                     join cat in data.tbProdutoCategorias on jc.categoriaId equals cat.categoriaId
                                     where cat.categoriaId == categoria &&
                                           p.produtoAtivo == "False"
                                     select new meusprodutoscategoriaespecifica
                                     {
                                         produtoId = p.produtoId,
                                         produtoIdDaEmpresa = p.produtoIdDaEmpresa,
                                         produtoNome = p.produtoNome,
                                         produtoPreco = p.produtoPreco,
                                         produtoPrecoDeCusto = p.produtoPrecoDeCusto,
                                         produtoPrecoPromocional = p.produtoPrecoPromocional,
                                         margemDeLucro = p.margemDeLucro,
                                         produtoFornecedor = p.produtoFornecedor,
                                         produtoAtivo = p.produtoAtivo,
                                         produtoDataDaCriacao = p.produtoDataDaCriacao,
                                         complementoIdDaEmpresa = p.complementoIdDaEmpresa,
                                         incluidoDeCombo = "False",
                                         produtoPeso = p.produtoPeso,
                                         precoVigente =
                                             p.produtoPrecoPromocional > 0 ? p.produtoPrecoPromocional : p.produtoPreco,
                                         porcentagemDesconto = 100 - ((((p.produtoPrecoPromocional > 0 ? p.produtoPrecoPromocional : p.produtoPreco) / 100) * 85) * 100 / p.produtoPreco),
                                         totalVendaCombo = combos.Count() == 0 ? 0 : (combos.Sum(x => (decimal)x.tbProduto.produtoPrecoPromocional > 0 ? (decimal)x.tbProduto.produtoPrecoPromocional : x.tbProduto.produtoPreco))
                                     }).ToList<meusprodutoscategoriaespecifica>();
                        }
                    }
                    if (rbtPESA.Checked)
                    {
                        // Produtos especificos só ativos:

                        using (
                            var txn =
                                new System.Transactions.TransactionScope(
                                    System.Transactions.TransactionScopeOption.Required,
                                    new System.Transactions.TransactionOptions
                                    {
                                        IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                                    }))

                        {
                            dados = (from p in data.tbProdutos
                                     join combo in data.tbProdutoRelacionados on p.produtoId equals combo.idProdutoPai into combos
                                     join jc in data.tbJuncaoProdutoCategorias on p.produtoId equals jc.produtoId
                                     join cat in data.tbProdutoCategorias on jc.categoriaId equals cat.categoriaId
                                     where cat.categoriaId == categoria &&
                                           p.produtoAtivo == "True"
                                     select new meusprodutoscategoriaespecifica
                                     {
                                         produtoId = p.produtoId,
                                         produtoIdDaEmpresa = p.produtoIdDaEmpresa,
                                         produtoNome = p.produtoNome,
                                         produtoPreco = p.produtoPreco,
                                         produtoPrecoDeCusto = p.produtoPrecoDeCusto,
                                         produtoPrecoPromocional = p.produtoPrecoPromocional,
                                         margemDeLucro = p.margemDeLucro,
                                         produtoFornecedor = p.produtoFornecedor,
                                         produtoAtivo = p.produtoAtivo,
                                         produtoDataDaCriacao = p.produtoDataDaCriacao,
                                         complementoIdDaEmpresa = p.complementoIdDaEmpresa,
                                         incluidoDeCombo = "False",
                                         produtoPeso = p.produtoPeso,
                                         precoVigente =
                                             p.produtoPrecoPromocional > 0 ? p.produtoPrecoPromocional : p.produtoPreco,
                                         porcentagemDesconto = 100 - ((((p.produtoPrecoPromocional > 0 ? p.produtoPrecoPromocional : p.produtoPreco) / 100) * 85) * 100 / p.produtoPreco),
                                         totalVendaCombo = combos.Count() == 0 ? 0 : (combos.Sum(x => (decimal)x.tbProduto.produtoPrecoPromocional > 0 ? (decimal)x.tbProduto.produtoPrecoPromocional : x.tbProduto.produtoPreco))
                                     }).ToList<meusprodutoscategoriaespecifica>();
                        }
                    }
                    if (rbtPET.Checked)
                    {
                        // Produtos especificos ativos e inativos:

                        using (
                            var txn =
                                new System.Transactions.TransactionScope(
                                    System.Transactions.TransactionScopeOption.Required,
                                    new System.Transactions.TransactionOptions
                                    {
                                        IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                                    }))

                        {
                            dados = (from p in data.tbProdutos
                                     join combo in data.tbProdutoRelacionados on p.produtoId equals combo.idProdutoPai into combos
                                     join jc in data.tbJuncaoProdutoCategorias on p.produtoId equals jc.produtoId
                                     join cat in data.tbProdutoCategorias on jc.categoriaId equals cat.categoriaId
                                     where cat.categoriaId == categoria
                                     select new meusprodutoscategoriaespecifica
                                     {
                                         produtoId = p.produtoId,
                                         produtoIdDaEmpresa = p.produtoIdDaEmpresa,
                                         produtoNome = p.produtoNome,
                                         produtoPreco = p.produtoPreco,
                                         produtoPrecoDeCusto = p.produtoPrecoDeCusto,
                                         produtoPrecoPromocional = p.produtoPrecoPromocional,
                                         margemDeLucro = p.margemDeLucro,
                                         produtoFornecedor = p.produtoFornecedor,
                                         produtoAtivo = p.produtoAtivo,
                                         produtoDataDaCriacao = p.produtoDataDaCriacao,
                                         complementoIdDaEmpresa = p.complementoIdDaEmpresa,
                                         incluidoDeCombo = "False",
                                         produtoPeso = p.produtoPeso,
                                         precoVigente =
                                             p.produtoPrecoPromocional > 0 ? p.produtoPrecoPromocional : p.produtoPreco,
                                         porcentagemDesconto = 100 - ((((p.produtoPrecoPromocional > 0 ? p.produtoPrecoPromocional : (p.produtoPreco == 0 ? 1 : p.produtoPreco) / 100) * 85) * 100 / (p.produtoPreco == 0 ? 1 : p.produtoPreco))),
                                         totalVendaCombo = combos.Count() == 0 ? 0 : (combos.Sum(x => (decimal)x.tbProduto.produtoPrecoPromocional > 0 ? (decimal)x.tbProduto.produtoPrecoPromocional : x.tbProduto.produtoPreco))

                                     }).ToList<meusprodutoscategoriaespecifica>();
                        }
                    }
                    // Produtos especificos só ativos:
                    //if (rbtPESA.Checked)
                    //{
                    //dados = tbProdutosAtivos(categoria);
                    //}

                    int linha = 2;
                    foreach (var item in dados)
                    {
                        sl.SetCellValue(linha, 1, item.produtoId);
                        sl.SetCellValue(linha, 2, item.produtoIdDaEmpresa);
                        sl.SetCellValue(linha, 3, item.produtoNome);
                        sl.SetCellValue(linha, 4, item.produtoPreco);
                        sl.SetCellValue(linha, 5, item.produtoPrecoDeCusto.ToString());
                        sl.SetCellValue(linha, 6, item.produtoPrecoPromocional.ToString());
                        sl.SetCellValue(linha, 7, item.margemDeLucro);
                        sl.SetCellValue(linha, 8, item.produtoFornecedor);
                        sl.SetCellValue(linha, 9, item.produtoAtivo);
                        sl.SetCellValue(linha, 10, item.produtoDataDaCriacao.ToString());
                        sl.SetCellValue(linha, 11, item.complementoIdDaEmpresa);
                        sl.SetCellValue(linha, 12, item.incluidoDeCombo);
                        sl.SetCellValue(linha, 13, item.produtoPeso);
                        sl.SetCellValue(linha, 14, item.precoVigente.ToString());
                        sl.SetCellValue(linha, 15, item.porcentagemDesconto.ToString());
                        sl.SetCellValue(linha, 16, item.totalVendaCombo);
                        linha++;
                    }
                }
            }

            //Exporta 
            sl.SaveAs(Response.OutputStream);
            Response.End();
        }
        catch (Exception ex)
        {

            throw;
        }
    }
}