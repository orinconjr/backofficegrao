﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="gerarNotasDoDia.aspx.cs" Inherits="admin_gerarNotasDoDia" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxEditors" Assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" Runat="Server">
    <table cellpadding="0" cellspacing="0" style="width: 920px">
        <tr>
            <td class="tituloPaginas" valign="top">
                <asp:Label ID="lblAcao" runat="server">Gerar Notas Fiscais</asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel runat="server" ID="panelSelecionarData">
                    <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
                        <tr class="rotulos">
                            <td style="padding-top: 10px;">
                                <asp:Button runat="server" ID="btnMostrarGerarNotas" OnClick="btnMostrarGerarNotas_OnClick" Text="Gerar Notas" Visible="True"/>
                                <asp:Button runat="server" ID="btnEnviarPDFNotas" OnClick="btnEnviarPDFNotas_OnClick" Text="Enviar PDF das notas Geradas" Visible="False"/>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Panel runat="server" ID="pnGerarNota" Visible="False">
                    <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
                        <tr class="rotulos">
                            <td>
                                Total de pedidos que serão gerados: <asp:Literal runat="server" ID="litTotalDePedidosSeraoGerados"></asp:Literal><br/>
                            </td>
                        </tr>
                        <tr style="font-weight: bold;" class="rotulos">
                            <td style="padding-top: 20px;">
                                Lista de Pedidos
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-top: 20px;">
                                <table>
                                    <asp:ListView runat="server" ID="lstPedidos" OnItemDataBound="lstPedidos_OnItemDataBound">
                                        <ItemTemplate>
                                            <tr class="rotulos">
                                                <td style="padding-right: 30px;">
                                                    <asp:HiddenField runat="server" Value='<%#Eval("pedidoId") %>' ID="hiddenPedidoId"/>
                                                    <asp:HiddenField runat="server" Value='<%#Eval("endCep") %>' ID="hiddenCep"/>
                                                    <b>Pedido:</b><br/>
                                                    <%#Eval("pedidoId") %>
                                                </td>
                                                <td style="padding-right: 30px;">
                                                    <b>Estado:</b><br/>
                                                    <%#Eval("endEstado") %>
                                                </td>
                                                <td style="padding-right: 30px;">
                                                    <b>Cidade:</b><br/>
                                                    <%#Eval("endCidade") %>
                                                </td>
                                                <td style="padding-right: 30px;">
                                                    <b>CEP:</b><br/>
                                                    <%#Eval("endCep") %>
                                                </td>
                                                <td>
                                                    <b>Código IBGE:</b><br/>
                                                    <asp:TextBox ID="txtCodigoIbge" runat="server" CssClass="campos"  Width="90px"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                                                                ControlToValidate="txtCodigoIbge" Display="None" 
                                                                                ErrorMessage="Preencha o código do IBGE." SetFocusOnError="True" ValidationGroup="notaFiscal"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:ListView>
                                </table>
                            </td>
                        </tr>
                        <tr style="font-weight: bold;" class="rotulos">
                            <td style="padding-top: 20px;">
                                Lista de Produtos
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-top: 20px;">
                                <table>
                                    <asp:ListView runat="server" ID="lstProdutos">
                                        <ItemTemplate>
                                            <tr class="rotulos">
                                                <td style="padding-right: 30px;">
                                                    <asp:HiddenField runat="server" Value='<%#Eval("produtoId") %>' ID="hiddenProdutoId"/>
                                                    <b>ID:</b><br/>
                                                    <%#Eval("produtoId") %>
                                                </td>
                                                <td style="padding-right: 30px;">
                                                    <b>Produto:</b><br/>
                                                    <%#Eval("produtoNome") %>
                                                </td>
                                                <td>
                                                    <b>NCM:</b><br/>
                                                    <asp:TextBox ID="txtNCM" runat="server" CssClass="campos"  Width="90px" Text='<%# Eval("ncm") %>'></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                                                                ControlToValidate="txtNCM" Display="None" 
                                                                                ErrorMessage="Preencha o NCM." SetFocusOnError="True" ValidationGroup="notaFiscal"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:ListView>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-top: 30px; padding-bottom: 50px;">
                                <asp:Button runat="server" ID="btnGerarNotas" Text="Gerar notas fiscais" OnClick="btnGerarNotas_OnClick" ValidationGroup="notaFiscal" />
                                <asp:ValidationSummary ID="vlds" runat="server" ShowMessageBox="True" ShowSummary="False" DisplayMode="List" ValidationGroup="notaFiscal" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
            
            </td>
        </tr>
</table>
</asp:Content>
