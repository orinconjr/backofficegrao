﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="gerarNewsLetter.aspx.cs" Inherits="admin_gerarNewsLetter" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" runat="Server">
    <div>

       <table style="width: 920px" cellpadding="0" cellspacing="0">
           <tr>
               <td class="tituloPaginas" valign="top">
                   <span id="ctl00_conteudoPrincipal_lblAcao">Criar Página de NewsLetter</span>
               </td>
           </tr>
           <tr>
               <td>
                   <table style="width: 834px" align="center" cellpadding="0" cellspacing="0">
						<tr class="rotulos">
                           <td colspan="2">
                               Texto da descrição
                               <br>
                                <asp:TextBox ID="txtdescricaoCampanha" CssClass="campos" Width="837px" runat="server"></asp:TextBox>
                            </td>
                       </tr>
                       <tr class="rotulos">
                           <td colspan="2">
                               Nome da Campanha
                               <br>
                                <asp:TextBox ID="txtnomeCampanha" CssClass="campos" Width="837px" runat="server"></asp:TextBox>
                            </td>
                       </tr>
                       <tr class="rotulos">
                           <td colspan="2">
                               IDs produtos Destaque
                               <br>
                                <asp:TextBox ID="txtprodutosdestaque" CssClass="campos" Width="837px" runat="server"></asp:TextBox>
                            </td>
                       </tr>
                       <tr class="rotulos" >
                           <td colspan="2" style="padding-top: 10px;">
                               IDs produtos Veja Também
                               <br>
                               <asp:TextBox ID="txtprodutosvejatambem"  CssClass="campos" Width="837px" runat="server"></asp:TextBox>
                           </td>
                       </tr>
                       <tr class="rotulos">
                           <td colspan="2" style="padding-top: 10px;">
                               Banner
                               <br>
                             <asp:FileUpload ID="flupFoto"    runat="server" /><br/><br/>
                           </td>
                       </tr>
                       <tr class="rotulos" >
                           <td colspan="2" style="padding-top: 10px;">Link do Banner
                               <br>
                               <asp:TextBox ID="txtlinkbanner" CssClass="campos" Width="837px" runat="server"></asp:TextBox>
                           </td>
                       </tr>
                       <tr class="rotulos" style="display: none;">
                           <td colspan="2" style="padding-top: 10px;">Altura do Banner (pixels)
                               <br>
                               <asp:TextBox ID="txtalturabanner" CssClass="campos" Width="130px" runat="server"></asp:TextBox>
                           </td>
                       </tr>
                       <tr class="rotulos" >
                           <td colspan="2" style="padding-top: 10px;">Selo
                               <br>
                             <asp:FileUpload ID="flupSelo"    runat="server" /><br/><br/>
                           </td>
                       </tr>
                        <tr class="rotulos">
                           <td colspan="2" align="center">
                               <br/><br/><br/>
                              <asp:Button ID="btnSalvar" runat="server" Text="Salvar" OnClick="btnSalvar_Click" />
                           </td>
                       </tr>
                       <tr class="rotulos">
                           <td colspan="2" align="center">
                               <br/><br/><br/>
                               <asp:Literal ID="ltmensagem" runat="server"></asp:Literal>
                           </td>
                       </tr>
                   </table>
               </td>
           </tr>
       </table>
     
       
    </div>
</asp:Content>
