﻿<%@ Page Title="" Theme="Glass" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="produtosReservadoCD2.aspx.cs" Inherits="admin_produtosReservadoCD2" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxTimer" TagPrefix="dxt" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" Runat="Server">
    
    <table cellpadding="0" cellspacing="0" style="width: 920px">
    <tr>
        <td class="tituloPaginas" valign="top">
            <asp:Label ID="lblAcao" runat="server">Produtos Reservados CD2</asp:Label>
        </td>
    </tr>
        
    <tr>
        <td>
            
    <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
        <tr>
            <td class="rotulos">
                Pedidos Não Completos
            </td>
        </tr>
        <tr>
            <td>
                <dxwgv:ASPxGridView ID="grd" runat="server" AutoGenerateColumns="False" Width="834px" Cursor="auto" OnHtmlRowCreated="gridClientes_HtmlRowCreated"
                    OnCustomUnboundColumnData="grd_OnCustomUnboundColumnData" KeyFieldName="pedidoId">
                    <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                    <Styles>
                        <Footer Font-Bold="True">
                        </Footer>
                    </Styles>
                    <SettingsText ConfirmDelete="Tem certeza que deseja excluir?" emptydatarow="Nenhum registro encontrado." />
                    <SettingsPager Position="TopAndBottom" PageSize="500" ShowDisabledButtons="False" AlwaysShowPager="True">
                        <Summary AllPagesText="Páginas: {0} - {1} ({2} items)" Text="Página {0} de {1} ({2} registros encontrados)" />
                    </SettingsPager>
                    <settings showfilterrow="True" ShowGroupButtons="False" ShowHeaderFilterButton="True" ShowFooter="True" />
                    <SettingsEditing EditFormColumnCount="4" PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1" PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter" PopupEditFormWidth="700px" />
                    <Columns>
                        <dxwgv:GridViewDataTextColumn Caption="#" VisibleIndex="0" Width="50">
                            <DataItemTemplate>
                                <%# Container.VisibleIndex + 1 %>
                            </DataItemTemplate>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="ID" FieldName="pedidoId" VisibleIndex="0" Width="50">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="ID" FieldName="pedidoIdCliente" VisibleIndex="0" UnboundType="Integer">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Romaneio" FieldName="idPedidoFornecedor" VisibleIndex="0">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Produto" FieldName="produtoNome" VisibleIndex="0">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Etiqueta" FieldName="idPedidoFornecedorItem" VisibleIndex="0">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataDateColumn Caption="Data Limite" FieldName="dataLimite" VisibleIndex="0">
                        </dxwgv:GridViewDataDateColumn>
                        <dxwgv:GridViewDataHyperLinkColumn Caption="Editar" FieldName="pedidoId" VisibleIndex="7" Width="30px">
                            <PropertiesHyperLinkEdit ImageUrl="~/admin/images/btEditar.jpg" NavigateUrlFormatString="pedido.aspx?pedidoId={0}" Target="_blank">
                            </PropertiesHyperLinkEdit>
                            <Settings AllowAutoFilter="False" />
                            <HeaderTemplate>
                                <img alt="" src="images/legendaEditar.jpg" />
                            </HeaderTemplate>
                        </dxwgv:GridViewDataHyperLinkColumn>
                    </Columns>
                    <StylesEditors>
                        <Label Font-Bold="True">
                        </Label>
                    </StylesEditors>
                </dxwgv:ASPxGridView>        
            </td>
        </tr>
        <tr>
            <td class="rotulos" style="padding-top: 30px;">
                Pedidos  Completos
            </td>
        </tr>
        <tr>
            <td>
                <dxwgv:ASPxGridView ID="grdCompletos" runat="server" AutoGenerateColumns="False" Width="834px" Cursor="auto" OnHtmlRowCreated="gridClientes_HtmlRowCreated"
                    OnCustomUnboundColumnData="grd_OnCustomUnboundColumnData" KeyFieldName="pedidoId">
                    <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                    <Styles>
                        <Footer Font-Bold="True">
                        </Footer>
                    </Styles>
                    <SettingsText ConfirmDelete="Tem certeza que deseja excluir?" emptydatarow="Nenhum registro encontrado." />
                    <SettingsPager Position="TopAndBottom" PageSize="500" ShowDisabledButtons="False" AlwaysShowPager="True">
                        <Summary AllPagesText="Páginas: {0} - {1} ({2} items)" Text="Página {0} de {1} ({2} registros encontrados)" />
                    </SettingsPager>
                    <settings showfilterrow="True" ShowGroupButtons="False" ShowHeaderFilterButton="True" ShowFooter="True" />
                    <SettingsEditing EditFormColumnCount="4" PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1" PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter" PopupEditFormWidth="700px" />
                    <Columns>
                        <dxwgv:GridViewDataTextColumn Caption="#" VisibleIndex="0" Width="50">
                            <DataItemTemplate>
                                <%# Container.VisibleIndex + 1 %>
                            </DataItemTemplate>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="pedidoId" FieldName="pedidoId" VisibleIndex="0" Width="50">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="ID" FieldName="pedidoIdCliente" VisibleIndex="0" UnboundType="Integer">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Romaneio" FieldName="idPedidoFornecedor" VisibleIndex="0">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Produto" FieldName="produtoNome" VisibleIndex="0">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Etiqueta" FieldName="idPedidoFornecedorItem" VisibleIndex="0">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataDateColumn Caption="Data Limite" FieldName="dataLimite" VisibleIndex="0">
                        </dxwgv:GridViewDataDateColumn>
                        <dxwgv:GridViewDataHyperLinkColumn Caption="Editar" FieldName="pedidoId" VisibleIndex="7" Width="30px">
                            <PropertiesHyperLinkEdit ImageUrl="~/admin/images/btEditar.jpg" NavigateUrlFormatString="pedido.aspx?pedidoId={0}" Target="_blank">
                            </PropertiesHyperLinkEdit>
                            <Settings AllowAutoFilter="False" />
                            <HeaderTemplate>
                                <img alt="" src="images/legendaEditar.jpg" />
                            </HeaderTemplate>
                        </dxwgv:GridViewDataHyperLinkColumn>
                    </Columns>
                    <StylesEditors>
                        <Label Font-Bold="True">
                        </Label>
                    </StylesEditors>
                </dxwgv:ASPxGridView>        
            </td>
        </tr>
        <tr>
            <td class="rotulos" style="padding-top: 30px;">
                Pedidos Aguardando Emissão 
            </td>
        </tr>
        <tr>
            <td>
                <dxwgv:ASPxGridView ID="grdAguardandoEmissao" runat="server" AutoGenerateColumns="False" Width="834px" Cursor="auto" OnHtmlRowCreated="gridClientes_HtmlRowCreated"
                    OnCustomUnboundColumnData="grd_OnCustomUnboundColumnData" KeyFieldName="pedidoId">
                    <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                    <Styles>
                        <Footer Font-Bold="True">
                        </Footer>
                    </Styles>
                    <SettingsText ConfirmDelete="Tem certeza que deseja excluir?" emptydatarow="Nenhum registro encontrado." />
                    <SettingsPager Position="TopAndBottom" PageSize="500" ShowDisabledButtons="False" AlwaysShowPager="True">
                        <Summary AllPagesText="Páginas: {0} - {1} ({2} items)" Text="Página {0} de {1} ({2} registros encontrados)" />
                    </SettingsPager>
                    <settings showfilterrow="True" ShowGroupButtons="False" ShowHeaderFilterButton="True" ShowFooter="True" />
                    <SettingsEditing EditFormColumnCount="4" PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1" PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter" PopupEditFormWidth="700px" />
                    <Columns>
                        <dxwgv:GridViewDataTextColumn Caption="#" VisibleIndex="0" Width="50">
                            <DataItemTemplate>
                                <%# Container.VisibleIndex + 1 %>
                            </DataItemTemplate>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="pedidoId" FieldName="pedidoId" VisibleIndex="0" Width="50">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="ID" FieldName="pedidoIdCliente" VisibleIndex="0" UnboundType="Integer">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Romaneio" FieldName="idPedidoFornecedor" VisibleIndex="0">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Produto" FieldName="produtoNome" VisibleIndex="0">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Etiqueta" FieldName="idPedidoFornecedorItem" VisibleIndex="0">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataDateColumn Caption="Data Limite" FieldName="dataLimite" VisibleIndex="0">
                        </dxwgv:GridViewDataDateColumn>
                        <dxwgv:GridViewDataHyperLinkColumn Caption="Editar" FieldName="pedidoId" VisibleIndex="7" Width="30px">
                            <PropertiesHyperLinkEdit ImageUrl="~/admin/images/btEditar.jpg" NavigateUrlFormatString="pedido.aspx?pedidoId={0}" Target="_blank">
                            </PropertiesHyperLinkEdit>
                            <Settings AllowAutoFilter="False" />
                            <HeaderTemplate>
                                <img alt="" src="images/legendaEditar.jpg" />
                            </HeaderTemplate>
                        </dxwgv:GridViewDataHyperLinkColumn>
                    </Columns>
                    <StylesEditors>
                        <Label Font-Bold="True">
                        </Label>
                    </StylesEditors>
                </dxwgv:ASPxGridView>        
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td style="text-align: right;">

            </td>
        </tr>
    </table>
        </td>
    </tr>
</table>
    </asp:Content>