﻿<%@ Page Title="" Theme="Glass" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="comprasCondicaoPagamentoLista.aspx.cs" Inherits="admin_comprasCondicaoPagamentoLista" %>

<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v11.1.Linq, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Data.Linq" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" runat="Server">

    <table cellpadding="0" cellspacing="0" style="width: 920px">
        <tr>
            <td class="tituloPaginas" valign="top">
                <asp:Label ID="lblAcao" runat="server">Compras - Condições de Pagamento</asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                       
                        <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">

                            <tr>
                                <td align="right">
                                    <table cellpadding="0" cellspacing="0" style="width: 834px">
                                        <tr>
                                            <td align="right">&nbsp;</td>
                                            <td height="38"
                                                style="background-image: url('images/bkExportar.jpg'); padding-bottom: 1px;"
                                                valign="bottom" width="231">
                                                <table cellpadding="0" cellspacing="0" style="2">
                                                    <tr>
                                                        <td style="width: 97px">&nbsp;</td>
                                                        <td style="width: 33px">
                                                            <asp:ImageButton ID="btPdf" runat="server" ImageUrl="~/admin/images/btPdf.jpg" Style="margin-left: 5px" OnClick="btPdf_Click" />
                                                        </td>
                                                        <td style="width: 31px">
                                                            <asp:ImageButton ID="btXsl" runat="server" ImageUrl="~/admin/images/btXls.jpg" Style="margin-left: 5px" OnClick="btXsl_Click" />
                                                        </td>
                                                        <td style="width: 36px">
                                                            <asp:ImageButton ID="btRtf" runat="server" ImageUrl="~/admin/images/btRtf.jpg" Style="margin-left: 5px" OnClick="btRtf_Click" />
                                                        </td>
                                                        <td valign="bottom">
                                                            <asp:ImageButton ID="btCsv" runat="server" ImageUrl="~/admin/images/btCsv.jpg" Style="margin-left: 5px" OnClick="btCsv_Click" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="rotulos"></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                        <dxwgv:ASPxGridView ID="grd" runat="server" AutoGenerateColumns="False" KeyFieldName="idComprasCondicaoPagamento" Cursor="auto" ClientInstanceName="grd" EnableCallBacks="False">
                                            <SettingsBehavior AutoFilterRowInputDelay="10200" ConfirmDelete="True" AutoExpandAllGroups="True" ColumnResizeMode="Control" AllowGroup="False" AllowFocusedRow="True" />
                                            <SettingsText ConfirmDelete="Tem certeza que deseja excluir?" EmptyDataRow="Nenhum registro encontrado." GroupPanel="Arraste uma coluna aqui para agrupar." />
                                            <SettingsPager Position="TopAndBottom" PageSize="30" ShowDisabledButtons="False" AlwaysShowPager="True">
                                                <Summary AllPagesText="Páginas: {0} - {1} ({2} items)" Text="Página {0} de {1} ({2} registros encontrados)" />
                                            </SettingsPager>
                                            <Settings ShowFilterRow="True" ShowGroupedColumns="True" ShowFooter="True" />
                                            <SettingsEditing EditFormColumnCount="4"
                                                PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1"
                                                PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter"
                                                PopupEditFormWidth="700px" Mode="Inline" />
                                            <Columns>
                                                <dxwgv:GridViewDataTextColumn Caption="ID Produto" FieldName="idComprasCondicaoPagamento" Name="idComprasCondicaoPagamento" VisibleIndex="0" Visible="false" Width="100px">
                                                    <Settings AutoFilterCondition="Contains" />
                                                </dxwgv:GridViewDataTextColumn>
                                                 <dxwgv:GridViewDataTextColumn Caption="Condição" FieldName="nome" Name="nome" VisibleIndex="3">
                                                </dxwgv:GridViewDataTextColumn>
                                                <dxwgv:GridViewDataHyperLinkColumn Caption="Novo" VisibleIndex="0" Width="37px">
                                                    <PropertiesHyperLinkEdit ImageUrl="~/admin/images/btNovo.jpg" NavigateUrlFormatString="produtoCad.aspx">
                                                    </PropertiesHyperLinkEdit>
                                                    <DataItemTemplate>
                                                        <asp:HyperLink ID="hplNovo" runat="server" ImageUrl="images/btNovo.jpg" ToolTip="Novo" NavigateUrl="comprasCondicaoPagamentoCadastro.aspx">Novo</asp:HyperLink>
                                                    </DataItemTemplate>
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dxwgv:GridViewDataHyperLinkColumn>
                                                <dxwgv:GridViewDataHyperLinkColumn Caption="Alterar" VisibleIndex="1" Width="45px">
                                                    <DataItemTemplate>
                                                        <asp:HyperLink ID="HyperLink1" runat="server" ImageUrl="images/btEditar.jpg" ToolTip="Alterar" NavigateUrl='<%# "comprasCondicaoPagamentoCadastro.aspx?id=" + Eval("idComprasCondicaoPagamento") %>'>Alterar</asp:HyperLink>
                                                          <asp:HiddenField ID="hfIdBanner" runat="server" Value='<%# Eval("idComprasCondicaoPagamento") %>' />
                                                    </DataItemTemplate>
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dxwgv:GridViewDataHyperLinkColumn>
                                            </Columns>
                                        </dxwgv:ASPxGridView>
                                    <dxwgv:ASPxGridViewExporter ID="grdEx" runat="server" FileName="fornecedores" GridViewID="grd" Landscape="True" PreserveGroupRowStates="True">
                                    </dxwgv:ASPxGridViewExporter>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                            </tr>
                        </table>

            </td>
        </tr>
    </table>
</asp:Content>
