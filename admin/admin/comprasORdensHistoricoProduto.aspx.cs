﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_comprasORdensHistoricoProduto : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        FillGrid(false);
    }

    protected void btnPesquisar_Click(object sender, ImageClickEventArgs e)
    {
        FillGrid(true);
    }

    private void FillGrid(bool rebind)
    {
        var data = new dbCommerceDataContext();
        var pedidos = (from c in data.tbComprasOrdems
                       join d in data.tbComprasOrdemProdutos on c.idComprasOrdem equals d.idComprasOrdem
                       join f in data.tbComprasProdutoFornecedors on d.idComprasProduto equals f.idComprasProduto
                       select new
                       {
                           c.idComprasOrdem,
                           c.dataCriacao,
                           c.idUsuarioCadastroOrdem,
                           c.idComprasEmpresa,
                           c.idComprasFornecedor,
                           quantidade = (d.quantidadeRecebida ?? d.quantidade),
                           valor = (d.precoRecebido ?? d.preco),
                           valorTotal = (d.precoRecebido ?? d.preco) * (d.quantidadeRecebida ?? d.quantidade),
                           c.dataPrevista,
                           c.icPago,
                           status = c.statusOrdemCompra == 1 ? "Aguardando Aprovação" : c.statusOrdemCompra == 2 ? "Aguardando Fechamento" : c.statusOrdemCompra == 3 ? "Aguardando Lançamento Financeiro" : c.statusOrdemCompra == 4 ? "Finalizado" : c.statusOrdemCompra == 5 ? "Cancelado" : c.statusOrdemCompra == 6 ? "Aguardando Conferência de Entrega" : "Pendente",
                           statusProduto = d.status == 1 ? "Aprovado" : "Reprovado",
                           d.tbComprasProduto.produto,
                           f.tbComprasUnidadesDeMedida.unidadeDeMedida
                       }).OrderByDescending(x => x.dataCriacao);
        grd.DataSource = pedidos;
        if ((!Page.IsPostBack && !Page.IsCallback) | rebind) grd.DataBind();
    }

    protected void btPdf_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WritePdfToResponse();
    }
    protected void btXsl_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteXlsToResponse();
    }
    protected void btRtf_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteRtfToResponse();
    }
    protected void btCsv_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteCsvToResponse();
    }

    protected void btnExportar_OnCommand(object sender, CommandEventArgs e)
    {
        Response.Redirect("comprasOrdemDetalhe.aspx?id=" + e.CommandArgument);
    }
}