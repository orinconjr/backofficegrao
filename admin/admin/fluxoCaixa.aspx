﻿<%@ Page Title="" Theme="Glass" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="fluxoCaixa.aspx.cs" Inherits="admin_fluxoCaixa" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.ASPxPivotGrid.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPivotGrid" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" Runat="Server">
    <table cellpadding="0" cellspacing="0" style="width: 920px">
        <tr>
            <td class="tituloPaginas" valign="top">
                <asp:Label ID="lblAcao" runat="server">Fluxo de Caixa</asp:Label>
            </td>
        </tr>
        <tr>
            <td>
            
        <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
            <tr>
                <td align="right">
                    <table cellpadding="0" cellspacing="0" style="width: 834px">
                        <tr>
            <td>
                <table cellpadding="0" cellspacing="0" style="width: 834px">
                    <tr>
                        <td class="rotulos" style="width: 100px">
                            Data inicial<br />
                                        <asp:TextBox ID="txtDataInicial" runat="server" CssClass="campos" 
                                                            Text='<%# Bind("faixaDeCepPesoInicial") %>' 
                                                            Width="90px" MaxLength="10"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rqvDataInicial" runat="server" 
                                                            ControlToValidate="txtDataInicial" Display="None" 
                                                            ErrorMessage="Preencha a data inicial." 
                                SetFocusOnError="True"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="rgeDataInicial" runat="server" 
                                            ControlToValidate="txtDataInicial" Display="None" 
                                            ErrorMessage="Por favor, preencha corretamente a data inicial." 
                                            SetFocusOnError="True" 
                                            
                                            
                                ValidationExpression="^(([0-2]\d|[3][0-1])\/([0]\d|[1][0-2])\/[1-2][0-9]\d{2})$"></asp:RegularExpressionValidator>
                            
                        </td>
                        <td class="rotulos" style="width: 100px">
                            Data final<br  />
                            <asp:TextBox ID="txtDataFinal" runat="server" CssClass="campos" 
                                                Text='<%# Bind("faixaDeCepPesoFinal") %>' ValidationGroup="grpInsert" 
                                                Width="90px" MaxLength="10"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rqvDataFinal" runat="server" 
                                                ControlToValidate="txtDataFinal" Display="None" 
                                                ErrorMessage="Preencha a data final." 
                                SetFocusOnError="True"></asp:RequiredFieldValidator>
                            <b>
                            <asp:RegularExpressionValidator ID="rgeDataInicial0" runat="server" 
                                            ControlToValidate="txtDataFinal" Display="None" 
                                            ErrorMessage="Por favor, preencha corretamente a data final" 
                                            SetFocusOnError="True" 
                                            
                                            
                                ValidationExpression="^(([0-2]\d|[3][0-1])\/([0]\d|[1][0-2])\/[1-2][0-9]\d{2})$"></asp:RegularExpressionValidator>
                            </b>
                        </td>
                        <td valign="bottom">
                            <asp:ImageButton ID="imbInsert" runat="server" ImageUrl="~/admin/images/btPesquisar.jpg" OnClick="imbInsert_OnClick" />
                            <asp:ValidationSummary ID="llds" runat="server" DisplayMode="List" 
                                ShowMessageBox="True" ShowSummary="False" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
                        <tr>
                            <td align="right">
                                &nbsp;</td>
                            <td height="38" 
                                style="background-image: url('images/bkExportar.jpg'); padding-bottom: 1px;" 
                                valign="bottom" width="231">
                                <%--<table cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td style="width: 97px">
                                            &nbsp;</td>
                                        <td style="width: 33px">
                                            <asp:ImageButton ID="btPdf" runat="server" ImageUrl="~/admin/images/btPdf.jpg"  OnClick="btPdf_Click"
                                                style="margin-left: 5px" />
                                        </td>
                                        <td style="width: 31px">
                                            <asp:ImageButton ID="btXsl" runat="server" ImageUrl="~/admin/images/btXls.jpg" OnClick="btXsl_Click"
                                                style="margin-left: 5px" />
                                        </td>
                                        <td style="width: 36px">
                                            <asp:ImageButton ID="btRtf" runat="server" ImageUrl="~/admin/images/btRtf.jpg" OnClick="btRtf_Click"
                                                style="margin-left: 5px" />
                                        </td>
                                        <td valign="bottom">
                                            <asp:ImageButton ID="btCsv" runat="server" ImageUrl="~/admin/images/btCsv.jpg" OnClick="btCsv_Click"
                                                style="margin-left: 5px" />
                                        </td>
                                    </tr>
                                </table>--%>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <dx:ASPxPivotGrid OnCustomCellValue="grd_OnCustomCellValue" ID="grd" runat="server" CustomizationFieldsLeft="600" CustomizationFieldsTop="400" ClientInstanceName="pivotGrid" Width="100%">
                        <Fields>
                            <dx:PivotGridField Area="DataArea" FieldName="valor" Caption="Valor" ID="Valor"  />
                            <dx:PivotGridField Area="RowArea" FieldName="agrupador" Caption="Descricao" ID="Agrupador" RunningTotal="True" />
                            <dx:PivotGridField Area="FilterArea" FieldName="agrupador" Caption="Descricao" ID="AgrupadorFilter" />
                            <dx:PivotGridField Area="ColumnArea" FieldName="dataVencimento" Caption="Dia" ID="DataVencimento" GroupInterval="Date"  />
                        </Fields>
                        <OptionsView ShowFilterHeaders="False" ShowHorizontalScrollBar="True" />
                    </dx:ASPxPivotGrid>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
            </td>
        </tr>
    </table>
</asp:Content>