﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_alterarValorItemPedido : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnSalvar_Click(object sender, EventArgs e)
    {
        string mensagem = validaCampos();
        if (mensagem != "")
        {
            Response.Write("<script>alert('" + mensagem + "');</script>");
            return;
        }

        int idpedido = Convert.ToInt32(txtPedidoId.Text);
        int itemPedidoId = Convert.ToInt32(txtitemPedidoId.Text);
        decimal ItemValor = Convert.ToDecimal(txtItemValor.Text);
        var data = new dbCommerceDataContext();
        var itemPedido = (from ip in data.tbItensPedidos
                          where ip.pedidoId == idpedido && ip.itemPedidoId == itemPedidoId
                          select ip).FirstOrDefault();


        if (itemPedido != null)
        {
            try
            {
                itemPedido.itemValor = ItemValor;
                data.SubmitChanges();
                mensagem = "Alteração salva com sucesso.";
            }
            catch (Exception ex)
            {
                mensagem = ex.Message;
            }
        }
        else
        {
            mensagem = "Item Não Encontrado";
        }
        Response.Write("<script>alert('" + mensagem + "');</script>");
    }

    string validaCampos()
    {
        string retorno = "";

        if (String.IsNullOrEmpty(txtPedidoId.Text.Trim()))
        {
            retorno += "Informe o id do pedido. ";
        }
        else
        {
            int id = 0;
            try { id = Convert.ToInt32(txtPedidoId.Text.Trim()); } catch { }
            if(id==0)
                retorno += "Id de pedido inválido. ";
        }

        if (String.IsNullOrEmpty(txtitemPedidoId.Text.Trim()))
        {
            retorno += "Informe o id do Item. ";
        }
        else
        {
            int id = 0;
            try { id = Convert.ToInt32(txtitemPedidoId.Text.Trim()); } catch { }
            if (id == 0)
                retorno += "Id do item inválido. ";
        }

        if (String.IsNullOrEmpty(txtItemValor.Text.Trim()))
        {
            retorno += "Informe o valor do Item. ";
        }
        else
        {
            decimal id = 0;
            try { id = Convert.ToDecimal(txtItemValor.Text.Trim()); } catch { }
            if (id == 0)
                retorno += "Valor do item inválido. ";
        }
        return retorno;
    }
}
