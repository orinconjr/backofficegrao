﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using CarlosAg.ExcelXmlWriter;
using DevExpress.Utils.OAuth;
using FtpLib;
using HtmlAgilityPack;
using Newtonsoft.Json;
using RestSharp;

public partial class admin_gerarIntegracoes : System.Web.UI.Page
{
    private static double progress;
    private static string servico;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            chkRakuten.Enabled = rnIntegracoes.habilitarRakuten;
            chkExtra.Enabled = rnIntegracoes.habilitarExtra;
            chkRakutenIgnorarFotos.Enabled = true;
            var integracoesDc = new dbCommerceDataContext();
            var extraPendente = (from c in integracoesDc.tbExtraImportacaos where c.status < 3 select c).FirstOrDefault();
            if (extraPendente != null)
            {
                int statusPentende = rnIntegracoes.consultaStatusImportacaoExtra(extraPendente.importerInfoId, extraPendente.status, extraPendente.extraImportacaoId);
                if (statusPentende < 3)
                {
                    litImportacoesExtraPendentes.Text = "SIM";
                    chkExtra.Enabled = false;
                    chkExtra.Checked = false;
                }

            }
            else
            {
                litImportacoesExtraPendentes.Text = "Não";
            }
        }
        //http://www.devexpress.com/Support/Center/Example/Details/E918
    }
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.ASPxCallback.CallbackEventArgs e)
    {
        enviarProdutos(chkRakuten.Checked, chkExtra.Checked);
    }

    protected void ASPxCallback2_Callback(object source, DevExpress.Web.ASPxCallback.CallbackEventArgs e)
    {
        e.Result = progress.ToString("0.0000").Replace(",", ".");
    }


    private void enviarProdutos(bool rakuten, bool extra)
    {
        HttpContext.Current.Server.ScriptTimeout = 60000;
        if (rakuten && rnIntegracoes.habilitarRakuten)
        {
            gerarRakuten();
        }
        if (extra && rnIntegracoes.habilitarExtra)
        {
            gerarExtra();
        }
    }

    private void gerarRakuten()
    {
        FileInfo file = new FileInfo(System.Configuration.ConfigurationManager.AppSettings["caminhoFisico"].ToString() + "P-" + rnIntegracoes.rakutenId + ".txt");
        if (file.Exists)
        {
            file.Delete();
        }

        StreamWriter sw = file.CreateText();

        var dataProdutos = new dbCommerceDataContext();
        dataProdutos.CommandTimeout = 0;

        var produtos = (from c in dataProdutos.tbProdutos
                        where c.produtoAtivo == "True"
                        && c.marketplaceCadastrar == "True"
                        select new
                        {
                            c.produtoId,
                            c.produtoPaiId,
                            c.produtoEstoqueAtual,
                            c.produtoEstoqueMinimo,
                            c.produtoUrl,
                            c.produtoNome,
                            c.produtoPrecoPromocional,
                            c.produtoLegendaAtacado,
                            c.produtoPrecoAtacado,
                            c.produtoFreteGratis,
                            c.produtoPromocao,
                            c.produtoLancamento,
                            c.produtoMarca,
                            c.produtoDataDaCriacao,
                            c.produtoPreco,
                            c.produtoIdDaEmpresa,
                            c.produtoComposicao,
                            c.produtoFios,
                            c.produtoPecas,
                            c.produtoBrindes,
                            c.produtoDescricao,
                            c.produtoPeso,
                            c.fotoDestaque,
                            c.produtoPrincipal,
                            c.produtoFornecedor,
                            c.marketplaceEnviarMarca
                        });

        int totalProdutos = produtos.Count();
        int atual = 1;

        foreach (var produto in produtos)
        {
            var descricaoCompleta = new StringBuilder();
            var dataInformacao = new dbCommerceDataContext();
            var informacoesAdicional = (from c in dataInformacao.tbInformacaoAdcionals where c.produtoId == produto.produtoId orderby c.informacaoAdcionalNome descending select c).ToList();
            foreach (var informacaoAdcional in informacoesAdicional)
            {
                descricaoCompleta.AppendFormat("<div class=\"tarjas\">{0}</div>", informacaoAdcional.informacaoAdcionalNome);
                descricaoCompleta.AppendFormat("<div class=\"descricaoInformacoesAdicionais\">{0}</div>", informacaoAdcional.informacaoAdcionalConteudo);
            }
            var dataProd = new dbCommerceDataContext();
            var categoria = (from c in dataProd.tbJuncaoProdutoCategorias
                             where
                                 c.produtoId == produto.produtoId && c.tbProdutoCategoria.exibirSite == true &&
                                 c.tbProdutoCategoria.categoriaPaiId == 0
                             select c).FirstOrDefault();
            if (categoria != null)
            {
                bool erro = false;
                if (chkRakutenIgnorarFotos.Checked == false)
                {
                    using (
                        FtpConnection ftp = new FtpConnection(rnIntegracoes.rakutenFtpFotos,
                                                              rnIntegracoes.rakutenLoginFtpFotos,
                                                              rnIntegracoes.rakutenSenhaFtpFotos))
                    {

                        ftp.Open(); /* Open the FTP connection */
                        ftp.Login(); /* Login using previously provided credentials */

                        try
                        {
                            if (!ftp.DirectoryExists("/" + rnIntegracoes.rakutenId))
                            {
                                ftp.CreateDirectory("/" + rnIntegracoes.rakutenId);
                            }
                            ftp.SetCurrentDirectory("/" + rnIntegracoes.rakutenId); /* change current directory */
                        }
                        catch
                        {
                        }

                        try
                        {
                            if (!ftp.DirectoryExists("/" + rnIntegracoes.rakutenId + "/produtos"))
                            {
                                ftp.CreateDirectory("/" + rnIntegracoes.rakutenId + "/produtos");
                            }
                            ftp.SetCurrentDirectory("/" + rnIntegracoes.rakutenId + "/produtos");
                                /* change current directory */
                        }
                        catch
                        {
                        }

                        try
                        {
                            if (!ftp.DirectoryExists("/" + rnIntegracoes.rakutenId + "/produtos/imagens"))
                            {
                                ftp.CreateDirectory("/" + rnIntegracoes.rakutenId + "/produtos/imagens");
                            }
                            ftp.SetCurrentDirectory("/" + rnIntegracoes.rakutenId + "/produtos/imagens");
                                /* change current directory */
                        }
                        catch
                        {
                        }

                        try
                        {
                            if (
                                !ftp.DirectoryExists("/" + rnIntegracoes.rakutenId + "/produtos/imagens/" +
                                                     produto.produtoId))
                            {
                                ftp.CreateDirectory("/" + rnIntegracoes.rakutenId + "/produtos/imagens/" +
                                                    produto.produtoId);
                            }
                            ftp.SetCurrentDirectory("/" + rnIntegracoes.rakutenId + "/produtos/imagens/" +
                                                    produto.produtoId); /* change current directory */
                        }
                        catch
                        {
                        }

                        try
                        {
                            if (
                                !ftp.FileExists("/" + rnIntegracoes.rakutenId + "/produtos/imagens/" + produto.produtoId +
                                                "/" + produto.fotoDestaque + ".jpg"))
                            {
                                string urlFoto =
                                    System.Configuration.ConfigurationManager.AppSettings["caminhoFisico"].ToString() +
                                    "fotos\\" + produto.produtoId + "\\" + produto.fotoDestaque + ".jpg";
                                ftp.PutFile(urlFoto, produto.fotoDestaque + ".jpg");
                            }
                        }
                        catch (Exception)
                        {
                            erro = true;
                        }
                    }
                }
                if (!erro)
                {
                    var dataFornecedor = new dbCommerceDataContext();
                    var fornecedor =
                        (from c in dataFornecedor.tbProdutoFornecedors
                         where c.fornecedorId == produto.produtoFornecedor
                         select c).First();
                    int prazoAdicional = 0;
                    if (fornecedor.fornecedorPrazoDeFabricacao != null)
                    {
                        prazoAdicional = (int) fornecedor.fornecedorPrazoDeFabricacao;
                    }
                    decimal precoPromocional = produto.produtoPreco;
                    if (produto.produtoPrecoPromocional > 0 && produto.produtoPrecoPromocional < produto.produtoPreco)
                        precoPromocional = (decimal) produto.produtoPrecoPromocional;

                    string produtoDescricao = descricaoCompleta.ToString();
                    if (string.IsNullOrEmpty(produtoDescricao))
                    {
                        produtoDescricao = produto.produtoDescricao.Replace(Environment.NewLine, "<br />");
                        if (string.IsNullOrEmpty(produtoDescricao)) produtoDescricao = produto.produtoNome;
                    }
                    produtoDescricao = produtoDescricao.Replace(Environment.NewLine, "");
                    produtoDescricao = rnIntegracoes.removeLinks(produtoDescricao);
                    
                    StringBuilder produtoLinha = new StringBuilder();
                    produtoLinha.Append("1\t");
                    produtoLinha.AppendFormat("{0}\t", produto.produtoNome);
                    produtoLinha.AppendFormat("{0}\t", produto.produtoId);
                    produtoLinha.Append("1\t");
                    produtoLinha.AppendFormat("{0}\t", produtoDescricao.Replace("\t", " "));
                    produtoLinha.Append("\t");
                    produtoLinha.Append("1\t");
                    produtoLinha.Append("0\t");
                    produtoLinha.Append("50\t");
                    produtoLinha.Append("\t");
                    produtoLinha.AppendFormat("{0}\t", categoria.categoriaId);
                    produtoLinha.AppendFormat("{0}\t", categoria.tbProdutoCategoria.categoriaNomeExibicao);
                    string marca = "";
                    if (produto.marketplaceEnviarMarca == "True")
                    {
                        var marcaDc = new dbCommerceDataContext();
                        var marcaObj =
                            (from c in marcaDc.tbMarcas where c.marcaId == produto.produtoMarca select c).FirstOrDefault
                                ();
                        if (marcaObj != null)
                        {
                            marca = marcaObj.marcaNome;
                        }
                        if (string.IsNullOrEmpty(marca)) marca = rnIntegracoes.marcaPadrao;
                    }
                    else
                    {
                        marca = rnIntegracoes.marcaPadrao;
                    }
                    produtoLinha.AppendFormat("{0}\t", marca);
                    produtoLinha.Append("1.0\t");
                    produtoLinha.AppendFormat("Produtos\\Imagens\\{0}\\{1}.jpg\t", produto.produtoId,
                                              produto.fotoDestaque);
                    produtoLinha.AppendFormat("{0}\t", prazoAdicional);
                    produtoLinha.AppendFormat("{0}", Environment.NewLine);
                    produtoLinha.Append("2\t");
                    produtoLinha.AppendFormat("{0}.1\t", produto.produtoId);
                    produtoLinha.AppendFormat("{0}\t", produto.produtoPreco.ToString("0.00").Replace(",", "."));
                    produtoLinha.AppendFormat("{0}\t", precoPromocional.ToString("0.00").Replace(",", "."));
                    produtoLinha.AppendFormat("{0}\t", 0);
                    produtoLinha.AppendFormat("{0}\t", produto.produtoEstoqueMinimo == null
                                                           ? 0
                                                           : (int) produto.produtoEstoqueMinimo);
                    produtoLinha.AppendFormat("{0}\t", produto.produtoFreteGratis == "True" ? 1 : 0);
                    produtoLinha.AppendFormat("{0}\t",
                                              (Convert.ToDecimal(produto.produtoPeso)/100).ToString("0.00")
                                                                                          .Replace(",", "."));
                    produtoLinha.Append("0\t");
                    produtoLinha.Append("0\t");
                    produtoLinha.Append("0\t");
                    produtoLinha.Append("0\t");
                    produtoLinha.Append("\t");
                    produtoLinha.Append("\t");
                    produtoLinha.Append("\t");
                    produtoLinha.Append("\t");
                    produtoLinha.AppendFormat("Produtos\\Imagens\\{0}\\{1}.jpg\t", produto.produtoId,
                                              produto.fotoDestaque);
                    produtoLinha.Append("\t");
                    sw.WriteLine(produtoLinha.ToString());
                }

            }

            progress = Convert.ToDouble(Convert.ToDecimal(atual)*100/totalProdutos);
            atual++;
        }

        sw.WriteLine();
        sw.Close();
        sw.Dispose();

        using (FtpConnection ftp = new FtpConnection(rnIntegracoes.rakutenFtpProdutos, rnIntegracoes.rakutenLoginFtpProdutos, rnIntegracoes.rakutenSenhaFtpProdutos))
        {

            ftp.Open(); /* Open the FTP connection */
            ftp.Login(); /* Login using previously provided credentials */
            string urlFoto = System.Configuration.ConfigurationManager.AppSettings["caminhoFisico"].ToString() + "P-" + rnIntegracoes.rakutenId + ".txt";
            ftp.PutFile(urlFoto, "P-" + rnIntegracoes.rakutenId + ".txt");
        }
    }

    private void gerarExtra()
    {
        var produto = new StringBuilder();

        var dataProdutos = new dbCommerceDataContext();
        var integracoesDc = new dbCommerceDataContext();

        dataProdutos.CommandTimeout = 0;
        var produtos = (from c in dataProdutos.tbProdutos
                        select new
                        {
                            c.produtoId,
                            c.produtoPaiId,
                            c.produtoEstoqueAtual,
                            c.produtoEstoqueMinimo,
                            c.produtoUrl,
                            produtoNome = c.produtoNomeNovo,
                            c.produtoPrecoPromocional,
                            c.produtoLegendaAtacado,
                            c.produtoPrecoAtacado,
                            c.produtoFreteGratis,
                            c.produtoPromocao,
                            c.produtoLancamento,
                            c.produtoMarca,
                            c.produtoDataDaCriacao,
                            c.produtoPreco,
                            c.produtoIdDaEmpresa,
                            c.produtoComposicao,
                            c.produtoFios,
                            c.produtoPecas,
                            c.produtoBrindes,
                            c.produtoDescricao,
                            c.produtoPeso,
                            c.fotoDestaque,
                            c.produtoPrincipal,
                            c.produtoFornecedor,
                            c.produtoAtivo,
                            c.marketplaceEnviarMarca,
                            c.marketplaceCadastrar,
                            largura = c.largura ?? 5,
                            altura = c.altura ?? 5,
                            profundidade = c.profundidade ?? 5
                        });
        /*if (chkExtraEnviarApenasNaoCadastrados.Checked)
        {
            var jaCadastrados = (from c in integracoesDc.tbExtraImportacaoProdutos select c.produtoId).ToList();
            produtos = (from c in produtos
                        join d in jaCadastrados on c.produtoId equals d into cadastrados
                        where cadastrados.Count() > 0 select c).ToList();
        }*/
        int totalProdutos = produtos.Count();
        int atual = 1;

        var produtosExtra = new List<rnIntegracoes.ExtraProduct>();
        List<int> produtosImportar = new List<int>();

        foreach (var prod in produtos)
        {
            var descricaoCompleta = new StringBuilder();
            var dataInformacao = new dbCommerceDataContext();
            var informacoesAdicional = (from c in dataInformacao.tbInformacaoAdcionals where c.produtoId == prod.produtoId orderby c.informacaoAdcionalNome descending select c).ToList();
            foreach (var informacaoAdcional in informacoesAdicional)
            {
                descricaoCompleta.AppendFormat("<div class=\"tarjas\">{0}</div>", informacaoAdcional.informacaoAdcionalNome);
                descricaoCompleta.AppendFormat("<div class=\"descricaoInformacoesAdicionais\">{0}</div>", informacaoAdcional.informacaoAdcionalConteudo);
            }
            var produtoImportado = (from c in integracoesDc.tbExtraImportacaoProdutos where c.produtoId == prod.produtoId select c).FirstOrDefault();
            var cadastrarMarketplace = prod.marketplaceCadastrar;
            cadastrarMarketplace = "False";

            if (prod.produtoAtivo == "True" && cadastrarMarketplace == "True")
            {
                if (produtoImportado == null | chkExtraRecadastrar.Checked == true)
                {
                    var dataProd = new dbCommerceDataContext();
                    var categoria = (from c in dataProd.tbJuncaoProdutoCategorias
                        where
                            c.produtoId == prod.produtoId && c.tbProdutoCategoria.exibirSite == true &&
                            c.tbProdutoCategoria.categoriaPaiId == 0
                        select c).FirstOrDefault();
                    if (categoria != null)
                    {
                        if(produtoImportado == null) produtosImportar.Add(prod.produtoId);

                        var fornecedorDc = new dbCommerceDataContext();
                        var fornecedor =
                            (from c in fornecedorDc.tbProdutoFornecedors
                                where c.fornecedorId == prod.produtoFornecedor
                                select c).FirstOrDefault();
                        string produtoDescricao = descricaoCompleta.ToString();
                        if (string.IsNullOrEmpty(produtoDescricao))
                        {
                            produtoDescricao = prod.produtoDescricao.Replace(Environment.NewLine, "<br />");
                            if (string.IsNullOrEmpty(produtoDescricao)) produtoDescricao = prod.produtoNome;
                        }
                        produtoDescricao = produtoDescricao.Replace(Environment.NewLine, "");
                        produtoDescricao = rnIntegracoes.removeLinks(produtoDescricao);

                        if (string.IsNullOrEmpty(produtoDescricao)) produtoDescricao = prod.produtoNome;
                        decimal precoPromocional = prod.produtoPreco;
                        if (prod.produtoPrecoPromocional > 0 && prod.produtoPrecoPromocional < prod.produtoPreco) precoPromocional = (decimal) prod.produtoPrecoPromocional;

                        var produtoExtra = new rnIntegracoes.ExtraProduct();
                        produtoExtra.skuIdOrigin = prod.produtoId.ToString();
                        produtoExtra.sellingTitle = prod.produtoNome;
                        produtoExtra.description = produtoDescricao;
                        string marca = "";
                        if (prod.marketplaceEnviarMarca == "True")
                        {
                            var marcaDc = new dbCommerceDataContext();
                            var marcaObj =
                                (from c in marcaDc.tbMarcas where c.marcaId == prod.produtoMarca select c).FirstOrDefault
                                    ();
                            if (marcaObj != null)
                            {
                                marca = marcaObj.marcaNome;
                            }
                            if (string.IsNullOrEmpty(marca)) marca = rnIntegracoes.marcaPadrao;
                        }
                        else
                        {
                            marca = rnIntegracoes.marcaPadrao;
                        }
                        produtoExtra.brand = marca;
                        produtoExtra.EAN = "";
                        produtoExtra.defaultPrice = Convert.ToDouble(Convert.ToDecimal(prod.produtoPreco.ToString("0.00")));
                        produtoExtra.salePrice = Convert.ToDouble(Convert.ToDecimal(precoPromocional.ToString("0.00")));
                        Response.Write(produtoExtra.salePrice + "<br>");
                        var categorias = new List<string>();
                        if (rnIntegracoes.extraHomologacao)
                        {
                            categorias.Add("Teste>API");
                        }
                        else
                        {
                            categorias.Add(categoria.tbProdutoCategoria.categoriaNomeExibicao);
                        }
                        produtoExtra.categoryList = categorias;
                        var udas = new List<rnIntegracoes.ProductUdaList>();
                        produtoExtra.productUdaLists = udas;
                        produtoExtra.Weight = Convert.ToDouble(Convert.ToDecimal(prod.produtoPeso)/100);
                        produtoExtra.Length = Convert.ToDouble(Convert.ToDecimal(prod.profundidade)/100);
                        produtoExtra.Width = Convert.ToDouble(Convert.ToDecimal(prod.largura)/100);
                        produtoExtra.Height = Convert.ToDouble(Convert.ToDecimal(prod.altura)/100);
                        produtoExtra.availableQuantity = prod.produtoEstoqueAtual;
                        int prazoAdicional = 0;
                        if (fornecedor.fornecedorPrazoDeFabricacao != null)
                        {
                            prazoAdicional = (int) fornecedor.fornecedorPrazoDeFabricacao;
                        }
                        produtoExtra.handlingTime = prazoAdicional;
                        var videos = new List<string>();
                        produtoExtra.videos = videos;
                        var fotos = new List<string>();
                        fotos.Add(System.Configuration.ConfigurationManager.AppSettings["caminhoVirtual"].ToString() +
                                  "fotos\\" + prod.produtoId + "\\" + prod.fotoDestaque + ".jpg");
                        var fotosDc = new dbCommerceDataContext();
                        var produtoFotos =
                            (from c in fotosDc.tbProdutoFotos
                                where c.produtoId == prod.produtoId && c.produtoFoto != prod.fotoDestaque
                                select c);
                        foreach (var produtoFoto in produtoFotos)
                        {
                            fotos.Add(
                                System.Configuration.ConfigurationManager.AppSettings["caminhoVirtual"].ToString() +
                                "fotos\\" + prod.produtoId + "\\" + produtoFoto.produtoFoto + ".jpg");
                        }
                        produtoExtra.images = fotos;

                        produtosExtra.Add(produtoExtra);

                    }
                }
                else
                {
                    string skuId = produtoImportado.skuId;
                    if (skuId == "")
                    {
                        skuId = rnIntegracoes.atualizaSkuExtra(prod.produtoId);
                    }

                    if (skuId != "")
                    {
                        try
                        {
                            rnIntegracoes.atualizaEstoqueExtra(produtoImportado.skuId, prod.produtoEstoqueAtual);
                            decimal precoPromocional = prod.produtoPreco;
                            if (prod.produtoPrecoPromocional > 0 && prod.produtoPrecoPromocional < prod.produtoPreco)
                                precoPromocional = (decimal)prod.produtoPrecoPromocional;
                            rnIntegracoes.atualizaPrecoExtra(produtoImportado.skuId, Convert.ToDecimal(prod.produtoPreco.ToString("0.00")), Convert.ToDecimal(precoPromocional.ToString("0.00")));
                        }
                        catch (Exception)
                        {
                            
                        }
                    }
                }
            }
            else
            {
                if (produtoImportado != null)
                {
                    string skuId = produtoImportado.skuId;
                    if (skuId == "")
                    {
                        skuId = rnIntegracoes.atualizaSkuExtra(prod.produtoId);
                    }

                    if (skuId != "")
                    {
                        try
                        {
                            rnIntegracoes.excluiProdutoExtra(skuId);
                        }
                        catch (Exception ex)
                        {
                            var teste = ex;
                        }
                    }
                }
            }
            progress = Convert.ToDouble(Convert.ToDecimal(atual) * 100 / totalProdutos);
            atual++;
        }

        if (produtosExtra.Count > 0)
        {
            var produtoJson = JsonConvert.SerializeObject(produtosExtra);
            var request = (HttpWebRequest) WebRequest.Create(rnIntegracoes.extraUrl + "/loads/products");
            request.Headers.Add("nova-auth-token", rnIntegracoes.extraAuthToken);
            request.Headers.Add("nova-app-token", rnIntegracoes.extraAppToken);
            request.Method = "POST";
            request.ContentType = "application/gzip";
            using (var requestStream = request.GetRequestStream())
            using (var gZipStream = new GZipStream(requestStream, CompressionMode.Compress))
            {
                var bytes = Encoding.UTF8.GetBytes(produtoJson);
                gZipStream.Write(bytes, 0, bytes.Length);
                gZipStream.Close();
                requestStream.Close();
            }
            var location = String.Empty;
            var result = String.Empty;
            using (var response = (HttpWebResponse) request.GetResponse())
            using (var stream = response.GetResponseStream())
            using (var reader = new StreamReader(stream))
            {
                location = response.Headers["Location"];
                result = reader.ReadToEnd();
            }
            
            var importacaoAtual = new tbExtraImportacao();
            importacaoAtual.data = DateTime.Now;
            importacaoAtual.importerInfoId = location.Replace("/loads/products/", "");
            importacaoAtual.status = 1;
            integracoesDc.tbExtraImportacaos.InsertOnSubmit(importacaoAtual);
            integracoesDc.SubmitChanges();

            foreach (var produtoImportar in produtosImportar)
            {
                var importacaoProduto = new tbExtraImportacaoProduto();
                importacaoProduto.produtoId = produtoImportar;
                importacaoProduto.skuId = "";
                importacaoProduto.extraImportacaoId = importacaoAtual.extraImportacaoId;
                integracoesDc.tbExtraImportacaoProdutos.InsertOnSubmit(importacaoProduto);
            }
            integracoesDc.SubmitChanges();
        }

    }

    protected void btnGerarExtra_Click(object sender, EventArgs e)
    {
        gerarExtra();
    }

    protected void btnGerarRakuten_Click(object sender, EventArgs e)
    {
        gerarRakuten();
    }

    protected void btnGerarSubmarino_OnClick(object sender, EventArgs e)
    {
        Workbook book = new Workbook();
        WorksheetStyle normal = book.Styles.Add("normal");
        normal.Font.Bold = false;
        Worksheet sheet = book.Worksheets.Add("Pedido");

        var produtosDc = new dbCommerceDataContext();
        var produtos = (from c in produtosDc.tbProdutos
            where c.produtoCodDeBarras != "" && c.ncm != "" && c.produtoCodDeBarras != null && c.ncm != ""
            select c).ToList();

        foreach (var produto in produtos)
        {
            var categoriasDc = new dbCommerceDataContext();
            var categoria = (from c in categoriasDc.tbJuncaoProdutoCategorias
                where
                    c.produtoId == produto.produtoId && c.tbProdutoCategoria.exibirSite == true &&
                    c.tbProdutoCategoria.categoriaPaiId == 0
                select c).FirstOrDefault();
            if (categoria != null)
            {
                WorksheetRow linha = sheet.Table.Rows.Add();
                linha.Cells.Add("10924051000163", DataType.String, "normal");
                linha.Cells.Add(produto.produtoId.ToString(), DataType.String, "normal");
                linha.Cells.Add(string.IsNullOrEmpty(produto.produtoNomeNovo) ? produto.produtoNome : produto.produtoNomeNovo, DataType.String, "normal");
                linha.Cells.Add("", DataType.String, "normal");
                linha.Cells.Add("", DataType.String, "normal");
                linha.Cells.Add("", DataType.String, "normal");
                linha.Cells.Add(produto.produtoPeso.ToString(), DataType.String, "normal");
                linha.Cells.Add(produto.produtoCodDeBarras, DataType.String, "normal");
                linha.Cells.Add("", DataType.String, "normal");
                linha.Cells.Add("", DataType.String, "normal");
                linha.Cells.Add("", DataType.String, "normal");
                linha.Cells.Add("", DataType.String, "normal");
                linha.Cells.Add("", DataType.String, "normal");
                linha.Cells.Add("", DataType.String, "normal");
                linha.Cells.Add(produto.ncm, DataType.String, "normal");
                linha.Cells.Add("0", DataType.String, "normal");
                linha.Cells.Add("", DataType.String, "normal");
                linha.Cells.Add("", DataType.String, "normal");
                linha.Cells.Add("E", DataType.String, "normal");
                linha.Cells.Add("A", DataType.String, "normal");
                linha.Cells.Add(produto.produtoNome, DataType.String, "normal");
                linha.Cells.Add(categoria.tbProdutoCategoria.submarinoGrupo, DataType.String, "normal");
                linha.Cells.Add(categoria.tbProdutoCategoria.submarinoGrupoDescricao, DataType.String, "normal");
                linha.Cells.Add(categoria.tbProdutoCategoria.submarinoDepartamento, DataType.String, "normal");
                linha.Cells.Add(categoria.tbProdutoCategoria.submarinoDepartamentoDescricao, DataType.String, "normal");
                linha.Cells.Add(categoria.tbProdutoCategoria.submarinoDepartamentoDescricao, DataType.String, "normal");
                linha.Cells.Add(categoria.tbProdutoCategoria.submarinoClasse, DataType.String, "normal");
                linha.Cells.Add(categoria.tbProdutoCategoria.submarinoClasseDescricao, DataType.String, "normal");
                linha.Cells.Add(categoria.tbProdutoCategoria.submarinoSubclasse, DataType.String, "normal");
                linha.Cells.Add(categoria.tbProdutoCategoria.submarinoSubclasseDescricao, DataType.String, "normal");
                linha.Cells.Add("http://cdn.graodegente.com.br/fotos/" + produto.produtoId + "/" + produto.fotoDestaque + ".jpg", DataType.String, "normal");
                
            }
        }


        using (MemoryStream memoryStream = new MemoryStream())
        {
            book.Save(memoryStream);
            Response.Clear();
            Response.ContentType = "application/ms-excel";
            Response.AddHeader("Content-Disposition", "attachment; filename=submarino.xls");
            Response.BinaryWrite(memoryStream.ToArray());
            Response.Flush();
            Response.Close();
            Response.End();
        }
    }

    protected void btnGerarSubmarinoFrete_OnClick(object sender, EventArgs e)
    {
        Workbook book = new Workbook();
        WorksheetStyle normal = book.Styles.Add("normal");
        normal.Font.Bold = false;
        Worksheet sheet = book.Worksheets.Add("Pedido");

        var faixasDc = new dbCommerceDataContext();
        var faixas = (from c in faixasDc.tbFaixaDeCeps where c.tipodeEntregaId == 9 select c).ToList();
        foreach (var faixa in faixas)
        {
            var faixaPrecosDc = new dbCommerceDataContext();
            var faixasPrecos =
                (from c in faixaPrecosDc.tbFaixaDeCepPesoPrecos where c.faixaDeCepId == faixa.faixaDeCepId select c)
                    .ToList();
            foreach (var faixasPreco in faixasPrecos)
            {
                WorksheetRow linha = sheet.Table.Rows.Add();
                linha.Cells.Add("10924051000163", DataType.String, "normal");
                linha.Cells.Add(faixa.faixaDeCepId.ToString(), DataType.String, "normal");
                linha.Cells.Add(faixa.faixaInicial, DataType.String, "normal");
                linha.Cells.Add(faixa.faixaFinal, DataType.String, "normal");
                linha.Cells.Add(faixasPreco.faixaDeCepPesoInicial.ToString("0") + "0", DataType.String, "normal");
                linha.Cells.Add(faixasPreco.faixaDeCepPesoFinal.ToString("0") + "0", DataType.String, "normal");
                linha.Cells.Add(faixasPreco.faixaDeCepPreco.ToString("0.0000").Replace(",", ""), DataType.String, "normal");
                linha.Cells.Add((faixa.prazo + 1 + 15).ToString(), DataType.String, "normal");
            }
        }

        using (MemoryStream memoryStream = new MemoryStream())
        {
            book.Save(memoryStream);
            Response.Clear();
            Response.ContentType = "application/ms-excel";
            Response.AddHeader("Content-Disposition", "attachment; filename=submarinoFrete.xls");
            Response.BinaryWrite(memoryStream.ToArray());
            Response.Flush();
            Response.Close();
            Response.End();
        }
    }

    protected void btnGerarSubmarinoPreco_OnClick(object sender, EventArgs e)
    {
        Workbook book = new Workbook();
        WorksheetStyle normal = book.Styles.Add("normal");
        normal.Font.Bold = false;
        Worksheet sheet = book.Worksheets.Add("Pedido");

        var produtosDc = new dbCommerceDataContext();
        var produtos = (from c in produtosDc.tbProdutos
                        where c.produtoCodDeBarras != "" && c.ncm != "" && c.produtoCodDeBarras != null && c.ncm != ""
                        select c).ToList();

        foreach (var produto in produtos)
        {
            decimal precoDe = produto.produtoPreco;
            decimal precoPor = produto.produtoPrecoPromocional > 0 &&
                               produto.produtoPrecoPromocional < produto.produtoPreco
                ? (decimal) produto.produtoPrecoPromocional
                : produto.produtoPreco;
            WorksheetRow linha = sheet.Table.Rows.Add();
            linha.Cells.Add("10924051000163", DataType.String, "normal");
            linha.Cells.Add(produto.produtoId.ToString(), DataType.String, "normal");
            linha.Cells.Add(precoDe.ToString("0.00").Replace(",", ""), DataType.String, "normal");
            linha.Cells.Add(precoPor.ToString("0.00").Replace(",", ""), DataType.String, "normal");
        }


        using (MemoryStream memoryStream = new MemoryStream())
        {
            book.Save(memoryStream);
            Response.Clear();
            Response.ContentType = "application/ms-excel";
            Response.AddHeader("Content-Disposition", "attachment; filename=submarinoPrecos.xls");
            Response.BinaryWrite(memoryStream.ToArray());
            Response.Flush();
            Response.Close();
            Response.End();
        }
    }
    protected void btnGerarSubmarinoEstoque_OnClick(object sender, EventArgs e)
    {
        Workbook book = new Workbook();
        WorksheetStyle normal = book.Styles.Add("normal");
        normal.Font.Bold = false;
        Worksheet sheet = book.Worksheets.Add("Pedido");

        var produtosDc = new dbCommerceDataContext();
        var produtos = (from c in produtosDc.tbProdutos
                        where c.produtoCodDeBarras != "" && c.ncm != "" && c.produtoCodDeBarras != null && c.ncm != ""
                        select c).ToList();

        foreach (var produto in produtos)
        {
            decimal precoDe = produto.produtoPreco;
            decimal precoPor = produto.produtoPrecoPromocional > 0 &&
                               produto.produtoPrecoPromocional < produto.produtoPreco
                ? (decimal) produto.produtoPrecoPromocional
                : produto.produtoPreco;
            WorksheetRow linha = sheet.Table.Rows.Add();
            linha.Cells.Add("10924051000163", DataType.String, "normal");
            linha.Cells.Add(produto.produtoId.ToString(), DataType.String, "normal");
            linha.Cells.Add("999", DataType.String, "normal");
        }


        using (MemoryStream memoryStream = new MemoryStream())
        {
            book.Save(memoryStream);
            Response.Clear();
            Response.ContentType = "application/ms-excel";
            Response.AddHeader("Content-Disposition", "attachment; filename=submarinoEstoque.xls");
            Response.BinaryWrite(memoryStream.ToArray());
            Response.Flush();
            Response.Close();
            Response.End();
        }
    }
}