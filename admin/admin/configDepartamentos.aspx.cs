﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_configDepartamentos : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            CarregarGrid();
            if (!IsPostBack)
            {
                Session.Add("sAssuntoTarefa", new List<tbChamadoDepartamentoAssuntoTarefa>());
                Session.Add("sAsssuntoDelete", new List<tbChamadoDepartamentoAssuntoTarefa>());
            }
        }
        catch (Exception ex)
        {
            lblMensagem.Text = "Erro ao carregar os departamentos: " + ex.Message;
        }
    }

    private void CarregarGrid()
    {
        try
        {
            var data = new dbCommerceDataContext();

            var departamentos = (from c in data.tbChamadoDepartamentos select c).OrderBy(x => x.nomeChamadoDepartamento).ToList();

            grd.DataSource = departamentos;
            grd.DataBind();

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void grd_HtmlRowCreated(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs e)
    {

    }

    private void CarregarUsuariosListas(int idDepartamento)
    {
        try
        {
            var data = new dbCommerceDataContext();

            List<int> lst = new List<int>();
            lst = (from c in data.tbUsuarios select c.usuarioId).ToList();

            var usuariosComAcesso = (from c in data.tbChamadoDepartamentoUsuarios
                                     join d in data.tbUsuarios on c.usuarioId equals d.usuarioId
                                     where lst.Contains(c.usuarioId) && c.idDepartamento == idDepartamento
                                     select d).Distinct().ToList();

            lstUsuarioComAcesso.DataSource = usuariosComAcesso;
            lstUsuarioComAcesso.DataTextField = "usuarioNome";
            lstUsuarioComAcesso.DataValueField = "usuarioId";
            lstUsuarioComAcesso.DataBind();

            var lstSemAcesso = lst.Where(p => !usuariosComAcesso.Select(u => u.usuarioId).Contains(p)).Distinct().ToList();

            var usuarioSemAcesso = (from c in data.tbUsuarios
                                    where lstSemAcesso.Contains(c.usuarioId)
                                    select c).Distinct().ToList();

            lstUsuarioSemAcesso.DataSource = usuarioSemAcesso;
            lstUsuarioSemAcesso.DataTextField = "usuarioNome";
            lstUsuarioSemAcesso.DataValueField = "usuarioId";
            lstUsuarioSemAcesso.DataBind();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void btnAlterarDepartamento_Command(object sender, CommandEventArgs e)
    {
        try
        {
            var data = new dbCommerceDataContext();

            hdfIdChamadoDepartamento.Value = e.CommandArgument.ToString();
            var idDepartamento = int.Parse(e.CommandArgument.ToString());
            var deptoSelecionado = (from c in data.tbChamadoDepartamentos where c.idChamadoDepartamento == idDepartamento select c).FirstOrDefault();

            if (e.CommandName == "Alterar" && e.CommandArgument != null)
            {
                txtNomeDepartamento.Text = deptoSelecionado.nomeChamadoDepartamento;
                txtOrdemDepartamento.Text = deptoSelecionado.ordem.ToString();
                CarregarAssuntos();
                CarregarGridAssuntos(idDepartamento);

                popAdicionarDepartamento.ShowOnPageLoad = true;
            }
            else if (e.CommandName == "VincularUsuario" && e.CommandArgument != null)
            {

                lblDepartamentoSelecionado.Text = deptoSelecionado.nomeChamadoDepartamento;
                CarregarUsuariosListas(idDepartamento);
                popManutencaoDepartamento.ShowOnPageLoad = true;
            }


        }
        catch (Exception ex)
        {
            lblMensagem.Text = "Erro ao alterar o departamento: " + ex.Message;
        }
    }

    private void CarregarGridAssuntos(int idDepartamento)
    {
        try
        {
            var data = new dbCommerceDataContext();

            var assuntosTarefas = (from c in data.tbChamadoDepartamentoAssuntoTarefas where c.idDepartamento == idDepartamento select c).ToList();

            Session["sAssuntoTarefa"] = assuntosTarefas;

            gvAssuntosTarefas.DataSource = assuntosTarefas;
            gvAssuntosTarefas.DataBind();

        }
        catch (Exception ex)
        {
            throw;
        }
    }

    protected void btnAdicionarDepartamento_Click(object sender, EventArgs e)
    {
        try
        {
            LimparCamposForm();
            CarregarAssuntos();
            gvAssuntosTarefas.DataSource = null;
            gvAssuntosTarefas.DataBind();

            popAdicionarDepartamento.ShowOnPageLoad = true;
        }
        catch (Exception ex)
        {
            lblMensagem.Text = "Erro ao adicionar um novo departamento: " + ex.Message;

        }
    }

    private void CarregarAssuntos()
    {
        try
        {
            var data = new dbCommerceDataContext();
            var assuntos = (from c in data.tbChamadoAssuntos select c).OrderBy(x => x.nomeChamadoAssunto).ToList();
            cboAssunto.TextField = "nomeChamadoAssunto";
            cboAssunto.ValueField = "idChamadoAssunto";
            cboAssunto.DataSource = assuntos;
            cboAssunto.DataBind();
            cboAssunto.Items.Insert(0, new DevExpress.Web.ASPxEditors.ListEditItem() { Index = 0, Text = "Selecione", Value = "-1", ValueString = "" });
            cboAssunto.SelectedIndex = 0;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void btnTirarAcesso_Click(object sender, EventArgs e)
    {
        try
        {
            var data = new dbCommerceDataContext();
            if (hdfIdChamadoDepartamento.Value != null)
            {
                var indices = lstUsuarioComAcesso.GetSelectedIndices();
                var idDepartamento = int.Parse(hdfIdChamadoDepartamento.Value);

                List<int> lstUsuarios = new List<int>();
                foreach (var item in indices)
                {
                    var idUsuario = int.Parse(lstUsuarioComAcesso.Items[item].Value);
                    RetirarAcesso(idDepartamento, idUsuario);
                }
                CarregarUsuariosListas(idDepartamento);
            }
        }
        catch (Exception ex)
        {
            lblMensagem.Text = "Erro ao retirar o acesso ao departamento: " + ex.Message;
        }
    }

    private bool RetirarAcesso(int idDepartamento, int idUsuario)
    {
        try
        {
            var data = new dbCommerceDataContext();
            var tb = new tbChamadoDepartamentoUsuario() { idDepartamento = idDepartamento, usuarioId = idUsuario };
            data.tbChamadoDepartamentoUsuarios.Attach(tb);
            data.tbChamadoDepartamentoUsuarios.DeleteOnSubmit(tb);
            data.SubmitChanges();
            return true;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void btnDarAcesso_Click(object sender, EventArgs e)
    {
        try
        {
            var data = new dbCommerceDataContext();
            if (hdfIdChamadoDepartamento.Value != null)
            {
                var indices = lstUsuarioSemAcesso.GetSelectedIndices();
                var idDepartamento = int.Parse(hdfIdChamadoDepartamento.Value);

                List<int> lstUsuarios = new List<int>();
                foreach (var item in indices)
                {
                    var idUsuario = int.Parse(lstUsuarioSemAcesso.Items[item].Value);
                    DarAcesso(idDepartamento, idUsuario);
                }
                CarregarUsuariosListas(idDepartamento);
            }
        }
        catch (Exception ex)
        {
            lblMensagem.Text = "Erro ao conceder acesso ao departamento: " + ex.Message;
        }
    }

    private bool DarAcesso(int idDepartamento, int item)
    {
        try
        {
            var data = new dbCommerceDataContext();
            var tb = new tbChamadoDepartamentoUsuario() { idDepartamento = idDepartamento, usuarioId = item };
            data.tbChamadoDepartamentoUsuarios.InsertOnSubmit(tb);
            data.SubmitChanges();
            return true;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void btnAddDepartamento_Click(object sender, EventArgs e)
    {
        try
        {
            var data = new dbCommerceDataContext();
            var tb = new tbChamadoDepartamento();

            if (string.IsNullOrEmpty(txtNomeDepartamento.Text))
            {
                lblMensagemAdd.Text = "Favor informar o nome do departamento";
                txtNomeDepartamento.Focus();
                return;
            }

            if (hdfIdChamadoDepartamento.Value != "")
            {
                var depto = (from c in data.tbChamadoDepartamentos where c.idChamadoDepartamento == int.Parse(hdfIdChamadoDepartamento.Value) select c).FirstOrDefault();

                depto.nomeChamadoDepartamento = txtNomeDepartamento.Text;
                if (!string.IsNullOrEmpty(txtOrdemDepartamento.Text))
                    depto.ordem = int.Parse(txtOrdemDepartamento.Text);
                data.SubmitChanges();

                AssociarAssuntosTarefas(depto.idChamadoDepartamento);
                RemoverAssociacaoAssuntoTarefa();
            }
            else
            {
                var idProximo = data.tbChamadoDepartamentos.Max(x => x.idChamadoDepartamento) + 1;
                tb.idChamadoDepartamento = idProximo;
                tb.nomeChamadoDepartamento = txtNomeDepartamento.Text;
                if (!string.IsNullOrEmpty(txtOrdemDepartamento.Text))
                    tb.ordem = int.Parse(txtOrdemDepartamento.Text);
                data.tbChamadoDepartamentos.InsertOnSubmit(tb);
                data.SubmitChanges();

                AssociarAssuntosTarefas(idProximo);
            }
            Session["sAssuntoTarefa"] = null;
            Session["sAssuntoDelete"] = null;

            Response.Redirect("configDepartamentos.aspx", true);
        }
        catch (Exception ex)
        {
            lblMensagemAdd.Text = "Erro ao tentar adicionar o departamento: " + ex.Message;
        }
    }

    private void RemoverAssociacaoAssuntoTarefa()
    {
        try
        {
            var data = new dbCommerceDataContext();
            var delete = (List<tbChamadoDepartamentoAssuntoTarefa>)Session["sAsssuntoDelete"];
            foreach (var item in delete)
            {
                var registro = (from c in data.tbChamadoDepartamentoAssuntoTarefas where c.idDepartamentAssuntoTarefa == item.idDepartamentAssuntoTarefa select c).FirstOrDefault();
                data.tbChamadoDepartamentoAssuntoTarefas.DeleteOnSubmit(registro);
                data.SubmitChanges();
            }
        }
        catch (Exception ex)
        {
            lblMensagemAdd.Text = "Erro ao remover a associação: " + ex.Message;
        }
    }

    private void AssociarAssuntosTarefas(int idProximo)
    {
        try
        {
            var data = new dbCommerceDataContext();

            var assuntosTarefas = (List<tbChamadoDepartamentoAssuntoTarefa>)Session["sAssuntoTarefa"];

            foreach (var item in assuntosTarefas)
            {
                if (item.idDepartamentAssuntoTarefa == 0)
                {
                    item.idDepartamento = idProximo;
                    item.idDepartamentAssuntoTarefa = data.tbChamadoDepartamentoAssuntoTarefas.Max(x => x.idDepartamentAssuntoTarefa) + 1;
                    data.tbChamadoDepartamentoAssuntoTarefas.InsertOnSubmit(item);
                    data.SubmitChanges();
                }

            }

        }
        catch (Exception ex)
        {
            throw ex; ;
        }
    }

    private void LimparCamposForm()
    {
        hdfIdChamadoDepartamento.Value = string.Empty;
        txtNomeDepartamento.Text = string.Empty;
        txtOrdemDepartamento.Text = string.Empty;
    }

    protected void cboAssunto_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            CarregarTarefa();
        }
        catch (Exception ex)
        {
            lblMensagemAdd.Text = "Erro ao selecionar o assunto: " + ex.Message;
        }
    }

    private void CarregarTarefa()
    {
        try
        {
            var data = new dbCommerceDataContext();
            var tarefas = (from c in data.tbChamadoTarefas select c).OrderBy(x => x.nomeChamadoTarefa).ToList();

            cboTarefa.TextField = "nomeChamadoTarefa";
            cboTarefa.ValueField = "idChamadoTarefa";
            cboTarefa.DataSource = tarefas;
            cboTarefa.DataBind();

            cboTarefa.Items.Insert(0, new DevExpress.Web.ASPxEditors.ListEditItem() { Index = 0, Text = "Assunto sem tarefa", Value = "-1" });

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void btnExcluir_Command(object sender, CommandEventArgs e)
    {
        try
        {
            var listDelete = (List<tbChamadoDepartamentoAssuntoTarefa>)Session["sAsssuntoDelete"];
            var assuntosTarefasGrid = (List<tbChamadoDepartamentoAssuntoTarefa>)Session["sAssuntoTarefa"];
            int? idTarefa = null;
            int idAssunto = 0;
            int? idDepartamento = null;

            if (e.CommandName == "ExcluirAssuntoTarefa" && !string.IsNullOrEmpty(e.CommandArgument.ToString()))
            {
                var indice = int.Parse(e.CommandArgument.ToString());
                var dados = gvAssuntosTarefas.DataKeys[indice].Values;

                idAssunto = int.Parse(dados["idAssunto"].ToString());

                if (dados["idDepartamento"] != null)
                   idDepartamento = int.Parse(dados["idDepartamento"].ToString());

                var idDepartamentAssuntoTarefa = dados["idDepartamentAssuntoTarefa"];

                if (dados["idTarefa"] != null)
                    idTarefa = int.Parse(dados["idTarefa"].ToString());

                dynamic tb;

                if (idDepartamentAssuntoTarefa != null)
                    tb = assuntosTarefasGrid.Where(x => x.idDepartamentAssuntoTarefa == int.Parse(idDepartamentAssuntoTarefa.ToString())).FirstOrDefault();
                else
                    tb = assuntosTarefasGrid.Where(x => x.idDepartamento == idDepartamento && x.idAssunto == idAssunto && x.idTarefa == idTarefa).FirstOrDefault();

                assuntosTarefasGrid.Remove(tb);

                listDelete.Add(tb);

                Session["sAsssuntoDelete"] = listDelete;

                Session["sAssuntoTarefa"] = assuntosTarefasGrid;

                gvAssuntosTarefas.DataSource = assuntosTarefasGrid;
                gvAssuntosTarefas.DataBind();
            }
        }
        catch (Exception ex)
        {
            lblMensagemAdd.Text = "Erro ao excluir a associação de assunto/tarefa ao departamento " + ex.Message;
        }
    }

    protected void btnAdicionarAssuntoTarefa_Click(object sender, EventArgs e)
    {
        try
        {
            if (cboAssunto.SelectedIndex > 0)
            {
                var assuntosTarefasGrid = (List<tbChamadoDepartamentoAssuntoTarefa>)Session["sAssuntoTarefa"];

                var assuntoTarefa = new tbChamadoDepartamentoAssuntoTarefa();
                assuntoTarefa.idAssunto = int.Parse(cboAssunto.SelectedItem.Value.ToString());

                if (cboTarefa.SelectedIndex > 0)
                    assuntoTarefa.idTarefa = int.Parse(cboTarefa.SelectedItem.Value.ToString());

                assuntosTarefasGrid.Add(assuntoTarefa);
                Session["sAssuntoTarefa"] = assuntosTarefasGrid;

                gvAssuntosTarefas.DataSource = assuntosTarefasGrid;
                gvAssuntosTarefas.DataBind();

            }
        }
        catch (Exception ex)
        {
            lblMensagemAdd.Text = "Erro ao associar um assunto/tarefa ao departamento: " + ex.Message;
        }
    }

    protected void gvAssuntosTarefas_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            var data = new dbCommerceDataContext();
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var lblAssunto = (Label)e.Row.FindControl("lblAssunto");
                if (!string.IsNullOrEmpty(lblAssunto.Text))
                {
                    var assunto = (from c in data.tbChamadoAssuntos where c.idChamadoAssunto == int.Parse(lblAssunto.Text) select c).FirstOrDefault();
                    lblAssunto.Text = assunto.nomeChamadoAssunto;
                }

                var lblTarefa = (Label)e.Row.FindControl("lblTarefa");
                if (!string.IsNullOrEmpty(lblTarefa.Text))
                {
                    var tarefa = (from c in data.tbChamadoTarefas where c.idChamadoTarefa == int.Parse(lblTarefa.Text) select c).FirstOrDefault();

                    if (tarefa == null)
                        lblTarefa.Text = "Sem tarefa";
                    else
                        lblTarefa.Text = tarefa.nomeChamadoTarefa;

                }
            }
        }
        catch (Exception ex)
        {
            lblMensagemAdd.Text = "Erro: " + ex.Message;
        }
    }

    protected void gvAssuntosTarefas_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {

        }
        catch (Exception ex)
        {
            lblMensagemAdd.Text = "Erro: " + ex.Message;
        }
    }

    protected void btnAdicionarAssunto_Click(object sender, EventArgs e)
    {
        try
        {
            txtAssunto.Text = string.Empty;
            CarregarGridAssuntos();
            popAdicionarAssunto.ShowOnPageLoad = true;
        }
        catch (Exception ex)
        {
            lblMensagem.Text = "Erro ao adicionar um novo assunto: " + ex.Message;

        }
    }

    private void CarregarGridTarefas()
    {
        try
        {
            var data = new dbCommerceDataContext();

            var tarefas = (from c in data.tbChamadoTarefas select c).OrderBy(x => x.idChamadoTarefa).ToList();

            gvTarefa.DataSource = tarefas;
            gvTarefa.DataBind();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void CarregarGridAssuntos()
    {
        try
        {
            var data = new dbCommerceDataContext();

            var assuntos = (from c in data.tbChamadoAssuntos select c).OrderBy(x => x.idChamadoAssunto).ToList();

            gvAssunto.DataSource = assuntos;
            gvAssunto.DataBind();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void btnAdicionarTarefa_Click(object sender, EventArgs e)
    {
        try
        {
            CarregarGridTarefas();
            popAdicionarTarefa.ShowOnPageLoad = true;
        }
        catch (Exception ex)
        {
            lblMensagem.Text = "Erro ao adicionar uma nova tarefa: " + ex.Message;
        }
    }

    protected void btnAlterarAssunto_Command(object sender, CommandEventArgs e)
    {
        var data = new dbCommerceDataContext();
        try
        {
            if (e.CommandName == "AlterarAssunto" && !string.IsNullOrEmpty(e.CommandArgument.ToString()))
            {
                var assunto = (from c in data.tbChamadoAssuntos where c.idChamadoAssunto == int.Parse(e.CommandArgument.ToString()) select c).FirstOrDefault();
                hdfAssuntoId.Value = assunto.idChamadoAssunto.ToString();
                txtAssunto.Text = assunto.nomeChamadoAssunto;
            }
        }
        catch (Exception ex)
        {
            lblMensagemAssunto.Text = "Erro ao alterar o assunto: " + ex.Message;
        }
    }

    protected void btnAddAssunto_Click(object sender, EventArgs e)
    {
        try
        {
            var data = new dbCommerceDataContext();

            if (!string.IsNullOrEmpty(hdfAssuntoId.Value))
            {
                var idAssunto = int.Parse(hdfAssuntoId.Value);
                var assunto = (from c in data.tbChamadoAssuntos where c.idChamadoAssunto == idAssunto select c).FirstOrDefault();
                assunto.nomeChamadoAssunto = txtAssunto.Text;
                data.SubmitChanges();
            }
            else
            {
                var assunto = new tbChamadoAssunto();
                var idProximo = data.tbChamadoAssuntos.Max(x => x.idChamadoAssunto) + 1;
                assunto.idChamadoAssunto = idProximo;
                assunto.nomeChamadoAssunto = txtAssunto.Text;
                data.tbChamadoAssuntos.InsertOnSubmit(assunto);
                data.SubmitChanges();
            }

            CarregarGridAssuntos();
            hdfAssuntoId.Value = string.Empty;
            txtAssunto.Text = string.Empty;
        }
        catch (Exception ex)
        {
            lblMensagemAssunto.Text = "Erro ao adicionar o assunto: " + ex.Message;
        }
    }

    protected void btnAlterarTarefa_Command(object sender, CommandEventArgs e)
    {
        try
        {
            var data = new dbCommerceDataContext();

            if (e.CommandName == "AlterarTarefa" && !string.IsNullOrEmpty(e.CommandArgument.ToString()))
            {
                var tarefa = (from c in data.tbChamadoTarefas where c.idChamadoTarefa == int.Parse(e.CommandArgument.ToString()) select c).FirstOrDefault();
                hdfTarefaId.Value = tarefa.idChamadoTarefa.ToString();
                txtTarefa.Text = tarefa.nomeChamadoTarefa;
            }
        }
        catch (Exception ex)
        {
            lblMensagemTarefa.Text = "Erro ao alterar a tarefa: " + ex.Message;
        }
    }

    protected void btnAddTarefa_Click(object sender, EventArgs e)
    {
        var data = new dbCommerceDataContext();

        if (!string.IsNullOrEmpty(hdfTarefaId.Value))
        {
            var idTarefa = int.Parse(hdfTarefaId.Value);
            var tarefa = (from c in data.tbChamadoTarefas where c.idChamadoTarefa == idTarefa select c).FirstOrDefault();
            tarefa.nomeChamadoTarefa = txtTarefa.Text;
            data.SubmitChanges();
        }
        else
        {
            var tarefa = new tbChamadoTarefa();
            var idProximo = data.tbChamadoTarefas.Max(x => x.idChamadoTarefa) + 1;
            tarefa.idChamadoTarefa = idProximo;
            tarefa.nomeChamadoTarefa = txtAssunto.Text;
            data.tbChamadoTarefas.InsertOnSubmit(tarefa);
            data.SubmitChanges();
        }

        CarregarGridTarefas();
        hdfTarefaId.Value = string.Empty;
        txtTarefa.Text = string.Empty;
    }
}