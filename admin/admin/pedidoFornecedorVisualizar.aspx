﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="pedidoFornecedorVisualizar.aspx.cs" Inherits="admin_pedidoFornecedorVisualizar" %>

<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxTimer" TagPrefix="dxt" %>
<%@ Register Assembly="CuteEditor" Namespace="CuteEditor" TagPrefix="CE" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" runat="Server">
    <table cellpadding="0" cellspacing="0" style="width: 920px">
        <tr>
            <td class="tituloPaginas" valign="top">
                <asp:Label ID="lblAcao" runat="server">Pedidos ao Fornecedor</asp:Label>
            </td>
        </tr>

        <tr>
            <td>

                <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
                    <tr class="rotulos" style="font-size: 16px;">
                        <td>
                            <b>Romaneio gerado por:</b>
                            <asp:Literal runat="server" ID="litUsuarioRomaneio"></asp:Literal><br />
                            <b>Data do Pedido:</b>
                            <asp:Literal runat="server" ID="litDataPedido"></asp:Literal><br />
                            <b>Data de Entrega:</b>
                            <asp:Literal runat="server" ID="litDataEntrega"></asp:Literal><br />
                            <b>Nº do Pedido:</b>
                            <asp:Literal runat="server" ID="litNumeroPedido"></asp:Literal><br />
                            <b>Fornecedor:</b>
                            <asp:Literal runat="server" ID="litFornecedor"></asp:Literal><br />
                            <b>Peso total:</b>
                            <asp:Literal runat="server" ID="litPeso"></asp:Literal><br />
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table style="width: 100%;">
                                <tr class="rotulos" style="text-align: center; font-weight: bold;">
                                    <td></td>
                                    <td></td>
                                    <td runat="server" id="tdCabecalhoCusto0" visible="False"></td>
                                    <td>Etiqueta
                                    </td>
                                    <td>ID Pedido
                                    </td>
                                    <td>ID Interno
                                    </td>
                                    <td>ID do Produto</td>
                                    <td>ID da Empresa
                                    </td>
                                    <td>Complemento ID
                                    </td>
                                    <td>Nome
                                    </td>
                                    <td>Pedidos
                                    </td>
                                    <td>Entregues
                                    </td>
                                    <td>Encomendas
                                    </td>
                                    <td>Data Entrega
                                    </td>
                                    <td>Entregue
                                    </td>
                                    <td>Foto</td>
                                    <td>Fila
                                    </td>
                                </tr>
                                <asp:ListView runat="server" ID="lstProdutos" OnItemDataBound="lstProdutos_OnItemDataBound">
                                    <ItemTemplate>
                                        <tr class="rotulos" style="text-align: center">
                                            <td style="padding-bottom: 3px; padding-top: 3px; border-bottom: 1px solid #b0e0e6;">
                                                <asp:LinkButton runat="server" Visible="False" CommandArgument='<%# Eval("idPedidoFornecedorItem") %>' OnCommand="btnExcluir_OnCommand" ID="btnExcluir" Text="Excluir" OnClientClick="return confirm('Deseja realmente excluir este item? Este procedimento é IRREVERSÍVEL!');"></asp:LinkButton>
                                            </td>
                                            <td style="padding-bottom: 3px; padding-top: 3px; border-bottom: 1px solid #b0e0e6;">
                                                <asp:LinkButton runat="server" Visible="False" ID="btnNaoEntregue" CommandArgument='<%# Eval("idPedidoFornecedorItem") %>' Text="Não Entregue" OnCommand="btnNaoEntregue_OnCommand" OnClientClick="return confirm('Deseja cancelar a entrega deste item? Este procedimento é IRREVERSÍVEL!');"></asp:LinkButton>
                                            </td>
                                            <td style="padding-bottom: 3px; padding-top: 3px; border-bottom: 1px solid #b0e0e6;" runat="server" id="tdInformarCusto0" visible="False">
                                                <asp:LinkButton runat="server" ID="btnCustoZero" CommandArgument='<%# Eval("idPedidoFornecedorItem") %>' Text="Custo R$0,00" OnCommand="btnCustoZero_OnCommand" OnClientClick="return confirm('Deseja informar a etiqueta como custo R$0,00? Este procedimento é IRREVERSÍVEL!');"></asp:LinkButton>
                                            </td>
                                            <td>
                                                <%# Eval("idPedidoFornecedorItem") %>
                                            </td>
                                            <td>
                                                <asp:HiddenField runat="server" ID="hdfIdPedido" Value='<%# Eval("idPedido") %>' />
                                                <asp:Literal runat="server" ID="litIdPedidoCliente"></asp:Literal>
                                            </td>
                                            <td>
                                                <%# Eval("idPedido") %>
                                            </td>
                                            <td>
                                                <%# Eval("idProduto") %>
                                            </td>                                            
                                            <td>
                                                <asp:Literal runat="server" ID="litProdutoIdDaEmpresa" Text='<%# Eval("produtoIdDaEmpresa") %>'></asp:Literal>
                                            </td>
                                            <td>
                                                <asp:Literal runat="server" ID="litComplementoIdDaEmpresa" Text='<%# Eval("complementoIdDaEmpresa") %>'></asp:Literal>
                                            </td>
                                            <td>
                                                <%# Eval("produtoNome") %>
                                            </td>
                                            <td>
                                                <%# Eval("pedidosPendentes") %>
                                            </td>
                                            <td>
                                                <%# Eval("produtosNaoEnderecados") %>
                                            </td>
                                            <td>
                                                <%# Eval("encomendasNaoEntregues") %>
                                            </td>
                                            <td>
                                                <%# Eval("dataEntrega") %>
                                            </td>
                                            <td>
                                                <asp:CheckBox runat="server" ID="chkEntregue" Checked='<%# Convert.ToBoolean(Eval("entregue")) %>' />
                                            </td>
                                            <td>                                                
                                                <asp:HyperLink runat="server" ID="hlFoto" Target="_blank" NavigateUrl='<%# "http://dmhxz00kguanp.cloudfront.net/fotos/" + Eval("idProduto") + "/" + Eval("fotoDestaque") + ".jpg" %>' Text="Foto" ></asp:HyperLink>
                                            </td>
                                            <td>
                                                <a href='filaEstoque.aspx?produtoId=<%# Eval("idProduto") %>' target="_blank">Fila</a>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:ListView>

                                <tr class="rotulos" style="text-align: center; font-weight: bold;">
                                    <td>&nbsp;
                                    </td>
                                    <td>&nbsp;
                                    </td>
                                    <td>&nbsp;
                                    </td>
                                    <td>&nbsp;
                                    </td>
                                    <td>&nbsp;
                                    </td>
                                    <td>&nbsp;
                                    </td>
                                    <td>
                                        <asp:Literal runat="server" ID="litTotalQuantidade"></asp:Literal>
                                    </td>
                                    <td>&nbsp;
                                    </td>
                                </tr>
                                <asp:ObjectDataSource ID="sql" runat="server" SelectMethod="pedidoFornecedorPorPedidoFornecedorId"
                                    TypeName="rnPedidos">
                                    <SelectParameters>
                                        <asp:QueryStringParameter DefaultValue="0" QueryStringField="idPedidoFornecedor" Name="idPedidoFornecedor" Type="Int32" />
                                    </SelectParameters>
                                </asp:ObjectDataSource>

                                <asp:SqlDataSource ID="sql2" runat="server" ConnectionString="<%$ ConnectionStrings:connectionString %>"
                                    SelectCommand="SELECT
                                                        tbPedidoFornecedorItem.idPedido,
                                                        tbProdutos.produtoIdDaEmpresa,
                                                        tbProdutos.complementoIdDaEmpresa,
                                                        tbProdutos.produtoNome,
                                                        sum(tbPedidoFornecedorItem.quantidade) as quantidade,
                                                        tbPedidoFornecedorItem.custo,
                                                        tbPedidoFornecedorItem.diferenca,
                                                        tbPedidoFornecedorItem.entregue,
                                                        tbPedidoFornecedorItem.dataEntrega,
                                                        tbPedidoFornecedorItem.idPedidoFornecedorItem,
                                                        tbPedidoFornecedorItem.idProduto,
                                                        tbPedidoFornecedorItem.pedidosPendentes,
                                                        tbPedidoFornecedorItem.produtosNaoEnderecados,
                                                        tbPedidoFornecedorItem.encomendasNaoEntregues,
                                                        tbPedidoFornecedorItem.idProduto,
                                                        tbProdutos.fotoDestaque
                                                        from tbPedidoFornecedorItem
                                                        join tbProdutos on tbPedidoFornecedorItem.idProduto = tbProdutos.produtoId
                                                        where tbPedidoFornecedorItem.idPedidoFornecedor = @idPedidoFornecedor
                                                        AND tbPedidoFornecedorItem.entregue = 0
                                                        group by 
                                                        tbPedidoFornecedorItem.idPedido,
                                                        tbProdutos.produtoIdDaEmpresa,
                                                        tbProdutos.complementoIdDaEmpresa,
                                                        tbProdutos.produtoNome,
                                                        tbPedidoFornecedorItem.custo,
                                                        tbPedidoFornecedorItem.diferenca,
                                                        tbPedidoFornecedorItem.entregue,
                                                        tbPedidoFornecedorItem.dataEntrega,
                                                        tbPedidoFornecedorItem.idPedidoFornecedorItem,
                                                        tbPedidoFornecedorItem.idProduto,
                                                        tbPedidoFornecedorItem.pedidosPendentes,
                                                        tbPedidoFornecedorItem.produtosNaoEnderecados,
                                                        tbPedidoFornecedorItem.encomendasNaoEntregues,
                                                        tbPedidoFornecedorItem.idProduto,
                                                        tbProdutos.fotoDestaque
                                                        order by tbProdutos.produtoNome, tbPedidoFornecedorItem.encomendasNaoEntregues">
                                    <SelectParameters>
                                        <asp:QueryStringParameter DefaultValue="0" QueryStringField="idPedidoFornecedor" Name="idPedidoFornecedor" Type="Int32" />
                                    </SelectParameters>
                                </asp:SqlDataSource>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right;">&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-top: 100px;">&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td style="border: 1px solid #7EACB1; padding-left: 15px; padding-bottom: 10px;">
                            <table cellpadding="0" cellspacing="0" style="width: 774px">
                                <tr>
                                    <td style="padding-top: 20px; padding-right: 20px;" class="rotulos">
                                        <b>Interação</b><ce:editor id="txtInteracao" runat="server"
                                            autoconfigure="Full_noform" contextmenumode="Simple"
                                            editorwysiwygmodecss="" filespath="" height="400px"
                                            urltype="Default" userelativelinks="False"
                                            width="782px"
                                            removeservernamesfromurl="False">
                                                  <TextAreaStyle CssClass="campos" />
                                                 <%-- <FrameStyle BackColor="White" BorderColor="#DDDDDD" BorderStyle="Solid" 
                                                      BorderWidth="1px" CssClass="CuteEditorFrame" Height="100%" Width="100%" />--%>
                                              </ce:editor></td>
                                </tr>
                                <tr>
                                    <td class="rotulos" valign="middle" align="right" style="padding-top: 5px; padding-bottom: 5px; padding-right: 20px;">
                                        <asp:ImageButton ID="btSalvarInteracao" runat="server" ImageUrl="images/btSalvarPeq1.jpg" OnClick="btSalvarInteracao_Click" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:DataList ID="dtlInteracao" runat="server" CellPadding="0">
                                            <ItemTemplate>
                                                <table cellpadding="0" cellspacing="0" class="rotulos" style="width: 782px">
                                                    <tr>
                                                        <td height="25" style="padding-left: 20px; width: 140px; border-top-style: solid; border-top-width: 1px; border-top-color: #7EACB1;">
                                                            <asp:Label ID="lblData" runat="server" Text='<%# Bind("data") %>'></asp:Label>
                                                        </td>
                                                        <td style="width: 200px; border-top-style: solid; border-top-width: 1px; border-top-color: #7EACB1;">
                                                            <asp:Label ID="lblusuario" runat="server"
                                                                Text='<%# Bind("usuario") %>'></asp:Label>
                                                        </td>
                                                        <td style="border-top-style: solid; border-width: 1px; border-color: #7EACB1">
                                                            <asp:Literal ID="ltrInteracao" runat="server" Text='<%# Bind("interacao") %>'></asp:Literal>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                            <HeaderTemplate>
                                                <table cellpadding="0" cellspacing="0" class="rotulos" style="width: 782px">
                                                    <tr bgcolor="#D4E3E6">
                                                        <td height="25" style="padding-left: 20px; width: 140px;">
                                                            <b>Data:</b></td>
                                                        <td style="width: 200px">
                                                            <b>Usuário:</b></td>
                                                        <td>
                                                            <b>Interação:</b></td>
                                                    </tr>
                                                </table>
                                            </HeaderTemplate>
                                        </asp:DataList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
