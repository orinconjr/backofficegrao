﻿<%@ Page Title="" Theme="Glass" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="comprasOrdensAguardandoFinanceiro.aspx.cs" Inherits="admin_comprasOrdensAguardandoFinanceiro" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dxwgv" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxPopupControl" Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>
<%@ Register Src="~/usrComprasOrdemCondicoes.ascx" TagPrefix="uc1" TagName="usrComprasOrdemCondicoes" %>


<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v11.1" namespace="DevExpress.Web.ASPxPopupControl" tagprefix="dx" %>


<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" runat="Server">
    <table cellpadding="0" cellspacing="0" style="width: 920px">
        <tr>
            <td class="tituloPaginas" valign="top">
                <asp:Label ID="lblAcao" runat="server">Ordens Aguardando Lançamento Financeiro</asp:Label>
            </td>
        </tr>
        <tr>
            <td>

                <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
                    <tr>
                        <td align="right">
                            <table cellpadding="0" cellspacing="0" style="width: 834px">
                                <tr>
                                    <td align="right">&nbsp;</td>
                                    <td height="38"
                                        style="background-image: url('images/bkExportar.jpg'); padding-bottom: 1px;"
                                        valign="bottom" width="231">
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 97px">&nbsp;</td>
                                                <td style="width: 33px">
                                                    <asp:ImageButton ID="btPdf" runat="server" ImageUrl="~/admin/images/btPdf.jpg"
                                                        Style="margin-left: 5px" OnClick="btPdf_Click" />
                                                </td>
                                                <td style="width: 31px">
                                                    <asp:ImageButton ID="btXsl" runat="server" ImageUrl="~/admin/images/btXls.jpg"
                                                        Style="margin-left: 5px" OnClick="btXsl_Click" />
                                                </td>
                                                <td style="width: 36px">
                                                    <asp:ImageButton ID="btRtf" runat="server" ImageUrl="~/admin/images/btRtf.jpg"
                                                        Style="margin-left: 5px" OnClick="btRtf_Click" />
                                                </td>
                                                <td valign="bottom">
                                                    <asp:ImageButton ID="btCsv" runat="server" ImageUrl="~/admin/images/btCsv.jpg"
                                                        Style="margin-left: 5px" OnClick="btCsv_Click" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <dxwgv:ASPxGridView ID="grd" runat="server" AutoGenerateColumns="False" 
                                KeyFieldName="idComprasOrdem" Width="834px" 
                                Cursor="auto"  ClientInstanceName="grd" EnableCallBacks="False">
                                <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                                <Styles>
                                    <Footer Font-Bold="True">
                                    </Footer>
                                </Styles>
                                <SettingsText ConfirmDelete="Tem certeza que deseja excluir?"
                                    EmptyDataRow="Nenhum registro encontrado." />
                                <SettingsPager Position="TopAndBottom" PageSize="500"
                                    ShowDisabledButtons="False" AlwaysShowPager="False" Visible="False">
                                    <Summary AllPagesText="Páginas: {0} - {1} ({2} items)"
                                        Text="Página {0} de {1} ({2} registros encontrados)" />
                                </SettingsPager>
                                <Settings ShowFilterRow="True" ShowGroupButtons="False" ShowFooter="True" />
                                <TotalSummary>
                                    <dxwgv:ASPxSummaryItem DisplayFormat="0" ShowInColumn="pedidoFornecedor" ShowInGroupFooterColumn="pedidoFornecedor" SummaryType="Count" />
                                    <dxwgv:ASPxSummaryItem FieldName="custo" ShowInColumn="custo" ShowInGroupFooterColumn="custo" SummaryType="Sum" DisplayFormat="{0:C}" />
                                </TotalSummary>
                                <SettingsEditing EditFormColumnCount="4"
                                    PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1"
                                    PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter"
                                    PopupEditFormWidth="700px" Mode="Inline" />
                                <Columns>
                                    <dxwgv:GridViewDataTextColumn Caption="ID" FieldName="idComprasOrdem">
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataDateColumn Name="dataCriacao" Caption="Data" FieldName="dataCriacao">
                                        <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit>
                                    </dxwgv:GridViewDataDateColumn>
                                    <dxwgv:GridViewDataComboBoxColumn Caption="Criado Por" FieldName="idUsuarioCadastroOrdem">
                                        <PropertiesComboBox DataSourceID="sqlUsuarios" TextField="usuarioNome" ValueField="usuarioId">
                                        </PropertiesComboBox>
                                        <Settings AllowAutoFilter="True" AllowAutoFilterTextInputTimer="True" FilterMode="DisplayText" />
                                    </dxwgv:GridViewDataComboBoxColumn>
                                    <dxwgv:GridViewDataComboBoxColumn Caption="Para uso em" FieldName="idComprasEmpresa">
                                        <PropertiesComboBox DataSourceID="sqlComprasEmpresa" TextField="empresa" ValueField="idComprasEmpresa">
                                        </PropertiesComboBox>
                                        <Settings AllowAutoFilter="True" AllowAutoFilterTextInputTimer="True" FilterMode="DisplayText" />
                                    </dxwgv:GridViewDataComboBoxColumn>
                                    <dxwgv:GridViewDataComboBoxColumn Caption="Fornecedor" FieldName="idComprasFornecedor">
                                        <PropertiesComboBox DataSourceID="sqlComprasFornecedor" TextField="fornecedor" ValueField="idComprasFornecedor">
                                        </PropertiesComboBox>
                                        <Settings AllowAutoFilter="True" AllowAutoFilterTextInputTimer="True" FilterMode="DisplayText" />
                                    </dxwgv:GridViewDataComboBoxColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Itens" FieldName="itens">
                                    </dxwgv:GridViewDataTextColumn>
                                     <dxwgv:GridViewDataTextColumn Caption="Valor" FieldName="valor">
                                        <PropertiesTextEdit DisplayFormatString="C">
                                        </PropertiesTextEdit>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataComboBoxColumn Caption="Cond. Pag" FieldName="colCondPag" Width="200">
                                        <PropertiesComboBox DataSourceID="sqlCondPag" TextField="nome" ValueField="idComprasCondicaoPagamento">
                                        </PropertiesComboBox>
                                        <Settings AllowAutoFilter="True" AllowAutoFilterTextInputTimer="True" FilterMode="DisplayText" />
                                    </dxwgv:GridViewDataComboBoxColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Finalizar" Name="finalizar" Width="80" Visible="True">
                                        <DataItemTemplate>
                                            <asp:LinkButton runat="server" OnCommand="btnVerCondicoes_OnCommand" ID="btnVerCondicoes" CommandArgument='<%# Eval("idComprasOrdem") %>' EnableViewState="False">
                                                Ver Condições
                                            </asp:LinkButton>
                                        </DataItemTemplate>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Exportar" Name="exportar" Width="80" Visible="True">
                                        <DataItemTemplate>
                                            <asp:LinkButton runat="server" OnCommand="btnExportar_OnCommand" OnClientClick="aspnetForm.target ='_blank'" ID="btnExportar" CommandArgument='<%# Eval("idComprasOrdem") %>' EnableViewState="False">
                                                Exportar
                                            </asp:LinkButton>
                                        </DataItemTemplate>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Finalizar" Name="finalizar" Width="80" Visible="True">
                                        <DataItemTemplate>
                                            <asp:LinkButton runat="server" OnCommand="btnEnviarAprovacao_OnCommand" ID="btnEnviarAprovacao" OnClientClick="return confirm('Deseja realmente marcar este pedido como Lançado?')"  CommandArgument='<%# Eval("idComprasOrdem") %>' EnableViewState="False">
                                                Marcar como Lançado
                                            </asp:LinkButton>
                                        </DataItemTemplate>
                                    </dxwgv:GridViewDataTextColumn>
                                </Columns>
                                <StylesEditors>
                                    <Label Font-Bold="True">
                                    </Label>
                                </StylesEditors>
                            </dxwgv:ASPxGridView>
                            <dxwgv:ASPxGridViewExporter ID="grdEx" runat="server" FileName="relatorio"
                                GridViewID="grd" Landscape="True" PreserveGroupRowStates="True">
                            </dxwgv:ASPxGridViewExporter>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <asp:LinqDataSource ID="sqlComprasFornecedor" runat="server" ContextTypeName="dbCommerceDataContext" TableName="tbComprasFornecedors">
    </asp:LinqDataSource>
    <asp:LinqDataSource ID="sqlComprasEmpresa" runat="server" ContextTypeName="dbCommerceDataContext" TableName="tbComprasEmpresas">
    </asp:LinqDataSource>
    <asp:LinqDataSource ID="sqlUsuarios" runat="server" ContextTypeName="dbCommerceDataContext" TableName="tbUsuarios">
    </asp:LinqDataSource>
      <asp:LinqDataSource ID="sqlCondPag" runat="server" ContextTypeName="dbCommerceDataContext" TableName="tbComprasCondicaoPagamentos">
    </asp:LinqDataSource>
    
    
    <dx:ASPxPopupControl ID="popCondicoesPagamento" runat="server" CloseAction="CloseButton" Modal="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
        ClientInstanceName="popCondicoesPagamento" HeaderText="Condições de Pagamento" AllowDragging="True" PopupAnimationType="None" EnableViewState="True" Height="75" AutoUpdatePosition="True">
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl10" runat="server">
                <div style="width: 800px; height: 500px;">
                    <uc1:usrComprasOrdemCondicoes runat="server" ID="usrComprasOrdemCondicoes" />
                </div>
            </dx:PopupControlContentControl>
        </ContentCollection>
        <ContentStyle>
            <Paddings PaddingBottom="5px" />
        </ContentStyle>
    </dx:ASPxPopupControl>
</asp:Content>