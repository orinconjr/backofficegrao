﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxGridView;
using Telerik.Web.UI;

public partial class admin_enderecamentoEmLote : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtNumeroRomaneio.Attributes.Add("onkeypress", "return soNumero(event);");
            txtNumetoEtiqueta.Attributes.Add("onkeypress", "return soNumero(event);");
            txtNumRomaneioEntrada.Attributes.Add("onkeypress", "return soNumero(event);");

            using (var data = new dbCommerceDataContext())
            {
                var areas = (from c in data.tbEnderecamentoAreas select c).ToList();

                ddlArea.Items.Clear();
                ddlArea.Items.Add(new ListItem("Selecione", "0"));

                foreach (var area in areas)
                {
                    ddlArea.Items.Add(new ListItem(area.area, area.idEnderecamentoArea.ToString()));
                }
            }

            Session["auxDataList"] = null;

            FillDropDownCarrinhos();
        }
        else
        {
            FillGrid();
        }
    }

    protected void imbPesquisar_OnClick(object sender, ImageClickEventArgs e)
    {

        if (rblTipoEnderecamento.SelectedValue == "1")
        {

            if (txtNumeroRomaneio.Text == "" | txtNumetoEtiqueta.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(),
                    "alert('Informe o romaneio e o numero de uma etiqueta pertencente ao mesmo!')", true);
                return;
            }

            int romaneio = Convert.ToInt32(txtNumeroRomaneio.Text);
            int etiqueta = Convert.ToInt32(txtNumetoEtiqueta.Text);

            using (var data = new dbCommerceDataContext())
            {
                var itemPedidoFornecedor = (from c in data.tbPedidoFornecedorItems
                                            where
                                                c.dataEntrega != null && c.liberado == null && c.idPedidoFornecedor == romaneio &&
                                                c.idPedidoFornecedorItem == etiqueta
                                            select c).FirstOrDefault();

                if (itemPedidoFornecedor != null)
                {
                    var itensPedidoFornecedor = (from c in data.tbPedidoFornecedorItems
                                                 join p in data.tbProdutos on c.idProduto equals p.produtoId
                                                 where
                                                     c.dataEntrega != null && c.liberado == null && c.idPedidoFornecedor == romaneio &&
                                                     c.idProduto == itemPedidoFornecedor.idProduto
                                                 select new { c.idPedidoFornecedorItem, produto = p.produtoNome });

                    grd.DataSource = itensPedidoFornecedor.ToList();
                    grd.DataBind();

                    Session["auxDataList"] = itensPedidoFornecedor;
                    pnlEndereco.Visible = true;
                }
                else
                {
                    pnlEndereco.Visible = false;
                    ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(),
                        "alert('O romaneio não foi encontrado!')", true);

                    grd.DataSource = null;
                    grd.DataBind();
                }
            }
        }
        else
        {
            int idEnderecamentoTransferencia = Convert.ToInt32(ddlCarrinhos.SelectedValue);

            using (var data = new dbCommerceDataContext())
            {
                var tranferenciaEndereco = (from c in data.tbTransferenciaEnderecos
                                            where
                                                c.dataFim == null && c.idUsuarioExpedicao != null && c.idEnderecamentoTransferencia == idEnderecamentoTransferencia
                                            select c).FirstOrDefault();

                if (tranferenciaEndereco != null)
                {
                    var itensPedidoFornecedor = (from c in data.tbPedidoFornecedorItems
                                                 join p in data.tbProdutos on c.idProduto equals p.produtoId
                                                 join t in data.tbTransferenciaEnderecoProdutos on c.idPedidoFornecedorItem equals t.idPedidoFornecedorItem
                                                 where
                                                     c.dataEntrega != null && (c.liberado == null | c.liberado == true) && t.idTransferenciaEndereco == tranferenciaEndereco.idTransferenciaEndereco
                                                 select new { c.idPedidoFornecedorItem, produto = p.produtoNome });

                    grd.DataSource = itensPedidoFornecedor.ToList();
                    grd.DataBind();

                    Session["auxDataList"] = itensPedidoFornecedor;
                    pnlEndereco.Visible = true;
                }
                else
                {
                    pnlEndereco.Visible = false;
                    ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(),
                        "alert('O carrinho não foi encontrado!')", true);

                    grd.DataSource = null;
                    grd.DataBind();
                }
            }
        }
    }

    protected void grd_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        int inicio = 0;
        int fim = 0;

        if (grd.PageIndex == 0)
        {
            inicio = 0;
            fim = inicio + grd.GetCurrentPageRowValues("idPedidoFornecedorItem").Count;
        }
        else
        {
            inicio += inicio + grd.SettingsPager.PageSize * grd.PageIndex;
            fim += inicio + grd.GetCurrentPageRowValues("idPedidoFornecedorItem").Count;
        }

        using (var data = new dbCommerceDataContext())
        {
            for (int i = inicio; i < fim; i++)
            {
                HiddenField txtProdutoId = (HiddenField)grd.FindRowCellTemplateControl(i, grd.Columns["idPedidoFornecedorItem"] as GridViewDataColumn, "hfIdPedidoFornecedorItem");
            }
        }


    }

    protected void rblTipoEnderecamento_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        if (rblTipoEnderecamento.SelectedValue == "1")
        {
            pnlEnderecamentoPorRomaneio.Visible = true;
            pnlEnderecamentoPorCarrinho.Visible = false;
        }
        else
        {
            pnlEnderecamentoPorRomaneio.Visible = false;
            pnlEnderecamentoPorCarrinho.Visible = true;
        }
    }

    protected void ddlArea_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        int idArea = Convert.ToInt32(ddlArea.SelectedValue);
        using (var data = new dbCommerceDataContext())
        {
            var ruas = (from c in data.tbEnderecamentoRuas where c.idEnderecamentoArea == idArea select c).ToList();

            ddlPredio.Enabled =
            ddlAndar.Enabled = false;
            btnEnderecar.Visible = false;

            ddlPredio.Items.Clear();
            ddlAndar.Items.Clear();

            ddlRua.Items.Clear();
            ddlRua.Items.Add(new ListItem("Selecione", "0"));

            foreach (var rua in ruas)
            {
                ddlRua.Items.Add(new ListItem(rua.rua, rua.idEnderecamentoRua.ToString()));
            }

            ddlRua.Enabled = true;
        }
    }

    protected void ddlRua_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        int idRua = Convert.ToInt32(ddlRua.SelectedValue);

        ddlPredio.Enabled = true;
        ddlPredio.Items.Clear();
        ddlPredio.Items.Add(new ListItem("Selecione", "0"));

        ddlAndar.Enabled = false;
        ddlAndar.Items.Clear();

        btnEnderecar.Visible = false;

        using (var data = new dbCommerceDataContext())
        {
            var predios = (from c in data.tbEnderecamentoPredios
                           join lado in data.tbEnderecamentoLados on c.idEnderecamentoLado equals lado.idEnderecamentoLado
                           where c.idEnderecamentoRua == idRua
                           select new { c.predio, c.idEnderecamentoPredio, lado.lado }).ToList();

            foreach (var predio in predios)
            {
                ddlPredio.Items.Add(new ListItem(predio.predio + " lado: " + predio.lado, predio.idEnderecamentoPredio.ToString()));
            }
        }
    }

    protected void ddlPredio_OnSelectedIndexChanged(object sender, EventArgs e)
    {

        int idPredio = Convert.ToInt32(ddlPredio.SelectedValue);
        using (var data = new dbCommerceDataContext())
        {
            var andares = (from c in data.tbEnderecamentoAndars where c.idEnderecamentoPredio == idPredio select c).ToList();

            ddlAndar.Enabled = true;
            ddlAndar.Items.Clear();
            ddlAndar.Items.Add(new ListItem("Selecione", "0"));

            foreach (var andar in andares)
            {
                ddlAndar.Items.Add(new ListItem(andar.andar, andar.idEnderecamentoAndar.ToString()));
            }

            btnEnderecar.Visible = true;
        }
    }

    protected void FillGrid()
    {
        if (Session["auxDataList"] != null)
        {
            //tbPedidoFornecedorItem lista = (tbPedidoFornecedorItem)Session["auxDataList"];

            //grd.DataSource = lista;
            //grd.DataBind();
        }

        if (!string.IsNullOrEmpty(txtNumRomaneioEntrada.Text))
        {
            FillGridItensSemEntrada();
        }
    }

    private void FillDropDownCarrinhos()
    {
        using (var data = new dbCommerceDataContext())
        {
            var carrinhos = (from c in data.tbEnderecamentoTransferencias orderby c.enderecamentoTransferencia select c).ToList();

            ddlCarrinhos.Items.Clear();
            foreach (var carrinho in carrinhos)
            {
                ddlCarrinhos.Items.Add(new ListItem(carrinho.enderecamentoTransferencia, carrinho.idEnderecamentoTransferencia.ToString()));
            }


            ddlCarrinhosEntrada.Items.Clear();
            foreach (var carrinho in carrinhos)
            {
                ddlCarrinhosEntrada.Items.Add(new ListItem(carrinho.enderecamentoTransferencia, carrinho.idEnderecamentoTransferencia.ToString()));
            }
        }
    }

    protected void btnEnderecar_OnClick(object sender, EventArgs e)
    {
        int inicio = 0;
        int fim = 0;
        int idTransfereciaEndereco = 0;
        int area = Convert.ToInt32(ddlArea.SelectedValue);
        int rua = Convert.ToInt32(ddlRua.SelectedValue);
        int predio = Convert.ToInt32(ddlPredio.SelectedValue);
        int andar = Convert.ToInt32(ddlAndar.SelectedValue);
        bool carrinhoLiberato = false;
        string carrinho = "";

        ((Literal)updateProgress.FindControl("litInfoLoad")).Text = "Endereçando produtos";

        if (rblTipoEnderecamento.SelectedValue == "1")
        {
            if (string.IsNullOrEmpty(txtNumeroRomaneio.Text) | string.IsNullOrEmpty(txtNumetoEtiqueta.Text))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('Informe um romaneio e uma etiqueta do mesmo!')", true);
                return;
            }
        }

        if (area == 0 | rua == 0 | predio == 0 | andar == 0)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('Selecione o endereço corretamente!')", true);
            return;
        }

        if (grd.PageIndex == 0)
        {
            inicio = 0;
            fim = inicio + grd.GetCurrentPageRowValues("idPedidoFornecedorItem").Count;
        }
        else
        {
            inicio += inicio + grd.SettingsPager.PageSize * grd.PageIndex;
            fim += inicio + grd.GetCurrentPageRowValues("idPedidoFornecedorItem").Count;
        }

        int totalDeLinhas = grd.VisibleRowCount;

        try
        {

            if (rblTipoEnderecamento.SelectedValue == "1" && totalDeLinhas > 50)
            {

                if (txtNumeroRomaneio.Text == "" | txtNumetoEtiqueta.Text == "")
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(),
                        "alert('Informe o romaneio e o numero de uma etiqueta pertencente ao mesmo!')", true);
                    return;
                }

                int romaneio = Convert.ToInt32(txtNumeroRomaneio.Text);
                int etiqueta = Convert.ToInt32(txtNumetoEtiqueta.Text);

                using (var data = new dbCommerceDataContext())
                {
                    var itemPedidoFornecedor = (from c in data.tbPedidoFornecedorItems
                                                where
                                                    c.dataEntrega != null && c.liberado == null && c.idPedidoFornecedor == romaneio &&
                                                    c.idPedidoFornecedorItem == etiqueta
                                                select c).FirstOrDefault();


                    var itensPedidoFornecedor = (from c in data.tbPedidoFornecedorItems
                                                 join p in data.tbProdutos on c.idProduto equals p.produtoId
                                                 where
                                                     c.dataEntrega != null && c.liberado == null && c.idPedidoFornecedor == romaneio &&
                                                     c.idProduto == itemPedidoFornecedor.idProduto
                                                 select new { c.idPedidoFornecedorItem, produto = p.produtoNome });

                    foreach (var item in itensPedidoFornecedor)
                    {
                        var produtoEstoque =
                            (from c in data.tbProdutoEstoques where c.idPedidoFornecedorItem == item.idPedidoFornecedorItem select c)
                                .FirstOrDefault();

                        if (produtoEstoque != null)
                        {
                            var areaDetalhe = (from c in data.tbEnderecamentoAreas where c.idEnderecamentoArea == area select c).First();
                            produtoEstoque.idEnderecamentoArea = area;
                            produtoEstoque.idEnderecamentoRua = rua;
                            produtoEstoque.idEnderecamentoPredio = predio;
                            produtoEstoque.idEnderecamentoAndar = andar;
                            produtoEstoque.liberado = true;
                            produtoEstoque.idCentroDistribuicao = (areaDetalhe.idCentroDistribuicao ?? 4);


                            tbQueue queue = new tbQueue
                            {
                                tipoQueue = 21,
                                agendamento = DateTime.Now.AddSeconds(7),
                                idRelacionado = produtoEstoque.produtoId,
                                mensagem = "",
                                concluido = false,
                                andamento = false
                            };


                            data.tbQueues.InsertOnSubmit(queue);

                            data.SubmitChanges();

                        }

                        var pedidoFornecedorItem =
                            (from c in data.tbPedidoFornecedorItems
                             where c.idPedidoFornecedorItem == item.idPedidoFornecedorItem
                             select c).FirstOrDefault();

                        if (pedidoFornecedorItem != null)
                        {
                            //pedidoFornecedorItem.entregue = true;
                            //pedidoFornecedorItem.dataEntrega = DateTime.Now;
                            pedidoFornecedorItem.liberado = true;
                            data.SubmitChanges();
                        }

                        var transfEnderecoProduto =
                            (from c in data.tbTransferenciaEnderecoProdutos
                             where c.idPedidoFornecedorItem == item.idPedidoFornecedorItem && c.dataEnderecamento == null
                             select c).FirstOrDefault();

                        if (transfEnderecoProduto != null)
                        {
                            idTransfereciaEndereco = transfEnderecoProduto.idTransferenciaEndereco;
                            transfEnderecoProduto.dataEnderecamento = DateTime.Now;
                            data.SubmitChanges();
                        }

                        var transferenciaEndereco =
                            (from c in data.tbTransferenciaEnderecos
                             where c.idTransferenciaEndereco == idTransfereciaEndereco
                             select c).FirstOrDefault();

                        if (transferenciaEndereco != null)
                        {
                            var faltaEnderecar =
                                (from c in data.tbTransferenciaEnderecoProdutos
                                 where c.idTransferenciaEndereco == transferenciaEndereco.idTransferenciaEndereco && c.dataEnderecamento == null
                                 select c).Count();

                            if (faltaEnderecar == 0)
                            {
                                transferenciaEndereco.dataFim = DateTime.Now;
                                data.SubmitChanges();
                                carrinhoLiberato = true;
                                carrinho = transferenciaEndereco.tbEnderecamentoTransferencia.enderecamentoTransferencia;
                            }
                        }
                    }

                    ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('Produtos endereçados com sucesso!" + (carrinhoLiberato ? "\\nCarrinho " + carrinho + " liberado!" : "") + "')", true);
                    grd.DataSource = null;
                    grd.DataBind();

                }
            }
            else
            {
                using (var data = new dbCommerceDataContext())
                {

                    if (rblTipoEnderecamento.SelectedValue == "2" && totalDeLinhas > 50)
                    {
                        int idEnderecamentoTransferencia = Convert.ToInt32(ddlCarrinhos.SelectedValue);

                        var tranferenciaEndereco = (from c in data.tbTransferenciaEnderecos
                                                    where
                                                        c.dataFim == null && c.idUsuarioExpedicao != null && c.idEnderecamentoTransferencia == idEnderecamentoTransferencia
                                                    select c).FirstOrDefault();

                        if (tranferenciaEndereco != null)
                        {
                            var itensPedidoFornecedor = (from c in data.tbPedidoFornecedorItems
                                                         join p in data.tbProdutos on c.idProduto equals p.produtoId
                                                         join t in data.tbTransferenciaEnderecoProdutos on c.idPedidoFornecedorItem equals t.idPedidoFornecedorItem
                                                         where
                                                             c.dataEntrega != null && (c.liberado == null | c.liberado == true) && t.idTransferenciaEndereco == tranferenciaEndereco.idTransferenciaEndereco
                                                         select new { c.idPedidoFornecedorItem }).ToList();

                            foreach (var item in itensPedidoFornecedor)
                            {
                                var produtoEstoque =
                                (from c in data.tbProdutoEstoques where c.idPedidoFornecedorItem == item.idPedidoFornecedorItem select c)
                                    .FirstOrDefault();

                                if (produtoEstoque != null)
                                {

                                    var areaDetalhe = (from c in data.tbEnderecamentoAreas where c.idEnderecamentoArea == area select c).First();
                                    produtoEstoque.idEnderecamentoArea = area;
                                    produtoEstoque.idEnderecamentoRua = rua;
                                    produtoEstoque.idEnderecamentoPredio = predio;
                                    produtoEstoque.idEnderecamentoAndar = andar;
                                    produtoEstoque.liberado = true;
                                    produtoEstoque.idCentroDistribuicao = (areaDetalhe.idCentroDistribuicao ?? 4);                           

                                    tbQueue queue = new tbQueue
                                    {
                                        tipoQueue = 21,
                                        agendamento = DateTime.Now.AddSeconds(7),
                                        idRelacionado = produtoEstoque.produtoId,
                                        mensagem = "",
                                        concluido = false,
                                        andamento = false
                                    };


                                    data.tbQueues.InsertOnSubmit(queue);

                                    data.SubmitChanges();

                                }

                                var pedidoFornecedorItem =
                                (from c in data.tbPedidoFornecedorItems
                                 where c.idPedidoFornecedorItem == item.idPedidoFornecedorItem
                                 select c).FirstOrDefault();

                                if (pedidoFornecedorItem != null)
                                {
                                    //pedidoFornecedorItem.entregue = true;
                                    //pedidoFornecedorItem.dataEntrega = DateTime.Now;
                                    pedidoFornecedorItem.liberado = true;
                                    data.SubmitChanges();
                                }

                                var transfEnderecoProduto =
                                    (from c in data.tbTransferenciaEnderecoProdutos
                                     where c.idPedidoFornecedorItem == item.idPedidoFornecedorItem && c.dataEnderecamento == null
                                     select c).FirstOrDefault();

                                if (transfEnderecoProduto != null)
                                {
                                    idTransfereciaEndereco = transfEnderecoProduto.idTransferenciaEndereco;
                                    transfEnderecoProduto.dataEnderecamento = DateTime.Now;
                                    data.SubmitChanges();
                                }

                                var transferenciaEndereco =
                                    (from c in data.tbTransferenciaEnderecos
                                     where c.idTransferenciaEndereco == idTransfereciaEndereco
                                     select c).FirstOrDefault();

                                if (transferenciaEndereco != null)
                                {
                                    var faltaEnderecar =
                                        (from c in data.tbTransferenciaEnderecoProdutos
                                         where c.idTransferenciaEndereco == transferenciaEndereco.idTransferenciaEndereco && c.dataEnderecamento == null
                                         select c).Count();

                                    if (faltaEnderecar == 0)
                                    {
                                        transferenciaEndereco.dataFim = DateTime.Now;
                                        data.SubmitChanges();
                                        carrinhoLiberato = true;
                                        carrinho = transferenciaEndereco.tbEnderecamentoTransferencia.enderecamentoTransferencia;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        for (int i = inicio; i < totalDeLinhas; i++)
                        {
                            HiddenField hfIdPedidoFornecedorItem = (HiddenField)grd.FindRowCellTemplateControl(i, grd.Columns["idPedidoFornecedorItem"] as GridViewDataColumn, "hfIdPedidoFornecedorItem");
                            int idPedidoFornecedorItem = Convert.ToInt32(hfIdPedidoFornecedorItem.Value);

                            var produtoEstoque =
                                (from c in data.tbProdutoEstoques where c.idPedidoFornecedorItem == idPedidoFornecedorItem select c)
                                    .FirstOrDefault();

                            if (produtoEstoque != null)
                            {
                                var areaDetalhe = (from c in data.tbEnderecamentoAreas where c.idEnderecamentoArea == area select c).First();
                                produtoEstoque.idEnderecamentoArea = area;
                                produtoEstoque.idEnderecamentoRua = rua;
                                produtoEstoque.idEnderecamentoPredio = predio;
                                produtoEstoque.idEnderecamentoAndar = andar;
                                produtoEstoque.liberado = true;
                                produtoEstoque.idCentroDistribuicao = (areaDetalhe.idCentroDistribuicao ?? 4);


                                tbQueue queue = new tbQueue
                                {
                                    tipoQueue = 21,
                                    agendamento = DateTime.Now.AddSeconds(7),
                                    idRelacionado = produtoEstoque.produtoId,
                                    mensagem = "",
                                    concluido = false,
                                    andamento = false
                                };


                                data.tbQueues.InsertOnSubmit(queue);

                                data.SubmitChanges();

                            }

                            var pedidoFornecedorItem =
                                (from c in data.tbPedidoFornecedorItems
                                 where c.idPedidoFornecedorItem == idPedidoFornecedorItem
                                 select c).FirstOrDefault();

                            if (pedidoFornecedorItem != null)
                            {
                                //pedidoFornecedorItem.entregue = true;
                                //pedidoFornecedorItem.dataEntrega = DateTime.Now;
                                pedidoFornecedorItem.liberado = true;
                                data.SubmitChanges();
                            }

                            var transfEnderecoProduto =
                                (from c in data.tbTransferenciaEnderecoProdutos
                                 where c.idPedidoFornecedorItem == idPedidoFornecedorItem && c.dataEnderecamento == null
                                 select c).FirstOrDefault();

                            if (transfEnderecoProduto != null)
                            {
                                idTransfereciaEndereco = transfEnderecoProduto.idTransferenciaEndereco;
                                transfEnderecoProduto.dataEnderecamento = DateTime.Now;
                                data.SubmitChanges();
                            }

                            var transferenciaEndereco =
                                (from c in data.tbTransferenciaEnderecos
                                 where c.idTransferenciaEndereco == idTransfereciaEndereco
                                 select c).FirstOrDefault();

                            if (transferenciaEndereco != null)
                            {
                                var faltaEnderecar =
                                    (from c in data.tbTransferenciaEnderecoProdutos
                                     where c.idTransferenciaEndereco == transferenciaEndereco.idTransferenciaEndereco && c.dataEnderecamento == null
                                     select c).Count();

                                if (faltaEnderecar == 0)
                                {
                                    transferenciaEndereco.dataFim = DateTime.Now;
                                    data.SubmitChanges();
                                    carrinhoLiberato = true;
                                    carrinho = transferenciaEndereco.tbEnderecamentoTransferencia.enderecamentoTransferencia;
                                }
                            }
                        }
                    }



                    ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('Produtos endereçados com sucesso!" + (carrinhoLiberato ? "\\nCarrinho " + carrinho + " liberado!" : "") + "')", true);
                    grd.DataSource = null;
                    grd.DataBind();
                }
            }

        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('ERRO: " + ex.Message + "')", true);
        }


    }


    protected void ibtnPesquisarItensRomaneioEntrada_OnClick(object sender, ImageClickEventArgs e)
    {
        FillGridItensSemEntrada();
    }

    protected void FillGridItensSemEntrada()
    {
        if (string.IsNullOrEmpty(txtNumRomaneioEntrada.Text)) return;

        try
        {
            int numeroRomaneio = Convert.ToInt32(txtNumRomaneioEntrada.Text);

            using (var data = new dbCommerceDataContext())
            {
                var itensSemEntrada = (from c in data.tbPedidoFornecedorItems
                                       where c.idPedidoFornecedor == numeroRomaneio && !c.entregue && c.dataEntrega == null
                                       orderby c.idPedidoFornecedorItem
                                       select new { c.idPedidoFornecedorItem, produto = c.tbProduto.produtoNome }).ToList();

                grdItemPedidoFornecedor.DataSource = itensSemEntrada;
                grdItemPedidoFornecedor.DataBind();

                grdItemPedidoFornecedor.Visible =
                btnRegistrarEntradaRomaneio.Visible = itensSemEntrada.Any();

                if (!itensSemEntrada.Any())
                    ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('Não há itens sem entrada neste romaneio');", true);
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('ERRO: " + ex.Message + "')", true);
        }
    }

    protected void btnRegistrarEntradaRomaneio_OnClick(object sender, EventArgs e)
    {
        try
        {
            ((Literal)updateProgress.FindControl("litInfoLoad")).Text = "Registrando Entrada de Romaneio";

            var pedidoFornecedorWs = new PedidosFornecedorWs();
            HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
            usuarioLogadoId = Request.Cookies["usuarioLogadoId"];
            int idUsuario = Convert.ToInt32(usuarioLogadoId.Value.ToString());
            using (var data = new dbCommerceDataContext())
            {
                int numeroRomaneio = Convert.ToInt32(txtNumRomaneioEntrada.Text);
                int idEnderecamentoTransferencia = Convert.ToInt32(ddlCarrinhosEntrada.SelectedValue);
                var itensSemEntrada = (from c in data.tbPedidoFornecedorItems
                                       where c.idPedidoFornecedor == numeroRomaneio && !c.entregue && c.dataEntrega == null
                                       orderby c.idPedidoFornecedorItem
                                       select c).ToList();

                foreach (var item in itensSemEntrada)
                {
                    pedidoFornecedorWs.RegistrarEntradaRomaneioComEndereco(item.idPedidoFornecedorItem, idUsuario, idEnderecamentoTransferencia, "glmp152029");
                    /*item.entregue = true;
                    item.dataEntrega = DateTime.Now;
                    data.SubmitChanges();*/
                }
            }

            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('Entrada de romaneio registrada com sucesso!')", true);

            grdItemPedidoFornecedor.DataSource = null;
            grdItemPedidoFornecedor.DataBind();
        }
        catch (Exception)
        {

            throw;
        }

    }
}

