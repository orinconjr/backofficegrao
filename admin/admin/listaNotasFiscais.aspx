﻿<%@ Page Title="" Language="C#" Theme="Glass" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="listaNotasFiscais.aspx.cs" Inherits="admin_listaNotasFiscais" %>

<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register TagPrefix="dxe" Namespace="DevExpress.Web.ASPxMenu" Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>

<%@ Register Assembly="DevExpress.Web.v11.1" Namespace="DevExpress.Web.ASPxMenu" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" runat="Server">
    <script language="javascript" type="text/javascript">
        function ApplyFilter(dde, dateFrom, dateTo) {
            var d1 = dateFrom.GetText();
            var d2 = dateTo.GetText();
            if (d1 == "" || d2 == "")
                return;
            dde.SetText(d1 + "|" + d2);
            //grd.ApplyFilter("[dataHoraDoPedido] >= '" + d1 + " 00:00:00' && [dataHoraDoPedido] <= '" + d2 + " 23:59:59'");
            grd.AutoFilterByColumn("dataSendoEmbalado", dde.GetText());
        }

        function OnDropDown(s, dateFrom, dateTo) {
            var str = s.GetText();
            if (str == "") {
                // default date (1950-1961 for demo)     
                dateFrom.SetDate(new Date(1950, 0, 1));
                dateTo.SetDate(new Date(1960, 11, 31));
                return;
            }
            var d = str.split("|");
            dateFrom.SetText(d[0]);
            dateTo.SetText(d[1]);
        }

        function grid_ContextMenu(s, e) {
            if (e.objectType == "header")
                pmColumnMenu.ShowAtPos(ASPxClientUtils.GetEventX(e.htmlEvent), ASPxClientUtils.GetEventY(e.htmlEvent));
        }
    </script>


    <dxe:ASPxPopupMenu ID="pmColumnMenu" runat="server" ClientInstanceName="pmColumnMenu">
        <Items>
            <dxe:MenuItem Name="cmdShowCustomization" Text="Escolher colunas"></dxe:MenuItem>

        </Items>
        <ClientSideEvents ItemClick="function(s, e) { if(e.item.name == 'cmdShowCustomization') grd.ShowCustomizationWindow(); }" />
    </dxe:ASPxPopupMenu>

    <table cellpadding="0" cellspacing="0" style="width: 920px">
        <tr>
            <td class="tituloPaginas" valign="top">
                <asp:Label ID="lblAcao" runat="server">Lista de Notas Fiscais <span style="font:bold 13px tahoma">&nbsp;(Dados de Notas disponíveis : a partir de 01/06/2017)</span></asp:Label>
                
            </td>
        </tr>
        <tr>
            <td>

                <table align="center" cellpadding="0" cellspacing="0" style="width: 734px">
                    <tr>
                        <td>
                            <div style="float: left; border: 1px solid #90c3c3;">
                                <table>
                                    <tr>
                                        <td>Data Inicial:
                                        </td>
                                        <td>Data Final:
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox runat="server" ID="txtDtInicial" Width="75px" ValidationGroup="idt" MaxLength="10"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rqvDataInicial" runat="server"
                                                ControlToValidate="txtDtInicial" Display="None"
                                                ErrorMessage="Preencha a data inicial." SetFocusOnError="True" ValidationGroup="idt">
                                            </asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="rgeDataInicial" runat="server"
                                                ControlToValidate="txtDtInicial" Display="None"
                                                ErrorMessage="Por favor, preencha corretamente a data inicial."
                                                SetFocusOnError="True"
                                                ValidationExpression="^(([0-2]\d|[3][0-1])\/([0]\d|[1][0-2])\/[1-2][0-9]\d{2})$" ValidationGroup="idt"></asp:RegularExpressionValidator>
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" ID="txtDtFinal" Width="75px" ValidationGroup="idt" MaxLength="10"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rqvDataFinal" runat="server"
                                                ControlToValidate="txtDtFinal" Display="None"
                                                ErrorMessage="Preencha a data final." SetFocusOnError="True" ValidationGroup="idt"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="rgeDataInicial0" runat="server"
                                                ControlToValidate="txtDtFinal" Display="None"
                                                ErrorMessage="Por favor, preencha corretamente a data final"
                                                SetFocusOnError="True"
                                                ValidationExpression="^(([0-2]\d|[3][0-1])\/([0]\d|[1][0-2])\/[1-2][0-9]\d{2})$" ValidationGroup="idt"></asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <asp:Button runat="server" ID="btnInclusaoDefeitoTransp_InclusaoSinistro" Width="163" Text="Exportar" ValidationGroup="idt" ToolTip="Exportar relatório" OnClick="btnInclusaoDefeitoTransp_InclusaoSinistro_OnClick" />
                                        </td>
                                    </tr>
                                </table>

                                <asp:ValidationSummary ID="llds" runat="server" DisplayMode="List" ShowMessageBox="True" ShowSummary="False" ValidationGroup="idt" />
                            </div>
                            <div style="float: left; border-bottom: 1px solid #90c3c3; padding-top: 30px; font-family: Tahoma; font-size: 12px;">
                                <b>
                                    <asp:RadioButtonList runat="server" ID="rblMotivoInclusao" RepeatDirection="Horizontal">
                                        <asp:ListItem Text="Exportar (Inclusão por defeito - transporte)" Value="inclusao por defeito transporte"></asp:ListItem>
                                        <asp:ListItem Text="Exportar (Inclusão por sinistro)" Value="inclusao por Sinistro"></asp:ListItem>
                                    </asp:RadioButtonList>
                                    <asp:RequiredFieldValidator ID="rqvNotivoInclusao" runat="server"
                                        ControlToValidate="rblMotivoInclusao" Display="None"
                                        ErrorMessage="Selecione uma opção de relatório." SetFocusOnError="True" ValidationGroup="idt"></asp:RequiredFieldValidator>
                                </b>
                            </div>
                            <div style="float: right; margin-top: 32px;">
                                <table cellpadding="0" cellspacing="0" style="width: 734px">
                                    <tr>
                                        <td align="right">&nbsp;</td>
                                        <td height="38"
                                            style="background-image: url('images/bkExportar.jpg'); padding-bottom: 1px;"
                                            valign="bottom" width="231">
                                            <table cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td style="width: 97px">&nbsp;</td>
                                                    <td style="width: 33px">
                                                        <asp:ImageButton ID="btPdf" runat="server" ImageUrl="~/admin/images/btPdf.jpg" OnClick="btPdf_Click"
                                                            Style="margin-left: 5px" />
                                                    </td>
                                                    <td style="width: 31px">
                                                        <asp:ImageButton ID="btXsl" runat="server" ImageUrl="~/admin/images/btXls.jpg" OnClick="btXsl_Click"
                                                            Style="margin-left: 5px" />
                                                    </td>
                                                    <td style="width: 36px">
                                                        <asp:ImageButton ID="btRtf" runat="server" ImageUrl="~/admin/images/btRtf.jpg" OnClick="btRtf_Click"
                                                            Style="margin-left: 5px" />
                                                    </td>
                                                    <td valign="bottom">
                                                        <asp:ImageButton ID="btCsv" runat="server" ImageUrl="~/admin/images/btCsv.jpg" OnClick="btCsv_Click"
                                                            Style="margin-left: 5px" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Panel runat="server" ID="pnGridVerPedidos">
                                <dxwgv:ASPxGridView ID="grd" ClientInstanceName="grd" runat="server" AutoGenerateColumns="False"
                                    OnAutoFilterCellEditorCreate="grid_AutoFilterCellEditorCreate" OnAutoFilterCellEditorInitialize="grid_AutoFilterCellEditorInitialize"
                                    OnProcessColumnAutoFilter="grid_ProcessColumnAutoFilter" KeyFieldName="pedidoId" Width="834px" Settings-ShowFilterRow="True" Settings-ShowFilterBar="Visible"
                                    Cursor="auto" OnCustomUnboundColumnData="grd_OnCustomUnboundColumnData">
                                    <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                                    <Styles>
                                        <Footer Font-Bold="True">
                                        </Footer>
                                    </Styles>
                                    <SettingsText ConfirmDelete="Tem certeza que deseja excluir?" EmptyDataRow="Nenhum registro encontrado." />
                                    <SettingsPager Position="TopAndBottom" PageSize="50"
                                        ShowDisabledButtons="False" AlwaysShowPager="True">
                                        <Summary AllPagesText="Páginas: {0} - {1} ({2} items)"
                                            Text="Página {0} de {1} ({2} registros encontrados)" />
                                    </SettingsPager>
                                    <TotalSummary>
                                        <dxwgv:ASPxSummaryItem FieldName="volumesEmbalados" ShowInColumn="volumesEmbalados" ShowInGroupFooterColumn="volumesEmbalados" SummaryType="Sum" DisplayFormat="{0}" />
                                        <dxwgv:ASPxSummaryItem FieldName="volumesEmbalados" ShowInColumn="pedidoIdCliente" ShowInGroupFooterColumn="pedidoIdCliente" SummaryType="Count" DisplayFormat="{0}" />
                                    </TotalSummary>
                                    <Settings ShowFilterRow="True" ShowGroupButtons="False" ShowHeaderFilterButton="True" ShowFooter="True" />

                                    <SettingsEditing EditFormColumnCount="4" PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1" PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter" PopupEditFormWidth="700px" />
                                    <Columns>
                                        <dxwgv:GridViewDataTextColumn Caption="Id Cliente" FieldName="pedidoIdCliente" VisibleIndex="0" UnboundType="Integer">
                                            <EditFormSettings Visible="False" />
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn Caption="Id" FieldName="pedidoId"
                                            VisibleIndex="0">
                                            <EditFormSettings Visible="False" />
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn Caption="Nome" FieldName="clienteId" VisibleIndex="0">
                                            <EditFormSettings Visible="False" />
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn Caption="Transportadora" FieldName="formaDeEnvio"
                                            VisibleIndex="3">
                                            <EditFormSettings Visible="False" />
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn Caption="Rastreio" FieldName="rastreio"
                                            VisibleIndex="3">
                                            <EditFormSettings Visible="False" />
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn Caption="Volumes" FieldName="volumesEmbalados"
                                            VisibleIndex="3">
                                            <EditFormSettings Visible="False" />
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn Caption="CD" FieldName="idCentroDeDistribuicao"
                                            VisibleIndex="3">
                                            <EditFormSettings Visible="False" />
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn Name="nfeNumero" Caption="Nota Fiscal" FieldName="nfeNumero" VisibleIndex="3" Width="120px">
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn Name="idLoteImpressaoGnre" Caption="Lote GNRE" FieldName="idLoteImpressaoGnre" VisibleIndex="3" Width="120px">
                                        </dxwgv:GridViewDataTextColumn>
                                        
                                        <dxwgv:GridViewDataTextColumn Name="pesoTotal" Caption="Peso" FieldName="pesoTotal" VisibleIndex="3" Width="120px">
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataCheckColumn Name="nfeImpressa" Caption="Nota Impressa" FieldName="nfeImpressa" VisibleIndex="3"></dxwgv:GridViewDataCheckColumn>
                                        <dxwgv:GridViewDataTextColumn Name="valorFrete" Caption="Valor Frete Nota" FieldName="valorFrete" VisibleIndex="3" Width="120px">
                                            <PropertiesTextEdit DisplayFormatString="c">
                                            </PropertiesTextEdit>
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn Name="valorFretePedido" Caption="Valor Frete Pedido" FieldName="valorFretePedido" VisibleIndex="3" Width="120px">
                                            <PropertiesTextEdit DisplayFormatString="c">
                                            </PropertiesTextEdit>
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn Name="valorNota" Caption="Valor Nota" FieldName="valorNota" VisibleIndex="3" Width="120px">
                                            <PropertiesTextEdit DisplayFormatString="c">
                                            </PropertiesTextEdit>
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn Name="valorCobrado" Caption="Valor Cobrado" FieldName="valorCobradoTransportadora" VisibleIndex="3" Width="120px" Visible="False">
                                            <PropertiesTextEdit DisplayFormatString="c">
                                            </PropertiesTextEdit>
                                        </dxwgv:GridViewDataTextColumn>

                                        <dxwgv:GridViewDataDateColumn Name="dataSendoEmbalado" Caption="Inicio Embalagem" FieldName="dataSendoEmbalado" VisibleIndex="3" Width="120px">
                                            <PropertiesDateEdit DisplayFormatString="g"></PropertiesDateEdit>
                                        </dxwgv:GridViewDataDateColumn>
                                        <dxwgv:GridViewDataDateColumn Name="dataFimEmbalado" Caption="Fim Embalagem" FieldName="dataFimEmbalado" VisibleIndex="3" Width="120px">
                                            <PropertiesDateEdit DisplayFormatString="g"></PropertiesDateEdit>
                                        </dxwgv:GridViewDataDateColumn>
                                        <dxwgv:GridViewDataDateColumn Caption="Data de Pagamento da Fatura" FieldName="dataPagamentoFatura" Name="dataPagamentoFatura" VisibleIndex="1" Visible="False">
                                        </dxwgv:GridViewDataDateColumn>
                                        <dxwgv:GridViewDataTextColumn Caption="Editar" Name="editar" VisibleIndex="3" Width="80">
                                            <DataItemTemplate>
                                                <asp:LinkButton runat="server" OnCommand="btnEditarValor_OnCommand" ID="btnEditarValor" CommandArgument='<%# Eval("idPedidoEnvio") %>' EnableViewState="False">Editar</asp:LinkButton>
                                            </DataItemTemplate>
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn Caption="Enviar XML" Name="enviarXml" VisibleIndex="3" Width="80">
                                            <DataItemTemplate>
                                                <asp:LinkButton runat="server" ToolTip="Envia XML para Transportadora" OnCommand="btnEnviarXml_OnCommand" ID="btnEnviarXml" CommandArgument='<%# Eval("nfeNumero") + "#" + Eval("formaDeEnvio") + "#" + Eval("idPedidoEnvio") %>' EnableViewState="False">Enviar XML</asp:LinkButton>
                                            </DataItemTemplate>
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataHyperLinkColumn Caption="Editar" FieldName="pedidoId"
                                            VisibleIndex="7" Width="30px">
                                            <PropertiesHyperLinkEdit ImageUrl="~/admin/images/btEditar.jpg"
                                                NavigateUrlFormatString="pedido.aspx?pedidoId={0}" Target="_blank">
                                            </PropertiesHyperLinkEdit>
                                            <Settings AllowAutoFilter="False" />
                                            <HeaderTemplate>
                                                <img alt="" src="images/legendaEditar.jpg" />
                                            </HeaderTemplate>
                                        </dxwgv:GridViewDataHyperLinkColumn>
                                    </Columns>
                                    <StylesEditors>
                                        <Label Font-Bold="True">
                                        </Label>
                                    </StylesEditors>
                                    <ClientSideEvents ContextMenu="grid_ContextMenu" />
                                    <SettingsCustomizationWindow Enabled="True" />
                                </dxwgv:ASPxGridView>
                            </asp:Panel>
                            <dxwgv:ASPxGridViewExporter ID="grdEx" runat="server" FileName="pedidos"
                                GridViewID="grd" Landscape="True" PreserveGroupRowStates="True">
                            </dxwgv:ASPxGridViewExporter>
                            <asp:LinqDataSource ID="sqlUsuarios" runat="server"
                                ContextTypeName="dbCommerceDataContext" EnableDelete="True"
                                EnableInsert="True" EnableUpdate="True" TableName="tbUsuarioExpedicaos" EntityTypeName="">
                            </asp:LinqDataSource>


                            <asp:Panel runat="server" ID="pnGridEditar" Visible="False">
                                <table style="width: 100%">
                                    <tr class="rotulos" style="font-weight: bold;">
                                        <td>
                                            <asp:HiddenField runat="server" ID="hdfIdPedidoEnvio" />
                                            Pedido:
                                            <asp:Literal runat="server" ID="litPedidoIdDiferenca"></asp:Literal>
                                        </td>
                                    </tr>
                                    <tr class="rotulos">
                                        <td>Data da Fatura:<br />
                                            <asp:TextBox runat="server" ID="txtDataFatura" CssClass="campos" Text="0"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr class="rotulos">
                                        <td>Data de Pagamento da Fatura:<br />
                                            <asp:TextBox runat="server" ID="txtDataPagamentoFatura" CssClass="campos" Text=""></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr class="rotulos">
                                        <td>Valor cobrado:<br />
                                            <asp:TextBox runat="server" ID="txtValorDiferenca" CssClass="campos" Text="0"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr class="rotulos">
                                        <td>Valor da Reversa:<br />
                                            <asp:TextBox runat="server" ID="txtValorReversa" CssClass="campos" Text="0"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr class="rotulos">
                                        <td>Valor da Cobrança Adicional:<br />
                                            <asp:TextBox runat="server" ID="txtValorCobrancaAdicional" CssClass="campos" Text="0"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr class="rotulos">
                                        <td>Observações:<br />
                                            <asp:TextBox runat="server" ID="txtObservacoes" CssClass="campos" Text="0" TextMode="MultiLine" Height="50" Width="100%"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr class="rotulos">
                                        <td style="padding-top: 10px;">
                                            <asp:Button runat="server" ID="btnGravarDiferenca" Text="Gravar valor recebido" OnClick="btnGravarDiferenca_onClick" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
