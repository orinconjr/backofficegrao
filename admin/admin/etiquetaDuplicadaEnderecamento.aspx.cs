﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_etiquetaDuplicadaEnderecamento : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtNumeroEtiqueta.Attributes.Add("onkeypress", "return soNumero(event);");
        }
    }

    protected void imbPesquisar_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            rptEtiquetasDuplicadas.DataSource = null;
            rptEtiquetasDuplicadas.DataBind();
            pnlEtiquetas.Visible = false;
            btnLiberarSenha.Visible = false;

            rptLocalizarEtiqueta.DataSource = null;
            rptLocalizarEtiqueta.DataBind();
            pnlLocalizarEtiqueta.Visible = false;

            int numeroEtiqueta = Convert.ToInt32(txtNumeroEtiqueta.Text);

            using (var data = new dbCommerceDataContext())
            {

                if (rbOpcoes.SelectedValue == "2")
                {
                    var localizarEtiqueta = (from c in data.tbPedidoFornecedorItems
                                             join pro in data.tbProdutos on c.idProduto equals pro.produtoId
                                             join te in data.tbTransferenciaEnderecoProdutos on c.idPedidoFornecedorItem equals
                                                 te.idPedidoFornecedorItem
                                             where c.idPedidoFornecedorItem == numeroEtiqueta && te.dataEnderecamento == null
                                             select new
                                             {
                                                 etiqueta = c.idPedidoFornecedorItem,
                                                 pro.produtoNome,
                                                 usuarioexpedicao = te.tbTransferenciaEndereco.tbUsuarioExpedicao.nome,
                                                 te.tbTransferenciaEndereco.tbEnderecamentoTransferencia.enderecamentoTransferencia,
                                                 te.dataEnderecamento
                                             }).ToList();


                    if (localizarEtiqueta.Count == 0)
                    {
                        var etiquetaJaEnderecada = (from c in data.tbProdutoEstoques
                                                    where c.idPedidoFornecedorItem == numeroEtiqueta && c.idEnderecamentoArea != null
                                                    select new { c.tbEnderecamentoArea.area, c.tbEnderecamentoRua.rua, c.tbEnderecamentoPredio.predio, c.tbEnderecamentoPredio.tbEnderecamentoLado.lado, c.tbEnderecamentoAndar.andar });
                        var usarioResponsavelEnderecamento =
                            (from c in data.tbTransferenciaEnderecoProdutos
                                where c.idPedidoFornecedorItem == numeroEtiqueta orderby c.idTransferenciaEnderecoProduto descending 
                                select c.tbTransferenciaEndereco.tbUsuarioExpedicao.nome ).FirstOrDefault();

                        if (etiquetaJaEnderecada.Any())
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(),
                          "alert('A etiqueta " + numeroEtiqueta + " já está endereçada.\\nArea: " + etiquetaJaEnderecada.FirstOrDefault().area +
                          "\\nRua: " + etiquetaJaEnderecada.FirstOrDefault().rua +
                          "\\nPredio: " + etiquetaJaEnderecada.FirstOrDefault().predio +
                          "\\nLado: " + etiquetaJaEnderecada.FirstOrDefault().lado +
                          "\\nAndar: " + etiquetaJaEnderecada.FirstOrDefault().andar +
                          "\\nEnderereçada por: " + usarioResponsavelEnderecamento + "');",
                          true);
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(),
                           "alert('Não há registro de entrada da etiqueta " + numeroEtiqueta + ".');",
                           true);
                        }

                    }
                    else
                    {
                        rptLocalizarEtiqueta.DataSource = localizarEtiqueta;
                        rptLocalizarEtiqueta.DataBind();
                        pnlLocalizarEtiqueta.Visible = true;
                    }

                }
                else
                {

                    var etiquetas = (from c in data.tbPedidoFornecedorItems
                                     join pro in data.tbProdutos on c.idProduto equals pro.produtoId
                                     join te in data.tbTransferenciaEnderecoProdutos on c.idPedidoFornecedorItem equals
                                         te.idPedidoFornecedorItem
                                     where c.idPedidoFornecedorItem == numeroEtiqueta
                                     select new
                                     {
                                         etiqueta = c.idPedidoFornecedorItem,
                                         pro.produtoNome,
                                         usuarioexpedicao = te.tbTransferenciaEndereco.tbUsuarioExpedicao.nome,
                                         te.dataEnderecamento,
                                         te.tbTransferenciaEndereco.tbEnderecamentoTransferencia.enderecamentoTransferencia
                                     }).ToList();

                    if (etiquetas.Select(x => x.enderecamentoTransferencia == "Cancelamento de Embalagem").Any())
                    {
                        var statusEtiqueta =
                            (from pe in data.tbProdutoEstoques
                             where pe.idPedidoFornecedorItem == numeroEtiqueta && pe.enviado == false
                             select pe).Any();

                        if (statusEtiqueta)
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(),
                                "alert('A etiqueta " + numeroEtiqueta +
                                " encontra-se no endereço Cancelamento de Embalagem, favor endereçar!');", true);

                            return;
                        }

                    }

                    if (etiquetas.Count < 2)
                    {
                        var etiquetaMarcadaComoEnviada =
                            (from c in data.tbProdutoEstoques
                             join te in data.tbTransferenciaEnderecoProdutos on c.idPedidoFornecedorItem equals
                                 te.idPedidoFornecedorItem
                             where
                                 c.idPedidoFornecedorItem == numeroEtiqueta && c.enviado &&
                                 te.dataEnderecamento == null
                             select c).Any();

                        if (etiquetaMarcadaComoEnviada)
                        {
                            var etiquetaJaEnviada = (from c in data.tbPedidoFornecedorItems
                                                     join pro in data.tbProdutos on c.idProduto equals pro.produtoId
                                                     join te in data.tbTransferenciaEnderecoProdutos on c.idPedidoFornecedorItem equals
                                                         te.idPedidoFornecedorItem
                                                     where c.idPedidoFornecedorItem == numeroEtiqueta
                                                     select new
                                                     {
                                                         etiqueta = c.idPedidoFornecedorItem,
                                                         pro.produtoNome,
                                                         usuarioexpedicao = te.tbTransferenciaEndereco.tbUsuarioExpedicao.nome,
                                                         te.dataEnderecamento
                                                     }).ToList();

                            rptEtiquetasDuplicadas.DataSource = etiquetaJaEnviada;
                            rptEtiquetasDuplicadas.DataBind();
                            pnlEtiquetas.Visible = true;
                            btnLiberarSenha.Visible = true;
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(),
                                "alert('A etiqueta " + numeroEtiqueta.ToString() + " não esta duplicada ou enviada!');",
                                true);
                        }

                    }
                    else
                    {
                        rptEtiquetasDuplicadas.DataSource = etiquetas;
                        rptEtiquetasDuplicadas.DataBind();
                        pnlEtiquetas.Visible = true;
                        btnLiberarSenha.Visible = true;
                    }

                }
            }
        }
        catch (Exception)
        {

        }
    }
    protected void btnLiberarSenha_Click(object sender, EventArgs e)
    {
        try
        {
            int numeroEtiqueta = Convert.ToInt32(txtNumeroEtiqueta.Text);

            using (var data = new dbCommerceDataContext())
            {
                var etiquetaDuplicada =
                    (from te in data.tbTransferenciaEnderecoProdutos
                     where te.idPedidoFornecedorItem == numeroEtiqueta && te.dataEnderecamento == null
                     select te);

                foreach (var etiqueta in etiquetaDuplicada)
                {


                    etiqueta.dataEnderecamento = DateTime.Now;
                    data.SubmitChanges();

                    int idEnderecamentoTransferencia = etiqueta.idTransferenciaEndereco;


                    var encerrarEnderecamento =
                    !(from c in data.tbTransferenciaEnderecoProdutos
                      where c.idTransferenciaEndereco == idEnderecamentoTransferencia && c.dataEnderecamento.Value == null
                      select c).Any();

                    if (encerrarEnderecamento)
                    {
                        var finalizarEnderecamento =
                            (from c in data.tbTransferenciaEnderecos
                             where c.idTransferenciaEndereco == idEnderecamentoTransferencia && c.dataFim == null
                             select c).SingleOrDefault();

                        if (finalizarEnderecamento != null)
                        {
                            finalizarEnderecamento.dataFim = DateTime.Now;
                            data.SubmitChanges();
                        }

                    }
                }


            }


            HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
            usuarioLogadoId = Request.Cookies["usuarioLogadoId"];
            string usuario = "";
            if (usuarioLogadoId != null)
            {
                usuario = rnUsuarios.usuarioSeleciona_PorUsuarioId(int.Parse(usuarioLogadoId.Value.ToString())).Tables[0].Rows[0]["usuarioNome"].ToString();
            }

            rnEmails.EnviaEmail("", "renato@bark.com.br,andre@bark.com.br", "", "", "",
                       "liberada etiqueta de numero: " + numeroEtiqueta + "</br>motivo: duplicidade</br>" + "usuario responsavel: " + usuario,
                       "Etiqueta duplicada - Endereçamento");

            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('Operação realizada com sucesso!');", true);

            rptEtiquetasDuplicadas.DataSource = null;
            rptEtiquetasDuplicadas.DataBind();
            pnlEtiquetas.Visible = false;
            btnLiberarSenha.Visible = false;
        }
        catch (Exception)
        {

            throw;
        }



    }
}