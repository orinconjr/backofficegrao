﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="comprasAlterarStatus.aspx.cs" Inherits="admin_comprasAlterarStatus" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" Runat="Server">
       <script src="js/jquery.min.js"></script>
    <style>
        .fieldsetatualizarprecos {
            width: 760px;
            float: left;
            clear: left;
            border-radius: 5px;
            margin-left: 25px;
        }

            .fieldsetatualizarprecos input[type=checkbox] {
                margin-top: 15px;
            }

            .fieldsetatualizarprecos label {
                margin-left: 5px;
                margin-right: 20px;
            }

        .meugrid {
            background-color: #F4FAFB;
            border: 0px none;
            /* border-collapse: separate !important; */
            overflow: hidden;
            font: 9pt Tahoma;
            color: #000;
            cursor: pointer;
            /* white-space: nowrap;*/
            font-weight: normal;
            text-align: left;
            width: 834px;
            border-collapse: collapse;
            float: left;
            clear: left;
            margin-top: 10px;
            margin-bottom: 10px;
            min-height: 500px;
            margin-left: 25px;
        }

            .meugrid th {
                background-image: url(../../App_Themes/Glass/GridView/gvHeaderBackground.gif);
                background-position: center top;
                background-repeat: repeat-x;
                background-color: #C0DDE0;
                overflow: hidden;
                height: 23px;
                line-height: 23px;
                padding: 4px 6px 5px;
                font-weight: normal;
                border: 1px solid #7EACB1;
                /* border-collapse: separate;*/
            }

            .meugrid td {
                overflow: hidden;
                height: 23px;
                line-height: 23px;
                padding: 4px 6px 5px;
                border: 1px solid #7EACB1;
            }

            .meugrid tr td:nth-child(4) {
                text-align: center;
            }

            .meugrid tr td:last-of-type {
                text-align: center;
            }

        .tabrotulos {
            width: 848px;
            margin: auto;
        }

        .materiaprima {
            width: 250px;
        }

            .materiaprima ul {
                margin: 0px;
                padding-left: 10px;
            }

                .materiaprima ul li {
                    max-width: 230px;
                    white-space: pre-wrap;
                }

        .meugrid tr td:last-of-type {
            text-align: center;
        }

        .nome {
            width: 237px;
        }

            .nome div {
                max-width: 230px;
                white-space: pre-wrap;
            }

        .rotulos td {
            font: bold 12px tahoma;
            padding-top: 20px;
            color: rgb(65, 105, 126);
            padding-left: 5px;
            padding-bottom: 5px;
        }

        /*****************************/
        .width150 {
        }

        .height150 {
        }

        .height80 {
        }

        .btn {
            -webkit-border-radius: 7px;
            -moz-border-radius: 7px;
            border-radius: 2px;
            font-family: Arial;
            color: #ffffff;
            font-size: 14px;
            background: #63c269;
            padding: 5px 10px 5px 10px;
            text-decoration: none;
            cursor: pointer;
            border: 0px;
        }

            .btn:hover {
                background: #a4dbab;
                text-decoration: none;
                color: black;
                font-weight: bold;
            }


        .marginTop5porcento {
            margin: 6%;
            font-size: 15px;
        }

        .quadroPrincipal {
            float: left;
            width: 150px;
            height: 155px;
            text-align: center;
            position: relative;
            font-family: monospace;
            border: 1px solid #B1B1B1;
            margin: 0 14px 5px 14px;
            border-radius: 15px;
        }

        .quadroInteriorQtds {
            margin-top: 5%;
            text-align: center;
            font-size: 30px;
        }

        .AlinharAoRodapeCentralizar {
            position: absolute;
            bottom: 5px;
            text-align: center;
            width: 150px;
        }
        .meubotao {
            cursor: pointer;
            font: bold 14px tahoma;
        }
    </style>

    <div class="tituloPaginas" valign="top">
        Compras - Alterar Status de Item
    </div>
    <div>
        <fieldset class="fieldsetatualizarprecos">
            <legend style="font-weight: bold;">Filtro</legend>

            <table class="tabrotulos">
                <tr class="rotulos">

                    <td>Item:<br />
                        <asp:TextBox runat="server" ID="txtItem"></asp:TextBox>
                    </td>
                    <td>&nbsp;<br />
                        <div style=" text-align: right">
                            <asp:Button runat="server" ID="btnBuscarStatus" CssClass="meubotao" Text="Buscar Status" OnClick="btnBuscarStatus_Click" />
                        </div>
                    </td>
                    <td>Status:<br />
                         <asp:TextBox runat="server" ID="txtStatusAtual" Width="370px"></asp:TextBox>
                        <%--<asp:DropDownList runat="server" ID="ddlStatus" Width="370px" CssClass="campos">
                        </asp:DropDownList>--%>
                    </td>
                </tr>

                <tr class="rotulos">
                    <td> Alterar status para <br />
                        <asp:DropDownList runat="server" ID="ddlAlterarStatusPara" Width="370px" CssClass="campos">
                            <asp:ListItem Value="">Selecione</asp:ListItem>
                            <asp:ListItem Value="0">Pendente</asp:ListItem>
                             <asp:ListItem Value="1">Aguardando Aprovação</asp:ListItem>
                             <asp:ListItem Value="2">Aprovada</asp:ListItem>
                             <asp:ListItem Value="3">Aguardando Financeiro</asp:ListItem>
                             <asp:ListItem Value="4">Finalizado</asp:ListItem>
                             <asp:ListItem Value="5">Cancelado</asp:ListItem>
                             <asp:ListItem Value="6">Aguardando Conferência de Entrega</asp:ListItem>
                        </asp:DropDownList>
                    </td>

                    <td>&nbsp;<br />
                        <asp:Button runat="server" ID="btnAlterar" CssClass="meubotao" Text="Alterar" OnClick="btnAlterar_Click" />
                    </td>

                    <td>
                        <div style="width: 370px; text-align: right">
                            
                        </div>
                    </td>

                </tr>

          
            </table>
        </fieldset>
        <div style="float:left;clear:left;font: bold 15px tahoma;width:100%;padding-left:25px;">
                    <asp:Label ID="lblMensagem" runat="server" Text=""></asp:Label>
        </div>

        
    </div>
</asp:Content>

