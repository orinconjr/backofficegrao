﻿using System;
using System.Activities.Expressions;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using ListItem = System.Web.UI.WebControls.ListItem;


public partial class admin_produtosCatalogo : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (IsPostBack)
        {


        }

    }

    protected void btnImportar_OnClick(object sender, EventArgs e)
    {
        //Response.Write("<script>window.open('../imprimirCatalogo.aspx?fornecedor=" + ddlExportarPlanilha.SelectedValue + "&filtro=" + txtFiltroProduto.Text + "','Catalogo de produtos');</script>");
        Response.Write("<script>window.open('../imprimirCatalogo.aspx?fornecedor=" + ddlExportarPlanilha.SelectedValue + "&filtro=" + ddlCategoriaFornecedor.SelectedValue + "','Catalogo de produtos');</script>");
        //FillGrid();
        //txtPesquisa.Text = "";
    }

    private void FillGrid()
    {
        int fornecedor = Convert.ToInt32(ddlExportarPlanilha.SelectedValue);
        using (var data = new dbCommerceDataContext())
        {
            var dados = (from c in data.tbProdutos where c.produtoFornecedor == fornecedor && c.produtoAtivo.ToLower().Contains("true") select new { c.fotoDestaque, c.produtoNome, c.produtoId, referencia = "Ref: " + c.produtoId }).ToList();

            //grd.DataSource = dados;
            //grd.DataBind();

            GridView1.DataSource = dados;
            GridView1.DataBind();

            if (GridView1.Rows.Count > 0)
            {
                txtPesquisa.Visible = true;
                btnFiltrar.Visible = true;
            }
            else
            {
                txtPesquisa.Visible = false;
                btnFiltrar.Visible = false;
            }
        }
    }

    private void FillGridFiltro(int fornecedor, string produtoPesquisa)
    {

        using (var data = new dbCommerceDataContext())
        {
            var dados = (from c in data.tbProdutos where c.produtoFornecedor == fornecedor && c.produtoAtivo.ToLower().Contains("true") && c.produtoNome.Contains(produtoPesquisa) select new { c.fotoDestaque, c.produtoNome, c.produtoId, referencia = "Ref: " + c.produtoId }).ToList();

            //grd.DataSource = dados;
            //grd.DataBind();

            GridView1.DataSource = dados;
            GridView1.DataBind();

            if (GridView1.Rows.Count > 0)
            {
                txtPesquisa.Visible = true;
                btnFiltrar.Visible = true;
            }
            else
            {
                txtPesquisa.Visible = false;
                btnFiltrar.Visible = false;
            }

        }
    }

    protected void btPdf_Click(object sender, ImageClickEventArgs e)
    {
        //this.grdEx.WritePdfToResponse();
        PDF_Export();
    }
    protected void btXsl_Click(object sender, ImageClickEventArgs e)
    {
        //this.grdEx.WriteXlsToResponse();
    }
    protected void btRtf_Click(object sender, ImageClickEventArgs e)
    {
        //this.grdEx.WriteRtfToResponse();
    }
    protected void btCsv_Click(object sender, ImageClickEventArgs e)
    {
        //this.grdEx.WriteCsvToResponse();
    }

    private void PDF_Export()
    {
        //Response.ContentType = "application/pdf";
        //Response.AddHeader("content-disposition", "attachment;filename=" + ddlExportarPlanilha.SelectedItem.Text.Replace(" ","").Trim() + ".pdf");
        //Response.Cache.SetCacheability(HttpCacheability.NoCache);
        //StringWriter sw = new StringWriter();
        //HtmlTextWriter hw = new HtmlTextWriter(sw);
        //GridView1.AllowPaging = false;
        //GridView1.DataBind();
        //GridView1.RenderControl(hw);
        //StringReader sr = new StringReader(sw.ToString());
        //Document pdfDoc = new Document(PageSize.A4, 10.0F, 10.0F, 10.0F, 0.0F);
        //HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
        //PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
        //pdfDoc.Open();
        //htmlparser.Parse(sr);
        ////pdfDoc.Close();
        //Response.Write(pdfDoc);
        //Response.End();

        //Response.ContentType = "application/pdf";
        //Response.AddHeader("content-disposition","attachment;filename=GridViewExport.pdf");
        //Response.Cache.SetCacheability(HttpCacheability.NoCache);
        //StringWriter sw = new StringWriter();
        //HtmlTextWriter hw = new HtmlTextWriter(sw);
        //GridView1.AllowPaging = false;
        //GridView1.DataBind();
        //GridView1.RenderControl(hw);
        //StringReader sr = new StringReader(sw.ToString());
        //Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 10f, 0f);
        //HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
        //PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
        //pdfDoc.Open();
        //htmlparser.Parse(sr);
        //pdfDoc.Close();
        //Response.Write(pdfDoc);
        //Response.End();

        //Response.ContentType = "application/pdf";
        //Response.AddHeader("content-disposition", "attachment;filename=Vithal_Wadje.pdf");
        //Response.Cache.SetCacheability(HttpCacheability.NoCache);
        //StringWriter sw = new StringWriter();
        //HtmlTextWriter hw = new HtmlTextWriter(sw);
        //GridView1.RenderControl(hw);
        //StringReader sr = new StringReader(sw.ToString());
        //Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 10f, 0f);
        //HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
        //PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
        //pdfDoc.Open();
        //htmlparser.Parse(sr);
        //pdfDoc.Close();
        //Response.Write(pdfDoc);
        //Response.End();
        //GridView1.AllowPaging = true;
        //GridView1.DataBind();

        //disable paging to export all data and make sure to bind griddata before begin
        GridView1.AllowPaging = false;
        int forncedor = Convert.ToInt32(ddlExportarPlanilha.SelectedValue);

        FillGrid();
        string fileName = "ExportToPdf_" + DateTime.Now.ToShortDateString();
        Response.ContentType = "application/pdf";
        Response.AddHeader("content-disposition", string.Format("attachment; filename={0}",
        fileName + ".pdf"));
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        StringWriter objSW = new StringWriter();
        HtmlTextWriter objTW = new HtmlTextWriter(objSW);
        GridView1.RenderControl(objTW);
        StringReader objSR = new StringReader(objSW.ToString());
        Document objPDF = new Document(PageSize.A4, 100f, 100f, 100f, 100f);
        HTMLWorker objHW = new HTMLWorker(objPDF);
        PdfWriter.GetInstance(objPDF, Response.OutputStream);
        objPDF.Open();
        objHW.Parse(objSR);
        objPDF.Close();
        Response.Write(objPDF);
        Response.End();
    }

    protected void OnClick(object sender, EventArgs e)
    {
        PDF_Export();
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Confirms that an HtmlForm control is rendered for the
        /* specified ASP.NET server control at run time. */
    }

    protected void GridView1_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        if (String.IsNullOrEmpty(txtPesquisa.Text))
        {
            GridView1.PageIndex = e.NewPageIndex;
            FillGrid();
        }
        else
        {
            int forncedor = Convert.ToInt32(ddlExportarPlanilha.SelectedValue);
            GridView1.PageIndex = e.NewPageIndex;
            FillGridFiltro(forncedor, txtPesquisa.Text);
        }

    }

    protected void btnFiltrar_OnClick(object sender, EventArgs e)
    {
        int forncedor = Convert.ToInt32(ddlExportarPlanilha.SelectedValue);
        FillGridFiltro(forncedor, txtPesquisa.Text);
    }

    protected void ddlExportarPlanilha_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            ddlCategoriaFornecedor.Items.Clear();

            using (var data = new dbCommerceDataContext())
            {
                int fornecedor = Convert.ToInt32(ddlExportarPlanilha.SelectedValue);
                var produtosFornecedor =
                    (from c in data.tbProdutos
                     where c.produtoFornecedor == fornecedor && c.produtoAtivo.ToLower().Contains("true")
                     select c.produtoId);

                List<int> todasCategoriasFornecedor = new List<int>();
                todasCategoriasFornecedor.AddRange(produtosFornecedor);

                var todasCategorias =
                    (from c in data.tbJuncaoProdutoCategorias where todasCategoriasFornecedor.Contains(c.produtoId) select c.categoriaId).Distinct();

                List<int> todasCategoriasProdutos = new List<int>();
                todasCategoriasProdutos.AddRange(todasCategorias);

                var categorias =
                    (from c in data.tbProdutoCategorias where todasCategoriasProdutos.Contains(c.categoriaId) && c.exibirSite == true orderby c.categoriaNome select new { c.categoriaNome, c.categoriaId });

                ddlCategoriaFornecedor.DataSource = categorias;
                ddlCategoriaFornecedor.DataBind();
            }

            ddlCategoriaFornecedor.Items.Insert(0, "Todos");
        }
        catch (Exception)
        {


        }

    }
}