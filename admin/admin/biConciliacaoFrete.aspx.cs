﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxPivotGrid;
using DevExpress.XtraPivotGrid;


public partial class admin_biConciliacaoFrete : System.Web.UI.Page
{
    List<MaxValueStorage> maxValue = new List<MaxValueStorage>();
    public class MaxValueStorage
    {
        decimal _value;
        string _key;
        public MaxValueStorage() { _value = decimal.MinValue; }

        public void UpdateMax(decimal _value, string key)
        {
            Value = Math.Max(_value, Value);
            Key = key;
        }
        public decimal Value { get { return _value; } set { _value = value; } }
        public string Key { get { return _key; } set { _key = value; } }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (txtDataInicial.Text == "")
            {
                txtDataInicial.Text = DateTime.Now.AddDays(-7).ToShortDateString();
            }
            if (txtDataFinal.Text == "")
            {
                txtDataFinal.Text = DateTime.Now.ToShortDateString();
            }
        }

        fillGrid(false);
    }

    private void fillGrid(bool rebind)
    {
        var dataInicial = DateTime.Now;
        var dataFinal = DateTime.Now;

        DateTime.TryParse(txtDataInicial.Text, out dataInicial);
        DateTime.TryParse(txtDataFinal.Text, out dataFinal);
        dataFinal = dataFinal.AddDays(1);
        var data = new dbCommerceDataContext();

        var relatorio = (from c in data.tbTransportadoraFaturaRegistros
                         where c.tbPedido.dataHoraDoPedido >= dataInicial && c.tbPedido.dataHoraDoPedido <= dataFinal
                         select new
                         {
                             transportadora = (c.tbTransportadoraFatura.tbTransportadora.transportadora ?? "Não Localizada"), // Transportadora 
                             c.tbPedidoEnvio.valorSelecionado, // Valor Pago
                             c.valorFrete, // Valor Total Orçado 
                             c.tbTransportadoraFatura.codigoFatura, //Numero da Fatura
                             c.numeroNota, //Número da NF 
                             c.peso, // Peso
                             c.tbPedido.dataHoraDoPedido, //Data do Pedido
                             c.dataEmissaoNota, // dia do envio
                             empresa = (c.tbEmpresa.nomeEmitente ?? "Não localizada"),
                             rowempresa = (c.tbEmpresa.nomeEmitente ?? "Não localizada")
                         }
                         );


        grd.DataSource = relatorio;
        if ((!Page.IsPostBack && !Page.IsCallback) | rebind)
        {
            grd.DataBind();
        }
    }

    protected void btnPequisar_Click(object sender, ImageClickEventArgs e)
    {
        fillGrid(true);
    }

    protected void grd_CustomUnboundFieldData(object sender, CustomFieldDataEventArgs e)
    {

        if (e.Field.FieldName != "diferenca") return;

        decimal valorSelecionado = Convert.ToDecimal(e.GetListSourceColumnValue("ValorSelecionado"));
        decimal valorCobrado = Convert.ToDecimal(e.GetListSourceColumnValue("ValorCobrado"));
        decimal result = valorSelecionado - valorCobrado;

        e.Value = result;
    }
}