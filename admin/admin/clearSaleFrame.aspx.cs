﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class admin_clearSaleFrame : System.Web.UI.Page
{
    protected void Page_Load(object sender, System.EventArgs e)
    {
        MClearSale();
    }

    private void MClearSale()
    {
        int pedidoId = Convert.ToInt32(Request.QueryString["codPedido"]);
        var data = new dbCommerceDataContext();
        var pedido = (from c in data.tbPedidos where c.pedidoId == pedidoId select c).First();

        //Seu login e sus senha de acesso ao fcontrol (são fixos)
        string login = "f17875c6-4dec-4c83-b02e-f5efe39b5b3e";

        //Abaixo os dados estão fixos mas devem ser substituídos pelos dados de seu pedido
        //Faça aqui a consulta em seu Banco de Dados para pegar os dados que devem ser adicionados ao Redirecionamento abaixo.

        //Dados da compra

        //Data em que o pedido foi feito
        string Data = rnPedidos.pedidoSeleciona_PorPedidoId(int.Parse(Request["codPedido"])).Tables[0].Rows[0]["dataHoraDoPedido"].ToString(); //A data deve estar no formato aaaa-mm-dd hh:mm:ss

        //Código do pedido
        string PedidoID = Request["codPedido"]; //Máximo de 26 caracteres, deve ser único pois ele identifica o pedido.
        string PedidoIDIntegracao = Request["codPedido"]; //Máximo de 26 caracteres, deve ser único pois ele identifica o pedido.
        if (pedido.dataHoraDoPedido >= Convert.ToDateTime("20/08/2014 15:30:00"))
            PedidoIDIntegracao = rnFuncoes.retornaIdCliente(pedido.pedidoId).ToString();

        //Ip do usuário que realizou a transação
        string IP = rnPedidos.pedidoSeleciona_PorPedidoId(int.Parse(PedidoID)).Tables[0].Rows[0]["ipDoCliente"].ToString();// pode ser para um * caso você não tenha o ip

        string TodosItens = "";

        DataSet ds = new DataSet();
        ds = rnPedidos.itensPedidoSelecionaAdmin_PorPedidoId(Convert.ToInt32(PedidoID));

        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
        {
            TodosItens += ds.Tables[0].Rows[i][3].ToString() + "|";
            TodosItens += ds.Tables[0].Rows[i][2].ToString() + "|";
            TodosItens += ds.Tables[0].Rows[i][4].ToString() + "|";
            TodosItens += ds.Tables[0].Rows[i][9].ToString() + "|";
            TodosItens += ds.Tables[0].Rows[i][11].ToString() + "#";
        }

        // número de itens (ex.: 5 lápis)
        //string items_tot = rnPedidos.itensPedidoConta(int.Parse(PedidoID)).Tables[0].Rows[0]["quantidadeTotalDeItens"].ToString();

        // número de itens DISTINTOS (ex.: 5 lápis, só o tipo lápis então  = 1)
        //string items_dif = int.Parse(rnPedidos.itensPedidoConta(int.Parse(PedidoID)).Tables[0].Rows[0]["quantidadeDeItensDestinstos"].ToString()).ToString();

        string Total = (decimal.Parse(rnPedidos.pedidoSeleciona_PorPedidoId(int.Parse(PedidoID)).Tables[0].Rows[0]["valorCobrado"].ToString())).ToString("##,###.00").Replace(",", ".");

        /*
        * Método de pagamento:
        * Código Método de Pagamentos
        * 1 Cartão de Crédito - Temporário
        * 2 Cartão Visa
        * 3 Cartão MasterCard
        * 4 Cartão Diners
        * 5 Cartão American Express
        * 6 Cartão HiperCard
        * 10 Pagamento na Entrega
        * 11 Débito/Transferência Eletrônica
        * 12 Boleto Bancário
        * 13 Financiamento
        * 14 Cheque
        * 15 Depósito
        * Lembrando que alguns métodos de pagamento não
        * necessitam da analise do fcontrol (Ex.: boleto), pois o pagamento
        * ocorre antes do envio do produto (e não é possível o charge-back no
        * caso do boleto).
        */
        string TipoCartao = string.Empty;
        string Cartao_Bin = string.Empty;
        string Cartao_Fim = string.Empty;
        string Cartao_Numero_Mascarado = string.Empty;

        string condicaoDePagamentoDoPedido = rnPedidos.pedidoSeleciona_PorPedidoId(int.Parse(PedidoID)).Tables[0].Rows[0]["condDePagamentoId"].ToString();

        switch (rnCondicoesDePagamento.condicaoDePagamentoSeleciona_PorCondicaoId(int.Parse(condicaoDePagamentoDoPedido)).Tables[0].Rows[0]["condicaoNome"].ToString())
        {
            case "Visa":
                TipoCartao = "3";
                break;
            case "MasterCard":
                TipoCartao = "2";
                break;
            case "Dinners":
                TipoCartao = "1";
                break;
            case "American Express":
                TipoCartao = "5";
                break;
        }

        string Parcelas = rnPedidos.pedidoSeleciona_PorPedidoId(int.Parse(PedidoID)).Tables[0].Rows[0]["numeroDeParcelas"].ToString();

        //Dados do comprador

        int clienteId = int.Parse(rnPedidos.pedidoSeleciona_PorPedidoId(int.Parse(PedidoID)).Tables[0].Rows[0]["clienteId"].ToString());

        //Nome do comprador
        string Cobranca_Nome = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteNome"].ToString();

        //Data nascimento comprador
        string Cobranca_Nascimento = string.Empty;

        //Rua do comprador
        string Cobranca_Logradouro = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteRua"].ToString();

        //Número da rua do comprador
        string Cobranca_Logradouro_Numero = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteNumero"].ToString();

        //Complemento do endereço do comprador
        string Cobranca_Logradouro_Complemento = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteComplemento"].ToString(); ;

        //Bairro do endereço do comprador
        string Cobranca_Bairro = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteBairro"].ToString(); ;

        //Cidade do comprador
        string Cobranca_Cidade = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteCidade"].ToString(); ;

        //Estado do comprador
        string Cobranca_Estado = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteEstado"].ToString();

        //C.E.P. do comprador
        string Cobranca_CEP = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteCep"].ToString(); ;

        //País do comprador
        string Cobranca_Pais = "BRA"; // Os países devem estar no formato ISO 3166 Alpha 3

        //Telefone do comprador
        string Cobranca_Telefone = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteFoneResidencial"].ToString();

        //DDD do celular do comprador
        string Cobranca_DDD_Telefone = string.Empty; //Caso não o tenha deve-se passar um *.

        //Celular do comprador
        string Cobranca_Celular = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteFoneCelular"].ToString();

        //DDD Celular do comprador
        string Cobranca_DDD_Celular = "";

        //O email do comprador
        string Cobranca_Email = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteEmail"].ToString();

        //CPF/CNPJ do comprador
        string Cobranca_Documento = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteCPFCNPJ"].ToString();

        //Senha usado pelo comprador para acessar a loja
        string shopper_password = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteSenha"].ToString();

        //Dados de entrega
        string Entrega_Nome = "";
        string Entrega_Nascimento = "";
        string Entrega_Email = "";
        string Entrega_Documento = "";
        string Entrega_Logradouro = "";
        string Entrega_Logradouro_Numero = "";
        string Entrega_Logradouro_Complemento = "";
        string Entrega_Bairro = "";
        string Entrega_Cidade = "";
        string Entrega_Estado = "";
        string Entrega_CEP = "";
        string Entrega_Pais = "";
        string Entrega_DDD_Telefone = "";
        string Entrega_Telefone = "";
        string Entrega_DDD_Celular = "";
        string Entrega_Celular = "";

        if (rnPedidos.pedidoSeleciona_PorPedidoId(int.Parse(PedidoID)).Tables[0].Rows[0]["endNomeDoDestinatario"].ToString() != string.Empty)
        {
            //Nome dos dados de entrega
            Entrega_Nome = rnPedidos.pedidoSeleciona_PorPedidoId(int.Parse(PedidoID)).Tables[0].Rows[0]["endNomeDoDestinatario"].ToString();

            //Rua do endereço de entrega
            Entrega_Logradouro = rnPedidos.pedidoSeleciona_PorPedidoId(int.Parse(PedidoID)).Tables[0].Rows[0]["endRua"].ToString();

            //Número do endereço de entrega
            Entrega_Logradouro_Numero = rnPedidos.pedidoSeleciona_PorPedidoId(int.Parse(PedidoID)).Tables[0].Rows[0]["endNumero"].ToString();

            //Complemento do endereço de entrega
            Entrega_Logradouro_Complemento = rnPedidos.pedidoSeleciona_PorPedidoId(int.Parse(PedidoID)).Tables[0].Rows[0]["endComplemento"].ToString();

            //Bairro do endereço de entrega
            Entrega_Bairro = rnPedidos.pedidoSeleciona_PorPedidoId(int.Parse(PedidoID)).Tables[0].Rows[0]["endBairro"].ToString();

            //Cidade do endereço de entrega
            Entrega_Cidade = rnPedidos.pedidoSeleciona_PorPedidoId(int.Parse(PedidoID)).Tables[0].Rows[0]["endCidade"].ToString();

            //Estado do endereço de entrega
            Entrega_Estado = rnPedidos.pedidoSeleciona_PorPedidoId(int.Parse(PedidoID)).Tables[0].Rows[0]["endEstado"].ToString();

            //C.E.P. do endereço de entrega
            Entrega_CEP = rnPedidos.pedidoSeleciona_PorPedidoId(int.Parse(PedidoID)).Tables[0].Rows[0]["endCep"].ToString();

            //País do endereço de entrega
            Entrega_Pais = "BRA"; //Os países devem estar no formato ISO 3166 Alpha 3

            //Telefone dos dados de entrega
            Entrega_Telefone = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteFoneResidencial"].ToString();

            //DDD do telefone dos dados de entrega
            Entrega_DDD_Celular = "*"; //Caso não tenha o dado passar um *.

            //Celular dos dados de entrega
            Entrega_Celular = "*"; //Caso não tenha o dado passar um *.
        }
        else
        {
            //Nome dos dados de entrega
            Entrega_Nome = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteNome"].ToString();

            //Rua do endereço de entrega
            Entrega_Logradouro = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteRua"].ToString();

            //Número do endereço de entrega
            Entrega_Logradouro_Numero = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteNumero"].ToString();

            //Complemento do endereço de entrega
            Entrega_Logradouro_Complemento = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteComplemento"].ToString(); ;

            //Bairro do endereço de entrega
            Entrega_Bairro = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteBairro"].ToString(); ;

            //Cidade do endereço de entrega
            Entrega_Cidade = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteCidade"].ToString(); ;

            //Estado do endereço de entrega
            Entrega_Estado = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteEstado"].ToString();

            //C.E.P. do endereço de entrega
            Entrega_CEP = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteCep"].ToString(); ;

            //País do endereço de entrega
            Entrega_Pais = "BRA"; //Os países devem estar no formato ISO 3166 Alpha 3

            //Telefone dos dados de entrega
            Entrega_Telefone = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteFoneResidencial"].ToString();

            //DDD do telefone dos dados de entrega
            Entrega_DDD_Celular = "*"; //Caso não tenha o dado passar um *.

            //Celular dos dados de entrega
            Entrega_Celular = "*"; //Caso não tenha o dado passar um *.
        }

        //Esta consulta retorna um iframe do clearsale
        string url = "http://www.clearsale.com.br/start/Entrada/EnviarPedido.aspx?" +
                     "CodigoIntegracao=" + HttpUtility.UrlEncode(login) +
                     "&Data=" + Convert.ToDateTime(Data).ToString("dd-MM-yyyy HH:mm:ss") +
                     "&PedidoID=" + HttpUtility.UrlEncode(PedidoIDIntegracao) +
                     "&IP=" + HttpUtility.UrlEncode(IP) +
                     "&Total=" + HttpUtility.UrlEncode(Total) +
                     "&TipoPagamento=" + HttpUtility.UrlEncode(TipoCartao) +
                     "&Parcelas=" + HttpUtility.UrlEncode(Parcelas) +
                     "&Cartao_Bin=" + HttpUtility.UrlEncode(Cartao_Bin) +
                     "&Cartao_Fim=" + HttpUtility.UrlEncode(Cartao_Fim) +
                     "&Cartao_Numero_Mascarado=" + HttpUtility.UrlEncode(Cartao_Numero_Mascarado) +
                     "&Cobranca_Nome=" + HttpUtility.UrlEncode(Cobranca_Nome) +
                     "&Cobranca_Nascimento=" + HttpUtility.UrlEncode(Cobranca_Nascimento) +
                     "&Cobranca_Email=" + HttpUtility.UrlEncode(Cobranca_Email) +
                     "&Cobranca_Documento=" + HttpUtility.UrlEncode(Cobranca_Documento) +
                     "&Cobranca_Logradouro=" + HttpUtility.UrlEncode(Cobranca_Logradouro) +
                     "&Cobranca_Logradouro_Numero=" + HttpUtility.UrlEncode(Cobranca_Logradouro_Numero) +
                     "&Cobranca_Logradouro_Complemento=" + HttpUtility.UrlEncode(Cobranca_Logradouro_Complemento) +
                     "&Cobranca_Bairro=" + HttpUtility.UrlEncode(Cobranca_Bairro) +
                     "&Cobranca_Cidade=" + HttpUtility.UrlEncode(Cobranca_Cidade) +
                     "&Cobranca_Estado=" + HttpUtility.UrlEncode(Cobranca_Estado) +
                     "&Cobranca_CEP=" + HttpUtility.UrlEncode(Cobranca_CEP.Replace("-", "")) +
                     "&Cobranca_Pais=" + HttpUtility.UrlEncode(Cobranca_Pais) +
                     "&Cobranca_DDD_Telefone_1=" + HttpUtility.UrlEncode(Cobranca_DDD_Telefone.Replace("(", "").Replace(")", "")) +
                     "&Cobranca_Telefone_1=" + HttpUtility.UrlEncode(Cobranca_Telefone.Replace("-", "")) +
                     "&Cobranca_DDD_Celular_1=" + HttpUtility.UrlEncode(Cobranca_DDD_Celular.Replace("(", "").Replace(")", "")) +
                     "&Cobranca_Celular_1=" + HttpUtility.UrlEncode(Cobranca_Celular.Replace("-", "")) +
                     //"&Cobranca_Documento=" + HttpUtility.UrlEncode(Cobranca_Documento) +
                     "&Entrega_Nome=" + HttpUtility.UrlEncode(Entrega_Nome) +
                     "&Entrega_Nascimento=" + HttpUtility.UrlEncode(Entrega_Nascimento) +
                     "&Entrega_Email=" + HttpUtility.UrlEncode(Entrega_Email) +
                     "&Entrega_Documento=" + HttpUtility.UrlEncode(Entrega_Documento) +
                     "&Entrega_Logradouro=" + HttpUtility.UrlEncode(Entrega_Logradouro) +
                     "&Entrega_Logradouro_Numero=" + HttpUtility.UrlEncode(Entrega_Logradouro_Numero) +
                     "&Entrega_Logradouro_Complemento=" + HttpUtility.UrlEncode(Entrega_Logradouro_Complemento) +
                     "&Entrega_Bairro=" + HttpUtility.UrlEncode(Entrega_Bairro) +
                     "&Entrega_Cidade=" + HttpUtility.UrlEncode(Entrega_Cidade) +
                     "&Entrega_Estado=" + HttpUtility.UrlEncode(Entrega_Estado) +
                     "&Entrega_CEP=" + HttpUtility.UrlEncode(Entrega_CEP.Replace("-", "")) +
                     "&Entrega_Pais=" + HttpUtility.UrlEncode(Entrega_Pais) +
                     "&Entrega_DDD_Telefone_1=" + HttpUtility.UrlEncode(Entrega_DDD_Telefone.Replace("(", "").Replace(")", "")) +
                     "&Entrega_Telefone_1=" + HttpUtility.UrlEncode(Entrega_Telefone.Replace("-", "")) +
                     "&Entrega_DDD_Celular_1=" + HttpUtility.UrlEncode(Entrega_DDD_Celular.Replace("(", "").Replace(")", "")) +
                     "&Entrega_Celular_1=" + HttpUtility.UrlEncode(Entrega_Celular.Replace("-", ""));


        string[] ArrayItens = TodosItens.Split('#');

        //for (int i = 0; i < ArrayItens.Length - 1; i++)
        //{
        //    string[] ArrayItem = ArrayItens[i].Split('|');

        //    url += "&Item_ID_" + i + "=" + ArrayItem[0];
        //    url += "&Item_Nome_" + i + "=" + ArrayItem[1];
        //    url += "&Item_Qtd_" + i + "=" + ArrayItem[2];
        //    url += "&Item_Valor_" + i + "=" + ArrayItem[3];
        //    url += "&Item_Categoria_" + i + "=" + ArrayItem[4];

        //}

        for (int i = 0; i < 1; i++)
        {
            string[] ArrayItem = ArrayItens[i].Split('|');

            url += "&Item_ID_" + (i + 1) + "=" + ArrayItem[0];
            url += "&Item_Nome_" + (i + 1) + "=" + ArrayItem[1];
            url += "&Item_Qtd_" + (i + 1) + "=" + ArrayItem[2];
            url += "&Item_Valor_" + (i + 1) + "=" + decimal.Parse(ArrayItem[3]).ToString("##,###.00").Replace(",", ".");
            url += "&Item_Categoria_" + (i + 1) + "=" + ArrayItem[4];

        }


        //neste exemplo o retorno é um html com o frame para administração do pedido
        Response.Redirect(url);

    }

    private void ClearSale()
    {

        //Seu login e sus senha de acesso ao fcontrol (são fixos)
        string login = "f136e608-ff20-4db2-bd3d-0eb3961be7e2";

        //Abaixo os dados estão fixos mas devem ser substituídos pelos dados de seu pedido
        //Faça aqui a consulta em seu Banco de Dados para pegar os dados que devem ser adicionados ao Redirecionamento abaixo.

        //Dados da compra

        //Data em que o pedido foi feito
        string date_entered = rnPedidos.pedidoSeleciona_PorPedidoId(int.Parse(Request["codPedido"])).Tables[0].Rows[0]["dataHoraDoPedido"].ToString(); //A data deve estar no formato aaaa-mm-dd hh:mm:ss

        //Código do pedido
        string Order_Id = Request["codPedido"]; //Máximo de 26 caracteres, deve ser único pois ele identifica o pedido.

        //Ip do usuário que realizou a transação
        string ip = rnPedidos.pedidoSeleciona_PorPedidoId(int.Parse(Order_Id)).Tables[0].Rows[0]["ipDoCliente"].ToString();// pode ser para um * caso você não tenha o ip

        // número de itens (ex.: 5 lápis)
        string items_tot = rnPedidos.itensPedidoConta(int.Parse(Order_Id)).Tables[0].Rows[0]["quantidadeTotalDeItens"].ToString();

        // número de itens DISTINTOS (ex.: 5 lápis, só o tipo lápis então  = 1)
        string items_dif = int.Parse(rnPedidos.itensPedidoConta(int.Parse(Order_Id)).Tables[0].Rows[0]["quantidadeDeItensDestinstos"].ToString()).ToString();

        string total = (decimal.Parse(rnPedidos.pedidoSeleciona_PorPedidoId(int.Parse(Order_Id)).Tables[0].Rows[0]["valorCobrado"].ToString())).ToString("##,###.00").Replace(",", ".");

        /*
        * Método de pagamento:
        * Código Método de Pagamentos
        * 1 Cartão de Crédito - Temporário
        * 2 Cartão Visa
        * 3 Cartão MasterCard
        * 4 Cartão Diners
        * 5 Cartão American Express
        * 6 Cartão HiperCard
        * 10 Pagamento na Entrega
        * 11 Débito/Transferência Eletrônica
        * 12 Boleto Bancário
        * 13 Financiamento
        * 14 Cheque
        * 15 Depósito
        * Lembrando que alguns métodos de pagamento não
        * necessitam da analise do fcontrol (Ex.: boleto), pois o pagamento
        * ocorre antes do envio do produto (e não é possível o charge-back no
        * caso do boleto).
        */
        string payment_method_id = string.Empty;
        string condicaoDePagamentoDoPedido = rnPedidos.pedidoSeleciona_PorPedidoId(int.Parse(Order_Id)).Tables[0].Rows[0]["condDePagamentoId"].ToString();

        switch (rnCondicoesDePagamento.condicaoDePagamentoSeleciona_PorCondicaoId(int.Parse(condicaoDePagamentoDoPedido)).Tables[0].Rows[0]["condicaoNome"].ToString())
        {
            case "Visa":
                payment_method_id = "2";
                break;
            case "MasterCard":
                payment_method_id = "3";
                break;
            case "Dinners":
                payment_method_id = "4";
                break;
            case "American Express":
                payment_method_id = "5";
                break;
        }

        string num_parcelas = rnPedidos.pedidoSeleciona_PorPedidoId(int.Parse(Order_Id)).Tables[0].Rows[0]["numeroDeParcelas"].ToString();

        //Dados do comprador

        int clienteId = int.Parse(rnPedidos.pedidoSeleciona_PorPedidoId(int.Parse(Order_Id)).Tables[0].Rows[0]["clienteId"].ToString());

        //Nome do comprador
        string shopper_name = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteNome"].ToString();

        //Rua do comprador
        string shopper_street = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteRua"].ToString();

        //Número da rua do comprador
        string shopper_street_number = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteNumero"].ToString();

        //Complemento do endereço do comprador
        string shopper_street_compl = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteComplemento"].ToString(); ;

        //Bairro do endereço do comprador
        string shopper_street_district = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteBairro"].ToString(); ;

        //Cidade do comprador
        string shopper_city = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteCidade"].ToString(); ;

        //Estado do comprador
        string shopper_state = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteEstado"].ToString();

        //C.E.P. do comprador
        string shopper_zip = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteCep"].ToString(); ;

        //País do comprador
        string shopper_country = "BRA"; // Os países devem estar no formato ISO 3166 Alpha 3

        //Telefone do comprador
        string shopper_phone = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteFoneResidencial"].ToString();

        //DDD do celular do comprador
        string shopper_ddd_cel_phone = "*"; //Caso não o tenha deve-se passar um *.

        //Celular do comprador
        string shopper_cel_phone = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteFoneCelular"].ToString();

        //O email do comprador
        string shopper_email = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteEmail"].ToString();

        //CPF/CNPJ do comprador
        string shopper_cpf_cnpj = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteCPFCNPJ"].ToString();

        //Senha usado pelo comprador para acessar a loja
        string shopper_password = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteSenha"].ToString();

        //Dados de entrega
        string ship_to_name;
        string ship_to_street;
        string ship_to_street_number;
        string ship_to_street_compl;
        string ship_to_street_district;
        string ship_to_city;
        string ship_to_state;
        string ship_to_zip;
        string ship_to_country;
        string ship_to_ddd_phone;
        string ship_to_phone;
        string ship_to_ddd_cel_phone;
        string ship_to_cel_phone;
        if (rnPedidos.pedidoSeleciona_PorPedidoId(int.Parse(Order_Id)).Tables[0].Rows[0]["endNomeDoDestinatario"].ToString() != string.Empty)
        {
            //Nome dos dados de entrega
            ship_to_name = rnPedidos.pedidoSeleciona_PorPedidoId(int.Parse(Order_Id)).Tables[0].Rows[0]["endNomeDoDestinatario"].ToString();

            //Rua do endereço de entrega
            ship_to_street = rnPedidos.pedidoSeleciona_PorPedidoId(int.Parse(Order_Id)).Tables[0].Rows[0]["endRua"].ToString();

            //Número do endereço de entrega
            ship_to_street_number = rnPedidos.pedidoSeleciona_PorPedidoId(int.Parse(Order_Id)).Tables[0].Rows[0]["endNumero"].ToString();

            //Complemento do endereço de entrega
            ship_to_street_compl = rnPedidos.pedidoSeleciona_PorPedidoId(int.Parse(Order_Id)).Tables[0].Rows[0]["endComplemento"].ToString();

            //Bairro do endereço de entrega
            ship_to_street_district = rnPedidos.pedidoSeleciona_PorPedidoId(int.Parse(Order_Id)).Tables[0].Rows[0]["endBairro"].ToString();

            //Cidade do endereço de entrega
            ship_to_city = rnPedidos.pedidoSeleciona_PorPedidoId(int.Parse(Order_Id)).Tables[0].Rows[0]["endCidade"].ToString();

            //Estado do endereço de entrega
            ship_to_state = rnPedidos.pedidoSeleciona_PorPedidoId(int.Parse(Order_Id)).Tables[0].Rows[0]["endEstado"].ToString();

            //C.E.P. do endereço de entrega
            ship_to_zip = rnPedidos.pedidoSeleciona_PorPedidoId(int.Parse(Order_Id)).Tables[0].Rows[0]["endCep"].ToString();

            //País do endereço de entrega
            ship_to_country = "BRA"; //Os países devem estar no formato ISO 3166 Alpha 3

            //Telefone dos dados de entrega
            ship_to_phone = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteFoneResidencial"].ToString();

            //DDD do telefone dos dados de entrega
            ship_to_ddd_cel_phone = "*"; //Caso não tenha o dado passar um *.

            //Celular dos dados de entrega
            ship_to_cel_phone = "*"; //Caso não tenha o dado passar um *.
        }
        else
        {
            //Nome dos dados de entrega
            ship_to_name = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteNome"].ToString();

            //Rua do endereço de entrega
            ship_to_street = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteRua"].ToString();

            //Número do endereço de entrega
            ship_to_street_number = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteNumero"].ToString();

            //Complemento do endereço de entrega
            ship_to_street_compl = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteComplemento"].ToString(); ;

            //Bairro do endereço de entrega
            ship_to_street_district = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteBairro"].ToString(); ;

            //Cidade do endereço de entrega
            ship_to_city = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteCidade"].ToString(); ;

            //Estado do endereço de entrega
            ship_to_state = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteEstado"].ToString();

            //C.E.P. do endereço de entrega
            ship_to_zip = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteCep"].ToString(); ;

            //País do endereço de entrega
            ship_to_country = "BRA"; //Os países devem estar no formato ISO 3166 Alpha 3

            //Telefone dos dados de entrega
            ship_to_phone = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteFoneResidencial"].ToString();

            //DDD do telefone dos dados de entrega
            ship_to_ddd_cel_phone = "*"; //Caso não tenha o dado passar um *.

            //Celular dos dados de entrega
            ship_to_cel_phone = "*"; //Caso não tenha o dado passar um *.
        }

        //Esta consulta retorna um iframe do fcontrol, já prove a interface visual.
        string url = "http://www.clearsale.com.br/integracaov2/freeclearsale/frame.aspx?" +
                     "login=" + HttpUtility.UrlEncode(login) +
                     "&date_entered=" + HttpUtility.UrlEncode(date_entered) +
                     "&Order_Id=" + HttpUtility.UrlEncode(Order_Id) +
                     "&ip=" + HttpUtility.UrlEncode(ip) +
                     "&shopper_name=" + HttpUtility.UrlEncode(shopper_name) +
                     "&shopper_street=" + HttpUtility.UrlEncode(shopper_street) +
                     "&shopper_street_number=" + HttpUtility.UrlEncode(shopper_street_number) +
                     "&shopper_street_compl=" + HttpUtility.UrlEncode(shopper_street_compl) +
                     "&shopper_street_district=" + HttpUtility.UrlEncode(shopper_street_district) +
                     "&shopper_city=" + HttpUtility.UrlEncode(shopper_city) +
                     "&shopper_state=" + HttpUtility.UrlEncode(shopper_state) +
                     "&shopper_zip=" + HttpUtility.UrlEncode(shopper_zip) +
                     "&shopper_country=" + HttpUtility.UrlEncode(shopper_country) +
                     "&shopper_phone=" + HttpUtility.UrlEncode(shopper_phone) +
                     "&shopper_ddd_cel_phone=" + HttpUtility.UrlEncode(shopper_ddd_cel_phone) +
                     "&shopper_cel_phone=" + HttpUtility.UrlEncode(shopper_cel_phone) +
                     "&shopper_email=" + HttpUtility.UrlEncode(shopper_email) +
                     "&shopper_cpf_cnpj=" + HttpUtility.UrlEncode(shopper_cpf_cnpj) +
                     "&shopper_password=" + HttpUtility.UrlEncode(shopper_password) +
                     "&ship_to_name=" + HttpUtility.UrlEncode(ship_to_name) +
                     "&ship_to_street=" + HttpUtility.UrlEncode(ship_to_street) +
                     "&ship_to_street_number=" + HttpUtility.UrlEncode(ship_to_street_number) +
                     "&ship_to_street_compl=" + HttpUtility.UrlEncode(ship_to_street_compl) +
                     "&ship_to_street_district=" + HttpUtility.UrlEncode(ship_to_street_district) +
                     "&ship_to_city=" + HttpUtility.UrlEncode(ship_to_city) +
                     "&ship_to_state=" + HttpUtility.UrlEncode(ship_to_state) +
                     "&ship_to_zip=" + HttpUtility.UrlEncode(ship_to_zip) +
                     "&ship_to_country=" + HttpUtility.UrlEncode(ship_to_country) +
                     "&ship_to_phone=" + HttpUtility.UrlEncode(ship_to_phone) +
                     "&ship_to_ddd_cel_phone=" + HttpUtility.UrlEncode(ship_to_ddd_cel_phone) +
                     "&ship_to_cel_phone=" + HttpUtility.UrlEncode(ship_to_cel_phone) +
                     "&items_dif=" + HttpUtility.UrlEncode(items_dif) +
                     "&items_tot=" + HttpUtility.UrlEncode(items_tot) +
                     "&total=" + HttpUtility.UrlEncode(total) +
                     "&payment_method_id=" + HttpUtility.UrlEncode(payment_method_id) +
                     "&num_parcelas=" + HttpUtility.UrlEncode(num_parcelas);

        //*Observação: para retornar um documento XML, deve-se adicionar o parâmetro &formato=XML,
        //neste exemplo o retorno é um html com o frame para administração do pedido
        Response.Redirect(url);

    }
}
