﻿
using System;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxGridView;
using System.Data;
using System.Text;
using System.Collections.Generic;


public partial class admin_pedidoNaoAutorizado : System.Web.UI.Page
{
    #region class , globals 
    int pedidoIDAtual;
    #endregion

    #region load
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {

            CarregaGrid(true);
            txtDataProximoAtend.Value = DateTime.Now;
            txtHoraProximoAtend.Value = DateTime.Now;

        }
        
    }
    #endregion


    #region  Eventos

    //bt
    protected void btnAgendar_OnCommand(object sender, CommandEventArgs e)
    {

        frmAgendamento.ShowOnPageLoad = true;
        hdfPedidoId.Value = e.CommandArgument.ToString();
        pedidoIDAtual = Convert.ToInt32(hdfPedidoId.Value);

    }
    protected void btnHistorico_OnCommand(object sender, CommandEventArgs e)
    {
        frmHistorico.ShowOnPageLoad = true;
        hdfPedidoId.Value = e.CommandArgument.ToString();
        pedidoIDAtual = Convert.ToInt32(hdfPedidoId.Value);
        CarregaGridHistorico(true);
    }

    
    protected void btRefresh_OnClick(object sender, EventArgs e)
    {
        CarregaGrid(true);
    }


    protected void btnVoltar_OnClick(object sender, EventArgs e)
    {

        Response.Redirect("pedidoNaoAutorizado.aspx");
    }

    protected void btnComentar_OnClick(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(txtComentario.Text))
        {
            AlertShow("Favor preencher o campo comentário ");
            txtComentario.Focus();
            return;
        }

        pedidoIDAtual = Convert.ToInt32(hdfPedidoId.Value);
        var atendimentos = getAtendimentoByPedido(pedidoIDAtual);

        if (atendimentos.Count > 0)
        {   
            AtendimentoGenerico(atendimentos.First(), 1); 
        }
        else
        {           
            AtendimentoGenerico(new tbAtendimento(), 2);
        }

    }

    protected void btnAgendamento_OnClick(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(txtComentario.Text))
        {
            AlertShow("Favor preencher o campo comentário ");
            txtComentario.Focus();
            return;
        }

        pedidoIDAtual = Convert.ToInt32(hdfPedidoId.Value);

        var atendimentos = getAtendimentoByPedido(pedidoIDAtual);
     
       // if (!atendimentos.First().observacao.Contains("/AGENDAMENTO:"))
           // AtendimentoGenerico(atendimentos.First(), 4);
       // else
            AtendimentoGenerico(new tbAtendimento(), 3);
      
    }

   


    //grd
    protected void grdpedidoNaoAutorizado_CustomData(object sender, ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "pedidoIdCliente")
        {
            int pedidoId = Convert.ToInt32(e.GetListSourceFieldValue("pedidoId"));
            if (pedidoId > 0) e.Value = rnFuncoes.retornaIdCliente(pedidoId);
        }

    }



    protected void grdpedidoNaoAutorizado_RowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {

        // tratar retorno Environment.NewLine
        //if (string.IsNullOrEmpty(grdpedidoNaoAutorizado.GetRowValues(e.VisibleIndex, new string[] { "observacao" }).ToString()))
        //{
        //    string descComent = grd.GetRowValues(e.VisibleIndex, new string[] { "observacao" }).ToString();
        //    descComent = descComent.Replace(Environment.NewLine, "<br />");

        //}

        if (e.RowType != GridViewRowType.Data)
        {
            return;
        }
        var grd = (ASPxGridView)sender;

        


           int pedidoId = 0;
          int.TryParse(grd.GetRowValues(e.VisibleIndex, new string[] { "pedidoId" }).ToString(), out pedidoId);

        DateTime dataProxAt; 
        DateTime.TryParse(grd.GetRowValues(e.VisibleIndex, new string[] { "dataProxAtendimento" }).ToString(), out dataProxAt);
    

        if (dataProxAt.Date < DateTime.Now.Date)
          
            {
                int i = 0;
                while ((i < e.Row.Cells.Count))
                {
                    e.Row.Cells[i].Style.Add("background", "#FFC6C7");
                    i++;
                }
            }

        else if (dataProxAt.Date == DateTime.Now.Date)
        {
            int i = 0;
            while ((i < e.Row.Cells.Count))
            {
                e.Row.Cells[i].Style.Add("background", "#FFFF99");
                i++;
            }
        }
        

    }

    protected void grdSemAgendamento_CustomData(object sender, ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "pedidoIdCliente")
        {
            int pedidoId = Convert.ToInt32(e.GetListSourceFieldValue("pedidoId"));
            if (pedidoId > 0) e.Value = rnFuncoes.retornaIdCliente(pedidoId);
        }

    }

    protected void grdSemAgendamento_RowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType != GridViewRowType.Data)
        {
            return;
        }

        int pedidoId = 0;

        int.TryParse(grdSemAgendamento.GetRowValues(e.VisibleIndex, new string[] { "pedidoId" }).ToString(), out pedidoId);

    }

    #endregion 





    #region  Activitys  / functions / camada dados 

    private void CarregaGrid(bool prCarrega)
    {
       

        var dB = new dbCommerceDataContext();

        var pedidos = (from tbPed in dB.tbPedidos
                       join cli in dB.tbClientes on tbPed.clienteId equals cli.clienteId
                       join atend in dB.tbAtendimentos on tbPed.pedidoId equals atend.pedidoId into _ate
                       from atend in _ate.DefaultIfEmpty()
                       join usu in dB.tbUsuarios on atend.usuarioId equals usu.usuarioId into _usu
                       from usu in _usu.DefaultIfEmpty()
                       where tbPed.statusDoPedido == 7
                       select new
                       {
                           pedidoId = tbPed.pedidoId,
                           observacao = atend.observacao,
                           dataPedido = tbPed.dataHoraDoPedido,
                           dataAtendimento = atend.dataAtendimento,
                           dataProxAtendimento = atend.dataProxAtendimento,
                           atendente = usu.usuarioNome,
                           nomeCliente = cli.clienteNome,
                           contatoCliente = cli.clienteFoneCelular,
                           pedidosApos = (from c in dB.tbPedidos where c.dataHoraDoPedido > tbPed.dataHoraDoPedido && c.clienteId == cli.clienteId select c).Count()
                       }).Where(x => x.pedidosApos == 0).ToList().OrderByDescending(x => x.dataProxAtendimento);



        var linqResult = pedidos.Where(x => x.dataProxAtendimento != null).GroupBy(x => x.pedidoId).Select(g => g.First()).ToList();

        var linqResultNo = pedidos.Where(x => x.dataProxAtendimento == null).GroupBy(x => x.pedidoId).Select(g => g.First()).ToList();

        grdpedidoNaoAutorizado.DataSource = linqResult.OrderBy(x => x.dataProxAtendimento);
        grdSemAgendamento.DataSource = linqResultNo.OrderByDescending(x => x.dataPedido);

        grdpedidoNaoAutorizado.DataBind();
        grdSemAgendamento.DataBind();
    }

    private void CarregaGridHistorico(bool prCarrega)
    {

        var dB = new dbCommerceDataContext();

        var linqResult = (from tbPed in dB.tbPedidos
                          join cli in dB.tbClientes on tbPed.clienteId equals cli.clienteId
                          join atend in dB.tbAtendimentos on tbPed.pedidoId equals atend.pedidoId into _ate
                          from atend in _ate.DefaultIfEmpty()
                          join usu in dB.tbUsuarios on atend.usuarioId equals usu.usuarioId into _usu
                          from usu in _usu.DefaultIfEmpty()
                          where tbPed.statusDoPedido == 7 && tbPed.pedidoId == pedidoIDAtual
                          select new
                          {
                              pedidoId = tbPed.pedidoId,
                              observacao = atend.observacao,
                              dataAtendimento = atend.dataAtendimento,
                              dataProxAtendimento = atend.dataProxAtendimento,
                              atendente = usu.usuarioNome

                          }).ToList().OrderByDescending(x => x.dataAtendimento);

        grdHistorico.DataSource = linqResult;
        grdHistorico.DataBind();
    }

    public void adicionaInteracaoPedido(string interacao, string alteracaoDeEstatus, int pedidoId)
    {
        HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
        usuarioLogadoId = HttpContext.Current.Request.Cookies["usuarioLogadoId"];
        if (usuarioLogadoId != null)
        {
            rnInteracoes.interacaoInclui(pedidoId, interacao, rnUsuarios.usuarioSeleciona_PorUsuarioId(int.Parse(usuarioLogadoId.Value.ToString())).Tables[0].Rows[0]["usuarioNome"].ToString(), alteracaoDeEstatus);
        }
    }

    public void AlertShow(string message)
    {
        base.ClientScript.RegisterStartupScript(base.GetType(), "myalert", "alert('" + message + "'); ", true);
    }

    public void AtendimentoGenerico(tbAtendimento prtbAtendimento, int operacao)
    {

        HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
        usuarioLogadoId = Request.Cookies["usuarioLogadoId"];
        int idUsuario = Convert.ToInt32(usuarioLogadoId.Value.ToString());

        var DB = new dbCommerceDataContext();

        tbAtendimento Atendimento = new tbAtendimento();
        Atendimento = prtbAtendimento;


         var dataAgendamento = DateTime.Now;
         DateTime.TryParse(txtDataProximoAtend.Text + " " + txtHoraProximoAtend.Text, out dataAgendamento);

        switch (operacao)
        {
            //Comentario  update
            case 1:

                
                if (Atendimento.observacao.Contains( "AGENDAMENTO:"))  // ordenando agendamento com prioridade
                    Atendimento.observacao =  prtbAtendimento.observacao + Environment.NewLine + " OBSERVAÇÃO ADICIONAL: " + txtComentario.Text ;
                else
                    Atendimento.observacao = Environment.NewLine +  " OBSERVAÇÃO ADICIONAL: " + txtComentario.Text + prtbAtendimento.observacao;
                    UpdateAtendimento(Atendimento);
                break;


            //Comentario novo
            case 2:

                prtbAtendimento.observacao = Environment.NewLine +  " OBSERVAÇÃO ADICIONAL: " + txtComentario.Text ;
                prtbAtendimento.pedidoId = pedidoIDAtual;
                prtbAtendimento.usuarioId = idUsuario;

                DB.tbAtendimentos.InsertOnSubmit(prtbAtendimento);
                DB.SubmitChanges();

                break;


            //  Agendamento novo
            case 3:
                prtbAtendimento.dataAtendimento = DateTime.Now;
                prtbAtendimento.dataProxAtendimento = dataAgendamento;
                prtbAtendimento.observacao = Environment.NewLine + " AGENDAMENTO:  " + txtComentario.Text ;
                prtbAtendimento.pedidoId = pedidoIDAtual;
                prtbAtendimento.usuarioId = idUsuario;
                DB.tbAtendimentos.InsertOnSubmit(prtbAtendimento);
                DB.SubmitChanges();

                break;


            // Agendamento update
            case 4:

                prtbAtendimento.observacao = Environment.NewLine + "  AGENDAMENTO: " + txtComentario.Text  + prtbAtendimento.observacao;
                if (prtbAtendimento.dataAtendimento == null) prtbAtendimento.dataAtendimento = DateTime.Now;
                if (prtbAtendimento.dataAtendimento == null) prtbAtendimento.dataProxAtendimento = dataAgendamento;
                prtbAtendimento.pedidoId = pedidoIDAtual;
                prtbAtendimento.usuarioId = idUsuario;
                UpdateAtendimento(Atendimento);
                break;         
        }


        var interacao = new StringBuilder();
        interacao.AppendFormat("<b>Observação adicional para agendamento de pedidos nao autorizados.</b><br>");
        interacao.AppendFormat("Comentario: " + txtComentario.Text);
        adicionaInteracaoPedido(interacao.ToString(), "False", pedidoIDAtual);

        CarregaGrid(true);

        txtComentario.Text = "";
        Response.Redirect("pedidoNaoAutorizado.aspx");

    }


    public List<tbAtendimento> getAtendimentoByPedido(int prPedido)
    {

       // pedidoIDAtual = Convert.ToInt32(hdfPedidoId.Value);

        var dbAtend = new dbCommerceDataContext();

        var linqResult = (from atend in dbAtend.tbAtendimentos
                          join ped in dbAtend.tbPedidos on atend.pedidoId equals ped.pedidoId
                    where atend.pedidoId == prPedido
                          select atend).OrderByDescending(x => x.dataAtendimento).ToList();
     
        return linqResult;
    }

    public void UpdateAtendimento(tbAtendimento prtbAtendimento)
    {

        var db = new dbCommerceDataContext();

        var linqResult = (from atend in db.tbAtendimentos
                          join ped in db.tbPedidos on atend.pedidoId equals ped.pedidoId
                          where atend.idAtendimento == prtbAtendimento.idAtendimento
                          select atend).ToList().First();

        linqResult.observacao = prtbAtendimento.observacao;

        db.SubmitChanges();

    }


    public void InsertAtendimento(tbAtendimento prtbAtendimento)
    {
        var db = new dbCommerceDataContext();

        db.tbAtendimentos.InsertOnSubmit(prtbAtendimento);
        db.SubmitChanges();

    }

    public void DeleteAtendimento(tbAtendimento prtbAtendimento)
    {
        var db = new dbCommerceDataContext();

        db.tbAtendimentos.DeleteOnSubmit(prtbAtendimento);
        db.SubmitChanges();

    }

    #endregion



}