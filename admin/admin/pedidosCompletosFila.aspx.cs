﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Utils.About;
using DevExpress.Web.ASPxGridView;

public partial class admin_pedidosCompletosFila : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        fillGrid(false);

        if (!IsPostBack)
        {

            var usuarioLogadoId = Request.Cookies["usuarioLogadoId"];

            if (usuarioLogadoId != null)
            {
                /*btnPriorizarMoveis.Visible =
                    rnUsuarios.usuarioPaginasPermitidaSeleciona_PorUsuarioIdPaginaPermitidaNome(
                        int.Parse(usuarioLogadoId.Value), "priorizarmoveiscarolinaplanet").Tables[0].Rows.Count == 1;


                btnPriorizarMoveisEm1.Visible =
                    rnUsuarios.usuarioPaginasPermitidaSeleciona_PorUsuarioIdPaginaPermitidaNome(
                        int.Parse(usuarioLogadoId.Value), "priorizarmoveisem1cd2").Tables[0].Rows.Count == 1;*/
            }
        }

        if (Request.QueryString["reiniciarSeparacao"] != null)
        {
            try
            {
                int pedidoId = int.Parse(Request.QueryString["reiniciarSeparacao"]);

                if (pedidoId.ToString().Length > 6)
                {
                    pedidoId = rnFuncoes.retornaIdInterno(pedidoId);
                }

                using (var data = new dbCommerceDataContext())
                {
                    var pedido = (from c in data.tbPedidos where c.pedidoId == pedidoId select c).First();
                    pedido.idUsuarioSeparacao = null;
                    pedido.dataInicioSeparacao = null;
                    pedido.dataFimSeparacao = null;
                    pedido.separado = false;
                    pedido.statusDoPedido = 11;
                    data.SubmitChanges();

                    var envio = (from c in data.tbPedidoEnvios where c.idPedido == pedidoId && c.dataInicioEmbalagem == null orderby c.idPedidoEnvio descending select c).FirstOrDefault();
                    if (envio != null)
                    {
                        var pacotes = (from c in data.tbPedidoPacotes where c.idPedidoEnvio == envio.idPedidoEnvio select c);
                        foreach (var pacote in pacotes)
                        {
                            data.tbPedidoPacotes.DeleteOnSubmit(pacote);
                        }
                        data.SubmitChanges();

                        var itensSeparados = (from c in data.tbProdutoEstoques where c.idPedidoEnvio == envio.idPedidoEnvio select c).ToList();
                        foreach (var itensSeparado in itensSeparados)
                        {
                            itensSeparado.idPedidoEnvio = null;
                            itensSeparado.pedidoId = null;
                            itensSeparado.itemPedidoId = null;
                            data.SubmitChanges();
                        }
                        data.tbPedidoEnvios.DeleteOnSubmit(envio);
                        data.SubmitChanges();
                    }
                }



                fillGrid(true);
                ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('Separação reiniciada com sucesso!');", true);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('Falha na reinicialização da separação! ERRO: " + ex.Message + "');", true);
            }
        }
    }
    private void fillGrid(bool rebind)
    {
        //grd.DataSource = sql;
        var data = new dbCommerceDataContext();
        if (rdbCd1e2.Checked)
        {
            var prioridadeEnvio = DateTime.Now.AddDays(2);
            var prioridadeEnvioGrande = DateTime.Now.AddDays(50);
            var pedidos = (from c in data.tbPedidos
                           join d in data.tbItemPedidoEstoques on c.pedidoId equals d.tbItensPedido.pedidoId into itensEstoque
                           join e in data.tbNotaFiscals on c.pedidoId equals e.idPedido into notas
                           where c.statusDoPedido == 11 &&
                           c.envioLiberado == true && c.itensPendenteEnvioCd1 > 0
                           orderby (c.entregaEnviarPor == "JadlogExpressa" ? 0 : c.prioridadeEnvio == null ? 9999 : c.prioridadeEnvio),
                           c.prazoMaximoPostagemAtualizado > prioridadeEnvio ? prioridadeEnvioGrande : c.prazoMaximoPostagemAtualizado,
                           c.dataHoraDoPedido
                           select new {
                               c.pedidoId,
                               c.endCidade,
                               c.endEstado,
                               c.endCep,
                               c.prazoFinalPedido,
                               c.prazoMaximoPostagemAtualizado,
                               c.dataInicioSeparacao,
                               c.dataFimSeparacao,
                               c.prioridadeEnvio,
                               c.formaDeEnvio,
                               itens = c.itensPendenteEnvioCd1,
                               separado = c.separadoCd1,
                               fornecedores = string.Join(",", (from g in data.tbItemPedidoEstoques where g.tbItensPedido.pedidoId == c.pedidoId && (g.cancelado == false && g.enviado == false && g.reservado == true && (g.idCentroDistribuicao == 1 | g.idCentroDistribuicao == 2)) select g.tbProduto.tbProdutoFornecedor.fornecedorNome).Distinct().ToArray())
                           }).ToList();
            grd.DataSource = pedidos;
        }
        else if (rdbCd1.Checked)
        {
            var prioridadeEnvio = DateTime.Now.AddDays(2);
            var prioridadeEnvioGrande = DateTime.Now.AddDays(50);
            var pedidos = (from c in data.tbPedidos
                           join d in data.tbItemPedidoEstoques on c.pedidoId equals d.tbItensPedido.pedidoId into itensEstoque
                           join e in data.tbNotaFiscals on c.pedidoId equals e.idPedido into notas
                           where c.statusDoPedido == 11 &&
                           c.envioLiberado == true && c.itensPendenteEnvioCd1 > 0 && c.itensPendetenEnvioCd2 == 0
                           orderby (c.entregaEnviarPor == "JadlogExpressa" ? 0 : c.prioridadeEnvio == null ? 9999 : c.prioridadeEnvio),
                           c.prazoMaximoPostagemAtualizado > prioridadeEnvio ? prioridadeEnvioGrande : c.prazoMaximoPostagemAtualizado,
                           c.dataHoraDoPedido
                           select new
                           {
                               c.pedidoId,
                               c.endCidade,
                               c.endEstado,
                               c.endCep,
                               c.prazoFinalPedido,
                               c.prazoMaximoPostagemAtualizado,
                               c.dataInicioSeparacao,
                               c.dataFimSeparacao,
                               c.prioridadeEnvio,
                               c.formaDeEnvio,
                               itens = c.itensPendenteEnvioCd1,
                               separado = c.separadoCd1,
                               fornecedores = string.Join(",", (from g in data.tbItemPedidoEstoques where g.tbItensPedido.pedidoId == c.pedidoId && (g.cancelado == false && g.enviado == false && g.reservado == true && g.idCentroDistribuicao == 1) select g.tbProduto.tbProdutoFornecedor.fornecedorNome).Distinct().ToArray())
                           }).ToList();
            grd.DataSource = pedidos;
        }
        else if (rdbCd2.Checked)
        {
            var prioridadeEnvio = DateTime.Now.AddDays(2);
            var prioridadeEnvioGrande = DateTime.Now.AddDays(50);
            var pedidos = (from c in data.tbPedidos
                           join d in data.tbItemPedidoEstoques on c.pedidoId equals d.tbItensPedido.pedidoId into itensEstoque
                           join e in data.tbNotaFiscals on c.pedidoId equals e.idPedido into notas
                           where c.statusDoPedido == 11 &&
                           c.envioLiberadoCd2 == true && c.itensPendetenEnvioCd2 > 0 && c.itensPendenteEnvioCd1 == 0
                           && itensEstoque.Count(x => x.cancelado == false && x.enviado == false) > 0 && (c.envioLiberadoCd2 == true | (c.itensPendetenEnvioCd2 > 0 && c.separadoCd2 == true))
                           orderby (c.entregaEnviarPor == "JadlogExpressa" ? 0 : c.prioridadeEnvio == null ? 9999 : c.prioridadeEnvio),
                           c.prazoMaximoPostagemAtualizado > prioridadeEnvio ? prioridadeEnvioGrande : c.prazoMaximoPostagemAtualizado,
                           c.dataHoraDoPedido
                           select new
                           {
                               c.pedidoId,
                               c.endCidade,
                               c.endEstado,
                               c.endCep,
                               c.prazoFinalPedido,
                               c.prazoMaximoPostagemAtualizado,
                               c.dataInicioSeparacao,
                               c.dataFimSeparacao,
                               c.prioridadeEnvio,
                               c.formaDeEnvio,
                               itens = c.itensPendetenEnvioCd2,
                               separado = c.separadoCd2,
                               fornecedores = string.Join(",", (from g in data.tbItemPedidoEstoques where g.tbItensPedido.pedidoId == c.pedidoId && (g.cancelado == false && g.enviado == false && g.reservado == true && g.idCentroDistribuicao == 2) select g.tbProduto.tbProdutoFornecedor.fornecedorNome).Distinct().ToArray())
                           }).ToList();
            grd.DataSource = pedidos;
        }
        else if (rdbCd3.Checked)
        {
            var pedidos = (from c in data.tbPedidos
                           join d in data.tbItemPedidoEstoques on c.pedidoId equals d.tbItensPedido.pedidoId into itensEstoque
                           where c.statusDoPedido == 11
                           && itensEstoque.Count(x => x.cancelado == false && x.enviado == false && x.idCentroDistribuicao == 3) > 0 && c.envioLiberadoCd3 == true
                           orderby (c.entregaEnviarPor == "JadlogExpressa" ? 0 : c.prioridadeEnvio == null ? 9999 : c.prioridadeEnvio),
                                    (c.prazoMaximoPostagemAtualizado != null ?
                                    c.prazoMaximoPostagemAtualizado :
                                    c.prazoFinalPedido == null ? DateTime.Now : c.prazoFinalPedido),
                           c.dataHoraDoPedido
                           select new
                           {
                               c.pedidoId,
                               c.endCidade,
                               c.endEstado,
                               c.endCep,
                               c.prazoFinalPedido,
                               c.prazoMaximoPostagemAtualizado,
                               c.dataInicioSeparacao,
                               c.dataFimSeparacao,
                               c.prioridadeEnvio,
                               c.formaDeEnvio,
                               itens = c.itensPendetenEnvioCd3,
                               separado = c.separadoCd3,
                               fornecedores = string.Join(",", (from g in data.tbItemPedidoEstoques where g.tbItensPedido.pedidoId == c.pedidoId && (g.cancelado == false && g.enviado == false && g.reservado == true && g.idCentroDistribuicao == 3) select g.tbProduto.tbProdutoFornecedor.fornecedorNome).Distinct().ToArray())
                           }).ToList();
            grd.DataSource = pedidos;
        }
        else if (rdbCd4.Checked)
        {
            var pedidos = (from c in data.tbPedidos
                           join d in data.tbItemPedidoEstoques on c.pedidoId equals d.tbItensPedido.pedidoId into itensEstoque
                           join e in data.tbNotaFiscals on c.pedidoId equals e.idPedido into notas
                           where c.statusDoPedido == 11
                           && itensEstoque.Count(x => x.cancelado == false && x.enviado == false && x.idCentroDistribuicao == 4) > 0 && c.envioLiberadoCd4 == true
                           orderby (c.entregaEnviarPor == "JadlogExpressa" ? 0 : c.prioridadeEnvio == null ? 9999 : c.prioridadeEnvio),
                                    (c.prazoMaximoPostagemAtualizado != null ?
                                    c.prazoMaximoPostagemAtualizado :
                                    c.prazoFinalPedido == null ? DateTime.Now : c.prazoFinalPedido),
                           c.dataHoraDoPedido
                           select new
                           {
                               c.pedidoId,
                               c.endCidade,
                               c.endEstado,
                               c.endCep,
                               c.prazoFinalPedido,
                               c.prazoMaximoPostagemAtualizado,
                               c.dataInicioSeparacao,
                               c.dataFimSeparacao,
                               c.prioridadeEnvio,
                               c.formaDeEnvio,
                               itens = c.itensPendentesEnvioCd4,
                               separado = c.separadoCd4,
                               fornecedores = string.Join(",", (from g in data.tbItemPedidoEstoques where g.tbItensPedido.pedidoId == c.pedidoId && (g.cancelado == false && g.enviado == false && g.reservado == true && g.idCentroDistribuicao == 4) select g.tbProduto.tbProdutoFornecedor.fornecedorNome).Distinct().ToArray())
                           }).ToList();
            grd.DataSource = pedidos;
        }
        else if (rdbCd5.Checked)
        {
            var prioridadeEnvio = DateTime.Now.AddDays(2);
            var prioridadeEnvioGrande = DateTime.Now.AddDays(50);
            var pedidos = (from c in data.tbPedidos
                           join d in data.tbItemPedidoEstoques on c.pedidoId equals d.tbItensPedido.pedidoId into itensEstoque
                           join e in data.tbNotaFiscals on c.pedidoId equals e.idPedido into notas
                           where c.statusDoPedido == 11 &&
                           c.envioLiberadoCd5 == true && c.itensPendentesEnvioCd5 > 0 && c.itensPendentesEnvioCd4 == 0
                           orderby (c.entregaEnviarPor == "JadlogExpressa" ? 0 : c.prioridadeEnvio == null ? 9999 : c.prioridadeEnvio),
                                (c.prazoMaximoPostagemAtualizado != null ?
                                c.prazoMaximoPostagemAtualizado :
                                c.prazoFinalPedido == null ? DateTime.Now : c.prazoFinalPedido),
                                c.dataHoraDoPedido
                           select new
                           {
                               c.pedidoId,
                               c.endCidade,
                               c.endEstado,
                               c.endCep,
                               c.prazoFinalPedido,
                               c.prazoMaximoPostagemAtualizado,
                               c.dataInicioSeparacao,
                               c.dataFimSeparacao,
                               c.prioridadeEnvio,
                               c.formaDeEnvio,
                               itens = c.itensPendentesEnvioCd5,
                               separado = c.separadoCd5,
                               fornecedores = string.Join(",", (from g in data.tbItemPedidoEstoques where g.tbItensPedido.pedidoId == c.pedidoId && (g.cancelado == false && g.enviado == false && g.reservado == true && g.idCentroDistribuicao == 5) select g.tbProduto.tbProdutoFornecedor.fornecedorNome).Distinct().ToArray())
                           }).ToList();
            grd.DataSource = pedidos;
        }
        else if (rdbCd4e5.Checked)
        {
            var prioridadeEnvio = DateTime.Now.AddDays(2);
            var prioridadeEnvioGrande = DateTime.Now.AddDays(50);
            var pedidos = (from c in data.tbPedidos
                           join d in data.tbItemPedidoEstoques on c.pedidoId equals d.tbItensPedido.pedidoId into itensEstoque
                           join e in data.tbNotaFiscals on c.pedidoId equals e.idPedido into notas
                           where c.statusDoPedido == 11 &&
                           c.itensPendentesEnvioCd5 > 0 && c.itensPendentesEnvioCd4 > 0 && c.envioLiberadoCd4 == true
                           orderby (c.entregaEnviarPor == "JadlogExpressa" ? 0 : c.prioridadeEnvio == null ? 9999 : c.prioridadeEnvio),
                           c.prazoMaximoPostagemAtualizado > prioridadeEnvio ? prioridadeEnvioGrande : c.prazoMaximoPostagemAtualizado,
                           c.dataHoraDoPedido
                           select new
                           {
                               c.pedidoId,
                               c.endCidade,
                               c.endEstado,
                               c.endCep,
                               c.prazoFinalPedido,
                               c.prazoMaximoPostagemAtualizado,
                               c.dataInicioSeparacao,
                               c.dataFimSeparacao,
                               c.prioridadeEnvio,
                               c.formaDeEnvio,
                               itens = c.itensPendentesEnvioCd5,
                               separado = (c.separadoCd5 ?? false) && (c.separadoCd4 ?? false),
                               fornecedores = string.Join(",", (from g in data.tbItemPedidoEstoques where g.tbItensPedido.pedidoId == c.pedidoId && (g.cancelado == false && g.enviado == false && g.reservado == true && (g.idCentroDistribuicao == 5 | g.idCentroDistribuicao == 4)) select g.tbProduto.tbProdutoFornecedor.fornecedorNome).Distinct().ToArray())
                           }).ToList();
            grd.DataSource = pedidos;
        }
        if ((!Page.IsPostBack && !Page.IsCallback) | rebind) grd.DataBind();
    }

    protected void gridClientes_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType != GridViewRowType.Data)
        {
            return;
        }
        bool completo = true;
        int pedidoId = 0;
        int.TryParse(grd.GetRowValues(e.VisibleIndex, new string[] { "pedidoId" }).ToString(), out pedidoId);
        TextBox txtPrioridade = (TextBox)grd.FindRowCellTemplateControl(e.VisibleIndex, grd.Columns["prioridade"] as GridViewDataColumn, "txtPrioridade");
        //DropDownList ddlEnviarPor = (DropDownList)grd.FindRowCellTemplateControl(e.VisibleIndex, grd.Columns["enviarPor"] as GridViewDataColumn, "ddlEnviarPor");
        txtPrioridade.Attributes.Add("onkeypress", "return soNumero(event);");

        var prioridadeEnvio = grd.GetRowValues(e.VisibleIndex, new string[] { "prioridadeEnvio" });

        if (prioridadeEnvio != null)
        {
            txtPrioridade.Text = prioridadeEnvio.ToString();
        }

        var formaDeEnvio = grd.GetRowValues(e.VisibleIndex, new string[] { "formaDeEnvio" });
        if (formaDeEnvio != null)
        {
            try
            {
                if (formaDeEnvio.ToString() == "jadlog") formaDeEnvio = "2";
                if (formaDeEnvio.ToString() == "tnt") formaDeEnvio = "1";

                //ddlEnviarPor.SelectedValue = formaDeEnvio.ToString();
            }
            catch(Exception ex)
            {

            }
        }

        var prazoFinal = grd.GetRowValues(e.VisibleIndex, new string[] { "prazoFinalPedido" });
        var prazoMaximoPostagemAtualizado = grd.GetRowValues(e.VisibleIndex, new string[] { "prazoMaximoPostagemAtualizado" });
        var dataAtraso = Convert.ToDateTime(DateTime.Now.ToShortDateString()).AddDays(1);
        if (prazoMaximoPostagemAtualizado != null)
        {
            DateTime dataLimite = DateTime.Now;
            DateTime.TryParse(prazoMaximoPostagemAtualizado.ToString(), out dataLimite);
            if (dataLimite.Date < dataAtraso)
            {
                int i = 0;
                while ((i < e.Row.Cells.Count))
                {
                    e.Row.Cells[i].Style.Add("background", "#FFC6C7");
                    i++;
                }
            }
        }
        else if (prazoFinal != null)
        {
            DateTime dataLimite = DateTime.Now;
            DateTime.TryParse(prazoFinal.ToString(), out dataLimite);
            if (dataLimite.Date < dataAtraso.Date)
            {
                int i = 0;
                while ((i < e.Row.Cells.Count))
                {
                    e.Row.Cells[i].Style.Add("background", "#FFC6C7");
                    i++;
                }
            }
        }

    }

    protected void btnGravar_OnClick(object sender, ImageClickEventArgs e)
    {

    }

    public void interacaoInclui(string interacao, string alteracaoDeEstatus, int pedidoId)
    {
        HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
        usuarioLogadoId = Request.Cookies["usuarioLogadoId"];
        if (usuarioLogadoId != null)
        {
            rnInteracoes.interacaoInclui(pedidoId, interacao, rnUsuarios.usuarioSeleciona_PorUsuarioId(int.Parse(usuarioLogadoId.Value.ToString())).Tables[0].Rows[0]["usuarioNome"].ToString(), alteracaoDeEstatus);
        }
    }

    protected void btnGravarPrioridade_OnClick(object sender, EventArgs e)
    {
        int inicio = 0;
        int fim = 0;

        if (grd.PageIndex == 0)
        {
            inicio = 0;
            fim = inicio + grd.GetCurrentPageRowValues("pedidoId").Count;
        }
        else
        {
            inicio += inicio + grd.SettingsPager.PageSize * grd.PageIndex;
            fim += inicio + grd.GetCurrentPageRowValues("pedidoId").Count;
        }

        for (int i = inicio; i < fim; i++)
        {
            var pedidoDc = new dbCommerceDataContext();
            TextBox txtPrioridade = (TextBox)grd.FindRowCellTemplateControl(i, grd.Columns["prioridade"] as GridViewDataColumn, "txtPrioridade");
            //DropDownList ddlEnviarPor = (DropDownList)grd.FindRowCellTemplateControl(i, grd.Columns["enviarPor"] as GridViewDataColumn, "ddlEnviarPor");
            int pedidoId = (int)grd.GetRowValues(i, new string[] { "pedidoId" });
            var pedido = (from c in pedidoDc.tbPedidos where c.pedidoId == pedidoId select c).First();

            if (!string.IsNullOrEmpty(txtPrioridade.Text))
            {
                if (pedido.prioridadeEnvio != Convert.ToInt32(txtPrioridade.Text))
                {
                    interacaoInclui("Pedido priorizado para " + Convert.ToInt32(txtPrioridade.Text), "False", pedido.pedidoId);
                }

                pedido.prioridadeEnvio = Convert.ToInt32(txtPrioridade.Text);
            }
            else
            {
                pedido.prioridadeEnvio = null;
            }
            //if (!string.IsNullOrEmpty(ddlEnviarPor.SelectedValue))
            //{
            //    if (pedido.formaDeEnvio != ddlEnviarPor.SelectedValue)
            //    {
            //        interacaoInclui("Forma de envio alterada para " + ddlEnviarPor.SelectedValue, "False", pedido.pedidoId);
            //    }
            //    pedido.formaDeEnvio = ddlEnviarPor.SelectedValue;
            //}
            //else
            //{
            //    pedido.formaDeEnvio = null;
            //}
            pedidoDc.SubmitChanges();
        }
        fillGrid(true);
        Response.Write("<script>alert('Prioridade gravada com sucesso.');</script>");
    }

    protected void grd_OnCustomUnboundColumnData(object sender, ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "pedidoIdCliente")
        {
            int pedidoId = Convert.ToInt32(e.GetListSourceFieldValue("pedidoId"));
            e.Value = rnFuncoes.retornaIdCliente(pedidoId);
        }
        /*if (e.Column.FieldName == "melhorFormaEntrega")
        {
            int pedidoId = Convert.ToInt32(e.GetListSourceFieldValue("pedidoId"));
            e.Value = determinaEntrega(pedidoId);
        }
        if (e.Column.FieldName == "dataLimitePostagem")
        {
            int pedidoId = Convert.ToInt32(e.GetListSourceFieldValue("pedidoId"));
            var data = new dbCommerceDataContext();
            DateTime dataRetorno = DateTime.Now;

            var dataLimite =
                (from c in data.tbPedidoFornecedorItems
                    where c.idPedido == pedidoId
                    orderby c.tbPedidoFornecedor.dataLimite descending
                    select c).FirstOrDefault();
            if (dataLimite != null)
            {
                dataRetorno = Convert.ToDateTime(dataLimite.tbPedidoFornecedor.dataLimite);
            }

            var itensPedidoDc = new dbCommerceDataContext();
            var itens = (from c in itensPedidoDc.tbItensPedidos
                         where c.pedidoId == pedidoId && c.prazoDeFabricacao != null
                         orderby c.prazoDeFabricacao descending
                         select c).FirstOrDefault();
            if (itens != null)
            {
                if (itens.prazoDeFabricacao > dataRetorno)
                {
                    dataRetorno = Convert.ToDateTime(itens.prazoDeFabricacao);
                }
            }

            var limitePedido = (from c in data.tbPedidos where c.pedidoId == pedidoId select c).First();
            if (limitePedido.prazoFinalPedido != null)
            {
                dataRetorno = Convert.ToDateTime(limitePedido.prazoFinalPedido);
            }

            e.Value = dataRetorno; 
        }*/
    }

    private string determinaEntrega(int idPedido)
    {
        var pedidoDc = new dbCommerceDataContext();
        var pedido = (from c in pedidoDc.tbPedidos where c.pedidoId == idPedido select c).First();
        string tipo = "";
        if (pedido.tipoDeEntregaId == 5)
        {
            return "Sedex";
        }
        if (!string.IsNullOrEmpty(pedido.formaDeEnvio))
        {
            return pedido.formaDeEnvio;
        }

        var faixasDc = new dbCommerceDataContext();
        long cep = 0;
        Int64.TryParse(pedido.endCep.Replace("-", ""), out cep);
        var faixas = faixasDc.admin_TipoDeEntregaPorFaixaDeCep(cep).Where(x => x.tipodeEntregaId == 9).FirstOrDefault();
        if (faixas != null)
        {
            if (faixas.interiorizacao == true)
            {
                var itensPedidoDc = new dbCommerceDataContext();
                List<int> produtos = new List<int>();
                produtos.AddRange((from c in itensPedidoDc.tbItensPedidos where c.pedidoId == pedido.pedidoId select c.pedidoId).ToList());
                var produtosDc = new dbCommerceDataContext();
                var produtosCubagem =
                    (from c in produtosDc.tbProdutos
                     where produtos.Contains(c.produtoId)
                     where c.cubagem == "True"
                     select c).Any();
                if (produtosCubagem) return "Jadlog";
                return "PAC";
            }
            else
            {
                return "Jadlog";
            }
        }
        else
        {
            return "PAC";
        }
        return tipo;
    }

    /// <summary>
    /// Prioriza os pedidos completos móveis da Carolina em 888888 e os da Planet em 777777
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnPriorizarMoveis_OnClick(object sender, EventArgs e)
    {
        try
        {
            int totalPriorizacoes = 0;
            using (var data = new dbCommerceDataContext())
            {
                List<int> pedidosCompletos = new List<int>();

                var pedidosCompletosLista = (from c in data.tbPedidos
                                             where c.statusDoPedido == 11 && c.envioLiberado
                                             select new { c.pedidoId });

                foreach (var pedido in pedidosCompletosLista)
                {
                    pedidosCompletos.Add(pedido.pedidoId);
                }

                var pedidosCarolina = (from ip in data.tbItensPedidos
                                       join ipe in data.tbItemPedidoEstoques on ip.itemPedidoId equals ipe.itemPedidoId
                                       join p in data.tbProdutos on ipe.produtoId equals p.produtoId
                                       where p.produtoFornecedor == 72 && pedidosCompletos.Contains(ip.pedidoId)
                                       select ip.tbPedido).Distinct().ToList();

                var pedidosPlanet = (from ip in data.tbItensPedidos
                                     join ipe in data.tbItemPedidoEstoques on ip.itemPedidoId equals ipe.itemPedidoId
                                     join p in data.tbProdutos on ipe.produtoId equals p.produtoId
                                     where p.produtoFornecedor == 112 && pedidosCompletos.Contains(ip.pedidoId)
                                     select ip.tbPedido).Distinct().ToList();

                foreach (var pedido in pedidosCarolina)
                {
                    pedido.prioridadeEnvio = 606060;//88888
                    data.SubmitChanges();
                }

                foreach (var pedido in pedidosPlanet)
                {
                    pedido.prioridadeEnvio = 505050;//77777
                    data.SubmitChanges();
                }

                totalPriorizacoes = pedidosCarolina.Count() + pedidosPlanet.Count();
            }
            fillGrid(true);
            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('" + totalPriorizacoes + " móveis priorizados com sucesso!');", true);
        }
        catch (Exception ex)
        {

            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('ERRO: " + ex.Message + "!');", true);
        }

    }

    /// <summary>
    /// prioriza pedidos completos de moveis com itens no CD2 em 1
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnPriorizarMoveisEm1_OnClick(object sender, EventArgs e)
    {
        try
        {
            using (var data = new dbCommerceDataContext())
            {
                var pedido = (from c in data.tbPedidos
                              join ip in data.tbItensPedidos on c.pedidoId equals ip.pedidoId
                              join ipe in data.tbItemPedidoEstoques on ip.itemPedidoId equals ipe.itemPedidoId
                              join p in data.tbProdutos on ipe.produtoId equals p.produtoId
                              where p.produtoFornecedor == 72 && ipe.idCentroDistribuicao == 2 && c.statusDoPedido == 11 && c.envioLiberado
                              select ipe).GroupBy(x => new { x.tbItensPedido.pedidoId, x.idCentroDistribuicao }).Select(g => new { g.Key.pedidoId, g.Key.idCentroDistribuicao }).Select(x => x.pedidoId).ToList();

                var pedidosPriorizar = (from c in data.tbPedidos where pedido.Contains(c.pedidoId) select c).ToList();

                int totalPriorizacoes = 0;

                foreach (var p in pedidosPriorizar)
                {
                    p.prioridadeEnvio = 1;
                    data.SubmitChanges();

                    totalPriorizacoes++;
                }

                ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('" + totalPriorizacoes + " móveis no CD2 priorizados com sucesso!');", true);
            }
        }
        catch (Exception ex)
        {

        }



    }

    protected void rdbCd1_CheckedChanged(object sender, EventArgs e)
    {
        fillGrid(true);
    }
    protected void rdbCd2_CheckedChanged(object sender, EventArgs e)
    {
        fillGrid(true);
    }
    protected void rdbCd3_CheckedChanged(object sender, EventArgs e)
    {
        fillGrid(true);
    }

    protected void rdbCd4_CheckedChanged(object sender, EventArgs e)
    {
        fillGrid(true);
    }
    protected void rdbCd5_CheckedChanged(object sender, EventArgs e)
    {
        fillGrid(true);
    }
}