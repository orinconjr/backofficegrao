﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="graficoEstoque.aspx.cs" Inherits="admin_graficoEstoque" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" Runat="Server">
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server"></telerik:RadScriptManager>
    <table cellpadding="0" cellspacing="0" style="width: 920px">
    <tr>
        <td class="tituloPaginas" valign="top">
            <asp:Label ID="lblAcao" runat="server">Relatório Estoque</asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
                <tr>
                    <td class="rotulos">
                        Data Inicial:<br />
                        <asp:TextBox runat="server" ID="txtDataInicial"></asp:TextBox>
                    </td>
                    <td class="rotulos">
                        Data Final:<br />
                        <asp:TextBox runat="server" ID="txtDataFinal"></asp:TextBox>
                    </td>
                    <td class="rotulos">
                        Estoque Minimo:<br />
                        <asp:TextBox runat="server" ID="txtEstoqueMinimo"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                       <asp:Button runat="server" ID="btnFiltrar" OnClick="btnFiltrar_OnClick" Text="Gerar Relatório"/>
                    </td>
                </tr>
                    <asp:ListView runat="server" ID="lstProdutosEstoque" OnItemDataBound="lstProdutosEstoque_OnItemDataBound">
                        <ItemTemplate>
                            <tr>
                                <td colspan="3" class="rotulos" style="padding-top: 50px; text-align: center;">
                                            <asp:HiddenField runat="server" ID="hdfProdutoId" Value='<%#Eval("idProduto") %>'/>
                                            <span style="font-size: 14px; font-weight: bold;"><asp:Literal runat="server" ID="litProdutoNome"></asp:Literal></span><br/>
                                            <asp:Literal runat="server" ID="litEstoque" Visible="False"></asp:Literal><br/>
                                              <telerik:RadHtmlChart ID="grafico" runat="server">
                                        <ChartTitle>
                                        <Appearance Visible="False">
                                        </Appearance>
                                        </ChartTitle>
                                        <Legend>
                                            <Appearance Visible="True">
                                            </Appearance>
                                        </Legend>
                                        <PlotArea>
                                            <XAxis>
                                            </XAxis>
                                            <YAxis>
                                            </YAxis>
                                            <Series>
                                                <telerik:LineSeries DataFieldY="compras" Name="Compras">
                                                </telerik:LineSeries>
                                                <telerik:LineSeries DataFieldY="pedidos" Name="Pedidos">
                                                </telerik:LineSeries>
                                                <telerik:LineSeries DataFieldY="entregas" Name="Entregas">
                                                </telerik:LineSeries>
                                                <telerik:LineSeries DataFieldY="reservas" Name="Reservas">
                                                </telerik:LineSeries>
                                                <telerik:LineSeries DataFieldY="cancelamentos" Name="Cancelamentos">
                                                </telerik:LineSeries>
                                                <telerik:LineSeries DataFieldY="estoqueLivre" Name="Estoque">
                                                </telerik:LineSeries>
                                            </Series>
                                        </PlotArea>
                                        <Navigator>
                                            <XAxis>
                                            </XAxis>
                                        </Navigator>
                                    </telerik:RadHtmlChart>
                                </td>
                            </tr>
                    </ItemTemplate>
                </asp:ListView>
            </table>
        </td>
    </tr>
</table>
</asp:Content>