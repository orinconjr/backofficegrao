﻿<%@ Page Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="comentarios.aspx.cs" Inherits="admin_comentarios" Theme="Glass" MaintainScrollPositionOnPostback=true %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dxwgv" %>
<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" Runat="Server">
    <table cellpadding="0" cellspacing="0" style="width: 920px">
    <tr>
        <td class="tituloPaginas" valign="top">
            <asp:Label ID="lblAcao" runat="server">Comentários</asp:Label>
            </td>
    </tr>
    <tr>
        <td>
            
    <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
        <tr>
            <td align="right">
                <table cellpadding="0" cellspacing="0" style="width: 834px">
                    <tr>
                        <td align="right">
                            &nbsp;</td>
                        <td height="38" 
                            style="background-image: url('images/bkExportar.jpg'); padding-bottom: 1px;" 
                            valign="bottom" width="231">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="width: 97px">
                                        &nbsp;</td>
                                    <td style="width: 33px">
                                        <asp:ImageButton ID="btPdf" runat="server" ImageUrl="~/admin/images/btPdf.jpg" 
                                            style="margin-left: 5px" />
                                    </td>
                                    <td style="width: 31px">
                                        <asp:ImageButton ID="btXsl" runat="server" ImageUrl="~/admin/images/btXls.jpg" 
                                            style="margin-left: 5px" />
                                    </td>
                                    <td style="width: 36px">
                                        <asp:ImageButton ID="btRtf" runat="server" ImageUrl="~/admin/images/btRtf.jpg" 
                                            style="margin-left: 5px" />
                                    </td>
                                    <td valign="bottom">
                                        <asp:ImageButton ID="btCsv" runat="server" ImageUrl="~/admin/images/btCsv.jpg" 
                                            style="margin-left: 5px" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                <dxwgv:aspxgridview ID="grd" runat="server" AutoGenerateColumns="False" 
                    DataSourceID="sqlComentarios" KeyFieldName="comentarioId" Width="834px" 
                    Cursor="auto">
                    <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" 
                        autoexpandallgroups="True" />
                    <SettingsText ConfirmDelete="Tem certeza que deseja excluir?" 
                        emptydatarow="Nenhum registro encontrado." 
                        grouppanel="Arraste uma coluna aqui para agrupar. " />
                    <SettingsPager Position="TopAndBottom" PageSize="50" 
                        ShowDisabledButtons="False" AlwaysShowPager="True">
                        <Summary AllPagesText="Páginas: {0} - {1} ({2} items)" 
                            Text="Página {0} de {1} ({2} registros encontrados)" />
                    </SettingsPager>
                    <settings showfilterrow="True" ShowGroupButtons="False" showgrouppanel="True" />
                    <SettingsEditing EditFormColumnCount="4" 
                        PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1" 
                        PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter" 
                        PopupEditFormWidth="700px" Mode="Inline" />
                    <Columns>
                        <dxwgv:GridViewDataComboBoxColumn Caption="produto" FieldName="produtoId" 
                            VisibleIndex="0" Width="100px">
                            <PropertiesComboBox DataSourceID="sqlProdutos" TextField="produtoNome" 
                                ValueField="produtoId" ValueType="System.String">
                            </PropertiesComboBox>
                            <EditFormSettings Visible="True" />
                        </dxwgv:GridViewDataComboBoxColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Nome" FieldName="comentaristaNome" 
                            VisibleIndex="1" Width="80px">
                            <PropertiesTextEdit>
                                <ValidationSettings ErrorText="" SetFocusOnError="True">
                                    <RequiredField ErrorText="Preencha o campo nome." IsRequired="True" />
                                </ValidationSettings>
                            </PropertiesTextEdit>
                            <Settings AutoFilterCondition="Contains" />
                            <editformsettings captionlocation="Top" columnspan="2" visible="True" 
                                VisibleIndex="1" />
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Email" FieldName="comentaristaEmail" 
                            VisibleIndex="2" Width="80px">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Cidade" FieldName="comentaristaCidade" 
                            VisibleIndex="3" Width="60px">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataMemoColumn Caption="Comentário" FieldName="comentario" 
                            VisibleIndex="4" Width="250px">
                        </dxwgv:GridViewDataMemoColumn>
                        <dxwgv:GridViewDataComboBoxColumn Caption="Publicado" 
                            FieldName="publicarComentario" VisibleIndex="5">
                            <PropertiesComboBox ValueType="System.String">
                                <Items>
                                    <dxe:ListEditItem Text="Sim" Value="True" />
                                    <dxe:ListEditItem Text="Não" Value="False" />
                                </Items>
                            </PropertiesComboBox>
                        </dxwgv:GridViewDataComboBoxColumn>
                        <dxwgv:GridViewCommandColumn VisibleIndex="6" Width="27px" ButtonType="Image">
                            <editbutton Text="Editar" visible="True">
                                <Image Url="~/admin/images/btEditar.jpg" />
                            </editbutton>
                            <NewButton Text="Novo">
                                <Image Url="~/admin/images/btNovo.jpg" />
                            </NewButton>
                            <deletebutton visible="True" Text="Excluir">
                                <Image Url="~/admin/images/btExcluir.jpg" />
                            </deletebutton>
                            <CancelButton Text="Cancelar">
                                <Image Url="~/admin/images/btCancelarIcon.jpg" />
                            </CancelButton>
                            <UpdateButton>
                                <Image Url="~/admin/images/btSalvarIcon.jpg" />
                            </UpdateButton>
                            <ClearFilterButton Visible="True" text="Limpar filtro">
                                <Image Url="~/admin/images/btLimparFiltro.jpg" />
                            </ClearFilterButton>
                            <HeaderTemplate>
                                <img alt="" src="images/tarjaEditarDeletar.jpg" />
                            </HeaderTemplate>
                        </dxwgv:GridViewCommandColumn>
                    </Columns>
                    <StylesEditors>
                        <Label Font-Bold="True">
                        </Label>
                    </StylesEditors>
                </dxwgv:aspxgridview>
<%--                <dxwgv:aspxgridviewexporter ID="grdEx" runat="server" FileName="fornecedores" 
                    GridViewID="grd" Landscape="True" PreserveGroupRowStates="True">
                </dxwgv:aspxgridviewexporter>--%>
                <asp:LinqDataSource ID="sqlComentarios" runat="server" 
                    ContextTypeName="tbComentariosDataContext" EnableDelete="True" 
                    EnableUpdate="True" TableName="tbProdutoComentarios">
                </asp:LinqDataSource>
                <asp:LinqDataSource ID="sqlProdutos" runat="server" 
                    ContextTypeName="dbCommerceDataContext" TableName="tbProdutos">
                </asp:LinqDataSource>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
    </table>
        </td>
    </tr>
</table>
    </asp:Content>


