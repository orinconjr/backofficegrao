﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;

public partial class admin_relacionarProdutosComColecoes : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    //protected void btPdf_Click(object sender, ImageClickEventArgs e)
    //{
    //    this.grdEx.WritePdfToResponse();
    //}
    //protected void btXsl_Click(object sender, ImageClickEventArgs e)
    //{
    //    this.grdEx.WriteXlsToResponse();
    //}
    //protected void btRtf_Click(object sender, ImageClickEventArgs e)
    //{
    //    this.grdEx.WriteRtfToResponse();
    //}
    //protected void btCsv_Click(object sender, ImageClickEventArgs e)
    //{
    //    this.grdEx.WriteCsvToResponse();
    //}


    protected void grd1_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        int inicio = 0;
        int fim = 0;

        if (grd1.PageIndex == 0)
        {
            inicio = 0;
            fim = inicio + grd1.GetCurrentPageRowValues("produtoId").Count;
        }
        else
        {
            inicio += inicio + grd1.SettingsPager.PageSize * grd1.PageIndex;
            fim += inicio + grd1.GetCurrentPageRowValues("produtoId").Count;
        }

        for (int i = inicio; i < fim; i++)
        {

            TextBox txtProdutoId = (TextBox) grd1.FindRowCellTemplateControl(i, grd1.Columns["produtoId"] as GridViewDataColumn, "txtProdutoId");
            TextBox txtProdutoPaiId = (TextBox) grd1.FindRowCellTemplateControl(i, grd1.Columns["produtoId"] as GridViewDataColumn, "txtProdutoPaiId");
            CheckBox ckbSelecionar = (CheckBox)grd1.FindRowCellTemplateControl(i, grd1.Columns["selecionar"] as GridViewDataColumn, "ckbSelecionar");
            int produtoId = Convert.ToInt32(txtProdutoId.Text);

            var produtoDc = new dbCommerceDataContext();
            var colecoesAtuais = (from c in produtoDc.tbJuncaoProdutoColecaos where c.produtoId == produtoId select c);
            if (ckbSelecionar.Checked)
            {
                foreach (ListItem colecaoItem in chkColecoes.Items)
                {
                    int colecaoId = Convert.ToInt32(colecaoItem.Value);
                    var possuiAtualmente = colecoesAtuais.Any(x => x.colecaoId == colecaoId);
                    if (colecaoItem.Selected && possuiAtualmente == false)
                    {
                        var colecaoAdicionar = new tbJuncaoProdutoColecao();
                        colecaoAdicionar.produtoId = produtoId;
                        colecaoAdicionar.colecaoId = colecaoId;
                        colecaoAdicionar.dataDaCriacao = DateTime.Now;
                        produtoDc.tbJuncaoProdutoColecaos.InsertOnSubmit(colecaoAdicionar);
                        produtoDc.SubmitChanges();
                    }
                }
            }
           
        }
        grd1.DataBind();
    }



    protected void grd1_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {

    }

    protected void lstFiltrosAtuais_PreRender(object sender, EventArgs e)
    {

    }

    protected void grd1_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType != GridViewRowType.Data) return;
        {
            var gridView = sender as ASPxGridView;
            var id = gridView.GetMasterRowKeyValue();
            HiddenField hiddenProdutoId = (HiddenField)grd1.FindRowCellTemplateControl(e.VisibleIndex, (GridViewDataColumn)grd1.Columns["filtros"], "hiddenProdutoId");
            ListView lstProdutos = (ListView)grd1.FindRowCellTemplateControl(e.VisibleIndex, (GridViewDataColumn)grd1.Columns["filtros"], "lstProdutos");
            int produtoId = Convert.ToInt32(hiddenProdutoId.Value);
            var produtosDc = new dbCommerceDataContext();
            var colecoes = (from c in produtosDc.tbJuncaoProdutoColecaos
                              where c.produtoId == produtoId 
                              select new { c.produtoId, c.colecaoId, c.tbColecao.colecaoNome });
            lstProdutos.DataSource = colecoes;
            lstProdutos.DataBind();
        }
    }

    protected void btnExcluir_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(hiddenProdutoId.Value) && !string.IsNullOrEmpty(hiddenColecaoId.Value))
        {
            int colecaoId = Convert.ToInt32(hiddenColecaoId.Value);
            int produtoId = Convert.ToInt32(hiddenProdutoId.Value);
            var produtosDc = new dbCommerceDataContext();
            var colecao = (from c in produtosDc.tbJuncaoProdutoColecaos where c.colecaoId == colecaoId && c.produtoId == produtoId select c).FirstOrDefault();
            if (colecao != null)
            {
                produtosDc.tbJuncaoProdutoColecaos.DeleteOnSubmit(colecao);
                produtosDc.SubmitChanges();
            }
            grd1.DataBind();
        }

    }
}
