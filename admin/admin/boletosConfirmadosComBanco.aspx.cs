﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BoletoNet;

public partial class admin_boletosConfirmadosComBanco : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnSalvar_Click(object sender, ImageClickEventArgs e)
    {
        lblarquivolido.Text = " -  "+fluArquivo.PostedFile.FileName;
         var status = new List<PedidoStatus>();
        var boletoRetorno = new BoletoNet.ArquivoRetornoCNAB240();
        boletoRetorno.LerArquivoRetorno(new Banco(104), fluArquivo.PostedFile.InputStream);
        string erro = "";
        foreach (var linhaRetorno in boletoRetorno.ListaDetalhes)
        {
            decimal valorPago = linhaRetorno.SegmentoU.ValorPagoPeloSacado;
            string numeroCobranca =
                (String.Format("{0:00000000}",
                    Convert.ToInt32(linhaRetorno.SegmentoT.NossoNumero.ToString().Substring(2, 15))));

////            select tpe.statusDoPedido from tbPedidoPagamento tpepa
////join tbPedidos tpe on tpepa.pedidoId = tpe.pedidoId
////where tpepa.numeroCobranca = '00282359';
            using (var pedidosDc2 = new dbCommerceDataContext())
            {
                var pedido = (from pp in pedidosDc2.tbPedidoPagamentos
                    join p in pedidosDc2.tbPedidos on pp.pedidoId equals p.pedidoId
                    join ps in pedidosDc2.tbPedidoSituacaos on p.statusDoPedido equals ps.situacaoId
                    where pp.numeroCobranca == numeroCobranca
                    select new
                    {
                       statusDoPedido = p.statusDoPedido,
                       pedidoId = p.pedidoId,
                       status = ps.situacao,
                       valorCobrado = p.valorCobrado,
                       valorCobradoBoleto = pp.valor

                    }).FirstOrDefault();

                PedidoStatus pedidoStatus = new PedidoStatus();
                try
                {
                    pedidoStatus.pedidoId = pedido.pedidoId;
                    pedidoStatus.valorPago = valorPago;
                    pedidoStatus.statusPedido = pedido.statusDoPedido;
                    pedidoStatus.status = pedido.status;
                    pedidoStatus.valorCobrado = Math.Round(Convert.ToDecimal(pedido.valorCobradoBoleto), 2);
                    //  pedidoStatus.valorCobrado = Math.Round(Convert.ToDecimal(pedidoStatus.valorCobrado),2);
                    status.Add(pedidoStatus);
                }
                catch (Exception ex)
                {
                    erro += "Nosso Número com erro :" + linhaRetorno.SegmentoT.NossoNumero + "<br/>";
                  
                }
               
              
            }

           

            //var pedidosDc = new dbCommerceDataContext();
            //var pedidoPagamento =
            //    (from c in pedidosDc.tbPedidoPagamentos where c.numeroCobranca == numeroCobranca select c)
            //        .FirstOrDefault();

        }
        literro.Text = erro;
        Repeater1.DataSource = status.OrderBy(x => x.statusPedido).ThenBy(x => x.pedidoId);
       // Repeater1.DataSource = status;
        Repeater1.DataBind();

    } 
    private class PedidoStatus
    {
        public int pedidoId { get; set; }
        public string status { get; set; }
        public decimal valorPago { get; set; }
        public decimal? valorCobrado { get; set; }
        public decimal? valorCobradoBoleto { get; set; }
        public int statusPedido { get; set; }
    }
}