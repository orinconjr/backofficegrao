﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_gerarsimulacoestransportadoras : System.Web.UI.Page
{
    private static double progress;
    private static string servico;

    protected void Page_Load(object sender, EventArgs e)
    {
       
    }
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.ASPxCallback.CallbackEventArgs e)
    {
        enviarProdutos();
    }

    protected void ASPxCallback2_Callback(object source, DevExpress.Web.ASPxCallback.CallbackEventArgs e)
    {
        e.Result = progress.ToString("0.0000").Replace(",", ".");
    }


    private void enviarProdutos()
    {
        var dataInicio = Convert.ToDateTime("01/01/2018");

        var dataFim = DateTime.Now.AddDays(-2);
        var data = new dbCommerceDataContext();
        var envios = (from c in data.tbPedidoEnvios where c.dataFimEmbalagem > dataInicio && c.dataFimEmbalagem < dataFim && c.formaDeEnvio != "" select new { c.idPedidoEnvio });
        int total = envios.Count();
        int atual = 0;
        var tabelas = (from c in data.tbTipoDeEntregas where c.habilitarSimulacao == true select c).ToList();
        foreach(var envio in envios)
        {
            atual++;
            progress = Convert.ToDouble((Convert.ToDecimal(atual) * 100) / total);
            rnEntrega.AtualizaEntregasPedido(envio.idPedidoEnvio);
        }

    }

    protected void ASPxButton2_OnClick(object sender, EventArgs e)
    {
        enviarProdutos();
    }
}