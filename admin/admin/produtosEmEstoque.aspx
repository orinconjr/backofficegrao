﻿<%@ Page Title="" Theme="Glass" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="produtosEmEstoque.aspx.cs" Inherits="admin_produtosEmEstoque" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxTimer" TagPrefix="dxt" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" Runat="Server">
    <table cellpadding="0" cellspacing="0" style="width: 920px">
    <tr>
        <td class="tituloPaginas" valign="top">
            <asp:Label ID="lblAcao" runat="server">Produtos em Estoque</asp:Label>
        </td>
    </tr>
        
    <tr>
        <td>
            
    <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
        <tr>
            <td>
                &nbsp;</td>
        </tr>

        <tr>
            <td>
                <dxwgv:ASPxGridView DataSourceID="sql" ID="grd" runat="server" AutoGenerateColumns="False" Width="834px" Cursor="auto" KeyFieldName="produtoId">
                    <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                    <Styles>
                        <Footer Font-Bold="True">
                        </Footer>
                    </Styles>
                    <SettingsText ConfirmDelete="Tem certeza que deseja excluir?" emptydatarow="Nenhum registro encontrado." />
                    <SettingsPager Position="TopAndBottom" PageSize="50" ShowDisabledButtons="False" AlwaysShowPager="True">
                        <Summary AllPagesText="Páginas: {0} - {1} ({2} items)" Text="Página {0} de {1} ({2} registros encontrados)" />
                    </SettingsPager>
                    <settings showfilterrow="True" ShowGroupButtons="False" ShowHeaderFilterButton="True" ShowFooter="True" />
                    <SettingsEditing EditFormColumnCount="4" PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1" PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter" PopupEditFormWidth="700px" />
                    <Columns>
                        <dxwgv:GridViewDataTextColumn Caption="ID" FieldName="produtoId" VisibleIndex="0" Width="50">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Pedido" FieldName="pedidoId" VisibleIndex="0">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Produto" FieldName="produtoNome" VisibleIndex="0">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="ID da Empresa" FieldName="produtoIdDaEmpresa" VisibleIndex="0">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Complemento ID" FieldName="complementoIdDaEmpresa" VisibleIndex="0">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Fornecedor" FieldName="fornecedorNome" VisibleIndex="0">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Quantidade" FieldName="quantidade" VisibleIndex="0" Width="30px">
                        </dxwgv:GridViewDataTextColumn>
                    </Columns>
                    <StylesEditors>
                        <Label Font-Bold="True">
                        </Label>
                    </StylesEditors>
                </dxwgv:ASPxGridView>
                <asp:SqlDataSource ID="sql" runat="server" ConnectionString="<%$ ConnectionStrings:connectionString %>"
                    SelectCommand="select * from (select
                tbProdutos.produtoId,
                tbPedidos.pedidoId, tbProdutos.produtoNome, tbProdutos.produtoIdDaEmpresa, tbProdutos.complementoIdDaEmpresa, tbProdutoFornecedor.fornecedorNome, tbItensPedido.itemQuantidade as quantidade
                from tbPedidos
                join tbItensPedido on tbPedidos.pedidoId = tbItensPedido.pedidoId
                join tbProdutos on tbItensPedido.produtoId = tbProdutos.produtoId
                join tbProdutoFornecedor on tbProdutoFornecedor.fornecedorId = tbProdutos.produtoFornecedor
                where tbItensPedido.entreguePeloFornecedor = 1 and tbPedidos.statusDoPedido &lt;&gt; 5 and tbPedidos.statusDoPedido &lt;&gt; 6 and tbPedidos.statusDoPedido &lt;&gt; 8 and tbPedidos.statusDoPedido &lt;&gt; 10
                UNION
                select
                tbProdutos.produtoId,
                tbPedidos.pedidoId, tbProdutos.produtoNome, tbProdutos.produtoIdDaEmpresa, tbProdutos.complementoIdDaEmpresa, tbProdutoFornecedor.fornecedorNome, tbPedidoFornecedorItem.quantidade
                from tbPedidos
                join tbPedidoFornecedorItem on tbPedidos.pedidoId = tbPedidoFornecedorItem.idPedido
                join tbProdutos on tbPedidoFornecedorItem.idProduto = tbProdutos.produtoId
                join tbProdutoFornecedor on tbProdutoFornecedor.fornecedorId = tbProdutos.produtoFornecedor
                where tbPedidoFornecedorItem.entregue = 1 and tbPedidos.statusDoPedido &lt;&gt; 5 and tbPedidos.statusDoPedido &lt;&gt; 6 and tbPedidos.statusDoPedido &lt;&gt; 8 and tbPedidos.statusDoPedido &lt;&gt; 10) produtos order by produtoNome"></asp:SqlDataSource>
            </td>
        </tr>
    </table>
        </td>
    </tr>
</table>
</asp:Content>