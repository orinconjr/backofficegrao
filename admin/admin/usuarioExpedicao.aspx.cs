﻿using DevExpress.Web.ASPxHiddenField;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_usuarioExpedicao : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            CarregarComboTipoPermissao();
        }
    }

    private void CarregarComboTipoPermissao()
    {
        var data = new dbCommerceDataContext();

        var permissoes = data.tbExpedicaoTipoPermissaos.Where(x => x.ativo == true).OrderBy(x => x.nomeExpedicaoTipoPermissao).ToList();
        cboTipoPermissao.DataSource = permissoes;
        cboTipoPermissao.TextField = "nomeExpedicaoTipoPermissao";
        cboTipoPermissao.ValueField = "idExpedicaoTipoPermissao";
        cboTipoPermissao.DataBind();

        cboTipoPermissao.Items.Insert(0, new DevExpress.Web.ASPxEditors.ListEditItem() { Index = 0, Text = "Selecione", Value = "" });
        cboTipoPermissao.SelectedIndex = 0;
    }

    protected void sqlUsuariosExpedicao_OnUpdating(object sender, SqlDataSourceCommandEventArgs e)
    {

        string senhaAtual = e.Command.Parameters["@senha"].Value.ToString();
        string usuarioExpedicaoId = e.Command.Parameters["@idUsuarioExpedicao"].Value.ToString();

        sqlValidarSenha.SelectCommand = "Select idUsuarioExpedicao From tbUsuarioExpedicao Where (senha <> 'senhadesativada') AND (senha = '" + senhaAtual + "' AND idUsuarioExpedicao <> '" + usuarioExpedicaoId + "')";

        DataView validar = (DataView)sqlValidarSenha.Select(new DataSourceSelectArguments());

        if (validar != null && validar.Table.Rows.Count > 0)
        {
            e.Cancel = true;
            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('Senha informada não pode ser utilizada!');", true);
        }

    }

    private void BindDataList(int usuario)
    {
        var data = new dbCommerceDataContext();

        var permissoes = (from c in data.tbUsuarioExpedicaoPermissaos
                          join d in data.tbExpedicaoTipoPermissaos on c.idExpedicaoTipoPermissao equals d.idExpedicaoTipoPermissao
                          where c.idUsuarioExpedicao == usuario
                          select new
                          {
                              c.idUsuarioExpedicaoPermissao,
                              c.idUsuarioExpedicao,
                              c.idExpedicaoTipoPermissao,
                              d.nomeExpedicaoTipoPermissao,
                              c.Id_Relacionado,
                              c.filtro
                          }).ToList();

        dtlPermissoes.DataSource = permissoes;
        dtlPermissoes.DataBind();

        if (dtlPermissoes.Items.Count == 0)
        {
            lblMensagem.Text = "Não existem permissões associadas ao usuário";
            lblMensagem.Visible = true;
            dtlPermissoes.Visible = false;
        }

    }

    protected void popControleAcesso_WindowCallback(object source, DevExpress.Web.ASPxPopupControl.PopupWindowCallbackArgs e)
    {
        var usuarios = e.Parameter;
        tdTipoEntrega.Visible = false;
        tdIdRelacionado.Visible = false;
        tdFiltro.Visible = false;

        var data = new dbCommerceDataContext();

        var usuario = (from c in data.tbUsuarioExpedicaos where c.idUsuarioExpedicao == Convert.ToInt32(usuarios.ToString()) select c).FirstOrDefault();

        if (usuario != null)
        {
            lblNomeUsuario.Text = usuario.nome;
            lblIdUsuario.Text = usuario.idUsuarioExpedicao.ToString();
            hdnUsuarioID.Value = usuario.idUsuarioExpedicao.ToString();
            lblMensagem.Text = string.Empty;
            lblMensagem.Visible = false;
            dtlPermissoes.Visible = true;

            BindDataList(usuario.idUsuarioExpedicao);
        }
    }

    protected void btnGravarPermissao_Click(object sender, EventArgs e)
    {

        if (cboTipoPermissao.SelectedIndex == 0)
        {
            lblMensagem.Text = "Favor selecinar o tipo de permissão";
            cboTipoPermissao.Focus();
            return;
        }

        var data = new dbCommerceDataContext();
        var usuario = int.Parse(hdnUsuarioID.Value);
        
        tbUsuarioExpedicaoPermissao tb = new tbUsuarioExpedicaoPermissao();
        tb.idUsuarioExpedicao = usuario;
        tb.idExpedicaoTipoPermissao = int.Parse(cboTipoPermissao.SelectedItem.Value.ToString());

        if (cboTipoPermissao.SelectedItem.Text == "Carregamento caminhão" && cboTipoDeEntrega.SelectedIndex > 0)
        {
            tb.Id_Relacionado = int.Parse(cboTipoDeEntrega.SelectedItem.Value.ToString());
            tb.filtro = txtFiltro.Text;
        }

        data.tbUsuarioExpedicaoPermissaos.InsertOnSubmit(tb);

        data.SubmitChanges();
        BindDataList(usuario);
    }

    protected void btnExcluir_Command(object sender, CommandEventArgs e)
    {
        var data = new dbCommerceDataContext();

        if (e.CommandName == "ExcluirControleAcesso")
        {
            var idExclusao = Convert.ToInt32(e.CommandArgument.ToString());
            var itemExclusao = (from c in data.tbUsuarioExpedicaoPermissaos where c.idUsuarioExpedicaoPermissao == idExclusao select c).FirstOrDefault();

            data.tbUsuarioExpedicaoPermissaos.DeleteOnSubmit(itemExclusao);
            data.SubmitChanges();

            BindDataList(Convert.ToInt32(hdnUsuarioID.Value.ToString()));
        }
    }

    protected void dtlPermissoes_ItemCommand(object source, DataListCommandEventArgs e)
    {

    }

    protected void cboTipoPermissao_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            var id = hdnUsuarioID.Value;

            var data = new dbCommerceDataContext();

            var usuario = (from c in data.tbUsuarioExpedicaos where c.idUsuarioExpedicao == int.Parse(id) select c).FirstOrDefault();

            lblNomeUsuario.Text = usuario.nome;
            hdnUsuarioID.Value = usuario.idUsuarioExpedicao.ToString();
            tdTipoEntrega.Visible = false;
            tdIdRelacionado.Visible = false;
            tdFiltro.Visible = false;

            if (cboTipoPermissao.SelectedIndex > 0 && cboTipoPermissao.SelectedItem.Text == "Carregamento caminhão")
            {
                CarregarTipoEntrega();
                tdTipoEntrega.Visible = true;
                //tdIdRelacionado.Visible = true;
                tdFiltro.Visible = true;
            }
        }
        catch (Exception ex)
        {
            lblMensagem.Text = "Erro ao selecionar o tipo de permissão: " + ex.Message;
        }
    }

    private void CarregarTipoEntrega()
    {
        try
        {
            var data = new dbCommerceDataContext();

            var tipoEntrega = (from c in data.tbTipoDeEntregas select c).OrderBy(x => x.tipoDeEntregaNome).ToList();

            cboTipoDeEntrega.TextField = "tipoDeEntregaNome";
            cboTipoDeEntrega.ValueField = "tipoDeEntregaId";
            cboTipoDeEntrega.DataSource = tipoEntrega;
            cboTipoDeEntrega.DataBind();

            cboTipoDeEntrega.Items.Insert(0, new DevExpress.Web.ASPxEditors.ListEditItem() { Index = 0, Text = "Selecione", Value = "" });
            cboTipoDeEntrega.SelectedIndex = 0;
        }
        catch (Exception ex)
        {
            lblMensagem.Text = "Erro ao carregar o tipo de entrega: " + ex.Message;
        }
    }
}