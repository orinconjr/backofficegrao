﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="pedidosClientesRomaneio.aspx.cs" Inherits="admin_pedidosClientesRomaneio" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" runat="Server">
    <style>
        .tabrotulos {
            width: 848px;
            margin: auto;
        }

        .rotulos td {
            font: bold 12px tahoma;
            padding-top: 20px;
            color: rgb(65, 105, 126);
            padding-left: 5px;
            padding-bottom: 5px;
        }

        .meubotao {
            cursor: pointer;
            font: bold 14px tahoma;
        }

        .fieldsetatualizarprecos {
            width: 760px;
            float: left;
            clear: left;
            border-radius: 5px;
            margin-left: 25px;
        }

            .fieldsetatualizarprecos input[type=checkbox] {
                margin-top: 15px;
            }

            .fieldsetatualizarprecos label {
                margin-left: 5px;
                margin-right: 20px;
            }

        .meugrid {
            background-color: #F4FAFB;
            border: 0px none;
            /* border-collapse: separate !important; */
            overflow: hidden;
            font: 9pt Tahoma;
            color: #000;
           /* cursor: pointer;*/
            /* white-space: nowrap;*/
            font-weight: normal;
            text-align: left;
            width: 834px;
            border-collapse: collapse;
            float: left;
            clear: left;
            margin-top: 10px;
            margin-bottom: 10px;
            min-height: 500px;
            margin-left: 25px;
        }

            .meugrid th {
                background-image: url(../../App_Themes/Glass/GridView/gvHeaderBackground.gif);
                background-position: center top;
                background-repeat: repeat-x;
                background-color: #C0DDE0;
                overflow: hidden;
                height: 23px;
                line-height: 23px;
                padding: 4px 6px 5px;
                font-weight: normal;
                border: 1px solid #7EACB1;
                /* border-collapse: separate;*/
            }

            .meugrid td {
                overflow: hidden;
                height: 23px;
                line-height: 23px;
                padding: 4px 6px 5px;
                border: 1px solid #7EACB1;
            }

            .meugrid tr td:nth-child(4) {
                text-align: center;
            }

            .meugrid tr td:last-of-type {
                text-align: center;
            }
    </style>

    <div class="tituloPaginas" valign="top">
        Pedidos vinculados a etiquetas do romaneio:<asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
    </div>

    <div style="float: left; clear: left;">

        <asp:GridView ID="GridView1" CssClass="meugrid" runat="server"  AutoGenerateColumns="False" OnRowDataBound="GridView1_RowDataBound">
             <Columns>

                        <asp:BoundField DataField="idPedidoFornecedorItem"  HeaderText="Etiqueta" />
                        <asp:BoundField DataField="pedidoId" HeaderText="Pedido" />
                        <asp:BoundField DataField="itemPedidoIda" HeaderText="Item Pedido" />
                        <asp:CheckBoxField DataField="brindea" HeaderText="Brinde" />
                        <asp:TemplateField HeaderText="Editar Pedido" ItemStyle-Width="80px" ItemStyle-HorizontalAlign="Center" ShowHeader="False">
                            <ItemTemplate>
                                <a id="linkeditarpedido" runat="server" target="_blank">
                                    <img src="images/btEditar.jpg" alt="Editar" style="border-style: none;" />
                                </a>
                            </ItemTemplate>

<ItemStyle HorizontalAlign="Center" Width="80px"></ItemStyle>
                        </asp:TemplateField>
                 </Columns>
        </asp:GridView>
    </div>

</asp:Content>

