﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="unificarCadastroCategoria.aspx.cs" Inherits="admin_unificarCadastroCategoria" %>

<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v11.1" Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v11.1" Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dxe" %>


<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" runat="Server">
    <div>
        <div style="overflow: auto;">
            <asp:HiddenField ID="hdfPaginaAtual" runat="server" Value="1" />
            <asp:HiddenField ID="hdfUnificados" runat="server" Value="0" />
            <div style="margin-top: 60px">
                <table>
                    <tr>
                        <td>Categoria:<br />
                            <%--<asp:DropDownList ID="ddlCategoria" runat="server" DataTextField="nomeExibicao" DataValueField="categoriaId"></asp:DropDownList>--%>
                            <dxe:ASPxComboBox ID="ddlCategoria" ClientInstanceName="ddlcategoria" runat="server" DropDownStyle="DropDown" IncrementalFilteringMode="Contains"
                                TextField="nomeExibicao" ValueField="categoriaId" Width="100%">
                            </dxe:ASPxComboBox>
                        </td>
                        <td>&nbsp;<br />

                            <dxe:ASPxButton ID="btnCarregar" runat="server" AutoPostBack="true" OnClick="btnCarregar_Click" Text="Filtrar" ClientInstanceName="carregar">
                            </dxe:ASPxButton>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Label runat="server" ID="lblMensagem" ForeColor="red"></asp:Label>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="overflow: auto;">
                <asp:ScriptManager ID="scrManager" runat="server"></asp:ScriptManager>
                <asp:UpdatePanel ID="updPanel" runat="server">
                    <ContentTemplate>
                        <asp:ListView runat="server" ID="lstProdutos" OnItemDataBound="lstProdutos_ItemDataBound" OnItemCommand="lstProdutos_ItemCommand">
                            <ItemTemplate>
                                <div style="float: left; border: 1px solid; margin-right: 5px; margin-bottom: 5px;">
                                    <%# Eval("produtoNome") %><br />
                                    <a href="https://www.graodegente.com.br/a/<%# Eval("produtoUrl") %>" target="_blank">
                                        <img src='https://dmhxz00kguanp.cloudfront.net/fotos/<%# Eval("produtoId") %>/media_<%# Eval("fotoDestaque") %>.jpg' /><br />
                                    </a>
                                    <dxe:ASPxComboBox ID="ddlCategoriaProduto" ClientInstanceName="ddlcategoria" DataSourceID="sqlCategoria"
                                        runat="server" DropDownStyle="DropDown" IncrementalFilteringMode="Contains" TextField="nomeExibicao" ValueField="categoriaId" Width="100%">
                                    </dxe:ASPxComboBox>

                                    <asp:HiddenField runat="server" ID="hdfProdutoId" Value='<%# Eval("produtoId") %>' />
                                    <asp:CheckBox ID="chkConferido" runat="server" AutoPostBack="true" OnCheckedChanged="chkConferido_CheckedChanged" />
                                    Conferido?
                                </div>
                            </ItemTemplate>
                        </asp:ListView>
                        <asp:LinqDataSource
                            TableName="tbCategorias" runat="server" ContextTypeName="dbCommerceDataContext" ID="sqlCategoria">
                        </asp:LinqDataSource>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <br />
                <br />
                <br />
            </div>
        </div>
        <div>
            <dxe:ASPxButton ID="btnGravar" runat="server" Text="Gravar" OnClick="btnGravar_Click"></dxe:ASPxButton>
        </div>
    </div>
</asp:Content>

