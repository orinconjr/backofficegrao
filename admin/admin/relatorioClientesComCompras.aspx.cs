﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CarlosAg.ExcelXmlWriter;
using ICSharpCode.SharpZipLib.Core;

public partial class admin_relatorioClientesComCompras : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnBaixarArquivos_OnClick(object sender, EventArgs e)
    {
        //Até 2 meses - Meninas - Com Kit Berço
        //Até 2 meses - Meninos - Com Kit Berço
        //Até 2 meses - Meninas - Sem Kit Berço
        //Até 2 meses - Meninos - Sem Kit Berço
        //De 2 à 4 meses - Meninas - Com Kit Berço
        //De 2 à 4 meses - Meninos - Com Kit Berço
        //De 2 à 4 meses - Meninas - Sem Kit Berço
        //De 2 à 4 meses - Meninos - Sem Kit Berço
        //De 4 à 7 meses - Meninas - Com Kit Berço
        //De 4 à 7 meses - Meninos - Com Kit Berço
        //De 4 à 7 meses - Meninas - Sem Kit Berço
        //De 4 à 7 meses - Meninos - Sem Kit Berço

        var meses1 = DateTime.Now.AddMonths(-1);
        //var meses2 = DateTime.Now.AddMonths(-1);
        //var meses4 = DateTime.Now.AddMonths(-4);
        //var meses7 = DateTime.Now.AddMonths(-7);

        var data = new dbCommerceDataContext();

        //var ComprasUnicas2 = (from c in data.tbPedidos
        //    join d in data.tbPedidos on c.clienteId equals d.clienteId into pedidosCliente
        //    where c.statusDoPedido == 5 &&
        //          pedidosCliente.Count(
        //              x =>
        //                  (x.statusDoPedido == 2 | x.statusDoPedido == 3 | x.statusDoPedido == 4 | x.statusDoPedido == 5 |
        //                   x.statusDoPedido == 11) && x.pedidoId != c.pedidoId) == 0 &&
        //          c.dataHoraDoPedido > meses2
        //    select c).ToList();

        //var ComprasUnicas4 = (from c in data.tbPedidos
        //    join d in data.tbPedidos on c.clienteId equals d.clienteId into pedidosCliente
        //    where c.statusDoPedido == 5 &&
        //          pedidosCliente.Count(
        //              x =>
        //                  (x.statusDoPedido == 2 | x.statusDoPedido == 3 | x.statusDoPedido == 4 | x.statusDoPedido == 5 |
        //                   x.statusDoPedido == 11) && x.pedidoId != c.pedidoId) == 0 &&
        //          c.dataHoraDoPedido > meses4 && c.dataHoraDoPedido <= meses2 
        //    select c).ToList();

        //var ComprasUnicas7 = (from c in data.tbPedidos
        //    join d in data.tbPedidos on c.clienteId equals d.clienteId into pedidosCliente
        //    where c.statusDoPedido == 5 &&
        //          pedidosCliente.Count(
        //              x =>
        //                  (x.statusDoPedido == 2 | x.statusDoPedido == 3 | x.statusDoPedido == 4 | x.statusDoPedido == 5 |
        //                   x.statusDoPedido == 11) && x.pedidoId != c.pedidoId) == 0 &&
        //          c.dataHoraDoPedido > meses7 && c.dataHoraDoPedido <= meses4
        //    select c).ToList();

        var ComprouENaoPagou = (from c in data.tbPedidos
            join d in data.tbPedidos on c.clienteId equals d.clienteId into pedidosCliente
            where (c.statusDoPedido == 6 | c.statusDoPedido == 7) &&
                  pedidosCliente.Count(
                      x =>
                          (x.statusDoPedido == 2 | x.statusDoPedido == 3 | x.statusDoPedido == 4 | x.statusDoPedido == 5 |
                           x.statusDoPedido == 11) && x.pedidoId != c.pedidoId) == 0 &&
                  c.dataHoraDoPedido > meses1
            select c).ToList();

        #region Clientes
        var clientes = (from c in data.tbPedidos
                              join d in data.tbPedidos on c.clienteId equals d.clienteId into pedidosCliente
                              join f in data.tbClientes on c.clienteId equals f.clienteId
                              where c.statusDoPedido == 5 &&
                                    pedidosCliente.Count(
                                        x =>
                                            (x.statusDoPedido == 2 | x.statusDoPedido == 3 | x.statusDoPedido == 4 | x.statusDoPedido == 5 |
                                             x.statusDoPedido == 11) && x.pedidoId != c.pedidoId) == 0 &&
                                    c.dataHoraDoPedido > meses1
                              select f).ToList();
        #endregion

        #region itensPedido
        var itensPedidoGeral = (from c in data.tbPedidos
                        join d in data.tbPedidos on c.clienteId equals d.clienteId into pedidosCliente
                        join f in data.tbItensPedidos on c.pedidoId equals f.pedidoId
                        join g in data.tbItensPedidoCombos on f.itemPedidoId equals g.idItemPedido
                        where c.statusDoPedido == 5 &&
                              pedidosCliente.Count(
                                  x =>
                                      (x.statusDoPedido == 2 | x.statusDoPedido == 3 | x.statusDoPedido == 4 | x.statusDoPedido == 5 |
                                       x.statusDoPedido == 11) && x.pedidoId != c.pedidoId) == 0 &&
                              c.dataHoraDoPedido > meses1
                        select f).ToList();
        #endregion


        #region categorias

        var categoriasGeral = (from c in data.tbPedidos
                                join d in data.tbPedidos on c.clienteId equals d.clienteId into pedidosCliente
                                join f in data.tbItensPedidos on c.pedidoId equals f.pedidoId
                                join g in data.tbJuncaoProdutoCategorias on f.produtoId equals g.produtoId
                                where c.statusDoPedido == 5 &&
                                      pedidosCliente.Count(
                                          x =>
                                              (x.statusDoPedido == 2 | x.statusDoPedido == 3 | x.statusDoPedido == 4 | x.statusDoPedido == 5 |
                                               x.statusDoPedido == 11) && x.pedidoId != c.pedidoId) == 0 &&
                                      c.dataHoraDoPedido > meses1
                                select g).ToList();
        #endregion



        //string MeninasComKitBerço2Email = "";
        //string MeninosComKitBerço2Email = "";
        //string MeninasSemKitBerço2Email = "";
        //string MeninosSemKitBerço2Email = "";
        //string MeninasComKitBerço4Email = "";
        //string MeninosComKitBerço4Email = "";
        //string MeninasSemKitBerço4Email = "";
        //string MeninosSemKitBerço4Email = "";
        //string MeninasComKitBerço7Email = "";
        //string MeninosComKitBerço7Email = "";
        //string MeninasSemKitBerço7Email = "";
        //string MeninosSemKitBerço7Email = "";

        //string MeninasComKitBerço2Telefone = "";
        //string MeninosComKitBerço2Telefone = "";
        //string MeninasSemKitBerço2Telefone = "";
        //string MeninosSemKitBerço2Telefone = "";
        //string MeninasComKitBerço4Telefone = "";
        //string MeninosComKitBerço4Telefone = "";
        //string MeninasSemKitBerço4Telefone = "";
        //string MeninosSemKitBerço4Telefone = "";
        //string MeninasComKitBerço7Telefone = "";
        //string MeninosComKitBerço7Telefone = "";
        //string MeninasSemKitBerço7Telefone = "";
        //string MeninosSemKitBerço7Telefone = "";


        string ComprouENaoPagouMeninasComKitEmail = "";
        string ComprouENaoPagouMeninosComKitEmail = "";
        string ComprouENaoPagouMeninasComKitTelefone = "";
        string ComprouENaoPagouMeninosComKitTelefone = "";
        string ComprouENaoPagouMeninasSemKitEmail = "";
        string ComprouENaoPagouMeninosSemKitEmail = "";
        string ComprouENaoPagouMeninasSemKitTelefone = "";
        string ComprouENaoPagouMeninosSemKitTelefone = "";


        //foreach (var compra in ComprasUnicas2)
        //{
        //    var itensPedido = (from c in itensPedidoGeral where c.pedidoId == compra.pedidoId select c.produtoId).ToList();
        //    var itensCombo = (from c in itensComboGeral where c.tbItensPedido.pedidoId == compra.pedidoId select c.produtoId).ToList();

        //    itensPedido.AddRange(itensCombo);
        //    var cliente = (from c in clientes where c.clienteId == compra.clienteId select c).FirstOrDefault();

        //    if (cliente != null)
        //    {
        //        var categorias = (from c in categoriasGeral where itensPedido.Contains(c.produtoId) select c).ToList();
        //        int menino = categorias.Count(x => x.categoriaId == 692);
        //        int menina = categorias.Count(x => x.categoriaId == 693);
        //        int kitberco = categorias.Count(x => x.categoriaId == 413);


        //        string telefone = "";
        //        string celular = "";

        //        if (!string.IsNullOrEmpty(cliente.clienteFoneResidencial)) telefone =
        //            "55" + cliente.clienteFoneResidencial.Replace("(", "")
        //                .Replace(")", "")
        //                .Replace(" ", "")
        //                .Replace("-", "");
        //        if (!string.IsNullOrEmpty(cliente.clienteFoneCelular)) celular =
        //            "55" + cliente.clienteFoneCelular.Replace("(", "")
        //                .Replace(")", "")
        //                .Replace(" ", "")
        //                .Replace("-", "");


        //        if (kitberco > 0)
        //        {
        //            if (menino > menina)
        //            {
        //                MeninosComKitBerço2Email += cliente.clienteEmail;
        //                MeninosComKitBerço2Email += Environment.NewLine;
        //                if (!string.IsNullOrEmpty(telefone)) MeninosComKitBerço2Telefone += telefone;
        //                if (!string.IsNullOrEmpty(telefone)) MeninosComKitBerço2Telefone += Environment.NewLine;
        //                if (!string.IsNullOrEmpty(celular)) MeninosComKitBerço2Telefone += celular;
        //                if (!string.IsNullOrEmpty(celular)) MeninosComKitBerço2Telefone += Environment.NewLine;
        //            }
        //            else
        //            {
        //                MeninasComKitBerço2Email += cliente.clienteEmail;
        //                MeninasComKitBerço2Email += Environment.NewLine;
        //                if (!string.IsNullOrEmpty(telefone)) MeninasComKitBerço2Telefone += telefone;
        //                if (!string.IsNullOrEmpty(telefone)) MeninasComKitBerço2Telefone += Environment.NewLine;
        //                if (!string.IsNullOrEmpty(celular)) MeninasComKitBerço2Telefone += celular;
        //                if (!string.IsNullOrEmpty(celular)) MeninasComKitBerço2Telefone += Environment.NewLine;
        //            }
        //        }
        //        else
        //        {
        //            if (menino > menina)
        //            {
        //                MeninosSemKitBerço2Email += cliente.clienteEmail;
        //                MeninosSemKitBerço2Email += Environment.NewLine;
        //                if (!string.IsNullOrEmpty(telefone)) MeninosSemKitBerço2Telefone += telefone;
        //                if (!string.IsNullOrEmpty(telefone)) MeninosSemKitBerço2Telefone += Environment.NewLine;
        //                if (!string.IsNullOrEmpty(celular)) MeninosSemKitBerço2Telefone += celular;
        //                if (!string.IsNullOrEmpty(celular)) MeninosSemKitBerço2Telefone += Environment.NewLine;
        //            }
        //            else
        //            {
        //                MeninasSemKitBerço2Email += cliente.clienteEmail;
        //                MeninasSemKitBerço2Email += Environment.NewLine;
        //                if (!string.IsNullOrEmpty(telefone)) MeninasSemKitBerço2Telefone += telefone;
        //                if (!string.IsNullOrEmpty(telefone)) MeninasSemKitBerço2Telefone += Environment.NewLine;
        //                if (!string.IsNullOrEmpty(celular)) MeninasSemKitBerço2Telefone += celular;
        //                if (!string.IsNullOrEmpty(celular)) MeninasSemKitBerço2Telefone += Environment.NewLine;
        //            }
        //        }
        //    }
        //}
        //foreach (var compra in ComprasUnicas4)
        //{
        //    var itensPedido = (from c in itensPedidoGeral where c.pedidoId == compra.pedidoId select c.produtoId).ToList();
        //    var itensCombo = (from c in itensComboGeral where c.tbItensPedido.pedidoId == compra.pedidoId select c.produtoId).ToList();

        //    itensPedido.AddRange(itensCombo);
        //    var cliente = (from c in clientes where c.clienteId == compra.clienteId select c).FirstOrDefault();

        //    if (cliente != null)
        //    {
        //        var categorias = (from c in categoriasGeral where itensPedido.Contains(c.produtoId) select c).ToList();
        //        int menino = categorias.Count(x => x.categoriaId == 692);
        //        int menina = categorias.Count(x => x.categoriaId == 693);
        //        int kitberco = categorias.Count(x => x.categoriaId == 413);


        //        string telefone = "";
        //        string celular = "";

        //        if (!string.IsNullOrEmpty(cliente.clienteFoneResidencial)) telefone =
        //            "55" + cliente.clienteFoneResidencial.Replace("(", "")
        //                .Replace(")", "")
        //                .Replace(" ", "")
        //                .Replace("-", "");
        //        if (!string.IsNullOrEmpty(cliente.clienteFoneCelular)) celular =
        //            "55" + cliente.clienteFoneCelular.Replace("(", "")
        //                .Replace(")", "")
        //                .Replace(" ", "")
        //                .Replace("-", "");

        //        if (kitberco > 0)
        //        {
        //            if (menino > menina)
        //            {
        //                MeninosComKitBerço4Email += cliente.clienteEmail;
        //                MeninosComKitBerço4Email += Environment.NewLine;
        //                if (!string.IsNullOrEmpty(telefone)) MeninosComKitBerço4Telefone += telefone;
        //                if (!string.IsNullOrEmpty(telefone)) MeninosComKitBerço4Telefone += Environment.NewLine;
        //                if (!string.IsNullOrEmpty(celular)) MeninosComKitBerço4Telefone += celular;
        //                if (!string.IsNullOrEmpty(celular)) MeninosComKitBerço4Telefone += Environment.NewLine;
        //            }
        //            else
        //            {
        //                MeninasComKitBerço4Email += cliente.clienteEmail;
        //                MeninasComKitBerço4Email += Environment.NewLine;
        //                if (!string.IsNullOrEmpty(telefone)) MeninasComKitBerço4Telefone += telefone;
        //                if (!string.IsNullOrEmpty(telefone)) MeninasComKitBerço4Telefone += Environment.NewLine;
        //                if (!string.IsNullOrEmpty(celular)) MeninasComKitBerço4Telefone += celular;
        //                if (!string.IsNullOrEmpty(celular)) MeninasComKitBerço4Telefone += Environment.NewLine;
        //            }
        //        }
        //        else
        //        {
        //            if (menino > menina)
        //            {
        //                MeninosSemKitBerço4Email += cliente.clienteEmail;
        //                MeninosSemKitBerço4Email += Environment.NewLine;
        //                if (!string.IsNullOrEmpty(telefone)) MeninosSemKitBerço4Telefone += telefone;
        //                if (!string.IsNullOrEmpty(telefone)) MeninosSemKitBerço4Telefone += Environment.NewLine;
        //                if (!string.IsNullOrEmpty(celular)) MeninosSemKitBerço4Telefone += celular;
        //                if (!string.IsNullOrEmpty(celular)) MeninosSemKitBerço4Telefone += Environment.NewLine;
        //            }
        //            else
        //            {
        //                MeninasSemKitBerço4Email += cliente.clienteEmail;
        //                MeninasSemKitBerço4Email += Environment.NewLine;
        //                if (!string.IsNullOrEmpty(telefone)) MeninasSemKitBerço4Telefone += telefone;
        //                if (!string.IsNullOrEmpty(telefone)) MeninasSemKitBerço4Telefone += Environment.NewLine;
        //                if (!string.IsNullOrEmpty(celular)) MeninasSemKitBerço4Telefone += celular;
        //                if (!string.IsNullOrEmpty(celular)) MeninasSemKitBerço4Telefone += Environment.NewLine;
        //            }
        //        }
        //    }
        //}
        //foreach (var compra in ComprasUnicas7)
        //{
        //    var itensPedido = (from c in itensPedidoGeral where c.pedidoId == compra.pedidoId select c.produtoId).ToList();
        //    var itensCombo = (from c in itensComboGeral where c.tbItensPedido.pedidoId == compra.pedidoId select c.produtoId).ToList();

        //    itensPedido.AddRange(itensCombo);
        //    var cliente = (from c in clientes where c.clienteId == compra.clienteId select c).FirstOrDefault();

        //    if (cliente != null)
        //    {
        //        var categorias = (from c in categoriasGeral where itensPedido.Contains(c.produtoId) select c).ToList();
        //        int menino = categorias.Count(x => x.categoriaId == 692);
        //        int menina = categorias.Count(x => x.categoriaId == 693);
        //        int kitberco = categorias.Count(x => x.categoriaId == 413);


        //        string telefone = "";
        //        string celular = "";

        //        if (!string.IsNullOrEmpty(cliente.clienteFoneResidencial)) telefone =
        //            "55" + cliente.clienteFoneResidencial.Replace("(", "")
        //                .Replace(")", "")
        //                .Replace(" ", "")
        //                .Replace("-", "");
        //        if (!string.IsNullOrEmpty(cliente.clienteFoneCelular)) celular =
        //            "55" + cliente.clienteFoneCelular.Replace("(", "")
        //                .Replace(")", "")
        //                .Replace(" ", "")
        //                .Replace("-", "");

        //        if (kitberco > 0)
        //        {
        //            if (menino > menina)
        //            {
        //                MeninosComKitBerço7Email += cliente.clienteEmail;
        //                MeninosComKitBerço7Email += Environment.NewLine;
        //                if (!string.IsNullOrEmpty(telefone)) MeninosComKitBerço7Telefone += telefone;
        //                if (!string.IsNullOrEmpty(telefone)) MeninosComKitBerço7Telefone += Environment.NewLine;
        //                if (!string.IsNullOrEmpty(celular)) MeninosComKitBerço7Telefone += celular;
        //                if (!string.IsNullOrEmpty(celular)) MeninosComKitBerço7Telefone += Environment.NewLine;
        //            }
        //            else
        //            {
        //                MeninasComKitBerço7Email += cliente.clienteEmail;
        //                MeninasComKitBerço7Email += Environment.NewLine;
        //                if (!string.IsNullOrEmpty(telefone)) MeninasComKitBerço7Telefone += telefone;
        //                if (!string.IsNullOrEmpty(telefone)) MeninasComKitBerço7Telefone += Environment.NewLine;
        //                if (!string.IsNullOrEmpty(celular)) MeninasComKitBerço7Telefone += celular;
        //                if (!string.IsNullOrEmpty(celular)) MeninasComKitBerço7Telefone += Environment.NewLine;
        //            }
        //        }
        //        else
        //        {
        //            if (menino > menina)
        //            {
        //                MeninosSemKitBerço7Email += cliente.clienteEmail;
        //                MeninosSemKitBerço7Email += Environment.NewLine;
        //                if (!string.IsNullOrEmpty(telefone)) MeninosSemKitBerço7Telefone += telefone;
        //                if (!string.IsNullOrEmpty(telefone)) MeninosSemKitBerço7Telefone += Environment.NewLine;
        //                if (!string.IsNullOrEmpty(celular)) MeninosSemKitBerço7Telefone += celular;
        //                if (!string.IsNullOrEmpty(celular)) MeninosSemKitBerço7Telefone += Environment.NewLine;
        //            }
        //            else
        //            {
        //                MeninasSemKitBerço7Email += cliente.clienteEmail;
        //                MeninasSemKitBerço7Email += Environment.NewLine;
        //                if (!string.IsNullOrEmpty(telefone)) MeninasSemKitBerço7Telefone += telefone;
        //                if (!string.IsNullOrEmpty(telefone)) MeninasSemKitBerço7Telefone += Environment.NewLine;
        //                if (!string.IsNullOrEmpty(celular)) MeninasSemKitBerço7Telefone += celular;
        //                if (!string.IsNullOrEmpty(celular)) MeninasSemKitBerço7Telefone += Environment.NewLine;
        //            }
        //        }
        //    }
        //}
        foreach (var compra in ComprouENaoPagou)
        {
            var itensPedido = (from c in itensPedidoGeral where c.pedidoId == compra.pedidoId select c.produtoId).ToList();

            var cliente = (from c in clientes where c.clienteId == compra.clienteId select c).FirstOrDefault();

            if (cliente != null)
            {
                var categorias = (from c in categoriasGeral where itensPedido.Contains(c.produtoId) select c).ToList();
                int menino = categorias.Count(x => x.categoriaId == 692);
                int menina = categorias.Count(x => x.categoriaId == 693);
                int kitberco = categorias.Count(x => x.categoriaId == 413);


                string telefone = "";
                string celular = "";

                if (!string.IsNullOrEmpty(cliente.clienteFoneResidencial)) telefone =
                    "55" + cliente.clienteFoneResidencial.Replace("(", "")
                        .Replace(")", "")
                        .Replace(" ", "")
                        .Replace("-", "");
                if (!string.IsNullOrEmpty(cliente.clienteFoneCelular)) celular =
                    "55" + cliente.clienteFoneCelular.Replace("(", "")
                        .Replace(")", "")
                        .Replace(" ", "")
                        .Replace("-", "");

                if (kitberco > 0)
                {
                    if (menino > menina)
                    {
                        ComprouENaoPagouMeninosComKitEmail += cliente.clienteEmail;
                        ComprouENaoPagouMeninosComKitEmail += Environment.NewLine;
                        if (!string.IsNullOrEmpty(telefone)) ComprouENaoPagouMeninosComKitTelefone += telefone;
                        if (!string.IsNullOrEmpty(telefone)) ComprouENaoPagouMeninosComKitTelefone += Environment.NewLine;
                        if (!string.IsNullOrEmpty(celular)) ComprouENaoPagouMeninosComKitTelefone += celular;
                        if (!string.IsNullOrEmpty(celular)) ComprouENaoPagouMeninosComKitTelefone += Environment.NewLine;
                    }
                    else
                    {
                        ComprouENaoPagouMeninasComKitEmail += cliente.clienteEmail;
                        ComprouENaoPagouMeninasComKitEmail += Environment.NewLine;
                        if (!string.IsNullOrEmpty(telefone)) ComprouENaoPagouMeninasComKitTelefone += telefone;
                        if (!string.IsNullOrEmpty(telefone)) ComprouENaoPagouMeninasComKitTelefone += Environment.NewLine;
                        if (!string.IsNullOrEmpty(celular)) ComprouENaoPagouMeninasComKitTelefone += celular;
                        if (!string.IsNullOrEmpty(celular)) ComprouENaoPagouMeninasComKitTelefone += Environment.NewLine;
                    }
                }
                else
                {
                    if (menino > menina)
                    {
                        ComprouENaoPagouMeninosSemKitEmail += cliente.clienteEmail;
                        ComprouENaoPagouMeninosSemKitEmail += Environment.NewLine;
                        if (!string.IsNullOrEmpty(telefone)) ComprouENaoPagouMeninosSemKitTelefone += telefone;
                        if (!string.IsNullOrEmpty(telefone)) ComprouENaoPagouMeninosSemKitTelefone += Environment.NewLine;
                        if (!string.IsNullOrEmpty(celular)) ComprouENaoPagouMeninosSemKitTelefone += celular;
                        if (!string.IsNullOrEmpty(celular)) ComprouENaoPagouMeninosSemKitTelefone += Environment.NewLine;
                    }
                    else
                    {
                        ComprouENaoPagouMeninasSemKitEmail += cliente.clienteEmail;
                        ComprouENaoPagouMeninasSemKitEmail += Environment.NewLine;
                        if (!string.IsNullOrEmpty(telefone)) ComprouENaoPagouMeninasSemKitTelefone += telefone;
                        if (!string.IsNullOrEmpty(telefone)) ComprouENaoPagouMeninasSemKitTelefone += Environment.NewLine;
                        if (!string.IsNullOrEmpty(celular)) ComprouENaoPagouMeninasSemKitTelefone += celular;
                        if (!string.IsNullOrEmpty(celular)) ComprouENaoPagouMeninasSemKitTelefone += Environment.NewLine;
                    }
                }
            }
        }





        Response.Clear();
        Response.ContentType = "application/zip";
        Response.AddHeader("Content-Disposition", string.Format("attachment; filename=\"{0}\"", "listas.zip"));
        Response.BufferOutput = false;
        
        var zipOutput = new ICSharpCode.SharpZipLib.Zip.ZipOutputStream(Response.OutputStream);
        //zipOutput = AdicionaArquivoZip(zipOutput, MeninasComKitBerço2Email, "MeninasComKitBerço2Email");
        //zipOutput = AdicionaArquivoZip(zipOutput, MeninosComKitBerço2Email, "MeninosComKitBerço2Email");
        //zipOutput = AdicionaArquivoZip(zipOutput, MeninasSemKitBerço2Email, "MeninasSemKitBerço2Email");
        //zipOutput = AdicionaArquivoZip(zipOutput, MeninosSemKitBerço2Email, "MeninosSemKitBerço2Email");
        //zipOutput = AdicionaArquivoZip(zipOutput, MeninasComKitBerço4Email, "MeninasComKitBerço4Email");
        //zipOutput = AdicionaArquivoZip(zipOutput, MeninosComKitBerço4Email, "MeninosComKitBerço4Email");
        //zipOutput = AdicionaArquivoZip(zipOutput, MeninasSemKitBerço4Email, "MeninasSemKitBerço4Email");
        //zipOutput = AdicionaArquivoZip(zipOutput, MeninosSemKitBerço4Email, "MeninosSemKitBerço4Email");
        //zipOutput = AdicionaArquivoZip(zipOutput, MeninasComKitBerço7Email, "MeninasComKitBerço7Email");
        //zipOutput = AdicionaArquivoZip(zipOutput, MeninosComKitBerço7Email, "MeninosComKitBerço7Email");
        //zipOutput = AdicionaArquivoZip(zipOutput, MeninasSemKitBerço7Email, "MeninasSemKitBerço7Email");
        //zipOutput = AdicionaArquivoZip(zipOutput, MeninosSemKitBerço7Email, "MeninosSemKitBerço7Email");

        //zipOutput = AdicionaArquivoZip(zipOutput, MeninasComKitBerço2Telefone, "MeninasComKitBerço2Telefone");
        //zipOutput = AdicionaArquivoZip(zipOutput, MeninosComKitBerço2Telefone, "MeninosComKitBerço2Telefone");
        //zipOutput = AdicionaArquivoZip(zipOutput, MeninasSemKitBerço2Telefone, "MeninasSemKitBerço2Telefone");
        //zipOutput = AdicionaArquivoZip(zipOutput, MeninosSemKitBerço2Telefone, "MeninosSemKitBerço2Telefone");
        //zipOutput = AdicionaArquivoZip(zipOutput, MeninasComKitBerço4Telefone, "MeninasComKitBerço4Telefone");
        //zipOutput = AdicionaArquivoZip(zipOutput, MeninosComKitBerço4Telefone, "MeninosComKitBerço4Telefone");
        //zipOutput = AdicionaArquivoZip(zipOutput, MeninasSemKitBerço4Telefone, "MeninasSemKitBerço4Telefone");
        //zipOutput = AdicionaArquivoZip(zipOutput, MeninosSemKitBerço4Telefone, "MeninosSemKitBerço4Telefone");
        //zipOutput = AdicionaArquivoZip(zipOutput, MeninasComKitBerço7Telefone, "MeninasComKitBerço7Telefone");
        //zipOutput = AdicionaArquivoZip(zipOutput, MeninosComKitBerço7Telefone, "MeninosComKitBerço7Telefone");
        //zipOutput = AdicionaArquivoZip(zipOutput, MeninasSemKitBerço7Telefone, "MeninasSemKitBerço7Telefone");
        //zipOutput = AdicionaArquivoZip(zipOutput, MeninosSemKitBerço7Telefone, "MeninosSemKitBerço7Telefone");

        zipOutput = AdicionaArquivoZip(zipOutput, ComprouENaoPagouMeninasComKitEmail, "ComprouENaoPagouMeninasComKitEmail");
        zipOutput = AdicionaArquivoZip(zipOutput, ComprouENaoPagouMeninosComKitEmail, "ComprouENaoPagouMeninosComKitEmail");
        zipOutput = AdicionaArquivoZip(zipOutput, ComprouENaoPagouMeninasComKitTelefone, "ComprouENaoPagouMeninasComKitTelefone");
        zipOutput = AdicionaArquivoZip(zipOutput, ComprouENaoPagouMeninosComKitTelefone, "ComprouENaoPagouMeninosComKitTelefone");

        zipOutput = AdicionaArquivoZip(zipOutput, ComprouENaoPagouMeninasSemKitEmail, "ComprouENaoPagouMeninasSemKitEmail");
        zipOutput = AdicionaArquivoZip(zipOutput, ComprouENaoPagouMeninosSemKitEmail, "ComprouENaoPagouMeninosSemKitEmail");
        zipOutput = AdicionaArquivoZip(zipOutput, ComprouENaoPagouMeninasSemKitTelefone, "ComprouENaoPagouMeninasSemKitTelefone");
        zipOutput = AdicionaArquivoZip(zipOutput, ComprouENaoPagouMeninosSemKitTelefone, "ComprouENaoPagouMeninosSemKitTelefone");
        
        zipOutput.Finish();
        Response.Flush();
        Response.End();
        zipOutput.Dispose();
    }

    private ICSharpCode.SharpZipLib.Zip.ZipOutputStream AdicionaArquivoZip(ICSharpCode.SharpZipLib.Zip.ZipOutputStream zipOutput, string texto, string nomeArquivo)
    {

        MemoryStream streamArquivo = new MemoryStream(UTF8Encoding.Default.GetBytes(texto));

        var nextEntry = new ICSharpCode.SharpZipLib.Zip.ZipEntry(nomeArquivo + ".txt");
        zipOutput.UseZip64 = ICSharpCode.SharpZipLib.Zip.UseZip64.Off;
        zipOutput.PutNextEntry(nextEntry);

        StreamUtils.Copy(streamArquivo, zipOutput, new byte[4096]);
        zipOutput.CloseEntry();

        return zipOutput;
    }

    protected void btnBaixarArquivos2_OnClick(object sender, EventArgs e)
    {
        var data = new dbCommerceDataContext();
        var dataInicial = DateTime.Now.AddMonths(-2);
        var pedidos = (from c in data.tbPedidos
                       join d in data.tbClientes on c.clienteId equals d.clienteId
                       where c.statusDoPedido == 5 && c.dataHoraDoPedido >= dataInicial
                       orderby c.pedidoId descending
                       select new
                       {
                           d.clienteEmail,
                           d.clienteNome,
                           c.pedidoId
                       });
       

        Workbook book = new Workbook();
        WorksheetStyle normal = book.Styles.Add("normal");
        normal.Font.Bold = false;
        Worksheet sheet = book.Worksheets.Add("Clientes");

        foreach (var pedido in pedidos)
        {
            WorksheetRow linha = sheet.Table.Rows.Add();
            linha.Cells.Add(pedido.clienteEmail.ToString(), DataType.String, "normal");
            linha.Cells.Add(pedido.clienteNome.ToString(), DataType.String, "normal");
            linha.Cells.Add(rnFuncoes.retornaIdCliente(pedido.pedidoId).ToString(), DataType.String, "normal");
        }


        using (MemoryStream memoryStream = new MemoryStream())
        {
            string nomeArquivo = "clientes.xls";
            book.Save(memoryStream);
            Response.Clear();
            Response.ContentType = "application/ms-excel";
            Response.AddHeader("Content-Disposition", "attachment; filename=" + nomeArquivo);
            Response.BinaryWrite(memoryStream.ToArray());
            Response.Flush();
            Response.Close();
            Response.End();
        }
    }
}