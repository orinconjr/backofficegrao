using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using SpreadsheetLight;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using System.Text.RegularExpressions;
public partial class admin_relatorioProdutosAdicionadosManualmente : System.Web.UI.Page
{
    public class Item
    {
        public int itemPedidoId { get; set; }
        public DateTime dataDaCriacao { get; set; }
        public int pedidoId { get; set; }
        public int produtoId { get; set; }
        public string produtoNome { get; set; }
        public decimal? produtoPrecoDeCusto { get; set; }
        public string motivoAdicao { get; set; }
        public string vlNota { get; set; }
        public decimal? vlFrete { get; set; }
        public string transportadora { get; set; }
        public int qtdNotas { get; set; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (validarcampos())
            {
                carregaGrid();
            }

        }
    }
    private void carregaGrid()
    {
        List<Item> itens = getDados();
        GridView1.DataSource = itens;
        GridView1.DataBind();
        lblitensencontrados.Text = itens.Count().ToString();
        lblprecodecusto.Text = Convert.ToDecimal(itens.Sum(x => x.produtoPrecoDeCusto)).ToString("C");
    }

    public List<Item> getDados()
    {
        var data = new dbCommerceDataContext();
        List<Item> itensPedido = (from ip in data.tbItensPedidos
                                  where ip.motivoAdicao != null && ip.tbPedido.statusDoPedido == 5
                                  select new Item()
                                  {
                                      itemPedidoId = ip.itemPedidoId,
                                      dataDaCriacao = ip.dataDaCriacao,
                                      pedidoId = ip.pedidoId,
                                      produtoId = ip.produtoId,
                                      produtoNome = ip.tbProduto.produtoNome,
                                      produtoPrecoDeCusto = ip.tbProduto.produtoPrecoDeCusto,
                                      motivoAdicao = ip.motivoAdicao,
                                      vlFrete = ip.tbPedido.valorDoFrete,
                                      qtdNotas = (from nf in data.tbNotaFiscals
                                                  where nf.idPedido == ip.pedidoId
                                                  select new { nf.numeroNota }).Count(),
                                      transportadora = (from pe in data.tbPedidoEnvios
                                                        where pe.idPedido == ip.pedidoId
                                                        select new { pe.formaDeEnvio }).Select(x => x.formaDeEnvio).FirstOrDefault().ToString(),
                                      vlNota = (from pe in data.tbPedidoEnvios
                                                where pe.idPedido == ip.pedidoId
                                                select new { pe.nfeValor }).Select(x => x.nfeValor).FirstOrDefault().ToString()

                                  }).ToList<Item>();



        if (!String.IsNullOrEmpty(txtNumPedido.Text))
        {
            int pedidoId = Convert.ToInt32(txtNumPedido.Text);
            itensPedido = (from ip in itensPedido where ip.pedidoId == pedidoId select ip).ToList();
        }

        if (txtDataInicial.Text != "" && txtDataFinal.Text != "")
        {
            var dtInicial = Convert.ToDateTime(txtDataInicial.Text);
            DateTime dtFinal = Convert.ToDateTime(txtDataFinal.Text + " 23:59:59");
            itensPedido =
                itensPedido.Where(
                    x => x.dataDaCriacao >= dtInicial && x.dataDaCriacao <= dtFinal).ToList();
        }
        if (ddlMotivoAdicionarItem.SelectedItem.Value != "0")
        {
            string motivoAdicao = ddlMotivoAdicionarItem.SelectedItem.Value.ToString();
            itensPedido = (from pp in itensPedido where pp.motivoAdicao == motivoAdicao select pp).ToList();
        }
        if (ddlordenarpor.SelectedItem.Value == "dataDaCriacao")
            itensPedido = itensPedido.OrderByDescending(x => x.dataDaCriacao).ToList();
        if (ddlordenarpor.SelectedItem.Value == "pedidoId")
            itensPedido = itensPedido.OrderByDescending(x => x.pedidoId).ToList();
        if (ddlordenarpor.SelectedItem.Value == "produtoPrecoDeCusto")
            itensPedido = itensPedido.OrderBy(x => x.produtoPrecoDeCusto).ToList();
        if (ddlordenarpor.SelectedItem.Value == "motivoAdicao")
            itensPedido = itensPedido.OrderBy(x => x.motivoAdicao).ToList();

        return itensPedido;

    }
    bool validarcampos()
    {
        string erro = "";
        bool valido = true;
        if (!String.IsNullOrEmpty(txtNumPedido.Text))
        {
            try { int teste = Convert.ToInt32(txtNumPedido.Text); }
            catch (Exception)
            {
                erro += @"Numero do pedido invalido\n";
                valido = false;
            }
        }

        if (!String.IsNullOrEmpty(txtDataInicial.Text))
        {
            try { DateTime teste = Convert.ToDateTime(txtDataInicial.Text); }
            catch (Exception)
            {
                erro += @"Data inicial invalida\n";
                valido = false;
            }
        }

        if (!String.IsNullOrEmpty(txtDataFinal.Text))
        {
            try { DateTime teste = Convert.ToDateTime(txtDataFinal.Text); }
            catch (Exception)
            {
                erro += @"Data final invalida\n";
                valido = false;
            }
        }

        if (erro != "")
        {
            string meuscript = @"alert('" + erro + "');";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "MyScript", meuscript, true);

        }
        return valido;
    }
    protected void btnFiltrar_Click(object sender, EventArgs e)
    {
        if (validarcampos())
            carregaGrid();
    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;

        carregaGrid();
    }
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            string pedidoId = DataBinder.Eval(e.Row.DataItem, "pedidoId").ToString();
            Decimal vlFrete = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "vlFrete") ?? 0);
            Decimal vlNota = 0;
            try { vlNota = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "vlNota").ToString().Replace(".", ",")); }
            catch (Exception) { }

            Label lblVlNota = (Label)e.Row.FindControl("lblVlNota");
            lblVlNota.Text = String.Format("{0:c}", (vlFrete + vlNota));

            HtmlAnchor linkeditar = (HtmlAnchor)e.Row.FindControl("linkeditar");
            linkeditar.HRef = "pedido.aspx?pedidoId=" + pedidoId;
        }
    }
    protected void btnExportarPlanilha_Click(object sender, EventArgs e)
    {
        if (validarcampos())
        {
            List<Item> itens = getDados();
            SLDocument sl = new SLDocument();
            SLStyle style1 = sl.CreateStyle();
            style1.Font.Bold = true;
            sl.SetRowStyle(1, style1);

            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("Content-Disposition", "attachment; filename=Produtos_Adicionados_Manualmente.xlsx");

            sl.SetCellValue("A1", "Data");
            sl.SetCellValue("B1", "Id Pedido");
            sl.SetCellValue("C1", "Pre�o de custo");
            sl.SetCellValue("D1", "VL 1� NF");
            sl.SetCellValue("E1", "VL Frete");
            sl.SetCellValue("F1", "Qtd Notas");
            sl.SetCellValue("G1", "Transportadora");
            sl.SetCellValue("H1", "Motivo da Adi��o");


            int linha = 2;
            foreach (var item in itens)
            {
                sl.SetCellValue(linha, 1, item.dataDaCriacao.ToShortDateString());
                sl.SetCellValue(linha, 2, item.pedidoId.ToString());
                sl.SetCellValue(linha, 3, Convert.ToDecimal(item.produtoPrecoDeCusto).ToString("C"));
                Decimal vlNota = 0;
                try { vlNota = Convert.ToDecimal(item.vlNota.Replace(".", ",")); }
                catch (Exception) { }
                vlNota += Convert.ToDecimal(item.vlFrete);
                sl.SetCellValue(linha, 4, vlNota.ToString("C"));
                sl.SetCellValue(linha, 5, Convert.ToDecimal(item.vlFrete).ToString("C"));
                sl.SetCellValue(linha, 6, item.qtdNotas.ToString());
                sl.SetCellValue(linha, 7, item.transportadora);
                sl.SetCellValue(linha, 8, item.motivoAdicao);
                linha++;
            }

            sl.SaveAs(Response.OutputStream);
            Response.End();
        }

    }

}