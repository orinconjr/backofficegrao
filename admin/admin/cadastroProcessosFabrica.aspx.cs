﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_cadastroProcessosFabrica : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            var data = new dbCommerceDataContext();
            var categorias =
                (from c in data.tbProdutoCategorias where c.categoriaPaiId == 0 && c.exibirSite == true orderby c.categoriaNome select c).ToList
                    ();
            ddlCategoria.DataSource = categorias;
            ddlCategoria.DataBind();

            var processos =
                (from c in data.tbSubProcessoFabricas
                 select new
                 {
                     c.idSubProcessoFabrica,
                     nome = c.tbProcessoFabrica.processo + " - " + c.nome
                 }).ToList
                    ();
            ddlProcessosFabrica.DataSource = processos.OrderBy(x => x.nome);
            ddlProcessosFabrica.DataBind();
        }

        //fillListProcesso();
    }

    private void filtrar()
    {
        var data = new dbCommerceDataContext();
        var produtosCategoria = (from c in data.tbJuncaoProdutoCategorias
                                 where c.tbProduto.produtoFornecedor == 82 && c.tbProdutoCategoria.categoriaPaiId == 0
                                 select c).ToList();
        var produtosProcessos = (from c in data.tbJuncaoProdutoProcessoFabricas
                                 where c.tbProduto.produtoFornecedor == 82
                                 select c).ToList();
        var produtos = (from c in data.tbProdutos
                        where c.produtoFornecedor == 82
                        select new
                        {
                            c.produtoId,
                            c.produtoNome,
                            c.fotoDestaque,
                            c.produtoAtivo
                        });
        if (!string.IsNullOrEmpty(txtNome.Text))
        {
            produtos = produtos.Where(x => x.produtoNome.Contains(txtNome.Text));
        }

        if (!string.IsNullOrEmpty(idsProdutos.Text))
        {
            bool idsValidos = true;
            List<int> produtoIds = new List<int>();

            try
            {
                produtoIds = idsProdutos.Text.Split(',').Select(x => Convert.ToInt32(x.Trim())).ToList();
            }
            catch (Exception)
            {
                idsValidos = false;
                ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('não foi possivel utilizar a lista de ids de produtos informada!')", true);
            }

            if (idsValidos)
                produtos = produtos.Where(x => produtoIds.Contains(x.produtoId));
        }

        if (ddlCategoria.SelectedValue != "")
        {
            int categoriaId = Convert.ToInt32(ddlCategoria.SelectedValue);
            produtosCategoria = produtosCategoria.Where(x => x.categoriaId == categoriaId).ToList();
            produtos = (from c in produtos where produtosCategoria.Select(x => x.produtoId).Contains(c.produtoId) select c);
        }
        if (ddlProcessosFabrica.SelectedValue != "")
        {
            int idProcesso = Convert.ToInt32(ddlProcessosFabrica.SelectedValue);
            produtosProcessos = produtosProcessos.Where(x => x.idSubProcessoFabrica == idProcesso).ToList();
            produtos = (from c in produtos where produtosProcessos.Select(x => x.idProduto).Contains(c.produtoId) select c);
        }
        if (chkAtivo.Checked)
        {
            produtos = produtos.Where(x => x.produtoAtivo.ToLower() == "true");
        }
        var listaFinal = produtos.OrderBy(x => x.produtoNome).ToList();
        lstProdutos.DataSource = listaFinal;
        lstProdutos.DataBind();

        litTotal.Text = listaFinal.Count.ToString();

    }

    private void FiltrarCarregaGrid()
    {
        var data = new dbCommerceDataContext();
        var produtosCategoria = (from c in data.tbJuncaoProdutoCategorias
                                 where c.tbProduto.produtoFornecedor == 82 && c.tbProdutoCategoria.categoriaPaiId == 0
                                 select c).ToList();
        var produtosProcessos = (from c in data.tbJuncaoProdutoProcessoFabricas
                                 where c.tbProduto.produtoFornecedor == 82
                                 select c).ToList();
        var produtos = (from c in data.tbProdutos
                        where c.produtoFornecedor == 82
                        select new
                        {
                            c.produtoId,
                            c.produtoNome,
                            c.fotoDestaque,
                            c.produtoAtivo
                        });
        if (!string.IsNullOrEmpty(txtNome.Text))
        {
            produtos = produtos.Where(x => x.produtoNome.Contains(txtNome.Text));
        }

        if (!string.IsNullOrEmpty(idsProdutos.Text))
        {
            bool idsValidos = true;
            List<int> produtoIds = new List<int>();

            try
            {
                produtoIds = idsProdutos.Text.Split(',').Select(x => Convert.ToInt32(x.Trim())).ToList();
            }
            catch (Exception)
            {
                idsValidos = false;
                ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('não foi possivel utilizar a lista de ids de produtos informada!')", true);
            }

            if (idsValidos)
                produtos = produtos.Where(x => produtoIds.Contains(x.produtoId));
        }

        if (ddlCategoria.SelectedValue != "")
        {
            int categoriaId = Convert.ToInt32(ddlCategoria.SelectedValue);
            produtosCategoria = produtosCategoria.Where(x => x.categoriaId == categoriaId).ToList();
            produtos = (from c in produtos where produtosCategoria.Select(x => x.produtoId).Contains(c.produtoId) select c);
        }
        if (ddlProcessosFabrica.SelectedValue != "")
        {
            int idProcesso = Convert.ToInt32(ddlProcessosFabrica.SelectedValue);
            produtosProcessos = produtosProcessos.Where(x => x.idSubProcessoFabrica == idProcesso).ToList();
            produtos = (from c in produtos where produtosProcessos.Select(x => x.idProduto).Contains(c.produtoId) select c);
        }
        if (chkAtivo.Checked)
        {
            produtos = produtos.Where(x => x.produtoAtivo.ToLower() == "true");
        }
        var listaFinal = produtos.OrderBy(x => x.produtoNome).ToList();
        GridView1.DataSource = listaFinal;
        GridView1.DataBind();

        litTotal.Text = listaFinal.Count.ToString();
    }

    protected void btnFiltrar_OnClick(object sender, EventArgs e)
    {
        //filtrar();
        FiltrarCarregaGrid();
    }

    protected void lstProdutos_OnItemDataBound(object sender, ListViewItemEventArgs e)
    {
        var hdfProdutoId = (HiddenField)e.Item.FindControl("hdfProdutoId");
        var hdfFoto = (HiddenField)e.Item.FindControl("hdfFoto");
        var imgFoto = (Image)e.Item.FindControl("imgFoto");

        imgFoto.ImageUrl = "http://dmhxz00kguanp.cloudfront.net/fotos/" + hdfProdutoId.Value + "/" + hdfFoto.Value + ".jpg";
    }

    protected void lstProdutos_OnDataBound(object sender, EventArgs e)
    {
        var data = new dbCommerceDataContext();

        var estoque = (from c in data.tbSubProcessoFabricas where c.idProcessoFabrica == 7 orderby c.nome select c);
        var materia = (from c in data.tbSubProcessoFabricas where c.idProcessoFabrica == 8 orderby c.nome select c);
        var corte = (from c in data.tbSubProcessoFabricas where c.idProcessoFabrica == 1 orderby c.nome select c);
        var bordado = (from c in data.tbSubProcessoFabricas where c.idProcessoFabrica == 2 orderby c.nome select c);
        var costura = (from c in data.tbSubProcessoFabricas where c.idProcessoFabrica == 3 && c.idSubProcessoFabrica != 10 orderby c.nome select c);
        var cola = (from c in data.tbSubProcessoFabricas where c.idProcessoFabrica == 4 orderby c.nome select c);
        var arremate = (from c in data.tbSubProcessoFabricas where c.idProcessoFabrica == 9 orderby c.nome select c);
        var enchimento = (from c in data.tbSubProcessoFabricas where c.idProcessoFabrica == 10 orderby c.nome select c);
        var embalagem = (from c in data.tbSubProcessoFabricas where c.idProcessoFabrica == 5 orderby c.nome select c);
        var entrega = (from c in data.tbSubProcessoFabricas where c.idProcessoFabrica == 6 orderby c.nome select c);


        lstEstoqueC.DataSource = estoque;
        lstEstoqueC.DataBind();

        lstMateriaC.DataSource = materia;
        lstMateriaC.DataBind();

        lstCorteC.DataSource = corte;
        lstCorteC.DataBind();

        lstBordadoC.DataSource = bordado;
        lstBordadoC.DataBind();

        lstCosturaC.DataSource = costura;
        lstCosturaC.DataBind();

        lstColaC.DataSource = cola;
        lstColaC.DataBind();

        lstArremateC.DataSource = arremate;
        lstArremateC.DataBind();

        lstEnchimentoC.DataSource = enchimento;
        lstEnchimentoC.DataBind();

        lstEmbalagemC.DataSource = embalagem;
        lstEmbalagemC.DataBind();

        lstEntregaC.DataSource = entrega;
        lstEntregaC.DataBind();

        foreach (var item in lstProdutos.Items)
        {
            var hdfProdutoId = (HiddenField)item.FindControl("hdfProdutoId");
            var lstEstoque = (ListView)item.FindControl("lstEstoque");
            var lstMateria = (ListView)item.FindControl("lstMateria");
            var lstCorte = (ListView)item.FindControl("lstCorte");
            var lstBordado = (ListView)item.FindControl("lstBordado");
            var lstCostura = (ListView)item.FindControl("lstCostura");
            var lstCola = (ListView)item.FindControl("lstCola");
            var lstArremate = (ListView)item.FindControl("lstArremate");
            var lstEnchimento = (ListView)item.FindControl("lstEnchimento");
            var lstEmbalagem = (ListView)item.FindControl("lstEmbalagem");
            var lstEntrega = (ListView)item.FindControl("lstEntrega");

            int produtoId = Convert.ToInt32(hdfProdutoId.Value);
            var subProcessos =
                (from c in data.tbJuncaoProdutoProcessoFabricas where c.idProduto == produtoId select c).ToList();

            lstEstoque.DataSource = estoque;
            lstEstoque.DataBind();
            foreach (var itemProcesso in lstEstoque.Items)
            {
                var hdfSubProcesso = (HiddenField)itemProcesso.FindControl("hdfSubProcesso");
                var chkEstoque = (CheckBox)itemProcesso.FindControl("chkEstoque");
                int idSubProcesso = Convert.ToInt32(hdfSubProcesso.Value);
                if (subProcessos.Any(x => x.idSubProcessoFabrica == idSubProcesso)) chkEstoque.Checked = true;
            }

            lstMateria.DataSource = materia;
            lstMateria.DataBind();
            foreach (var itemProcesso in lstMateria.Items)
            {
                var hdfSubProcesso = (HiddenField)itemProcesso.FindControl("hdfSubProcesso");
                var chkMateria = (CheckBox)itemProcesso.FindControl("chkMateria");
                int idSubProcesso = Convert.ToInt32(hdfSubProcesso.Value);
                if (subProcessos.Any(x => x.idSubProcessoFabrica == idSubProcesso)) chkMateria.Checked = true;
            }

            lstCorte.DataSource = corte;
            lstCorte.DataBind();
            foreach (var itemProcesso in lstCorte.Items)
            {
                var hdfSubProcesso = (HiddenField)itemProcesso.FindControl("hdfSubProcesso");
                var chkCorte = (CheckBox)itemProcesso.FindControl("chkCorte");
                int idSubProcesso = Convert.ToInt32(hdfSubProcesso.Value);
                if (subProcessos.Any(x => x.idSubProcessoFabrica == idSubProcesso)) chkCorte.Checked = true;
            }

            lstBordado.DataSource = bordado;
            lstBordado.DataBind();
            foreach (var itemProcesso in lstBordado.Items)
            {
                var hdfSubProcesso = (HiddenField)itemProcesso.FindControl("hdfSubProcesso");
                var chkBordado = (CheckBox)itemProcesso.FindControl("chkBordado");
                int idSubProcesso = Convert.ToInt32(hdfSubProcesso.Value);
                if (subProcessos.Any(x => x.idSubProcessoFabrica == idSubProcesso)) chkBordado.Checked = true;
            }

            lstCostura.DataSource = costura;
            lstCostura.DataBind();
            foreach (var itemProcesso in lstCostura.Items)
            {
                var hdfSubProcesso = (HiddenField)itemProcesso.FindControl("hdfSubProcesso");
                var chkCostura = (CheckBox)itemProcesso.FindControl("chkCostura");
                int idSubProcesso = Convert.ToInt32(hdfSubProcesso.Value);
                if (subProcessos.Any(x => x.idSubProcessoFabrica == idSubProcesso)) chkCostura.Checked = true;
            }

            lstCola.DataSource = cola;
            lstCola.DataBind();
            foreach (var itemProcesso in lstCola.Items)
            {
                var hdfSubProcesso = (HiddenField)itemProcesso.FindControl("hdfSubProcesso");
                var chkCola = (CheckBox)itemProcesso.FindControl("chkCola");
                int idSubProcesso = Convert.ToInt32(hdfSubProcesso.Value);
                if (subProcessos.Any(x => x.idSubProcessoFabrica == idSubProcesso)) chkCola.Checked = true;
            }

            lstArremate.DataSource = arremate;
            lstArremate.DataBind();
            foreach (var itemProcesso in lstArremate.Items)
            {
                var hdfSubProcesso = (HiddenField)itemProcesso.FindControl("hdfSubProcesso");
                var chkArremate = (CheckBox)itemProcesso.FindControl("chkArremate");
                int idSubProcesso = Convert.ToInt32(hdfSubProcesso.Value);
                if (subProcessos.Any(x => x.idSubProcessoFabrica == idSubProcesso)) chkArremate.Checked = true;
            }

            lstEnchimento.DataSource = enchimento;
            lstEnchimento.DataBind();
            foreach (var itemProcesso in lstEnchimento.Items)
            {
                var hdfSubProcesso = (HiddenField)itemProcesso.FindControl("hdfSubProcesso");
                var chkEnchimento = (CheckBox)itemProcesso.FindControl("chkEnchimento");
                int idSubProcesso = Convert.ToInt32(hdfSubProcesso.Value);
                if (subProcessos.Any(x => x.idSubProcessoFabrica == idSubProcesso)) chkEnchimento.Checked = true;
            }

            lstEmbalagem.DataSource = embalagem;
            lstEmbalagem.DataBind();
            foreach (var itemProcesso in lstEmbalagem.Items)
            {
                var hdfSubProcesso = (HiddenField)itemProcesso.FindControl("hdfSubProcesso");
                var chkEmbalagem = (CheckBox)itemProcesso.FindControl("chkEmbalagem");
                int idSubProcesso = Convert.ToInt32(hdfSubProcesso.Value);
                if (subProcessos.Any(x => x.idSubProcessoFabrica == idSubProcesso)) chkEmbalagem.Checked = true;
            }

            lstEntrega.DataSource = entrega;
            lstEntrega.DataBind();
            foreach (var itemProcesso in lstEntrega.Items)
            {
                var hdfSubProcesso = (HiddenField)itemProcesso.FindControl("hdfSubProcesso");
                var chkEntrega = (CheckBox)itemProcesso.FindControl("chkEntrega");
                int idSubProcesso = Convert.ToInt32(hdfSubProcesso.Value);
                if (subProcessos.Any(x => x.idSubProcessoFabrica == idSubProcesso)) chkEntrega.Checked = true;
            }
        }
    }

    //protected void btnGravar_OnClick(object sender, EventArgs e)
    //{
    //    var data = new dbCommerceDataContext();

    //    var subProcessosLista = (from c in data.tbSubProcessoFabricas select c).ToList();
    //    foreach (var item in lstProdutos.Items)
    //    {
    //        var hdfProdutoId = (HiddenField)item.FindControl("hdfProdutoId");
    //        var lstEstoque = (ListView)item.FindControl("lstEstoque");
    //        var lstMateria = (ListView)item.FindControl("lstMateria");
    //        var lstCorte = (ListView)item.FindControl("lstCorte");
    //        var lstBordado = (ListView)item.FindControl("lstBordado");
    //        var lstCostura = (ListView)item.FindControl("lstCostura");
    //        var lstCola = (ListView)item.FindControl("lstCola");
    //        var lstArremate = (ListView)item.FindControl("lstArremate");
    //        var lstEnchimento = (ListView)item.FindControl("lstEnchimento");
    //        var lstEmbalagem = (ListView)item.FindControl("lstEmbalagem");
    //        var lstEntrega = (ListView)item.FindControl("lstEntrega");

    //        int produtoId = Convert.ToInt32(hdfProdutoId.Value);
    //        var subProcessos = (from c in data.tbJuncaoProdutoProcessoFabricas where c.idProduto == produtoId select c).ToList();
    //        lstEstoque.DataBind();
    //        foreach (var itemProcesso in lstEstoque.Items)
    //        {
    //            var hdfSubProcesso = (HiddenField)itemProcesso.FindControl("hdfSubProcesso");
    //            var chkEstoque = (CheckBox)itemProcesso.FindControl("chkEstoque");
    //            int idSubProcesso = Convert.ToInt32(hdfSubProcesso.Value);
    //            if (subProcessos.Any(x => x.idSubProcessoFabrica == idSubProcesso) && chkEstoque.Checked == false)
    //            {
    //                var itemExcluir = (from c in data.tbJuncaoProdutoProcessoFabricas
    //                                   where c.idSubProcessoFabrica == idSubProcesso && c.idProduto == produtoId
    //                                   select c).First();
    //                data.tbJuncaoProdutoProcessoFabricas.DeleteOnSubmit(itemExcluir);
    //                data.SubmitChanges();
    //            }
    //            if (subProcessos.Any(x => x.idSubProcessoFabrica == idSubProcesso) == false && chkEstoque.Checked == true)
    //            {
    //                var itemAdicionar = new tbJuncaoProdutoProcessoFabrica();
    //                itemAdicionar.idProduto = produtoId;
    //                itemAdicionar.idSubProcessoFabrica = idSubProcesso;
    //                itemAdicionar.idProcessoFabrica =
    //                    (from c in subProcessosLista where c.idSubProcessoFabrica == idSubProcesso select c).First()
    //                        .idProcessoFabrica;
    //                data.tbJuncaoProdutoProcessoFabricas.InsertOnSubmit(itemAdicionar);
    //                data.SubmitChanges();
    //            }
    //        }

    //        lstMateria.DataBind();
    //        foreach (var itemProcesso in lstMateria.Items)
    //        {
    //            var hdfSubProcesso = (HiddenField)itemProcesso.FindControl("hdfSubProcesso");
    //            var chkMateria = (CheckBox)itemProcesso.FindControl("chkMateria");
    //            int idSubProcesso = Convert.ToInt32(hdfSubProcesso.Value);
    //            if (subProcessos.Any(x => x.idSubProcessoFabrica == idSubProcesso) && chkMateria.Checked == false)
    //            {
    //                var itemExcluir = (from c in data.tbJuncaoProdutoProcessoFabricas
    //                                   where c.idSubProcessoFabrica == idSubProcesso && c.idProduto == produtoId
    //                                   select c).First();
    //                data.tbJuncaoProdutoProcessoFabricas.DeleteOnSubmit(itemExcluir);
    //                data.SubmitChanges();
    //            }
    //            if (subProcessos.Any(x => x.idSubProcessoFabrica == idSubProcesso) == false && chkMateria.Checked == true)
    //            {
    //                var itemAdicionar = new tbJuncaoProdutoProcessoFabrica();
    //                itemAdicionar.idProduto = produtoId;
    //                itemAdicionar.idSubProcessoFabrica = idSubProcesso;
    //                itemAdicionar.idProcessoFabrica =
    //                    (from c in subProcessosLista where c.idSubProcessoFabrica == idSubProcesso select c).First()
    //                        .idProcessoFabrica;
    //                data.tbJuncaoProdutoProcessoFabricas.InsertOnSubmit(itemAdicionar);
    //                data.SubmitChanges();
    //            }
    //        }

    //        lstCorte.DataBind();
    //        foreach (var itemProcesso in lstCorte.Items)
    //        {
    //            var hdfSubProcesso = (HiddenField)itemProcesso.FindControl("hdfSubProcesso");
    //            var chkCorte = (CheckBox)itemProcesso.FindControl("chkCorte");
    //            int idSubProcesso = Convert.ToInt32(hdfSubProcesso.Value);
    //            if (subProcessos.Any(x => x.idSubProcessoFabrica == idSubProcesso) && chkCorte.Checked == false)
    //            {
    //                var itemExcluir = (from c in data.tbJuncaoProdutoProcessoFabricas
    //                                   where c.idSubProcessoFabrica == idSubProcesso && c.idProduto == produtoId
    //                                   select c).First();
    //                data.tbJuncaoProdutoProcessoFabricas.DeleteOnSubmit(itemExcluir);
    //                data.SubmitChanges();
    //            }
    //            if (subProcessos.Any(x => x.idSubProcessoFabrica == idSubProcesso) == false && chkCorte.Checked == true)
    //            {
    //                var itemAdicionar = new tbJuncaoProdutoProcessoFabrica();
    //                itemAdicionar.idProduto = produtoId;
    //                itemAdicionar.idSubProcessoFabrica = idSubProcesso;
    //                itemAdicionar.idProcessoFabrica =
    //                    (from c in subProcessosLista where c.idSubProcessoFabrica == idSubProcesso select c).First()
    //                        .idProcessoFabrica;
    //                data.tbJuncaoProdutoProcessoFabricas.InsertOnSubmit(itemAdicionar);
    //                data.SubmitChanges();
    //            }
    //        }

    //        lstBordado.DataBind();
    //        foreach (var itemProcesso in lstBordado.Items)
    //        {
    //            var hdfSubProcesso = (HiddenField)itemProcesso.FindControl("hdfSubProcesso");
    //            var chkBordado = (CheckBox)itemProcesso.FindControl("chkBordado");
    //            int idSubProcesso = Convert.ToInt32(hdfSubProcesso.Value);
    //            if (subProcessos.Any(x => x.idSubProcessoFabrica == idSubProcesso) && chkBordado.Checked == false)
    //            {
    //                var itemExcluir = (from c in data.tbJuncaoProdutoProcessoFabricas
    //                                   where c.idSubProcessoFabrica == idSubProcesso && c.idProduto == produtoId
    //                                   select c).First();
    //                data.tbJuncaoProdutoProcessoFabricas.DeleteOnSubmit(itemExcluir);
    //                data.SubmitChanges();
    //            }
    //            if (subProcessos.Any(x => x.idSubProcessoFabrica == idSubProcesso) == false && chkBordado.Checked == true)
    //            {
    //                var itemAdicionar = new tbJuncaoProdutoProcessoFabrica();
    //                itemAdicionar.idProduto = produtoId;
    //                itemAdicionar.idSubProcessoFabrica = idSubProcesso;
    //                itemAdicionar.idProcessoFabrica =
    //                    (from c in subProcessosLista where c.idSubProcessoFabrica == idSubProcesso select c).First()
    //                        .idProcessoFabrica;
    //                data.tbJuncaoProdutoProcessoFabricas.InsertOnSubmit(itemAdicionar);
    //                data.SubmitChanges();
    //            }
    //        }

    //        lstCostura.DataBind();
    //        foreach (var itemProcesso in lstCostura.Items)
    //        {
    //            var hdfSubProcesso = (HiddenField)itemProcesso.FindControl("hdfSubProcesso");
    //            var chkCostura = (CheckBox)itemProcesso.FindControl("chkCostura");
    //            int idSubProcesso = Convert.ToInt32(hdfSubProcesso.Value);
    //            if (subProcessos.Any(x => x.idSubProcessoFabrica == idSubProcesso) && chkCostura.Checked == false)
    //            {
    //                var itemExcluir = (from c in data.tbJuncaoProdutoProcessoFabricas
    //                                   where c.idSubProcessoFabrica == idSubProcesso && c.idProduto == produtoId
    //                                   select c).First();
    //                data.tbJuncaoProdutoProcessoFabricas.DeleteOnSubmit(itemExcluir);
    //                data.SubmitChanges();
    //            }
    //            if (subProcessos.Any(x => x.idSubProcessoFabrica == idSubProcesso) == false && chkCostura.Checked == true)
    //            {
    //                var itemAdicionar = new tbJuncaoProdutoProcessoFabrica();
    //                itemAdicionar.idProduto = produtoId;
    //                itemAdicionar.idSubProcessoFabrica = idSubProcesso;
    //                itemAdicionar.idProcessoFabrica =
    //                    (from c in subProcessosLista where c.idSubProcessoFabrica == idSubProcesso select c).First()
    //                        .idProcessoFabrica;
    //                data.tbJuncaoProdutoProcessoFabricas.InsertOnSubmit(itemAdicionar);
    //                data.SubmitChanges();
    //            }
    //        }

    //        lstCola.DataBind();
    //        foreach (var itemProcesso in lstCola.Items)
    //        {
    //            var hdfSubProcesso = (HiddenField)itemProcesso.FindControl("hdfSubProcesso");
    //            var chkCola = (CheckBox)itemProcesso.FindControl("chkCola");
    //            int idSubProcesso = Convert.ToInt32(hdfSubProcesso.Value);
    //            if (subProcessos.Any(x => x.idSubProcessoFabrica == idSubProcesso) && chkCola.Checked == false)
    //            {
    //                var itemExcluir = (from c in data.tbJuncaoProdutoProcessoFabricas
    //                                   where c.idSubProcessoFabrica == idSubProcesso && c.idProduto == produtoId
    //                                   select c).First();
    //                data.tbJuncaoProdutoProcessoFabricas.DeleteOnSubmit(itemExcluir);
    //                data.SubmitChanges();
    //            }
    //            if (subProcessos.Any(x => x.idSubProcessoFabrica == idSubProcesso) == false && chkCola.Checked == true)
    //            {
    //                var itemAdicionar = new tbJuncaoProdutoProcessoFabrica();
    //                itemAdicionar.idProduto = produtoId;
    //                itemAdicionar.idSubProcessoFabrica = idSubProcesso;
    //                itemAdicionar.idProcessoFabrica =
    //                    (from c in subProcessosLista where c.idSubProcessoFabrica == idSubProcesso select c).First()
    //                        .idProcessoFabrica;
    //                data.tbJuncaoProdutoProcessoFabricas.InsertOnSubmit(itemAdicionar);
    //                data.SubmitChanges();
    //            }
    //        }

    //        lstArremate.DataBind();
    //        foreach (var itemProcesso in lstArremate.Items)
    //        {
    //            var hdfSubProcesso = (HiddenField)itemProcesso.FindControl("hdfSubProcesso");
    //            var chkArremate = (CheckBox)itemProcesso.FindControl("chkArremate");
    //            int idSubProcesso = Convert.ToInt32(hdfSubProcesso.Value);
    //            if (subProcessos.Any(x => x.idSubProcessoFabrica == idSubProcesso) && chkArremate.Checked == false)
    //            {
    //                var itemExcluir = (from c in data.tbJuncaoProdutoProcessoFabricas
    //                                   where c.idSubProcessoFabrica == idSubProcesso && c.idProduto == produtoId
    //                                   select c).First();
    //                data.tbJuncaoProdutoProcessoFabricas.DeleteOnSubmit(itemExcluir);
    //                data.SubmitChanges();
    //            }
    //            if (subProcessos.Any(x => x.idSubProcessoFabrica == idSubProcesso) == false && chkArremate.Checked == true)
    //            {
    //                var itemAdicionar = new tbJuncaoProdutoProcessoFabrica();
    //                itemAdicionar.idProduto = produtoId;
    //                itemAdicionar.idSubProcessoFabrica = idSubProcesso;
    //                itemAdicionar.idProcessoFabrica =
    //                    (from c in subProcessosLista where c.idSubProcessoFabrica == idSubProcesso select c).First()
    //                        .idProcessoFabrica;
    //                data.tbJuncaoProdutoProcessoFabricas.InsertOnSubmit(itemAdicionar);
    //                data.SubmitChanges();
    //            }
    //        }

    //        lstEnchimento.DataBind();
    //        foreach (var itemProcesso in lstEnchimento.Items)
    //        {
    //            var hdfSubProcesso = (HiddenField)itemProcesso.FindControl("hdfSubProcesso");
    //            var chkEnchimento = (CheckBox)itemProcesso.FindControl("chkEnchimento");
    //            int idSubProcesso = Convert.ToInt32(hdfSubProcesso.Value);
    //            if (subProcessos.Any(x => x.idSubProcessoFabrica == idSubProcesso) && chkEnchimento.Checked == false)
    //            {
    //                var itemExcluir = (from c in data.tbJuncaoProdutoProcessoFabricas
    //                                   where c.idSubProcessoFabrica == idSubProcesso && c.idProduto == produtoId
    //                                   select c).First();
    //                data.tbJuncaoProdutoProcessoFabricas.DeleteOnSubmit(itemExcluir);
    //                data.SubmitChanges();
    //            }
    //            if (subProcessos.Any(x => x.idSubProcessoFabrica == idSubProcesso) == false && chkEnchimento.Checked == true)
    //            {
    //                var itemAdicionar = new tbJuncaoProdutoProcessoFabrica();
    //                itemAdicionar.idProduto = produtoId;
    //                itemAdicionar.idSubProcessoFabrica = idSubProcesso;
    //                itemAdicionar.idProcessoFabrica =
    //                    (from c in subProcessosLista where c.idSubProcessoFabrica == idSubProcesso select c).First()
    //                        .idProcessoFabrica;
    //                data.tbJuncaoProdutoProcessoFabricas.InsertOnSubmit(itemAdicionar);
    //                data.SubmitChanges();
    //            }
    //        }

    //        lstEmbalagem.DataBind();
    //        foreach (var itemProcesso in lstEmbalagem.Items)
    //        {
    //            var hdfSubProcesso = (HiddenField)itemProcesso.FindControl("hdfSubProcesso");
    //            var chkEmbalagem = (CheckBox)itemProcesso.FindControl("chkEmbalagem");
    //            int idSubProcesso = Convert.ToInt32(hdfSubProcesso.Value);
    //            if (subProcessos.Any(x => x.idSubProcessoFabrica == idSubProcesso) && chkEmbalagem.Checked == false)
    //            {
    //                var itemExcluir = (from c in data.tbJuncaoProdutoProcessoFabricas
    //                                   where c.idSubProcessoFabrica == idSubProcesso && c.idProduto == produtoId
    //                                   select c).First();
    //                data.tbJuncaoProdutoProcessoFabricas.DeleteOnSubmit(itemExcluir);
    //                data.SubmitChanges();
    //            }
    //            if (subProcessos.Any(x => x.idSubProcessoFabrica == idSubProcesso) == false && chkEmbalagem.Checked == true)
    //            {
    //                var itemAdicionar = new tbJuncaoProdutoProcessoFabrica();
    //                itemAdicionar.idProduto = produtoId;
    //                itemAdicionar.idSubProcessoFabrica = idSubProcesso;
    //                itemAdicionar.idProcessoFabrica =
    //                    (from c in subProcessosLista where c.idSubProcessoFabrica == idSubProcesso select c).First()
    //                        .idProcessoFabrica;
    //                data.tbJuncaoProdutoProcessoFabricas.InsertOnSubmit(itemAdicionar);
    //                data.SubmitChanges();
    //            }
    //        }

    //        lstEntrega.DataBind();
    //        foreach (var itemProcesso in lstEntrega.Items)
    //        {
    //            var hdfSubProcesso = (HiddenField)itemProcesso.FindControl("hdfSubProcesso");
    //            var chkEntrega = (CheckBox)itemProcesso.FindControl("chkEntrega");
    //            int idSubProcesso = Convert.ToInt32(hdfSubProcesso.Value);
    //            if (subProcessos.Any(x => x.idSubProcessoFabrica == idSubProcesso) && chkEntrega.Checked == false)
    //            {
    //                var itemExcluir = (from c in data.tbJuncaoProdutoProcessoFabricas
    //                                   where c.idSubProcessoFabrica == idSubProcesso && c.idProduto == produtoId
    //                                   select c).First();
    //                data.tbJuncaoProdutoProcessoFabricas.DeleteOnSubmit(itemExcluir);
    //                data.SubmitChanges();
    //            }
    //            if (subProcessos.Any(x => x.idSubProcessoFabrica == idSubProcesso) == false && chkEntrega.Checked == true)
    //            {
    //                var itemAdicionar = new tbJuncaoProdutoProcessoFabrica();
    //                itemAdicionar.idProduto = produtoId;
    //                itemAdicionar.idSubProcessoFabrica = idSubProcesso;
    //                itemAdicionar.idProcessoFabrica =
    //                    (from c in subProcessosLista where c.idSubProcessoFabrica == idSubProcesso select c).First()
    //                        .idProcessoFabrica;
    //                data.tbJuncaoProdutoProcessoFabricas.InsertOnSubmit(itemAdicionar);
    //                data.SubmitChanges();
    //            }
    //        }
    //    }

    //    filtrar();
    //}

    protected void btnGravar_OnClick(object sender, EventArgs e)
    {
        var data = new dbCommerceDataContext();

        var subProcessosLista = (from c in data.tbSubProcessoFabricas select c).ToList();
        foreach (GridViewRow item in GridView1.Rows)
        {
            var hdfProdutoId = (HiddenField)item.FindControl("hdfProdutoId");
            var lstEstoque = (ListView)item.FindControl("lstEstoque");
            var lstMateria = (ListView)item.FindControl("lstMateria");
            var lstCorte = (ListView)item.FindControl("lstCorte");
            var lstBordado = (ListView)item.FindControl("lstBordado");
            var lstCostura = (ListView)item.FindControl("lstCostura");
            var lstCola = (ListView)item.FindControl("lstCola");
            var lstArremate = (ListView)item.FindControl("lstArremate");
            var lstEnchimento = (ListView)item.FindControl("lstEnchimento");
            var lstEmbalagem = (ListView)item.FindControl("lstEmbalagem");
            var lstEntrega = (ListView)item.FindControl("lstEntrega");

            int produtoId = Convert.ToInt32(hdfProdutoId.Value);
            var subProcessos = (from c in data.tbJuncaoProdutoProcessoFabricas where c.idProduto == produtoId select c).ToList();
            lstEstoque.DataBind();
            foreach (var itemProcesso in lstEstoque.Items)
            {
                var hdfSubProcesso = (HiddenField)itemProcesso.FindControl("hdfSubProcesso");
                var chkEstoque = (CheckBox)itemProcesso.FindControl("chkEstoque");
                int idSubProcesso = Convert.ToInt32(hdfSubProcesso.Value);
                if (subProcessos.Any(x => x.idSubProcessoFabrica == idSubProcesso) && chkEstoque.Checked == false)
                {
                    var itemExcluir = (from c in data.tbJuncaoProdutoProcessoFabricas
                                       where c.idSubProcessoFabrica == idSubProcesso && c.idProduto == produtoId
                                       select c).First();
                    data.tbJuncaoProdutoProcessoFabricas.DeleteOnSubmit(itemExcluir);
                    data.SubmitChanges();
                }
                if (subProcessos.Any(x => x.idSubProcessoFabrica == idSubProcesso) == false && chkEstoque.Checked == true)
                {
                    var itemAdicionar = new tbJuncaoProdutoProcessoFabrica();
                    itemAdicionar.idProduto = produtoId;
                    itemAdicionar.idSubProcessoFabrica = idSubProcesso;
                    itemAdicionar.idProcessoFabrica =
                        (from c in subProcessosLista where c.idSubProcessoFabrica == idSubProcesso select c).First()
                            .idProcessoFabrica;
                    data.tbJuncaoProdutoProcessoFabricas.InsertOnSubmit(itemAdicionar);
                    data.SubmitChanges();
                }
            }

            lstMateria.DataBind();
            foreach (var itemProcesso in lstMateria.Items)
            {
                var hdfSubProcesso = (HiddenField)itemProcesso.FindControl("hdfSubProcesso");
                var chkMateria = (CheckBox)itemProcesso.FindControl("chkMateria");
                int idSubProcesso = Convert.ToInt32(hdfSubProcesso.Value);
                if (subProcessos.Any(x => x.idSubProcessoFabrica == idSubProcesso) && chkMateria.Checked == false)
                {
                    var itemExcluir = (from c in data.tbJuncaoProdutoProcessoFabricas
                                       where c.idSubProcessoFabrica == idSubProcesso && c.idProduto == produtoId
                                       select c).First();
                    data.tbJuncaoProdutoProcessoFabricas.DeleteOnSubmit(itemExcluir);
                    data.SubmitChanges();
                }
                if (subProcessos.Any(x => x.idSubProcessoFabrica == idSubProcesso) == false && chkMateria.Checked == true)
                {
                    var itemAdicionar = new tbJuncaoProdutoProcessoFabrica();
                    itemAdicionar.idProduto = produtoId;
                    itemAdicionar.idSubProcessoFabrica = idSubProcesso;
                    itemAdicionar.idProcessoFabrica =
                        (from c in subProcessosLista where c.idSubProcessoFabrica == idSubProcesso select c).First()
                            .idProcessoFabrica;
                    data.tbJuncaoProdutoProcessoFabricas.InsertOnSubmit(itemAdicionar);
                    data.SubmitChanges();
                }
            }

            lstCorte.DataBind();
            foreach (var itemProcesso in lstCorte.Items)
            {
                var hdfSubProcesso = (HiddenField)itemProcesso.FindControl("hdfSubProcesso");
                var chkCorte = (CheckBox)itemProcesso.FindControl("chkCorte");
                int idSubProcesso = Convert.ToInt32(hdfSubProcesso.Value);
                if (subProcessos.Any(x => x.idSubProcessoFabrica == idSubProcesso) && chkCorte.Checked == false)
                {
                    var itemExcluir = (from c in data.tbJuncaoProdutoProcessoFabricas
                                       where c.idSubProcessoFabrica == idSubProcesso && c.idProduto == produtoId
                                       select c).First();
                    data.tbJuncaoProdutoProcessoFabricas.DeleteOnSubmit(itemExcluir);
                    data.SubmitChanges();
                }
                if (subProcessos.Any(x => x.idSubProcessoFabrica == idSubProcesso) == false && chkCorte.Checked == true)
                {
                    var itemAdicionar = new tbJuncaoProdutoProcessoFabrica();
                    itemAdicionar.idProduto = produtoId;
                    itemAdicionar.idSubProcessoFabrica = idSubProcesso;
                    itemAdicionar.idProcessoFabrica =
                        (from c in subProcessosLista where c.idSubProcessoFabrica == idSubProcesso select c).First()
                            .idProcessoFabrica;
                    data.tbJuncaoProdutoProcessoFabricas.InsertOnSubmit(itemAdicionar);
                    data.SubmitChanges();
                }
            }

            lstBordado.DataBind();
            foreach (var itemProcesso in lstBordado.Items)
            {
                var hdfSubProcesso = (HiddenField)itemProcesso.FindControl("hdfSubProcesso");
                var chkBordado = (CheckBox)itemProcesso.FindControl("chkBordado");
                int idSubProcesso = Convert.ToInt32(hdfSubProcesso.Value);
                if (subProcessos.Any(x => x.idSubProcessoFabrica == idSubProcesso) && chkBordado.Checked == false)
                {
                    var itemExcluir = (from c in data.tbJuncaoProdutoProcessoFabricas
                                       where c.idSubProcessoFabrica == idSubProcesso && c.idProduto == produtoId
                                       select c).First();
                    data.tbJuncaoProdutoProcessoFabricas.DeleteOnSubmit(itemExcluir);
                    data.SubmitChanges();
                }
                if (subProcessos.Any(x => x.idSubProcessoFabrica == idSubProcesso) == false && chkBordado.Checked == true)
                {
                    var itemAdicionar = new tbJuncaoProdutoProcessoFabrica();
                    itemAdicionar.idProduto = produtoId;
                    itemAdicionar.idSubProcessoFabrica = idSubProcesso;
                    itemAdicionar.idProcessoFabrica =
                        (from c in subProcessosLista where c.idSubProcessoFabrica == idSubProcesso select c).First()
                            .idProcessoFabrica;
                    data.tbJuncaoProdutoProcessoFabricas.InsertOnSubmit(itemAdicionar);
                    data.SubmitChanges();
                }
            }

            lstCostura.DataBind();
            foreach (var itemProcesso in lstCostura.Items)
            {
                var hdfSubProcesso = (HiddenField)itemProcesso.FindControl("hdfSubProcesso");
                var chkCostura = (CheckBox)itemProcesso.FindControl("chkCostura");
                int idSubProcesso = Convert.ToInt32(hdfSubProcesso.Value);
                if (subProcessos.Any(x => x.idSubProcessoFabrica == idSubProcesso) && chkCostura.Checked == false)
                {
                    var itemExcluir = (from c in data.tbJuncaoProdutoProcessoFabricas
                                       where c.idSubProcessoFabrica == idSubProcesso && c.idProduto == produtoId
                                       select c).First();
                    data.tbJuncaoProdutoProcessoFabricas.DeleteOnSubmit(itemExcluir);
                    data.SubmitChanges();
                }
                if (subProcessos.Any(x => x.idSubProcessoFabrica == idSubProcesso) == false && chkCostura.Checked == true)
                {
                    var itemAdicionar = new tbJuncaoProdutoProcessoFabrica();
                    itemAdicionar.idProduto = produtoId;
                    itemAdicionar.idSubProcessoFabrica = idSubProcesso;
                    itemAdicionar.idProcessoFabrica =
                        (from c in subProcessosLista where c.idSubProcessoFabrica == idSubProcesso select c).First()
                            .idProcessoFabrica;
                    data.tbJuncaoProdutoProcessoFabricas.InsertOnSubmit(itemAdicionar);
                    data.SubmitChanges();
                }
            }

            lstCola.DataBind();
            foreach (var itemProcesso in lstCola.Items)
            {
                var hdfSubProcesso = (HiddenField)itemProcesso.FindControl("hdfSubProcesso");
                var chkCola = (CheckBox)itemProcesso.FindControl("chkCola");
                int idSubProcesso = Convert.ToInt32(hdfSubProcesso.Value);
                if (subProcessos.Any(x => x.idSubProcessoFabrica == idSubProcesso) && chkCola.Checked == false)
                {
                    var itemExcluir = (from c in data.tbJuncaoProdutoProcessoFabricas
                                       where c.idSubProcessoFabrica == idSubProcesso && c.idProduto == produtoId
                                       select c).First();
                    data.tbJuncaoProdutoProcessoFabricas.DeleteOnSubmit(itemExcluir);
                    data.SubmitChanges();
                }
                if (subProcessos.Any(x => x.idSubProcessoFabrica == idSubProcesso) == false && chkCola.Checked == true)
                {
                    var itemAdicionar = new tbJuncaoProdutoProcessoFabrica();
                    itemAdicionar.idProduto = produtoId;
                    itemAdicionar.idSubProcessoFabrica = idSubProcesso;
                    itemAdicionar.idProcessoFabrica =
                        (from c in subProcessosLista where c.idSubProcessoFabrica == idSubProcesso select c).First()
                            .idProcessoFabrica;
                    data.tbJuncaoProdutoProcessoFabricas.InsertOnSubmit(itemAdicionar);
                    data.SubmitChanges();
                }
            }

            lstArremate.DataBind();
            foreach (var itemProcesso in lstArremate.Items)
            {
                var hdfSubProcesso = (HiddenField)itemProcesso.FindControl("hdfSubProcesso");
                var chkArremate = (CheckBox)itemProcesso.FindControl("chkArremate");
                int idSubProcesso = Convert.ToInt32(hdfSubProcesso.Value);
                if (subProcessos.Any(x => x.idSubProcessoFabrica == idSubProcesso) && chkArremate.Checked == false)
                {
                    var itemExcluir = (from c in data.tbJuncaoProdutoProcessoFabricas
                                       where c.idSubProcessoFabrica == idSubProcesso && c.idProduto == produtoId
                                       select c).First();
                    data.tbJuncaoProdutoProcessoFabricas.DeleteOnSubmit(itemExcluir);
                    data.SubmitChanges();
                }
                if (subProcessos.Any(x => x.idSubProcessoFabrica == idSubProcesso) == false && chkArremate.Checked == true)
                {
                    var itemAdicionar = new tbJuncaoProdutoProcessoFabrica();
                    itemAdicionar.idProduto = produtoId;
                    itemAdicionar.idSubProcessoFabrica = idSubProcesso;
                    itemAdicionar.idProcessoFabrica =
                        (from c in subProcessosLista where c.idSubProcessoFabrica == idSubProcesso select c).First()
                            .idProcessoFabrica;
                    data.tbJuncaoProdutoProcessoFabricas.InsertOnSubmit(itemAdicionar);
                    data.SubmitChanges();
                }
            }

            lstEnchimento.DataBind();
            foreach (var itemProcesso in lstEnchimento.Items)
            {
                var hdfSubProcesso = (HiddenField)itemProcesso.FindControl("hdfSubProcesso");
                var chkEnchimento = (CheckBox)itemProcesso.FindControl("chkEnchimento");
                int idSubProcesso = Convert.ToInt32(hdfSubProcesso.Value);
                if (subProcessos.Any(x => x.idSubProcessoFabrica == idSubProcesso) && chkEnchimento.Checked == false)
                {
                    var itemExcluir = (from c in data.tbJuncaoProdutoProcessoFabricas
                                       where c.idSubProcessoFabrica == idSubProcesso && c.idProduto == produtoId
                                       select c).First();
                    data.tbJuncaoProdutoProcessoFabricas.DeleteOnSubmit(itemExcluir);
                    data.SubmitChanges();
                }
                if (subProcessos.Any(x => x.idSubProcessoFabrica == idSubProcesso) == false && chkEnchimento.Checked == true)
                {
                    var itemAdicionar = new tbJuncaoProdutoProcessoFabrica();
                    itemAdicionar.idProduto = produtoId;
                    itemAdicionar.idSubProcessoFabrica = idSubProcesso;
                    itemAdicionar.idProcessoFabrica =
                        (from c in subProcessosLista where c.idSubProcessoFabrica == idSubProcesso select c).First()
                            .idProcessoFabrica;
                    data.tbJuncaoProdutoProcessoFabricas.InsertOnSubmit(itemAdicionar);
                    data.SubmitChanges();
                }
            }

            lstEmbalagem.DataBind();
            foreach (var itemProcesso in lstEmbalagem.Items)
            {
                var hdfSubProcesso = (HiddenField)itemProcesso.FindControl("hdfSubProcesso");
                var chkEmbalagem = (CheckBox)itemProcesso.FindControl("chkEmbalagem");
                int idSubProcesso = Convert.ToInt32(hdfSubProcesso.Value);
                if (subProcessos.Any(x => x.idSubProcessoFabrica == idSubProcesso) && chkEmbalagem.Checked == false)
                {
                    var itemExcluir = (from c in data.tbJuncaoProdutoProcessoFabricas
                                       where c.idSubProcessoFabrica == idSubProcesso && c.idProduto == produtoId
                                       select c).First();
                    data.tbJuncaoProdutoProcessoFabricas.DeleteOnSubmit(itemExcluir);
                    data.SubmitChanges();
                }
                if (subProcessos.Any(x => x.idSubProcessoFabrica == idSubProcesso) == false && chkEmbalagem.Checked == true)
                {
                    var itemAdicionar = new tbJuncaoProdutoProcessoFabrica();
                    itemAdicionar.idProduto = produtoId;
                    itemAdicionar.idSubProcessoFabrica = idSubProcesso;
                    itemAdicionar.idProcessoFabrica =
                        (from c in subProcessosLista where c.idSubProcessoFabrica == idSubProcesso select c).First()
                            .idProcessoFabrica;
                    data.tbJuncaoProdutoProcessoFabricas.InsertOnSubmit(itemAdicionar);
                    data.SubmitChanges();
                }
            }

            lstEntrega.DataBind();
            foreach (var itemProcesso in lstEntrega.Items)
            {
                var hdfSubProcesso = (HiddenField)itemProcesso.FindControl("hdfSubProcesso");
                var chkEntrega = (CheckBox)itemProcesso.FindControl("chkEntrega");
                int idSubProcesso = Convert.ToInt32(hdfSubProcesso.Value);
                if (subProcessos.Any(x => x.idSubProcessoFabrica == idSubProcesso) && chkEntrega.Checked == false)
                {
                    var itemExcluir = (from c in data.tbJuncaoProdutoProcessoFabricas
                                       where c.idSubProcessoFabrica == idSubProcesso && c.idProduto == produtoId
                                       select c).First();
                    data.tbJuncaoProdutoProcessoFabricas.DeleteOnSubmit(itemExcluir);
                    data.SubmitChanges();
                }
                if (subProcessos.Any(x => x.idSubProcessoFabrica == idSubProcesso) == false && chkEntrega.Checked == true)
                {
                    var itemAdicionar = new tbJuncaoProdutoProcessoFabrica();
                    itemAdicionar.idProduto = produtoId;
                    itemAdicionar.idSubProcessoFabrica = idSubProcesso;
                    itemAdicionar.idProcessoFabrica =
                        (from c in subProcessosLista where c.idSubProcessoFabrica == idSubProcesso select c).First()
                            .idProcessoFabrica;
                    data.tbJuncaoProdutoProcessoFabricas.InsertOnSubmit(itemAdicionar);
                    data.SubmitChanges();
                }
            }
        }

        //filtrar();
        FiltrarCarregaGrid();
    }

    protected void btnChecar_OnCommand(object sender, CommandEventArgs e)
    {
        int subProcesso = Convert.ToInt32(e.CommandArgument);

        foreach (var item in lstProdutos.Items)
        {
            var hdfProdutoId = (HiddenField)item.FindControl("hdfProdutoId");
            var lstEstoque = (ListView)item.FindControl("lstEstoque");
            var lstMateria = (ListView)item.FindControl("lstMateria");
            var lstCorte = (ListView)item.FindControl("lstCorte");
            var lstBordado = (ListView)item.FindControl("lstBordado");
            var lstCostura = (ListView)item.FindControl("lstCostura");
            var lstCola = (ListView)item.FindControl("lstCola");
            var lstArremate = (ListView)item.FindControl("lstArremate");
            var lstEnchimento = (ListView)item.FindControl("lstEnchimento");
            var lstEmbalagem = (ListView)item.FindControl("lstEmbalagem");
            var lstEntrega = (ListView)item.FindControl("lstEntrega");

            foreach (var itemProcesso in lstEstoque.Items)
            {
                var hdfSubProcesso = (HiddenField)itemProcesso.FindControl("hdfSubProcesso");
                var chkEstoque = (CheckBox)itemProcesso.FindControl("chkEstoque");
                int idSubProcesso = Convert.ToInt32(hdfSubProcesso.Value);
                if (idSubProcesso == subProcesso)
                {
                    chkEstoque.Checked = true;
                }
            }

            foreach (var itemProcesso in lstMateria.Items)
            {
                var hdfSubProcesso = (HiddenField)itemProcesso.FindControl("hdfSubProcesso");
                var chkMateria = (CheckBox)itemProcesso.FindControl("chkMateria");
                int idSubProcesso = Convert.ToInt32(hdfSubProcesso.Value);
                if (idSubProcesso == subProcesso)
                {
                    chkMateria.Checked = true;
                }
            }

            foreach (var itemProcesso in lstCorte.Items)
            {
                var hdfSubProcesso = (HiddenField)itemProcesso.FindControl("hdfSubProcesso");
                var chkCorte = (CheckBox)itemProcesso.FindControl("chkCorte");
                int idSubProcesso = Convert.ToInt32(hdfSubProcesso.Value);
                if (idSubProcesso == subProcesso)
                {
                    chkCorte.Checked = true;
                }
            }

            foreach (var itemProcesso in lstBordado.Items)
            {
                var hdfSubProcesso = (HiddenField)itemProcesso.FindControl("hdfSubProcesso");
                var chkBordado = (CheckBox)itemProcesso.FindControl("chkBordado");
                int idSubProcesso = Convert.ToInt32(hdfSubProcesso.Value);
                if (idSubProcesso == subProcesso)
                {
                    chkBordado.Checked = true;
                }
            }

            foreach (var itemProcesso in lstCostura.Items)
            {
                var hdfSubProcesso = (HiddenField)itemProcesso.FindControl("hdfSubProcesso");
                var chkCostura = (CheckBox)itemProcesso.FindControl("chkCostura");
                int idSubProcesso = Convert.ToInt32(hdfSubProcesso.Value);
                if (idSubProcesso == subProcesso)
                {
                    chkCostura.Checked = true;
                }
            }

            foreach (var itemProcesso in lstCola.Items)
            {
                var hdfSubProcesso = (HiddenField)itemProcesso.FindControl("hdfSubProcesso");
                var chkCola = (CheckBox)itemProcesso.FindControl("chkCola");
                int idSubProcesso = Convert.ToInt32(hdfSubProcesso.Value);
                if (idSubProcesso == subProcesso)
                {
                    chkCola.Checked = true;
                }
            }

            foreach (var itemProcesso in lstArremate.Items)
            {
                var hdfSubProcesso = (HiddenField)itemProcesso.FindControl("hdfSubProcesso");
                var chkArremate = (CheckBox)itemProcesso.FindControl("chkArremate");
                int idSubProcesso = Convert.ToInt32(hdfSubProcesso.Value);
                if (idSubProcesso == subProcesso)
                {
                    chkArremate.Checked = true;
                }
            }

            foreach (var itemProcesso in lstEnchimento.Items)
            {
                var hdfSubProcesso = (HiddenField)itemProcesso.FindControl("hdfSubProcesso");
                var chkEnchimento = (CheckBox)itemProcesso.FindControl("chkEnchimento");
                int idSubProcesso = Convert.ToInt32(hdfSubProcesso.Value);
                if (idSubProcesso == subProcesso)
                {
                    chkEnchimento.Checked = true;
                }
            }

            foreach (var itemProcesso in lstEmbalagem.Items)
            {
                var hdfSubProcesso = (HiddenField)itemProcesso.FindControl("hdfSubProcesso");
                var chkEmbalagem = (CheckBox)itemProcesso.FindControl("chkEmbalagem");
                int idSubProcesso = Convert.ToInt32(hdfSubProcesso.Value);
                if (idSubProcesso == subProcesso)
                {
                    chkEmbalagem.Checked = true;
                }
            }

            foreach (var itemProcesso in lstEntrega.Items)
            {
                var hdfSubProcesso = (HiddenField)itemProcesso.FindControl("hdfSubProcesso");
                var chkEntrega = (CheckBox)itemProcesso.FindControl("chkEntrega");
                int idSubProcesso = Convert.ToInt32(hdfSubProcesso.Value);
                if (idSubProcesso == subProcesso)
                {
                    chkEntrega.Checked = true;
                }
            }
        }
    }

    protected void GridView1_OnDataBound(object sender, EventArgs e)
    {
        var data = new dbCommerceDataContext();

        var estoque = (from c in data.tbSubProcessoFabricas where c.idProcessoFabrica == 7 orderby c.nome select c);
        var materia = (from c in data.tbSubProcessoFabricas where c.idProcessoFabrica == 8 orderby c.nome select c);
        var corte = (from c in data.tbSubProcessoFabricas where c.idProcessoFabrica == 1 orderby c.nome select c);
        var bordado = (from c in data.tbSubProcessoFabricas where c.idProcessoFabrica == 2 orderby c.nome select c);
        var costura = (from c in data.tbSubProcessoFabricas where c.idProcessoFabrica == 3 && c.idSubProcessoFabrica != 10 orderby c.nome select c);
        var cola = (from c in data.tbSubProcessoFabricas where c.idProcessoFabrica == 4 orderby c.nome select c);
        var arremate = (from c in data.tbSubProcessoFabricas where c.idProcessoFabrica == 9 orderby c.nome select c);
        var enchimento = (from c in data.tbSubProcessoFabricas where c.idProcessoFabrica == 10 orderby c.nome select c);
        var embalagem = (from c in data.tbSubProcessoFabricas where c.idProcessoFabrica == 5 orderby c.nome select c);
        var entrega = (from c in data.tbSubProcessoFabricas where c.idProcessoFabrica == 6 orderby c.nome select c);


        lstEstoqueC.DataSource = estoque;
        lstEstoqueC.DataBind();

        lstMateriaC.DataSource = materia;
        lstMateriaC.DataBind();

        lstCorteC.DataSource = corte;
        lstCorteC.DataBind();

        lstBordadoC.DataSource = bordado;
        lstBordadoC.DataBind();

        lstCosturaC.DataSource = costura;
        lstCosturaC.DataBind();

        lstColaC.DataSource = cola;
        lstColaC.DataBind();

        lstArremateC.DataSource = arremate;
        lstArremateC.DataBind();

        lstEnchimentoC.DataSource = enchimento;
        lstEnchimentoC.DataBind();

        lstEmbalagemC.DataSource = embalagem;
        lstEmbalagemC.DataBind();

        lstEntregaC.DataSource = entrega;
        lstEntregaC.DataBind();

        foreach (GridViewRow item in GridView1.Rows)
        {
            var hdfProdutoId = (HiddenField)item.FindControl("hdfProdutoId");
            var lstEstoque = (ListView)item.FindControl("lstEstoque");
            var lstMateria = (ListView)item.FindControl("lstMateria");
            var lstCorte = (ListView)item.FindControl("lstCorte");
            var lstBordado = (ListView)item.FindControl("lstBordado");
            var lstCostura = (ListView)item.FindControl("lstCostura");
            var lstCola = (ListView)item.FindControl("lstCola");
            var lstArremate = (ListView)item.FindControl("lstArremate");
            var lstEnchimento = (ListView)item.FindControl("lstEnchimento");
            var lstEmbalagem = (ListView)item.FindControl("lstEmbalagem");
            var lstEntrega = (ListView)item.FindControl("lstEntrega");

            int produtoId = Convert.ToInt32(hdfProdutoId.Value);
            var subProcessos =
                (from c in data.tbJuncaoProdutoProcessoFabricas where c.idProduto == produtoId select c).ToList();

            lstEstoque.DataSource = estoque;
            lstEstoque.DataBind();
            foreach (var itemProcesso in lstEstoque.Items)
            {
                var hdfSubProcesso = (HiddenField)itemProcesso.FindControl("hdfSubProcesso");
                var chkEstoque = (CheckBox)itemProcesso.FindControl("chkEstoque");
                int idSubProcesso = Convert.ToInt32(hdfSubProcesso.Value);
                if (subProcessos.Any(x => x.idSubProcessoFabrica == idSubProcesso)) chkEstoque.Checked = true;
            }

            lstMateria.DataSource = materia;
            lstMateria.DataBind();
            foreach (var itemProcesso in lstMateria.Items)
            {
                var hdfSubProcesso = (HiddenField)itemProcesso.FindControl("hdfSubProcesso");
                var chkMateria = (CheckBox)itemProcesso.FindControl("chkMateria");
                int idSubProcesso = Convert.ToInt32(hdfSubProcesso.Value);
                if (subProcessos.Any(x => x.idSubProcessoFabrica == idSubProcesso)) chkMateria.Checked = true;
            }

            lstCorte.DataSource = corte;
            lstCorte.DataBind();
            foreach (var itemProcesso in lstCorte.Items)
            {
                var hdfSubProcesso = (HiddenField)itemProcesso.FindControl("hdfSubProcesso");
                var chkCorte = (CheckBox)itemProcesso.FindControl("chkCorte");
                int idSubProcesso = Convert.ToInt32(hdfSubProcesso.Value);
                if (subProcessos.Any(x => x.idSubProcessoFabrica == idSubProcesso)) chkCorte.Checked = true;
            }

            lstBordado.DataSource = bordado;
            lstBordado.DataBind();
            foreach (var itemProcesso in lstBordado.Items)
            {
                var hdfSubProcesso = (HiddenField)itemProcesso.FindControl("hdfSubProcesso");
                var chkBordado = (CheckBox)itemProcesso.FindControl("chkBordado");
                int idSubProcesso = Convert.ToInt32(hdfSubProcesso.Value);
                if (subProcessos.Any(x => x.idSubProcessoFabrica == idSubProcesso)) chkBordado.Checked = true;
            }

            lstCostura.DataSource = costura;
            lstCostura.DataBind();
            foreach (var itemProcesso in lstCostura.Items)
            {
                var hdfSubProcesso = (HiddenField)itemProcesso.FindControl("hdfSubProcesso");
                var chkCostura = (CheckBox)itemProcesso.FindControl("chkCostura");
                int idSubProcesso = Convert.ToInt32(hdfSubProcesso.Value);
                if (subProcessos.Any(x => x.idSubProcessoFabrica == idSubProcesso)) chkCostura.Checked = true;
            }

            lstCola.DataSource = cola;
            lstCola.DataBind();
            foreach (var itemProcesso in lstCola.Items)
            {
                var hdfSubProcesso = (HiddenField)itemProcesso.FindControl("hdfSubProcesso");
                var chkCola = (CheckBox)itemProcesso.FindControl("chkCola");
                int idSubProcesso = Convert.ToInt32(hdfSubProcesso.Value);
                if (subProcessos.Any(x => x.idSubProcessoFabrica == idSubProcesso)) chkCola.Checked = true;
            }

            lstArremate.DataSource = arremate;
            lstArremate.DataBind();
            foreach (var itemProcesso in lstArremate.Items)
            {
                var hdfSubProcesso = (HiddenField)itemProcesso.FindControl("hdfSubProcesso");
                var chkArremate = (CheckBox)itemProcesso.FindControl("chkArremate");
                int idSubProcesso = Convert.ToInt32(hdfSubProcesso.Value);
                if (subProcessos.Any(x => x.idSubProcessoFabrica == idSubProcesso)) chkArremate.Checked = true;
            }

            lstEnchimento.DataSource = enchimento;
            lstEnchimento.DataBind();
            foreach (var itemProcesso in lstEnchimento.Items)
            {
                var hdfSubProcesso = (HiddenField)itemProcesso.FindControl("hdfSubProcesso");
                var chkEnchimento = (CheckBox)itemProcesso.FindControl("chkEnchimento");
                int idSubProcesso = Convert.ToInt32(hdfSubProcesso.Value);
                if (subProcessos.Any(x => x.idSubProcessoFabrica == idSubProcesso)) chkEnchimento.Checked = true;
            }

            lstEmbalagem.DataSource = embalagem;
            lstEmbalagem.DataBind();
            foreach (var itemProcesso in lstEmbalagem.Items)
            {
                var hdfSubProcesso = (HiddenField)itemProcesso.FindControl("hdfSubProcesso");
                var chkEmbalagem = (CheckBox)itemProcesso.FindControl("chkEmbalagem");
                int idSubProcesso = Convert.ToInt32(hdfSubProcesso.Value);
                if (subProcessos.Any(x => x.idSubProcessoFabrica == idSubProcesso)) chkEmbalagem.Checked = true;
            }

            lstEntrega.DataSource = entrega;
            lstEntrega.DataBind();
            foreach (var itemProcesso in lstEntrega.Items)
            {
                var hdfSubProcesso = (HiddenField)itemProcesso.FindControl("hdfSubProcesso");
                var chkEntrega = (CheckBox)itemProcesso.FindControl("chkEntrega");
                int idSubProcesso = Convert.ToInt32(hdfSubProcesso.Value);
                if (subProcessos.Any(x => x.idSubProcessoFabrica == idSubProcesso)) chkEntrega.Checked = true;
            }
        }
    }

    protected void GridView1_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            var hdfProdutoId = (HiddenField)e.Row.FindControl("hdfProdutoId");
            var hdfFoto = (HiddenField)e.Row.FindControl("hdfFoto");
            var imgFoto = (Image)e.Row.FindControl("imgFoto");

            if (string.IsNullOrEmpty(hdfFoto.Value))
                imgFoto.ImageUrl = "images/produto-sem-foto.jpg";
            else
                imgFoto.ImageUrl = "http://dmhxz00kguanp.cloudfront.net/fotos/" + hdfProdutoId.Value + "/" +
                                   hdfFoto.Value + ".jpg";
        }

    }

    protected void GridView1_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;

        FiltrarCarregaGrid();
    }
}