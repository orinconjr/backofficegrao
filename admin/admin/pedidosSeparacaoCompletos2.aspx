﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" Theme="Glass" AutoEventWireup="true" CodeFile="pedidosSeparacaoCompletos2.aspx.cs" Inherits="admin_pedidosSeparacaoCompletos2" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxTimer" TagPrefix="dxt" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" Runat="Server">
    <table cellpadding="0" cellspacing="0" style="width: 920px">
    <tr>
        <td class="tituloPaginas" valign="top">
            <asp:Label ID="lblAcao" runat="server">Pedidos Completos</asp:Label>
        </td>
    </tr>
        
    <tr>
        <td>
            
    <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
        <tr>
            <td style="text-align: right;">
                <asp:Button runat="server" ID="btnAlterarStatus" Text="Alterar pedidos para Sendo Embalado" OnClick="btnAlterarStatus_OnClick" ValidationGroup="validaEntrega"/>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>

        <tr>
            <td>
                <dxwgv:ASPxGridView ID="grd" runat="server" AutoGenerateColumns="False" Width="834px" Cursor="auto" OnHtmlRowCreated="gridClientes_HtmlRowCreated">
                    <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                    <Styles>
                        <Footer Font-Bold="True">
                        </Footer>
                    </Styles>
                    <SettingsText ConfirmDelete="Tem certeza que deseja excluir?" emptydatarow="Nenhum registro encontrado." />
                    <SettingsPager Position="TopAndBottom" PageSize="50" ShowDisabledButtons="False" AlwaysShowPager="True">
                        <Summary AllPagesText="Páginas: {0} - {1} ({2} items)" Text="Página {0} de {1} ({2} registros encontrados)" />
                    </SettingsPager>
                    <settings showfilterrow="True" ShowGroupButtons="False" ShowHeaderFilterButton="True" ShowFooter="True" />
                    <SettingsEditing EditFormColumnCount="4" PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1" PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter" PopupEditFormWidth="700px" />
                    <Columns>
                        <dxwgv:GridViewDataTextColumn Caption="ID" FieldName="pedidoId" VisibleIndex="0" Width="50">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataDateColumn Caption="Cidade" FieldName="endCidade" VisibleIndex="0">
                        </dxwgv:GridViewDataDateColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Estado" FieldName="endEstado" VisibleIndex="0" Width="60">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="CEP" FieldName="endCep" VisibleIndex="0">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Entrega" FieldName="tipoDeEntregaNome" VisibleIndex="0">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Valor da Entrega" FieldName="valorDoFrete" VisibleIndex="0">
                            <PropertiesTextEdit DisplayFormatString="c">
                            </PropertiesTextEdit>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Enviar por" FieldName="enviarPor" VisibleIndex="0">
                            <Settings AutoFilterCondition="Contains" />
                            <DataItemTemplate>
                                <asp:TextBox runat="server" ID="txtEnviarPor" TextMode="MultiLine" Width="200" Height="60"></asp:TextBox>
                            </DataItemTemplate>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataHyperLinkColumn Caption="Editar" FieldName="pedidoId" 
                            VisibleIndex="7" Width="30px">
                            <PropertiesHyperLinkEdit ImageUrl="~/admin/images/btEditar.jpg" 
                                NavigateUrlFormatString="pedido.aspx?pedidoId={0}" Target="_blank">
                            </PropertiesHyperLinkEdit>
                            <Settings AllowAutoFilter="False" />
                            <HeaderTemplate>
                                <img alt="" src="images/legendaEditar.jpg" />
                            </HeaderTemplate>
                        </dxwgv:GridViewDataHyperLinkColumn>
                    </Columns>
                    <StylesEditors>
                        <Label Font-Bold="True">
                        </Label>
                    </StylesEditors>
                </dxwgv:ASPxGridView>            
                <asp:ObjectDataSource ID="sql" runat="server" SelectMethod="admin_pedidosCompletosSeparacaoEstoque2" TypeName="rnPedidos">
                </asp:ObjectDataSource>        
            </td>
        </tr>
        <tr><td>
                &nbsp;
            </td></tr>
        <tr>
            <td style="text-align: right">
                <table>
                    <tr>
                        <td>
                            <asp:HyperLink ID="hplGerarEtiqueta" runat="server" ImageUrl="~/admin/images/btGerarEtiqueta.jpg" NavigateUrl='etiquetaLote2.aspx' Target="_blank"></asp:HyperLink>            
                        </td>
                        <td>
                            <asp:ImageButton ID="btnGravar" runat="server" ImageAlign="AbsMiddle" ImageUrl="images/btSalvarPeq1.jpg" onclick="btnGravar_OnClick" ValidationGroup="validaEntrega" />
                        </td>
                    </tr>
                </table>
                <asp:ValidationSummary ID="vlds" runat="server" ShowMessageBox="True" ShowSummary="False" ValidationGroup="validaEntrega" DisplayMode="List" />
            </td>
        </tr>
    </table>
        </td>
    </tr>
</table>
    </asp:Content>