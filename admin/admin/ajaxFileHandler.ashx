﻿<%@ WebHandler Language="C#" Class="ajaxFileHandler" %>

using System;
using System.Web;
using System.IO;
using System.Configuration;
using System.Drawing.Imaging;
using Amazon.S3;
using Amazon.S3.Model;
using System.Data;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

public class ajaxFileHandler : IHttpHandler
{

    public void ProcessRequest(HttpContext context)
    {
        if (context.Request.Files.Count > 0)
        {
            int produtoId = Int32.TryParse(context.Request["produtoId"], out produtoId) ? produtoId : 0;
            var data = new dbCommerceDataContext();
            var produtoDetalhe = (from c in data.tbProdutos where c.produtoId == produtoId select c).First();

            string Location;
            string Target;

            for (int i = 0; i < context.Request.Files.Count; i++)
            {
                var file = context.Request.Files[i];
                //file.SaveAs("");

                #region Recupero o produtoFotoId
                int fotoId;
                if (rnFotos.produtoFotoSelecionaUltimoId().Tables[0].Rows.Count > 0)
                    fotoId = int.Parse(rnFotos.produtoFotoSelecionaUltimoId().Tables[0].Rows[0]["produtoFotoId"].ToString()) + 1;
                else
                    fotoId = 0;
                #endregion


                //string fotoRenomeada = fotoId + ".jpg";
                string fotoRenomeada = rnFuncoes.limpaString(produtoDetalhe.produtoNome.Trim()).ToLower() + "-" + fotoId;
                string fotoNomeProduto = fotoRenomeada + ".jpg";

                //Salva foto no banco
                rnFotos.fotosInclui(produtoId, fotoRenomeada.ToString(), "");

                if (!Directory.Exists(ConfigurationManager.AppSettings["caminhoFisico"] + "fotos\\" + produtoId + "\\"))//se não existir o diretório então cria
                    Directory.CreateDirectory(ConfigurationManager.AppSettings["caminhoFisico"] + "fotos\\" + produtoId + "\\");


                //Salva foto no disco
                string caminhoFoto = ConfigurationManager.AppSettings["caminhoFisico"] + "fotos\\" + produtoId + "\\";
                file.SaveAs(caminhoFoto + "original_" + fotoNomeProduto);

                var agora = DateTime.Now;
                var queueCompactarFoto = new tbQueue
                {
                    tipoQueue = 26,
                    agendamento = agora,
                    idRelacionado = produtoId,
                    mensagem = "",
                    concluido = false,
                    andamento = false
                };
                data.tbQueues.InsertOnSubmit(queueCompactarFoto);
                data.SubmitChanges();
                
                /*var tinify = new Tinify();
                var fotoOtimizada = tinify.Shrink(caminhoFoto + "original_" + fotoNomeProduto, caminhoFoto + fotoNomeProduto);



                gerarFotoWebP(caminhoFoto, fotoNomeProduto, produtoId);

                Location = caminhoFoto + fotoNomeProduto;
                uploadImagemAmazonS3(fotoNomeProduto, produtoId);

                //Cria a foto média
                tinify.Cover(fotoOtimizada, int.Parse(ConfigurationManager.AppSettings["fotoMedia"]), int.Parse(ConfigurationManager.AppSettings["fotoMedia"]), caminhoFoto + "media_" + fotoNomeProduto);
                uploadImagemAmazonS3("media_" + fotoNomeProduto, produtoId);
                /*Target = caminhoFoto + "media_" + fotoNomeProduto;
                System.Threading.Thread.Sleep(500);
                rnFotos.ResizeImageFile(int.Parse(ConfigurationManager.AppSettings["fotoMedia"]), Location, Target, ImageFormat.Jpeg);*/
                /*gerarFotoWebP(caminhoFoto, "media_" + fotoNomeProduto, produtoId);

                //Cria a foto pequena
                tinify.Cover(fotoOtimizada, int.Parse(ConfigurationManager.AppSettings["fotoPequena"]), int.Parse(ConfigurationManager.AppSettings["fotoPequena"]), caminhoFoto + "pequena_" + fotoNomeProduto);
                uploadImagemAmazonS3("pequena_" + fotoNomeProduto, produtoId);
                /*Target = caminhoFoto + "pequena_" + fotoNomeProduto;
                System.Threading.Thread.Sleep(500);
                rnFotos.ResizeImageFile(int.Parse(ConfigurationManager.AppSettings["fotoPequena"]), Location, Target, ImageFormat.Jpeg);*/
                /*gerarFotoWebP(caminhoFoto, "pequena_" + fotoNomeProduto, produtoId);

                //Cria a foto 500 mobile
                tinify.Cover(fotoOtimizada, 500, 500, caminhoFoto + "500_" + fotoNomeProduto);
                uploadImagemAmazonS3("500_" + fotoNomeProduto, produtoId);
                /*Target = caminhoFoto + "500_" + fotoNomeProduto;
                System.Threading.Thread.Sleep(500);
                rnFotos.ResizeImageFile(500, Location, Target, ImageFormat.Jpeg);*/
                /*gerarFotoWebP(caminhoFoto, "500_" + fotoNomeProduto, produtoId);

                //Cria a foto 125 mobile
                tinify.Cover(fotoOtimizada, 125, 125, caminhoFoto + "125_" + fotoNomeProduto);
                uploadImagemAmazonS3("125_" + fotoNomeProduto, produtoId);
                /*Target = caminhoFoto + "125_" + fotoNomeProduto;
                System.Threading.Thread.Sleep(500);
                rnFotos.ResizeImageFile(125, Location, Target, ImageFormat.Jpeg);*/
                /*gerarFotoWebP(caminhoFoto, "125_" + fotoNomeProduto, produtoId);*/

                var contagemFotos = (from c in data.tbProdutoFotos where c.produtoId == produtoId select c).Count();
                if(contagemFotos == 1)
                {
                    produtoDetalhe.fotoDestaque = fotoNomeProduto;
                    data.SubmitChanges();
                }


                /*using (var data = new dbCommerceDataContext())
                {
                    var queueCompactarFoto = new tbQueue
                    {
                        tipoQueue = 26,
                        agendamento = DateTime.Now.AddMinutes(5),
                        idRelacionado = fotoId,
                        mensagem = "",
                        concluido = false,
                        andamento = false
                    };
                    data.tbQueues.InsertOnSubmit(queueCompactarFoto);
                    data.SubmitChanges();
                }*/

                context.Response.ContentType = "text/plain";
                var serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                var result = new { name = fotoRenomeada };
                context.Response.Write(serializer.Serialize(result));

            }

        }

    }



    private void gerarFotoWebP(string caminhoFoto, string nomeArquivo, int produtoId)
    {
        bool webpCarregado = true;
        try
        {
            Imazen.WebP.Extern.LoadLibrary.LoadWebPOrFail();
        }
        catch (Exception ex)
        {
            webpCarregado = false;
        }

        if (webpCarregado)
        {
            try
            {
                string nomeArquivoWebp = nomeArquivo.Split('.')[0] + ".webp";
                System.Drawing.Bitmap mBitmap;
                FileStream outStream = new FileStream(caminhoFoto + nomeArquivoWebp, FileMode.Create);
                using (Stream BitmapStream = System.IO.File.Open(caminhoFoto + nomeArquivo, System.IO.FileMode.Open))
                {
                    System.Drawing.Image img = System.Drawing.Image.FromStream(BitmapStream);

                    mBitmap = new System.Drawing.Bitmap(img);
                    var encoder = new Imazen.WebP.SimpleEncoder();
                    encoder.Encode(mBitmap, outStream, 70);
                    outStream.Close();
                    uploadImagemAmazonS3(nomeArquivoWebp, produtoId);
                }
            }
            catch (Exception ex) { }
        }
    }


    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

    private void uploadImagemAmazonS3(string foto, int produtoId)
    {

        string s3ServiceUrl = "s3-sa-east-1.amazonaws.com";
        string bucketName = "cdn2.graodegente.com.br";
        string filePath = ConfigurationManager.AppSettings["caminhoFisico"] + "fotos\\" + produtoId + "\\" + foto;
        string fileContentType = "image/jpg";
        S3CannedACL fileCannedACL = S3CannedACL.PublicReadWrite;

        string accessKey = "AKIAIP7IE6WVNK2K6TIQ";
        string secretAccessKey = "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";

        try
        {
            AmazonS3Config config = new AmazonS3Config();
            config.ServiceURL = s3ServiceUrl;
            config.CommunicationProtocol = Protocol.HTTP;

            using (var s3Client = Amazon.AWSClientFactory.CreateAmazonS3Client(accessKey, secretAccessKey, config))
            {
                PutObjectRequest request = new PutObjectRequest
                {
                    BucketName = bucketName,
                    CannedACL = fileCannedACL,
                    Key = "fotos/" + produtoId + "/" + foto,
                    FilePath = filePath,
                    ContentType = fileContentType
                };

                PutObjectResponse response = s3Client.PutObject(request);

            }

        }
        catch (AmazonS3Exception s3Exception)
        {
            Console.WriteLine(s3Exception.Message, s3Exception.InnerException);

            Console.ReadKey();
        }
    }

}