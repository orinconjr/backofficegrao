﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Xml.Serialization;
public partial class admin_simularFrete : System.Web.UI.Page
{
    public class retornoCalculoFrete
    {
        public rnFrete.RetornoCalculoFrete calculoFrete { get; set; }
        public string transportadora { get; set; }
    }

    protected void btnadicionarpacote_Click(object sender, EventArgs e)
    {
        if (!validarCampos())
            return;
        rnFrete.Pacotes pacote = new rnFrete.Pacotes();
        pacote.peso = (Convert.ToInt32(txtPeso.Text));
        pacote.altura = (Convert.ToInt32(txtAltura.Text));
        pacote.largura = (Convert.ToInt32(txtLargura.Text));
        pacote.comprimento = (Convert.ToInt32(txtComprimento.Text));
        adicionaItemListaTemporaria(pacote);
    }

    private void adicionaItemListaTemporaria(rnFrete.Pacotes item)
    {
        trtitulopacotes.Visible = true;
        thtitulopacotes.Visible = true;
        btnSimular.Visible = true;
        var lista = new List<rnFrete.Pacotes>();
        if (!string.IsNullOrEmpty(hdfLista.Text))
        {
            lista = Deserialize(hdfLista.Text);
        }
        lista.Add(item);
        hdfLista.Text = Serialize(lista);
        fillGrid();
    }

    public static List<rnFrete.Pacotes> Deserialize(string tData)
    {
        var serializer = new XmlSerializer(typeof(List<rnFrete.Pacotes>));

        TextReader reader = new StringReader(tData);

        return (List<rnFrete.Pacotes>)serializer.Deserialize(reader);
    }

    public static string Serialize(List<rnFrete.Pacotes> tData)
    {
        var serializer = new XmlSerializer(typeof(List<rnFrete.Pacotes>));

        TextWriter writer = new StringWriter();
        serializer.Serialize(writer, tData);

        return writer.ToString();
    }

    private void fillGrid()
    {
        tbFretes.Visible = false;
        lstCalculos.DataSource = null;
        var lista = new List<rnFrete.Pacotes>();
        if (!string.IsNullOrEmpty(hdfLista.Text))
        {
            lista = Deserialize(hdfLista.Text);
            if(lista.Count>0)
            {
                lstPacotes.DataSource = lista;
            }
            else
            {
                thtitulopacotes.Visible = false;
                trtitulopacotes.Visible = false;
                btnSimular.Visible = false;

                lstPacotes.DataSource = null;
            }

        }


        lstPacotes.DataBind();
        lstCalculos.DataBind();
    }

    protected void btnRemover_OnCommand(object sender, CommandEventArgs e)
    {
        var lista = new List<rnFrete.Pacotes>();
        if (!string.IsNullOrEmpty(hdfLista.Text))
        {
            lista = Deserialize(hdfLista.Text);
        }
        try
        {
            lista.Remove(lista[Convert.ToInt32(e.CommandArgument)]);
            hdfLista.Text = Serialize(lista);
        }
        catch (Exception)
        {

        }
        fillGrid();
    }

    protected void btnSimular_Click(object sender, EventArgs e)
    {
        tbFretes.Visible = true;
        var lista = new List<rnFrete.Pacotes>();
        if (!string.IsNullOrEmpty(hdfLista.Text))
        {
            lista = Deserialize(hdfLista.Text);
        }
        if (!lista.Any())
        {
            Response.Write("<script>alert('Favor adicionar algum pacote à lista.');</script>");
            return;
        }

        List<retornoCalculoFrete> fretes = calculaFretes(lista, txtCep.Text, (Convert.ToInt32(txtPeso.Text)), Convert.ToDecimal(txtValor.Text));
        lstCalculos.DataSource = fretes;
        lstCalculos.DataBind();
    }

    List<retornoCalculoFrete> calculaFretes(List<rnFrete.Pacotes> pacotes, string meucep, int pesoTotal, decimal valorTotal)
    {
        List<retornoCalculoFrete> fretes = new List<retornoCalculoFrete>();

        long cep = Convert.ToInt64(meucep.Replace("-", "").Replace(".", "").Replace(" ", "").Trim());

        #region Calculo na Jadlog
        var valoresJadlog = rnFrete.CalculaFrete(13, meucep.Replace("-", "").Trim(), valorTotal, pacotes);
        var valoresJadlogCom = rnFrete.CalculaFreteWebserviceJadlog(valoresJadlog.pesoAferido, cep, "jadlogcom", valorTotal);
        if (valoresJadlog.valor > 0)
        {
            var valorJadlogWs = rnFrete.CalculaFreteWebserviceJadlog(valoresJadlog.pesoAferido, cep, "jadlog", valorTotal);
            if (valorJadlogWs.valor > 0)
            {
                valoresJadlog = valorJadlogWs;
            }
        }
        retornoCalculoFrete freteJad = new retornoCalculoFrete();
        freteJad.calculoFrete = valoresJadlog;
        freteJad.transportadora = "jadlog";
        fretes.Add(freteJad);
        #endregion


        #region Calculo TNT
        retornoCalculoFrete freteTnt = new retornoCalculoFrete();
        var valoresTnt = rnFrete.CalculaFrete(22, meucep.Replace("-", "").Trim(), valorTotal, pacotes);
        freteTnt.calculoFrete = valoresTnt;
        freteTnt.transportadora = "tnt";
        fretes.Add(freteTnt);
        #endregion


        #region Calculo na Plimor
        retornoCalculoFrete fretePlimor = new retornoCalculoFrete();
        //var valoresPlimor = rnFrete.CalculaFrete(19, Convert.ToInt32(pesoTotal), meucep.Replace("-", "").Trim(), valorTotal, 0, 0, 0);
        var valoresPlimor = rnFrete.CalculaFrete(21, Convert.ToInt32(pesoTotal), meucep.Replace("-", "").Trim(), valorTotal, 0, 0, 0);
        fretePlimor.calculoFrete = valoresPlimor;
        fretePlimor.transportadora = "plimor";
        fretes.Add(fretePlimor);
        #endregion

        #region Calculo Correios

        int prazoCorreios = 0;
        decimal valorTotalCorreios = 0;
        bool correiosPacoteIndividual = true;
        if (correiosPacoteIndividual)
        {
            foreach (var pacote in pacotes)
            {
                decimal valorDoPacote = valorTotal / pacotes.Count();
                var consultaCorreios = rnFrete.CalculaFrete(20, Convert.ToInt32(pacote.peso),
                    meucep.Replace("-", "").Trim(), valorDoPacote,
                    Convert.ToDecimal(((decimal?)pacote.largura ?? 0)), Convert.ToDecimal(((decimal?)pacote.altura ?? 0)),
                    Convert.ToDecimal(((decimal?)pacote.comprimento ?? 0)));
                valorTotalCorreios += consultaCorreios.valor;
                if (consultaCorreios.prazo > prazoCorreios) prazoCorreios = consultaCorreios.prazo;
                retornoCalculoFrete fretePac = new retornoCalculoFrete();
                fretePac.calculoFrete = new rnFrete.RetornoCalculoFrete();
                fretePac.calculoFrete.valor = consultaCorreios.valor;
                fretePac.calculoFrete.prazo = prazoCorreios;
                fretePac.transportadora = "pac";
                fretes.Add(fretePac);
            }

            retornoCalculoFrete fretePacTotal = new retornoCalculoFrete();
            fretePacTotal.calculoFrete = new rnFrete.RetornoCalculoFrete();
            fretePacTotal.calculoFrete.valor = valorTotalCorreios;
            fretePacTotal.transportadora = "<strong> Total pac</strong>";
            fretes.Add(fretePacTotal);
        }
        else
        {
            var consultaCorreios = rnFrete.CalculaFrete(12, Convert.ToInt32(pesoTotal),
                meucep.Replace("-", "").Trim(), valorTotal, 0, 0, 0);
            valorTotalCorreios += consultaCorreios.valor;
            if (consultaCorreios.prazo > prazoCorreios) prazoCorreios = consultaCorreios.prazo;
        }

        #endregion

        return fretes;
    }

    bool validarCampos()
    {
        bool ok = true;
        string mensagem = "";

        int cep = 0;
        try
        {
            cep = Convert.ToInt32(txtCep.Text.Replace("-",""));
        }
        catch (Exception)
        {

        }

        if (cep == 0)
        {
            ok = false;
            mensagem += @"Cep inválido\n";
        }



        decimal valor = 0;
        try
        {
            valor = Convert.ToDecimal(txtValor.Text.Replace(",", "."));
        }
        catch (Exception)
        {

        }

        if (valor == 0)
        {
            ok = false;
            mensagem += @"Valor inválido\n";
        }


        int peso = 0;
        try
        {
            peso = Convert.ToInt32(txtPeso.Text.Replace(",", "."));
        }
        catch (Exception)
        {

        }

        if (peso == 0)
        {
            ok = false;
            mensagem += @"Peso inválido\n";
        }



        int altura = 0;
        try
        {
            altura = Convert.ToInt32(txtAltura.Text.Replace(",", "."));
        }
        catch (Exception)
        {

        }

        if (altura == 0)
        {
            ok = false;
            mensagem += @"Altura inválida\n";
        }

        int largura = 0;
        try
        {
            largura = Convert.ToInt32(txtLargura.Text.Replace(",", "."));
        }
        catch (Exception)
        {

        }

        if (largura == 0)
        {
            ok = false;
            mensagem += @"Largura inválida\n";
        }


        int comprimento = 0;
        try
        {
            comprimento = Convert.ToInt32(txtComprimento.Text.Replace(",", "."));
        }
        catch (Exception)
        {

        }

        if (comprimento == 0)
        {
            ok = false;
            mensagem += @"Comprimento inválido\n";
        }

        if (!ok)
        {
            string meuscript = @"alert('" + mensagem + "');";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "MyScript", meuscript, true);

        }
        return ok;

    }

}