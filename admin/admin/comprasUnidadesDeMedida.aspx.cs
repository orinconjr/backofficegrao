﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.IO;
using System.Web.DynamicData;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SpreadsheetLight;

public partial class admin_comprasUnidadesDeMedida : System.Web.UI.Page
{
    private string[] unidadeBasica = { "", "Grama", "Centímetro", "Unidade" };
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            int id = 0;
            bool ok = Int32.TryParse(Request.QueryString["idUnidade"], out id);

            if (id != 0)
            {
                fieldseteditarunidade.Visible = true;
                fieldsetnovaunidade.Visible = false;
                carregarDadosEditar(id);
            }

            carregaGrid();
        }


    }
    private void carregaGrid()
    {
        var data = new dbCommerceDataContext();
        var unidadeMedidas = (from um in data.tbComprasUnidadesDeMedidas
            select um).ToList();

        if (!String.IsNullOrEmpty(txtidunidade.Text))
        {
            int unidadeId = Convert.ToInt32(txtidunidade.Text);
            unidadeMedidas =
                (from um in unidadeMedidas where um.idComprasUnidadesDeMedida == unidadeId select um).ToList();
            
        }
        if (!String.IsNullOrEmpty(txtunidademedida.Text))
        {
            string unidademedida = txtunidademedida.Text;
            unidadeMedidas =
                (from um in unidadeMedidas where um.unidadeDeMedida.ToLower().Contains(unidademedida.ToLower()) select um).ToList();
          
        }
        if (ddlUnidadeBasica.SelectedItem.Value != "0")
        {
            int idunidadebasica = Convert.ToInt32(ddlUnidadeBasica.SelectedItem.Value);
            unidadeMedidas = (from um in unidadeMedidas where um.idUnidadeBasica == idunidadebasica select um).ToList();
       
        }
        if (!String.IsNullOrEmpty(txtFatorConversao.Text))
        {
            decimal fatorConversao = 0;
            bool ok = Decimal.TryParse(txtFatorConversao.Text, out fatorConversao);
            if (fatorConversao != 0)
            {
                unidadeMedidas =
                             (from um in unidadeMedidas where um.fatorConversao == fatorConversao select um).ToList();
            }
         
        }
      
        GridView1.DataSource = unidadeMedidas;
        GridView1.DataBind();


    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;

        carregaGrid();
    }

    protected void btnFiltrar_Click(object sender, EventArgs e)
    {
        carregaGrid();
    }
    protected void btnExportarPlanilha_Click(object sender, EventArgs e)
    {
        var data = new dbCommerceDataContext();
        var unidadeMedidas = (from um in data.tbComprasUnidadesDeMedidas
                              select um).ToList();

        if (!String.IsNullOrEmpty(txtidunidade.Text))
        {
            int unidadeId = Convert.ToInt32(txtidunidade.Text);
            unidadeMedidas =
                (from um in unidadeMedidas where um.idComprasUnidadesDeMedida == unidadeId select um).ToList();

        }
        if (!String.IsNullOrEmpty(txtunidademedida.Text))
        {
            string unidademedida = txtunidademedida.Text;
            unidadeMedidas =
                (from um in unidadeMedidas where um.unidadeDeMedida.ToLower().Contains(unidademedida.ToLower()) select um).ToList();

        }
        if (ddlUnidadeBasica.SelectedItem.Value != "0")
        {
            int idunidadebasica = Convert.ToInt32(ddlUnidadeBasica.SelectedItem.Value);
            unidadeMedidas = (from um in unidadeMedidas where um.idUnidadeBasica == idunidadebasica select um).ToList();

        }

        SLDocument sl = new SLDocument();
        SLStyle style1 = sl.CreateStyle();
        style1.Font.Bold = true;
        sl.SetRowStyle(1, style1);

        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        Response.AddHeader("Content-Disposition", "attachment; filename=unidadesDeMedida.xlsx");



        sl.SetCellValue("A1", "Id");
        sl.SetCellValue("B1", "Unidade de Medida");
        sl.SetCellValue("C1", "Unidade Básica");
        sl.SetCellValue("D1", "Fator de Conversão");
  
        int linha = 2;
        foreach (var item in unidadeMedidas)
        {
            sl.SetCellValue(linha, 1, item.idComprasUnidadesDeMedida);
            sl.SetCellValue(linha, 2, item.unidadeDeMedida);
            sl.SetCellValue(linha, 3, item.idUnidadeBasica.ToString());
            sl.SetCellValue(linha, 4, item.fatorConversao.ToString());
            

            linha++;
        }
        sl.SaveAs(Response.OutputStream);
        Response.End();
      
    }
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        if (e.Row.RowType == DataControlRowType.DataRow)
        {

            Label lblunidadebasica = (Label)e.Row.FindControl("lblunidadebasica");
            int unidadebasicaindex = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "idUnidadeBasica") ?? 0);

            lblunidadebasica.Text = unidadeBasica[unidadebasicaindex];


            HtmlAnchor linkeditar = (HtmlAnchor)e.Row.FindControl("linkeditar");
            linkeditar.HRef = "comprasUnidadesDeMedida.aspx?idUnidade=" + DataBinder.Eval(e.Row.DataItem, "idComprasUnidadesDeMedida");

            LinkButton MyButton = (LinkButton)e.Row.FindControl("cmdDelete");
            MyButton.Attributes.Add("onclick", "javascript:return " +
            "confirm('Confirma a exclusão do item ?')");

        }
    }

    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        using (var data = new dbCommerceDataContext())
        {
            int idUnidade = Convert.ToInt32(GridView1.DataKeys[e.RowIndex].Value);
            var unidade = (from um in data.tbComprasUnidadesDeMedidas
                where um.idComprasUnidadesDeMedida == idUnidade
                select um).FirstOrDefault();
            data.tbComprasUnidadesDeMedidas.DeleteOnSubmit(unidade);
            string meuscript = "";
            try
            {
                data.SubmitChanges();
                carregaGrid();
            }
            catch (Exception ex)
            {

                meuscript = @"alert('" + ex.Message + "');";
            }
            Page.ClientScript.RegisterStartupScript(this.GetType(), "MyScript", meuscript, true);

        }

        
    }

    protected void carregarDadosEditar(int idUnidadeMedida)
    {
        using (var data = new dbCommerceDataContext())
        {
            var unidadeMedida = (from um in data.tbComprasUnidadesDeMedidas
                where um.idComprasUnidadesDeMedida == idUnidadeMedida
                select um).FirstOrDefault();
            lblId.Text = unidadeMedida.idComprasUnidadesDeMedida.ToString();
            txtunidadedemedidaeditar.Text = unidadeMedida.unidadeDeMedida;
            ddlunidadebasicaeditar.SelectedIndex =
                ddlunidadebasicaeditar.Items.IndexOf(ddlunidadebasicaeditar.Items.FindByValue(unidadeMedida.idUnidadeBasica.ToString()));
            txtfatorconversaoeditar.Text = unidadeMedida.fatorConversao.ToString();
        }
    }
    protected void btnSalvar_Click(object sender, EventArgs e)
    {
       
        //string erro = "";
        //if (String.IsNullOrEmpty(txtunidadedemedidainserir.Text))
        //    erro = "Informe a unidade de medida.\n";
        //if (ddlunidadebasicainserir.SelectedValue == "0")
        //    erro += "Selecione a unidade básica.\n";
        //if (String.IsNullOrEmpty(txtfatorconversaoinserir.Text))
        //    erro += "Informe o Fator de Conversão";
        //else
        //{
        //    decimal fatorConversao = 0;
        //    try
        //    {
        //        fatorConversao = Convert.ToDecimal(txtfatorconversaoinserir.Text);
        //    }
        //    catch (Exception ex)
        //    {

        //        erro += "Fator de conversão precisa ser numérico";
        //    }
        //}
        string erro = validaCampos(txtunidadedemedidainserir.Text, ddlunidadebasicainserir.SelectedValue,
            txtfatorconversaoinserir.Text);
        if ( erro == "")
        {
            using (var data = new dbCommerceDataContext())
            {
                var unidadeMedida = new tbComprasUnidadesDeMedida();
                unidadeMedida.unidadeDeMedida = txtunidadedemedidainserir.Text;
                unidadeMedida.idUnidadeBasica = Convert.ToInt32(ddlunidadebasicainserir.SelectedValue);
                unidadeMedida.fatorConversao = Convert.ToDecimal(txtfatorconversaoinserir.Text);
                data.tbComprasUnidadesDeMedidas.InsertOnSubmit(unidadeMedida);
                data.SubmitChanges();
            }
            fieldseteditarunidade.Visible = false;
            fieldsetnovaunidade.Visible = false;
            carregaGrid();
        }
        else
        {
             string  meuscript = @"alert('" + erro + "');";
             Page.ClientScript.RegisterStartupScript(this.GetType(), "MyScript", meuscript, true);
        }
       
       
    }

    string validaCampos(string unidadeDeMedida, string unidadeBasica, string fatorConversao)
    {
        string erro = "";
        if (String.IsNullOrEmpty(unidadeDeMedida))
            erro = "Informe a unidade de medida. ";
        if (unidadeBasica == "0")
            erro += "Selecione a unidade básica. ";
        if (String.IsNullOrEmpty(fatorConversao))
            erro += "Informe o Fator de Conversão. ";
        else
        {
            decimal fatConversao = 0;
            try
            {
                fatConversao = Convert.ToDecimal(fatorConversao);
            }
            catch (Exception ex)
            {

                erro += "Fator de conversão precisa ser numérico. ";
            }
        }
        return erro;
    }
    protected void btnNovaUnidadeMedida_Click(object sender, EventArgs e)
    {
        fieldseteditarunidade.Visible = false;
        fieldsetnovaunidade.Visible = true;
    }
    protected void btnAtualizar_Click(object sender, EventArgs e)
    {
          string erro = validaCampos(txtunidadedemedidaeditar.Text, ddlunidadebasicaeditar.SelectedValue,
            txtfatorconversaoeditar.Text);
        if (erro == "")
        {
            using (var data = new dbCommerceDataContext())
            {
                int idUnidadeMedida = Convert.ToInt32(Request.QueryString["idUnidade"]);
                var unidadeMedida = (from um in data.tbComprasUnidadesDeMedidas
                                     where um.idComprasUnidadesDeMedida == idUnidadeMedida
                                     select um).FirstOrDefault();

                unidadeMedida.unidadeDeMedida = txtunidadedemedidaeditar.Text;
                unidadeMedida.idUnidadeBasica = Convert.ToInt32(ddlunidadebasicaeditar.SelectedValue);
                unidadeMedida.fatorConversao = Convert.ToDecimal(txtfatorconversaoeditar.Text);
                data.SubmitChanges();

                fieldseteditarunidade.Visible = false;
                fieldsetnovaunidade.Visible = false;
                carregaGrid();


            }
        }
        else
        {
            string meuscript = @"alert('" + erro + "');";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "MyScript", meuscript, true);
        }
     
    }
}