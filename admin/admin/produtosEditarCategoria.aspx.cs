﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;

public partial class admin_produtosEditarCategoria : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ASPxGridView.RegisterBaseScript(Page);

        if (!Page.IsPostBack)
        {
            //grd.FilterExpression =  "Contains([Ativo], 'True')";
            PopulateRootLevel();
        }
    }

    protected void grd_HtmlRowCreated(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType != GridViewRowType.Data) return;
        {
            TextBox txtIdDaEmpresa = (TextBox)grd.FindRowCellTemplateControl(e.VisibleIndex, (GridViewDataColumn)grd.Columns["produtoIdDaEmpresa"], "txtIdDaEmpresa");
            TextBox txtNome = (TextBox)grd.FindRowCellTemplateControl(e.VisibleIndex, (GridViewDataColumn)grd.Columns["produtoNome"], "txtNome");
            //TextBox txtProdutoId = (TextBox)grd.FindRowCellTemplateControl(e.VisibleIndex, (GridViewDataColumn)grd.Columns["produtoId"], "txtProdutoId");

            
            txtIdDaEmpresa.Attributes.Add("onclick", "document.getElementById('" + txtIdDaEmpresa.ClientID + "').focus();document.getElementById('" + txtIdDaEmpresa.ClientID + "').select();");
            txtNome.Attributes.Add("onclick", "document.getElementById('" + txtNome.ClientID + "').focus();document.getElementById('" + txtNome.ClientID + "').select();");
        }
    }

    private void PopulateRootLevel()
    {
        PopulateNodes(rnCategorias.categoriasSelecionaNivelPai().Tables[0], treeCategorias.Nodes);
    }

    protected void TreeView1_TreeNodePopulate(object sender, TreeNodeEventArgs e)
    {
        PopulateSubLevel(Int32.Parse(e.Node.Value), e.Node);
    }

    protected void treeCategoriasProduto_TreeNodePopulate(object sender, TreeNodeEventArgs e)
    {
        var tree = (TreeView) sender;
        int pIdProduto = Convert.ToInt32(tree.ToolTip);
        PopulateSubLevel(Int32.Parse(e.Node.Value), e.Node, pIdProduto);
    }

    private void PopulateSubLevel(int categoriaPaiId, TreeNode parentNode)
    {
        PopulateNodes(rnCategorias.categoriasSelecionaNivelFilho(categoriaPaiId).Tables[0], parentNode.ChildNodes);
    }


    private void PopulateSubLevel(int categoriaPaiId, TreeNode parentNode, int pIdProduto)
    {
        PopulateNodes(rnCategorias.categoriasSelecionaNivelFilho(categoriaPaiId).Tables[0], parentNode.ChildNodes, pIdProduto);
    }


    private void PopulateNodes(DataTable dt, TreeNodeCollection nodes)
    {
        foreach (DataRow dr in dt.Rows)
        {
            TreeNode tn = new TreeNode();
            tn.Text = dr["categoriaNome"].ToString();
            tn.Value = dr["categoriaId"].ToString();

            nodes.Add(tn);

            tn.Expand();

            //If node has child nodes, then enable on-demand populating
            tn.PopulateOnDemand = ((int)(dr["childnodecount"]) > 0);
            tn.Expanded = ((int)(dr["childnodecount"]) > 0);
        }
    }

    private void PopulateNodes(DataTable dt, TreeNodeCollection nodes, int pIdProduto)
    {
        foreach (DataRow dr in dt.Rows)
        {
            TreeNode tn = new TreeNode();
            tn.Text = dr["categoriaNome"].ToString();
            tn.Value = dr["categoriaId"].ToString();
            
            
            /**/
            //var categoriaContext = new dbCommerceDataContext();
            //var categorias = (from p in categoriaContext.tbProdutoCategorias where )

            int idCategoria = Convert.ToInt32(dr["categoriaId"].ToString());
            var data = new dbCommerceDataContext();
            var categorias = (from c in data.tbJuncaoProdutoCategorias where c.produtoId == pIdProduto && c.categoriaId == idCategoria select c);
            //Response.Write(pIdProduto + "<br>");
            if (categorias.Any()) tn.Checked = true;


            nodes.Add(tn);

            tn.Expand();

            /*var categorias = rnCategorias.juncaoProdutoCategoriaSeleciona_PorProdutoId(pIdProduto);

            if (tn.Value == categorias.ToString())
                tn.Checked = true;*/
            /***/
            
            
            
            //If node has child nodes, then enable on-demand populating
            tn.PopulateOnDemand = ((int)(dr["childnodecount"]) > 0);
            tn.Expanded = ((int)(dr["childnodecount"]) > 0);
        }
    }

    protected void btSelecionarPrincipal_Command(object sender, CommandEventArgs e)
    {
        rnProdutos.alteraProdutoPrincipal(int.Parse(e.CommandName.ToString()), int.Parse(e.CommandArgument.ToString()));
        grd.DataBind();
    }

    protected void grd_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        try
        {
            string estoqueAtual;

            int inicio = 0;
            int fim = 0;

            if (grd.PageIndex == 0)
            {
                inicio = 0;
                fim = inicio + grd.GetCurrentPageRowValues("produtoId").Count;
            }
            else
            {
                inicio += inicio + grd.SettingsPager.PageSize * grd.PageIndex;
                fim += inicio + grd.GetCurrentPageRowValues("produtoId").Count;
            }

            for (int i = inicio; i < fim; i++)
            {
                TextBox txtProdutoId = (TextBox)grd.FindRowCellTemplateControl(i, grd.Columns[3] as GridViewDataColumn, "txtProdutoId");
                TextBox txtProdutoPaiId = (TextBox)grd.FindRowCellTemplateControl(i, grd.Columns[6] as GridViewDataColumn, "txtProdutoPaiId");
                ASPxDropDownEdit ddlCategorias = (ASPxDropDownEdit)grd.FindRowCellTemplateControl(i, grd.Columns[1] as GridViewDataColumn, "ddlCategorias");
                TreeView treeCategoriasProduto = (TreeView) ddlCategorias.FindControl("treeCategoriasProduto");
                int produtoId = Convert.ToInt32(txtProdutoId.Text);
                int produtoPaiId = Convert.ToInt32(txtProdutoPaiId.Text);

                var checkedNode = treeCategoriasProduto.CheckedNodes;
                var data = new dbCommerceDataContext();
                var categorias = (from c in data.tbJuncaoProdutoCategorias where c.produtoId == produtoId where c.tbProdutoCategoria.exibirSite == false select c);

                List<int> categoriasChecadas = new List<int>();

               /*categoriaExclui(produtoId, produtoPaiId);
                categoriaIncluiNoGrupoDeProdutos(produtoId, produtoPaiId, treeCategoriasProduto);*/

                foreach (TreeNode node in checkedNode)
                {
                    int categoriaId = Convert.ToInt32(node.Value);
                    categoriasChecadas.Add(categoriaId);
                    if (categorias.Where(x => x.categoriaId == categoriaId).Count() == 0)
                    {
                        Response.Write(categoriaId.ToString());
                        var categoria = new tbJuncaoProdutoCategoria();
                        categoria.categoriaId = categoriaId;
                        categoria.dataDaCriacao = DateTime.Now;
                        categoria.produtoId = produtoId;
                        categoria.produtoPaiId = produtoPaiId;
                        data.tbJuncaoProdutoCategorias.InsertOnSubmit(categoria);
                        data.SubmitChanges();
                    }
                }

                foreach (var categoriaAtual in categorias)
                {
                    if (!categoriasChecadas.Any(x => x == categoriaAtual.categoriaId))
                    {
                        var dataExcluir = new dbCommerceDataContext();
                        var categoriaExcluir = (from c in dataExcluir.tbJuncaoProdutoCategorias
                                                where
                                                    c.produtoId == produtoId &&
                                                    c.categoriaId == categoriaAtual.categoriaId
                                                select c).FirstOrDefault();
                        dataExcluir.tbJuncaoProdutoCategorias.DeleteOnSubmit(categoriaExcluir);
                        dataExcluir.SubmitChanges();
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.ToString());
        }
        grd.DataBind();
    }

    private void categoriaExclui(int produtoId, int produtoPaiId)
    {
        if (produtoPaiId == 0)
            rnCategorias.juncaoProdutoCategoriaExclui_PorProdutoId(produtoId);
        else
            rnCategorias.juncaoProdutoCategoriaExclui_PorProdutoPaiId(produtoPaiId);
    }

    private void categoriaIncluiNoGrupoDeProdutos(int produtoId, int produtoPaiId, TreeView treeCategorias)
    {
        if (treeCategorias.CheckedNodes.Count > 0)
        {
            if (produtoPaiId == 0)
            {
                foreach (TreeNode node in treeCategorias.CheckedNodes)
                {
                    rnCategorias.juncaoProdutoCategoriaInclui(produtoId, Convert.ToInt32(node.Value), produtoPaiId);
                }
            }
            else
            {
                foreach (DataRow row in rnProdutos.produtoSeleciona_PorProdutoPaiId(produtoPaiId).Tables[0].Rows)
                {
                    produtoId = int.Parse(row["produtoId"].ToString());
                    foreach (TreeNode node in treeCategorias.CheckedNodes)
                    {
                        rnCategorias.juncaoProdutoCategoriaInclui(produtoId, Convert.ToInt32(node.Value), produtoPaiId);
                    }
                }
            }
        }
    }
    protected void ddlCategorias_OnPreRender(object sender, EventArgs e)
    {
       
        var ddlCategorias = (ASPxDropDownEdit)sender;
        var tree = (TreeView)ddlCategorias.FindControl("treeCategoriasProduto");
        //var txtProdutoId = (TextBox) ddlCategorias.FindControl("txtProdutoId");
        var txtProdutoId = ddlCategorias.ToolTip;
        tree.ToolTip = txtProdutoId;
        PopulateNodes(rnCategorias.categoriasSelecionaNivelPaiOcultos().Tables[0], tree.Nodes, Convert.ToInt32("0" + txtProdutoId));
    }

    protected void treeCategorias_databound(object sender, EventArgs e)
    {
        var drop = (ASPxDropDownEdit) sender;
        var trees = (TreeView)drop.FindControl("treeCategoriasProduto");
        int idProduto = 0;
        Int32.TryParse(drop.ToolTip, out idProduto);
        if (idProduto == 0) return;
        var data = new dbCommerceDataContext();
        var categorias = (from c in data.tbJuncaoProdutoCategorias where c.produtoId == idProduto select c).ToList();
        foreach (TreeNode tree in trees.Nodes)
        {
            int idCategoria = Convert.ToInt32(tree.Value);
            if (categorias.Any(x => x.categoriaId == idCategoria)) tree.Checked = true;
            foreach (TreeNode childNode in tree.ChildNodes)
            {
                marcaNodes(childNode, idProduto, categorias);
            }
            
        }
    }

    private void marcaNodes(TreeNode node, int idProduto, List<tbJuncaoProdutoCategoria> categorias)
    {
        int idCategoria = Convert.ToInt32(node.Value);
        if (categorias.Any(x => x.categoriaId == idCategoria)) node.Checked = true;

        Response.Write(idCategoria + "<br>");

        foreach (TreeNode childNode in node.ChildNodes)
        {
            marcaNodes(childNode, idProduto, categorias);
        }
        
    }
}