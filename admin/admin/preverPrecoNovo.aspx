﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="preverPrecoNovo.aspx.cs" Inherits="admin_preverPrecoNovo" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .style1
        {
            width: 600px;
        }
        .style2
        {
            width: 109px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <asp:SqlDataSource ID="sqlMarcas" runat="server" 
            ConnectionString="<%$ ConnectionStrings:connectionString %>" 
            SelectCommand="SELECT [marcaId], [marcaNome] FROM [tbMarca] ORDER BY [marcaNome]">
        </asp:SqlDataSource>
        <table cellspacing="0" class="style1">
            <tr>
                <td class="style2">
                    Marca:<asp:DropDownList ID="drpMarcas" runat="server" AppendDataBoundItems="True" 
            DataSourceID="sqlMarcas" DataTextField="marcaNome" DataValueField="marcaId" 
                        AutoPostBack="True">
            <asp:ListItem Selected="True" Value="">Selecione</asp:ListItem>
        </asp:DropDownList>
                </td>
                <td>
                    Porcentagem:<br />
                    <asp:TextBox ID="txtPorcentagem" runat="server"></asp:TextBox>
                </td>
                <td>
                    Palavra chave:<br />
                    <asp:TextBox ID="txtPalavraChave" runat="server" Text=""></asp:TextBox>
                </td>
                <td>
                    Preço inicial:<br />
                    <asp:TextBox ID="txtPrecoInicial" runat="server">0</asp:TextBox>
                </td>
                <td>
                    Preço final:<br />
                    <asp:TextBox ID="txtPrecoFinal" runat="server">9999999</asp:TextBox>
                </td>
                <td>&nbsp;<br/>
                    <asp:Button ID="Button1" runat="server" Text="Filtrar" OnClick="Button1_Click" />
                </td>
            </tr>
        </table>
&nbsp;<asp:GridView ID="grd" runat="server" AutoGenerateColumns="False" 
            DataKeyNames="produtoId" DataSourceID="sqlProdutos" 
            EnableModelValidation="True" AllowSorting="True" OnRowDataBound="grd_OnRowDataBound">
            <Columns>
                <asp:TemplateField HeaderText="Count">
                    <ItemTemplate>
                        <%# Container.DataItemIndex + 1 %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Id" InsertVisible="False" 
                    SortExpression="produtoId">
                    <ItemTemplate>
                        <asp:Label ID="lblProdutoId" runat="server" Text='<%# Bind("produtoId") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Id da empresa"   
                    SortExpression="produtoIdDaEmpresa">
                    <ItemTemplate>
                        <asp:TextBox ID="txtIdDaEmpresa" runat="server" Width="100"
                            Text='<%# Bind("produtoIdDaEmpresa") %>'></asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Nome" 
                    SortExpression="produtoNome">
                    <ItemTemplate>
                        <asp:TextBox ID="txtNome" runat="server"  Width="300"
                            Text='<%# Bind("produtoNome") %>'></asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Preço de custo" 
                    SortExpression="produtoPrecoDeCusto">
                    <ItemTemplate>
                        <asp:TextBox ID="txtPrecoDeCusto" runat="server" 
                            Text='<%# Bind("produtoPrecoDeCusto") %>'></asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Preço promocional" 
                    SortExpression="produtoPrecoPromocional">
                    <ItemTemplate>
                        <asp:TextBox ID="txtPrecoPromocional" runat="server" 
                            Text='<%# Bind("produtoPrecoPromocional") %>'></asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="produtoPreco" DataFormatString="{0:c}" 
                    HeaderText="Preço" SortExpression="produtoPreco" />
                <asp:TemplateField HeaderText="Preço Novo">
                    <ItemTemplate>
                        <asp:Label ID="lblPrecoNovo" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Preço Novo Promocional">
                    <ItemTemplate>
                        <asp:Label ID="lblPrecoNovoPromocional" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Margem Atual" SortExpression="margem">
                    <ItemTemplate>
                        <asp:Label ID="lblMargem" runat="server" Text='<%# Bind("margem") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Desativar">
                    <ItemTemplate>
                        <asp:CheckBox ID="ckbDesativar" runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        <br />
        <asp:Button ID="btAlterar" runat="server" onclick="btAlterar_Click" 
            Text="Salvar" />
        <asp:SqlDataSource ID="sqlProdutos" runat="server"  CancelSelectOnNullParameter="False"
            ConnectionString="<%$ ConnectionStrings:connectionString %>" 
            
            SelectCommand="SELECT produtoId, produtoIdDaEmpresa, produtoNome, produtoPrecoDeCusto, produtoPreco, produtoPrecoPromocional, produtoAtivo, (((produtoPreco * 100) / produtoPrecoDeCusto) - 100) as margem FROM tbProdutos WHERE (produtoAtivo = @produtoAtivo) and (produtoPrecoDeCusto >= @produtoPrecoInicial) and (produtoPrecoDeCusto <= @produtoPrecoFinal) AND (produtoNome LIKE '%' + CASE @palavraChave WHEN '' THEN [produtoNome] ELSE @palavraChave END + '%') ORDER BY produtoIdDaEmpresa">
            <SelectParameters>
                <asp:Parameter DefaultValue="True" Name="produtoAtivo" Type="String" />
                <asp:ControlParameter ControlID="txtPrecoInicial" Name="produtoPrecoInicial" PropertyName="Text" Type="Int32" />
                <asp:ControlParameter ControlID="txtPrecoFinal" Name="produtoPrecoFinal" PropertyName="Text" Type="Int32" />
                <asp:ControlParameter ControlID="txtPalavraChave" Name="palavraChave" PropertyName="Text" Type="String" ConvertEmptyStringToNull="False" />
            </SelectParameters>
        </asp:SqlDataSource>
    
    </div>
    </form>
</body>
</html>