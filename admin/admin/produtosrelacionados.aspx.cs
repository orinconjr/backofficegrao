﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SpreadsheetLight;

public partial class admin_produtosrelacionados : System.Web.UI.Page
{
    public class meusprodutos
    {
        public int produtoId { get; set; }
        public string produtoIdDaEmpresa { get; set; }
        public string produtoNome { get; set; }
        public decimal produtoPreco { get; set; }
        public decimal? produtoPrecoDeCusto { get; set; }
        public decimal? produtoPrecoPromocional { get; set; }
        public decimal margemDeLucro { get; set; }
        public int produtoFornecedor { get; set; }
        public string produtoAtivo { get; set; }
        public DateTime produtoDataDaCriacao { get; set; }
        public string complementoIdDaEmpresa { get; set; }
        public string incluidoDeCombo { get; set; }
        public double produtoPeso { get; set; }
        public string fornecedorNome { get; set; }
        public decimal totalVendaCombo { get; set; }
        public decimal totalPrecoDeCombo { get; set; }



        internal bool Contains()
        {
            throw new NotImplementedException();
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnPesquisar_Click(object sender, ImageClickEventArgs e)
    {
        using (var data = new dbCommerceDataContext())
        {
            var buscaNormalizada = txtBusca.Text.Replace(Environment.NewLine, ",").Replace(";", ",").Replace(" ", ",").Replace(",,", ",").TrimEnd(',').Trim();
            var buscaSplit = buscaNormalizada.Split(',');
            var buscaInt = buscaSplit.Select(x => Convert.ToInt32(x));
            //int produtoIdFilho = Convert.ToInt32(txtBusca.Text);
            var produtoFilho = (from c in data.tbProdutos
                                where buscaInt.Contains(c.produtoId)
                                select new
                                {
                                    c.produtoNome
                                }).ToList();

            var produtos = (from c in data.tbProdutoRelacionados
                            join pr in data.tbProdutos on c.idProdutoPai equals pr.produtoId
                            where buscaInt.Contains(c.idProdutoFilho)
                            select new
                            {
                                pr.produtoId,
                                pr.produtoNome
                            }).ToList();
            if (produtos.Count > 0)
            {
                containergrid.Attributes.Add("style", "display:block;");
                foreach (var produto in produtoFilho)
                {
                    lblprodutofilho.Text = "<strong>Produto consultado:</strong> " + produto.produtoNome + "<br>";
                }
                lstProdutos.DataSource = produtos;
                lstProdutos.DataBind();
            }
            else
            {
                containergrid.Attributes.Add("style", "display:none;");
                if (produtoFilho.Count() > 0)
                {
                    lblprodutofilho.Text = "<strong>Produtos</strong> não pertence a nenhum combo ";
                }
                else
                {
                    lblprodutofilho.Text = "<strong>Produto filho não cadastrado</strong>";
                }

            }

        }
    }

    protected void btnPesquisarCategoriaProduto_OnClick(object sender, ImageClickEventArgs e)
    {
        int produtoId = Convert.ToInt32(txtProdutoId.Text);
        litProdutoNome.Text = "";
        try
        {
            using (var data = new dbCommerceDataContext())
            {
                var dados = (from c in data.tbProdutoCategorias
                             join d in data.tbJuncaoProdutoCategorias on c.categoriaId equals d.categoriaId
                             where d.produtoId == produtoId
                             select new
                             {
                                 c.categoriaId,
                                 c.categoriaPaiId,
                                 c.categoriaNome,
                                 d.produtoId,
                                 d.tbProduto.produtoNome
                             });

                lstCategoriaProduto.DataSource = dados;
                lstCategoriaProduto.DataBind();

                containergrid2.Attributes.Add("style", "display:block;");

                var firstOrDefault = dados.FirstOrDefault();
                if (firstOrDefault != null) litProdutoNome.Text = firstOrDefault.produtoNome;
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('ERRO: " + ex.Message + "!');", true);
        }


    }

    protected void btnExportar_Click(object sender, EventArgs e)
    {
        try
        {

            SLDocument sl = new SLDocument();
            SLStyle style1 = sl.CreateStyle();
            style1.Font.Bold = true;
            sl.SetRowStyle(1, style1);

            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("Content-Disposition", "attachment; filename=produtosRelacionados.xlsx");

            sl.SetCellValue("A1", "produtoId");
            sl.SetCellValue("B1", "produtoIdDaEmpresa");
            sl.SetCellValue("C1", "produtoNome");
            sl.SetCellValue("D1", "produtoPreco");
            sl.SetCellValue("E1", "produtoPrecoDeCusto");
            sl.SetCellValue("F1", "produtoPrecoPromocional");
            sl.SetCellValue("G1", "margemDeLucro");
            sl.SetCellValue("H1", "produtoFornecedor");
            sl.SetCellValue("I1", "produtoAtivo");
            sl.SetCellValue("J1", "produtoDataDaCriacao");
            sl.SetCellValue("K1", "complementoIdDaEmpresa");
            sl.SetCellValue("L1", "incluidoDeCombo");
            sl.SetCellValue("M1", "produtoPeso");
            sl.SetCellValue("N1", "totalVendaCombo");
            sl.SetCellValue("O1", "totalPrecoDeCombo");

            using (var data = new dbCommerceDataContext())
            {

                IEnumerable<meusprodutos> dados = null;
                var buscaNormalizada = txtBusca.Text.Replace(Environment.NewLine, ",").Replace(";", ",").Replace(" ", ",").Replace(",,", ",").TrimEnd(',').Trim();
                var buscaSplit = buscaNormalizada.Split(',');
                var buscaInt = buscaSplit.Select(x => Convert.ToInt32(x));

                dados = (from c in data.tbProdutoRelacionados
                                join p in data.tbProdutos on c.idProdutoPai equals p.produtoId
                                join combo in data.tbProdutoRelacionados on c.idProdutoPai equals combo.idProdutoPai into combos
                                where buscaInt.Contains(c.idProdutoFilho)
                                select new meusprodutos
                                {
                                    produtoId = p.produtoId,
                                    produtoIdDaEmpresa = p.produtoIdDaEmpresa,
                                    produtoNome = p.produtoNome,
                                    produtoPreco = p.produtoPreco,
                                    produtoPrecoDeCusto = p.produtoPrecoDeCusto,
                                    produtoPrecoPromocional = p.produtoPrecoPromocional,
                                    margemDeLucro = p.margemDeLucro,
                                    produtoFornecedor = p.produtoFornecedor,
                                    produtoAtivo = p.produtoAtivo,
                                    produtoDataDaCriacao = p.produtoDataDaCriacao,
                                    complementoIdDaEmpresa = p.complementoIdDaEmpresa,
                                    incluidoDeCombo = "False",
                                    produtoPeso = p.produtoPeso,
                                    totalVendaCombo = (combos.Sum(x => (decimal)x.tbProduto.produtoPrecoPromocional > 0 ? (decimal)x.tbProduto.produtoPrecoPromocional : x.tbProduto.produtoPreco)),
                                    totalPrecoDeCombo = (combos.Sum(x => x.tbProduto.produtoPreco))
                                }).ToList<meusprodutos>();                 
                


                
                int linha = 2;
                foreach (var item in dados)
                {
                    sl.SetCellValue(linha, 1, item.produtoId);
                    sl.SetCellValue(linha, 2, item.produtoIdDaEmpresa);
                    sl.SetCellValue(linha, 3, item.produtoNome);
                    sl.SetCellValue(linha, 4, item.produtoPreco);
                    sl.SetCellValue(linha, 5, item.produtoPrecoDeCusto.ToString());
                    sl.SetCellValue(linha, 6, item.produtoPrecoPromocional.ToString());
                    sl.SetCellValue(linha, 7, item.margemDeLucro);
                    sl.SetCellValue(linha, 8, item.produtoFornecedor);
                    sl.SetCellValue(linha, 9, item.produtoAtivo);
                    sl.SetCellValue(linha, 10, item.produtoDataDaCriacao.ToString());
                    sl.SetCellValue(linha, 11, item.complementoIdDaEmpresa);
                    sl.SetCellValue(linha, 12, item.incluidoDeCombo);
                    sl.SetCellValue(linha, 13, item.produtoPeso);
                    sl.SetCellValue(linha, 14, item.totalVendaCombo);
                    sl.SetCellValue(linha, 15, item.totalPrecoDeCombo);
                    linha++;
                }
                //GridView1.DataSource = dados;
                //GridView1.DataBind();
            }

            //Exporta 
            sl.SaveAs(Response.OutputStream);
            Response.End();
        }
        catch (Exception ex)
        {

            throw;
        }
    }
}