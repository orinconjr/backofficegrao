﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

public partial class admin_comprasOrdensAguardandoFinanceiro : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        FillGrid(false);
    }

    protected void btnPesquisar_Click(object sender, ImageClickEventArgs e)
    {
        FillGrid(true);
    }

    private void FillGrid(bool rebind)
    {
        var data = new dbCommerceDataContext();
        var pedidos = (from c in data.tbComprasOrdems
                       join d in data.tbComprasOrdemProdutos on c.idComprasOrdem equals d.idComprasOrdem into produtos
                       where c.statusOrdemCompra == 3
                       select new
                       {
                           c.idComprasOrdem,
                           c.dataCriacao,
                           c.idUsuarioCadastroOrdem,
                           c.idComprasEmpresa,
                           c.idComprasFornecedor,
                           itens = produtos.Count(),
                           valor = produtos.Count(x => x.status == 1) > 0 ? produtos.Where(x => x.status == 1).Sum(x => (x.precoRecebido ?? x.preco) * (x.quantidadeRecebida ?? x.quantidade)) : 0,
                           colCondPag = c.idComprasCondicaoPagamento == null ? "Nenhuma selecionada" : c.idComprasCondicaoPagamento.ToString()
                       });
        grd.DataSource = pedidos;
        if ((!Page.IsPostBack && !Page.IsCallback) | rebind) grd.DataBind();
    }


    private int RetornaIdUsuarioLogado()
    {
        HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
        usuarioLogadoId = Request.Cookies["usuarioLogadoId"];
        if (usuarioLogadoId == null)
        {
            return 0;
        }

        return Convert.ToInt32(usuarioLogadoId.Value);
    }

    protected void btPdf_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WritePdfToResponse();
    }
    protected void btXsl_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteXlsToResponse();
    }
    protected void btRtf_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteRtfToResponse();
    }
    protected void btCsv_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteCsvToResponse();
    }

    protected void btnEnviarAprovacao_OnCommand(object sender, CommandEventArgs e)
    {
        int idComprasOrdem = Convert.ToInt32(e.CommandArgument);
        var data = new dbCommerceDataContext();
        var pedido = (from c in data.tbComprasOrdems where c.idComprasOrdem == idComprasOrdem select c).First();
        pedido.statusOrdemCompra = 4; //Enviado para Financeiro
        pedido.idUsuarioLancamentoFinanceiro = RetornaIdUsuarioLogado();
        if (pedido.dataEnvioFinanceiro == null) pedido.dataEnvioFinanceiro = DateTime.Now;

        var pedidoHistorico = new tbComprasOrdemHistorico();
        pedidoHistorico.data = DateTime.Now;
        pedidoHistorico.usuario = rnUsuarios.retornaNomeUsuarioLogado();
        pedidoHistorico.idComprasOrdem = pedido.idComprasOrdem;
        pedidoHistorico.descricao = "Pedido marcado como Lançado pelo Financeiro";
        data.tbComprasOrdemHistoricos.InsertOnSubmit(pedidoHistorico);
        data.SubmitChanges();

        FillGrid(true);
    }

    protected void btnExportar_OnCommand(object sender, CommandEventArgs e)
    {
        Response.Redirect("comprasOrdemDetalhe.aspx?id=" + e.CommandArgument);
    }

    protected void btnVerCondicoes_OnCommand(object sender, CommandEventArgs e)
    {
        usrComprasOrdemCondicoes.IdComprasOrdem = Convert.ToInt32(e.CommandArgument);
        usrComprasOrdemCondicoes.FillCondicoes();
        popCondicoesPagamento.ShowOnPageLoad = true;
    }
}