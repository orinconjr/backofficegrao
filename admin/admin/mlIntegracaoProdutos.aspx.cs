﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FtpLib;
using Newtonsoft.Json;

public partial class admin_mlIntegracaoProdutos : System.Web.UI.Page
{
    private static double progress;
    private static string servico;

    protected void Page_Load(object sender, EventArgs e)
    {
        //HttpContext.Current.Session["mlAccessToken"] = "";
        //HttpContext.Current.Session["mlRefreshToken"] = "";
        /*var meli = mlIntegracao.verificaAutenticacao("mlIntegracaoProdutos.aspx", Request.QueryString["code"]);
        var p = new RestSharp.Parameter();
        p.Name = "access_token";
        p.Value = meli.AccessToken;
        var ps = new List<RestSharp.Parameter>();
        ps.Add(p);

        if (Request.QueryString["cancelar"] != null)
        {
            var integracoesDc = new dbCommerceDataContext();
            var produtosMercadoLivre = (from c in integracoesDc.tbMlProdutos select c);
            foreach (var produto in produtosMercadoLivre)
            {

                var obj =
                    new
                    {
                        status = "closed"
                    };
                RestSharp.IRestResponse r = meli.Put("/items/" + produto.id, ps, obj);
            }
        }*/
    }
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.ASPxCallback.CallbackEventArgs e)
    {
        enviarProdutos();
    }

    protected void ASPxCallback2_Callback(object source, DevExpress.Web.ASPxCallback.CallbackEventArgs e)
    {
        e.Result = progress.ToString("0.0000").Replace(",", ".");
    }


    private void enviarProdutos()
    {
        HttpContext.Current.Server.ScriptTimeout = 60000;

        var meli = mlIntegracao.verificaAutenticacao("mlIntegracaoProdutos.aspx", Request.QueryString["code"]);
        var p = new RestSharp.Parameter();
        p.Name = "access_token";
        p.Value = meli.AccessToken;
        var ps = new List<RestSharp.Parameter>();
        ps.Add(p);

        var produto = new StringBuilder();

        var dataProdutos = new dbCommerceDataContext();
        var integracoesDc = new dbCommerceDataContext();

        dataProdutos.CommandTimeout = 0;
        var produtos = (from c in dataProdutos.tbProdutos
                        join d in dataProdutos.tbMlProdutos on c.produtoId equals d.idProduto into mlProdutos
                        where c.ativoMercadoLivre == true | mlProdutos.Count() > 0
                        select new
                        {
                            c.produtoId,
                            c.produtoPaiId,
                            c.produtoEstoqueAtual,
                            c.produtoEstoqueMinimo,
                            c.produtoUrl,
                            c.produtoNome,
                            c.produtoNomeNovo,
                            c.produtoPrecoPromocional,
                            c.produtoLegendaAtacado,
                            c.produtoPrecoAtacado,
                            c.produtoFreteGratis,
                            c.produtoPromocao,
                            c.produtoLancamento,
                            c.produtoMarca,
                            c.produtoDataDaCriacao,
                            c.produtoPreco,
                            c.produtoIdDaEmpresa,
                            c.produtoComposicao,
                            c.produtoFios,
                            c.produtoPecas,
                            c.produtoBrindes,
                            c.produtoDescricao,
                            c.produtoPeso,
                            c.fotoDestaque,
                            c.produtoPrincipal,
                            c.produtoFornecedor,
                            c.produtoAtivo,
                            c.marketplaceEnviarMarca,
                            c.marketplaceCadastrar,
                            c.largura,
                            c.altura,
                            c.profundidade,
                            c.disponibilidadeEmEstoque,
                            c.ativoMercadoLivre
                        });

        int totalProdutos = produtos.Count();
        int atual = 1;
        StringBuilder erros = new StringBuilder();
        foreach (var prod in produtos)
        {
            var descricaoCompleta = new StringBuilder();
            var dataInformacao = new dbCommerceDataContext();
            var informacoesAdicional = (from c in dataInformacao.tbInformacaoAdcionals where c.produtoId == prod.produtoId orderby c.informacaoAdcionalNome descending select c).ToList();
            foreach (var informacaoAdcional in informacoesAdicional)
            {
                descricaoCompleta.AppendFormat("<div style='font-family: Arial; font-size: 26px; font-weight: bold; color: #686A39; border-bottom: 1px solid #E6E7C4; padding-bottom: 10px; margin-top: 20px; margin-bottom: 10px;'>{0}</div>", informacaoAdcional.informacaoAdcionalNome);
                descricaoCompleta.AppendFormat("<div>{0}</div>", informacaoAdcional.informacaoAdcionalConteudo);
            }
            descricaoCompleta = descricaoCompleta.Replace("class=\"templateDetalheTabela\"", "style='color: #333333;font-family: Arial;font-size: 13px;margin: 0;padding: 0;width:100%;font-weight:bold;'");
            descricaoCompleta = descricaoCompleta.Replace("class=\"templateDetalheTituloGeralBloco\"", "style='color: #7D8043;font-size: 14px;font-weight: bold;padding-bottom: 3px;padding-left: 7px;'");
            descricaoCompleta = descricaoCompleta.Replace("class=\"templateDetalheTituloBloco\"", "style='border-bottom: 1px dotted #BEC165;font-size: 16px;font-weight: bold;padding-bottom: 3px;padding-left: 7px;'");
            descricaoCompleta = descricaoCompleta.Replace("class=\"templateDetalheTexto\"", "style='height: 24px;'");
            descricaoCompleta = descricaoCompleta.Replace("class=\"templateDetalheNome\"", "style='padding-bottom: 10px;padding-left: 7px;padding-top: 10px;width: 156px;'");
            descricaoCompleta = descricaoCompleta.Replace("class=\"templateDetalheQuantidade\"", "style='border-left: 1px solid #CCCCCC;border-right: 1px solid #CCCCCC;text-align: center;width: 40px;'");
            descricaoCompleta = descricaoCompleta.Replace("class=\"templateDetalheDescricao\"", "style='padding-left: 7px;'");
            descricaoCompleta = descricaoCompleta.Replace("class=\"templateDetalheTitulo\"", "style='background: none repeat scroll 0 0 #E6E7C4;font-weight: bold;height: 24px;'");
            descricaoCompleta = descricaoCompleta.Replace("class=\"templateDetalheNomeDetalhe\"", "style='padding-bottom: 10px;padding-left: 7px;padding-top: 10px;width: 156px;'");
            descricaoCompleta = descricaoCompleta.Replace("class=\"templateDetalheQuantidadeDetalhe\"", "style='border-left: 1px solid #CCCCCC;border-right: 1px solid #CCCCCC;padding-bottom: 10px;padding-top: 10px;text-align: center;width: 40px;'");
            descricaoCompleta = descricaoCompleta.Replace("class=\"templateDetalheDescricaoDetalhe\"", "style='padding-bottom: 10px;padding-left: 30px;padding-top: 10px;'");

            var produtoImportado = (from c in integracoesDc.tbMlProdutos where c.idProduto == prod.produtoId && c.stop_time > DateTime.Now select c).FirstOrDefault();

            if (prod.ativoMercadoLivre == true)
            {
                if (produtoImportado == null)
                {
                    var dataProd = new dbCommerceDataContext();
                    var categoria = (from c in dataProd.tbJuncaoProdutoCategorias
                        where
                            c.produtoId == prod.produtoId && c.tbProdutoCategoria.exibirSite == true &&
                            c.tbProdutoCategoria.categoriaPaiId == 0 && c.tbProdutoCategoria.categoriaMl != "" &&
                            c.tbProdutoCategoria.categoriaMl != null && c.categoriaId != 610
                        select c).FirstOrDefault();
                    if (categoria != null)
                    {
                            var produtoMl = new mlProduto();

                            string produtoDescricao = descricaoCompleta.ToString();
                            if (string.IsNullOrEmpty(produtoDescricao))
                            {
                                produtoDescricao = prod.produtoDescricao.Replace(Environment.NewLine, "<br />");
                                if (string.IsNullOrEmpty(produtoDescricao)) produtoDescricao = prod.produtoNome;
                            }
                            produtoDescricao = produtoDescricao.Replace(Environment.NewLine, "");
                            produtoDescricao = rnIntegracoes.removeLinks(produtoDescricao);
                            if (string.IsNullOrEmpty(produtoDescricao)) produtoDescricao = prod.produtoNome;
                            produtoDescricao = mlIntegracao.montaHtmlDescricao(prod.disponibilidadeEmEstoque.Replace("mais prazo de entrega exposto no carrinho de compras", "mais prazo de entrega"),
                                produtoDescricao);
                            decimal precoPromocional = prod.produtoPreco;
                            if (prod.produtoPrecoPromocional > 0 && prod.produtoPrecoPromocional < prod.produtoPreco)
                                precoPromocional = (decimal) prod.produtoPrecoPromocional;

                            string produtoNome = string.IsNullOrEmpty(prod.produtoNomeNovo)
                                ? prod.produtoNome
                                : prod.produtoNomeNovo;
                            produtoMl.title = produtoNome;
                            if (mlIntegracao.mlHomologacao) produtoMl.title = "TESTE NÃO COMPRE - " + produtoNome;
                            //produtoMl.subtitle = "A maior loja de Decoração de Quarto de Bebê do Brasil";
                            produtoMl.category_id = categoria.tbProdutoCategoria.categoriaMl;
                            produtoMl.official_store_id = mlIntegracao.mlOfficialStoreId;
                            produtoMl.price = Convert.ToDecimal(precoPromocional.ToString("0.00"));
                            produtoMl.currency_id = "BRL";
                            int estoque = prod.produtoEstoqueAtual > 999 ? 999 : prod.produtoEstoqueAtual;
                            produtoMl.available_quantity = estoque;
                            produtoMl.buying_mode = "buy_it_now";
                            produtoMl.listing_type_id = "gold_pro";
                            produtoMl.condition = "new";
                            produtoMl.description = produtoDescricao;
                            produtoMl.video_id = "";
                            produtoMl.warranty = "";

                            //produtoExtra.Weight = Convert.ToDouble(Convert.ToDecimal(prod.produtoPeso) / 100);
                            //produtoExtra.Length = Convert.ToDouble(Convert.ToDecimal(prod.profundidade) / 100);
                            //produtoExtra.Width = Convert.ToDouble(Convert.ToDecimal(prod.largura) / 100);
                            //produtoExtra.Height = Convert.ToDouble(Convert.ToDecimal(prod.altura) / 100);
                            //int prazoAdicional = 0;
                            //if (fornecedor.fornecedorPrazoDeFabricacao != null)
                            //{
                            //    prazoAdicional = (int)fornecedor.fornecedorPrazoDeFabricacao;
                            //}
                            //produtoExtra.handlingTime = prazoAdicional;


                        var fotos = new List<mlPicture>();
                            var fotoPrincipal = new mlPicture();
                            //fotoPrincipal.source =
                            //    System.Configuration.ConfigurationManager.AppSettings["caminhoVirtual"].ToString() +
                            //    "fotos\\" + prod.produtoId + "\\" + prod.fotoDestaque + ".jpg";
                            fotoPrincipal.source =
                                "https://dmhxz00kguanp.cloudfront.net/" +
                                "fotos/" + prod.produtoId + "/" + prod.fotoDestaque + ".jpg";
                        if (!string.IsNullOrEmpty(fotoPrincipal.source)) fotos.Add(fotoPrincipal);
                            var fotosDc = new dbCommerceDataContext();
                            var produtoFotos =
                                (from c in fotosDc.tbProdutoFotos
                                    where c.produtoId == prod.produtoId && c.produtoFoto != prod.fotoDestaque
                                    select c);
                        int totalFotosPermitidas = 11;
                        if (prod.produtoBrindes != "")
                        {
                            totalFotosPermitidas = 10;
                        }
                        foreach (var produtoFoto in produtoFotos)
                        {
                            if (fotos.Count < totalFotosPermitidas)
                            {
                                var foto = new mlPicture();
                                //foto.source =
                                //    System.Configuration.ConfigurationManager.AppSettings["caminhoVirtual"].ToString() +
                                //    "fotos/" + prod.produtoId + "/" + produtoFoto.produtoFoto + ".jpg";
                                foto.source =
                                    "https://dmhxz00kguanp.cloudfront.net/" +
                                    "fotos/" + prod.produtoId + "/" + produtoFoto.produtoFoto + ".jpg";
                                if (!string.IsNullOrEmpty(foto.source)) fotos.Add(foto);
                            }
                        }
                        /*if (prod.produtoBrindes != "")
                        {
                            var foto = new mlPicture();
                            foto.source = System.Configuration.ConfigurationManager.AppSettings["caminhoVirtual"].ToString() + "mercadoLivre/brindes" + prod.produtoBrindes + ".jpg";
                            if (!string.IsNullOrEmpty(foto.source)) fotos.Add(foto);
                                
                        }*/
                        var fotoLogo = new mlPicture();
                        fotoLogo.source = System.Configuration.ConfigurationManager.AppSettings["caminhoVirtual"].ToString() + "imagens/mercadoLivre/logo.jpg";
                        fotos.Add(fotoLogo);

                        produtoMl.pictures = fotos;
                        /*if (categoria.tbProdutoCategoria.categoriaMl != "MLB40593" && categoria.tbProdutoCategoria.categoriaMl != "MLB40595")
                        {
                            if (prod.produtoBrindes == "4")
                            {
                                produtoNome = produtoNome + " - 4 brindes";
                            }
                            if (produtoMl.price > 499)
                            {
                                produtoMl.shipping = mlIntegracao.retornaShippingGratis(prod.largura, prod.altura,
                                    prod.profundidade, Convert.ToInt32(prod.produtoPeso));
                                produtoNome = produtoNome + " - Frete Grátis! Grão de Gente";
                                if (produtoNome.Length > 200) produtoNome.Replace(" - Frete Grátis! Grão de Gente", "");
                            }
                            else
                            {
                                
                            }
                        }*/
                        produtoMl.shipping = mlIntegracao.retornaShippingPadrao(prod.largura, prod.altura, prod.profundidade, Convert.ToInt32(prod.produtoPeso));


                        RestSharp.IRestResponse r = meli.Post("/items", ps, produtoMl);
                        /*Response.Write(r.Content);
                        return;*/


                        dynamic retorno = JsonConvert.DeserializeObject(r.Content);
                        var dataFim = DateTime.Now;
                        try
                        {
                            dataFim = Convert.ToDateTime(retorno.start_time);
                        }
                        catch (Exception)
                        {
                            
                        }
                        if (dataFim > DateTime.Now.AddDays(-1))
                        {
                            var produtoMlGravar = new tbMlProduto();
                            produtoMlGravar.id = retorno.id;
                            produtoMlGravar.site_id = retorno.site_id;
                            produtoMlGravar.title = retorno.title;
                            produtoMlGravar.sold_quantity = 0;
                            produtoMlGravar.permalink = retorno.permalink;
                            produtoMlGravar.idProduto = prod.produtoId;
                            produtoMlGravar.start_time = Convert.ToDateTime(retorno.start_time);
                            produtoMlGravar.stop_time = Convert.ToDateTime(retorno.stop_time);
                            Response.Write(produtoMlGravar.start_time);
                            Response.Write(produtoMlGravar.stop_time);
                            Response.Write(produtoMlGravar.site_id);
                            integracoesDc.tbMlProdutos.InsertOnSubmit(produtoMlGravar);
                            integracoesDc.SubmitChanges();
                        }
                        else
                        {
                            erros.AppendFormat("ID do Produto: {0}<br>", prod.produtoId);
                            string erro = r.Content.Contains("length")
                                ? "Nome deve ser inferior a 60 caracteres"
                                : r.Content;
                            erros.AppendFormat("Erro: {0}<br><br>", erro);
                        }
                    }
                }
                else
                {
                    var dataProd = new dbCommerceDataContext();
                    var categoria = (from c in dataProd.tbJuncaoProdutoCategorias
                        where
                            c.produtoId == prod.produtoId && c.tbProdutoCategoria.exibirSite == true &&
                            c.tbProdutoCategoria.categoriaPaiId == 0 && c.tbProdutoCategoria.categoriaMl != "" &&
                            c.tbProdutoCategoria.categoriaMl != null && c.categoriaId != 610
                        select c).FirstOrDefault();
                    if (categoria != null)
                    {
                        var produtoMl = new mlProduto();

                        decimal precoPromocional = prod.produtoPreco;
                        if (prod.produtoPrecoPromocional > 0 && prod.produtoPrecoPromocional < prod.produtoPreco) precoPromocional = (decimal) prod.produtoPrecoPromocional;
                        string produtoNome = string.IsNullOrEmpty(prod.produtoNomeNovo) ? prod.produtoNome : prod.produtoNomeNovo;

                        /*if (categoria.tbProdutoCategoria.categoriaMl != "MLB40593")
                        {
                            if (prod.produtoBrindes == "4")
                            {
                                produtoNome = produtoNome + " - 4 brindes";
                            }
                            if (produtoMl.price > 499)
                            {
                                produtoNome = produtoNome + " - Frete Grátis! Grão de Gente";
                            }
                            else
                            {
                                produtoNome = produtoNome + " - Grão de Gente";
                            }
                        }*/
                        produtoMl.title = produtoNome;
                        //produtoMl.subtitle = "A maior loja de Decoração de Quarto de Bebê do Brasil";
                        produtoMl.price = Convert.ToDecimal(precoPromocional.ToString("0.00"));
                        int estoque = prod.produtoEstoqueAtual > 999 ? 999 : prod.produtoEstoqueAtual;
                        produtoMl.available_quantity = estoque;
                        produtoMl.shipping = mlIntegracao.retornaShippingPadrao(prod.largura, prod.altura, prod.profundidade, Convert.ToInt32(prod.produtoPeso));

                        var obj = new
                            {
                                price = Convert.ToDecimal(precoPromocional.ToString("0.00")),
                                available_quantity = estoque,
                                shipping = mlIntegracao.retornaShippingPadrao(prod.largura, prod.altura, prod.profundidade, Convert.ToInt32(prod.produtoPeso))
                        };
                        RestSharp.IRestResponse r = meli.Put("/items/" + produtoImportado.id, ps, obj);

                        HttpContext.Current.Response.Write(r.Content.ToString());
                    }
                }
            }
            else
            {
                if (produtoImportado != null)
                {
                    var obj =
                    new
                    {
                        status = "closed"
                    };
                    RestSharp.IRestResponse r = meli.Put("/items/" + produtoImportado.id, ps, obj);
                }
            }
            progress = Convert.ToDouble(Convert.ToDecimal(atual) * 100 / totalProdutos);
            atual++;
        }
        litExportacao.Text = erros.ToString();
    }

    protected void ASPxButton2_OnClick(object sender, EventArgs e)
    {
        enviarProdutos();
    }
}