﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="produtoCalculo.aspx.cs" Inherits="admin_produtoCalculo" %>

<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>

<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>

<%@ Register TagPrefix="dxe" Namespace="DevExpress.Web.ASPxEditors" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script language="JavaScript" type="text/javascript" src="js/funcoes.js"></script>
    <script type="text/javascript">
            function GetClientId(strid) {
                var count = document.forms[0].length;
                var i = 0;
                var eleName;
                for (i = 0; i < count; i++) {
                    eleName = document.forms[0].elements[i].id;
                    pos = eleName.indexOf(strid);
                    if (pos >= 0) break;
                }
                return eleName;
            }

            function CalculoMargemConcorrencia(txtCustoConcorrencia, lblCustoConcorrencia, txtCusto) {
                var obj_txtCustoConcorrencia = document.getElementById(txtCustoConcorrencia);
                var obj_lblCustoConcorrencia = document.getElementById(lblCustoConcorrencia);
                var obj_txtCusto = document.getElementById(txtCusto);
                var valorConcorrencia = parseFloat(obj_txtCustoConcorrencia.value.replace(',','.'));
                var valorCusto = parseFloat(obj_txtCusto.value.replace(',', '.'));
                obj_lblCustoConcorrencia.innerHTML = (((valorConcorrencia * 100) / valorCusto) - 100).toFixed(1) + '%';
            }

            function CalculoMargemVenda(txtPrecoPromocional, lblPrecoPromocional, lblPrecoVenda, txtCusto) {
                var obj_txtPrecoPromocional = document.getElementById(txtPrecoPromocional);
                var obj_lblPrecoPromocional = document.getElementById(lblPrecoPromocional);
                var obj_lblPrecoVenda = document.getElementById(lblPrecoVenda);
                var obj_txtCusto = document.getElementById(txtCusto);
                var valorPrecoPromocional = parseInt(obj_txtPrecoPromocional.value);
                var valorCusto = parseFloat(obj_txtCusto.value.replace(',', '.'));
                var valorPromocao = ((valorPrecoPromocional / 100) * valorCusto) + valorCusto;
                var valorVenda = (valorPromocao * 0.3) + valorPromocao;
                obj_lblPrecoPromocional.innerHTML = valorPromocao.toFixed(2);
                obj_lblPrecoVenda.innerHTML = valorVenda.toFixed(2);
            }

            function PrecoIgual(txtPrecoPromocional) {
                var obj_txtPrecoPromocional = document.getElementById(txtPrecoPromocional);
                for (i = 0; i < 100; i++) {
                    var obj_txtPrecoPromocional2 = document.getElementById('grid_cell' + i + '_5_txtPrecoPromocional');
                    document.getElementById('grid_cell' + i + '_5_txtPrecoPromocional').value = obj_txtPrecoPromocional.value;
                }
                return false;              
            }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <dx:ASPxGridView ID="grid" runat="server" AutoGenerateColumns="False" 
            DataSourceID="sqlProdutos" Width="100%" 
            onhtmlrowcreated="grid_HtmlRowCreated" KeyFieldName="produtoId" EnableCallBacks="False">
            <Columns>
                <dx:GridViewDataTextColumn Caption="ID" FieldName="produtoId" Name="id"
                    VisibleIndex="1" Width="50px">
                    <DataItemTemplate>                        
                        <asp:TextBox ID="txtId" runat="server" Text='<%# Eval("produtoId") %>' Enabled="false" Width="50px"></asp:TextBox>
                    </DataItemTemplate>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="ID da Empresa" 
                    FieldName="produtoIdDaEmpresa" ReadOnly="True" VisibleIndex="2" 
                    Width="100px">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Nome" FieldName="produtoNome" Name="nome"
                    ReadOnly="True" VisibleIndex="3">
                    <DataItemTemplate>                        
                        <asp:TextBox ID="txtNome" runat="server" Text='<%# Eval("produtoNome") %>' Width="300"></asp:TextBox><br />
                        <asp:TextBox ID="txtNomeAntigo" runat="server" Text='<%# Eval("produtoNome") %>' Visible="false"></asp:TextBox><br />
                    </DataItemTemplate>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataComboBoxColumn Caption="Fornecedor" 
                    FieldName="produtoFornecedor" ReadOnly="True" VisibleIndex="0" 
                    Width="100px">
                    <PropertiesComboBox DataSourceID="sqlFornecedor" TextField="fornecedorNome" 
                        ValueField="fornecedorId" ValueType="System.String">
                    </PropertiesComboBox>
                </dx:GridViewDataComboBoxColumn>
                <dx:GridViewDataTextColumn Caption="Preço Concorrência" 
                    Name="PrecoConcorrencia" VisibleIndex="2" Width="100px">
                    <DataItemTemplate>                        
                        <asp:TextBox ID="txtPrecoConcorrencia" runat="server"></asp:TextBox><br />
                        Margem: <asp:Label ID="lblMargemDaConcorrencia" runat="server" Text=""></asp:Label>                        
                    </DataItemTemplate>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Preço Promocional" Name="PrecoPromocional" 
                    VisibleIndex="2" Width="180px">
                    <DataItemTemplate>                        
                        <asp:TextBox ID="txtPrecoPromocional" runat="server"></asp:TextBox><asp:Button ID="btnIgual" runat="server" Text="\/" /><br />
                        Preço Promocional: <asp:Label ID="lblPrecoPromocional" runat="server" Text=""></asp:Label><br />                     
                        Preço Venda: <asp:Label ID="lblPrecoVenda" runat="server" Text=""></asp:Label>                   
                    </DataItemTemplate>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Custo" FieldName="produtoPrecoDeCusto" 
                    Name="Custo" VisibleIndex="4" Width="100px">
                    <DataItemTemplate>                        
                        <asp:TextBox ID="txtCusto" runat="server" Text='<%# Eval("produtoPrecoDeCusto", "{0:C}").Replace("R$","").Trim() %>'></asp:TextBox><br />
                        <asp:TextBox ID="txtCustoAntigo" runat="server" Visible="false" Text='<%# Eval("produtoPrecoDeCusto", "{0:C}").Replace("R$","").Trim() %>'></asp:TextBox><br />
                    </DataItemTemplate>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataCheckColumn Caption="Ativo" FieldName="produtoAtivo" 
                    ReadOnly="True" VisibleIndex="9" Width="50px" Name="ativo">
                    <DataItemTemplate>
                        <dx:ASPxCheckBox ID="ckbAtivo" runat="server" Value='<%# Bind("produtoAtivo") %>' ValueChecked="True" ValueType="System.String" ValueUnchecked="False">
                        </dx:ASPxCheckBox>
                        <dx:ASPxCheckBox ID="ckbAtivoAntigo" Visible="false" runat="server" Value='<%# Bind("produtoAtivo") %>' ValueChecked="True" ValueType="System.String" ValueUnchecked="False">
                        </dx:ASPxCheckBox>
                    </DataItemTemplate>
                </dx:GridViewDataCheckColumn>
                <dx:GridViewDataTextColumn Caption="Estoque" FieldName="produtoEstoqueAtual" Name="estoque" 
                    ReadOnly="True" VisibleIndex="5" Width="50px">
                    <DataItemTemplate>                        
                        <asp:TextBox ID="txtEstoque" runat="server" Text='<%# Eval("produtoEstoqueAtual") %>' Width="60"></asp:TextBox><br />
                        <asp:TextBox ID="txtEstoqueAntigo" Visible="false" runat="server" Text='<%# Eval("produtoEstoqueAtual") %>'></asp:TextBox><br />
                    </DataItemTemplate>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Peso" FieldName="produtoPeso" Name="peso" 
                    ReadOnly="True" VisibleIndex="7" Width="50px">
                    <DataItemTemplate>                        
                        <asp:TextBox ID="txtPeso" runat="server" Text='<%# Eval("produtoPeso") %>'></asp:TextBox><br />
                        <asp:TextBox ID="txtPesoAntigo" Visible="false" runat="server" Text='<%# Eval("produtoPeso") %>'></asp:TextBox><br />
                    </DataItemTemplate>
                </dx:GridViewDataTextColumn>
            </Columns>
            <SettingsPager PageSize="100">
            </SettingsPager>
            <Settings ShowFilterRow="True" />
        </dx:ASPxGridView>
        <asp:LinqDataSource ID="sqlProdutos" runat="server" 
            ContextTypeName="dbCommerceDataContext" EntityTypeName="" 
            Select="new (produtoId, produtoIdDaEmpresa, produtoNome, produtoFornecedor, produtoPrecoDeCusto, produtoAtivo, produtoEstoqueAtual, produtoPeso)" 
            TableName="tbProdutos">
        </asp:LinqDataSource>
        <br />
        <asp:SqlDataSource ID="sqlFornecedor" runat="server" 
            ConnectionString="<%$ ConnectionStrings:connectionString %>" 
            SelectCommand="SELECT [fornecedorId], [fornecedorNome] FROM [tbProdutoFornecedor] ORDER BY [fornecedorNome]">
        </asp:SqlDataSource>
        <asp:ImageButton ID="btnGravar" runat="server" ImageUrl="images/btSalvar.jpg" 
            onclick="btnGravar_Click" />
    </div>
    </form>
</body>
</html>
