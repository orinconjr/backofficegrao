﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="simularFrete.aspx.cs" Inherits="admin_simularFrete" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" runat="Server">
    <style>
        .fieldsetatualizarprecos {
            width: 760px;
            float: left;
            clear: left;
            border-radius: 5px;
            margin-left: 25px;
        }

            .fieldsetatualizarprecos input[type=checkbox] {
                margin-top: 15px;
            }

            .fieldsetatualizarprecos label {
                margin-left: 5px;
                margin-right: 20px;
            }

        .meugrid {
            background-color: #F4FAFB;
            border: 0px none;
            /* border-collapse: separate !important; */
            overflow: hidden;
            font: 9pt Tahoma;
            color: #000;
            cursor: pointer;
            /* white-space: nowrap;*/
            font-weight: normal;
            text-align: left;
            width: 834px;
            border-collapse: collapse;
            float: left;
            clear: left;
            margin-top: 10px;
            margin-bottom: 10px;
            min-height: 500px;
            margin-left: 25px;
        }

            .meugrid th {
                background-image: url(../../App_Themes/Glass/GridView/gvHeaderBackground.gif);
                background-position: center top;
                background-repeat: repeat-x;
                background-color: #C0DDE0;
                overflow: hidden;
                height: 23px;
                line-height: 23px;
                padding: 4px 6px 5px;
                font-weight: normal;
                border: 1px solid #7EACB1;
                /* border-collapse: separate;*/
            }

            .meugrid td {
                overflow: hidden;
                height: 23px;
                line-height: 23px;
                padding: 4px 6px 5px;
                border: 1px solid #7EACB1;
            }

            .meugrid tr td:nth-child(4) {
                text-align: center;
            }

            .meugrid tr td:last-of-type {
                text-align: center;
            }

        .tabrotulos {
            width: 848px;
            margin: auto;
        }

        .materiaprima {
            width: 250px;
        }

            .materiaprima ul {
                margin: 0px;
                padding-left: 10px;
            }

                .materiaprima ul li {
                    max-width: 230px;
                    white-space: pre-wrap;
                }

        .meugrid tr td:last-of-type {
            text-align: center;
        }

        .nome {
            width: 237px;
        }

            .nome div {
                max-width: 230px;
                white-space: pre-wrap;
            }

        .rotulos td, .rotulos th {
            font: bold 12px tahoma;
            padding-top: 20px;
            color: rgb(65, 105, 126);
            padding-left: 5px;
            padding-bottom: 5px;
            text-align: left;
        }

        .rotulos td {
            font: normal 12px tahoma;
        }
        /*****************************/
        .width150 {
        }

        .height150 {
        }

        .height80 {
        }

        .btn {
            -webkit-border-radius: 7px;
            -moz-border-radius: 7px;
            border-radius: 2px;
            font-family: Arial;
            color: #ffffff;
            font-size: 14px;
            background: #63c269;
            padding: 5px 10px 5px 10px;
            text-decoration: none;
            cursor: pointer;
            border: 0px;
        }

            .btn:hover {
                background: #a4dbab;
                text-decoration: none;
                color: black;
                font-weight: bold;
            }


        .marginTop5porcento {
            margin: 6%;
            font-size: 15px;
        }

        .quadroPrincipal {
            float: left;
            width: 150px;
            height: 155px;
            text-align: center;
            position: relative;
            font-family: monospace;
            border: 1px solid #B1B1B1;
            margin: 0 14px 5px 14px;
            border-radius: 15px;
        }

        .quadroInteriorQtds {
            margin-top: 5%;
            text-align: center;
            font-size: 30px;
        }

        .AlinharAoRodapeCentralizar {
            position: absolute;
            bottom: 5px;
            text-align: center;
            width: 150px;
        }
    </style>

    <div class="tituloPaginas" valign="top">
        Simular Frete
        
    </div>

    <table class="tabrotulos">

        <tr class="rotulos">
            <th>CEP:<br />
                <asp:TextBox runat="server" ID="txtCep"></asp:TextBox>
            </th>

            <th>&nbsp;
            </th>

            <th>Valor:<br />
                <asp:TextBox runat="server" ID="txtValor"></asp:TextBox>
            </th>

            <th>&nbsp;
            </th>

            <th>&nbsp;
            </th>
        </tr>

        <tr class="rotulos">
            <th>Peso ( Em gramas ):<br />
                <asp:TextBox runat="server" ID="txtPeso"></asp:TextBox>
            </th>

            <th>Altura:<br />
                <asp:TextBox runat="server" ID="txtAltura"></asp:TextBox>
            </th>

            <th>Largura:<br />
                <asp:TextBox runat="server" ID="txtLargura"></asp:TextBox>
            </th>

            <th>comprimento:<br />
                <asp:TextBox runat="server" ID="txtComprimento"></asp:TextBox>
            </th>

            <th>
                <br />
                <asp:Button ID="btnadicionarpacote" runat="server" Text="Adicionar Pacote" OnClick="btnadicionarpacote_Click" />
            </th>

        </tr>

        <tr class="rotulos" id="trtitulopacotes" runat="server" visible="false">
            <th colspan="5"><br /><br /><span style="text-decoration:underline">Pacotes</span></th>
        </tr>
        <tr class="rotulos" id="thtitulopacotes" runat="server" visible="false">
            <th>Peso:
            </th>

            <th>Altura:
            </th>

            <th>Largura:
            </th>

            <th>Comprimento:
            </th>

            <th>
                <asp:TextBox runat="server" ID="hdfLista" Visible="False" />
            </th>


        </tr>

        <asp:ListView runat="server" ID="lstPacotes">
            <ItemTemplate>
                <tr class="rotulos">
                    <td><%# Eval("peso") %>
                    </td>

                    <td><%# Eval("altura") %>
                    </td>

                    <td><%# Eval("largura") %>
                    </td>

                    <td><%# Eval("comprimento") %>
                    </td>

                    <td>
                        <asp:LinkButton runat="server" ID="btnRemover" OnCommand="btnRemover_OnCommand" CommandArgument="<%# Container.DataItemIndex %>">Remover</asp:LinkButton>
                    </td>


                </tr>
            </ItemTemplate>
        </asp:ListView>

        <tr class="rotulos">
            <td colspan="4"></td>

            <td>
                <br />
                <asp:Button ID="btnSimular" runat="server" Text="Simular" Visible="false" OnClick="btnSimular_Click" />
            </td>
        </tr>
    </table>

    <table class="tabrotulos" >

        <tr class="rotulos" id="tbFretes"  runat="server" visible="false">
            <th>Transportadora:                
            </th>

            <th>Prazo:
            </th>

            <th>Valor:
            </th>
        </tr>
        <asp:ListView runat="server" ID="lstCalculos">
            <ItemTemplate>
                <tr class="rotulos" style="font-weight: normal;">
                    <td><%# Eval("transportadora") %>
                    </td>

                    <td><%# Eval("calculoFrete.prazo") %>
                    </td>

                    <td><%# Eval("calculoFrete.valor", "{0:c}") %>
                </tr>
            </ItemTemplate>
        </asp:ListView>
    </table>

</asp:Content>

