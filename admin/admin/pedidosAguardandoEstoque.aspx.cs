﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Data.Filtering;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;

public partial class admin_pedidosAguardandoEstoque : System.Web.UI.Page
{
    DateTemplate dateTemplate = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        fillGrid(false);
    }

    private void fillGrid(bool rebind)
    {
        var data = new dbCommerceDataContext();
        var pedidos = (from c in data.tbItemPedidoAguardandoEstoques
                       join d in data.tbClientes on c.tbItensPedido.tbPedido.clienteId equals d.clienteId
                       where c.reservado == false && (c.tbItensPedido.cancelado ?? false) == false && (c.tbItensPedido.enviado ?? false) == false && ((c.tbItensPedido.tbPedido.statusDoPedido == 2 && c.tbItensPedido.tbPedido.tipoDePagamentoId == 11) | c.tbItensPedido.tbPedido.statusDoPedido == 3 | c.tbItensPedido.tbPedido.statusDoPedido == 4 | c.tbItensPedido.tbPedido.statusDoPedido == 11) 
                       select new
                       {
                           c.tbItensPedido.pedidoId,
                           d.clienteNome,
                           c.tbProduto.produtoNome,
                           c.dataSolicitacao,
                           c.tbProduto.tbProdutoFornecedor.fornecedorNome,
                           c.tbProduto.tbProdutoFornecedor.fornecedorId
                       });
        grd.DataSource = pedidos;
        if ((!Page.IsPostBack && !Page.IsCallback) | rebind)
        {
            grd.DataBind();
        }


    }
    protected void btPdf_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WritePdfToResponse();
    }
    protected void btXsl_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteXlsToResponse();
    }
    protected void btRtf_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteRtfToResponse();
    }
    protected void btCsv_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteCsvToResponse();
    }

    protected void grid_AutoFilterCellEditorCreate(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewEditorCreateEventArgs e)
    {
        ASPxGridView grid = sender as ASPxGridView;
        if (e.Column.FieldName == "dataSolicitacao")
        {
            DropDownEditProperties dde = new DropDownEditProperties();
            dde.EnableClientSideAPI = true;
            dateTemplate = new DateTemplate();
            dde.DropDownWindowTemplate = dateTemplate;
            e.EditorProperties = dde;
        }
        if (e.Column.FieldName == "vencimentoParcela")
        {
            DropDownEditProperties dde = new DropDownEditProperties();
            dde.EnableClientSideAPI = true;
            dateTemplate = new DateTemplate();
            dde.DropDownWindowTemplate = dateTemplate;
            e.EditorProperties = dde;
        }
    }
    protected void grid_AutoFilterCellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
    {
        ASPxGridView grid = sender as ASPxGridView;
        //if (e.Column.FieldName == "dataSolicitacao")
        //{
        //    ASPxDropDownEdit dde = e.Editor as ASPxDropDownEdit;
        //    dde.ClientSideEvents.CloseUp = String.Format("function (s, e) {{ ApplyFilter(s, {0}, {1}); }}", dateTemplate.cIdFrom, dateTemplate.cIdTo);
        //    //dde.ClientSideEvents.DropDown = String.Format("function (s, e) {{ OnDropDown(s, {0}, {1}); }}", dateTemplate.cIdFrom, dateTemplate.cIdTo);
        //    dde.ReadOnly = true;
        //}
        if (e.Column.FieldName == "dataSolicitacao")
        {
            ASPxDropDownEdit dde = e.Editor as ASPxDropDownEdit;
            dde.ClientSideEvents.CloseUp = String.Format("function (s, e) {{ ApplyFilter(s, {0}, {1}, 'dataSolicitacao'); }}", dateTemplate.cIdFrom, dateTemplate.cIdTo);
            //dde.ClientSideEvents.DropDown = String.Format("function (s, e) {{ OnDropDown(s, {0}, {1}); }}", dateTemplate.cIdFrom, dateTemplate.cIdTo);
            dde.ReadOnly = true;
        }
        if (e.Column.FieldName == "vencimentoParcela")
        {
            ASPxDropDownEdit dde = e.Editor as ASPxDropDownEdit;
            dde.ClientSideEvents.CloseUp = String.Format("function (s, e) {{ ApplyFilter(s, {0}, {1}, 'vencimentoParcela'); }}", dateTemplate.cIdFrom, dateTemplate.cIdTo);
            //dde.ClientSideEvents.DropDown = String.Format("function (s, e) {{ OnDropDown(s, {0}, {1}); }}", dateTemplate.cIdFrom, dateTemplate.cIdTo);
            dde.ReadOnly = true;
        }
    }

    protected void grid_ProcessColumnAutoFilter(object sender, ASPxGridViewAutoFilterEventArgs e)
    {
        if (e.Value == "|") return;
        //if (e.Column.FieldName == "dataSolicitacao")
        //{
        //    if (e.Kind == GridViewAutoFilterEventKind.CreateCriteria)
        //    {
        //        Session["dataSolicitacao"] = e.Value;
        //        String[] dates = e.Value.Split('|');
        //        DateTime dateFrom = Convert.ToDateTime(dates[0]),
        //            dateTo = Convert.ToDateTime(dates[1]);
        //        e.Criteria = (new OperandProperty("dataSolicitacao") >= dateFrom) &
        //                     (new OperandProperty("dataSolicitacao") <= dateTo);
        //    }
        //    else
        //    {
        //        if (Session["dataSolicitacao"] != null)
        //            e.Value = Session["dataSolicitacao"].ToString();
        //    }
        //}
        if (e.Column.FieldName == "dataSolicitacao")
        {
            if (e.Kind == GridViewAutoFilterEventKind.CreateCriteria)
            {
                Session["dataSolicitacao"] = e.Value;
                String[] dates = e.Value.Split('|');
                DateTime dateFrom = Convert.ToDateTime(dates[0]),
                    dateTo = Convert.ToDateTime(dates[1]);
                e.Criteria = (new OperandProperty("dataSolicitacao") >= dateFrom) &
                             (new OperandProperty("dataSolicitacao") <= dateTo);
            }
            else
            {
                if (Session["dataSolicitacao"] != null)
                    e.Value = Session["dataSolicitacao"].ToString();
            }
        }
        if (e.Column.FieldName == "vencimentoParcela")
        {
            if (e.Kind == GridViewAutoFilterEventKind.CreateCriteria)
            {
                Session["vencimentoParcela"] = e.Value;
                String[] dates = e.Value.Split('|');
                DateTime dateFrom = Convert.ToDateTime(dates[0]),
                    dateTo = Convert.ToDateTime(dates[1]);
                e.Criteria = (new OperandProperty("vencimentoParcela") >= dateFrom) &
                             (new OperandProperty("vencimentoParcela") <= dateTo);
            }
            else
            {
                if (Session["vencimentoParcela"] != null)
                    e.Value = Session["vencimentoParcela"].ToString();
            }
        }
    }


    protected void grd_OnCustomUnboundColumnData(object sender, ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "pedidoIdCliente")
        {
            int pedidoId = Convert.ToInt32(e.GetListSourceFieldValue("pedidoId"));
            e.Value = rnFuncoes.retornaIdCliente(pedidoId);
        }
    }
    
}
