﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="pedidoFornecedorEditar.aspx.cs" Inherits="admin_pedidoFornecedorEditar" %>

<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxTimer" TagPrefix="dxt" %>
<%@ Register Assembly="CuteEditor" Namespace="CuteEditor" TagPrefix="CE" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" runat="Server">
    <script type="text/javascript">
        function toggle() {
            var divs = document.querySelectorAll("[id]");
            for (var i = 0, len = divs.length; i < len; i++) {
                var div = divs[i];
                if (div.id.indexOf("chkEntregue") > -1) {
                    div.checked = true;
                }
            }
        }

        function somaTotalCusto() {

            var totalCamposCusto = document.getElementById('<%=hfTotalCamposCusto.ClientID%>').value;
            var somaTotal = 0;

            for (var i = 0; i < totalCamposCusto; i++) {
                if (document.getElementById('ctl00_conteudoPrincipal_lstProdutos_ctrl' + i + '_txtCusto')) {
                    var valor = document.getElementById('ctl00_conteudoPrincipal_lstProdutos_ctrl' + i + '_txtCusto').value.replace(".", "").replace(",", ".");
                    var qtd = document.getElementById('ctl00_conteudoPrincipal_lstProdutos_ctrl' + i + '_lblQtdProdutos').innerHTML;
                    somaTotal += (parseFloat(valor) * parseInt(qtd));
                }
            }

            var totalDiferenca = document.getElementById('divTotalDiferenca').innerHTML.replace("R$", "");

            document.getElementById('divTotalCusto').innerHTML = "R$ " + (somaTotal).formatMoney(2, ',', '.');
            document.getElementById('divTotalGeral').innerHTML = "R$ " + somaTotal.formatMoney(2, ',', '.');

        }

        function somaTotalDiferenca() {

            var totalCamposDiferenca = document.getElementById('<%=hfTotalCamposCusto.ClientID%>').value;
            var somaTotal = 0;

            for (var i = 0; i < totalCamposDiferenca; i++) {
                if (document.getElementById('ctl00_conteudoPrincipal_lstProdutos_ctrl' + i + '_txtDiferenca')) {
                    var valor = document.getElementById('ctl00_conteudoPrincipal_lstProdutos_ctrl' + i + '_txtDiferenca').value;
                    var qtd = document.getElementById('ctl00_conteudoPrincipal_lstProdutos_ctrl' + i + '_lblQtdProdutos').innerHTML;
                    somaTotal += (parseFloat(valor) * parseInt(qtd));
                }
            }

            var totalCusto = document.getElementById('divTotalCusto').innerHTML.replace("R$", "");

            document.getElementById('divTotalDiferenca').innerHTML = "R$ " + (somaTotal).formatMoney(2, ',', '.');
            //document.getElementById('divTotalGeral').innerHTML = "R$ " + somaTotal.formatMoney(2, ',', '.');

        }

        Number.prototype.formatMoney = function (c, d, t) {
            var n = this, c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "," : d, t = t == undefined ? "." : t, s = n < 0 ? "-" : "", i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
            return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
        };
    </script>

    <table cellpadding="0" cellspacing="0" style="width: 920px">

        <tr>
            <td class="tituloPaginas" valign="top">
                <asp:Label ID="lblAcao" runat="server">Pedidos ao Fornecedor</asp:Label>
            </td>
        </tr>

        <tr>
            <td>

                <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
                    <tr class="rotulos">
                        <td>
                            <table style="width: 100%; text-align: center;">
                                <tr class="rotulos">
                                    <td><b>Data de Entrega do Pedido:</b></td>
                                    <td><b>Número da Cobrança</b></td>
                                    <td><b>Data de Vencimento da Cobrança:</b></td>
                                    <td><b>Data de Pagamento da Cobrança</b></td>
                                    <td><b>Data prevista pelo Fornecedor</b></td>
                                    <td><b>Hora prevista pelo Fornecedor</b></td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:TextBox runat="server" ID="txtDataEntrega" CssClass="campos"></asp:TextBox></td>
                                    <td>
                                        <asp:TextBox runat="server" ID="txtNumeroCobranca" CssClass="campos"></asp:TextBox></td>
                                    <td>
                                        <asp:TextBox runat="server" ID="txtDataVencimento" CssClass="campos"></asp:TextBox></td>
                                    <td>
                                        <asp:TextBox runat="server" ID="txtDataPagamento" CssClass="campos"></asp:TextBox></td>
                                    <td>
                                        <dxe:ASPxDateEdit ID="txtDataPrevista" runat="server" Width="150px">
                                        </dxe:ASPxDateEdit>
                                        <asp:HiddenField ID="hdfDataPrevista" runat="server" />
                                    </td>
                                    <td>
                                        <dxe:ASPxTimeEdit ID="txtHoraPrevista" runat="server" Width="100px">
                                        </dxe:ASPxTimeEdit>
                                    </td>
                                </tr>
                                <tr class="rotulos">
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td><strong>Motivo do Atraso</strong></td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>
                                        <asp:HiddenField ID="hdfidPedidoFornecedorMotivoAtraso" runat="server" />
                                        <asp:DropDownList ID="ddlMotivoAtraso" runat="server" AutoPostBack="True"></asp:DropDownList>
                                    </td>
                                    <td><asp:Button runat="server" ID="btnGravarAlteracoes" Text="Gravar Alterações" OnClick="btnGravarAlteracoes_OnClick" /></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr class="rotulos" style="font-size: 16px;">
                        <td>
                            <b>Data do Pedido:</b>
                            <asp:Literal runat="server" ID="litDataPedido"></asp:Literal><br />
                            <b>Data de Entrega:</b>
                            <asp:Literal runat="server" ID="litDataEntrega"></asp:Literal><br />
                            <b>Nº do Pedido:</b>
                            <asp:Literal runat="server" ID="litNumeroPedido"></asp:Literal><br />
                            <b>Fornecedor:</b>
                            <asp:Literal runat="server" ID="litFornecedor"></asp:Literal><br />
                            <b>Faltando entregar:</b>
                            <asp:Literal runat="server" ID="litTotalFaltandoEntregar"></asp:Literal><br />
                        </td>
                    </tr>
                    <td>&nbsp;
                    </td>
        </tr>
        <tr class="rotulos">
            <td></td>
        </tr>
        <tr class="rotulos">
            <td></td>
        </tr>
        <tr>
            <td style="height: 50px;">&nbsp;
            </td>
        </tr>
        <tr>
            <td>
                <table style="width: 100%;">
                    <tr class="rotulos" style="text-align: center; font-weight: bold;">
                        <td>&nbsp;
                        </td>
                        <td>&nbsp;
                        </td>
                        <td>&nbsp;
                        </td>
                        <td>&nbsp;
                        </td>
                        <td>&nbsp;
                        </td>
                        <td>&nbsp;
                        </td>
                        <td>&nbsp;
                        </td>
                        <td>&nbsp;
                        </td>
                        <td style="text-align: right;">
                            <asp:Button runat="server" OnClientClick="toggle(); return false;" Text="Checar todos" />
                        </td>
                    </tr>
                    <tr class="rotulos" style="text-align: center; font-weight: bold;">
                        <td>Foto</td>
                        <td>ID do Produto</td>
                        <td>ID da Empresa
                        </td>
                        <td>Complemento ID
                        </td>
                        <td>Nome
                        </td>
                        <td>Quantidade
                        </td>
                        <td>Custo
                        </td>
                        <td>Diferença
                        </td>
                        <td>Pedidos
                        </td>
                        <td>Entregue
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <asp:ListView runat="server" ID="lstProdutos" OnItemDataBound="lstProdutos_OnItemDataBound">
                        <ItemTemplate>
                            <tr class="rotulos" style="text-align: center;">
                                <td style="padding-bottom: 3px; padding-top: 3px; border-bottom: 1px solid #b0e0e6;">                                                
                                    <asp:HyperLink runat="server" ID="hlFoto" Target="_blank" NavigateUrl='<%# "http://dmhxz00kguanp.cloudfront.net/fotos/" + Eval("produtoId") + "/" + Eval("fotoDestaque") + ".jpg" %>' Text="Foto" ></asp:HyperLink>
                                </td>
                                <td style="padding-bottom: 3px; padding-top: 3px; border-bottom: 1px solid #b0e0e6;">
                                    <%# Eval("produtoId") %>
                                </td>    
                                <td style="padding-bottom: 3px; padding-top: 3px; border-bottom: 1px solid #b0e0e6;">
                                    <asp:HiddenField runat="server" ID="hiddenProdutoId" Value='<%# Eval("produtoId") %>' />
                                    <asp:Literal runat="server" ID="litProdutoIdDaEmpresa" Text='<%# Eval("produtoIdDaEmpresa") %>'></asp:Literal>
                                </td>
                                <td style="padding-bottom: 3px; padding-top: 3px; border-bottom: 1px solid #b0e0e6;">
                                    <asp:Literal runat="server" ID="litComplementoIdDaEmpresa" Text='<%# Eval("complementoIdDaEmpresa") %>'></asp:Literal>&nbsp;
                                </td>
                                <td style="padding-bottom: 3px; padding-top: 3px; border-bottom: 1px solid #b0e0e6;">
                                    <%# Eval("produtoNome") %>
                                </td>
                                <td style="padding-bottom: 3px; padding-top: 3px; border-bottom: 1px solid #b0e0e6;">
                                    <asp:Label runat="server" ID="lblQtdProdutos" Text='<%# Eval("quantidade") %>'></asp:Label>
                                </td>
                                <td style="padding-bottom: 3px; padding-top: 3px; border-bottom: 1px solid #b0e0e6;">
                                    <asp:TextBox runat="server" CssClass="campos" ID="txtCusto" onblur="somaTotalCusto()" Text='<%# Convert.ToDecimal(Eval("custo")).ToString("0.00") %>' Width="70"></asp:TextBox>
                                </td>
                                <td style="padding-bottom: 3px; padding-top: 3px; border-bottom: 1px solid #b0e0e6;">
                                    <asp:TextBox runat="server" CssClass="campos" ID="txtDiferenca" onblur="somaTotalDiferenca()" Text='<%# Convert.ToDecimal(Eval("diferenca")).ToString("0.00") %>' Width="70"></asp:TextBox>
                                </td>
                                 <td style="padding-bottom: 3px; padding-top: 3px; border-bottom: 1px solid #b0e0e6;">
                                     <asp:HyperLink ID="hplPedidos"  runat="server" Target="_blank">Pedidos</asp:HyperLink>
                                </td>
                                <td style="padding-bottom: 3px; padding-top: 3px; border-bottom: 1px solid #b0e0e6;">
                                    <asp:ListView runat="server" ID="lstEntregas" OnItemDataBound="lstEntregas_OnItemDataBound">
                                        <ItemTemplate>
                                            <asp:HiddenField runat="server" ID="hdfItemPedidoId" Value='<%# Eval("idPedidoFornecedorItem") %>' />
                                            <asp:CheckBox runat="server" ID="chkEntregue" Checked='<%# Convert.ToBoolean(Eval("entregue")) %>' />
                                        </ItemTemplate>
                                    </asp:ListView>

                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:ListView>
                    <tr class="rotulos" style="text-align: center; font-weight: bold;">
                        <td>&nbsp;
                        </td>
                        <td>&nbsp;
                        </td>
                        <td>&nbsp;
                        </td>
                        <td>
                            <asp:Literal runat="server" ID="litTotalQuantidade"></asp:Literal>
                        </td>
                        <td>
                            <div id="divTotalCusto">
                                <asp:Literal runat="server" ID="litTotalCusto"></asp:Literal>
                            </div>
                            <asp:HiddenField ID="hfTotalCamposCusto" runat="server" />
                        </td>
                        <td>
                            <div id="divTotalDiferenca">
                                <asp:Literal runat="server" ID="litTotalDiferenca"></asp:Literal>
                            </div>
                        </td>
                        <td>
                            <div id="divTotalGeral">
                                <asp:Literal runat="server" ID="litTotalGeral"></asp:Literal>
                            </div>
                        </td>
                    </tr>
                    <asp:ObjectDataSource ID="sql" runat="server" SelectMethod="pedidoFornecedorPorPedidoFornecedorIndividualId"
                        TypeName="rnPedidos">
                        <SelectParameters>
                            <asp:QueryStringParameter DefaultValue="0" QueryStringField="idPedidoFornecedor" Name="idPedidoFornecedor" Type="Int32" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </table>
            </td>
        </tr>
        <tr>
            <td style="text-align: right;">&nbsp;
            </td>
        </tr>
        <tr>
            <td style="text-align: right;">
                <asp:Button runat="server" ID="btnGravar" Text="Gravar" OnClick="btnGravar_OnClick" />
            </td>
        </tr>
        <tr>
            <td style="padding-top: 100px;">&nbsp;
            </td>
        </tr>
        <tr>
            <td style="border: 1px solid #7EACB1; padding-left: 15px; padding-bottom: 10px;">
                <table cellpadding="0" cellspacing="0" style="width: 774px">
                    <tr>
                        <td style="padding-top: 20px; padding-right: 20px;" class="rotulos">
                            <b>Interação</b><ce:editor id="txtInteracao" runat="server"
                                autoconfigure="Full_noform" contextmenumode="Simple"
                                editorwysiwygmodecss="" filespath="" height="400px"
                                urltype="Default" userelativelinks="False"
                                width="782px"
                                removeservernamesfromurl="False">
                                                  <TextAreaStyle CssClass="campos" />
                                                 <%-- <FrameStyle BackColor="White" BorderColor="#DDDDDD" BorderStyle="Solid" 
                                                      BorderWidth="1px" CssClass="CuteEditorFrame" Height="100%" Width="100%" />--%>
                                              </ce:editor></td>
                    </tr>
                    <tr>
                        <td class="rotulos" valign="middle" align="right" style="padding-top: 5px; padding-bottom: 5px; padding-right: 20px;">
                            <asp:ImageButton ID="btSalvarInteracao" runat="server" ImageUrl="images/btSalvarPeq1.jpg" OnClick="btSalvarInteracao_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:DataList ID="dtlInteracao" runat="server" CellPadding="0">
                                <ItemTemplate>
                                    <table cellpadding="0" cellspacing="0" class="rotulos" style="width: 782px">
                                        <tr>
                                            <td height="25" style="padding-left: 20px; width: 140px; border-top-style: solid; border-top-width: 1px; border-top-color: #7EACB1;">
                                                <asp:Label ID="lblData" runat="server" Text='<%# Bind("data") %>'></asp:Label>
                                            </td>
                                            <td style="width: 200px; border-top-style: solid; border-top-width: 1px; border-top-color: #7EACB1;">
                                                <asp:Label ID="lblusuario" runat="server"
                                                    Text='<%# Bind("usuario") %>'></asp:Label>
                                            </td>
                                            <td style="border-top-style: solid; border-width: 1px; border-color: #7EACB1">
                                                <asp:Literal ID="ltrInteracao" runat="server" Text='<%# Bind("interacao") %>'></asp:Literal>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                                <HeaderTemplate>
                                    <table cellpadding="0" cellspacing="0" class="rotulos" style="width: 782px">
                                        <tr bgcolor="#D4E3E6">
                                            <td height="25" style="padding-left: 20px; width: 140px;">
                                                <b>Data:</b></td>
                                            <td style="width: 200px">
                                                <b>Usuário:</b></td>
                                            <td>
                                                <b>Interação:</b></td>
                                        </tr>
                                    </table>
                                </HeaderTemplate>
                            </asp:DataList>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </td>
    </tr>
</table>
</asp:Content>
