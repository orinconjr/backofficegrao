﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxGridView;

public partial class admin_chamados : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        CarregarGrid();
        if (!Page.IsPostBack)
        {
            CarregarComboDepartamento();
            CarregarComboAssunto();
            CarregarUsuarioDepartamento(0);
        }
    }

    private void CarregarComboAssunto()
    {
        var data = new dbCommerceDataContext();

        var assuntos = (from c in data.tbChamadoAssuntos select c).ToList();

        ddlAssunto.DataSource = assuntos;
        ddlAssunto.DataTextField = "nomeChamadoAssunto";
        ddlAssunto.DataValueField = "idChamadoAssunto";
        ddlAssunto.DataBind();

        ddlAssunto.Items.Insert(0, new ListItem() { Text = "Selecione", Value = "0" });
        ddlAssunto.SelectedIndex = 0;

        ddlAssunto.Enabled = true;
    }

    private void CarregarGrid()
    {

        HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
        usuarioLogadoId = Request.Cookies["usuarioLogadoId"];

        grd.DataSource = null;
        grd.DataBind();

        bool concluido = false;

        if (rdbAbertos.Checked)
            concluido = false;
        else
            concluido = true;

        var data = new dbCommerceDataContext();

        var DepartamentoUsuario = (from c in data.tbChamadoDepartamentoUsuarios
                                   where c.usuarioId == Convert.ToInt32(usuarioLogadoId.Value)
                                   select c).Distinct().ToList();

        var nomes = string.Empty;

        foreach (var depto in DepartamentoUsuario)
        {
            nomes += (from c in data.tbChamadoDepartamentos where c.idChamadoDepartamento == depto.idDepartamento select c).FirstOrDefault().nomeChamadoDepartamento + "<br>";
        }

        lblSeusDepartamentos.Text = nomes;

        bool adminChamado = (from c in data.tbUsuarioPaginasPermitidas
                             where c.paginaPermitidaNome == "adminchamado" && c.usuarioId == Convert.ToInt32(usuarioLogadoId.Value)
                             select c).Any();

        bool gestorDepartamento = (from c in data.tbUsuarioPaginasPermitidas
                                   where c.paginaPermitidaNome == "gestordepartamento" && c.usuarioId == Convert.ToInt32(usuarioLogadoId.Value)
                                   select c).Any();

        dynamic chamados;

        if (rdbMeus.Checked)
            chamados = (from c in data.tbChamados
                        where (c.idUsuario == Convert.ToInt32(usuarioLogadoId.Value)) && (c.concluido == concluido)
                        select new
                        {
                            c.concluido,
                            c.dataLimite,
                            c.titulo,
                            c.descricao,
                            c.idChamado,
                            c.tbUsuario.usuarioNome,
                            c.idPedido,
                            c.idChamadoDepartamento,
                            c.idChamadoTarefa,
                            c.idChamadoAssunto,
                            c.prioridade,
                            c.idDepartamentoAbertura,
                            c.tbChamadoDepartamento.ordem
                        }).OrderByDescending(x => x.prioridade).ToList();

        else
        {
            if (adminChamado)
            {
                chamados = (from c in data.tbChamados
                            where (c.concluido == concluido)
                            select new
                            {
                                c.concluido,
                                c.dataLimite,
                                c.titulo,
                                c.descricao,
                                c.idChamado,
                                c.tbUsuario.usuarioNome,
                                c.idPedido,
                                c.idChamadoDepartamento,
                                c.idChamadoTarefa,
                                c.idChamadoAssunto,
                                c.idDepartamentoAbertura,
                                c.prioridade,
                                c.tbChamadoDepartamento.ordem
                            }).OrderByDescending(x => x.prioridade).ToList();
            }
            else if (gestorDepartamento)
            {
                var idDepartamento = DepartamentoUsuario.Select(x => x.idDepartamento).ToList();

                chamados = (from c in data.tbChamados
                            where idDepartamento.Contains(c.idChamadoDepartamento ?? 0) &&
                            //(c.idChamadoDepartamento == DepartamentoUsuario.idDepartamento) &&
                            (c.concluido == concluido)
                            select new
                            {
                                c.concluido,
                                c.dataLimite,
                                c.titulo,
                                c.descricao,
                                c.idChamado,
                                c.tbUsuario.usuarioNome,
                                c.idPedido,
                                c.idChamadoDepartamento,
                                c.idChamadoTarefa,
                                c.idChamadoAssunto,
                                c.idDepartamentoAbertura,
                                c.tbChamadoDepartamento.ordem,
                                c.prioridade
                            }).OrderByDescending(x => x.prioridade).ToList();
            }
            else
            {
                var idDepartamento = DepartamentoUsuario.Select(x => x.idDepartamento).ToList();

                chamados = (from c in data.tbChamados
                            where idDepartamento.Contains(c.idChamadoDepartamento ?? 0) &&
                            //(c.idChamadoDepartamento == DepartamentoUsuario.idDepartamento) &&
                            (c.idUsuario == Convert.ToInt32(usuarioLogadoId.Value) || c.idUsuario == null) && (c.concluido == concluido)
                            select new
                            {
                                c.concluido,
                                c.dataLimite,
                                c.titulo,
                                c.descricao,
                                c.idChamado,
                                c.tbUsuario.usuarioNome,
                                c.idPedido,
                                c.idChamadoDepartamento,
                                c.idChamadoTarefa,
                                c.idChamadoAssunto,
                                c.idDepartamentoAbertura,
                                c.prioridade,
                                c.tbChamadoDepartamento.ordem
                            }).OrderByDescending(x => x.prioridade).ToList();
            }
        }

        grd.DataSource = chamados;
        grd.DataBind();
    }

    private void CarregarComboDepartamento()
    {
        ddlUsuarioDepartamento.Items.Clear();

        var data = new dbCommerceDataContext();

        var deptos = data.tbChamadoDepartamentos.ToList();
        deptos.Insert(0, new tbChamadoDepartamento() { idChamadoDepartamento = 0, nomeChamadoDepartamento = "Selecione" });

        ddlDepartamento.DataSource = deptos.OrderBy(x => x.idChamadoDepartamento);
        ddlDepartamento.DataTextField = "nomeChamadoDepartamento";
        ddlDepartamento.DataValueField = "idChamadoDepartamento";
        ddlDepartamento.DataBind();

        ddlUsuarioDepartamento.Items.Insert(0, new ListItem() { Text = "Fila do Departamento", Value = null });
    }

    private void alteraSelectInteracao(string idChamado)
    {
        sqlChamadoInteracao.SelectParameters.Clear();
        sqlChamadoInteracao.SelectParameters.Add("idChamado", idChamado);
    }
    protected void rdb_OnCheckedChanged(object sender, EventArgs e)
    {
        CarregarGrid();
    }

    protected void btnAlterarChamado_OnCommand(object sender, CommandEventArgs e)
    {
        int idChamado = Convert.ToInt32(e.CommandArgument);
        var data = new dbCommerceDataContext();
        var chamado = (from c in data.tbChamados where c.idChamado == idChamado select c).FirstOrDefault();
        txtChamadoData.Text = chamado.dataLimite.ToShortDateString();
        txtChamadoHora.Text = chamado.dataLimite.ToShortTimeString();
        txtChamadoTitulo.Text = chamado.titulo;

        HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
        usuarioLogadoId = Request.Cookies["usuarioLogadoId"];
        //int idUsuario = Convert.ToInt32(usuarioLogadoId.Value.ToString());
        //chamado.idUsuario = idUsuario;
        //data.SubmitChanges();

        ddlPrioridade.SelectedValue = chamado.prioridade.ToString();

        if (chamado.idChamadoDepartamento != null)
        {
            ddlDepartamento.SelectedValue = chamado.idChamadoDepartamento.ToString();
            CarregarUsuarioDepartamento(chamado.idChamadoDepartamento.Value);
        }
        else
        {
            ddlDepartamento.SelectedIndex = 0;
            ddlAssunto.SelectedIndex = 0;
            ddlAssunto.Enabled = false;
            ddlTarefa.Enabled = false;
        }

        //CarregarUsuarioDepartamento(Convert.ToInt32(ddlDepartamento.SelectedValue));

        if (chamado.idChamadoTarefa != null)
        {
            ddlTarefa.SelectedValue = chamado.idChamadoTarefa.ToString();
            ddlAssunto.Enabled = true;
        }
        else
        {
            ddlAssunto.SelectedIndex = 0;
            ddlAssunto.Enabled = false;
            trTarefa.Visible = false;
        }

        if (chamado.idChamadoAssunto != null)
            ddlAssunto.SelectedValue = chamado.idChamadoAssunto.ToString();
        else
            ddlAssunto.SelectedIndex = 0;

        txtChamado.Text = "";
        chkChamadoConcluido.Checked = chamado.concluido;
        //ddlChamadoUsuario.SelectedValue = chamado.idUsuario.ToString();
        hdfIdAgendamento.Value = e.CommandArgument.ToString();
        popAdicionarChamado.ShowOnPageLoad = true;
        alteraSelectInteracao(idChamado.ToString());
        sqlChamadoInteracao.DataBind();
    }

    protected void grd_OnCustomUnboundColumnData(object sender, ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "pedidoIdCliente")
        {
            int pedidoId = Convert.ToInt32(e.GetListSourceFieldValue("idPedido"));
            e.Value = rnFuncoes.retornaIdCliente(pedidoId);
        }
    }
    protected void grd_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType != GridViewRowType.Data)
        {
            return;
        }

        var data = DateTime.Now;
        Label lblData = (Label)grd.FindRowCellTemplateControl(e.VisibleIndex, grd.Columns["dataLimite"] as GridViewDataColumn, "lblData");
        DateTime.TryParse(lblData.Text, out data);
        var hdfPedido = "";
        bool concluido = false;
        var hdfConcluido = e.GetValue("concluido");
        if (e.GetValue("idPedido") != null)
            hdfPedido = e.GetValue("idPedido").ToString();

        var prioridade = e.GetValue("prioridade");

        if (prioridade != null)
        {
            var lblPrioridade = (Label)grd.FindRowCellTemplateControl(e.VisibleIndex, grd.Columns["prioridade"] as GridViewDataColumn, "lblPrioridade");

            switch (int.Parse(prioridade.ToString()))
            {
                case 1:
                    lblPrioridade.Text = "Muito Baixo";
                    break;

                case 2:
                    lblPrioridade.Text = "Baixo";
                    break;

                case 3:
                    lblPrioridade.Text = "Normal";
                    break;

                case 4:
                    lblPrioridade.Text = "Alta";
                    break;

                case 5:
                    lblPrioridade.Text = "Urgente";
                    break;
            }
        }


        HyperLink hplPedido = (HyperLink)grd.FindRowCellTemplateControl(e.VisibleIndex, grd.Columns["verPedido"] as GridViewDataColumn, "hplPedido");
        if (string.IsNullOrEmpty(hdfPedido))
        {
            hplPedido.Visible = false;
        }
        else
        {
            hplPedido.NavigateUrl = "pedido.aspx?pedidoId=" + hdfPedido;
        }
        Boolean.TryParse(hdfConcluido.ToString(), out concluido);
        if (concluido)
        {
            int i = 0;
            while ((i < e.Row.Cells.Count))
            {
                e.Row.Cells[i].Style.Add("background", "#D2E9FF");
                i++;
            }
        }
        else if (data < DateTime.Now)
        {
            int i = 0;
            while ((i < e.Row.Cells.Count))
            {
                e.Row.Cells[i].Style.Add("background", "#FFFFFF");
                i++;
            }
        }
        else
        {
            int i = 0;
            while ((i < e.Row.Cells.Count))
            {
                e.Row.Cells[i].Style.Add("background", "#FFFFBB");
                i++;
            }
        }
        var departamentoOrigem = e.GetValue("idDepartamentoAbertura");
        if (departamentoOrigem != null)
        {
            if (int.Parse(departamentoOrigem.ToString()) == 9)
            {
                int i = 0;
                while ((i < e.Row.Cells.Count))
                {
                    e.Row.Cells[i].Style.Add("background", "#ff6666");
                    i++;
                }
            }
            else if (int.Parse(departamentoOrigem.ToString()) == 7)
            {
                int i = 0;
                while ((i < e.Row.Cells.Count))
                {
                    e.Row.Cells[i].Style.Add("background", "#ffcc99");
                    i++;
                }
            }
            else if (int.Parse(departamentoOrigem.ToString()) == 2)
            {
                int i = 0;
                while ((i < e.Row.Cells.Count))
                {
                    e.Row.Cells[i].Style.Add("background", "#0099e6");
                    i++;
                }
            }
            else if (int.Parse(departamentoOrigem.ToString()) == 19)
            {
                int i = 0;
                while ((i < e.Row.Cells.Count))
                {
                    e.Row.Cells[i].Style.Add("background", "#C39EBA");
                    i++;
                }
            }
        }
    }

    protected void btnGravarChamado_OnClick(object sender, EventArgs e)
    {
        HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
        usuarioLogadoId = Request.Cookies["usuarioLogadoId"];
        int idUsuario = Convert.ToInt32(usuarioLogadoId.Value.ToString());
        int idChamado = 0;
        int.TryParse(hdfIdAgendamento.Value.ToString(), out idChamado);
        int idDepartamento = Convert.ToInt32(ddlDepartamento.SelectedValue);
        int idTarefa = 0;

        if (ddlTarefa.SelectedIndex > 0)
            idTarefa = Convert.ToInt32(ddlTarefa.SelectedValue);

        int idAssunto = Convert.ToInt32(ddlAssunto.SelectedValue);
        var data = new dbCommerceDataContext();
        if (idChamado > 0)
        {
            string interacao = "";
            var chamado = (from c in data.tbChamados where c.idChamado == idChamado select c).FirstOrDefault();
            interacao += "Chamado " + chamado.titulo + "alterado:<br>";
            if (chamado.concluido != chkChamadoConcluido.Checked)
            {
                interacao += "Marcado como ";
                interacao += chkChamadoConcluido.Checked ? "concluído" : "não concluído";
                interacao += "<br>";
            }
            chamado.concluido = chkChamadoConcluido.Checked;

            if (chkChamadoConcluido.Checked)
            {
                chamado.dataConclusao = System.DateTime.Now;
                chamado.idUsuarioConclusao = int.Parse(usuarioLogadoId.Value);
            }

            var dataAgendamento = DateTime.Now;
            DateTime.TryParse(txtChamadoData.Text + " " + txtChamadoHora.Text, out dataAgendamento);
            if (chamado.dataLimite != dataAgendamento)
            {
                interacao += "Data para conclusão: " + dataAgendamento.ToString() + "<br>";
            }

            var idUsuarioChamado = 0;
            if (chkRetorno.Checked)
            {
                //var interaccaoChamado = (from c in data.tbChamadoInteracaos where c.idChamado == chamado.idChamado select c).OrderByDescending(x => x.idChamado).FirstOrDefault();
                var interaccaoChamado = (from c in data.tbPedidoInteracoes
                                         where c.pedidoId == chamado.idPedido
                                         where c.interacao.Contains("Chamado") && (c.interacao.Contains("criado") || c.interacao.Contains("Retorno de chamado")) && c.interacao.Contains(chamado.titulo)
                                         select new { c.usuarioQueInteragiu, c.interacaoId }).OrderByDescending(x => x.interacaoId).FirstOrDefault();
                if (interaccaoChamado != null)
                {
                    var usuarioAberturaChamado = (from c in data.tbUsuarios where c.usuarioNome == interaccaoChamado.usuarioQueInteragiu select c).FirstOrDefault();
                    if (usuarioAberturaChamado != null)
                    {
                        idUsuarioChamado = usuarioAberturaChamado.usuarioId;
                        interacao += "Retorno de chamado para: " + usuarioAberturaChamado.usuarioNome.ToString();

                    }
                }
                else
                {
                    var pedidoInteracao = (from c in data.tbPedidoInteracoes where c.pedidoId == chamado.idPedido select c).OrderByDescending(x => x.interacaoId).FirstOrDefault();
                    var usuario = (from u in data.tbUsuarios where u.usuarioNome == pedidoInteracao.usuarioQueInteragiu select u).FirstOrDefault();
                    if (pedidoInteracao != null)
                        idUsuarioChamado = usuario.usuarioId;
                }
            }
            else
            {
                var idUsuarioDepartamento = int.Parse(usuarioLogadoId.Value.ToString());

                if (!ddlUsuarioDepartamento.SelectedValue.Contains("Fila do departamento"))
                    idUsuarioDepartamento = int.Parse(ddlUsuarioDepartamento.SelectedValue);

                idUsuarioChamado = int.Parse(usuarioLogadoId.Value.ToString());

                if (idUsuarioDepartamento != idUsuarioChamado)
                    idUsuarioChamado = idUsuarioDepartamento;
                //if (chamado.idUsuario.ToString() != ddlChamadoUsuario.SelectedValue)
                //{

                var usuario = (from u in data.tbUsuarios where u.usuarioId == idUsuarioChamado select u).FirstOrDefault();

                if (usuario != null)
                    interacao += "Responsável alterado para: " + usuario.usuarioNome + "<br>";
                //}
            }

            chamado.dataLimite = dataAgendamento;
            chamado.titulo = txtChamadoTitulo.Text;
            chamado.idUsuario = idUsuarioChamado;
            data.SubmitChanges();

            var chamadoInteracao = new tbChamadoInteracao();

            //chamado.idChamadoDepartamento = chamado.idChamadoDepartamento
            //if (idTarefa > 0)
            //    chamado.idChamadoTarefa = chamado.idChamadoTarefa

            //chamado.idChamadoAssunto = idAssunto;
            chamadoInteracao.interacao = txtChamado.Text;
            chamadoInteracao.dataHora = DateTime.Now;
            chamadoInteracao.idChamado = chamado.idChamado;
            chamadoInteracao.idUsuario = idUsuario;
            data.tbChamadoInteracaos.InsertOnSubmit(chamadoInteracao);
            data.SubmitChanges();

            if (chamado.idPedido != null)
            {
                interacao += "Interação: " + txtChamado.Text;
                interacaoInclui(interacao, "False", (int)chamado.idPedido);
            }
        }
        else
        {
            string interacao = "";
            var chamado = new tbChamado();
            interacao += "Chamado " + chamado.titulo + "criado<br>";
            var dataAgendamento = DateTime.Now;
            DateTime.TryParse(txtChamadoData.Text + " " + txtChamadoHora.Text, out dataAgendamento);
            interacao += "Data para conclusão: " + dataAgendamento.ToString() + "<br>";
            interacao += "Responsável: " + ddlChamadoUsuario.SelectedItem.Text + "<br>";
            chamado.idChamadoDepartamento = idDepartamento;
            chamado.idChamadoTarefa = idTarefa;
            chamado.idChamadoAssunto = idAssunto;
            chamado.dataLimite = dataAgendamento;
            chamado.titulo = txtChamadoTitulo.Text;
            chamado.dataCadastro = DateTime.Now;
            chamado.descricao = txtChamado.Text;
            chamado.idUsuario = Convert.ToInt32(ddlChamadoUsuario.SelectedValue);
            data.tbChamados.InsertOnSubmit(chamado);
            data.SubmitChanges();

            if (chamado.idPedido != null)
            {
                interacao += "Descricao: " + txtChamado.Text;
                interacaoInclui(interacao, "False", (int)chamado.idPedido);
            }
        }
        popAdicionarChamado.ShowOnPageLoad = false;
        CarregarGrid();

    }

    protected void btnAdicionarChamado_OnClick(object sender, EventArgs e)
    {
        trAssunto.Visible = false;
        trTarefa.Visible = false;
        ddlAssunto.Enabled = false;
        ddlTarefa.Enabled = false;

        ddlAssunto.DataSource = null;
        ddlAssunto.DataBind();

        ddlTarefa.DataSource = null;
        ddlTarefa.DataBind();

        popAdicionarChamado.ShowOnPageLoad = true;
        chkChamadoConcluido.Checked = false;
        txtChamado.Text = "";
        hdfIdAgendamento.Value = "";
        HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
        usuarioLogadoId = Request.Cookies["usuarioLogadoId"];
        ddlChamadoUsuario.SelectedValue = usuarioLogadoId.Value;
        txtChamadoData.Text = DateTime.Now.ToShortDateString();
        txtChamadoHora.Text = DateTime.Now.ToShortTimeString();
    }

    protected void dtlChamados_OnItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            var data = DateTime.Now;
            Label lblData = (Label)e.Item.FindControl("lblData");
            DateTime.TryParse(lblData.Text, out data);

            bool concluido = false;
            HiddenField hdfConcluido = (HiddenField)e.Item.FindControl("hdfConcluido");
            HiddenField hdfPedido = (HiddenField)e.Item.FindControl("hdfPedido");
            HyperLink hplPedido = (HyperLink)e.Item.FindControl("hplPedido");
            if (string.IsNullOrEmpty(hdfPedido.Value))
            {
                hplPedido.Visible = false;
            }
            else
            {
                hplPedido.NavigateUrl = "pedido.aspx?pedidoId=" + hdfPedido.Value;
            }
            Boolean.TryParse(hdfConcluido.Value, out concluido);
            if (concluido)
            {
                e.Item.BackColor = ColorTranslator.FromHtml("#D2E9FF");
            }
            else if (data < DateTime.Now)
            {
                e.Item.BackColor = ColorTranslator.FromHtml("#FFC6C7");
            }
            else
            {
                e.Item.BackColor = ColorTranslator.FromHtml("#FFFFBB");
            }


        }
    }
    public void interacaoInclui(string interacao, string alteracaoDeEstatus, int pedidoId)
    {
        HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
        usuarioLogadoId = Request.Cookies["usuarioLogadoId"];
        if (usuarioLogadoId != null)
        {
            rnInteracoes.interacaoInclui(pedidoId, interacao, rnUsuarios.usuarioSeleciona_PorUsuarioId(int.Parse(usuarioLogadoId.Value.ToString())).Tables[0].Rows[0]["usuarioNome"].ToString(), alteracaoDeEstatus);
        }
    }

    protected void ddlDepartamento_SelectedIndexChanged(object sender, EventArgs e)
    {
        var data = new dbCommerceDataContext();

        if (ddlDepartamento.SelectedIndex > 0)
        {
            trAssunto.Visible = true;
            ddlTarefa.Enabled = false;
            CarregarComboAssunto();
            //CarregarUsuarioDepartamento(Convert.ToInt32(ddlDepartamento.SelectedValue));

            var usuarios = (from c in data.tbChamadoDepartamentoUsuarios
                            join d in data.tbUsuarios on c.usuarioId equals d.usuarioId
                            where c.idDepartamento == int.Parse(ddlDepartamento.SelectedValue)
                            select d).Distinct().ToList().OrderBy(x => x.usuarioNome);

            ddlUsuarioDepartamento.DataSource = usuarios;
            ddlUsuarioDepartamento.DataTextField = "usuarioNome";
            ddlUsuarioDepartamento.DataValueField = "usuarioId";
            ddlUsuarioDepartamento.DataBind();

            ddlUsuarioDepartamento.Items.Insert(0, new ListItem() { Text = "Fila do departamento", Value = null });
        }
        else
        {
            trAssunto.Visible = false;
            ddlAssunto.Enabled = false;
            ddlTarefa.Enabled = false;
        }
    }

    private void CarregarUsuarioDepartamento(int idDepartamento)
    {
        var data = new dbCommerceDataContext();

        var usuarioLogado = Request.Cookies["usuarioLogadoId"];

        dynamic usuarios;
        if (idDepartamento > 0)
            usuarios = data.tbUsuarios.Where(x => x.idDepartamento == idDepartamento || x.usuarioId == Convert.ToInt32(usuarioLogado.Value.ToString())).ToList().OrderBy(x => x.usuarioNome);
        else
            usuarios = data.tbUsuarios.ToList().OrderBy(x => x.usuarioNome);

        ddlUsuarioDepartamento.DataSource = usuarios;
        ddlUsuarioDepartamento.DataTextField = "usuarioNome";
        ddlUsuarioDepartamento.DataValueField = "usuarioId";
        ddlUsuarioDepartamento.DataBind();

        ddlUsuarioDepartamento.Items.Insert(0, new ListItem() { Text = "Fila do Departamento", Value = null });
        //alteraSelect();

    }

    private void CarregarComboTarefa()
    {
        var data = new dbCommerceDataContext();
        var Tarefas = data.tbChamadoTarefas.ToList();

        Tarefas.Add(new tbChamadoTarefa() { idChamadoTarefa = 0, nomeChamadoTarefa = "Selecione" });

        ddlTarefa.DataSource = Tarefas.OrderBy(x => x.idChamadoTarefa);
        ddlTarefa.DataTextField = "nomeCHamadoTarefa";
        ddlTarefa.DataValueField = "idChamadotarefa";
        ddlTarefa.DataBind();

    }

    protected void ddlTarefa_SelectedIndexChanged(object sender, EventArgs e)
    {
        txtChamadoTitulo.Text = string.Empty;

        if (ddlTarefa.SelectedIndex > 0)
            txtChamadoTitulo.Text = ddlDepartamento.SelectedItem.Text + " - " + ddlAssunto.SelectedItem.Text + " - " + ddlTarefa.SelectedItem.Text;
        else if (ddlDepartamento.SelectedIndex > 0 && ddlAssunto.SelectedIndex > 0)
            txtChamadoTitulo.Text = ddlDepartamento.SelectedItem.Text + " - " + ddlAssunto.SelectedItem.Text;
    }

    private void CarregarComboCategoria()
    {
        var data = new dbCommerceDataContext();
        var Categorias = data.tbChamadoCategorias.ToList();

        Categorias.Add(new tbChamadoCategoria() { idChamadoCategoria = 0, nomeChamadoCategoria = "Selecione" });

        ddlAssunto.DataSource = Categorias.OrderBy(x => x.idChamadoCategoria);
        ddlAssunto.DataTextField = "nomeChamadoCategoria";
        ddlAssunto.DataValueField = "idChamadoCategoria";
        ddlAssunto.DataBind();
    }

    protected void ddlAssunto_SelectedIndexChanged(object sender, EventArgs e)
    {
        CarregarComboTarefa();
        if (ddlAssunto.SelectedIndex > 0)
        {
            trTarefa.Visible = true;
            ddlTarefa.Enabled = true;
        }
        else
        {
            trTarefa.Visible = false;
            ddlTarefa.Enabled = false;
        }

        //txtChamadoTitulo.Text = string.Empty;

        //if (ddlDepartamento.SelectedIndex > 0 && ddlTarefa.SelectedIndex > 0 && ddlAssunto.SelectedIndex > 0)
        //{
        //    System.Text.StringBuilder sb = new System.Text.StringBuilder();
        //    sb.Append(ddlDepartamento.SelectedItem.Text);
        //    sb.Append(" - ");
        //    sb.Append(ddlTarefa.SelectedItem.Text);
        //    sb.Append(" - ");
        //    sb.Append(ddlAssunto.SelectedItem.Text);
        //    txtChamadoTitulo.Text = sb.ToString();
        //}
    }

    protected void chkRetorno_CheckedChanged(object sender, EventArgs e)
    {
        var data = new dbCommerceDataContext();
        ddlDepartamento.Enabled = true;
        ddlAssunto.Enabled = true;
        ddlTarefa.Enabled = true;
        ddlUsuarioDepartamento.Enabled = true;

        if (chkRetorno.Checked)
        {
            ddlUsuarioDepartamento.Enabled = false;
            ddlDepartamento.Enabled = false;
            ddlAssunto.Enabled = false;
            ddlTarefa.Enabled = false;
            txtChamadoTitulo.Text = "Retorno " + txtChamadoTitulo.Text;
        }





    }
}