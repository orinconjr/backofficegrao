﻿<%@ Page Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="produtosEmEspera.aspx.cs" Inherits="admin_produtosEmEspera"  Theme="Glass" MaintainScrollPositionOnPostback=true %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dxwgv" %>
<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" Runat="Server">
    <table cellpadding="0" cellspacing="0" style="width: 920px">
    <tr>
        <td class="tituloPaginas" valign="top">
            <asp:Label ID="lblAcao" runat="server">Produtos em espera</asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            
    <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
        <tr>
            <td align="right">
                <table cellpadding="0" cellspacing="0" style="width: 834px">
                    <tr>
                        <td align="right">
                            &nbsp;</td>
                        <td height="38" 
                            style="background-image: url('images/bkExportar.jpg'); padding-bottom: 1px;" 
                            valign="bottom" width="231">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="width: 97px">
                                        &nbsp;</td>
                                    <td style="width: 33px">
                                        <asp:ImageButton ID="btPdf" runat="server" ImageUrl="~/admin/images/btPdf.jpg" 
                                            style="margin-left: 5px" />
                                    </td>
                                    <td style="width: 31px">
                                        <asp:ImageButton ID="btXsl" runat="server" ImageUrl="~/admin/images/btXls.jpg" 
                                            style="margin-left: 5px" />
                                    </td>
                                    <td style="width: 36px">
                                        <asp:ImageButton ID="btRtf" runat="server" ImageUrl="~/admin/images/btRtf.jpg" 
                                            style="margin-left: 5px" />
                                    </td>
                                    <td valign="bottom">
                                        <asp:ImageButton ID="btCsv" runat="server" ImageUrl="~/admin/images/btCsv.jpg" 
                                            style="margin-left: 5px" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="height: 19px">
                </td>
        </tr>
        <tr>
            <td>
                <dxwgv:aspxgridview ID="grd" runat="server" AutoGenerateColumns="False" 
                    DataSourceID="sqlEmEspera" KeyFieldName="produtoId" Width="834px" 
                    Cursor="auto">
                    <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                    <SettingsText ConfirmDelete="Tem certeza que deseja excluir?" 
                        emptydatarow="Nenhum registro encontrado." />
                    <SettingsPager Position="TopAndBottom" PageSize="50" 
                        ShowDisabledButtons="False" AlwaysShowPager="True">
                        <Summary AllPagesText="Páginas: {0} - {1} ({2} items)" 
                            Text="Página {0} de {1} ({2} registros encontrados)" />
                    </SettingsPager>
                    <settings showfilterrow="True" ShowGroupButtons="False" />
                    <Templates>
                        <DetailRow>
                            <dxwgv:ASPxGridView ID="grdClientes" runat="server" AutoGenerateColumns="False" 
                                Cursor="auto" DataSourceID="sqlClientes" KeyFieldName="produtoId" 
                                onbeforeperformdataselect="grdClientes_BeforePerformDataSelect" Width="834px">
                                <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                                <SettingsText ConfirmDelete="Tem certeza que deseja excluir?" 
                                    emptydatarow="Nenhum registro encontrado." />
                                <SettingsPager PageSize="50" Position="TopAndBottom" 
                                    ShowDisabledButtons="False">
                                    <Summary AllPagesText="Páginas: {0} - {1} ({2} items)" 
                                        Text="Página {0} de {1} ({2} registros encontrados)" />
                                </SettingsPager>
                                <settings showfilterrow="True" ShowGroupButtons="False" />
                                <SettingsEditing EditFormColumnCount="4" mode="Inline" 
                                    PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1" 
                                    PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter" 
                                    PopupEditFormWidth="700px" />
                                <Columns>
                                    <dxwgv:GridViewDataTextColumn Caption="Nome do cliente" FieldName="clienteNome" 
                                        VisibleIndex="0" Width="250px">
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="E-mail" FieldName="clienteEmail" 
                                        VisibleIndex="1" Width="250px">
                                        <PropertiesTextEdit MaxLength="4">
                                            <ValidationSettings SetFocusOnError="True">
                                                <RequiredField ErrorText="Preencha o prazo de entrega." IsRequired="True" />
                                                <RegularExpression ErrorText="Preencha apenas com números e virgula." 
                                                    ValidationExpression="^\d*\,?\d*$" />
                                            </ValidationSettings>
                                        </PropertiesTextEdit>
                                    </dxwgv:GridViewDataTextColumn>
                                </Columns>
                                <StylesEditors>
                                    <Label Font-Bold="True">
                                    </Label>
                                </StylesEditors>
                            </dxwgv:ASPxGridView>
                        </DetailRow>
                    </Templates>
                    <SettingsEditing EditFormColumnCount="4" 
                        PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1" 
                        PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter" 
                        PopupEditFormWidth="700px" mode="Inline" />
                    <Columns>
                        <dxwgv:GridViewDataTextColumn Caption="Id da empresa" 
                            FieldName="produtoIdDaEmpresa" ReadOnly="True" VisibleIndex="0" 
                            Width="30px">
                            <Settings AutoFilterCondition="Contains" />
                            <editformsettings visible="False" CaptionLocation="Top" ColumnSpan="2" />
                        </dxwgv:GridViewDataTextColumn>
<dxwgv:GridViewDataTextColumn FieldName="produtoId" Width="30px" Caption="Id do produto" 
                            VisibleIndex="1">
</dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Nome do produto" FieldName="produtoNome" 
                            VisibleIndex="2" Width="300px">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Em espera" FieldName="quantidadeEmEspera" 
                            VisibleIndex="3" Width="60px">
                            <PropertiesTextEdit>
                            <ValidationSettings SetFocusOnError="True">
                                    <RequiredField ErrorText="Preencha a descrição" IsRequired="True" />
                                </ValidationSettings>
                            </PropertiesTextEdit>
                            <PropertiesTextEdit MaxLength="4">
                                <ValidationSettings SetFocusOnError="True">
                                    <RequiredField ErrorText="Preencha o prazo de entrega." IsRequired="True" />
                                    <RegularExpression ErrorText="Preencha apenas com números e virgula." 
                                        ValidationExpression="^\d*\,?\d*$" />
                                </ValidationSettings>
                            </PropertiesTextEdit>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewCommandColumn VisibleIndex="4" Width="30px" ButtonType="Image">
                            <editbutton Text="Editar">
                                <Image Url="~/admin/images/btEditar.jpg" />
                            </editbutton>
                            <NewButton Text="Novo">
                                <Image Url="~/admin/images/btNovo.jpg" />
                            </NewButton>
                            <deletebutton visible="True" Text="Excluir">
                                <Image Url="~/admin/images/btExcluir.jpg" />
                            </deletebutton>
                            <ClearFilterButton Visible="True">
                            </ClearFilterButton>
                            <HeaderTemplate>
                                <img alt="" src="images/tarjaDeletar.jpg" />
                            </HeaderTemplate>
                        </dxwgv:GridViewCommandColumn>
                    </Columns>
                    <SettingsDetail ShowDetailRow="True" />
                    <StylesEditors>
                        <Label Font-Bold="True">
                        </Label>
                    </StylesEditors>
                </dxwgv:aspxgridview>
<%--                <dxwgv:aspxgridviewexporter ID="grdEx" runat="server" FileName="fornecedores" 
                    GridViewID="grd" Landscape="True" PreserveGroupRowStates="True">
                </dxwgv:aspxgridviewexporter>--%>
                <asp:ObjectDataSource ID="sqlEmEspera" runat="server" 
                    DeleteMethod="produtoEmEsperaExclui" SelectMethod="produtoSelecionaEmEspera" 
                    TypeName="rnProdutoEmEspera">
                    <DeleteParameters>
                        <asp:Parameter Name="produtoId" Type="Int32" />
                    </DeleteParameters>
                </asp:ObjectDataSource>
                <asp:LinqDataSource ID="sqlClientes" runat="server" 
                    ContextTypeName="dbCommerceDataContext" EnableDelete="True" 
                    TableName="tbProdutosEmEsperas" Where="produtoId == @produtoId">
                    <WhereParameters>
                        <asp:SessionParameter Name="produtoId" SessionField="produtoId" Type="Int32" />
                    </WhereParameters>
                </asp:LinqDataSource>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
    </table>
        </td>
    </tr>
</table>
    </asp:Content>
