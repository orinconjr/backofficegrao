﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Data.Filtering;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;

public partial class admin_pedidosAtrasados : System.Web.UI.Page
{
    DateTemplate dateTemplate = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        fillGrid(false);
    }

    public class Pedidos
    {
        public int? pedidoId { get; set; }
        public decimal? valorCobrado { get; set; }
        public DateTime? dataHoraDoPedido { get; set; }
        public DateTime? prazoFinalPedido { get; set; }
        public string clienteNome { get; set; }
        public int statusDoPedido { get; set; }
        public string motivoAtraso { get; set; }
        public string motivoFornecedor { get; set; }
        public DateTime? prazoMaximoPostagemAtualizado { get; set; }
        public string completo { get; set; }
        public string fornecedorPendente { get; set; }
        public string personalizadosPendentes { get; set; }
        public string produtoId { get; set; }
    }

    private void fillGrid(bool rebind)
    {
        try
        {
            var completosCd1 = rnPedidos.verificaPedidosCompletos().ToList();
            var completosCd4 = rnPedidos.verificaPedidosCompletos(4).ToList();
            var completosCd5 = rnPedidos.verificaPedidosCompletos(5).ToList();
            var completos = completosCd1.Union(completosCd4).Union(completosCd5);

            var idsPedidos = completos.Select(x => x.pedidoId).Take(2090);


            var data = new dbCommerceDataContext();
            var hoje = Convert.ToDateTime(DateTime.Now.ToShortDateString());

            var listaFornecedoresPendentes = (from e in data.tbItemPedidoEstoques
                                              where !e.reservado && !e.cancelado && !e.enviado
                                              select e);
            var personalizacao = (from e in data.tbItemPedidoEstoques
                                  join f in data.tbProdutoPersonalizacaos on e.produtoId equals f.idProdutoPai
                                  where !e.reservado && !e.cancelado && !e.enviado
                                  select e.tbItensPedido.pedidoId).Distinct();

            //using (
            //    var txn = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required,
            //        new System.Transactions.TransactionOptions
            //        {
            //            IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
            //        }))
            //{

            var pedidos = (from c in data.tbPedidos
                           join d in data.tbClientes on c.clienteId equals d.clienteId
                           where
                               (c.prazoMaximoPostagemAtualizado ?? c.prazoFinalPedido) <= hoje &&
                               (c.statusDoPedido == 3 | c.statusDoPedido == 11) && (c.itensPendentesEnvioCd4 > 0 | c.itensPendentesEnvioCd5 > 0)
                           //| c.statusDoPedido == 4 a pedido da Lyndsay foi retirado para que os pedidos com status - Seu pedido está sendo embalado - não fiquem na lista de atrasados
                           orderby c.prazoFinalPedido
                           select new
                           {
                               c.pedidoId,
                               c.valorCobrado,
                               c.dataHoraDoPedido,
                               prazoFinalPedido = c.prazoFinalPedido,
                               d.clienteNome,
                               c.statusDoPedido,
                               c.motivoAtraso,
                               c.prazoMaximoPostagemAtualizado,
                               completo = (c.itensPendentesEnvioCd4 > 0 ? (c.envioLiberadoCd4 ?? false) : true) && (c.itensPendentesEnvioCd5 > 0 ? (c.envioLiberadoCd5 ?? false) : true) ? "Sim": "Não",
                               //fornecedorPendente = (from e in data.tbItemPedidoEstoques where e.tbItensPedido.pedidoId == c.pedidoId && !e.reservado && !e.cancelado && !e.enviado select new { e.tbProduto.tbProdutoFornecedor.fornecedorNome }).FirstOrDefault().fornecedorNome,
                               fornecedorPendente =
                                   listaFornecedoresPendentes.Where(x => x.tbItensPedido.pedidoId == c.pedidoId)
                                       .Select(x => x.tbProduto.tbProdutoFornecedor.fornecedorNome)
                                       .FirstOrDefault(),
                               personalizadosPendentes = personalizacao.Count(x => x == c.pedidoId) > 0 ? "Sim" : "Não",
                               produtoId = listaFornecedoresPendentes.Where(x => x.tbItensPedido.pedidoId == c.pedidoId)
                                       .Select(x => x.tbProduto.produtoId)
                                       .FirstOrDefault().ToString()
                           }).ToList();

            if (rblFiltro.Items[0].Selected)
                pedidos = pedidos.Where(x => x.completo == "Não").ToList();

            grd.DataSource = pedidos;
            if ((!Page.IsPostBack && !Page.IsCallback) | rebind) grd.DataBind();
            //}
        }
        catch (Exception ex)
        {
            Response.Write("<script>alert('Erro no serviço, favor tentar novamente!" + ex.Message + "')</script>");
        }

    }
    protected void btPdf_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WritePdfToResponse();
    }
    protected void btXsl_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteXlsToResponse();
    }
    protected void btRtf_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteRtfToResponse();
    }
    protected void btCsv_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteCsvToResponse();
    }

    protected void grid_AutoFilterCellEditorCreate(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewEditorCreateEventArgs e)
    {
        ASPxGridView grid = sender as ASPxGridView;
        if (e.Column.FieldName == "dataHoraDoPedido")
        {
            DropDownEditProperties dde = new DropDownEditProperties();
            dde.EnableClientSideAPI = true;
            dateTemplate = new DateTemplate();
            dde.DropDownWindowTemplate = dateTemplate;
            e.EditorProperties = dde;
        }
        if (e.Column.FieldName == "prazoFinalPedido")
        {
            DropDownEditProperties dde = new DropDownEditProperties();
            dde.EnableClientSideAPI = true;
            dateTemplate = new DateTemplate();
            dde.DropDownWindowTemplate = dateTemplate;
            e.EditorProperties = dde;
        }
    }
    protected void grid_AutoFilterCellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
    {
        ASPxGridView grid = sender as ASPxGridView;
        //if (e.Column.FieldName == "dataHoraDoPedido")
        //{
        //    ASPxDropDownEdit dde = e.Editor as ASPxDropDownEdit;
        //    dde.ClientSideEvents.CloseUp = String.Format("function (s, e) {{ ApplyFilter(s, {0}, {1}); }}", dateTemplate.cIdFrom, dateTemplate.cIdTo);
        //    //dde.ClientSideEvents.DropDown = String.Format("function (s, e) {{ OnDropDown(s, {0}, {1}); }}", dateTemplate.cIdFrom, dateTemplate.cIdTo);
        //    dde.ReadOnly = true;
        //}
        if (e.Column.FieldName == "dataHoraDoPedido")
        {
            ASPxDropDownEdit dde = e.Editor as ASPxDropDownEdit;
            dde.ClientSideEvents.CloseUp = String.Format("function (s, e) {{ ApplyFilter(s, {0}, {1}, 'dataHoraDoPedido'); }}", dateTemplate.cIdFrom, dateTemplate.cIdTo);
            //dde.ClientSideEvents.DropDown = String.Format("function (s, e) {{ OnDropDown(s, {0}, {1}); }}", dateTemplate.cIdFrom, dateTemplate.cIdTo);
            dde.ReadOnly = true;
        }
        if (e.Column.FieldName == "prazoFinalPedido")
        {
            ASPxDropDownEdit dde = e.Editor as ASPxDropDownEdit;
            dde.ClientSideEvents.CloseUp = String.Format("function (s, e) {{ ApplyFilter(s, {0}, {1}, 'prazoFinalPedido'); }}", dateTemplate.cIdFrom, dateTemplate.cIdTo);
            //dde.ClientSideEvents.DropDown = String.Format("function (s, e) {{ OnDropDown(s, {0}, {1}); }}", dateTemplate.cIdFrom, dateTemplate.cIdTo);
            dde.ReadOnly = true;
        }
    }

    protected void grid_ProcessColumnAutoFilter(object sender, ASPxGridViewAutoFilterEventArgs e)
    {
        if (e.Value == "|") return;
        //if (e.Column.FieldName == "dataHoraDoPedido")
        //{
        //    if (e.Kind == GridViewAutoFilterEventKind.CreateCriteria)
        //    {
        //        Session["dataHoraDoPedido"] = e.Value;
        //        String[] dates = e.Value.Split('|');
        //        DateTime dateFrom = Convert.ToDateTime(dates[0]),
        //            dateTo = Convert.ToDateTime(dates[1]);
        //        e.Criteria = (new OperandProperty("dataHoraDoPedido") >= dateFrom) &
        //                     (new OperandProperty("dataHoraDoPedido") <= dateTo);
        //    }
        //    else
        //    {
        //        if (Session["dataHoraDoPedido"] != null)
        //            e.Value = Session["dataHoraDoPedido"].ToString();
        //    }
        //}
        if (e.Column.FieldName == "dataHoraDoPedido")
        {
            if (e.Kind == GridViewAutoFilterEventKind.CreateCriteria)
            {
                Session["dataHoraDoPedido"] = e.Value;
                String[] dates = e.Value.Split('|');
                DateTime dateFrom = Convert.ToDateTime(dates[0]),
                    dateTo = Convert.ToDateTime(dates[1]);
                e.Criteria = (new OperandProperty("dataHoraDoPedido") >= dateFrom) &
                             (new OperandProperty("dataHoraDoPedido") <= dateTo);
            }
            else
            {
                if (Session["dataHoraDoPedido"] != null)
                    e.Value = Session["dataHoraDoPedido"].ToString();
            }
        }
        if (e.Column.FieldName == "prazoFinalPedido")
        {
            if (e.Kind == GridViewAutoFilterEventKind.CreateCriteria)
            {
                Session["prazoFinalPedido"] = e.Value;
                String[] dates = e.Value.Split('|');
                DateTime dateFrom = Convert.ToDateTime(dates[0]),
                    dateTo = Convert.ToDateTime(dates[1]);
                e.Criteria = (new OperandProperty("prazoFinalPedido") >= dateFrom) &
                             (new OperandProperty("prazoFinalPedido") <= dateTo);
            }
            else
            {
                if (Session["prazoFinalPedido"] != null)
                    e.Value = Session["prazoFinalPedido"].ToString();
            }
        }
    }

    protected void btnChecarPedidosMl_OnClick(object sender, EventArgs e)
    {
        //var meli = mlIntegracao.verificaAutenticacao("pedidos.aspx", Request.QueryString["code"]);
        mlIntegracao.checaPedidosML();
        rnIntegracoes.checaPedidosExtra(true);
        grd.DataBind();
    }

    protected void grd_OnCustomUnboundColumnData(object sender, ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "pedidoIdCliente")
        {
            int pedidoId = Convert.ToInt32(e.GetListSourceFieldValue("pedidoId"));
            e.Value = rnFuncoes.retornaIdCliente(pedidoId);
        }


    }

    protected void alterarPedido(object sender, CommandEventArgs e)
    {
        popAlterarRastreio.ShowOnPageLoad = true;
        var idPedido = 0;
        int.TryParse(e.CommandArgument.ToString(), out idPedido);
        if (idPedido > 0)
        {
            hdfPedidoAlterando.Value = idPedido.ToString();
            var pedidoDc = new dbCommerceDataContext();
            var pedido = (from c in pedidoDc.tbPedidos where c.pedidoId == idPedido select c).First();
            ddlMotivoAtraso.SelectedValue = pedido.motivoAtraso;
        }
    }

    protected void btnAlterarPedido_OnClick(object sender, EventArgs e)
    {
        var idPedido = 0;
        int.TryParse(hdfPedidoAlterando.Value, out idPedido);
        if (idPedido > 0)
        {
            var pedidoDc = new dbCommerceDataContext();
            var pedido = (from c in pedidoDc.tbPedidos where c.pedidoId == idPedido select c).First();
            if (pedido.motivoAtraso != ddlMotivoAtraso.SelectedValue)
            {
                rnInteracoes.interacaoInclui(idPedido,
                    "Motivo de atraso alterado para: " + ddlMotivoAtraso.SelectedValue,
                    rnUsuarios.retornaNomeUsuarioLogado(), "False");
            }
            pedido.motivoAtraso = ddlMotivoAtraso.SelectedValue;
            pedidoDc.SubmitChanges();
            popAlterarRastreio.ShowOnPageLoad = false;
            fillGrid(true);

        }
    }

    protected void rblFiltro_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        fillGrid(true);
    }

    protected void grd_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType != GridViewRowType.Data)
        {
            return;
        }
        Label lblMotivoFornecedor = (Label)grd.FindRowCellTemplateControl(e.VisibleIndex, grd.Columns["motivoFornecedor"] as GridViewDataColumn, "lblMotivoFornecedor");
        int pedidoId = 0;
        int.TryParse(grd.GetRowValues(e.VisibleIndex, new string[] { "pedidoId" }).ToString(), out pedidoId);

        int produtoId = 0;
        var produto = grd.GetRowValues(e.VisibleIndex, new string[] { "produtoId" });
        if (produto != null)
        {
            produtoId = Convert.ToInt32(produto);
        }
        int romaneio = romaneioPedido(pedidoId, produtoId);
        lblMotivoFornecedor.Text = motivoAtraso(romaneio);
    }

    public string motivoAtraso(int idPedidoFornecedor)
    {
        var data = new dbCommerceDataContext();
        string motivo = (from c in data.tbPedidoFornecedors
                         where c.idPedidoFornecedor == idPedidoFornecedor
                         select c.tbPedidoFornecedorMotivoAtraso.motivo).FirstOrDefault();
        return motivo;

    }

    public int romaneioPedido(int pedidoId, int produtoId)
    {
        var data = new dbCommerceDataContext();
        int romaneio = 0;

        List<tbItemPedidoEstoque> produtosAguardandoEstoque = new List<tbItemPedidoEstoque>();


        produtosAguardandoEstoque = (from c in data.tbItemPedidoEstoques
                                     where
                                         c.dataLimite != null && c.cancelado == false && c.reservado == false &&
                                         c.tbItensPedido.pedidoId == pedidoId
                                     select c).ToList();


        var aguardandoGeral = (from c in data.tbItemPedidoEstoques
                               where
                                   produtosAguardandoEstoque.Select(x => x.produtoId).Contains(c.produtoId) && c.reservado == false &&
                                   c.cancelado == false && c.dataLimite != null
                               orderby c.dataLimite
                               select new
                               {
                                   c.idItemPedidoEstoque,
                                   c.tbItensPedido.pedidoId,
                                   c.dataCriacao,
                                   dataLimiteReserva = c.dataLimite,
                                   c.produtoId,
                                   c.itemPedidoId
                               }).ToList();
        var pedidosFornecedorGeral = (from c in data.tbPedidoFornecedorItems
                                      join d in data.tbProdutoFornecedors on c.tbPedidoFornecedor.idFornecedor equals d.fornecedorId
                                      join e in data.tbProdutoEstoques on c.idPedidoFornecedorItem equals e.idPedidoFornecedorItem into produtoEstoque
                                      where
                                          (c.entregue == false | (c.entregue == true && (c.liberado ?? false) == false) | produtoEstoque.Any(x => (x.liberado ?? false) == false)) && (c.motivo ?? "") == "" &&
                                          produtosAguardandoEstoque.Select(x => x.produtoId).Contains(c.idProduto)
                                      orderby c.tbPedidoFornecedor.dataLimite, c.tbPedidoFornecedor.idPedidoFornecedor
                                      select new
                                      {
                                          c.idPedidoFornecedor,
                                          c.idPedidoFornecedorItem,
                                          dataLimiteFornecedor = c.tbPedidoFornecedor.dataLimite,
                                          c.idProduto,
                                          d.fornecedorNome,
                                          c.entregue,
                                          c.tbPedidoFornecedor.dataPrevista
                                      }).ToList();

        foreach (var produtoAguardandoEstoque in produtosAguardandoEstoque)
        {
            var aguardando = (from c in aguardandoGeral
                              where c.produtoId == produtoAguardandoEstoque.produtoId
                              orderby c.dataLimiteReserva
                              select c).ToList();
            var pedidoFornecedor = (from c in pedidosFornecedorGeral
                                    where c.idProduto == produtoAguardandoEstoque.produtoId
                                    orderby c.dataLimiteFornecedor, c.idPedidoFornecedor
                                    select c).ToList();
            var listaFinal = (from c in aguardando.Select((item, index) => new { item, index })
                              join d in pedidoFornecedor.Select((item, index) => new { item, index }) on c.index equals d.index into
                                  pedidosFornecedorListaInterna
                              from e in pedidosFornecedorListaInterna.DefaultIfEmpty()
                              select new
                              {
                                  c.item.idItemPedidoEstoque,
                                  c.item.pedidoId,
                                  c.item.dataCriacao,
                                  c.item.dataLimiteReserva,
                                  c.item.itemPedidoId,
                                  idPedidoFornecedor = e != null ? e.item.idPedidoFornecedor : 0,
                                  dataLimiteFornecedor = e != null ? (DateTime?)e.item.dataLimiteFornecedor : null,
                                  idPedidoFornecedorItem = e != null ? (int?)e.item.idPedidoFornecedorItem : null,
                                  fornecedorNome = e != null ? e.item.fornecedorNome : "",
                                  dataPrevista = e != null ? e.item.dataPrevista : null,
                                  c.item.produtoId
                              }
                ).ToList();
            var itemNaLista =
                listaFinal.FirstOrDefault(x => x.idItemPedidoEstoque == produtoAguardandoEstoque.idItemPedidoEstoque);
            if (itemNaLista != null)
            {
                if (itemNaLista.produtoId == produtoId)
                    return itemNaLista.idPedidoFornecedor;


            }
        }
        return romaneio;
    }

    public List<Pedidos> romaneioPedido2(List<Pedidos> pedidos)
    {
        int pedidoId = 0;
        int produtoId = 0;
        string nomeMotivo = string.Empty;
        List<string> lMotivo = new List<string>();

        var data = new dbCommerceDataContext();

        //List<tbItemPedidoEstoque> produtosAguardandoEstoqueGeral = new List<tbItemPedidoEstoque>();


        var motivos = (from c in data.tbPedidoFornecedorItems
                       where c.entregue == false
                       select new
                       {
                           motivo = c.tbPedidoFornecedor.tbPedidoFornecedorMotivoAtraso == null ? "" : c.tbPedidoFornecedor.tbPedidoFornecedorMotivoAtraso.motivo,
                           c.idPedidoFornecedor
                       }).Distinct().ToList();

        var produtosAguardandoEstoqueGeral = (from c in data.tbItemPedidoEstoques
                                              where
                                                  c.dataLimite != null && c.cancelado == false && c.reservado == false
                                              //c.tbItensPedido.pedidoId == pedidoId
                                              select new
                                              {
                                                  c.idItemPedidoEstoque,
                                                  c.tbItensPedido.pedidoId,
                                                  c.tbItensPedido.produtoId

                                              }).ToList();


        var aguardandoGeral = (from c in data.tbItemPedidoEstoques
                               where
                                   c.reservado == false &&
                                   c.cancelado == false && c.dataLimite != null
                               orderby c.dataLimite
                               select new
                               {
                                   c.idItemPedidoEstoque,
                                   c.tbItensPedido.pedidoId,
                                   c.dataCriacao,
                                   dataLimiteReserva = c.dataLimite,
                                   c.produtoId,
                                   c.itemPedidoId
                               }).ToList();

        var pedidosFornecedorGeral = (from c in data.tbPedidoFornecedorItems
                                      join d in data.tbProdutoFornecedors on c.tbPedidoFornecedor.idFornecedor equals d.fornecedorId
                                      join e in data.tbProdutoEstoques on c.idPedidoFornecedorItem equals e.idPedidoFornecedorItem into produtoEstoque
                                      where
                                          (c.entregue == false | (c.entregue == true && (c.liberado ?? false) == false) | produtoEstoque.Any(x => (x.liberado ?? false) == false)) && (c.motivo ?? "") == ""


                                      orderby c.tbPedidoFornecedor.dataLimite, c.tbPedidoFornecedor.idPedidoFornecedor
                                      select new
                                      {
                                          c.idPedidoFornecedor,
                                          c.idPedidoFornecedorItem,
                                          dataLimiteFornecedor = c.tbPedidoFornecedor.dataLimite,
                                          c.idProduto,
                                          d.fornecedorNome,
                                          c.entregue,
                                          c.tbPedidoFornecedor.dataPrevista
                                      }).ToList();


        foreach (Pedidos pedido in pedidos)
        {
            if (pedido != null)
            {
                if (pedido.pedidoId != null)
                    int.TryParse(pedido.pedidoId.ToString(), out pedidoId);

                if (pedido.produtoId != null)
                    int.TryParse(pedido.produtoId.ToString(), out produtoId);

                var produtosAguardandoEstoque = produtosAguardandoEstoqueGeral.Where(x => x.pedidoId == pedido.pedidoId).ToList();

                foreach (var produtoAguardandoEstoque in produtosAguardandoEstoque)
                {
                    var aguardando = (from c in aguardandoGeral
                                      where c.produtoId == produtoAguardandoEstoque.produtoId
                                      orderby c.dataLimiteReserva
                                      select c).ToList();
                    var pedidoFornecedor = (from c in pedidosFornecedorGeral
                                            where c.idProduto == produtoAguardandoEstoque.produtoId
                                            orderby c.dataLimiteFornecedor, c.idPedidoFornecedor
                                            select c).ToList();
                    var listaFinal = (from c in aguardando.Select((item, index) => new { item, index })
                                      join d in pedidoFornecedor.Select((item, index) => new { item, index }) on c.index equals d.index into
                                          pedidosFornecedorListaInterna
                                      from e in pedidosFornecedorListaInterna.DefaultIfEmpty()
                                      select new
                                      {
                                          c.item.idItemPedidoEstoque,
                                          c.item.pedidoId,
                                          c.item.dataCriacao,
                                          c.item.dataLimiteReserva,
                                          c.item.itemPedidoId,
                                          idPedidoFornecedor = e != null ? e.item.idPedidoFornecedor : 0,
                                          dataLimiteFornecedor = e != null ? (DateTime?)e.item.dataLimiteFornecedor : null,
                                          idPedidoFornecedorItem = e != null ? (int?)e.item.idPedidoFornecedorItem : null,
                                          fornecedorNome = e != null ? e.item.fornecedorNome : "",
                                          dataPrevista = e != null ? e.item.dataPrevista : null,
                                          c.item.produtoId
                                      }
                        ).ToList();
                    var itemNaLista =
                        listaFinal.FirstOrDefault(x => x.idItemPedidoEstoque == produtoAguardandoEstoque.idItemPedidoEstoque);
                    if (itemNaLista != null)
                    {
                        if (itemNaLista.produtoId == produtoId)
                            if (motivos.Where(x => x.idPedidoFornecedor == itemNaLista.idPedidoFornecedor).FirstOrDefault() != null)
                                pedido.motivoFornecedor = motivos.Where(x => x.idPedidoFornecedor == itemNaLista.idPedidoFornecedor).FirstOrDefault().motivo;
                    }
                }

            }

        }
        return pedidos;
    }

    protected void btnVerResumo_Click(object sender, EventArgs ev)
    {
        var data = new dbCommerceDataContext();
        var aguardandoGeral = (from c in data.tbItemPedidoEstoques
                               where c.reservado == false && c.cancelado == false && c.dataLimite != null
                               orderby c.dataLimite
                               select new
                               {
                                   c.produtoId,
                                   c.tbItensPedido.tbPedido.prazoMaximoPostagemAtualizado,
                                   c.idItemPedidoEstoque,
                                   c.tbItensPedido.pedidoId,
                                   dataLimiteReserva = c.dataLimite,
                               }).ToList().Select((r, i) => new { idx = i, r.idItemPedidoEstoque, r.pedidoId, r.produtoId, r.dataLimiteReserva, r.prazoMaximoPostagemAtualizado }).ToList();
        var aguardando = (from c in aguardandoGeral
                          join d in aguardandoGeral on c.produtoId equals d.produtoId into aguardandos
                          select new
                          {
                              c.produtoId,
                              c.prazoMaximoPostagemAtualizado,
                              c.idItemPedidoEstoque,
                              c.pedidoId,
                              c.dataLimiteReserva,
                              indice = c.produtoId + "_" + (c.idx - (aguardandos.OrderBy(x => x.idx).FirstOrDefault(x => x.produtoId == c.produtoId).idx - 1))
                          }).ToList();

        var pedidosGeral = (from c in data.tbPedidoFornecedorItems
                            join e in data.tbProdutoEstoques on c.idPedidoFornecedorItem equals e.idPedidoFornecedorItem into produtoEstoque
                            where (c.entregue == false | produtoEstoque.Any(x => (x.liberado ?? false) == false))
                            orderby c.tbPedidoFornecedor.dataLimite, c.tbPedidoFornecedor.idPedidoFornecedor
                            select new
                            {
                                motivoFornecedor = c.tbPedidoFornecedor.tbPedidoFornecedorMotivoAtraso.motivo,
                                fornecedorPendente = c.tbProduto.tbProdutoFornecedor.fornecedorNome,
                                c.idPedidoFornecedor,
                                c.idPedidoFornecedorItem,
                                dataLimiteFornecedor = c.tbPedidoFornecedor.dataLimite,
                                c.idProduto
                            }).ToList().Select((r, i) => new
                            {
                                idx = i,
                                r.idPedidoFornecedor,
                                r.idPedidoFornecedorItem,
                                r.dataLimiteFornecedor,
                                r.idProduto,
                                r.motivoFornecedor,
                                r.fornecedorPendente
                            }).ToList();

        var pedidos = (from c in pedidosGeral
                       join d in pedidosGeral on c.idProduto equals d.idProduto into gerals
                       select new
                       {
                           c.motivoFornecedor,
                           c.fornecedorPendente,
                           c.idPedidoFornecedor,
                           c.idPedidoFornecedorItem,
                           c.dataLimiteFornecedor,
                           c.idProduto,
                           indice = c.idProduto + "_" + (c.idx - (gerals.OrderBy(x => x.idx).FirstOrDefault(x => x.idProduto == c.idProduto).idx - 1)),

                       }).ToList();

        var listaFinal = (from c in aguardando
                          join d in pedidos on c.indice equals d.indice into romaneio
                          from e in romaneio.DefaultIfEmpty()
                          select new
                          {
                              //c.produtoId,
                              //c.idItemPedidoEstoque,
                              c.pedidoId,
                              //c.dataLimiteReserva,
                              //idPedidoFornecedor = e != null ? e.idPedidoFornecedor : 0,
                              //dataLimiteFornecedor = e != null ? (DateTime?)e.dataLimiteFornecedor : null,
                              //idPedidoFornecedorItem = e != null ? (int?)e.idPedidoFornecedorItem : null,
                              motivoFornecedor = e != null ? e.motivoFornecedor : null,
                              fornecedorPendente = e != null ? e.fornecedorPendente : null,
                              c.prazoMaximoPostagemAtualizado
                          }
            ).Distinct().Where(x => x.prazoMaximoPostagemAtualizado < DateTime.Now).ToList();

        var qryResumo = listaFinal.GroupBy(p => p.motivoFornecedor)
        .Select(g => new { motivoAtraso = g.FirstOrDefault().motivoFornecedor, qtdMotivoAtraso = g.Count(), percentualMotivo = g.Count() * 100 / listaFinal.Count() })
        .ToList();

        var qryResumoFornecedor = listaFinal.GroupBy(p => p.fornecedorPendente)
        .Select(g => new { fornecedor = g.FirstOrDefault().fornecedorPendente, qtdFornecedor = g.Count(), percentualFornecedor = g.Count() * 100 / listaFinal.Count() })
        .ToList();

        grdResumoMotivoAtraso.DataSource = qryResumo.OrderByDescending(x => x.qtdMotivoAtraso);
        grdResumoMotivoAtraso.DataBind();

        grdResumoFornecedores.DataSource = qryResumoFornecedor.OrderByDescending(x => x.qtdFornecedor);
        grdResumoFornecedores.DataBind();

        trResumo.Visible = true;

    }
}
