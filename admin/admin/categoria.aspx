﻿<%@ Page Language="C#" MasterPageFile="~/admin/admin.master" Theme="Glass" AutoEventWireup="true" CodeFile="categoria.aspx.cs" Inherits="admin_categoria" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" Runat="Server">

    <div class="tituloPaginas">Categoria</div>
    
    <div class="alinhaConteudo">
        <asp:Panel ID="Panel1" runat="server" DefaultButton="imbBuscar">
            <table style="width: 100%; border: #7EACB1 1px solid" cellpadding="0" cellspacing="0" border="0">
                <tr class="topoDetailsView">
                    <td style="padding-left: 10px;">
                        Pesquisar Categoria</td>
                </tr>
                <tr>
                    <td height="40" style="padding-left: 5px; padding-right: 5px;">
                        <asp:TextBox ID="txtPalavraChave" runat="server"></asp:TextBox>     
                    </td>
                </tr>
                <tr>
                    <td align="right" height="40" style="padding-left: 5px; padding-right: 5px;">
                        <asp:ImageButton ID="imbBuscar" runat="server" SkinID="imgBuscar" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <br /><br />
        <div style="float: right;">
            <asp:ImageButton ID="imbIncluir" runat="server" SkinID="imgIncluir" onclick="imbIncluir_Click" />
        </div>
        <br/><br/><br/><br/>
        <asp:GridView ID="gdvEnquete" runat="server" DataSourceID="sqlEnquete" 
            EnableModelValidation="True" AutoGenerateColumns="False" DataKeyNames="idCategoriaProduto" 
            EmptyDataText="Não existem registros cadastrados" 
            onrowdeleted="gdvEnquete_RowDeleted">
           <Columns>
                <asp:BoundField DataField="idCategoriaProduto" HeaderText="Código" SortExpression="idCategoriaProduto" ItemStyle-Width="32"/>
                <asp:TemplateField HeaderText="Nome da Categoria" SortExpression="nmCategoriaProduto">
                    <ItemTemplate>
                        <asp:Label ID="hplEnquete" runat="server" Text='<%# Bind("nmCategoriaProduto") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ShowHeader="False">
                <ItemTemplate>
                    <asp:ImageButton SkinID="imgEditar" ID="imbAlterar" runat="server" CausesValidation="True" PostBackUrl='<%# "categoria.aspx?idCategoria=" + Eval("idCategoriaProduto") %>' ></asp:ImageButton>
                </ItemTemplate>
                <ItemStyle Width="32px" />
            </asp:TemplateField>
                <asp:TemplateField ItemStyle-Width="32" ShowHeader="False">
                    <ItemTemplate>
                        <asp:ImageButton SkinID="imgExcluir" ID="imbExcluir" runat="server" CausesValidation="False" CommandName="Delete"></asp:ImageButton>
                    </ItemTemplate>
                </asp:TemplateField>
           </Columns> 
        </asp:GridView>           
        <asp:SqlDataSource ID="sqlEnquete" runat="server" ConnectionString="<%$ ConnectionStrings:connectionString %>" 
        SelectCommandType="StoredProcedure" SelectCommand="probioticaCategoriaSeleciona"
        DeleteCommandType="StoredProcedure" DeleteCommand="probioticaCategoriaExclui">
            <DeleteParameters>
                <asp:Parameter Name="idCategoriaProduto" Type="Int32" />
            </DeleteParameters>
            <SelectParameters>
                <asp:ControlParameter Name="nmCategoriaProduto" ControlID="txtPalavraChave" Type="String" DefaultValue="%%" />
            </SelectParameters>
        </asp:SqlDataSource>         

        <asp:DetailsView ID="dtvAlteraEnquete" runat="server" 
            DataSourceID="sqlAlteraEnquete" EnableModelValidation="True" 
            DefaultMode="Edit" AutoGenerateRows="False" DataKeyNames="idCategoriaProduto" 
            oniteminserted="dtvAlteraEnquete_ItemInserted" 
            onitemupdated="dtvAlteraEnquete_ItemUpdated" 
            oniteminserting="dtvAlteraEnquete_ItemInserting">
            <Fields>
                <asp:TemplateField ShowHeader="false">
                    <EditItemTemplate>
                        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%; border: #7EACB1 1px solid">
                            <tr class="topoDetailsView">
                                <td style="padding-left: 10px;">
                                   Nome da Categoria:</td>
                            </tr>
                            <tr>
                                <td height="40" style="padding-left: 5px; padding-right: 5px;">
                                    <asp:TextBox ID="txtCategoriaProduto" runat="server" Text='<%# Bind("nmCategoriaProduto") %>'></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" height="40" style="padding-left: 5px; padding-right: 5px;">
                                    <br />
                                    <asp:ImageButton SkinID="imgAlterar" ID="imbIncluir" runat="server" CausesValidation="True" CommandName="Update">
                                    </asp:ImageButton>
                                    &nbsp;<asp:ImageButton SkinID="imgCalcel" ID="imbCancelar" runat="server" 
                                        CausesValidation="True" CommandName="Cancel" onclick="imbCancelar_Click">
                                    </asp:ImageButton> 
                                </td>
                            </tr>
                        </table>
                        <br />
                        
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%; border: #7EACB1 1px solid">
                            <tr class="topoDetailsView">
                                <td style="padding-left: 10px;">
                                   Nome da Categoria:</td>
                            </tr>
                            <tr>
                                <td height="40" style="padding-left: 5px; padding-right: 5px;">
                                    <asp:TextBox ID="txtCategoriaProduto" runat="server" Text='<%# Bind("nmCategoriaProduto") %>'></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" height="40" 
                                    style="padding-left: 5px; padding-right: 5px;">
                                    <br />
                                    <asp:ImageButton SkinID="imgIncluir" ID="imbIncluir" runat="server" CausesValidation="True" CommandName="Insert">
                                    </asp:ImageButton>
                                    &nbsp;<asp:ImageButton SkinID="imgCalcel" ID="imbCancelar" runat="server" CausesValidation="True" CommandName="Cancel" onclick="imbCancelar_Click">
                                    </asp:ImageButton>
                                </td>
                            </tr>
                        </table>
                        <br />
                         
                    </InsertItemTemplate>
                </asp:TemplateField>
            </Fields>
        </asp:DetailsView>
        <asp:SqlDataSource ID="sqlAlteraEnquete" runat="server" SelectCommandType="StoredProcedure" ConnectionString="<%$ ConnectionStrings:connectionString %>" 
            SelectCommand="probioticaCategoriaSelecionaPorId" UpdateCommandType="StoredProcedure" 
            UpdateCommand="probioticaCategoriaAltera" InsertCommandType="StoredProcedure" InsertCommand="probioticaCategoriaInclui">
            <InsertParameters>
                <asp:Parameter Name="nmCategoriaProduto" Type="String" />
            </InsertParameters>
            <SelectParameters>
                <asp:QueryStringParameter Name="idCategoriaProduto" QueryStringField="idCategoria" Type="Int32" />
            </SelectParameters>
            <UpdateParameters>
                <asp:Parameter Name="idCategoriaProduto" Type="Int32" />
                <asp:Parameter Name="nmCategoriaProduto" Type="String" />
            </UpdateParameters>
        </asp:SqlDataSource>         
    </div>
</asp:Content>



