﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxEditors;

public partial class admin_comprasOrdemConferirEntrega : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            FillListaPendentes();
        }
    }


    private void FillListaPendentes()
    {
        int id = 0;
        id = Convert.ToInt32(Request.QueryString["id"]);

        usrComprasOrdemCondicoes.IdComprasOrdem = id;
        usrComprasOrdemCondicoes.FillCondicoes();

        var data = new dbCommerceDataContext();
        var ordem = (from c in data.tbComprasOrdems where c.idComprasOrdem == id select c).FirstOrDefault();
        if (ordem == null) Response.Redirect("Default.aspx");
        if (ordem.statusOrdemCompra == 1) Response.Redirect("compraOrdensPendentes.aspx");
        if (ordem.statusOrdemCompra == 2)
        {
            btnGravarOrdemCompra.OnClientClick = "return confirm('Atenção, a ordem de compra será reenviada para aprovação. Deseja gravar alterações?')";
        }

        litFornecedor.Text = ordem.tbComprasFornecedor.fornecedor;
        litEmpresa.Text = ordem.tbComprasEmpresa.empresa;

        var produtosPendentes = (from c in data.tbComprasOrdemProdutos
                                 where c.idComprasOrdem == id
                                 select new
                                 {
                                     c.idComprasOrdemProduto,
                                     c.idComprasSolicitacaoProduto,
                                     c.idComprasProduto,
                                     c.tbComprasProduto.produto,
                                     c.tbComprasSolicitacaoProduto.idComprasSolicitacao,
                                     c.quantidade,
                                     c.preco,
                                     status = c.status == 1 ? "Aprovado" : c.status == 2 ? "Reprovado" : "Aguardando",
                                     c.comentario,
                                     c.quantidadeRecebida,
                                     c.precoRecebido
                                 });
        lstProdutosSolicitacao.DataSource = produtosPendentes;
        lstProdutosSolicitacao.DataBind();

        litTotal.Text = produtosPendentes.Any()
            ? produtosPendentes.Sum(x => (x.precoRecebido ?? x.preco) * (x.quantidadeRecebida ?? x.quantidade)).ToString("C")
            : 0.ToString("C");
    }


    protected void btnGravarOrdemCompra_OnClick(object sender, EventArgs e)
    {
        var data = new dbCommerceDataContext();
        foreach (var item in lstProdutosSolicitacao.Items)
        {
            var idComprasOrdemProduto = (HiddenField)item.FindControl("hdfIdComprasOrdemProduto");
            var txtQuantidade = (ASPxTextBox)item.FindControl("txtQuantidade");
            var txtValor = (ASPxTextBox)item.FindControl("txtValor");
            int quantidade = Convert.ToInt32(txtQuantidade.Value);
            decimal valor = Convert.ToDecimal(txtValor.Value);

            if (quantidade > 0 | valor > 0)
            {
                var compraOrdem =
                    (from c in data.tbComprasOrdemProdutos
                     where c.idComprasOrdemProduto == Convert.ToInt32(idComprasOrdemProduto.Value)
                     select c).First();
                if (quantidade > 0)
                {

                    var produtoHistorico = new tbComprasOrdemProdutoHistorico();
                    produtoHistorico.data = DateTime.Now.ToShortDateString();
                    produtoHistorico.usuario = rnUsuarios.retornaNomeUsuarioLogado();
                    produtoHistorico.idComprasOrdemProduto = compraOrdem.idComprasOrdemProduto;
                    produtoHistorico.descricao = "Quantidade na entrega alterada de " + compraOrdem.quantidade + " para " +
                                                 quantidade;
                    compraOrdem.quantidadeRecebida = quantidade;

                    data.tbComprasOrdemProdutoHistoricos.InsertOnSubmit(produtoHistorico);
                    data.SubmitChanges();
                }
                if (valor > 0)
                {

                    var produtoHistorico = new tbComprasOrdemProdutoHistorico();
                    produtoHistorico.data = DateTime.Now.ToShortDateString();
                    produtoHistorico.usuario = rnUsuarios.retornaNomeUsuarioLogado();
                    produtoHistorico.idComprasOrdemProduto = compraOrdem.idComprasOrdemProduto;
                    produtoHistorico.descricao = "Valor na entrega alterado de " + compraOrdem.preco + " para " + valor;
                    compraOrdem.precoRecebido = valor;

                    data.tbComprasOrdemProdutoHistoricos.InsertOnSubmit(produtoHistorico);
                    data.SubmitChanges();
                }

                data.SubmitChanges();
            }
        }

        FillListaPendentes();
        Response.Write("<script>alert('Ordem de compra alterada com sucesso.');</script>");
    }




    protected void lstProdutosSolicitacao_OnItemDataBound(object sender, ListViewItemEventArgs e)
    {
        var dataitem = (dynamic)e.Item.DataItem;
        var txtQuantidade = (ASPxTextBox)e.Item.FindControl("txtQuantidade");
        var txtValor = (ASPxTextBox)e.Item.FindControl("txtValor");
        if (dataitem.quantidadeRecebida != null) txtQuantidade.Value = dataitem.quantidadeRecebida;
        if (dataitem.precoRecebido != null) txtValor.Value = dataitem.precoRecebido;
    }
}