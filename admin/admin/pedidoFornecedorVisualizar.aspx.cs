﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_pedidoFornecedorVisualizar : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            fillLista();
            fillInteracoes();
            fillTotais();
        }

        lblAcao.Text = Request.QueryString["itensPendentesEntrega"] != null ? "Pedidos ao Fornecedor - Itens Pendentes de Entrega" : "Pedidos ao Fornecedor";
    }

    private void fillLista()
    {
        if (Request.QueryString["itensPendentesEntrega"] != null)
            lstProdutos.DataSource = sql2;
        else
            lstProdutos.DataSource = sql;

        lstProdutos.DataBind();
        int idPedidoFornecedor = Convert.ToInt32("0" + Request.QueryString["idPedidoFornecedor"]);
        var pedidoDc = new dbCommerceDataContext();
        var pedidoFornecedor =
            (from c in pedidoDc.tbPedidoFornecedors where c.idPedidoFornecedor == idPedidoFornecedor select c)
                .FirstOrDefault();
        if (pedidoFornecedor != null)
        {
            litDataPedido.Text = pedidoFornecedor.data.ToShortDateString();
            litDataEntrega.Text = pedidoFornecedor.dataLimite.ToShortDateString();
            litNumeroPedido.Text = idPedidoFornecedor.ToString();
            litUsuarioRomaneio.Text = pedidoFornecedor.usuario;

            var fornecedorDc = new dbCommerceDataContext();
            var fornecedor =
                (from c in fornecedorDc.tbProdutoFornecedors
                 where c.fornecedorId == pedidoFornecedor.idFornecedor
                 select c).FirstOrDefault();
            if (fornecedor != null)
            {
                litFornecedor.Text = fornecedor.fornecedorNome;
            }
        }
    }

    private void fillTotais()
    {
        var pedidosDc = new dbCommerceDataContext();
        int idPedidoFornecedor = Convert.ToInt32(Request.QueryString["idPedidoFornecedor"]);
        var pedidoItens = (from c in pedidosDc.tbPedidoFornecedorItems
                           where c.idPedidoFornecedor == idPedidoFornecedor
                           select new
                           {
                               custo = c.quantidade * c.custo,
                               diferenca = c.quantidade * ((c.diferenca == 0 ? c.custo : c.diferenca) - c.custo),
                               c.quantidade,
                               c.tbProduto.produtoPeso,
                               c.pedidosPendentes,
                               c.produtosNaoEnderecados,
                               c.encomendasNaoEntregues,
                               c.entregue
                           });
        litTotalQuantidade.Text = Request.QueryString["itensPendentesEntrega"] != null ? pedidoItens.Where(x => !x.entregue).Sum(x => x.quantidade).ToString()
                                                                                       : pedidoItens.Sum(x => x.quantidade).ToString();

        litPeso.Text = Request.QueryString["itensPendentesEntrega"] != null ? (pedidoItens.Where(x => !x.entregue).Sum(x => x.produtoPeso) / 1000) + " kg"
                                                                            : (pedidoItens.Sum(x => x.produtoPeso) / 1000) + " kg";
        /*litTotalCusto.Text = pedidoItens.Sum(x => x.custo).ToString("C");
        litTotalDiferenca.Text = pedidoItens.Sum(x => x.diferenca).ToString("C");
        litTotalGeral.Text = (pedidoItens.Sum(x => x.diferenca) + pedidoItens.Sum(x => x.custo)).ToString("C");*/

    }
    private void fillInteracoes()
    {
        var pedidosDc = new dbCommerceDataContext();
        int idPedidoFornecedor = Convert.ToInt32(Request.QueryString["idPedidoFornecedor"]);
        var interacoes =
            (from c in pedidosDc.tbPedidoFornecedorInteracaos where c.idPedidoFornecedor == idPedidoFornecedor orderby c.idPedidoFornecedorInteracao descending select c);
        dtlInteracao.DataSource = interacoes;
        dtlInteracao.DataBind();
    }


    protected void lstProdutos_OnItemDataBound(object sender, ListViewItemEventArgs e)
    {
        var data = new dbCommerceDataContext();
        var chkEntregue = (CheckBox)e.Item.FindControl("chkEntregue");
        var hdfIdPedido = (HiddenField)e.Item.FindControl("hdfIdPedido");
        var litIdPedidoCliente = (Literal)e.Item.FindControl("litIdPedidoCliente");
        var btnExcluir = (LinkButton)e.Item.FindControl("btnExcluir");
        var btnNaoEntregue = (LinkButton)e.Item.FindControl("btnNaoEntregue");
        var tdInformarCusto0 = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdInformarCusto0");

        if (!string.IsNullOrEmpty(hdfIdPedido.Value))
        {
            int idPedido = Convert.ToInt32(hdfIdPedido.Value);
            litIdPedidoCliente.Text = rnFuncoes.retornaIdCliente(idPedido).ToString();
        }
        chkEntregue.Enabled = false;


        HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
        usuarioLogadoId = Request.Cookies["usuarioLogadoId"];
        if (usuarioLogadoId == null)
        {
            return;
        }
        int usuarioId = Convert.ToInt32(usuarioLogadoId.Value);
        var autorizacaoExcluir = (from c in data.tbUsuarioPaginasPermitidas
                                  where c.paginaPermitidaNome == "excluiritemromaneio" && c.usuarioId == usuarioId
                                  select c).Any();
        if (autorizacaoExcluir)
        {
            btnExcluir.Visible = true;
        }
        var autorizacaoDesmarcar = (from c in data.tbUsuarioPaginasPermitidas
                                    where c.paginaPermitidaNome == "desmarcaritemromaneio" && c.usuarioId == usuarioId
                                    select c).Any();
        if (autorizacaoDesmarcar)
        {
            btnNaoEntregue.Visible = true;
        }


        var autorizacaoInformarCustoZero = (from c in data.tbUsuarioPaginasPermitidas
                                            where c.paginaPermitidaNome == "informarcustozeronaetiqueta" && c.usuarioId == usuarioId
                                            select c).Any();

        if (autorizacaoInformarCustoZero)
        {
            tdCabecalhoCusto0.Visible = true;
            tdInformarCusto0.Visible = true;
        }

    }

    protected void btSalvarInteracao_Click(object sender, ImageClickEventArgs e)
    {
        var pedidosDc = new dbCommerceDataContext();
        HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
        usuarioLogadoId = Request.Cookies["usuarioLogadoId"];
        if (usuarioLogadoId == null)
        {
            return;
        }
        var interacaoObj = new tbPedidoFornecedorInteracao();
        interacaoObj.data = DateTime.Now;
        interacaoObj.idPedidoFornecedor = Convert.ToInt32(Request.QueryString["idPedidoFornecedor"]);
        interacaoObj.interacao = txtInteracao.Text;
        interacaoObj.usuario = rnUsuarios.usuarioSeleciona_PorUsuarioId(int.Parse(usuarioLogadoId.Value.ToString())).Tables[0].Rows[0]["usuarioNome"].ToString();
        pedidosDc.tbPedidoFornecedorInteracaos.InsertOnSubmit(interacaoObj);
        pedidosDc.SubmitChanges();
        fillInteracoes();
        txtInteracao.Text = "";
    }

    protected void btnExcluir_OnCommand(object sender, CommandEventArgs e)
    {
        int idPedidoFornecedorItem = Convert.ToInt32(e.CommandArgument);
        var data = new dbCommerceDataContext();

        try
        {

            var estoque =
          (from c in data.tbProdutoEstoques where c.idPedidoFornecedorItem == idPedidoFornecedorItem select c)
              .FirstOrDefault();
            if (estoque != null)
            {
                var itemPedido = (from c in data.tbItemPedidoEstoques where c.idItemPedidoEstoque == estoque.idItemPedidoEstoqueReserva && c.cancelado == false && c.enviado == false select c).FirstOrDefault();
                if (itemPedido != null)
                {
                    itemPedido.reservado = false;
                    itemPedido.dataReserva = null;
                    string interacao = itemPedido.tbProduto.produtoNome + " excluído. Produto retornado à lista de aguardando estoque.";
                    rnInteracoes.interacaoInclui(itemPedido.tbItensPedido.pedidoId, interacao, rnUsuarios.retornaNomeUsuarioLogado(), "False");
                }


                data.tbProdutoEstoques.DeleteOnSubmit(estoque);
                data.SubmitChanges();
            }

            var itemTransferenciaEnderecoProduto = (from c in data.tbTransferenciaEnderecoProdutos where c.idPedidoFornecedorItem == idPedidoFornecedorItem select c).FirstOrDefault();
            if (itemTransferenciaEnderecoProduto != null)
            {
                data.tbTransferenciaEnderecoProdutos.DeleteOnSubmit(itemTransferenciaEnderecoProduto);
                data.SubmitChanges();
            }

            var item = (from c in data.tbPedidoFornecedorItems where c.idPedidoFornecedorItem == idPedidoFornecedorItem select c).FirstOrDefault();
            if (item != null)
            {

                string interacao = "Produto excluído:<br>";
                interacao += "Produto: " + item.tbProduto.produtoNome + "<br>";
                interacao += "ID da Empresa: " + item.tbProduto.produtoIdDaEmpresa + "<br>";
                interacao += "Complemento: " + item.tbProduto.complementoIdDaEmpresa + "<br>";
                interacao += "Pedido: " + item.idPedido + "<br>";
                interacao += "Item do Pedido: " + item.idItemPedido + "<br>";

                HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
                usuarioLogadoId = Request.Cookies["usuarioLogadoId"];
                if (usuarioLogadoId == null)
                {
                    return;
                }
                var interacaoObj = new tbPedidoFornecedorInteracao();
                interacaoObj.data = DateTime.Now;
                interacaoObj.idPedidoFornecedor = Convert.ToInt32(Request.QueryString["idPedidoFornecedor"]);
                interacaoObj.interacao = interacao.ToString();
                interacaoObj.usuario = rnUsuarios.usuarioSeleciona_PorUsuarioId(int.Parse(usuarioLogadoId.Value.ToString())).Tables[0].Rows[0]["usuarioNome"].ToString();
                data.tbPedidoFornecedorInteracaos.InsertOnSubmit(interacaoObj);
                data.SubmitChanges();

                if (item.idPedido != null)
                {
                    string interacaoPedido = "Produto " + item.tbProduto.produtoNome + " excluído do romaneio " + item.idPedidoFornecedor;
                    rnInteracoes.interacaoInclui(Convert.ToInt32(item.idPedido), interacaoPedido, interacaoObj.usuario, "False");

                    var pedido = (from c in data.tbPedidos where c.pedidoId == item.idPedido select c).First();
                    pedido.envioLiberado = false;
                    data.SubmitChanges();
                    rnQueue.AdicionaQueueChecagemPedidoCompleto((int)item.idPedido);
                }


                data.tbPedidoFornecedorItems.DeleteOnSubmit(item);
                data.SubmitChanges();


            }

            fillLista();
            fillInteracoes();
            fillTotais();
        }
        catch (Exception)
        {

        }



    }

    protected void btnNaoEntregue_OnCommand(object sender, CommandEventArgs e)
    {
        int idPedidoFornecedorItem = Convert.ToInt32(e.CommandArgument);
        var data = new dbCommerceDataContext();
        var estoque =
            (from c in data.tbProdutoEstoques where c.idPedidoFornecedorItem == idPedidoFornecedorItem select c)
                .FirstOrDefault();
        if (estoque != null)
        {
            var itemPedido = (from c in data.tbItemPedidoEstoques where c.idItemPedidoEstoque == estoque.idItemPedidoEstoqueReserva && c.cancelado == false && c.enviado == false select c).FirstOrDefault();
            if (itemPedido != null)
            {
                itemPedido.reservado = false;
                itemPedido.dataReserva = null;
                string interacao = itemPedido.tbProduto.produtoNome + " marcado como não entregue. Produto retornado à lista de aguardando estoque.";
                rnInteracoes.interacaoInclui(itemPedido.tbItensPedido.pedidoId, interacao, rnUsuarios.retornaNomeUsuarioLogado(), "False");
            }

            data.tbProdutoEstoques.DeleteOnSubmit(estoque);
            data.SubmitChanges();
        }

        var item =
            (from c in data.tbPedidoFornecedorItems where c.idPedidoFornecedorItem == idPedidoFornecedorItem select c)
                .FirstOrDefault();
        if (item != null)
        {
            string interacao = "Produto marcado como não entregue:<br>";
            interacao += "Produto: " + item.tbProduto.produtoNome + "<br>";
            interacao += "ID da Empresa: " + item.tbProduto.produtoIdDaEmpresa + "<br>";
            interacao += "Complemento: " + item.tbProduto.complementoIdDaEmpresa + "<br>";
            interacao += "Pedido: " + item.idPedido + "<br>";
            interacao += "Item do Pedido: " + item.idItemPedido + "<br>";

            HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
            usuarioLogadoId = Request.Cookies["usuarioLogadoId"];
            if (usuarioLogadoId == null)
            {
                return;
            }
            var interacaoObj = new tbPedidoFornecedorInteracao();
            interacaoObj.data = DateTime.Now;
            interacaoObj.idPedidoFornecedor = Convert.ToInt32(Request.QueryString["idPedidoFornecedor"]);
            interacaoObj.interacao = interacao.ToString();
            interacaoObj.usuario = rnUsuarios.usuarioSeleciona_PorUsuarioId(int.Parse(usuarioLogadoId.Value.ToString())).Tables[0].Rows[0]["usuarioNome"].ToString();
            data.tbPedidoFornecedorInteracaos.InsertOnSubmit(interacaoObj);
            data.SubmitChanges();

            if (item.idPedido != null)
            {
                string interacaoPedido = "Produto " + item.tbProduto.produtoNome + " marcado como não entregue no romaneio " + item.idPedidoFornecedor;
                rnInteracoes.interacaoInclui(Convert.ToInt32(item.idPedido), interacaoPedido, interacaoObj.usuario, "False");

                var pedido = (from c in data.tbPedidos where c.pedidoId == item.idPedido select c).First();
                pedido.envioLiberado = false;
                data.SubmitChanges();
            }

            item.entregue = false;
            item.dataEntrega = null;
            data.SubmitChanges();
        }
    }

    protected void btnCustoZero_OnCommand(object sender, CommandEventArgs e)
    {
        int idPedidoFornecedorItem = Convert.ToInt32(e.CommandArgument);

        try
        {
            using (var data = new dbCommerceDataContext())
            {
                var item =
                (from c in data.tbPedidoFornecedorItems where c.idPedidoFornecedorItem == idPedidoFornecedorItem select c)
                    .FirstOrDefault();
                if (item != null)
                {
                    string interacao = "Informado custo R$0,00 na etiqueta:<br>";
                    interacao += "Produto: " + item.tbProduto.produtoNome + "<br>";
                    interacao += "ID da Empresa: " + item.tbProduto.produtoIdDaEmpresa + "<br>";
                    interacao += "Complemento: " + item.tbProduto.complementoIdDaEmpresa + "<br>";
                    interacao += "Pedido: " + item.idPedido + "<br>";
                    interacao += "Item do Pedido: " + item.idItemPedido + "<br>";

                    item.custo = 0;

                    var usuarioLogadoId = Request.Cookies["usuarioLogadoId"];
                    if (usuarioLogadoId == null)
                    {
                        return;
                    }
                    var interacaoObj = new tbPedidoFornecedorInteracao
                    {
                        data = DateTime.Now,
                        idPedidoFornecedor = Convert.ToInt32(Request.QueryString["idPedidoFornecedor"]),
                        interacao = interacao,
                        usuario =
                            rnUsuarios.usuarioSeleciona_PorUsuarioId(int.Parse(usuarioLogadoId.Value)).Tables[0]
                                .Rows[0]["usuarioNome"].ToString()
                    };
                    data.tbPedidoFornecedorInteracaos.InsertOnSubmit(interacaoObj);
                    data.SubmitChanges();

                    if (item.idPedido != null)
                    {
                        string interacaoPedido = "Produto " + item.tbProduto.produtoNome + " marcado como custo R$0,00 no romaneio " + item.idPedidoFornecedor;
                        rnInteracoes.interacaoInclui(Convert.ToInt32(item.idPedido), interacaoPedido, interacaoObj.usuario, "False");
                        data.SubmitChanges();
                    }
                    data.SubmitChanges();
                }
            }

            fillLista();
            fillInteracoes();
            fillTotais();
        }
        catch (Exception)
        {

        }


    }
}