﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxPivotGrid;
using DevExpress.XtraPivotGrid;

public partial class admin_biProdutosPeriodo : System.Web.UI.Page
{
       

    protected void Page_Load(object sender, EventArgs e)
    {
        bool datas = true;
        if (!Page.IsPostBack)
        {
            if (txtDataInicial.Text == "" | txtDataFinal.Text == "" | txtDataInicial1.Text == "" | txtDataFinal1.Text == "")
            {
                datas = false;
            }
        }
        if(datas) fillGrid(false);
    }

    private void fillGrid(bool rebind)
    {
        var dataInicial1 = DateTime.Now;
        var dataFinal1 = DateTime.Now;
        DateTime.TryParse(txtDataInicial.Text, out dataInicial1);
        DateTime.TryParse(txtDataFinal.Text, out dataFinal1);

        var dataInicial2 = DateTime.Now;
        var dataFinal2 = DateTime.Now;
        DateTime.TryParse(txtDataInicial1.Text, out dataInicial2);
        DateTime.TryParse(txtDataFinal1.Text, out dataFinal2);

        dataFinal1 = dataFinal1.AddDays(1);
        dataFinal2 = dataFinal2.AddDays(1);
        var data = new dbCommerceDataContext();

        var relatorioGeral = (from c in data.tbItensPedidos
                         where (c.tbPedido.dataHoraDoPedido >= dataInicial1 && c.tbPedido.dataHoraDoPedido <= dataFinal1) | (c.tbPedido.dataHoraDoPedido >= dataInicial2 && c.tbPedido.dataHoraDoPedido <= dataFinal2)
                          select new
                         {
                             c.produtoId,
                             c.tbProduto.produtoNome,
                             categoria =
                                 c.tbProduto.tbJuncaoProdutoCategorias.FirstOrDefault(x => x.tbProdutoCategoria.categoriaPaiId == 0 && x.tbProdutoCategoria.exibirSite == true) ==
                                 null
                                     ? "Sem Categoria"
                                     : c.tbProduto.tbJuncaoProdutoCategorias.FirstOrDefault(
                                         x => x.tbProdutoCategoria.categoriaPaiId == 0 && x.tbProdutoCategoria.exibirSite == true)
                                         .tbProdutoCategoria.categoriaNomeExibicao,
                              colecao = c.tbProduto.tbJuncaoProdutoColecaos.FirstOrDefault() == null
                                    ? "Sem Colecao"
                                    : c.tbProduto.tbJuncaoProdutoColecaos.FirstOrDefault()
                            .tbColecao.colecaoNome,
                              c.tbProduto.tbProdutoFornecedor.fornecedorNome,
                              c.tbProduto.tbMarca.marcaNome,
                              c.valorCusto,
                              c.itemValor,
                             itemValorPeriodo1 = (c.tbPedido.dataHoraDoPedido >= dataInicial1 && c.tbPedido.dataHoraDoPedido <= dataFinal1) ? c.itemValor : 0,
                             itemValorPeriodo2 = (c.tbPedido.dataHoraDoPedido >= dataInicial2 && c.tbPedido.dataHoraDoPedido <= dataFinal2) ? c.itemValor : 0,
                             itemQuantidade = Convert.ToInt32(c.itemQuantidade),
                              itemQuantidadePeriodo1 = (c.tbPedido.dataHoraDoPedido >= dataInicial1 && c.tbPedido.dataHoraDoPedido <= dataFinal1) ? Convert.ToInt32(c.itemQuantidade) : 0,
                              itemQuantidadePeriodo2 = (c.tbPedido.dataHoraDoPedido >= dataInicial2 && c.tbPedido.dataHoraDoPedido <= dataFinal2) ? Convert.ToInt32(c.itemQuantidade) : 0,
                              statusPedido = c.tbPedido.statusDoPedido,
                              status = "Capturado"
                         }).ToList();

        var relatorioCapturado = (from c in relatorioGeral
                                 group c by c.produtoId into prods
                                 select new
                                 {
                                     prods.First().produtoId,
                                     prods.First().produtoNome,
                                     prods.First().categoria,
                                     prods.First().fornecedorNome,
                                     prods.First().marcaNome,
                                     valorCusto = prods.Sum(x => x.valorCusto),
                                     itemValor = prods.Sum(x => x.itemValor),
                                     itemValorPeriodo1 = prods.Sum(x => x.itemValorPeriodo1),
                                     itemValorPeriodo2 = prods.Sum(x => x.itemValorPeriodo2),
                                     itemQuantidade = prods.Sum(x => x.itemQuantidade),
                                     itemQuantidadePeriodo1 = prods.Sum(x => x.itemQuantidadePeriodo1),
                                     itemQuantidadePeriodo2 = prods.Sum(x => x.itemQuantidadePeriodo2),
                                     status = "Capturado"
                                 });
        var relatorioFaturado = (from c in relatorioGeral
                                 where (c.statusPedido == 3 | c.statusPedido == 4 | c.statusPedido == 5 | c.statusPedido == 9 | c.statusPedido == 10 | c.statusPedido == 11)
                                 group c by c.produtoId into prods
                                 select new
                                 {
                                     prods.First().produtoId,
                                     prods.First().produtoNome,
                                     prods.First().categoria,
                                     prods.First().fornecedorNome,
                                     prods.First().marcaNome,
                                     valorCusto = prods.Sum(x => x.valorCusto),
                                     itemValor = prods.Sum(x => x.itemValor),
                                     itemValorPeriodo1 = prods.Sum(x => x.itemValorPeriodo1),
                                     itemValorPeriodo2 = prods.Sum(x => x.itemValorPeriodo2),
                                     itemQuantidade = prods.Sum(x => x.itemQuantidade),
                                     itemQuantidadePeriodo1 = prods.Sum(x => x.itemQuantidadePeriodo1),
                                     itemQuantidadePeriodo2 = prods.Sum(x => x.itemQuantidadePeriodo2),
                                     status = "Faturado"
                                 });
        var relatorioFinal = relatorioCapturado.Union(relatorioFaturado);
        
        grd.DataSource = relatorioFinal;
        if ((!Page.IsPostBack && !Page.IsCallback) | rebind)
        {
            grd.DataBind();
        }
    }
    protected void btnPequisar_Click(object sender, ImageClickEventArgs e)
    {
        fillGrid(true);
    }

    protected void chkHoraAtual_OnCheckedChanged(object sender, EventArgs e)
    {
        fillGrid(true);
    }
    
    

    protected void grd_CustomUnboundFieldData(object sender, CustomFieldDataEventArgs e)
    {
        //if (e.Field.FieldName == "variacaoQuantidade")
        //{
        //    string nome = e.GetListSourceColumnValue("produtoNome").ToString();
        //    if(nome.ToLower().Contains("orthoflex"))
        //    {
        //        var teste = "";
        //    }
        //    var source1 = e.GetListSourceColumnValue(e.ListSourceRowIndex, "itemQuantidadePeriodo1");
        //    var source2 = e.GetListSourceColumnValue(e.ListSourceRowIndex, "itemQuantidadePeriodo2");
        //    decimal quantidade1 = Convert.ToDecimal(source1);
        //    decimal quantidade2 = Convert.ToDecimal(source2);
        //    if (quantidade1 == 0) { e.Value = 100; }
        //    else
        //    {
        //        e.Value = Math.Round((((quantidade2 * 100) / quantidade1) - 100), 2);
        //    }
        //}
    }
}