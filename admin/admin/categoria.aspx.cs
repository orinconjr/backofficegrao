﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_categoria : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["idCategoria"] != null)
        {
            gdvEnquete.Visible = false;
        }
        else
        {
            gdvEnquete.Visible = true;
        }
    }
    protected void imbIncluir_Click(object sender, ImageClickEventArgs e)
    {
        dtvAlteraEnquete.DefaultMode = DetailsViewMode.Insert;
        gdvEnquete.Visible = false;
    }
    protected void imbCancelar_Click(object sender, ImageClickEventArgs e)
    {
        gdvEnquete.Visible = true;
        gdvEnquete.DataBind();
        dtvAlteraEnquete.DefaultMode = DetailsViewMode.ReadOnly;
    }
    protected void dtvAlteraEnquete_ItemInserted(object sender, DetailsViewInsertedEventArgs e)
    {
        gdvEnquete.Visible = true;
        gdvEnquete.DataBind();
        dtvAlteraEnquete.DefaultMode = DetailsViewMode.ReadOnly;
        Response.Redirect(Request.Path.Substring(Request.Path.LastIndexOf("/") + 1));
    }
    protected void dtvAlteraEnquete_ItemUpdated(object sender, DetailsViewUpdatedEventArgs e)
    {
        gdvEnquete.Visible = true;
        gdvEnquete.DataBind();
        dtvAlteraEnquete.DefaultMode = DetailsViewMode.ReadOnly;
        Response.Redirect(Request.Path.Substring(Request.Path.LastIndexOf("/") + 1));
    }
    protected void dtvAlteraEnquete_ItemInserting(object sender, DetailsViewInsertEventArgs e)
    {

    }
    protected void gdvEnquete_RowDeleted(object sender, GridViewDeletedEventArgs e)
    {
        if (e.Exception != null)
        {
            Response.Write("<script>alert('Não foi possível excluir, verifique as dependências desse registro.');</script>");
            e.ExceptionHandled = true;
        }
    }
}