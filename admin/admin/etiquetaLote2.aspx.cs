﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_etiquetaLote2 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
            usuarioLogadoId = Request.Cookies["usuarioLogadoId"];
            if (usuarioLogadoId != null)
            {
                string paginaSolicitada = Request.Path.Substring(Request.Path.LastIndexOf("/") + 1);
                if (rnUsuarios.usuarioPaginasPermitidaSeleciona_PorUsuarioIdPaginaPermitidaNome(int.Parse(usuarioLogadoId.Value.ToString()), paginaSolicitada).Tables[0].Rows.Count == 0)
                {
                    Response.Write("<script>alert('Você não tem acesso a essa área.');</script>");
                    Response.Write("<script>window.close();</script>");
                }
            }
            else
            {
                Response.Write("<script>alert('Você não tem acesso a essa área.');</script>");
                Response.Write("<script>window.close();</script>");
            }
        }
    }

    protected void DataList1_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            int produtoId = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "produtoId"));
            string codigoDeBarras = rnProdutos.produtoSeleciona_PorProdutoId(produtoId).Tables[0].Rows[0]["produtoCodDeBarras"].ToString();
            decimal produtoPreco = Convert.ToDecimal(rnProdutos.produtoSeleciona_PorProdutoId(produtoId).Tables[0].Rows[0]["produtoPreco"].ToString());
            decimal produtoPrecoDeCusto = Convert.ToDecimal(rnProdutos.produtoSeleciona_PorProdutoId(produtoId).Tables[0].Rows[0]["produtoPrecoDeCusto"].ToString());
            decimal produtoPrecoPromocional = Convert.ToDecimal(rnProdutos.produtoSeleciona_PorProdutoId(produtoId).Tables[0].Rows[0]["produtoPrecoPromocional"].ToString());
            decimal valorDeVenda = produtoPrecoPromocional > 0 ? produtoPrecoPromocional : produtoPreco;
            Label lblPrecoDeCusto = (Label)e.Item.FindControl("lblPrecoDeCusto");
            Label lblPrecoDeVenda = (Label)e.Item.FindControl("lblPrecoDeVenda");
            Label lblMargem = (Label)e.Item.FindControl("lblMargem");
            Label lblCodigoDeBarras = (Label)e.Item.FindControl("lblCodigoDeBarras");
            ListView lstCombo = (ListView)e.Item.FindControl("lstCombo");
            lblCodigoDeBarras.Text = codigoDeBarras;

            lstCombo.DataSource = rnProdutos.produtoSelecionaCombo_porProdutoId(produtoId);
            lstCombo.DataBind();

            //lblPrecoDeCusto.Text = "R$ " + produtoPrecoDeCusto.ToString("0.00");
            //lblPrecoDeVenda.Text = "R$ " + valorDeVenda.ToString("0.00");
            //lblMargem.Text = (((valorDeVenda*100)/produtoPrecoDeCusto) - 100).ToString("0.##") + "%";


            /*Label lblQuantidade = (Label)e.Item.FindControl("lblQuantidade");
            Label lblPreco = (Label)e.Item.FindControl("lblPreco");

            decimal itemQuantidade = decimal.Parse(lblQuantidade.Text);

            lblPrecoTotal.Text = String.Format("{0:c}", decimal.Parse((produtoPreco * itemQuantidade).ToString()));*/
        }
    }

    protected void lstPedidos_OnItemDataBound(object sender, ListViewItemEventArgs e)
    {
        ObjectDataSource sqlItensPedido = (ObjectDataSource)e.Item.FindControl("sqlItensPedido");
        Label lblPedidoId = (Label)e.Item.FindControl("lblPedidoId");
        Label lblPedidoIdInterno = (Label)e.Item.FindControl("lblPedidoIdInterno");
        //Label lblCPF = (Label)e.Item.FindControl("lblCPF");
        Label lblTelResidencial = (Label)e.Item.FindControl("lblTelResidencial");
        Label lblTelComercial = (Label)e.Item.FindControl("lblTelComercial");
        Label lblTelCelular = (Label)e.Item.FindControl("lblTelCelular");
        Label lblNomeDoClienteEntrega1 = (Label)e.Item.FindControl("lblNomeDoClienteEntrega1");
        Label lblRuaEntrega1 = (Label)e.Item.FindControl("lblRuaEntrega1");
        Label lblNumeroEntrega1 = (Label)e.Item.FindControl("lblNumeroEntrega1");
        Label lblComplementoEntrega1 = (Label)e.Item.FindControl("lblComplementoEntrega1");
        Label lblBairroEntrega1 = (Label)e.Item.FindControl("lblBairroEntrega1");
        Label lblCidadeEntrega1 = (Label)e.Item.FindControl("lblCidadeEntrega1");
        Label lblEstadoEntrega1 = (Label)e.Item.FindControl("lblEstadoEntrega1");
        Label lblCepEntrega1 = (Label)e.Item.FindControl("lblCepEntrega1");
        Label lblReferenciaParaEntregaEntrega1 = (Label)e.Item.FindControl("lblReferenciaParaEntregaEntrega1");
        Label lblEnviarPor = (Label)e.Item.FindControl("lblEnviarPor");
        DataList dtlItensPedido = (DataList)e.Item.FindControl("dtlItensPedido");

        sqlItensPedido.SelectParameters.Clear();
        sqlItensPedido.SelectParameters.Add("pedidoId", lblPedidoIdInterno.Text);
        var itensPedido = rnPedidos.itensPedidoSelecionaAdmin_PorPedidoId(Convert.ToInt32(lblPedidoIdInterno.Text));
        dtlItensPedido.DataSource = itensPedido;
        dtlItensPedido.DataBind();



        int clienteId = int.Parse(rnPedidos.pedidoSeleciona_PorPedidoId(int.Parse(lblPedidoId.Text.ToString())).Tables[0].Rows[0]["clienteId"].ToString());
        lblPedidoIdInterno.Text = rnFuncoes.retornaIdCliente(Convert.ToInt32(lblPedidoId.Text)).ToString();
        //lblCPF.Text = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteCPFCNPJ"].ToString();
        lblTelResidencial.Text = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteFoneResidencial"].ToString();
        lblTelComercial.Text = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteFoneComercial"].ToString();
        lblTelCelular.Text = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteFoneCelular"].ToString();


        if (rnPedidos.pedidoSeleciona_PorPedidoId(int.Parse(lblPedidoId.Text.ToString())).Tables[0].Rows[0]["endNomeDoDestinatario"].ToString() != string.Empty)
        {
            lblNomeDoClienteEntrega1.Text = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteNome"].ToString();
            lblRuaEntrega1.Text = rnPedidos.pedidoSeleciona_PorPedidoId(int.Parse(lblPedidoId.Text.ToString())).Tables[0].Rows[0]["endRua"].ToString();
            lblNumeroEntrega1.Text = rnPedidos.pedidoSeleciona_PorPedidoId(int.Parse(lblPedidoId.Text.ToString())).Tables[0].Rows[0]["endNumero"].ToString();
            lblComplementoEntrega1.Text = rnPedidos.pedidoSeleciona_PorPedidoId(int.Parse(lblPedidoId.Text.ToString())).Tables[0].Rows[0]["endComplemento"].ToString();
            lblBairroEntrega1.Text = rnPedidos.pedidoSeleciona_PorPedidoId(int.Parse(lblPedidoId.Text.ToString())).Tables[0].Rows[0]["endBairro"].ToString();
            lblCidadeEntrega1.Text = rnPedidos.pedidoSeleciona_PorPedidoId(int.Parse(lblPedidoId.Text.ToString())).Tables[0].Rows[0]["endCidade"].ToString();
            lblEstadoEntrega1.Text = rnPedidos.pedidoSeleciona_PorPedidoId(int.Parse(lblPedidoId.Text.ToString())).Tables[0].Rows[0]["endEstado"].ToString();
            //lblPaisEntrega1.Text = rnPedidos.pedidoSeleciona_PorPedidoId(int.Parse(lblPedidoId.Text.ToString())).Tables[0].Rows[0]["endPais"].ToString();
            lblCepEntrega1.Text = rnPedidos.pedidoSeleciona_PorPedidoId(int.Parse(lblPedidoId.Text.ToString())).Tables[0].Rows[0]["endCep"].ToString();
            lblReferenciaParaEntregaEntrega1.Text = rnPedidos.pedidoSeleciona_PorPedidoId(int.Parse(lblPedidoId.Text.ToString())).Tables[0].Rows[0]["endReferenciaParaEntrega"].ToString();
        }
        else
        {
            lblNomeDoClienteEntrega1.Text = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteNome"].ToString();
            lblRuaEntrega1.Text = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteRua"].ToString();
            lblNumeroEntrega1.Text = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteNumero"].ToString();
            lblComplementoEntrega1.Text = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteComplemento"].ToString();
            lblBairroEntrega1.Text = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteBairro"].ToString();
            lblCidadeEntrega1.Text = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteCidade"].ToString();
            lblEstadoEntrega1.Text = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteEstado"].ToString();
            lblCepEntrega1.Text = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteCep"].ToString();
            lblReferenciaParaEntregaEntrega1.Text = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteReferenciaParaEntrega"].ToString();
        }
    }
}
