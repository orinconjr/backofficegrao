﻿<%@ Page Title="" Theme="Glass" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="relatorioTransportadora.aspx.cs" Inherits="admin_relatorioTransportadora" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxTimer" TagPrefix="dxt" %>
<%@ Register assembly="CuteEditor" namespace="CuteEditor" tagprefix="CE" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" Runat="Server">
        <script language="javascript" type="text/javascript">
            function ApplyFilter(dde, dateFrom, dateTo) {
                var d1 = dateFrom.GetText();
                var d2 = dateTo.GetText();
                if (d1 == "" || d2 == "")
                    return;
                dde.SetText(d1 + "|" + d2);
                //grd.ApplyFilter("[dataHoraDoPedido] >= '" + d1 + " 00:00:00' && [dataHoraDoPedido] <= '" + d2 + " 23:59:59'");
                grd.AutoFilterByColumn("dataEmbaladoFim", dde.GetText());
            }

            function OnDropDown(s, dateFrom, dateTo) {
                var str = s.GetText();
                if (str == "") {
                    // default date (1950-1961 for demo)     
                    dateFrom.SetDate(new Date(1950, 0, 1));
                    dateTo.SetDate(new Date(1960, 11, 31));
                    return;
                }
                var d = str.split("|");
                dateFrom.SetText(d[0]);
                dateTo.SetText(d[1]);
            }
    </script>

    <table cellpadding="0" cellspacing="0" style="width: 920px">
    <tr>
        <td class="tituloPaginas" valign="top">
            <asp:Label ID="lblAcao" runat="server">Relatório Transportadoras</asp:Label>
        </td>
    </tr>
        
    <tr>
        <td>
            
    <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
        <tr>
            <td>
                <asp:Panel runat="server" ID="pnGridVerPedidos">
                    <dxwgv:ASPxGridView ID="grd" ClientInstanceName="grd" runat="server" AutoGenerateColumns="False" Width="834px" Cursor="auto" OnCustomUnboundColumnData="grd_OnCustomUnboundColumnData"
                        OnAutoFilterCellEditorCreate="grid_AutoFilterCellEditorCreate" OnAutoFilterCellEditorInitialize="grid_AutoFilterCellEditorInitialize"
                        OnProcessColumnAutoFilter="grid_ProcessColumnAutoFilter" KeyFieldName="pedidoId" >
                        <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                        <Styles>
                            <Footer Font-Bold="True">
                            </Footer>
                        </Styles>
                        <SettingsText ConfirmDelete="Tem certeza que deseja excluir?" emptydatarow="Nenhum registro encontrado." />
                        <SettingsPager Position="TopAndBottom" PageSize="500" ShowDisabledButtons="False" AlwaysShowPager="True">
                            <Summary AllPagesText="Páginas: {0} - {1} ({2} items)" Text="Página {0} de {1} ({2} registros encontrados)" />
                        </SettingsPager>
                        <TotalSummary>
                            <dxwgv:ASPxSummaryItem DisplayFormat="c" FieldName="valorSelecionado" ShowInColumn="valorSelecionado" ShowInGroupFooterColumn="valorSelecionado" SummaryType="Sum" />
                            <dxwgv:ASPxSummaryItem DisplayFormat="c" FieldName="valorCobradoTransportadora" ShowInColumn="valorCobradoTransportadora" ShowInGroupFooterColumn="valorCobradoTransportadora" SummaryType="Sum" />
                        </TotalSummary>
                        <settings showfilterrow="True" ShowGroupButtons="False" ShowHeaderFilterButton="True" ShowFooter="True" />
                        <SettingsEditing EditFormColumnCount="4" PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1" PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter" PopupEditFormWidth="700px" />
                        <Columns>
                            <dxwgv:GridViewDataTextColumn Caption="Pedido" FieldName="pedidoIdCliente" VisibleIndex="0" UnboundType="Integer">
                            </dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataTextColumn Caption="ID" FieldName="pedidoId" VisibleIndex="0" Visible="False">
                            </dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataDateColumn Caption="Data" FieldName="dataEmbaladoFim" VisibleIndex="0">
                            </dxwgv:GridViewDataDateColumn>
                            <dxwgv:GridViewDataTextColumn Caption="Forma de Envio" FieldName="formaDeEnvio" VisibleIndex="0">
                            </dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataTextColumn Caption="Volumes" FieldName="volumesEmbalados" VisibleIndex="0">
                            </dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataTextColumn Caption="Valor Total" FieldName="valorSelecionado" VisibleIndex="0">
                                <PropertiesTextEdit DisplayFormatString="c">
                                </PropertiesTextEdit>
                            </dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataTextColumn Caption="Valor Recebido" FieldName="valorCobradoTransportadora" VisibleIndex="0">
                                <PropertiesTextEdit DisplayFormatString="c">
                                </PropertiesTextEdit>
                            </dxwgv:GridViewDataTextColumn>
                              <dxwgv:GridViewDataTextColumn Caption="Taxa Extra" FieldName="taxaExtra" VisibleIndex="0">
                                <PropertiesTextEdit DisplayFormatString="c">
                                </PropertiesTextEdit>
                            </dxwgv:GridViewDataTextColumn>
                              <dxwgv:GridViewDataTextColumn Caption="valor total + taxa extra" FieldName="valorTotalComTaxaExtra" VisibleIndex="0">
                                <PropertiesTextEdit DisplayFormatString="c">
                                </PropertiesTextEdit>
                            </dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataTextColumn Caption="Editar" Name="editar" VisibleIndex="0" Width="80">
                                <DataItemTemplate>
                                    <asp:LinkButton runat="server" OnCommand="btnEditarValor_OnCommand" ID="btnEditarValor" CommandArgument='<%# Eval("pedidoId") %>' EnableViewState="False">Editar</asp:LinkButton>
                                </DataItemTemplate>
                            </dxwgv:GridViewDataTextColumn>
                        </Columns>
                        <StylesEditors>
                            <Label Font-Bold="True">
                            </Label>
                        </StylesEditors>
                    </dxwgv:ASPxGridView> 
                </asp:Panel>  

                <asp:Panel runat="server" ID="pnGridEditar" Visible="False">
                    <table style="width: 100%">
                        <tr class="rotulos" style="font-weight: bold;">
                            <td>
                                <asp:HiddenField runat="server" ID="hiddenIdPedidoJadlogEditar" />
                                Pedido: <asp:Literal runat="server" ID="litPedidoIdDiferenca"></asp:Literal>
                            </td>
                        </tr>
                        <tr class="rotulos">
                            <td>
                                Valor recebido:<br/>
                                <asp:TextBox runat="server" ID="txtValorDiferenca" CssClass="campos" Text="0"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="rotulos">
                            <td style="padding-top: 10px;">
                                <asp:Button runat="server" ID="btnGravarDiferenca" Text="Gravar valor recebido" OnClick="btnGravarDiferenca_onClick" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>       
            </td>
        </tr>
        <tr>
            <td style="padding-top: 100px;">
                &nbsp;
            </td>
        </tr>
    </table>
        </td>
    </tr>
</table>
</asp:Content>