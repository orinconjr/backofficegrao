﻿using System;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxGridView;
using System.Collections.Generic;
using GraoDeGente.Infrastructure.Services.Marketplace.Interfaces;
using GraoDeGente.Infrastructure.Services.Marketplace;
using GraoDeGente.Infrastructure.Services.Marketplace.Models;
using System.Linq.Expressions;
using GraoDeGente.Infrastructure.Services.Marketplace.Orders;

public partial class admin_pedidosMarketplace : System.Web.UI.Page
{
    private readonly ISyncResource _syncResource = new SyncResource();
    private readonly IOrdersResource _ordersResource = new OrdersResource();

    protected void Page_Load(object sender, EventArgs e)
    {
        ASPxGridView.RegisterBaseScript(Page);

        PopularGrid();
    }

    protected void btPdf_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WritePdfToResponse();
    }
    protected void btXsl_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteXlsToResponse();
    }
    protected void btRtf_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteRtfToResponse();
    }
    protected void btCsv_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteCsvToResponse();
    }

    protected void btnExportarXls_Click(object sender, EventArgs e)
    {
        this.grdEx.WriteXlsToResponse();
    }

    protected void btnImportarPedidos_Click(object sender, EventArgs e)
    {
        var request = new SyncOrdersRequest()
        {
            Marketplace = this.obterMarketplaceSelecionado()
        };

        var response = this._syncResource.PostOrdersStatusNew(request);
        if (!response.Success)
        {
            this.Alert(string.Concat("Ocorreu um erro ao sincronizar novos pedidos. ", response.Exception.Message));
            return;
        }

        response = this._syncResource.PostOrdersStatusApproved(request);
        if (!response.Success)
        {
            this.Alert(string.Concat("Ocorreu um erro ao sincronizar pedidos pagos. ", response.Exception.Message));
            return;
        }

        response = this._syncResource.PostOrdersStatusCanceled(request);
        if (!response.Success)
        {
            this.Alert(string.Concat("Ocorreu um erro ao sincronizar pedidos cancelados. ", response.Exception.Message));
            return;
        }

        this.Alert("Pedidos importados com sucesso!");

        PopularGrid();
    }

    protected void btnEnviarPedido_OnCommand(object sender, CommandEventArgs e)
    {
        var pedidoId = Convert.ToInt32(e.CommandArgument);
        var request = new UpdateOrderStatusSentRequest()
        {
            OrderId = pedidoId
        };

        var response = this._ordersResource.PostStatusSent(request);
        if (!response.Success)
        {
            this.Alert(string.Concat("Ocorreu um erro ao enviar pedido. ", response.Exception.Message));
            return;
        }

        this.Alert("Pedido enviado com sucesso!");

        PopularGrid();
    }

    private void PopularGrid()
    {
        grd.DataSource = this.obterPedidos();
        grd.DataBind();
    }

    private List<PedidoDTO> obterPedidos()
    {

        byte? marketplace = null;
        byte? statusIntegracao = null;

        if (!string.IsNullOrEmpty(ddlMarketplace.SelectedValue))
            marketplace = Convert.ToByte(ddlMarketplace.SelectedValue);

        if (!string.IsNullOrEmpty(ddlStatusIntegracao.SelectedValue))
            statusIntegracao = Convert.ToByte(ddlStatusIntegracao.SelectedValue);

        Expression<Func<Marketplace_Pedido, bool>> predicate = c =>
            (!marketplace.HasValue || c.Marketplace == marketplace) &&
            (!statusIntegracao.HasValue || c.Status == statusIntegracao);

        using (var dbContext = new dbCommerceDataContext())
        {
            var pedidos = dbContext.Marketplace_Pedidos
             .Where(predicate)
             .OrderByDescending(o => o.DataCriacao);

            return pedidos
                .Select(s => new PedidoDTO
                {
                    PedidoId = s.IDPedido,
                    Cliente = this.obterCliente(s.tbPedido.clienteId),
                    Total = s.tbPedido.valorTotalGeral,
                    Site = s.Marketplace_Site.Nome,
                    StatusPedido = this.obterStatusPedido(s.tbPedido.statusDoPedido),
                    StatusIntegracao = this.obterStatusIntegracao(s.Status),
                    DataCriacao = s.DataCriacao,
                    PedidoMarketplaceId = s.IDPedidoMarketplace,
                    PermiteEnviar = s.tbPedido.statusDoPedido == 3
                })
                .ToList();
        }
    }

    private string obterStatusPedido(int status)
    {

        switch (status)
        {
            case 1:
                return "Criado";
            case 2:
                return "Aguardando Pagamento";
            case 3:
                return "Pagamento Confirmado";
            case 4:
                return "Embalando";
            case 5:
                return "Enviado";
            case 6:
                return "Cancelado";
            case 7:
                return "Pagamento não autorizado";
            case 8:
                return "Devolvido";
            case 9:
                return "Em fabricação";
            case 10:
                return "A Resolver";
            case 11:
                return "Em separação";
            default:
                break;
        }

        return null;
    }

    private string obterCliente(int? clienteId)
    {
        using (var dbContext = new dbCommerceDataContext())
        {
            var cliente = dbContext.tbClientes.FirstOrDefault(i => i.clienteId == clienteId);
            if (cliente != null)
                return cliente.clienteNome;
        }
        return null;
    }

    private string obterStatusIntegracao(byte status)
    {
        switch (status)
        {
            case (byte)IntegrationStatus.Pending:
                return "Pendente";
            case (byte)IntegrationStatus.Integrated:
                return "Integrado";
            case (byte)IntegrationStatus.Error:
                return "Erro";
            default:
                break;
        }

        return null;
    }

    private MarketplaceEnum obterMarketplaceSelecionado()
    {
        var marketplace = Convert.ToByte(ddlMarketplace.SelectedValue);
        return (MarketplaceEnum)marketplace;
    }

    private void Alert(string message)
    {
        ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), string.Format("alert('{0}');", message), true);
    }

    public class PedidoDTO
    {
        public int PedidoId { get; set; }
        public string Cliente { get; set; }
        public string StatusPedido { get; set; }
        public string StatusIntegracao { get; set; }
        public decimal? Total { get; set; }
        public string PedidoMarketplaceId { get; set; }
        public string Site { get; set; }
        public DateTime DataCriacao { get; set; }
        public bool PermiteEnviar { get; set; }
    }
}
