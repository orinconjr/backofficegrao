﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="materiaPrimaProduto.aspx.cs" Inherits="admin_materiaPrimaProduto" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" runat="Server">
    <style>
        .meugrid {
            background-color: #F4FAFB;
            border: 0px none;
            /* border-collapse: separate !important; */
            overflow: hidden;
            font: 9pt Tahoma;
            color: #000;
            cursor: pointer;
            white-space: nowrap;
            font-weight: normal;
            width: 805px;
            text-align: left;
            margin-left: 4px;
            width: 805px;
        }

            .meugrid th {
                background-image: url(../../App_Themes/Glass/GridView/gvHeaderBackground.gif);
                background-position: center top;
                background-repeat: repeat-x;
                background-color: #C0DDE0;
                overflow: hidden;
                height: 23px;
                line-height: 23px;
                padding: 4px 6px 5px;
                font-weight: normal;
                border: 1px solid #7EACB1;
                /* border-collapse: separate;*/
            }

            .meugrid td {
                overflow: hidden;
                height: 23px;
                line-height: 23px;
                padding: 4px 6px 5px;
                border: 1px solid #7EACB1;
            }

            .meugrid tr td:last-of-type {
                text-align: center;
            }

        .rotulos td {
            font: bold 12px tahoma;
            padding-top: 20px;
            color: rgb(65, 105, 126);
            padding-left: 5px;
            padding-bottom: 5px;
        }
    </style>
    <script src="js/jquery.min.js"></script>
    <script type="text/javascript">
        function validaform() {
            var valido = true;
            var mensagem = "";
            var txtquantidade = $('input[id *= txtQuantidade]').val().replace(",", ".");
            var materiaprima = $('select[id *= ddlComprasProduto]').val();
            if (txtquantidade == "0" || txtquantidade == "") {
                mensagem = "Informe a quantidade\n";
                valido = false;
            } else {
                if (isNaN(txtquantidade)) {
                    mensagem = "Informe apenas valores núméricos para a quantidade\n";
                    valido = false;
                }
            }
            if (materiaprima == "-1") {
                mensagem += "Selecione a matéria prima";
                valido = false;
            }
            if (!valido) {
                alert(mensagem);
            }
            $('input[id *= txtQuantidade]').val(txtquantidade.replace(".", ","));
            return valido;

        }
    </script>
    <table cellpadding="0" cellspacing="0" style="width: 920px">
        <tr>
            <td class="tituloPaginas" valign="top">
                <asp:Label ID="lblAcao" runat="server">Matéria Prima por produto</asp:Label>
            </td>
        </tr>

        <tr>
            <td>

                <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
                    <tr>
                        <td>
                            <div style="float: left; clear: left;">
                                <img id="imgfotodestaque" runat="server" style="width: 250px;"/>
                            </div>
                             <div style="float: left;font: bold 16px tahoma;color: #41697E;padding-left: 20px;">
                                 <asp:Label ID="lblprodutonome" runat="server" Text="Label"></asp:Label>
                            </div>
                        </td>
                    </tr>


                    <tr class="campos">
                        <td>
                            <table style="width: 100%">
                                <tr class="rotulos">
                                    <td>Produto:<br />
                              
                                        <asp:DropDownList ID="ddlComprasProduto" Width="400" runat="server" AppendDataBoundItems="True" DataTextField="produto" DataValueField="idComprasProduto">
                                            <asp:ListItem Value="-1">
                                                -- Escolha uma matéria prima --
                                            </asp:ListItem>
                                        </asp:DropDownList>
                                     
                                    </td>
                                    <td>
                                        Quantidade:<br />
                                        <asp:TextBox ID="txtQuantidade" Width="100" runat="server"></asp:TextBox>
                                    </td>
                                    <td>
                                        Largura:<br />
                                        <asp:TextBox ID="txtLargura" Width="100" runat="server"></asp:TextBox>
                                    </td>
                                    <td>
                                        Comprimento:<br />
                                        <asp:TextBox ID="txtComprimento" Width="100" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr class="rotulos">
                                    <td colspan="3">
                                        Observação:<br />
                                        <asp:TextBox ID="txtObservacao" Width="600" runat="server"></asp:TextBox>
                                    </td>
                                    <td>&nbsp;<br />
                                        <asp:Button runat="server" ID="btnGravarProduto" Text="Adicionar Produto"  OnClientClick="return validaform();"  OnClick="btnGravarProduto_Click" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr class="rotulos">
                        <td>
                            Matérias primas:
                        </td>
                    </tr>
                    <tr>
                        <td>

                            <asp:GridView ID="GridView1" CssClass="meugrid" DataKeyNames="idProdutoMateriaPrima" BorderWidth="0px"
                                runat="server" AutoGenerateColumns="False" OnRowDeleting="GridView1_RowDeleting" OnRowDataBound="GridView1_RowDataBound">
                                <Columns>
                                    <asp:BoundField DataField="produtoNome" HeaderText="Produto"></asp:BoundField>
                                    <asp:BoundField DataField="consumo" HeaderText="Consumo"></asp:BoundField>
                                    <asp:BoundField DataField="largura" HeaderText="Largura"></asp:BoundField>
                                    <asp:BoundField DataField="comprimento" HeaderText="Comprimento"></asp:BoundField>
                                    
                                    <asp:TemplateField HeaderText="Valor Unitário">
                                         <ItemTemplate>
                                             <asp:Label ID="lblvalorunitario" runat="server" ></asp:Label>
                                         </ItemTemplate>
                                     </asp:TemplateField>
                                      <asp:TemplateField HeaderText="Total">
                                         <ItemTemplate>
                                             <asp:Label ID="lblvalortotal" runat="server" ></asp:Label>
                                         </ItemTemplate>
                                     </asp:TemplateField>
                                    
                                   
                                    <asp:BoundField DataField="fatorConversao" HeaderText="Fator Conversão"></asp:BoundField>
                                    <asp:BoundField DataField="observacao" HeaderText="Observação"></asp:BoundField>

                                    <asp:TemplateField HeaderText="Excluir" ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="cmdDelete" runat="server" CausesValidation="False" CommandName="Delete"
                                                Text="Delete">
                                            <img  src="images/btExcluir.jpg" alt="Excluir" style="border-style:none;"/>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>

                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right;">&nbsp;
                        </td>
                    </tr>


                </table>
            </td>
        </tr>
    </table>
</asp:Content>

