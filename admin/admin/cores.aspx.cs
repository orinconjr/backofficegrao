﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;

public partial class admin_cores : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!Page.IsPostBack) PopulateRootLevel();
    }

    #region categorias

    protected void treeCategorias_PreRender(object sender, EventArgs e)
    {
        treeCategorias.Attributes.Add("OnClick", "client_OnTreeNodeChecked(event)");
    }

    private void PopulateRootLevel()
    {
        PopulateNodes(rnCategorias.categoriasSelecionaNivelPai().Tables[0], treeCategorias.Nodes);
    }

    private void PopulateSubLevel(int categoriaPaiId, TreeNode parentNode)
    {
        PopulateNodes(rnCategorias.categoriasSelecionaNivelFilho(categoriaPaiId).Tables[0], parentNode.ChildNodes);
    }

    protected void treeCategorias_TreeNodePopulate(object sender, TreeNodeEventArgs e)
    {
        PopulateSubLevel(Int32.Parse(e.Node.Value), e.Node);
    }

    private void PopulateNodes(DataTable dt, TreeNodeCollection nodes)
    {
        foreach (DataRow dr in dt.Rows)
        {
            TreeNode tn = new TreeNode();
            tn.Text = dr["categoriaNome"].ToString();
            tn.Value = dr["categoriaId"].ToString();

            //desabilita os links do treeview
            nodes.Add(tn);

            //If node has child nodes, then enable on-demand populating
            tn.PopulateOnDemand = ((int)(dr["childnodecount"]) > 0);
        }
    }

    protected void treeCategorias_SelectedNodeChanged(object sender, EventArgs e)
    {
        desmarcaCheckbox(treeCategorias.Nodes);
        marcaCheckbox(treeCategorias.SelectedNode.Value.ToString(), treeCategorias.Nodes);
    }

    private void desmarcaCheckbox(TreeNodeCollection treeview)
    {
        int i = 0;
        foreach (TreeNode tre in treeview)
        {
            //utilizando a recursividade
            this.desmarcaCheckbox(treeview[i].ChildNodes);
            //faz a busca por trecho de texto

            tre.Checked = false; //desmarca o checkbox do item encontrado
            if (tre.Parent == null) //se não tiver parent (caso do 1º nivel)
                tre.Expand(); //expande o nivel atual
            else
                tre.Parent.Expand(); //expande o nivel anterior

            i++;
        }
    }

    private void marcaCheckbox(string valorProcurado, TreeNodeCollection treeview)
    {
        int i = 0;
        foreach (TreeNode tre in treeview)
        {
            //utilizando a recursividade
            this.marcaCheckbox(valorProcurado, treeview[i].ChildNodes);
            //faz a busca por trecho de texto
            if (tre.Value.ToString() == valorProcurado)
            {
                tre.Checked = true; //marca o checkbox do item encontrado
                if (tre.Parent == null) //se não tiver parent (caso do 1º nivel)
                    tre.Expand(); //expande o nivel atual
                else
                    tre.Parent.Expand(); //expande o nivel anterior
            }
            i++;
        }
    }

    private int pegaItensMarcados()
    {
        int categoriaPaiId = 0;
        if (treeCategorias.CheckedNodes.Count > 0)
        {
            foreach (TreeNode node in treeCategorias.CheckedNodes)
            {
                categoriaPaiId = Convert.ToInt32(node.Value);
            }
        }
        return categoriaPaiId;
    }

    #endregion

    //protected void btPdf_Click(object sender, ImageClickEventArgs e)
    //{
    //    this.grdEx.WritePdfToResponse();
    //}
    //protected void btXsl_Click(object sender, ImageClickEventArgs e)
    //{
    //    this.grdEx.WriteXlsToResponse();
    //}
    //protected void btRtf_Click(object sender, ImageClickEventArgs e)
    //{
    //    this.grdEx.WriteRtfToResponse();
    //}
    //protected void btCsv_Click(object sender, ImageClickEventArgs e)
    //{
    //    this.grdEx.WriteCsvToResponse();
    //}

    protected void grd_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType != GridViewRowType.EditForm) return;
        {
            Image image1 = (Image)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "imagem1");
            Image image2 = (Image)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "imagem2");
            Image image3 = (Image)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "imagem3");

            CheckBox ckbExcluir1 = (CheckBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "ckbExcluir1");
            CheckBox ckbExcluir2 = (CheckBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "ckbExcluir2");
            CheckBox ckbExcluir3 = (CheckBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "ckbExcluir3");

            #region Oculta imagem quando for inserir um novo registro ou quando for editar um registro e não existir imagem no bd
            if (image1.ImageUrl.ToString() == "../cores//" || image1.ImageUrl.ToString() == "../cores/" + e.KeyValue + "/")
            {
                image1.Visible = false;
                ckbExcluir1.Visible = false;
            }

            if (image2.ImageUrl.ToString() == "../cores//" || image2.ImageUrl.ToString() == "../cores/" + e.KeyValue + "/")
            {
                image2.Visible = false;
                ckbExcluir2.Visible = false;
            }

            if (image3.ImageUrl.ToString() == "../cores//" || image3.ImageUrl.ToString() == "../cores/" + e.KeyValue + "/")
            {
                image3.Visible = false;
                ckbExcluir3.Visible = false;
            }
            #endregion
        }
    }

    protected void grd_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        sqlCategorias.InsertParameters.Clear();

        sqlCategorias.InsertParameters.Add("categoriaId", pegaItensMarcados().ToString());

        ASPxTextBox txtNome = (ASPxTextBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["corNome"], "txtNome");
        ASPxTextBox txtCorUrl = (ASPxTextBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["corUrl"], "txtCorUrl");
        FileUpload flu1 = (FileUpload)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "flu1");
        FileUpload flu2 = (FileUpload)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "flu2");
        FileUpload flu3 = (FileUpload)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "flu3");

        sqlCategorias.InsertParameters.Add("corNome", txtNome.Text);
        sqlCategorias.InsertParameters.Add("corUrl", txtCorUrl.Text);
        
        if (flu1.HasFile)
            sqlCategorias.InsertParameters.Add("imagem1", "imagem1.jpg");
        else
            sqlCategorias.InsertParameters.Add("imagem1", string.Empty);

        if (flu2.HasFile)
            sqlCategorias.InsertParameters.Add("imagem2", "imagem2.jpg");
        else
            sqlCategorias.InsertParameters.Add("imagem2", string.Empty);

        if (flu3.HasFile)
            sqlCategorias.InsertParameters.Add("imagem3", "imagem3.jpg");
        else
            sqlCategorias.InsertParameters.Add("imagem3", string.Empty);
    }

    protected void grd_RowInserted(object sender, DevExpress.Web.Data.ASPxDataInsertedEventArgs e)
    {
        if (e.Exception == null)
        {
            FileUpload flu1 = (FileUpload)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "flu1");
            FileUpload flu2 = (FileUpload)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "flu2");
            FileUpload flu3 = (FileUpload)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "flu3");

            var corData = new dbCommerceDataContext();

            int ultimoId = (from c in corData.tbCores orderby c.corId descending select c).FirstOrDefault().corId;
            Directory.CreateDirectory(ConfigurationManager.AppSettings["caminhoFisico"] + "cores\\" + ultimoId);

            if (flu1.HasFile) flu1.PostedFile.SaveAs(ConfigurationManager.AppSettings["caminhoFisico"] + "cores\\" + ultimoId + "\\" + "imagem1.jpg");

            if (flu2.HasFile)
                flu2.PostedFile.SaveAs(ConfigurationManager.AppSettings["caminhoFisico"] + "cores\\" + ultimoId + "\\" + "imagem2.jpg");

            if (flu3.HasFile)
                flu3.PostedFile.SaveAs(ConfigurationManager.AppSettings["caminhoFisico"] + "cores\\" + ultimoId + "\\" + "imagem3.jpg");

            Response.Write("<script>window.location=('cores.aspx');</script>");
        }
    }

    protected void grd_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        sqlCategorias.UpdateParameters.Clear();

        sqlCategorias.UpdateParameters.Add("categoriaId", pegaItensMarcados().ToString());

        ASPxTextBox txtNome = (ASPxTextBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["corNome"], "txtNome");
        ASPxTextBox txtCorUrl = (ASPxTextBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["corUrl"], "txtCorUrl");
        FileUpload flu1 = (FileUpload)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "flu1");
        FileUpload flu2 = (FileUpload)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "flu2");
        FileUpload flu3 = (FileUpload)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "flu3");

        CheckBox ckbExcluir1 = (CheckBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "ckbExcluir1");
        CheckBox ckbExcluir2 = (CheckBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "ckbExcluir2");
        CheckBox ckbExcluir3 = (CheckBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "ckbExcluir3");

        int categoriaId = Convert.ToInt32(e.Keys[0]);

        sqlCategorias.UpdateParameters.Add("corNome", txtNome.Text);
        sqlCategorias.UpdateParameters.Add("corUrl", txtCorUrl.Text);


        if (flu1.HasFile)
            sqlCategorias.UpdateParameters.Add("imagem1", "imagem1.jpg");
        else
            sqlCategorias.UpdateParameters.Add("imagem1", string.Empty);

        if (flu2.HasFile)
            sqlCategorias.UpdateParameters.Add("imagem2", "imagem2.jpg");
        else
            sqlCategorias.UpdateParameters.Add("imagem2", string.Empty);

        if (flu3.HasFile)
            sqlCategorias.UpdateParameters.Add("imagem3", "imagem3.jpg");
        else
            sqlCategorias.UpdateParameters.Add("imagem3", string.Empty);

        if (ckbExcluir1.Checked == true)
            rnCategorias.excluiFoto(categoriaId, "imagem1.jpg");

        if (ckbExcluir2.Checked == true)
            rnCategorias.excluiFoto(categoriaId, "imagem2.jpg");

        if (ckbExcluir3.Checked == true)
            rnCategorias.excluiFoto(categoriaId, "imagem3.jpg");
    }

    protected void grd_RowUpdated(object sender, DevExpress.Web.Data.ASPxDataUpdatedEventArgs e)
    {
        if (e.Exception == null)
        {
            FileUpload flu1 = (FileUpload)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "flu1");
            FileUpload flu2 = (FileUpload)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "flu2");
            FileUpload flu3 = (FileUpload)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "flu3");

            int categoriaId = Convert.ToInt32(e.Keys[0]);

            if (flu1.HasFile)
                flu1.PostedFile.SaveAs(ConfigurationManager.AppSettings["caminhoFisico"] + "cores\\" + categoriaId + "\\" + "imagem1.jpg");

            if (flu2.HasFile)
                flu2.PostedFile.SaveAs(ConfigurationManager.AppSettings["caminhoFisico"] + "cores\\" + categoriaId + "\\" + "imagem2.jpg");

            if (flu3.HasFile)
                flu3.PostedFile.SaveAs(ConfigurationManager.AppSettings["caminhoFisico"] + "cores\\" + categoriaId + "\\" + "imagem3.jpg");

            Response.Write("<script>window.location=('cores.aspx');</script>");
        }
    }

    protected void grd_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
    {
        
    }

    protected void grd_RowDeleted(object sender, DevExpress.Web.Data.ASPxDataDeletedEventArgs e)
    {
        int categoriaId = Convert.ToInt32(e.Keys[0]);

        if (Directory.Exists(ConfigurationManager.AppSettings["caminhoFisico"] + "cores\\" + categoriaId))
        {
            Directory.Delete(ConfigurationManager.AppSettings["caminhoFisico"] + "cores\\" + categoriaId, true);
        }

        Response.Write("<script>window.location=('cores.aspx');</script>");
    }
}
