﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="dashboardpagamentos.aspx.cs" Inherits="admin_dashboardpagamentos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" runat="Server">
    <script src="js/jquery.min.js"></script>
    <style>
        .fieldsetatualizarprecos {
            width: 760px;
            float: left;
            clear: left;
            border-radius: 5px;
            margin-left: 25px;
        }

            .fieldsetatualizarprecos input[type=checkbox] {
                margin-top: 15px;
            }

            .fieldsetatualizarprecos label {
                margin-left: 5px;
                margin-right: 20px;
            }

        .meugrid {
            background-color: #F4FAFB;
            border: 0px none;
            /* border-collapse: separate !important; */
            overflow: hidden;
            font: 9pt Tahoma;
            color: #000;
            /* white-space: nowrap;*/
            font-weight: normal;
            text-align: left;
            width: 834px;
            border-collapse: collapse;
            float: left;
            clear: left;
            margin-top: 10px;
            margin-bottom: 10px;
            min-height: 500px;
            margin-left: 25px;
        }

            .meugrid th {
                background-image: url(../../App_Themes/Glass/GridView/gvHeaderBackground.gif);
                background-position: center top;
                background-repeat: repeat-x;
                background-color: #C0DDE0;
                overflow: hidden;
                height: 23px;
                line-height: 23px;
                padding: 4px 6px 5px;
                font-weight: normal;
                border: 1px solid #7EACB1;
                /* border-collapse: separate;*/
            }

            .meugrid td {
                overflow: hidden;
                height: 23px;
                line-height: 23px;
                padding: 4px 6px 5px;
                border: 1px solid #7EACB1;
            }

            .meugrid tr td:nth-child(4) {
                text-align: center;
            }

            .meugrid tr td:last-of-type {
                text-align: center;
            }

        .tabrotulos {
            width: 848px;
            margin: auto;
        }

        .materiaprima {
            width: 250px;
        }

            .materiaprima ul {
                margin: 0px;
                padding-left: 10px;
            }

                .materiaprima ul li {
                    max-width: 230px;
                    white-space: pre-wrap;
                }

        .meugrid tr td:last-of-type {
            text-align: center;
        }

        .nome {
            width: 237px;
        }

            .nome div {
                max-width: 230px;
                white-space: pre-wrap;
            }

        .rotulos td {
            font: bold 12px tahoma;
            padding-top: 20px;
            color: rgb(65, 105, 126);
            padding-left: 5px;
            padding-bottom: 5px;
        }

        /*****************************/
        .width150 {
        }

        .height150 {
        }

        .height80 {
        }

        .btn {
            -webkit-border-radius: 7px;
            -moz-border-radius: 7px;
            border-radius: 2px;
            font-family: Arial;
            color: #ffffff;
            font-size: 14px;
            background: #63c269;
            padding: 5px 10px 5px 10px;
            text-decoration: none;
            cursor: pointer;
            border: 0px;
        }

            .btn:hover {
                background: #a4dbab;
                text-decoration: none;
                color: black;
                font-weight: bold;
            }


        .marginTop5porcento {
            margin: 6%;
            font-size: 15px;
        }

        .quadroPrincipal {
            float: left;
            width: 150px;
            height: 155px;
            text-align: center;
            position: relative;
            font-family: monospace;
            border: 1px solid #B1B1B1;
            margin: 0 14px 5px 14px;
            border-radius: 15px;
        }

        .quadroInteriorQtds {
            margin-top: 5%;
            text-align: center;
            font-size: 30px;
        }

        .AlinharAoRodapeCentralizar {
            position: absolute;
            bottom: 5px;
            text-align: center;
            width: 150px;
        }
    </style>

    <div class="tituloPaginas" valign="top">
        Status de Pagamentos
        <asp:HiddenField ID="hdstatusatual" runat="server" />
    </div>

    <asp:Panel ID="panelPaiQuadros" runat="server">
        <div class="quadroPrincipal" id="quadronprocclerasale" runat="server">
            <div class="marginTop5porcento">NÃO PROCESSADOS CLEARSALE</div>
            <div class="quadroInteriorQtds">
                <asp:Literal runat="server" ID="litnprocclearsale"></asp:Literal>
            </div>
            <div class="AlinharAoRodapeCentralizar">
                <asp:Button runat="server" ID="btnnprocclearsale" Text="Detalhar" CssClass="btn" OnClick="btnnprocclearsale_Click" />
            </div>
        </div>

        <div class="quadroPrincipal" id="quadroanaliseclearsale" runat="server">
            <div class="marginTop5porcento">EM ANÁLISE CLEARSALE</div>
            <div class="quadroInteriorQtds">
                <asp:Literal runat="server" ID="litanaliseclearsale"></asp:Literal>
            </div>
            <div class="AlinharAoRodapeCentralizar">
                <asp:Button runat="server" ID="btnanaliseclearsale" Text="Detalhar" CssClass="btn" OnClick="btnanaliseclearsale_Click" />
            </div>
        </div>

        <div class="quadroPrincipal" id="quadrorejmundipagg" runat="server">
            <div class="marginTop5porcento">REJEITADOS NO MUNDIPAGG</div>
            <div class="quadroInteriorQtds">
                <asp:Literal runat="server" ID="litrejmundipagg"></asp:Literal>
            </div>
            <div class="AlinharAoRodapeCentralizar">
                <asp:Button runat="server" ID="btnrejmundipagg" Text="Detalhar" CssClass="btn" OnClick="btnrejmundipagg_Click" />
            </div>
        </div>

        <div class="quadroPrincipal" id="quadropagosnaoatualizados" runat="server">
            <div class="marginTop5porcento">PAGOS COM STATUs NÃO ATUALIZADOS</div>
            <div class="quadroInteriorQtds">
                <asp:Literal runat="server" ID="litpagosnaoatualizados"></asp:Literal>
            </div>
            <div class="AlinharAoRodapeCentralizar">
                <asp:Button runat="server" ID="btnpagosnaoatualizados" Text="Detalhar" CssClass="btn" OnClick="btnpagosnaoatualizados_Click" />
            </div>
        </div>

        <div class="quadroPrincipal" id="quadropagosboletorej" runat="server">
            <div class="marginTop5porcento">PAGOS EM BOLETO REJEITADOS</div>
            <div class="quadroInteriorQtds">
                <asp:Literal runat="server" ID="litpagosboletorej"></asp:Literal>
            </div>
            <div class="AlinharAoRodapeCentralizar">
                <asp:Button runat="server" ID="btnpagosboletorej" Text="Detalhar" CssClass="btn" OnClick="btnpagosboletorej_Click" />
            </div>
        </div>

        <div class="quadroPrincipal" id="quadropedidospagamento" runat="server">
            <div class="marginTop5porcento">CONSULTAR EM tbPedidosPagamento</div>
            <div class="quadroInteriorQtds">
                <asp:Literal runat="server" ID="litpedidospagamento"></asp:Literal>
            </div>
            <div class="AlinharAoRodapeCentralizar">
                <asp:Button runat="server" ID="btnpedidospagamento" Text="Detalhar" CssClass="btn" OnClick="btnpedidospagamento_Click" />
            </div>
        </div>

        <div class="quadroPrincipal" id="quadropagoscartaonaofinalizados" runat="server">
            <div class="marginTop5porcento">Pedidos com cartão ainda não pagos</div>
            <div class="quadroInteriorQtds">
                <asp:Literal runat="server" ID="litpagoscartaonaofinalizados"></asp:Literal>
            </div>
            <div class="AlinharAoRodapeCentralizar">
                <asp:Button runat="server" ID="btnpagoscartaonaofinalizados" Text="Detalhar" CssClass="btn" OnClick="btnpagoscartaonaofinalizados_Click" />
            </div>
        </div>

        <div class="quadroPrincipal" id="quadroanalisequebra" runat="server">
            <div class="marginTop5porcento">Análise de Quebra</div>
            <div class="quadroInteriorQtds">
                <asp:Literal runat="server" ID="litanalisequebra"></asp:Literal>
            </div>
            <div class="AlinharAoRodapeCentralizar">
                <asp:Button runat="server" ID="btnanalisequebra" Text="Detalhar" CssClass="btn" OnClick="btnanalisequebra_Click" />
            </div>
        </div>
    </asp:Panel>


    <div align="center" style="min-height: 500px; float: left; clear: left; width: 834px;">
        <asp:Panel ID="panelPai" runat="server">
            <asp:Panel ID="pnnprocclearsale" runat="server">
                <fieldset class="fieldsetatualizarprecos" runat="server" id="filtronprocclearsale">
                    <legend style="font-weight: bold;">Filtro</legend>

                    <table class="tabrotulos">

                        <tr class="rotulos">
                            <td>Pedido:<br />
                                <asp:TextBox runat="server" ID="txtidpedidonprocclearsale"></asp:TextBox>
                            </td>
                            <td>Pagamento:<br />
                                <asp:TextBox runat="server" ID="txtidpagamentonprocclearsale"></asp:TextBox>
                            </td>
                            <td><br />
                                <%--<asp:CheckBox ID="ckbPedidoPagamentosnprocclearsale" runat="server" />--%>
                            </td>
                            <td>&nbsp;<br />
                                <asp:Button runat="server" ID="btnFiltrarnprocclearsale" Text="Filtrar" OnClick="btnFiltrarnprocclearsale_Click" />

                            </td>
                        </tr>
                    </table>
                </fieldset>
                <asp:GridView ID="grdnprocclearsale" CssClass="meugrid" runat="server" Width="834px" DataKeyNames="pedidoId,idPedidoPagamento" AutoGenerateColumns="False"
                    AllowPaging="True" OnPageIndexChanging="grdnprocclearsale_PageIndexChanging" OnRowDataBound="grdnprocclearsale_RowDataBound"
                    OnRowCommand="grdnprocclearsale_RowCommand" PageSize="20">
                    <Columns>

                        <asp:BoundField DataField="pedidoId" ItemStyle-Width="50px" HeaderText="Pedido" />
                        <asp:BoundField DataField="idPedidoPagamento" ItemStyle-Width="50px" HeaderText="Pagamento" />
                        <asp:BoundField DataField="dataEnvio" ItemStyle-Width="120px" HeaderText="Data/Hora de envio" />
                        <asp:BoundField DataField="valor" DataFormatString="{0:c}" ItemStyle-Width="50px" HeaderText="Valor" />

                        <asp:TemplateField HeaderText="Mensagem">

                            <ItemTemplate>
                                <asp:Label ID="Label1" Wrap="True" runat="server" Text='<%# Bind("message") %>'></asp:Label>
                            </ItemTemplate>

                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ChecarClearSale" ItemStyle-Width="120px" ShowHeader="False">
                            <ItemTemplate>
                                <asp:Button ID="btnclearsale" runat="server" Text="Checar ClearSale" CommandName="ChecarClearSale" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Editar Pedido" ItemStyle-Width="80px" ItemStyle-HorizontalAlign="Center" ShowHeader="False">
                            <ItemTemplate>
                                <a id="linkeditarpedido" runat="server" target="blank">
                                    <img src="images/btEditar.jpg" alt="Editar" style="border-style: none;" />
                                </a>
                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                </asp:GridView>

            </asp:Panel>

            <asp:Panel ID="pnanaliseclearsale" runat="server">
                <fieldset class="fieldsetatualizarprecos" runat="server" id="filtroanaliseclearsale">
                    <legend style="font-weight: bold;">Filtro</legend>

                    <table class="tabrotulos">

                        <tr class="rotulos">
                            <td>Pedido:<br />
                                <asp:TextBox runat="server" ID="txtidpedidoanaliseclearsale"></asp:TextBox>
                            </td>
                            <td>Pagamento:<br />
                                <asp:TextBox runat="server" ID="txtidpagamentoanaliseclearsale"></asp:TextBox>
                            </td>
                            <td><br />
                                <%--<asp:CheckBox ID="ckbPedidoPagamentosanaliseclearsale" runat="server" />--%>
                            </td>
                            <td>&nbsp;<br />
                                <asp:Button runat="server" ID="btnFiltraranaliseclearsale" Text="Filtrar" OnClick="btnFiltraranaliseclearsale_Click" />

                            </td>
                        </tr>
                    </table>
                </fieldset>
                <asp:GridView ID="grdanaliseclearsale" CssClass="meugrid" runat="server" Width="834px" DataKeyNames="pedidoId,idPedidoPagamento" AutoGenerateColumns="False"
                    AllowPaging="True" OnPageIndexChanging="grdanaliseclearsale_PageIndexChanging" OnRowDataBound="grdanaliseclearsale_RowDataBound"
                    OnRowCommand="grdanaliseclearsale_RowCommand" PageSize="20">
                    <Columns>

                        <asp:BoundField DataField="pedidoId" ItemStyle-Width="50px" HeaderText="Pedido" />
                        <asp:BoundField DataField="idPedidoPagamento" ItemStyle-Width="50px" HeaderText="Pagamento" />
                        <asp:BoundField DataField="dataEnvio" ItemStyle-Width="120px" HeaderText="Data/Hora de envio" />
                        <asp:BoundField DataField="valor" DataFormatString="{0:c}" ItemStyle-Width="50px" HeaderText="Valor" />

                        <asp:TemplateField HeaderText="Mensagem">

                            <ItemTemplate>
                                <asp:Label ID="Label1" Wrap="True" runat="server" Text='<%# Bind("message") %>'></asp:Label>
                            </ItemTemplate>

                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ChecarClearSale" ItemStyle-Width="120px" ShowHeader="False">
                            <ItemTemplate>
                                <asp:Button ID="btnclearsale" runat="server" Text="Checar ClearSale" CommandName="ChecarClearSale" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Editar Pedido" ItemStyle-Width="80px" ItemStyle-HorizontalAlign="Center" ShowHeader="False">
                            <ItemTemplate>
                                <a id="linkeditarpedido" runat="server" target="blank">
                                    <img src="images/btEditar.jpg" alt="Editar" style="border-style: none;" />
                                </a>
                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                </asp:GridView>
            </asp:Panel>

            <asp:Panel ID="pnrejmundipagg" runat="server">
                <fieldset class="fieldsetatualizarprecos" runat="server" id="filtrorejmundipagg">
                    <legend style="font-weight: bold;">Filtro</legend>

                    <table class="tabrotulos">

                        <tr class="rotulos">
                            <td>Pedido:<br />
                                <asp:TextBox runat="server" ID="txtidpedidorejmundipagg"></asp:TextBox>
                            </td>
                            <td>Pagamento:<br />
                                <asp:TextBox runat="server" ID="txtidpagamentorejmundipagg"></asp:TextBox>
                            </td>
                            <td><br />
                             <%--   <asp:CheckBox ID="ckbPedidoPagamentosrejmundipagg" runat="server" />--%>
                            </td>
                            <td>&nbsp;<br />
                                <asp:Button runat="server" ID="btnFiltrarrejmundipagg" Text="Filtrar" OnClick="btnFiltrarrejmundipagg_Click" />

                            </td>
                        </tr>
                    </table>
                </fieldset>
                <asp:GridView ID="grdrejmundipagg" CssClass="meugrid" runat="server" Width="834px" DataKeyNames="pedidoId,idPedidoPagamento" AutoGenerateColumns="False"
                    AllowPaging="True" OnPageIndexChanging="grdrejmundipagg_PageIndexChanging" OnRowDataBound="grdrejmundipagg_RowDataBound"
                    OnRowCommand="grdrejmundipagg_RowCommand" PageSize="20">
                    <Columns>

                        <asp:BoundField DataField="pedidoId" ItemStyle-Width="50px" HeaderText="Pedido" />
                        <asp:BoundField DataField="idPedidoPagamento" ItemStyle-Width="50px" HeaderText="Pagamento" />
                        <asp:BoundField DataField="dataSolicitacao" ItemStyle-Width="120px" HeaderText="Data/Hora de envio" />
                        <asp:BoundField DataField="valor" DataFormatString="{0:c}" ItemStyle-Width="50px" HeaderText="Valor" />

                        <asp:TemplateField HeaderText="Mensagem">

                            <ItemTemplate>
                                <asp:Label ID="Label1" Wrap="True" runat="server" Text='<%# Bind("responseMessage") %>'></asp:Label>
                            </ItemTemplate>

                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Editar Pedido" ItemStyle-Width="80px" ItemStyle-HorizontalAlign="Center" ShowHeader="False">
                            <ItemTemplate>
                                <a id="linkeditarpedido" runat="server" target="blank">
                                    <img src="images/btEditar.jpg" alt="Editar" style="border-style: none;" />
                                </a>
                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                </asp:GridView>


            </asp:Panel>

            <asp:Panel ID="pnpagosnaoatualizados" runat="server">
                <fieldset class="fieldsetatualizarprecos" runat="server" id="filtropagosnaoatualizados">
                    <legend style="font-weight: bold;">Filtro</legend>

                    <table class="tabrotulos">

                        <tr class="rotulos">
                            <td>Pedido:<br />
                                <asp:TextBox runat="server" ID="txtidpedidopagosnaoatualizados"></asp:TextBox>
                            </td>
                            <td>Pagamento:<br />
                                <asp:TextBox runat="server" ID="txtidpagamentopagosnaoatualizados"></asp:TextBox>
                            </td>
                            <td><br />
                                <%--<asp:CheckBox ID="ckbPedidoPagamentospagosnaoatualizados" runat="server" />--%>
                            </td>
                            <td>&nbsp;<br />
                                <asp:Button runat="server" ID="btnFiltrarpagosnaoatualizados" Text="Filtrar" OnClick="btnFiltrarpagosnaoatualizados_Click" />

                            </td>
                        </tr>
                    </table>
                </fieldset>
                <asp:GridView ID="grdpagosnaoatualizados" CssClass="meugrid" runat="server" Width="834px" DataKeyNames="pedidoId" AutoGenerateColumns="False"
                    AllowPaging="True" OnPageIndexChanging="grdpagosnaoatualizados_PageIndexChanging" OnRowDataBound="grdpagosnaoatualizados_RowDataBound" PageSize="20">
                    <Columns>

                        <asp:BoundField DataField="pedidoId" ItemStyle-Width="50px" HeaderText="Pedido" />
                        <asp:BoundField DataField="dataHoraDoPedido" HeaderText="Data/Hora do Pedido" />
                        <asp:BoundField DataField="valorCobrado" DataFormatString="{0:c}" HeaderText="Valor Cobrado " />
                        <asp:BoundField DataField="totalPago" DataFormatString="{0:c}" HeaderText="Total Pago" />



                        <asp:TemplateField HeaderText="Editar Pedido" ShowHeader="False">
                            <ItemTemplate>
                                <a id="linkeditarpedido" runat="server" target="blank">
                                    <img src="images/btEditar.jpg" alt="Editar" style="border-style: none;" />
                                </a>
                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                </asp:GridView>
            </asp:Panel>

            <asp:Panel ID="pnpagosboletorej" runat="server">
                <fieldset class="fieldsetatualizarprecos" runat="server" visible="true" id="filtropagosboletorej">
                    <legend style="font-weight: bold;">Filtro</legend>

                    <table class="tabrotulos">

                        <tr class="rotulos">
                            <td>Nosso Número:<br />
                                <asp:TextBox runat="server" ID="txtnossonumeropagosboletorej"></asp:TextBox>
                            </td>
                            <td>Data Inicial:<br />
                                <asp:TextBox runat="server" ID="txtdatainicialpagosboletorej"></asp:TextBox>
                            </td>
                            <td>Data Final:<br />
                                <asp:TextBox runat="server" ID="txtdatafinalpagosboletorej"></asp:TextBox>
                            </td>
                            <td>Mensagem:<br />
                                <asp:DropDownList ID="ddlmensagempagosboletorej" runat="server">
                                    <asp:ListItem Value="0">Selecione</asp:ListItem>
                                    <asp:ListItem>Erro no processamento</asp:ListItem>
                                    <asp:ListItem>Status do pedido incorreto</asp:ListItem>
                                    <asp:ListItem>Valor Incorreto</asp:ListItem>
                                    <asp:ListItem>Pagamento parcialmente confirmado</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td>&nbsp;<br />
                                <asp:Button runat="server" ID="btnFiltrarpagosboletorej" Text="Filtrar" OnClick="btnFiltrarpagosboletorej_Click" />

                            </td>
                        </tr>
                    </table>
                </fieldset>
                <asp:GridView ID="grdpagosboletorej" CssClass="meugrid" runat="server" Width="834px" DataKeyNames="pedidoId,idLoteCobrancaRetornoRegistro" AutoGenerateColumns="False" AllowPaging="True" OnPageIndexChanging="grdpagosboletorej_PageIndexChanging" OnRowDataBound="grdpagosboletorej_RowDataBound" PageSize="20" OnRowCommand="grdpagosboletorej_RowCommand">
                    <Columns>

                        <asp:BoundField DataField="pedidoId" ItemStyle-Width="50px" HeaderText="Pedido" />
                        <asp:BoundField DataField="nossoNumero" ItemStyle-Width="80px" HeaderText="Nosso Número" />
                        <asp:BoundField DataField="arquivoRetorno" ItemStyle-Width="120px" HeaderText="Arquivo de Retorno" />
                        <asp:BoundField DataField="mensagem" ItemStyle-Width="50px" HeaderText="Mensagem" />
                        <asp:BoundField DataField="dataProcessamento" ItemStyle-Width="150px" HeaderText="Data/Hora processamento" />
                        <asp:BoundField DataField="valorPago" DataFormatString="{0:c}" ItemStyle-Width="50px" HeaderText="Valor Pago" />

                        <asp:TemplateField HeaderText="Problema Tratado" ItemStyle-Width="120px" ShowHeader="False">
                            <ItemTemplate>
                                <asp:Button ID="btnproblematratado" runat="server" Text="Problema Tratado" CommandName="ProblemaTratado" />
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Editar Pedido" ItemStyle-Width="80px" ItemStyle-HorizontalAlign="Center" ShowHeader="False">
                            <ItemTemplate>
                                <a id="linkeditarpedido" runat="server" target="blank">
                                    <img src="images/btEditar.jpg" alt="Editar" style="border-style: none;" />
                                </a>
                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                </asp:GridView>
            </asp:Panel>

              <asp:Panel ID="pnpedidospagamento" runat="server">
                <fieldset class="fieldsetatualizarprecos" runat="server" id="filtropedidospagamento">
                    <legend style="font-weight: bold;">Filtro</legend>

                    <table class="tabrotulos">

                        <tr class="rotulos">
                            <td>Pedido:<br />
                                <asp:TextBox runat="server" ID="txtidpedidopedidospagamento"></asp:TextBox>
                            </td>
                            <td>Pagamento:<br />
                                <asp:TextBox runat="server" ID="txtidpagamentopedidospagamento"></asp:TextBox>
                            </td>
                            <td><br />
                               <%-- <asp:CheckBox ID="ckbPedidoPagamentospedidospagamento" runat="server" />--%>
                            </td>
                            <td>&nbsp;<br />
                                <asp:Button runat="server" ID="btnFiltrarpedidospagamento" Text="Filtrar" OnClick="btnFiltrarpedidospagamento_Click" />

                            </td>
                        </tr>
                    </table>
                </fieldset>
                <asp:GridView ID="grdpedidospagamento" CssClass="meugrid" runat="server" Width="834px" DataKeyNames="pedidoId,idPedidoPagamento" AutoGenerateColumns="False"
                    AllowPaging="True" OnPageIndexChanging="grdpedidospagamento_PageIndexChanging" OnRowDataBound="grdpedidospagamento_RowDataBound"
                    OnRowCommand="grdpedidospagamento_RowCommand" PageSize="20">
                    <Columns>

                        <asp:BoundField DataField="pedidoId" ItemStyle-Width="50px" HeaderText="Pedido" />
                        <asp:BoundField DataField="idPedidoPagamento" ItemStyle-Width="50px" HeaderText="Pagamento" />
                        <asp:BoundField DataField="dataEnvio" ItemStyle-Width="120px" HeaderText="Data/Hora de envio" />
                        <asp:BoundField DataField="valor" DataFormatString="{0:c}" ItemStyle-Width="50px" HeaderText="Valor" />

                        <asp:TemplateField HeaderText="Mensagem">

                            <ItemTemplate>
                                <asp:Label ID="Label1" Wrap="True" runat="server" Text='<%# Bind("message") %>'></asp:Label>
                            </ItemTemplate>

                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ChecarClearSale" ItemStyle-Width="120px" ShowHeader="False">
                            <ItemTemplate>
                                <asp:Button ID="btnclearsale" runat="server" Text="Checar ClearSale" CommandName="ChecarClearSale" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Editar Pedido" ItemStyle-Width="80px" ItemStyle-HorizontalAlign="Center" ShowHeader="False">
                            <ItemTemplate>
                                <a id="linkeditarpedido" runat="server" target="blank">
                                    <img src="images/btEditar.jpg" alt="Editar" style="border-style: none;" />
                                </a>
                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                </asp:GridView>

            </asp:Panel>

             <asp:Panel ID="pnpagoscartaonaofinalizados" runat="server">
                <fieldset class="fieldsetatualizarprecos" runat="server" id="filtropagoscartaonaofinalizados">
                    <legend style="font-weight: bold;">Filtro</legend>

                    <table class="tabrotulos">

                        <tr class="rotulos">
                            <td>Data:<br />
                                <asp:TextBox runat="server" ID="txtdatapagoscartaonaofinalizados"></asp:TextBox>
                            </td>
                            <td>&nbsp;:<br />
                              
                            </td>
                            <td><br />
                               <%-- <asp:CheckBox ID="ckbPedidoPagamentospagoscartaonaofinalizados" runat="server" />--%>
                            </td>
                            <td>&nbsp;<br />
                                <asp:Button runat="server" ID="btnFiltrarpagoscartaonaofinalizados" Text="Filtrar" OnClick="btnFiltrarpagoscartaonaofinalizados_Click" />

                            </td>
                        </tr>
                    </table>
                </fieldset>
                <asp:GridView ID="grdpagoscartaonaofinalizados" CssClass="meugrid" runat="server" Width="834px" DataKeyNames="pedidoId,idPedidoPagamento" AutoGenerateColumns="False"
                    AllowPaging="True" OnPageIndexChanging="grdpagoscartaonaofinalizados_PageIndexChanging" OnRowDataBound="grdpagoscartaonaofinalizados_RowDataBound"
                    OnRowCommand="grdpagoscartaonaofinalizados_RowCommand" PageSize="20">
                    <Columns>

                        <asp:BoundField DataField="pedidoId" ItemStyle-Width="50px" HeaderText="Pedido" />
                        <asp:BoundField DataField="idPedidoPagamento" ItemStyle-Width="120px" HeaderText="Pagamento" />
                        <asp:BoundField DataField="dataHoraDoPedido" ItemStyle-Width="120px" HeaderText="Data do pedido" />
                        <asp:BoundField DataField="valorCobrado" DataFormatString="{0:c}" ItemStyle-Width="50px" HeaderText="Valor Cobrado no Pedido" />



                        <asp:TemplateField HeaderText="ChecarClearSale" ItemStyle-Width="120px" ShowHeader="False">
                            <ItemTemplate>
                                <asp:Button ID="btnclearsale" runat="server" Text="Checar ClearSale" CommandName="ChecarClearSale" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Editar Pedido" ItemStyle-Width="80px" ItemStyle-HorizontalAlign="Center" ShowHeader="False">
                            <ItemTemplate>
                                <a id="linkeditarpedido" runat="server" target="blank">
                                    <img src="images/btEditar.jpg" alt="Editar" style="border-style: none;" />
                                </a>
                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                </asp:GridView>

            </asp:Panel>

            <asp:Panel ID="pnanalisequebra" runat="server">
                <fieldset class="fieldsetatualizarprecos" runat="server" id="filtroanalisequebra">
                    <legend style="font-weight: bold;">Filtro</legend>

                    <table class="tabrotulos">

                        <tr class="rotulos">
                            <td>Pedido:<br />
                                <asp:TextBox runat="server" ID="txtidpedidoanalisequebra"></asp:TextBox>
                            </td>
                            <td>Pagamento:<br />
                                <asp:TextBox runat="server" ID="txtidpagamentoanalisequebra"></asp:TextBox>
                            </td>
                            <td><br />
                             <%--   <asp:CheckBox ID="ckbPedidoPagamentosanalisequebra" runat="server" />--%>
                            </td>
                            <td>&nbsp;<br />
                                <asp:Button runat="server" ID="btnFiltraranalisequebra" Text="Filtrar" OnClick="btnFiltraranalisequebra_Click" />

                            </td>
                        </tr>
                    </table>
                </fieldset>
                <asp:GridView ID="grdanalisequebra" CssClass="meugrid" runat="server" Width="834px" DataKeyNames="pedidoId,idPedidoPagamento" AutoGenerateColumns="False"
                    AllowPaging="True" OnPageIndexChanging="grdanalisequebra_PageIndexChanging" OnRowDataBound="grdanalisequebra_RowDataBound"
                    OnRowCommand="grdanalisequebra_RowCommand" PageSize="20">
                    <Columns>

                        <asp:BoundField DataField="pedidoId" ItemStyle-Width="50px" HeaderText="Pedido" />
                        <asp:BoundField DataField="idPedidoPagamento" ItemStyle-Width="120px" HeaderText="Pagamento" />
                        <asp:BoundField DataField="dataSolicitacao" ItemStyle-Width="120px" HeaderText="Data" />
                        <asp:BoundField DataField="valor" DataFormatString="{0:c}" ItemStyle-Width="50px" HeaderText="Valor" />
                        <asp:BoundField DataField="processorMessage"  ItemStyle-Width="50px" HeaderText="Mensagem Retorno Mundipagg" />
                        <asp:BoundField DataField="processorReturnCode"  ItemStyle-Width="50px" HeaderText="Cód Retorno Mundipagg" />




                        <asp:TemplateField HeaderText="Editar Pedido" ItemStyle-Width="80px" ItemStyle-HorizontalAlign="Center" ShowHeader="False">
                            <ItemTemplate>
                                <a id="linkeditarpedido" runat="server" target="blank">
                                    <img src="images/btEditar.jpg" alt="Editar" style="border-style: none;" />
                                </a>
                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                </asp:GridView>

            </asp:Panel>
        </asp:Panel>

 
        <br />

    </div>


</asp:Content>

