﻿<%@ Page Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="relacionarProdutosComCategoria.aspx.cs" Inherits="admin_relacionarProdutosComCategoria"   Theme="Glass" MaintainScrollPositionOnPostback=true %>

<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dxwgv" %>


<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" Runat="Server">
    <table cellpadding="0" cellspacing="0" style="width: 920px">
    <tr>
        <td class="tituloPaginas" valign="top">
            <asp:Label ID="lblAcao" runat="server">Relacionar produtos com categorias</asp:Label>
            </td>
    </tr>
    <tr>
        <td>
            
    <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
        <tr>
            <td align="right">
                <table cellpadding="0" cellspacing="0" style="width: 834px">
                    <tr>
                        <td align="right">
                            &nbsp;</td>
                        <td height="38" 
                            style="background-image: url('images/bkExportar.jpg'); padding-bottom: 1px;" 
                            valign="bottom" width="231">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="width: 97px">
                                        &nbsp;</td>
                                    <td style="width: 33px">
                                        <asp:ImageButton ID="btPdf" OnClick="btPdf_Click" runat="server" ImageUrl="~/admin/images/btPdf.jpg" 
                                            style="margin-left: 5px" />
                                    </td>
                                    <td style="width: 31px">
                                        <asp:ImageButton ID="btXsl" OnClick="btXsl_Click" runat="server" ImageUrl="~/admin/images/btXls.jpg" 
                                            style="margin-left: 5px" />
                                    </td>
                                    <td style="width: 36px">
                                        <asp:ImageButton ID="btRtf" OnClick="btRtf_Click" runat="server" ImageUrl="~/admin/images/btRtf.jpg" 
                                            style="margin-left: 5px" />
                                    </td>
                                    <td valign="bottom">
                                        <asp:ImageButton ID="btCsv" OnClick="btCsv_Click" runat="server" ImageUrl="~/admin/images/btCsv.jpg" 
                                            style="margin-left: 5px" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                <table cellpadding="0" cellspacing="0" style="width: 834px">
                    <tr>
                        <td style="width: 220px" valign="top">
         <asp:TreeView ID="treeCategorias" runat="server" 
             ShowLines="True" ontreenodepopulate="treeCategorias_TreeNodePopulate" Font-Names="Tahoma" 
                                Font-Size="12px" ForeColor="Black" ExpandDepth="30" 
                                onprerender="treeCategorias_PreRender" ShowCheckBoxes="All" 
                                onselectednodechanged="treeCategorias_SelectedNodeChanged">
             <HoverNodeStyle Font-Underline="True" />
             <SelectedNodeStyle BackColor="#D2ECEC" />
             <Nodes>
                 <asp:TreeNode Text="Categorias" Value="-1" Checked="True" Expanded="True" 
                     Selected="True"></asp:TreeNode>
             </Nodes>

</asp:TreeView>



                        </td>
                        <td valign="top" align="right">
                    <dxwgv:ASPxGridView ID="grd" runat="server" AutoGenerateColumns="False" 
                    DataSourceID="sqlProdutoCategorias" KeyFieldName="produtoId" 
                    Cursor="auto" oncustomcallback="grd_CustomCallback" 
                        ClientInstanceName="grd" Width="614px" EnableCallBacks="False">
                        <SettingsBehavior AutoFilterRowInputDelay="10200" ConfirmDelete="True" 
                            AutoExpandAllGroups="True" ColumnResizeMode="Control" />
                        <SettingsText ConfirmDelete="Tem certeza que deseja excluir?" 
                        emptydatarow="Nenhum registro encontrado." 
                        GroupPanel="Arraste uma coluna aqui para agrupar." />
                        <SettingsPager Position="TopAndBottom" PageSize="5000" 
                        ShowDisabledButtons="False" AlwaysShowPager="True" Visible="False">
                            <Summary AllPagesText="Páginas: {0} - {1} ({2} items)" 
                            Text="Página {0} de {1} ({2} registros encontrados)" />
                        </SettingsPager>
                        <settings showfilterrow="True" ShowGroupedColumns="True" />
                        <SettingsEditing EditFormColumnCount="4" 
                        PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1" 
                        PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter" 
                        PopupEditFormWidth="700px" Mode="Inline" />
                        <Columns>
                            <dxwgv:GridViewDataTextColumn Width="100px" Caption="Id do produto" FieldName="produtoId" VisibleIndex="0">
                                <DataItemTemplate>
                                    <asp:TextBox ID="txtProdutoId" runat="server" BorderStyle="None" 
                                        CssClass="rotulos" Text='<%# Bind("produtoId") %>' Width="100px"></asp:TextBox>
                                </DataItemTemplate>
                            </dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataTextColumn Caption="ID da Empresa" 
                                FieldName="produtoIdDaEmpresa" Name="idDaEmpresa" VisibleIndex="1" 
                                Width="100px">
                            </dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataTextColumn Caption="Nome" FieldName="produtoNome" 
                                VisibleIndex="2" Width="150px">								
                                <Settings AutoFilterCondition="Contains" />
                                <DataItemTemplate>
                                    <asp:TextBox ID="txtIdDaEmpresa" runat="server" BorderStyle="None" CssClass="campos" 
                                        Text='<%# Bind("produtoNome") %>' Width="100%"></asp:TextBox>
                                </DataItemTemplate>
                            </dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataTextColumn Caption="Ativo" FieldName="produtoAtivo" 
                                VisibleIndex="3" Width="50px" UnboundType="Boolean">
                                <DataItemTemplate>
                                    <asp:TextBox ID="txtNome" runat="server" BorderStyle="None" CssClass="campos" 
                                        Text='<%# Bind("produtoNome") %>' Width="100%"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rqvNome" runat="server" 
                                        ControlToValidate="txtNome" Display="Dynamic" ErrorMessage="Preencha o nome." 
                                        Font-Size="10px" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                    <dxe:ASPxCheckBox ID="ckbAtivo" runat="server" Enabled="False" 
                                        Value='<%# Bind("produtoAtivo") %>' ValueChecked="True" 
                                        ValueType="System.String" ValueUnchecked="False">
                                    </dxe:ASPxCheckBox>
                                
                                </DataItemTemplate>
                                <DataItemTemplate>
                                    <dxe:ASPxCheckBox ID="ckbAtivo0" runat="server" Enabled="False" 
                                        Value='<%# Bind("produtoAtivo") %>' ValueChecked="True" 
                                        ValueType="System.String" ValueUnchecked="False">
                                    </dxe:ASPxCheckBox>
                                </DataItemTemplate>
                                <CellStyle HorizontalAlign="Center">
                                </CellStyle>
                            </dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataTextColumn Caption="Fornecedor" FieldName="fornecedorNome" 
                                VisibleIndex="4" Width="160px">
                            </dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataTextColumn Caption="Excluir" 
                                VisibleIndex="5" Width="50px" Name="excluir">
                                <DataItemTemplate>
                                    <dxe:ASPxCheckBox ID="ckbExcluir" runat="server" ValueChecked="True" 
                                        ValueType="System.String" ValueUnchecked="False">
                                    </dxe:ASPxCheckBox>
                                </DataItemTemplate>
                                <CellStyle HorizontalAlign="Center">
                                </CellStyle>
                            </dxwgv:GridViewDataTextColumn>
                        </Columns>
                    </dxwgv:ASPxGridView>
                <asp:ObjectDataSource ID="sqlProdutoCategorias" runat="server" 
                    SelectMethod="produtoAdminSeleciona" TypeName="rnProdutos" 
                    DeleteMethod="produtoExclui">
                    <DeleteParameters>
                        <asp:Parameter Name="produtoId" Type="String" />
                    </DeleteParameters>
                    <SelectParameters>
                        <asp:ControlParameter ControlID="treeCategorias" Name="categoriaId" 
                            PropertyName="SelectedValue" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            <img src="images/btSalvar.jpg" onclick="grd.PerformCallback(this.value);" 
                                style="margin-top: 16px;"/></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                    <dxwgv:ASPxGridView ID="grd1" runat="server" AutoGenerateColumns="False" 
                    DataSourceID="sqlProdutos" KeyFieldName="produtoId" 
                    Cursor="auto" oncustomcallback="grd1_CustomCallback" 
                        ClientInstanceName="grd1" Width="834px" EnableCallBacks="False">
                        <SettingsBehavior AutoFilterRowInputDelay="10200" ConfirmDelete="True" 
                            AutoExpandAllGroups="True" ColumnResizeMode="Control" />
                        <SettingsText ConfirmDelete="Tem certeza que deseja excluir?" 
                        emptydatarow="Nenhum registro encontrado." 
                        GroupPanel="Arraste uma coluna aqui para agrupar." />
                        <SettingsPager Position="TopAndBottom" PageSize="100" 
                        ShowDisabledButtons="False" AlwaysShowPager="True">
                            <Summary AllPagesText="Páginas: {0} - {1} ({2} items)" 
                            Text="Página {0} de {1} ({2} registros encontrados)" />
                        </SettingsPager>
                        <settings showfilterrow="True" ShowGroupedColumns="True" />
                        <SettingsEditing EditFormColumnCount="4" 
                        PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1" 
                        PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter" 
                        PopupEditFormWidth="700px" Mode="Inline" />
                        <Columns>
<dxwgv:GridViewDataTextColumn Width="100px" Caption="Id do produto" VisibleIndex="0">
    <DataItemTemplate>
        <asp:TextBox ID="txtProdutoId" runat="server" BorderStyle="None" 
            CssClass="rotulos" Text='<%# Bind("produtoId") %>' Width="100px"></asp:TextBox>
        <asp:TextBox ID="txtProdutoPaiId" runat="server" BorderStyle="None" 
            CssClass="rotulos" Text='<%# Bind("produtoPaiId") %>' Visible="False" 
            Width="100px"></asp:TextBox>
    </DataItemTemplate>
</dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataTextColumn Caption="ID da Empresa" 
                                FieldName="produtoIdDaEmpresa" Name="idDaEmpresa" VisibleIndex="1" 
                                Width="100px">
                            </dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataTextColumn Caption="Nome" FieldName="produtoNome" 
                                VisibleIndex="2" Width="210px">				
                                <Settings AutoFilterCondition="Contains" />
                                <DataItemTemplate>
                                    <asp:TextBox ID="txtIdDaEmpresa0" runat="server" BorderStyle="None" CssClass="campos" 
                                        Text='<%# Bind("produtoNome") %>' Width="100%"></asp:TextBox>
                                </DataItemTemplate>
                            </dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataTextColumn Caption="Ativo" FieldName="produtoAtivo" 
                                VisibleIndex="3" Width="30px" UnboundType="Boolean">
                                <DataItemTemplate>
                                    <asp:TextBox ID="txtNome0" runat="server" BorderStyle="None" CssClass="campos" 
                                        Text='<%# Bind("produtoNome") %>' Width="100%"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rqvNome0" runat="server" 
                                        ControlToValidate="txtNome" Display="Dynamic" ErrorMessage="Preencha o nome." 
                                        Font-Size="10px" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                    <dxe:ASPxCheckBox ID="ckbAtivo1" runat="server" Enabled="False" 
                                        Value='<%# Bind("produtoAtivo") %>' ValueChecked="True" 
                                        ValueType="System.String" ValueUnchecked="False">
                                    </dxe:ASPxCheckBox>
                                
                                </DataItemTemplate>
                                <DataItemTemplate>
                                    <dxe:ASPxCheckBox ID="ckbAtivo2" runat="server" Enabled="False" 
                                        Value='<%# Bind("produtoAtivo") %>' ValueChecked="True" 
                                        ValueType="System.String" ValueUnchecked="False">
                                    </dxe:ASPxCheckBox>
                                </DataItemTemplate>
                                <CellStyle HorizontalAlign="Center">
                                </CellStyle>
                            </dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataTextColumn Caption="Fornecedor" FieldName="fornecedorNome" 
                                VisibleIndex="4" Width="160px">
                            </dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataTextColumn Caption="Selecionar" 
                                VisibleIndex="5" Width="50px" Name="selecionar">
                                <DataItemTemplate>
                                    <dxe:ASPxCheckBox ID="ckbSelecionar" runat="server" ValueChecked="True" 
                                        ValueType="System.String" ValueUnchecked="False">
                                    </dxe:ASPxCheckBox>
                                </DataItemTemplate>
                                <CellStyle HorizontalAlign="Center">
                                </CellStyle>
                            </dxwgv:GridViewDataTextColumn>
                        </Columns>
                    </dxwgv:ASPxGridView>
            </td>
        </tr>
        <tr>
            <td align="right">
                &nbsp;</td>
        </tr>
        <tr>
            <td align="right">
            <img src="images/btSalvar.jpg" onclick="grd1.PerformCallback(this.value);"/></td>
        </tr>
        <tr>
            <td>
                <dxwgv:ASPxGridViewExporter ID="grdEx" runat="server" FileName="fornecedores" 
                    GridViewID="grd" Landscape="True" PreserveGroupRowStates="True">
                </dxwgv:ASPxGridViewExporter>-
                <asp:ObjectDataSource ID="sqlProdutos" runat="server" 
                    SelectMethod="produtoAdminSelecionaSemCategoria" TypeName="rnProdutos" 
                    DeleteMethod="produtoExclui">
                    <DeleteParameters>
                        <asp:Parameter Name="produtoId" Type="Int32" />
                    </DeleteParameters>
                    <SelectParameters>
                        <asp:ControlParameter ControlID="treeCategorias" Name="categoriaId" 
                            PropertyName="SelectedValue" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </td>
        </tr>
    </table>
        </td>
    </tr>
</table>
    </asp:Content>
