﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.IO;
using System.Threading;
using SpreadsheetLight;

public partial class admin_atualizarPrazosTNT : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public void MainFlow()
    {
        System.Threading.Tasks.Task taskWork = System.Threading.Tasks.Task.Factory.StartNew(DoWork);
        taskWork.Start();
        taskWork.Wait();
    }

    private void DoWork()
    {
        string caminho = MapPath("~/");

        if (File.Exists(caminho + "planilhaPrazosTransportadora" + Path.GetExtension(fupPlanilha.FileName).ToLower()))
            File.Delete(caminho + "planilhaPrazosTransportadora" + Path.GetExtension(fupPlanilha.FileName).ToLower());
        fupPlanilha.SaveAs(caminho + "planilhaPrazosTransportadora" + Path.GetExtension(fupPlanilha.FileName).ToLower());


        string caminhoPlanilha = caminho + "planilhaPrazosTransportadora" + Path.GetExtension(fupPlanilha.FileName).ToLower();
        int totalAtualizado = 0, totalErros = 0;
        string cidadeFalhaAtualizacao = "";
        List<string> listCidadeFalhaAtualizacao = new List<string>();
        string cidadeSemCadastro = "";
        List<string> listCidadeSemCadastro = new List<string>();
        List<string> listCidadeComCadastro = new List<string>();
        int tipoDeEntrega = Convert.ToInt32(ddlTransportadora.SelectedValue);
        string inicio = "", termino = "";
        string transportadora = ddlTransportadora.SelectedItem.Text;

        try
        {
            using (SLDocument sl = new SLDocument(caminhoPlanilha, "Plan1"))
            {
                inicio = DateTime.Now.ToString();

                SLWorksheetStatistics stats = sl.GetWorksheetStatistics();

                if (sl.GetCellValueAsString(1, 1) != "MUNICÍPIOS/ DISTRITOS" || sl.GetCellValueAsString(1, 2) != "UF" || sl.GetCellValueAsString(1, 3) != "PRAZO TOTAL")
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('É necessário que a planilha tenha a estrutura de colunas: MUNICÍPIOS/ DISTRITOS, UF, PRAZO TOTAL!');", true);

                    return;
                }

                for (int row = stats.StartRowIndex + 1; row <= stats.EndRowIndex; ++row)
                //for (int row = stats.StartRowIndex + 1; row <= 1002; ++row)
                {
                    string cidadeComUF = "";

                    try
                    {
                        cidadeComUF = sl.GetCellValueAsString(row, 1) + " - " + sl.GetCellValueAsString(row, 2);
                        int prazoTotal = sl.GetCellValueAsInt32(row, 3);

                        using (var data = new dbCommerceDataContext())
                        {
                            //var dados = (from c in data.tbFaixaDeCeps
                            //             where c.tipodeEntregaId == 10 && cidadeComUF.Contains(c.faixaDeCepDescricao)
                            //             select c).ToList();

                            var dados =
                                data.tbFaixaDeCeps.Where(
                                    x => x.tipodeEntregaId == tipoDeEntrega && x.faixaDeCepDescricao.Contains(cidadeComUF))
                                    .ToList();

                            if (!dados.Any())
                            {
                                cidadeSemCadastro += cidadeComUF + "</br>";
                                listCidadeSemCadastro.Add(cidadeComUF);
                                continue;
                            }

                            foreach (var faixaDeCep in dados)
                            {
                                if (faixaDeCep.prazo != prazoTotal)
                                {
                                    listCidadeComCadastro.Add(cidadeComUF + " prazo antigo: " + faixaDeCep.prazo + " prazo novo: " + prazoTotal);
                                    faixaDeCep.prazo = prazoTotal;
                                    data.SubmitChanges();
                                }

                            }

                            totalAtualizado++;
                        }
                    }
                    catch (Exception)
                    {
                        totalErros++;
                        cidadeFalhaAtualizacao += cidadeComUF + "</br>";
                        listCidadeFalhaAtualizacao.Add(cidadeComUF);
                    }
                }

                termino = DateTime.Now.ToString();

                #region Log de atualização dos prazos

                try
                {
                    //Declaração do método StreamWriter passando o caminho e nome do arquivo que deve ser salvo
                    StreamWriter writer = new StreamWriter(caminho + @"logUpdatePrazos/AtualizacaoPrazos_" + transportadora + "_" + DateTime.Now.ToString("ddMMyyyyHHmmss") + ".txt");
                    //Escrevendo o Arquivo e pulando uma linha
                    writer.WriteLine("Foram atualizados: " + totalAtualizado + "/" + stats.EndRowIndex + " prazos da transportadora " + transportadora + " Inicio " + inicio + " Termino " + termino);
                    writer.WriteLine();
                    //Escrevendo o Arquivo sem pular linha
                    writer.Write("Falhas na atualização: " + totalErros);
                    writer.WriteLine();

                    foreach (var item in listCidadeFalhaAtualizacao)
                    {
                        writer.WriteLine(item);
                    }

                    writer.WriteLine();

                    writer.WriteLine("Cidades sem Cadastro no BD:");
                    writer.WriteLine();

                    foreach (var item in listCidadeSemCadastro)
                    {
                        writer.WriteLine(item);
                    }

                    writer.WriteLine();
                    writer.WriteLine("Alteracoes realizadas:");

                    foreach (var item in listCidadeComCadastro)
                    {
                        writer.WriteLine(item);
                    }

                    //Fechando o arquivo
                    writer.Close();
                    //Limpando a referencia dele da memória
                    writer.Dispose();
                }
                catch (Exception ex)
                {

                }

                #endregion Log de atualização dos prazos

                //rnEmails.EnviaEmail("atendimento@graodegente.com.br", "renato@bark.com.br", "", "", "", "Foram atualizados: " + totalAtualizado + "/" + stats.EndRowIndex + " prazos da transportadora " + ddlTransportadora.SelectedItem.Text + " Inicio " + inicio + " Termino " + termino + "</br></br>" +
                //                                                                                        "Falhas na atualização: " + totalErros + "</br>" + cidadeFalhaAtualizacao + "</br>" +
                //                                                                                        "Cidades sem Cadastro no BD: </br>" + cidadeSemCadastro, "Atualização de prazos concluida");
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('Falha no upload do arquivo verifique se a aba da planilha esta nomeada como, Plan1, e a extensão do arquivo xlxs!');", true);
        }
    }

    private void StartLongProcess()
    {
        //Replace the following line of code with your long process
        //Thread.Sleep(Convert.ToInt32(this.txtProcessLength.Text) * 1000);

        //Set session variable equal to true when process completes
        //Session[PROCESS_NAME] = true;
    }

    private void SubmitButton_Click(object sender, System.EventArgs e)
    {
        //Initialize Session Variable
        //Session[PROCESS_NAME] = false;

        //Create and initialize new thread with the 
        //address of the StartLongProcess function
        Thread thread = new Thread(new ThreadStart(StartLongProcess));

        //Start thread
        thread.Start();

        //Pass redirect page and session var name 
        //to the process wait (interum) page
        //Response.Redirect("ProcessingMessage.aspx?" +
        //  "redirectPage=SubmitLongProcess.aspx&ProcessSessionName="
        //  + PROCESS_NAME);
    }

    protected void btnImportarPlanilhaPrazos_OnClick(object sender, EventArgs e)
    {
        if (!fupPlanilha.HasFile)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('Informe um arquivo com extensão xlsx e no padrão solicitado!');", true);
            return;
        }

        string ext = Path.GetExtension(fupPlanilha.FileName);

        if (ext.ToLower() != ".xlsx")
        {
            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('Apenas arquivos xlsx são permitidos!');", true);
            return;
        }
        System.Threading.Tasks.Task.Factory.StartNew(DoWork);
        //System.Threading.Tasks.Task taskWork = System.Threading.Tasks.Task.Factory.StartNew(DoWork);
        //taskWork.Start();
        //taskWork.Wait();
        //taskWork.RunSynchronously();
        ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('Após conclusão das atualizações será criado arquivo de log contendo detalhes da atualização o arquivo terá o nome AtualizacaoPrazos_[nome transportadora]_[ddMMyyyyHHmmss]!');", true);

    }

    private void EscreveArquivo(List<string> listCidadeSemCadastro, List<string> listCidadeFalhaAtualizacao, int totalAtualizado, int totalErros, int totalLinhas, string transportadora, string inicio, string termino)
    {


    }
}