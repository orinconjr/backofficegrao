﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="empresanfe.aspx.cs" Theme="Glass" Inherits="admin_empresanfe" %>

<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" runat="Server">
    <style>
        .tamanhoTxt {
            width: 90px;
        }
    </style>
    <table style="width: 920px">
        <tr>
            <td class="tituloPaginas">
                <asp:Label ID="lblAcao" runat="server">Empresas</asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <div style="width: 500px; margin: 0 auto; text-align: center; font-family: Tahoma; font-size: 21px; padding: 20px 0 0px 0;">
                    Mês:<asp:DropDownList runat="server" ID="ddlMes" Style="font-family: Tahoma; font-size: 21px;" OnSelectedIndexChanged="ddlMes_OnSelectedIndexChanged" AutoPostBack="True" />
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <table style="width: 834px; margin: 0 auto;">
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <div>
                                <dxwgv:ASPxGridView ID="grd" runat="server" AutoGenerateColumns="False" OnStartRowEditing="grd_OnStartRowEditing"
                                    DataSourceID="sqlEmpresa" KeyFieldName="idEmpresa" Width="834px" OnCustomUnboundColumnData="grd_OnCustomUnboundColumnData"
                                    Cursor="auto">
                                    <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                                    <SettingsText ConfirmDelete="Tem certeza que deseja excluir?"
                                        EmptyDataRow="Nenhum registro encontrado." />
                                    <SettingsPager Position="TopAndBottom" PageSize="50"
                                        ShowDisabledButtons="False" AlwaysShowPager="True">
                                        <Summary AllPagesText="Páginas: {0} - {1} ({2} items)"
                                            Text="Página {0} de {1} ({2} registros encontrados)"></Summary>
                                    </SettingsPager>
                                    <Settings ShowFilterRow="True" ShowGroupButtons="False" />
                                    <SettingsEditing EditFormColumnCount="4"
                                        PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1"
                                        PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter"
                                        PopupEditFormWidth="700px" />
                                    <Columns>
                                        <dxwgv:GridViewDataTextColumn Caption="Id"
                                            FieldName="idEmpresa" ReadOnly="True" VisibleIndex="0" Width="30px">
                                            <Settings AutoFilterCondition="Contains" />
                                            <EditFormSettings Visible="False" CaptionLocation="Top" ColumnSpan="2" />
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn Caption="Nome" FieldName="nome" Width="250"
                                            VisibleIndex="1">
                                            <PropertiesTextEdit>
                                                <ValidationSettings ErrorText="" SetFocusOnError="True">
                                                    <RequiredField ErrorText="Preencha o campo nome." IsRequired="True" />
                                                </ValidationSettings>
                                            </PropertiesTextEdit>
                                            <Settings AutoFilterCondition="Contains" />
                                            <EditFormSettings CaptionLocation="Top" ColumnSpan="2" Visible="True"
                                                VisibleIndex="1" />
                                            <EditItemTemplate>
                                                <dxe:ASPxTextBox runat="server" Text='<%# Eval("nome")%>' ReadOnly="True"></dxe:ASPxTextBox>
                                            </EditItemTemplate>
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn Caption="Nome Emitente" FieldName="nomeEmitente" Visible="False"
                                            VisibleIndex="3" Width="90px">
                                            <Settings AutoFilterCondition="Contains" />
                                            <EditFormSettings CaptionLocation="Top" Visible="True" VisibleIndex="4" />
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn Caption="CNPJ" FieldName="cnpj"
                                            Visible="True" VisibleIndex="3" UnboundType="Integer">
                                            <PropertiesTextEdit>
                                                <ValidationSettings SetFocusOnError="True">
                                                    <RegularExpression ErrorText="Preencha corretamente o CNPJ ou CPF."
                                                        ValidationExpression="(^(\d{2}.\d{3}.\d{3}/\d{4}-\d{2})|(\d{14})$)|(^(\d{3}.\d{3}.\d{3}-\d{2})|(\d{11})$)" />
                                                </ValidationSettings>
                                            </PropertiesTextEdit>
                                            <EditFormSettings CaptionLocation="Top" Visible="True"
                                                VisibleIndex="2" />
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn Caption="IE" FieldName="ieEmitente"
                                            Visible="True" VisibleIndex="3">
                                            <EditFormSettings CaptionLocation="Top" Visible="True"
                                                VisibleIndex="3" />
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataCheckColumn Caption="Simples" FieldName="simples" ReadOnly="True" VisibleIndex="4">
                                            <EditFormSettings CaptionLocation="Top" Visible="False"
                                                VisibleIndex="5" />
                                        </dxwgv:GridViewDataCheckColumn>
                                        <dxwgv:GridViewDataCheckColumn Caption="Ativo" FieldName="ativo" ReadOnly="True" VisibleIndex="5">
                                            <EditFormSettings CaptionLocation="Top" Visible="False"
                                                VisibleIndex="6" />
                                        </dxwgv:GridViewDataCheckColumn>
                                        <dxwgv:GridViewDataTextColumn Caption="Logradouro" FieldName="logradouroEmitente"
                                            Visible="False" VisibleIndex="4">
                                            <EditFormSettings CaptionLocation="Top" Visible="True"
                                                VisibleIndex="7" />
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn Caption="Numero" FieldName="numeroEmitente"
                                            Visible="False" VisibleIndex="5">
                                            <EditFormSettings CaptionLocation="Top" Visible="True"
                                                VisibleIndex="8" />
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn Caption="Bairro"
                                            FieldName="bairroEmitente" Visible="False" VisibleIndex="6">
                                            <EditFormSettings CaptionLocation="Top" Visible="True"
                                                VisibleIndex="10" />
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn Caption="Municipio" FieldName="municipioEmitente"
                                            Visible="False" VisibleIndex="8">
                                            <EditFormSettings CaptionLocation="Top" Visible="True"
                                                VisibleIndex="11" />
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn Caption="UF"
                                            FieldName="ufEmitente" Visible="False" VisibleIndex="4">
                                            <EditFormSettings CaptionLocation="Top" Visible="True"
                                                VisibleIndex="12" />
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn Caption="Cep" FieldName="cepEmitente"
                                            Visible="False" VisibleIndex="4">
                                            <PropertiesTextEdit MaxLength="8">
                                                <ClientSideEvents KeyPress="function(s, e) {}" />
                                                <ValidationSettings SetFocusOnError="True">
                                                    <RegularExpression ErrorText="Preencha corretamente o campo CEP, EX:(00000000)."
                                                        ValidationExpression="^\d{8}?$" />
                                                </ValidationSettings>
                                            </PropertiesTextEdit>
                                            <EditFormSettings CaptionLocation="Top" Visible="True" VisibleIndex="12" />
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn Caption="Codigo Municipio"
                                            FieldName="codigoMunicipioEmitente" Visible="False" VisibleIndex="7">
                                            <EditFormSettings CaptionLocation="Top" Visible="True"
                                                VisibleIndex="13" />
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn Caption="Telefone" FieldName="foneEmitente"
                                            Visible="False" VisibleIndex="4">
                                            <EditFormSettings CaptionLocation="Top" Visible="True"
                                                VisibleIndex="14" />
                                        </dxwgv:GridViewDataTextColumn>


                                        <%--<dxwgv:GridViewDataComboBoxColumn Caption="Estado" FieldName="fornecedorEstado"
                                        Visible="False" VisibleIndex="4">
                                        <PropertiesComboBox DataSourceID="sqlEstados" DropDownHeight="21px"
                                            TextField="EstadoNome" ValueField="estadoId" ValueType="System.String">
                                        </PropertiesComboBox>
                                        <EditFormSettings CaptionLocation="Top" Visible="True" VisibleIndex="12" />
                                    </dxwgv:GridViewDataComboBoxColumn>--%>

                                        <dxwgv:GridViewDataTextColumn Caption="Codigo UF" FieldName="codigoUfEmitente"
                                            Visible="False" VisibleIndex="4">
                                            <EditFormSettings CaptionLocation="Top" Visible="True"
                                                VisibleIndex="15" />
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn Caption="Limite" FieldName="limite" UnboundType="Decimal"
                                            Visible="False" VisibleIndex="4">
                                            <EditFormSettings CaptionLocation="Top" Visible="True"
                                                VisibleIndex="16" />
                                        </dxwgv:GridViewDataTextColumn>

                                        <dxwgv:GridViewDataTextColumn Caption="CC Jadlog" FieldName="contaCorrenteJadlog"
                                            Visible="False" VisibleIndex="4">
                                            <EditFormSettings CaptionLocation="Top" Visible="True"
                                                VisibleIndex="17" />
                                        </dxwgv:GridViewDataTextColumn>

                                        <dxwgv:GridViewDataTextColumn Caption="Regime Simples" FieldName="simples" Name="simples" Visible="False" VisibleIndex="4">
                                            <EditFormSettings CaptionLocation="Top" Visible="True"
                                                VisibleIndex="29" />
                                            <EditItemTemplate>
                                                <dxe:ASPxCheckBox runat="server" ID="chkSimples" Checked='<%# Convert.ToBoolean(Eval("simples")) %>' AutoPostBack="false">
                                                </dxe:ASPxCheckBox>
                                            </EditItemTemplate>
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn Caption="Ativo" FieldName="ativo" Visible="False" Name="ativo">
                                            <EditFormSettings CaptionLocation="Top" Visible="True"
                                                VisibleIndex="30" />
                                            <EditItemTemplate>
                                                <dxe:ASPxCheckBox ID="chkAtivo" runat="server" Checked='<%# Convert.ToBoolean(Eval("ativo"))%>' />
                                            </EditItemTemplate>
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn Caption="Emissões Mês" FieldName="emissoesMes" Name="emissoesMes" UnboundType="String" VisibleIndex="10" CellStyle-HorizontalAlign="Center">
                                            <EditFormSettings CaptionLocation="Top" Visible="True"
                                                VisibleIndex="31" />
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn Caption="Total R$" FieldName="totalEmitido" Name="totalEmitido" UnboundType="String" VisibleIndex="10" CellStyle-HorizontalAlign="Center">
                                            <EditFormSettings CaptionLocation="Top" Visible="True"
                                                VisibleIndex="32" />
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn Caption="Limite R$" FieldName="limite" Name="limite" VisibleIndex="10" CellStyle-HorizontalAlign="Center" PropertiesTextEdit-DisplayFormatString="N">
                                            <EditFormSettings CaptionLocation="Top" Visible="True"
                                                VisibleIndex="32" />
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn Caption="IE - UF" Visible="False" Name="ieUF">
                                            <EditFormSettings CaptionLocation="Top" Visible="True" ColumnSpan="4"
                                                VisibleIndex="31" />
                                            <EditItemTemplate>
                                                <br />
                                                <div style="padding-left: 35px;">
                                                    <div style="float: left; padding-right: 5px;">
                                                        ES
                                                <dxe:ASPxTextBox runat="server" Text='<%# Eval("ieES")%>' CssClass="tamanhoTxt"></dxe:ASPxTextBox>
                                                    </div>
                                                    <div style="float: left; padding-right: 5px;">
                                                        MA
                                                <dxe:ASPxTextBox runat="server" Text='<%# Eval("ieMA")%>' CssClass="tamanhoTxt"></dxe:ASPxTextBox>
                                                    </div>
                                                    <div style="float: left; padding-right: 5px;">
                                                        PR
                                                <dxe:ASPxTextBox runat="server" Text='<%# Eval("iePR")%>' CssClass="tamanhoTxt"></dxe:ASPxTextBox>
                                                    </div>
                                                    <div style="float: left; padding-right: 5px;">
                                                        DF
                                                <dxe:ASPxTextBox runat="server" Text='<%# Eval("ieDF")%>' CssClass="tamanhoTxt"></dxe:ASPxTextBox>
                                                    </div>
                                                    <div style="float: left; padding-right: 5px;">
                                                        SE
                                                <dxe:ASPxTextBox runat="server" Text='<%# Eval("ieSE")%>' CssClass="tamanhoTxt"></dxe:ASPxTextBox>
                                                    </div>
                                                    <div style="float: left; padding-right: 5px;">
                                                        TO
                                                <dxe:ASPxTextBox runat="server" Text='<%# Eval("ieTO")%>' CssClass="tamanhoTxt"></dxe:ASPxTextBox>
                                                    </div>
                                                    <div style="float: left; padding-right: 5px;">
                                                        RJ
                                                <dxe:ASPxTextBox runat="server" Text='<%# Eval("ieRJ")%>' CssClass="tamanhoTxt"></dxe:ASPxTextBox>
                                                    </div>
                                                    <div style="float: left; padding-right: 5px;">
                                                        PB
                                                <dxe:ASPxTextBox runat="server" Text='<%# Eval("iePB")%>' CssClass="tamanhoTxt"></dxe:ASPxTextBox>
                                                    </div>
                                                    <div style="float: left; padding-right: 5px;">
                                                        MT
                                                <dxe:ASPxTextBox runat="server" Text='<%# Eval("ieMT")%>' CssClass="tamanhoTxt"></dxe:ASPxTextBox>
                                                    </div>
                                                    <div style="float: left; padding-right: 5px;">
                                                        RS
                                                <dxe:ASPxTextBox runat="server" Text='<%# Eval("ieRS")%>' CssClass="tamanhoTxt"></dxe:ASPxTextBox>
                                                    </div>
                                                    <div style="float: left; padding-right: 5px;">
                                                        AP
                                                <dxe:ASPxTextBox runat="server" Text='<%# Eval("ieAP")%>' CssClass="tamanhoTxt"></dxe:ASPxTextBox>
                                                    </div>
                                                    <div style="float: left; padding-right: 5px;">
                                                        SC
                                                <dxe:ASPxTextBox runat="server" Text='<%# Eval("ieSC")%>' CssClass="tamanhoTxt"></dxe:ASPxTextBox>
                                                    </div>
                                                    <div style="float: left; padding-right: 5px;">
                                                        CE
                                                <dxe:ASPxTextBox runat="server" Text='<%# Eval("ieCE")%>' CssClass="tamanhoTxt"></dxe:ASPxTextBox>
                                                    </div>
                                                    <div style="float: left; padding-right: 5px;">
                                                        BA
                                                <dxe:ASPxTextBox runat="server" Text='<%# Eval("ieBA")%>' CssClass="tamanhoTxt"></dxe:ASPxTextBox>
                                                    </div>
                                                    <div style="float: left; padding-right: 5px;">
                                                        MG
                                                <dxe:ASPxTextBox runat="server" Text='<%# Eval("ieMG")%>' CssClass="tamanhoTxt"></dxe:ASPxTextBox>
                                                    </div>
                                                    <div style="float: left; padding-right: 5px;">
                                                        MS
                                                <dxe:ASPxTextBox runat="server" Text='<%# Eval("ieMS")%>' CssClass="tamanhoTxt"></dxe:ASPxTextBox>
                                                    </div>
                                                    <div style="float: left; padding-right: 5px;">
                                                        PE
                                                <dxe:ASPxTextBox runat="server" Text='<%# Eval("iePE")%>' CssClass="tamanhoTxt"></dxe:ASPxTextBox>
                                                    </div>
                                                    <div style="float: left; padding-right: 5px;">
                                                        AM
                                                <dxe:ASPxTextBox runat="server" Text='<%# Eval("ieAM")%>' CssClass="tamanhoTxt"></dxe:ASPxTextBox>
                                                    </div>
                                                    <div style="float: left; padding-right: 5px;">
                                                        RN
                                                <dxe:ASPxTextBox runat="server" Text='<%# Eval("ieRN")%>' CssClass="tamanhoTxt"></dxe:ASPxTextBox>
                                                    </div>
                                                    <div style="float: left; padding-right: 5px;">
                                                        AC
                                                <dxe:ASPxTextBox runat="server" Text='<%# Eval("ieAC")%>' CssClass="tamanhoTxt"></dxe:ASPxTextBox>
                                                    </div>
                                                    <div style="float: left; padding-right: 5px;">
                                                        PA
                                                <dxe:ASPxTextBox runat="server" Text='<%# Eval("iePA")%>' CssClass="tamanhoTxt"></dxe:ASPxTextBox>
                                                    </div>
                                                    <div style="float: left; padding-right: 5px;">
                                                        GO
                                                <dxe:ASPxTextBox runat="server" Text='<%# Eval("ieGO")%>' CssClass="tamanhoTxt"></dxe:ASPxTextBox>
                                                    </div>
                                                    <div style="float: left; padding-right: 5px;">
                                                        PI
                                                <dxe:ASPxTextBox runat="server" Text='<%# Eval("iePI")%>' CssClass="tamanhoTxt"></dxe:ASPxTextBox>
                                                    </div>
                                                    <div style="float: left; padding-right: 5px;">
                                                        RO
                                                <dxe:ASPxTextBox runat="server" Text='<%# Eval("ieRO")%>' CssClass="tamanhoTxt"></dxe:ASPxTextBox>
                                                    </div>
                                                    <div style="float: left; padding-right: 5px;">
                                                        AL
                                                <dxe:ASPxTextBox runat="server" Text='<%# Eval("ieAL")%>' CssClass="tamanhoTxt"></dxe:ASPxTextBox>
                                                    </div>
                                                    <div style="float: left; padding-right: 5px;">
                                                        RR
                                                <dxe:ASPxTextBox runat="server" Text='<%# Eval("ieRR")%>' CssClass="tamanhoTxt"></dxe:ASPxTextBox>
                                                    </div>
                                                </div>
                                            </EditItemTemplate>
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewCommandColumn VisibleIndex="50" Width="50px" ButtonType="Image">
                                            <EditButton Visible="True" Text="Editar">
                                                <Image Url="~/admin/images/btEditar-v2.jpg" />
                                            </EditButton>
                                            <NewButton Visible="False" Text="Novo">
                                                <Image Url="~/admin/images/btNovo.jpg" />
                                            </NewButton>
                                            <CancelButton Text="Cancelar">
                                                <Image Url="~/admin/images/btCancelar.jpg" />
                                            </CancelButton>
                                            <UpdateButton Text="Salvar">
                                                <Image Url="~/admin/images/btSalvarPeq.jpg" />
                                            </UpdateButton>
                                            <ClearFilterButton Visible="True" Text="Limpar filtro">
                                                <Image Url="~/admin/images/btLimparFiltro.jpg" />
                                            </ClearFilterButton>
                                            <HeaderTemplate>
                                                <%--<img alt="" src="images/legendaIcones.jpg" />--%>
                                            Editar
                                            </HeaderTemplate>
                                        </dxwgv:GridViewCommandColumn>
                                    </Columns>
                                    <StylesEditors>
                                        <Label Font-Bold="True">
                                        </Label>
                                    </StylesEditors>
                                </dxwgv:ASPxGridView>
                            </div>

                            <asp:LinqDataSource ID="sqlEmpresa" runat="server" ContextTypeName="dbCommerceDataContext" EnableDelete="False" EnableInsert="True" EnableUpdate="True"
                                TableName="tbEmpresas" OnUpdating="sqlEmpresa_Updating" OnUpdated="sqlEmpresa_OnUpdated">
                            </asp:LinqDataSource>
                            <asp:LinqDataSource ID="sqlEstados" runat="server"
                                ContextTypeName="dbCommerceDataContext" TableName="tbEstados">
                            </asp:LinqDataSource>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>

