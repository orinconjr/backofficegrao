﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxEditors;

public partial class admin_comprasSolicitacoesPendentes : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            FillListaSemCadastro();
            FillListaPendentes();
        }
    }

    private void FillListaSemCadastro()
    {
        var data = new dbCommerceDataContext();
        var produtosSemCadastro = (from c in data.tbComprasSolicitacaoProdutos
            where c.idComprasProduto == null
            orderby c.tbComprasSolicitacao.dataSolicitacao
            select c).ToList();
        if (produtosSemCadastro.Count == 0) pnProdutosPendentes.Visible = false;
        lstProdutosSemCadastro.DataSource = produtosSemCadastro;
        lstProdutosSemCadastro.DataBind();
    }

    protected void ASPxComboBox_OnItemsRequestedByFilterCondition_SQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {
        ASPxComboBox comboBox = (ASPxComboBox)source;
        SqlDataSource1.SelectCommandType = SqlDataSourceCommandType.StoredProcedure;
        SqlDataSource1.SelectCommand = @"comboFornecedores";
          //     @"SELECT [ID], [Phone], [FirstName], [LastName] FROM (select [ID], [Phone], [FirstName], [LastName], row_number()over(order by t.[LastName]) as [rn] from [Persons] as t where (([FirstName] + ' ' + [LastName] + ' ' + [Phone]) LIKE @filter)) as st where st.[rn] between @startIndex and @endIndex";

        SqlDataSource1.SelectParameters.Clear();
        SqlDataSource1.SelectParameters.Add("filter", TypeCode.String, string.Format("%{0}%", e.Filter));
        //SqlDataSource1.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        //SqlDataSource1.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());
        comboBox.DataSource = SqlDataSource1;
        comboBox.DataBind();
    }
    protected void ASPxComboBox_OnItemRequestedByValue_SQL(object source, ListEditItemRequestedByValueEventArgs e)
    {
        //long value = 0;
        //if (e.Value == null || !Int64.TryParse(e.Value.ToString(), out value))
        //    return;
        //ASPxComboBox comboBox = (ASPxComboBox)source;
        //SqlDataSource1.SelectCommand = @"SELECT ID, LastName, [Phone], FirstName FROM Persons WHERE (ID = @ID) ORDER BY FirstName";

        //SqlDataSource1.SelectParameters.Clear();
        //SqlDataSource1.SelectParameters.Add("ID", TypeCode.Int64, e.Value.ToString());
        //comboBox.DataSource = SqlDataSource1;
        //comboBox.DataBind();
    }
    private void FillListaPendentes()
    {
        var data = new dbCommerceDataContext();
        var produtosPendentes = (from c in data.tbComprasSolicitacaoProdutos
                                   where c.statusSolicitacao == 0 && c.idComprasProduto != null
                                   orderby c.tbComprasSolicitacao.dataSolicitacao
                                   select new {
                                       c.tbComprasSolicitacao.dataSolicitacao,
                                       c.tbComprasSolicitacao.tbUsuario.usuarioNome,
                                       c.tbComprasProduto.produto,
                                       c.idComprasProduto,
                                       c.quantidade,
                                       c.idComprasSolicitacaoProduto,
                                       c.tbComprasSolicitacao.motivo,
                                       c.idComprasSolicitacao
                                   }).ToList();
        if (produtosPendentes.Count == 0) pnSolicitacoesPendentes.Visible = false;
        lstProdutosSolicitacao.DataSource = produtosPendentes;
        lstProdutosSolicitacao.DataBind();
    }

    protected void btnGravar_OnClick(object sender, EventArgs e)
    {
        foreach (var item in lstProdutosSemCadastro.Items)
        {
            
            var chkGravar = (CheckBox)item.FindControl("chkGravar");
            if (chkGravar.Checked)
            {
                var hdfIdComprasSolicitacaoProduto = (HiddenField)item.FindControl("hdfIdComprasSolicitacaoProduto");
                var ddlProduto = (ASPxComboBox)item.FindControl("ddlProduto");
                int idComprasSolicitacaoProduto = Convert.ToInt32(hdfIdComprasSolicitacaoProduto.Value);
                int idComprasProduto = 0;
                try
                {
                    idComprasProduto = Convert.ToInt32(ddlProduto.SelectedItem.Value);
                }
                catch (Exception)
                {

                }

                var data = new dbCommerceDataContext();
                var produto =
                    (from c in data.tbComprasSolicitacaoProdutos
                     where c.idComprasSolicitacaoProduto == idComprasSolicitacaoProduto
                     select c).First();
                if (idComprasProduto > 0)
                {
                    produto.idComprasProduto = idComprasProduto;
                    data.SubmitChanges();
                }
                else
                {
                    string nomeProduto = ddlProduto.Text;
                    if (!string.IsNullOrEmpty(nomeProduto))
                    {
                        var produtoCadastro = new tbComprasProduto();
                        produtoCadastro.dataCadastro = DateTime.Now;
                        produtoCadastro.produto = nomeProduto;
                        data.tbComprasProdutos.InsertOnSubmit(produtoCadastro);
                        data.SubmitChanges();

                        produto.idComprasProduto = produtoCadastro.idComprasProduto;
                        data.SubmitChanges();
                    }
                }
            }
        }

        FillListaSemCadastro();
        FillListaPendentes();
    }

    protected void lstProdutosSolicitacao_OnItemDataBound(object sender, ListViewItemEventArgs e)
    {
        var ddlFornecedor = (ASPxComboBox) e.Item.FindControl("ddlFornecedor");
        var hdfIdComprasProduto = (HiddenField)e.Item.FindControl("hdfIdComprasProduto");
        var txtQuantidade = (ASPxTextBox)e.Item.FindControl("txtQuantidade");
        txtQuantidade.Value = txtQuantidade.Text;
        if (ddlFornecedor != null && hdfIdComprasProduto != null)
        {
            int idComprasProduto = Convert.ToInt32(hdfIdComprasProduto.Value);
            var data = new dbCommerceDataContext();

            var produtosFornecedor = (from c in data.tbComprasProdutoFornecedors
                where c.idComprasProduto == idComprasProduto
                select new
                {
                    c.idComprasProduto,
                    c.idComprasFornecedor,
                    c.dataValor,
                    c.valor,
                    c.tbComprasFornecedor.prazo,
                    c.tbComprasUnidadesDeMedida.unidadeDeMedida
                }).ToList();

            var fornecedoresLista = (from c in data.tbComprasFornecedors
                                select new
                                {
                                    c.idComprasFornecedor,
                                    c.fornecedor,
                                    c.telefone,
                                    c.prazo
                                }).ToList();

            var fornecedores = (from c in fornecedoresLista
                                select new
                {
                                    c.idComprasFornecedor,
                    c.fornecedor,
                    c.telefone,
                    c.prazo,
                    valor =
                        (from d in produtosFornecedor where d.idComprasFornecedor == c.idComprasFornecedor select d)
                            .FirstOrDefault() == null
                            ? null
                            : (decimal?)
                                (from d in produtosFornecedor
                                    where d.idComprasFornecedor == c.idComprasFornecedor
                                    select d).FirstOrDefault().valor,
                    dataValor =
                        (from d in produtosFornecedor where d.idComprasFornecedor == c.idComprasFornecedor select d)
                            .FirstOrDefault() == null
                            ? null
                            : (DateTime?)
                                (from d in produtosFornecedor
                                 where d.idComprasFornecedor == c.idComprasFornecedor
                                 select d).FirstOrDefault().dataValor,
                                    unidadeDeMedida =
                        (from d in produtosFornecedor where d.idComprasFornecedor == c.idComprasFornecedor select d)
                            .FirstOrDefault() == null
                            ? "" :
                                (from d in produtosFornecedor
                                 where d.idComprasFornecedor == c.idComprasFornecedor
                                 select d).FirstOrDefault().unidadeDeMedida
                                });
            fornecedores = fornecedores.OrderBy(x => x.valor == null).ThenBy(x => x.valor).ThenBy(x => x.fornecedor);
            ddlFornecedor.ValueType = typeof(System.Int32);
            ddlFornecedor.DataSource = fornecedores;
            ddlFornecedor.ValueType = typeof(System.Int32);
            ddlFornecedor.DataBind();
            ddlFornecedor.ValueType = typeof(System.Int32);
        }
    }

    protected void btnGravarOrdemCompra_OnClick(object sender, EventArgs e)
    {
        var data = new dbCommerceDataContext();
        foreach (var item in lstProdutosSolicitacao.Items)
        {
            var chkGravar = (CheckBox) item.FindControl("chkGravar");
            if (chkGravar.Checked)
            {
                var ddlEmpresa = (ASPxComboBox)item.FindControl("ddlEmpresa");
                var ddlFornecedor = (ASPxComboBox)item.FindControl("ddlFornecedor");
                var ddlUnidadesDeMedida = (ASPxComboBox)item.FindControl("ddlUnidadesDeMedida");
                var txtQuantidade = (ASPxTextBox)item.FindControl("txtQuantidade");
                var hdfIdComprasSolicitacaoProduto = (HiddenField)item.FindControl("hdfIdComprasSolicitacaoProduto");
                var hdfIdComprasProduto = (HiddenField)item.FindControl("hdfIdComprasProduto");
                var txtValor = (ASPxTextBox)item.FindControl("txtValor");
                int idEmpresa = 0;
                int idFornecedor = 0;
                int idUnidadeDeMedida = 0;
                int quantidade = 0;
                decimal valor = 0;
                int idComprasSolicitacaoProduto = Convert.ToInt32(hdfIdComprasSolicitacaoProduto.Value);
                int idComprasProduto = Convert.ToInt32(hdfIdComprasProduto.Value); 
                try { idEmpresa = Convert.ToInt32(ddlEmpresa.SelectedItem.Value); } catch (Exception) { }
                try { idFornecedor = Convert.ToInt32(ddlFornecedor.Value); } catch (Exception) { }
                try
                    {
                        idUnidadeDeMedida = Convert.ToInt32(ddlUnidadesDeMedida.SelectedItem.Value);
                }
                catch (Exception) { }
                try
                        {
                            quantidade = Convert.ToInt32(txtQuantidade.Value);
                }
                catch (Exception) { }
                try
                            {
                                valor = Convert.ToDecimal(txtValor.Value);
                }
                catch (Exception) { }
                if (idEmpresa > 0 && idFornecedor > 0 && quantidade > 0)
                {
                    #region Atualiza valor no fornecedor
                    if (valor > 0 | idUnidadeDeMedida > 0)
                    {
                        var produtoFornecedorChecagem = (from c in data.tbComprasProdutoFornecedors
                                                         where
                                                             c.idComprasProduto == idComprasProduto &&
                                                             c.idComprasFornecedor == idFornecedor
                                                 select c).FirstOrDefault();
                        if (produtoFornecedorChecagem != null)
                        {
                            if(idUnidadeDeMedida > 0) produtoFornecedorChecagem.idComprasUnidadeMedida = idUnidadeDeMedida;
                            if(valor > 0) produtoFornecedorChecagem.valor = valor;
                            produtoFornecedorChecagem.dataValor = DateTime.Now;

                            var fornecedorHistorico = new tbComprasProdutoFornecedorHistorico
                            {
                                idComprasProduto = produtoFornecedorChecagem.idComprasProduto,
                                idComprasFornecedor = produtoFornecedorChecagem.idComprasFornecedor,
                                idComprasUnidadeMedida = produtoFornecedorChecagem.idComprasUnidadeMedida,
                                valor = produtoFornecedorChecagem.valor,
                                data = DateTime.Now,
                                nomeFornecedor = produtoFornecedorChecagem.nomeFornecedor,
                                idUsuario = RetornaIdUsuarioLogado()
                            };
                            data.tbComprasProdutoFornecedorHistoricos.InsertOnSubmit(fornecedorHistorico);
                            data.SubmitChanges();
                        }
                        else
                        {
                            if (valor > 0 && idUnidadeDeMedida > 0)
                            {
                                var produtoFornecedor = new tbComprasProdutoFornecedor();
                                produtoFornecedor.idComprasProduto = idComprasProduto;
                                produtoFornecedor.idComprasFornecedor = idFornecedor;
                                produtoFornecedor.idComprasUnidadeMedida = idUnidadeDeMedida;
                                produtoFornecedor.valor = valor;
                                produtoFornecedor.dataValor = DateTime.Now;
                                produtoFornecedor.nomeFornecedor = "";

                                data.tbComprasProdutoFornecedors.InsertOnSubmit(produtoFornecedor);

                                var fornecedorHistorico = new tbComprasProdutoFornecedorHistorico
                                {
                                    idComprasProduto = produtoFornecedor.idComprasProduto,
                                    idComprasFornecedor = produtoFornecedor.idComprasFornecedor,
                                    idComprasUnidadeMedida = produtoFornecedor.idComprasUnidadeMedida,
                                    valor = produtoFornecedor.valor,
                                    data = DateTime.Now,
                                    nomeFornecedor = produtoFornecedor.nomeFornecedor,
                                    idUsuario = RetornaIdUsuarioLogado()
                                };
                                data.tbComprasProdutoFornecedorHistoricos.InsertOnSubmit(fornecedorHistorico);
                                data.SubmitChanges();
                            }
                        }
                    }
                    #endregion

                    var fornecedorProduto = (from c in data.tbComprasProdutoFornecedors
                        where c.idComprasProduto == idComprasProduto && c.idComprasFornecedor == idFornecedor
                        select c).FirstOrDefault();
                    if (fornecedorProduto != null)
                    {
                        var itemOrdem = new tbComprasOrdemProduto();
                        itemOrdem.idComprasProduto = idComprasProduto;
                        itemOrdem.quantidade = quantidade;
                        itemOrdem.preco = fornecedorProduto.valor;
                        itemOrdem.idComprasOrdem = RetornaIdOrdemPendente(idEmpresa, idFornecedor);
                        itemOrdem.status = 0;
                        itemOrdem.comentario = "";
                        itemOrdem.idComprasSolicitacaoProduto = idComprasSolicitacaoProduto;
                        itemOrdem.dataCadastro = DateTime.Now;

                        data.tbComprasOrdemProdutos.InsertOnSubmit(itemOrdem);
                        data.SubmitChanges();

                        var itemOrdemHistorico = new tbComprasOrdemProdutoHistorico();
                        itemOrdemHistorico.data = DateTime.Now.ToShortDateString();
                        itemOrdemHistorico.idComprasOrdemProduto = itemOrdem.idComprasOrdemProduto;
                        itemOrdemHistorico.usuario = rnUsuarios.retornaNomeUsuarioLogado();
                        itemOrdemHistorico.descricao = String.Format("Solicitacao Inicial | Quantidade: {0} - Preço: {1} - Fornecedor: {2}", itemOrdem.quantidade, itemOrdem.preco.ToString("C"), fornecedorProduto.tbComprasFornecedor.fornecedor);
                        
                        data.tbComprasOrdemProdutoHistoricos.InsertOnSubmit(itemOrdemHistorico);
                        data.SubmitChanges();

                        var itemSolicitacao =
                            (from c in data.tbComprasSolicitacaoProdutos
                                where c.idComprasSolicitacaoProduto == idComprasSolicitacaoProduto
                                select c).First();
                        itemSolicitacao.statusSolicitacao = 1;
                        data.SubmitChanges();
                    }
                }

                if (quantidade == 0)
                {
                    var itemSolicitacao =
                        (from c in data.tbComprasSolicitacaoProdutos
                         where c.idComprasSolicitacaoProduto == idComprasSolicitacaoProduto
                         select c).First();
                    itemSolicitacao.statusSolicitacao = 2;
                    data.SubmitChanges();
                }
                
            }
        }

        FillListaPendentes();
    }


    private int RetornaIdOrdemPendente(int idEmpresa, int idFornecedor)
    {
        var data = new dbCommerceDataContext();
        var ordemCompra = (from c in data.tbComprasOrdems
            where c.idComprasEmpresa == idEmpresa && c.idComprasFornecedor == idFornecedor && c.statusOrdemCompra == 0
            select c).FirstOrDefault();
        if (ordemCompra != null) return ordemCompra.idComprasOrdem;

        var ordem = new tbComprasOrdem();
        ordem.idComprasEmpresa = idEmpresa;
        ordem.idComprasFornecedor = idFornecedor;
        ordem.idUsuarioCadastroOrdem = RetornaIdUsuarioLogado();
        ordem.dataCriacao = DateTime.Now;
        ordem.statusOrdemCompra = 0;
        data.tbComprasOrdems.InsertOnSubmit(ordem);
        data.SubmitChanges();
        return ordem.idComprasOrdem;
    }

    private int RetornaIdUsuarioLogado()
    {
        HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
        usuarioLogadoId = Request.Cookies["usuarioLogadoId"];
        if (usuarioLogadoId == null)
        {
            return 0;
        }

        return Convert.ToInt32(usuarioLogadoId.Value);
    }
}