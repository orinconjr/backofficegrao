﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="relatorioPedidosFornecedor.aspx.cs" Inherits="admin_relatorioPedidosFornecedor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" runat="Server">
    <script src="js/jquery.min.js"></script>
    <style>
        .meubotao {
            cursor: pointer;
            font: bold 14px tahoma;
        }

        .fieldsetatualizarprecos {
            width: 848px;
            float: left;
            clear: left;
            border-radius: 5px;
            margin-left: 25px;
            font: bold 14px tahoma;
        }

            .fieldsetatualizarprecos input[type=checkbox] {
                margin-top: 15px;
            }

            .fieldsetatualizarprecos label {
                margin-left: 5px;
                margin-right: 20px;
            }

        .meugrid {
            background-color: #F4FAFB;
            border: 0px none;
            /* border-collapse: separate !important; */
            overflow: hidden;
            font: 9pt Tahoma;
            color: #000;
            cursor: pointer;
            white-space: nowrap;
            font-weight: normal;
            text-align: left;
            width: 834px;
            border-collapse: collapse;
            float: left;
            clear: left;
            margin-top: 10px;
            margin-bottom: 10px;
            min-height: 500px;
            margin-left: 25px;
        }

            .meugrid th {
                background-image: url(../../App_Themes/Glass/GridView/gvHeaderBackground.gif);
                background-position: center top;
                background-repeat: repeat-x;
                background-color: #C0DDE0;
                overflow: hidden;
                height: 23px !important;
                line-height: 23px !important;
                padding: 4px 6px 5px;
                font-weight: normal;
                border: 1px solid #7EACB1;
                /* border-collapse: separate;*/
            }

            .meugrid td {
                overflow: hidden;
                height: 23px !important;
                line-height: 23px !important;
                padding: 4px 6px 5px;
                border: 1px solid #7EACB1;
            }

            .meugrid tr td:last-of-type {
                text-align: center;
            }

        .tabrotulos {
            width: 848px;
            margin: auto;
        }

        .materiaprima {
            width: 250px;
        }

            .materiaprima ul {
                margin: 0px;
                padding-left: 10px;
            }

                .materiaprima ul li {
                    max-width: 230px;
                    white-space: pre-wrap;
                }

        .meugrid tr td:last-of-type {
            text-align: center;
        }

        .nome {
            width: 237px;
        }

            .nome div {
                max-width: 230px;
                white-space: pre-wrap;
            }

        .rotulos td {
            font: bold 12px tahoma;
            /*padding-top: 20px; */
            color: rgb(65, 105, 126);
            padding-left: 5px;
            /*padding-bottom: 5px;*/
        }
    </style>
    <script type="text/javascript">

        function marcarDesmarcarCheckBoxes(marcar) {
            $("[id='<%=GridView1.ClientID%>'] tr td input[name$='ckbPago']").each(function () {
                if (marcar) {
                    $("#btnMarcarTodosComoPago").val("Desmarcar todos pagos");
                    $("#btnMarcarTodosComoPago").attr("onclick", "marcarDesmarcarCheckBoxes(false)");

                    if ($(this).attr("checked") == undefined) {
                        $(this).attr("checked", true);
                    }
                }
                else {
                    $("#btnMarcarTodosComoPago").val("Marcar todos como pagos");
                    $("#btnMarcarTodosComoPago").attr("onclick", "marcarDesmarcarCheckBoxes(true)");
                    if ($(this).attr("disabled") == undefined) {
                        $(this).attr("checked", false);
                    }
                }
            }
            );
        }
    </script>
    <div class="tituloPaginas" valign="top">
        Relatório de Pedidos ao Fornecedor
    </div>
    <div>
        <fieldset class="fieldsetatualizarprecos">
            <legend style="font-weight: bold;">Filtro</legend>

            <table class="tabrotulos">
                <tr class="rotulos">
                    <td>Entregue:<br />
                        <asp:RadioButton runat="server" ID="rdbEntregueSim" Text="Sim" Checked="True" GroupName="filtroEntregue" />
                        <asp:RadioButton runat="server" ID="rdbEntregueParcial" Text="Parcial" Checked="False" GroupName="filtroEntregue" />
                    </td>

                    <td>Pago:<br />
                        <asp:RadioButton runat="server" ID="rdbTodos" Text="Todos" Checked="True" GroupName="filtroPago" />
                          <asp:RadioButton runat="server" ID="rdbNao" Text="Não" Checked="False" GroupName="filtroPago" />
                          <asp:RadioButton runat="server" ID="rdbSim" Text="Sim" Checked="False" GroupName="filtroPago" />
                    </td>

                    <td>Fornecedor:<br />
                        <asp:DropDownList ID="ddlfornecedor" CssClass="combobox" runat="server"></asp:DropDownList>
                    </td>

                </tr>
                <tr class="rotulos">

                    <td>Data Inicial:<br />
                        <asp:TextBox runat="server" ID="txtDataInicial"></asp:TextBox>
                    </td>
                    <td>Data Final:<br />
                        <asp:TextBox runat="server" ID="txtDataFinal"></asp:TextBox>
                    </td>
                    <td>Ordenar por:<br />
                        <asp:DropDownList runat="server" ID="ddlordenarpor" Width="370px" CssClass="campos">
                            <Items>
                                <asp:ListItem Text="id PedidoFornecedor" Value="idPedidoFornecedor"></asp:ListItem>
                                <asp:ListItem Text="fornecedorNome" Value="Nome do fornecedor"></asp:ListItem>
                                <asp:ListItem Text="Custo" Value="Custo"></asp:ListItem>
                               <asp:ListItem Text="dataPedido" Value="Data do Pedido"></asp:ListItem>
                               <asp:ListItem Text="dataPagamento" Value="Data do Pagamento"></asp:ListItem>
                            </Items>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr class="rotulos">
                    <td colspan="2">
                        Id Pedido Fornecedor:<br />
                        <asp:TextBox runat="server" ID="txtIdPedidoFornecedor"></asp:TextBox>
                    </td>

                    <td style="padding-top: 15px; text-align: right;">
<%--                        <asp:Button runat="server"  CssClass="meubotao" Text="Filtrar"  />--%>
                        <asp:ImageButton ID="btnFiltrar" runat="server" OnClick="btnFiltrar_Click"
                                            ImageUrl="~/admin/images/btPesquisar.jpg" />
                    </td>
                </tr>

            </table>
        </fieldset>

        <div style="float: left; clear: left; width: 868px; text-align: right; margin-left: 25px; margin-top: 15px;">
            <asp:Button runat="server" ID="btnExportarPlanilha" CssClass="meubotao" Text="Exportar Planilha" OnClick="btnExportarPlanilha_Click" />
            <input id="btnMarcarTodosComoPago" type="button" class="meubotao"  onclick="marcarDesmarcarCheckBoxes(true);" value="Marcar todos como pagos" />
        </div>
    </div>
    <div align="center" style="min-height: 500px;">

        <asp:GridView ID="GridView1" CssClass="meugrid" runat="server" DataKeyNames="idPedidoFornecedor" Width="834px" AutoGenerateColumns="False" AllowPaging="False"  OnRowDataBound="GridView1_RowDataBound" >
            <Columns>
                <asp:BoundField DataField="idPedidoFornecedor" HeaderText="pedidoFornecedor" />
                <asp:BoundField DataField="fornecedorNome" HeaderText="Fornecedor" />
                <asp:BoundField DataField="custo"  DataFormatString=" {0:C}"  HeaderText="Custo" />
                <asp:BoundField DataField="dataPedido" HeaderText="Data do Pedido" />
         <%--       <asp:BoundField DataField="dataDeEntrega" HeaderText="Data de Entrega" />--%>
                <asp:TemplateField HeaderText="Datas de Entrega" HeaderStyle-Width="150px" ShowHeader="False">
                    <ItemTemplate>
                        <asp:Label ID="lblDatasEntrega" Width="150px" runat="server" Text=""></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="dataPagamento" HeaderText="Data de Pagamento" />

                <asp:TemplateField HeaderText="Editar" ShowHeader="False">
                    <ItemTemplate>
                        <a id="linkeditar" runat="server" target="_blank">
                           Ver
                        </a>
                    </ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField HeaderText="Pago" ShowHeader="False">
                    <ItemTemplate>
                        <asp:HiddenField ID="hdidPedidoFornecedor" runat="server" />
                        <asp:CheckBox ID="ckbPago" runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>

    </div>
    <div style="float: left; clear: left; width: 868px; margin-left: 25px; margin-top: 15px;">
        <strong>Quantidade de itens encontrados nesta busca:
        <span style="text-decoration: underline">
            <asp:Label ID="lblitensencontrados" runat="server" Text=""></asp:Label></span>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
           Valor total nesta busca: <span style="text-decoration: underline">
               <asp:Label ID="lblvalortotal" runat="server" Text=""></asp:Label></span>
        </strong>

    </div>
    <div style="float: left; clear: left; width: 868px; margin-left: 25px; margin-top: 15px;text-align:right;">
       <asp:ImageButton ID="imgSalvar" src="images/btSalvar.jpg"   runat="server" OnClick="imgSalvar_Click"   />

    </div>
</asp:Content>

