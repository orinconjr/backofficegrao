﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxTabControl; 
using DevExpress.Web.ASPxEditors;
using DevExpress.Data;
using DevExpress.Web.ASPxUploadControl;
using System.IO;
using DevExpress.Web.Data;


public partial class admin_usuarios : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }


    //protected void btPdf_Click(object sender, ImageClickEventArgs e)
    //{
    //    this.grdEx.WritePdfToResponse();
    //}
    //protected void btXsl_Click(object sender, ImageClickEventArgs e)
    //{
    //    this.grdEx.WriteXlsToResponse();
    //}
    //protected void btRtf_Click(object sender, ImageClickEventArgs e)
    //{
    //    this.grdEx.WriteRtfToResponse();
    //}
    //protected void btCsv_Click(object sender, ImageClickEventArgs e)
    //{
    //    this.grdEx.WriteCsvToResponse();
    //}

    protected void grd_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType != GridViewRowType.EditForm) return;
        {

            var idDepartamento = grd.GetRowValues(0, "idDepartamento");
            if (idDepartamento == null) idDepartamento = "0";

            var teste = (GridViewDataColumn)grd.Columns["paginasPermitidas"];
            CheckBoxList ckbPaginasPermitidas = (CheckBoxList)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["paginasPermitidas"], "ckbPaginasPermitidas");

            foreach (ListItem ltbx in ckbPaginasPermitidas.Items)
            {
                if (e.KeyValue != null)
                {
                    if (rnUsuarios.usuarioPaginasPermitidaSeleciona_PorUsuarioIdPaginaPermitidaNome(int.Parse(e.KeyValue.ToString()), ltbx.Value.ToString()).Tables[0].Rows.Count > 0)
                        ltbx.Selected = true;
                }
            }

            DropDownList ddlD = (DropDownList)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["Departamento"], "ddlDepartamento");
            ddlD.SelectedValue = idDepartamento.ToString();

        }
    }

    protected void grd_RowInserted(object sender, DevExpress.Web.Data.ASPxDataInsertedEventArgs e)
    {
        if (e.Exception == null)
        {
            CheckBoxList ckbPaginasPermitidas = (CheckBoxList)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["paginasPermitidas"], "ckbPaginasPermitidas");

            foreach (ListItem ltbx in ckbPaginasPermitidas.Items)
            {
                if (ltbx.Selected)
                {
                    rnUsuarios.usuarioPaginasPermitidaInclui(int.Parse(rnUsuarios.usuarioSelecionaUltimoId().Tables[0].Rows[0]["usuarioId"].ToString()), ltbx.Value.ToString());
                }
            }
        }
    }

    protected void grd_RowUpdated(object sender, DevExpress.Web.Data.ASPxDataUpdatedEventArgs e)
    {
        if (e.Exception == null)
        {
            CheckBoxList ckbPaginasPermitidas = (CheckBoxList)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["paginasPermitidas"], "ckbPaginasPermitidas");

            rnUsuarios.usuarioPaginasPermitidaExclui(int.Parse(e.Keys["usuarioId"].ToString()));

            foreach (ListItem ltbx in ckbPaginasPermitidas.Items)
            {
                if (ltbx.Selected)
                {
                    rnUsuarios.usuarioPaginasPermitidaInclui(int.Parse(e.Keys["usuarioId"].ToString()), ltbx.Value.ToString());
                }
            }
        }
    }

    protected void grd_OnRowUpdating(object sender, ASPxDataUpdatingEventArgs e)
    {
        DropDownList ddlDepartamento = (DropDownList)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["Departamento"], "ddlDepartamento");


        Session["ddlDepartamento"] = 0;

        if (ddlDepartamento.SelectedIndex > 0)
            Session["ddlDepartamento"] = Convert.ToInt32(ddlDepartamento.SelectedValue.ToString());
        //sqlUsuarios.UpdateParameters.Add("idDepartamento", );
    }

    protected void sqlUsuarios_OnUpdating(object sender, LinqDataSourceUpdateEventArgs e)
    {
        var usuarioOriginal = (tbUsuario)e.OriginalObject;
        var usuarioAlterado = (tbUsuario)e.NewObject;
        usuarioAlterado.idDepartamento = (int)Session["ddlDepartamento"];

        if (string.IsNullOrEmpty(usuarioAlterado.usuarioSenha)) usuarioAlterado.usuarioSenha = usuarioOriginal.usuarioSenha;
    }



    protected void ddlDepartamento_Init(object sender, EventArgs e)
    {
        DropDownList ddl = sender as DropDownList;
        var data = new dbCommerceDataContext();

        var Departamentos = (from c in data.tbChamadoDepartamentos select c).ToList();

        Departamentos.Insert(0, new tbChamadoDepartamento() { idChamadoDepartamento = 0, nomeChamadoDepartamento = "Outros" });

        ddl.DataSource = Departamentos;
        ddl.DataTextField = "nomeChamadoDepartamento";
        ddl.DataValueField = "idChamadoDepartamento";
        ddl.DataBind();
    }
}
