﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_vincularNotaPedido : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            FillEmpresas();
        }
    }

    protected void FillEmpresas()
    {
        using (var data = new dbCommerceDataContext())
        {
            var usuarioLogadoId = Request.Cookies["usuarioLogadoId"];

            if (usuarioLogadoId == null)
                Response.Redirect("default.aspx");

            int usuarioId = Convert.ToInt32(usuarioLogadoId.Value);

            if(usuarioId == 167 || usuarioId == 370)
            {
                var empresas = (from c in data.tbEmpresas orderby c.nome select c).ToList();
                ddlEmpresa.DataSource = empresas;
                ddlEmpresa.DataBind();
            }
            else
            {
                var empresas = (from c in data.tbEmpresas where c.idEmpresa== 6 orderby c.nome select c).ToList();
                ddlEmpresa.DataSource = empresas;
                ddlEmpresa.DataBind();
            }


        }
    }



    protected void btnvincular_Click(object sender, EventArgs e)
    {
        if (!validarCampos())
            return;

        var data = new dbCommerceDataContext();
        string nf = txtNota.Text;
        string cnp = ddlEmpresa.SelectedItem.Value;
        tbNotaFiscal nota = (from c in data.tbNotaFiscals where c.numeroNota == Convert.ToInt32(txtNota.Text) && c.idCNPJ == Convert.ToInt32(ddlEmpresa.SelectedItem.Value) select c).FirstOrDefault();
        string mensagem = "";
        if(nota != null)
        {
            try
            {
                nota.idPedido = Convert.ToInt32(txtPedido.Text);
                string chave = txtChave.Text.Replace(" ", "").Trim();
                nota.nfeKey = chave.Substring(0, (chave.Length - 1));
                nota.valor = Convert.ToDecimal(txtValor.Text.Replace(",", "."));
                data.SubmitChanges();
                mensagem = "A Nfe " + txtNota.Text + " foi vinculada com sucesso ao pedido " + txtPedido.Text;
            }
            catch(Exception ex)
            {
                mensagem = "Erro ao vincular a Nfe ao pedido";
            }
        }
        else
        {
            mensagem = "A Nfe " + txtNota.Text + " não existe";
        }

        string meuscript = @"alert('" + mensagem + "');";
        Page.ClientScript.RegisterStartupScript(this.GetType(), "MyScript", meuscript, true);
    }

    bool validarCampos()
    {
        bool ok = true;
        string mensagem = "";

        int nota = 0;
        if(String.IsNullOrEmpty(txtNota.Text))
        {
            ok = false;
            mensagem += @"Digite a Nfe\n";
        }
        else
        {
            try
            {
                nota = Convert.ToInt32(txtNota.Text);
            }
            catch (Exception)
            {

            }
            if (nota == 0)
            {
                ok = false;
                mensagem += @"Nota inválida\n";
            }
        }


        long chave = 0;
        if (String.IsNullOrEmpty(txtChave.Text))
        {
            ok = false;
            mensagem += @"Digite a Chave\n";
        }


        int pedido = 0;
        if (String.IsNullOrEmpty(txtPedido.Text))
        {
            ok = false;
            mensagem += @"Digite o Pedido\n";
        }
        else
        {
            try
            {
                pedido = Convert.ToInt32(txtPedido.Text);
            }
            catch (Exception)
            {

            }
            if (pedido == 0)
            {
                ok = false;
                mensagem += @"Pedido inválida\n";
            }
        }



        decimal valor = 0;
        if (String.IsNullOrEmpty(txtValor.Text))
        {
            ok = false;
            mensagem += @"Digite o Valor\n";
        }
        else
        {
            try
            {
                valor = Convert.ToDecimal(txtValor.Text.Replace(",", "."));
            }
            catch (Exception)
            {

            }

            if (valor == 0)
            {
                ok = false;
                mensagem += @"Valor inválido\n";
            }
        }
  


  

        if (!ok)
        {
            string meuscript = @"alert('" + mensagem + "');";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "MyScript", meuscript, true);

        }
        return ok;

    }
}