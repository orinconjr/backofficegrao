﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxTabControl;
using DevExpress.Web.ASPxEditors;
using DevExpress.Data;
using DevExpress.Web.ASPxUploadControl;
using System.IO;
using System.Collections.Generic;

public partial class admin_paisesPrecosEPesos : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request.QueryString["paisNome"]))
        {
            if (!Page.IsPostBack)
            {
                lblAcao.Text = "Pesos e preços da faixa de cep de: " + Request.QueryString["paisNome"].ToString();
                atribuiJs();
            }
        }
        else
        {
            Response.Write("<script>alert('Selecione um pais.');</script>");
            Response.Write("<script>window.location=('paises.aspx');</script>");
        }
    }

    public void atribuiJs()
    {
        txtPesoInicial.Attributes.Add("onclick", "document.getElementById('" + txtPesoInicial.ClientID + "').focus();document.getElementById('" + txtPesoInicial.ClientID + "').select();");
        txtPesoInicial.Attributes.Add("onkeypress", "return soNumero(event);");
        txtPesoFinal.Attributes.Add("onclick", "document.getElementById('" + txtPesoFinal.ClientID + "').focus();document.getElementById('" + txtPesoFinal.ClientID + "').select();");
        txtPesoFinal.Attributes.Add("onkeypress", "return soNumero(event);");
        txtPreco.Attributes.Add("onclick", "document.getElementById('" + txtPreco.ClientID + "').focus();document.getElementById('" + txtPreco.ClientID + "').select();");
        txtPreco.Attributes.Add("onkeyPress", "return moeda(this,'.',',',event);");
    }

    //protected void btPdf_Click(object sender, ImageClickEventArgs e)
    //{
    //    this.grdEx.WritePdfToResponse();
    //}
    //protected void btXsl_Click(object sender, ImageClickEventArgs e)
    //{
    //    this.grdEx.WriteXlsToResponse();
    //}
    //protected void btRtf_Click(object sender, ImageClickEventArgs e)
    //{
    //    this.grdEx.WriteRtfToResponse();
    //}
    //protected void btCsv_Click(object sender, ImageClickEventArgs e)
    //{
    //    this.grdEx.WriteCsvToResponse();
    //}

    protected void imbInsert_Click(object sender, ImageClickEventArgs e)
    {
        if (rnEntregaInternacional.paisesPrecoDeEntregaSeleciona_PorPaisIdPesoInicialPesoFinal(int.Parse(Request.QueryString["paisId"].ToString()), decimal.Parse(txtPesoInicial.Text), decimal.Parse(txtPesoFinal.Text)).Tables[0].Rows.Count > 0)
        {
            Response.Write("<script>alert('Já existe um peso inicial ou final para essa faixa de cep.');</script>");
        }
        else
        {
            if (rnEntregaInternacional.faixaDeCepInternacionalPesoPrecoInclui(int.Parse(Request.QueryString["paisId"].ToString()), Convert.ToDecimal(txtPreco.Text), decimal.Parse(txtPesoInicial.Text), decimal.Parse(txtPesoFinal.Text), Request.QueryString["paisNome"].ToString()))
            {
                grd.DataBind();
            }
        }
    }

    protected void grd_HtmlRowCreated(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType != GridViewRowType.Data) return;
        {
            TextBox txtPesoInicial = (TextBox)grd.FindRowCellTemplateControl(e.VisibleIndex, (GridViewDataColumn)grd.Columns["faixaDeCepPesoInicial"], "txtPesoInicial");
            TextBox txtPesoFinal = (TextBox)grd.FindRowCellTemplateControl(e.VisibleIndex, (GridViewDataColumn)grd.Columns["faixaDeCepPesoFinal"], "txtPesoFinal");
            TextBox txtPreco = (TextBox)grd.FindRowCellTemplateControl(e.VisibleIndex, (GridViewDataColumn)grd.Columns["faixaDeCepPreco"], "txtPreco");

            txtPesoInicial.Attributes.Add("onclick", "document.getElementById('" + txtPesoInicial.ClientID + "').focus();document.getElementById('" + txtPesoInicial.ClientID + "').select();");
            txtPesoInicial.Attributes.Add("onkeypress", "return soNumero(event);");
            txtPesoFinal.Attributes.Add("onclick", "document.getElementById('" + txtPesoFinal.ClientID + "').focus();document.getElementById('" + txtPesoFinal.ClientID + "').select();");
            txtPesoFinal.Attributes.Add("onkeypress", "return soNumero(event);");
            txtPreco.Attributes.Add("onclick", "document.getElementById('" + txtPreco.ClientID + "').focus();document.getElementById('" + txtPreco.ClientID + "').select();");
            txtPreco.Attributes.Add("onkeyPress", "return moeda(this,'.',',',event);");
        }
    }

    protected void grd_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        for (int i = 0; i < grd.GetCurrentPageRowValues("faixaDeCepPesoPrecoId").Count; i++)
        {
            TextBox txtPesoInicial = (TextBox)grd.FindRowCellTemplateControl(i, grd.Columns["faixaDeCepPesoInicial"] as GridViewDataColumn, "txtPesoInicial");
            TextBox txtPesoFinal = (TextBox)grd.FindRowCellTemplateControl(i, grd.Columns["faixaDeCepPesoFinal"] as GridViewDataColumn, "txtPesoFinal");
            TextBox txtPreco = (TextBox)grd.FindRowCellTemplateControl(i, grd.Columns["faixaDeCepPreco"] as GridViewDataColumn, "txtPreco");
            TextBox txtFaixaDeCepPesoPrecoId = (TextBox)grd.FindRowCellTemplateControl(i, grd.Columns["faixaDeCepPesoPrecoId"] as GridViewDataColumn, "txtFaixaDeCepPesoPrecoId");

            rnEntregaInternacional.entregaInternacionalAlterar(int.Parse(Request.QueryString["paisId"].ToString()), Convert.ToDecimal(txtPreco.Text), int.Parse(txtPesoInicial.Text), int.Parse(txtPesoFinal.Text), Request.QueryString["paisNome"].ToString(), int.Parse(txtFaixaDeCepPesoPrecoId.Text));

            ASPxCheckBox ckbExcluir = (ASPxCheckBox)grd.FindRowCellTemplateControl(i, grd.Columns["excluir"] as GridViewDataColumn, "ckbExcluir");
            if (ckbExcluir.Checked.ToString() == "True")
                rnEntregaInternacional.entregaInternacionalExcluir(int.Parse(txtFaixaDeCepPesoPrecoId.Text));
        }
        grd.DataBind();
    }
}
