﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AmazingCloudSearch;

public partial class admin_atualizarProdutosBusca : System.Web.UI.Page
{
    private static double progress;
    private static string status;

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnAtualizarBusca_OnClick(object sender, EventArgs e)
    {
        rnBuscaCloudSearch.AtualizarTodos(0, 1);
    }

    protected void ASPxCallback1_Callback(object source, DevExpress.Web.ASPxCallback.CallbackEventArgs e)
    {
        if (chkAtualizaNomes.Checked)
        {
            /*string caminho = MapPath("~/");
            string connStr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + caminho + "nomes.xls;Extended Properties=Excel 12.0;";

            OleDbConnection cnnExcel = new OleDbConnection(connStr);
            OleDbCommand cmmPlan = null;
            OleDbDataReader drPlan = null;

            cnnExcel.Open();
            for (int i = 1; i < 2; i++)
            {
                string nomePlanilha = "plan" + i;
                cmmPlan = new OleDbCommand("select * from [" + nomePlanilha + "$]", cnnExcel);
                drPlan = cmmPlan.ExecuteReader(CommandBehavior.CloseConnection);

                int contagem = 0;
                while (drPlan.Read())
                {
                    progress = contagem;
                    string id = drPlan[0].ToString();
                    string nome = drPlan[1].ToString();

                    int produtoId = 0;
                    int.TryParse(id, out produtoId);

                    if (produtoId > 0)
                    {
                        var produtoDc = new dbCommerceDataContext();
                        var produto = (from c in produtoDc.tbProdutos where c.produtoId == produtoId select c).FirstOrDefault();
                        if (produto != null)
                        {
                            produto.produtoNomeNovo = nome;
                            produtoDc.SubmitChanges();
                        }
                    }

                    contagem++;
                }
            }
            cnnExcel.Close();
            cnnExcel.Dispose();*/
            var data = new dbCommerceDataContext();
            var produtos =
                (from c in data.tbJuncaoProdutoCategorias
                    where c.categoriaId != 412 && c.categoriaId != 679
                    select c.tbProduto);
            foreach (var produto in produtos)
            {
                var produtoCheck = (from c in data.tbJuncaoProdutoCategorias
                    where c.produtoId == produto.produtoId && (c.categoriaId == 412 | c.categoriaId == 679)
                    select c).Any();
                if (!produtoCheck)
                {
                    produto.produtoPecas = "";
                    data.SubmitChanges();
                }
            }
        }

        if (chkAtualizaDescricao.Checked)
        {
            string caminho = MapPath("~/");
            //fluArquivo.SaveAs(caminho + "nomes.xls");
            string connStr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + caminho + "descricao.xls;Extended Properties=Excel 12.0;";

            OleDbConnection cnnExcel = new OleDbConnection(connStr);
            OleDbCommand cmmPlan = null;
            OleDbDataReader drPlan = null;

            cnnExcel.Open();
            for (int i = 1; i < 2; i++)
            {
                string nomePlanilha = "plan" + i;
                cmmPlan = new OleDbCommand("select * from [" + nomePlanilha + "$]", cnnExcel);
                drPlan = cmmPlan.ExecuteReader(CommandBehavior.CloseConnection);

                int contagem = 0;
                while (drPlan.Read())
                {
                    progress = contagem;
                    string id = drPlan[0].ToString();
                    string descricao = drPlan[1].ToString();

                    int produtoId = 0;
                    int.TryParse(id, out produtoId);

                    if (produtoId > 0)
                    {
                        var produtoDc = new dbCommerceDataContext();
                        var produto = (from c in produtoDc.tbProdutos where c.produtoId == produtoId select c).FirstOrDefault();
                        if (produto != null)
                        {
                            produto.produtoDescricao = descricao;
                            produtoDc.SubmitChanges();
                        }
                    }

                    contagem++;
                }
            }
            cnnExcel.Close();
            cnnExcel.Dispose();
        }

        if (chkAtualizarCategorias.Checked)
        {
            int contagem = 0;
            var data = new dbCommerceDataContext();
            var categorias = (from c in data.tbProdutoCategorias select c);
            foreach (var categoria in categorias)
            {
                progress = contagem;
                categoria.categoriaNome = categoria.categoriaNomeExibicao;
                categoria.categoriaUrl = rnCategorias.GerarPermalinkCategoria(categoria.categoriaNomeExibicao);
                data.SubmitChanges();
                contagem ++;
            }
        }

        if (chkAtualizarNomesAntigos.Checked)
        {
            int contagem = 0;
            var data = new dbCommerceDataContext();

            /*var produtos = (from c in data.tbProdutos
                            join d in data.tbProdutos on c.produtoUrl equals d.produtoUrl into grupoProduto
                            where grupoProduto.Count() > 1 select c).ToList();*/
            var produtos = (from c in data.tbProdutos where c.produtoAtivo.ToLower() == "true" select c).ToList(); ;
            var contagemProdutos = produtos.Count;
            foreach (var produto in produtos)
            {
                progress = Convert.ToDouble((contagem * 100) / contagemProdutos);
                if (!string.IsNullOrEmpty(produto.produtoNomeNovo))
                {
                    try
                    {
                        //produto.produtoNomeAntigo = produto.produtoNome;
                        //produto.produtoNome = produto.produtoNomeNovo;
                        produto.produtoUrl = rnProdutos.GerarPermalinkProduto(produto.produtoId, produto.produtoNomeNovo);
                        data.SubmitChanges();
                    }
                    catch (Exception)
                    {

                    }
                }
                contagem++;
            }

            rnBuscaCloudSearch.AtualizarTodos(0, 15000);

            contagem = 0;
            produtos = (from c in data.tbProdutos where c.produtoAtivo.ToLower() == "false" select c).ToList();
            contagemProdutos = produtos.Count;
            foreach (var produto in produtos)
            {
                progress = Convert.ToDouble((contagem * 100) / contagemProdutos);
                if (!string.IsNullOrEmpty(produto.produtoNomeNovo))
                {
                    try
                    {
                        //produto.produtoNomeAntigo = produto.produtoNome;
                        //produto.produtoNome = produto.produtoNomeNovo;
                        produto.produtoUrl = rnProdutos.GerarPermalinkProduto(produto.produtoId, produto.produtoNomeNovo);
                        data.SubmitChanges();
                        rnBuscaCloudSearch.AtualizarProduto(produto.produtoId);
                    }
                    catch (Exception)
                    {

                    }
                }
                contagem++;
            }

            rnBuscaCloudSearch.AtualizarTodos(0, 15000);
        }

        if (chkAtualizarBusca.Checked)
        {
            progress = 1;
            int contagem = 0;
            rnBuscaCloudSearch.AtualizarTodos(0, 15000);
            progress = 100;
            
        }
        if (chkAtualizarDescricao.Checked)
        {
            var data = new dbCommerceDataContext();
            var informacoes = (from c in data.tbInformacaoAdcionals select c).ToList();

            int contagem = 0;
            int contagemProdutos = informacoes.Count;
            foreach (var informacao in informacoes)
            {
                try
                {
                    var dataAlterar = new dbCommerceDataContext();
                    var informacaoAlterar =
                        (from c in dataAlterar.tbInformacaoAdcionals
                         where c.informacaoAdcionalId == informacao.informacaoAdcionalId
                         select c).First();
                    progress = Convert.ToDouble((contagem * 100) / contagemProdutos);
                    informacaoAlterar.informacaoAdcionalConteudo = rnIntegracoes.removeLinks(informacao.informacaoAdcionalConteudo);
                    dataAlterar.SubmitChanges();
                    contagem++;
                }
                catch (Exception)
                {
                    
                }
            }

        }
    }


    protected void ASPxCallback2_Callback(object source, DevExpress.Web.ASPxCallback.CallbackEventArgs e)
    {
        e.Result = progress.ToString("0.0000").Replace(",", ".");
    }
}