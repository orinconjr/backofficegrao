﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="usuarioDuplicarPermissao.aspx.cs" Inherits="admin_usuarioDuplicarPermissao" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" runat="Server">

    <script src="js/jquery.min.js"></script>
    <style>
        .fieldsetatualizarprecos {
            width: 760px;
            float: left;
            clear: left;
            border-radius: 5px;
            margin-left: 25px;
        }

            .fieldsetatualizarprecos input[type=checkbox] {
                margin-top: 15px;
            }

            .fieldsetatualizarprecos label {
                margin-left: 5px;
                margin-right: 20px;
            }

        .meugrid {
            background-color: #F4FAFB;
            border: 0px none;
            /* border-collapse: separate !important; */
            overflow: hidden;
            font: 9pt Tahoma;
            color: #000;
            cursor: pointer;
            /* white-space: nowrap;*/
            font-weight: normal;
            text-align: left;
            width: 834px;
            border-collapse: collapse;
            float: left;
            clear: left;
            margin-top: 10px;
            margin-bottom: 10px;
            min-height: 500px;
            margin-left: 25px;
        }

            .meugrid th {
                background-image: url(../../App_Themes/Glass/GridView/gvHeaderBackground.gif);
                background-position: center top;
                background-repeat: repeat-x;
                background-color: #C0DDE0;
                overflow: hidden;
                height: 23px;
                line-height: 23px;
                padding: 4px 6px 5px;
                font-weight: normal;
                border: 1px solid #7EACB1;
                /* border-collapse: separate;*/
            }

            .meugrid td {
                overflow: hidden;
                height: 23px;
                line-height: 23px;
                padding: 4px 6px 5px;
                border: 1px solid #7EACB1;
            }

            .meugrid tr td:nth-child(4) {
                text-align: center;
            }

            .meugrid tr td:last-of-type {
                text-align: center;
            }

        .tabrotulos {
            width: 848px;
            margin: auto;
        }

        .materiaprima {
            width: 250px;
        }

            .materiaprima ul {
                margin: 0px;
                padding-left: 10px;
            }

                .materiaprima ul li {
                    max-width: 230px;
                    white-space: pre-wrap;
                }

        .meugrid tr td:last-of-type {
            text-align: center;
        }

        .nome {
            width: 237px;
        }

            .nome div {
                max-width: 230px;
                white-space: pre-wrap;
            }

        .rotulos td {
            font: bold 12px tahoma;
            padding-top: 20px;
            color: rgb(65, 105, 126);
            padding-left: 5px;
            padding-bottom: 5px;
        }

        /*****************************/
        .width150 {
        }

        .height150 {
        }

        .height80 {
        }

        .btn {
            -webkit-border-radius: 7px;
            -moz-border-radius: 7px;
            border-radius: 2px;
            font-family: Arial;
            color: #ffffff;
            font-size: 14px;
            background: #63c269;
            padding: 5px 10px 5px 10px;
            text-decoration: none;
            cursor: pointer;
            border: 0px;
        }

            .btn:hover {
                background: #a4dbab;
                text-decoration: none;
                color: black;
                font-weight: bold;
            }


        .marginTop5porcento {
            margin: 6%;
            font-size: 15px;
        }

        .quadroPrincipal {
            float: left;
            width: 150px;
            height: 155px;
            text-align: center;
            position: relative;
            font-family: monospace;
            border: 1px solid #B1B1B1;
            margin: 0 14px 5px 14px;
            border-radius: 15px;
        }

        .quadroInteriorQtds {
            margin-top: 5%;
            text-align: center;
            font-size: 30px;
        }

        .AlinharAoRodapeCentralizar {
            position: absolute;
            bottom: 5px;
            text-align: center;
            width: 150px;
        }

        .meubotao {
            cursor: pointer;
            font: bold 14px tahoma;
        }
    </style>

    <div class="tituloPaginas" valign="top">
        Criar usuário duplicando permissões
    </div>
    <div>
        <fieldset class="fieldsetatualizarprecos">
            <legend style="font-weight: bold;">Filtro</legend>

            <table class="tabrotulos">
                <tr class="rotulos">

                    <td>Duplicar Permissões de<br />
                        <asp:DropDownList runat="server" ID="ddlUsuarioOrigem" Width="370px" CssClass="campos"></asp:DropDownList>
                    </td>
                    <td>
                        <div style="width: 370px; text-align: left">
                            <br />
                            <asp:Button runat="server" ID="bntBuscar" CssClass="meubotao" Text="Buscar" OnClick="bntBuscar_Click" />
                        </div>
                    </td>
                    <td></td>
                </tr>

                <tr class="rotulos">
                    <td>Usuário novo:<br />
                        <asp:TextBox runat="server" ID="txtusuarionovo"></asp:TextBox>
                    </td>

                    <td>Senha:
                        <br />
                        <asp:TextBox runat="server" ID="txtsenha"></asp:TextBox>
                    </td>

                    <td>

                        <asp:Button runat="server" ID="btnSalvar" CssClass="meubotao" Text="Salvar" OnClick="btnSalvar_Click" />

                    </td>

                </tr>

                <tr class="rotulos">
                    <td>&nbsp;
                    </td>

                    <td>&nbsp;
                    </td>

                    <td></td>

                </tr>
            </table>
        </fieldset>


    </div>
    <div align="center" style="min-height: 500px; float: left; clear: left; width: 834px;">
        <asp:GridView ID="grdpermissoesusuario" CssClass="meugrid" runat="server" Width="500px" DataKeyNames="usuarioId" AutoGenerateColumns="False"
            AllowPaging="True" OnPageIndexChanging="grdpermissoesusuario_PageIndexChanging" PageSize="20">
            <Columns>

                <asp:BoundField DataField="usuarioId"  HeaderText="Id do Usuario" />
                <asp:BoundField DataField="paginaPermitidaNome" HeaderText="paginaPermitida" />

            </Columns>
        </asp:GridView>
    </div>
    <div style="float: left; clear: left; width: 868px; margin-left: 25px; margin-top: 15px;">
        <strong>Quantidade de itens encontrados nesta busca:
        <span style="text-decoration: underline">
            <asp:Label ID="lblitensencontrados" runat="server" Text=""></asp:Label></span>

        </strong>
    </div>
</asp:Content>

