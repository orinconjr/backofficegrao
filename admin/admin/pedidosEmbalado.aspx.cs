﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxGridView;

public partial class admin_pedidosEmbalado : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void gridClientes_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType != GridViewRowType.Data)
        {
            return;
        }

        int pedidoId = (int)grd.GetRowValues(e.VisibleIndex, new string[] { grd.KeyFieldName });

        var itensData = new dbCommerceDataContext();
        var prazos = (from c in itensData.tbItensPedidos
                      where c.pedidoId == pedidoId
                      orderby c.prazoDeFabricacao descending
                      select c);
        if (prazos.Any())
        {
            var prazo = prazos.First();
            var agora = Convert.ToDateTime(DateTime.Now.ToShortDateString()).AddHours(23);
            var doisdias = DateTime.Now.Add(TimeSpan.FromDays(2));

            if (prazo.prazoDeFabricacao <= doisdias && prazo.prazoDeFabricacao >= agora)
            {
                int i = 0;
                while ((i < e.Row.Cells.Count))
                {
                    e.Row.Cells[i].Style.Add("background", "#FDF057");
                    i++;
                }
            }
            if (prazo.prazoDeFabricacao <= agora)
            {
                int i = 0;
                while ((i < e.Row.Cells.Count))
                {
                    e.Row.Cells[i].Style.Add("background", "#f04c4c");
                    i++;
                }
            }
        }
    }



    protected void btPdf_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WritePdfToResponse();
    }
    protected void btXsl_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteXlsToResponse();
    }
    protected void btRtf_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteRtfToResponse();
    }
    protected void btCsv_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteCsvToResponse();
    }
    protected void grd_OnCustomUnboundColumnData(object sender, ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "pedidoIdCliente")
        {
            int pedidoId = Convert.ToInt32(e.GetListSourceFieldValue("pedidoId"));
            e.Value = rnFuncoes.retornaIdCliente(pedidoId);
        }
    }
}