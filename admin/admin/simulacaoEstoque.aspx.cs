﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxGridView;
using System.Transactions;

public partial class admin_simulacaoEstoque : System.Web.UI.Page
{
    public class RetornoSimulacao
    {
        public int produtoId { get; set; }
        public string produtoNome { get; set; }
        public double peso { get; set; }
        public int vendas { get; set; }
        public int estoqueReal { get; set; }
        public int estoqueReservado { get; set; }
        public int encomendas { get; set; }
        public int estoquePrevisto { get; set; }
        public decimal mediaVendasDia { get; set; }
        public int estoqueNecessarioPeriodo { get; set; }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        
            gerarEstoque(false);
        
    }
    
    
    protected void btPdf_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WritePdfToResponse();
    }
    protected void btXsl_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteXlsToResponse();
    }
    protected void btRtf_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteRtfToResponse();
    }
    protected void btCsv_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteCsvToResponse();
    }

    private void gerarEstoque(bool rebind)
    {
        if (!rebind)
        {
            var retorno = new List<RetornoSimulacao>();
            grd.DataSource = retorno.Where(x => x.estoqueNecessarioPeriodo > 0 | x.encomendas > 0 | x.estoquePrevisto > 0 | x.vendas > 0).OrderBy(x => x.produtoNome);
            if ((!Page.IsPostBack && !Page.IsCallback) | rebind) grd.DataBind();
            return;
        }
        try
        {
            using (new TransactionScope(TransactionScopeOption.Required, new TransactionOptions
            {
                IsolationLevel = IsolationLevel.ReadUncommitted
            }))
            {
                using (var data = new dbCommerceDataContext())
                {
                    var retorno = new List<RetornoSimulacao>();
                    var dataFinalSimulacao = Convert.ToDateTime(txtDataInicial.Text);
                    DateTime now = DateTime.Now;
                    TimeSpan ts = dataFinalSimulacao - now;
                    int diasSimulacao = Math.Abs(ts.Days);

                    var dataAntes = DateTime.Now.AddDays(-30);
                    //var data = new dbCommerceDataContext();
                    var produtosCarolina = (from c in data.tbProdutos
                                            where c.produtoFornecedor == 72
                                            select new
                                            {
                                                c.produtoId,
                                                c.produtoNome,
                                                c.produtoPeso
                                            }).Distinct().ToList();
                    var listaItemEstoques = (from c in data.tbItemPedidoEstoques
                                             where
                                                 ((c.dataLimite != null && c.tbItensPedido.tbPedido.dataHoraDoPedido > dataAntes) | (c.enviado == false && c.cancelado == false &&
                                                             c.reservado == false && c.dataLimite != null)) &&
                                                 c.tbProduto.produtoFornecedor == 72
                                             select c).ToList();
                    var produtoEstoques = (from c in data.tbProdutoEstoques
                                           where c.tbProduto.produtoFornecedor == 72 && c.enviado == false
                                           select c).ToList();
                    var fornecedorItens =
                        (from c in data.tbPedidoFornecedorItems
                         where c.tbProduto.produtoFornecedor == 72 && c.entregue == false
                         select c).ToList();

                    foreach (var produto in produtosCarolina)
                    {
                        int vendas = (from c in listaItemEstoques
                                      where
                                c.dataLimite != null && c.tbItensPedido.tbPedido.dataHoraDoPedido > dataAntes &&
                                c.produtoId == produto.produtoId
                                      select c).Count();
                        if (vendas > 0)
                        {
                            var estoqueReal =
                                (from c in produtoEstoques
                                 where c.produtoId == produto.produtoId && c.enviado == false
                                 select c)
                                    .Count();
                            var estoqueReservado =
                                (from c in produtoEstoques
                                 where
                                        c.produtoId == produto.produtoId && c.enviado == false &&
                                        c.pedidoIdReserva != null
                                 select c)
                                    .Count();
                            var aguardandoReserva =
                                (from c in listaItemEstoques
                                 where
                                        c.produtoId == produto.produtoId && c.enviado == false && c.cancelado == false &&
                                        c.reservado == false && c.dataLimite != null
                                 select c)
                                    .Count();
                            var encomendas =
                                (from c in fornecedorItens
                                 where c.idProduto == produto.produtoId && c.entregue == false
                                 select c)
                                    .Count();
                            var estoquePrevisto = (estoqueReal - estoqueReservado - aguardandoReserva + encomendas);


                            int totalNecessario = 0;
                            if (estoquePrevisto < 0) totalNecessario = estoquePrevisto * (-1);


                            decimal mediaVendasDia = 0;
                            if (vendas > 0) mediaVendasDia = Convert.ToDecimal(vendas) / 30;
                            if (mediaVendasDia > 0)
                            {
                                int estoqueNecessarioPeriodo = Convert.ToInt32(mediaVendasDia * diasSimulacao);
                                totalNecessario += estoqueNecessarioPeriodo;
                            }

                            if (estoquePrevisto > 0) totalNecessario = totalNecessario - estoquePrevisto;
                            if (totalNecessario < 0) totalNecessario = 0;
                            var itemRetorno = new RetornoSimulacao()
                            {
                                encomendas = encomendas,
                                estoqueNecessarioPeriodo = totalNecessario,
                                estoquePrevisto = estoquePrevisto,
                                estoqueReal = estoqueReal,
                                estoqueReservado = estoqueReservado,
                                mediaVendasDia = mediaVendasDia,
                                produtoId = produto.produtoId,
                                produtoNome = produto.produtoNome,
                                vendas = vendas,
                                peso = produto.produtoPeso
                            };
                            retorno.Add(itemRetorno);
                        }
                    }
                    grd.DataSource = retorno.Where(x => x.estoqueNecessarioPeriodo > 0 | x.encomendas > 0 | x.estoquePrevisto > 0 | x.vendas > 0).OrderBy(x => x.produtoNome);
                    if ((!Page.IsPostBack && !Page.IsCallback) | rebind) grd.DataBind();
                }
            }
        }
        catch (Exception ex)
        {

        }
    }
    protected void btnGerar_Click(object sender, ImageClickEventArgs e)
    {
        gerarEstoque(true);
    }
}