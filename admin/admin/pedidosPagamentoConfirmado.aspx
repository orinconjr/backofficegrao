﻿<%@ Page Title="" Language="C#" Theme="Glass" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="pedidosPagamentoConfirmado.aspx.cs" Inherits="admin_pedidosPagamentoConfirmado" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxTimer" TagPrefix="dxt" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" Runat="Server">
    <table cellpadding="0" cellspacing="0" style="width: 920px">
    <tr>
        <td class="tituloPaginas" valign="top">
            <asp:Label ID="lblAcao" runat="server">Pedidos Pagamento Confirmado</asp:Label>
        </td>
    </tr>
        
    <tr>
        <td>
            
    <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
        <tr>
            <td>
                &nbsp;</td>
        </tr>

        <tr>
            <td>
                <dxwgv:ASPxGridView DataSourceID="sql" ID="grd" runat="server" AutoGenerateColumns="False" Width="834px" Cursor="auto" KeyFieldName="produtoId">
                    <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                    <Styles>
                        <Footer Font-Bold="True">
                        </Footer>
                    </Styles>
                    <SettingsText ConfirmDelete="Tem certeza que deseja excluir?" emptydatarow="Nenhum registro encontrado." />
                    <SettingsPager Position="TopAndBottom" PageSize="50" ShowDisabledButtons="False" AlwaysShowPager="True">
                        <Summary AllPagesText="Páginas: {0} - {1} ({2} items)" Text="Página {0} de {1} ({2} registros encontrados)" />
                    </SettingsPager>
                    <settings showfilterrow="True" ShowGroupButtons="False" ShowHeaderFilterButton="True" ShowFooter="True" />
                    <SettingsEditing EditFormColumnCount="4" PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1" PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter" PopupEditFormWidth="700px" />
                    <Columns>
                        <dxwgv:GridViewDataTextColumn Caption="ID" FieldName="produtoId" VisibleIndex="0" Width="50">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Nome" FieldName="produtoNome" VisibleIndex="0" Width="30px">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="ID da Empresa" FieldName="produtoIdDaEmpresa" VisibleIndex="0">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Complemento" FieldName="complementoIdDaEmpresa" VisibleIndex="0">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Fornecedor" FieldName="fornecedorNome" VisibleIndex="0">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Quantidade" FieldName="faltando" VisibleIndex="0">
                        </dxwgv:GridViewDataTextColumn>
                    </Columns>
                    <Templates>
                        <DetailRow>
                            <dxwgv:aspxgridview ID="grdDetalhes" runat="server" AutoGenerateColumns="False" Width="100%" OnCustomUnboundColumnData="grd_OnCustomUnboundColumnData"
                                Cursor="auto" EnableCallBacks="False" onbeforeperformdataselect="grdDetalhes_BeforePerformDataSelect" OnInit="grdDetalhes_OnInit">
                                <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True"  />
                                <SettingsText ConfirmDelete="Tem certeza que deseja excluir?" 
                                    emptydatarow="Nenhum registro encontrado." />
                                <SettingsPager Position="TopAndBottom" PageSize="50" 
                                    ShowDisabledButtons="False" AlwaysShowPager="True">
                                    <Summary AllPagesText="Páginas: {0} - {1} ({2} items)" 
                                        Text="Página {0} de {1} ({2} registros encontrados)" />
                                </SettingsPager>
                                <settings showfilterrow="True" ShowGroupButtons="False" />
                                <SettingsEditing EditFormColumnCount="4" 
                                    PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1" 
                                    PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter" 
                                    PopupEditFormWidth="700px" />
                                <Columns>
                                    <dxwgv:GridViewDataTextColumn Caption="Id Cliente" FieldName="pedidoIdCliente" VisibleIndex="0" UnboundType="Integer">
                                        <EditFormSettings Visible="False" />
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Id do Pedido" FieldName="pedidoId" 
                                        VisibleIndex="0">
                                        <EditFormSettings Visible="False" />
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Nome" FieldName="clienteNome" 
                                        VisibleIndex="1" Settings-AutoFilterCondition="Contains">
                                        <PropertiesTextEdit>
                                            <ValidationSettings>
                                                <RequiredField ErrorText="Preencha o nome." IsRequired="True" />
                                            </ValidationSettings>
                                        </PropertiesTextEdit>
                                        <Settings AutoFilterCondition="Contains" />
                                        <EditFormSettings CaptionLocation="Top" />
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Valor" FieldName="valorCobrado" 
                                        VisibleIndex="2" Settings-AutoFilterCondition="Contains" Settings-FilterMode="DisplayText">
                                        <PropertiesTextEdit DisplayFormatString="c">
                                        </PropertiesTextEdit>
                                        <EditFormSettings Visible="False" />
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataDateColumn Caption="Data" FieldName="dataHoraDoPedido" 
                                        UnboundType="DateTime" VisibleIndex="3">
                                        <PropertiesDateEdit DisplayFormatString="" EditFormat="DateTime">
                                            <CalendarProperties ClearButtonText="Apagar" TodayButtonText="Hoje">
                                            </CalendarProperties>
                                        </PropertiesDateEdit>
                                        <Settings GroupInterval="Date" />
                                    </dxwgv:GridViewDataDateColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Pedido Fornecedor" FieldName="idPedidoFornecedor" UnboundType="String" VisibleIndex="3">
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Reserva" FieldName="reserva" UnboundType="String" VisibleIndex="3">
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataHyperLinkColumn Caption="Editar" FieldName="pedidoId" 
                                        VisibleIndex="7" Width="30px">
                                        <PropertiesHyperLinkEdit ImageUrl="~/admin/images/btEditar.jpg" 
                                            NavigateUrlFormatString="pedido.aspx?pedidoId={0}" Target="_blank">
                                        </PropertiesHyperLinkEdit>
                                        <Settings AllowAutoFilter="False" />
                                        <HeaderTemplate>
                                            <img alt="" src="images/legendaEditar.jpg" />
                                        </HeaderTemplate>
                                    </dxwgv:GridViewDataHyperLinkColumn>
                                </Columns>
                                <StylesEditors>
                                    <Label Font-Bold="True">
                                    </Label>
                                </StylesEditors>
                            </dxwgv:aspxgridview>
                        </DetailRow>
                    </Templates>
                    <SettingsDetail AllowOnlyOneMasterRowExpanded="True" ShowDetailRow="True" />
                    <StylesEditors>
                        <Label Font-Bold="True">
                        </Label>
                    </StylesEditors>
                </dxwgv:ASPxGridView>         
                <asp:ObjectDataSource ID="sql" runat="server" SelectMethod="pedidosPagamentoConfirmado" TypeName="rnPedidos">
                </asp:ObjectDataSource>
            </td>
        </tr>
    </table>
        </td>
    </tr>
</table>
</asp:Content>