﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxGridView;
using Amazon.S3.Model;
using Amazon.S3;
using System.Threading;
using System.Threading.Tasks;

public partial class admin_bannerAlt : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.QueryString["bannerId"] == null) Response.Redirect("banners.aspx");
            categoriabox.Visible = false;
            produtobox.Visible = false;

            using (var data = new dbCommerceDataContext())
            {
                int idBanner = Convert.ToInt32(Request.QueryString["bannerId"]);

                var banner = (from b in data.tbBanners where b.Id == idBanner select b).FirstOrDefault();

                if (banner == null)
                    Response.Redirect("banners.aspx");

                imgBannerAtual.ImageUrl = banner.local == 1 ? ConfigurationManager.AppSettings["caminhoCDN"] + "banners\\home\\" + banner.foto : ConfigurationManager.AppSettings["caminhoCDN"] + "banners\\" + banner.foto;
                txtLinkProduto.Text = banner.linkProduto;
                txtIdProduto.Text = banner.idProduto.ToString();
                ddlLocal.SelectedValue = banner.local.ToString();
                localSelecionado();
                ddlPosicao.SelectedValue = banner.posicao.ToString();
                ckbAtivo.Checked = Convert.ToBoolean(banner.ativo);
                ckbBannerMobile.Checked = Convert.ToBoolean(banner.mobile);
                txtRelevancia.Text = Convert.ToDecimal(banner.relevancia).ToString("#0.0") ?? "";

                var estilo = (from c in data.tbEstilos where c.id == banner.estilo orderby c.nome select c).FirstOrDefault();
                if(estilo != null)
                {         
                    ddlLayoutContador.SelectedValue = banner.estilo.ToString();
                }

                ckbContador.Checked = banner.inicioContador != null;
                txtInicioContadorData.Text = banner.inicioContador != null ? banner.inicioContador.Value.Date.ToString("d") : "";
                txtInicioContadorHora.Text = banner.inicioContador != null ? banner.inicioContador.Value.ToString("t") : "00:00";
                txtFimContadorData.Text = banner.fimContador != null ? banner.fimContador.Value.Date.ToString("d") : "";
                txtFimContadorHora.Text = banner.fimContador != null ? banner.fimContador.Value.ToString("t") : "00:00";
                //btnExcluir.OnClientClick = "return confirm('Deseja realmente excluir o banner?\\n" + banner.foto + "')";

                carregaFiltrosBanner(data,banner.Id);
            }


            txtAgendamentoDataAtivacao.Text = DateTime.Now.Date.ToString("d");
            txtAgendamentoHoraAtivacao.Text = DateTime.Now.AddMinutes(20).ToString("t");

            txtAgendamentoDataDesativacao.Text = DateTime.Now.AddDays(1).ToString("d");
            txtAgendamentoHoraDesativacao.Text = "00:00";


        }

        carregaAgendamentos(false);  
    }

    private void carregaFiltrosBanner(dbCommerceDataContext data, int bannerId)
    {
        #region getBannerFiltros
        var getRelacaoFiltro = (from c in data.tbBannersFiltros where c.bannerId == bannerId select c.filtro).ToList();

        var getCategoriasFiltro = (from c in data.tbProdutoCategorias where getRelacaoFiltro.Contains(c.categoriaId) select c);
        var getProdutosFiltro = (from c in data.tbProdutos where getRelacaoFiltro.Contains(c.produtoId) select c);
        var script = "categoriasEscolhidas.length=0;produtosEscolhidos.length=0;";
        hdnCategorias.Value = "0";
        hdnProdutos.Value = "0";

        foreach (var filtroCategorias in getCategoriasFiltro)
        {
            hdnCategorias.Value += "," + filtroCategorias.categoriaId;
            script += "categoriasEscolhidas.push({ id:" + filtroCategorias.categoriaId + ", name: '" + filtroCategorias.categoriaNome + "' });";
        }

        foreach (var filtroProdutos in getProdutosFiltro)
        {
            hdnProdutos.Value += "," + filtroProdutos.produtoId;
            script += "produtosEscolhidos.push({ id:" + filtroProdutos.produtoId + ", name: '" + filtroProdutos.produtoId + " - " + filtroProdutos.produtoNome + "' });";
        }

        hdnCategorias.Value = hdnCategorias.Value.Replace("0,", "");
        hdnProdutos.Value = hdnProdutos.Value.Replace("0,", "");

        int local = Convert.ToInt32(ddlLocal.SelectedValue);
        switch (local)
        {
            case 4:
                ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), script + "renderCategoriesTable();", true);
                break;
            case 2:
                ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), script + "renderProductsTable();", true);
                break;
        }


        #endregion
    }

    private void carregaAgendamentos(bool rebind)
    {
        var data = new dbCommerceDataContext();
        int idBanner = Convert.ToInt32(Request.QueryString["bannerId"]);
        var agendamentos = (from c in data.tbBannerAgendamentos where c.idBanner == idBanner select c);
        grd.DataSource = agendamentos;
        if((!Page.IsPostBack && !Page.IsPostBack) | rebind)
        {
            grd.DataBind();
        }
    }

    protected void grd_OnCustomUnboundColumnData(object sender, ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "tempoAtivacao")
        {
            DateTime agendamento = Convert.ToDateTime(e.GetListSourceFieldValue("agendamento"));
            var tempoParaAtivacao = agendamento.TimeOfDay - DateTime.Now.TimeOfDay;
            e.Value = tempoParaAtivacao.Hours > 0 ? tempoParaAtivacao.ToString() : "";


        }
    }

    protected void grd_OnHtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType != GridViewRowType.Data) return;

        /*Label lblTempoParaAtivacao = grd.FindRowCellTemplateControl(e.VisibleIndex, null, "lblTempoParaAtivacao") as Label;
        Label lblAgendamento = grd.FindRowCellTemplateControl(e.VisibleIndex, null, "lblAgendamento") as Label;
        Label lblConcluido = grd.FindRowCellTemplateControl(e.VisibleIndex, null, "lblConcluido") as Label;

        var ativarCountDown = Convert.ToBoolean(lblConcluido.Text);
        var dataAgendamento = Convert.ToDateTime(lblAgendamento.Text);

        if (!ativarCountDown && dataAgendamento.ToOADate() > DateTime.Now.ToOADate())
        {
            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "setTimer('" + lblTempoParaAtivacao.ClientID + "','" + dataAgendamento.ToString("yyyy-MM-dd HH:mm:ss") + "');", true);
        }
        else
        {
            lblTempoParaAtivacao.Text = "Concluído";
        }*/

    }
    protected void btnSalvar_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            int idBanner = Convert.ToInt32(Request.QueryString["bannerId"]);
            string bannerId = "";
            tbBanner banner;
            using (var data = new dbCommerceDataContext())
            {
                banner = (from b in data.tbBanners where b.Id == idBanner select b).FirstOrDefault();

                if (banner == null)
                    return;

                if (banner.ativo != ckbAtivo.Checked)
                {
                    var log = new rnLog { usuario = rnUsuarios.retornaNomeUsuarioLogado() };
                    log.descricoes.Add(ckbAtivo.Checked ? "Ativação manual do banner id: " + idBanner : "Desativação manual do banner id: " + idBanner);
                    log.tiposRelacionados.Add(new rnEnums.TipoRelacionadoIds { idRegistroRelacionado = idBanner, tipoRelacionado = rnEnums.TipoRegistroRelacionado.Banner });
                    log.tiposOperacao.Add(rnEnums.TipoOperacao.Banner);
                    log.InsereLog();
                }

                banner.linkProduto = txtLinkProduto.Text;
                banner.idProduto = txtIdProduto.Text != "" ? Convert.ToInt32(txtIdProduto.Text) : 0;
                banner.local = Convert.ToInt32(ddlLocal.SelectedValue);
                banner.posicao = Convert.ToInt32(ddlPosicao.SelectedValue);
                banner.estilo = Convert.ToInt32(ddlLayoutContador.SelectedValue);

                if (FileUploadBanner.HasFile)
                    banner.foto = idBanner + FileUploadBanner.FileName;

                banner.ativo = false;
                banner.mobile = ckbBannerMobile.Checked;

                banner.relevancia = string.IsNullOrEmpty(txtRelevancia.Text) ? 1 : Convert.ToDecimal(txtRelevancia.Text);

                data.SubmitChanges();

                bannerId = banner.Id.ToString();

                imgBannerAtual.ImageUrl = banner.local == 1 ? ConfigurationManager.AppSettings["caminhoCDN"] + "banners\\home\\" + banner.foto : ConfigurationManager.AppSettings["caminhoCDN"] + "banners\\" + banner.foto;

                #region CadastrarFiltros
                var deletaRelacaoFiltro = (from c in data.tbBannersFiltros where c.bannerId == idBanner select c);
                foreach (var bannerFiltro in deletaRelacaoFiltro)
                {
                    data.tbBannersFiltros.DeleteOnSubmit(bannerFiltro);
                    data.SubmitChanges();
                }

                List<string> listFiltros = new List<string>();
                var produtos = hdnProdutos.Value.Split(new char[] { ',' });
                var categorias = hdnCategorias.Value.Split(new char[] { ',' });

                listFiltros.AddRange(produtos);
                listFiltros.AddRange(categorias);

                foreach (var idFiltro in listFiltros)
                {
                    if (idFiltro!="0") {
                        var bnFiltro = new tbBannersFiltro()
                        {
                            bannerId = banner.Id,
                            filtro = int.Parse(idFiltro),
                            dataCadastro = DateTime.Now
                        };

                        data.tbBannersFiltros.InsertOnSubmit(bnFiltro);
                        data.SubmitChanges();
                    }
                }
                #endregion
            }
            if (FileUploadBanner.HasFile)
            {
                string caminhoBannerLocalParcial = (banner.local == 1 ? "banners\\home\\" : "banners\\");
                string caminhoBannerRemotoParcial = (banner.local == 1 ? "banners/home/" : "banners/");
                string caminhoBanner = ConfigurationManager.AppSettings["caminhoFisico"] + caminhoBannerLocalParcial;
                string nomeArquivoOriginal = banner.Id + FileUploadBanner.FileName;
                string nomeArquivoWebp = banner.Id + FileUploadBanner.FileName.Split('.')[0] + ".webp";

                FileUploadBanner.PostedFile.SaveAs(caminhoBanner + "original_" + nomeArquivoOriginal);
                FileUploadBanner.PostedFile.SaveAs(caminhoBanner + nomeArquivoOriginal);

                var tinify = new Tinify();
                //var fotoOtimizada = tinify.Shrink(caminhoBanner + "original_" + nomeArquivoOriginal, caminhoBanner + nomeArquivoOriginal);
                uploadImagemAmazonS3(caminhoBannerLocalParcial, caminhoBannerRemotoParcial, nomeArquivoOriginal);

                //uploadImagemAmazonS3(caminhoBannerLocalParcial, caminhoBannerRemotoParcial, nomeArquivoOriginal);

                bool webpCarregado = true;
                try
                {
                    Imazen.WebP.Extern.LoadLibrary.LoadWebPOrFail();
                }
                catch (System.Exception ex)
                {
                    webpCarregado = false;
                }

                if (webpCarregado)
                {
                    System.Drawing.Bitmap mBitmap;
                    FileStream outStream = new FileStream(caminhoBanner + nomeArquivoWebp, FileMode.Create);
                    using (Stream BitmapStream = System.IO.File.Open(caminhoBanner + nomeArquivoOriginal, System.IO.FileMode.Open))
                    {
                        System.Drawing.Image img = System.Drawing.Image.FromStream(BitmapStream);

                        mBitmap = new System.Drawing.Bitmap(img);
                        var encoder = new Imazen.WebP.SimpleEncoder();
                        encoder.Encode(mBitmap, outStream, 90);
                        outStream.Close();
                        uploadImagemAmazonS3(caminhoBannerLocalParcial, caminhoBannerRemotoParcial, nomeArquivoWebp);
                    }
                }                
            }

            using (var data = new dbCommerceDataContext())
            {
                var ativarBanner = (from c in data.tbBanners where c.Id == idBanner select c).First();
                ativarBanner.ativo = ckbAtivo.Checked;
                data.SubmitChanges();
            }


            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('Banner alterado com sucesso!');", true);
            localSelecionado();
        }
        catch (System.Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('Erro não foi possivel realizar a alteração.\\n" + ex.StackTrace + "');", true);
            //Response.Write(ex.Message);
        }

    }
    protected void btnExcluir_Click(object sender, ImageClickEventArgs e)
    {
        int idBanner = Convert.ToInt32(Request.QueryString["bannerId"]);
        string caminho = ddlLocal.SelectedValue == "1"
             ? ConfigurationManager.AppSettings["caminhoFisico"] + "banners\\home\\"
             : ConfigurationManager.AppSettings["caminhoFisico"] + "banners\\";

        try
        {
            using (var data = new dbCommerceDataContext())
            {
                var banner = (from b in data.tbBanners where b.Id == idBanner select b).First();
                //string caminhoBanner = Server.MapPath((Convert.ToInt32(ddlLocal.SelectedValue) == 1 ? ConfigurationManager.AppSettings["caminhoFisico"] + "banners/home/" : ConfigurationManager.AppSettings["caminhoFisico"] + "banners/") + banner.foto);

                //if (System.IO.File.Exists(caminhoBanner))
                //System.IO.File.Delete(caminhoBanner);

                if (File.Exists(caminho + banner.foto))
                    File.Delete(caminho + banner.foto);

                data.tbBanners.DeleteOnSubmit(banner);
                data.SubmitChanges();

            }
            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('Banner deletado!');", true);

        }
        catch (System.Exception)
        {

            throw;
        }

    }
    


    private void CarregarEstilos(int idEstilo)
    {
        try
        {
            using (var data = new dbCommerceDataContext())
            {

                var dados = (from c in data.tbEstilos where c.idTipoEstilo == idEstilo orderby c.nome select c).ToList();
                ddlLayoutContador.DataSource = dados;
                ddlLayoutContador.DataBind();
            }
        }
        catch (System.Exception ex)
        {

        }
    }


    private void uploadImagemAmazonS3(string caminhoLocal, string caminhoRemoto, string foto)
    {

        string s3ServiceUrl = "s3-sa-east-1.amazonaws.com";
        string bucketName = "cdn2.graodegente.com.br";
        string filePath = ConfigurationManager.AppSettings["caminhoFisico"] + caminhoLocal + foto;
        string fileContentType = "image/jpg";
        S3CannedACL fileCannedACL = S3CannedACL.PublicReadWrite;

        string accessKey = "AKIAIP7IE6WVNK2K6TIQ";
        string secretAccessKey = "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";

        try
        {
            AmazonS3Config config = new AmazonS3Config();
            config.ServiceURL = s3ServiceUrl;
            config.CommunicationProtocol = Protocol.HTTP;

            using (var s3Client = Amazon.AWSClientFactory.CreateAmazonS3Client(accessKey, secretAccessKey, config))
            {
                PutObjectRequest request = new PutObjectRequest
                {
                    BucketName = bucketName,
                    CannedACL = fileCannedACL,
                    Key = caminhoRemoto + foto,
                    FilePath = filePath,
                    ContentType = fileContentType
                };

                request.AddHeader("expires", "Thu, 21 Mar 2042 08:16:32 GMT");
                PutObjectResponse response = s3Client.PutObject(request);

            }

        }
        catch (AmazonS3Exception s3Exception)
        {
            Console.WriteLine(s3Exception.Message, s3Exception.InnerException);

            Console.ReadKey();
        }
    }

    protected void ddlLocal_SelectedIndexChanged(object sender, EventArgs e)
    {
        localSelecionado();

    }

    private void localSelecionado()
    {
        categoriabox.Visible = false;
        produtobox.Visible = false;
        int local = Convert.ToInt32(ddlLocal.SelectedValue);

        if (local == 2 || local == 4)
        {
            using (var data = new dbCommerceDataContext())
            {
                carregaFiltrosBanner(data, int.Parse(Request["bannerId"]));
            }
        }

        switch (local)
        {
            case 1:
                CarregarEstilos(1);
                break;
            case 2:
                produtobox.Visible = true;
                CarregarEstilos(5);
                ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "initBuscaProduto();", true);
                break;
            case 4:
                CarregarEstilos(2);
                categoriabox.Visible = true;
                ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "getAllCategories();initBuscaCategoria();", true);
                break;
            default:
                CarregarEstilos(2);            
                break;
        }

    }

    protected void btnAdicionarAgendamento_Click(object sender, EventArgs e)
    {
        int idBanner = Convert.ToInt32(Request.QueryString["bannerId"]);
        if (ckbAgendamentoAtivacao.Checked | ckbAgendamentoDesativacao.Checked | ckbContador.Checked)
        {
            var data = new dbCommerceDataContext();
            var log = new rnLog { usuario = rnUsuarios.retornaNomeUsuarioLogado() };
            log.tiposRelacionados.Add(new rnEnums.TipoRelacionadoIds() { idRegistroRelacionado = idBanner, tipoRelacionado = rnEnums.TipoRegistroRelacionado.Banner });
            log.tiposOperacao.Add(rnEnums.TipoOperacao.Banner);
            log.InsereLog();


            var agendamento = new tbBannerAgendamento();
            agendamento.idBanner = idBanner;
            agendamento.queueConcluidoEntrada = false;
            agendamento.queueConcluidoSaida = false;
            agendamento.queueConcluidoContador = false;

            #region Agendamento do Banner

            if (ckbAgendamentoAtivacao.Checked)
            {
                DateTime dataAgendamentoAtivacao = Convert.ToDateTime(Convert.ToDateTime(txtAgendamentoDataAtivacao.Text).ToString("yyyy-MM-dd") + " " + Convert.ToDateTime(txtAgendamentoHoraAtivacao.Text).TimeOfDay);
                agendamento.dataEntrada = dataAgendamentoAtivacao;


                log = new rnLog { usuario = rnUsuarios.retornaNomeUsuarioLogado() };
                log.descricoes.Add("Ativação agendada - " + dataAgendamentoAtivacao.ToString("G") + " - do banner id: " + idBanner);
                log.tiposOperacao.Add(rnEnums.TipoOperacao.Banner);
                log.InsereLog();

            }

            if (ckbAgendamentoDesativacao.Checked)
            {
                DateTime dataAgendamentoDesativacao = Convert.ToDateTime(Convert.ToDateTime(txtAgendamentoDataDesativacao.Text).ToString("yyyy-MM-dd") + " " + Convert.ToDateTime(txtAgendamentoHoraDesativacao.Text).TimeOfDay);
                agendamento.dataSaida = dataAgendamentoDesativacao;


                log = new rnLog { usuario = rnUsuarios.retornaNomeUsuarioLogado() };
                log.descricoes.Add("Desativação agendada - " + dataAgendamentoDesativacao.ToString("G") + " - do banner id: " + idBanner);
                log.tiposOperacao.Add(rnEnums.TipoOperacao.Banner);
                log.InsereLog();
            }

            #endregion Agendamento do Banner

            #region Contador no Banner

            if (ckbContador.Checked)
            {
                var dataInicioContador = Convert.ToDateTime(Convert.ToDateTime(txtInicioContadorData.Text).ToString("yyyy-MM-dd") + " " + Convert.ToDateTime(txtInicioContadorHora.Text).TimeOfDay);
                var dataFimContador = Convert.ToDateTime(Convert.ToDateTime(txtFimContadorData.Text).ToString("yyyy-MM-dd") + " " + Convert.ToDateTime(txtFimContadorHora.Text).TimeOfDay);

                agendamento.inicioContador = dataInicioContador;
                agendamento.fimContador = dataFimContador;
                

                log = new rnLog { usuario = rnUsuarios.retornaNomeUsuarioLogado() };
                log.descricoes.Add("Contador inicio - " + dataInicioContador.ToString("G") + " fim - " + dataFimContador.ToString("G") + " - no banner id: " + idBanner);
                log.tiposOperacao.Add(rnEnums.TipoOperacao.Banner);
                log.InsereLog();
                #endregion Contador no Banner
            }

            if (!string.IsNullOrEmpty(txtOrdemAgendamento.Text))
            {
                agendamento.posicao = Convert.ToInt32(txtOrdemAgendamento.Text);
            }

            data.tbBannerAgendamentos.InsertOnSubmit(agendamento);
            data.SubmitChanges();

            carregaAgendamentos(true);
        }

    }

    protected void btExcluir_Command(object sender, CommandEventArgs e)
    {
        int idBannerAgendamento = Convert.ToInt32(e.CommandArgument);
        var data = new dbCommerceDataContext();
        var agendamento = (from c in data.tbBannerAgendamentos where c.idBannerAgendamento == idBannerAgendamento select c).FirstOrDefault();
        if(agendamento != null)
        {
            if (agendamento.queueConcluidoEntrada == false | agendamento.queueConcluidoContador | agendamento.queueConcluidoSaida)
            {
                data.tbBannerAgendamentos.DeleteOnSubmit(agendamento);
                data.SubmitChanges();
            }
        }
        carregaAgendamentos(true);
    }
}