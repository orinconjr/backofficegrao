﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_unificarcadastro : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
        usuarioLogadoId = Request.Cookies["usuarioLogadoId"];
        if (usuarioLogadoId != null)
        {
            fillDrops();
            fillProdutos();
            //fillProdutosUnificados();
        }
        else
        {
            Response.Redirect("default.aspx");
        }
    }


    private void fillProdutosUnificados()
    {
        int pagina = Convert.ToInt32(hdfPaginaAtual.Value);
        lblPagina.Text = "Página " + pagina;
        int take = 30;
        int skip = (take * (pagina - 1));
        var data = new dbCommerceDataContext();
        var produtos = (from d in data.tbProdutos
                        join f in data.tbJuncaoProdutoCategorias on d.produtoId equals f.produtoId into filtros
                        where (d.unificacaoProdutoId != null && d.unificacaoProdutoId == d.produtoId)
                        select new
                        {
                            d.produtoId,
                            d.produtoNome,
                            d.fotoDestaque,
                            d.produtoFornecedor,
                            d.produtoMarca,
                            d.produtoUrl,
                            idCor = filtros.FirstOrDefault(x => x.tbProdutoCategoria.categoriaPaiId == 723) == null ? 0 : filtros.FirstOrDefault(x => x.tbProdutoCategoria.categoriaPaiId == 723).categoriaId
                        }).Distinct().Skip(skip).Take(take);
        lstProdutosUnificados.DataSource = produtos.OrderBy(x => x.idCor).ThenBy(x => x.produtoMarca).ThenBy(x => x.produtoFornecedor);
        lstProdutosUnificados.DataBind();

    }

    private void fillProdutos()
    {
        if (hdfUnificados.Value == "0")
        {
            int pagina = Convert.ToInt32(hdfPaginaAtual.Value);
            lblPagina.Text = "Página " + pagina;
            int take = 200;
            int skip = (take * (pagina - 1));
            var data = new dbCommerceDataContext();
            var produtos = (from c in data.tbJuncaoProdutoCategorias
                            join d in data.tbProdutos on c.produtoId equals d.produtoId
                            join f in data.tbJuncaoProdutoCategorias on d.produtoId equals f.produtoId into filtros
                            where c.categoriaId == 0 && d.produtoAtivo == "True" && d.ocultarLista == false && (d.unificacaoProdutoId == null | d.unificacaoProdutoId == c.produtoId)
                            select new
                            {
                                d.produtoId,
                                d.produtoNome,
                                d.fotoDestaque,
                                d.produtoFornecedor,
                                d.produtoMarca,
                                d.produtoUrl,
                                idCor = filtros.FirstOrDefault(x => x.tbProdutoCategoria.categoriaPaiId == 723) == null ? 0 : filtros.FirstOrDefault(x => x.tbProdutoCategoria.categoriaPaiId == 723).categoriaId
                            }).Distinct();
            if (ddlCategoria.SelectedItem != null)
            {
                int categoriaId = Convert.ToInt32(ddlCategoria.SelectedItem.Value);
                produtos = (from c in data.tbJuncaoProdutoCategorias
                            join d in data.tbProdutos on c.produtoId equals d.produtoId
                            join f in data.tbJuncaoProdutoCategorias on d.produtoId equals f.produtoId into filtros
                            where c.categoriaId == categoriaId && d.produtoAtivo == "True" && d.ocultarLista == false && (d.unificacaoProdutoId == null | d.unificacaoProdutoId == c.produtoId)
                            select new
                            {
                                d.produtoId,
                                d.produtoNome,
                                d.fotoDestaque,
                                d.produtoFornecedor,
                                d.produtoMarca,
                                d.produtoUrl,
                                idCor = filtros.FirstOrDefault(x => x.tbProdutoCategoria.categoriaPaiId == 723) == null ? 0 : filtros.FirstOrDefault(x => x.tbProdutoCategoria.categoriaPaiId == 723).categoriaId
                            }).Distinct();

            }
            else
            {
                produtos = (from c in data.tbJuncaoProdutoCategorias
                            join d in data.tbProdutos on c.produtoId equals d.produtoId
                            join f in data.tbJuncaoProdutoCategorias on d.produtoId equals f.produtoId into filtros
                            where d.produtoAtivo == "True" && d.ocultarLista == false && (d.unificacaoProdutoId == null | d.unificacaoProdutoId == c.produtoId)
                            select new
                            {
                                d.produtoId,
                                d.produtoNome,
                                d.fotoDestaque,
                                d.produtoFornecedor,
                                d.produtoMarca,
                                d.produtoUrl,
                                idCor = filtros.FirstOrDefault(x => x.tbProdutoCategoria.categoriaPaiId == 723) == null ? 0 : filtros.FirstOrDefault(x => x.tbProdutoCategoria.categoriaPaiId == 723).categoriaId
                            }).Distinct();
            }
            if (ddlFornecedor.SelectedItem != null)
            {
                produtos = produtos.Where(x => x.produtoFornecedor == Convert.ToInt32(ddlFornecedor.SelectedItem.Value));
            }
            if (ddlCor.SelectedItem != null)
            {
                produtos = produtos.Where(x => x.idCor == Convert.ToInt32(ddlCor.SelectedItem.Value));
            }
            lstProdutos.DataSource = produtos.OrderBy(x => x.idCor).ThenBy(x => x.produtoMarca).ThenBy(x => x.produtoFornecedor).Skip(skip).Take(take);
            lstProdutos.DataBind();
        }
    }
    private void fillDrops()
    {
        if (!Page.IsPostBack && !Page.IsCallback)
        {
            var data = new dbCommerceDataContext();
            var categorias = (from c in data.tbProdutoCategorias where c.categoriaPaiId == 0 && c.exibirSite == true orderby c.categoriaNome select c);

            var fornecedor = (from c in
                                  data.tbProdutoFornecedors
                              join d in data.tbProdutos on c.fornecedorId equals d.produtoFornecedor
                              where d.produtoAtivo == "True" && d.tbProdutoFornecedor != null
                              orderby c.fornecedorNome
                              select c).Distinct().OrderBy(x => x.fornecedorNome);

            var cores = (from c in
                                  data.tbProdutoCategorias
                         where c.categoriaPaiId == 723
                         orderby c.categoriaNome
                         select c).Distinct().OrderBy(x => x.categoriaNome);

            ddlFornecedor.DataSource = fornecedor;
            ddlCategoria.DataSource = categorias;
            ddlCor.DataSource = cores;
            ddlCategoria.DataBind();
            ddlFornecedor.DataBind();
            ddlCor.DataBind();
        }

    }

    protected void conteudo_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        if (e.Parameter == null) return;

        if (e.Parameter == "gravar")
        {
            int produtoId = 0;
            foreach (var item in lstProdutos.Items)
            {
                var chk = (CheckBox)item.FindControl("chkProduto");
                if (chk.Checked)
                {
                    var hdfProdutoId = (HiddenField)item.FindControl("hdfProdutoId");
                    var produtoIdUnificar = Convert.ToInt32(hdfProdutoId.Value);
                    if (produtoId == 0) produtoId = produtoIdUnificar;
                    var data = new dbCommerceDataContext();
                    var produto = (from c in data.tbProdutos where c.produtoId == produtoIdUnificar select c).FirstOrDefault();
                    if (produto != null)
                    {
                        produto.unificacaoProdutoId = produtoId;
                        data.SubmitChanges();
                    }
                }
            }

            fillProdutos();
        }
        if (e.Parameter == "exibir")
        {
            if (lstProdutos.Visible == true)
            {
                hdfUnificados.Value = "1";
                lstProdutos.Visible = false;
                lstProdutosUnificados.Visible = true;
                btnProximaPagina.Visible = true;
                hdfPaginaAtual.Value = "1";
                fillProdutosUnificados();
            }
            else
            {
                hdfUnificados.Value = "0";
                hdfPaginaAtual.Value = "0";
                lstProdutos.Visible = true;
                hdfPaginaAtual.Value = "1";
                lstProdutosUnificados.Visible = false;
            }
        }
        if (e.Parameter == "ocultar")
        {

            hdfUnificados.Value = "0";
            hdfPaginaAtual.Value = "0";
            lstProdutos.Visible = true;
            hdfPaginaAtual.Value = "1";
            lstProdutosUnificados.Visible = false;
            fillProdutos();
            
        }
        if (e.Parameter.Contains("principal_"))
        {
            lstProdutos.Visible = false;
            lstProdutosUnificados.Visible = true;
            int produtoid = Convert.ToInt32(e.Parameter.Split('_')[1]);
            var data = new dbCommerceDataContext();
            var produto = (from c in data.tbProdutos where c.produtoId == produtoid select c).First();
            var produtos = (from c in data.tbProdutos where c.unificacaoProdutoId == produto.unificacaoProdutoId select c).ToList();
            produto.unificacaoProdutoId = produtoid;
            data.SubmitChanges();
            foreach (var produtoalt in produtos)
            {
                produtoalt.unificacaoProdutoId = produtoid;
                data.SubmitChanges();
            }

            fillProdutosUnificados();
        }
        if (e.Parameter.Contains("remover_"))
        {
            lstProdutos.Visible = false;
            lstProdutosUnificados.Visible = true;
            int produtoid = Convert.ToInt32(e.Parameter.Split('_')[1]);
            var data = new dbCommerceDataContext();
            var produto = (from c in data.tbProdutos where c.produtoId == produtoid select c).First();
            var produtos = (from c in data.tbProdutos where c.unificacaoProdutoId == produto.unificacaoProdutoId select c).ToList();
            produto.unificacaoProdutoId = null;
            data.SubmitChanges();

            fillProdutosUnificados();
        }
        if (e.Parameter.Contains("proximapaginaunificados"))
        {
            int paginaAtual = Convert.ToInt32(hdfPaginaAtual.Value);
            paginaAtual = paginaAtual + 1;
            hdfPaginaAtual.Value = paginaAtual.ToString();
            lstProdutos.Visible = false;
            lstProdutosUnificados.Visible = true;
            fillProdutosUnificados();
        }
        if (e.Parameter.Contains("proximapagina"))
        {
            int paginaAtual = Convert.ToInt32(hdfPaginaAtual.Value);
            paginaAtual = paginaAtual + 1;
            hdfPaginaAtual.Value = paginaAtual.ToString();
            fillProdutos();
        }
    }

    protected void ddlFornecedor_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        if (e.Parameter == null) return;
        int categoriaId = Convert.ToInt32(e.Parameter);
        var data = new dbCommerceDataContext();
        var fornecedor = (from c in data.tbJuncaoProdutoCategorias
                          join d in data.tbProdutos on c.produtoId equals d.produtoId
                          join f in data.tbProdutoFornecedors on d.produtoFornecedor equals f.fornecedorId
                          where d.produtoAtivo == "True" && d.tbProdutoFornecedor != null && c.categoriaId == categoriaId
                          orderby f.fornecedorNome
                          select new
                          {
                              f.fornecedorNome,
                              f.fornecedorId
                          }).Distinct().OrderBy(x => x.fornecedorNome).ToList();
        ddlFornecedor.DataSource = fornecedor;
        ddlFornecedor.DataBind();
    }

    protected void ddlCor_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        if (e.Parameter == null) return;
        int categoriaId = Convert.ToInt32(e.Parameter);
        var data = new dbCommerceDataContext();
        var cores = (from c in data.tbJuncaoProdutoCategorias
                     join d in data.tbProdutos on c.produtoId equals d.produtoId
                     join f in data.tbJuncaoProdutoCategorias on d.produtoId equals f.produtoId
                     join g in data.tbProdutoCategorias on f.categoriaId equals g.categoriaId
                     where d.produtoAtivo == "True" && d.tbProdutoFornecedor != null && c.categoriaId == categoriaId
                     && g.categoriaPaiId == 723
                     orderby g.categoriaNome
                     select new
                     {
                         g.categoriaNome,
                         g.categoriaId
                     }).Distinct().OrderBy(x => x.categoriaNome).ToList();
        ddlCor.DataSource = cores;
        ddlCor.DataBind();
    }


    protected void lstProdutosUnificados_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        var hdfProdutoId = (HiddenField)e.Item.FindControl("hdfProdutoId");
        var lstProdutosUnificadosDetalhe = (ListView)e.Item.FindControl("lstProdutosUnificadosDetalhe");
        int produtoId = Convert.ToInt32(hdfProdutoId.Value);

        var data = new dbCommerceDataContext();
        var produtos = (from d in data.tbProdutos
                        join f in data.tbJuncaoProdutoCategorias on d.produtoId equals f.produtoId into filtros
                        where (d.unificacaoProdutoId == produtoId) 
                        select new
                        {
                            d.produtoId,
                            d.produtoNome,
                            d.fotoDestaque,
                            d.produtoFornecedor,
                            d.produtoMarca,
                            d.produtoUrl,
                            idCor = filtros.FirstOrDefault(x => x.tbProdutoCategoria.categoriaPaiId == 723) == null ? 0 : filtros.FirstOrDefault(x => x.tbProdutoCategoria.categoriaPaiId == 723).categoriaId
                        }).Distinct();
        if (produtos.Count() > 1) produtos = produtos.Where(x => x.produtoId != produtoId);
        lstProdutosUnificadosDetalhe.DataSource = produtos.OrderBy(x => x.idCor).ThenBy(x => x.produtoMarca).ThenBy(x => x.produtoFornecedor);
        lstProdutosUnificadosDetalhe.DataBind();
    }

    protected void btnTornarPrincipal_Command(object sender, CommandEventArgs e)
    {

    }

    protected void btnRemover_Command(object sender, CommandEventArgs e)
    {
        int produtoid = Convert.ToInt32(e.CommandArgument);
        var data = new dbCommerceDataContext();
        var produto = (from c in data.tbProdutos where c.produtoId == produtoid select c).First();
        var produtos = (from c in data.tbProdutos where c.unificacaoProdutoId == produto.unificacaoProdutoId select c).ToList();
        produto.unificacaoProdutoId = null;
        data.SubmitChanges();

        fillProdutosUnificados();
    }
}