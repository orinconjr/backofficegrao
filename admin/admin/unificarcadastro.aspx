﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="unificarcadastro.aspx.cs" Inherits="admin_unificarcadastro" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v11.1" Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v11.1" Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dxe" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript">
        var unificados = false;
    </script>
</head>
    
<body>
    <form id="form1" runat="server">
        <div>
            <div style="position: fixed; top: 0; left: 0; height: 50px; width: 100%; background: #CECECE; text-align: right;">     
                <div style="float: right; padding-right: 50px; padding-top: 7px;">
                    <dxe:ASPxButton ID="btnGravar" runat="server" AutoPostBack="False" Text="UNIFICAR CADASTROS" ClientInstanceName="gravar" Font-Size="20px">
                    <ClientSideEvents 
                        Click="function(s, e) {
                            conteudoProdutos.PerformCallback('gravar');
                        }" />
                    </dxe:ASPxButton>                
                </div>  
                <div style="float: right; padding-right: 50px; padding-top: 7px;">
                    <dxe:ASPxButton ID="btnExibirUnificados" runat="server" AutoPostBack="False" Text="Exibir Unificados" ClientInstanceName="exibirunificados" Font-Size="20px">
                    <ClientSideEvents 
                        Click="function(s, e) {
                        if(unificados == false){
                            conteudoProdutos.PerformCallback('exibir');
                            exibirunificados.SetText('Ocultar Unificados');
                            unificados = true;
                        }
                        else
                        {
                            conteudoProdutos.PerformCallback('ocultar');
                            exibirunificados.SetText('Exibir Unificados');
                            unificados = false;
                        }
                        }" />
                    </dxe:ASPxButton>                
                </div> 
            </div>
            <div style="overflow:auto;">
                <dxe:ASPxCallbackPanel runat="server" ID="conteudo" ClientInstanceName="conteudoProdutos" RenderMode="Div" OnCallback="conteudo_Callback">
                    <PanelCollection>
                        <dxe:PanelContent>
                            <asp:HiddenField ID="hdfPaginaAtual" runat="server" Value="1" />
                            <asp:HiddenField ID="hdfUnificados" runat="server" Value="0" />
                            <div style="margin-top: 60px">
                                <table>
                                    <tr>
                                        <td>
                                            Categoria:<br />
                                            <dxe:ASPxComboBox ID="ddlCategoria" ClientInstanceName="ddlcategoria" runat="server" DropDownStyle="DropDown" IncrementalFilteringMode="Contains"
                                                    TextField="categoriaNome" ValueField="categoriaId" Width="100%">
                                                    <ClientSideEvents SelectedIndexChanged="function(s, e) {
                                                        ddlfornecedor.PerformCallback(ddlcategoria.GetValue().toString());
                                                        ddlcor.PerformCallback(ddlcategoria.GetValue().toString());
                                                    }" />
                                            </dxe:ASPxComboBox>
                                        </td>
                                        <td>
                                            Cor:<br />
                                            <dxe:ASPxComboBox ID="ddlCor" ClientInstanceName="ddlcor" runat="server" DropDownStyle="DropDown" IncrementalFilteringMode="Contains"
                                                    TextField="categoriaNome" ValueField="categoriaId" Width="100%" OnCallback="ddlCor_Callback" />
                                        </td>
                                        <td>
                                            Fornecedor:<br />
                                            <dxe:ASPxComboBox ID="ddlFornecedor" ClientInstanceName="ddlfornecedor" runat="server" DropDownStyle="DropDown" IncrementalFilteringMode="Contains"
                                                    TextField="fornecedorNome" ValueField="fornecedorId" Width="100%" OnCallback="ddlFornecedor_Callback" />
                                        </td>
                                        <td>
                                            &nbsp;<br />                            
                                            <dxe:ASPxButton ID="btnCarregar" runat="server" AutoPostBack="False" Text="Carregar" ClientInstanceName="carregar">
                                                <ClientSideEvents 
                                                    Click="function(s, e) {
                                                        proximapagina.SetVisible(true); 
                                                        conteudoProdutos.PerformCallback();
                                                    }" />
                                            </dxe:ASPxButton>
                                        </td>
                                        <td>
                                            <asp:Label runat="server" ID="lblPagina" Text="Pagina 1"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div style="overflow:auto;">
                                <asp:Literal runat="server" ID="litPaginaUnificada" Text="Página: 1" Visible="false"></asp:Literal><br /><br />
                                <asp:ListView runat="server" ID="lstProdutosUnificados" Visible="false" OnItemDataBound="lstProdutosUnificados_ItemDataBound">
                                    <ItemTemplate>
                                        <div style="border: 1px solid; margin-right: 5px; margin-bottom: 5px;">
                                            <div>
                                                <a href="https://www.graodegente.com.br/a/<%# Eval("produtoUrl") %>" target="_blank">
                                                    <img src='https://dmhxz00kguanp.cloudfront.net/fotos/<%# Eval("produtoId") %>/media_<%# Eval("fotoDestaque") %>.jpg' /><br />
                                                </a>     
                                                <%# Eval("produtoId") %> - <%# Eval("produtoNome") %>  <br />                                     
                                                <asp:HiddenField runat="server" ID="hdfProdutoId" Value='<%# Eval("produtoId") %>' />
                                            </div>
                                            <div style="overflow: auto;">
                                                <asp:ListView runat="server" ID="lstProdutosUnificadosDetalhe">
                                                    <ItemTemplate>
                                                        <div style="float: left; border: 1px solid; margin-right: 5px; margin-bottom: 5px;">
                                                            <a href="https://www.graodegente.com.br/a/<%# Eval("produtoUrl") %>" target="_blank">
                                                                <img src='https://dmhxz00kguanp.cloudfront.net/fotos/<%# Eval("produtoId") %>/media_<%# Eval("fotoDestaque") %>.jpg' /><br />
                                                            </a>
                                                            <%# Eval("produtoId") %> - <%# Eval("produtoNome") %>  <br />
                    <dxe:ASPxButton ID="btnPrincipal" runat="server" AutoPostBack="False" Text="Principal" 
                        ClientSideEvents-Click='<%# "function(s, e) { conteudoProdutos.PerformCallback(\"principal_" + Eval("produtoId") + "\");}" %>'
                        ClientInstanceName="principalunificado" Font-Size="20px">
                                        </dxe:ASPxButton>  
                                                        <dxe:ASPxButton ID="btnRemover" runat="server" AutoPostBack="False" Text="Remover" 
                        ClientSideEvents-Click='<%# "function(s, e) { conteudoProdutos.PerformCallback(\"remover_" + Eval("produtoId") + "\");}" %>'
                        ClientInstanceName="removerunificado" Font-Size="20px">
                                        </dxe:ASPxButton>
                                                            <asp:HiddenField runat="server" ID="hdfProdutoIdUnificado" Value='<%# Eval("produtoId") %>' />
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:ListView>
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                </asp:ListView>
                                <asp:ListView runat="server" ID="lstProdutos">
                                    <ItemTemplate>
                                        <div style="float: left; border: 1px solid; margin-right: 5px; margin-bottom: 5px;">
                                            <a href="https://www.graodegente.com.br/a/<%# Eval("produtoUrl") %>" target="_blank">
                                                <img src='https://dmhxz00kguanp.cloudfront.net/fotos/<%# Eval("produtoId") %>/media_<%# Eval("fotoDestaque") %>.jpg' /><br />
                                            </a>
                                            <asp:CheckBox runat="server" ID="chkProduto" /> <%# Eval("produtoNome") %>                                            
                                            <asp:HiddenField runat="server" ID="hdfProdutoId" Value='<%# Eval("produtoId") %>' />
                                        </div>
                                    </ItemTemplate>
                                </asp:ListView><br /><br /><br />

                                
                            </div>
                        </dxe:PanelContent>
                    </PanelCollection>
                </dxe:ASPxCallbackPanel>
            </div>
            <div>
                <dxe:ASPxButton ID="btnProximaPagina" runat="server" AutoPostBack="False" Text="Próxima Página"                                   
                                    ClientInstanceName="proximapagina" Font-Size="20px">
                                    <ClientSideEvents 
                                        Click="function(s, e) {
                                            window.scrollTo(0, 0);
                                            if(unificados == false){
                                                conteudoProdutos.PerformCallback('proximapagina');
                                            }
                                            else
                                            {
                                                conteudoProdutos.PerformCallback('proximapaginaunificados');
                                            }
                                        }" />
                                </dxe:ASPxButton>
            </div>
        </div>
    </form>
</body>
</html>
