﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="produtosCatalogo.aspx.cs" Inherits="admin_produtosCatalogo" EnableEventValidation="false" %>

<%@ Register Assembly="DevExpress.Web.v11.1.Linq, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Data.Linq" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" runat="Server">

    <table cellpadding="0" cellspacing="0" style="width: 920px">
        <tr>
            <td class="tituloPaginas" valign="top">
                <asp:Label ID="lblAcao" runat="server">Produtos Catalogo</asp:Label>
            </td>
        </tr>
        <tr>
            <td>

                <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
                    <tr>
                        <td align="right">
                            <table cellpadding="0" cellspacing="0" style="width: 834px; display: none;">
                                <tr>
                                    <td align="right">&nbsp;</td>
                                    <td height="38"
                                        style="background-image: url('images/bkExportar.jpg'); padding-bottom: 1px;"
                                        valign="bottom" width="231">
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 97px">&nbsp;</td>
                                                <td style="width: 33px">
                                                    <asp:ImageButton ID="btPdf" runat="server" ImageUrl="~/admin/images/btPdf.jpg" OnClick="btPdf_Click"
                                                        Style="margin-left: 5px" />
                                                </td>
                                                <td style="width: 31px">
                                                    <asp:ImageButton ID="btXsl" runat="server" ImageUrl="~/admin/images/btXls.jpg" OnClick="btXsl_Click"
                                                        Style="margin-left: 5px" />
                                                </td>
                                                <td style="width: 36px">
                                                    <asp:ImageButton ID="btRtf" runat="server" ImageUrl="~/admin/images/btRtf.jpg" OnClick="btRtf_Click"
                                                        Style="margin-left: 5px" />
                                                </td>
                                                <td valign="bottom">
                                                    <asp:ImageButton ID="btCsv" runat="server" ImageUrl="~/admin/images/btCsv.jpg" OnClick="btCsv_Click"
                                                        Style="margin-left: 5px" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td>Fornecedor:
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlExportarPlanilha" runat="server" AppendDataBoundItems="True" Width="175" AutoPostBack="True" OnSelectedIndexChanged="ddlExportarPlanilha_OnSelectedIndexChanged"
                                            DataSourceID="sqlFornecedores" DataTextField="fornecedorNome" DataValueField="fornecedorId">
                                        </asp:DropDownList>
                                        <asp:SqlDataSource ID="sqlFornecedores" runat="server"
                                            ConnectionString="<%$ ConnectionStrings:connectionString %>"
                                            SelectCommand="SELECT [fornecedorId], [fornecedorNome] FROM [tbProdutoFornecedor] ORDER BY [fornecedorNome]"></asp:SqlDataSource>
                                    </td>
                                    <td>Produto:
                                    </td>
                                    <td>
                                        <asp:TextBox runat="server" ID="txtFiltroProduto" Visible="False"></asp:TextBox>
                                        <asp:DropDownList ID="ddlCategoriaFornecedor" runat="server" Width="175" DataTextField="categoriaNome" DataValueField="categoriaId"></asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:Button runat="server" ID="btnImportar" Text="Pesquisar" OnClick="btnImportar_OnClick" />
                                    </td>
                                </tr>
                                <tr style="display: none;">
                                    <td>Produto:
                                    </td>
                                    <td>
                                        <asp:TextBox runat="server" ID="txtPesquisa" Visible="False"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:Button runat="server" ID="btnFiltrar" Visible="False" Text="Filtrar" OnClick="btnFiltrar_OnClick" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>


                            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false" AllowPaging="True" PageSize="20" OnPageIndexChanging="GridView1_OnPageIndexChanging"
                                Font-Names="Arial" Width="100%">
                                <Columns>
                                    <asp:TemplateField ItemStyle-Width="200">
                                        <ItemTemplate>
                                            <asp:Image ID="Image1" runat="server" Width="200"
                                                ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings["caminhoVirtual"] + "/fotos/" + Eval("produtoId") + "/" + Eval("fotoDestaque") + ".jpg" %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="produtoNome" HeaderText="Nome"
                                        ItemStyle-Height="150" />
                                    <asp:BoundField DataField="referencia" HeaderText="Referencia"
                                        ItemStyle-Height="150" />
                                </Columns>
                            </asp:GridView>

                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

</asp:Content>

