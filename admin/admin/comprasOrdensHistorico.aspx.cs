﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_comprasOrdensHistorico : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        FillGrid(false);
    }

    protected void btnPesquisar_Click(object sender, ImageClickEventArgs e)
    {
        FillGrid(true);
    }

    private void FillGrid(bool rebind)
    {
        var data = new dbCommerceDataContext();
        var pedidos = (from c in data.tbComprasOrdems
                       join d in data.tbComprasOrdemProdutos on c.idComprasOrdem equals d.idComprasOrdem into produtos
                       select new
                       {
                           c.idComprasOrdem,
                           c.dataCriacao,
                           c.idUsuarioCadastroOrdem,
                           c.idComprasEmpresa,
                           c.idComprasFornecedor,
                           itens = produtos.Count(),
                           aprovados = produtos.Count(x => x.status == 1),
                           reprovados = produtos.Count(x => x.status == 2),
                           valorAprovado = produtos.Count(x => x.status == 1) > 0 ? produtos.Where(x => x.status == 1).Sum(x => (x.precoRecebido ?? x.preco) * (x.quantidadeRecebida ?? x.quantidade)) : 0,
                           valor = produtos.Sum(x => x.preco * x.quantidade),
                           c.dataPrevista,
                           c.icPago,
                           status = c.statusOrdemCompra == 1 ? "Aguardando Aprovação" : c.statusOrdemCompra == 2 ? "Aguardando Fechamento" : c.statusOrdemCompra == 3 ? "Aguardando Lançamento Financeiro" : c.statusOrdemCompra == 4 ? "Finalizado" : c.statusOrdemCompra == 5 ? "Cancelado" : c.statusOrdemCompra == 6 ? "Aguardando Conferência de Entrega" : "Pendente"
                       }).OrderByDescending(x => x.dataCriacao);
        grd.DataSource = pedidos;
        if ((!Page.IsPostBack && !Page.IsCallback) | rebind) grd.DataBind();
    }

    /// <summary>
    /// Marca ou desmarca ordem de compra como Paga
    /// </summary>
    /// <param name="idLinha">id da linha no banco de dados</param>
    /// <param name="ativo">boleano que identifica se será ativada ou desativada</param>
    [WebMethod]
    public static void MarcaDesmarcaPago(int idLinha, string ativo)
    {
        var data = new dbCommerceDataContext();
        var linha = (from c in data.tbComprasOrdems where c.idComprasOrdem == idLinha select c).First();
        linha.icPago = Convert.ToBoolean(ativo);
        data.SubmitChanges();
        //var data = new dbPapaizEntities();
        //var linha = (from l in data.tbLinhas where l.linhaId == idLinha select l).First();

        //linha.ativo = Convert.ToBoolean(ativo);

        //data.SaveChanges();
    }


    private int RetornaIdUsuarioLogado()
    {
        HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
        usuarioLogadoId = Request.Cookies["usuarioLogadoId"];
        if (usuarioLogadoId == null)
        {
            return 0;
        }

        return Convert.ToInt32(usuarioLogadoId.Value);
    }

    protected void btPdf_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WritePdfToResponse();
    }
    protected void btXsl_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteXlsToResponse();
    }
    protected void btRtf_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteRtfToResponse();
    }
    protected void btCsv_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteCsvToResponse();
    }

    protected void btnExportar_OnCommand(object sender, CommandEventArgs e)
    {
        Response.Redirect("comprasOrdemDetalhe.aspx?id=" + e.CommandArgument);
    }
}