﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_transferenciaProdutos : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            fillList();
        }
    }

    protected void btnConfirmarTransferencia_OnClick(object sender, EventArgs e)
    {
        var pedidosDc = new dbCommerceDataContext();
        var itensTransferencia =
            (from c in pedidosDc.tbItensPedidos
             where (c.tbProduto.requerTransferencia ?? false) == true && (c.cancelado ?? false) == false && (c.enviado ?? false) == false && (c.statusTransferencia ?? 0) == 1
             select c);
        foreach (var itensPedido in itensTransferencia)
        {
            itensPedido.statusTransferencia = 2;
            rnInteracoes.interacaoInclui(itensPedido.pedidoId,
                itensPedido.tbProduto.produtoNome + " transferido para expedição", rnUsuarios.retornaNomeUsuarioLogado(),
                "False");
            var queue = new tbQueue();
            queue.agendamento = DateTime.Now;
            queue.andamento = false;
            queue.concluido = false;
            queue.idRelacionado = itensPedido.pedidoId;
            queue.mensagem = "";
            queue.tipoQueue = 8;
            pedidosDc.tbQueues.InsertOnSubmit(queue);
        }
        var itensTransferenciaCombo =
            (from c in pedidosDc.tbItensPedidoCombos
             where (c.tbProduto.requerTransferencia ?? false) == true && (c.tbItensPedido.cancelado ?? false) == false && (c.enviado) == false
             && (c.statusTransferencia ?? 0) == 1
             select c).ToList();
        foreach (var itensPedido in itensTransferenciaCombo)
        {
            itensPedido.statusTransferencia = 2;
            rnInteracoes.interacaoInclui(itensPedido.tbItensPedido.pedidoId,
                itensPedido.tbProduto.produtoNome + " transferido para expedição", rnUsuarios.retornaNomeUsuarioLogado(),
                "False");
            var queue = new tbQueue();
            queue.agendamento = DateTime.Now;
            queue.andamento = false;
            queue.concluido = false;
            queue.idRelacionado = itensPedido.tbItensPedido.pedidoId;
            queue.mensagem = "";
            queue.tipoQueue = 8;
            pedidosDc.tbQueues.InsertOnSubmit(queue);
        }
        pedidosDc.SubmitChanges();

        fillList();
    }

    private void fillList()
    {
        int totalTransferido = 0;
        var pedidosDc = new dbCommerceDataContext();
        var itensTransferencia =
            (from c in pedidosDc.tbItensPedidos
             where (c.tbProduto.requerTransferencia ?? false) == true && (c.cancelado ?? false) == false && (c.enviado ?? false) == false && (c.statusTransferencia ?? 0) == 1
             group c by c.produtoId into produtos
             select new
             {
                 produtos.First().tbProduto.produtoNome,
                 produtos.First().tbProduto.complementoIdDaEmpresa,
                 produtos.First().tbProduto.tbProdutoFornecedor.fornecedorNome,
                 total = produtos.Count(),
                 produtos.First().itemPedidoId,
                 produtos.First().produtoId
             }).ToList();


        var itensTransferenciaCombo =
            (from c in pedidosDc.tbItensPedidoCombos
             where (c.tbProduto.requerTransferencia ?? false) == true && (c.tbItensPedido.cancelado ?? false) == false && (c.enviado) == false
             && (c.statusTransferencia ?? 0) == 1
             group c by c.produtoId into produtos
             select new
             {
                 produtos.First().tbProduto.produtoNome,
                 produtos.First().tbProduto.complementoIdDaEmpresa,
                 produtos.First().tbProduto.tbProdutoFornecedor.fornecedorNome,
                 total = produtos.Count(),
                 produtos.First().tbItensPedido.itemPedidoId,
                 produtos.First().produtoId
             }).ToList();

        string retorno = "";
        foreach (var itemGeral in itensTransferencia)
        {
            var transferenciaCombo = itensTransferenciaCombo.Any(x => x.itemPedidoId == itemGeral.itemPedidoId);
            if (!transferenciaCombo)
            {
                retorno += "Fornecedor: " + itemGeral.fornecedorNome + "<br>Produto: " + itemGeral.produtoNome + "<br>Complemento: " + itemGeral.complementoIdDaEmpresa + "<br>Quantidade: " + itemGeral.total + "<br><br>";                
            }
        }
        foreach (var itemGeral in itensTransferenciaCombo)
        {
                retorno += "Fornecedor: " + itemGeral.fornecedorNome + "<br>Produto: " + itemGeral.produtoNome + "<br>Complemento: " + itemGeral.complementoIdDaEmpresa + "<br>Quantidade: " + itemGeral.total + "<br><br>";            
        }

        litListaPedidos.Text = retorno;
    }
}