﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;

public partial class admin_relacionarProdutosComCategoriasEmLote : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
            PopulateRootLevel();
    }

    #region categorias

    protected void treeCategorias_PreRender(object sender, EventArgs e)
    {
        //treeCategorias.Attributes.Add("OnClick", "client_OnTreeNodeChecked(event)");
    }

    private void PopulateRootLevel()
    {
        treeCategorias.Nodes.Clear();
        var root = new TreeNode();
        root.Text = "Categorias";
        root.Value = "0";
        root.Checked = true;
        root.Expanded = true;
        root.Selected = true;

        treeCategorias.Nodes.Add(root);
        int idSite = 0;
        int.TryParse(ddlSite.SelectedValue, out idSite);
        if (idSite == 0) idSite = 1;
        PopulateNodes(rnCategorias.categoriasSelecionaNivelPaiPorSiteId(idSite).Tables[0], treeCategorias.Nodes);
        treeCategorias.ExpandAll();
        //PopulateNodes(rnCategorias.categoriasSelecionaNivelPaiOcultos().Tables[0], treeCategorias.Nodes);
    }

    private void PopulateSubLevel(int categoriaPaiId, TreeNode parentNode)
    {
        PopulateNodes(rnCategorias.categoriasSelecionaNivelFilho(categoriaPaiId).Tables[0], parentNode.ChildNodes);
    }

    protected void treeCategorias_TreeNodePopulate(object sender, TreeNodeEventArgs e)
    {
        PopulateSubLevel(Int32.Parse(e.Node.Value), e.Node);
    }

    private void PopulateNodes(DataTable dt, TreeNodeCollection nodes)
    {
        foreach (DataRow dr in dt.Rows)
        {
            TreeNode tn = new TreeNode();
            tn.Text = dr["categoriaNome"].ToString();
            tn.Value = dr["categoriaId"].ToString();

            //desabilita os links do treeview
            nodes.Add(tn);

            //If node has child nodes, then enable on-demand populating
            tn.PopulateOnDemand = ((int)(dr["childnodecount"]) > 0);
        }
    }

    protected void treeCategorias_SelectedNodeChanged(object sender, EventArgs e)
    {
        desmarcaCheckbox(treeCategorias.Nodes);
        marcaCheckbox(treeCategorias.SelectedNode.Value.ToString(), treeCategorias.Nodes);
    }

    private void desmarcaCheckbox(TreeNodeCollection treeview)
    {
        int i = 0;
        foreach (TreeNode tre in treeview)
        {
            //utilizando a recursividade
            this.desmarcaCheckbox(treeview[i].ChildNodes);
            //faz a busca por trecho de texto

            tre.Checked = false; //desmarca o checkbox do item encontrado
            if (tre.Parent == null) //se não tiver parent (caso do 1º nivel)
                tre.Expand(); //expande o nivel atual
            else
                tre.Parent.Expand(); //expande o nivel anterior

            i++;
        }
    }

    private void marcaCheckbox(string valorProcurado, TreeNodeCollection treeview)
    {
        int i = 0;
        foreach (TreeNode tre in treeview)
        {
            //utilizando a recursividade
            this.marcaCheckbox(valorProcurado, treeview[i].ChildNodes);
            //faz a busca por trecho de texto
            if (tre.Value.ToString() == valorProcurado)
            {
                tre.Checked = true; //marca o checkbox do item encontrado
                if (tre.Parent == null) //se não tiver parent (caso do 1º nivel)
                    tre.Expand(); //expande o nivel atual
                else
                    tre.Parent.Expand(); //expande o nivel anterior
            }
            i++;
        }
    }

    private int pegaItensMarcados()
    {
        int categoriaPaiId = 0;
        if (treeCategorias.CheckedNodes.Count > 0)
        {
            foreach (TreeNode node in treeCategorias.CheckedNodes)
            {
                categoriaPaiId = Convert.ToInt32(node.Value);
            }
        }
        return categoriaPaiId;
    }

    #endregion

    //protected void btPdf_Click(object sender, ImageClickEventArgs e)
    //{
    //    this.grdEx.WritePdfToResponse();
    //}
    //protected void btXsl_Click(object sender, ImageClickEventArgs e)
    //{
    //    this.grdEx.WriteXlsToResponse();
    //}
    //protected void btRtf_Click(object sender, ImageClickEventArgs e)
    //{
    //    this.grdEx.WriteRtfToResponse();
    //}
    //protected void btCsv_Click(object sender, ImageClickEventArgs e)
    //{
    //    this.grdEx.WriteCsvToResponse();
    //}


    protected void grd1_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        int inicio = 0;
        int fim = 0;

        if (grd1.PageIndex == 0)
        {
            inicio = 0;
            fim = inicio + grd1.GetCurrentPageRowValues("produtoId").Count;
        }
        else
        {
            inicio += inicio + grd1.SettingsPager.PageSize * grd1.PageIndex;
            fim += inicio + grd1.GetCurrentPageRowValues("produtoId").Count;
        }

        for (int i = inicio; i < fim; i++)
        {
            
                TextBox txtProdutoId =
                    (TextBox)
                    grd1.FindRowCellTemplateControl(i, grd1.Columns["produtoId"] as GridViewDataColumn, "txtProdutoId");
                TextBox txtProdutoPaiId =
                    (TextBox)
                    grd1.FindRowCellTemplateControl(i, grd1.Columns["produtoId"] as GridViewDataColumn,
                                                    "txtProdutoPaiId");
                CheckBox ckbSelecionar = (CheckBox) grd1.FindRowCellTemplateControl(i, grd1.Columns["selecionar"] as GridViewDataColumn, "ckbSelecionar");

                if (ckbSelecionar.Checked)
                {
                    foreach (TreeNode node in treeCategorias.CheckedNodes)
                    {
                        try
                        {
                        if (node.Checked == true)
                            rnCategorias.juncaoProdutoCategoriaInclui(int.Parse(txtProdutoId.Text.ToString()),
                                                                      int.Parse(node.Value.ToString()),
                                                                      int.Parse(txtProdutoPaiId.Text.ToString()));
                        }
                    catch
                    {
                    }
                }
            
            }
        }
        grd1.DataBind();
    }

   

    protected void grd1_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
       
    }

    protected void lstFiltrosAtuais_PreRender(object sender, EventArgs e)
    {

    }

    protected void grd1_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType != GridViewRowType.Data) return;
        {
            var gridView = sender as ASPxGridView;
            var id = gridView.GetMasterRowKeyValue();
            HiddenField hiddenProdutoId = (HiddenField)grd1.FindRowCellTemplateControl(e.VisibleIndex, (GridViewDataColumn)grd1.Columns["filtros"], "hiddenProdutoId");
            ListView lstProdutos = (ListView)grd1.FindRowCellTemplateControl(e.VisibleIndex, (GridViewDataColumn)grd1.Columns["filtros"], "lstProdutos");
            int produtoId = Convert.ToInt32(hiddenProdutoId.Value);
            var categoriasDc = new dbCommerceDataContext();
            var categorias = (from c in categoriasDc.tbJuncaoProdutoCategorias
                where c.produtoId == produtoId 
                select new {c.produtoId, c.categoriaId, c.tbProdutoCategoria.categoriaNomeExibicao});
            lstProdutos.DataSource = categorias;
            lstProdutos.DataBind();

        }
    }

    protected void btnExcluir_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(hiddenProdutoId.Value) && !string.IsNullOrEmpty(hiddenCategoriaId.Value))
        {
            int categoriaId = Convert.ToInt32(hiddenCategoriaId.Value);
            int produtoId = Convert.ToInt32(hiddenProdutoId.Value);
            var categoriasDc = new dbCommerceDataContext();
            var categoria = (from c in categoriasDc.tbJuncaoProdutoCategorias where c.categoriaId == categoriaId && c.produtoId == produtoId select c).FirstOrDefault();
            if (categoria != null)
            {
                categoriasDc.tbJuncaoProdutoCategorias.DeleteOnSubmit(categoria);
                categoriasDc.SubmitChanges();
            }
            grd1.DataBind();
        }
        
    }

    protected void ddlSite_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        PopulateRootLevel();
        treeCategorias.ExpandAll();
    }
}
