﻿<%@ Page Title="" Theme="Glass" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="relacionarProdutosComColecoes.aspx.cs" Inherits="admin_relacionarProdutosComColecoes" %>

<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dxwgv" %>


<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" runat="Server">
    <script type="text/javascript">
        function toggle() {
            var divs = document.querySelectorAll("[id]");
            for (var i = 0, len = divs.length; i < len; i++) {
                var div = divs[i];
                if (div.id.indexOf("ckbSelecionar") > -1) {
                    div.checked = true;
                }
            }
        }
    </script>
    <table cellpadding="0" cellspacing="0" style="width: 920px">
        <tr>
            <td class="tituloPaginas" valign="top">
                <asp:Label ID="lblAcao" runat="server">Relacionar produtos com coleções</asp:Label>
            </td>
        </tr>
        <tr>
            <td>

                <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
                    <tr>
                        <td align="right">
                            <table cellpadding="0" cellspacing="0" style="width: 834px">
                                <tr>
                                    <td align="right">&nbsp;</td>
                                    <td height="38"
                                        style="background-image: url('images/bkExportar.jpg'); padding-bottom: 1px;"
                                        valign="bottom" width="231">
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 97px">&nbsp;</td>
                                                <td style="width: 33px">
                                                    <asp:ImageButton ID="btPdf" runat="server" ImageUrl="~/admin/images/btPdf.jpg"
                                                        Style="margin-left: 5px" />
                                                </td>
                                                <td style="width: 31px">
                                                    <asp:ImageButton ID="btXsl" runat="server" ImageUrl="~/admin/images/btXls.jpg"
                                                        Style="margin-left: 5px" />
                                                </td>
                                                <td style="width: 36px">
                                                    <asp:ImageButton ID="btRtf" runat="server" ImageUrl="~/admin/images/btRtf.jpg"
                                                        Style="margin-left: 5px" />
                                                </td>
                                                <td valign="bottom">
                                                    <asp:ImageButton ID="btCsv" runat="server" ImageUrl="~/admin/images/btCsv.jpg"
                                                        Style="margin-left: 5px" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right; padding-top: 15px;">
                            <asp:Button runat="server" OnClientClick="toggle(); return false;" Text="Checar todos" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" style="width: 834px">
                                <tr class="campos">
                                    <td style="width: 220px" valign="top">
                                        <asp:CheckBoxList Width="150px" ID="chkColecoes" runat="server" DataSourceID="sqlColecoes" DataTextField="colecaoNome" DataValueField="colecaoId" Font-Size="12px"></asp:CheckBoxList>
                                        <asp:SqlDataSource ID="sqlColecoes" runat="server" ConnectionString="<%$ ConnectionStrings:connectionString %>"
                                            SelectCommand="SELECT colecaoId, colecaoNome FROM tbColecao order by colecaoId desc"></asp:SqlDataSource>
                                    </td>
                                    <td valign="top" align="right">
                                        <dxwgv:ASPxGridView ID="grd1" runat="server" AutoGenerateColumns="False"
                                            DataSourceID="sqlProdutos" KeyFieldName="produtoId"
                                            Cursor="auto" OnCustomCallback="grd1_CustomCallback" OnHtmlRowCreated="grd1_HtmlRowCreated" OnHtmlRowPrepared="grd1_HtmlRowPrepared"
                                            ClientInstanceName="grd1" Width="834px" EnableCallBacks="False">
                                            <SettingsBehavior AutoFilterRowInputDelay="10200" ConfirmDelete="True"
                                                AutoExpandAllGroups="True" ColumnResizeMode="Control" />
                                            <SettingsText ConfirmDelete="Tem certeza que deseja excluir?"
                                                EmptyDataRow="Nenhum registro encontrado."
                                                GroupPanel="Arraste uma coluna aqui para agrupar." />
                                            <SettingsPager Position="TopAndBottom" PageSize="100"
                                                ShowDisabledButtons="False" AlwaysShowPager="True">
                                                <Summary AllPagesText="Páginas: {0} - {1} ({2} items)"
                                                    Text="Página {0} de {1} ({2} registros encontrados)" />
                                            </SettingsPager>
                                            <Settings ShowFilterRow="True" ShowGroupedColumns="True" />
                                            <SettingsEditing EditFormColumnCount="4"
                                                PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1"
                                                PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter"
                                                PopupEditFormWidth="700px" Mode="Inline" />
                                            <Columns>
                                                <dxwgv:GridViewDataTextColumn Width="100px" Caption="Id do produto" VisibleIndex="0">
                                                    <DataItemTemplate>
                                                        <asp:TextBox ID="txtProdutoId" runat="server" BorderStyle="None"
                                                            CssClass="rotulos" Text='<%# Bind("produtoId") %>' Width="100px"></asp:TextBox>
                                                        <asp:TextBox ID="txtProdutoPaiId" runat="server" BorderStyle="None"
                                                            CssClass="rotulos" Text='<%# Bind("produtoPaiId") %>' Visible="False"
                                                            Width="100px"></asp:TextBox>
                                                    </DataItemTemplate>
                                                </dxwgv:GridViewDataTextColumn>
                                                <dxwgv:GridViewDataTextColumn Caption="ID da Empresa"
                                                    FieldName="produtoIdDaEmpresa" Name="idDaEmpresa" VisibleIndex="1"
                                                    Width="100px">
                                                </dxwgv:GridViewDataTextColumn>
                                                <dxwgv:GridViewDataTextColumn Caption="Nome" FieldName="produtoNome"
                                                    VisibleIndex="2" Width="210px">
                                                    <Settings AutoFilterCondition="Contains" />
                                                    <DataItemTemplate>
                                                        <asp:TextBox ID="txtIdDaEmpresa0" runat="server" BorderStyle="None" CssClass="campos"
                                                            Text='<%# Bind("produtoNome") %>' Width="100%"></asp:TextBox>
                                                    </DataItemTemplate>
                                                </dxwgv:GridViewDataTextColumn>
                                                <dxwgv:GridViewDataTextColumn Caption="Filtros" VisibleIndex="2" Width="210px" Name="filtros">
                                                    <DataItemTemplate>
                                                        <asp:HiddenField runat="server" ID="hiddenProdutoId" Value='<%# Bind("produtoId") %>' />
                                                        <asp:ListView runat="server" ID="lstProdutos">
                                                            <ItemTemplate>
                                                                <%# Eval("colecaoNome") %>
                                                                <a style="cursor: pointer;" onclick="javacript:excluiCategoria(<%# Eval("produtoId") %>, <%# Eval("colecaoId") %>);">X</a>&nbsp;&nbsp;&nbsp;
                                                            </ItemTemplate>
                                                        </asp:ListView>
                                                    </DataItemTemplate>
                                                </dxwgv:GridViewDataTextColumn>
                                                <dxwgv:GridViewDataTextColumn Caption="Fornecedor" FieldName="fornecedorNome"
                                                    VisibleIndex="4" Width="100px">
                                                </dxwgv:GridViewDataTextColumn>
                                                <dxwgv:GridViewDataTextColumn Caption="Ativo" FieldName="produtoAtivo"
                                                    VisibleIndex="4" Width="60px" Settings-AllowHeaderFilter="True">
                                                </dxwgv:GridViewDataTextColumn>
                                                <dxwgv:GridViewDataTextColumn Caption="Colecoes" FieldName="colecoes" Settings-AllowHeaderFilter="True"
                                                    VisibleIndex="4" Width="70px">
                                                </dxwgv:GridViewDataTextColumn>
                                                <dxwgv:GridViewDataTextColumn Caption="Selecionar"
                                                    VisibleIndex="5" Width="50px" Name="selecionar">
                                                    <DataItemTemplate>
                                                        <asp:CheckBox runat="server" ID="ckbSelecionar" />
                                                        <%--                                    <dxe:ASPxCheckBox ID="ckbSelecionar" runat="server" ValueChecked="True" 
                                        ValueType="System.String" ValueUnchecked="False">
                                    </dxe:ASPxCheckBox>--%>
                                                    </DataItemTemplate>
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dxwgv:GridViewDataTextColumn>
                                            </Columns>
                                        </dxwgv:ASPxGridView>
                                        <img src="images/btSalvar.jpg" onclick="grd1.PerformCallback(this.value);"
                                            style="margin-top: 16px;" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                    <tr>
                        <td align="right">
                            <script type="text/javascript">
                                function excluiCategoria(produto, colecao) {
                                    var r = confirm("Deseja realmente remover esta coleção?");
                                    if (r == true) {
                                        document.getElementById("ctl00_conteudoPrincipal_hiddenProdutoId").value = produto;
                                        document.getElementById("ctl00_conteudoPrincipal_hiddenColecaoId").value = colecao;
                                        document.getElementById("ctl00_conteudoPrincipal_btnExcluir").click();
                                        return false;
                                    }

                                }
                            </script>
                            <div style="display: none;">
                                <asp:Button ID="btnExcluir" runat="server" Text="Excluir" OnClick="btnExcluir_Click" />
                                <asp:HiddenField ID="hiddenProdutoId" runat="server" />
                                <asp:HiddenField ID="hiddenColecaoId" runat="server" />
                            </div>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="right"></td>
                    </tr>
                    <tr>
                        <td>
                            <%--<dxwgv:ASPxGridViewExporter ID="grdEx" runat="server" FileName="fornecedores" 
                                    GridViewID="grd" Landscape="True" PreserveGroupRowStates="True">
                            </dxwgv:ASPxGridViewExporter>--%>
                            <asp:ObjectDataSource ID="sqlProdutos" runat="server"
                                SelectMethod="produtoAdminSelecionaAtivos" TypeName="rnProdutos"
                                DeleteMethod="produtoExclui">
                                <SelectParameters>
                                    <asp:Parameter Name="categoriaId" Type="String" DefaultValue="0" />
                                    <asp:Parameter Name="idSite" Type="String" DefaultValue="1" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
