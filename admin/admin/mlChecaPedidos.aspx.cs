﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using RestSharp;

public partial class admin_mlChecaPedidos : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        var meli = mlIntegracao.verificaAutenticacao("mlChecaPedidos.aspx", Request.QueryString["code"]);
    }

    protected void btnChecar_OnClick(object sender, EventArgs e)
    {
        var meli = mlIntegracao.verificaAutenticacao("mlChecaPedidos.aspx", Request.QueryString["code"]);
        var p = new RestSharp.Parameter();
        p.Name = "access_token";
        p.Value = meli.AccessToken;
        var ps = new List<RestSharp.Parameter>();
        ps.Add(p);

        var sellerId = new RestSharp.Parameter();
        sellerId.Name = "seller";
        sellerId.Value = "241861815"; //"147209156";
        ps.Add(sellerId);

        var sort = new RestSharp.Parameter();
        sort.Name = "sort";
        sort.Value = "date_desc";
        ps.Add(sort);

        IRestResponse r = meli.Post("/orders/search", ps, "");
        var pedidos = JsonConvert.DeserializeObject<mlPedidos>(r.Content);
        

    }
}