﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxGridView;

public partial class admin_consultaLog : System.Web.UI.Page
{
    private static int logId;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtDataInicial.Attributes.Add("onkeypress", "Mascaras('DATA', '" + txtDataInicial.ClientID + "');");
            txtDataFinal.Attributes.Add("onkeypress", "Mascaras('DATA', '" + txtDataFinal.ClientID + "');");
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(),
                "$(\"#" + txtDataInicial.ClientID + "\").datepicker({" +
                "inline: true," +
                "showOtherMonths: true," +
                "dateFormat: 'dd/mm/yy'," +
                "dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado']," +
                "dayNamesMin: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S', 'D']," +
                "dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb', 'Dom']," +
                "monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro']," +
                "monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez']," +
                "nextText: 'Próximo'," +
                "prevText: 'Anterior'" +
            "});" +
                "$(\"#" + txtDataFinal.ClientID + "\").datepicker({" +
                "inline: true," +
                "showOtherMonths: true," +
                "dateFormat: 'dd/mm/yy'," +
                "dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado']," +
                "dayNamesMin: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S', 'D']," +
                "dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb', 'Dom']," +
                "monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro']," +
                "monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez']," +
                "nextText: 'Próximo'," +
                "prevText: 'Anterior'" +
            "});", true);
        }
    }

    protected void grdDetalhes_BeforePerformDataSelect(object sender, EventArgs e)
    {
        Session["logId"] = (sender as ASPxGridView).GetMasterRowKeyValue();
        logId = Convert.ToInt32((sender as ASPxGridView).GetMasterRowKeyValue());
    }
}