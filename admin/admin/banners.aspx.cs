﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Data.Filtering;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxEditors;

public partial class admin_banners : System.Web.UI.Page
{
    DateTemplate dateTemplate = null;

    protected void Page_Load(object sender, EventArgs e)
    {

        grd.DataSourceID = "sqlBanners";
        carregaAgendamentos(false);
    }

    private void carregaGrid()
    {

        using (var data = new dbCommerceDataContext())
        {
            var dados = (from b in data.tbBanners
                         join p in data.tbProdutos on b.idProduto equals p.produtoId
                         join f in data.tbProdutoFornecedors on p.produtoFornecedor equals f.fornecedorId
                         select new
                         {
                             b.Id,
                             f.fornecedorNome,
                             b.local,
                             b.linkProduto,
                             b.foto,
                             b.posicao,
                             b.visualizacoes,
                             b.ativo,
                             b.clicks
                         });

            grd.DataSource = dados.ToList();
            grd.DataBind();
        }
    }


    private void carregaAgendamentos(bool rebind)
    {
        var data = new dbCommerceDataContext();
        int idBanner = Convert.ToInt32(Request.QueryString["bannerId"]);
        var agora = DateTime.Now;
        var agendamentos = (from c in data.tbBannerAgendamentos where c.dataEntrada > agora | c.dataSaida > agora | c.inicioContador > agora | c.fimContador > agora
        orderby c.dataEntrada, c.dataSaida, c.inicioContador, c.fimContador
        select new {
            c.tbBanner.Id,
            c.idBannerAgendamento,
            c.dataEntrada,
            c.dataSaida,
            c.inicioContador,
            c.fimContador,
            c.posicao,
            c.queueConcluidoEntrada,
            c.queueConcluidoContador,
            c.queueConcluidoSaida,
            c.tbBanner.linkProduto,
            c.tbBanner.foto,
            c.tbBanner.local,
            c.tbBanner.mobile
        });
        grdAgendamentos.DataSource = agendamentos;
        if ((!Page.IsPostBack && !Page.IsPostBack) | rebind)
        {
            grdAgendamentos.DataBind();
        }
    }

    protected void grd_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType != GridViewRowType.Data) return;

        Label lbllinkproduto = grd.FindRowCellTemplateControl(e.VisibleIndex, null, "linkProduto") as Label;
        DropDownList ddlLocal = grd.FindRowCellTemplateControl(e.VisibleIndex, null, "ddlLocal") as DropDownList;
        DropDownList ddlPosicao = grd.FindRowCellTemplateControl(e.VisibleIndex, null, "ddlPosicao") as DropDownList;

        HiddenField hfLocal = grd.FindRowCellTemplateControl(e.VisibleIndex, null, "hfLocal") as HiddenField;
        HiddenField hfPosicao = grd.FindRowCellTemplateControl(e.VisibleIndex, null, "hfPosicao") as HiddenField;

        ddlLocal.SelectedValue = hfLocal.Value;
        ddlPosicao.SelectedValue = hfPosicao.Value;

        if(lbllinkproduto != null)
        {
            lbllinkproduto.Text = lbllinkproduto.Text.ToLower().Replace("https://www.graodegente.com.br/",".../");
        }
    }
    protected void grd_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        try
        {

            int inicio = 0;
            int fim = 0;

            if (grd.PageIndex == 0)
            {
                inicio = 0;
                fim = inicio + grd.GetCurrentPageRowValues("Id").Count;
            }
            else
            {
                inicio += inicio + grd.SettingsPager.PageSize * grd.PageIndex;
                fim += inicio + grd.GetCurrentPageRowValues("Id").Count;
            }

            for (int i = inicio; i < fim; i++)
            {
                //TextBox txtlinkProduto = (TextBox)grd.FindRowCellTemplateControl(i, grd.Columns["linkProduto"] as GridViewDataColumn, "txtlinkProduto");
                HiddenField hfIdBanner = (HiddenField)grd.FindRowCellTemplateControl(i, grd.Columns[10] as GridViewDataColumn, "hfIdBanner");
                //DropDownList ddlLocal = (DropDownList)grd.FindRowCellTemplateControl(i, grd.Columns["local"] as GridViewDataColumn, "ddlLocal");
                //DropDownList ddlPosicao = (DropDownList)grd.FindRowCellTemplateControl(i, grd.Columns["posicao"] as GridViewDataColumn, "ddlPosicao");
                ASPxCheckBox ckbAtivo = (ASPxCheckBox)grd.FindRowCellTemplateControl(i, grd.Columns["ativo"] as GridViewDataColumn, "ckbAtivo");

                int idBanner = Convert.ToInt32(hfIdBanner.Value);

                using (var data = new dbCommerceDataContext())
                {
                    var banner = (from b in data.tbBanners where b.Id == idBanner orderby b.Id descending select b).FirstOrDefault();

                    if (banner != null)
                    {
                        //banner.linkProduto = txtlinkProduto.Text;
                        //banner.local = Convert.ToInt32(ddlLocal.SelectedValue);
                        //banner.posicao = Convert.ToInt32(ddlPosicao.SelectedValue);
                        banner.ativo = ckbAtivo.Checked;
                        data.SubmitChanges();

                    }
                }
            }

            grd.DataBind();

            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('Informações atualizadas com sucesso!');", true);
        }
        catch (Exception ex)
        {

        }
    }


    protected void grdAgendamentos_OnCustomUnboundColumnData(object sender, ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "tempoAtivacao")
        {
            DateTime agendamento = Convert.ToDateTime(e.GetListSourceFieldValue("agendamento"));
            var tempoParaAtivacao = agendamento.TimeOfDay - DateTime.Now.TimeOfDay;
            e.Value = tempoParaAtivacao.Hours > 0 ? tempoParaAtivacao.ToString() : "";


        }
    }

    protected void grdAgendamentos_OnHtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType != GridViewRowType.Data) return;

        Label lblTempoParaAtivacao = grdAgendamentos.FindRowCellTemplateControl(e.VisibleIndex, null, "lblTempoParaAtivacao") as Label;
        Label lblAgendamento = grdAgendamentos.FindRowCellTemplateControl(e.VisibleIndex, null, "lblAgendamento") as Label;
        Label lblConcluido = grdAgendamentos.FindRowCellTemplateControl(e.VisibleIndex, null, "lblConcluido") as Label;

        var ativarCountDown = Convert.ToBoolean(lblConcluido.Text);
        var dataAgendamento = Convert.ToDateTime(lblAgendamento.Text);

        if (!ativarCountDown && dataAgendamento.ToOADate() > DateTime.Now.ToOADate())
        {
            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "setTimer('" + lblTempoParaAtivacao.ClientID + "','" + dataAgendamento.ToString("yyyy-MM-dd HH:mm:ss") + "');", true);
        }
        else
        {
            lblTempoParaAtivacao.Text = "Concluído";
        }

    }

    protected void ibtCancelarAgendamento_OnCommand(object sender, CommandEventArgs e)
    {

        try
        {
            string[] dados = e.CommandArgument.ToString().Split('#');
            int idBanner = Convert.ToInt32(dados[0]);
            int idQueue = Convert.ToInt32(dados[1]);
            string dataAgendamento;
            string tipoAgendamento;
            bool concluido;

            using (var data = new dbCommerceDataContext())
            {
                var queue = (from c in data.tbQueues where c.idQueue == idQueue select c).FirstOrDefault();
                dataAgendamento = queue.agendamento.ToString("G");
                tipoAgendamento = queue.tipoQueue == 29 ? "Ativação" : "Desativação";
                concluido = queue.concluido;
                data.tbQueues.DeleteOnSubmit(queue);
                data.SubmitChanges();
            }

            if (!concluido)
            {
                var log = new rnLog { usuario = rnUsuarios.retornaNomeUsuarioLogado() };
                log.descricoes.Add("Cancelado agendamento " + tipoAgendamento + " - " + dataAgendamento + " - do banner id: " + idBanner);
                log.tiposRelacionados.Add(new rnEnums.TipoRelacionadoIds { idRegistroRelacionado = idBanner, tipoRelacionado = rnEnums.TipoRegistroRelacionado.Banner });
                log.tiposOperacao.Add(rnEnums.TipoOperacao.Banner);
                log.InsereLog();

                ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('Agendamento cancelado com sucesso!');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('Não será possivel cancelar o agendamento pois o mesmo já foi concluído!');", true);
            }

            grdAgendamentos.DataBind();
            //grdLogs.DataBind();


        }
        catch (Exception)
        {

        }

    }

    protected void grdAgendamentos_OnAutoFilterCellEditorCreate(object sender, ASPxGridViewEditorCreateEventArgs e)
    {
        if (e.Column.FieldName == "agendamento")
        {
            DropDownEditProperties dde = new DropDownEditProperties { EnableClientSideAPI = true };
            dateTemplate = new DateTemplate();
            dde.DropDownWindowTemplate = dateTemplate;
            e.EditorProperties = dde;
        }
    }

    protected void grdAgendamentos_OnAutoFilterCellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
    {
        if (e.Column.FieldName == "agendamento")
        {
            ASPxDropDownEdit dde = e.Editor as ASPxDropDownEdit;
            dde.ClientSideEvents.CloseUp = String.Format("function (s, e) {{ ApplyFilter(s, {0}, {1}, 'agendamento'); }}", dateTemplate.cIdFrom, dateTemplate.cIdTo);
            dde.ReadOnly = true;
        }
    }

    protected void grdAgendamentos_OnProcessColumnAutoFilter(object sender, ASPxGridViewAutoFilterEventArgs e)
    {
        if (e.Column.FieldName == "agendamento")
        {
            if (e.Kind == GridViewAutoFilterEventKind.CreateCriteria)
            {
                Session["agendamentoSession"] = e.Value;
                String[] dates = e.Value.Split('|');
                DateTime dateFrom = Convert.ToDateTime(dates[0]),
                    dateTo = Convert.ToDateTime(dates[1]);
                e.Criteria = (new OperandProperty("agendamento") >= dateFrom) &
                             (new OperandProperty("agendamento") <= dateTo.AddHours(23).AddMinutes(59).AddSeconds(59));
            }
            else
            {
                if (Session["agendamentoSession"] != null)
                    e.Value = Session["agendamentoSession"].ToString();
            }
        }
    }
    

    protected void btArquivar_Command(object sender, CommandEventArgs e)
    {
        int id = Convert.ToInt32(e.CommandArgument);
        var data = new dbCommerceDataContext();
        var banner = (from c in data.tbBanners where c.Id == id select c).First();
        if(banner.arquivado == true)
        {
            banner.arquivado = false;
        }
        else
        {
            banner.arquivado = true;
            banner.ativo = false;
        }
        data.SubmitChanges();
    }

    protected void sqlBanners_Selecting(object sender, DevExpress.Data.Linq.LinqServerModeDataSourceSelectEventArgs e)
    {
        var data = new dbCommerceDataContext();
        e.KeyExpression = "id";
        if(rblAtivos.SelectedValue == "ativos")
        {
            e.QueryableSource = data.viewListaBanners.Where(x => x.ativo == true);
            grd.Columns["colArquivar"].Visible = false;
            grd.Columns["colArquivado"].Visible = false; 
        }
        else if (rblAtivos.SelectedValue == "agendados")
        {
            e.QueryableSource = data.viewListaBanners.Where(x => x.agendamentos > 0);
            grd.Columns["colArquivar"].Visible = false;
            grd.Columns["colArquivado"].Visible = false;

        }
        else if (rblAtivos.SelectedValue == "inativos")
        {
            e.QueryableSource = data.viewListaBanners.Where(x => x.ativo == false && x.arquivado == false);
            grd.Columns["colArquivar"].Visible = true;
            grd.Columns["colArquivado"].Visible = false;
        }
        else if (rblAtivos.SelectedValue == "arquivados")
        {
            e.QueryableSource = data.viewListaBanners.Where(x => x.arquivado == true);
            grd.Columns["colArquivar"].Visible = true;
            grd.Columns["colArquivado"].Visible = false;
        }
        else if (rblAtivos.SelectedValue == "todos")
        {
            e.QueryableSource = data.viewListaBanners;
            grd.Columns["colArquivar"].Visible = true;
            grd.Columns["colArquivado"].Visible = true;
        }
    }
}