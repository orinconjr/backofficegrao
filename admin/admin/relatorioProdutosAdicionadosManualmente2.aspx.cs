﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using SpreadsheetLight;
using System.IO;
using System.Xml;
public partial class admin_relatorioProdutosAdicionadosManualmente2 : System.Web.UI.Page
{
    public class Item
    {
        public int itemPedidoId { get; set; }
        public DateTime dataDaCriacao { get; set; }
        public int pedidoId { get; set; }
        public int produtoId { get; set; }
        public string produtoNome { get; set; }
        public decimal? produtoPrecoDeCusto { get; set; }
        public string motivoAdicao { get; set; }
    }
    public class Nota
    {
        public decimal? vlNota { get; set; }
        public decimal? vlFrete { get; set; }
        public decimal? vlCusto { get; set; }
        public string transportadora { get; set; }

    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (validarcampos())
            {
                carregaGrid();
            }
            //getValoresNota(@"C:/Users/Marcelo/Desktop/Adilson/Projetos/graodegentemaster2017/admin/admin/xml_34015.xml");

        }
    }
    private void carregaGrid()
    {
        List<Item> itens = getDados();
        GridView1.DataSource = itens;
        GridView1.DataBind();
        lblitensencontrados.Text = itens.Count().ToString();
        lblprecodecusto.Text = Convert.ToDecimal(itens.Sum(x => x.produtoPrecoDeCusto)).ToString("C");
    }

    public List<Item> getDados()
    {
        var data = new dbCommerceDataContext();
        List<Item> itensPedido = (from ip in data.tbItensPedidos
                                  where ip.motivoAdicao != null
                                  select new Item()
                                  {
                                      itemPedidoId = ip.itemPedidoId,
                                      dataDaCriacao = ip.dataDaCriacao,
                                      pedidoId = ip.pedidoId,
                                      produtoId = ip.produtoId,
                                      produtoNome = ip.tbProduto.produtoNome,
                                      produtoPrecoDeCusto = ip.tbProduto.produtoPrecoDeCusto,
                                      motivoAdicao = ip.motivoAdicao

                                  }).ToList<Item>();

        if (!String.IsNullOrEmpty(txtNumPedido.Text))
        {
            int pedidoId = Convert.ToInt32(txtNumPedido.Text);
            itensPedido = (from ip in itensPedido where ip.pedidoId == pedidoId select ip).ToList();
        }

        //if (!String.IsNullOrEmpty(txtprodutoId.Text))
        //{
        //    int produtoId = Convert.ToInt32(txtprodutoId.Text);
        //    itensPedido = (from ip in itensPedido where ip.produtoId == produtoId select ip).ToList();
        //}

        if (txtDataInicial.Text != "" && txtDataFinal.Text != "")
        {
            var dtInicial = Convert.ToDateTime(txtDataInicial.Text);
            DateTime dtFinal = Convert.ToDateTime(txtDataFinal.Text + " 23:59:59");
            itensPedido =
                itensPedido.Where(
                    x => x.dataDaCriacao >= dtInicial && x.dataDaCriacao <= dtFinal).ToList();
        }
        if (ddlMotivoAdicionarItem.SelectedItem.Value != "0")
        {
            string motivoAdicao = ddlMotivoAdicionarItem.SelectedItem.Value.ToString();
            itensPedido = (from pp in itensPedido where pp.motivoAdicao == motivoAdicao select pp).ToList();
        }
        if (ddlordenarpor.SelectedItem.Value == "dataDaCriacao")
            itensPedido = itensPedido.OrderByDescending(x => x.dataDaCriacao).ToList();
        if (ddlordenarpor.SelectedItem.Value == "pedidoId")
            itensPedido = itensPedido.OrderByDescending(x => x.pedidoId).ToList();
        //if (ddlordenarpor.SelectedItem.Value == "produtoId")
        //    itensPedido = itensPedido.OrderBy(x => x.produtoId).ToList();
        //if (ddlordenarpor.SelectedItem.Value == "produtoNome")
        //    itensPedido = itensPedido.OrderBy(x => x.produtoNome).ToList();
        if (ddlordenarpor.SelectedItem.Value == "produtoPrecoDeCusto")
            itensPedido = itensPedido.OrderBy(x => x.produtoPrecoDeCusto).ToList();
        if (ddlordenarpor.SelectedItem.Value == "motivoAdicao")
            itensPedido = itensPedido.OrderBy(x => x.motivoAdicao).ToList();

        return itensPedido;
        //  }).OrderByDescending(x => x.dataDaCriacao).ToList<Item>();
    }
    bool validarcampos()
    {
        string erro = "";
        bool valido = true;
        if (!String.IsNullOrEmpty(txtNumPedido.Text))
        {
            try { int teste = Convert.ToInt32(txtNumPedido.Text); }
            catch (Exception)
            {
                erro += @"Numero do pedido invalido\n";
                valido = false;
            }
        }

        //if (!String.IsNullOrEmpty(txtprodutoId.Text))
        //{
        //    try { int teste = Convert.ToInt32(txtprodutoId.Text); }
        //    catch (Exception)
        //    {
        //        erro += @"Id do Produto invalido\n";
        //        valido = false;
        //    }
        //}

        if (!String.IsNullOrEmpty(txtDataInicial.Text))
        {
            try { DateTime teste = Convert.ToDateTime(txtDataInicial.Text); }
            catch (Exception)
            {
                erro += @"Data inicial invalida\n";
                valido = false;
            }
        }

        if (!String.IsNullOrEmpty(txtDataFinal.Text))
        {
            try { DateTime teste = Convert.ToDateTime(txtDataFinal.Text); }
            catch (Exception)
            {
                erro += @"Data final invalida\n";
                valido = false;
            }
        }

        if (erro != "")
        {
            string meuscript = @"alert('" + erro + "');";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "MyScript", meuscript, true);

        }


        return valido;
    }
    protected void btnFiltrar_Click(object sender, EventArgs e)
    {
        if (validarcampos())
            carregaGrid();
    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;

        carregaGrid();
    }
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            string pedidoId = DataBinder.Eval(e.Row.DataItem, "pedidoId").ToString();
            Nota nota = getValoresNota(Convert.ToInt32(pedidoId));

            Label lblvlNota = (Label)e.Row.FindControl("lblvlNota");
            lblvlNota.Text = String.Format("{0:c}", nota.vlNota);

            Label lblvlFrete = (Label)e.Row.FindControl("lblvlFrete");
            lblvlFrete.Text = String.Format("{0:c}", nota.vlFrete);

            Label lblTransportadora = (Label)e.Row.FindControl("lblTransportadora");
            lblTransportadora.Text = nota.transportadora;

            HtmlAnchor linkeditar = (HtmlAnchor)e.Row.FindControl("linkeditar");
            linkeditar.HRef = "pedido.aspx?pedidoId=" + pedidoId;
        }
    }
    protected void btnExportarPlanilha_Click(object sender, EventArgs e)
    {
        if (validarcampos())
        {
            List<Item> itens = getDados();
            SLDocument sl = new SLDocument();
            SLStyle style1 = sl.CreateStyle();
            style1.Font.Bold = true;
            sl.SetRowStyle(1, style1);

            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("Content-Disposition", "attachment; filename=Produtos_Adicionados_Manualmente.xlsx");

            sl.SetCellValue("A1", "Data");
            sl.SetCellValue("B1", "Id Pedido");
            sl.SetCellValue("C1", "Id Produto");
            sl.SetCellValue("D1", "Nome do Produto");
            sl.SetCellValue("E1", "Preço de custo");
            sl.SetCellValue("F1", "Motivo da Adição");


            int linha = 2;
            foreach (var item in itens)
            {
                sl.SetCellValue(linha, 1, item.dataDaCriacao.ToShortDateString());
                sl.SetCellValue(linha, 2, item.pedidoId.ToString());
                sl.SetCellValue(linha, 3, item.produtoId);
                sl.SetCellValue(linha, 4, item.produtoNome);
                sl.SetCellValue(linha, 5, Convert.ToDecimal(item.produtoPrecoDeCusto).ToString("C"));
                sl.SetCellValue(linha, 6, item.motivoAdicao);
                linha++;
            }

            sl.SaveAs(Response.OutputStream);
            Response.End();
        }

    }

    public Nota getValoresNota(int pedidoId)
    {
        Nota nota = new Nota();
        var data = new dbCommerceDataContext();
        var notaDoPedido = (from nf in data.tbNotaFiscals
                            where nf.idPedido == pedidoId
                            select new { nf.numeroNota, nf.dataHora }).OrderBy(x => x.dataHora).FirstOrDefault();
        try
        {
            string caminho = System.Configuration.ConfigurationManager.AppSettings["caminhoFisico"] + "\\admin\\notas\\" + pedidoId  + "_" + notaDoPedido.numeroNota + ".xml";
            XmlDocument doc = new XmlDocument();
            doc.Load(caminho);

            //Display all the book titles.
            XmlNodeList vNF = doc.GetElementsByTagName("vNF");
            nota.vlNota = Convert.ToDecimal(vNF[0].InnerXml.Replace(".",","));

            XmlNodeList vFrete = doc.GetElementsByTagName("vFrete");
            nota.vlFrete = Convert.ToDecimal(vFrete[0].InnerXml.Replace(".", ","));

            XmlNode root = doc.FirstChild;
            nota.transportadora = root.FirstChild.FirstChild.ChildNodes[6].ChildNodes[1].ChildNodes[1].InnerXml;

        }
        catch (Exception ex) {
            string erro = ex.Message;
        }

        return nota;
    }

}