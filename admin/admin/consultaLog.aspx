﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="consultaLog.aspx.cs" Inherits="admin_consultaLog" Theme="Glass" %>

<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxTimer" TagPrefix="dxt" %>
<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" runat="Server">

    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/jquery-1.10.2.js" type="text/javascript"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js" type="text/javascript"></script>

    <script type="text/javascript">

        $(function () {
            $("#<%=txtDataInicial.ClientID%>").datepicker({
                inline: true,
                showOtherMonths: true,
                dateFormat: 'dd/mm/yy',
                dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
                dayNamesMin: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S', 'D'],
                dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb', 'Dom'],
                monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
                monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
                nextText: 'Próximo',
                prevText: 'Anterior'
            });
            $("#<%=txtDataFinal.ClientID%>").datepicker({
                inline: true,
                showOtherMonths: true,
                dateFormat: 'dd/mm/yy',
                dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
                dayNamesMin: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S', 'D'],
                dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb', 'Dom'],
                monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
                monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
                nextText: 'Próximo',
                prevText: 'Anterior'
            });

        });
    </script>



    <asp:ScriptManager runat="server"></asp:ScriptManager>
    <table cellpadding="0" cellspacing="0" style="width: 920px">
        <tr>
            <td class="tituloPaginas" valign="top">
                <asp:Label ID="lblAcao" runat="server">Consulta Log de alteração dos produtos</asp:Label>
            </td>
        </tr>

        <tr>
            <td>
                <asp:UpdatePanel runat="server">
                    <ContentTemplate>
                        <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
                            <tr>
                                <td>
                                    <table cellpadding="0" cellspacing="0" style="width: 490px">
                                        <tr>
                                            <td class="rotulos" style="width: 100px">Data inicial<br __designer:mapid="3fc" />
                                                <asp:TextBox ID="txtDataInicial" runat="server" CssClass="ll-skin-latoja campos"
                                                    Text='<%# Bind("faixaDeCepPesoInicial") %>'
                                                    Width="90px" MaxLength="10"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rqvDataInicial" runat="server"
                                                    ControlToValidate="txtDataInicial" Display="None"
                                                    ErrorMessage="Preencha a data inicial."
                                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                <b __designer:mapid="27df">
                                                    <asp:RegularExpressionValidator ID="rgeDataInicial" runat="server"
                                                        ControlToValidate="txtDataInicial" Display="None"
                                                        ErrorMessage="Por favor, preencha corretamente a data inicial."
                                                        SetFocusOnError="True"
                                                        ValidationExpression="^(([0-2]\d|[3][0-1])\/([0]\d|[1][0-2])\/[1-2][0-9]\d{2})$"></asp:RegularExpressionValidator>


                                                </b>
                                            </td>
                                            <td class="rotulos" style="width: 100px">Data final<br __designer:mapid="401" />
                                                <asp:TextBox ID="txtDataFinal" runat="server" CssClass="ll-skin-latoja campos"
                                                    Text='<%# Bind("faixaDeCepPesoFinal") %>' ValidationGroup="grpInsert"
                                                    Width="90px" MaxLength="10"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rqvDataFinal" runat="server"
                                                    ControlToValidate="txtDataFinal" Display="None"
                                                    ErrorMessage="Preencha a data final."
                                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                <b __designer:mapid="27df">
                                                    <asp:RegularExpressionValidator ID="rgeDataInicial0" runat="server"
                                                        ControlToValidate="txtDataFinal" Display="None"
                                                        ErrorMessage="Por favor, preencha corretamente a data final"
                                                        SetFocusOnError="True"
                                                        ValidationExpression="^(([0-2]\d|[3][0-1])\/([0]\d|[1][0-2])\/[1-2][0-9]\d{2})$"></asp:RegularExpressionValidator>
                                                </b>
                                            </td>
                                            <td class="rotulos" style="width: 100px">ID do Produto:<br />
                                                <asp:TextBox ID="txtProdutoId" runat="server" CssClass="ll-skin-latoja campos"
                                                     ValidationGroup="grpInsert"
                                                    Width="90px" MaxLength="10"></asp:TextBox>
                                                </b>
                                            </td>
                                            <td valign="middle">
                                                <asp:ImageButton ID="imbInsert" runat="server" Style="margin-top: 14px;"
                                                    ImageUrl="~/admin/images/btPesquisar.jpg" />
                                                <asp:ValidationSummary ID="llds" runat="server" DisplayMode="List"
                                                    ShowMessageBox="True" ShowSummary="False" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>

                            <tr>
                                <td>
                                    <dxwgv:ASPxGridView Settings-ShowHorizontalScrollBar="True" DataSourceID="sql" ID="grd" runat="server" AutoGenerateColumns="False" Width="834px"
                                        Cursor="auto" KeyFieldName="idLog" Settings-ShowVerticalScrollBar="True" Settings-VerticalScrollableHeight="500">
                                        <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                                        <Styles>
                                            <Footer Font-Bold="True">
                                            </Footer>
                                        </Styles>
                                        <SettingsText ConfirmDelete="Tem certeza que deseja excluir?" EmptyDataRow="Nenhum registro encontrado." />
                                        <SettingsPager Position="TopAndBottom" PageSize="50" ShowDisabledButtons="False" AlwaysShowPager="True">
                                            <Summary AllPagesText="Páginas: {0} - {1} ({2} items)" Text="Página {0} de {1} ({2} registros encontrados)" />
                                        </SettingsPager>
                                        <Settings ShowFilterRow="True" ShowGroupButtons="False" ShowHeaderFilterButton="True" ShowFooter="True" />
                                        <SettingsEditing EditFormColumnCount="4" PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1" PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter" PopupEditFormWidth="700px" />
                                        <Columns>
                                            <dxwgv:GridViewDataTextColumn Caption="ID Produto" FieldName="produtoId" VisibleIndex="0">
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn Caption="Produto" FieldName="produtoNome" VisibleIndex="0" Width="380">
                                                <Settings AutoFilterCondition="Contains" />
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn Caption="Data do Log" FieldName="dataDoLog" VisibleIndex="0">
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn Caption="Usuario Log" FieldName="usuario" VisibleIndex="0">
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn Caption="idLog" FieldName="idLog" VisibleIndex="0">
                                            </dxwgv:GridViewDataTextColumn>
                                            <%--<dxwgv:GridViewDataDateColumn Caption="Último Pedido" FieldName="ultimoPedido" UnboundType="DateTime" VisibleIndex="0" Width="100">
                                        <PropertiesDateEdit DisplayFormatString="" EditFormat="DateTime">
                                            <CalendarProperties ClearButtonText="Apagar" TodayButtonText="Hoje">
                                            </CalendarProperties>
                                        </PropertiesDateEdit>
                                        <Settings GroupInterval="Date" />
                                    </dxwgv:GridViewDataDateColumn>
                                    <dxwgv:GridViewDataDateColumn Caption="Última Compra" FieldName="ultimaCompra" UnboundType="DateTime" VisibleIndex="0" Width="100">
                                        <PropertiesDateEdit DisplayFormatString="" EditFormat="DateTime">
                                            <CalendarProperties ClearButtonText="Apagar" TodayButtonText="Hoje">
                                            </CalendarProperties>
                                        </PropertiesDateEdit>
                                        <Settings GroupInterval="Date" />
                                    </dxwgv:GridViewDataDateColumn>--%>
                                        </Columns>
                                        <Templates>
                                            <DetailRow>
                                                <dxwgv:ASPxGridView ID="grdDetalhes" runat="server" AutoGenerateColumns="False" DataSourceID="sqlDetalhes" Width="100%"
                                                    Cursor="auto" EnableCallBacks="False" OnBeforePerformDataSelect="grdDetalhes_BeforePerformDataSelect">
                                                    <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                                                    <SettingsText ConfirmDelete="Tem certeza que deseja excluir?"
                                                        EmptyDataRow="Nenhum registro encontrado." />
                                                    <SettingsPager Position="TopAndBottom" PageSize="50"
                                                        ShowDisabledButtons="False" AlwaysShowPager="True">
                                                        <Summary AllPagesText="Páginas: {0} - {1} ({2} items)"
                                                            Text="Página {0} de {1} ({2} registros encontrados)" />
                                                    </SettingsPager>
                                                    <Settings ShowFilterRow="True" ShowGroupButtons="False" />
                                                    <SettingsEditing EditFormColumnCount="4"
                                                        PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1"
                                                        PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter"
                                                        PopupEditFormWidth="700px" />
                                                    <Columns>
                                                        <dxwgv:GridViewDataTextColumn Caption="Alterações realizadas" FieldName="descricao"
                                                            VisibleIndex="1" Settings-AutoFilterCondition="Contains">
                                                            <Settings AutoFilterCondition="Contains" />
                                                            <EditFormSettings CaptionLocation="Top" />
                                                        </dxwgv:GridViewDataTextColumn>
                                                    </Columns>
                                                    <StylesEditors>
                                                        <Label Font-Bold="True">
                                                        </Label>
                                                    </StylesEditors>
                                                </dxwgv:ASPxGridView>
                                            </DetailRow>
                                        </Templates>
                                        <SettingsDetail AllowOnlyOneMasterRowExpanded="True" ShowDetailRow="True" />
                                        <StylesEditors>
                                            <Label Font-Bold="True">
                                            </Label>
                                        </StylesEditors>
                                    </dxwgv:ASPxGridView>
                                    <asp:SqlDataSource runat="server" ID="sql" ConnectionString="<%$ ConnectionStrings:connectionString %>"
                                        SelectCommand="select p.produtoId, p.produtoNome, log.data as dataDoLog, log.usuario, log.idLog
                                                FROM tbLog log 
                                                INNER JOIN tbLogRegistroRelacionado on log.idLog = tbLogRegistroRelacionado.idLog
                                                INNER JOIN tbProdutos p on p.produtoId = tbLogRegistroRelacionado.idRegistroRelacionado
                                                INNER JOIN tbLogDescricao on log.idLog = tbLogDescricao.idLog
                                                WHERE (CONVERT(varchar(8), log.data, 112) 
                                                       BETWEEN CONVERT(varchar(8), CONVERT(datetime, @dataInicial, 103), 112) 
                                                        AND CONVERT(varchar(8), CONVERT(datetime, @dataFinal, 103), 112)) AND tbLogRegistroRelacionado.idRegistroRelacionado = @produtoId
                                                GROUP BY p.produtoId, p.produtoNome, log.data, log.usuario, log.idLog
                                                ORDER BY p.produtoNome">
                                        <SelectParameters>
                                            <asp:ControlParameter ControlID="txtDataInicial" Name="dataInicial" />
                                            <asp:ControlParameter ControlID="txtDataFinal" Name="dataFinal" />
                                            <asp:ControlParameter ControlID="txtProdutoId" Name="produtoId" />
                                        </SelectParameters>
                                    </asp:SqlDataSource>
                                    <asp:SqlDataSource runat="server" ID="sqlDetalhes" ConnectionString="<%$ ConnectionStrings:connectionString %>"
                                        SelectCommand="select descricao FROM tbLogDescricao WHERE idLog = @logId">
                                        <SelectParameters>
                                            <asp:SessionParameter Name="logId" SessionField="logId" Type="Int32" />
                                        </SelectParameters>
                                    </asp:SqlDataSource>

                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
                
                <asp:UpdateProgress ID="updateProgress" runat="server" DisplayAfter="500">
                    <ProgressTemplate>
                        <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                            <div style="background-color: white; position: fixed; top: 44%; left: 40%; width: 238px; height: 50px;">Pesquisando logs...</div>
                            <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="../admin/images/245.gif" AlternateText="Loading ..." ToolTip="Pesquisando logs..." Style="padding: 10px; position: fixed; top: 45%; left: 40%;" />
                        </div>
                    </ProgressTemplate>
                </asp:UpdateProgress>

            </td>
        </tr>
    </table>

</asp:Content>

