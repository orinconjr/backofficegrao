﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="produtoContador.aspx.cs" Theme="Glass" Inherits="admin_produtoContador" %>

<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxEditors" Assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>
<%@ Register TagPrefix="dxwgv" Namespace="DevExpress.Web.ASPxGridView" Assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" runat="Server">
    <asp:ScriptManager runat="server"></asp:ScriptManager>

    <script type="text/javascript">
        function checaItemTodos()
        {
            var retorno = true;
            var e = document.getElementById("<%=ddlCategoria.ClientID%>");
            var idCategoria = e.options[e.selectedIndex].value;
            if (idCategoria == "99999999")
            {
                retorno = confirm("Todos os contadores serão excluídos.Confirma?");
            }
            else {
                retorno = confirm("As remoções serão realizadas de acordo com os dados informados nos campos, confirma?");
            }
            return retorno;
        }
    </script>
    <table cellpadding="0" cellspacing="0" style="width: 920px">
        <tr>
            <td class="tituloPaginas" valign="top">
                <asp:Label ID="lblAcao" runat="server">Produto Contador</asp:Label>
            </td>
        </tr>
        <tr class="rotulos">
            <td style="padding-left: 50px;">Categoria:
                <asp:DropDownList runat="server" ID="ddlCategoria" DataValueField="categoriaId" DataTextField="categoriaNome" Width="448" />
            </td>
        </tr>
        <tr class="rotulos">
            <td style="padding: 10px 50px;">
                <div style="width: 506px;">
                    Informar os Id's dos Produtos (ex.: 12345,67890,74185)<br />
                    <asp:TextBox ID="txtIdsProdutos" runat="server" TextMode="MultiLine" Height="65" Width="500" />
                </div>
            </td>
        </tr>
        <tr class="rotulos">
            <td style="padding-left: 30px;">
                <div style="width: 400px;">
                    <div style="float: left; width: 370px; padding: 6px 0 15px 20px;">
                        <div style="float: left; width: 186px;">Data Início:</div>
                        <div style="float: left;">Hora Início:</div>
                        <dx:ASPxDateEdit ID="txtInicioContadorData" runat="server" Style="float: left;">
                        </dx:ASPxDateEdit>
                        <dx:ASPxTimeEdit ID="txtInicioContadorHora" runat="server" Style="float: left; margin-left: 15px;">
                        </dx:ASPxTimeEdit>

                        <div style="float: left; width: 186px;">Data Fim:</div>
                        <div style="float: left;">Hora Fim:</div>
                        <dx:ASPxDateEdit ID="txtFimContadorData" runat="server" Style="float: left;">
                        </dx:ASPxDateEdit>
                        <dx:ASPxTimeEdit ID="txtFimContadorHora" runat="server" Style="float: left; margin-left: 15px;">
                        </dx:ASPxTimeEdit>
                    </div>
                </div>
            </td>
        </tr>
        <tr class="rotulos">
            <td style="padding-left: 30px;">
                <div style="width: 370px; padding: 6px 0 0 20px;">
                    <div style="float: left; width: 48px;">Estilo:</div>
                    <div style="float: left; width: 305px; padding-top: 5px;">
                        <asp:DropDownList runat="server" ID="ddlLayoutContador" DataTextField="nome" DataValueField="id" Style="float: left;" Width="306" />
                    </div>
                </div>
                <div style="float: right; width: 360px; float: right; margin-top: -12px;">
                    <%--<asp:Button runat="server" ID="btnRemover" Text="Remover" OnClick="btnRemover_OnClick" OnClientClick="return confirm('As remoções serão realizadas de acordo com os dados informados nos campos, confirma?');checaItemTodos();"--%>
                        <asp:Button runat="server" ID="btnRemover" Text="Remover" OnClick="btnRemover_OnClick" OnClientClick="return checaItemTodos();"
                        Style="float: right; padding: 8px; margin: -1px 44px 0 0; background-color: #d90e00; color: white; border: 1px solid; border-radius: 6px; font-size: 16px; cursor: pointer; font-weight: bold;" />
                    <asp:ImageButton ID="btnSalvar" runat="server" ImageUrl="images/btSalvar.jpg" AlternateText="Salvar" OnClick="btnSalvar_OnClick" ValidationGroup="cadastro" Style="cursor: pointer;" /><br />

                </div>
            </td>
        </tr>
        <tr>
            <td style="float: right; padding-right: 25px;"></td>
        </tr>
        <tr>
            <td>&nbsp;
            </td>
        </tr>
        <tr>
            <td class="tituloPaginas" valign="top">
                <asp:Label ID="Label1" runat="server">Contadores Cadastrados</asp:Label>
            </td>
        </tr>
        <tr>
            <td style="padding-left: 40px;">
                <asp:UpdatePanel runat="server">
                    <ContentTemplate>
                        <asp:Panel ID="pnlGrid" runat="server" ScrollBars="Horizontal" Width="834px">
                            <dxwgv:ASPxGridView ID="grd" runat="server" AutoGenerateColumns="False"
                                DataSourceID="sqlDS" KeyFieldName="id" Cursor="auto"
                                ClientInstanceName="grd" EnableCallBacks="False">
                                <SettingsBehavior AutoFilterRowInputDelay="10200" ConfirmDelete="True"
                                    AutoExpandAllGroups="True" ColumnResizeMode="Control" AllowGroup="False"
                                    AllowFocusedRow="True" />
                                <SettingsText ConfirmDelete="Tem certeza que deseja excluir?"
                                    EmptyDataRow="Nenhum registro encontrado."
                                    GroupPanel="Arraste uma coluna aqui para agrupar." />
                                <SettingsPager Position="TopAndBottom" PageSize="30"
                                    ShowDisabledButtons="False" AlwaysShowPager="True">
                                    <Summary AllPagesText="Páginas: {0} - {1} ({2} items)"
                                        Text="Página {0} de {1} ({2} registros encontrados)" />
                                </SettingsPager>
                                <Settings ShowFilterRow="True" ShowGroupedColumns="True" ShowFooter="True" />
                                <TotalSummary>
                                    <dxwgv:ASPxSummaryItem DisplayFormat="0" FieldName="margem"
                                        ShowInColumn="Margem" ShowInGroupFooterColumn="Margem"
                                        SummaryType="Average" />
                                </TotalSummary>
                                <SettingsEditing EditFormColumnCount="4"
                                    PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1"
                                    PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter"
                                    PopupEditFormWidth="700px" Mode="Inline" />
                                <Columns>
                                    <dxwgv:GridViewDataTextColumn Caption="ID Banner"
                                        FieldName="id" Name="id" VisibleIndex="3" Visible="false"
                                        Width="100px">
                                        <Settings AutoFilterCondition="Contains" />
                                        <DataItemTemplate>
                                            <asp:TextBox ID="txtIdDoBanner" runat="server" BorderStyle="None"
                                                CssClass="campos" Text='<%# Bind("id") %>' Width="100%"></asp:TextBox>

                                        </DataItemTemplate>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="produtoId"
                                        FieldName="produtoId" Name="produtoId" VisibleIndex="3"
                                        Width="75">
                                        <DataItemTemplate>
                                            <%#Eval("produtoId") %>
                                        </DataItemTemplate>
                                    </dxwgv:GridViewDataTextColumn>

                                    <dxwgv:GridViewDataTextColumn Caption="produto"
                                        FieldName="produtoNome" Name="produtoNome" VisibleIndex="3"
                                        Width="75">
                                        <DataItemTemplate>
                                            <%#Eval("produtoNome") %>
                                        </DataItemTemplate>
                                    </dxwgv:GridViewDataTextColumn>

                                    <dxwgv:GridViewDataTextColumn Caption="inicioContador" FieldName="inicioContador" CellStyle-HorizontalAlign="Center"
                                        VisibleIndex="6" Width="50px">
                                        <DataItemTemplate>
                                            <asp:Label ID="lblVisualizacoes" runat="server" Text='<%# Eval("inicioContador") %>'></asp:Label>
                                        </DataItemTemplate>
                                    </dxwgv:GridViewDataTextColumn>

                                    <dxwgv:GridViewDataTextColumn Caption="fimContador" FieldName="fimContador" CellStyle-HorizontalAlign="Center"
                                        VisibleIndex="6" Width="50px">
                                        <DataItemTemplate>
                                            <asp:Label ID="lblClicks" runat="server" Text='<%# Eval("fimContador") %>'></asp:Label>
                                        </DataItemTemplate>
                                    </dxwgv:GridViewDataTextColumn>

                                    <dxwgv:GridViewDataTextColumn Caption="estilo" FieldName="estilo" CellStyle-HorizontalAlign="Center"
                                        VisibleIndex="6" Width="50px">
                                        <DataItemTemplate>
                                            <asp:Label ID="lblClicks" runat="server" Text='<%# Eval("estilo") %>'></asp:Label>
                                        </DataItemTemplate>
                                    </dxwgv:GridViewDataTextColumn>


                                    <dxwgv:GridViewDataTextColumn Caption="ativo" FieldName="ativo" CellStyle-HorizontalAlign="Center"
                                        UnboundType="Boolean" VisibleIndex="2" Width="40px">
                                        <DataItemTemplate>
                                            <dxe:ASPxCheckBox ID="ckbAtivo" runat="server"
                                                Value='<%#Eval("ativo") %>' ValueChecked="True"
                                                ValueType="System.Boolean" ValueUnchecked="False">
                                            </dxe:ASPxCheckBox>
                                        </DataItemTemplate>
                                    </dxwgv:GridViewDataTextColumn>
                                </Columns>
                            </dxwgv:ASPxGridView>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>

                <asp:SqlDataSource ConnectionString="<%$ ConnectionStrings:connectionString %>" runat="server" ID="sqlDS"
                    SelectCommand="SELECT tbProdutoContador.id, tbProdutoContador.produtoId, tbProdutos.produtoNome, inicioContador, fimContador, tbEstilos.nome As 'estilo', ativo 
                                        FROM tbProdutoContador
                                    INNER JOIN tbEstilos ON tbProdutoContador.estilo = tbEstilos.id
                                    INNER JOIN tbProdutos ON tbProdutoContador.produtoId = tbProdutos.produtoId"></asp:SqlDataSource>
            </td>
        </tr>
    </table>

</asp:Content>

