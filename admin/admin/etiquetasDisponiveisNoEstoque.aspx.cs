﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_etiquetasDisponiveisNoEstoque : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            carregaGrid();
        }
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {


        GridView1.PageIndex = e.NewPageIndex;

        carregaGrid();
    }
    void carregaGrid()
    {
        var data = new dbCommerceDataContext();

        var dataFiltroIni = DateTime.Now.AddMonths(-3);
        var dataFiltroFim = DateTime.Now.AddDays(-2);

        var pagamentos = (from pe in data.tbProdutoEstoques
                          join ipe in data.tbItemPedidoEstoques on pe.idItemPedidoEstoqueReserva equals ipe.idItemPedidoEstoque
                          where (pe.tbPedido.statusDoPedido == 5 || pe.tbPedido.statusDoPedido == 6)
                          && (!pe.enviado && ipe.enviado) && (pe.dataEntrada.Date >= dataFiltroIni.Date && pe.dataEntrada.Date <= dataFiltroFim.Date)
                          && pe.pedidoId == null
                          select new
                          {
                              pe.idPedidoFornecedorItem,
                              pe.tbEnderecamentoArea.area,
                              pe.tbEnderecamentoRua.rua,
                              predio = pe.tbEnderecamentoPredio.predio + " - " + pe.tbEnderecamentoPredio.tbEnderecamentoLado.lado,
                              pe.tbEnderecamentoAndar.andar,
                              pe.pedidoId,
                              pe.itemPedidoId,
                              enviadotbProdutoEstoque = pe.enviado,
                              enviadotbItemPedidoEstoque = ipe.enviado,
                              pe.tbProduto.produtoId,
                              pe.tbProduto.produtoNome,
                              pe.tbPedido.tbPedidoSituacao.situacao,
                              pe.idCentroDistribuicao

                          }).Distinct().OrderByDescending(x => x.pedidoId).ToList();

        if (!String.IsNullOrEmpty(txtetiqueta.Text))
        {
            pagamentos = (from p in pagamentos
                          where p.idPedidoFornecedorItem == Convert.ToInt32(txtetiqueta.Text)
                          select p).ToList();
        }

        if (!String.IsNullOrEmpty(txtidpedido.Text))
        {
            pagamentos = (from p in pagamentos
                          where p.pedidoId == Convert.ToInt32(txtidpedido.Text)
                          select p).ToList();
        }
        lblqtde.Text = pagamentos.Count.ToString();
        GridView1.DataSource = pagamentos;
        GridView1.DataBind();
    }

    protected void btnFiltrar_Click(object sender, EventArgs e)
    {
        carregaGrid();
    }
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        if (e.Row.RowType == DataControlRowType.DataRow)
        {



        }
    }

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "ChecarClearSale")
        {


        }

    }
}