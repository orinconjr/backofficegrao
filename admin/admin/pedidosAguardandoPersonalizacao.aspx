﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="pedidosAguardandoPersonalizacao.aspx.cs" Theme="Glass" Inherits="admin_pedidosAguardandoPersonalizacao" %>

<%@ Register TagPrefix="dxwgv" Namespace="DevExpress.Web.ASPxGridView" Assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" runat="Server">

    <asp:ScriptManager runat="server"></asp:ScriptManager>

    <table cellpadding="0" cellspacing="0" style="width: 920px">
        <tr>
            <td class="tituloPaginas" valign="top">
                <asp:Label ID="lblAcao" runat="server">Pedidos Aguardando Personalização</asp:Label>
            </td>
        </tr>
        <tr>
            <td>

                <asp:UpdatePanel runat="server">
                    <ContentTemplate>
                        <dxwgv:ASPxGridView ID="grd" runat="server" AutoGenerateColumns="False"
                            DataSourceID="sqlDS" KeyFieldName="pedidoId" Cursor="auto"
                            ClientInstanceName="grd" EnableCallBacks="False">
                            <SettingsBehavior AutoFilterRowInputDelay="10200" ConfirmDelete="True"
                                AutoExpandAllGroups="True" ColumnResizeMode="Control" AllowGroup="False"
                                AllowFocusedRow="True" />
                            <SettingsText ConfirmDelete="Tem certeza que deseja excluir?"
                                EmptyDataRow="Nenhum registro encontrado."
                                GroupPanel="Arraste uma coluna aqui para agrupar." />
                            <SettingsPager Position="TopAndBottom" PageSize="30"
                                ShowDisabledButtons="False" AlwaysShowPager="True">
                                <Summary AllPagesText="Páginas: {0} - {1} ({2} items)"
                                    Text="Página {0} de {1} ({2} registros encontrados)" />
                            </SettingsPager>
                            <Settings ShowFilterRow="True" ShowGroupedColumns="True" ShowFooter="True" />
                            <TotalSummary>
                                <dxwgv:ASPxSummaryItem DisplayFormat="0" FieldName="margem"
                                    ShowInColumn="Margem" ShowInGroupFooterColumn="Margem"
                                    SummaryType="Average" />
                            </TotalSummary>
                            <SettingsEditing EditFormColumnCount="4"
                                PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1"
                                PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter"
                                PopupEditFormWidth="700px" Mode="Inline" />
                            <Columns>
                                <dxwgv:GridViewDataTextColumn Caption="IdPedido"
                                    FieldName="pedidoId" Name="pedidoId" VisibleIndex="0"
                                    Width="100px">
                                    <Settings AutoFilterCondition="Contains" />
                                    <DataItemTemplate>
                                        <%# Eval("pedidoId") %>
                                    </DataItemTemplate>
                                </dxwgv:GridViewDataTextColumn>

                                <dxwgv:GridViewDataTextColumn Caption="itemPedidoId"
                                    FieldName="itemPedidoId" Name="itemPedidoId" VisibleIndex="1"
                                    Width="90px">
                                    <DataItemTemplate>
                                        <%#Eval("itemPedidoId") %>
                                    </DataItemTemplate>
                                </dxwgv:GridViewDataTextColumn>

                                <dxwgv:GridViewDataTextColumn Caption="IdProduto"
                                    FieldName="produtoId" Name="produtoId" VisibleIndex="2"
                                    Width="75px">
                                    <DataItemTemplate>
                                        <%#Eval("produtoId") %>
                                    </DataItemTemplate>
                                </dxwgv:GridViewDataTextColumn>

                                <dxwgv:GridViewDataTextColumn Caption="Produto"
                                    FieldName="produtoNome" Name="produtoNome" VisibleIndex="3">
                                    <DataItemTemplate>
                                        <%#Eval("produtoNome") %>
                                    </DataItemTemplate>
                                </dxwgv:GridViewDataTextColumn>

                                <dxwgv:GridViewDataTextColumn Caption="Data Pedido"
                                    FieldName="dataHoraDoPedido" Name="dataHoraDoPedido" VisibleIndex="4"
                                    Width="140px">
                                    <DataItemTemplate>
                                        <%#Eval("dataHoraDoPedido") %>
                                    </DataItemTemplate>
                                </dxwgv:GridViewDataTextColumn>

                                <dxwgv:GridViewDataComboBoxColumn Caption="Situação" FieldName="statusDoPedido" VisibleIndex="4" Width="130px" CellStyle-HorizontalAlign="Center">
                                    <PropertiesComboBox DataSourceID="sqlSituacao" TextField="situacao"
                                        ValueField="situacaoId" ValueType="System.String">
                                    </PropertiesComboBox>
                                </dxwgv:GridViewDataComboBoxColumn>

                                <dxwgv:GridViewDataHyperLinkColumn Caption="Alterar" VisibleIndex="5"
                                    Width="45px">
                                    <DataItemTemplate>
                                        <asp:HyperLink ID="HyperLink1" runat="server" Target="_blank" ImageUrl="images/btEditar-v2.jpg" ToolTip="Alterar"
                                            NavigateUrl='<%# "pedido.aspx?pedidoId=" + Eval("pedidoId") %>'></asp:HyperLink>
                                    </DataItemTemplate>
                                    <CellStyle HorizontalAlign="Center">
                                    </CellStyle>
                                </dxwgv:GridViewDataHyperLinkColumn>

                            </Columns>
                        </dxwgv:ASPxGridView>
                    </ContentTemplate>
                </asp:UpdatePanel>

                <asp:SqlDataSource ConnectionString="<%$ ConnectionStrings:connectionString %>" runat="server" ID="sqlDS"
                    SelectCommand="SELECT DISTINCT tbPedidos.pedidoId, tbItensPedido.itemPedidoId, tbItensPedido.produtoId, tbProdutos.produtoNome, tbPedidos.dataHoraDoPedido, tbPedidos.statusDoPedido
                                    FROM tbPedidos WITH (NOLOCK)
                                    INNER JOIN tbItensPedido WITH (NOLOCK) ON tbPedidos.pedidoId = tbItensPedido.pedidoId AND tbPedidos.statusDoPedido IN (2,3,11)
                                    INNER JOIN tbItemPedidoEstoque WITH (NOLOCK) ON tbItensPedido.itemPedidoId = tbItemPedidoEstoque.itemPedidoId AND tbItemPedidoEstoque.reservado = 0 AND tbItemPedidoEstoque.cancelado = 0
                                    INNER JOIN tbProdutoPersonalizacao WITH (NOLOCK) ON tbItemPedidoEstoque.produtoId = tbProdutoPersonalizacao.idProdutoPai
                                    INNER JOIN tbProdutos WITH (NOLOCK) ON tbProdutos.produtoId = tbProdutoPersonalizacao.idProdutoPai
                                    ORDER BY dataHoraDoPedido"></asp:SqlDataSource>

                <asp:ObjectDataSource ID="sqlSituacao" runat="server" SelectMethod="situacaoSeleciona" FilterExpression="situacaoId IN(2,3,11)"
                    TypeName="rnSituacao"></asp:ObjectDataSource>
            </td>
        </tr>
    </table>

</asp:Content>

