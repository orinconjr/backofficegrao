﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="comprasUnidadesDeMedida.aspx.cs" Inherits="admin_comprasUnidadesDeMedida" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" runat="Server">
    <script src="js/jquery.min.js"></script>
    <style>
        .fieldsetatualizarprecos {
            width: 760px;
            float: left;
            clear: left;
            border-radius: 5px;
            margin-left: 25px;
        }

            .fieldsetatualizarprecos input[type=checkbox] {
                margin-top: 15px;
            }

            .fieldsetatualizarprecos label {
                margin-left: 5px;
                margin-right: 20px;
            }

        .meugrid {
            background-color: #F4FAFB;
            border: 0px none;
            /* border-collapse: separate !important; */
            overflow: hidden;
            font: 9pt Tahoma;
            color: #000;
            cursor: pointer;
            white-space: nowrap;
            font-weight: normal;
            text-align: left;
            width: 834px;
            border-collapse: collapse;
            float: left;
            clear: left;
            margin-top: 10px;
            margin-bottom: 10px;
            min-height: 500px;
            margin-left: 25px;
        }

            .meugrid th {
                background-image: url(../../App_Themes/Glass/GridView/gvHeaderBackground.gif);
                background-position: center top;
                background-repeat: repeat-x;
                background-color: #C0DDE0;
                overflow: hidden;
                height: 23px;
                line-height: 23px;
                padding: 4px 6px 5px;
                font-weight: normal;
                border: 1px solid #7EACB1;
                /* border-collapse: separate;*/
            }

            .meugrid td {
                overflow: hidden;
                height: 23px;
                line-height: 23px;
                padding: 4px 6px 5px;
                border: 1px solid #7EACB1;
            }

            .meugrid tr td:nth-child(4) {
                text-align: center;
            }

            .meugrid tr td:last-of-type {
                text-align: center;
            }

        .tabrotulos {
            width: 848px;
            margin: auto;
        }

        .materiaprima {
            width: 250px;
        }

            .materiaprima ul {
                margin: 0px;
                padding-left: 10px;
            }

                .materiaprima ul li {
                    max-width: 230px;
                    white-space: pre-wrap;
                }

        .meugrid tr td:last-of-type {
            text-align: center;
        }

        .nome {
            width: 237px;
        }

            .nome div {
                max-width: 230px;
                white-space: pre-wrap;
            }

        .rotulos td {
            font: bold 12px tahoma;
            padding-top: 20px;
            color: rgb(65, 105, 126);
            padding-left: 5px;
            padding-bottom: 5px;
        }
    </style>
    <div class="tituloPaginas" valign="top">
        Compras - Unidades de Medidas
    </div>
    <div>
        <fieldset class="fieldsetatualizarprecos">
            <legend style="font-weight: bold;">Filtro</legend>

            <table class="tabrotulos">

                <tr class="rotulos">
                    <td>Id:<br />
                        <asp:TextBox runat="server" ID="txtidunidade"></asp:TextBox>
                    </td>
                    <td>Unidade de Medida:<br />
                        <asp:TextBox runat="server" ID="txtunidademedida"></asp:TextBox>
                    </td>
                    <td>Unidade Básica:<br />
                        <asp:DropDownList runat="server" ID="ddlUnidadeBasica" CssClass="campos">
                            <Items>
                                <asp:ListItem Text="Selecione" Value="0"></asp:ListItem>
                                <asp:ListItem Text="Grama" Value="1"></asp:ListItem>
                                <asp:ListItem Text="Centímetro" Value="2"></asp:ListItem>
                                <asp:ListItem Text="Unidade" Value="3"></asp:ListItem>

                            </Items>
                        </asp:DropDownList>
                    </td>

                    <td>Fator de Conversão:<br />
                        <asp:TextBox runat="server" ID="txtFatorConversao"></asp:TextBox>
                    </td>
                    <td>&nbsp;<br />
                        <asp:Button runat="server" ID="btnFiltrar" Text="Filtrar" OnClick="btnFiltrar_Click" />

                    </td>
                </tr>
            </table>
        </fieldset>
         <table class="tabrotulos">
               <tr class="rotulos">
                    <td>
                        <asp:Button runat="server" ID="btnNovaUnidadeMedida" Text="Nova Unidade de Medida" OnClick="btnNovaUnidadeMedida_Click" />
                    </td>
                    <td></td>

                    <td>&nbsp;&nbsp;
                        <asp:Button runat="server" ID="Button1" Text="Exportar Planilha" OnClick="btnExportarPlanilha_Click" />
                    </td>


                    <td></td>

                </tr>
              

            </table>
        
         <fieldset class="fieldsetatualizarprecos" runat="server" id="fieldsetnovaunidade"  Visible="False">
            <legend style="font-weight: bold;">Nova Unidade de Medida</legend>

            <table class="tabrotulos">

                <tr class="rotulos">
                  
                    <td>Unidade de Medida:<br />
                        <asp:TextBox runat="server" ID="txtunidadedemedidainserir"></asp:TextBox>
                    </td>
                    <td>Unidade Básica:<br />
                        <asp:DropDownList runat="server" ID="ddlunidadebasicainserir" CssClass="campos">
                            <Items>
                                <asp:ListItem Text="Selecione" Value="0"></asp:ListItem>
                                <asp:ListItem Text="Grama" Value="1"></asp:ListItem>
                                <asp:ListItem Text="Centímetro" Value="2"></asp:ListItem>
                                <asp:ListItem Text="Unidade" Value="3"></asp:ListItem>

                            </Items>
                        </asp:DropDownList>
                    </td>

                    <td>Fator de Conversão:<br />
                        <asp:TextBox runat="server" ID="txtfatorconversaoinserir"></asp:TextBox>
                    </td>
                    <td>&nbsp;<br />
                        <asp:Button runat="server" ID="btnSalvar" Text="Salvar" OnClick="btnSalvar_Click"  />

                    </td>

                </tr>
              

            </table>
        </fieldset>
        
        <fieldset class="fieldsetatualizarprecos" runat="server" id="fieldseteditarunidade" Visible="False">
            <legend style="font-weight: bold;">Atualizar Unidade de Medida</legend>

            <table class="tabrotulos">

                <tr class="rotulos">
                  <td>Id:<br />
                      <asp:Label ID="lblId" runat="server" Text=""></asp:Label>
                    </td>
                    <td>Unidade de Medida:<br />
                        <asp:TextBox runat="server" ID="txtunidadedemedidaeditar"></asp:TextBox>
                    </td>
                    <td>Unidade Básica:<br />
                        <asp:DropDownList runat="server" ID="ddlunidadebasicaeditar" CssClass="campos">
                            <Items>
                                <asp:ListItem Text="Selecione" Value="0"></asp:ListItem>
                                <asp:ListItem Text="Grama" Value="1"></asp:ListItem>
                                <asp:ListItem Text="Centímetro" Value="2"></asp:ListItem>
                                <asp:ListItem Text="Unidade" Value="3"></asp:ListItem>

                            </Items>
                        </asp:DropDownList>
                    </td>

                    <td>Fator de Conversão:<br />
                        <asp:TextBox runat="server" ID="txtfatorconversaoeditar"></asp:TextBox>
                    </td>
                    <td>&nbsp;<br />
                        <asp:Button runat="server" ID="btnAtualizar" Text="Atualizar" OnClick="btnAtualizar_Click"   />

                    </td>

                </tr>
              

            </table>
        </fieldset>
    </div>
    <div align="center" style="min-height: 500px; float: left; clear: left; width: 100%; ">
     <%--   <table class="meugrid" align="center">--%>

            <asp:GridView ID="GridView1" CssClass="meugrid" runat="server"  OnRowDeleting="GridView1_RowDeleting" DataKeyNames="idComprasUnidadesDeMedida" AutoGenerateColumns="False" AllowPaging="True" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowDataBound="GridView1_RowDataBound" PageSize="20">
                <Columns>

                    <asp:BoundField DataField="idComprasUnidadesDeMedida" HeaderText="Id" />
                    <asp:BoundField DataField="unidadeDeMedida" HeaderText="Unidade de Medida" />
                    <asp:TemplateField HeaderText="Unidade Básica">

                        <ItemTemplate>

                            <asp:Label ID="lblunidadebasica" runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                 
                   
                     <asp:BoundField DataField="fatorConversao" HeaderText="Fator de Conversão" />

                    <asp:TemplateField HeaderText="Editar" ShowHeader="False">
                        <ItemTemplate>
                            <a id="linkeditar" runat="server">
                                <img src="images/btEditar.jpg" alt="Editar" style="border-style: none;" />
                            </a>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Excluir" ShowHeader="False">
                        <ItemTemplate>
                            <asp:LinkButton ID="cmdDelete" runat="server" CausesValidation="False" CommandName="Delete"
                                Text="Delete">
                            <img  src="images/btExcluir.jpg" alt="Excluir o banner" style="border-style:none;"/>
                            </asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
       <%-- </table>--%>
    </div>
</asp:Content>

