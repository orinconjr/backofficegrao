﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_ImpressaoCarregamentoCaminhao : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
            usuarioLogadoId = Request.Cookies["usuarioLogadoId"];
            if (usuarioLogadoId != null)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["NumeroCarregamento"]))
                {
                    int idCarregamento = int.Parse(Request.QueryString["NumeroCarregamento"].ToString());
                    litDados.Text = GerarPackList(idCarregamento);
                    litDadosTransportadora.Text = litDados.Text;
                }
            }
            else
            {
                Response.Redirect("default.aspx");
                Response.Write("<script>alert('Você não tem acesso a essa área.');</script>");
                Response.Write("<script>window.location=('default.aspx');</script>");
            }


        }
        catch (Exception ex)
        {
            lblMensagem.Text = "Erro ao carregar os dados do carregamento: " + ex.Message;
        }
    }

    private string GerarPackList(int idCarregamento)
    {
        System.Text.StringBuilder sb = null;
        var data = new dbCommerceDataContext();
        try
        {
            var empresa = CarregarDadosEmpresa();

            var transportadoras = (from c in data.tbCarregamentoCaminhaoTransportadoras
                                   join d in data.tbTipoDeEntregas on c.idTransportadora equals d.idTransportadora
                                   where c.idCarregamentoCaminhao == idCarregamento
                                   select new
                                   {
                                       idCarregamentoTransportadora = c.idCarregamentoCaminhaoTransportadora,
                                       nome = d.razaoSocial,
                                       cnpj = d.cnpj,
                                       sequencial = c.sequencial,
                                   }).Distinct().ToList();

            var i = 0;
            sb = new System.Text.StringBuilder();

            foreach (var item in transportadoras)
            {
                int total = 0;
                sb.Append("<div style='width: 100%; page-break-after: always;' id='div" + i.ToString() + "' runat='server' class='texto'>");
                sb.Append(" <table width='100%' id='tbRemetente' class='rotulos' style='border: 1px solid #000000' class='texto' > ");
                sb.Append("     <tr>");
                sb.Append("         <td colspan='4'><b>Número de série: </b>");
                sb.Append(item.sequencial);
                sb.Append("         </td>");
                sb.Append("     <tr>");
                sb.Append("     <tr> ");
                sb.Append("         <td colspan='4'> ");
                sb.Append("             <b>Remetente</b> ");
                sb.Append("         </td> ");
                sb.Append("     </tr> ");
                sb.Append("     <tr> ");
                sb.Append("         <td>Empresa:</td> ");
                sb.Append("         <td colspan='2'> ");
                sb.Append(empresa.nomeEmitente);
                sb.Append("     </tr> ");
                sb.Append("     <tr>  ");
                sb.Append("          <td>Endereço:</td> ");
                sb.Append("          <td> ");
                sb.Append(empresa.logradouroEmitente + ", " + empresa.numeroEmitente + " - " + empresa.bairroEmitente + "</td>");
                sb.Append("          <td>Cidade / Uf.:</td> ");
                sb.Append("          <td> ");
                sb.Append(empresa.municipioEmitente + " / " + empresa.ufEmitente);
                sb.Append("          </td> ");
                sb.Append("     </tr> ");
                sb.Append("     <tr> ");
                sb.Append("         <td>Telefone:</td> ");
                sb.Append("         <td> ");
                sb.Append(empresa.foneEmitente);
                sb.Append("         <td>Cep.:</td> ");
                sb.Append("         <td> ");
                sb.Append(empresa.cepEmitente);
                sb.Append("         </td> ");
                sb.Append("     </tr> ");
                sb.Append("     <tr> ");
                sb.Append("         <td colspan= '4'>&nbsp;</td>");
                sb.Append("     </tr>");
                sb.Append("     <tr>");
                sb.Append("         <td colspan='4'> ");
                sb.Append("             <b>Destinatário</b>");
                sb.Append("         </td>");
                sb.Append("     </tr>");
                sb.Append("     <tr>");
                sb.Append("         <td>Empresa:</td>");
                sb.Append("         <td colspan='2'>");
                sb.Append(item.nome + " - " + item.cnpj);
                sb.Append("         </td>");
                sb.Append("     </tr>");
                sb.Append(" </table>");
                sb.Append("<br>");
                sb.Append(" <table style='width: 100%;' cellpadding='0' cellspacing='0' border='1'>");
                sb.Append("     <tr>");
                sb.Append("         <td style='width: 10%'>NF</td>");
                sb.Append("         <td style='width: 30%'>Nome</td>");
                sb.Append("         <td style='width: 10%'>Valor</td>");
                sb.Append("         <td style='width: 10%'>Volumes</td>");
                sb.Append("         <td style='width: *%'>Detalhes</td>");
                sb.Append("     </tr>");


                var pacotes = (from a in data.tbCarregamentoCaminhaoPacote
                               join b in data.tbPedidoPacotes on a.idPedidoPacote equals b.idPedidoPacote
                               join c in data.tbPedidoEnvios on b.idPedidoEnvio equals c.idPedidoEnvio
                               join d in data.tbPedidos on c.idPedido equals d.pedidoId
                               join e in data.tbClientes on d.clienteId equals e.clienteId
                               where a.idCarregamentoCaminhaoTransportadora == item.idCarregamentoTransportadora
                               select new
                               {
                                   c.idPedidoEnvio,
                                   e.clienteId,
                                   e.clienteNome,
                                   c.nfeNumero,
                                   c.nfeValor,
                                   a.tbCarregamentoCaminhaoTransportadora.tbCarregamentoCaminhao.nomeMotorista,
                                   a.tbCarregamentoCaminhaoTransportadora.tbCarregamentoCaminhao.placaCaminhao
                               }
                               ).Distinct().OrderByDescending(x=> x.nfeNumero).ToList();


                foreach (var pacote in pacotes)
                {
                    var counter = (from a in data.tbCarregamentoCaminhaoPacote
                                   join b in data.tbPedidoPacotes on a.idPedidoPacote equals b.idPedidoPacote
                                   join c in data.tbPedidoEnvios on b.idPedidoEnvio equals c.idPedidoEnvio
                                   where c.idPedidoEnvio == pacote.idPedidoEnvio
                                   select new
                                   {
                                       b.idPedidoEnvio,
                                       detalhes = string.Join(",", (from g in data.tbPedidoPacotes where g.idPedidoEnvio == c.idPedidoEnvio select g.peso.ToString() + '(' + g.largura.ToString() + 'x' + g.altura.ToString() + 'x' + g.profundidade.ToString() + ')').Distinct().ToArray())
                                   }).ToList();

                    sb.Append("     <tr>");
                    sb.Append("         <td style='width: 10%'>" + pacote.nfeNumero + "</td>");
                    sb.Append("         <td style='width: 30%'>" + pacote.clienteNome + "</td>");
                    sb.Append("         <td style='width: 10%'>" + pacote.nfeValor + "</td>");
                    sb.Append("         <td style='width: 10%'>" + counter.Count() + "</td>");
                    sb.Append("         <td style='width: *%'>" + counter.First().detalhes + "</td>");
                    sb.Append("     </tr>");

                    total += counter.Count();
                }

                var Nfs = pacotes.GroupBy(x => x.nfeNumero).Count();

                sb.Append("<tr border='1'>");
                sb.Append("     <td colspan='4'>&nbsp;</td>");
                sb.Append("     <td><b>Total Volumes</b>:");
                sb.Append(total.ToString());
                sb.Append("     | <b>Total de Notas Fiscais</b>:");
                sb.Append(Nfs.ToString());
                sb.Append("     </td>");
                sb.Append("</tr>");
                sb.Append("</table>");
                sb.Append("<br>");

                if (pacotes != null && pacotes.Count() > 0)
                {
                    sb.Append("<table style='width: 100%' class='texto'>");
                    sb.Append("     <tr>");
                    sb.Append("         <td>Placa:");
                    sb.Append(pacotes.FirstOrDefault().placaCaminhao);
                    sb.Append("          </td>");
                    sb.Append("          <td>Nome do motorista: ");
                    sb.Append(pacotes.FirstOrDefault().nomeMotorista);
                    sb.Append("          </td>");
                    sb.Append("          <td>Nº documento: ");
                    sb.Append("          </td>");
                    sb.Append("     </tr>");
                    sb.Append("     <tr>");
                    sb.Append("          <td>Data:</td>");
                    sb.Append("          <td colspan='2'>Assinatura do motorista:</td>");
                    sb.Append("     </tr>");
                    sb.Append("</table>");
                }
                sb.Append("<br>");
                sb.Append("</div>");
                i++;
            }
            return sb.ToString();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private tbEmpresa CarregarDadosEmpresa()
    {
        var data = new dbCommerceDataContext();
        try
        {
            return (from c in data.tbEmpresas where c.idEmpresa == 6 select c).FirstOrDefault();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
}