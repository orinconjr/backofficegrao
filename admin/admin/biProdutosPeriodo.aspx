﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="biProdutosPeriodo.aspx.cs" Inherits="admin_biProdutosPeriodo" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.ASPxPivotGrid.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPivotGrid" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" Runat="Server">
    <table cellpadding="0" cellspacing="0" style="width: 920px">
        <tr>
            <td class="tituloPaginas" valign="top">
                <asp:Label ID="lblAcao" runat="server">BI - Itens por Periodo</asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
                    <tr>
                        <td align="right">
                            <table cellpadding="0" cellspacing="0" style="width: 834px">
                                <tr>
                    <td>
                        <table cellpadding="0" cellspacing="0" style="width: 834px">
                            <tr>
                                <td class="rotulos" style="width: 100px">
                                    Data inicial 1<br />
                                                <asp:TextBox ID="txtDataInicial" runat="server" CssClass="campos" 
                                                                    Width="90px" MaxLength="10"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rqvDataInicial" runat="server" 
                                                                    ControlToValidate="txtDataInicial" Display="None" 
                                                                    ErrorMessage="Preencha a data inicial." 
                                        SetFocusOnError="True"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="rgeDataInicial" runat="server" 
                                                    ControlToValidate="txtDataInicial" Display="None" 
                                                    ErrorMessage="Por favor, preencha corretamente a data inicial." 
                                                    SetFocusOnError="True" 
                                        ValidationExpression="^(([0-2]\d|[3][0-1])\/([0]\d|[1][0-2])\/[1-2][0-9]\d{2})$"></asp:RegularExpressionValidator>
                            
                                </td>
                                <td class="rotulos" style="width: 100px">
                                    Data final 1<br  />
                                    <asp:TextBox ID="txtDataFinal" runat="server" CssClass="campos" 
                                                        Width="90px" MaxLength="10"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rqvDataFinal" runat="server" 
                                                        ControlToValidate="txtDataFinal" Display="None" 
                                                        ErrorMessage="Preencha a data final." 
                                        SetFocusOnError="True"></asp:RequiredFieldValidator>
                                    <b>
                                    <asp:RegularExpressionValidator ID="rgeDataInicial0" runat="server" 
                                                    ControlToValidate="txtDataFinal" Display="None" 
                                                    ErrorMessage="Por favor, preencha corretamente a data final" 
                                                    SetFocusOnError="True" 
                                            
                                            
                                        ValidationExpression="^(([0-2]\d|[3][0-1])\/([0]\d|[1][0-2])\/[1-2][0-9]\d{2})$"></asp:RegularExpressionValidator>
                                    </b>
                                </td>
                                <td valign="bottom">

                                </td>
                            </tr>
                            <tr>
                                <td class="rotulos" style="width: 100px">
                                    Data inicial 1<br />
                                                <asp:TextBox ID="txtDataInicial1" runat="server" CssClass="campos" 
                                                                    Width="90px" MaxLength="10"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                                                    ControlToValidate="txtDataInicial" Display="None" 
                                                                    ErrorMessage="Preencha a data inicial." 
                                        SetFocusOnError="True"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                                                    ControlToValidate="txtDataInicial1" Display="None" 
                                                    ErrorMessage="Por favor, preencha corretamente a data inicial." 
                                                    SetFocusOnError="True" 
                                        ValidationExpression="^(([0-2]\d|[3][0-1])\/([0]\d|[1][0-2])\/[1-2][0-9]\d{2})$"></asp:RegularExpressionValidator>
                            
                                </td>
                                <td class="rotulos" style="width: 100px">
                                    Data final 2<br  />
                                    <asp:TextBox ID="txtDataFinal1" runat="server" CssClass="campos" 
                                                        Width="90px" MaxLength="10"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                                                        ControlToValidate="txtDataFinal" Display="None" 
                                                        ErrorMessage="Preencha a data final." 
                                        SetFocusOnError="True"></asp:RequiredFieldValidator>
                                    <b>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" 
                                                    ControlToValidate="txtDataFinal1" Display="None" 
                                                    ErrorMessage="Por favor, preencha corretamente a data final" 
                                                    SetFocusOnError="True" 
                                            
                                            
                                        ValidationExpression="^(([0-2]\d|[3][0-1])\/([0]\d|[1][0-2])\/[1-2][0-9]\d{2})$"></asp:RegularExpressionValidator>
                                    </b>
                                </td>
                                <td valign="bottom">
                                    <asp:ImageButton ID="btnPesquisar" runat="server" ImageUrl="~/admin/images/btPesquisar.jpg" OnClick="btnPequisar_Click" />
                                    <asp:ValidationSummary ID="llds" runat="server" DisplayMode="List" ShowMessageBox="True" ShowSummary="False" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                                <tr>
                                    <td align="right">
                                        &nbsp;</td>
                                    <td height="38" 
                                        style="background-image: url('images/bkExportar.jpg'); padding-bottom: 1px;" 
                                        valign="bottom" width="231">
                                        <%--<table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 97px">
                                                    &nbsp;</td>
                                                <td style="width: 33px">
                                                    <asp:ImageButton ID="btPdf" runat="server" ImageUrl="~/admin/images/btPdf.jpg"  OnClick="btPdf_Click"
                                                        style="margin-left: 5px" />
                                                </td>
                                                <td style="width: 31px">
                                                    <asp:ImageButton ID="btXsl" runat="server" ImageUrl="~/admin/images/btXls.jpg" OnClick="btXsl_Click"
                                                        style="margin-left: 5px" />
                                                </td>
                                                <td style="width: 36px">
                                                    <asp:ImageButton ID="btRtf" runat="server" ImageUrl="~/admin/images/btRtf.jpg" OnClick="btRtf_Click"
                                                        style="margin-left: 5px" />
                                                </td>
                                                <td valign="bottom">
                                                    <asp:ImageButton ID="btCsv" runat="server" ImageUrl="~/admin/images/btCsv.jpg" OnClick="btCsv_Click"
                                                        style="margin-left: 5px" />
                                                </td>
                                            </tr>
                                        </table>--%>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <dx:ASPxPivotGrid OptionsData-DataFieldUnboundExpressionMode="UseSummaryValues" CustomizationFieldsLeft="300" CustomizationFieldsTop="400" OnCustomUnboundFieldData="grd_CustomUnboundFieldData" ID="grd" runat="server"  ClientInstanceName="pivotGrid" Width="1300px">
                                <Fields>
                                    <dx:PivotGridField Area="FilterArea" FieldName="itemValor" Caption="Valor Geral" ID="ValorGeral"  />
                                    <dx:PivotGridField Area="FilterArea" FieldName="itemValor" Caption="Valor Medio" ID="ValorMedio" SummaryType="Average"  />
                                    <dx:PivotGridField Area="FilterArea" FieldName="fornecedorNome" Caption="Fornecedor" ID="Fornecedor" />
                                    <dx:PivotGridField Area="FilterArea" FieldName="marcaNome" Caption="Marca" ID="Marca" />
                                    <dx:PivotGridField Area="FilterArea" FieldName="itemQuantidade" Caption="Quantidade" ID="Quantidade" />
                                    <dx:PivotGridField Area="FilterArea" FieldName="valorCusto" Caption="Custo" ID="Custo" />
                                    <dx:PivotGridField Area="FilterArea" FieldName="produtoId" Caption="Id do Produto" ID="IdDoProduto" />
                                    <dx:PivotGridField Area="FilterArea" FieldName="pedidoId" Caption="Id do Pedido" ID="IdDoPedido" />
                                    <dx:PivotGridField Area="FilterArea" FieldName="dataHoraDoPedido" Caption="Hora do Pedido" ID="HoraPedido" GroupInterval="Hour"  />
                                    <dx:PivotGridField Area="FilterArea" FieldName="categoria" Caption="Categoria" ID="Categoria" />
                                    <dx:PivotGridField Area="FilterArea" FieldName="colecao" Caption="Coleção" ID="Colecao" />
                                    <dx:PivotGridField Area="FilterArea" FieldName="dataHoraDoPedido" Caption="Data do Pedido" ID="DataPedido" GroupInterval="Date"  />
                                    <dx:PivotGridField Area="FilterArea" Caption="TicketMedioP1" ID="TicketMedioP1" UnboundExpression="Iif([QuantidadeP1]>0, ([ValorP1] / [QuantidadeP1]),0)" FieldName="ticketMedioP1" UnboundType="Decimal" UnboundFieldName="ticketMedioP1" CellFormat-FormatString="{0:C}"  CellFormat-FormatType="Numeric" />
                                    <dx:PivotGridField Area="FilterArea" Caption="TicketMedioP2" ID="TicketMedioP2" UnboundExpression="Iif([QuantidadeP2]>0, ([ValorP2] / [QuantidadeP2]),0)" FieldName="ticketMedioP2" UnboundType="Decimal" UnboundFieldName="ticketMedioP2" CellFormat-FormatString="{0:C}"  CellFormat-FormatType="Numeric" />

                                    <dx:PivotGridField Area="RowArea" FieldName="produtoNome" Caption="Nome do Produto" ID="NomeDoProduto"  />
                                    <dx:PivotGridField Area="ColumnArea" FieldName="status" Caption="Status" ID="Status"  />

                                    
                                    <dx:PivotGridField Area="DataArea" FieldName="itemQuantidadePeriodo1" Caption="Quantidade P1" ID="QuantidadeP1"  />
                                    <dx:PivotGridField Area="DataArea" FieldName="itemQuantidadePeriodo2" Caption="Quantidade P2" ID="QuantidadeP2"  />
                                    <dx:PivotGridField Area="DataArea" Caption="VariacaoQ" ID="VariacaoQuantidade" UnboundExpression="Iif([QuantidadeP1]>0, ((([QuantidadeP2] * 100) / [QuantidadeP1]) - 100),0)" FieldName="variacaoQuantidade" UnboundType="Decimal" UnboundFieldName="variacaoQuantidade" CellFormat-FormatString="{0:0.##}%"  CellFormat-FormatType="Numeric" />                            
                                    <dx:PivotGridField Area="DataArea" Caption="DiferencaQ" ID="DiferencaQtd" UnboundExpression="[QuantidadeP2] - [QuantidadeP1]" FieldName="diferencaQuantidade" UnboundType="Decimal" UnboundFieldName="diferencaQuantidade" CellFormat-FormatString="{0:0.##}"  CellFormat-FormatType="Numeric" />
                                    <dx:PivotGridField Area="DataArea" FieldName="itemValorPeriodo1" Caption="Valor P1" ID="ValorP1"  />
                                    <dx:PivotGridField Area="DataArea" FieldName="itemValorPeriodo2" Caption="Valor P2" ID="ValorP2"  />
                                    <dx:PivotGridField Area="DataArea" Caption="DiferencaV" ID="DiferencaValor" UnboundExpression="[ValorP2] - [ValorP1]" FieldName="diferencaValor" UnboundType="Decimal" UnboundFieldName="diferencaValor" CellFormat-FormatString="{0:C}"  CellFormat-FormatType="Numeric" />
                                    <dx:PivotGridField Area="DataArea" Caption="VariacaoV" ID="VariacaoValor" UnboundExpression="Iif([ValorP1]>0, ((([ValorP2] * 100) / [ValorP1]) - 100),0)" FieldName="variacaoValor" UnboundType="Decimal" UnboundFieldName="variacaoValor" CellFormat-FormatString="{0:0.##}%"  CellFormat-FormatType="Numeric" />
                                </Fields>
                                <OptionsView ShowFilterHeaders="True" ShowHorizontalScrollBar="True" ShowColumnGrandTotals="False"  ShowRowGrandTotals="False" ShowRowTotals="False" />
                                <OptionsCustomization AllowSortBySummary="True" AllowSort="True" AllowFilterInCustomizationForm="True"></OptionsCustomization>
                                <OptionsPager RowsPerPage="100"></OptionsPager>
                            </dx:ASPxPivotGrid>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>