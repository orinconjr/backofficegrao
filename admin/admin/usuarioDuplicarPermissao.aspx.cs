﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using SpreadsheetLight;
using System.Web.UI.HtmlControls;
public partial class admin_usuarioDuplicarPermissao : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            carregaDdlUsuarioOrigem();
            // carregaGrid();
        }

    }
    void carregaGrid()
    {
        var data = new dbCommerceDataContext();
        int usuarioId = Convert.ToInt32(ddlUsuarioOrigem.SelectedItem.Value);
        var permissoes = (from c in data.tbUsuarioPaginasPermitidas
                          where c.usuarioId == usuarioId
                          select new
                          {
                              c.usuarioId,
                              c.paginaPermitidaNome
                          }
                         ).ToList();
        if (permissoes.Count == 0)
        {
            grdpermissoesusuario.DataSource = null;
        }
        else
        {
            grdpermissoesusuario.DataSource = permissoes;
        }
        grdpermissoesusuario.DataBind();

    }
    void carregaDdlUsuarioOrigem()
    {
        ddlUsuarioOrigem.Items.Clear();
        ddlUsuarioOrigem.Items.Add(new ListItem("Selecione", "0"));

        var data = new dbCommerceDataContext();
        var usuarios = (from c in data.tbUsuarios
                        where !c.usuarioNome.ToLower().Contains("inativo")
                        select new
                        {
                            c.usuarioId,
                            c.usuarioNome
                        }).ToList().OrderBy(x => x.usuarioNome);

        foreach (var usuario in usuarios)
        {
            ddlUsuarioOrigem.Items.Add(new ListItem(usuario.usuarioNome, usuario.usuarioId.ToString()));
        }
    }

    protected void grdpermissoesusuario_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grdpermissoesusuario.PageIndex = e.NewPageIndex;
        carregaGrid();
    }


    bool validarcampos()
    {
        string erro = "";
        bool valido = true;

        if (ddlUsuarioOrigem.SelectedItem.Value == "0")
        {
            erro += @"Selecione o usuário de origem\n";
            valido = false;
        }

        if (String.IsNullOrEmpty(txtusuarionovo.Text))
        {
            erro += @"Informe o novo usuário\n";
            valido = false;
        }

        if (String.IsNullOrEmpty(txtsenha.Text))
        {
            erro += @"Informe a senha do novo usuário\n";
            valido = false;
        }

        if (erro != "")
        {
            string meuscript = @"alert('" + erro + "');";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "MyScript", meuscript, true);

        }
        return valido;
    }


    protected void bntBuscar_Click(object sender, EventArgs e)
    {
        carregaGrid();
    }
    protected void btnSalvar_Click(object sender, EventArgs e)
    {
        if (!validarcampos())
            return;
   
        var data = new dbCommerceDataContext();
       
       bool usuariojaexiste = (from c in data.tbUsuarios where c.usuarioNome == txtusuarionovo.Text select c).Any();
       if (usuariojaexiste)
        {
            string meuscript = @"alert('Usuário já existe. Selecione outro nome para login.');";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "MyScript", meuscript, true);
            return;
        }

        tbUsuario usuario = new tbUsuario();
        usuario.usuarioNome = txtusuarionovo.Text;
        usuario.usuarioSenha = txtsenha.Text;
        usuario.dataDaCriacao = DateTime.Now;
        data.tbUsuarios.InsertOnSubmit(usuario);
        try
        {
            data.SubmitChanges();
        }
        catch (Exception) { }

        var paginasaincluir = (from c in data.tbUsuarioPaginasPermitidas
                               where c.usuarioId == Convert.ToInt32(ddlUsuarioOrigem.SelectedItem.Value)
                               select new
                               {
                                   c.usuarioId,
                                   c.paginaPermitidaNome
                               }).ToList();
        if(usuario.usuarioId != null)
        {
            string mensagem = "";
            try
            {
                foreach (var pagina in paginasaincluir)
                {
                    tbUsuarioPaginasPermitida usuariopaginapermitida = new tbUsuarioPaginasPermitida();
                    usuariopaginapermitida.usuarioId = usuario.usuarioId;
                    usuariopaginapermitida.paginaPermitidaNome = pagina.paginaPermitidaNome;
                    usuariopaginapermitida.dataDaCriacao = DateTime.Now;
                    data.tbUsuarioPaginasPermitidas.InsertOnSubmit(usuariopaginapermitida);
                    try
                    {
                        data.SubmitChanges();
                    }
                    catch (Exception) { }
                }

                mensagem = "Usuário " + txtusuarionovo.Text + " criado com sucesso com as mesmas permissões do usuário " + ddlUsuarioOrigem.SelectedItem.Text;
            }
            catch (Exception ex) {
                mensagem = ex.Message;
            }
            string meuscript = @"alert('"+mensagem+"');";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "MyScript", meuscript, true);
        }
    }
}