﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="bannerAlt.aspx.cs" Theme="Glass" Inherits="admin_bannerAlt" %>

<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxEditors" Assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>
<%@ Register TagPrefix="dxwgv" Namespace="DevExpress.Web.ASPxGridView" Assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>

<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxClasses" Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxTabControl" Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" runat="Server">
    <asp:ScriptManager runat="server"></asp:ScriptManager>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="js/jquery.plugin.js"></script>
    <script src="js/jquery.countdown.js"></script>
    
    <script>

        function previewFileUpload(input) {

            var img = $("#ctl00_conteudoPrincipal_imgBannerAtual");

            if (input.files && input.files[0]) {

                var reader = new FileReader();
                reader.onload = function (e) {
                    $("#ctl00_conteudoPrincipal_imgBannerAtual").attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        function setTimer(idElemento, data) {
            var dataAgendamento = new Date(data);
            var austDay = new Date();
            austDay = new Date(austDay.getFullYear() + 1, 1 - 1, 26);
            $('#' + idElemento + '').countdown({ until: dataAgendamento, compact: true, description: '' });
            $('#year').text(dataAgendamento.getFullYear());
        }
    </script>

    <style>
        fieldset {
            display: block;
            margin-left: 2px;
            margin-right: 2px;
            padding-top: 0.35em;
            padding-bottom: 0.625em;
            padding-left: 0.75em;
            padding-right: 0.75em;
            border: 2px solid #708090;
            height: 85px;
        }

            fieldset legend {
                font-weight: bold;
            }
    </style>

    <table cellpadding="0" cellspacing="0" style="width: 920px">
        <tr>
            <td class="tituloPaginas" valign="top">
                <asp:Label ID="lblAcao" runat="server">Banner Alteração</asp:Label>
            </td>
        </tr>
        <tr>
            <td>        
                <dx:ASPxPageControl ID="tabsBanners" runat="server" ActiveTabIndex="0" Width="845px">
                    <TabPages>
                        <dx:TabPage Text="Geral">
                            <ContentCollection>
                                <dx:ContentControl runat="server">
                                    
                                    <table cellpadding="0" cellspacing="0" style="width: 920px">
                                        <tr class="rotulos">
                                            <td colspan="2" style="padding-left: 36px;">
                                                <asp:TextBox ID="txtIdProduto" runat="server" Visible="false"></asp:TextBox>
                                                <asp:Image ID="imgBannerAtual" runat="server" AlternateText="" Style="max-width: 860px;" /><br />
                                                Banner<br />
                                                <asp:FileUpload ID="FileUploadBanner" runat="server" onchange="previewFileUpload(this)" Width="97%" ValidationGroup="cadastro" /><br />
                                            </td>
                                        </tr>
                                        <tr class="rotulos">
                                            <td colspan="2" style="padding-left: 36px; padding-top: 20px; padding-bottom: 10px;">
                                                <asp:CheckBox ID="ckbAtivo" Text="Ativo" runat="server" />&nbsp;&nbsp;&nbsp;<asp:CheckBox ID="ckbBannerMobile" Text="Mobile" runat="server" />
                                            </td>
                                        </tr>
                                        <tr class="rotulos">
                                            <td style="padding-left: 36px;">
                                                Link do Produto (opcional)<br />
                                                <asp:TextBox ID="txtLinkProduto" runat="server" Width="97%" MaxLength="300"></asp:TextBox>
                                            </td>
                                            <td rowspan="3" style="vertical-align: top;" class="produto-box" id="produtobox" runat="server">
                                                <p> Mostrar apenas nos seguintes produtos</p>
                                                <input type="text" id="txtBuscaProduto" name="txtBuscaProduto" maxlength="200" placeholder="Buscar Produto" style="width:99%"/>
                                                <ul class="selectedProducts"></ul>
                                            </td>
                                            <td rowspan="3" style="vertical-align: top;" class="categoria-box" id="categoriabox" runat="server">
                                                <p> Mostrar apenas nas seguintes categorias</p>
                                                <p class="alert-mini">(deixar em branco para mostrar em TODAS)</p>
                                                <input type="text" id="txtBusca" name="txtBusca" maxlength="200" placeholder="Buscar Categoria" style="width:99%"/>
                                                <ul class="selectedCategories"></ul>
                                            </td>
                                        </tr>
                                        <tr class="rotulos">
                                            <td style="padding-left: 36px;">
                                                <table style="border-spacing: 0;">
                                                    <tr class="rotulos">
                                                        <td>
                                                            Ordem<br />
                                                            <asp:TextBox ID="txtRelevancia" runat="server" MaxLength="3" Width="50px" Style="text-align: center"></asp:TextBox>
                                                        </td>
                                                        <td style="padding-left: 30px;">
                                                            Local<br />
                                                            <asp:DropDownList ID="ddlLocal" runat="server" Width="172" Height="22" AutoPostBack="true" OnSelectedIndexChanged="ddlLocal_SelectedIndexChanged">
                                                                <asp:ListItem Text="Home" Value="1" />
                                                                <asp:ListItem Text="Detalhes Produto" Value="2" />
                                                                <asp:ListItem Text="Busca" Value="3" />
                                                                <asp:ListItem Text="Lista Categoria" Value="4" />
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td style="padding-left: 30px;">
                                                            Posição<br />
                                                            <asp:DropDownList ID="ddlPosicao" runat="server" Width="172" Height="22">
                                                                <asp:ListItem Text="Topo" Value="1" />
                                                                <asp:ListItem Text="Direita" Value="2" />
                                                                <asp:ListItem Text="Rodapé" Value="3" />
                                                                <asp:ListItem Text="Esquerda" Value="4" />
                                                                <asp:ListItem Text="Conteudo Home - Topo" Value="5" />
                                                                <asp:ListItem Text="Conteudo Home - Rodapé" Value="6" />
                                                                <asp:ListItem Text="Topo Split" Value="7" />
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr class="rotulos">
                                            <td style="padding-left: 36px; padding-top: 20px;">
                                                <table>
                                                    <tr class="rotulos">
                                                        <td>
                                                            Layout:<br />
                                                            <asp:DropDownList runat="server" ID="ddlLayoutContador" DataTextField="nome" DataValueField="id" Style="float: left;" Width="306"/>

                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" style="text-align: right; padding: 40px 23px;">
                                                <br />
                                                <div style="margin:0 auto; position:relative; width: fit-content;">
                                                    <asp:ImageButton ID="btnSalvar" runat="server" ImageUrl="images/btSalvar.jpg" AlternateText="Salvar" ValidationGroup="cadastro" ToolTip="Salvar Banner" OnClick="btnSalvar_Click" CommandName="Salvar" Style="cursor: pointer;" /><br />
                                                </div>
                                                <div style="float: left; margin-top: -37px; margin-left: 23px;">
                                                    <asp:ImageButton ID="btnExcluir" runat="server" ImageUrl="images/excluir-x-grande.png" AlternateText="Salvar" Width="35" ToolTip="Excluir Banner" ValidationGroup="cadastro" OnClick="btnExcluir_Click" OnClientClick="return confirm('Deseja realmente excluir o banner?')" />
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </dx:ContentControl>
                            </ContentCollection>
                        </dx:TabPage>
                        <dx:TabPage Text="Agendamentos">
                            <ContentCollection>
                                <dx:ContentControl runat="server">
                                    <table>
                                        <tr>
                                            <td>
                                                <table>
                                                    <tr class="rotulos">
                                                        <td style="padding-left: 36px; padding-top: 20px;">
                                                            <table style="width: 100%;">
                                                                <td class="rotulos">
                                                                    <td >
                                                                        <asp:CheckBox ID="ckbAgendamentoAtivacao" Text="Agendar Ativação" runat="server" /><br />
                                                                        <div style="width: 350px;">
                                                                            <div style="float: left; width: 370px;">
                                                                                <div style="float: left; width: 186px;">Data:</div>
                                                                                <div style="float: left;">Hora:</div>
                                                                                <dx:ASPxDateEdit ID="txtAgendamentoDataAtivacao" runat="server" Style="float: left;">
                                                                                </dx:ASPxDateEdit>
                                                                                <dx:ASPxTimeEdit ID="txtAgendamentoHoraAtivacao" runat="server" Style="float: left; margin-left: 15px;">
                                                                                </dx:ASPxTimeEdit>
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                    <td style="padding-left: 10px;">
                                                                        <asp:CheckBox ID="ckbAgendamentoDesativacao" Text="Agendar Desativação" runat="server" /><br />
                                                                        <div style="width: 350px;">
                                                                            <div style="float: left; width: 370px;">
                                                                                <div style="float: left; width: 186px;">Data:</div>
                                                                                <div style="float: left;">Hora:</div>
                                                                                <dx:ASPxDateEdit ID="txtAgendamentoDataDesativacao" runat="server" Style="float: left;">
                                                                                </dx:ASPxDateEdit>
                                                                                <dx:ASPxTimeEdit ID="txtAgendamentoHoraDesativacao" runat="server" Style="float: left; margin-left: 15px;">
                                                                                </dx:ASPxTimeEdit>
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                    <td style="padding-left: 10px;">
                                                                        &nbsp;<br />
                                                                        Ordem<br />
                                                                        <asp:TextBox ID="txtOrdemAgendamento" runat="server" MaxLength="3" Width="50px" Style="text-align: center"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr class="rotulos">
                                                        <td style="padding-left: 36px; padding-top: 10px;">
                                                            <div style="float: left; width: 500px; margin-top: 10px;">
                                                                <asp:CheckBox ID="ckbContador" Text="Contador" runat="server" /><br />
                                                            </div>
                                                            <div style="width: 400px;">
                                                                <div style="float: left; width: 370px">
                                                                    <div style="float: left; width: 186px;">Data Início:</div>
                                                                    <div style="float: left;">Hora Início:</div>
                                                                    <dx:ASPxDateEdit ID="txtInicioContadorData" runat="server" Style="float: left;">
                                                                    </dx:ASPxDateEdit>
                                                                    <dx:ASPxTimeEdit ID="txtInicioContadorHora" runat="server" Style="float: left; margin-left: 15px;">
                                                                    </dx:ASPxTimeEdit>

                                                                    <div style="float: left; width: 186px;">Data Fim:</div>
                                                                    <div style="float: left;">Hora Fim:</div>
                                                                    <dx:ASPxDateEdit ID="txtFimContadorData" runat="server" Style="float: left;">
                                                                    </dx:ASPxDateEdit>
                                                                    <dx:ASPxTimeEdit ID="txtFimContadorHora" runat="server" Style="float: left; margin-left: 15px;">
                                                                    </dx:ASPxTimeEdit>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding-left: 36px; padding-top: 10px;">
                                                            <asp:Button runat="server" ID="btnAdicionarAgendamento" OnClick="btnAdicionarAgendamento_Click" Text="Adicionar Agendamento" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="padding-top: 30px;">                                                
                                                <dxwgv:ASPxGridView ID="grd" runat="server" AutoGenerateColumns="False"
                                                    KeyFieldName="idBannerAgendamento" Cursor="auto" OnCustomUnboundColumnData="grd_OnCustomUnboundColumnData"
                                                    ClientInstanceName="grd" EnableCallBacks="False" OnHtmlRowPrepared="grd_OnHtmlRowPrepared">
                                                    <SettingsBehavior AutoFilterRowInputDelay="10200" ConfirmDelete="True"
                                                        AutoExpandAllGroups="True" ColumnResizeMode="Control" AllowGroup="False"
                                                        AllowFocusedRow="True" />
                                                    <SettingsText ConfirmDelete="Tem certeza que deseja excluir?" EmptyDataRow="Nenhum registro encontrado." GroupPanel="Arraste uma coluna aqui para agrupar." />
                                                    <SettingsPager Position="TopAndBottom" PageSize="30" ShowDisabledButtons="False" AlwaysShowPager="True">
                                                        <Summary AllPagesText="Páginas: {0} - {1} ({2} items)" Text="Página {0} de {1} ({2} registros encontrados)" />
                                                    </SettingsPager>
                                                    <Settings ShowFilterRow="True" ShowGroupedColumns="True" ShowFooter="True" />
                                                    <TotalSummary>
                                                        <dxwgv:ASPxSummaryItem DisplayFormat="0" FieldName="margem" ShowInColumn="Margem" ShowInGroupFooterColumn="Margem" SummaryType="Average" />
                                                    </TotalSummary>
                                                    <SettingsEditing EditFormColumnCount="4"
                                                        PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1"
                                                        PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter"
                                                        PopupEditFormWidth="700px" Mode="Inline" />
                                                    <Columns>
                                                        <dxwgv:GridViewDataTextColumn Caption="ID Banner"
                                                            FieldName="idBannerAgendamento" Name="Id" VisibleIndex="3" Visible="false"
                                                            Width="100px">
                                                            <Settings AutoFilterCondition="Contains" />
                                                            <DataItemTemplate>
                                                                <asp:TextBox ID="txtIdDoBanner" runat="server" BorderStyle="None"
                                                                    CssClass="campos" Text='<%# Bind("id") %>' Width="100%"></asp:TextBox>
                                                            </DataItemTemplate>
                                                        </dxwgv:GridViewDataTextColumn>
                                                        <dxwgv:GridViewDataTextColumn Caption="Data Entrada" FieldName="dataEntrada" CellStyle-HorizontalAlign="Center" UnboundType="String"
                                                            Width="50px">
                                                            <DataItemTemplate>
                                                                <asp:Label ID="lblTempoParaAtivacao" runat="server" Text='<%# Eval("dataEntrada") %>'></asp:Label>
                                                            </DataItemTemplate>
                                                        </dxwgv:GridViewDataTextColumn>
                                                        <dxwgv:GridViewDataTextColumn Caption="Data Saída" FieldName="dataSaida" CellStyle-HorizontalAlign="Center" UnboundType="String"
                                                            Width="50px">
                                                            <DataItemTemplate>
                                                                <asp:Label ID="lblTempoParaAtivacao" runat="server" Text='<%# Eval("dataSaida") %>'></asp:Label>
                                                            </DataItemTemplate>
                                                        </dxwgv:GridViewDataTextColumn>
                                                        <dxwgv:GridViewDataTextColumn Caption="Inicio Contador" FieldName="dataSaida" CellStyle-HorizontalAlign="Center" UnboundType="String"
                                                            Width="50px">
                                                            <DataItemTemplate>
                                                                <asp:Label ID="lblTempoParaAtivacao" runat="server" Text='<%# Eval("inicioContador") %>'></asp:Label>
                                                            </DataItemTemplate>
                                                        </dxwgv:GridViewDataTextColumn>
                                                        <dxwgv:GridViewDataTextColumn Caption="Fim Contador" FieldName="dataSaida" CellStyle-HorizontalAlign="Center" UnboundType="String"
                                                            Width="50px">
                                                            <DataItemTemplate>
                                                                <asp:Label ID="lblTempoParaAtivacao" runat="server" Text='<%# Eval("fimContador") %>'></asp:Label>
                                                            </DataItemTemplate>
                                                        </dxwgv:GridViewDataTextColumn>
                                                        <dxwgv:GridViewDataTextColumn Caption="Posicao" FieldName="posicao" CellStyle-HorizontalAlign="Center" UnboundType="String"
                                                            Width="50px">
                                                            <DataItemTemplate>
                                                                <asp:Label ID="lblTempoParaAtivacao" runat="server" Text='<%# Eval("posicao") %>'></asp:Label>
                                                            </DataItemTemplate>
                                                        </dxwgv:GridViewDataTextColumn>

                                                        <dxwgv:GridViewDataCheckColumn Caption="Entrada" FieldName="queueConcluidoEntrada" CellStyle-HorizontalAlign="Center"
                                                            Width="50px">
                                                        </dxwgv:GridViewDataCheckColumn>

                                                        <dxwgv:GridViewDataCheckColumn Caption="Saida" FieldName="queueConcluidoSaida" CellStyle-HorizontalAlign="Center"
                                                            Width="50px">
                                                        </dxwgv:GridViewDataCheckColumn>

                                                        <dxwgv:GridViewDataCheckColumn Caption="Contador" FieldName="queueConcluidoContador" CellStyle-HorizontalAlign="Center"
                                                            Width="50px">
                                                        </dxwgv:GridViewDataCheckColumn>
                                                                    
                                                        <dxwgv:GridViewDataHyperLinkColumn Caption="Excluir" VisibleIndex="11" Width="40px" Name="colExcluir">
                                                            <DataItemTemplate>
                                                                <asp:ImageButton runat="server" ImageUrl="images/btExcluir.jpg" CommandArgument='<%#Eval("idBannerAgendamento")%>' ID="btExcluir" OnCommand="btExcluir_Command" OnClientClick="return confirm('Deseja excluir este agendamento?')" ToolTip="Arquivar Banner" />
                                                            </DataItemTemplate>
                                                            <CellStyle HorizontalAlign="Center">
                                                            </CellStyle>
                                                        </dxwgv:GridViewDataHyperLinkColumn>
                                                    </Columns>
                                                </dxwgv:ASPxGridView>
                                            </td>
                                        </tr>
                                    </table>
                                </dx:ContentControl>
                            </ContentCollection>
                        </dx:TabPage>
                    </TabPages>
                </dx:ASPxPageControl>
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="hdnCategorias" ClientIDMode="Static" runat="server"/>
    <asp:HiddenField ID="hdnProdutos" ClientIDMode="Static" runat="server"/>

    <!-- Dependencias do componente que vincula categoria e produto ao banner -->
    <script src="https://code.jquery.com/ui/1.9.2/jquery-ui.min.js" integrity="sha256-eEa1kEtgK9ZL6h60VXwDsJ2rxYCwfxi40VZ9E0XwoEA=" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">   
    <link rel="stylesheet" href="css/banner-categoria-component.css">   
    <link rel="stylesheet" href="css/banner-produto-component.css">  
    <script src="js/banner-categoria-component.js"></script>
    <script src="js/banner-produto-component.js"></script>
</asp:Content>

