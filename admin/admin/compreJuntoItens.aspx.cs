﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxTabControl;
using DevExpress.Web.ASPxEditors;
using DevExpress.Data;
using DevExpress.Web.ASPxUploadControl;
using System.IO;
using System.Collections.Generic;

public partial class admin_compreJuntoItens : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request.QueryString["idDoGrupo"]))
        {
            if (!Page.IsPostBack)
            {
                lblAcao.Text = "Grupo: " + Request.QueryString["nomeDoGrupo"].ToString();
                PopulateRootLevel();
            }
        }
        else
        {
            Response.Write("<script>alert('Selecione um compre junto.');</script>");
            Response.Write("<script>window.location=('compreJunto.aspx');</script>");
        }
    }

    private void PopulateRootLevel()
    {
        PopulateNodes(rnCategorias.categoriasSelecionaNivelPai().Tables[0], treeCategorias.Nodes);
    }

    private void PopulateSubLevel(int categoriaPaiId, TreeNode parentNode)
    {
        PopulateNodes(rnCategorias.categoriasSelecionaNivelFilho(categoriaPaiId).Tables[0], parentNode.ChildNodes);
    }

    protected void TreeView1_TreeNodePopulate(object sender, TreeNodeEventArgs e)
    {
        PopulateSubLevel(Int32.Parse(e.Node.Value), e.Node);
    }

    private void PopulateNodes(DataTable dt, TreeNodeCollection nodes)
    {
        foreach (DataRow dr in dt.Rows)
        {
            TreeNode tn = new TreeNode();
            tn.Text = dr["categoriaNome"].ToString();
            tn.Value = dr["categoriaId"].ToString();

            nodes.Add(tn);

            //If node has child nodes, then enable on-demand populating
            tn.PopulateOnDemand = ((int)(dr["childnodecount"]) > 0);
        }
    }

    //protected void btPdf_Click(object sender, ImageClickEventArgs e)
    //{
    //    this.grdEx.WritePdfToResponse();
    //}
    //protected void btXsl_Click(object sender, ImageClickEventArgs e)
    //{
    //    this.grdEx.WriteXlsToResponse();
    //}
    //protected void btRtf_Click(object sender, ImageClickEventArgs e)
    //{
    //    this.grdEx.WriteRtfToResponse();
    //}
    //protected void btCsv_Click(object sender, ImageClickEventArgs e)
    //{
    //    this.grdEx.WriteCsvToResponse();
    //}


    protected void grd_HtmlRowCreated(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType != GridViewRowType.Data) return;
        {
            TextBox txtProdutoId = (TextBox)grd.FindRowCellTemplateControl(e.VisibleIndex, null, "txtProdutoId");
            ASPxCheckBox ckbSelecionar = (ASPxCheckBox)grd.FindRowCellTemplateControl(e.VisibleIndex, null, "ckbSelecionar");
            if (!string.IsNullOrEmpty(txtProdutoId.Text))
            {
                  if (rnCompreJunto.juncaoProdutoCompreJuntoSeleciona_PorProdutoIdCompreJuntoGrupo(int.Parse(txtProdutoId.Text), int.Parse(Request.QueryString["idDoGrupo"].ToString())).Tables[0].Rows.Count > 0) ckbSelecionar.Checked = true;
            }
        }
    }

    protected void grd_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        int inicio = 0;
        int fim = 0;

        if (grd.PageIndex == 0)
        {
            inicio = 0;
            fim = inicio + grd.GetCurrentPageRowValues("produtoId").Count;
        }
        else
        {
            inicio += inicio + grd.SettingsPager.PageSize * grd.PageIndex;
            fim += inicio + grd.GetCurrentPageRowValues("produtoId").Count;
        }

        for (int i = inicio; i < fim; i++)
        {
            TextBox txtProdutoId = (TextBox)grd.FindRowCellTemplateControl(i, grd.Columns["produtoId"] as GridViewDataColumn, "txtProdutoId");

            ASPxCheckBox ckbSelecionar = (ASPxCheckBox)grd.FindRowCellTemplateControl(i, grd.Columns["selecionar"] as GridViewDataColumn, "ckbSelecionar");

            if (ckbSelecionar.Checked.ToString() == "True")
            {
                if (rnCompreJunto.juncaoProdutoCompreJuntoSeleciona_PorProdutoIdCompreJuntoGrupo(int.Parse(txtProdutoId.Text), int.Parse(Request.QueryString["idDoGrupo"].ToString())).Tables[0].Rows.Count == 0)
                    rnCompreJunto.juncaoProdutoCompreJuntoInclui(int.Parse(Request.QueryString["idDoGrupo"].ToString()), int.Parse(txtProdutoId.Text.ToString()));
            }
            else
                rnCompreJunto.juncaoProdutoCompreJuntoExclui(int.Parse(Request.QueryString["idDoGrupo"].ToString()), int.Parse(txtProdutoId.Text.ToString()));
        }
        grd.DataBind();
    }
}
