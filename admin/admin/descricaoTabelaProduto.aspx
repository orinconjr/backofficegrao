﻿<%@ Page Title="" Theme="Glass" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="descricaoTabelaProduto.aspx.cs" Inherits="admin_descricaoTabelaProduto" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>


<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" Runat="Server">
    <style type="text/css">
        .btnAdicionarProduto {
            height: 27px;
        }
    </style>
    <table cellpadding="0" cellspacing="0" style="width: 920px">
        <tr>
            <td class="tituloPaginas" valign="top">
                <asp:Label ID="lblAcao" runat="server">Tabela de Descrição - Produtos</asp:Label>
                </td>
        </tr>
        <tr>
            <td>
                <asp:Panel runat="server" ID="pnItens">
                    <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-top: 10px; padding-bottom: 15px; text-align: right;">
                            <asp:Button runat="server" ID="btnAdicionar" OnClick="btnAdicionar_OnClick" Text="Adicionar Novo Produto"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" style="width: 834px">
                                <tr>
                                    <td valign="top">
                                        <dxwgv:ASPxGridView ID="grd" runat="server" AutoGenerateColumns="False" KeyFieldName="idDescricaoTabelaProduto" Width="834px" Cursor="auto" EnableCallBacks="False">
                                            <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                                            <SettingsText ConfirmDelete="Tem certeza que deseja excluir?" emptydatarow="Nenhum registro encontrado." />
                                            <SettingsPager Position="TopAndBottom" PageSize="50" ShowDisabledButtons="False" AlwaysShowPager="True">
                                                <Summary AllPagesText="Páginas: {0} - {1} ({2} items)" Text="Página {0} de {1} ({2} registros encontrados)" />
                                            </SettingsPager>
                                            <settings showfilterrow="True" ShowGroupButtons="False" />
                                            <SettingsEditing EditFormColumnCount="4" 
                                                PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1" 
                                                PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter" 
                                                PopupEditFormWidth="700px" />
                                            <Columns>
                                                <dxwgv:GridViewDataTextColumn Caption="ID" FieldName="idDescricaoTabelaProduto" ReadOnly="True" VisibleIndex="0" Width="80px">
                                                    <Settings AutoFilterCondition="Contains" />
                                                    <editformsettings visible="False" CaptionLocation="Top" VisibleIndex="2" />
                                                </dxwgv:GridViewDataTextColumn>
                                                <dxwgv:GridViewDataComboBoxColumn Caption="Site" FieldName="idSite" VisibleIndex="4" Width="130px">
                                                    <PropertiesComboBox DataSourceID="sqlSite" TextField="nome" ValueField="idSite" ValueType="System.String">
                                                    </PropertiesComboBox>
                                                </dxwgv:GridViewDataComboBoxColumn>
                                                <dxwgv:GridViewDataTextColumn Caption="Nome Interno" FieldName="nomeInterno" ReadOnly="True" VisibleIndex="0" Width="80px">
                                                    <Settings AutoFilterCondition="Contains" />
                                                    <editformsettings visible="False" CaptionLocation="Top" VisibleIndex="2" />
                                                </dxwgv:GridViewDataTextColumn>
                                                <dxwgv:GridViewDataTextColumn Caption="Nome Amigável" FieldName="nomeAmigavel" ReadOnly="True" VisibleIndex="0" Width="80px">
                                                    <Settings AutoFilterCondition="Contains" />
                                                    <editformsettings visible="False" CaptionLocation="Top" VisibleIndex="2" />
                                                </dxwgv:GridViewDataTextColumn>
                                                <dxwgv:GridViewDataTextColumn Caption="Editar" FieldName="idDescricaoTabelaProduto" VisibleIndex="7" Width="30px">
                                                    <DataItemTemplate>
                                                        <asp:ImageButton runat="server" ImageUrl="~/admin/images/btEditar.jpg" ID="btnEditar" CommandArgument='<%# Eval("idDescricaoTabelaProduto") %>' OnCommand="btnEditar_OnCommand"  />
                                                    </DataItemTemplate>
                                                    <Settings AllowAutoFilter="False" />
                                                    <HeaderTemplate>
                                                        <img alt="" src="images/legendaEditar.jpg" />
                                                    </HeaderTemplate>
                                                </dxwgv:GridViewDataTextColumn>
                                            </Columns>
                                            <StylesEditors>
                                                <Label Font-Bold="True">
                                                </Label>
                                            </StylesEditors>
                                        </dxwgv:ASPxGridView>
                                        <asp:LinqDataSource ID="sqlSite" runat="server" 
                                            ContextTypeName="dbCommerceDataContext" EnableDelete="True" 
                                            EnableInsert="True" EnableUpdate="True" TableName="tbSites">
                                        </asp:LinqDataSource>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                    </tr>
                </table>
                </asp:Panel>
                <asp:Panel runat="server" ID="pnNovo" Visible="False">
                    <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
                        <tr>
                            <td>
                                &nbsp;
                                <asp:TextBox runat="server" ID="txtListaProdutos" Visible="false"></asp:TextBox>
                                <asp:HiddenField runat="server" ID="hdfProdutoAlterar"/>
                            </td>
                        </tr>
                        <tr class="rotulos">
                            <td>
                                Site
                            </td>
                        </tr>
                        <tr class="campos">
                            <td>
                                <asp:DropDownList runat="server" CssClass="campos" ID="ddlSite" DataValueField="idSite" DataTextField="nome" DataSourceID="sqlSite" />
                            </td>
                        </tr>
                        <tr class="rotulos">
                            <td>
                                Nome interno:
                            </td>
                        </tr>
                        <tr class="campos">
                            <td>
                                <asp:TextBox runat="server" ID="txtNomeInterno" CssClass="campos" Width="100%"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="rotulos">
                            <td style="padding-top: 10px;">
                                Título para o Site:
                            </td>
                        </tr>
                        <tr class="campos">
                            <td>
                                <asp:TextBox runat="server" ID="txtNomeSite" CssClass="campos" Width="100%"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="rotulos">
                            <td style="padding-top: 10px; font-weight: bold;">
                                Produtos
                            </td>
                        </tr>
                        <tr class="rotulos">
                            <td style="padding-top: 10px;">
                                <table>
                                    <tr class="rotulos">
                                        <td>
                                            Quantidade:<br/>
                                            <asp:TextBox runat="server" ID="txtQuantidade" CssClass="campos" Width="70px"></asp:TextBox>
                                        </td>
                                        <td>
                                            Descrição:<br/>
                                            <asp:TextBox runat="server" ID="txtDescricao" CssClass="campos" Width="660px"></asp:TextBox>
                                        </td>
                                        <td style="padding-top: 15px">
                                            <asp:Button runat="server" ID="btnAdicionarDescricao" Text="Adicionar" CssClass="btnAdicionarProduto" OnClick="btnAdicionarDescricao_OnClick"/>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr class="rotulos">
                            <td style="padding-top: 10px;">
                                <table>
                                    <tr class="rotulos" style="font-weight: bold;">
                                        <td>
                                            Quantidade:
                                        </td>
                                        <td style="padding-left: 10px;">
                                            Descrição:
                                        </td>
                                        <td style="padding-left: 10px;">
                                            Remover
                                        </td>
                                    </tr>
                                    <asp:ListView runat="server" ID="lstItensAdicionar">
                                        <ItemTemplate>
                                            <tr class="rotulos">
                                                <td>
                                                    <%# Eval("quantidade") %>
                                                </td>
                                                <td style="padding-left: 10px; width: 625px">
                                                    <%# Eval("descricao") %>
                                                </td>
                                                <td style="padding-left: 10px;">
                                                    <asp:LinkButton runat="server" ID="btnRemoverProduto" OnCommand="btnRemoverProduto_OnCommand" CommandArgument='<%# Eval("idDescricaoTabelaProdutoItem") %>'>
                                                        Remover Produto
                                                    </asp:LinkButton>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:ListView>
                                </table>
                            </td>
                        </tr>
                        <tr class="campos">
                            <td style="padding-top: 15px; text-align: right;">
                                <asp:Button runat="server" ID="btnGravarProduto" Text="Gravar Produto" OnClick="btnGravarProduto_OnClick"/>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
    </table>
</asp:Content>