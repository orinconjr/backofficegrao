﻿using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_carregamentoCaminhao : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack && !Page.IsCallback)
        {
            FillGrid();
        }

    }

    private void FillCaminhao()
    {
        var data = new dbCommerceDataContext();
        var caminhoes = (from c in data.tbCaminhaos select c).ToList();

        ddlCaminhao.DataSource = caminhoes.OrderBy(c => c.descricao);
        ddlCaminhao.DataTextField = "descricao";
        ddlCaminhao.DataValueField = "idCaminhao";
        ddlCaminhao.DataBind();
    }

    private void FillTransportadora(DropDownList pDDL)
    {
        var data = new dbCommerceDataContext();
        var trasnportadoras = (from c in data.tbTransportadoras select c).ToList();

        pDDL.DataSource = trasnportadoras.OrderBy(c => c.transportadora);
        pDDL.DataTextField = "transportadora";
        pDDL.DataValueField = "idTransportadora";
        pDDL.DataBind();

        pDDL.Items.Insert(0, new ListItem() { Text = "Selecione", Value = "0" });
        pDDL.SelectedIndex = 0;
    }

    private void FillTipoCaminhao()
    {
        var data = new dbCommerceDataContext();

        var tipoCaminhao = (from c in data.tbCarregamentoCaminhaoTipos select c).OrderBy(x => x.descricao);
        ddlTipoCaminhao.DataSource = tipoCaminhao;
        ddlTipoCaminhao.DataTextField = "descricao";
        ddlTipoCaminhao.DataValueField = "idCarregamentoCaminhaoTipo";
        ddlTipoCaminhao.DataBind();

        ddlTipoCaminhao.Items.Insert(0, new ListItem() { Text = "Selecione", Value = "0" });
        ddlTipoCaminhao.SelectedIndex = 0;
    }

    private void FillGrid()
    {
        grd.DataSource = null;
        grd.DataBind();

        var data = new dbCommerceDataContext();

        var carregamentos = (from c in data.tbCarregamentoCaminhaos select c).ToList();

        if (rdbAbertos.Checked)
            carregamentos = carregamentos.Where(x => x.dataFechamento == null).ToList();
        else if (rdbFinalizados.Checked)
            carregamentos = carregamentos.Where(x => x.dataFechamento != null).ToList();
        else if (rdoTodos.Checked)
            carregamentos = carregamentos.ToList();

        grd.DataSource = carregamentos.OrderByDescending(x => x.dataAbertura);
        grd.DataBind();
    }


    protected void grd_OnCustomUnboundColumnData(object sender, ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "pedidoIdCliente")
        {
            int pedidoId = Convert.ToInt32(e.GetListSourceFieldValue("idPedido"));
            e.Value = rnFuncoes.retornaIdCliente(pedidoId);
        }
    }
    protected void grd_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType != GridViewRowType.Data)
        {
            return;
        }

        var data = new dbCommerceDataContext();

        var idCarregamentoCaminhao = e.GetValue("idCarregamentoCaminhao");
        var idUsuarioAbertura = e.GetValue("idUsuarioAbertura");
        var idUsuarioFechamento = e.GetValue("idUsuarioFechamento");

        if (idUsuarioAbertura != null)
        {
            var usuarioAbertura = (from c in data.tbUsuarios where c.usuarioId == Convert.ToInt32(idUsuarioAbertura) select c).FirstOrDefault();
            Label lblUsuarioAbertura = (Label)grd.FindRowCellTemplateControl(e.VisibleIndex, grd.Columns["idUsuarioAbertura"] as GridViewDataColumn, "lblUsuarioAbertura");
            lblUsuarioAbertura.Text = usuarioAbertura.usuarioNome;
        }

        if (idUsuarioFechamento != null)
        {
            var usuarioFechamento = (from c in data.tbUsuarios where c.usuarioId == Convert.ToInt32(idUsuarioFechamento) select c).FirstOrDefault();
            Label lblUsuarioFechamento = (Label)grd.FindRowCellTemplateControl(e.VisibleIndex, grd.Columns["idUsuarioFechamento"] as GridViewDataColumn, "lblUsuarioFechamento");
            lblUsuarioFechamento.Text = usuarioFechamento.usuarioNome;
        }

        var idTipoCarregamento = e.GetValue("idCarregamentoCaminhaoTipo");
        if (idTipoCarregamento != null)
        {
            var tc = (from c in data.tbCarregamentoCaminhaoTipos where c.idCarregamentoCaminhaoTipo == Convert.ToInt32(idTipoCarregamento) select c).FirstOrDefault();
            Label lblTipoCarregamento = (Label)grd.FindRowCellTemplateControl(e.VisibleIndex, grd.Columns["idCarregamentoCaminhaoTipo"] as GridViewDataColumn, "lblTipoCarregamento");
            lblTipoCarregamento.Text = tc.descricao;
        }

        var idCaminhao = e.GetValue("idCaminhao");
        if (idCaminhao != null)
        {
            var caminhao = (from c in data.tbCaminhaos where c.idCaminhao == Convert.ToInt32(idCaminhao) select c).FirstOrDefault();
            Label lblCaminhao = (Label)grd.FindRowCellTemplateControl(e.VisibleIndex, grd.Columns["idCaminhao"] as GridViewDataColumn, "lblCaminhao");
            lblCaminhao.Text = caminhao.descricao;
        }

        if (idUsuarioFechamento != null && idCarregamentoCaminhao != null)
        {
            HyperLink linkImprimir = (HyperLink)grd.FindRowCellTemplateControl(e.VisibleIndex, grd.Columns["alterar"] as GridViewDataColumn, "linkImprimir");
            linkImprimir.NavigateUrl = "ImpressaoCarregamentoCaminhao.aspx?numeroCarregamento=" + idCarregamentoCaminhao.ToString();
            linkImprimir.Visible = true;
        }

        var hdfPedido = "";
        bool concluido = false;
        //var hdfConcluido = e.GetValue("concluido");
        if (e.GetValue("idCarregamentoCaminhao") != null)
            hdfPedido = e.GetValue("idCarregamentoCaminhao").ToString();
    }

    protected void rdb_OnCheckedChanged(object sender, EventArgs e)
    {
        FillGrid();
    }

    protected void btnAbrirCarregamento_Init(object sender, EventArgs e)
    {
        popCarregamentoCaminhao.PopupElementID = (sender as ASPxButton).ClientID;
        FillTipoCaminhao();
        FillCaminhao();
        FillTransportadoraCheckBox();
        //FillTransportadora(ddlTransportadora);
        divAbertura.Visible = true;
    }

    private void FillTransportadoraCheckBox()
    {
        var data = new dbCommerceDataContext();

        var transportadoras = (from c in data.tbTransportadoras select c).ToList();

        chkTransportadora.DataSource = transportadoras;
        chkTransportadora.DataTextField = "transportadora";
        chkTransportadora.DataValueField = "idTransportadora";
        chkTransportadora.DataBind();
    }

    protected void btnGravarAberturaCarregamento_Click(object sender, EventArgs e)
    {
        var data = new dbCommerceDataContext();
        HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
        usuarioLogadoId = Request.Cookies["usuarioLogadoId"];

        var idCarregamento = hdfIdCarregamentoCaminhao.Value;

        if (string.IsNullOrEmpty(idCarregamento))
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            foreach (ListItem item in chkTransportadora.Items)
            {
                if (item.Selected)
                    sb.Append(item.Text + " ");
            }

            var abertura = new tbCarregamentoCaminhao();
            var dtAgora = System.DateTime.Now;

            abertura.idUsuarioAbertura = Convert.ToInt32(usuarioLogadoId.Value);
            abertura.dataAbertura = dtAgora;
            abertura.nomeMotorista = txtMotorista.Text;
            abertura.placaCaminhao = txtPlaca.Text;
            abertura.idCarregamentoCaminhaoTipo = Convert.ToInt32(ddlTipoCaminhao.SelectedValue);
            abertura.idCaminhao = Convert.ToInt32(ddlCaminhao.SelectedValue);
            abertura.nomeTransportadora = sb.ToString();

            if (!string.IsNullOrEmpty(txtCusto.Text))
                abertura.custoTransporte = Convert.ToDecimal(txtCusto.Text);

            data.tbCarregamentoCaminhaos.InsertOnSubmit(abertura);
            data.SubmitChanges();

            idCarregamento = abertura.idCarregamentoCaminhao.ToString();

        }

        AbrirCarregamentoTransportadoa(Convert.ToInt32(idCarregamento));

        Response.Redirect("carregamentoCaminhao.aspx", true);

    }

    private void FillGridCarregamentoTransportadora(int idCarregamentoCaminhao)
    {
        var data = new dbCommerceDataContext();

        List<tbCarregamentoCaminhaoTransportadora> lista = new List<tbCarregamentoCaminhaoTransportadora>();

        lista = (from c in data.tbCarregamentoCaminhaoTransportadoras
                 where c.idCarregamentoCaminhao == idCarregamentoCaminhao
                 select c).ToList();

        gridCarregamentoTransportadora.DataSource = lista;
        gridCarregamentoTransportadora.DataBind();


    }

    protected void btnAlterar_Command(object sender, CommandEventArgs e)
    {
        popCarregamentoCaminhao.ShowOnPageLoad = true;
        var data = new dbCommerceDataContext();
        trTransportadora.Visible = false;

        var idCarregamentoCaminhao = e.CommandArgument.ToString();
        var carregamento = (from c in data.tbCarregamentoCaminhaos where c.idCarregamentoCaminhao == Convert.ToInt32(idCarregamentoCaminhao) select c).FirstOrDefault();
        //Deixar fechar o carregamento somente se existir carregamento da transportadora fechado
        hdfIdCarregamentoCaminhao.Value = idCarregamentoCaminhao.ToString();

        FillGridCarregamentoTransportadora(carregamento.idCarregamentoCaminhao);

        var tipoCaminhao = (from c in data.tbCarregamentoCaminhaoTipos where c.idCarregamentoCaminhaoTipo == carregamento.idCarregamentoCaminhaoTipo select c).FirstOrDefault();
        var caminhao = (from c in data.tbCaminhaos where c.idCaminhao == carregamento.idCaminhao select c).FirstOrDefault();
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        sb.Append("<b>Transportadora: </b> " + carregamento.nomeTransportadora + "<br>");
        sb.Append("<b>Tipo de Caminhão: </b>" + tipoCaminhao.descricao + "<br>");

        if (caminhao != null)
            sb.Append("<b>Caminhão: </b>" + caminhao.descricao + "<br>");

        sb.Append("<b>Placa: </b>" + carregamento.placaCaminhao + "<br>");
        sb.Append("<b>Motorista: </b>" + carregamento.nomeMotorista + "<br>");
        sb.Append("<b>Custo: <b>" + carregamento.custoTransporte + "<br>");

        litCabecalho.Text = sb.ToString();
        trCabecalho.Visible = true;

        divAbertura.Visible = true;
        trSalvar.Visible = false;
        trGrid.Visible = true;
        //verificaTipoCaminhao(carregamento.idCarregamentoCaminhaoTipo);

        trFechamento.Visible = true;
        //trAbertura.Visible = true;

    }

    private void verificaTipoCaminhao(int idTipoCaminhao)
    {
        switch (idTipoCaminhao)
        {
            case 1:
                tdPlaca.Visible = false;
                tdCusto.Visible = false;
                break;

            case 2:
                tdCusto.Visible = false;
                tdCaminhao.Visible = false;
                break;

            case 3:
                tdCaminhao.Visible = false;
                break;
        }
    }


    protected void ddlTransportadora_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    private void AbrirCarregamentoTransportadoa(int idCarregamentoCaminhao)
    {

        HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
        usuarioLogadoId = Request.Cookies["usuarioLogadoId"];

        var data = new dbCommerceDataContext();
        var dtAgora = System.DateTime.Now;

        foreach (ListItem item in chkTransportadora.Items)
        {
            if (item.Selected)
            {
                tbCarregamentoCaminhaoTransportadora cct = new tbCarregamentoCaminhaoTransportadora();
                cct.idCarregamentoCaminhao = idCarregamentoCaminhao;
                cct.idTransportadora = int.Parse(item.Value);
                cct.sequencial = CarregarSequencial(cct.idTransportadora);
                data.tbCarregamentoCaminhaoTransportadoras.InsertOnSubmit(cct);
                data.SubmitChanges();
            }

        }
    }

    private string CarregarSequencial(int idTransportadora)
    {
        try
        {
            var data = new dbCommerceDataContext();

            var seq = (from c in data.tbCarregamentoCaminhaoTransportadoras where c.idTransportadora == idTransportadora select c).Count();

            string retorno = string.Empty;
            
            switch (seq.ToString().Length)
            {
                case 1:
                    retorno = "000" + (seq + 1).ToString();
                    break;

                case 2:
                    retorno = "00" + (seq + 1).ToString();
                    break;
                case 3:
                    retorno = "0" + (seq + 1).ToString();
                    break;

                default:
                    retorno = (seq + 1).ToString();
                    break;
            }

            return retorno;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void ddlTipoCaminhao_SelectedIndexChanged(object sender, EventArgs e)
    {
        tdCaminhao.Visible = false;
        tdMotorista.Visible = false;
        tdPlaca.Visible = false;
        tdCusto.Visible = false;

        if (ddlTipoCaminhao.SelectedIndex > 0)
        {
            var tipoCaminhao = Convert.ToInt32(ddlTipoCaminhao.SelectedValue);
            tdCaminhao.Visible = true;
            tdMotorista.Visible = true;
            tdPlaca.Visible = true;
            tdCusto.Visible = true;
            verificaTipoCaminhao(tipoCaminhao);
        }



    }

    protected void gridCarregamentoTransportadora_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            var data = new dbCommerceDataContext();

            tbCarregamentoCaminhaoTransportadora row = (tbCarregamentoCaminhaoTransportadora)e.Row.DataItem;

            var idTransportadora = row.idTransportadora;

            Label lblTrasportadora = (Label)e.Row.FindControl("lbTrasportadora");
            Label lblUsuarioAbertura = (Label)e.Row.FindControl("lblUsuarioAbertura");
            Label lblUsuarioFechamento = (Label)e.Row.FindControl("lblUsuarioFechamento");


            var transportadora = (from c in data.tbTransportadoras where c.idTransportadora == idTransportadora select c).FirstOrDefault();
            if (transportadora != null)
                lblTrasportadora.Text = transportadora.transportadora;

            var usuarioAbertura = (from c in data.tbUsuarios where c.usuarioId == row.idUsuarioAbertura select c).FirstOrDefault();
            if (usuarioAbertura != null)
                lblUsuarioAbertura.Text = usuarioAbertura.usuarioNome;


            if (row.idUsuarioFechamento != null)
            {
                var usuarioFechamento = (from c in data.tbUsuarios where c.usuarioId == row.idUsuarioFechamento select c).FirstOrDefault();
                if (usuarioFechamento != null)
                    lblUsuarioFechamento.Text = usuarioFechamento.usuarioNome;
            }
        }
    }


    protected void btnCriarCarregamentoTransportadora_Click(object sender, EventArgs e)
    {
        trAbertura.Visible = false;
        lblMensagem.Text = string.Empty;

        var existe = false;
        foreach (ListItem item in chkTransportadora.Items)
        {
            if (item.Selected)
            {
                existe = true;
                break;
            }
        }

        if (!existe) return;

        var idCarregamento = hdfIdCarregamentoCaminhao.Value;

        var data = new dbCommerceDataContext();

        if (!string.IsNullOrEmpty(idCarregamento))
        {

            var cct = (from c in data.tbCarregamentoCaminhaoTransportadoras
                       where c.dataFechamento == null
                       select c).ToList();

            if (cct.Count() > 0)
            {
                lblMensagem.Text = "Já existe um carregamento de transportadora aberto";
                return;
            }
        }
        else
        {

            foreach (ListItem item in chkTransportadora.Items)
            {
                if (item.Selected)
                {
                    var carregamentoCaminhaoAberto = (from c in data.tbCarregamentoCaminhaoTransportadoras
                                                      where c.idTransportadora == int.Parse(item.Value) &&
                                                      c.dataFechamento == null
                                                      select c).ToList();

                    if (carregamentoCaminhaoAberto.Count() > 0)
                    {
                        lblMensagem.Text = "Já existe um camnhão aberto para essa transportadora";
                        return;
                    }
                }
            }


        }

        trSalvar.Visible = true;
        tdCaminhao.Visible = false;
        tdPlaca.Visible = false;
        tdMotorista.Visible = false;
        tdCusto.Visible = false;
        trAbertura.Visible = true;
        trGrid.Visible = false;
        FillTipoCaminhao();
        FillCaminhao();
    }

    protected void btnAbrirCarregamento_Click(object sender, EventArgs e)
    {
        trSalvar.Visible = false;
        hdfIdCarregamentoCaminhao.Value = string.Empty;
        trGrid.Visible = false;
        lblMensagem.Text = string.Empty;
        FillTransportadoraCheckBox();
        hdfIdCarregamentoCaminhao.Value = string.Empty;
        trAbertura.Visible = false;
        trTransportadora.Visible = true;
        trFechamento.Visible = false;
        trCabecalho.Visible = false;
        popCarregamentoCaminhao.ShowOnPageLoad = true;

    }

    protected void gridCarregamentoTransportadora_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        var data = new dbCommerceDataContext();
        HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
        usuarioLogadoId = Request.Cookies["usuarioLogadoId"];


        if (e.CommandName == "Fechar")
        {
            var idCarregamentoCaminhaoTransportadora = int.Parse(e.CommandArgument.ToString());

            var cct = (from c in data.tbCarregamentoCaminhaoTransportadoras
                       where c.idCarregamentoCaminhaoTransportadora == idCarregamentoCaminhaoTransportadora
                       select c).FirstOrDefault();

            if (cct != null)
            {
                var dtAgora = System.DateTime.Now;

                cct.idUsuarioFechamento = Convert.ToInt32(usuarioLogadoId.Value);
                cct.dataFechamento = dtAgora;
                data.SubmitChanges();

                Response.Redirect("carregamentoCaminhao.aspx", true);
            }
            else
                lblMensagem.Text = "Erro ao atualizar o carregamento de transportadora";

        }
    }

    protected void btnFecharCarregamentoCaminhao_Click(object sender, EventArgs e)
    {
        var data = new dbCommerceDataContext();
        HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
        usuarioLogadoId = Request.Cookies["usuarioLogadoId"];

        var dtAgora = System.DateTime.Now;

        var idCarregamentoCaminhao = int.Parse(hdfIdCarregamentoCaminhao.Value);

        var caminhaoFechado = (from c in data.tbCarregamentoCaminhaos
                               where c.idCarregamentoCaminhao == idCarregamentoCaminhao && c.dataFechamento != null
                               select c).FirstOrDefault();

        var transportadora = (from c in data.tbCarregamentoCaminhaoTransportadoras
                              where c.idCarregamentoCaminhao == idCarregamentoCaminhao && c.dataFechamento == null
                              select c).ToList();

        if (transportadora.Count > 0)
        {
            lblMensagemFechamento.Text = "Não foi possível fechar o caminhão. Existem carregamentos de transportadora em aberto";
            return;
        }

        if (caminhaoFechado != null)
        {
            lblMensagemFechamento.Text = "Esse caminhão já está com o carregamento fechado";
            return;
        }

        var cc = (from c in data.tbCarregamentoCaminhaos where c.idCarregamentoCaminhao == idCarregamentoCaminhao select c).FirstOrDefault();

        cc.dataFechamento = dtAgora;
        cc.idUsuarioFechamento = Convert.ToInt32(usuarioLogadoId.Value);
        data.SubmitChanges();

        Response.Redirect("carregamentoCaminhao.aspx", true);
    }


}