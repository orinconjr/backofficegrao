﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" Theme="Glass" AutoEventWireup="true" CodeFile="consultaInfoPagamento.aspx.cs" Inherits="admin_consultaInfoPagamento" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" runat="Server">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>

    <script type="text/javascript">
        function getvalue() {
            var opcao = $("#<%=RadioButtonList1.ClientID %> input[type=radio]:checked").val();

            if (opcao === "nsu") {
                $("#<%=pnlConsultaNsu.ClientID%>").show();
                $("#<%=pnlConsultaNumeroBoleto.ClientID%>").hide();
                $("#<%=pnlConsultaCodMaxiPago.ClientID%>").hide();
                $("#<%=pnlConsultaNumeroPgto.ClientID%>").hide();

                $("#divConteudo").width("340px");
            }
            else if (opcao === "numero") {
                $("#<%=pnlConsultaNumeroPgto.ClientID%>").show();
                $("#<%=pnlConsultaNumeroBoleto.ClientID%>").hide();
                $("#<%=pnlConsultaCodMaxiPago.ClientID%>").hide();
                $("#<%=pnlConsultaNsu.ClientID%>").hide();

                $("#divConteudo").width("380px");
            }
            else if (opcao === "boleto") {
                $("#<%=pnlConsultaNumeroPgto.ClientID%>").hide();
                $("#<%=pnlConsultaNumeroBoleto.ClientID%>").show();
                $("#<%=pnlConsultaCodMaxiPago.ClientID%>").hide();
                $("#<%=pnlConsultaNsu.ClientID%>").hide();

                $("#divConteudo").width("380px");
            }
            else
            {
                $("#<%=pnlConsultaCodMaxiPago.ClientID%>").show();
                $("#<%=pnlConsultaNumeroBoleto.ClientID%>").hide();
                $("#<%=pnlConsultaNsu.ClientID%>").hide();
                $("#<%=pnlConsultaNumeroPgto.ClientID%>").hide();

                $("#divConteudo").width("405px");
            }
    }
    </script>

    <style>
        .meugrid {
            background-color: #F4FAFB;
            border: 0px none;
            /* border-collapse: separate !important; */
            overflow: hidden;
            font: 9pt Tahoma;
            color: #000;
            /*cursor: pointer;*/
            white-space: nowrap;
            font-weight: normal;
            text-align: left;
        }

            .meugrid th {
                background-image: url(../../App_Themes/Glass/GridView/gvHeaderBackground.gif);
                background-position: center top;
                background-repeat: repeat-x;
                background-color: #C0DDE0;
                overflow: hidden;
                height: 23px;
                line-height: 23px;
                padding: 4px 6px 5px;
                font-weight: normal;
                border: 1px solid #7EACB1;
                /* border-collapse: separate;*/
            }

            .meugrid td {
                overflow: hidden;
                height: 23px;
                line-height: 23px;
                padding: 4px 6px 5px;
                border: 1px solid #7EACB1;
            }

            .meugrid tr td:last-of-type {
                text-align: center;
            }

            .meugrid tr td:nth-child(4) {
                text-align: center;
            }

                .meugrid tr td:nth-child(4) input {
                    text-decoration: underline;
                }

        .rotulos td {
            font: bold 12px tahoma;
            padding-top: 20px;
            color: rgb(65, 105, 126);
            padding-left: 5px;
            padding-bottom: 5px;
        }
    </style>

    <div class="tituloPaginas">
        Consultar Pedido por Dados de Pagamento
    </div>

    <div style="width: 340px; margin: auto;" id="divConteudo">

        <asp:RadioButtonList runat="server" RepeatDirection="Horizontal" ID="RadioButtonList1" Style="font-family: Tahoma; font-size: 13pt;">
            <asp:ListItem Text="NSU" Value="nsu" Selected="True" onclick="getvalue()"></asp:ListItem>
            <asp:ListItem Text="Cod. MaxiPago" Value="maxipago" onclick="getvalue()"></asp:ListItem>
            <asp:ListItem Text="Numero Pgto." Value="numero" onclick="getvalue()"></asp:ListItem>
            <asp:ListItem Text="Numero Boleto." Value="boleto" onclick="getvalue()"></asp:ListItem>
        </asp:RadioButtonList>
        <br />


        <asp:Panel runat="server" ID="pnlConsultaNsu">

            <div style="width: 340px; font-family: Tahoma; font-size: 10pt;">
                <div>
                    Informe o NSU separando por vírgula:<br />
                    (ex.: 123456,789456,456123)
                <asp:TextBox runat="server" ID="txtNsu" TextMode="MultiLine" Height="80" Width="334"></asp:TextBox>
                </div>
                <div style="text-align: right;">
                    <asp:Button runat="server" ID="btnPesquisar" Text="Pesquisar" OnClick="btnPesquisar_Click" />
                </div>
            </div>
            <br />
            <div>
                <asp:GridView ID="grd" runat="server" CssClass="meugrid" AutoGenerateColumns="False">
                    <Columns>
                        <asp:BoundField DataField="numeroCobranca" HeaderText="NSU" />
                        <asp:TemplateField HeaderText="Cliente ID">
                            <ItemTemplate>
                                <a style="text-decoration: none;" href='pedido.aspx?pedidoId=<%#Eval("pedidoId").ToString() %>' title="visualizar pedido" target="_blanck"><%# rnFuncoes.retornaIdInterno(Convert.ToInt32(Eval("pedidoId"))).ToString() %></a>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ID Pedido">
                            <ItemTemplate>
                                <a style="text-decoration: none;" href='pedido.aspx?pedidoId=<%#Eval("pedidoId").ToString() %>' title="visualizar pedido" target="_blanck"><%# Eval("pedidoId").ToString() %></a>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%--<asp:BoundField DataField="pedidoId" HeaderText="ID Pedido" />--%>
                        <asp:BoundField DataField="valor" HeaderText="Valor" />
                        <asp:BoundField DataField="situacao" HeaderText="Status" />
                    </Columns>
                </asp:GridView>
            </div>

        </asp:Panel>

        <asp:Panel runat="server" ID="pnlConsultaCodMaxiPago" Style="display: none;">

            <div style="width: 405px; font-family: Tahoma; font-size: 10pt;">
                <div>
                    Informe o código da MaxiPago:<br />
                    (ex.: 0A0A1E22:01555B9830DA:68D3:0F05AB32)
                <asp:TextBox runat="server" ID="txtCodMaxiPago" Width="400"></asp:TextBox>
                </div>
                <div style="text-align: right;">
                    <asp:Button runat="server" ID="btnPesquisarCodMaxiPago" Text="Pesquisar" OnClick="btnPesquisarCodMaxiPago_OnClick" />
                </div>
            </div>
            <br />
            <div>
                <asp:GridView ID="grdCodMaxiPago" runat="server" AutoGenerateColumns="False" CssClass="meugrid">
                    <Columns>
                        <asp:BoundField DataField="numeroCobranca" HeaderText="NSU" />
                        <asp:TemplateField HeaderText="Cliente ID">
                            <ItemTemplate>
                                <a style="text-decoration: none;" href='pedido.aspx?pedidoId=<%#Eval("pedidoId").ToString() %>' title="visualizar pedido" target="_blanck"><%# rnFuncoes.retornaIdInterno(Convert.ToInt32(Eval("pedidoId"))).ToString() %></a>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ID Pedido">
                            <ItemTemplate>
                                <a style="text-decoration: none;" href='pedido.aspx?pedidoId=<%#Eval("pedidoId").ToString() %>' title="visualizar pedido" target="_blanck"><%# Eval("pedidoId").ToString() %></a>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%--<asp:BoundField DataField="pedidoId" HeaderText="ID Pedido" />--%>
                        <asp:BoundField DataField="valor" HeaderText="Valor" />
                        <asp:BoundField DataField="situacao" HeaderText="Status" />
                    </Columns>
                </asp:GridView>
            </div>

        </asp:Panel>

        <asp:Panel runat="server" ID="pnlConsultaNumeroPgto" Style="display: none;">

            <div style="width: 405px; font-family: Tahoma; font-size: 10pt;">
                <div>
                    Informe o numero do pagamento:(uma consulta por vez)<br />
                    <asp:TextBox runat="server" ID="txtNumeroPgto" Width="400"></asp:TextBox>
                </div>
                <div style="text-align: right;">
                    <asp:Button runat="server" ID="btnPesquisarNumeroPgto" Text="Pesquisar" OnClick="btnPesquisarNumeroPgto_OnClick" />
                </div>
            </div>
            <br />
            <div>
                <asp:GridView ID="grdNumeroPgto" runat="server" AutoGenerateColumns="False" CssClass="meugrid">
                    <Columns>
                        <asp:BoundField DataField="numeroCobranca" HeaderText="NSU" />
                        <asp:BoundField DataField="authorizationCode" HeaderText="Cód. Autorização" />
                        <asp:TemplateField HeaderText="Cliente ID">
                            <ItemTemplate>
                                <a style="text-decoration: none;" href='pedido.aspx?pedidoId=<%#Eval("pedidoId").ToString() %>' title="visualizar pedido" target="_blanck"><%# rnFuncoes.retornaIdInterno(Convert.ToInt32(Eval("pedidoId"))).ToString() %></a>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ID Pedido">
                            <ItemTemplate>
                                <a style="text-decoration: none;" href='pedido.aspx?pedidoId=<%#Eval("pedidoId").ToString() %>' title="visualizar pedido" target="_blanck"><%# Eval("pedidoId").ToString() %></a>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%--<asp:BoundField DataField="pedidoId" HeaderText="ID Pedido" />--%>
                        <asp:BoundField DataField="valor" HeaderText="Valor" />
                        <asp:BoundField DataField="situacao" HeaderText="Status" />
                    </Columns>
                </asp:GridView>
            </div>

        </asp:Panel>

           <asp:Panel runat="server" ID="pnlConsultaNumeroBoleto" Style="display: none;">

            <div style="width: 405px; font-family: Tahoma; font-size: 10pt;">
                <div>
                    Informe o numero do boleto:(uma consulta por vez)<br />
                    <asp:TextBox runat="server" ID="txtNumeroBoleto" Width="400"></asp:TextBox>
                </div>
                <div style="text-align: right;">
                    <asp:Button runat="server" ID="btnPesquisarNumeroBoleto" Text="Pesquisar" OnClick="btnPesquisarNumeroBoleto_OnClick" />
                </div>
            </div>
            <br />
            <div>
                <asp:GridView ID="grdNumeroBoleto" runat="server" AutoGenerateColumns="False" CssClass="meugrid">
                    <Columns>
                        <asp:BoundField DataField="idPedidoPagamento" HeaderText="Boleto" />
                        <asp:BoundField DataField="pedidoId" HeaderText="Pedido" />
                        <asp:BoundField DataField="valorParcela" HeaderText="Valor Cobrado" />
                        <asp:BoundField DataField="valorDesconto" HeaderText="Desconto" />
                        <asp:BoundField DataField="valorTotal" HeaderText="Valor Total" />

                        <asp:TemplateField HeaderText="ID Pedido">
                            <ItemTemplate>
                                <a style="text-decoration: none;" href='pedido.aspx?pedidoId=<%#Eval("pedidoId").ToString() %>' title="visualizar pedido" target="_blanck"><%# Eval("pedidoId").ToString() %></a>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%--<asp:BoundField DataField="pedidoId" HeaderText="ID Pedido" />--%>

                    </Columns>
                </asp:GridView>
                <br />
                <asp:GridView ID="GridView1" runat="server"></asp:GridView>
            </div>

        </asp:Panel>

    </div>

</asp:Content>

