﻿<%@ Page Title="" Language="C#" Theme="Glass" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="comprasMinhasSolicitacoes.aspx.cs" Inherits="admin_comprasMinhasSolicitacoes" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dxwgv" %>
<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" runat="Server">
    <table cellpadding="0" cellspacing="0" style="width: 920px">
        <tr>
            <td class="tituloPaginas" valign="top">
                <asp:Label ID="lblAcao" runat="server">Minhas solicitações de Compra</asp:Label>
            </td>
        </tr>
        <tr>
            <td>

                <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
                    <tr>
                        <td align="right">
                            <table cellpadding="0" cellspacing="0" style="width: 834px">
                                <tr>
                                    <td align="right">&nbsp;</td>
                                    <td height="38"
                                        style="background-image: url('images/bkExportar.jpg'); padding-bottom: 1px;"
                                        valign="bottom" width="231">
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 97px">&nbsp;</td>
                                                <td style="width: 33px">
                                                    <asp:ImageButton ID="btPdf" runat="server" ImageUrl="~/admin/images/btPdf.jpg"
                                                        Style="margin-left: 5px" OnClick="btPdf_Click" />
                                                </td>
                                                <td style="width: 31px">
                                                    <asp:ImageButton ID="btXsl" runat="server" ImageUrl="~/admin/images/btXls.jpg"
                                                        Style="margin-left: 5px" OnClick="btXsl_Click" />
                                                </td>
                                                <td style="width: 36px">
                                                    <asp:ImageButton ID="btRtf" runat="server" ImageUrl="~/admin/images/btRtf.jpg"
                                                        Style="margin-left: 5px" OnClick="btRtf_Click" />
                                                </td>
                                                <td valign="bottom">
                                                    <asp:ImageButton ID="btCsv" runat="server" ImageUrl="~/admin/images/btCsv.jpg"
                                                        Style="margin-left: 5px" OnClick="btCsv_Click" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" style="width: 834px">
                                <tr>
                                    <td colspan="3" class="rotulos" style="width: 100px">
                                        Status:<br/>
                                        <asp:RadioButton runat="server" ID="rdbPendente" Text="Pendentes" Checked="True" GroupName="filtroEntregue"/>&nbsp;
                                        <asp:RadioButton runat="server" ID="rdbAprovados" Text="Aprovados" Checked="False" GroupName="filtroEntregue"/>&nbsp;
                                        <asp:RadioButton runat="server" ID="rdbReprovados" Text="Reprovados" Checked="False" GroupName="filtroEntregue"/>&nbsp;
                                        <asp:RadioButton runat="server" ID="rdbParcialReprovados" Text="Parcialmente Reprovados" Checked="False" GroupName="filtroEntregue"/>&nbsp;
                                        <asp:RadioButton runat="server" ID="rdbTodos" Text="Todos" Checked="False" GroupName="filtroEntregue"/>

                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" valign="bottom">
                                        <asp:ImageButton ID="imbInsert" runat="server" OnClick="btnPesquisar_Click"
                                            ImageUrl="~/admin/images/btPesquisar.jpg" />
                                        <asp:ValidationSummary ID="llds" runat="server" DisplayMode="List"
                                            ShowMessageBox="True" ShowSummary="False" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <dxwgv:ASPxGridView ID="grd" runat="server" AutoGenerateColumns="False" 
                                KeyFieldName="idComprasSolicitacaoProduto" Width="834px" 
                                Cursor="auto"  ClientInstanceName="grd" EnableCallBacks="False">
                                <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                                <Styles>
                                    <Footer Font-Bold="True">
                                    </Footer>
                                </Styles>
                                <SettingsText ConfirmDelete="Tem certeza que deseja excluir?"
                                    EmptyDataRow="Nenhum registro encontrado." />
                                <SettingsPager Position="TopAndBottom" PageSize="500"
                                    ShowDisabledButtons="False" AlwaysShowPager="False" Visible="False">
                                    <Summary AllPagesText="Páginas: {0} - {1} ({2} items)"
                                        Text="Página {0} de {1} ({2} registros encontrados)" />
                                </SettingsPager>
                                <Settings ShowFilterRow="True" ShowGroupButtons="False" ShowFooter="True" />
                                <TotalSummary>
                                    <dxwgv:ASPxSummaryItem DisplayFormat="0" ShowInColumn="pedidoFornecedor" ShowInGroupFooterColumn="pedidoFornecedor" SummaryType="Count" />
                                    <dxwgv:ASPxSummaryItem FieldName="custo" ShowInColumn="custo" ShowInGroupFooterColumn="custo" SummaryType="Sum" DisplayFormat="{0:C}" />
                                </TotalSummary>
                                <SettingsEditing EditFormColumnCount="4"
                                    PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1"
                                    PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter"
                                    PopupEditFormWidth="700px" Mode="Inline" />
                                <Columns>
                                    <dxwgv:GridViewDataTextColumn Caption="ID" FieldName="idComprasSolicitacao">
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Data Solicitação" FieldName="dataSolicitacao">
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Produtos Solicitados" FieldName="totalProdutos">
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Produtos Aprovados" FieldName="aprovados">
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Produtos Reprovados" FieldName="reprovados">
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Produtos Pendentes" FieldName="pendentes">
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Motivo" FieldName="motivo"></dxwgv:GridViewDataTextColumn>
                                </Columns>
                                <StylesEditors>
                                    <Label Font-Bold="True">
                                    </Label>
                                </StylesEditors>
                            </dxwgv:ASPxGridView>
                            <dxwgv:ASPxGridViewExporter ID="grdEx" runat="server" FileName="relatorio"
                                GridViewID="grd" Landscape="True" PreserveGroupRowStates="True">
                            </dxwgv:ASPxGridViewExporter>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>