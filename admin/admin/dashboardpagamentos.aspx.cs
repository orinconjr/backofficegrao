﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

public partial class admin_dashboardpagamentos : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            mudaPanel(null, true);
            litnprocclearsale.Text = contanprocclearsale().ToString();
            litanaliseclearsale.Text = contaanaliseclearsale().ToString();
            litrejmundipagg.Text = contarejmundipagg().ToString();
            litpagosnaoatualizados.Text = contapagosnaoatualizados().ToString();
            litpagosboletorej.Text = contapagosboletorej().ToString();
        }
    }

    #region ========= Quadros do Topo=================
    protected void btnnprocclearsale_Click(object sender, EventArgs e)
    {
        mudaPanel(pnnprocclearsale, false);
        destacaquadro(quadronprocclerasale);
        carregaGrdnprocclearsale();

    }
    protected void btnanaliseclearsale_Click(object sender, EventArgs e)
    {
        mudaPanel(pnanaliseclearsale, false);
        destacaquadro(quadroanaliseclearsale);
        carregaGrdanaliseclearsale();
    }
    protected void btnrejmundipagg_Click(object sender, EventArgs e)
    {
        mudaPanel(pnrejmundipagg, false);
        destacaquadro(quadrorejmundipagg);
        carregaGrdrejmundipagg();
    }

    protected void btnpagosnaoatualizados_Click(object sender, EventArgs e)
    {
        mudaPanel(pnpagosnaoatualizados, false);
        destacaquadro(quadropagosnaoatualizados);
        carregaGrdpagosnaoatualizados();
    }
    protected void btnpagosboletorej_Click(object sender, EventArgs e)
    {
        mudaPanel(pnpagosboletorej, false);
        destacaquadro(quadropagosboletorej);
        carregaGrdpagosboletorej();

    }
    protected void btnpedidospagamento_Click(object sender, EventArgs e)
    {
        mudaPanel(pnpedidospagamento, false);
        destacaquadro(quadropedidospagamento);
        // carregaGrdpedidospagamento();
    }

    protected void btnpagoscartaonaofinalizados_Click(object sender, EventArgs e)
    {
        mudaPanel(pnpagoscartaonaofinalizados, false);
        destacaquadro(quadropagoscartaonaofinalizados);
        carregaGrdpagoscartaonaofinalizados();
        // carregaGrdpedidospagamento();
    }

    protected void btnanalisequebra_Click(object sender, EventArgs e)
    {
        mudaPanel(pnanalisequebra, false);
        destacaquadro(quadroanalisequebra);
        carregaGrdanalisequebra();
        // carregaGrdpedidospagamento();
    }
    #endregion

    #region ========= Métodos Diversos =================
    public void interacaoInclui(int pedidoId, string interacao, string alteracaoDeEstatus)
    {
        HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
        usuarioLogadoId = Request.Cookies["usuarioLogadoId"];
        if (usuarioLogadoId != null)
        {
            rnInteracoes.interacaoInclui(pedidoId, interacao, rnUsuarios.usuarioSeleciona_PorUsuarioId(int.Parse(usuarioLogadoId.Value)).Tables[0].Rows[0]["usuarioNome"].ToString(), alteracaoDeEstatus);
        }


    }
    void mudaPanel(Panel panel, bool escondetodos)
    {
        foreach (Control childcontrol in panelPai.Controls)
        {
            if (childcontrol is Panel)
            {
                if (!escondetodos)
                {
                    if (childcontrol.ID != panel.ID)
                        childcontrol.Visible = false;
                    else
                        childcontrol.Visible = true;
                }
                else
                {
                    childcontrol.Visible = false;
                }

            }
        }
    }
    void destacaquadro(HtmlContainerControl div)
    {
        foreach (Control childcontrol in panelPaiQuadros.Controls)
        {
            if (childcontrol is HtmlContainerControl)
            {
                HtmlContainerControl child = (HtmlContainerControl)childcontrol;
                if (child.ID != null)
                {
                    if (childcontrol.ID != div.ID)
                    {

                        child.Attributes.Add("style", "border-color:#b1b1b1");
                    }
                    else
                    {
                        child.Attributes.Add("style", "border-color:red");
                    }
                }
            }
        }
    }

    void checarClarsale(int idPedidoPagamento)
    {
        string erro = "";
        try
        {
            using (var data = new dbCommerceDataContext())
            {
                var pagamentoRisco = (from c in data.tbPedidoPagamentoRiscos
                                      where c.idPedidoPagamento == idPedidoPagamento
                                           && c.statusInterno == 0
                                      select c).FirstOrDefault();
                if (pagamentoRisco != null)
                    data.tbPedidoPagamentoRiscos.DeleteOnSubmit(pagamentoRisco);

                var queue = new tbQueue();
                queue.agendamento = DateTime.Now;
                queue.andamento = false;
                queue.concluido = false;

                queue.idRelacionado = null;
                queue.mensagem = idPedidoPagamento.ToString();
                queue.tipoQueue = 24;
                data.tbQueues.InsertOnSubmit(queue);
                data.SubmitChanges();
            }
        }
        catch (Exception ex)
        {
            erro = ex.Message;
        }

        string meuscript;
        if (erro == "")
        {
            meuscript = @"alert('Criado registro na fila para checar pedido com Clearsale');";
        }
        else
        {
            meuscript = @"alert('" + erro + "');";
        }

        Page.ClientScript.RegisterStartupScript(this.GetType(), "MyScript", meuscript, true);
    }
    #endregion

    #region ========= Não processados Clear Sale=================
    public int contanprocclearsale()
    {
        var data = new dbCommerceDataContext();
        int qtd = (from r in data.tbPedidoPagamentoRiscos
                   where r.statusInterno == 0
                   select new { r.idPedidoPagamentoRisco }).Count();
        return qtd;
    }
    protected void btnFiltrarnprocclearsale_Click(object sender, EventArgs e)
    {
        carregaGrdnprocclearsale();

    }
    void carregaGrdnprocclearsale()
    {
        var data = new dbCommerceDataContext();
        var pagamentos = (from ppr in data.tbPedidoPagamentoRiscos
                          join pp in data.tbPedidoPagamentos on ppr.idPedidoPagamento equals pp.idPedidoPagamento
                          where ppr.statusInterno == 0
                          select new
                          {
                              pp.pedidoId,
                              pp.idPedidoPagamento,
                              ppr.dataEnvio,
                              pp.valor,
                              ppr.message,
                              pp.tbPedido.clienteId

                          }).OrderByDescending(x => x.pedidoId).ToList();

        if (!String.IsNullOrEmpty(txtidpedidonprocclearsale.Text))
        {
            int pedidoId = Convert.ToInt32(txtidpedidonprocclearsale.Text);
            pagamentos = (from pa in pagamentos where pa.pedidoId == pedidoId select pa).ToList();

        }

        if (!String.IsNullOrEmpty(txtidpagamentonprocclearsale.Text))
        {
            int pagamentoId = Convert.ToInt32(txtidpagamentonprocclearsale.Text);
            pagamentos = (from pa in pagamentos where pa.idPedidoPagamento == pagamentoId select pa).ToList();

        }

        grdnprocclearsale.DataSource = pagamentos;
        grdnprocclearsale.DataBind();
    }
    protected void grdnprocclearsale_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grdnprocclearsale.PageIndex = e.NewPageIndex;

        carregaGrdnprocclearsale();
    }
    protected void grdnprocclearsale_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        if (e.Row.RowType == DataControlRowType.DataRow)
        {

            HtmlAnchor linkeditarpedido = (HtmlAnchor)e.Row.FindControl("linkeditarpedido");
            linkeditarpedido.HRef = "pedido.aspx?pedidoId=" + DataBinder.Eval(e.Row.DataItem, "pedidoId");

        }
    }

    protected void grdnprocclearsale_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "ChecarClearSale")
        {
            GridViewRow row = (GridViewRow)((Control)e.CommandSource).NamingContainer;
            int idPedidoPagamento = Convert.ToInt32(grdnprocclearsale.DataKeys[row.RowIndex].Values["idPedidoPagamento"]);
            checarClarsale(idPedidoPagamento);
            carregaGrdnprocclearsale();
        }
    }

    #endregion

    #region ========= Em Análise Clear Sale=================
    public int contaanaliseclearsale()
    {
        var data = new dbCommerceDataContext();
        int qtd = (from ppr in data.tbPedidoPagamentoRiscos
                   join pp in data.tbPedidoPagamentos on ppr.idPedidoPagamento equals pp.idPedidoPagamento
                   where ppr.statusInterno == 1 && pp.tbPedido.statusDoPedido == 2
                   select new { ppr.idPedidoPagamentoRisco }).Count();
        return qtd;
    }
    protected void btnFiltraranaliseclearsale_Click(object sender, EventArgs e)
    {
        carregaGrdanaliseclearsale();

    }
    void carregaGrdanaliseclearsale()
    {
        var data = new dbCommerceDataContext();
        var pagamentos = (from ppr in data.tbPedidoPagamentoRiscos
                          join pp in data.tbPedidoPagamentos on ppr.idPedidoPagamento equals pp.idPedidoPagamento
                          where ppr.statusInterno == 1 && pp.tbPedido.statusDoPedido == 2
                          select new
                          {
                              pp.pedidoId,
                              pp.idPedidoPagamento,
                              ppr.dataEnvio,
                              pp.valor,
                              ppr.message,
                              pp.tbPedido.clienteId

                          }).OrderByDescending(x => x.pedidoId).ToList();

        if (!String.IsNullOrEmpty(txtidpedidoanaliseclearsale.Text))
        {
            int pedidoId = Convert.ToInt32(txtidpedidoanaliseclearsale.Text);
            pagamentos = (from pa in pagamentos where pa.pedidoId == pedidoId select pa).ToList();

        }

        if (!String.IsNullOrEmpty(txtidpagamentoanaliseclearsale.Text))
        {
            int pagamentoId = Convert.ToInt32(txtidpagamentoanaliseclearsale.Text);
            pagamentos = (from pa in pagamentos where pa.idPedidoPagamento == pagamentoId select pa).ToList();

        }

        grdanaliseclearsale.DataSource = pagamentos;
        grdanaliseclearsale.DataBind();
    }
    protected void grdanaliseclearsale_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grdanaliseclearsale.PageIndex = e.NewPageIndex;

        carregaGrdanaliseclearsale();
    }
    protected void grdanaliseclearsale_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        if (e.Row.RowType == DataControlRowType.DataRow)
        {

            HtmlAnchor linkeditarpedido = (HtmlAnchor)e.Row.FindControl("linkeditarpedido");
            linkeditarpedido.HRef = "pedido.aspx?pedidoId=" + DataBinder.Eval(e.Row.DataItem, "pedidoId");

        }
    }

    protected void grdanaliseclearsale_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "ChecarClearSale")
        {
            GridViewRow row = (GridViewRow)((Control)e.CommandSource).NamingContainer;
            int idPedidoPagamento = Convert.ToInt32(grdanaliseclearsale.DataKeys[row.RowIndex].Values["idPedidoPagamento"]);
            checarClarsale(idPedidoPagamento);
            carregaGrdanaliseclearsale();
        }
    }

    #endregion

    #region ========= Rejeitados Mundipagg=================

    public int contarejmundipagg()
    {
        var data = new dbCommerceDataContext();
        int qtd = (from pp in data.tbPedidoPagamentos
                   join ppg in data.tbPedidoPagamentoGateways on pp.idPedidoPagamento equals ppg.idPedidoPagamento
                   where ppg.responseMessage == "WithError" && pp.pagamentoNaoAutorizado == false && pp.cancelado == false && pp.pago == false
                   select new
                   {
                       pp.pedidoId,
                   }).Count();
        return qtd;
    }
    protected void btnFiltrarrejmundipagg_Click(object sender, EventArgs e)
    {
        carregaGrdrejmundipagg();

    }
    void carregaGrdrejmundipagg()
    {
        var data = new dbCommerceDataContext();
        var pagamentos = (from pp in data.tbPedidoPagamentos
                          join ppg in data.tbPedidoPagamentoGateways on pp.idPedidoPagamento equals ppg.idPedidoPagamento
                          where ppg.responseMessage == "WithError" && pp.pagamentoNaoAutorizado == false && pp.cancelado == false && pp.pago == false
                          select new
                          {
                              pp.pedidoId,
                              pp.idPedidoPagamento,
                              ppg.dataSolicitacao,
                              pp.valor,
                              ppg.responseMessage
                          }).OrderByDescending(x => x.pedidoId).ToList();

        if (!String.IsNullOrEmpty(txtidpedidorejmundipagg.Text))
        {
            int pedidoId = Convert.ToInt32(txtidpedidorejmundipagg.Text);
            pagamentos = (from pa in pagamentos where pa.pedidoId == pedidoId select pa).ToList();

        }

        if (!String.IsNullOrEmpty(txtidpagamentorejmundipagg.Text))
        {
            int pagamentoId = Convert.ToInt32(txtidpagamentorejmundipagg.Text);
            pagamentos = (from pa in pagamentos where pa.idPedidoPagamento == pagamentoId select pa).ToList();

        }

        grdrejmundipagg.DataSource = pagamentos;
        grdrejmundipagg.DataBind();
    }
    protected void grdrejmundipagg_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grdrejmundipagg.PageIndex = e.NewPageIndex;

        carregaGrdrejmundipagg();
    }
    protected void grdrejmundipagg_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        if (e.Row.RowType == DataControlRowType.DataRow)
        {

            HtmlAnchor linkeditarpedido = (HtmlAnchor)e.Row.FindControl("linkeditarpedido");
            linkeditarpedido.HRef = "pedido.aspx?pedidoId=" + DataBinder.Eval(e.Row.DataItem, "pedidoId");

        }
    }

    protected void grdrejmundipagg_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "ChecarClearSale")
        {
            GridViewRow row = (GridViewRow)((Control)e.CommandSource).NamingContainer;
            int idPedidoPagamento = Convert.ToInt32(grdrejmundipagg.DataKeys[row.RowIndex].Values["idPedidoPagamento"]);
            checarClarsale(idPedidoPagamento);
            carregaGrdrejmundipagg();
        }
    }

    #endregion

    #region ========= Pagos Não Atualizados =================
    protected int contapagosnaoatualizados()
    {
        string sqlquery =
        "select pe.pedidoId,pe.dataHoraDoPedido,pe.valorCobrado, " +
    "(select sum(pp.valor) from tbPedidoPagamento pp where pp.pedidoId = pe.pedidoId and pp.pago = 1) as totalPago " +
    "from tbPedidos pe  " +
    "where pe.valorCobrado <= " +
    "(select sum(pp.valor) from tbPedidoPagamento pp where pp.pedidoId = pe.pedidoId and pp.pago = 1)  " +
    "and pe.statusDoPedido = 2  " +
    "order by pe.pedidoId desc ";

        string connectionString = ConfigurationManager.ConnectionStrings["connectionString"].ToString();

        using (
                SqlConnection conn =
                    new SqlConnection(connectionString))
        {
            SqlCommand command = conn.CreateCommand();
            command.Connection.Open();
            command.CommandType = CommandType.Text;
            command.CommandText = sqlquery;
            SqlDataReader reader = command.ExecuteReader(CommandBehavior.CloseConnection);

            DataTable table = new DataTable();

            table.Load(reader);
            return table.Rows.Count;

        }
    }
    protected void btnFiltrarpagosnaoatualizados_Click(object sender, EventArgs e)
    {
        carregaGrdpagosnaoatualizados();

    }
    void carregaGrdpagosnaoatualizados()
    {
        string sqlquery =
        "select pe.pedidoId,pe.dataHoraDoPedido,pe.valorCobrado, " +
        "(select sum(pp.valor) from tbPedidoPagamento pp where pp.pedidoId = pe.pedidoId and pp.pago = 1) as totalPago " +
        "from tbPedidos pe  " +
        "where pe.valorCobrado <= " +
        "(select sum(pp.valor) from tbPedidoPagamento pp where pp.pedidoId = pe.pedidoId and pp.pago = 1)  " +
        "and pe.statusDoPedido = 2  " +
        "order by pe.pedidoId desc ";

        string connectionString = ConfigurationManager.ConnectionStrings["connectionString"].ToString();

        using (
                SqlConnection conn =
                    new SqlConnection(connectionString))
        {
            SqlCommand command = conn.CreateCommand();
            command.Connection.Open();
            command.CommandType = CommandType.Text;
            command.CommandText = sqlquery;
            SqlDataReader reader = command.ExecuteReader(CommandBehavior.CloseConnection);

            DataTable table = new DataTable();

            table.Load(reader);

            grdpagosnaoatualizados.DataSource = table;
            grdpagosnaoatualizados.DataBind();
        }

    }
    protected void grdpagosnaoatualizados_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grdpagosnaoatualizados.PageIndex = e.NewPageIndex;

        carregaGrdpagosnaoatualizados();
    }
    protected void grdpagosnaoatualizados_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        if (e.Row.RowType == DataControlRowType.DataRow)
        {

            HtmlAnchor linkeditarpedido = (HtmlAnchor)e.Row.FindControl("linkeditarpedido");
            linkeditarpedido.HRef = "pedido.aspx?pedidoId=" + DataBinder.Eval(e.Row.DataItem, "pedidoId");

        }
    }

    protected void grdpagosnaoatualizados_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "ChecarClearSale")
        {
            GridViewRow row = (GridViewRow)((Control)e.CommandSource).NamingContainer;
            int idPedidoPagamento = Convert.ToInt32(grdpagosnaoatualizados.DataKeys[row.RowIndex].Values["idPedidoPagamento"]);
            checarClarsale(idPedidoPagamento);
            carregaGrdpagosnaoatualizados();
        }
    }

    #endregion

    #region ========= Pagos em Boleto rejeitados =================

    int contapagosboletorej()
    {
        var data = new dbCommerceDataContext();
        int qtd = (from tlcrr in data.tbLoteCobrancaRetornoRegistros
                   join pp in data.tbPedidoPagamentos on tlcrr.idPedidoPagamento equals pp.idPedidoPagamento
                   where tlcrr.status == 0
                   select new
                   {
                       tlcrr.idLoteCobrancaRetornoRegistro,
                   }
              ).Count();
        return qtd;
    }
    protected void btnFiltrarpagosboletorej_Click(object sender, EventArgs e)
    {
        carregaGrdpagosboletorej();
    }
    void carregaGrdpagosboletorej()
    {
        var data = new dbCommerceDataContext();
        var pagamentos = (from tlcrr in data.tbLoteCobrancaRetornoRegistros
                          join pp in data.tbPedidoPagamentos on tlcrr.idPedidoPagamento equals pp.idPedidoPagamento
                          where tlcrr.status == 0
                          select new
                          {
                              tlcrr.idLoteCobrancaRetornoRegistro,
                              tlcrr.nossoNumero,
                              tlcrr.dataProcessamento,
                              tlcrr.arquivoRetorno,
                              tlcrr.valorPago,
                              tlcrr.mensagem,
                              pp.pedidoId

                          }
                          ).Take(10).OrderByDescending(x => x.dataProcessamento).ToList();
        if (!String.IsNullOrEmpty(txtnossonumeropagosboletorej.Text))
        {
            pagamentos = (from pa in pagamentos where pa.nossoNumero == txtnossonumeropagosboletorej.Text select pa).ToList();

        }

        if (txtdatainicialpagosboletorej.Text != "")
        {
            try
            {
                var dtInicial = Convert.ToDateTime(txtdatainicialpagosboletorej.Text);
                pagamentos = pagamentos.Where(x => x.dataProcessamento >= dtInicial.Date).ToList();
            }
            catch (Exception) { }
        }
        if (txtdatafinalpagosboletorej.Text != "")
        {
            try
            {
                var dtFinal = Convert.ToDateTime(txtdatafinalpagosboletorej.Text);
                pagamentos = pagamentos.Where(x => x.dataProcessamento <= dtFinal.Date.AddDays(1)).ToList();
            }
            catch (Exception) { }
        }

        if (ddlmensagempagosboletorej.SelectedItem.Value != "0")
        {
            pagamentos = pagamentos.Where(x => x.mensagem == ddlmensagempagosboletorej.SelectedItem.Text).ToList();
        }

        grdpagosboletorej.DataSource = pagamentos;
        grdpagosboletorej.DataBind();

    }
    protected void grdpagosboletorej_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grdpagosboletorej.PageIndex = e.NewPageIndex;

        carregaGrdpagosboletorej();
    }
    protected void grdpagosboletorej_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        if (e.Row.RowType == DataControlRowType.DataRow)
        {


            Button btnproblematratado = (Button)e.Row.FindControl("btnproblematratado");
            btnproblematratado.Attributes.Add("onclick", "javascript:return " +
            "confirm('O problema foi tratado ?')");

            HtmlAnchor linkeditarpedido = (HtmlAnchor)e.Row.FindControl("linkeditarpedido");
            linkeditarpedido.HRef = "pedido.aspx?pedidoId=" + DataBinder.Eval(e.Row.DataItem, "pedidoId");
        }
    }


    protected void grdpagosboletorej_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "ProblemaTratado")
        {
            GridViewRow row = (GridViewRow)((Control)e.CommandSource).NamingContainer;
            int pedidoId = Convert.ToInt32(grdpagosboletorej.DataKeys[row.RowIndex].Values["pedidoId"]);
            int idLoteCobrancaRetornoRegistro = Convert.ToInt32(grdpagosboletorej.DataKeys[row.RowIndex].Values["idLoteCobrancaRetornoRegistro"]);
            string erro = "";
            try
            {
                using (var data = new dbCommerceDataContext())
                {


                    var loteCobrancaRetornoRegistro = (from tlcrr in data.tbLoteCobrancaRetornoRegistros
                                                       join pp in data.tbPedidoPagamentos on tlcrr.idPedidoPagamento equals pp.idPedidoPagamento
                                                       where tlcrr.idLoteCobrancaRetornoRegistro == idLoteCobrancaRetornoRegistro
                                                       select tlcrr).FirstOrDefault();

                    loteCobrancaRetornoRegistro.status = 1;
                    data.SubmitChanges();

                    string interacao = "Boleto nosso número:" + loteCobrancaRetornoRegistro.nossoNumero + "-valor:" +
                        loteCobrancaRetornoRegistro.valorPago.ToString("c2") + " que estava rejeitado foi tradado ";
                    interacaoInclui(pedidoId, interacao, "False");

                }

            }
            catch (Exception ex)
            {
                erro = ex.Message;
            }

            string meuscript;
            if (erro != "")
            {
                meuscript = @"alert('" + erro + "');";

                Page.ClientScript.RegisterStartupScript(this.GetType(), "MyScript", meuscript, true);
            }

            litpagosboletorej.Text = contapagosboletorej().ToString();
            carregaGrdpagosboletorej();


        }

    }

    #endregion

    #region ========= Consulta em tbPedidosPagamento=================
    public int contapedidospagamento()
    {
        var data = new dbCommerceDataContext();
        int qtd = (from ppr in data.tbPedidoPagamentoRiscos
                   join pp in data.tbPedidoPagamentos on ppr.idPedidoPagamento equals pp.idPedidoPagamento
                   where ppr.statusInterno == 1
                   select new { ppr.idPedidoPagamentoRisco }).Count();
        return qtd;
    }
    protected void btnFiltrarpedidospagamento_Click(object sender, EventArgs e)
    {
        carregaGrdpedidospagamento();

    }
    void carregaGrdpedidospagamento()
    {
        int pedidoId = 0;
        if (!String.IsNullOrEmpty(txtidpedidopedidospagamento.Text))
        {
            pedidoId = Convert.ToInt32(txtidpedidopedidospagamento.Text);
            // pagamentos = (from pa in pagamentos where pa.pedidoId == pedidoId select pa).ToList();

        }

        var data = new dbCommerceDataContext();
        var pagamentos = (from pp in data.tbPedidoPagamentos
                          where pp.pedidoId == pedidoId && pp.numeroCartao != ""
                          select new
                          {
                              pp.pedidoId,
                              pp.idPedidoPagamento,
                              dataEnvio = DateTime.Now,
                              pp.valor,
                              message = "",
                              pp.tbPedido.clienteId

                          }).OrderByDescending(x => x.pedidoId).ToList();



        if (!String.IsNullOrEmpty(txtidpagamentopedidospagamento.Text))
        {
            int pagamentoId = Convert.ToInt32(txtidpagamentopedidospagamento.Text);
            pagamentos = (from pa in pagamentos where pa.idPedidoPagamento == pagamentoId select pa).ToList();

        }

        grdpedidospagamento.DataSource = pagamentos;
        grdpedidospagamento.DataBind();
    }
    protected void grdpedidospagamento_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grdpedidospagamento.PageIndex = e.NewPageIndex;

        carregaGrdpedidospagamento();
    }
    protected void grdpedidospagamento_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        if (e.Row.RowType == DataControlRowType.DataRow)
        {

            HtmlAnchor linkeditarpedido = (HtmlAnchor)e.Row.FindControl("linkeditarpedido");
            linkeditarpedido.HRef = "pedido.aspx?pedidoId=" + DataBinder.Eval(e.Row.DataItem, "pedidoId");

        }
    }

    protected void grdpedidospagamento_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "ChecarClearSale")
        {
            GridViewRow row = (GridViewRow)((Control)e.CommandSource).NamingContainer;
            int idPedidoPagamento = Convert.ToInt32(grdpedidospagamento.DataKeys[row.RowIndex].Values["idPedidoPagamento"]);
            checarClarsale(idPedidoPagamento);
            carregaGrdpedidospagamento();
        }
    }

    #endregion

    #region ========= Pagos com cartão não finalizados =================
    public int contapagoscartaonaofinalizados()
    {
        var data = new dbCommerceDataContext();
        int qtd = (from ppr in data.tbPedidoPagamentoRiscos
                   join pp in data.tbPedidoPagamentos on ppr.idPedidoPagamento equals pp.idPedidoPagamento
                   where ppr.statusInterno == 1
                   select new { ppr.idPedidoPagamentoRisco }).Count();
        return qtd;
    }
    protected void btnFiltrarpagoscartaonaofinalizados_Click(object sender, EventArgs e)
    {
        carregaGrdpagoscartaonaofinalizados();

    }
    void carregaGrdpagoscartaonaofinalizados()
    {
        DateTime datainicio = DateTime.Today.AddDays(-5);
        DateTime datafinal = DateTime.Today.AddDays(-1).AddHours(23).AddMinutes(59).AddSeconds(59);

        if(!String.IsNullOrEmpty(txtdatapagoscartaonaofinalizados.Text))
        {
            datainicio = Convert.ToDateTime(txtdatapagoscartaonaofinalizados.Text);
            datafinal = datainicio.AddHours(23).AddMinutes(59).AddSeconds(59);
        }
       

        var data = new dbCommerceDataContext();

        
        var pagamentos = ( from pp in data.tbPedidoPagamentos 
                           where pp.pago == false && pp.numeroCartao != null && pp.numeroCartao != String.Empty && pp.cancelado == false && pp.pagamentoNaoAutorizado == false
                           && (pp.tbPedido.statusDoPedido == 2 || pp.tbPedido.statusDoPedido == 8 )
                           && (pp.tbPedido.dataHoraDoPedido >= datainicio && pp.tbPedido.dataHoraDoPedido <= datafinal)
                           select new{
                               pp.pedidoId,
                               pp.tbPedido.dataHoraDoPedido,
                               pp.idPedidoPagamento,
                               pp.tbPedido.valorCobrado
                           }).OrderByDescending(x => x.pedidoId).Take(100).ToList();

        grdpagoscartaonaofinalizados.DataSource = pagamentos;
        grdpagoscartaonaofinalizados.DataBind();
    }
    protected void grdpagoscartaonaofinalizados_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grdpagoscartaonaofinalizados.PageIndex = e.NewPageIndex;

        carregaGrdpagoscartaonaofinalizados();
    }
    protected void grdpagoscartaonaofinalizados_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        if (e.Row.RowType == DataControlRowType.DataRow)
        {

            HtmlAnchor linkeditarpedido = (HtmlAnchor)e.Row.FindControl("linkeditarpedido");
            linkeditarpedido.HRef = "pedido.aspx?pedidoId=" + DataBinder.Eval(e.Row.DataItem, "pedidoId");

        }
    }

    protected void grdpagoscartaonaofinalizados_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "ChecarClearSale")
        {
            GridViewRow row = (GridViewRow)((Control)e.CommandSource).NamingContainer;
            int idPedidoPagamento = Convert.ToInt32(grdpagoscartaonaofinalizados.DataKeys[row.RowIndex].Values["idPedidoPagamento"]);
            checarClarsale(idPedidoPagamento);
            carregaGrdpagoscartaonaofinalizados();
        }
    }

    #endregion

    #region ========= Análise de Quebra  =================
    public int contaanalisequebra()
    {
        var data = new dbCommerceDataContext();
        int qtd = (from ppr in data.tbPedidoPagamentoRiscos
                   join pp in data.tbPedidoPagamentos on ppr.idPedidoPagamento equals pp.idPedidoPagamento
                   where ppr.statusInterno == 1
                   select new { ppr.idPedidoPagamentoRisco }).Count();
        return qtd;
    }
    protected void btnFiltraranalisequebra_Click(object sender, EventArgs e)
    {
        carregaGrdanalisequebra();

    }
    void carregaGrdanalisequebra()
    {

        var data = new dbCommerceDataContext();

        DateTime dataInicial = DateTime.Today.AddMonths(-1);


        var pagamentos = (from pp in data.tbPedidoPagamentos
                          join ppg in data.tbPedidoPagamentoGateways on pp.idPedidoPagamento equals ppg.idPedidoPagamento
                          //where ppg.responseMessage == "WithError" && pp.pagamentoNaoAutorizado == false && pp.cancelado == false && pp.pago == false
                          where ppg.dataSolicitacao >= dataInicial
                          select new
                          {
                              pp.pedidoId,
                              pp.idPedidoPagamento,
                              ppg.dataSolicitacao,
                              pp.valor,
                              ppg.processorReturnCode,
                              ppg.processorMessage,
                              ppg.responseMessage
                          }).OrderByDescending(x => x.pedidoId).ToList();

        if (!String.IsNullOrEmpty(txtidpedidoanalisequebra.Text))
        {
            int pedidoId = Convert.ToInt32(txtidpedidoanalisequebra.Text);
            pagamentos = (from pa in pagamentos where pa.pedidoId == pedidoId select pa).ToList();

        }

        if (!String.IsNullOrEmpty(txtidpagamentoanalisequebra.Text))
        {
            int pagamentoId = Convert.ToInt32(txtidpagamentoanalisequebra.Text);
            pagamentos = (from pa in pagamentos where pa.idPedidoPagamento == pagamentoId select pa).ToList();

        }

        grdanalisequebra.DataSource = pagamentos;
        grdanalisequebra.DataBind();
    }
    protected void grdanalisequebra_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grdanalisequebra.PageIndex = e.NewPageIndex;

        carregaGrdanalisequebra();
    }
    protected void grdanalisequebra_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        if (e.Row.RowType == DataControlRowType.DataRow)
        {

            HtmlAnchor linkeditarpedido = (HtmlAnchor)e.Row.FindControl("linkeditarpedido");
            linkeditarpedido.HRef = "pedido.aspx?pedidoId=" + DataBinder.Eval(e.Row.DataItem, "pedidoId");

        }
    }

    protected void grdanalisequebra_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "ChecarClearSale")
        {
            GridViewRow row = (GridViewRow)((Control)e.CommandSource).NamingContainer;
            int idPedidoPagamento = Convert.ToInt32(grdanalisequebra.DataKeys[row.RowIndex].Values["idPedidoPagamento"]);
            checarClarsale(idPedidoPagamento);
            carregaGrdanalisequebra();
        }
    }

    #endregion
}