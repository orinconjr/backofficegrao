﻿<%@ Page Title="" Theme="Glass" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="comprasProdutosCadastro.aspx.cs" Inherits="admin_comprasProdutosCadastro" %>

<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxTimer" TagPrefix="dxt" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxMenu" Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxPopupControl" Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>

<%@ Register Assembly="CuteEditor" Namespace="CuteEditor" TagPrefix="CE" %>


<%@ Register Assembly="DevExpress.Web.v11.1" Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>


<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" runat="Server">
    <table cellpadding="0" cellspacing="0" style="width: 920px">
        <tr>
            <td class="tituloPaginas" valign="top">
                <asp:Label ID="lblAcao" runat="server">Compras - Produtos</asp:Label>
            </td>
        </tr>

        <tr>
            <td>

                <table align="center" cellpadding="0" cellspacing="0" style="width: 834px" border="0">
                    <tr>
                        <td colspan="2">
                            <asp:TextBox runat="server" ID="hdfLista" Visible="False" />
                            <asp:TextBox runat="server" ID="hdfIdProduto" Visible="False" />
                            &nbsp;
                        </td>
                    </tr>
                    <tr class="campos">
                        <td colspan="2" valign="top">Nome do Produto:<br />
                            <asp:TextBox runat="server" ID="txtProduto" Width="300"></asp:TextBox>&nbsp;
                <asp:CheckBox runat="server" ID="chkMateriaPrima" Text="Matéria Prima"></asp:CheckBox><br />
                            <br />
                            <asp:FileUpload ID="fileUpLoad" runat="server" />
                        </td>
                    </tr>
                    <tr class="campos">
                        <td>&nbsp;
                        </td>
                        <td style="text-align: right">
                            <asp:Image ID="imgProduto" runat="server" Width="100px" Height="100px" />
                        </td>

                    </tr>
                    <tr class="rotulos">
                        <td colspan="2" style="font-weight: bold; padding-top: 30px;">Produtos:
                        </td>
                    </tr>
                    <tr class="rotulos">
                        <td style="text-align: right;" colspan="2">
                            <asp:Button runat="server" ID="btnAdicionar" Text="Adicionar Fornecedor" OnClick="btnAdicionar_OnClick" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <dxwgv:ASPxGridView ID="grdFornecedores" runat="server" AutoGenerateColumns="False" Width="834px" Cursor="auto" KeyFieldName="idComprasFornecedor">
                                <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                                <Styles>
                                    <Footer Font-Bold="True">
                                    </Footer>
                                </Styles>
                                <SettingsText EmptyDataRow="Nenhum registro encontrado." />
                                <SettingsPager Position="Bottom" PageSize="50" ShowDisabledButtons="False" AlwaysShowPager="True">
                                    <Summary AllPagesText="Páginas: {0} - {1} ({2} items)" Text="Página {0} de {1} ({2} registros encontrados)" />
                                </SettingsPager>
                                <Settings ShowFilterRow="True" ShowGroupButtons="False" ShowHeaderFilterButton="True" ShowFooter="True" />
                                <SettingsEditing EditFormColumnCount="4" PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1" PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter" PopupEditFormWidth="700px" />
                                <Columns>
                                    <dxwgv:GridViewDataTextColumn Caption="ID" FieldName="idComprasFornecedor" Visible="False" VisibleIndex="0">
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataComboBoxColumn Caption="Fornecedor" FieldName="idComprasFornecedor">
                                        <PropertiesComboBox DataSourceID="sqlComprasFornecedor" TextField="fornecedor" ValueField="idComprasFornecedor">
                                        </PropertiesComboBox>
                                        <Settings AllowAutoFilter="True" AllowAutoFilterTextInputTimer="True" FilterMode="DisplayText" />
                                    </dxwgv:GridViewDataComboBoxColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Nome/Ref." FieldName="nomeFornecedor">
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataComboBoxColumn Caption="Unidade de Medida" Width="55px" FieldName="idComprasUnidadeMedida">
                                        <PropertiesComboBox DataSourceID="sqlUnidadesDeMedida" TextField="unidadeDeMedida" ValueField="idComprasUnidadesDeMedida">
                                        </PropertiesComboBox>
                                        <Settings AllowAutoFilter="True" AllowAutoFilterTextInputTimer="True" FilterMode="DisplayText" />
                                    </dxwgv:GridViewDataComboBoxColumn>
                                    <dxwgv:GridViewDataComboBoxColumn Caption="Fator Conversao" Width="55px" FieldName="idComprasUnidadeMedida">
                                        <PropertiesComboBox DataSourceID="sqlUnidadesDeMedida" TextField="fatorConversao" ValueField="idComprasUnidadesDeMedida">
                                        </PropertiesComboBox>
                                        <Settings AllowAutoFilter="True" AllowAutoFilterTextInputTimer="True" FilterMode="DisplayText" />
                                    </dxwgv:GridViewDataComboBoxColumn>
                                    <dxwgv:GridViewDataDateColumn Name="dataValor" Caption="Data do Valor" FieldName="dataValor">
                                        <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit>
                                    </dxwgv:GridViewDataDateColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Valor" FieldName="valor">
                                        <PropertiesTextEdit DisplayFormatString="C">
                                        </PropertiesTextEdit>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Dias da última atualização de valor " FieldName="dif">
                                    </dxwgv:GridViewDataTextColumn>
                                    
                                    <dxwgv:GridViewDataTextColumn Caption="Histórico" Name="historico" Width="80">
                                        <DataItemTemplate>
                                            <asp:LinkButton runat="server" OnCommand="btnHistorico_OnCommand" ID="btnHistorico" CommandArgument='<%# Eval("idComprasFornecedor") %>' EnableViewState="False">
                                    Ver Histórico
                                            </asp:LinkButton>
                                        </DataItemTemplate>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Editar" Name="editar" Width="80">
                                        <DataItemTemplate>
                                            <asp:ImageButton ImageUrl="~/admin/images/btEditar.jpg" runat="server" OnCommand="btnEditarFornecedor_OnCommand" ID="btnEditarFornecedor" CommandArgument='<%# Eval("idComprasFornecedor") %>' EnableViewState="False"></asp:ImageButton>
                                        </DataItemTemplate>
                                    </dxwgv:GridViewDataTextColumn>
                                </Columns>
                                <StylesEditors>
                                    <Label Font-Bold="True">
                                    </Label>
                                </StylesEditors>
                            </dxwgv:ASPxGridView>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right;">&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right;">
                            <asp:Button runat="server" ID="btnGravar" Text="Gravar" OnClick="btnGravar_OnClick" />
                        </td>
                    </tr>

                </table>
            </td>
        </tr>
    </table>


    <dx:ASPxPopupControl ID="popFornecedor" runat="server" CloseAction="CloseButton" Modal="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
        ClientInstanceName="popFornecedor" HeaderText="Adicionar Fornecedor" AllowDragging="True" PopupAnimationType="None" EnableViewState="True" Height="75" AutoUpdatePosition="True">
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl9" runat="server">
                <div style="width: 800px; height: 65px;">
                    <asp:Panel runat="server" DefaultButton="btnGravarFornecedor">
                        <table style="width: 100%">
                            <tr>
                                <td>Fornecedor:<br />
                                    <dxe:ASPxComboBox DataSourceID="sqlComprasFornecedor" ID="ddlFornecedor" runat="server" ValueField="idComprasFornecedor" TextField="fornecedor" Width="200px" DropDownStyle="DropDownList" IncrementalFilteringMode="StartsWith">
                                    </dxe:ASPxComboBox>
                                </td>
                                <td>Nome/Referência:<br />
                                    <asp:TextBox runat="server" ID="txtNomeFornecedor"></asp:TextBox>
                                </td>
                                <td>Unidade de Medida:<br />
                                    <dxe:ASPxComboBox DataSourceID="sqlUnidadesDeMedida" ID="ddlUnidadesDeMedida" runat="server" ValueField="idComprasUnidadesDeMedida" TextField="unidadeDeMedida" Width="150px" DropDownStyle="DropDown" IncrementalFilteringMode="StartsWith">
                                    </dxe:ASPxComboBox>
                                </td>
                                <td>Valor:<br />
                                    <dxe:ASPxTextBox ID="txtValor" runat="server" Width="100">
                                        <MaskSettings Mask="$<0..999999g>.<00..99>" IncludeLiterals="DecimalSymbol" />
                                        <ValidationSettings ErrorDisplayMode="ImageWithTooltip" />
                                    </dxe:ASPxTextBox>
                                    <asp:RequiredFieldValidator ID="rqvpreco" ValidationGroup="vgFornecedor" runat="server" ControlToValidate="txtValor" Display="Dynamic" ErrorMessage="Preencha o valor." Font-Size="10px" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                </td>
                                <td>&nbsp;<br />
                                    <asp:Button runat="server" ID="btnGravarFornecedor" Text="Gravar Fornecedor" OnClick="btnGravarFornecedor_OnClick" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <asp:HiddenField runat="server" ID="hdfIdFornecedor" />
                </div>
            </dx:PopupControlContentControl>
        </ContentCollection>
        <ContentStyle>
            <Paddings PaddingBottom="5px" />
        </ContentStyle>
    </dx:ASPxPopupControl>

    <dx:ASPxPopupControl ID="popHistoricoFornecedor" runat="server" CloseAction="CloseButton" Modal="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
        ClientInstanceName="popHistoricoFornecedor" HeaderText="Adicionar Fornecedor" AllowDragging="True" PopupAnimationType="None" EnableViewState="True" Height="75" AutoUpdatePosition="True">
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl10" runat="server">
                <div style="width: 800px; height: 500px;">
                    <dxwgv:ASPxGridView Settings-ShowVerticalScrollBar="True" Settings-VerticalScrollableHeight="330" ID="grdHistoricoFornecedor" runat="server" AutoGenerateColumns="False" Width="800px" Cursor="auto" KeyFieldName="idComprasFornecedor">
                        <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                        <Styles>
                            <Footer Font-Bold="True">
                            </Footer>
                        </Styles>
                        <SettingsText EmptyDataRow="Nenhum registro encontrado." />
                        <SettingsPager Position="Bottom" PageSize="50" ShowDisabledButtons="False" AlwaysShowPager="True">
                            <Summary AllPagesText="Páginas: {0} - {1} ({2} items)" Text="Página {0} de {1} ({2} registros encontrados)" />
                        </SettingsPager>
                        <Settings ShowFilterRow="True" ShowGroupButtons="False" ShowHeaderFilterButton="True" ShowFooter="True" />
                        <SettingsEditing EditFormColumnCount="4" PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1" PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter" PopupEditFormWidth="700px" />
                        <Columns>
                            <dxwgv:GridViewDataTextColumn Caption="ID" FieldName="idComprasFornecedor" Visible="False" VisibleIndex="0" Width="50">
                            </dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataComboBoxColumn Caption="Usuário" FieldName="idUsuario">
                                <PropertiesComboBox DataSourceID="sqlUsuarios" TextField="usuarioNome" ValueField="usuarioId">
                                </PropertiesComboBox>
                                <Settings AllowAutoFilter="True" AllowAutoFilterTextInputTimer="True" FilterMode="DisplayText" />
                            </dxwgv:GridViewDataComboBoxColumn>
                            <dxwgv:GridViewDataTextColumn Caption="Nome/Ref." FieldName="nomeFornecedor">
                            </dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataComboBoxColumn Caption="Unidade de Medida" FieldName="idComprasUnidadeMedida">
                                <PropertiesComboBox DataSourceID="sqlUnidadesDeMedida" TextField="unidadeDeMedida" ValueField="idComprasUnidadesDeMedida">
                                </PropertiesComboBox>
                                <Settings AllowAutoFilter="True" AllowAutoFilterTextInputTimer="True" FilterMode="DisplayText" />
                            </dxwgv:GridViewDataComboBoxColumn>
                            <dxwgv:GridViewDataDateColumn Name="data" Caption="Data do Valor" FieldName="data">
                                <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit>
                            </dxwgv:GridViewDataDateColumn>
                            <dxwgv:GridViewDataTextColumn Caption="Valor" FieldName="valor">
                                <PropertiesTextEdit DisplayFormatString="C">
                                </PropertiesTextEdit>
                            </dxwgv:GridViewDataTextColumn>
                        </Columns>
                        <StylesEditors>
                            <Label Font-Bold="True">
                            </Label>
                        </StylesEditors>
                    </dxwgv:ASPxGridView>
                </div>
            </dx:PopupControlContentControl>
        </ContentCollection>
        <ContentStyle>
            <Paddings PaddingBottom="5px" />
        </ContentStyle>
    </dx:ASPxPopupControl>

    <asp:LinqDataSource ID="sqlComprasFornecedor" runat="server" ContextTypeName="dbCommerceDataContext" TableName="tbComprasFornecedors">
    </asp:LinqDataSource>
    <asp:LinqDataSource ID="sqlUnidadesDeMedida" runat="server" ContextTypeName="dbCommerceDataContext" TableName="tbComprasUnidadesDeMedidas">
    </asp:LinqDataSource>
    <asp:LinqDataSource ID="sqlUsuarios" runat="server" ContextTypeName="dbCommerceDataContext" TableName="tbUsuarios">
    </asp:LinqDataSource>
</asp:Content>
