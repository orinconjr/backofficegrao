﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxGridView;

public partial class admin_notaFiscalFabrica : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
            carregaGrid();
    }

    protected void grd_OnCustomUnboundColumnData(object sender, ASPxGridViewColumnDataEventArgs e)
    {

    }
    protected void grd_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {

    }
    protected void btnGerarNotaFiscal_OnClick(object sender, EventArgs e)
    {
        int idPedidoFornecedor = 0;
        int.TryParse(txtNumeroRomaneio.Text, out idPedidoFornecedor);
        if(idPedidoFornecedor > 0)
        {
            var data = new dbCommerceDataContext();
            var pedidoFornecedor = (from c in data.tbPedidoFornecedors where c.idPedidoFornecedor == idPedidoFornecedor select c).FirstOrDefault();

            if (pedidoFornecedor == null)
            {
                rnFuncoes.Alerta(this, "Romaneio não encontrado.");
                return;
            }

            if (pedidoFornecedor.idFornecedor != 82 && pedidoFornecedor.idFornecedor != 153 && pedidoFornecedor.idFornecedor != 182 && pedidoFornecedor.idFornecedor != 204 && pedidoFornecedor.idFornecedor != 205 && pedidoFornecedor.idFornecedor != 206 && pedidoFornecedor.idFornecedor != 124 && pedidoFornecedor.idFornecedor != 71 && pedidoFornecedor.idFornecedor != 222)
            {
                rnFuncoes.Alerta(this, "Romaneio não pertence à fábrica.");
                return;
            }

            var notaGerada = rnNotaFiscal.GerarNotaFiscalRomaneioFabrica(idPedidoFornecedor);
            rnNotaFiscal.autorizarNota(notaGerada.xml, 0, notaGerada.idNotaFiscal);

            carregaGrid();
            rnFuncoes.Alerta(this, "Nota gerada com sucesso.");
            popAdicionarChamado.ShowOnPageLoad = false;
        }
    }

    private void carregaGrid()
    {
        using (var data = new dbCommerceDataContext())
        {
            var notasFiscais =
                from n in data.tbNotaFiscals where n.idPedidoFornecedor != null
                join r in data.tbPedidoFornecedors on n.idPedidoFornecedor equals r.idPedidoFornecedor
                join f in data.tbProdutoFornecedors on r.idFornecedor equals f.fornecedorId
                orderby n.dataHora descending
                select new
                {
                    n.idPedidoFornecedor,
                    f.fornecedorNome,
                    n.numeroNota,
                    n.linkDanfe,
                    n.dataHora
                };

            grd.DataSource = notasFiscais.ToList();
            grd.DataBind();
        }
    }

    protected void btnGerarNovaNotaFiscal_OnClick(object sender, EventArgs e)
    {
        popAdicionarChamado.ShowOnPageLoad = true;        
        hdfIdAgendamento.Value = string.Empty;
        txtNumeroRomaneio.Text = string.Empty;
        HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
        usuarioLogadoId = Request.Cookies["usuarioLogadoId"];
    }    
}