﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxTabControl;
using DevExpress.Web.ASPxEditors;
using DevExpress.Data;
using DevExpress.Web.ASPxUploadControl;
using System.IO;
using System.Data.SqlClient;

public partial class admin_contasParaDeposito : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    //protected void btPdf_Click(object sender, ImageClickEventArgs e)
    //{
    //    this.grdEx.WritePdfToResponse();
    //}
    //protected void btXsl_Click(object sender, ImageClickEventArgs e)
    //{
    //    this.grdEx.WriteXlsToResponse();
    //}
    //protected void btRtf_Click(object sender, ImageClickEventArgs e)
    //{
    //    this.grdEx.WriteRtfToResponse();
    //}
    //protected void btCsv_Click(object sender, ImageClickEventArgs e)
    //{
    //    this.grdEx.WriteCsvToResponse();
    //}

    protected void grd_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType != GridViewRowType.EditForm) return;
        {
            Image image1 = (Image)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "imagem1");
            if (image1.ImageUrl.ToString() == "../images/")
            {
                image1.Visible = false;
            }
        }
    }

    protected void grd_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        FileUpload flu1 = (FileUpload)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "flu1");

        if (flu1.HasFile)
            e.NewValues["logoDobanco"] = rnFuncoes.limpaStringDeixaPonto(flu1.PostedFile.FileName.Substring(flu1.PostedFile.FileName.LastIndexOf("\\") + 1).Trim().ToLower());
    }

    protected void grd_RowInserted(object sender, DevExpress.Web.Data.ASPxDataInsertedEventArgs e)
    {
        FileUpload flu1 = (FileUpload)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "flu1");

        if (flu1.HasFile)
        {
            flu1.PostedFile.SaveAs(ConfigurationManager.AppSettings["caminhoFisico"] + "images\\" + rnFuncoes.limpaStringDeixaPonto(flu1.PostedFile.FileName.Substring(flu1.PostedFile.FileName.LastIndexOf("\\") + 1).Trim().ToLower()));
        }
    }

    protected void grd_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        FileUpload flu1 = (FileUpload)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "flu1");

        if (flu1.HasFile)
            e.NewValues["logoDobanco"] = rnFuncoes.limpaStringDeixaPonto(flu1.PostedFile.FileName.Substring(flu1.PostedFile.FileName.LastIndexOf("\\") + 1).Trim().ToLower());
    }

    protected void grd_RowUpdated(object sender, DevExpress.Web.Data.ASPxDataUpdatedEventArgs e)
    {
        if (e.Exception == null)
        {
            FileUpload flu1 = (FileUpload)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "flu1");

            if (flu1.HasFile)
            {
                flu1.PostedFile.SaveAs(ConfigurationManager.AppSettings["caminhoFisico"] + "images\\" + rnFuncoes.limpaStringDeixaPonto(flu1.PostedFile.FileName.Substring(flu1.PostedFile.FileName.LastIndexOf("\\") + 1).Trim().ToLower()));
            }
        }
    }

    protected void grd_RowDeleted(object sender, DevExpress.Web.Data.ASPxDataDeletedEventArgs e)
    {
        if (e.Exception == null)
        {
            int marcaId = Convert.ToInt32(e.Keys[0]);

            if (Directory.Exists(ConfigurationManager.AppSettings["caminhoFisico"] + "marcas\\" + marcaId))
            {
                Directory.Delete(ConfigurationManager.AppSettings["caminhoFisico"] + "marcas\\" + marcaId, true);
            }
        }
        else
        {
            SqlException ex = (SqlException)e.Exception;

            if (ex.Number == 547)
            {
                Response.Write("<script>alert('Essa marca não pode ser excluida por que existe produtos relacionado a ela.');</script>");
                e.ExceptionHandled = true;
            }
        }
    }
}
