﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxCallback;
using DevExpress.Web.ASPxGridView;

public partial class admin_carrinhosEnderecamento : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        fillGrid(false);
    }
    
    private void fillGrid(bool rebind)
    {
        //grd.DataSource = sql;
        int idCentroDistribuicao = rdbCd1.Checked ? 1 : rdbCd2.Checked ? 2 : rdbCd3.Checked ? 3 : rdbCd4.Checked ? 4 : 5;
        var data = new dbCommerceDataContext();
        if (!Page.IsPostBack && !Page.IsCallback)
        {
            var produtosEnviados = (from c in data.tbTransferenciaEnderecoProdutos
                                    join d in data.tbProdutoEstoques on c.idPedidoFornecedorItem equals d.idPedidoFornecedorItem
                                    where c.dataEnderecamento == null && d.enviado == true
                                    select c).ToList();
            foreach(var produtoEnvido in produtosEnviados)
            {
                produtoEnvido.dataEnderecamento = DateTime.Now;
            }
            data.SubmitChanges();
        }

        var carrinhos = (from c in data.tbTransferenciaEnderecos
                         join d in data.tbTransferenciaEnderecoProdutos on c.idTransferenciaEndereco equals d.idTransferenciaEndereco into produtos
                       orderby c.idTransferenciaEndereco
                       select new
                       {
                           c.idTransferenciaEndereco,
                           c.usuarioEntrada,
                           c.tbEnderecamentoTransferencia.enderecamentoTransferencia,
                           c.dataCadastro,
                           c.dataInicio,
                           c.dataFim,
                           c.tbEnderecamentoTransferencia.idCentroDistribuicao,
                           usuarioEnderecamento = c.idUsuarioExpedicao == null ? "" : c.tbUsuarioExpedicao.nome,
                           totalItens = produtos.Count(),
                           faltandoEnderecar = produtos.Count(x => x.dataEnderecamento == null)
                       }).Where(x => x.idCentroDistribuicao == idCentroDistribuicao);
        if (rdbAberto.Checked) carrinhos = carrinhos.Where(x => x.dataFim == null && x.faltandoEnderecar > 0);
        grd.DataSource = carrinhos;
        if ((!Page.IsPostBack && !Page.IsCallback) | rebind) grd.DataBind();
    }
    
    protected void rdbAberto_OnCheckedChanged(object sender, EventArgs e)
    {
        fillGrid(true);
    }


    protected void rdbCd1_CheckedChanged(object sender, EventArgs e)
    {
        fillGrid(true);
    }

    protected void rdbCd2_CheckedChanged(object sender, EventArgs e)
    {
        fillGrid(true);
    }

    protected void rdbCd3_CheckedChanged(object sender, EventArgs e)
    {
        fillGrid(true);
    }

    protected void rdbCd4_CheckedChanged(object sender, EventArgs e)
    {
        fillGrid(true);
    }
    protected void rdbCd5_CheckedChanged(object sender, EventArgs e)
    {
        fillGrid(true);
    }
}