﻿<%@ Page Title="" Language="C#" Theme="Glass" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="pedidosJadlog.aspx.cs" Inherits="admin_pedidosJadlog" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxEditors" Assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" Runat="Server">
    <script language="javascript" type="text/javascript">
        function ApplyFilter(dde, dateFrom, dateTo) {
            var d1 = dateFrom.GetText();
            var d2 = dateTo.GetText();
            if (d1 == "" || d2 == "")
                return;
            dde.SetText(d1 + "|" + d2);
            //grd.ApplyFilter("[data] >= '" + d1 +" 00:00:00' && [data] <= '" + d2 + " 23:59:59'");
            grd.AutoFilterByColumn("data", dde.GetText());
        }

        function OnDropDown(s, dateFrom, dateTo) {
            var str = s.GetText();
            if (str == "") {
                // default date (1950-1961 for demo)     
                dateFrom.SetDate(new Date(1950, 0, 1));
                dateTo.SetDate(new Date(1960, 11, 31));
                return;
            }
            var d = str.split("|");
            dateFrom.SetText(d[0]);
            dateTo.SetText(d[1]);
        }
    </script>

    <script type="text/javascript">
        function CalculaTotalCorreios() {
            var txtCorreiosAr1 = document.getElementById('<%= txtCorreiosAr1.ClientID %>');
            var txtCorreiosAr2 = document.getElementById('<%= txtCorreiosAr2.ClientID %>');
            var txtCorreiosAr3 = document.getElementById('<%= txtCorreiosAr3.ClientID %>');
            var txtCorreiosAr4 = document.getElementById('<%= txtCorreiosAr4.ClientID %>');
            var txtCorreiosMaoPropria1 = document.getElementById('<%= txtCorreiosMaoPropria1.ClientID %>');
            var txtCorreiosMaoPropria2 = document.getElementById('<%= txtCorreiosMaoPropria2.ClientID %>');
            var txtCorreiosMaoPropria3 = document.getElementById('<%= txtCorreiosMaoPropria3.ClientID %>');
            var txtCorreiosMaoPropria4 = document.getElementById('<%= txtCorreiosMaoPropria4.ClientID %>');
            var txtCorreiosSeguro1 = document.getElementById('<%= txtCorreiosSeguro1.ClientID %>');
            var txtCorreiosSeguro2 = document.getElementById('<%= txtCorreiosSeguro2.ClientID %>');
            var txtCorreiosSeguro3 = document.getElementById('<%= txtCorreiosSeguro3.ClientID %>');
            var txtCorreiosSeguro4 = document.getElementById('<%= txtCorreiosSeguro4.ClientID %>');
            var txtCorreiosValor1 = document.getElementById('<%= txtCorreiosValor1.ClientID %>');
            var txtCorreiosValor2 = document.getElementById('<%= txtCorreiosValor2.ClientID %>');
            var txtCorreiosValor3 = document.getElementById('<%= txtCorreiosValor3.ClientID %>');
            var txtCorreiosValor4 = document.getElementById('<%= txtCorreiosValor4.ClientID %>');

            var valor1 = parseFloat('0' + txtCorreiosValor1.value.replace(".", "").replace(",", "."));
            var valor2 = parseFloat('0' + txtCorreiosValor2.value.replace(".", "").replace(",", "."));
            var valor3 = parseFloat('0' + txtCorreiosValor3.value.replace(".", "").replace(",", "."));
            var valor4 = parseFloat('0' + txtCorreiosValor4.value.replace(".", "").replace(",", "."));
            var ar1 = parseFloat('0' + txtCorreiosAr1.value.replace(".", "").replace(",", "."));
            var ar2 = parseFloat('0' + txtCorreiosAr2.value.replace(".", "").replace(",", "."));
            var ar3 = parseFloat('0' + txtCorreiosAr3.value.replace(".", "").replace(",", "."));
            var ar4 = parseFloat('0' + txtCorreiosAr4.value.replace(".", "").replace(",", "."));
            var maopropria1 = parseFloat('0' + txtCorreiosMaoPropria1.value.replace(".", "").replace(",", "."));
            var maopropria2 = parseFloat('0' + txtCorreiosMaoPropria2.value.replace(".", "").replace(",", "."));
            var maopropria3 = parseFloat('0' + txtCorreiosMaoPropria3.value.replace(".", "").replace(",", "."));
            var maopropria4 = parseFloat('0' + txtCorreiosMaoPropria4.value.replace(".", "").replace(",", "."));
            var seguro1 = parseFloat('0' + txtCorreiosSeguro1.value.replace(".", "").replace(",", "."));
            var seguro2 = parseFloat('0' + txtCorreiosSeguro2.value.replace(".", "").replace(",", "."));
            var seguro3 = parseFloat('0' + txtCorreiosSeguro3.value.replace(".", "").replace(",", "."));
            var seguro4 = parseFloat('0' + txtCorreiosSeguro4.value.replace(".", "").replace(",", "."));


            var valorTotal = valor1 + valor2 + valor3 + valor4 + ar1 + ar2 + ar3 + ar4 + maopropria1 + maopropria2 + maopropria3 + maopropria4 + seguro1 + seguro2 + seguro3 + seguro4;

            var litCorreiosTotal = document.getElementById('<%= litCorreiosTotal.ClientID %>');
            litCorreiosTotal.innerHTML = valorTotal.toFixed(2).replace(".", ",");

        }
        
        function CalculaKg() {
            var txtVol1 = document.getElementById('<%= txtVol1.ClientID %>');
            var txtVol2 = document.getElementById('<%= txtVol2.ClientID %>');
            var txtVol3 = document.getElementById('<%= txtVol3.ClientID %>');
            var txtVol4 = document.getElementById('<%= txtVol4.ClientID %>');
            var txtVol5 = document.getElementById('<%= txtVol5.ClientID %>');
            var txtVol6 = document.getElementById('<%= txtVol6.ClientID %>');
            var txtVol7 = document.getElementById('<%= txtVol7.ClientID %>');
            var txtVol8 = document.getElementById('<%= txtVol8.ClientID %>');
            var vol1 = parseInt('0' + txtVol1.value);
            var vol2 = parseInt('0' + txtVol2.value);
            var vol3 = parseInt('0' + txtVol3.value);
            var vol4 = parseInt('0' + txtVol4.value);
            var vol5 = parseInt('0' + txtVol5.value);
            var vol6 = parseInt('0' + txtVol6.value);
            var vol7 = parseInt('0' + txtVol7.value);
            var vol8 = parseInt('0' + txtVol8.value);
            var volTotal = vol1 + vol2 + vol3 + vol4 + vol5 + vol6 + vol7 + vol8;
            var txtKg = document.getElementById('<%= txtKg.ClientID %>');
            txtKg.value = String(volTotal);
            


            var txtFrete = document.getElementById('<%= txtFrete.ClientID %>');
            var txtInterior = document.getElementById('<%= txtInterior.ClientID %>');
            var txtValorAr = document.getElementById('<%= txtValorAr.ClientID %>');
            var txtValorMaoPropria = document.getElementById('<%= txtValorMaoPropria.ClientID %>');
            var txtValorSeguro = document.getElementById('<%= txtValorSeguro.ClientID %>');
            var txtNota = document.getElementById('<%= txtNota.ClientID %>');
            var ddlTransportadora = document.getElementById('<%= ddlTransportadora.ClientID %>');
            
            var transportadora = ddlTransportadora.options[ddlTransportadora.selectedIndex].value;
            var seguro = 0;
            
            if (transportadora == "jadlog") {
                if (parseFloat('0' + txtNota.value.replace(".", "").replace(",", ".")) > 0) {
                    seguro = parseFloat('0' + txtNota.value.replace(".", "").replace(",", ".")) * 0.0066;
                } else {
                    seguro = 0.33;
                }
            }
            
            var frete = parseFloat('0' + txtFrete.value.replace(".", "").replace(",", "."));
            var valor = parseFloat('0' + txtInterior.value.replace(".", "").replace(",", "."));
            

            var valorTotal = frete + valor + seguro + parseFloat('0' + txtValorAr.value.replace(".", "").replace(",", ".")) + parseFloat('0' + txtValorMaoPropria.value.replace(".", "").replace(",", "."));

            var litValorTotal = document.getElementById('<%= litValorTotal.ClientID %>');
            litValorTotal.innerHTML = valorTotal.toFixed(2).replace(".", ",");
            
            txtValorSeguro.value = seguro.toFixed(2).replace(".", ",");

        }
    </script>
    <table cellpadding="0" cellspacing="0" style="width: 920px">
        <tr>
            <td class="tituloPaginas" valign="top">
                <asp:Label ID="lblAcao" runat="server">Enviar pedido para Transportadora</asp:Label>
            </td>
        </tr>
        <tr>
            <td>
    <asp:Panel runat="server" ID="pnEnviarJadlog">
        <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
            <tr class="rotulos">
                <td style="text-align: right;padding-bottom: 15px;">
                    <asp:LinkButton runat="server" ID="btnVerPedidos" OnClick="btnVerPedidos_OnClick">Ver pedidos enviados</asp:LinkButton>
                </td>
            </tr>
        </table>
                <asp:Panel runat="server" ID="pnSelecionarPedido" DefaultButton="btnSelecionar">
                    <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
                        <tr class="rotulos">
                            <td>
                                <table>
                                    <tr>
                                        <td style="padding-right: 30px;">Pedido:</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="padding-right: 30px;">
                                            <asp:TextBox runat="server" ID="txtPedidoId" CssClass="campos"></asp:TextBox>
                                        </td>
                                        <td style="padding-right: 30px;">
                                            <asp:Button runat="server" ID="btnSelecionar" Text="Selecionar" OnClick="btnSelecionar_OnClick"/>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Panel runat="server" ID="pnEnviarPedido" Visible="False">
                    <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
                        <tr class="rotulos">
                            <td colspan="4">
                                <table>
                                    <tr>
                            <td>
                                <span style="font-size: 36px;">
                                Pedido: <asp:Literal runat="server" ID="litIdPedido"></asp:Literal>
                                </span>
                                <asp:Panel runat="server" ID="pnMensagemEnviado" Visible="False">
                                    <span style="font-weight: bold; color:darkred;">ATENÇÃO: Pedido já enviado no dia <asp:Literal runat="server" ID="litDataEnviado"></asp:Literal></span>
                                </asp:Panel>
                            </td>
                            <td>
                                
                            </td>
                            <td>
                                
                            </td>
                            <td>
                                
                            </td>
                        </tr>
                        <tr class="rotulos">
                            <td>
                                Transportadora:<br/>
                               <asp:DropDownList runat="server" ID="ddlTransportadora" OnSelectedIndexChanged="ddlTransportadora_OnSelectedIndexChanged" AutoPostBack="True">
                                   <Items>
                                       <asp:ListItem Text="Jadlog" Value="jadlog"></asp:ListItem>
                                       <asp:ListItem Text="Correios" Value="correios"></asp:ListItem>
                                   </Items> 
                               </asp:DropDownList> 
                            </td>
                            <td>
                                
                            </td>
                            <td>
                                
                            </td>
                            <td>
                                
                            </td></tr>
                            </table>
                        </td>
                        </tr>
                        <asp:Panel runat="server" ID="pnCadastroJadlog">
                        <tr class="rotulos">
                            <td>
                                Peso Vol 1:<br/>
                                <asp:TextBox runat="server" ID="txtVol1" CssClass="campos" Text="0"></asp:TextBox>
                            </td>
                            <td>
                                Peso Vol 2:<br/>
                                <asp:TextBox runat="server" ID="txtVol2" CssClass="campos" Text="0"></asp:TextBox>
                            </td>
                            <td>
                                Peso Vol 3:<br/>
                                <asp:TextBox runat="server" ID="txtVol3" CssClass="campos" Text="0"></asp:TextBox>
                            </td>
                            <td>
                                Peso Vol 4:<br/>
                                <asp:TextBox runat="server" ID="txtVol4" CssClass="campos" Text="0"></asp:TextBox>
                            </td>
                            <td>
                                Peso Vol 5:<br/>
                                <asp:TextBox runat="server" ID="txtVol5" CssClass="campos" Text="0"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="rotulos">
                            <td>
                                Peso Vol 6:<br/>
                                <asp:TextBox runat="server" ID="txtVol6" CssClass="campos" Text="0"></asp:TextBox>
                            </td>
                            <td>
                                Peso Vol 7:<br/>
                                <asp:TextBox runat="server" ID="txtVol7" CssClass="campos" Text="0"></asp:TextBox>
                            </td>
                            <td>
                                Peso Vol 8:<br/>
                                <asp:TextBox runat="server" ID="txtVol8" CssClass="campos" Text="0"></asp:TextBox>
                            </td>
                            <td>
                                Peso total:<br/>
                                <asp:TextBox runat="server" ID="txtKg" CssClass="campos" Text="0" ReadOnly="True"></asp:TextBox>
                            </td>
                            <td>
                                
                            </td>
                        </tr>
                        <tr class="rotulos">
                            <td>
                                Faixa:<br/>
                                <asp:TextBox runat="server" ID="txtFaixa" CssClass="campos" Text="0"></asp:TextBox>
                            </td>
                            <td>
                                Valor do Frete:<br/>
                                <asp:TextBox runat="server" ID="txtFrete" CssClass="campos" Text="0"></asp:TextBox>
                            </td>
                            <td>
                                Valor do Interior:<br/>
                                <asp:TextBox runat="server" ID="txtInterior" CssClass="campos" Text="0"></asp:TextBox>
                            </td>
                            <td>
                                Valor da Nota:<br/>
                                <asp:TextBox runat="server" ID="txtNota" CssClass="campos" Text="0"></asp:TextBox>
                            </td>
                            <td>
                                Valor do AR:<br/>
                                <asp:TextBox runat="server" ID="txtValorAr" CssClass="campos" Text="0"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="rotulos">
                            <td>
                                Mão Própria:<br/>
                                <asp:TextBox runat="server" ID="txtValorMaoPropria" CssClass="campos" Text="0"></asp:TextBox>
                            </td>
                            <td>
                                Valor do Seguro:<br/>
                                <asp:TextBox runat="server" ID="txtValorSeguro" CssClass="campos" Text="0"></asp:TextBox>
                            </td>
                            <td style="font-size: 14px;">
                                <b>
                                Valor Total:<br/>
                                <asp:Label runat="server" ID="litValorTotal"></asp:Label></b>
                                
                            </td>
                            <td>
                                
                            </td>
                            <td>
                                
                            </td>
                        </tr>
                        </asp:Panel>
                        <asp:Panel runat="server" ID="pnCadastroCorreios" Visible="False">
                            <tr class="rotulos">
                                <td>
                                    Peso Vol 1:<br/>
                                    <asp:TextBox runat="server" ID="txtCorreiosVol1" CssClass="campos" Text="0"></asp:TextBox>
                                </td>
                                <td>
                                    Peso Vol 2:<br/>
                                    <asp:TextBox runat="server" ID="txtCorreiosVol2" CssClass="campos" Text="0"></asp:TextBox>
                                </td>
                                <td>
                                    Peso Vol 3:<br/>
                                    <asp:TextBox runat="server" ID="txtCorreiosVol3" CssClass="campos" Text="0"></asp:TextBox>
                                </td>
                                <td>
                                    Peso Vol 4:<br/>
                                    <asp:TextBox runat="server" ID="txtCorreiosVol4" CssClass="campos" Text="0"></asp:TextBox>
                                </td>
                                <td>
                                
                                </td>
                            </tr>
                            <tr class="rotulos">
                                <td>
                                    Cubagem Vol 1:<br/>
                                    <asp:TextBox runat="server" ID="txtCorreiosCubagem1" CssClass="campos" Text="0"></asp:TextBox>
                                </td>
                                <td>
                                    Cubagem Vol 2:<br/>
                                    <asp:TextBox runat="server" ID="txtCorreiosCubagem2" CssClass="campos" Text="0"></asp:TextBox>
                                </td>
                                <td>
                                    Cubagem Vol 3:<br/>
                                    <asp:TextBox runat="server" ID="txtCorreiosCubagem3" CssClass="campos" Text="0"></asp:TextBox>
                                </td>
                                <td>
                                    Cubagem Vol 4:<br/>
                                    <asp:TextBox runat="server" ID="txtCorreiosCubagem4" CssClass="campos" Text="0"></asp:TextBox>
                                </td>
                                <td>
                                
                                </td>
                            </tr>
                            <tr class="rotulos">
                                <td>
                                    Valor Vol 1:<br/>
                                    <asp:TextBox runat="server" ID="txtCorreiosValor1" CssClass="campos" Text="0"></asp:TextBox>
                                </td>
                                <td>
                                    Valor Vol 2:<br/>
                                    <asp:TextBox runat="server" ID="txtCorreiosValor2" CssClass="campos" Text="0"></asp:TextBox>
                                </td>
                                <td>
                                    Valor Vol 3:<br/>
                                    <asp:TextBox runat="server" ID="txtCorreiosValor3" CssClass="campos" Text="0"></asp:TextBox>
                                </td>
                                <td>
                                    Valor Vol 4:<br/>
                                    <asp:TextBox runat="server" ID="txtCorreiosValor4" CssClass="campos" Text="0"></asp:TextBox>
                                </td>
                                <td>
                                
                                </td>
                            </tr>
                            <tr class="rotulos">
                                <td>
                                    AR Vol 1:<br/>
                                    <asp:TextBox runat="server" ID="txtCorreiosAr1" CssClass="campos" Text="0"></asp:TextBox>
                                </td>
                                <td>
                                    AR Vol 2:<br/>
                                    <asp:TextBox runat="server" ID="txtCorreiosAr2" CssClass="campos" Text="0"></asp:TextBox>
                                </td>
                                <td>
                                    AR Vol 3:<br/>
                                    <asp:TextBox runat="server" ID="txtCorreiosAr3" CssClass="campos" Text="0"></asp:TextBox>
                                </td>
                                <td>
                                    AR Vol 4:<br/>
                                    <asp:TextBox runat="server" ID="txtCorreiosAr4" CssClass="campos" Text="0"></asp:TextBox>
                                </td>
                                <td>
                                
                                </td>
                            </tr>
                            <tr class="rotulos">
                                <td>
                                    Seguro Vol 1:<br/>
                                    <asp:TextBox runat="server" ID="txtCorreiosSeguro1" CssClass="campos" Text="0"></asp:TextBox>
                                </td>
                                <td>
                                    Seguro Vol 2:<br/>
                                    <asp:TextBox runat="server" ID="txtCorreiosSeguro2" CssClass="campos" Text="0"></asp:TextBox>
                                </td>
                                <td>
                                    Seguro Vol 3:<br/>
                                    <asp:TextBox runat="server" ID="txtCorreiosSeguro3" CssClass="campos" Text="0"></asp:TextBox>
                                </td>
                                <td>
                                    Seguro Vol 4:<br/>
                                    <asp:TextBox runat="server" ID="txtCorreiosSeguro4" CssClass="campos" Text="0"></asp:TextBox>
                                </td>
                                <td>
                                
                                </td>
                            </tr>
                            <tr class="rotulos">
                                <td>
                                    Mão Própria Vol 1:<br/>
                                    <asp:TextBox runat="server" ID="txtCorreiosMaoPropria1" CssClass="campos" Text="0"></asp:TextBox>
                                </td>
                                <td>
                                    Mão Própria Vol 2:<br/>
                                    <asp:TextBox runat="server" ID="txtCorreiosMaoPropria2" CssClass="campos" Text="0"></asp:TextBox>
                                </td>
                                <td>
                                    Mão Própria Vol 3:<br/>
                                    <asp:TextBox runat="server" ID="txtCorreiosMaoPropria3" CssClass="campos" Text="0"></asp:TextBox>
                                </td>
                                <td>
                                    Mão Própria Vol 4:<br/>
                                    <asp:TextBox runat="server" ID="txtCorreiosMaoPropria4" CssClass="campos" Text="0"></asp:TextBox>
                                </td>
                                <td>
                                
                                </td>
                            </tr>
                            <tr class="rotulos">
                                <td>
                                    Rastreio Vol 1:<br/>
                                    <asp:TextBox runat="server" onkeydown="return (event.keyCode!=13);" ID="txtCorreiosRastreio1" CssClass="campos" Text=""></asp:TextBox>
                                </td>
                                <td>
                                    Rastreio Vol 2:<br/>
                                    <asp:TextBox runat="server" onkeydown="return (event.keyCode!=13);" ID="txtCorreiosRastreio2" CssClass="campos" Text=""></asp:TextBox>
                                </td>
                                <td>
                                    Rastreio Vol 3:<br/>
                                    <asp:TextBox runat="server" onkeydown="return (event.keyCode!=13);" ID="txtCorreiosRastreio3" CssClass="campos" Text=""></asp:TextBox>
                                </td>
                                <td>
                                    Rastreio Vol 4:<br/>
                                    <asp:TextBox runat="server" onkeydown="return (event.keyCode!=13);" ID="txtCorreiosRastreio4" CssClass="campos" Text=""></asp:TextBox>
                                </td>
                                <td>
                                
                                </td>
                            </tr>
                        <tr class="rotulos">
                            <td>
                                Faixa:<br/>
                                <asp:TextBox runat="server" ID="txtCorreiosFaixa" CssClass="campos" Text="0"></asp:TextBox>
                            </td>
                            <td style="font-size: 14px;">
                                <b>
                                Valor Total:<br/>
                                <asp:Label runat="server" ID="litCorreiosTotal"></asp:Label></b>
                                
                            </td>
                            <td>
                                    
                            </td>
                            <td>
                                    
                            </td>
                            <td>
                                    
                            </td>
                        </tr>
                        </asp:Panel>
                        <tr class="rotulos">
                            <td colspan="5" style="text-align: right; padding-top: 15px;">
                                <asp:Button runat="server" ID="btnEnviar" OnClick="btnEnviar_OnClick" Text="Enviar para Transportadora"/>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
    </asp:Panel>
                
                
                
    <asp:Panel runat="server" ID="pnVerEnvios" Visible="False">
                    <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
                        <tr class="rotulos">
                            <td style="text-align: right; padding-bottom: 15px;">
                                <asp:LinkButton runat="server" ID="btnEnviarPedidos" OnClick="btnEnviarPedidos_OnClick">Enviar pedidos à Transportadora</asp:LinkButton>
                            </td>
                        </tr>
                        <tr class="rotulos">
                            <td>
                                
    <asp:Panel runat="server" ID="pnGridVerPedidos" ScrollBars="Horizontal" Width="834px">
        <table>
        <tr>
            <td align="right">
                <table cellpadding="0" cellspacing="0" style="width: 834px">
                    <tr>
                        <td align="right">
                            &nbsp;</td>
                        <td height="38" 
                            style="background-image: url('../admin/images/bkExportar.jpg'); padding-bottom: 1px;" 
                            valign="bottom" width="231">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="width: 97px">
                                        &nbsp;</td>
                                    <td style="width: 33px">
                                        <asp:ImageButton ID="btPdf" runat="server" ImageUrl="~/admin/images/btPdf.jpg" 
                                            style="margin-left: 5px" OnClick="btPdf_Click" />
                                    </td>
                                    <td style="width: 31px">
                                        <asp:ImageButton ID="btXsl" runat="server" ImageUrl="~/admin/images/btXls.jpg" 
                                            style="margin-left: 5px" OnClick="btXsl_Click" />
                                    </td>
                                    <td style="width: 36px">
                                        <asp:ImageButton ID="btRtf" runat="server" ImageUrl="~/admin/images/btRtf.jpg" 
                                            style="margin-left: 5px" OnClick="btRtf_Click" />
                                    </td>
                                    <td valign="bottom">
                                        <asp:ImageButton ID="btCsv" runat="server" ImageUrl="~/admin/images/btCsv.jpg" 
                                            style="margin-left: 5px" OnClick="btCsv_Click" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr></table>
              <dxwgv:ASPxGridViewExporter ID="grdEx" runat="server" FileName="transportadora" 
                    GridViewID="grd" Landscape="True" PreserveGroupRowStates="True">
                </dxwgv:ASPxGridViewExporter>
        <dxwgv:ASPxGridView ID="grd" ClientInstanceName="grd" runat="server" AutoGenerateColumns="False" Width="834px" Cursor="auto" KeyFieldName="idPedidoJadlog"
            OnAutoFilterCellEditorCreate="grid_AutoFilterCellEditorCreate" OnAutoFilterCellEditorInitialize="grid_AutoFilterCellEditorInitialize" OnProcessColumnAutoFilter="grid_ProcessColumnAutoFilter" 
            OnDataBound="grd_OnDataBound" OnCustomUnboundColumnData="grd_OnCustomUnboundColumnData"  EnableCallBacks="False" Settings-ShowFilterRow="True" Settings-ShowFilterBar="Visible">
                    <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                    <Styles>
                        <Footer Font-Bold="True">
                        </Footer>
                    </Styles>
                    <SettingsText ConfirmDelete="Tem certeza que deseja excluir?" emptydatarow="Nenhum registro encontrado." />
                    <SettingsPager Position="TopAndBottom" PageSize="50" ShowDisabledButtons="False" AlwaysShowPager="True">
                        <Summary AllPagesText="Páginas: {0} - {1} ({2} items)" Text="Página {0} de {1} ({2} registros encontrados)" />
                    </SettingsPager>
                    <TotalSummary>
                        <dxwgv:ASPxSummaryItem DisplayFormat="c" FieldName="total" 
                            ShowInColumn="total" ShowInGroupFooterColumn="total" SummaryType="Sum" />
                        <dxwgv:ASPxSummaryItem DisplayFormat="c" FieldName="valorRecebido" 
                            ShowInColumn="valorRecebido" ShowInGroupFooterColumn="valorRecebido" SummaryType="Sum" />
                        <dxwgv:ASPxSummaryItem DisplayFormat="c" FieldName="diferenca" 
                            ShowInColumn="diferenca" ShowInGroupFooterColumn="diferenca" SummaryType="Sum" />
                    </TotalSummary>
                    <settings showfilterrow="True" ShowGroupButtons="False" ShowHeaderFilterButton="True" ShowFooter="True" />
                    <SettingsEditing EditFormColumnCount="4" PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1" PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter" PopupEditFormWidth="700px" />
                    <Columns>
                        <dxwgv:GridViewDataTextColumn Caption="pedido" FieldName="pedidoIdCliente" VisibleIndex="0" UnboundType="Integer">
                            <EditFormSettings Visible="False" />
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="pedido" FieldName="idPedido" VisibleIndex="0" Width="50">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataDateColumn Caption="Data" FieldName="data" Name="data" VisibleIndex="0">
                        </dxwgv:GridViewDataDateColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Transportadora" FieldName="transportadora" VisibleIndex="0">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Vol1" FieldName="vol1" VisibleIndex="0">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Vol2" FieldName="vol2" VisibleIndex="0">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Vol3" FieldName="vol3" VisibleIndex="0">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Vol4" FieldName="vol4" VisibleIndex="0">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Vol5" FieldName="vol5" VisibleIndex="0">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Vol6" FieldName="vol6" VisibleIndex="0">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Vol7" FieldName="vol7" VisibleIndex="0">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Vol8" FieldName="vol8" VisibleIndex="0">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Cubagem1" FieldName="cubagem1" VisibleIndex="0">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Cubagem2" FieldName="cubagem2" VisibleIndex="0">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Cubagem3" FieldName="cubagem3" VisibleIndex="0">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Cubagem4" FieldName="cubagem4" VisibleIndex="0">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Rastreio1" FieldName="rastreio1" VisibleIndex="0">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Rastreio2" FieldName="rastreio2" VisibleIndex="0">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Rastreio3" FieldName="rastreio3" VisibleIndex="0">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Rastreio4" FieldName="rastreio4" VisibleIndex="0">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Peso" FieldName="kg" VisibleIndex="0">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Faixa" FieldName="faixa" VisibleIndex="0">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Frete" FieldName="frete" VisibleIndex="0">
                            <PropertiesTextEdit DisplayFormatString="c">
                            </PropertiesTextEdit>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Interior" FieldName="interior" VisibleIndex="0">
                            <PropertiesTextEdit DisplayFormatString="c">
                            </PropertiesTextEdit>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Total" FieldName="total" VisibleIndex="0">
                            <PropertiesTextEdit DisplayFormatString="c">
                            </PropertiesTextEdit>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Nota" FieldName="nota" VisibleIndex="0">
                            <PropertiesTextEdit DisplayFormatString="c">
                            </PropertiesTextEdit>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Seguro" FieldName="seguro" VisibleIndex="0">
                            <PropertiesTextEdit DisplayFormatString="c">
                            </PropertiesTextEdit>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Seguro2" FieldName="seguro2" VisibleIndex="0">
                            <PropertiesTextEdit DisplayFormatString="c">
                            </PropertiesTextEdit>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Seguro3" FieldName="seguro3" VisibleIndex="0">
                            <PropertiesTextEdit DisplayFormatString="c">
                            </PropertiesTextEdit>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Seguro4" FieldName="seguro4" VisibleIndex="0">
                            <PropertiesTextEdit DisplayFormatString="c">
                            </PropertiesTextEdit>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Valor Vol1" FieldName="valorVol1" VisibleIndex="0">
                            <PropertiesTextEdit DisplayFormatString="c">
                            </PropertiesTextEdit>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Valor Vol2" FieldName="valorVol2" VisibleIndex="0">
                            <PropertiesTextEdit DisplayFormatString="c">
                            </PropertiesTextEdit>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Valor Vol3" FieldName="valorVol3" VisibleIndex="0">
                            <PropertiesTextEdit DisplayFormatString="c">
                            </PropertiesTextEdit>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Valor Vol4" FieldName="valorVol4" VisibleIndex="0">
                            <PropertiesTextEdit DisplayFormatString="c">
                            </PropertiesTextEdit>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="AR1" FieldName="ar" VisibleIndex="0">
                            <PropertiesTextEdit DisplayFormatString="c">
                            </PropertiesTextEdit>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="AR2" FieldName="ar2" VisibleIndex="0">
                            <PropertiesTextEdit DisplayFormatString="c">
                            </PropertiesTextEdit>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="AR3" FieldName="ar3" VisibleIndex="0">
                            <PropertiesTextEdit DisplayFormatString="c">
                            </PropertiesTextEdit>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="AR4" FieldName="ar4" VisibleIndex="0">
                            <PropertiesTextEdit DisplayFormatString="c">
                            </PropertiesTextEdit>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Mão Própria 1" FieldName="maoPropria" VisibleIndex="0">
                            <PropertiesTextEdit DisplayFormatString="c">
                            </PropertiesTextEdit>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Mão Própria 2" FieldName="maoPropria2" VisibleIndex="0">
                            <PropertiesTextEdit DisplayFormatString="c">
                            </PropertiesTextEdit>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Mão Própria 3" FieldName="maoPropria3" VisibleIndex="0">
                            <PropertiesTextEdit DisplayFormatString="c">
                            </PropertiesTextEdit>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Mão Própria 4" FieldName="maoPropria4" VisibleIndex="0">
                            <PropertiesTextEdit DisplayFormatString="c">
                            </PropertiesTextEdit>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Recebido" FieldName="valorRecebido" VisibleIndex="0">
                            <PropertiesTextEdit DisplayFormatString="c">
                            </PropertiesTextEdit>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Diferenca" Name="diferenca" FieldName="diferenca" VisibleIndex="0" UnboundType="Decimal">
                            <PropertiesTextEdit DisplayFormatString="c">
                            </PropertiesTextEdit>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Cancelado" FieldName="cancelado" VisibleIndex="0" Name="cancelado">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Exportar" Name="exportar" VisibleIndex="0" Width="80">
                            <DataItemTemplate>
                                <asp:LinkButton runat="server" OnCommand="btnEnviarEmail_OnCommand" ID="btnEnviarEmail" CommandArgument='<%# Eval("idPedidoJadlog") %>' EnableViewState="False">Reenviar email</asp:LinkButton>
                            </DataItemTemplate>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Cancelar" Name="cancelar" VisibleIndex="0" Width="80">
                            <DataItemTemplate>
                                <asp:LinkButton runat="server" OnCommand="btnCancelar_OnCommand" ID="btnCancelar" OnClientClick="return confirm('Deseja realmente cancelar?');" CommandArgument='<%# Eval("idPedidoJadlog") %>' EnableViewState="False">Cancelar</asp:LinkButton>
                            </DataItemTemplate>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Editar" Name="editar" VisibleIndex="0" Width="80">
                            <DataItemTemplate>
                                <asp:LinkButton runat="server" OnCommand="btnEditarValor_OnCommand" ID="btnEditarValor" CommandArgument='<%# Eval("idPedidoJadlog") %>' EnableViewState="False">Editar</asp:LinkButton>
                            </DataItemTemplate>
                        </dxwgv:GridViewDataTextColumn>
                    </Columns>
                    <StylesEditors>
                        <Label Font-Bold="True">
                        </Label>
                    </StylesEditors>
                </dxwgv:ASPxGridView> 
    </asp:Panel>
    <asp:Panel runat="server" ID="pnGridEditar" Visible="False">
        <table style="width: 100%">
            <tr class="rotulos" style="font-weight: bold;">
                <td>
                    <asp:HiddenField runat="server" ID="hiddenIdPedidoJadlogEditar" />
                    Pedido: <asp:Literal runat="server" ID="litPedidoIdDiferenca"></asp:Literal>
                </td>
            </tr>
            <tr class="rotulos">
                <td>
                    Valor recebido:<br/>
                    <asp:TextBox runat="server" ID="txtValorDiferenca" CssClass="campos" Text="0"></asp:TextBox>
                </td>
            </tr>
            <tr class="rotulos">
                <td style="padding-top: 10px;">
                    <asp:Button runat="server" ID="btnGravarDiferenca" Text="Gravar valor recebido" OnClick="btnGravarDiferenca_onClick" />
                </td>
            </tr>
        </table>
    </asp:Panel>

   
                            </td>
                        </tr>

                    </table>
    </asp:Panel>
        

            </td>
        </tr>
        <tr>
            <td>
            
            </td>
        </tr>
</table>
    
    
</asp:Content>