﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxCallback;

public class produtoQuantidade
{
    public int id { get; set; }
    public int quantidade { get; set; }
}

public partial class admin_gerarRelevancia : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {


    }

    protected void btnSalvar_Click(object sender, ImageClickEventArgs e)
    {
        string caminho = MapPath("~/");
        fluArquivo.SaveAs(caminho + "relevancia.xls");
        


        Response.Write("<script>alert('Produtos atualizados com sucesso.');</script>");
    }

    protected void btnAtualizar_OnCallback(object source, CallbackEventArgs e)
    {
        int fatorMultiplicacaoPedidos = 3;
        int.TryParse(txtFator.Text, out fatorMultiplicacaoPedidos);
        string caminho = MapPath("~/");

        var listaProdutosQuantidade = new List<produtoQuantidade>();

        string connStr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + caminho + "relevancia.xls;Extended Properties=Excel 12.0;";

        OleDbConnection cnnExcel = new OleDbConnection(connStr);
        OleDbCommand cmmPlan = null;
        OleDbDataReader drPlan = null;

        cnnExcel.Open();
        cmmPlan = new OleDbCommand("select * from [relatorio$]", cnnExcel);
        drPlan = cmmPlan.ExecuteReader(CommandBehavior.CloseConnection);
        var data = new dbCommerceDataContext();

        int contagem = 0;
        while (drPlan.Read())
        {
            if (contagem > 0)
            {
                try
                {

                string url = drPlan[0].ToString();
                string quantidade = drPlan[1].ToString();
                //string multiplicador = drPlan[2].ToString();
                int produtoId = 0;
                int quantidadeAcessos = 0;
                int fatorMultiplicacao = 1;

                //int.TryParse(multiplicador, out fatorMultiplicacao);
                url = url.Trim('/');
                if (url.ToLower().Contains("/produto"))
                {
                    var urlSplit = url.Split('/');
                    try
                    {
                        produtoId = Convert.ToInt32(urlSplit[2]);
                    }
                    catch (Exception)
                    {

                    }
                }
                else
                {
                    var urlSplit = url.Split('/');
                    if (urlSplit.Count() == 2)
                    {
                        var permalink = urlSplit[1];
                        var produto = (from c in data.tbProdutos where c.produtoUrl == permalink select c).FirstOrDefault();
                        if (produto != null) produtoId = produto.produtoId;
                    }

                }

                int.TryParse(quantidade, out quantidadeAcessos);

                quantidadeAcessos = quantidadeAcessos * fatorMultiplicacao;

                if (produtoId > 0 && quantidadeAcessos > 0)
                {
                    if (listaProdutosQuantidade.Any(x => x.id == produtoId))
                    {
                        listaProdutosQuantidade.First(x => x.id == produtoId).quantidade =
                            listaProdutosQuantidade.First(x => x.id == produtoId).quantidade + quantidadeAcessos;
                    }
                    else
                    {
                        var produtoQuantidadeAcessos = new produtoQuantidade();
                        produtoQuantidadeAcessos.id = produtoId;
                        produtoQuantidadeAcessos.quantidade = quantidadeAcessos;
                        listaProdutosQuantidade.Add(produtoQuantidadeAcessos);
                    }
                }
                }
                catch (Exception)
                {

                }

            }
            contagem++;
        }

        var produtos = (from c in data.tbProdutos where c.produtoAtivo == "True" select c);
        foreach (var produto in produtos)
        {
            try
            {

            int totalPedidos = 0;
            int totalPedidosAlternativo = 0;
            var itensPedidoDc = new dbCommerceDataContext();
            var pedidos =
                (from c in itensPedidoDc.tbItensPedidos
                 where c.produtoId == produto.produtoId
                 select new { c.itemQuantidade });
            if (pedidos.Any())
            {
                totalPedidos = Convert.ToInt32(pedidos.Sum(x => x.itemQuantidade));
            }
            var produtoAlternativo =
                (from c in data.tbProdutos where c.produtoAlternativoId == produto.produtoId select c).FirstOrDefault();
            if (produtoAlternativo != null)
            {
                var pedidosalternativo =
                    (from c in itensPedidoDc.tbItensPedidos
                     where c.produtoId == produtoAlternativo.produtoId
                     select new { c.itemQuantidade });
                if (pedidosalternativo.Any())
                {
                    totalPedidosAlternativo = Convert.ToInt32(pedidos.Sum(x => x.itemQuantidade));
                }

            }
            totalPedidos = (totalPedidos * fatorMultiplicacaoPedidos) + (totalPedidosAlternativo + fatorMultiplicacaoPedidos);
            int totalVisitas = 0;
            if (listaProdutosQuantidade.Any(x => x.id == produto.produtoId))
            {
                totalVisitas = listaProdutosQuantidade.First(x => x.id == produto.produtoId).quantidade;
            }
            if (produtoAlternativo != null)
            {
                if (listaProdutosQuantidade.Any(x => x.id == produtoAlternativo.produtoId))
                {
                    totalVisitas += listaProdutosQuantidade.First(x => x.id == produtoAlternativo.produtoId).quantidade;
                }
            }
            produto.relevancia = totalPedidos + totalVisitas;
            //data.SubmitChanges();

            var dataAlterar = new dbCommerceDataContext();
            var produtoAlterar = (from c in dataAlterar.tbProdutos where c.produtoId == produto.produtoId select c).FirstOrDefault();
            if (produtoAlternativo != null)
            {
                var produtoAlterarAlternativo = (from c in dataAlterar.tbProdutos where c.produtoId == produtoAlternativo.produtoId select c).FirstOrDefault();
                produtoAlterarAlternativo.relevancia = totalPedidos + totalVisitas;

            }
            produtoAlterar.relevancia = totalPedidos + totalVisitas;
            dataAlterar.SubmitChanges();
            dataAlterar.Dispose();

            }
            catch (Exception)
            {

            }
        }

        cnnExcel.Close();
        cnnExcel.Dispose();

    }
}