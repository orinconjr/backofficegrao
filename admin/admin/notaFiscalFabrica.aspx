﻿<%@ Page Title="" Language="C#" Theme="Glass" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="notaFiscalFabrica.aspx.cs" Inherits="admin_notaFiscalFabrica" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxTimer" TagPrefix="dxt" %>
<%@ Register TagPrefix="dxe" Namespace="DevExpress.Web.ASPxMenu" Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxPopupControl" Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxEditors" Assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>
<%@ Register TagPrefix="CE" Namespace="CuteEditor"  %>


<%@ Register assembly="DevExpress.Web.v11.1" namespace="DevExpress.Web.ASPxPopupControl" tagprefix="dx" %>


<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" Runat="Server">
    <dx:ASPxPopupControl ID="popAdicionarChamado" runat="server" CloseAction="CloseButton" Modal="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
        ClientInstanceName="popAdicionarChamado" HeaderText="Gerar nova Nota Fiscal" AllowDragging="True" PopupAnimationType="None" EnableViewState="True" Height="200" AutoUpdatePosition="True">
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl6" runat="server">
                <asp:HiddenField runat="server" ID="hdfIdAgendamento" />
                <div style="width: 400px; height: 100px;">
                    <table width="100%">                     
                        <tr class="rotulos">
                            <td>
                                Número do Romaneio:
                            </td>
                        </tr>                       
                        <tr class="rotulos">
                            <td>
                                <asp:TextBox runat="server" ID="txtNumeroRomaneio" Width="100%"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="rotulos" colspan="2" style="text-align: right">
                                <asp:Button runat="server" ID="btnGerarNotaFiscal" OnClick="btnGerarNotaFiscal_OnClick" Text="Gravar"/>
                            </td>
                        </tr>
                    </table>
                </div>
            </dx:PopupControlContentControl>
        </ContentCollection>
        <ContentStyle>
            <Paddings PaddingBottom="5px" />
        </ContentStyle>
    </dx:ASPxPopupControl>
    
    <table cellpadding="0" cellspacing="0" style="width: 920px">
    <tr>
        <td class="tituloPaginas" valign="top">
            <asp:Label ID="lblAcao" runat="server">Notas Fiscais Fábrica</asp:Label>
        </td>
    </tr>
        
    <tr>
        <td>
            
    <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                <dxwgv:ASPxGridView ID="grd" ClientInstanceName="grd" runat="server" AutoGenerateColumns="False"
                    KeyFieldName="idChamado" Width="834px"  Settings-ShowFilterRow="False" Settings-ShowFilterBar="Hidden"
                    Cursor="auto" OnHtmlRowCreated="grd_HtmlRowCreated" EnableViewState="False">
                    <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                    <Styles>
                        <Footer Font-Bold="True">
                        </Footer>
                    </Styles>
                    <SettingsText ConfirmDelete="Tem certeza que deseja excluir?" emptydatarow="Nenhum registro encontrado." />
                    <SettingsPager Position="TopAndBottom" PageSize="500" 
                        ShowDisabledButtons="False" AlwaysShowPager="True">
                        <Summary AllPagesText="Páginas: {0} - {1} ({2} items)" 
                            Text="Página {0} de {1} ({2} registros encontrados)" />
                    </SettingsPager>
                    <settings showfilterrow="True" ShowGroupButtons="False" ShowFooter="True" />
                    <SettingsEditing EditFormColumnCount="4" 
                        PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1" 
                        PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter" 
                        PopupEditFormWidth="700px" />
                    <Columns>
                       
                        <dxwgv:GridViewDataTextColumn Caption="Romaneio" FieldName="idPedidoFornecedor" VisibleIndex="0">
                            <DataItemTemplate>
                                <asp:Literal runat="server" Text='<%# Eval("idPedidoFornecedor") %>'></asp:Literal>
                            </DataItemTemplate>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Fornecedor" FieldName="fornecedorNome" VisibleIndex="0">
                            <DataItemTemplate>
                                <asp:Literal runat="server" Text='<%# Eval("fornecedorNome") %>'></asp:Literal>
                            </DataItemTemplate>
                        </dxwgv:GridViewDataTextColumn>
                        
                        <dxwgv:GridViewDataTextColumn Caption="Nota" FieldName="numeroNota" VisibleIndex="0">
                            <DataItemTemplate>
                                <asp:Literal runat="server" Text='<%# Eval("numeroNota") %>'></asp:Literal>
                            </DataItemTemplate>
                        </dxwgv:GridViewDataTextColumn>

                        <dxwgv:GridViewDataTextColumn Caption="Danf" FieldName="linkDanfe" VisibleIndex="0">
                            <DataItemTemplate>
                                <asp:HyperLink runat="server" Text="Link" NavigateUrl='<%# Eval("linkDanfe") %>' Target="_blank"></asp:HyperLink>
                            </DataItemTemplate>
                        </dxwgv:GridViewDataTextColumn>

                        <dxwgv:GridViewDataTextColumn Caption="Data/Hora" FieldName="dataHora" VisibleIndex="0">
                            <DataItemTemplate>
                                <asp:Literal runat="server" Text='<%# Eval("dataHora") %>'></asp:Literal>
                            </DataItemTemplate>
                        </dxwgv:GridViewDataTextColumn>

                    </Columns>
                    <StylesEditors>
                        <Label Font-Bold="True">
                        </Label>
                    </StylesEditors>
                </dxwgv:ASPxGridView>
            </td>
        </tr>
        <tr class="rotulos">
            <td style="padding-top: 30px; font-size: 20px;">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td style="text-align: right; padding-top: 20px;">
                <asp:Button runat="server" ID="btnGerarNovaNotaFiscal" Text="Gerar nova Nota Fiscal" OnClick="btnGerarNovaNotaFiscal_OnClick"/>
            </td>
        </tr>
    </table>
        </td>
    </tr>
</table>
<asp:LinqDataSource ID="sqlUsuario" runat="server" 
    ContextTypeName="dbCommerceDataContext" EnableDelete="True" 
    EnableInsert="True" EnableUpdate="True" TableName="tbUsuarios">
</asp:LinqDataSource>
</asp:Content>