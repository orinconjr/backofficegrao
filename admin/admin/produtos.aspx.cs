﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxTabControl;
using DevExpress.Web.ASPxEditors;
using DevExpress.Data;
using DevExpress.Web.ASPxUploadControl;
using System.IO;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.Reflection;
using Amazon.S3;
using Amazon.S3.Model;
using DevExpress.Data.Linq;

public partial class admin_produtos : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ASPxGridView.RegisterBaseScript(Page);

        if (!Page.IsPostBack)
        {
            //PopulateRootLevel();
            PopulaRooLevelCategoriasFiltros();
            PopulaRooLevelCategoriasAtivasNoSite();
            //popEditarProdutoDuplicado.ShowOnPageLoad = true;

            txtProdutoPrecoDeCusto.Attributes.Add("onkeyPress", "return moeda(this,'.',',',event);");
            txtProdutoPreco.Attributes.Add("onkeyPress", "return moeda(this,'.',',',event);");
            txtProdutoPrecoPromocional.Attributes.Add("onkeyPress", "return moeda(this,'.',',',event);");
            txtProdutoPrecoAtacado.Attributes.Add("onkeyPress", "return moeda(this,'.',',',event);");
            txtMargem.Attributes.Add("onkeyup", "javascript:return CalculoMargemVenda('" + txtMargem.ClientID + "', '" + lblPrecoPromocional.ClientID + "'," + "'" + lblPrecoVenda.ClientID + "', '" + txtProdutoPrecoDeCusto.ClientID + "');");

            grd.DataSourceID = "sqlProdutosServer";

            var usuarioLogadoId = Request.Cookies["usuarioLogadoId"];
            if (usuarioLogadoId != null)
                pmColumnMenu.Visible =
                    rnUsuarios.usuarioPaginasPermitidaSeleciona_PorUsuarioIdPaginaPermitidaNome(
                        int.Parse(usuarioLogadoId.Value), "listadecolunasprodutos").Tables[0].Rows.Count == 0;
        }
        else
        {
            //grd.DataSourceID = treeCategorias.SelectedValue != "0" ? "sqlProdutosPorCategoria" : "sqlProdutos";

            if (treeCategoriasAtivasNoSite.SelectedValue != "0")
            {
                grd.DataSourceID = "sqlProdutosPorCategoriaAtivasNoSite";
            }

            if (treeCategoriasFiltros.SelectedValue != "0")
            {
                grd.DataSourceID = "sqlProdutosPorCategoriaFiltros";
            }

            if (treeCategoriasAtivasNoSite.SelectedValue == "0" && treeCategoriasFiltros.SelectedValue == "0")
            {
                grd.DataSourceID = "sqlProdutosServer";
            }
        }

    }

    protected void grd_HtmlRowCreated(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType != GridViewRowType.Data) return;
        {
            //TextBox txtIdDaEmpresa = (TextBox)grd.FindRowCellTemplateControl(e.VisibleIndex, (GridViewDataColumn)grd.Columns["produtoIdDaEmpresa"], "txtIdDaEmpresa");
            //TextBox txtNome = (TextBox)grd.FindRowCellTemplateControl(e.VisibleIndex, (GridViewDataColumn)grd.Columns["produtoNome"], "txtNome");
            //TextBox txtPreco = (TextBox)grd.FindRowCellTemplateControl(e.VisibleIndex, (GridViewDataColumn)grd.Columns["produtoPreco"], "txtPreco");
            //TextBox txtEstoque = (TextBox)grd.FindRowCellTemplateControl(e.VisibleIndex, (GridViewDataColumn)grd.Columns["produtoEstoqueAtual"], "txtEstoque");
            //TextBox txtPeso = (TextBox)grd.FindRowCellTemplateControl(e.VisibleIndex, (GridViewDataColumn)grd.Columns["produtoPeso"], "txtPeso");
            //TextBox txtProdutoPaiId = (TextBox)grd.FindRowCellTemplateControl(e.VisibleIndex, (GridViewDataColumn)grd.Columns["produtoPaiId"], "txtProdutoPaiId");
            //TextBox txtCodDeBarras = (TextBox)grd.FindRowCellTemplateControl(e.VisibleIndex, (GridViewDataColumn)grd.Columns["produtoCodDeBarras"], "txtCodDeBarras");
            //TextBox txtEstoqueMinimo = (TextBox)grd.FindRowCellTemplateControl(e.VisibleIndex, (GridViewDataColumn)grd.Columns["produtoEstoqueMinimo"], "txtEstoqueMinimo");
            //TextBox txtPrecoDeCusto = (TextBox)grd.FindRowCellTemplateControl(e.VisibleIndex, (GridViewDataColumn)grd.Columns["produtoPrecoDeCusto"], "txtPrecoDeCusto");
            //TextBox txtPrecoPromocional = (TextBox)grd.FindRowCellTemplateControl(e.VisibleIndex, (GridViewDataColumn)grd.Columns["produtoPrecoPromocional"], "txtPrecoPromocional");
            //TextBox txtPrecoAtacado = (TextBox)grd.FindRowCellTemplateControl(e.VisibleIndex, (GridViewDataColumn)grd.Columns["produtoPrecoAtacado"], "txtPrecoAtacado");
            //ASPxCheckBox ckbProdutoPrincipal = (ASPxCheckBox)grd.FindRowCellTemplateControl(e.VisibleIndex, (GridViewDataColumn)grd.Columns["produtoPrincipal"], "ckbProdutoPrincipal");
            //HyperLink hplDuplicar = (HyperLink)grd.FindRowCellTemplateControl(e.VisibleIndex, (GridViewDataColumn)grd.Columns["duplicar"], "hplDuplicar");
            //ImageButton btSelecionarPrincipal = (ImageButton)grd.FindRowCellTemplateControl(e.VisibleIndex, (GridViewDataColumn)grd.Columns["principal"], "btSelecionarPrincipal");
            //ASPxCheckBox ckbExcluir = (ASPxCheckBox)grd.FindRowCellTemplateControl(e.VisibleIndex, (GridViewDataColumn)grd.Columns["excluir"], "ckbExcluir");

            //txtIdDaEmpresa.Attributes.Add("onclick", "document.getElementById('" + txtIdDaEmpresa.ClientID + "').focus();document.getElementById('" + txtIdDaEmpresa.ClientID + "').select();");
            //txtNome.Attributes.Add("onclick", "document.getElementById('" + txtNome.ClientID + "').focus();document.getElementById('" + txtNome.ClientID + "').select();");
            //txtPreco.Attributes.Add("onclick", "document.getElementById('" + txtPreco.ClientID + "').focus();document.getElementById('" + txtPreco.ClientID + "').select();");
            //txtPreco.Attributes.Add("onkeyPress", "return moeda(this,'.',',',event);");
            //txtEstoque.Attributes.Add("onclick", "document.getElementById('" + txtEstoque.ClientID + "').focus();document.getElementById('" + txtEstoque.ClientID + "').select();");
            //txtEstoque.Attributes.Add("onkeypress", "return soNumero(event);");
            //txtPeso.Attributes.Add("onclick", "document.getElementById('" + txtPeso.ClientID + "').focus();document.getElementById('" + txtPeso.ClientID + "').select();");
            //txtPeso.Attributes.Add("onkeypress", "return soNumero(event);");
            //txtProdutoPaiId.Attributes.Add("onclick", "document.getElementById('" + txtProdutoPaiId.ClientID + "').focus();document.getElementById('" + txtProdutoPaiId.ClientID + "').select();");
            //txtProdutoPaiId.Attributes.Add("onkeypress", "return soNumero(event);");
            //txtCodDeBarras.Attributes.Add("onclick", "document.getElementById('" + txtCodDeBarras.ClientID + "').focus();document.getElementById('" + txtCodDeBarras.ClientID + "').select();");
            //txtEstoqueMinimo.Attributes.Add("onclick", "document.getElementById('" + txtEstoqueMinimo.ClientID + "').focus();document.getElementById('" + txtEstoqueMinimo.ClientID + "').select();");
            //txtEstoqueMinimo.Attributes.Add("onkeypress", "return soNumero(event);");
            //txtPrecoDeCusto.Attributes.Add("onclick", "document.getElementById('" + txtPrecoDeCusto.ClientID + "').focus();document.getElementById('" + txtPrecoDeCusto.ClientID + "').select();");
            //txtPrecoDeCusto.Attributes.Add("onkeyPress", "return moeda(this,'.',',',event);");
            //txtPrecoPromocional.Attributes.Add("onclick", "document.getElementById('" + txtPrecoPromocional.ClientID + "').focus();document.getElementById('" + txtPrecoPromocional.ClientID + "').select();");
            //txtPrecoPromocional.Attributes.Add("onkeyPress", "return moeda(this,'.',',',event);");
            //txtPrecoAtacado.Attributes.Add("onclick", "document.getElementById('" + txtPrecoAtacado.ClientID + "').focus();document.getElementById('" + txtPrecoAtacado.ClientID + "').select();");
            //txtPrecoAtacado.Attributes.Add("onkeyPress", "return moeda(this,'.',',',event);");

            //if (ckbProdutoPrincipal.Checked == true)
            //{
            //    e.Row.ToolTip = "Produto principal";
            //}

            //if (ckbProdutoPrincipal.Checked == true || txtProdutoPaiId.Text == "0")
            //{
            //    btSelecionarPrincipal.Visible = false;
            //}
        }
    }

    private void PopulateRootLevel()
    {
        PopulateNodes(rnCategorias.categoriasSelecionaNivelPai().Tables[0], treeCategorias.Nodes);
    }

    private void PopulaRooLevelCategoriasAtivasNoSite()
    {
        using (var data = new dbCommerceDataContext())
        {
            var categoriasAtivasNoSite = (from c in data.tbProdutoCategorias where c.categoriaPaiId == 0 && c.exibirSite == true orderby c.categoriaNome select c).ToList();

            foreach (var categoria in categoriasAtivasNoSite)
            {
                TreeNode tn = new TreeNode { Text = categoria.categoriaNome, Value = categoria.categoriaId.ToString() };

                treeCategoriasAtivasNoSite.Nodes.Add(tn);
            }
        }
    }

    private void PopulaRooLevelCategoriasFiltros()
    {
        using (var data = new dbCommerceDataContext())
        {
            List<int> categoriasFiltrosPai = (from c in data.tbProdutoCategorias
                                              where c.categoriaPaiId == 0 && c.exibirSite == false
                                              orderby c.categoriaNome
                                              select new { c.categoriaId }).Select(x => x.categoriaId).ToList();

            var categoriasComFiltros = (from c in data.tbProdutoCategorias
                                        where categoriasFiltrosPai.Contains(c.categoriaPaiId)
                                        orderby c.categoriaNome
                                        select new { c.categoriaPaiId }).Distinct().Select(x => x.categoriaPaiId).ToList();

            var categoriasFiltros =
                (from c in data.tbProdutoCategorias
                 where categoriasComFiltros.Contains(c.categoriaId)
                 orderby c.categoriaNome
                 select c);

            foreach (var categoria in categoriasFiltros)
            {
                TreeNode tn = new TreeNode { Text = categoria.categoriaNome, Value = categoria.categoriaId.ToString() };

                treeCategoriasFiltros.Nodes.Add(tn);

                tn.PopulateOnDemand = true;
            }
        }
    }

    private void PopulateSubLevel(int categoriaPaiId, TreeNode parentNode)
    {
        PopulateNodes(rnCategorias.categoriasSelecionaNivelFilho(categoriaPaiId).Tables[0], parentNode.ChildNodes);
    }

    protected void TreeView1_TreeNodePopulate(object sender, TreeNodeEventArgs e)
    {
        PopulateSubLevel(Int32.Parse(e.Node.Value), e.Node);
    }

    private void PopulateNodes(DataTable dt, TreeNodeCollection nodes)
    {
        foreach (DataRow dr in dt.Rows)
        {
            TreeNode tn = new TreeNode();
            tn.Text = dr["categoriaNome"].ToString();
            tn.Value = dr["categoriaId"].ToString();

            nodes.Add(tn);

            //If node has child nodes, then enable on-demand populating
            tn.PopulateOnDemand = ((int)(dr["childnodecount"]) > 0);
        }
    }

    protected void btPdf_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WritePdfToResponse();
    }
    protected void btXsl_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteXlsToResponse();
    }
    protected void btRtf_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteRtfToResponse();
    }
    protected void btCsv_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteCsvToResponse();
    }

    protected void btSelecionarPrincipal_Command(object sender, CommandEventArgs e)
    {
        rnProdutos.alteraProdutoPrincipal(int.Parse(e.CommandName.ToString()), int.Parse(e.CommandArgument.ToString()));
        grd.DataBind();
    }

    protected void grd_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        /*try
        {
            string estoqueAtual;

            int inicio = 0;
            int fim = 0;

            if (grd.PageIndex == 0)
            {
                inicio = 0;
                fim = inicio + grd.GetCurrentPageRowValues("produtoId").Count;
            }
            else
            {
                inicio += inicio + grd.SettingsPager.PageSize * grd.PageIndex;
                fim += inicio + grd.GetCurrentPageRowValues("produtoId").Count;
            }

            for (int i = inicio; i < fim; i++)
            {
                TextBox txtIdDaEmpresa = (TextBox)grd.FindRowCellTemplateControl(i, grd.Columns["produtoIdDaEmpresa"] as GridViewDataColumn, "txtIdDaEmpresa");
                TextBox txtcomplementoIdDaEmpresa = (TextBox)grd.FindRowCellTemplateControl(i, grd.Columns["complementoIdDaEmpresa"] as GridViewDataColumn, "txtcomplementoIdDaEmpresa");
                TextBox txtNome = (TextBox)grd.FindRowCellTemplateControl(i, grd.Columns["produtoNome"] as GridViewDataColumn, "txtNome");
                TextBox txtPreco = (TextBox)grd.FindRowCellTemplateControl(i, grd.Columns["produtoPreco"] as GridViewDataColumn, "txtPreco");
                TextBox txtEstoque = (TextBox)grd.FindRowCellTemplateControl(i, grd.Columns["produtoEstoqueAtual"] as GridViewDataColumn, "txtEstoque");
                TextBox txtDescricao = (TextBox)grd.FindRowCellTemplateControl(i, grd.Columns["produtoDescricao"] as GridViewDataColumn, "txtDescricao");
                TextBox txtPeso = (TextBox)grd.FindRowCellTemplateControl(i, grd.Columns["produtoPeso"] as GridViewDataColumn, "txtPeso");
                TextBox txtProdutoPaiId = (TextBox)grd.FindRowCellTemplateControl(i, grd.Columns["produtoPaiId"] as GridViewDataColumn, "txtProdutoPaiId");
                ASPxCheckBox ckbAtivo = (ASPxCheckBox)grd.FindRowCellTemplateControl(i, grd.Columns["produtoAtivo"] as GridViewDataColumn, "ckbAtivo");
                TextBox txtCodDeBarras = (TextBox)grd.FindRowCellTemplateControl(i, grd.Columns["produtoCodDeBarras"] as GridViewDataColumn, "txtCodDeBarras");
                TextBox txtEstoqueMinimo = (TextBox)grd.FindRowCellTemplateControl(i, grd.Columns["produtoEstoqueMinimo"] as GridViewDataColumn, "txtEstoqueMinimo");
                TextBox txtPrecoDeCusto = (TextBox)grd.FindRowCellTemplateControl(i, grd.Columns["produtoPrecoDeCusto"] as GridViewDataColumn, "txtPrecoDeCusto");
                TextBox txtPrecoPromocional = (TextBox)grd.FindRowCellTemplateControl(i, grd.Columns["produtoPrecoPromocional"] as GridViewDataColumn, "txtPrecoPromocional");
                TextBox txtPrecoAtacado = (TextBox)grd.FindRowCellTemplateControl(i, grd.Columns["produtoPrecoAtacado"] as GridViewDataColumn, "txtPrecoAtacado");
                ASPxCheckBox ckbFreteGratis = (ASPxCheckBox)grd.FindRowCellTemplateControl(i, grd.Columns["produtoFreteGratis"] as GridViewDataColumn, "ckbFreteGratis");
                ASPxCheckBox ckbLancamento = (ASPxCheckBox)grd.FindRowCellTemplateControl(i, grd.Columns["produtoLancamento"] as GridViewDataColumn, "ckbLancamento");
                ASPxCheckBox ckbPromocao = (ASPxCheckBox)grd.FindRowCellTemplateControl(i, grd.Columns["produtoPromocao"] as GridViewDataColumn, "ckbPromocao");
                ASPxCheckBox ckbDestaque1 = (ASPxCheckBox)grd.FindRowCellTemplateControl(i, grd.Columns["destaque1"] as GridViewDataColumn, "ckbDestaque1");
                ASPxCheckBox ckbDestaque2 = (ASPxCheckBox)grd.FindRowCellTemplateControl(i, grd.Columns["destaque2"] as GridViewDataColumn, "ckbDestaque2");
                ASPxCheckBox ckbDestaque3 = (ASPxCheckBox)grd.FindRowCellTemplateControl(i, grd.Columns["destaque3"] as GridViewDataColumn, "ckbDestaque3");
                ASPxCheckBox ckbDescontoProgressivoFrete = (ASPxCheckBox)grd.FindRowCellTemplateControl(i, grd.Columns["descontoProgressivoFrete"] as GridViewDataColumn, "ckbDescontoProgressivoFrete");
                ASPxCheckBox ckbForaDeLinha = (ASPxCheckBox)grd.FindRowCellTemplateControl(i, grd.Columns["foraDeLinha"] as GridViewDataColumn, "ckbForaDeLinha");

                TextBox txtProdutoId = (TextBox)grd.FindRowCellTemplateControl(i, grd.Columns["produtoId"] as GridViewDataColumn, "txtProdutoId");

                #region Dados produto original
                int produtoId = int.Parse(txtProdutoId.Text);

                var dataOriginal = new dbCommerceDataContext();
                var produtoOriginal = (from c in dataOriginal.tbProdutos where c.produtoId == produtoId select c).First();
                #endregion Dados produto original

                //atribuo o valor antes do update que para mandar o email avisando a atualização do estoque tenho que comparar o estoque atual com o novo valor que vai ser preenchido no campo estoque
                estoqueAtual = rnProdutos.produtoSeleciona_PorProdutoId(int.Parse(txtProdutoId.Text)).Tables[0].Rows[0]["produtoEstoqueAtual"].ToString();

                string produtoUrl = rnProdutos.GerarPermalinkProduto(int.Parse(txtProdutoId.Text), txtNome.Text).Trim().ToLower();


                rnProdutos.produtoAlterarEmLote(int.Parse(txtProdutoPaiId.Text), txtIdDaEmpresa.Text, txtNome.Text, produtoUrl, Convert.ToDecimal(txtPreco.Text), Convert.ToInt32(txtEstoque.Text),
                                                   Convert.ToDecimal(txtPeso.Text), ckbAtivo.Checked.ToString(), txtCodDeBarras.Text,
                                                   Convert.ToInt32(txtEstoqueMinimo.Text), Convert.ToDecimal(txtPrecoDeCusto.Text),
                                                   Convert.ToDecimal(txtPrecoPromocional.Text), Convert.ToDecimal(txtPrecoAtacado.Text),
                                                   ckbFreteGratis.Checked.ToString(), ckbLancamento.Checked.ToString(), ckbPromocao.Checked.ToString(),
                                                   ckbDestaque1.Checked.ToString(), ckbDestaque2.Checked.ToString(),
                                                   ckbDestaque3.Checked.ToString(), Convert.ToInt32(txtProdutoId.Text), txtcomplementoIdDaEmpresa.Text, txtDescricao.Text,
                                                   ckbDescontoProgressivoFrete.Checked.ToString(), ckbForaDeLinha.Checked);



                rnProdutos.AtualizaDescontoCombo(Convert.ToInt32(txtProdutoId.Text));
                rnBuscaCloudSearch.AtualizarProduto(Convert.ToInt32(txtProdutoId.Text));
                ASPxCheckBox ckbExcluir = (ASPxCheckBox)grd.FindRowCellTemplateControl(i, grd.Columns["excluir"] as GridViewDataColumn, "ckbExcluir");
                if (ckbExcluir.Checked.ToString() == "True")
                {
                    if (rnPedidos.itensPedidoSeleciona_PorProdutoId(int.Parse(txtProdutoId.Text)).Tables[0].Rows.Count > 0)
                    {
                        Response.Write("<script>alert('O produto: " + txtNome.Text + ", não pode ser excluido por que existe pedido relacionado a ele.');</script>");
                    }
                    else
                    {

                        rnProdutos.produtoExclui(int.Parse(txtProdutoId.Text));
                        rnBuscaCloudSearch.ExcluirProduto(int.Parse(txtProdutoId.Text));
                        if (Directory.Exists(ConfigurationManager.AppSettings["caminhoFisico"] + "fotos\\" + txtProdutoId.Text))
                        {
                            Directory.Delete(ConfigurationManager.AppSettings["caminhoFisico"] + "fotos\\" + txtProdutoId.Text, true);
                        }


                    }
                }

                //aviso de atualizacao de estoque
                if (estoqueAtual != txtEstoque.Text)
                    if (rnProdutoEmEspera.selecionaClientesEsperandoProdutos_PorProdutoId(int.Parse(txtProdutoId.Text)))
                        rnProdutoEmEspera.produtoEmEsperaExclui(int.Parse(txtProdutoId.Text));


                #region Registra Log

                var dataAlterado = new dbCommerceDataContext();
                var produtoAlterado = (from c in dataAlterado.tbProdutos where c.produtoId == produtoId select c).First();
                var log = new rnLog();
                log.usuario = rnUsuarios.retornaNomeUsuarioLogado();
                PropertyInfo[] propriedadesOriginal = produtoOriginal.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);
                PropertyInfo[] propriedadesAlterado = produtoAlterado.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);
                foreach (var propriedadeOriginal in propriedadesOriginal)
                {
                    if (propriedadeOriginal.PropertyType.BaseType != null)
                    {
                        var propriedadeAlterado = propriedadesAlterado.Where(x => x.Name == propriedadeOriginal.Name).FirstOrDefault();
                        if (propriedadeAlterado != null)
                        {
                            try
                            {
                                string valorPropriedadeOriginal = propriedadeOriginal.GetValue(produtoOriginal, null).ToString();
                                string valorPropriedadeAlterado = propriedadeAlterado.GetValue(produtoAlterado, null).ToString();
                                if (valorPropriedadeOriginal != valorPropriedadeAlterado)
                                {
                                    log.descricoes.Add(propriedadeOriginal.Name + " alterado de " + propriedadeOriginal.GetValue(produtoOriginal, null) + " para " + propriedadeAlterado.GetValue(produtoAlterado, null));
                                }
                            }
                            catch (Exception)
                            {

                            }
                        }
                    }
                }
                var tipoRelacionado = new rnEnums.TipoRelacionadoIds() { idRegistroRelacionado = produtoId, tipoRelacionado = rnEnums.TipoRegistroRelacionado.Produto };
                log.tiposRelacionados.Add(tipoRelacionado);
                log.tiposOperacao.Add(rnEnums.TipoOperacao.Produto);
                log.InsereLog();

                #endregion Registra Log
            }
        }
        catch (Exception)
        {

        }
        grd.DataBind();*/
    }

    protected void btnGravarProcessoFabrica_OnClick(object sender, EventArgs e)
    {
        //processoFabricaInclui(Convert.ToInt32(hdfIdProduto.Value));
        // popChecarItens.ShowOnPageLoad = false;
    }

    protected void btnAlterarProcessosFabrica_OnCommand(object sender, CommandEventArgs e)
    {
        /*hdfIdProduto.Value = e.CommandArgument.ToString();
        popChecarItens.ShowOnPageLoad = true;

        chkProcessosFabrica.DataSourceID = "sqlProcessosFabrica";
        chkProcessosFabrica.DataBind();*/
    }

    protected void chkProcessosFabrica_OnDataBound(object sender, EventArgs e)
    {
        if (hdfIdProduto.Value != "")
        {
            var produtosDc = new dbCommerceDataContext();
            int produtoId = Convert.ToInt32(hdfIdProduto.Value);
            var categoriasProduto = (from c in produtosDc.tbJuncaoProdutoProcessoFabricas where c.idProduto == produtoId select c);
            foreach (var processo in categoriasProduto)
            {
                var itemCheck = chkProcessosFabrica.Items.FindByValue(processo.idProcessoFabrica.ToString());
                if (itemCheck != null) itemCheck.Selected = true;
            }
        }
    }
    private void processoFabricaInclui(int produtoId)
    {

        var produtoDc = new dbCommerceDataContext();
        var processosAtuais = (from c in produtoDc.tbJuncaoProdutoProcessoFabricas where c.idProduto == produtoId select c);
        foreach (ListItem processoItem in chkProcessosFabrica.Items)
        {
            int processoId = Convert.ToInt32(processoItem.Value);
            var possuiAtualmente = processosAtuais.Any(x => x.idProcessoFabrica == processoId);
            if (processoItem.Selected && possuiAtualmente == false)
            {
                var processoAdicionar = new tbJuncaoProdutoProcessoFabrica();
                processoAdicionar.idProduto = produtoId;
                processoAdicionar.idProcessoFabrica = processoId;
                produtoDc.tbJuncaoProdutoProcessoFabricas.InsertOnSubmit(processoAdicionar);
                produtoDc.SubmitChanges();
            }
            else if (processoItem.Selected == false && possuiAtualmente)
            {
                var processoExcluir =
                    (from c in produtoDc.tbJuncaoProdutoProcessoFabricas
                     where c.idProduto == produtoId && c.idProcessoFabrica == processoId
                     select c).First();
                produtoDc.tbJuncaoProdutoProcessoFabricas.DeleteOnSubmit(processoExcluir);
                produtoDc.SubmitChanges();
            }
        }
    }

    protected void ibtDuplicarAll_OnCommand(object sender, CommandEventArgs e)
    {
        if (duplicar(Convert.ToInt32(e.CommandArgument)))
            popEditarProdutoDuplicado.ShowOnPageLoad = true;

    }

    protected bool duplicar(int idProdutoPai)
    {
        try
        {
            using (var data = new dbCommerceDataContext())
            {
                var produtoPai = (from c in data.tbProdutos where c.produtoId == idProdutoPai select c).FirstOrDefault();

                int produtoId;
                int produtoPaiId = (int)produtoPai.produtoPaiId;
                string produtoMetaDescription = produtoPai.produtoNome + " " + produtoPai.produtoDescricao;
                string produtoMetakeywords = produtoPai.produtoNome + " " + produtoPai.produtoDescricao;
                string produtoUrl = rnProdutos.GerarPermalinkProduto(0, produtoPai.produtoNome).Trim().ToLower();

                int produtoAlternativoId = 0;
                if ((produtoPai.produtoAlternativoId ?? 0) > 0) int.TryParse(produtoPai.produtoAlternativoId.ToString(), out produtoAlternativoId);

                #region Define valores dos checkboxs
                string produtoAtivo = "False";
                string produtoPrincipal = produtoPai.produtoPrincipal;
                string produtoFreteGratis = produtoPai.produtoFreteGratis;
                string produtoLancamento = produtoPai.produtoLancamento;
                string preVenda = produtoPai.preVenda;
                string produtoPromocao = produtoPai.produtoPromocao;
                string destaque1 = produtoPai.destaque1;
                string destaque2 = produtoPai.destaque2;
                string destaque3 = produtoPai.destaque3;
                string exclusivo = produtoPai.produtoExclusivo;
                string marketplaceCadastrar = produtoPai.marketplaceCadastrar;
                string marketplaceEnviarMarca = produtoPai.marketplaceEnviarMarca;
                string descontoProgressivoFrete = produtoPai.descontoProgressivoFrete;
                string cubagem = produtoPai.cubagem;
                string exibirDiferencaCombo = produtoPai.exibirDiferencaCombo;
                #endregion

                produtoId = rnProdutos.produtoInclui(produtoPaiId, produtoPrincipal, produtoPai.produtoIdDaEmpresa, produtoPai.produtoCodDeBarras, produtoPai.produtoNome,
                                                     produtoUrl, produtoPai.produtoDescricao, produtoPai.produtoFornecedor, produtoPai.produtoMarca, produtoPai.produtoPreco,
                                                     produtoPai.produtoPrecoDeCusto ?? 0, produtoPai.produtoPrecoPromocional ?? 0, produtoPai.produtoPrecoAtacado ?? 0,
                                                     produtoPai.produtoQuantidadeMininaParaAtacado ?? 0, produtoPai.produtoLegendaAtacado, produtoPai.produtoEstoqueAtual,
                                                     produtoPai.produtoEstoqueMinimo ?? 0, (decimal)produtoPai.produtoPeso, produtoAtivo, produtoFreteGratis, produtoLancamento,
                                                     produtoPromocao, produtoMetaDescription, produtoMetakeywords, destaque1, destaque2, destaque3, produtoPai.disponibilidadeEmEstoque,
                                                     preVenda, produtoPai.produtoComposicao, produtoPai.produtoFios, produtoPai.produtoPecas, produtoPai.produtoBrindes, produtoPai.produtoTags,
                                                     produtoPai.ncm, marketplaceCadastrar, marketplaceEnviarMarca, exclusivo, produtoPai.altura ?? 0, produtoPai.largura ?? 0,
                                                     produtoPai.profundidade ?? 0, produtoPai.complementoIdDaEmpresa, descontoProgressivoFrete, cubagem, produtoPai.produtoNomeNovo,
                                                     exibirDiferencaCombo, produtoPai.siteId, produtoAlternativoId, produtoPai.bloqueado, produtoPai.relevancia ?? 0, produtoPai.prontaEntrega,
                                                     produtoPai.foraDeLinha, produtoPai.icms ?? 0, "False", produtoPai.produtoNomeTitle, produtoPai.produtoNomeShopping, produtoPai.foto360,
                                                     produtoPai.limitarVendaEstoqueReal ?? false, produtoPai.limitarVendaEstoqueVirtual ?? false, produtoPai.idCentroDistribuicao, produtoPai.criacaoPropria ?? false,
                                                     produtoPai.ocultarLista ?? false, produtoPai.ocultarBusca ?? false, produtoPai.ativoMercadoLivre ?? false, produtoPai.ativoB2w ?? false, produtoPai.ativoCnova ?? false, produtoPai.ativoWalmart ?? false,
                                                     produtoPai.ativoMagazineLuiza ?? false);

                if (produtoId > 0)
                {

                    if (produtoPrincipal == "True")
                        rnProdutos.alteraProdutoPrincipal(produtoPaiId, produtoId);

                    #region categoria
                    var listaDeCategorias =
                        (from c in data.tbJuncaoProdutoCategorias where c.produtoId == produtoPai.produtoId select c);

                    foreach (var categoria in listaDeCategorias)
                    {
                        rnCategorias.juncaoProdutoCategoriaInclui(produtoId, categoria.categoriaId, produtoPaiId);
                    }
                    #endregion categoria

                    #region produtos relacionados (combo)

                    var relacionados = (from c in data.tbProdutoRelacionados
                                        join d in data.tbProdutos on c.idProdutoFilho equals d.produtoId
                                        where c.idProdutoPai == produtoPai.produtoId
                                        select
                                            new
                                            {
                                                d.produtoIdDaEmpresa,
                                                d.complementoIdDaEmpresa,
                                                d.produtoId,
                                                d.fotoDestaque,
                                                d.produtoNome,
                                                c.idProdutoRelacionado,
                                                c.desconto,
                                                d.produtoPrecoDeCusto,
                                                c.idProdutoFilho
                                            });

                    foreach (var produtoRelacinado in relacionados)
                    {
                        var produto = new tbProdutoRelacionado
                        {
                            idProdutoPai = produtoId,
                            idProdutoFilho = produtoRelacinado.idProdutoFilho,
                            desconto = produtoRelacinado.desconto
                        };

                        data.tbProdutoRelacionados.InsertOnSubmit(produto);
                    }

                    #endregion produtos relacionados (combo)

                    #region colecão
                    var colecoesAtuais = (from c in data.tbJuncaoProdutoColecaos where c.produtoId == produtoPai.produtoId select c);

                    foreach (var colecao in colecoesAtuais)
                    {
                        var colecaoAdicionar = new tbJuncaoProdutoColecao
                        {
                            produtoId = produtoId,
                            colecaoId = colecao.colecaoId,
                            dataDaCriacao = DateTime.Now
                        };
                        data.tbJuncaoProdutoColecaos.InsertOnSubmit(colecaoAdicionar);
                        data.SubmitChanges();
                    }
                    #endregion coleção

                    #region descrição tabela grupo
                    var tabelasAtuais = (from c in data.tbProdutoDescricaoTabelaGrupos where c.produtoId == produtoPai.produtoId select c);

                    foreach (var descricaoTab in tabelasAtuais)
                    {
                        var tabelaAdicionar = new tbProdutoDescricaoTabelaGrupo
                        {
                            produtoId = produtoId,
                            idDescricaoTabelaGrupo = descricaoTab.idDescricaoTabelaGrupo
                        };
                        data.tbProdutoDescricaoTabelaGrupos.InsertOnSubmit(tabelaAdicionar);
                        data.SubmitChanges();
                    }
                    #endregion descrição tabela grupo

                    #region especificações
                    var especificacoes =
                        (from c in data.tbJuncaoProdutoEspecificacaos where c.produtoId == produtoPai.produtoId select c);

                    foreach (var item1 in especificacoes)
                    {

                        int especificacaoId = item1.especificacaoId;

                        #region inclui produtoId e especificacao na tabela tbJuncaoProdutoEspecificacao. antes de incluir verifico se já existe na procedure
                        rnEspecificacao.juncaoProdutoEspecificacaoInclui(produtoId, especificacaoId);
                        #endregion

                        //int produtoItemEspecificacaoId = int.Parse(drpEspecificacoes.SelectedItem.Value);

                        //#region incluo as especificacoes selecionadas na tabela tbProdutoItemEspecificacao
                        //rnEspecificacao.produtoItensEspecificacaoInclui(produtoId, produtoPaiId, especificacaoId, produtoItemEspecificacaoId);
                        //#endregion

                    }

                    #endregion especificações

                    #region garantia estendida

                    var garantiasEstendidas =
                        (from c in data.tbProdutoGarantiaEstendidas where c.produtoId == produtoPai.produtoId select c);

                    foreach (var garantia in garantiasEstendidas)
                    {
                        rnGarantiaEstendida.garantiaEstendidaInclui(produtoId, garantia.garantiaNome, garantia.garantiaValor);
                    }

                    #endregion garantia estendida

                    #region cria diretorio de foto
                    Directory.CreateDirectory(ConfigurationManager.AppSettings["caminhoFisico"] + "fotos\\" + produtoId);
                    #endregion cria diretorio de foto

                    #region foto inclui

                    string Location;
                    string Target;

                    #region Fotos Galeria

                    var fotos = (from c in data.tbProdutoFotos where c.produtoId == produtoPai.produtoId select c);

                    foreach (var foto in fotos)
                    {

                        FileInfo file = new FileInfo(foto.produtoFoto);

                        #region Recupero o produtoFotoId

                        int fotoId;
                        if (rnFotos.produtoFotoSelecionaUltimoId().Tables[0].Rows.Count > 0)
                            fotoId =
                                int.Parse(
                                    rnFotos.produtoFotoSelecionaUltimoId().Tables[0].Rows[0]["produtoFotoId"]
                                        .ToString()) + 1;
                        else
                            fotoId = 0;

                        #endregion

                        string fotoRenomeada = fotoId + ".jpg";

                        //Salva foto no banco
                        rnFotos.fotosInclui(produtoId, fotoId.ToString(), foto.produtoFotoDescricao);

                        if (!Directory.Exists(ConfigurationManager.AppSettings["caminhoFisico"] + "fotos\\" + produtoId + "\\")) //se não existir o diretório então cria
                            Directory.CreateDirectory(ConfigurationManager.AppSettings["caminhoFisico"] + "fotos\\" + produtoId + "\\");

                        //foto do produto pai
                        Location = ConfigurationManager.AppSettings["caminhoFisico"] + "fotos\\" + idProdutoPai + "\\" + foto.produtoFoto + ".jpg";
                        if (File.Exists(Location))
                        {
                            File.Copy(Location, ConfigurationManager.AppSettings["caminhoFisico"] + "fotos\\" + produtoId + "\\" + fotoRenomeada, true);
                            uploadImagemAmazonS3(fotoRenomeada, produtoId);
                        }

                        //foto do produto pai média
                        Target = ConfigurationManager.AppSettings["caminhoFisico"] + "fotos\\" + idProdutoPai + "\\" + "media_" + foto.produtoFoto + ".jpg";
                        if (File.Exists(Target))
                        {
                            File.Copy(Target, ConfigurationManager.AppSettings["caminhoFisico"] + "fotos\\" + produtoId + "\\media_" + fotoRenomeada, true);
                            uploadImagemAmazonS3("media_" + fotoRenomeada, produtoId);
                        }

                        //foto do produto pai pequena
                        Target = ConfigurationManager.AppSettings["caminhoFisico"] + "fotos\\" + idProdutoPai + "\\" + "pequena_" + foto.produtoFoto + ".jpg";
                        if (File.Exists(Target))
                        {
                            File.Copy(Target, ConfigurationManager.AppSettings["caminhoFisico"] + "fotos\\" + produtoId + "\\pequena_" + fotoRenomeada, true);
                            uploadImagemAmazonS3("pequena_" + fotoRenomeada, produtoId);
                        }

                        //foto do produto pai 500 mobile
                        Target = ConfigurationManager.AppSettings["caminhoFisico"] + "fotos\\" + idProdutoPai + "\\" + "500_" + foto.produtoFoto + ".jpg";
                        if (File.Exists(Target))
                        {
                            File.Copy(Target, ConfigurationManager.AppSettings["caminhoFisico"] + "fotos\\" + produtoId + "\\500_" + fotoRenomeada, true);
                            uploadImagemAmazonS3("500_" + fotoRenomeada, produtoId);
                        }


                    }

                    #endregion Fotos Galeria

                    #region Foto Produto Destaque Categoria

                    try
                    {

                        Target = ConfigurationManager.AppSettings["caminhoFisico"] + "fotos\\" + idProdutoPai + "\\" + "destaquecategoria.jpg";
                        if (File.Exists(Target))
                        {
                            //Salva foto no banco
                            rnFotos.fotosInclui(produtoId, "destaquecategoria.jpg", "");

                            File.Copy(Target, ConfigurationManager.AppSettings["caminhoFisico"] + "fotos\\" + produtoId + "destaquecategoria.jpg", true);
                        }

                    }
                    catch (Exception)
                    {


                    }

                    #endregion Foto Produto Destaque Categoria

                    #endregion foto inclui

                    #region informações adicionais

                    var informacoesAdicionais = (from c in data.tbInformacaoAdcionals where c.produtoId == produtoPai.produtoId select c);

                    foreach (var itemInfo in informacoesAdicionais)
                    {
                        rnInformacoesAdcionais.produtoInformacoesAdcionaisInclui(produtoId, itemInfo.informacaoAdcionalNome, itemInfo.informacaoAdcionalConteudo);
                    }

                    #endregion informações adicionais

                    #region videos

                    var videos = (from v in data.tbProdutoVideos where v.produtoId == produtoPai.produtoId select v).ToList();

                    foreach (var video in videos)
                    {
                        var adicionaLink = new tbProdutoVideo
                        {
                            linkVideo = video.linkVideo,
                            produtoId = produtoId,
                            dataCadastro = DateTime.Now
                        };
                        data.tbProdutoVideos.InsertOnSubmit(adicionaLink);
                        data.SubmitChanges();
                    }

                    #endregion videos

                    var produtoAlt = (from c in data.tbProdutos where c.produtoId == produtoId select c).First();
                    produtoUrl = rnProdutos.GerarPermalinkProduto(produtoId, produtoAlt.produtoNome).Trim().ToLower();
                    produtoAlt.produtoUrl = produtoUrl;
                    produtoAlt.fotoDestaque = produtoPai.fotoDestaque;
                    produtoAlt.relevancia = produtoPai.relevancia;
                    produtoAlt.produtoNomeAntigo = produtoPai.produtoNomeAntigo;
                    produtoAlt.relevanciaManual = produtoPai.relevanciaManual;
                    produtoAlt.idCentroDistribuicao = produtoPai.idCentroDistribuicao;
                    produtoAlt.produtoPrecoCustoAntigo = produtoPai.produtoPrecoCustoAntigo;
                    produtoAlt.produtoPrecoPromocionalAntigo = produtoPai.produtoPrecoPromocionalAntigo;
                    data.SubmitChanges();

                    rnProdutos.AtualizaDescontoCombo(produtoId);

                    rnBuscaCloudSearch.AtualizarProduto(produtoId);

                    var queueFoto = new tbQueue()
                    {
                        agendamento = DateTime.Now,
                        andamento = false,
                        concluido = false,
                        idRelacionado = produtoId,
                        mensagem = "1",
                        tipoQueue = 26
                    };
                    data.tbQueues.InsertOnSubmit(queueFoto);
                    data.SubmitChanges();


                }
                Response.Write("<script>alert('Produto duplicado com sucesso.');</script>");

                if (produtoId > 0)
                {
                    fillRelacionados(produtoId);
                    hdfIdProdutoNovo.Value = produtoId.ToString();
                    txtNomeProdutoRecemDuplicado.Text = produtoPai.produtoNome;
                    txtProdutoPreco.Text = produtoPai.produtoPreco.ToString("n");
                    txtProdutoPrecoPromocional.Text = ((decimal)produtoPai.produtoPrecoPromocional).ToString("n");
                    txtProdutoPrecoAtacado.Text = ((decimal)produtoPai.produtoPrecoAtacado).ToString("n");
                    txtProdutoPrecoDeCusto.Text = ((decimal)produtoPai.produtoPrecoDeCusto).ToString("n");
                }


                //Response.Write("<script>popEditarProdutoDuplicado.Show();</script>");

                return true;
            }
        }
        catch (Exception e)
        {
            throw new Exception(e.Message);
        }
    }

    protected bool duplicarPorCategoria(int idProdutoPai)
    {
        try
        {
            using (var data = new dbCommerceDataContext())
            {
                var produtoPai = (from c in data.tbProdutos where c.produtoId == idProdutoPai select c).FirstOrDefault();

                int produtoId;
                int produtoPaiId = (int)produtoPai.produtoPaiId;
                string produtoMetaDescription = produtoPai.produtoNome + " + Berço Lila" + " " + produtoPai.produtoDescricao;
                string produtoMetakeywords = produtoPai.produtoNome + " + Berço Lila" + " " + produtoPai.produtoDescricao;
                string produtoUrl = rnProdutos.GerarPermalinkProduto(0, produtoPai.produtoNome + " + Berço Lila").Trim().ToLower();

                int produtoAlternativoId = 0;
                if ((produtoPai.produtoAlternativoId ?? 0) > 0) int.TryParse(produtoPai.produtoAlternativoId.ToString(), out produtoAlternativoId);

                #region Define valores dos checkboxs
                string produtoAtivo = "False";
                string produtoPrincipal = produtoPai.produtoPrincipal;
                string produtoFreteGratis = produtoPai.produtoFreteGratis;
                string produtoLancamento = produtoPai.produtoLancamento;
                string preVenda = produtoPai.preVenda;
                string produtoPromocao = produtoPai.produtoPromocao;
                string destaque1 = produtoPai.destaque1;
                string destaque2 = produtoPai.destaque2;
                string destaque3 = produtoPai.destaque3;
                string exclusivo = produtoPai.produtoExclusivo;
                string marketplaceCadastrar = produtoPai.marketplaceCadastrar;
                string marketplaceEnviarMarca = produtoPai.marketplaceEnviarMarca;
                string descontoProgressivoFrete = produtoPai.descontoProgressivoFrete;
                string cubagem = produtoPai.cubagem;
                string exibirDiferencaCombo = produtoPai.exibirDiferencaCombo;
                #endregion

                produtoId = rnProdutos.produtoInclui(produtoPaiId, produtoPrincipal, produtoPai.produtoIdDaEmpresa, produtoPai.produtoCodDeBarras, produtoPai.produtoNome + " + Berço Lila",
                                                     produtoUrl, produtoPai.produtoDescricao, produtoPai.produtoFornecedor, produtoPai.produtoMarca, produtoPai.produtoPreco,
                                                     produtoPai.produtoPrecoDeCusto ?? 0, produtoPai.produtoPrecoPromocional ?? 0, produtoPai.produtoPrecoAtacado ?? 0,
                                                     produtoPai.produtoQuantidadeMininaParaAtacado ?? 0, produtoPai.produtoLegendaAtacado, produtoPai.produtoEstoqueAtual,
                                                     produtoPai.produtoEstoqueMinimo ?? 0, (decimal)produtoPai.produtoPeso, produtoAtivo, produtoFreteGratis, produtoLancamento,
                                                     produtoPromocao, produtoMetaDescription, produtoMetakeywords, destaque1, destaque2, destaque3, produtoPai.disponibilidadeEmEstoque,
                                                     preVenda, produtoPai.produtoComposicao, produtoPai.produtoFios, produtoPai.produtoPecas, produtoPai.produtoBrindes, produtoPai.produtoTags,
                                                     produtoPai.ncm, marketplaceCadastrar, marketplaceEnviarMarca, exclusivo, produtoPai.altura ?? 0, produtoPai.largura ?? 0,
                                                     produtoPai.profundidade ?? 0, produtoPai.complementoIdDaEmpresa, descontoProgressivoFrete, cubagem, produtoPai.produtoNomeNovo + " + Berço Lila",
                                                     exibirDiferencaCombo, produtoPai.siteId, produtoAlternativoId, produtoPai.bloqueado, produtoPai.relevancia ?? 0, produtoPai.prontaEntrega,
                                                     produtoPai.foraDeLinha, produtoPai.icms ?? 0, "", "", "", "");

                if (produtoId > 0)
                {

                    if (produtoPrincipal == "True")
                        rnProdutos.alteraProdutoPrincipal(produtoPaiId, produtoId);

                    #region categoria
                    var listaDeCategorias =
                        (from c in data.tbJuncaoProdutoCategorias where c.produtoId == produtoPai.produtoId select c);

                    foreach (var categoria in listaDeCategorias)
                    {
                        rnCategorias.juncaoProdutoCategoriaInclui(produtoId, categoria.categoriaId, produtoPaiId);
                    }
                    #endregion categoria

                    #region produtos relacionados (combo)

                    var relacionados = (from c in data.tbProdutoRelacionados
                                        join d in data.tbProdutos on c.idProdutoFilho equals d.produtoId
                                        where c.idProdutoPai == produtoPai.produtoId
                                        select
                                            new
                                            {
                                                d.produtoIdDaEmpresa,
                                                d.complementoIdDaEmpresa,
                                                d.produtoId,
                                                d.fotoDestaque,
                                                d.produtoNome,
                                                c.idProdutoRelacionado,
                                                c.desconto,
                                                d.produtoPrecoDeCusto,
                                                c.idProdutoFilho
                                            });

                    foreach (var produtoRelacinado in relacionados)
                    {
                        var produto = new tbProdutoRelacionado
                        {
                            idProdutoPai = produtoId,
                            idProdutoFilho = produtoRelacinado.idProdutoFilho,
                            desconto = produtoRelacinado.desconto
                        };

                        data.tbProdutoRelacionados.InsertOnSubmit(produto);
                    }

                    #endregion produtos relacionados (combo)

                    #region colecão
                    var colecoesAtuais = (from c in data.tbJuncaoProdutoColecaos where c.produtoId == produtoPai.produtoId select c);

                    foreach (var colecao in colecoesAtuais)
                    {
                        var colecaoAdicionar = new tbJuncaoProdutoColecao
                        {
                            produtoId = produtoId,
                            colecaoId = colecao.colecaoId,
                            dataDaCriacao = DateTime.Now
                        };
                        data.tbJuncaoProdutoColecaos.InsertOnSubmit(colecaoAdicionar);
                        data.SubmitChanges();
                    }
                    #endregion coleção

                    #region descrição tabela grupo
                    var tabelasAtuais = (from c in data.tbProdutoDescricaoTabelaGrupos where c.produtoId == produtoPai.produtoId select c);

                    foreach (var descricaoTab in tabelasAtuais)
                    {
                        var tabelaAdicionar = new tbProdutoDescricaoTabelaGrupo
                        {
                            produtoId = produtoId,
                            idDescricaoTabelaGrupo = descricaoTab.idDescricaoTabelaGrupo
                        };
                        data.tbProdutoDescricaoTabelaGrupos.InsertOnSubmit(tabelaAdicionar);
                        data.SubmitChanges();
                    }
                    #endregion descrição tabela grupo

                    #region especificações
                    var especificacoes =
                        (from c in data.tbJuncaoProdutoEspecificacaos where c.produtoId == produtoPai.produtoId select c);

                    foreach (var item1 in especificacoes)
                    {

                        int especificacaoId = item1.especificacaoId;

                        #region inclui produtoId e especificacao na tabela tbJuncaoProdutoEspecificacao. antes de incluir verifico se já existe na procedure
                        rnEspecificacao.juncaoProdutoEspecificacaoInclui(produtoId, especificacaoId);
                        #endregion

                        //int produtoItemEspecificacaoId = int.Parse(drpEspecificacoes.SelectedItem.Value);

                        //#region incluo as especificacoes selecionadas na tabela tbProdutoItemEspecificacao
                        //rnEspecificacao.produtoItensEspecificacaoInclui(produtoId, produtoPaiId, especificacaoId, produtoItemEspecificacaoId);
                        //#endregion

                    }

                    #endregion especificações

                    #region garantia estendida

                    var garantiasEstendidas =
                        (from c in data.tbProdutoGarantiaEstendidas where c.produtoId == produtoPai.produtoId select c);

                    foreach (var garantia in garantiasEstendidas)
                    {
                        rnGarantiaEstendida.garantiaEstendidaInclui(produtoId, garantia.garantiaNome, garantia.garantiaValor);
                    }

                    #endregion garantia estendida

                    #region cria diretorio de foto
                    Directory.CreateDirectory(ConfigurationManager.AppSettings["caminhoFisico"] + "fotos\\" + produtoId);
                    #endregion cria diretorio de foto

                    #region foto inclui

                    string Location;
                    string Target;

                    #region Fotos Galeria

                    var fotos = (from c in data.tbProdutoFotos where c.produtoId == produtoPai.produtoId select c);

                    foreach (var foto in fotos)
                    {

                        FileInfo file = new FileInfo(foto.produtoFoto);

                        #region Recupero o produtoFotoId

                        int fotoId;
                        if (rnFotos.produtoFotoSelecionaUltimoId().Tables[0].Rows.Count > 0)
                            fotoId =
                                int.Parse(
                                    rnFotos.produtoFotoSelecionaUltimoId().Tables[0].Rows[0]["produtoFotoId"]
                                        .ToString()) + 1;
                        else
                            fotoId = 0;

                        #endregion

                        string fotoRenomeada = fotoId + ".jpg";

                        //Salva foto no banco
                        rnFotos.fotosInclui(produtoId, fotoId.ToString(), foto.produtoFotoDescricao);

                        if (!Directory.Exists(ConfigurationManager.AppSettings["caminhoFisico"] + "fotos\\" + produtoId + "\\")) //se não existir o diretório então cria
                            Directory.CreateDirectory(ConfigurationManager.AppSettings["caminhoFisico"] + "fotos\\" + produtoId + "\\");

                        //foto do produto pai
                        Location = ConfigurationManager.AppSettings["caminhoFisico"] + "fotos\\" + idProdutoPai + "\\" + foto.produtoFoto + ".jpg";
                        if (File.Exists(Location))
                        {
                            File.Copy(Location, ConfigurationManager.AppSettings["caminhoFisico"] + "fotos\\" + produtoId + "\\" + fotoRenomeada, true);
                            uploadImagemAmazonS3(fotoRenomeada, produtoId);
                        }

                        //foto do produto pai média
                        Target = ConfigurationManager.AppSettings["caminhoFisico"] + "fotos\\" + idProdutoPai + "\\" + "media_" + foto.produtoFoto + ".jpg";
                        if (File.Exists(Target))
                        {
                            File.Copy(Target, ConfigurationManager.AppSettings["caminhoFisico"] + "fotos\\" + produtoId + "\\media_" + fotoRenomeada, true);
                            uploadImagemAmazonS3("media_" + fotoRenomeada, produtoId);
                        }

                        //foto do produto pai pequena
                        Target = ConfigurationManager.AppSettings["caminhoFisico"] + "fotos\\" + idProdutoPai + "\\" + "pequena_" + foto.produtoFoto + ".jpg";
                        if (File.Exists(Target))
                        {
                            File.Copy(Target, ConfigurationManager.AppSettings["caminhoFisico"] + "fotos\\" + produtoId + "\\pequena_" + fotoRenomeada, true);
                            uploadImagemAmazonS3("pequena_" + fotoRenomeada, produtoId);
                        }

                        //foto do produto pai 500 mobile
                        Target = ConfigurationManager.AppSettings["caminhoFisico"] + "fotos\\" + idProdutoPai + "\\" + "500_" + foto.produtoFoto + ".jpg";
                        if (File.Exists(Target))
                        {
                            File.Copy(Target, ConfigurationManager.AppSettings["caminhoFisico"] + "fotos\\" + produtoId + "\\500_" + fotoRenomeada, true);
                            uploadImagemAmazonS3("500_" + fotoRenomeada, produtoId);
                        }


                    }

                    #endregion Fotos Galeria

                    #region Foto Produto Destaque Categoria

                    try
                    {

                        Target = ConfigurationManager.AppSettings["caminhoFisico"] + "fotos\\" + idProdutoPai + "\\" + "destaquecategoria.jpg";
                        if (File.Exists(Target))
                        {
                            //Salva foto no banco
                            rnFotos.fotosInclui(produtoId, "destaquecategoria.jpg", "");

                            File.Copy(Target, ConfigurationManager.AppSettings["caminhoFisico"] + "fotos\\" + produtoId + "destaquecategoria.jpg", true);
                        }

                    }
                    catch (Exception)
                    {


                    }

                    #endregion Foto Produto Destaque Categoria

                    #endregion foto inclui

                    #region informações adicionais

                    var informacoesAdicionais = (from c in data.tbInformacaoAdcionals where c.produtoId == produtoPai.produtoId select c);

                    foreach (var itemInfo in informacoesAdicionais)
                    {
                        rnInformacoesAdcionais.produtoInformacoesAdcionaisInclui(produtoId, itemInfo.informacaoAdcionalNome, itemInfo.informacaoAdcionalConteudo);
                    }

                    #endregion informações adicionais

                    #region videos

                    var videos = (from v in data.tbProdutoVideos where v.produtoId == produtoPai.produtoId select v).ToList();

                    foreach (var video in videos)
                    {
                        var adicionaLink = new tbProdutoVideo
                        {
                            linkVideo = video.linkVideo,
                            produtoId = produtoId,
                            dataCadastro = DateTime.Now
                        };
                        data.tbProdutoVideos.InsertOnSubmit(adicionaLink);
                        data.SubmitChanges();
                    }

                    #endregion videos

                    var produtoAlt = (from c in data.tbProdutos where c.produtoId == produtoId select c).First();
                    produtoUrl = rnProdutos.GerarPermalinkProduto(produtoId, produtoAlt.produtoNome).Trim().ToLower();
                    produtoAlt.produtoUrl = produtoUrl;
                    produtoAlt.fotoDestaque = produtoPai.fotoDestaque;
                    produtoAlt.relevancia = produtoPai.relevancia;
                    produtoAlt.produtoNomeAntigo = produtoPai.produtoNomeAntigo + " + Berço Lila";
                    produtoAlt.relevanciaManual = produtoPai.relevanciaManual;
                    produtoAlt.idCentroDistribuicao = produtoPai.idCentroDistribuicao;
                    produtoAlt.produtoPrecoCustoAntigo = produtoPai.produtoPrecoCustoAntigo;
                    produtoAlt.produtoPrecoPromocionalAntigo = produtoPai.produtoPrecoPromocionalAntigo;
                    data.SubmitChanges();

                    rnProdutos.AtualizaDescontoCombo(produtoId);

                    rnBuscaCloudSearch.AtualizarProduto(produtoId);


                }
                Response.Write("<script>alert('Produto duplicado com sucesso.');</script>");

                if (produtoId > 0)
                {
                    fillRelacionados(produtoId);
                    hdfIdProdutoNovo.Value = produtoId.ToString();
                    txtNomeProdutoRecemDuplicado.Text = produtoPai.produtoNome + " + Berço Lila";
                    txtProdutoPreco.Text = produtoPai.produtoPreco.ToString("n");
                    txtProdutoPrecoPromocional.Text = ((decimal)produtoPai.produtoPrecoPromocional).ToString("n");
                    txtProdutoPrecoAtacado.Text = ((decimal)produtoPai.produtoPrecoAtacado).ToString("n");
                    txtProdutoPrecoDeCusto.Text = ((decimal)produtoPai.produtoPrecoDeCusto).ToString("n");
                }


                //Response.Write("<script>popEditarProdutoDuplicado.Show();</script>");

                return true;
            }
        }
        catch (Exception e)
        {
            throw new Exception(e.Message);
        }
    }

    protected void btnProdutoRelacionadoAdicionar_OnClick(object sender, EventArgs e)
    {
        int produtoPai = int.Parse(hdfIdProdutoNovo.Value);
        var produtoDc = new dbCommerceDataContext();
        int desconto = 0;
        int.TryParse(txtProdutoRelacionadoDesconto.Text, out desconto);

        var ids = txtProdutoRelacionadoId.Text.Split(';');
        var listaMultiplasOpcoes = new List<string>();

        int quantidade = 1;
        if (!string.IsNullOrEmpty(txtProdutoRelacionadoQuantidade.Text))
        {
            int.TryParse(txtProdutoRelacionadoQuantidade.Text, out quantidade);
        }
        if (quantidade == 0) quantidade = 1;

        if (lstProdutoRelacionadoComplemento.Visible == false)
        {
            foreach (var id in ids)
            {
                var produtos = (from c in produtoDc.tbProdutos where c.produtoIdDaEmpresa == id.Trim() select c);
                if (produtos.Count() > 1)
                {
                    listaMultiplasOpcoes.Add(id);
                    lstProdutoRelacionadoComplemento.Visible = true;
                }
                else if (produtos.Count() == 1)
                {
                    int produtoId = produtos.First().produtoId;
                    var produtosRelacionados = (from c in produtoDc.tbProdutoRelacionados where c.idProdutoPai == produtoId select c);
                    if (produtosRelacionados.Any())
                    {
                        foreach (var relacionado in produtosRelacionados)
                        {
                            var possuiEsteProduto = validaSePossuiProdutoRelacionado(relacionado.idProdutoFilho);
                            if (chkProdutoRelacionadoValidar.Checked == false) possuiEsteProduto = false;
                            if (!possuiEsteProduto)
                            {
                                for (int i = 1; i <= quantidade; i++)
                                {
                                    var produtoRelacionado = new tbProdutoRelacionado
                                    {
                                        idProdutoPai = produtoPai,
                                        idProdutoFilho = relacionado.idProdutoFilho,
                                        desconto = desconto
                                    };
                                    produtoDc.tbProdutoRelacionados.InsertOnSubmit(produtoRelacionado);
                                    produtoDc.SubmitChanges();
                                }
                                txtProdutoRelacionadoId.Text = "";
                                txtProdutoRelacionadoDesconto.Text = "0";
                                fillRelacionados(produtoPai);
                            }
                        }
                    }
                    else
                    {
                        var possuiEsteProduto = validaSePossuiProdutoRelacionado(produtoId);
                        if (chkProdutoRelacionadoValidar.Checked == false) possuiEsteProduto = false;
                        if (!possuiEsteProduto)
                        {
                            for (int i = 1; i <= quantidade; i++)
                            {
                                var produtoRelacionado = new tbProdutoRelacionado
                                {
                                    idProdutoPai = produtoPai,
                                    idProdutoFilho = produtoId,
                                    desconto = desconto
                                };
                                produtoDc.tbProdutoRelacionados.InsertOnSubmit(produtoRelacionado);
                                produtoDc.SubmitChanges();
                            }
                            txtProdutoRelacionadoId.Text = "";
                            txtProdutoRelacionadoDesconto.Text = "0";
                            fillRelacionados(produtoPai);
                        }
                        else
                        {
                            Response.Write("<script>alert('Produto relacionado já cadastrado.');</script>");
                        }
                    }

                }
                else
                {
                    Response.Write("<script>alert('Produto não localizado.');</script>");
                }
            }
            if (listaMultiplasOpcoes.Count > 0)
            {
                Response.Write("<script>alert('Produto com múltiplas opções. Favor selecionar da lista e adicionar.');</script>");
                lstProdutoRelacionadoComplemento.DataSource = listaMultiplasOpcoes;
                lstProdutoRelacionadoComplemento.DataBind();
            }
        }
        else
        {
            foreach (var item in lstProdutoRelacionadoComplemento.Items)
            {
                var ddlProdutoRelacionadoComplemento = (DropDownList)item.FindControl("ddlProdutoRelacionadoComplemento");
                int produtoId = Convert.ToInt32(ddlProdutoRelacionadoComplemento.SelectedValue);
                var produtosRelacionados = (from c in produtoDc.tbProdutoRelacionados where c.idProdutoPai == produtoId select c);
                if (produtosRelacionados.Any())
                {
                    foreach (var relacionado in produtosRelacionados)
                    {
                        var possuiEsteProduto = validaSePossuiProdutoRelacionado(relacionado.idProdutoFilho);
                        if (chkProdutoRelacionadoValidar.Checked == false) possuiEsteProduto = false;
                        if (!possuiEsteProduto)
                        {
                            for (int i = 1; i <= quantidade; i++)
                            {
                                var produtoRelacionado = new tbProdutoRelacionado
                                {
                                    idProdutoPai = produtoPai,
                                    idProdutoFilho = relacionado.idProdutoFilho,
                                    desconto = desconto
                                };
                                produtoDc.tbProdutoRelacionados.InsertOnSubmit(produtoRelacionado);
                                produtoDc.SubmitChanges();
                            }
                            txtProdutoRelacionadoId.Text = "";
                            txtProdutoRelacionadoDesconto.Text = "0";
                            fillRelacionados(produtoPai);
                        }
                    }
                }
                else
                {
                    bool validaSePossuiProduto = validaSePossuiProdutoRelacionado(produtoId);

                    if (chkProdutoRelacionadoValidar.Checked == false) validaSePossuiProduto = false;
                    if (!validaSePossuiProduto)
                    {
                        for (int i = 1; i <= quantidade; i++)
                        {
                            var produtoRelacionado = new tbProdutoRelacionado
                            {
                                idProdutoPai = produtoPai,
                                idProdutoFilho = produtoId,
                                desconto = desconto
                            };
                            produtoDc.tbProdutoRelacionados.InsertOnSubmit(produtoRelacionado);
                            produtoDc.SubmitChanges();
                        }
                        txtProdutoRelacionadoId.Text = "";
                        ddlProdutoRelacionadoComplemento.Items.Clear();
                        Response.Write("<script>alert('Produto relacionado inserido com sucesso.');</script>");
                        txtProdutoRelacionadoDesconto.Text = "0";
                        fillRelacionados(produtoPai);
                    }
                    else
                    {
                        Response.Write("<script>alert('Produto relacionado já cadastrado.');</script>");
                    }
                }
            }
            lstProdutoRelacionadoComplemento.Visible = false;
        }


        rnProdutos.AtualizaDescontoCombo(produtoPai);
        rnBuscaCloudSearch.AtualizarProduto(produtoPai);

    }

    protected void btnRemoverProdutoRelacionado_OnCommand(object sender, CommandEventArgs e)
    {
        int produtoPai = int.Parse(hdfIdProdutoNovo.Value);
        int produtoRelacionadoId = Convert.ToInt32(e.CommandArgument);
        var data = new dbCommerceDataContext();
        var produtoRelacionado =
            (from c in data.tbProdutoRelacionados where c.idProdutoRelacionado == produtoRelacionadoId select c)
                .FirstOrDefault();
        if (produtoRelacionado != null)
        {
            data.tbProdutoRelacionados.DeleteOnSubmit(produtoRelacionado);
            data.SubmitChanges();
        }
        Response.Write("<script>alert('Produto relacionado removido com sucesso.');</script>");
        fillRelacionados(produtoPai);

        rnProdutos.AtualizaDescontoCombo(produtoPai);
        rnBuscaCloudSearch.AtualizarProduto(produtoPai);

    }

    protected void btnGravar_OnClick(object sender, EventArgs e)
    {
        try
        {
            int produtoId = Convert.ToInt32(hdfIdProdutoNovo.Value);
            fotoInclui(produtoId);

            using (var data = new dbCommerceDataContext())
            {
                var produtoAlt = (from c in data.tbProdutos where c.produtoId == produtoId select c).First();
                string produtoUrl = rnProdutos.GerarPermalinkProduto(produtoId, txtNomeProdutoRecemDuplicado.Text).Trim().ToLower();
                produtoAlt.produtoUrl = produtoUrl;
                produtoAlt.produtoPreco = decimal.Parse(txtProdutoPreco.Text);
                produtoAlt.produtoPrecoPromocional = decimal.Parse(txtProdutoPrecoPromocional.Text);
                produtoAlt.produtoPrecoAtacado = decimal.Parse(txtProdutoPrecoAtacado.Text);
                produtoAlt.produtoPrecoDeCusto = decimal.Parse(txtProdutoPrecoDeCusto.Text);
                produtoAlt.produtoNome = txtNomeProdutoRecemDuplicado.Text;
                data.SubmitChanges();
            }


            rnBuscaCloudSearch.AtualizarProduto(produtoId);

            popEditarProdutoDuplicado.ShowOnPageLoad = false;

            Response.Write("<script>alert('Dados salvos com sucesso.');</script>");
        }
        catch (Exception)
        {

        }

    }

    protected void lstProdutoRelacionadoComplemento_OnItemDataBound(object sender, ListViewItemEventArgs e)
    {
        var litProdutoRelacionadoId = (Literal)e.Item.FindControl("litProdutoRelacionadoId");
        var ddlProdutoRelacionadoComplemento = (DropDownList)e.Item.FindControl("ddlProdutoRelacionadoComplemento");

        var produtoDc = new dbCommerceDataContext();
        var produtos = (from c in produtoDc.tbProdutos where c.produtoIdDaEmpresa == litProdutoRelacionadoId.Text select c);
        foreach (var produto in produtos)
        {
            ddlProdutoRelacionadoComplemento.Items.Add(new ListItem(produto.complementoIdDaEmpresa, produto.produtoId.ToString()));
        }
    }

    private void fillRelacionados(int produtoId)
    {
        int produtoPai = produtoId;
        var data = new dbCommerceDataContext();
        var relacionados = (from c in data.tbProdutoRelacionados
                            join d in data.tbProdutos on c.idProdutoFilho equals d.produtoId
                            where c.idProdutoPai == produtoPai
                            select
                                new
                                {
                                    d.produtoIdDaEmpresa,
                                    d.complementoIdDaEmpresa,
                                    d.produtoId,
                                    d.fotoDestaque,
                                    d.produtoNome,
                                    c.idProdutoRelacionado,
                                    c.desconto,
                                    d.produtoPrecoDeCusto
                                });
        lstProdutosRelacionados.DataSource = relacionados;
        lstProdutosRelacionados.DataBind();
        litProdutosRelacionados.Text = relacionados.Count().ToString();
        if (relacionados.Any())
        {
            decimal totalCusto = 0;
            decimal.TryParse(relacionados.Where(x => x.desconto == 0).Sum(x => x.produtoPrecoDeCusto).ToString(), out totalCusto);
            litCustoCombo.Text = totalCusto.ToString("C");
        }

    }

    private bool validaSePossuiProdutoRelacionado(int produtoId)
    {
        int produtoPai = int.Parse(hdfIdProdutoNovo.Value);
        var data = new dbCommerceDataContext();
        int quantidade =
            (from c in data.tbProdutoRelacionados
             where c.idProdutoFilho == produtoId && c.idProdutoPai == produtoPai
             select c).Count();
        return (quantidade > 0);

    }

    private void fotoInclui(int produtoId)
    {
        int i = 1;
        string Location;
        string Target;

        #region Fotos Galeria
        for (i = 1; i <= 3; i++)
        {
            FileUpload upl = (FileUpload)pnlFotos.FindControl("upl" + i.ToString());
            TextBox txtFotoDescricao = (TextBox)pnlFotos.FindControl("txtFotoDescricao" + i.ToString());

            if (upl.HasFile == true)
            {

                FileInfo file = new FileInfo(upl.PostedFile.FileName);

                #region Recupero o produtoFotoId
                int fotoId;
                if (rnFotos.produtoFotoSelecionaUltimoId().Tables[0].Rows.Count > 0)
                    fotoId = int.Parse(rnFotos.produtoFotoSelecionaUltimoId().Tables[0].Rows[0]["produtoFotoId"].ToString()) + 1;
                else
                    fotoId = 0;
                #endregion

                string fotoRenomeada = fotoId + ".jpg";

                //Salva foto no banco
                rnFotos.fotosInclui(produtoId, fotoId.ToString(), txtFotoDescricao.Text);

                if (!Directory.Exists(ConfigurationManager.AppSettings["caminhoFisico"] + "fotos\\" + produtoId + "\\"))//se não existir o diretório então cria
                    Directory.CreateDirectory(ConfigurationManager.AppSettings["caminhoFisico"] + "fotos\\" + produtoId + "\\");

                Location = ConfigurationManager.AppSettings["caminhoFisico"] + "fotos\\" + produtoId + "\\" + fotoRenomeada;

                #region Compacta imagem com Kraken

                KrakenConfig _kConfig = new KrakenConfig();
                string key = _kConfig.APIKey;
                string secret = _kConfig.APISecret;

                Kraken k = new Kraken(key, secret);

                KrakenRequest kr = new KrakenRequest();

                kr.Lossy = true;

                if (_kConfig.UseCallbacks)
                {
                    kr.CallbackUrl = _kConfig.CallbackURL;
                }
                else
                {
                    kr.Wait = true;
                }

                // Can't trust the length of Stream. Converting to a MemoryStream
                using (System.IO.MemoryStream ms = new System.IO.MemoryStream())
                {
                    upl.FileContent.CopyTo(ms);

                    kr.File = ms.ToArray();
                }


                #endregion Compacta imagem com Kraken

                #region S3 foto Grande

                kr.s3_store = new KrakenRequestS3Amazon();
                kr.s3_store.S3Key = "AKIAIP7IE6WVNK2K6TIQ";
                kr.s3_store.S3Secret = "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";
                kr.s3_store.S3Bucket = "cdn2.graodegente.com.br";
                kr.s3_store.S3Region = "sa-east-1";
                kr.s3_store.S3Path = "fotos/" + produtoId + "/" + fotoRenomeada;

                var response = k.Upload(kr, fotoRenomeada, upl.PostedFile.ContentType);

                if (response.Success == false || response.Error != null)
                {

                }

                using (var webClient = new System.Net.WebClient())
                using (var stream = webClient.OpenRead(response.KrakedUrl))
                {

                    webClient.DownloadFile(response.KrakedUrl, ConfigurationManager.AppSettings["caminhoFisico"] + "fotos\\" + produtoId + "\\" + response.KrakedUrl.Substring(response.KrakedUrl.LastIndexOf('/') + 1));
                }

                #endregion S3 foto Grande

                //Cria a foto média
                Target = ConfigurationManager.AppSettings["caminhoFisico"] + "fotos\\" + produtoId + "\\" + "media_" + fotoRenomeada;
                rnFotos.ResizeImageFile(int.Parse(ConfigurationManager.AppSettings["fotoMedia"]), Location, Target, ImageFormat.Jpeg);
                //Cria a foto pequena
                Target = ConfigurationManager.AppSettings["caminhoFisico"] + "fotos\\" + produtoId + "\\" + "pequena_" + fotoRenomeada;
                rnFotos.ResizeImageFile(int.Parse(ConfigurationManager.AppSettings["fotoPequena"]), Location, Target, ImageFormat.Jpeg);
                //Cria a foto 500 mobile
                Target = ConfigurationManager.AppSettings["caminhoFisico"] + "fotos\\" + produtoId + "\\" + "500_" + fotoRenomeada;
                rnFotos.ResizeImageFile(500, Location, Target, ImageFormat.Jpeg);

                #region S3 foto Media

                if (System.IO.File.Exists(ConfigurationManager.AppSettings["caminhoFisico"] + "fotos\\" + produtoId + "\\" + "media_" + fotoRenomeada))
                {

                    System.IO.FileStream imageData = System.IO.File.Open((ConfigurationManager.AppSettings["caminhoFisico"] + "fotos\\" + produtoId + "\\" + "media_" + fotoRenomeada), System.IO.FileMode.Open);

                    if (imageData != null)
                    {


                        // Can't trust the length of Stream. Converting to a MemoryStream
                        using (System.IO.MemoryStream ms = new System.IO.MemoryStream())
                        {
                            imageData.CopyTo(ms);

                            kr.File = ms.ToArray();
                        }

                        kr.s3_store.S3Path = "fotos/" + produtoId + "/media_" + fotoRenomeada;

                        response = k.Upload(kr, (ConfigurationManager.AppSettings["caminhoFisico"] + "fotos\\" + produtoId + "\\media_" + fotoRenomeada + ""), ".jpg");

                        if (response.Success == false || response.Error != null)
                        {

                        }

                        using (var webClient = new System.Net.WebClient())
                        using (var stream = webClient.OpenRead(response.KrakedUrl))
                        {
                            if (!System.IO.Directory.Exists(ConfigurationManager.AppSettings["caminhoFisico"] + "fotos\\" + produtoId + "\\"))//se não existir o diretório então cria
                                System.IO.Directory.CreateDirectory(ConfigurationManager.AppSettings["caminhoFisico"] + "fotos\\" + produtoId + "\\");

                            imageData.Dispose();

                            webClient.DownloadFile(response.KrakedUrl, ConfigurationManager.AppSettings["caminhoFisico"] + "fotos\\" + produtoId + "\\" + response.KrakedUrl.Substring(response.KrakedUrl.LastIndexOf('/') + 1));

                        }

                    }
                }

                #endregion S3 foto Media

                #region S3 foto Pequena

                if (System.IO.File.Exists(ConfigurationManager.AppSettings["caminhoFisico"] + "fotos\\" + produtoId + "\\" + "pequena_" + fotoRenomeada))
                {

                    System.IO.FileStream imageData = System.IO.File.Open((ConfigurationManager.AppSettings["caminhoFisico"] + "fotos\\" + produtoId + "\\pequena_" + fotoRenomeada), System.IO.FileMode.Open);

                    if (imageData != null)
                    {

                        // Can't trust the length of Stream. Converting to a MemoryStream
                        using (System.IO.MemoryStream ms = new System.IO.MemoryStream())
                        {
                            imageData.CopyTo(ms);

                            kr.File = ms.ToArray();
                        }


                        kr.s3_store.S3Path = "fotos/" + produtoId + "/pequena_" + fotoRenomeada;

                        response = k.Upload(kr, (ConfigurationManager.AppSettings["caminhoFisico"] + "fotos\\" + produtoId + "\\pequena_" + fotoRenomeada + ""), ".jpg");

                        if (response.Success == false || response.Error != null)
                        {

                        }


                        using (var webClient = new System.Net.WebClient())
                        using (var stream = webClient.OpenRead(response.KrakedUrl))
                        {
                            if (!System.IO.Directory.Exists(ConfigurationManager.AppSettings["caminhoFisico"] + "fotos\\" + produtoId + "\\"))//se não existir o diretório então cria
                                System.IO.Directory.CreateDirectory(ConfigurationManager.AppSettings["caminhoFisico"] + "fotos\\" + produtoId + "\\");

                            imageData.Dispose();

                            webClient.DownloadFile(response.KrakedUrl, ConfigurationManager.AppSettings["caminhoFisico"] + "fotos\\" + produtoId + "\\" + response.KrakedUrl.Substring(response.KrakedUrl.LastIndexOf('/') + 1));

                        }

                    }

                }

                #endregion S3 foto Pequena

                #region S3 foto 500 mobile

                if (System.IO.File.Exists(ConfigurationManager.AppSettings["caminhoFisico"] + "fotos\\" + produtoId + "\\" + "500_" + fotoRenomeada))
                {

                    System.IO.FileStream imageData = System.IO.File.Open((ConfigurationManager.AppSettings["caminhoFisico"] + "fotos\\" + produtoId + "\\500_" + fotoRenomeada), System.IO.FileMode.Open);

                    if (imageData != null)
                    {

                        // Can't trust the length of Stream. Converting to a MemoryStream
                        using (System.IO.MemoryStream ms = new System.IO.MemoryStream())
                        {
                            imageData.CopyTo(ms);

                            kr.File = ms.ToArray();
                        }


                        kr.s3_store.S3Path = "fotos/" + produtoId + "/500_" + fotoRenomeada;

                        response = k.Upload(kr, (ConfigurationManager.AppSettings["caminhoFisico"] + "fotos\\" + produtoId + "\\500_" + fotoRenomeada + ""), ".jpg");

                        if (response.Success == false || response.Error != null)
                        {

                        }


                        using (var webClient = new System.Net.WebClient())
                        using (var stream = webClient.OpenRead(response.KrakedUrl))
                        {
                            if (!System.IO.Directory.Exists(ConfigurationManager.AppSettings["caminhoFisico"] + "fotos\\" + produtoId + "\\"))//se não existir o diretório então cria
                                System.IO.Directory.CreateDirectory(ConfigurationManager.AppSettings["caminhoFisico"] + "fotos\\" + produtoId + "\\");

                            imageData.Dispose();

                            webClient.DownloadFile(response.KrakedUrl, ConfigurationManager.AppSettings["caminhoFisico"] + "fotos\\" + produtoId + "\\" + response.KrakedUrl.Substring(response.KrakedUrl.LastIndexOf('/') + 1));

                        }

                    }

                }
                #endregion foto 500 mobile

            }
        }
        #endregion Fotos Galeria
    }

    private void uploadImagemAmazonS3(string foto, int produtoId)
    {

        string s3ServiceUrl = "s3-sa-east-1.amazonaws.com";
        string bucketName = "cdn2.graodegente.com.br";
        string filePath = ConfigurationManager.AppSettings["caminhoFisico"] + "fotos\\" + produtoId + "\\" + foto;
        string fileContentType = "image/jpg";
        S3CannedACL fileCannedACL = S3CannedACL.PublicReadWrite;

        string accessKey = "AKIAIP7IE6WVNK2K6TIQ";
        string secretAccessKey = "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";

        try
        {
            string key = DateTime.Now.ToString("ddMMyyyyHHmmssffffff");
            AmazonS3Config config = new AmazonS3Config();
            config.ServiceURL = s3ServiceUrl;
            config.CommunicationProtocol = Protocol.HTTP;

            using (var s3Client = Amazon.AWSClientFactory.CreateAmazonS3Client(accessKey, secretAccessKey, config))
            {
                PutObjectRequest request = new PutObjectRequest
                {
                    BucketName = bucketName,
                    CannedACL = fileCannedACL,
                    Key = "fotos/" + produtoId + "/" + foto,
                    FilePath = filePath,
                    ContentType = fileContentType
                };

                PutObjectResponse response = s3Client.PutObject(request);

                //Console.WriteLine("File Key: {0}", key);
            }

            //Console.ReadKey();
        }
        catch (AmazonS3Exception s3Exception)
        {
            Console.WriteLine(s3Exception.Message, s3Exception.InnerException);

            Console.ReadKey();
        }
    }


    protected void btnExportarXls_Click(object sender, EventArgs e)
    {
        this.grdEx.WriteXlsToResponse();
    }

    protected void sqlProdutosServer_OnSelecting(object sender, LinqServerModeDataSourceSelectEventArgs e)
    {
        var data = new dbCommerceDataContext();
        e.KeyExpression = "produtoId";
        e.QueryableSource = rblAtivos.SelectedValue != "Todos" ? data.viewProdutosAdmins.Where(x => x.produtoAtivo == rblAtivos.SelectedValue) : data.viewProdutosAdmins;

    }



    protected void grd_OnCustomUnboundColumnData(object sender, ASPxGridViewColumnDataEventArgs e)
    {

        decimal precoDeCusto = Convert.ToDecimal(e.GetListSourceFieldValue("produtoPrecoDeCusto"));
        decimal precoDeVenda = Convert.ToDecimal(e.GetListSourceFieldValue("produtoPreco"));
        decimal precoPromocional = Convert.ToDecimal(e.GetListSourceFieldValue("produtoPrecoPromocional"));

        if (precoPromocional > 0)
            precoDeVenda = precoPromocional;

        decimal zero = 0;
        switch (e.Column.FieldName)
        {
            case "cmvCartao":
                if (precoDeVenda > 0)
                    e.Value = decimal.Round(precoDeCusto / precoDeVenda * 100, 2);
                else
                    e.Value = zero;
                break;

            case "cmvBoleto":
                if (precoDeVenda > 0)
                    e.Value = decimal.Round(precoDeCusto / (precoDeVenda * 0.85M) * 100, 2);
                else
                    e.Value = zero;
                break;

            case "cmvMedio":
                if (precoDeVenda > 0)
                    e.Value = decimal.Round((precoDeCusto / ((precoDeVenda + (precoDeVenda * 0.85M)) / 2) * 100), 2);
                
                else
                    e.Value = 0;
                break;

        }

    }
}
