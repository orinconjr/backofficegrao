﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="financeiroImportarCartao.aspx.cs" Inherits="admin_financeiroImportarCartao" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" Runat="Server">
    <table cellpadding="0" cellspacing="0" style="width: 920px">
        <tr>
            <td class="tituloPaginas" valign="top">
                <asp:Label ID="lblAcao" runat="server">Importar extrato Cartões</asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
                    <tr>
                        <td class="textoPreto">
                            Importar Arquivo Cielo
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:FileUpload ID="fluArquivo" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="textoPreto">
                            <asp:ImageButton ID="btnSalvar" runat="server" ImageAlign="AbsMiddle" ImageUrl="images/btSalvarPeq1.jpg" onclick="btnSalvar_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
                    <tr>
                        <td class="textoPreto">
                            Importar Arquivo Elavon
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:FileUpload ID="fluArquivoElavon" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="textoPreto">
                            <asp:ImageButton ID="btnSalvarElavon" runat="server" ImageAlign="AbsMiddle" ImageUrl="images/btSalvarPeq1.jpg" onclick="btnSalvarElavon_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>