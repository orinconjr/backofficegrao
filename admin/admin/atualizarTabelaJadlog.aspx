﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="atualizarTabelaJadlog.aspx.cs" Inherits="admin_atualizarTabelaJadlog" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxLoadingPanel" Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxCallback" Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxEditors" Assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" Runat="Server">
    <table cellpadding="0" cellspacing="0" style="width: 920px">
        <tr>
            <td class="tituloPaginas" valign="top">
                <asp:Label ID="lblAcao" runat="server">Atualizar Tabela Jadlog</asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
                    <tr>
                        <td class="textoPreto">
                            Selecione a planilha (Excel)
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:FileUpload ID="fluArquivo" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td class="textoPreto">
                            <asp:ImageButton ID="btnSalvar" runat="server" ImageAlign="AbsMiddle" ImageUrl="images/btSalvarPeq1.jpg" onclick="btnSalvar_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td class="textoPreto" style="padding-top: 20px;">
                            <dx:ASPxButton ID="btnEnviarGravar" runat="server" Text="Atualizar Tabela" AutoPostBack="False">
                                <ClientSideEvents Click="function(s, e) {
                                    cbGravar.PerformCallback();
                                    LoadingPanel.Show();
                                }" />
                            </dx:ASPxButton>
                            <dx:ASPxLoadingPanel ID="LoadingPanel" runat="server" ClientInstanceName="LoadingPanel" Modal="True" Text="Carregando... Por favor Aguarde&hellip;">
                            </dx:ASPxLoadingPanel>
                            <dx:ASPxCallback OnCallback="btnAtualizar_OnCallback" ID="cbGravar" runat="server" ClientInstanceName="cbGravar">
                                <ClientSideEvents CallbackComplete="function(s, e) { 
                                    LoadingPanel.Hide(); 
                                    grd.Refresh();
                                    }" />
                            </dx:ASPxCallback>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
