﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_gerarNotasDoDia : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnMostrarGerarNotas_OnClick(object sender, EventArgs e)
    {
        panelSelecionarData.Visible = false;
        pnGerarNota.Visible = true;
        selecionaPedidosParaNotas();

    }

    private void selecionaPedidosParaNotas()
    {
        var dcPedidos = new dbCommerceDataContext();
        var dcPedidoItens = new dbCommerceDataContext();
        var dcProdutos = new dbCommerceDataContext();

        //var pedidos = (from c in dcPedidos.tbPedidos  select c);
        //List<int> idPedidos = new List<int>();
        //idPedidos.AddRange(pedidos.Select(x => x.pedidoId));

        var diaFiltro = new DateTime(2014, 1, 24);
        #region pedidos que não foi emitido nota
        var pedidosFiltrar = (from c in dcPedidos.tbPedidos
                              where c.dataConfirmacaoPagamento != null && c.statusDoPedido != 6 && c.dataConfirmacaoPagamento > diaFiltro
                              select new
                              {
                                  marketplace = c.tipoDePagamentoId == 10 ? 1 : 0,
                                  c.pedidoId,
                                  c.valorCobrado,
                                  estadoOrdenacao = c.endEstado == "ES" | c.endEstado == "MG" | c.endEstado == "SP" | c.endEstado == "PR" | c.endEstado == "RS" | c.endEstado == "SC" ? 0 : 1,
                                  nfeNumero = c.nfeNumero == null ? 0 : c.nfeNumero
                              });
        pedidosFiltrar = pedidosFiltrar.Where(x => x.nfeNumero == 0);

        
        #endregion


        //Adiciono todos os pedidos do marketplace pois é obrigatória a emissão de nota fiscal
        var pedidoIds = new List<int>();
        pedidoIds.AddRange(pedidosFiltrar.Where(x => x.marketplace == 1).Select(x => x.pedidoId).ToList());
        pedidoIds.AddRange(pedidosFiltrar.Where(x => x.estadoOrdenacao == 1).Select(x => x.pedidoId).ToList());
        pedidoIds = pedidoIds.Distinct().ToList();


        litTotalDePedidosSeraoGerados.Text = pedidoIds.Count.ToString();

        var pedidosListaGerar = (from ped in dcPedidos.tbPedidos where pedidoIds.Contains(ped.pedidoId) select ped);
        lstPedidos.DataSource = pedidosListaGerar;
        lstPedidos.DataBind();


        var listaProdutoItens = (from c in dcPedidoItens.tbItensPedidos where pedidoIds.Contains(c.pedidoId) select c.produtoId).Distinct().ToList();
        var listProdutos = (from c in dcProdutos.tbProdutos where listaProdutoItens.Contains(c.produtoId) orderby c.produtoNome select c).ToList();
        lstProdutos.DataSource = listProdutos;
        lstProdutos.DataBind();
    }


    protected void lstPedidos_OnItemDataBound(object sender, ListViewItemEventArgs e)
    {
        var hiddenCep = (HiddenField)e.Item.FindControl("hiddenCep");
        var txtCodigoIbge = (TextBox)e.Item.FindControl("txtCodigoIbge");

        var cepsDc = new dbCommerceDataContext();
        try
        {
            string cepOut = "";
            if (hiddenCep != null)
            {
                cepOut = hiddenCep.Value;
            }

            var endereco = (from c in cepsDc.tbCepEnderecos where c.cep == cepOut.Replace("-", "") select c).FirstOrDefault();

            if (endereco != null)
            {
                var cidade = (from c in cepsDc.tbCepCidades where c.id_cidade == endereco.id_cidade select c).FirstOrDefault();
                if (cidade != null)
                {
                    txtCodigoIbge.Text = cidade.cod_ibge;
                }
            }

        }
        catch (Exception)
        {

        }
    }

    protected void btnGerarNotas_OnClick(object sender, EventArgs e)
    {
        var listaPedidosIgbe = new List<listaPedidosIbgs>();
        foreach (var pedido in lstPedidos.Items)
        {
            var hiddenPedidoId = (HiddenField)pedido.FindControl("hiddenPedidoId");
            var txtCodigoIbge = (TextBox)pedido.FindControl("txtCodigoIbge");

            var pedidoItem = new listaPedidosIbgs();
            pedidoItem.pedidoId = Convert.ToInt32(hiddenPedidoId.Value);
            pedidoItem.codigoIbge = txtCodigoIbge.Text.Trim();

            listaPedidosIgbe.Add(pedidoItem);
        }

        var produtodc = new dbCommerceDataContext();
        foreach (var produto in lstProdutos.Items)
        {
            var hiddenProdutoId = (HiddenField)produto.FindControl("hiddenProdutoId");
            var txtNCM = (TextBox)produto.FindControl("txtNCM");
            int produtoId = Convert.ToInt32(hiddenProdutoId.Value);
            var produtoItem = (from c in produtodc.tbProdutos where c.produtoId == produtoId select c).First();
            produtoItem.ncm = txtNCM.Text;
        }
        produtodc.SubmitChanges();

        rnNotaFiscal.gerarNotaFiscalEmLote(listaPedidosIgbe);

        string lote = System.Configuration.ConfigurationManager.AppSettings["caminhoFisico"] + "notas\\lote_" + Guid.NewGuid() + ".zip";


        Response.Clear();
        Response.ContentType = "application/zip";
        Response.AddHeader("Content-Disposition", string.Format("attachment; filename=\"{0}\"", "notasFiscais.zip"));
        Response.BufferOutput = false;

        byte[] buffer = new byte[1024 * 8];

        using (ICSharpCode.SharpZipLib.Zip.ZipOutputStream zipOutput = new ICSharpCode.SharpZipLib.Zip.ZipOutputStream(Response.OutputStream))
        {

            foreach (var pedido in listaPedidosIgbe)
            {
                ICSharpCode.SharpZipLib.Zip.ZipEntry zipEntry = new ICSharpCode.SharpZipLib.Zip.ZipEntry(pedido.pedidoId + ".xml");
                zipOutput.PutNextEntry(zipEntry);
                using (var fread = System.IO.File.OpenRead(System.Configuration.ConfigurationManager.AppSettings["caminhoFisico"] + "\\admin\\notas\\" + pedido.pedidoId + ".xml"))
                {
                    ICSharpCode.SharpZipLib.Core.StreamUtils.Copy(fread, zipOutput, buffer);
                }
            }
            zipOutput.Finish();
        }

        Response.Flush();
        Response.End();
    }


    protected void btnEnviarPDFNotas_OnClick(object sender, EventArgs e)
    {
        Response.Redirect("enviarDanfeNotas.aspx");
    }
}