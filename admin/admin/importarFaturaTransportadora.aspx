﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="importarFaturaTransportadora.aspx.cs" Inherits="admin_importarFaturaTransportadora" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" Runat="Server">
    <table cellpadding="0" cellspacing="0" style="width: 920px">
        <tr>
            <td class="tituloPaginas" valign="top">
                <asp:Label ID="lblAcao" runat="server">Importar Planilha de Transportadora</asp:Label>
            </td>
        </tr>
        <tr>
            <td style="padding-left: 10px">
                <asp:Label ID="lblMensagem" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
                    <tr>
                        <td class="rotulos">Planilha:<br />
                            <asp:FileUpload runat="server" CssClass="fileUpload" ID="fupPlanilha" /><br />
                        </td>
                    </tr>
                    <tr>
                        <td class="rotulos" style="padding-top: 15px;">
                            <table width="100%">
                                <tr>
                                    <td><b>Transportadora</b></td>
                                    <td><b>Tabela</b></td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:DropDownList runat="server" ID="ddlTransportadora" DataValueField="idTransportadora" OnSelectedIndexChanged="ddlTransportadora_SelectedIndexChanged" DataTextField="transportadora" AutoPostBack="True"></asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="rotulos" style="padding-top: 15px;">
                            <asp:Button runat="server" ID="btnImportar" CssClass="btnexportar" Text="Importar" OnClick="btnImportar_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right;">&nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>

