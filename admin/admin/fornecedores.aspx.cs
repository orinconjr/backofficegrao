﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxTabControl;
using DevExpress.Web.ASPxEditors;
using DevExpress.Data;
using DevExpress.Web.ASPxUploadControl;
using System.IO;
using System.Data.SqlClient;
using DevExpress.Web.Data;
using System.Collections.Generic;

public partial class admin_fornecedores : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    //protected void btPdf_Click(object sender, ImageClickEventArgs e)
    //{
    //    this.grdEx.WritePdfToResponse();
    //}
    protected void btXsl_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteXlsToResponse();
    }
    //protected void btRtf_Click(object sender, ImageClickEventArgs e)
    //{
    //    this.grdEx.WriteRtfToResponse();
    //}
    //protected void btCsv_Click(object sender, ImageClickEventArgs e)
    //{
    //    this.grdEx.WriteCsvToResponse();
    //}

    protected void grd_RowDeleted(object sender, DevExpress.Web.Data.ASPxDataDeletedEventArgs e)
    {
        if (e.Exception != null)
        {
            SqlException ex = (SqlException)e.Exception;

            if (ex.Number == 547)
            {
                throw new InvalidOperationException("Esse fornecedor não pode ser excluido por que existe produtos relacionado a ele.");
                e.ExceptionHandled = true;
            }
        }
    }

    protected void sqlFornecedor_Updating(object sender, LinqDataSourceUpdateEventArgs e)
    {
        grd.BeginUpdate();
        tbProdutoFornecedor especificacao = (tbProdutoFornecedor)e.NewObject;
        ASPxCheckBox chkPedidoDiasUteis = (ASPxCheckBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["prazoFornecedorDiasUteis"], "chkPedidoDiasUteis");
        ASPxCheckBox chkGerarPedido = (ASPxCheckBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["gerarPedido"], "chkGerarPedido");
        ASPxCheckBox chkPedidoSegunda = (ASPxCheckBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["pedidoSegunda"], "chkPedidoSegunda");
        ASPxCheckBox chkPedidoTerca = (ASPxCheckBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["pedidoTerca"], "chkPedidoTerca");
        ASPxCheckBox chkPedidoQuarta = (ASPxCheckBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["pedidoQuarta"], "chkPedidoQuarta");
        ASPxCheckBox chkPedidoQuinta = (ASPxCheckBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["pedidoQuinta"], "chkPedidoQuinta");
        ASPxCheckBox chkPedidoSexta = (ASPxCheckBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["pedidoSexta"], "chkPedidoSexta");
        ASPxCheckBox chkPedidoSabado = (ASPxCheckBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["pedidoSabado"], "chkPedidoSabado");
        ASPxCheckBox chkPedidoDomingo = (ASPxCheckBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["pedidoDomingo"], "chkPedidoDomingo");
        ASPxCheckBox chkReposicaoAutomaticaEstoque = (ASPxCheckBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["reposicaoAutomaticaEstoque"], "chkReposicaoAutomaticaEstoque");
        ASPxCheckBox chkEntrega = (ASPxCheckBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["entrega"], "chkEntrega");
        ASPxCheckBox chkStoqueConferido = (ASPxCheckBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["estoqueConferido"], "chkEstoqueConferido");

        TextBox txtDataInicioFabricacao = (TextBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["dataInicioFabricacao"], "txtDataInicioFabricacao");
        TextBox txtDataFimFabricacao = (TextBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["dataFimFabricacao"], "txtDataFimFabricacao");
        TextBox txtDataUltimoRomaneio = (TextBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["dataUltimoRomaneio"], "txtDataUltimoRomaneio");

        DropDownList ddlCentroDistribuicao = (DropDownList)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["CentroDeDistribuicao"], "ddlCentroDeDistribuicao");

        var centroDistribuicao = ddlCentroDistribuicao.SelectedValue;

        if (centroDistribuicao == null) centroDistribuicao = "0";

        especificacao.prazoFornecedorDiasUteis = chkPedidoDiasUteis.Checked;
        especificacao.gerarPedido = chkGerarPedido.Checked;
        especificacao.pedidoSegunda = chkPedidoSegunda.Checked;
        especificacao.pedidoTerca = chkPedidoTerca.Checked;
        especificacao.pedidoQuarta = chkPedidoQuarta.Checked;
        especificacao.pedidoQuinta = chkPedidoQuinta.Checked;
        especificacao.pedidoSexta = chkPedidoSexta.Checked;
        especificacao.pedidoSabado = chkPedidoSabado.Checked;
        especificacao.pedidoDomingo = chkPedidoDomingo.Checked;
        especificacao.reposicaoAutomaticaEstoque = chkReposicaoAutomaticaEstoque.Checked;
        especificacao.entrega = chkEntrega.Checked;
        especificacao.estoqueConferido = chkStoqueConferido.Checked;
        especificacao.idCentroDistribuicao = Convert.ToInt32(centroDistribuicao);
        if (txtDataInicioFabricacao.Text != "" && txtDataFimFabricacao.Text != "" && txtDataUltimoRomaneio.Text != "")
        {
            try
            {
                especificacao.dataInicioFabricacao = Convert.ToDateTime(txtDataInicioFabricacao.Text);
                especificacao.dataFimFabricacao = Convert.ToDateTime(txtDataFimFabricacao.Text);
                especificacao.dataUltimoRomaneio = Convert.ToDateTime(txtDataUltimoRomaneio.Text).AddHours(10);
            }
            catch (Exception ex)
            {
                especificacao.dataInicioFabricacao = null;
                especificacao.dataFimFabricacao = null;
                especificacao.dataUltimoRomaneio = null;

            }
        }
        else
        {
            especificacao.dataInicioFabricacao = null;
            especificacao.dataFimFabricacao = null;
            especificacao.dataUltimoRomaneio = null;
        }

        grd.EndUpdate();
        grd.FilterExpression = string.Empty;
    }

    protected void ddlCentroDeDistribuicao_Init(object sender, EventArgs e)
    {
        DropDownList ddl = sender as DropDownList;
        var data = new dbCommerceDataContext();

        List<tbCentroDistribuicao> list = new List<tbCentroDistribuicao>()
        {
            new tbCentroDistribuicao() { idCentroDistribuicao = 0, nome = "Selecione" },
            new tbCentroDistribuicao() { idCentroDistribuicao = 4, nome = "Enxoval CD4" },
            new tbCentroDistribuicao() { idCentroDistribuicao = 5, nome = "Móveis" },
        };

        ddl.DataSource = list;
        ddl.DataTextField = "nome";
        ddl.DataValueField = "idCentroDistribuicao";
        ddl.DataBind();
    }

    protected void grd_HtmlRowCreated1(object sender, ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType != GridViewRowType.EditForm) return;
        {
            var idCentroDistribuicao = grd.GetRowValues(0, "idCentroDistribuicao");
            if (idCentroDistribuicao == null) idCentroDistribuicao = "0";

            DropDownList ddlD = (DropDownList)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["CentroDeDistribuicao"], "ddlCentroDeDistribuicao");
            ddlD.SelectedValue = idCentroDistribuicao.ToString();

        }
    }

    protected void sqlFornecedor_Inserting(object sender, LinqDataSourceInsertEventArgs e)
    {
        grd.BeginUpdate();
        tbProdutoFornecedor especificacao = (tbProdutoFornecedor)e.NewObject;
        ASPxCheckBox chkPedidoDiasUteis = (ASPxCheckBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["prazoFornecedorDiasUteis"], "chkPedidoDiasUteis");
        ASPxCheckBox chkGerarPedido = (ASPxCheckBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["gerarPedido"], "chkGerarPedido");
        ASPxCheckBox chkPedidoSegunda = (ASPxCheckBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["pedidoSegunda"], "chkPedidoSegunda");
        ASPxCheckBox chkPedidoTerca = (ASPxCheckBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["pedidoTerca"], "chkPedidoTerca");
        ASPxCheckBox chkPedidoQuarta = (ASPxCheckBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["pedidoQuarta"], "chkPedidoQuarta");
        ASPxCheckBox chkPedidoQuinta = (ASPxCheckBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["pedidoQuinta"], "chkPedidoQuinta");
        ASPxCheckBox chkPedidoSexta = (ASPxCheckBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["pedidoSexta"], "chkPedidoSexta");
        ASPxCheckBox chkPedidoSabado = (ASPxCheckBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["pedidoSabado"], "chkPedidoSabado");
        ASPxCheckBox chkPedidoDomingo = (ASPxCheckBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["pedidoDomingo"], "chkPedidoDomingo");
        ASPxCheckBox chkReposicaoAutomaticaEstoque = (ASPxCheckBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["reposicaoAutomaticaEstoque"], "chkReposicaoAutomaticaEstoque");
        ASPxCheckBox chkEntrega = (ASPxCheckBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["entrega"], "chkEntrega");
        ASPxCheckBox chkStoqueConferido = (ASPxCheckBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["estoqueConferido"], "chkEstoqueConferido");

        TextBox txtDataInicioFabricacao = (TextBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["dataInicioFabricacao"], "txtDataInicioFabricacao");
        TextBox txtDataFimFabricacao = (TextBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["dataFimFabricacao"], "txtDataFimFabricacao");
        TextBox txtDataUltimoRomaneio = (TextBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["dataUltimoRomaneio"], "txtDataUltimoRomaneio");

        DropDownList ddlCentroDistribuicao = (DropDownList)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["CentroDeDistribuicao"], "ddlCentroDeDistribuicao");

        var centroDistribuicao = ddlCentroDistribuicao.SelectedValue;

        if (centroDistribuicao == null) centroDistribuicao = "0";

        especificacao.prazoFornecedorDiasUteis = chkPedidoDiasUteis.Checked;
        especificacao.gerarPedido = chkGerarPedido.Checked;
        especificacao.pedidoSegunda = chkPedidoSegunda.Checked;
        especificacao.pedidoTerca = chkPedidoTerca.Checked;
        especificacao.pedidoQuarta = chkPedidoQuarta.Checked;
        especificacao.pedidoQuinta = chkPedidoQuinta.Checked;
        especificacao.pedidoSexta = chkPedidoSexta.Checked;
        especificacao.pedidoSabado = chkPedidoSabado.Checked;
        especificacao.pedidoDomingo = chkPedidoDomingo.Checked;
        especificacao.reposicaoAutomaticaEstoque = chkReposicaoAutomaticaEstoque.Checked;
        especificacao.entrega = chkEntrega.Checked;
        especificacao.estoqueConferido = chkStoqueConferido.Checked;
        especificacao.idCentroDistribuicao = Convert.ToInt32(centroDistribuicao);
        if (txtDataInicioFabricacao.Text != "" && txtDataFimFabricacao.Text != "" && txtDataUltimoRomaneio.Text != "")
        {
            try
            {
                especificacao.dataInicioFabricacao = Convert.ToDateTime(txtDataInicioFabricacao.Text);
                especificacao.dataFimFabricacao = Convert.ToDateTime(txtDataFimFabricacao.Text);
                especificacao.dataUltimoRomaneio = Convert.ToDateTime(txtDataUltimoRomaneio.Text).AddHours(10);
            }
            catch (Exception ex)
            {
                especificacao.dataInicioFabricacao = null;
                especificacao.dataFimFabricacao = null;
                especificacao.dataUltimoRomaneio = null;

            }
        }
        else
        {
            especificacao.dataInicioFabricacao = null;
            especificacao.dataFimFabricacao = null;
            especificacao.dataUltimoRomaneio = null;
        }

        grd.EndUpdate();
        grd.FilterExpression = string.Empty;
    }
}
