﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="usuarioExpedicao.aspx.cs" Inherits="admin_usuarioExpedicao" Theme="Glass" MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="DevExpress.Web.v11.1" Namespace="DevExpress.Web.ASPxHiddenField" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v11.1" Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxPopupControl" Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>


<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" runat="Server">
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script type="text/javascript">
        function Pop(s, e) {
            if (e.buttonID == 'btnControleAcesso') {
                var usuarioID = grd.GetRowKey(e.visibleIndex);
                grd.GetRowValues(e.visibleIndex, 'idUsuarioExpedicao;nome', fnCallBack);
                popControleAcesso.PerformCallback(usuarioID);
                popControleAcesso.Show();

            }
        }

        function fnCallBack(retorno) {
            var id = retorno[0];
            var nome = retorno[1];
            console.log(id, nome);
            lblNomeUsuario.SetText(nome);
            hdfUsuarioID.SetText(id);
        }



    </script>
    <asp:ScriptManager ID="scpManager" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="uptPanel" runat="server">
        <ContentTemplate>
            <dx:ASPxPopupControl ID="popControleAcesso" runat="server" CloseAction="CloseButton" Modal="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
                ClientInstanceName="popControleAcesso" HeaderText="Controle de Acesso" AllowDragging="false" PopupAnimationType="None" EnableViewState="True" Height="500"
                AutoUpdatePosition="True" OnWindowCallback="popControleAcesso_WindowCallback">
                <ContentCollection>
                    <dx:PopupControlContentControl ID="PopupControlContentControl" runat="server">
                        <dx:ASPxLabel ID="lblIdUsuario" runat="server" Visible="false" ClientInstanceName="lblIdUsuario"></dx:ASPxLabel>
                        <div style="width: 800px; height: 600px;" align="center">
                            <table width="100%;" align="center">
                                <tr bgcolor="#D4E3E6">
                                    <td>
                                        <dx:ASPxHiddenField ID="hdfUsuarioID" runat="server" ClientInstanceName="hdfUsuarioID"></dx:ASPxHiddenField>
                                        <table style="width: 100%;" class="rotulos">
                                            <tr>
                                                <td><b>Usuário:</b><br />
                                                    <dx:ASPxLabel runat="server" ID="lblNomeUsuario" ClientInstanceName="lblNomeUsuario" />
                                                </td>
                                                <td><b>Tipo Permissão:</b><br />
                                                    <dx:ASPxComboBox ID="cboTipoPermissao" AutoPostBack="true" runat="server" ClientInstanceName="cboTipoPermissao" OnSelectedIndexChanged="cboTipoPermissao_SelectedIndexChanged"></dx:ASPxComboBox>
                                                </td>
                                                <td id="tdTipoEntrega" runat="server">
                                                    <b>Tipo de Entrega:</b><br />
                                                    <dx:ASPxComboBox ID="cboTipoDeEntrega" runat="server" ClientInstanceName="cboTipoDeEntrega"></dx:ASPxComboBox>
                                                </td>
                                                <td id="tdIdRelacionado" runat="server">
                                                    <b>Id Relacionado:</b><br />
                                                    <dx:ASPxTextBox ID="txtIdRelacionado" runat="server" ClientInstanceName="txtIdRelacionado"></dx:ASPxTextBox>
                                                </td>
                                                <td id="tdFiltro" runat="server">
                                                    <b>Filtro:</b><br />
                                                    <dx:ASPxTextBox ID="txtFiltro" runat="server" ClientInstanceName="txtFiltro"></dx:ASPxTextBox>
                                                </td>
                                                <td style="width: 50px;">
                                                    <br />
                                                    <dx:ASPxButton ID="btnGravarPermissao" Text="Incluir" runat="server" ClientInstanceName="btnGravarPermissao" OnClick="btnGravarPermissao_Click"></dx:ASPxButton>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="rotulos" colspan="2" style="text-align: right">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div style="overflow: scroll; height: 500px;" align="center">
                                            <asp:HiddenField ID="hdnUsuarioID" runat="server" />
                                            <asp:Label ID="lblMensagem" runat="server" Visible="false"></asp:Label>

                                            <asp:DataList ID="dtlPermissoes" runat="server" CellPadding="0" DataKeyField="idUsuarioExpedicaoPermissao" OnItemCommand="dtlPermissoes_ItemCommand">
                                                <HeaderTemplate>
                                                    <table cellpadding="0" cellspacing="0" class="rotulos" border="0" style="width: 100%;">
                                                        <tr bgcolor="#D4E3E6">
                                                            <td height="25" style="width: 240px; padding-left: 10px;">
                                                                <b>Tipo de Permissão:</b>
                                                            </td>
                                                            <td height="25" style="width: 220px; padding-left: 10px;">
                                                                <b>Id Relacionado</b>
                                                            </td>
                                                            <td style="width: 200px; padding-left: 10px;">
                                                                <b>Filtro:</b>
                                                            </td>
                                                            <td height="25" style="text-align: center; width: 30px;">&nbsp;
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <table cellpadding="0" cellspacing="0" style="width: 100%;" align="center">
                                                        <tr>
                                                            <td height="25" style="padding-left: 10px; width: 240px;">
                                                                <asp:Label ID="lblTipoPermissao" runat="server" Text='<%# Bind("nomeExpedicaoTipoPermissao") %>'></asp:Label>
                                                            </td>
                                                            <td style="width: 220px; padding-left: 10px;">
                                                                <asp:Label ID="lblIdRelacionado" runat="server" Text='<%# Bind("Id_Relacionado") %>'></asp:Label>
                                                            </td>
                                                            <td style="width: 200px; padding-left: 10px;">
                                                                <asp:Label ID="lblFiltro" runat="server" Text='<%# Bind("filtro") %>'></asp:Label>
                                                            </td>
                                                            <td height="25" style="text-align: center; width: 30px;">
                                                                <dx:ASPxButton ID="btnExcluir" runat="server"  OnCommand="btnExcluir_Command" AutoPostBack="true" CommandName="ExcluirControleAcesso" CommandArgument='<%# Eval("idUsuarioExpedicaoPermissao") %>' Image-Url="~/admin/images/btExcluir.jpg"></dx:ASPxButton>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ItemTemplate>
                                                
                                            </asp:DataList>
                                        </div>

                                    </td>
                                </tr>
                            </table>
                        </div>
                    </dx:PopupControlContentControl>
                </ContentCollection>
                <ContentStyle>
                    <Paddings PaddingBottom="5px" />
                </ContentStyle>
            </dx:ASPxPopupControl>
        </ContentTemplate>
    </asp:UpdatePanel>

    <table cellpadding="0" cellspacing="0" style="width: 920px">
        <tr>
            <td class="tituloPaginas" valign="top">
                <asp:Label ID="lblAcao" runat="server">Usuários Expedição</asp:Label>
            </td>
        </tr>
        <tr>
            <td>

                <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
                    <tr>
                        <td align="right">
                            <table cellpadding="0" cellspacing="0" style="width: 834px">
                                <tr>
                                    <td align="right">&nbsp;</td>
                                    <td height="38"
                                        style="background-image: url('images/bkExportar.jpg'); padding-bottom: 1px;"
                                        valign="bottom" width="231">
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 97px">&nbsp;</td>
                                                <td style="width: 33px">
                                                    <asp:ImageButton ID="btPdf" runat="server" ImageUrl="~/admin/images/btPdf.jpg"
                                                        Style="margin-left: 5px" />
                                                </td>
                                                <td style="width: 31px">
                                                    <asp:ImageButton ID="btXsl" runat="server" ImageUrl="~/admin/images/btXls.jpg"
                                                        Style="margin-left: 5px" />
                                                </td>
                                                <td style="width: 36px">
                                                    <asp:ImageButton ID="btRtf" runat="server" ImageUrl="~/admin/images/btRtf.jpg"
                                                        Style="margin-left: 5px" />
                                                </td>
                                                <td valign="bottom">
                                                    <asp:ImageButton ID="btCsv" runat="server" ImageUrl="~/admin/images/btCsv.jpg"
                                                        Style="margin-left: 5px" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <dxwgv:ASPxGridView ID="grd" ClientInstanceName="grd" runat="server" AutoGenerateColumns="False"
                                DataSourceID="sqlUsuariosExpedicao" KeyFieldName="idUsuarioExpedicao" Width="834px"
                                Cursor="auto" EnableCallBacks="true">
                                <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                                <SettingsText ConfirmDelete="Tem certeza que deseja excluir?"
                                    EmptyDataRow="Nenhum registro encontrado." />
                                <SettingsPager Position="TopAndBottom" PageSize="50"
                                    ShowDisabledButtons="False" AlwaysShowPager="True">
                                    <Summary AllPagesText="Páginas: {0} - {1} ({2} items)"
                                        Text="Página {0} de {1} ({2} registros encontrados)" />
                                </SettingsPager>
                                <Settings ShowFilterRow="True" ShowGroupButtons="False" />
                                <SettingsEditing EditFormColumnCount="4"
                                    PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1"
                                    PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter"
                                    PopupEditFormWidth="700px" />
                                <ClientSideEvents CustomButtonClick="Pop" />
                                <Columns>
                                    <dxwgv:GridViewDataTextColumn Caption="ID do usuário" Width="100px"
                                        FieldName="idUsuarioExpedicao" ReadOnly="True" VisibleIndex="0">
                                        <Settings AutoFilterCondition="Contains" />
                                        <EditFormSettings Visible="False" CaptionLocation="Top" />
                                    </dxwgv:GridViewDataTextColumn>

                                    <dxwgv:GridViewDataTextColumn Caption="Nome" FieldName="nome"
                                        VisibleIndex="1">
                                        <PropertiesTextEdit>
                                            <ValidationSettings ErrorText="" SetFocusOnError="True">
                                                <RequiredField ErrorText="Preencha o campo nome." IsRequired="True" />
                                            </ValidationSettings>
                                        </PropertiesTextEdit>
                                        <Settings AutoFilterCondition="Contains" />
                                        <EditFormSettings CaptionLocation="Top" Visible="True" />
                                    </dxwgv:GridViewDataTextColumn>

                                    <dxwgv:GridViewDataTextColumn VisibleIndex="2" Visible="False" Caption="Senha" Width="250px" FieldName="senha">
                                        <PropertiesTextEdit>
                                            <ValidationSettings ErrorText="" SetFocusOnError="True">
                                                <RequiredField ErrorText="Preencha o campo senha." IsRequired="True" />
                                            </ValidationSettings>
                                        </PropertiesTextEdit>
                                        <EditFormSettings CaptionLocation="Top" Visible="True" />
                                    </dxwgv:GridViewDataTextColumn>

                                    <dxwgv:GridViewDataComboBoxColumn Caption="Centro de Distribuição" FieldName="idCentroDistribuicao"
                                        Visible="True" VisibleIndex="3">
                                        <PropertiesComboBox DataSourceID="sqlCentroDistribuicao" TextField="nome"
                                            ValueField="idCentroDistribuicao" ValueType="System.String">
                                            <ValidationSettings ErrorText="" SetFocusOnError="True">
                                                <RequiredField ErrorText="Selecione o Centro de Distribuição." IsRequired="True" />
                                            </ValidationSettings>
                                        </PropertiesComboBox>
                                        <Settings AutoFilterCondition="Contains" />
                                        <EditFormSettings CaptionLocation="Top" Visible="True" />
                                    </dxwgv:GridViewDataComboBoxColumn>

                                    <dxwgv:GridViewDataCheckColumn Visible="False" Caption="Separação" FieldName="separacao">
                                        <EditFormSettings CaptionLocation="Top" Visible="True" />
                                    </dxwgv:GridViewDataCheckColumn>
                                    <dxwgv:GridViewCommandColumn Width="120px" ButtonType="Image">
                                        <EditButton Visible="True" Text="Editar">
                                            <Image Url="~/admin/images/btEditar.jpg" />
                                        </EditButton>
                                        <NewButton Visible="True" Text="Novo">
                                            <Image Url="~/admin/images/btNovo.jpg" />
                                        </NewButton>
                                        <CancelButton Text="Cancelar">
                                            <Image Url="~/admin/images/btCancelar.jpg" />
                                        </CancelButton>
                                        <CustomButtons>
                                            <dxwgv:GridViewCommandColumnCustomButton ID="btnControleAcesso" Image-AlternateText="Controle de acesso"
                                                Text="Controle de Acesso" Image-Url="images/btnEngrenagens.jpg">
                                            </dxwgv:GridViewCommandColumnCustomButton>
                                        </CustomButtons>
                                        <UpdateButton Text="Salvar">
                                            <Image Url="~/admin/images/btSalvarPeq.jpg" />
                                        </UpdateButton>
                                        <ClearFilterButton Visible="True" Text="Limpar filtro">
                                            <Image Url="~/admin/images/btLimparFiltro.jpg" />
                                        </ClearFilterButton>
                                        <HeaderTemplate>
                                            <img alt="" src="images/legendaIconesacesso.jpg" />
                                        </HeaderTemplate>
                                    </dxwgv:GridViewCommandColumn>
                                </Columns>
                                <StylesEditors>
                                    <Label Font-Bold="True">
                                    </Label>
                                </StylesEditors>
                            </dxwgv:ASPxGridView>

                            <asp:SqlDataSource runat="server" ID="sqlUsuariosExpedicao" ConnectionString="<%$ ConnectionStrings:connectionString %>"
                                SelectCommand="SELECT idUsuarioExpedicao, senha, nome, separacao, idCentroDistribuicao FROM tbUsuarioExpedicao"
                                InsertCommand="insert into tbUsuarioExpedicao ( senha, nome, separacao, idCentroDistribuicao) values ( @senha, @nome, @separacao, @idCentroDistribuicao)"
                                UpdateCommand="UPDATE tbUsuarioExpedicao SET nome=@nome, senha=@senha, separacao=@separacao,idCentroDistribuicao=@idCentroDistribuicao WHERE idUsuarioExpedicao=@idUsuarioExpedicao"
                                OnUpdating="sqlUsuariosExpedicao_OnUpdating"></asp:SqlDataSource>

                            <asp:SqlDataSource runat="server" ID="sqlCentroDistribuicao" ConnectionString="<%$ ConnectionStrings:connectionString %>"
                                SelectCommand="SELECT distinct idCentroDistribuicao,nome FROM tbCentroDistribuicao"></asp:SqlDataSource>

                            <asp:SqlDataSource runat="server" ID="sqlValidarSenha" ConnectionString="<%$ ConnectionStrings:connectionString %>"></asp:SqlDataSource>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

</asp:Content>

