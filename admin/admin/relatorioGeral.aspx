﻿<%@ Page Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="relatorioGeral.aspx.cs" Inherits="admin_relatorioGeral" Theme="Glass" MaintainScrollPositionOnPostback=true %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dxwgv" %>
<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" Runat="Server">
    <table cellpadding="0" cellspacing="0" style="width: 920px">
    <tr>
        <td class="tituloPaginas" valign="top">
            <asp:Label ID="lblAcao" runat="server">Relatório geral</asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            
    <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
        <tr>
            <td align="right">
                <table cellpadding="0" cellspacing="0" style="width: 834px">
                    <tr>
                        <td align="right">
                            &nbsp;</td>
                        <td height="38" 
                            style="background-image: url('images/bkExportar.jpg'); padding-bottom: 1px;" 
                            valign="bottom" width="231">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="width: 97px">
                                        &nbsp;</td>
                                    <td style="width: 33px">
                                        <asp:ImageButton ID="btPdf" runat="server" ImageUrl="~/admin/images/btPdf.jpg" 
                                            style="margin-left: 5px" />
                                    </td>
                                    <td style="width: 31px">
                                        <asp:ImageButton ID="btXsl" runat="server" ImageUrl="~/admin/images/btXls.jpg" 
                                            style="margin-left: 5px" />
                                    </td>
                                    <td style="width: 36px">
                                        <asp:ImageButton ID="btRtf" runat="server" ImageUrl="~/admin/images/btRtf.jpg" 
                                            style="margin-left: 5px" />
                                    </td>
                                    <td valign="bottom">
                                        <asp:ImageButton ID="btCsv" runat="server" ImageUrl="~/admin/images/btCsv.jpg" 
                                            style="margin-left: 5px" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                <table cellpadding="0" cellspacing="0" style="width: 834px">
                    <tr>
                        <td class="rotulos" style="width: 100px">
                            Data inicial<br __designer:mapid="3fc" />
                                        <asp:TextBox ID="txtDataInicial" runat="server" CssClass="campos" 
                                                            Text='<%# Bind("faixaDeCepPesoInicial") %>' 
                                                            Width="90px" MaxLength="10"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rqvDataInicial" runat="server" 
                                                            ControlToValidate="txtDataInicial" Display="None" 
                                                            ErrorMessage="Preencha a data inicial." 
                                SetFocusOnError="True"></asp:RequiredFieldValidator>
                                    <b __designer:mapid="27df">
                            <asp:RegularExpressionValidator ID="rgeDataInicial" runat="server" 
                                            ControlToValidate="txtDataInicial" Display="None" 
                                            ErrorMessage="Por favor, preencha corretamente a data inicial." 
                                            SetFocusOnError="True" 
                                            
                                            
                                ValidationExpression="^(([0-2]\d|[3][0-1])\/([0]\d|[1][0-2])\/[1-2][0-9]\d{2})$"></asp:RegularExpressionValidator>
                            </b>
                        </td>
                        <td class="rotulos" style="width: 100px">
                            Data final<br __designer:mapid="401" />
                            <asp:TextBox ID="txtDataFinal" runat="server" CssClass="campos" 
                                                Text='<%# Bind("faixaDeCepPesoFinal") %>' ValidationGroup="grpInsert" 
                                                Width="90px" MaxLength="10"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rqvDataFinal" runat="server" 
                                                ControlToValidate="txtDataFinal" Display="None" 
                                                ErrorMessage="Preencha a data final." 
                                SetFocusOnError="True"></asp:RequiredFieldValidator>
                            <b __designer:mapid="27df">
                            <asp:RegularExpressionValidator ID="rgeDataInicial0" runat="server" 
                                            ControlToValidate="txtDataFinal" Display="None" 
                                            ErrorMessage="Por favor, preencha corretamente a data final" 
                                            SetFocusOnError="True" 
                                            
                                            
                                ValidationExpression="^(([0-2]\d|[3][0-1])\/([0]\d|[1][0-2])\/[1-2][0-9]\d{2})$"></asp:RegularExpressionValidator>
                            </b>
                        </td>
                        <td valign="bottom">
                            <asp:ImageButton ID="imbInsert" runat="server" 
                                                ImageUrl="~/admin/images/btPesquisar.jpg" />
                            <asp:ValidationSummary ID="llds" runat="server" DisplayMode="List" 
                                ShowMessageBox="True" ShowSummary="False" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                <dxwgv:aspxgridview ID="grd" runat="server" AutoGenerateColumns="False" 
                    KeyFieldName="tipoDeEntregaId" Width="834px" 
                    Cursor="auto" DataSourceID="sqlRelatorioGeral">
                    <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                    <Styles>
                        <Footer Font-Bold="True">
                        </Footer>
                    </Styles>
                    <SettingsText ConfirmDelete="Tem certeza que deseja excluir?" 
                        emptydatarow="Nenhum registro encontrado." />
                    <SettingsPager Position="TopAndBottom" PageSize="50" 
                        ShowDisabledButtons="False" AlwaysShowPager="True">
                        <Summary AllPagesText="Páginas: {0} - {1} ({2} items)" 
                            Text="Página {0} de {1} ({2} registros encontrados)" />
                    </SettingsPager>
                    <settings showfilterrow="True" ShowGroupButtons="False" ShowFooter="True" />
                    <SettingsEditing EditFormColumnCount="4" 
                        PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1" 
                        PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter" 
                        PopupEditFormWidth="700px" Mode="Inline" />
                    <Columns>
                        <dxwgv:GridViewDataTextColumn Caption="Id do ped." FieldName="pedidoId" 
                            VisibleIndex="0">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataComboBoxColumn Caption="Cidade" FieldName="clienteCidade" 
                            VisibleIndex="1">
                            <PropertiesComboBox DataSourceID="sqlCidade" TextField="clienteCidade" 
                                ValueField="clienteCidade" ValueType="System.String">
                            </PropertiesComboBox>
                        </dxwgv:GridViewDataComboBoxColumn>
                        <dxwgv:GridViewDataComboBoxColumn FieldName="clienteEstado" VisibleIndex="2">
                            <PropertiesComboBox DataSourceID="sqlEstado" TextField="clienteEstado" 
                                ValueField="clienteEstado" ValueType="System.String">
                            </PropertiesComboBox>
                        </dxwgv:GridViewDataComboBoxColumn>
                        <dxwgv:GridViewDataComboBoxColumn Caption="Cond. de pagamento" 
                            FieldName="condicaoNome" VisibleIndex="3">
                            <PropertiesComboBox DataSourceID="sqlCondDePagamento" TextField="condicaoNome" 
                                ValueField="condicaoNome" ValueType="System.String">
                            </PropertiesComboBox>
                        </dxwgv:GridViewDataComboBoxColumn>
                        <dxwgv:GridViewDataComboBoxColumn Caption="Tipo de entrega" 
                            FieldName="tipoDeEntregaNome" VisibleIndex="4">
                            <PropertiesComboBox DataSourceID="sqlTipoDeEntrega" 
                                TextField="tipoDeEntregaNome" ValueField="tipoDeEntregaNome" 
                                ValueType="System.String">
                            </PropertiesComboBox>
                        </dxwgv:GridViewDataComboBoxColumn>
                    </Columns>
                    <StylesEditors>
                        <Label Font-Bold="True">
                        </Label>
                    </StylesEditors>
                </dxwgv:aspxgridview>
<%--                <dxwgv:aspxgridviewexporter ID="grdEx" runat="server" FileName="fornecedores" 
                    GridViewID="grd" Landscape="True" PreserveGroupRowStates="True">
                </dxwgv:aspxgridviewexporter>--%>
                <asp:ObjectDataSource ID="sqlRelatorioGeral" runat="server" 
                    SelectMethod="relatorioGeral" TypeName="rnRelatorios">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="txtDataInicial" Name="dataInicial" 
                            PropertyName="Text" Type="String" />
                        <asp:ControlParameter ControlID="txtDataFinal" Name="dataFinal" 
                            PropertyName="Text" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="sqlCidade" runat="server" 
                    SelectMethod="clienteSelecionaCidade" TypeName="rnClientes">
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="sqlEstado" runat="server" 
                    SelectMethod="clienteSelecionaEstado" TypeName="rnClientes">
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="sqlCondDePagamento" runat="server" 
                    SelectMethod="condicaoDePagamentoSeleciona" TypeName="rnCondicoesDePagamento">
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="sqlTipoDeEntrega" runat="server" 
                    SelectMethod="tipoDeEntregaSeleciona" TypeName="rnTipoDeEntrega">
                </asp:ObjectDataSource>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
    </table>
        </td>
    </tr>
</table>
    </asp:Content>
