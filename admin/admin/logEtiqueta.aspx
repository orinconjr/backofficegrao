﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="logEtiqueta.aspx.cs" Inherits="admin_logEtiqueta" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" Runat="Server">
    <style type="text/css">
            
             .btnexportar {
                 float: right;
             }
            .fileUpload {
                 float: left;
             }
            .fieldsetatualizarprecos {
                width: 760px; float: left; clear: left; border-radius: 5px;margin-left:25px;
            }
            .fieldsetatualizarprecos input[type=checkbox] {
                margin-top: 15px;
            }
            .fieldsetatualizarprecos label {
                margin-left: 5px;margin-right: 20px;
            }
            .atualizarprecosbotoes {
                width: 780px; float: left; clear: left;  height: 64px;  margin-top: 15px;
            }
            .atualizarprecosbotoes label{
                 float: left; margin-top: 5px;
            }
            .meugrid {
            background-color: #F4FAFB;
            border: 0px none;
            /* border-collapse: separate !important; */
            overflow: hidden;
            font: 9pt Tahoma;
            color: #000;
            cursor: pointer;
            white-space: nowrap;
            font-weight: normal;
            text-align: left;
            width: 834px;
            border-collapse: collapse;
            float: left;
            clear: left;
            margin-top: 10px;
            margin-bottom: 10px;
            min-height: 500px;
            margin-left: 25px;
        }

            .meugrid th {
                background-image: url(../../App_Themes/Glass/GridView/gvHeaderBackground.gif);
                background-position: center top;
                background-repeat: repeat-x;
                background-color: #C0DDE0;
                overflow: hidden;
                height: 23px !important;
                line-height: 23px !important;
                padding: 4px 6px 5px;
                font-weight: normal;
                border: 1px solid #7EACB1;
                /* border-collapse: separate;*/
            }

            .meugrid td {
                overflow: hidden;
                height: 23px !important;
                line-height: 23px !important;
                padding: 4px 6px 5px;
                border: 1px solid #7EACB1;
            }

            .meugrid tr td:nth-child(4) {
                text-align: center;
            }

            .meugrid tr td:last-of-type {
                text-align: center;
            }
        </style>
      <div class="tituloPaginas" valign="top">
        Rastreamento de Etiqueta
    </div>
    <div>
        <fieldset class="fieldsetatualizarprecos">
            <legend style="font-weight: bold;">Filtro</legend>

            <table class="tabrotulos">
               
                <tr class="rotulos">
                    <td>Etiqueta:<br />
                        <asp:TextBox runat="server" ID="txtiditempedido"></asp:TextBox>
                    </td>
                    <td>&nbsp;
                    </td>
                    <td>&nbsp;
                    </td>
                    <td>&nbsp;<br />
                        <asp:Button runat="server" ID="btnFiltrar" Text="Filtrar" OnClick="btnFiltrar_Click" />
                       
                    </td>

                </tr>
                <tr class="rotulos">
                    <td colspan="4">
                        <asp:Label ID="lblerrofiltro" runat="server" Text="" Font-Names="tahoma" ForeColor="Red" Font-Bold="True"></asp:Label>
                    </td>

                </tr>

            </table>
        </fieldset>
    </div>
      <div align="center" style="min-height: 500px;">
        

            <asp:GridView ID="GridView1" CssClass="meugrid" runat="server" DataKeyNames="idLogRegistroRelacionado" Width="834px" AutoGenerateColumns="False" AllowPaging="True" OnPageIndexChanging="GridView1_PageIndexChanging"  PageSize="40">
                <Columns>
                    
                    <asp:BoundField DataField="data" HeaderText="Data" />
                    <asp:BoundField DataField="usuario" HeaderText="Usuário" />
                    <asp:BoundField DataField="registro" HeaderText="Registro" />
                    <asp:BoundField DataField="descricao" HeaderText="Descrição" />
                    
                   
                </Columns>
            </asp:GridView>
       
    </div>
</asp:Content>