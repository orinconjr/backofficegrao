﻿<%@ Page Title="" Theme="Glass" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="pedidosEmbaladosPorData.aspx.cs" Inherits="admin_pedidosEmbaladosPorData" %>

<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" runat="Server">
    <script language="javascript" type="text/javascript">
        function ApplyFilter(dde, dateFrom, dateTo) {
            var d1 = dateFrom.GetText();
            var d2 = dateTo.GetText();
            if (d1 == "" || d2 == "")
                return;
            dde.SetText(d1 + "|" + d2);
            //grd.ApplyFilter("[dataHoraDoPedido] >= '" + d1 + " 00:00:00' && [dataHoraDoPedido] <= '" + d2 + " 23:59:59'");
            grd.AutoFilterByColumn("dataSendoEmbalado", dde.GetText());
        }

        function OnDropDown(s, dateFrom, dateTo) {
            var str = s.GetText();
            if (str == "") {
                // default date (1950-1961 for demo)     
                dateFrom.SetDate(new Date(1950, 0, 1));
                dateTo.SetDate(new Date(1960, 11, 31));
                return;
            }
            var d = str.split("|");
            dateFrom.SetText(d[0]);
            dateTo.SetText(d[1]);
        }
    </script>
    <table cellpadding="0" cellspacing="0" style="width: 920px">
        <tr>
            <td class="tituloPaginas" valign="top">
                <asp:Label ID="lblAcao" runat="server">Pedidos</asp:Label>
            </td>
        </tr>
        <tr>
            <td>

                <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="rotulos" style="padding-bottom: 20px;">
                            Centro de Distribuição: 
                            <asp:RadioButton runat="server" ID="rdbCd1" Checked="true" Text="CD 1" GroupName="groupdCds" AutoPostBack="true"  OnCheckedChanged="rdbCd1_CheckedChanged"/>
                            <asp:RadioButton runat="server" ID="rdbCd2" Text="CD 2" GroupName="groupdCds" AutoPostBack="true"  OnCheckedChanged="rdbCd2_CheckedChanged" />
                            <asp:RadioButton runat="server" ID="rdbCd3" Text="CD 3" GroupName="groupdCds" AutoPostBack="true"  OnCheckedChanged="rdbCd3_CheckedChanged" />
                            <asp:RadioButton runat="server" ID="rdbCd4" Text="CD 4" GroupName="groupdCds" AutoPostBack="true"  OnCheckedChanged="rdbCd4_CheckedChanged" />
                            <asp:RadioButton runat="server" ID="rdbCd5" Text="CD 5" GroupName="groupdCds" AutoPostBack="true"  OnCheckedChanged="rdbCd5_CheckedChanged" />
                            <asp:RadioButton runat="server" ID="rdbCd45" Text="CD 4 e 5" GroupName="groupdCds" AutoPostBack="true"  OnCheckedChanged="rdbCd45_CheckedChanged" />
                        </td>
                    </tr>
                    
                    <tr>
                        <td>
                            Início:<asp:TextBox runat="server" ID="txtDataInicio" ValidationGroup="dataFiltroEmbalagem" MaxLength="10" Width="80"></asp:TextBox>
                            Fim:<asp:TextBox runat="server" ID="txtDataFim" ValidationGroup="dataFiltroEmbalagem" MaxLength="10" Width="80"></asp:TextBox>
                            <asp:Button runat="server" ID="btnFiltroEmbalagem" Text="Pesquisar" OnClick="btnFiltroEmbalagem_Click" ValidationGroup="dataFiltroEmbalagem1"/>
                            
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server"
                                ControlToValidate="txtDataInicio" Display="Dynamic" ValidationGroup="dataFiltroEmbalagem1"
                                ErrorMessage="Preencha a data."
                                SetFocusOnError="True"></asp:RequiredFieldValidator>

                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server"
                                ControlToValidate="txtDataInicio" Display="Dynamic"
                                ErrorMessage="Por favor, preencha corretamente a data."
                                SetFocusOnError="True" ValidationGroup="dataFiltroEmbalagem1"
                                ValidationExpression="^(([0-2]\d|[3][0-1])\/([0]\d|[1][0-2])\/[1-2][0-9]\d{2})$"></asp:RegularExpressionValidator>
                            
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server"
                                ControlToValidate="txtDataFim" Display="Dynamic" ValidationGroup="dataFiltroEmbalagem1"
                                ErrorMessage="Preencha a data."
                                SetFocusOnError="True"></asp:RequiredFieldValidator>

                            <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server"
                                ControlToValidate="txtDataFim" Display="Dynamic"
                                ErrorMessage="Por favor, preencha corretamente a data."
                                SetFocusOnError="True" ValidationGroup="dataFiltroEmbalagem1"
                                ValidationExpression="^(([0-2]\d|[3][0-1])\/([0]\d|[1][0-2])\/[1-2][0-9]\d{2})$"></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr class="rotulos">
                        <td style="font-size:18px; padding-top: 20px">
                            Embalagens por Usuário
                        </td>
                    </tr>
                    <tr>
                        <td>                            
                            <dxwgv:ASPxGridView ID="grdEmbaladosData" ClientInstanceName="grdEmbaladosData" runat="server" AutoGenerateColumns="False"
                                KeyFieldName="idPedido" Width="834px" Settings-ShowFilterRow="True"
                                Settings-ShowFilterBar="Visible"
                                Cursor="auto" OnCustomUnboundColumnData="grdEmbaladosData_CustomUnboundColumnData">
                                <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                                <Styles>
                                    <Footer Font-Bold="True">
                                    </Footer>
                                </Styles>
                                <SettingsText ConfirmDelete="Tem certeza que deseja excluir?" EmptyDataRow="Nenhum registro encontrado." />
                                <SettingsPager Position="TopAndBottom" PageSize="50"
                                    ShowDisabledButtons="False" AlwaysShowPager="True">
                                    <Summary AllPagesText="Páginas: {0} - {1} ({2} items)"
                                        Text="Página {0} de {1} ({2} registros encontrados)" />
                                </SettingsPager>
                                <TotalSummary>
                                    <dxwgv:ASPxSummaryItem DisplayFormat="0" FieldName="pedidos" ShowInColumn="Pedidos" ShowInGroupFooterColumn="Pedidos" SummaryType="Sum" />
                                    <dxwgv:ASPxSummaryItem DisplayFormat="0" FieldName="pacotes" ShowInColumn="Pacotes" ShowInGroupFooterColumn="Pacotes" SummaryType="Sum" />
                                </TotalSummary>
                                <Settings ShowFilterRow="True" ShowGroupButtons="False" ShowFooter="True" />
                                <SettingsEditing EditFormColumnCount="4"
                                    PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1"
                                    PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter"
                                    PopupEditFormWidth="700px" />
                                <Columns>
                                    <dxwgv:GridViewDataTextColumn Caption="Embalador" Name="Embalador" FieldName="usuario" Width="50px"
                                        VisibleIndex="3">
                                        <EditFormSettings Visible="False" />
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Pedidos" Name="Pedidos" FieldName="pedidos"
                                        VisibleIndex="3">
                                        <EditFormSettings Visible="False" />
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Pacotes" Name="Pacotes" FieldName="pacotes"
                                        VisibleIndex="3">
                                        <EditFormSettings Visible="False" />
                                    </dxwgv:GridViewDataTextColumn>
                                </Columns>
                                <StylesEditors>
                                    <Label Font-Bold="True">
                                    </Label>
                                </StylesEditors>
                            </dxwgv:ASPxGridView>
                        </td>
                    </tr>
                    <tr class="rotulos">
                        <td style="font-size:18px; padding-top: 20px">
                            Separados por Usuário
                        </td>
                    </tr>
                    <tr>
                        <td>                            
                            <dxwgv:ASPxGridView ID="grdSeparadosPorData" ClientInstanceName="grdSeparadosPorData" runat="server" AutoGenerateColumns="False"
                                KeyFieldName="idPedido" Width="834px" Settings-ShowFilterRow="True"
                                Settings-ShowFilterBar="Visible"
                                Cursor="auto" OnCustomUnboundColumnData="grdEmbaladosData_CustomUnboundColumnData">
                                <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                                <Styles>
                                    <Footer Font-Bold="True">
                                    </Footer>
                                </Styles>
                                <TotalSummary>
                                    <dxwgv:ASPxSummaryItem DisplayFormat="0" FieldName="pedidos" ShowInColumn="Pedidos" ShowInGroupFooterColumn="Pedidos" SummaryType="Sum" />
                                    <dxwgv:ASPxSummaryItem DisplayFormat="0" FieldName="pacotes" ShowInColumn="Pacotes" ShowInGroupFooterColumn="Pacotes" SummaryType="Sum" />
                                </TotalSummary>
                                <SettingsText ConfirmDelete="Tem certeza que deseja excluir?" EmptyDataRow="Nenhum registro encontrado." />
                                <SettingsPager Position="TopAndBottom" PageSize="50"
                                    ShowDisabledButtons="False" AlwaysShowPager="True">
                                    <Summary AllPagesText="Páginas: {0} - {1} ({2} items)"
                                        Text="Página {0} de {1} ({2} registros encontrados)" />
                                </SettingsPager>
                                <Settings ShowFilterRow="True" ShowGroupButtons="False" ShowFooter="True" />
                                <SettingsEditing EditFormColumnCount="4"
                                    PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1"
                                    PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter"
                                    PopupEditFormWidth="700px" />
                                <Columns>
                                    <dxwgv:GridViewDataTextColumn Caption="Embalador" Name="Embalador" FieldName="usuario" Width="50px"
                                        VisibleIndex="3">
                                        <EditFormSettings Visible="False" />
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Pedidos" Name="Pedidos" FieldName="pedidos"
                                        VisibleIndex="3">
                                        <EditFormSettings Visible="False" />
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Pacotes" Name="Pacotes" FieldName="pacotes"
                                        VisibleIndex="3">
                                        <EditFormSettings Visible="False" />
                                    </dxwgv:GridViewDataTextColumn>
                                </Columns>
                                <StylesEditors>
                                    <Label Font-Bold="True">
                                    </Label>
                                </StylesEditors>
                            </dxwgv:ASPxGridView>
                        </td>
                    </tr>
                    <tr class="rotulos">
                        <td style="font-size:18px; padding-top: 20px">
                            Pedidos detalhados
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <dxwgv:ASPxGridView ID="grd" ClientInstanceName="grd" runat="server" AutoGenerateColumns="False"
                                OnAutoFilterCellEditorCreate="grid_AutoFilterCellEditorCreate" OnAutoFilterCellEditorInitialize="grid_AutoFilterCellEditorInitialize"
                                OnProcessColumnAutoFilter="grid_ProcessColumnAutoFilter" KeyFieldName="pedidoId" Width="834px" Settings-ShowFilterRow="True" Settings-ShowFilterBar="Visible"
                                Cursor="auto" OnCustomUnboundColumnData="grd_OnCustomUnboundColumnData">
                                <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                                <Styles>
                                    <Footer Font-Bold="True">
                                    </Footer>
                                </Styles>
                                <SettingsText ConfirmDelete="Tem certeza que deseja excluir?" EmptyDataRow="Nenhum registro encontrado." />
                                <SettingsPager Position="TopAndBottom" PageSize="50"
                                    ShowDisabledButtons="False" AlwaysShowPager="True">
                                    <Summary AllPagesText="Páginas: {0} - {1} ({2} items)"
                                        Text="Página {0} de {1} ({2} registros encontrados)" />
                                </SettingsPager>
                                <Settings ShowFilterRow="True" ShowGroupButtons="False" ShowFooter="True" />
                                <SettingsEditing EditFormColumnCount="4"
                                    PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1"
                                    PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter"
                                    PopupEditFormWidth="700px" />
                                <Columns>
                                    <dxwgv:GridViewDataTextColumn Caption="Id Cliente" FieldName="pedidoIdCliente" VisibleIndex="0" UnboundType="Integer">
                                        <EditFormSettings Visible="False" />
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Id" FieldName="pedidoId"
                                        VisibleIndex="0">
                                        <EditFormSettings Visible="False" />
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataComboBoxColumn Caption="Separador" FieldName="idUsuarioSeparacao" VisibleIndex="3" Width="130px">
                                        <PropertiesComboBox DataSourceID="sqlUsuarios" TextField="nome"
                                            ValueField="idUsuarioExpedicao" ValueType="System.String">
                                        </PropertiesComboBox>
                                    </dxwgv:GridViewDataComboBoxColumn>
                                    <dxwgv:GridViewDataComboBoxColumn Caption="Embalador" FieldName="idUsuarioEmbalado" VisibleIndex="3" Width="130px">
                                        <PropertiesComboBox DataSourceID="sqlUsuarios" TextField="nome"
                                            ValueField="idUsuarioExpedicao" ValueType="System.String">
                                        </PropertiesComboBox>
                                    </dxwgv:GridViewDataComboBoxColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Volumes" FieldName="volumesEmbalados"
                                        VisibleIndex="3">
                                        <EditFormSettings Visible="False" />
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Name="tempoEmbalagem" Caption="Tempo" FieldName="tempoEmbalagem" VisibleIndex="3" Width="120px" UnboundType="DateTime">
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataDateColumn Name="dataSendoEmbalado" Caption="Inicio Embalagem" FieldName="dataSendoEmbalado" VisibleIndex="3" Width="120px">
                                        <PropertiesDateEdit DisplayFormatString="g"></PropertiesDateEdit>
                                    </dxwgv:GridViewDataDateColumn>
                                    <dxwgv:GridViewDataDateColumn Name="dataEmbaladoFim" Caption="Fim Embalagem" FieldName="dataEmbaladoFim" VisibleIndex="3" Width="120px">
                                        <PropertiesDateEdit DisplayFormatString="g"></PropertiesDateEdit>
                                    </dxwgv:GridViewDataDateColumn>
                                    <dxwgv:GridViewDataTextColumn Name="tempoEmbalagem" Caption="Tempo" FieldName="tempoEmbalagem" VisibleIndex="3" Width="120px" UnboundType="DateTime">
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataHyperLinkColumn Caption="Editar" FieldName="pedidoId"
                                        VisibleIndex="7" Width="30px">
                                        <PropertiesHyperLinkEdit ImageUrl="~/admin/images/btEditar.jpg"
                                            NavigateUrlFormatString="pedido.aspx?pedidoId={0}" Target="_blank">
                                        </PropertiesHyperLinkEdit>
                                        <Settings AllowAutoFilter="False" />
                                        <HeaderTemplate>
                                            <img alt="" src="images/legendaEditar.jpg" />
                                        </HeaderTemplate>
                                    </dxwgv:GridViewDataHyperLinkColumn>
                                </Columns>
                                <StylesEditors>
                                    <Label Font-Bold="True">
                                    </Label>
                                </StylesEditors>
                            </dxwgv:ASPxGridView>
                            <dxwgv:ASPxGridViewExporter ID="grdEx" runat="server" FileName="pedidos"
                                GridViewID="grd" Landscape="True" PreserveGroupRowStates="True">
                            </dxwgv:ASPxGridViewExporter>
                            <asp:LinqDataSource ID="sqlUsuarios" runat="server"
                                ContextTypeName="dbCommerceDataContext" EnableDelete="True"
                                EnableInsert="True" EnableUpdate="True" TableName="tbUsuarioExpedicaos" OrderBy="nome" EntityTypeName="">
                            </asp:LinqDataSource>
                        </td>
                    </tr>                    
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
