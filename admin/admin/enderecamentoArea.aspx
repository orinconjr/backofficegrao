﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="enderecamentoArea.aspx.cs" Inherits="admin_enderecamentoArea" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" Runat="Server">
     idEnderecamentoArea
area
idCentroDistribuicao

    
    
    <asp:GridView ID="GridView1" DataKeyNames="idEnderecamentoArea"  BorderWidth="0px"
            runat="server" AutoGenerateColumns="False" AllowPaging="True" OnPageIndexChanging="GridView1_PageIndexChanging"
             OnRowDeleting="GridView1_RowDeleting" OnRowDataBound="GridView1_RowDataBound"
            OnRowCancelingEdit="GridView1_RowCancelingEdit" OnRowEditing="GridView1_RowEditing"
            OnRowUpdating="GridView1_RowUpdating" PageSize="50">
            <Columns>
                <asp:BoundField DataField="idEnderecamentoArea" HeaderText="Cód" ReadOnly="True"></asp:BoundField>
                <asp:TemplateField HeaderText="Área">
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("area") %>'></asp:Label>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("area") %>'></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                            ControlToValidate="TextBox1" ErrorMessage="Digite a área" ForeColor="Red"></asp:RequiredFieldValidator>
                    </EditItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Centro de Distribuição">
                    <ItemTemplate>
                        <asp:Label ID="Label2" runat="server" Text='<%# Bind("idCentroDistribuicao") %>'></asp:Label>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("idCentroDistribuicao") %>'></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server"
                            ControlToValidate="TextBox2" ErrorMessage="Digite o centro de distribuição" ForeColor="Red"></asp:RequiredFieldValidator>
                    </EditItemTemplate>
                </asp:TemplateField>
                <asp:CommandField CancelText="Cancela" EditText="Alterar" ControlStyle-Width="22px"
                    ControlStyle-Height="24px" HeaderText="Editar" ShowEditButton="True" UpdateText="Confirma"
                    ButtonType="Image" EditImageUrl="imagens/lapis.png" HeaderStyle-HorizontalAlign="Center">
                    <ItemStyle></ItemStyle>
                    <ControlStyle Height="24px" Width="22px"></ControlStyle>
                    <HeaderStyle />
                </asp:CommandField>
                <asp:TemplateField HeaderText="Excluir" ShowHeader="False">
                    <ItemTemplate>
                        <asp:LinkButton ID="cmdDelete" runat="server" CausesValidation="False" CommandName="Delete"
                            Text="Delete">
                                <img  src="imagens/lixeira.png" alt="Excluir a área" style="border-style:none;"/>
                        </asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <AlternatingRowStyle BackColor="#E6E6E6" />
            <PagerSettings Mode="NumericFirstLast" />
        </asp:GridView>
</asp:Content>

