﻿using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_simulacaoFrete : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                pnlGrid.Visible = true;
                pnlResult.Visible = false;
                CarregarGrid();
            }
            else
                CarregarSimulacao();



        }
        catch (Exception ex)
        {
            lblMensagem.Text = "Erro: " + ex.Message;
        }
    }

    private void CarregarSimulacao()
    {
        try
        {
            lblMensagem.Text = string.Empty;


            if (string.IsNullOrEmpty(dataEditInicial.Text))
            {
                lblMensagem.Text = "Favor informar a data inicial";
                dataEditInicial.Focus();
                return;
            }

            if (string.IsNullOrEmpty(dataEditFinal.Text))
            {
                lblMensagem.Text = "Favor informar a data final";
                dataEditFinal.Focus();
                return;
            }


            var data = new dbCommerceDataContext();

            List<int> listTabelas = new List<int>();

            if (Session["ListaTabelaCheck"] != null)
            {
                listTabelas = (List<int>)Session["ListaTabelaCheck"];
            }

            for (int i = 0; i < grdTransportadoras.VisibleRowCount; i++)
            {
                ASPxCheckBox chk = grdTransportadoras.FindRowCellTemplateControl(i, null, "chk") as ASPxCheckBox;
                if (chk.Checked)
                    listTabelas.Add(int.Parse(chk.Value.ToString()));
            }

            Session["ListaTabelaCheck"] = listTabelas;

            var dtInicial = DateTime.Parse(dataEditInicial.Text);
            var dtFInal = DateTime.Parse(dataEditFinal.Text);

            var envios = (from c in data.tbPedidoEnvios
                          where c.dataFimEmbalagem >= dtInicial && c.idCentroDeDistribuicao == 4 && c.dataFimEmbalagem < dtFInal
                          select new
                          {
                              c.idPedidoEnvio,
                              c.tbPedido.endCep,
                              c.valorSelecionado,
                              c.formaDeEnvio,
                              c.tbPedido.pedidoId
                          }).ToList();

            var enviosTransporte = (from c in data.tbPedidoEnvioValorTransportes
                                    where c.tbPedidoEnvio.idCentroDeDistribuicao >= 4 && c.tbPedidoEnvio.dataFimEmbalagem >= dtInicial
                                    && c.tbPedidoEnvio.dataFimEmbalagem < dtFInal && listTabelas.Contains(c.tipoDeEntregaId.Value)
                                    && c.valor > 0
                                    select new retornoSimulacao()
                                    {
                                        transportadora = c.servico,
                                        idPedidoEnvio = c.tbPedidoEnvio.idPedidoEnvio,
                                        valor = c.valor,
                                        volume = c.tbPedidoEnvio.tbPedidoPacotes.Count(),
                                        data = c.tbPedidoEnvio.dataFimEmbalagem.Value.ToShortDateString()
                                    }).ToList();

            var enviosFinais = (from c in enviosTransporte
                               group c by c.idPedidoEnvio into grupos
                               select new
                               {
                                   grupos.First().idPedidoEnvio,
                                   valor = grupos.OrderBy(x => x.valor).First().valor,
                                   volumes = grupos.OrderBy(x => x.volume).Count(),
                                   transpotadora = grupos.GroupBy(x => x.transportadora),
                                   data = grupos.OrderBy(x => x.data).First().data
                               }).ToList();


            grd.DataSource = enviosFinais;
            grd.DataBind();

            pnlGrid.Visible = false;
            pnlResult.Visible = true;
        }
        catch (Exception ex)
        {
            lblMensagem.Text = "Erro: " + ex.Message;
        }
    }

    protected void chk_Init(object sender, EventArgs e)
    {
        ASPxCheckBox chk = sender as ASPxCheckBox;
        GridViewDataItemTemplateContainer container = chk.NamingContainer as GridViewDataItemTemplateContainer;
    }

    private void CarregarGrid()
    {
        try
        {
            var data = new dbCommerceDataContext();

            var dados = (from c in data.tbTipoDeEntregas
                         join d in data.tbTransportadoras on c.idTransportadora equals d.idTransportadora
                         where (c.idTransportadoraString != null)
                         select c).OrderBy(x => x.tipoDeEntregaNome).OrderBy(x => x.idTransportadora).ToList();

            grdTransportadoras.DataSource = dados;
            grdTransportadoras.DataBind();

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }


    protected void btnVerSimulacao_Click(object sender, EventArgs e)
    {
        try
        {
            CarregarSimulacao();
        }
        catch (Exception ex)
        {
            lblMensagem.Text = ex.Message;
        }
    }

    public class retornoSimulacao
    {
        public string transportadora { get; set; }
        public int idPedidoEnvio { get; set; }
        public decimal valor { get; set; }
        public int volume { get; set; }
        public string data { get; set; }
    }


    protected void dataEditFinal_Init(object sender, EventArgs e)
    {
        try
        {
            dataEditFinal.MaxDate = DateTime.Today.AddDays(-1);
        }
        catch (Exception ex)
        {
            lblMensagem.Text = "Erro: " + ex.Message;
        }
    }

    protected void dataEditInicial_Init(object sender, EventArgs e)
    {
        try
        {
            dataEditInicial.MaxDate = DateTime.Today.AddDays(-1);
        }
        catch (Exception ex)
        {
            lblMensagem.Text = "Erro: " + ex.Message;
        }
    }

    protected void dltSimulacao_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        try
        {
            switch (e.Item.ItemType)
            {
                case ListItemType.Footer:
                    Label lblValorTotal = (Label)e.Item.FindControl("lblValorTotal");
                    lblValorTotal.Text = "1.243,00";
                    break;
                case ListItemType.Item:

                    Label lblTransportadora = (Label)e.Item.FindControl("lblTransportadora");
                    lblTransportadora.Text = ((DataRowView)e.Item.DataItem).Row.ItemArray[0].ToString();


                    break;
                default:
                    break;
            }
        }
        catch (Exception ex)
        {
            lblMensagem.Text = "Erro: " + ex.Message;
        }
    }
}