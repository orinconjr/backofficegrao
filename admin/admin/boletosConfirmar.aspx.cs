﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BoletoNet;
using DevExpress.Web.ASPxGridView;

public partial class admin_boletosConfirmar : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnSalvar_Click(object sender, ImageClickEventArgs e)
    {
        //var status = new List<pedidoStatus>();

        //string tabelaConteudo = "";
        //List<string> listaConteudo = new List<string>();
        //bool boletoCaixa = false;

        //if (fluArquivo.HasFile)
        //{
        //    using (Stream fileStream = fluArquivo.PostedFile.InputStream)
        //    using (StreamReader sr = new StreamReader(fileStream))
        //    {
        //        string conteudo = null;
        //        while ((conteudo = sr.ReadLine()) != null)
        //        {
        //            tabelaConteudo += conteudo;
        //            listaConteudo.Add(conteudo);
        //            if (conteudo.Trim() == "CONSULTA EXTRATO COBRANCA")
        //            {
        //                boletoCaixa = true;
        //            }
        //        }
        //    }
        //}

        //if (boletoCaixa)
        //{
        //    processaBoletoCaixa(listaConteudo);
        //}
        //else
        //{
        //    processaBoletoHsbc(tabelaConteudo);
        //}
        HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
        usuarioLogadoId = Request.Cookies["usuarioLogadoId"];
        string usuario = "";
        if (usuarioLogadoId != null)
        {
            usuario = rnUsuarios.usuarioSeleciona_PorUsuarioId(int.Parse(usuarioLogadoId.Value.ToString())).Tables[0].Rows[0]["usuarioNome"].ToString();
        }
        //string caminho = Server.MapPath("testelog.log").ToString();
        File.AppendAllText(Server.MapPath("BoletosConfirmar.log"), "Usuário:" + usuario + " - Data/Hora: " + DateTime.Now + " - Arquivo: " + fluArquivo.PostedFile.FileName + Environment.NewLine);
        processaBoletoCaixaCnab();
    }
    private void processaBoletoCaixaCnab()
    {
        var status = new List<pedidoStatus>();
        var boletoRetorno = new BoletoNet.ArquivoRetornoCNAB240();
        boletoRetorno.LerArquivoRetorno(new Banco(104), fluArquivo.PostedFile.InputStream);
        foreach (var linhaRetorno in boletoRetorno.ListaDetalhes)
        {
            decimal valorPago = linhaRetorno.SegmentoU.ValorPagoPeloSacado;
            string numeroCobranca = (String.Format("{0:00000000}", Convert.ToInt32(linhaRetorno.SegmentoT.NossoNumero.ToString().Substring(2, 15))));

            var pedidosDc = new dbCommerceDataContext();
            var pedidoPagamento = (from c in pedidosDc.tbPedidoPagamentos where c.numeroCobranca == numeroCobranca select c).FirstOrDefault();
            var statusPedido = new pedidoStatus();
            statusPedido.pedidoId = 0;
            statusPedido.valorPago = valorPago;

            if (pedidoPagamento != null)
            {
                var pedido = pedidoPagamento.tbPedido;
                statusPedido.pedidoId = pedido.pedidoId;

                if (pedido.statusDoPedido == 2)
                {
                    if (pedidoPagamento.valor >= (valorPago - Convert.ToDecimal("0,10")) && pedidoPagamento.valor <= (valorPago + Convert.ToDecimal("0,10")))
                    {
                        statusPedido.status = "Valor Correto";
                        HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
                        usuarioLogadoId = Request.Cookies["usuarioLogadoId"];
                        string usuario = "";
                        if (usuarioLogadoId != null)
                        {
                            usuario = rnUsuarios.usuarioSeleciona_PorUsuarioId(int.Parse(usuarioLogadoId.Value.ToString())).Tables[0].Rows[0]["usuarioNome"].ToString();
                        }
                        int retorno = 0;

                        try
                        {
                            int retornoConfirmacao = rnPagamento.confirmarPagamento(pedidoPagamento.idPedidoPagamento, 1, numeroCobranca, "Pagamento confirmado via importação de arquivo do banco", true);
                            retorno = retornoConfirmacao;

                            if (retorno == 0) statusPedido.status = "Pagamento Confirmado";
                            if (retorno == 1) statusPedido.status = "Pagamento Confirmado / Separação de Estoque";
                            if (retorno == 99) statusPedido.status = "Pagamento parcialmente confirmado. Aguardando restante do pagamento";
                        }
                        catch (Exception)
                        {
                            statusPedido.status = "Erro no processamento";
                        }
                    }
                    else
                    {
                        statusPedido.status = "Valor Incorreto";
                    }
                }
                else if (pedido.statusDoPedido == 7 && (pedido.tipoDePagamentoId == 8 | pedido.tipoDePagamentoId == 1))
                {
                    if (pedidoPagamento.valor >= (valorPago - Convert.ToDecimal("0,10")) && pedidoPagamento.valor <= (valorPago + Convert.ToDecimal("0,10")))
                    {
                        statusPedido.status = "Valor Correto";
                        HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
                        usuarioLogadoId = Request.Cookies["usuarioLogadoId"];
                        string usuario = "";
                        if (usuarioLogadoId != null)
                        {
                            usuario = rnUsuarios.usuarioSeleciona_PorUsuarioId(int.Parse(usuarioLogadoId.Value.ToString())).Tables[0].Rows[0]["usuarioNome"].ToString();
                        }
                        int retorno = 0;

                        try
                        {
                            int retornoConfirmacao = rnPagamento.confirmarPagamento(pedidoPagamento.idPedidoPagamento, 1, numeroCobranca, "Pagamento confirmado via importação de arquivo do banco", true);
                            retorno = retornoConfirmacao;

                            if (retorno == 0) statusPedido.status = "Pagamento Confirmado";
                            if (retorno == 1) statusPedido.status = "Pagamento Confirmado / Separação de Estoque";
                            if (retorno == 99) statusPedido.status = "Pagamento parcialmente confirmado. Aguardando restante do pagamento";
                        }
                        catch (Exception)
                        {
                            statusPedido.status = "Erro no processamento";
                        }
                    }
                    else
                    {
                        statusPedido.status = "Valor Incorreto";
                    }
                }
                else
                {
                    statusPedido.status = "Status do pedido incorreto";
                }
            }
            else
            {
                statusPedido.status = "Pedido não localizado";
            }
            status.Add(statusPedido);


        }


        grd.DataSource = status.OrderByDescending(x => x.status);
        grd.DataBind();
        pnAtualizar.Visible = false;
        pnStatus.Visible = true;
    }

    private void processaBoletoCaixa2(List<string> conteudo)
    {
        var status = new List<pedidoStatus>();

        foreach (var linha in conteudo)
        {
            if (linha.StartsWith("24"))
            {
                var linhaSplit = linha.Split(';');
                string nossoNumero = linhaSplit[0].Trim();
                string valor = linhaSplit[1].Trim();
                string valor2 = linhaSplit[2].Trim();


                decimal valorPago = 0;
                decimal.TryParse(valor, out valorPago);
                decimal valorPago2 = 0;
                decimal.TryParse(valor2, out valorPago2);

                if (valorPago == valorPago2)
                {
                    string numeroCobranca = (String.Format("{0:00000000}", Convert.ToInt32(nossoNumero.ToString().Substring(2, 15))));

                    var pedidosDc = new dbCommerceDataContext();
                    var pedidoPagamento = (from c in pedidosDc.tbPedidoPagamentos where c.numeroCobranca == numeroCobranca select c).FirstOrDefault();
                    var statusPedido = new pedidoStatus();
                    statusPedido.pedidoId = 0;
                    statusPedido.valorPago = valorPago;

                    if (pedidoPagamento != null)
                    {
                        var pedido = pedidoPagamento.tbPedido;
                        statusPedido.pedidoId = pedido.pedidoId;

                        if (pedido.statusDoPedido == 2)
                        {

                            if (pedidoPagamento.valor >= (valorPago - Convert.ToDecimal("0,01")) && pedidoPagamento.valor <= (valorPago + Convert.ToDecimal("0,01")))
                            {
                                HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
                                usuarioLogadoId = Request.Cookies["usuarioLogadoId"];
                                string usuario = "";
                                if (usuarioLogadoId != null)
                                {
                                    usuario = rnUsuarios.usuarioSeleciona_PorUsuarioId(int.Parse(usuarioLogadoId.Value.ToString())).Tables[0].Rows[0]["usuarioNome"].ToString();
                                }
                                int retorno = 0;

                                try
                                {
                                    //int retornoConfirmacao = rnPedidos.confirmarPagamentoPedido(pedidoId, usuario);
                                    int retornoConfirmacao = rnPagamento.confirmarPagamento(pedidoPagamento.idPedidoPagamento, 1, numeroCobranca, "Pagamento confirmado via importação de arquivo do banco", true);
                                    retorno = retornoConfirmacao;

                                    if (retorno == 0) statusPedido.status = "Pagamento Confirmado";
                                    if (retorno == 1) statusPedido.status = "Pagamento Confirmado / Separação de Estoque";
                                    if (retorno == 99) statusPedido.status = "Pagamento parcialmente confirmado. Aguardando restante do pagamento";
                                }
                                catch (Exception)
                                {
                                    statusPedido.status = "Erro no processamento";
                                }



                            }
                            else
                            {
                                statusPedido.status = "Valor Incorreto";
                            }
                        }
                        else
                        {
                            statusPedido.status = "Status do pedido incorreto";
                        }
                    }
                    else
                    {
                        statusPedido.status = "Pedido não localizado";
                    }
                    status.Add(statusPedido);
                }
            }
        }


        grd.DataSource = status.OrderByDescending(x => x.status);
        grd.DataBind();
        pnAtualizar.Visible = false;
        pnStatus.Visible = true;
    }

    private void processaBoletoCaixa(List<string> conteudo)
    {
        var status = new List<pedidoStatus>();

        foreach (var linha in conteudo)
        {
            if (linha.StartsWith("24"))
            {
                var linhaSplit = linha.Split(';');
                Response.Write(linha[0]);
                string nossoNumero = linha.Substring(0, 17).Trim();
                string valor = linha.Substring(177, 15).Trim();
                //string historico = linha.Substring(193, 45).Trim();

                decimal valorPago = 0;
                decimal.TryParse(valor, out valorPago);

                if (valorPago > 4)
                {
                    string numeroCobranca = (String.Format("{0:00000000}", Convert.ToInt32(nossoNumero.ToString().Substring(2, 15))));

                    var pedidosDc = new dbCommerceDataContext();
                    var pedidoPagamento = (from c in pedidosDc.tbPedidoPagamentos where c.numeroCobranca == numeroCobranca select c).FirstOrDefault();
                    var statusPedido = new pedidoStatus();
                    statusPedido.pedidoId = 0;
                    statusPedido.valorPago = valorPago;

                    if (pedidoPagamento != null)
                    {
                        var pedido = pedidoPagamento.tbPedido;
                        statusPedido.pedidoId = pedido.pedidoId;

                        if (pedido.statusDoPedido == 2)
                        {

                            if (pedidoPagamento.valor >= (valorPago - Convert.ToDecimal("0,01")) && pedidoPagamento.valor <= (valorPago + Convert.ToDecimal("0,01")))
                            {
                                HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
                                usuarioLogadoId = Request.Cookies["usuarioLogadoId"];
                                string usuario = "";
                                if (usuarioLogadoId != null)
                                {
                                    usuario = rnUsuarios.usuarioSeleciona_PorUsuarioId(int.Parse(usuarioLogadoId.Value.ToString())).Tables[0].Rows[0]["usuarioNome"].ToString();
                                }
                                int retorno = 0;

                                try
                                {
                                    //int retornoConfirmacao = rnPedidos.confirmarPagamentoPedido(pedidoId, usuario);
                                    int retornoConfirmacao = rnPagamento.confirmarPagamento(pedidoPagamento.idPedidoPagamento, 1, numeroCobranca, "Pagamento confirmado via importação de arquivo do banco", true);
                                    retorno = retornoConfirmacao;

                                    if (retorno == 0) statusPedido.status = "Pagamento Confirmado";
                                    if (retorno == 1) statusPedido.status = "Pagamento Confirmado / Separação de Estoque";
                                    if (retorno == 99) statusPedido.status = "Pagamento parcialmente confirmado. Aguardando restante do pagamento";
                                }
                                catch (Exception)
                                {
                                    statusPedido.status = "Erro no processamento";
                                }



                            }
                            else
                            {
                                statusPedido.status = "Valor Incorreto";
                            }
                        }
                        else
                        {
                            statusPedido.status = "Status do pedido incorreto";
                        }
                    }
                    else
                    {
                        statusPedido.status = "Pedido não localizado";
                    }
                    status.Add(statusPedido);
                }
            }
        }

        grd.DataSource = status.OrderByDescending(x => x.status);
        grd.DataBind();
        pnAtualizar.Visible = false;
        pnStatus.Visible = true;
    }

    private void processaBoletoHsbc(string tabelaConteudo)
    {
        var status = new List<pedidoStatus>();

        HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
        doc.LoadHtml(tabelaConteudo);

        var table = doc.DocumentNode
                   .Descendants("tr")
                   .Select(n => n.Elements("td").Select(x => x.InnerText).ToArray());
        int linha = 0;
        int linhaSequencial = 1;
        long codPedidoAtual = 0;
        decimal valorPago = 0;
        foreach (var tr in table)
        {
            if (linha > 0)
            {
                if (linhaSequencial == 1)
                {
                    long.TryParse(tr[0].ToString(), out codPedidoAtual);
                    if (codPedidoAtual == 0) linha = 0;
                }
                if (linhaSequencial == 4)
                {
                    decimal.TryParse(tr[2].ToString(), out valorPago);
                    string numeroCobranca = (String.Format("{0:00000000}", Convert.ToInt32(codPedidoAtual.ToString().Substring(0, (codPedidoAtual.ToString().Length - 3)))));
                    //int pedidoId = Convert.ToInt32(codPedidoAtual.ToString().Substring(0, 5));
                    var pedidosDc = new dbCommerceDataContext();
                    var pedidoPagamento = (from c in pedidosDc.tbPedidoPagamentos where c.numeroCobranca == numeroCobranca select c).FirstOrDefault();


                    var statusPedido = new pedidoStatus();
                    statusPedido.pedidoId = 0;
                    statusPedido.valorPago = valorPago;

                    if (pedidoPagamento != null)
                    {
                        var pedido = pedidoPagamento.tbPedido;
                        statusPedido.pedidoId = pedido.pedidoId;

                        if (pedido.statusDoPedido == 2)
                        {

                            if (pedidoPagamento.valor >= (valorPago - Convert.ToDecimal("0,01")) && pedidoPagamento.valor <= (valorPago + Convert.ToDecimal("0,01")))
                            {
                                HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
                                usuarioLogadoId = Request.Cookies["usuarioLogadoId"];
                                string usuario = "";
                                if (usuarioLogadoId != null)
                                {
                                    usuario = rnUsuarios.usuarioSeleciona_PorUsuarioId(int.Parse(usuarioLogadoId.Value.ToString())).Tables[0].Rows[0]["usuarioNome"].ToString();
                                }
                                int retorno = 0;

                                //int retornoConfirmacao = rnPedidos.confirmarPagamentoPedido(pedidoId, usuario);
                                int retornoConfirmacao = rnPagamento.confirmarPagamento(pedidoPagamento.idPedidoPagamento, 1, numeroCobranca, "Pagamento confirmado via importação de arquivo do banco", true);
                                retorno = retornoConfirmacao;

                                if (retorno == 0) statusPedido.status = "Pagamento Confirmado";
                                if (retorno == 1) statusPedido.status = "Pagamento Confirmado / Separação de Estoque";
                                if (retorno == 99) statusPedido.status = "Pagamento parcialmente confirmado. Aguardando restante do pagamento";
                            }
                            else
                            {
                                statusPedido.status = "Valor Incorreto";
                            }
                        }
                        else
                        {
                            statusPedido.status = "Status do pedido incorreto";
                        }
                    }
                    else
                    {
                        statusPedido.status = "Pedido não localizado";
                    }
                    status.Add(statusPedido);
                    linhaSequencial = 1;
                    codPedidoAtual = 0;
                }
                else
                {
                    linhaSequencial++;
                }
            }

            string td = tr[0];
            if (td.Contains("do documento"))
            {
                linha = 1;
            }
        }

        grd.DataSource = status.OrderByDescending(x => x.status);
        grd.DataBind();
        pnAtualizar.Visible = false;
        pnStatus.Visible = true;
    }

    protected void gridClientes_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType != GridViewRowType.Data)
        {
            return;
        }

        string status = grd.GetRowValues(e.VisibleIndex, new string[] { "status" }).ToString();
        if (status.ToLower() == "pagamento confirmado" | status.ToLower() == "pagamento parcialmente confirmado. aguardando restante do pagamento.")
        {
            int i = 0;
            while ((i < e.Row.Cells.Count))
            {
                e.Row.Cells[i].Style.Add("background", "#D2E9FF");
                i++;
            }
        }
        else
        {
            int i = 0;
            while ((i < e.Row.Cells.Count))
            {
                e.Row.Cells[i].Style.Add("background", "#FFC6C7");
                i++;
            }
        }
    }
    private class pedidoStatus
    {
        public int pedidoId { get; set; }
        public string status { get; set; }
        public decimal valorPago { get; set; }
    }

}