﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_botaoAdicionarItemPedido : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnAdicionarbotao_Click(object sender, EventArgs e)
    {
        int pedidoId = Convert.ToInt32(txtpedidoId.Text);
        var data = new dbCommerceDataContext();

        var enviosGeral = (from c in data.tbPedidoEnvios where c.idPedido ==pedidoId select c).ToList();
        foreach (var item in enviosGeral)
        {
            item.dataFimSeparacao = DateTime.Today;
            item.dataInicioEmbalagem = DateTime.Today;
            item.dataFimEmbalagem = DateTime.Today;
        }
        try
        {
            data.SubmitChanges();
            Response.Write("ok");
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
       
    }
}