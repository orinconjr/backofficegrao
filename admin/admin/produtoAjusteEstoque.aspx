﻿<%@ Page Title="" Theme="Glass" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="produtoAjusteEstoque.aspx.cs" Inherits="admin_produtoAjusteEstoque" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxTimer" TagPrefix="dxt" %>
<%@ Register assembly="DevExpress.Web.v11.1" namespace="DevExpress.Web.ASPxPopupControl" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" Runat="Server">
    <dx:ASPxPopupControl ID="pcNota" runat="server" CloseAction="CloseButton" Modal="True"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="TopSides" ClientInstanceName="pcNota"
        HeaderText="Movimento de Estoque" AllowDragging="True" PopupAnimationType="None" EnableViewState="True">
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server">
                <div style="width: 200px; height: 300px; ">
                    <table style="width: 100%">
                        <tr class="campos">
                            <td>
                                Tipo:<br/>
                                <asp:DropDownList runat="server" ID="ddlTipo">
                                    <Items>
                                        <asp:ListItem Text="Entrada" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="Saída" Value="0"></asp:ListItem>
                                    </Items>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr class="campos">
                            <td>
                                ID da Empresa:<br/>
                                <asp:TextBox runat="server" ID="txtIdDaEmpresa"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rqvNomeNovo" ValidationGroup="adiciona" runat="server" ControlToValidate="txtIdDaEmpresa" ErrorMessage="Preencha o ID da Empresa" SetFocusOnError="True"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr class="campos" id="trComplementoId" runat="server" Visible="False">
                            <td>
                                Complemento ID:<br/>
                                <asp:DropDownList runat="server" ID="ddlComplementoIdDaEmpresa" Visible="False" DataValueField="produtoId" DataTextField="complementoIdDaEmpresa"/>
                            </td>
                        </tr>
                        <tr class="campos">
                            <td>
                                Quantidade:<br/>
                                <asp:TextBox runat="server" ID="txtQuantidade"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="adiciona" runat="server" ControlToValidate="txtQuantidade" ErrorMessage="Preencha a quantidade" SetFocusOnError="True"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr class="campos">
                            <td>
                                Motivo:<br/>
                                <asp:DropDownList runat="server" ID="ddlMotivo">
                                    <Items>
                                        <asp:ListItem Text="Brinde" Value="Brinde"></asp:ListItem>
                                        <asp:ListItem Text="Troca" Value="Troca"></asp:ListItem>
                                        <asp:ListItem Text="Defeito" Value="Defeito"></asp:ListItem>
                                        <asp:ListItem Text="Devolução" Value="Devolução"></asp:ListItem>
                                        <asp:ListItem Text="Compra" Value="Compra"></asp:ListItem>
                                        <asp:ListItem Text="Ajuste de Estoque" Value="Ajuste de Estoque"></asp:ListItem>
                                    </Items>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr class="campos">
                            <td>
                                Observação:<br/>
                                <asp:TextBox runat="server" ID="txtObservacao" Width="100%" TextMode="MultiLine" Height="50"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="campos">
                            <td>
                                <asp:Button runat="server" ID="btnAdicionar" ValidationGroup="adiciona" OnClick="btnAdicionar_OnClick" Text="Gravar"/>
                            </td>
                        </tr>
                    </table>
                </div>
            </dx:PopupControlContentControl>
        </ContentCollection>
        <ContentStyle>
            <Paddings PaddingBottom="5px" />
        </ContentStyle>
    </dx:ASPxPopupControl>

    <table cellpadding="0" cellspacing="0" style="width: 920px">
    <tr>
        <td class="tituloPaginas" valign="top">
            <asp:Label ID="lblAcao" runat="server">Ajuste de Estoque</asp:Label>
        </td>
    </tr>
        
    <tr>
        <td>
            
    <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td style="text-align: right; padding-bottom: 10px;">
                <dx:ASPxButton ID="btShowModal" runat="server" Text="Novo ajuste de Estoque" AutoPostBack="False">
                    <ClientSideEvents Click="function(s, e) { ShowNotaWindow(); }" />
                </dx:ASPxButton>
                                        
                <script type="text/javascript">
                    // <![CDATA[
                    function ShowNotaWindow() {
                        pcNota.Show();
                    }
                    // ]]>
                </script>
            </td>
        </tr>

        <tr>
            <td>
                <dxwgv:ASPxGridView ID="grd" runat="server" AutoGenerateColumns="False" Width="834px" Cursor="auto" KeyFieldName="idProdutoAjusteEstoque">
                    <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                    <Styles>
                        <Footer Font-Bold="True">
                        </Footer>
                    </Styles>
                    <SettingsText ConfirmDelete="Tem certeza que deseja excluir?" emptydatarow="Nenhum registro encontrado." />
                    <SettingsPager Position="TopAndBottom" PageSize="50" ShowDisabledButtons="False" AlwaysShowPager="True">
                        <Summary AllPagesText="Páginas: {0} - {1} ({2} items)" Text="Página {0} de {1} ({2} registros encontrados)" />
                    </SettingsPager>
                    <settings showfilterrow="True" ShowGroupButtons="False" ShowHeaderFilterButton="True" ShowFooter="True" />
                    <SettingsEditing EditFormColumnCount="4" PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1" PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter" PopupEditFormWidth="700px" />
                    <Columns>
                        <dxwgv:GridViewDataDateColumn Caption="Data" FieldName="data" VisibleIndex="0">
                        </dxwgv:GridViewDataDateColumn>
                        <dxwgv:GridViewDataTextColumn Caption="ID" FieldName="idProduto" VisibleIndex="0" Width="50">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Produto" FieldName="produtoNome" VisibleIndex="0">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="ID da Empresa" FieldName="produtoIdDaEmpresa" VisibleIndex="0">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Complemento ID" FieldName="complementoIdDaEmpresa" VisibleIndex="0">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Tipo" FieldName="tipo" VisibleIndex="0" Width="30px">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Quantidade" FieldName="quantidade" VisibleIndex="0" Width="30px">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Motivo" FieldName="motivo" VisibleIndex="0" Width="30px">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Usuário" FieldName="usuario" VisibleIndex="0" Width="30px">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Observação" FieldName="observacao" VisibleIndex="0" Width="30px">
                        </dxwgv:GridViewDataTextColumn>
                    </Columns>
                    <StylesEditors>
                        <Label Font-Bold="True">
                        </Label>
                    </StylesEditors>
                </dxwgv:ASPxGridView>         
                <asp:ObjectDataSource ID="sql" runat="server" SelectMethod="produtoEstoqueReal" TypeName="rnProdutos">
                </asp:ObjectDataSource>
            </td>
        </tr>
    </table>
        </td>
    </tr>
</table>
</asp:Content>
