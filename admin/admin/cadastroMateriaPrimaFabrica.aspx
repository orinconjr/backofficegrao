﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="cadastroMateriaPrimaFabrica.aspx.cs" Inherits="admin_cadastroMateriaPrimaFabrica" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxPopupControl" Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>

<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxPopupControl" Assembly="DevExpress.Web.v11.1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" Runat="Server">
    <script src="js/jquery.min.js"></script>
    <link rel="stylesheet" href="css/bootstrap-table.css" type="text/css" />
    <style type="text/css">
        .classehoje a {
            color: #FFFFFF;
        }
        .classehoje a:hover {
            color: #FFFFFF;
        }
        .classehoje a:active {
            color: #FFFFFF;
        }
        .classehoje a:visited {
            color: #FFFFFF;
        }
        .soimprimir {
            display: none;
        }
        .borderN {
            border: 0px;
        }
        .borderR {
            border-right: 2px solid black;
        }
        @media print {
            #itensDoRomaneio {
                background-color: white;
                height: 100%;
                width: 100%;
                position: fixed;
                top: 0;
                left: 0;
                margin: 0;
                padding: 15px;
                font-size: 14px;
                line-height: 18px;
            }
        }
        #tbProcessoAtual tr:nth-child(odd) {
            background-color: #DEDEDE;
        }
        .tabela {
            border-spacing: 0;
        }
         .tabela td {
            vertical-align: top;
            padding: 2px;
        }
        .tabela tr:nth-child(odd) {
            background-color: #DEDEDE;
        }

        .tabela, .tabela th, .tabela td {
            border:1px solid #BBB;
        }
        .tabela {
            border-bottom:0;
            border-left:0;
        }
        .tabela th {
            font-weight: bold;
            font-size: 14px;
        }
        .tabela td, .tabela th {
            border-top:0;
            border-right:0;
        }
    </style>
    <script type="text/javascript">
        function checaChecks(campo) {
            var divs = document.querySelectorAll("[id]");
            for (var i = 0, len = divs.length; i < len; i++) {
                var div = divs[i];
                if (div.id.indexOf(campo) > -1) {
                    div.checked = true;
                }
            }
        }
        function checaEstoque() {
            var divs = document.querySelectorAll("[id]");
            for (var i = 0, len = divs.length; i < len; i++) {
                var div = divs[i];
                if (div.id.indexOf("chkChecar") > -1) {
                    div.checked = true;
                }
            }
        }
    </script>
    <div class="tituloPaginas" valign="top">
            Matérias Primas Fábrica
    </div>
    <div>
        <table>
            <tr class="rotulos">
                <td colspan="5" style="padding-bottom: 20px; padding-left: 30px;">
                    <span style="font-size: 16px; font-weight: bold;">
                        Selecione a Matéria Prima:<br/>
                        <asp:DropDownList runat="server" ID="ddlMateriaPrima" DataValueField="idComprasProduto" DataTextField="produto" AppendDataBoundItems="True" Width="300" AutoPostBack="True" OnSelectedIndexChanged="ddlMateriaPrima_OnSelectedIndexChanged">
                            <Items>
                                <asp:ListItem Text="Selecione" Value=""></asp:ListItem>
                            </Items>
                        </asp:DropDownList>
                    </span>
                </td>
            </tr>
            <tr class="rotulos">
                <td colspan="5" style="padding-left: 30px;">
                    &nbsp;<br/>
                    <asp:CheckBox runat="server" ID="chkAtivo" Text="Ativo" />
                </td>
            </tr>
            <tr class="rotulos">
                <td style="padding-left: 30px;">
                    Nome:<br/>
                    <asp:TextBox runat="server" ID="txtNome"></asp:TextBox>
                </td>
                <td>
                    Categoria:<br/>
                    <asp:DropDownList runat="server" ID="ddlCategoria" DataValueField="categoriaId" DataTextField="categoriaNome" AppendDataBoundItems="True">
                        <Items>
                            <asp:ListItem Text="Selecione" Value=""></asp:ListItem>
                        </Items>
                    </asp:DropDownList>
                </td>
                <td>
                    Processos:<br/>
                    <asp:DropDownList runat="server" ID="ddlProcessosFabrica" DataValueField="idSubProcessoFabrica" DataTextField="nome" AppendDataBoundItems="True">
                        <Items>
                            <asp:ListItem Text="Selecione" Value=""></asp:ListItem>
                        </Items>
                    </asp:DropDownList>
                </td>
                <td>
                    &nbsp;<br/>
                    <asp:Button runat="server" ID="btnFiltrar" Text="Filtrar" OnClick="btnFiltrar_OnClick"/>
                </td>
                <td>
                    &nbsp;<br/>
                    Total: 
                    <asp:Literal runat="server" ID="litTotal"></asp:Literal>
                </td>
            </tr>
            <tr class="rotulos">
                <td>
                    &nbsp;
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
        </table>
    </div>
    <div>
        <table class="rotulos tabela" width="100%">
            <tr>
                <th>
                    Foto
                </th>
                <th>
                    Nome
                </th>
                <th>
                    Id da Empresa
                </th>
                <th>
                    Quantidade
                </th>
            </tr>
            <asp:ListView runat="server" ID="lstProdutos" OnItemDataBound="lstProdutos_OnItemDataBound" OnDataBound="lstProdutos_OnDataBound">
                <ItemTemplate>
                    <tr>
                        <asp:HiddenField runat="server" ID="hdfProdutoId" Value='<%# Eval("produtoId") %>' />
                        <asp:HiddenField runat="server" ID="hdfFoto" Value='<%# Eval("fotoDestaque") %>' />
                        <td>
                            <asp:Image runat="server" id="imgFoto" Width="250"/>
                        </td>
                        <td>
                            <%# Eval("produtoNome") %>
                        </td>
                        <td>
                            <%# Eval("produtoIdDaEmpresa") %>
                        </td>
                        <td>
                           <asp:TextBox runat="server" ID="txtQuantidade" Width="100"></asp:TextBox>
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:ListView>
        </table>
    </div>
    <div style="text-align: right; padding-top: 15px">
        <asp:Button runat="server" ID="btnGravar" Text="Gravar" OnClick="btnGravar_OnClick"/>
    </div>

    
    
</asp:Content>