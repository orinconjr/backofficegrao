﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Linq;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Net;
using System.IO;
using System.Xml;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxMenu;
using rakutenPedidos;
using Image = System.Web.UI.WebControls.Image;
using System.Security.Cryptography;
using System.IO;

public class pedidoEmail
{
    public int idFornecedor { get; set; }
    public string fornecedorNome { get; set; }
    public string produtoNome { get; set; }
    public string produtoIdDaEmpresa { get; set; }
    public int produtoQuantidade { get; set; }
    public string produtoPrazo { get; set; }
    public string produtoFoto { get; set; }
    public decimal produtoPrecoDeCusto { get; set; }
    public string fornecedorEmail { get; set; }
}

public class itemDevolucao
{
    public int? idNotaFiscal { get; set; }
    public int? idCentroDistribuicao { get; set; }
    public int? numeroNota { get; set; }
    public int? produtoId { get; set; }
    public int idItemPedidoEstoque { get; set; }
    public bool brinde { get; set; }
    public string produtoNome { get; set; }
    public string ncm { get; set; }
    public int? idCNPJ { get; set; }
    public int? pedidoId { get; set; }
}
public partial class admin_pedido : System.Web.UI.Page
{


    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (Request.QueryString["pedidoId"] != null)
        {
            int pedidoId;

            if (!int.TryParse(Request.QueryString["pedidoId"], out pedidoId))
                Response.Redirect("pedidos.aspx");
            bool check = false;
            try
            {
                check = Convert.ToBoolean(Request.QueryString["check"]);
            }
            catch (Exception ex) { }

            if (!check)
            {
                if (pedidoId.ToString().Length > 6)
                {
                    var data = new dbCommerceDataContext();
                    bool existePedido = (from c in data.tbPedidos where c.pedidoId == pedidoId select c).Any();
                    if (!existePedido)
                    {
                        pedidoId = rnFuncoes.retornaIdInterno(pedidoId);
                    }
                    Response.Redirect("pedido.aspx?pedidoId=" + pedidoId + "&check=true");
                }
                else
                {
                    try
                    {
                        //rnPedidos.atualizaStatusRastreio(pedidoId);
                    }
                    catch (Exception ex)
                    {

                    }
                }
            }
            if (Request.QueryString["atualizaExtra"] != null)
            {
                //rnIntegracoes.checaPedidoAvulsoExtra(pedidoId);
                var atualiza = rnIntegracoes.atualizaStatusPedidoExtra(pedidoId, 5);
                Response.Write(atualiza);
            }

            if (Request.QueryString["enviaConfirmacaoDePagamento"] != null)
            {
                if (Request.QueryString["enviaConfirmacaoDePagamento"] == pedidoId.ToString())
                    if (!rnEmails.enviaEmailPagamentoConfirmado(pedidoId))
                        Response.Write("<script>alert('Confirmação de pagamento reenviada com sucesso.');</script>");


            }

        }


        if (Request.QueryString["atualizarcombo"] != null)
        {
            atualizarCombo(Convert.ToInt32(Request.QueryString["atualizarcombo"]));
        }
        if (Request.QueryString["atualizarDesconto"] != null)
        {
            rnPedidos.AtualizaDescontoPedido(int.Parse(Request.QueryString["pedidoId"]));
        }
        if (Request.QueryString["recalcularValores"] != null)
        {
            bool recalcularFrete = Request.QueryString["recalcularFreteTotal"] != null;

            rnPedidos.recalculaTotaisPedido(int.Parse(Request.QueryString["pedidoId"]), recalcularFrete);
            rnPagamento.atualizaDescontoMaximoPedido(int.Parse(Request.QueryString["pedidoId"]));
            rnPedidos.AtualizaDescontoPedido(int.Parse(Request.QueryString["pedidoId"]));
        }
        if (Request.QueryString["recalcularFrete"] != null)
        {
            decimal valorFrete = decimal.Parse(Request.QueryString["recalcularFrete"]);
            int pedidoId = int.Parse(Request.QueryString["pedidoId"]);

            using (var data = new dbCommerceDataContext())
            {
                var pedido = (from c in data.tbPedidos where c.pedidoId == pedidoId select c).First();

                try
                {
                    interacaoInclui("valor do frete alterado de " + (pedido.valorDoFrete ?? 0).ToString("C") + " para " + valorFrete.ToString("C"), "False");
                }
                catch (Exception)
                {

                }

                pedido.valorDoFrete = valorFrete;
                data.SubmitChanges();
            }

            rnPedidos.recalculaTotaisPedido(pedidoId, false);
        }

        if (Request.QueryString["nota"] != null)
        {
            geraNotaClique();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {

        var usuarioLogadoId = Request.Cookies["usuarioLogadoId"];

        if (usuarioLogadoId == null)
            Response.Redirect("default.aspx");

        int usuarioId = Convert.ToInt32(usuarioLogadoId.Value);
        if (usuarioId == 312 || usuarioId == 167)
        {
            ckbGerarNovaNota.Enabled = true;
        }
        if (!Page.IsPostBack)
        {
            fillDrops();
            fillProdutosSemCodigo();
            fillPedido();
            fillRastreios();
            fillNotas();
            PreenchiDropDownAnoVencimentoCartao();
            CarregarComboDepartamento();
            //CarregarDepartamentoOrigem();
            //var xmlNotaFiscal = rnNotaFiscal.gerarNotaFiscalAvulsa();
            //rnNotaFiscal.autorizarNota(xmlNotaFiscal.xml, 0, xmlNotaFiscal.idNotaFiscal);

            ckbGerarNovaNota.Checked = false;

            var permicaoGerarNota = new dbCommerceDataContext();
            ckbGerarNovaNota.Visible = (from c in permicaoGerarNota.tbUsuarioPaginasPermitidas
                                        where c.paginaPermitidaNome == "gerarnovanota" && c.usuarioId == usuarioId
                                        select c).Any();

            ddlCondicaoPagamentoNovaCobranca.Items.FindByValue("31").Enabled = (from c in permicaoGerarNota.tbUsuarioPaginasPermitidas
                                                                                where c.paginaPermitidaNome == "gerarcreditonopedido" && c.usuarioId == usuarioId
                                                                                select c).Any();

            FillInteracoesTransportadora();
            gerarNotaDevolucao();
            selecionarProdutoGerarNotaManualmente();

            Session.Add(Request.QueryString["pedidoId"], null);
        }

        calculoCustoPedido();
        manterCoresStatusProdutos();
    }

    private void CarregarDepartamentoOrigem(bool deptoUsuario)
    {
        var data = new dbCommerceDataContext();

        var usuarioLogadoId = Request.Cookies["usuarioLogadoId"];

        dynamic departamentos;
        if (deptoUsuario)
        {
            departamentos = (from c in data.tbChamadoDepartamentoUsuarios
                             join d in data.tbChamadoDepartamentos on c.idDepartamento equals d.idChamadoDepartamento
                             where c.usuarioId == int.Parse(usuarioLogadoId.Value)
                             select new
                             {
                                 d.idChamadoDepartamento,
                                 d.nomeChamadoDepartamento
                             }).ToList();
        }
        else
        {
            departamentos = (from c in data.tbChamadoDepartamentos select c).ToList();
        }

        ddlDepartamentoOrigem.DataSource = departamentos;
        ddlDepartamentoOrigem.DataTextField = "nomeChamadoDepartamento";
        ddlDepartamentoOrigem.DataValueField = "idChamadoDepartamento";
        ddlDepartamentoOrigem.DataBind();

    }

    private void fillDrops()
    {
        int idPedido = Convert.ToInt32(Request.QueryString["pedidoId"]);

        var data = new dbCommerceDataContext();
        var operadores = (from c in data.tbOperadorPagamentos orderby c.nome select c);
        ddlOperadorPagamentoConfirmacao.DataSource = operadores;
        ddlOperadorPagamentoConfirmacao.DataBind();

        var empresas = (from c in data.tbEmpresas join n in data.tbNotaFiscals on c.idEmpresa equals n.idCNPJ where n.idPedido == idPedido select new { c.nome, c.idEmpresa }).Distinct();
        ddlEmpresasDevolucao.DataSource = empresas;
        ddlEmpresasDevolucao.DataBind();




        var notasReferencia = (from c in data.tbNotaFiscals where c.idPedido == idPedido select new { c.numeroNota, nfeKey = c.nfeKey + "#" + c.idCNPJ });
        ddlNotasReferenciaDevolucao.DataSource = notasReferencia;
        ddlNotasReferenciaDevolucao.DataBind();

        var motivosCancelamentoPedido = (from c in data.tbMotivoCancelamentoPedidos select c);
        ddlMotivosCancelamentoPedido.DataSource = motivosCancelamentoPedido;
        ddlMotivosCancelamentoPedido.DataBind();

        var motivosInclusao = (from c in data.tbItemPedidoTipoAdicaos where c.idItemPedidoTipoAdicao > 1 orderby c.tipoDeAdicao select c).ToList();
        ddlMotivoAdicionarItem.DataSource = motivosInclusao;
        ddlMotivoAdicionarItem.DataBind();
    }

    private void fillPedido()
    {
        //frameClearSale.Attributes["src"] = "clearSaleFrame.aspx?codPedido=" + Request.QueryString["pedidoId"];

        txtValorNovaCobranca.Attributes.Add("onkeyPress", "return moeda(this,'.',',',event);");
        txtNumeroCartaoNovaCobranca.Attributes.Add("onkeypress", "return soNumero(event);");
        txtVencimentoNovaCobranca.Attributes.Add("onkeypress", "Mascaras('DATA', '" + txtVencimentoNovaCobranca.ClientID + "');");


        int pedidoId = Convert.ToInt32(Request.QueryString["pedidoId"]);
        var data = new dbCommerceDataContext();
        var pedido = (from c in data.tbPedidos
                      where c.pedidoId == pedidoId
                      select new
                      {
                          c.pedidoId,
                          c.statusDoPedido,
                          c.clienteId,
                          c.prazoMaximoPedidos,
                          c.prazoFinalPedido,
                          c.tbPedidoSituacao.situacao,
                          c.dataHoraDoPedido,
                          c.dataConfirmacaoPagamento,
                          c.prazoInicioFabricao,
                          c.prazoDeEntrega,
                          c.prazoMaximoPostagemAtualizado,
                          c.tipoDePagamentoId,
                          c.condDePagamentoId,
                          c.tbCondicoesDePagamento.condicaoNome,
                          c.valorCobrado
                      }).FirstOrDefault();
        //pedido = (from c in data.tbPedidos where c.pedidoId == pedidoId select c).FirstOrDefault();
        if (pedido == null)
        {
            Response.Redirect("pedidos.aspx");
        }
        if (pedido.statusDoPedido == 2)
        {
            rnPagamento.verificaPagamentoCompleto(pedido.pedidoId);
        }
        var enviosGeral = (from c in data.tbPedidoEnvios where c.idPedido == pedido.pedidoId select c).ToList();

        var envioPendente = (from c in enviosGeral
                             where c.idPedido == pedido.pedidoId && (c.dataFimSeparacao == null | c.dataInicioEmbalagem == null | c.dataFimEmbalagem == null)
                             select c).Any();

        if (envioPendente)
        {
            //btnAdicionarItemPedido.Visible = false;

            if (!(from c in data.tbItensPedidos where c.pedidoId == pedido.pedidoId && !(c.cancelado ?? false) select c).Any())
            {
                btnAdicionarItemPedido.Visible = true;
            }

            if (pedido.statusDoPedido == 5)
            {
                btnAdicionarItemPedido.Visible = true;
            }
        }

        var usuarioLogadoId = Request.Cookies["usuarioLogadoId"];

        if (usuarioLogadoId != null)
        {
            btnProdutosCancelados.Visible = (from c in data.tbItensPedidos where c.pedidoId == pedido.pedidoId && (c.cancelado ?? false) select new { c.itemPedidoId }).Any()
                                            && rnUsuarios.usuarioPaginasPermitidaSeleciona_PorUsuarioIdPaginaPermitidaNome(int.Parse(usuarioLogadoId.Value), "reativarprodutocanceladonopedido").Tables[0].Rows.Count == 1;

            btnProdutosPendentesDeEnvio.Visible = (from c in data.tbItemPedidoEstoques where c.tbItensPedido.pedidoId == pedido.pedidoId && !c.cancelado && !c.enviado select new { c.itemPedidoId }).Any()
                                                  && rnUsuarios.usuarioPaginasPermitidaSeleciona_PorUsuarioIdPaginaPermitidaNome(int.Parse(usuarioLogadoId.Value), "marcarprodutocomoenviadonopedido").Tables[0].Rows.Count == 1;
        }

        verificaPrazoFinalPedido(pedido.pedidoId, pedido.statusDoPedido, pedido.prazoMaximoPedidos, pedido.prazoFinalPedido);

        var cliente = (from c in data.tbClientes where c.clienteId == pedido.clienteId select c).FirstOrDefault();

        litStatusAtual.Text = pedido.situacao;
        if (pedido.statusDoPedido == 2)
        {
            tdHeaderStatus.Style.Add("background", "#ffc6c7");
            tdHeaderStatus.Style.Add("border", "1px solid #ffc6c7");
        }
        else if (pedido.statusDoPedido == 3 | pedido.statusDoPedido == 4 | pedido.statusDoPedido == 11)
        {
            tdHeaderStatus.Style.Add("background", "#daffcb");
            tdHeaderStatus.Style.Add("border", "1px solid #aaeacd");
        }
        else if (pedido.statusDoPedido == 5)
        {
            tdHeaderStatus.Style.Add("background", "#dbf4fd");
            tdHeaderStatus.Style.Add("border", "1px solid #88c0d5");
        }
        else
        {
            tdHeaderStatus.Style.Add("background", "#f6dbdb");
            tdHeaderStatus.Style.Add("border", "1px solid #dc9c9c");
        }

        txtNumeroDoPedidoCliente.Text = rnFuncoes.retornaIdCliente(pedidoId).ToString();
        txtNumeroDoPedido.Text = (pedidoId).ToString();
        if (pedido.dataHoraDoPedido != null) txtDataDaRealizacao.Text = pedido.dataHoraDoPedido.Value.ToString();
        if (pedido.dataConfirmacaoPagamento != null) txtDataConfirmacaoPagamento.Text = pedido.dataConfirmacaoPagamento.Value.ToString();
        txtCliente.Text = cliente.clienteNome;

        int prazoExibido = pedido.prazoInicioFabricao ?? 0;
        prazoExibido += pedido.prazoMaximoPedidos ?? 0;
        txtPrazoExibido.Text = prazoExibido.ToString();

        if (pedido.statusDoPedido == 5)
        {
            var prazoData = DateTime.Now;
            int prazoFinal = 0;

            var dataCarregamento = (from p in data.tbPedidoPacotes where p.idPedido == pedido.pedidoId select new { p.dataDeCarregamento }).Max(x => x.dataDeCarregamento);
            var dataEmbalagem = (from p in enviosGeral where p.idPedido == pedido.pedidoId select new { p.dataFimEmbalagem }).Max(x => x.dataFimEmbalagem);
            if (dataCarregamento != null)
            {
                prazoData = (DateTime)dataCarregamento;
                prazoFinal = rnFuncoes.retornaPrazoDiasUteis(Convert.ToInt32(pedido.prazoDeEntrega), 0, prazoData);
            }
            else if (dataEmbalagem != null)
            {
                prazoData = (DateTime)dataEmbalagem;
                prazoFinal = rnFuncoes.retornaPrazoDiasUteis(Convert.ToInt32(pedido.prazoDeEntrega) + 1, 0, prazoData);
            }

            var dataEntrega = prazoData.AddDays(prazoFinal).ToShortDateString();
            txtPrazoFinalPosEnvio.Text = dataEntrega;
            pnPrazoPosEnvio.Visible = true;
        }

        if (pedido.prazoFinalPedido != null)
        {
            txtPrazoMaximoPostagem.Text = pedido.prazoFinalPedido.Value.ToShortDateString();
            int prazoFinal = rnFuncoes.retornaPrazoDiasUteis(Convert.ToInt32(pedido.prazoDeEntrega), 0, (DateTime)pedido.prazoFinalPedido);

            txtPrazoMaximoFinal.Text = pedido.prazoFinalPedido.Value.AddDays(prazoFinal).ToShortDateString();

            txtPrazoMaximoPostagemAtualizado.Text = pedido.prazoMaximoPostagemAtualizado == null ? "" : pedido.prazoMaximoPostagemAtualizado.Value.ToShortDateString();

            if (pedido.prazoMaximoPostagemAtualizado != null && pedido.prazoFinalPedido != null)
                if (pedido.prazoMaximoPostagemAtualizado.Value.Date != pedido.prazoFinalPedido.Value.Date)
                {
                    prazoFinal = rnFuncoes.retornaPrazoDiasUteis(Convert.ToInt32(pedido.prazoDeEntrega), 0, (DateTime)pedido.prazoMaximoPostagemAtualizado);
                }

            txtPrazoMaximoFinalAtualizado.Text = pedido.prazoMaximoPostagemAtualizado == null ? "" : pedido.prazoMaximoPostagemAtualizado.Value.AddDays(prazoFinal).ToShortDateString();

            if (pedido.tipoDePagamentoId == 11)
            {
                var cobrancas = (from c in data.tbPedidoPagamentos
                                 where c.pedidoId == pedido.pedidoId && c.cancelado == false
                                 orderby c.dataVencimento
                                 select c).ToList();

                txtPrazoMaximoFinal.Text =
                                cobrancas.OrderByDescending(x => x.dataVencimento)
                                    .First()
                                    .dataVencimento.AddDays(3)
                                    .AddDays(Convert.ToInt32(pedido.prazoDeEntrega))
                                    .ToShortDateString();
            }
        }

        if (pedido.condDePagamentoId != null)
        {
            txtFormaDePagamento.Text = pedido.condicaoNome;
        }

        //var tbPedidoPagamento = (from c in data.tbPedidoPagamentos where c.pedidoId == pedido.pedidoId && c.cancelado == false && c.pagamentoNaoAutorizado == false select c).FirstOrDefault();
        //if (tbPedidoPagamento != null && (pedido.tipoDePagamentoId != 8 && pedido.condDePagamentoId != tbPedidoPagamento.condicaoDePagamentoId))
        //{
        //    txtFormaDePagamento.Text =
        //        (from c in data.tbPedidoPagamentos where c.pedidoId == pedido.pedidoId && c.cancelado == false && c.pagamentoNaoAutorizado == false select c).First
        //            ().tbCondicoesDePagamento.condicaoNome;
        //}

        if (pedido.tipoDePagamentoId == 2 | pedido.tipoDePagamentoId == 6)
        {
            var pagamentosGateway =
                (from c in data.tbPedidoPagamentos
                 where c.pedidoId == pedido.pedidoId && (c.gateway ?? false)
                 select c).Any();
            if (pedido.dataHoraDoPedido > Convert.ToDateTime("06/06/2014"))
            {
                if (pedido.valorCobrado > 300) pnlFraudes.Visible = true;
                if (pagamentosGateway) pnlFraudes.Visible = false;
            }
        }
        if (pedido.tipoDePagamentoId == 8)
        {
            if (pedido.valorCobrado > 300) pnlFraudes.Visible = true;
        }

        decimal totalPedido = Convert.ToDecimal(pedido.valorCobrado);
        txtTotalPedido.Text = totalPedido.ToString("C");
        decimal totalPago = 0;
        var pagamentosPagos = (from c in data.tbPedidoPagamentos
                               where c.pedidoId == pedidoId && c.cancelado == false && c.pago && c.pagamentoPrincipal
                               select c);
        if (pagamentosPagos.Any())
        {
            totalPago = pagamentosPagos.Sum(x => x.valor);
        }
        txtTotalPago.Text = totalPago.ToString("C");

        decimal faltandoPagamento = totalPedido - totalPago;
        txtTotalPendente.Text = faltandoPagamento.ToString("C");

        var pagamentos = (from c in data.tbPedidoPagamentos where c.pedidoId == pedidoId && c.pagamentoPrincipal select c);
        lstPagamentos.DataSource = pagamentos;
        lstPagamentos.DataBind();

        decimal totalPagamentosEmitidos = 0;
        if (pagamentos.Any(x => x.cancelado == false && x.pagamentoNaoAutorizado == false))
        {
            totalPagamentosEmitidos = pagamentos.Where(x => x.cancelado == false && x.pagamentoNaoAutorizado == false).Sum(x => x.valor);
        }
        decimal faltandoEmitirPagamento = totalPedido - totalPagamentosEmitidos;
        txtFaltandoCobranca.Text = faltandoEmitirPagamento.ToString("C");

        if (String.IsNullOrEmpty(txtPrazoMaximoPostagem.Text) && String.IsNullOrEmpty(txtPrazoMaximoFinal.Text))
        {
            var pedidoRefreshDatasPrazos = (from c in data.tbPedidos where c.pedidoId == pedidoId select c).FirstOrDefault();

            if (pedidoRefreshDatasPrazos.prazoFinalPedido != null)
            {
                txtPrazoMaximoPostagem.Text = pedidoRefreshDatasPrazos.prazoFinalPedido.Value.ToShortDateString();
                int prazoFinal = rnFuncoes.retornaPrazoDiasUteis(Convert.ToInt32(pedidoRefreshDatasPrazos.prazoDeEntrega), 0, (DateTime)pedidoRefreshDatasPrazos.prazoFinalPedido);
                txtPrazoMaximoFinal.Text = pedidoRefreshDatasPrazos.prazoFinalPedido.Value.AddDays(prazoFinal).ToShortDateString();

                txtPrazoMaximoPostagemAtualizado.Text = pedido.prazoMaximoPostagemAtualizado == null ? "" : pedido.prazoMaximoPostagemAtualizado.Value.ToShortDateString();
                txtPrazoMaximoFinalAtualizado.Text = pedido.prazoMaximoPostagemAtualizado == null ? "" : pedido.prazoMaximoPostagemAtualizado.Value.AddDays(prazoFinal).ToShortDateString();
            }
        }

    }


    private void fillRastreios()
    {
        int pedidoId = Convert.ToInt32(Request.QueryString["pedidoId"]);
        var pedidoDc = new dbCommerceDataContext();
        var volumes = (from c in pedidoDc.tbPedidoPacotes
                       where c.idPedido == pedidoId
                       orderby c.prazoFinalEntrega
                       select new
                       {
                           c.idPedidoPacote,
                           pedidoId = c.idPedido,
                           c.rastreio,
                           c.tbPedido.prazoFinalPedido,
                           c.prazoFinalEntrega,
                           c.formaDeEnvio,
                           c.statusAtual,
                           despachado = (c.despachado ?? false),
                           c.taxaExtra,
                           c.largura,
                           c.altura,
                           c.profundidade,
                           c.peso
                       });
        lstPacotes.DataSource = volumes;
        lstPacotes.DataBind();




    }
    private void fillNotas()
    {

        int pedidoId = Convert.ToInt32(Request.QueryString["pedidoId"]);

        var pedidoDc = new dbCommerceDataContext();
        IEnumerable<tbNotaFiscal> notas = (from c in pedidoDc.tbNotaFiscals
                                           where c.idPedido == pedidoId
                                           select c).ToList<tbNotaFiscal>();
        foreach (var item in notas)
        {
            item.nfeKey = item.nfeKey + rnNotaFiscal.GerarDigitoVerificadorNFe2(item.nfeKey);
        }
        lstNotas.DataSource = notas;
        lstNotas.DataBind();
    }

    private void calculoCustoPedido()
    {
        var usuarioLogadoId = Request.Cookies["usuarioLogadoId"];

        if (usuarioLogadoId != null)
            if (rnUsuarios.usuarioPaginasPermitidaSeleciona_PorUsuarioIdPaginaPermitidaNome(int.Parse(usuarioLogadoId.Value), "custodopedido").Tables[0].Rows.Count != 1)
                return;

        decimal custoDoPedido = 0;
        decimal freteEstimado = 0;
        decimal freteFinal = 0;
        int pedidoId = Convert.ToInt32(Request.QueryString["pedidoId"]);

        using (var data = new dbCommerceDataContext())
        {
            var temItens =
                (from c in data.tbItensPedidos where c.pedidoId == pedidoId && (c.cancelado ?? false) == false select c)
                    .Any();

            if (temItens)
            {
                custoDoPedido = (from c in data.tbItensPedidos where c.pedidoId == pedidoId && (c.cancelado ?? false) == false select c).Sum(x => ((x.valorCusto ?? 0) * x.itemQuantidade));
                freteEstimado = (from c in data.tbPedidos where c.pedidoId == pedidoId select c).Sum(x => (x.valorDoFrete ?? 0));
                var fretePeso = (from c in data.tbPedidoEnvios where c.idPedido == pedidoId select c).ToList();
                if (fretePeso != null) freteFinal = fretePeso.Sum(x => (x.valorSelecionado ?? 0));
            }

        }

        if (custoDoPedido > 0)
        {
            lblCustoPedido.Text = "Custo dos produtos: " + custoDoPedido.ToString("c") + " - Frete estimado: " + freteEstimado.ToString("c") + " - Frete final: " + (freteFinal > 0 ? freteFinal.ToString("c") : "não calculado");
            lblCustoPedido.Text += "<br>Custo Final: " + (custoDoPedido + (freteFinal > 0 ? freteFinal : freteEstimado)).ToString("C");

            lblCustoPedido.Visible = true;

        }
    }

    private void gerarNotaDevolucao()
    {
        var usuarioLogadoId = Request.Cookies["usuarioLogadoId"];

        if (usuarioLogadoId != null)
            btShowModalNotaDevolucao.Visible =
                rnUsuarios.usuarioPaginasPermitidaSeleciona_PorUsuarioIdPaginaPermitidaNome(
                    int.Parse(usuarioLogadoId.Value), "gerarnotadevolucao").Tables[0].Rows.Count == 1;

    }

    private void selecionarProdutoGerarNotaManualmente()
    {
        var usuarioLogadoId = Request.Cookies["usuarioLogadoId"];

        if (usuarioLogadoId != null)
            lstProdutoNota.DataSourceID =
                rnUsuarios.usuarioPaginasPermitidaSeleciona_PorUsuarioIdPaginaPermitidaNome(
                    int.Parse(usuarioLogadoId.Value), "selecionarprodutogerarnota").Tables[0].Rows.Count == 1 ? "sqlProdutosNota" : "";

    }

    protected void dtvPedido_DataBound(object sender, EventArgs e)
    {
        DataRowView rowView = (DataRowView)dtvPedido.DataItem;

        Image imgDadosDoCartao = (Image)dtvPedido.FindControl("imgDadosDoCartao");
        HyperLink hplnkTracking = (HyperLink)dtvPedido.FindControl("hplnkTracking");
        TextBox txtNomeEntrega = (TextBox)dtvPedido.FindControl("txtNomeEntrega");
        DropDownList ddlTipoDeEntrega = (DropDownList)dtvPedido.FindControl("ddlTipoDeEntrega");
        DropDownList ddlEnviarPor = (DropDownList)dtvPedido.FindControl("ddlEnviarPor");
        DropDownList ddlUsuarioSeparacao = (DropDownList)dtvPedido.FindControl("ddlUsuarioSeparacao");
        DropDownList ddlUsuarioEmbalagem = (DropDownList)dtvPedido.FindControl("ddlUsuarioEmbalagem");
        TextBox txtRua = (TextBox)dtvPedido.FindControl("txtRua");
        TextBox txtNumero = (TextBox)dtvPedido.FindControl("txtNumero");
        TextBox txtComplemento = (TextBox)dtvPedido.FindControl("txtComplemento");
        TextBox txtBairro = (TextBox)dtvPedido.FindControl("txtBairro");
        TextBox txtCidade = (TextBox)dtvPedido.FindControl("txtCidade");
        TextBox txtEstado = (TextBox)dtvPedido.FindControl("txtEstado");
        TextBox txtCep = (TextBox)dtvPedido.FindControl("txtCep");
        TextBox txtReferencia = (TextBox)dtvPedido.FindControl("txtReferencia");
        TextBox txtDespesaValor = (TextBox)dtvPedido.FindControl("txtDespesaValor");
        CheckBox chkPago = (CheckBox)dtvPedido.FindControl("chkPago");
        Panel pnPagamento = (Panel)dtvPedido.FindControl("pnPagamento");
        Panel pnDoisPagamentos = (Panel)dtvPedido.FindControl("pnDoisPagamentos");
        Label lblCep = (Label)dtvPedido.FindControl("lblCep");
        ListView lstRastreios = (ListView)dtvPedido.FindControl("lstRastreios");
        Button btnCancelarPedido = (Button)dtvPedido.FindControl("btnCancelarPedido");
        Button btnPriorizarPedido = (Button)dtvPedido.FindControl("btnPriorizarPedido");
        Button btnPedidoEnviado = (Button)dtvPedido.FindControl("btnPedidoEnviado");
        Button btnReenviarPedido = (Button)dtvPedido.FindControl("btnReenviarPedido");
        Button btnReenviarConfirmacaoDePagamento = (Button)dtvPedido.FindControl("btnReenviarConfirmacaoDePagamento");
        Button btnDescancelarPedido = (Button)dtvPedido.FindControl("btnDescancelarPedido");

        HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
        usuarioLogadoId = Request.Cookies["usuarioLogadoId"];
        if (usuarioLogadoId != null)
        {
            if (rnUsuarios.usuarioPaginasPermitidaSeleciona_PorUsuarioIdPaginaPermitidaNome(int.Parse(usuarioLogadoId.Value.ToString()), "alterarstatuspedidos").Tables[0].Rows.Count == 0)
            {
                btnPedidoEnviado.Visible = false;
                btnReenviarPedido.Visible = false;
            }
            if (rnUsuarios.usuarioPaginasPermitidaSeleciona_PorUsuarioIdPaginaPermitidaNome(int.Parse(usuarioLogadoId.Value.ToString()), "priorizarpedido").Tables[0].Rows.Count == 0)
            {
                btnPriorizarPedido.Visible = false;
            }
        }

        //txtDespesaValor.Attributes.Add("onkeyPress", "return moeda(this,'.',',',event);");


        var cepsDc = new dbCommerceDataContext();
        try
        {
            int pedidoIdCep = Convert.ToInt32(Request.QueryString["pedidoId"]);

            string cepOut = (from c in cepsDc.tbPedidos where c.pedidoId == pedidoIdCep select c).FirstOrDefault().endCep.Replace("-", "");// lblCep.Text.Replace("-", "");

            var endereco = (from c in cepsDc.tbCepEnderecos where c.cep == cepOut.Replace("-", "") select c).FirstOrDefault();

            if (endereco != null)
            {
                var cidade = (from c in cepsDc.tbCepCidades where c.id_cidade == endereco.id_cidade select c).FirstOrDefault();
                if (cidade != null)
                {
                    txtCodigoIbge.Text =
                    txtCodigoIbgeDevolucao.Text = cidade.cod_ibge;
                    txtCodigoIbgeDevolucao2.Text = cidade.cod_ibge;
                }
            }

        }
        catch (Exception)
        {

        }


        /*Label lblCustoProdutos = (Label)dtvPedido.FindControl("lblCustoProdutos");
        decimal totalCustos = 0;
        foreach (DataListItem item in dtlItensPedido.Items)
        {
            decimal custo = 0;
            int quantidade = 0;
            TextBox txtValorCusto = (TextBox)item.FindControl("txtValorCusto");
            Label lblQuantidade = (Label)item.FindControl("lblQuantidade");
            decimal.TryParse(txtValorCusto.Text, out custo);
            int.TryParse(lblQuantidade.Text, out quantidade);
            if (custo > 0 && quantidade > 0) totalCustos = totalCustos + (custo * quantidade);
        }
        lblCustoProdutos.Text = totalCustos.ToString("0.00");*/

        var data = new dbCommerceDataContext();
        var tiposDeEnvio = (from c in data.tbTransportadoras select c).ToList();
        ddlEnviarPor.DataSource = tiposDeEnvio;
        ddlEnviarPor.DataBind();

        var usuariosExpedicao = (from c in data.tbUsuarioExpedicaos where c.ativo == true select c).ToList();
        ddlUsuarioSeparacao.DataSource = usuariosExpedicao;
        ddlUsuarioSeparacao.DataBind();

        ddlUsuarioEmbalagem.DataSource = usuariosExpedicao;
        ddlUsuarioEmbalagem.DataBind();

        HtmlAnchor linkGoogle = (HtmlAnchor)dtvPedido.FindControl("linkGoogle");
        string tipodeentrega = rowView.Row["tipoDeEntregaId"].ToString();
        if (String.IsNullOrEmpty(tipodeentrega) || tipodeentrega == "0")
        {
            tipodeentrega = "9";
        }
        //ddlTipoDeEntrega.SelectedValue = rowView.Row["tipoDeEntregaId"].ToString();
        ddlTipoDeEntrega.SelectedValue = tipodeentrega;
        ddlEnviarPor.SelectedValue = rowView.Row["formaDeEnvio"].ToString();

        if (rowView.Row["statusDoPedido"].ToString() == "6")
        {
            btnCancelarPedido.Visible = false;
            btnDescancelarPedido.Visible = true;
        }
        if (rowView.Row["statusDoPedido"].ToString() == "5")
        {
            btnPedidoEnviado.Visible = false;
        }
        if (rowView.Row["statusDoPedido"].ToString() != "5")
        {
            btnReenviarPedido.Visible = false;
        }

        if (rowView.Row["statusDoPedido"].ToString() == "3" | rowView.Row["statusDoPedido"].ToString() == "4" | rowView.Row["statusDoPedido"].ToString() == "11")
        {
            btnReenviarConfirmacaoDePagamento.Visible = true;
        }

        if (rowView.Row["endNomeDoDestinatario"].ToString() != string.Empty)
        {
            txtNomeEntrega.Text = rowView.Row["endNomeDoDestinatario"].ToString();
            txtRua.Text = rowView.Row["endRua"].ToString();
            txtNumero.Text = rowView.Row["endNumero"].ToString();
            txtComplemento.Text = rowView.Row["endComplemento"].ToString();
            txtBairro.Text = rowView.Row["endBairro"].ToString();
            txtCidade.Text = rowView.Row["endCidade"].ToString();
            txtEstado.Text = rowView.Row["endEstado"].ToString();
            txtCep.Text = rowView.Row["endCep"].ToString();
            txtReferencia.Text = rowView.Row["endReferenciaParaEntrega"].ToString();
        }
        else
        {
            txtRua.Text = rowView.Row["clienteRua"].ToString();
            txtNumero.Text = rowView.Row["clienteNumero"].ToString();
            txtComplemento.Text = rowView.Row["clienteComplemento"].ToString();
            txtBairro.Text = rowView.Row["clienteBairro"].ToString();
            txtCidade.Text = rowView.Row["clienteCidade"].ToString();
            txtEstado.Text = rowView.Row["clienteEstado"].ToString();
            txtCep.Text = rowView.Row["clienteCep"].ToString();
            txtReferencia.Text = rowView.Row["clienteReferenciaParaEntrega"].ToString();
        }
        linkGoogle.HRef = "https://www.google.com/maps?q=" + txtRua.Text + " " + txtNumero.Text + " - " + txtCidade.Text + " - " + txtEstado.Text;


        decimal valorPedido = Convert.ToDecimal(rowView.Row["valorCobrado"].ToString());
        var dataHoraDoPedido = Convert.ToDateTime(rowView.Row["dataHoraDoPedido"].ToString());


        if (rowView.Row["numeroDoTracking"].ToString() != string.Empty)
        {
            hplnkTracking.NavigateUrl = String.Format("http://websro.correios.com.br/sro_bin/txect01$.QueryList?P_COD_UNI={0}&P_LINGUA=001&P_TIPO=001", rowView.Row["numeroDoTracking"].ToString());
        }

        int pedidoId = Convert.ToInt32(Request.QueryString["pedidoId"]);
        var pedidoDc = new dbCommerceDataContext();

        var infoPedido = (from c in pedidoDc.tbPedidos where c.pedidoId == pedidoId select c).First();
        GerarEmailPrazosEntrega(infoPedido);

        if (infoPedido.prazoFinalPedido != null)
        {
            litPrazoMaximoFabricacao.Text = infoPedido.prazoFinalPedido.Value.ToShortDateString();
            litPrazoCepEnvio.Text = txtCep.Text;
            litPrazoCorreios.Text = infoPedido.prazoDeEntrega;
        }
        else
        {
            tbPedidoFornecedorItem pedidoItensFornecedor;
            using (
                var txn = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required,
                    new System.Transactions.TransactionOptions
                    {
                        IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                    }))

            {
                pedidoItensFornecedor = (from c in pedidoDc.tbPedidoFornecedorItems
                                         where c.idPedido == pedidoId
                                         orderby c.tbPedidoFornecedor.dataLimite descending
                                         select c).FirstOrDefault();
            }

            int totalDiasFabricacao = 0;
            int prazoDeEntrega = 0;
            if (pedidoItensFornecedor != null)
            {
                litPrazoMaximoFabricacao.Text = pedidoItensFornecedor.tbPedidoFornecedor.dataLimite.AddDays(3).ToShortDateString();
                var produtosDc = new dbCommerceDataContext();
                var produtoPrazoMaximo = (from c in produtosDc.tbProdutos
                                          where
                                              c.produtoFornecedor == pedidoItensFornecedor.tbPedidoFornecedor.idFornecedor &&
                                              c.produtoAtivo.ToLower() == "true"
                                          select c).FirstOrDefault();
                if (produtoPrazoMaximo != null)
                {
                    litPrazoMaiorFornecedor.Text = produtoPrazoMaximo.disponibilidadeEmEstoque;
                }
                totalDiasFabricacao = Convert.ToInt32((pedidoItensFornecedor.tbPedidoFornecedor.dataLimite.AddDays(3) - DateTime.Now).TotalDays);
            }
            var pedido = (from c in pedidoDc.tbPedidos where c.pedidoId == pedidoId select c).FirstOrDefault();
            if (pedido != null)
            {
                litPrazoCorreios.Text = pedido.prazoDeEntrega;
                int.TryParse(pedido.prazoDeEntrega, out prazoDeEntrega);
                if (pedido.dataConfirmacaoPagamento != null)
                {
                    litPrazoCepEnvio.Text = pedido.endCep;
                    litDataConfirmacaoPagamento.Text =
                        Convert.ToDateTime(pedido.dataConfirmacaoPagamento).ToShortDateString();
                }
                else
                {
                    litPrazoCepEnvio.Text = "aguardando confirmação de pagamento";
                    litPrazoCorreios.Text = "";
                }
            }
            if (totalDiasFabricacao >= 0)
            {
                int finsDeSemana = 0;

                for (int i = 1; i <= prazoDeEntrega; i++)
                {
                    if (pedido.dataConfirmacaoPagamento != null)
                    {
                        var dataContada = Convert.ToDateTime(pedido.dataConfirmacaoPagamento).AddDays(i).DayOfWeek;
                        if (dataContada == DayOfWeek.Saturday | dataContada == DayOfWeek.Sunday) finsDeSemana++;
                    }

                }
                litPrazoEntrega.Text = (totalDiasFabricacao + prazoDeEntrega + finsDeSemana).ToString();
            }
            else
            {
                litPrazoEntrega.Text = (totalDiasFabricacao * (-1)).ToString() + " dias de atraso";
            }

            //var pedidosFornencedorItens = (from c in pedidoDc.tbPedidoFornecedorItems where c.idPedido == pedidoId select new { c.tbPedidoFornecedor.idFornecedor, c.tbPedidoFornecedor.idPedidoFornecedor }).Distinct().ToList();

        }


        if (!String.IsNullOrEmpty(txtPrazoMaximoFinal.Text))
            litPrazoEntrega.Text = (Convert.ToDateTime(txtPrazoMaximoFinal.Text) - DateTime.Now).TotalDays > 0 ? Convert.ToInt32((Convert.ToDateTime(txtPrazoMaximoFinal.Text) - DateTime.Now).TotalDays).ToString() : "0";
        if (!String.IsNullOrEmpty(txtPrazoMaximoFinalAtualizado.Text))
            litPrazoEntrega.Text = (Convert.ToDateTime(txtPrazoMaximoFinalAtualizado.Text) - DateTime.Now).TotalDays > 0 ? Convert.ToInt32((Convert.ToDateTime(txtPrazoMaximoFinalAtualizado.Text) - DateTime.Now).TotalDays).ToString() : "0";

    }

    protected void imbSalvarTraking_Click(object sender, ImageClickEventArgs e)
    {
        var pedidoContext = new dbCommerceDataContext();
        int pedidoId = int.Parse(Request.QueryString["pedidoId"]);
        var pedido = (from c in pedidoContext.tbPedidos where c.pedidoId == pedidoId select c).First();

        TextBox txtTracking = (TextBox)dtvPedido.FindControl("txtTracking");
        TextBox txtTracking2 = (TextBox)dtvPedido.FindControl("txtTracking2");
        TextBox txtTracking3 = (TextBox)dtvPedido.FindControl("txtTracking3");
        TextBox txtTracking4 = (TextBox)dtvPedido.FindControl("txtTracking4");
        TextBox txtNomeDoCliente = (TextBox)dtvPedido.FindControl("txtNomeDoCliente");
        TextBox txtEmail = (TextBox)dtvPedido.FindControl("txtEmail");
        rnPedidos.pedidoAlteraTraking(txtTracking.Text, txtTracking2.Text, txtTracking3.Text, txtTracking4.Text, int.Parse(Request.QueryString["pedidoId"]));

        if (pedido.marketplace == null | pedido.marketplace == false)
        {
            rnEmails.enviaCodigoDoTraking(txtNomeDoCliente.Text, Request.QueryString["pedidoId"], txtTracking.Text, txtTracking2.Text, txtTracking3.Text, txtTracking4.Text, txtEmail.Text);
        }
        else if (pedido.marketplace == true && !string.IsNullOrEmpty(pedido.marketplaceId) && pedido.condDePagamentoId == rnIntegracoes.rakutenCondicaoDePagamentoId)
        {
            rnIntegracoes.atualizaStatusPedidoRakuten(pedidoId, 5);
        }
        else if (pedido.marketplace == true && !string.IsNullOrEmpty(pedido.marketplaceId) && pedido.condDePagamentoId == rnIntegracoes.extraCondicaoDePagamentoId)
        {
            rnIntegracoes.atualizaStatusPedidoExtra(pedidoId, 5);
        }
        else if (pedido.marketplace == true && !string.IsNullOrEmpty(pedido.marketplaceId) && pedido.condDePagamentoId == mlIntegracao.mlCondicaoDePagamentoId)
        {
            mlIntegracao.atualizaStatusPedidoMl(pedidoId, 5);
        }
        var interacaoTracking = new StringBuilder();
        if (txtTracking.Text != "") interacaoTracking.AppendFormat("Tracking 1: " + txtTracking.Text + "<br>");
        if (txtTracking2.Text != "") interacaoTracking.AppendFormat("Tracking 2: " + txtTracking2.Text);
        if (txtTracking3.Text != "") interacaoTracking.AppendFormat("Tracking 3: " + txtTracking3.Text);
        if (txtTracking4.Text != "") interacaoTracking.AppendFormat("Tracking 4: " + txtTracking4.Text);

        interacaoInclui(interacaoTracking.ToString(), "False");
        Response.Write("<script>alert('Código do traking alterado com sucesso.');</script>");
    }

    protected void drpSituacao_DataBound(object sender, EventArgs e)
    {
        string situacaoId = rnPedidos.pedidoSeleciona_PorPedidoId(int.Parse(Request.QueryString["pedidoId"])).Tables[0].Rows[0]["statusDoPedido"].ToString();
        drpSituacao.Items.FindByValue(situacaoId.ToString()).Selected = true;
    }

    protected void drpSituacaoInterna_DataBound(object sender, EventArgs e)
    {
        var pedidoContext = new dbCommerceDataContext();
        int pedidoId = int.Parse(Request.QueryString["pedidoId"]);
        var situacaoatual = (from c in pedidoContext.tbPedidos where c.pedidoId == pedidoId select c).First().statusInternoPedido;

        if (situacaoatual != null)
        {
            string situacaoInternaId = situacaoatual.ToString();
            drpSituacaoInterna.Items.FindByValue(situacaoInternaId.ToString()).Selected = true;
        }
    }

    protected void btSalvarInteracao_Click(object sender, ImageClickEventArgs e)
    {
        string interacao = txtInteracao.Text;
        if (!string.IsNullOrEmpty(ddlEmailInteracao.SelectedValue))
        {
            rnEmails.EnviaEmail("atendimento@graodegente.com.br", ddlEmailInteracao.SelectedValue, "", "", "", txtInteracao.Text, "Grão de Gente - Informações do Pedido: " + Request.QueryString["pedidoId"]);
            interacao = interacao + "<br><br><b>Interação enviada por email para: </b><br>" + ddlEmailInteracao.SelectedItem.Text + " - " + ddlEmailInteracao.SelectedValue;
        }
        interacaoInclui(interacao, "False");
        Response.Write("<script>alert('Interação incluida com sucesso.');</script>");
    }

    protected void imbEnviarInteracaoParaCliente_Click(object sender, ImageClickEventArgs e)
    {
        interacaoInclui(txtInteracao.Text, "False");
        TextBox txtEmail = (TextBox)dtvPedido.FindControl("txtEmail");
        if (rnEmails.enviaInterecao(txtInteracao.Text, txtEmail.Text))
            Response.Write("<script>alert('Interação enviada com sucesso.');</script>");
        else
            Response.Write("<script>alert('SInteração não foi enviada.');</script>");
    }

    protected void imbAlterarSituacao_Click(object sender, ImageClickEventArgs e)
    {
        HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
        usuarioLogadoId = Request.Cookies["usuarioLogadoId"];
        string usuario = "";
        if (usuarioLogadoId != null)
        {
            usuario = rnUsuarios.usuarioSeleciona_PorUsuarioId(int.Parse(usuarioLogadoId.Value.ToString())).Tables[0].Rows[0]["usuarioNome"].ToString();
        }
        int pedidoId = int.Parse(Request.QueryString["pedidoId"]);

        var pedidoContext = new dbCommerceDataContext();
        var pedido = (from c in pedidoContext.tbPedidos where c.pedidoId == pedidoId select c).First();


        var situacaoatual = pedido.statusDoPedido;
        var situacaointernoatual = pedido.statusInternoPedido;



        if (situacaoatual.ToString() != drpSituacao.SelectedItem.Value.ToString())
        {

            if (drpSituacao.SelectedValue == "5")
            {
                rnPedidos.alteraEstoqueProdutosPorPedido(pedidoId);
            }

            if (drpSituacao.SelectedValue == "6")
            {
                rnPedidos.retiraVinculoPedidoFornecedor(pedidoId);
            }

            interacaoInclui(drpSituacao.SelectedItem.Text, "True");

            if (drpSituacao.SelectedItem.Value == "3")
            {
                rnPedidos.confirmarPagamentoPedido(pedidoId, usuario);
            }
            else if (rnPedidos.pedidoAlteraStatus(int.Parse(drpSituacao.SelectedItem.Value), int.Parse(Request.QueryString["pedidoId"])))
            {
                if (pedido.marketplace == true)
                {
                    if (pedido.condDePagamentoId == rnIntegracoes.rakutenCondicaoDePagamentoId)
                    {
                        if (drpSituacao.SelectedItem.Value == "11" | drpSituacao.SelectedItem.Value == "9" | drpSituacao.SelectedItem.Value == "4")
                        {
                            try
                            {
                                rnIntegracoes.atualizaStatusPedidoRakuten(pedidoId, Convert.ToInt32(drpSituacao.SelectedItem.Value));
                            }
                            catch (Exception)
                            {

                            }
                        }
                    }
                }

                TextBox txtNomeDoCliente = (TextBox)dtvPedido.FindControl("txtNomeDoCliente");
                TextBox txtEmail = (TextBox)dtvPedido.FindControl("txtEmail");


                if (drpSituacao.SelectedValue == "7" && pedido.marketplace != true)
                {
                    rnEmails.enviaEmailPagamentoRecusado(pedidoId);
                }
                else if (drpSituacao.SelectedValue == "6" && pedido.marketplace != true)
                {
                    rnEmails.enviaEmailPedidoCancelado(pedidoId);
                }
                else if (drpSituacao.SelectedValue == "11" && pedido.marketplace != true)
                {
                    rnEmails.enviaEmailPedidoProcessado(pedidoId);
                }

                if (drpSituacao.SelectedItem.Value != "10" && situacaoatual != 10 && pedido.marketplace != true)
                {
                    //rnEmails.enviaAlteracaoDeStatus(txtNomeDoCliente.Text, Request.QueryString["pedidoId"].ToString(), drpSituacao.SelectedItem.Text, txtEmail.Text);
                }

                if (drpSituacao.SelectedItem.Value == "6")
                {
                    /*DataTable dtt = rnPedidos.itensPedidoSelecionaAdmin_PorPedidoId(int.Parse(Request.QueryString["pedidoId"])).Tables[0];

                    int produtoEstoqueAtual;
                    int produtoItensVendido;
                    foreach (DataRow dr in dtt.Rows)
                    {
                        produtoEstoqueAtual = int.Parse(rnProdutos.produtoSeleciona_PorProdutoId(int.Parse(dr["produtoId"].ToString())).Tables[0].Rows[0]["produtoEstoqueAtual"].ToString());
                        produtoItensVendido = int.Parse(rnPedidos.itensPedidoSeleciona_PorItemPedidoId(int.Parse(dr["itemPedidoId"].ToString())).Tables[0].Rows[0]["itemQuantidade"].ToString());
                        produtoEstoqueAtual = produtoEstoqueAtual + produtoItensVendido;

                        rnProdutos.produtoAlteraEstoque(produtoEstoqueAtual, int.Parse(dr["produtoId"].ToString()));
                    }*/
                }

                if (drpSituacao.SelectedItem.Value == "5" || drpSituacao.SelectedItem.Value == "6" || drpSituacao.SelectedItem.Value == "7")
                {
                    string situacaoFcontrol = string.Empty;
                    switch (drpSituacao.SelectedItem.Value)
                    {
                        case "5":
                            situacaoFcontrol = "enviado";
                            break;
                        case "6":
                            situacaoFcontrol = "cancelado";
                            break;
                        case "7":
                            situacaoFcontrol = "cancelado_suspeita";
                            break;
                    }
                }
                if (drpSituacao.SelectedValue == "9" | drpSituacao.SelectedValue == "11")
                {
                    var produtosNoPedido = new List<pedidoEmail>();
                    foreach (ListViewItem itemPedido in dtlItensPedido.Items)
                    {
                        pedidoEmail produtoNoPedido = new pedidoEmail();

                        HiddenField txtItemPedidoId = (HiddenField)itemPedido.FindControl("txtItemPedidoId");
                        int itemPedidoId = Convert.ToInt32(txtItemPedidoId.Value);
                        var itemPedidoData = new dbCommerceDataContext();
                        var itemPedidoRow =
                            (from c in itemPedidoData.tbItensPedidos where c.itemPedidoId == itemPedidoId select c).
                                FirstOrDefault();
                        if (itemPedidoRow != null)
                        {
                            var produtoData = new dbCommerceDataContext();
                            var produto = (from c in produtoData.tbProdutos where c.produtoId == itemPedidoRow.produtoId select c).FirstOrDefault();
                            if (produto != null)
                            {
                                var fornecedorData = new dbCommerceDataContext();
                                var fornecedor = (from c in fornecedorData.tbProdutoFornecedors where c.fornecedorId == produto.produtoFornecedor select c).First();
                                int prazoDeFabricacao = 0;
                                if (fornecedor.fornecedorPrazoDeFabricacao != null) prazoDeFabricacao = Convert.ToInt32(fornecedor.fornecedorPrazoDeFabricacao);
                                itemPedidoRow.prazoDeFabricacao = DateTime.Now.AddDays(prazoDeFabricacao);
                                itemPedidoRow.entreguePeloFornecedor = itemPedidoRow.entreguePeloFornecedor ?? false;
                                itemPedidoData.SubmitChanges();

                                if (produto.produtoPrecoDeCusto != null) produtoNoPedido.produtoPrecoDeCusto = Convert.ToDecimal(produto.produtoPrecoDeCusto);
                                produtoNoPedido.idFornecedor = fornecedor.fornecedorId;
                                produtoNoPedido.fornecedorNome = fornecedor.fornecedorNome;
                                produtoNoPedido.fornecedorEmail = fornecedor.fornecedorEmail;
                                produtoNoPedido.produtoNome = produto.produtoNome;
                                produtoNoPedido.produtoIdDaEmpresa = produto.produtoIdDaEmpresa;
                                produtoNoPedido.produtoQuantidade = Convert.ToInt32(itemPedidoRow.itemQuantidade);
                                produtoNoPedido.produtoPrazo = DateTime.Now.AddDays(prazoDeFabricacao).ToShortDateString();
                                produtoNoPedido.produtoFoto = ConfigurationManager.AppSettings["caminhoCDN"] + "fotos/" + produto.produtoId + "/" + "pequena_" + produto.fotoDestaque + ".jpg";
                                produtosNoPedido.Add(produtoNoPedido);
                            }

                        }
                    }
                }
            }
        }



        if (situacaointernoatual.ToString() != drpSituacaoInterna.SelectedItem.Value)
        {
            var pedidoContext2 = new dbCommerceDataContext();
            interacaoInclui("Situação interna: " + drpSituacaoInterna.SelectedItem.Text, "False");
            var pedidoSituacaoInterna = (from c in pedidoContext2.tbPedidos where c.pedidoId == pedidoId select c).First();
            pedidoSituacaoInterna.statusInternoPedido = Convert.ToInt32(drpSituacaoInterna.SelectedItem.Value);
            pedidoContext2.SubmitChanges();
        }



        Response.Write("<script>alert('Situação alterada com sucesso.');</script>");
    }

    public void interacaoInclui(string interacao, string alteracaoDeEstatus)
    {
        HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
        usuarioLogadoId = Request.Cookies["usuarioLogadoId"];
        if (usuarioLogadoId != null)
        {
            rnInteracoes.interacaoInclui(int.Parse(Request.QueryString["pedidoId"]), interacao, rnUsuarios.usuarioSeleciona_PorUsuarioId(int.Parse(usuarioLogadoId.Value)).Tables[0].Rows[0]["usuarioNome"].ToString(), alteracaoDeEstatus);
        }

        dtlInteracao.DataBind();
    }

    int itemPedidoIdCombo = 0;
    int pedidoIdCombo = 0;
    decimal itemQuantidadeCombo = 0;
    bool canceladoCombo = false;
    bool brindeCombo = false;
    bool enviadoCombo = false;


    protected void dtlItensPedido_OnDataBound(object sender, EventArgs args)
    {

        var data = new dbCommerceDataContext();
        int pedidoId = int.Parse(Request.QueryString["pedidoId"]);



        var itensCombo = (from c in data.tbItemPedidoEstoques
                          where c.tbItensPedido.pedidoId == pedidoId
                          select new
                          {
                              produtoNome = c.tbProduto.produtoNome,
                              c.produtoId,
                              c.tbProduto.produtoPreco,
                              c.tbProduto.produtoIdDaEmpresa,
                              c.tbProduto.produtoCodDeBarras,
                              c.tbProduto.tbProdutoFornecedor.fornecedorNome,
                              c.tbProduto.produtoPrecoDeCusto,
                              c.tbProduto.ncm,
                              c.tbProduto.cfop,
                              preco =
                                  (c.tbProduto.produtoPrecoPromocional ?? 0) > 0
                                      ? (decimal)c.tbProduto.produtoPrecoPromocional
                                      : c.tbProduto.produtoPreco,
                              precoNoCombo = c.precoDeCusto,
                              c.itemPedidoId,
                              c.idItemPedidoEstoque,
                              c.autorizarParcial,
                              c.enviado,
                              c.idCentroDistribuicao,
                              motivoAdicao = c.tbItensPedido.idItemPedidoTipoAdicao > 1 ? c.tbItensPedido.tbItemPedidoTipoAdicao.tipoDeAdicao : ""
                          }).ToList();


        List<tbProdutoEstoque> reservasGeral = new List<tbProdutoEstoque>();

        using (var txn = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required,
            new System.Transactions.TransactionOptions
            {
                IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
            }))

        {

            reservasGeral = (from c in data.tbProdutoEstoques
                             where c.pedidoIdReserva == pedidoId
                             select c).ToList();
        }


        List<tbItemPedidoEstoque> produtosAguardandoEstoque = new List<tbItemPedidoEstoque>();

        using (var txn = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required,
            new System.Transactions.TransactionOptions
            {
                IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
            }))

        {

            produtosAguardandoEstoque = (from c in data.tbItemPedidoEstoques
                                         where
                                             c.dataLimite != null && c.cancelado == false && c.reservado == false &&
                                             c.tbItensPedido.pedidoId == pedidoId
                                         select c).ToList();
        }

        var aguardandoGeral = (from c in data.tbItemPedidoEstoques
                               where
                                   produtosAguardandoEstoque.Select(x => x.produtoId).Contains(c.produtoId) && c.reservado == false &&
                                   c.cancelado == false && c.dataLimite != null
                               orderby c.dataLimite
                               select new
                               {
                                   c.idItemPedidoEstoque,
                                   c.tbItensPedido.pedidoId,
                                   c.dataCriacao,
                                   dataLimiteReserva = c.dataLimite,
                                   c.produtoId,
                                   c.itemPedidoId
                               }).ToList();
        var pedidosFornecedorGeral = (from c in data.tbPedidoFornecedorItems
                                      join d in data.tbProdutoFornecedors on c.tbPedidoFornecedor.idFornecedor equals d.fornecedorId
                                      join e in data.tbProdutoEstoques on c.idPedidoFornecedorItem equals e.idPedidoFornecedorItem into produtoEstoque
                                      where
                                          (c.entregue == false | produtoEstoque.Any(x => (x.liberado ?? false) == false)) && (c.motivo ?? "") == "" &&
                                          produtosAguardandoEstoque.Select(x => x.produtoId).Contains(c.idProduto)
                                      orderby c.tbPedidoFornecedor.dataLimite, c.tbPedidoFornecedor.idPedidoFornecedor
                                      select new
                                      {
                                          c.idPedidoFornecedor,
                                          c.idPedidoFornecedorItem,
                                          dataLimiteFornecedor = c.tbPedidoFornecedor.dataLimite,
                                          c.idProduto,
                                          d.fornecedorNome,
                                          c.entregue,
                                          c.tbPedidoFornecedor.dataPrevista
                                      }).ToList();
        var previsaoEntregaItens = new StringBuilder();
        foreach (var produtoAguardandoEstoque in produtosAguardandoEstoque)
        {
            var aguardando = (from c in aguardandoGeral
                              where c.produtoId == produtoAguardandoEstoque.produtoId
                              orderby c.dataLimiteReserva
                              select c).ToList();
            var pedidoFornecedor = (from c in pedidosFornecedorGeral
                                    where c.idProduto == produtoAguardandoEstoque.produtoId
                                    orderby c.dataLimiteFornecedor, c.idPedidoFornecedor
                                    select c).ToList();
            var listaFinal = (from c in aguardando.Select((item, index) => new { item, index })
                              join d in pedidoFornecedor.Select((item, index) => new { item, index }) on c.index equals d.index into
                                  pedidosFornecedorListaInterna
                              from e in pedidosFornecedorListaInterna.DefaultIfEmpty()
                              select new
                              {
                                  c.item.idItemPedidoEstoque,
                                  c.item.pedidoId,
                                  c.item.dataCriacao,
                                  c.item.dataLimiteReserva,
                                  c.item.itemPedidoId,
                                  idPedidoFornecedor = e != null ? e.item.idPedidoFornecedor : 0,
                                  dataLimiteFornecedor = e != null ? (DateTime?)e.item.dataLimiteFornecedor : null,
                                  idPedidoFornecedorItem = e != null ? (int?)e.item.idPedidoFornecedorItem : null,
                                  fornecedorNome = e != null ? e.item.fornecedorNome : "",
                                  dataPrevista = e != null ? e.item.dataPrevista : null
                              }
                ).ToList();
            var itemNaLista =
                listaFinal.FirstOrDefault(x => x.idItemPedidoEstoque == produtoAguardandoEstoque.idItemPedidoEstoque);
            if (itemNaLista != null)
            {
                previsaoEntregaItens.AppendFormat("Romaneio: {0} - Fornecedor: {1} - Data Prevista: {2}<br>",
                    itemNaLista.idPedidoFornecedor, itemNaLista.fornecedorNome,
                    itemNaLista.dataPrevista != null ? itemNaLista.dataPrevista.Value.ToShortDateString() :
                    (itemNaLista.dataLimiteFornecedor == null ? "-" : itemNaLista.dataLimiteFornecedor.Value.ToShortDateString()));
            }
        }
        litPrevisaoEntrega.Text = previsaoEntregaItens.ToString();

        List<CoresLinhas> manterSituacaoProdutos = new List<CoresLinhas>();

        HttpCookie usuarioLogadoId = Request.Cookies["usuarioLogadoId"];
        bool permissaoTrocarEtiqueta = rnUsuarios.usuarioPaginasPermitidaSeleciona_PorUsuarioIdPaginaPermitidaNome(int.Parse(usuarioLogadoId.Value), "trocaretiqueta").Tables[0].Rows.Count == 1;
        bool permissaoAtribuirEtiqueta = rnUsuarios.usuarioPaginasPermitidaSeleciona_PorUsuarioIdPaginaPermitidaNome(int.Parse(usuarioLogadoId.Value), "atribuiretiqueta").Tables[0].Rows.Count == 1;
        bool permissaoEnvioParcial = rnUsuarios.usuarioPaginasPermitidaSeleciona_PorUsuarioIdPaginaPermitidaNome(int.Parse(usuarioLogadoId.Value), "solicitarenvioparcial").Tables[0].Rows.Count == 1;

        foreach (var itemPedido in dtlItensPedido.Items)
        {

            HiddenField txtItemPedidoId = (HiddenField)itemPedido.FindControl("txtItemPedidoId");

            int itemPedidoId = Convert.ToInt32(txtItemPedidoId.Value);

            HiddenField hdfProdutoCancelado = (HiddenField)itemPedido.FindControl("hdfProdutoCancelado");
            bool cancelado = false;
            Boolean.TryParse(hdfProdutoCancelado.Value, out cancelado);

            DataList lstCombo = (DataList)itemPedido.FindControl("lstCombo");

            HtmlGenericControl divHoverPrazoFornecedor = (HtmlGenericControl)itemPedido.FindControl("divHoverPrazoFornecedor");

            if (!cancelado)
            {
                var subItens = itensCombo.Where(x => x.itemPedidoId == itemPedidoId).ToList();
                lstCombo.DataSource = subItens;
                lstCombo.DataBind();

                List<bool> mostrarPopUpPrazoFornecedor = new List<bool>();

                foreach (DataListItem itemCombo in lstCombo.Items)
                {
                    var itemSituacao = new CoresLinhas { itemPedido = itemPedidoId };

                    HiddenField hdfAutorizarParcial = (HiddenField)itemCombo.FindControl("hdfAutorizarParcial");
                    bool autorizarParcial = false;
                    Boolean.TryParse(hdfAutorizarParcial.Value, out autorizarParcial);

                    HiddenField hdfIdItemPedidoEstoque =
                        (HiddenField)itemCombo.FindControl("hdfIdItemPedidoEstoque");
                    int idItemPedidoEstoque = 0;
                    int.TryParse(hdfIdItemPedidoEstoque.Value, out idItemPedidoEstoque);

                    HiddenField hdfProdutoId = (HiddenField)itemCombo.FindControl("hdfProdutoId");
                    int produtoId = 0;
                    int.TryParse(hdfProdutoId.Value, out produtoId);

                    HiddenField hdfEnviado = (HiddenField)itemCombo.FindControl("hdfEnviado");
                    bool enviado = false;
                    Boolean.TryParse(hdfEnviado.Value, out enviado);

                    HiddenField hdfIdCentroDistribuicao = (HiddenField)itemCombo.FindControl("hdfIdCentroDistribuicao");
                    int idCentroDistribuicao = 0;
                    int.TryParse(hdfIdCentroDistribuicao.Value, out idCentroDistribuicao);

                    HyperLink hplTrocarEtiqueta = (HyperLink)itemCombo.FindControl("hplTrocarEtiqueta");

                    LinkButton btnAtribuirEtiqueta = (LinkButton)itemCombo.FindControl("btnAtribuirEtiqueta");

                    var itemQuantidade = 1;

                    Label lblProdutoNome = (Label)itemCombo.FindControl("lblProdutoNome");

                    itemSituacao.produtoId = produtoId;

                    ASPxButton btnEnvioParcial = (ASPxButton)itemCombo.FindControl("btnEnvioParcial");
                    Label lblEntregaRomaneio = (Label)itemCombo.FindControl("lblEntregaRomaneio");
                    Label lblItemRomaneio = (Label)itemCombo.FindControl("lblItemRomaneio");

                    var reservas =
                        (from c in reservasGeral
                         where c.idItemPedidoEstoqueReserva == idItemPedidoEstoque
                         select c);

                    int totalProdutosEntregues = 0;
                    foreach (var reserva in reservas)
                    {
                        lblItemRomaneio.Text += "<b style=\"font-size:larger;\">CD" + idCentroDistribuicao + "</b>" + " - " + reserva.idPedidoFornecedorItem + " - ";
                        totalProdutosEntregues += 1;
                        lblEntregaRomaneio.Text += reserva.dataEntrada.ToString();
                    }

                    if (enviado && autorizarParcial == false)
                    {
                        itemCombo.BackColor = ColorTranslator.FromHtml("#D2E9FF");
                        btnEnvioParcial.Visible = false;
                        btnEnvioParcial.Text = "Não Enviado";
                        btnEnvioParcial.ClientSideEvents.Click = "function(s, e) { autorizarParcial(" + itemPedidoId + ", " + produtoId + ", 'sim'); }";
                        //lblProdutoNome.Text += " " + centroDeDistribuicao;
                    }
                    else if (itemQuantidade <= totalProdutosEntregues && autorizarParcial == false)
                    {
                        itemCombo.BackColor = ColorTranslator.FromHtml("#FFFFBB");
                        btnEnvioParcial.Enabled = false;
                        btnEnvioParcial.Text = "Autorizar Parcial";
                        //lblProdutoNome.Text += " " + centroDeDistribuicao;
                    }
                    else
                    {
                        if (autorizarParcial)
                        {
                            btnEnvioParcial.Text = "Envio Parcial Solicitado";
                            btnEnvioParcial.Enabled = false;
                            if (itemQuantidade <= totalProdutosEntregues)
                            {
                                itemCombo.BackColor = ColorTranslator.FromHtml("#FFFFBB");
                                //lblProdutoNome.Text += " " + centroDeDistribuicao;

                                if (enviado)
                                    itemCombo.BackColor = ColorTranslator.FromHtml("#D2E9FF");

                            }
                            else
                            {
                                itemCombo.BackColor = ColorTranslator.FromHtml("#FFC6C7");
                                mostrarPopUpPrazoFornecedor.Add(true);
                            }
                        }
                        else
                        {
                            itemCombo.BackColor = ColorTranslator.FromHtml("#FFC6C7");
                            btnEnvioParcial.Visible = true;
                            btnEnvioParcial.Text = "Autorizar Parcial";
                            mostrarPopUpPrazoFornecedor.Add(true);

                            if (permissaoAtribuirEtiqueta)
                            {
                                btnAtribuirEtiqueta.Visible = true;
                            }

                            #region Verifica se tem permissão para trocar etiqueta
                            if (permissaoTrocarEtiqueta)
                            {
                                hplTrocarEtiqueta.Visible = true;
                                hplTrocarEtiqueta.NavigateUrl = "estoquereservado.aspx?produtoId=" + produtoId + "&trocarEtiqueta=sim";
                                hplTrocarEtiqueta.ToolTip = "verificar se há possibilidade de reservar o produto";
                            }
                            #endregion Verifica se tem permissão para trocar etiqueta
                        }

                        #region Verifica se tem permissão para solicitacar envio parcial de pedido                        

                        btnEnvioParcial.Visible = permissaoEnvioParcial;

                        #endregion Verifica se tem permissão para solicitacar envio parcial de pedido

                        var aguardando = (from c in aguardandoGeral
                                          where c.produtoId == produtoId
                                          orderby c.dataLimiteReserva
                                          select c).ToList();
                        var pedidoFornecedor = (from c in pedidosFornecedorGeral
                                                where c.idProduto == produtoId
                                                orderby c.dataLimiteFornecedor, c.idPedidoFornecedor
                                                select c).ToList();
                        var listaFinal = (from c in aguardando.Select((item, index) => new { item, index })
                                          join d in pedidoFornecedor.Select((item, index) => new { item, index }) on c.index equals
                                              d.index into
                                              pedidosFornecedorListaInterna
                                          from e in pedidosFornecedorListaInterna.DefaultIfEmpty()
                                          select new
                                          {
                                              c.item.idItemPedidoEstoque,
                                              c.item.pedidoId,
                                              c.item.dataCriacao,
                                              c.item.dataLimiteReserva,
                                              c.item.itemPedidoId,
                                              idPedidoFornecedor = e != null ? e.item.idPedidoFornecedor : 0,
                                              dataLimiteFornecedor = e != null ? (DateTime?)e.item.dataLimiteFornecedor : null,
                                              idPedidoFornecedorItem = e != null ? (int?)e.item.idPedidoFornecedorItem : null,
                                              fornecedorNome = e != null ? e.item.fornecedorNome : "",
                                              entregue = e != null ? e.item.entregue : false
                                          }
                            ).ToList();
                        var itemNaLista =
                            listaFinal.FirstOrDefault(x => x.idItemPedidoEstoque == idItemPedidoEstoque);
                        if (itemNaLista != null)
                        {
                            if (itemNaLista.entregue)
                            {
                                itemCombo.BackColor = ColorTranslator.FromHtml("#F2B84E");
                                mostrarPopUpPrazoFornecedor.Add(false);
                            }
                            lblItemRomaneio.Text =
                                String.Format(
                                    "Romaneio: {0} - Fornecedor: {1} - Data Prevista: {2} - Data limite: {3}<br>",
                                    itemNaLista.idPedidoFornecedor, itemNaLista.fornecedorNome,
                                    itemNaLista.dataLimiteFornecedor == null
                                        ? "-"
                                        : itemNaLista.dataLimiteFornecedor.Value.ToShortDateString(),
                                    itemNaLista.dataLimiteReserva == null
                                        ? "-"
                                        : itemNaLista.dataLimiteReserva.Value.ToShortDateString());
                        }
                    }

                    itemSituacao.cor = itemCombo.BackColor.Name;
                    manterSituacaoProdutos.Add(itemSituacao);
                }

                if (mostrarPopUpPrazoFornecedor.Count(x => x) == mostrarPopUpPrazoFornecedor.Count(x => !x))
                {
                    divHoverPrazoFornecedor.Attributes.Add("style", "cursor:default;");
                    divHoverPrazoFornecedor.Attributes["class"] = "";
                }

            }
        }
        Session.Add(Request.QueryString["pedidoId"], manterSituacaoProdutos);
    }

    protected void dtlItensPedido_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        int produtoId = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "produtoId"));
        string cFotoDestaque = DataBinder.Eval(e.Item.DataItem, "fotoDestaque").ToString();

        #region fotos
        Image fotoDestaque = (Image)e.Item.FindControl("fotoDestaque");
        fotoDestaque.ImageUrl = ConfigurationManager.AppSettings["caminhoCDN"] + "fotos/" + produtoId + "/pequena_" + cFotoDestaque + ".jpg";
        #endregion

        HiddenField txtItemPedidoId = (HiddenField)e.Item.FindControl("txtItemPedidoId");
        HiddenField hdfPedidoId = (HiddenField)e.Item.FindControl("hdfPedidoId");

        int itemPedidoId = Convert.ToInt32(txtItemPedidoId.Value);
        int pedidoId = Convert.ToInt32(hdfPedidoId.Value);

        HiddenField hdfProdutoCancelado = (HiddenField)e.Item.FindControl("hdfProdutoCancelado");
        bool cancelado = false;
        Boolean.TryParse(hdfProdutoCancelado.Value, out cancelado);

        HiddenField hdfProdutoBrinde = (HiddenField)e.Item.FindControl("hdfProdutoBrinde");
        bool brinde = false;
        Boolean.TryParse(hdfProdutoBrinde.Value, out brinde);

        HiddenField hdfEnviado = (HiddenField)e.Item.FindControl("hdfEnviado");
        bool enviado = false;
        Boolean.TryParse(hdfEnviado.Value, out enviado);


        #region produtoNome
        Label lblProdutoNome = (Label)e.Item.FindControl("lblProdutoNome");
        lblProdutoNome.Text = DataBinder.Eval(e.Item.DataItem, "produtoNome").ToString();
        if (cancelado)
        {
            lblProdutoNome.Text = "<b>CANCELADO</b> - " + lblProdutoNome.Text;
        }
        if (brinde)
        {
            lblProdutoNome.Text = "<b>BRINDE</b> - " + lblProdutoNome.Text;
        }
        #endregion


        Label lblPrecoTotal = (Label)e.Item.FindControl("lblPrecoTotal");
        Label lblQuantidade = (Label)e.Item.FindControl("lblQuantidade");
        Label lblPreco = (Label)e.Item.FindControl("lblPreco");

        decimal itemQuantidade = decimal.Parse(lblQuantidade.Text);
        decimal produtoPreco = decimal.Parse(lblPreco.Text.Replace("R$", ""));

        itemPedidoIdCombo = itemPedidoId;
        pedidoIdCombo = pedidoId;
        itemQuantidadeCombo = itemQuantidade;
        canceladoCombo = cancelado;
        brindeCombo = brinde;
        enviadoCombo = enviado;

        lblPrecoTotal.Text = String.Format("{0:c}", decimal.Parse((produtoPreco * itemQuantidade).ToString()));
        ASPxButton btnCancelarItem = (ASPxButton)e.Item.FindControl("btnCancelarItem");
        btnCancelarItem.ClientSideEvents.Click = "function(s, e) { cancelarItemPedido(" + DataBinder.Eval(e.Item.DataItem, "itemPedidoId") + "); }";

        if (cancelado)
        {
            btnCancelarItem.Visible = false;
        }


        ASPxButton btnProdutoPersonalizado = (ASPxButton)e.Item.FindControl("btnProdutoPersonalizado");
        using (var data = new dbCommerceDataContext())
        {

            List<int> itemPedidoProdutos = (from c in data.tbItemPedidoEstoques where c.itemPedidoId == itemPedidoId select c).Select(x => x.produtoId).ToList();

            var produtoItemPersonalizado = (from c in data.tbProdutoPersonalizacaos
                                            where itemPedidoProdutos.Contains(c.idProdutoPai)
                                            select new
                                            {
                                                c.idProdutoPai
                                            }).Distinct().ToList();

            btnProdutoPersonalizado.Visible = produtoItemPersonalizado.Any();


            //if (produtoItemPersonalizado.Any(x => x.idProdutoFilhoPersonalizacao > 0))
            //{
            //    var produtoCombo = (from c in data.tbProdutoRelacionados where c.idProdutoPai == produtoId select c).ToList();

            //    if (produtoCombo.Any(x => x.idProdutoFilho > 0))
            //    {
            //        List<int> produtosCombo = produtoCombo.Select(produto => produto.idProdutoFilho).Distinct().ToList();

            //        var itensComPersonalizacao = (from c in data.tbProdutoPersonalizacaos where produtosCombo.Contains(c.idProdutoPai) select c).ToList();
            //        var bloqueiaSegundaTroca = (from c in data.tbItemPedidoEstoques where c.itemPedidoId == itemPedidoId select c).ToList();

            //        var itensPaisPendentesTroca = (from c in itensComPersonalizacao
            //                                       join d in bloqueiaSegundaTroca on c.idProdutoPai equals d.produtoId
            //                                       select new
            //                                       {
            //                                           c.tbProduto.produtoNome,
            //                                           c.idProdutoPai,
            //                                           itemPedidoId
            //                                       }).Distinct().ToList();

            //        btnProdutoPersonalizado.Visible = itensPaisPendentesTroca.Any();
            //    }
            //}
            //else
            //{
            //    var bloqueiaSegundaTroca = (from c in data.tbItemPedidoEstoques where c.itemPedidoId == itemPedidoId select c).ToList();
            //    var itemPaiPendenteTroca = (from c in produtoItemPersonalizado
            //                                join d in bloqueiaSegundaTroca on c.idProdutoPai equals d.produtoId
            //                                select c).ToList();
            //    btnProdutoPersonalizado.Visible = itemPaiPendenteTroca.Any();
            //}

        }

    }

    protected void lstCombo_OnItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            HiddenField hdfAutorizarParcial = (HiddenField)e.Item.FindControl("hdfAutorizarParcial");
            bool autorizarParcial = false;
            Boolean.TryParse(hdfAutorizarParcial.Value, out autorizarParcial);

            HiddenField hdfIdItemPedidoEstoque = (HiddenField)e.Item.FindControl("hdfIdItemPedidoEstoque");
            int idItemPedidoEstoque = 0;
            int.TryParse(hdfIdItemPedidoEstoque.Value, out idItemPedidoEstoque);

            //var pedidosDc = new dbCommerceDataContext();

            int produtoId = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "produtoId"));
            int itemPedidoId = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "itemPedidoId"));
            bool enviado = Convert.ToBoolean(DataBinder.Eval(e.Item.DataItem, "enviado"));
            //var itemPedidoId = itemPedidoIdCombo;
            var itemQuantidade = 1;

            Label lblProdutoNome = (Label)e.Item.FindControl("lblProdutoNome");

            //var itemPedidoCombo =
            //    (from c in pedidosDc.tbItemPedidoEstoques
            //     where c.produtoId == produtoId && c.itemPedidoId == itemPedidoId && c.idItemPedidoEstoque == idItemPedidoEstoque
            //     select c).First();


            ASPxButton btnEnvioParcial = (ASPxButton)e.Item.FindControl("btnEnvioParcial");
            Label lblEntregaRomaneio = (Label)e.Item.FindControl("lblEntregaRomaneio");
            Label lblItemRomaneio = (Label)e.Item.FindControl("lblItemRomaneio");
            btnEnvioParcial.ClientSideEvents.Click = "function(s, e) { autorizarParcial(" + idItemPedidoEstoque + ", " + produtoId + "), 'nao'; }";

            int totalProdutosEntregues = 0;
            string centroDeDistribuicao = "";
            /**/
        }
    }

    protected void imbBoleto_Click(object sender, ImageClickEventArgs e)
    {
        int clienteId = int.Parse(rnPedidos.pedidoSeleciona_PorPedidoId(int.Parse(Request.QueryString["pedidoId"])).Tables[0].Rows[0]["clienteId"].ToString());
        int tipoDepagamentoId = int.Parse(rnPedidos.pedidoSeleciona_PorPedidoId(int.Parse(Request.QueryString["pedidoId"])).Tables[0].Rows[0]["tipoDePagamentoId"].ToString());

        string pedidoId = Request.QueryString["pedidoId"].ToString();

        string valor;
        if (tipoDepagamentoId == 4)
        {
            valor = rnPedidos.pedidoSeleciona_PorPedidoId(int.Parse(Request.QueryString["pedidoId"])).Tables[0].Rows[0]["valorDoFrete"].ToString();
        }
        else if (tipoDepagamentoId == 8)
        {
            int tipoDepagamentoId1 = int.Parse(rnPedidos.pedidoSeleciona_PorPedidoId(int.Parse(Request.QueryString["pedidoId"])).Tables[0].Rows[0]["tipoDePagamentoId1"].ToString());
            int tipoDepagamentoId2 = int.Parse(rnPedidos.pedidoSeleciona_PorPedidoId(int.Parse(Request.QueryString["pedidoId"])).Tables[0].Rows[0]["tipoDePagamentoId2"].ToString());
            if (tipoDepagamentoId1 == 1)
            {
                valor = rnPedidos.pedidoSeleciona_PorPedidoId(int.Parse(Request.QueryString["pedidoId"])).Tables[0].Rows[0]["valorPagamento1"].ToString();
            }
            else if (tipoDepagamentoId2 == 1)
            {
                valor = rnPedidos.pedidoSeleciona_PorPedidoId(int.Parse(Request.QueryString["pedidoId"])).Tables[0].Rows[0]["valorPagamento2"].ToString();
            }
            else
            {
                valor = rnPedidos.pedidoSeleciona_PorPedidoId(int.Parse(Request.QueryString["pedidoId"])).Tables[0].Rows[0]["valorCobrado"].ToString();
            }
        }
        else
            valor = rnPedidos.pedidoSeleciona_PorPedidoId(int.Parse(Request.QueryString["pedidoId"])).Tables[0].Rows[0]["valorCobrado"].ToString();

        string Cedente = ConfigurationManager.AppSettings["Cedente"];
        string Banco = ConfigurationManager.AppSettings["Banco"];
        string Agencia = ConfigurationManager.AppSettings["Agencia"];
        string Conta = ConfigurationManager.AppSettings["Conta"];
        string Carteira = ConfigurationManager.AppSettings["Carteira"];
        string Modalidade = ConfigurationManager.AppSettings["Modalidade"];
        string Convenio = ConfigurationManager.AppSettings["Convenio"];
        string CodCedente = ConfigurationManager.AppSettings["CodCedente"];
        string clienteNome = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteNome"].ToString();
        string clienteCPFCNPJ = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteCPFCNPJ"].ToString();
        string clienteRua = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteRua"].ToString();
        string clienteBairro = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteBairro"].ToString();
        string clienteCep = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteCep"].ToString();
        string clienteCidade = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteCidade"].ToString();
        string clienteEstado = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteEstado"].ToString();
        string data = DateTime.Now.ToString("dd/MM/yyyy");
        string dataVencimento = DateTime.Now.AddDays(double.Parse(ConfigurationManager.AppSettings["PrazoDoVencimento"].ToString())).ToString("dd/MM/yyyy");
        string Demonstrativo = ConfigurationManager.AppSettings["Demonstrativo"];
        string Instrucoes = ConfigurationManager.AppSettings["Instrucoes"];

        TextBox txtNomeDoCliente = (TextBox)dtvPedido.FindControl("txtNomeDoCliente");
        TextBox txtEmail = (TextBox)dtvPedido.FindControl("txtEmail");
        string linkDoBoleto;
        //linkDoBoleto = ConfigurationManager.AppSettings["caminhoVirtual"] + "boleto/BoletoExibe.asp?Cedente=" + Cedente.Replace(" ", "+") + "&Banco=" + Banco + "&Agencia=" + Agencia + "&Conta=" + Conta + "&Carteira=" + Carteira + "&Modalidade=" + Modalidade.Replace(" ", "+") + "&Convenio=" + Convenio + "&CodCedente=" + CodCedente + "&Sacado=" + clienteNome.Replace(" ", "+") + "&SacadoDOC=CPF/CNPJ=" + clienteCPFCNPJ + "&Endereco1=" + clienteRua.Replace(" ", "+") + "&Endereco2=" + clienteBairro.Replace(" ", "+") + "&Endereco3=" + clienteCep + "&NumeroDocumento=" + pedidoId + "&NossoNumero=" + pedidoId + "&Valor=" + valor + "&DataDocumento=" + data + "&DataVencimento=" + dataVencimento + "&Demonstrativo" + Demonstrativo.Replace(" ", "+") + "=&Instrucoes=" + Instrucoes.Replace(" ", "+") + "";
        linkDoBoleto = ConfigurationManager.AppSettings["caminhoVirtual"] + "boleto.aspx?sacado=" + clienteNome.Replace(" ", "+") + "&CPFCNPJ=" + clienteCPFCNPJ + "&endereco=" + clienteRua.Replace(" ", "+") + "&bairro=" + clienteBairro.Replace(" ", "+") + "&cidade=" + clienteCidade.Replace(" ", "+") + "&cep=" + clienteCep + "&uf=" + clienteEstado + "&numeroDocumento=" + pedidoId + "&nossoNumero=" + pedidoId + "&valor=" + valor + "";

        if (rnEmails.enviaSegundaViaDoBoleto(txtNomeDoCliente.Text, Request.QueryString["pedidoId"].ToString(), linkDoBoleto, txtEmail.Text))
        {
            Response.Write("<script>alert('Segunda via de boleto enviada por e-mail com sucesso.');</script>");
            interacaoInclui("Segunda via de boleto enviada", "False");
        }
        else
            Response.Write("<script>alert('Segunda via de boleto não foi enviada.');</script>");
    }

    protected void imbCupom_Click(object sender, ImageClickEventArgs e)
    {
        TextBox txtNomeDoCliente = (TextBox)dtvPedido.FindControl("txtNomeDoCliente");
        TextBox txtEmail = (TextBox)dtvPedido.FindControl("txtEmail");
        TextBox txtCupomDeDesconto = (TextBox)dtvPedido.FindControl("txtCupomDeDesconto");

        Random r = new Random();
        int cupom;
        cupom = r.Next(99999999);

        if (rnCupomDeDesconto.cupomDeDescontoInclui(cupom.ToString(), decimal.Parse(txtCupomDeDesconto.Text)))
        {
            if (rnEmails.enviaCupomDeDesconto(txtNomeDoCliente.Text, cupom.ToString(), txtCupomDeDesconto.Text, txtEmail.Text))
                Response.Write("<script>alert('Cupom de desconto enviado com sucesso.');</script>");
        }
        else
            Response.Write("<script>alert('Cupom de desconto não foi enviado.');</script>");
    }

    protected void btnSalvarDespesas_Click(object sender, ImageClickEventArgs e)
    {
        HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
        usuarioLogadoId = Request.Cookies["usuarioLogadoId"];

        TextBox txtDespesasDescricao = (TextBox)dtvPedido.FindControl("txtDespesasDescricao");
        TextBox txtDespesaValor = (TextBox)dtvPedido.FindControl("txtDespesaValor");
        DropDownList drpDespesasCategoria = (DropDownList)dtvPedido.FindControl("drpDespesasCategoria");
        ASPxDateEdit txtDespesaData = (ASPxDateEdit)dtvPedido.FindControl("txtDespesaData");
        ASPxDateEdit txtDespesaVencimento = (ASPxDateEdit)dtvPedido.FindControl("txtDespesaVencimento");

        var despesasContext = new dbCommerceDataContext();
        var despesa = new tbDespesa();
        despesa.idUsuario = Convert.ToInt32(usuarioLogadoId.Value.ToString());
        despesa.data = txtDespesaData.Date;
        despesa.despesa = txtDespesasDescricao.Text;
        despesa.idDespesaCategoria = Convert.ToInt32(drpDespesasCategoria.SelectedValue);
        despesa.pago = false;
        despesa.valor = Convert.ToDecimal(txtDespesaValor.Text.Replace(".", ""));
        despesa.vencimento = txtDespesaVencimento.Date;
        despesa.idPedido = Convert.ToInt32(Request.QueryString["pedidoId"]);
        despesasContext.tbDespesas.InsertOnSubmit(despesa);
        despesasContext.SubmitChanges();
        txtDespesasDescricao.Text = "";
        txtDespesaValor.Text = "";

        gridDespesas.DataBind();
        Response.Write("<script>alert('Despesa gravada com sucesso.');</script>");
    }

    protected void btnSalvarCustos_Click(object sender, ImageClickEventArgs e)
    {
        foreach (ListViewItem itemPedido in dtlItensPedido.Items)
        {
            TextBox txtValorCusto = (TextBox)itemPedido.FindControl("txtValorCusto");
            HiddenField txtItemPedidoId = (HiddenField)itemPedido.FindControl("txtItemPedidoId");
            CheckBox chkEntregue = (CheckBox)itemPedido.FindControl("chkEntregue");

            decimal valorCusto = txtValorCusto.Text == "" ? 0 : Convert.ToDecimal(txtValorCusto.Text);

            rnPedidos.itensPedidoAlteraCustoProduto(valorCusto, Convert.ToInt32(txtItemPedidoId.Value), chkEntregue.Checked ? 1 : 0);
        }
        Response.Write("<script>alert('Preços de custo atualizados com sucesso.');</script>");
    }

    protected void btnCodDeBarras_Click(object sender, EventArgs e)
    {
        if (ddlProdutosCodBarras.Visible == true)
        {
            if (string.IsNullOrEmpty(ddlProdutosCodBarras.SelectedValue))
            {
                Response.Write("<script>alert('Nenhum produto selecionado.');</script>");
                return;
            }
            var produtoUpdateDc = new dbCommerceDataContext();
            int idProduto = Convert.ToInt32(ddlProdutosCodBarras.SelectedValue);
            var produtoUpdate = (from c in produtoUpdateDc.tbProdutos where c.produtoId == idProduto select c).First();
            produtoUpdate.produtoCodDeBarras = txtCodDeBarras.Text;
            produtoUpdateDc.SubmitChanges();
            ddlProdutosCodBarras.Visible = false;
        }

        var produtoContext = new dbCommerceDataContext();
        var produto =
            (from c in produtoContext.tbProdutos where c.produtoCodDeBarras == txtCodDeBarras.Text select c)
                .FirstOrDefault();
        if (produto != null)
        {
            var itensPedidoContext = new dbCommerceDataContext();
            int pedidoId = Convert.ToInt32(Request.QueryString["pedidoId"]);
            var itemPedido =
                (from c in itensPedidoContext.tbItensPedidos where c.produtoId == produto.produtoId && c.pedidoId == pedidoId select c)
                    .FirstOrDefault();
            if (itemPedido != null)
            {
                itemPedido.entreguePeloFornecedor = true;
                itensPedidoContext.SubmitChanges();
                txtCodDeBarras.Text = "";
                dtlItensPedido.DataBind();
            }
            else
            {
                txtCodDeBarras.Text = "";
                Response.Write("<script>alert('Produto não localizado neste pedido.');</script>");
            }
        }
        else
        {
            Response.Write("<script>alert('Código de Barras não localizado, favor selecionar o produto.');</script>");
            ddlProdutosCodBarras.Visible = true;
        }
        txtCodDeBarras.Focus();
    }

    private void fillProdutosSemCodigo()
    {
        ddlProdutosCodBarras.Items.Clear();
        var itensPedidoContext = new dbCommerceDataContext();
        int pedidoId = Convert.ToInt32(Request.QueryString["pedidoId"]);
        var itensPedido = (from c in itensPedidoContext.tbItensPedidos where c.pedidoId == pedidoId select c);
        foreach (var pedido in itensPedido)
        {
            var produtoDc = new dbCommerceDataContext();
            var produto = (from c in produtoDc.tbProdutos where c.produtoId == pedido.produtoId select c).First();
            if (string.IsNullOrEmpty(produto.produtoCodDeBarras))
            {
                ddlProdutosCodBarras.Items.Add(new ListItem(produto.produtoIdDaEmpresa + " - " + produto.produtoNome, produto.produtoId.ToString()));
            }
        }
    }

    protected void btnGerarNota_Click(object sender, EventArgs e)
    {
        geraNotaClique();
    }

    private void geraNotaClique()
    {
        List<int> itensMarcados = new List<int>();

        foreach (ListViewItem item in lstProdutoNota.Items)
        {

            CheckBox ckbMarcar = (CheckBox)item.FindControl("ckbMarcar");

            if (ckbMarcar.Checked)
            {
                HiddenField hiddenProdutoId = (HiddenField)item.FindControl("hiddenProdutoId");
                TextBox txtncm = (TextBox)item.FindControl("txtncm");
                int produtoId = Convert.ToInt32(hiddenProdutoId.Value);

                var produtoDc = new dbCommerceDataContext();
                var produto = (from c in produtoDc.tbProdutos where c.produtoId == produtoId select c).FirstOrDefault();
                produto.ncm = txtncm.Text;
                produtoDc.SubmitChanges();

                itensMarcados.Add(produtoId);
            }
        }

        var pedidoDc = new dbCommerceDataContext();
        int pedidoId = Convert.ToInt32(Request.QueryString["pedidoId"]);




        var xmlNotaFiscal = rnNotaFiscal.gerarNotaFiscal(pedidoId, txtCodigoIbge.Text, itensMarcados, ckbGerarNovaNota.Checked);
        rnNotaFiscal.autorizarNota(xmlNotaFiscal.xml, 0, xmlNotaFiscal.idNotaFiscal);


        int numeroNota = (from c in pedidoDc.tbNotaFiscals
                          where c.idNotaFiscal == xmlNotaFiscal.idNotaFiscal
                          select c.numeroNota).FirstOrDefault();
        interacaoInclui("Gerada Nfe de saida  " + numeroNota, "False");
        pcNota.ShowOnPageLoad = false;

        int nfeReferenciada = 0;
        try { nfeReferenciada = Convert.ToInt32(txtNfeReferenciada.Text); } catch { }
        int? idPedidoEnvioAnterior = 0;
        if (nfeReferenciada != 0)
        {
            idPedidoEnvioAnterior = (from c in pedidoDc.tbNotaFiscals
                                     where c.numeroNota == nfeReferenciada && c.idPedido == pedidoId
                                     select c.idPedidoEnvio).FirstOrDefault();
            tbNotaFiscal notaAnterior = (from c in pedidoDc.tbNotaFiscals
                                         where c.numeroNota == nfeReferenciada && c.idPedido == pedidoId
                                         select c).FirstOrDefault();

            tbNotaFiscal notaNova = (from c in pedidoDc.tbNotaFiscals
                                     where c.numeroNota == numeroNota && c.idPedido == pedidoId
                                     select c).FirstOrDefault();
            if (notaNova != null && notaAnterior != null)
            {
                notaAnterior.idPedidoEnvio = 0;
                notaNova.idPedidoEnvio = idPedidoEnvioAnterior;
                pedidoDc.SubmitChanges();
            }
        }




        fillNotas();

        /* XmlWriterSettings settings = new XmlWriterSettings();
        settings.Indent = true;
        MemoryStream ms = new MemoryStream();
        HttpContext.Current.Response.Clear();
        HttpContext.Current.Response.ContentType = "text/xml";
        HttpContext.Current.Response.AddHeader("Content-Disposition:","attachment;filename=" + HttpUtility.UrlEncode("nota" + pedidoId + ".xml"));
        HttpContext.Current.Response.Write(nota);
        HttpContext.Current.Response.End();*/
    }
    protected void btnGerarNotaDevolucao_Click(object sender, EventArgs e)
    {
        try
        {
            List<int> itensMarcados = new List<int>();
            List<int> itensNfeReferencia = new List<int>();
            List<int> idsItemPedidoEstoque = new List<int>();
            string produtosDevolucao = "";
            //int qtdTotalItensPedido;
            int qtdTotalItensDevolvidos = 0;
            int pedidoId = Convert.ToInt32(Request.QueryString["pedidoId"]);
            string notaReferencia = ddlNotasReferenciaDevolucao.SelectedItem.Text;
            bool itemDiferenteNfeReferencia = false;

            //using (var data = new dbCommerceDataContext())
            //{
            //    qtdTotalItensPedido = (from c in data.tbItemPedidoEstoques
            //                           where c.tbItensPedido.pedidoId == pedidoId && !c.cancelado
            //                           select new { c.idItemPedidoEstoque }).Count();
            //}

            foreach (ListViewItem item in lstProdutoNotaDevolucao.Items)
            {

                CheckBox ckbMarcar = (CheckBox)item.FindControl("ckbMarcar");

                if (ckbMarcar.Checked)
                {
                    HiddenField hiddenProdutoId = (HiddenField)item.FindControl("hiddenProdutoId");
                    HiddenField hiddenIdItemPedidoEstoque = (HiddenField)item.FindControl("hiddenIdItemPedidoEstoque");
                    HiddenField hiddenBrinde = (HiddenField)item.FindControl("hiddenBrinde");
                    TextBox txtncm = (TextBox)item.FindControl("txtncm");
                    int produtoId = Convert.ToInt32(hiddenProdutoId.Value);

                    var produtoDc = new dbCommerceDataContext();
                    var produto = (from c in produtoDc.tbProdutos where c.produtoId == produtoId select c).First();
                    produto.ncm = txtncm.Text;
                    produtoDc.SubmitChanges();
                    produtosDevolucao += (Convert.ToBoolean(hiddenBrinde.Value) ? "(BRINDE) " : "") + produto.produtoNome + "<br>";
                    itensMarcados.Add(produtoId);
                    idsItemPedidoEstoque.Add(Convert.ToInt32(hiddenIdItemPedidoEstoque.Value));
                    qtdTotalItensDevolvidos++;
                }
            }

            string sCaminhoDoArquivoXml = ConfigurationManager.AppSettings["caminhoFisico"] + "\\admin\\notas\\xmls\\" + ddlNotasReferenciaDevolucao.SelectedValue.Split('#')[1] + "_" + notaReferencia + ".xml";

            if (!File.Exists(sCaminhoDoArquivoXml))
            {
                int idNotaFiscal;
                int numeroNota = Convert.ToInt32(notaReferencia);
                int idCNPJ = Convert.ToInt32(ddlNotasReferenciaDevolucao.SelectedValue.Split('#')[1]);

                using (var data = new dbCommerceDataContext())
                {
                    idNotaFiscal =
                        (from c in data.tbNotaFiscals
                         where c.numeroNota == numeroNota && c.idCNPJ == idCNPJ
                         select new { c.idNotaFiscal }).First().idNotaFiscal;
                }

                XmlDocument doc = new XmlDocument();
                string xml = rnNotaFiscal.retornaXmlNota(numeroNota, idNotaFiscal);

                if (string.IsNullOrEmpty(xml))
                {
                    return;
                }
                doc.LoadXml(xml);
                doc.Save(ConfigurationManager.AppSettings["caminhoFisico"] + "\\admin\\notas\\xmls\\" + ddlNotasReferenciaDevolucao.SelectedValue.Split('#')[1] + "_" + notaReferencia + ".xml");
            }

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(sCaminhoDoArquivoXml); //Carregando o arquivo

            //Pegando elemento pelo nome da TAG
            XmlNodeList xnList = xmlDoc.GetElementsByTagName("prod");

            for (int i = 0; i < xnList.Count; i++)
            {
                itensNfeReferencia.Add(Convert.ToInt32(xnList[i]["cProd"].InnerText));
            }

            var verificaQtdItensDevolvidos = itensNfeReferencia.ToList();

            foreach (var item in itensMarcados)
            {
                if (itensNfeReferencia.Contains(item))
                {
                    var removerItem = verificaQtdItensDevolvidos.First(x => x == item);
                    verificaQtdItensDevolvidos.Remove(removerItem);
                }
                else
                {
                    itemDiferenteNfeReferencia = true;
                }
            }

            if (!itemDiferenteNfeReferencia)
            {
                rnNotaFiscal.transportadora = ddlTransportadora.SelectedItem.Value;
                rnNotaFiscal.dataSaida = null;
                DateTime? dtSaida = null;
                if (!String.IsNullOrEmpty(txtdatasaida.Text))
                {
                    try
                    {
                        dtSaida = Convert.ToDateTime(txtdatasaida.Text);
                    }
                    catch (Exception ex)
                    {

                    }
                    if (dtSaida != null)
                    {
                        rnNotaFiscal.dataSaida = dtSaida;
                    }
                    else
                    {
                        Response.Write("<script>alert('Data de saída inválida');</script>");
                        return;
                    }
                }

                var xmlNotaFiscal = rnNotaFiscal.gerarNotaFiscalDevolucao(pedidoId, txtCodigoIbgeDevolucao.Text,
                    itensMarcados, ckbGerarNovaNotaDevolucao.Checked,
                    Convert.ToInt32(ddlEmpresasDevolucao.SelectedValue), ddlNotasReferenciaDevolucao.SelectedValue.Split('#')[0],
                    txtInformacoesAdicionais.Text, idsItemPedidoEstoque, notaReferencia,
                    (verificaQtdItensDevolvidos.Count != 0));

                rnNotaFiscal.autorizarNota(xmlNotaFiscal.xml, 0, xmlNotaFiscal.idNotaFiscal);
                pcNotaDevolucao.ShowOnPageLoad = false;

                interacaoInclui("Gerada Nfe de devolução (" + (verificaQtdItensDevolvidos.Count != 0 ? "Parcial" : "Total") + ") referente a NF-e " + notaReferencia + "<br>Produtos da devolução:<br>" +
                                produtosDevolucao, "False");

                fillNotas();

                Response.Write("<script>alert('Nota fiscal de devolução em processo de validação, para confirmar que a nota foi gerada com sucesso verificar se o link para download foi disponibilizado!');</script>");
            }
            else
            {
                Response.Write("<script>alert('Itens marcados para devolução não constam na nota de referência, favor checar e realizar nova tentativa!');</script>");
            }
        }
        catch (Exception ex)
        {
            Response.Write("<script>alert('ERRO: " + ex.Message + "');</script>");
        }
    }

    protected void btnGerarNotaDevolucao2_Click(object sender, EventArgs e)
    {
        // try
        //{
        List<int> itensMarcados = new List<int>();
        List<int> itensNfeReferencia = new List<int>();
        List<int> idsItemPedidoEstoque = new List<int>();
        string produtosDevolucao = "";
        //int qtdTotalItensPedido;
        int qtdTotalItensDevolvidos = 0;
        int pedidoId = Convert.ToInt32(Request.QueryString["pedidoId"]);
        // string notaReferencia = ddlNotasReferenciaDevolucao.SelectedItem.Text;
        string notaReferencia = lblNotaFiscalReferenciaDevolucao.Text;
        bool itemDiferenteNfeReferencia = false;

        //using (var data = new dbCommerceDataContext())
        //{
        //    qtdTotalItensPedido = (from c in data.tbItemPedidoEstoques
        //                           where c.tbItensPedido.pedidoId == pedidoId && !c.cancelado
        //                           select new { c.idItemPedidoEstoque }).Count();
        //}

        foreach (ListViewItem item in lstProdutoNotaDevolucao2.Items)
        {

            CheckBox ckbMarcar = (CheckBox)item.FindControl("ckbMarcar");

            if (ckbMarcar.Checked)
            {
                HiddenField hiddenProdutoId = (HiddenField)item.FindControl("hiddenProdutoId");
                HiddenField hiddenIdItemPedidoEstoque = (HiddenField)item.FindControl("hiddenIdItemPedidoEstoque");
                HiddenField hiddenBrinde = (HiddenField)item.FindControl("hiddenBrinde");
                TextBox txtncm = (TextBox)item.FindControl("txtncm");
                int produtoId = Convert.ToInt32(hiddenProdutoId.Value);

                var produtoDc = new dbCommerceDataContext();
                var produto = (from c in produtoDc.tbProdutos where c.produtoId == produtoId select c).First();
                produto.ncm = txtncm.Text;
                produtoDc.SubmitChanges();
                produtosDevolucao += (Convert.ToBoolean(hiddenBrinde.Value) ? "(BRINDE) " : "") + produto.produtoNome + "<br>";
                itensMarcados.Add(produtoId);
                idsItemPedidoEstoque.Add(Convert.ToInt32(hiddenIdItemPedidoEstoque.Value));
                qtdTotalItensDevolvidos++;
            }
        }

        string sCaminhoDoArquivoXml = ConfigurationManager.AppSettings["caminhoFisico"] + "\\admin\\notas\\xmls\\" + hdIdEmpresaNotaFiscalDevolucao.Value + "_" + notaReferencia + ".xml";
        int idNotaFiscal = Convert.ToInt32(hdfIdNotaFiscal.Value);
        if (!File.Exists(sCaminhoDoArquivoXml))
        {
            int numeroNota = Convert.ToInt32(notaReferencia);
            //int idCNPJ = Convert.ToInt32(ddlNotasReferenciaDevolucao.SelectedValue.Split('#')[1]);
            int idCNPJ = Convert.ToInt32(hdIdEmpresaNotaFiscalDevolucao.Value);

            //using (var data = new dbCommerceDataContext())
            //{
            //    idNotaFiscal =
            //        (from c in data.tbNotaFiscals
            //         where c.numeroNota == numeroNota && c.idCNPJ == idCNPJ
            //         select new { c.idNotaFiscal }).First().idNotaFiscal;
            //}

            XmlDocument doc = new XmlDocument();
            string xml = rnNotaFiscal.retornaXmlNota(numeroNota, idNotaFiscal);

            if (string.IsNullOrEmpty(xml))
            {
                return;
            }
            doc.LoadXml(xml);
            //doc.Save(ConfigurationManager.AppSettings["caminhoFisico"] + "\\admin\\notas\\xmls\\" + ddlNotasReferenciaDevolucao.SelectedValue.Split('#')[1] + "_" + notaReferencia + ".xml");
            doc.Save(ConfigurationManager.AppSettings["caminhoFisico"] + "\\admin\\notas\\xmls\\" + idCNPJ + "_" + notaReferencia + ".xml");
        }

        XmlDocument xmlDoc = new XmlDocument();
        xmlDoc.Load(sCaminhoDoArquivoXml); //Carregando o arquivo

        //Pegando elemento pelo nome da TAG
        XmlNodeList xnList = xmlDoc.GetElementsByTagName("prod");

        for (int i = 0; i < xnList.Count; i++)
        {
            itensNfeReferencia.Add(Convert.ToInt32(xnList[i]["cProd"].InnerText));
        }

        var verificaQtdItensDevolvidos = itensNfeReferencia.ToList();

        foreach (var item in itensMarcados)
        {
            if (itensNfeReferencia.Contains(item))
            {
                var removerItem = verificaQtdItensDevolvidos.First(x => x == item);
                verificaQtdItensDevolvidos.Remove(removerItem);
            }
            else
            {
                itemDiferenteNfeReferencia = true;
            }
        }

        if (!itemDiferenteNfeReferencia)
        {
            rnNotaFiscal.transportadora = ddlTransportadora2.SelectedItem.Value;
            rnNotaFiscal.dataSaida = null;
            DateTime? dtSaida = null;
            if (!String.IsNullOrEmpty(txtdatasaida2.Text))
            {
                try
                {
                    dtSaida = Convert.ToDateTime(txtdatasaida2.Text);
                }
                catch (Exception ex)
                {

                }
                if (dtSaida != null)
                {
                    rnNotaFiscal.dataSaida = dtSaida;
                }
                else
                {
                    Response.Write("<script>alert('Data de saída inválida');</script>");
                    return;
                }
            }


            var xmlNotaFiscal = rnNotaFiscal.gerarNotaFiscalDevolucao1(pedidoId, 0, txtCodigoIbgeDevolucao2.Text, Convert.ToInt32(hdIdEmpresaNotaFiscalDevolucao.Value), txtInformacoesAdicionais2.Text, idsItemPedidoEstoque, idNotaFiscal);

            /*var xmlNotaFiscal = rnNotaFiscal.gerarNotaFiscalDevolucao(pedidoId, txtCodigoIbgeDevolucao2.Text,
                itensMarcados, ckbGerarNovaNotaDevolucao2.Checked,
                Convert.ToInt32(hdIdEmpresaNotaFiscalDevolucao.Value), hdNfeKey.Value,
                txtInformacoesAdicionais2.Text, idsItemPedidoEstoque, notaReferencia,
                (verificaQtdItensDevolvidos.Count != 0));*/

            rnNotaFiscal.autorizarNota(xmlNotaFiscal.xml, 0, xmlNotaFiscal.idNotaFiscal);

            var pedidoDc = new dbCommerceDataContext();
            int numeroNota = (from c in pedidoDc.tbNotaFiscals
                              where c.idNotaFiscal == xmlNotaFiscal.idNotaFiscal
                              select c.numeroNota).FirstOrDefault();
            interacaoInclui("Gerada Nfe de devolução " + numeroNota + " (" + (verificaQtdItensDevolvidos.Count != 0 ? "Parcial" : "Total") + ") referente a NF-e " + notaReferencia + "<br>Produtos da devolução:<br>" +
            produtosDevolucao, "False");

            pcNotaDevolucao.ShowOnPageLoad = false;





            fillNotas();

            Response.Write("<script>alert('Nota fiscal de devolução em processo de validação, para confirmar que a nota foi gerada com sucesso verificar se o link para download foi disponibilizado!');</script>");
            pcNotaDevolucao2.ShowOnPageLoad = false;
        }
        else
        {
            Response.Write("<script>alert('Itens marcados para devolução não constam na nota de referência, favor checar e realizar nova tentativa!');</script>");
        }
        /*}
        catch (Exception ex)
        {
            Response.Write("<script>alert('ERRO: " + ex.InnerException + "');</script>");
        }*/
    }

    protected void lstProdutoNota_DataBound(object sender, ListViewItemEventArgs e)
    {
        HiddenField hiddenProdutoId = (HiddenField)e.Item.FindControl("hiddenProdutoId");
        int produtoId = Convert.ToInt32(hiddenProdutoId.Value);

        #region fotos
        Image fotoDestaque = (Image)e.Item.FindControl("fotoDestaque");
        if (rnFotos.produtoFotoDestaqueSeleciona_PorProdutoId(produtoId).Tables[0].Rows.Count > 0)
            fotoDestaque.ImageUrl = ConfigurationManager.AppSettings["caminhoCDN"] + "fotos/" + produtoId + "/pequena_" + rnFotos.produtoFotoDestaqueSeleciona_PorProdutoId(produtoId).Tables[0].Rows[0]["produtoFoto"].ToString() + ".jpg";
        else
            fotoDestaque.ImageUrl = ConfigurationManager.AppSettings["caminhoCDN"] + "fotos/naoExiste/pequena.jpg";
        #endregion

    }

    protected void gridDespesas_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType != GridViewRowType.EditForm) return;
        {
            TextBox txtValor = (TextBox)gridDespesas.FindEditRowCellTemplateControl((GridViewDataColumn)gridDespesas.Columns["valor"], "txtValor");
            txtValor.Attributes.Add("onkeyPress", "return moeda(this,'.',',',event);");
        }
    }

    protected void gridDespesas_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {

        TextBox txtValor = (TextBox)gridDespesas.FindEditRowCellTemplateControl((GridViewDataColumn)gridDespesas.Columns["valor"], "txtValor");
        e.NewValues["valor"] = txtValor.Text.Replace(".", "");
        //e.NewValues["idPedido"] = Convert.ToInt32(Request.QueryString["pedidoId"]);

    }

    protected void btnSalvarEnderecoEntrega_onClick(object sender, ImageClickEventArgs e)
    {
        TextBox txtNomeEntrega = (TextBox)dtvPedido.FindControl("txtNomeEntrega");
        TextBox txtRua = (TextBox)dtvPedido.FindControl("txtRua");
        TextBox txtNumero = (TextBox)dtvPedido.FindControl("txtNumero");
        TextBox txtComplemento = (TextBox)dtvPedido.FindControl("txtComplemento");
        TextBox txtBairro = (TextBox)dtvPedido.FindControl("txtBairro");
        TextBox txtCidade = (TextBox)dtvPedido.FindControl("txtCidade");
        TextBox txtEstado = (TextBox)dtvPedido.FindControl("txtEstado");
        TextBox txtCep = (TextBox)dtvPedido.FindControl("txtCep");
        TextBox txtReferencia = (TextBox)dtvPedido.FindControl("txtReferencia");

        if (String.IsNullOrEmpty(txtCep.Text))
        {
            Response.Write("<script>alert('Favor preeencher o cep.');</script>");
            return;
        }
        else if (String.IsNullOrEmpty(txtEstado.Text))
        {
            Response.Write("<script>alert('Favor preeencher o estado.');</script>");
            return;
        }
        else if (String.IsNullOrEmpty(txtNumero.Text))
        {
            Response.Write("<script>alert('Favor preeencher o numero do endereço.');</script>");
            return;
        }

        int pedidoId = int.Parse(Request.QueryString["pedidoId"]);


        try
        {
            var pedidoDc = new dbCommerceDataContext();
            var pedido = (from c in pedidoDc.tbPedidos where c.pedidoId == pedidoId select c).FirstOrDefault();
            if (pedido != null)
            {
                StringBuilder interacao = new StringBuilder();
                interacao.Append("<b>Endereço de entrega alterado de:</b><br>");
                interacao.AppendFormat("Nome para Entrega: {0}<br>", pedido.endNomeDoDestinatario);
                interacao.AppendFormat("Rua: {0}<br>", pedido.endRua);
                interacao.AppendFormat("Número: {0}<br>", pedido.endNumero);
                interacao.AppendFormat("Complemento: {0}<br>", pedido.endComplemento);
                interacao.AppendFormat("Bairro: {0}<br>", pedido.endBairro);
                interacao.AppendFormat("Cidade: {0}<br>", pedido.endCidade);
                interacao.AppendFormat("Estado: {0}<br>", pedido.endEstado);
                interacao.AppendFormat("CEP: {0}<br>", pedido.endCep);
                interacao.AppendFormat("Referência: {0}<br>", pedido.endReferenciaParaEntrega);
                interacao.Append("<b>Para:</b><br>");
                interacao.AppendFormat("Nome para Entrega: {0}<br>", txtNomeEntrega.Text);
                interacao.AppendFormat("Rua: {0}<br>", txtRua.Text);
                interacao.AppendFormat("Número: {0}<br>", txtNumero.Text);
                interacao.AppendFormat("Complemento: {0}<br>", txtComplemento.Text);
                interacao.AppendFormat("Bairro: {0}<br>", txtBairro.Text);
                interacao.AppendFormat("Cidade: {0}<br>", txtCidade.Text);
                interacao.AppendFormat("Estado: {0}<br>", txtEstado.Text);
                interacao.AppendFormat("CEP: {0}<br>", txtCep.Text);
                interacao.AppendFormat("Referência: {0}<br>", txtReferencia.Text);

                var frete = rnFrete.montaFrete(txtCep.Text.Replace("-", "").Trim().TrimEnd(), pedidoId).FirstOrDefault();

                if (frete != null)
                {
                    if (pedido.prazoDeEntrega != frete.prazo.ToString())
                    {
                        interacao.AppendFormat("Prazo para o cep alterado de: {0} para {1} dias úteis<br>", pedido.prazoDeEntrega, frete.prazo);
                        pedido.prazoDeEntrega = frete.prazo.ToString();
                        litPrazoEntrega.Text = frete.prazo.ToString();
                    }

                    //if (frete.faixaDeCepPreco > pedido.valorDoFrete)
                    //{
                    //    decimal diferencaFrete = (frete.faixaDeCepPreco - (pedido.valorDoFrete ?? 0));
                    //    interacao.AppendFormat("Acrescimo de {0} no valor do frete, valor anterior {1} novo valor {2}<br>", diferencaFrete.ToString("C"), (pedido.valorDoFrete ?? 0).ToString("C"), frete.faixaDeCepPreco);
                    //    pedido.valorDoFrete += diferencaFrete;
                    //}
                }

                pedido.endNomeDoDestinatario = txtNomeEntrega.Text;
                pedido.endRua = txtRua.Text;
                pedido.endNumero = txtNumero.Text;
                pedido.endComplemento = txtComplemento.Text;
                pedido.endBairro = txtBairro.Text;
                pedido.endCidade = txtCidade.Text;
                pedido.endEstado = txtEstado.Text;
                pedido.endCep = txtCep.Text;
                pedido.endReferenciaParaEntrega = txtReferencia.Text;

                pedidoDc.SubmitChanges();

                interacaoInclui(interacao.ToString(), "False");

            }

            Response.Write("<script>alert('Endereço de entrega alterado com sucesso!');</script>");
        }
        catch (Exception ex)
        {

        }

    }

    protected void btnSalvarTipoDeEntrega_Click(object sender, ImageClickEventArgs e)
    {
        DropDownList ddlEnviarPor = (DropDownList)dtvPedido.FindControl("ddlEnviarPor");
        DropDownList ddlUsuarioSeparacao = (DropDownList)dtvPedido.FindControl("ddlUsuarioSeparacao");
        DropDownList ddlUsuarioEmbalagem = (DropDownList)dtvPedido.FindControl("ddlUsuarioEmbalagem");

        int pedidoId = int.Parse(Request.QueryString["pedidoId"]);

        var pedidoDc = new dbCommerceDataContext();
        var pedido = (from c in pedidoDc.tbPedidos where c.pedidoId == pedidoId select c).FirstOrDefault();
        if (pedido != null)
        {
            string formaEnvio = null;
            if (!string.IsNullOrEmpty(ddlEnviarPor.SelectedValue))
                formaEnvio = ddlEnviarPor.SelectedValue;

            StringBuilder interacao = new StringBuilder();
            interacao.Append("<b>Tipo de Entrega alterado para:</b><br>");
            interacao.AppendFormat("{0}", ddlEnviarPor.SelectedItem.Text);
            pedido.formaDeEnvio = formaEnvio;
            pedidoDc.SubmitChanges();
            interacaoInclui(interacao.ToString(), "False");

            DateTime dtAgora = System.DateTime.Now;

            string idUsuarioSeparacao = null;
            if (!string.IsNullOrEmpty(ddlUsuarioSeparacao.SelectedValue))
            {
                idUsuarioSeparacao = ddlUsuarioSeparacao.SelectedValue;
                StringBuilder interacaoSeparacao = new StringBuilder();
                interacaoSeparacao.Append("<b>Usuário separação alteradp para: </b><br>");
                interacaoSeparacao.AppendFormat("{0}", ddlUsuarioSeparacao.SelectedItem.Text);
                pedido.idUsuarioSeparacao = int.Parse(idUsuarioSeparacao);
                pedido.dataInicioSeparacao = dtAgora;
                pedidoDc.SubmitChanges();
                interacaoInclui(interacaoSeparacao.ToString(), "False");
            }

            string idUsuarioEmbalagem = null;
            if (!string.IsNullOrEmpty(ddlUsuarioSeparacao.SelectedValue))
            {
                idUsuarioEmbalagem = ddlUsuarioSeparacao.SelectedValue;
                StringBuilder interacaoEmbalagem = new StringBuilder();
                interacaoEmbalagem.Append("<b>Usuário embalagem alterado para: </b><br>");
                interacaoEmbalagem.AppendFormat("{0}", ddlUsuarioEmbalagem.SelectedItem.Text);
                pedido.idUsuarioEmbalado = int.Parse(idUsuarioEmbalagem);
                pedido.dataSendoEmbalado = dtAgora;
                pedidoDc.SubmitChanges();
                interacaoInclui(interacaoEmbalagem.ToString(), "False");
            }

            Response.Write("<script>alert('Tipo de entrega alterado com sucesso.');</script>");
        }
    }

    protected void btnSalvarMaoPropria_Click(object sender, ImageClickEventArgs e)
    {
        CheckBox chkMaoPropria = (CheckBox)dtvPedido.FindControl("chkMaoPropria");
        CheckBox chkReversa = (CheckBox)dtvPedido.FindControl("chkReversa");

        int pedidoId = int.Parse(Request.QueryString["pedidoId"]);

        var pedidoDc = new dbCommerceDataContext();
        var pedido = (from c in pedidoDc.tbPedidos where c.pedidoId == pedidoId select c).FirstOrDefault();
        if (pedido != null)
        {
            if (chkMaoPropria.Checked && pedido.maoPropria == false)
            {
                StringBuilder interacao = new StringBuilder();
                interacao.Append("<b>Marcado para enviar por Mão Própria</b><br>");
                interacaoInclui(interacao.ToString(), "False");
            }
            else if (chkMaoPropria.Checked == false && pedido.maoPropria == true)
            {
                StringBuilder interacao = new StringBuilder();
                interacao.Append("<b>Marcado para não enviar por Mão Própria</b><br>");
                interacaoInclui(interacao.ToString(), "False");
            }
            if (chkReversa.Checked && pedido.gerarReversa == false)
            {
                StringBuilder interacao = new StringBuilder();
                interacao.Append("<b>Marcado para realizar Reversa</b><br>");
                interacaoInclui(interacao.ToString(), "False");
            }
            else if (chkReversa.Checked == false && pedido.gerarReversa == true)
            {
                StringBuilder interacao = new StringBuilder();
                interacao.Append("<b>Marcado para não realizar Reversa</b><br>");
                interacaoInclui(interacao.ToString(), "False");
            }
            pedido.maoPropria = chkMaoPropria.Checked;
            pedido.gerarReversa = chkReversa.Checked;
            pedidoDc.SubmitChanges();
            Response.Write("<script>alert('Opções de envio gravadas com sucesso.');</script>");

        }
    }

    private void verificaPrazoFinalPedido(int pedidoId, int statusDoPedido, int? prazoMaximoPedidos, DateTime? prazoFinalPedido)
    {
        if (statusDoPedido == 3 | statusDoPedido == 11)
        {
            var pedidoDc = new dbCommerceDataContext();

            if (prazoMaximoPedidos != null)
            {
                if (prazoFinalPedido == null)
                {
                    var pedido = (from c in pedidoDc.tbPedidos where c.pedidoId == pedidoId select c).First();
                    int prazoInicioFabricacao = 0;
                    if (pedido.prazoInicioFabricao != null)
                    {
                        int diferencaDias = (DateTime.Now - Convert.ToDateTime(pedido.dataHoraDoPedido)).Days;
                        prazoInicioFabricacao = (int)pedido.prazoInicioFabricao - diferencaDias;
                        if (prazoInicioFabricacao < 0) prazoInicioFabricacao = 0;
                    }
                    int prazoFinal = rnFuncoes.retornaPrazoDiasUteis((int)pedido.prazoMaximoPedidos, prazoInicioFabricacao);

                    pedido.prazoFinalPedido = DateTime.Now.AddDays(prazoFinal);
                    pedidoDc.SubmitChanges();
                }
            }
        }
    }
    protected void lstPedidosFornecedor_OnItemDataBound(object sender, ListViewItemEventArgs e)
    {
        int pedidoId = int.Parse(Request.QueryString["pedidoId"]);
        var pedidoDc = new dbCommerceDataContext();
        var hdfIdPedidoFornecedor = (HiddenField)e.Item.FindControl("hdfIdPedidoFornecedor");
        var litEntregue = (Literal)e.Item.FindControl("litEntregue");
        var litDataPrevista = (Literal)e.Item.FindControl("litDataPrevista");
        int idPedidoFornecedor = Convert.ToInt32(hdfIdPedidoFornecedor.Value);
        var itensDoPedido = (from c in pedidoDc.tbPedidoFornecedorItems
                             where c.idPedido == pedidoId && c.idPedidoFornecedor == idPedidoFornecedor
                             select c).Count();
        var pedidoFornecedor = (from c in pedidoDc.tbPedidoFornecedors
                                where c.idPedidoFornecedor == idPedidoFornecedor
                                select c).First();
        var itensDoPedidoEntregues = (from c in pedidoDc.tbPedidoFornecedorItems
                                      where c.idPedido == pedidoId && c.idPedidoFornecedor == idPedidoFornecedor && c.entregue == true
                                      select c).Count();
        litEntregue.Text = itensDoPedidoEntregues == 0 ? "Não" : itensDoPedidoEntregues < itensDoPedido ? "Parcialmente" : "Sim";

        if (itensDoPedidoEntregues == 0 | itensDoPedidoEntregues < itensDoPedido)
        {
            if (pedidoFornecedor.dataPrevista != null)
            {
                litDataPrevista.Text = "Data prevista pelo Fornecedor: " + pedidoFornecedor.dataPrevista.Value.ToShortDateString();
            }
        }
    }


    #region modificar itens do pedido
    protected void btnCancelarItemPedido_Click(object sender, EventArgs e)
    {
        int produtoId = cancelaItemNoPedido(Convert.ToInt32(hdfItemCancelar.Value));
        int itemPedidoId = Convert.ToInt32(hdfItemCancelar.Value);
        var produtosDc = new dbCommerceDataContext();
        var produto = (from c in produtosDc.tbProdutos where c.produtoId == produtoId select c).FirstOrDefault();
        var pedidoId = int.Parse(Request.QueryString["pedidoId"]);
        var produtoPrecoNoCarrinho = (from c in produtosDc.tbItensPedidos where c.pedidoId == pedidoId && c.produtoId == produtoId select new { c.itemValor, c.itemQuantidade }).FirstOrDefault();
        interacaoInclui(produto.produtoNome + " - quantidade: " + produtoPrecoNoCarrinho.itemQuantidade + " valor: " + produtoPrecoNoCarrinho.itemValor.ToString("c") + (produtoPrecoNoCarrinho.itemQuantidade > 1 ? " total = " + (produtoPrecoNoCarrinho.itemQuantidade * produtoPrecoNoCarrinho.itemValor).ToString("c") : "") + " - " + " cancelado do pedido<br>" + "Motivo: " + ddlMotivoCancelarItem.SelectedItem.Text + "<br>" + txtObservacaoCancelarItemPedido.Text + "<br>Fornecedor: " + produto.tbProdutoFornecedor.fornecedorNome, "False");
        popCancelarItemPedido.ShowOnPageLoad = false;
        hdfItemCancelar.Value = "";
        dtlItensPedido.DataBind();
        dtvPedido.DataBind();

        #region Cancelar na tbItemPedidoEstoque
        var itemsPedidoEstoque = (from c in produtosDc.tbItemPedidoEstoques where c.itemPedidoId == itemPedidoId select c);
        foreach (var itemPedidoEstoque in itemsPedidoEstoque)
        {
            itemPedidoEstoque.dataReserva = null;
            itemPedidoEstoque.reservado = false;
            itemPedidoEstoque.cancelado = true;
        }
        produtosDc.SubmitChanges();
        #endregion Cancelar na tbItemPedidoEstoque

        var item = (from c in produtosDc.tbItensPedidos where c.itemPedidoId == itemPedidoId select c).First();
        atualizaPrazoPostagemAposInclusaoDeProduto(int.Parse(Request.QueryString["pedidoId"]));

        var queueLiberar = new tbQueue();
        queueLiberar.tipoQueue = 20; //Libera produtos cancelados
        queueLiberar.agendamento = DateTime.Now;
        queueLiberar.idRelacionado = item.pedidoId;
        queueLiberar.mensagem = "";
        queueLiberar.concluido = false;
        queueLiberar.andamento = false;
        produtosDc.tbQueues.InsertOnSubmit(queueLiberar);
        produtosDc.SubmitChanges();
        bool recalcularTotais = true;
        if ((item.brinde ?? false) == false) recalcularTotais = true;
        if (item.tbItemPedidoTipoAdicao != null)
        {
            if (item.tbItemPedidoTipoAdicao.cobrarCliente == false) recalcularTotais = false;
        }
        if (recalcularTotais) rnPedidos.recalculaTotaisPedido(int.Parse(Request.QueryString["pedidoId"]));
        if (recalcularTotais) rnPagamento.atualizaDescontoMaximoPedido(int.Parse(Request.QueryString["pedidoId"]));
        if (recalcularTotais) rnPedidos.AtualizaDescontoPedido(int.Parse(Request.QueryString["pedidoId"]));

        rnPedidos.AdicionaQueueAtualizaPrazo(pedidoId);
        #region Manda para tabela de Queue para poder verificar se coloca na fila de completos
        var queue = new tbQueue();
        queue.agendamento = DateTime.Now;
        queue.andamento = false;
        queue.concluido = false;
        queue.idRelacionado = pedidoId;
        queue.tipoQueue = 8;
        queue.mensagem = "";
        produtosDc.tbQueues.InsertOnSubmit(queue);
        produtosDc.SubmitChanges();
        #endregion

    }

    protected void btnAdicionarItemPedido_Click(object sender, EventArgs e)
    {
        int pedidoId = int.Parse(Request.QueryString["pedidoId"]);
        var data = new dbCommerceDataContext();
        var envioPendente = (from c in data.tbPedidoEnvios
                             where c.idPedido == pedidoId && (c.dataFimSeparacao == null | c.dataInicioEmbalagem == null | c.dataFimEmbalagem == null) && c.tbPedido.statusDoPedido != 5
                             select c).Any();

        if (envioPendente)
        {
            AlertShow("Pedido já iniciou a separação. Não foi possível adicionar produto.");
            return;
        }

        if (ddlComplementoIdDaEmpresa.Visible == false)
        {
            int produtoId = 0;
            var produtosDc = new dbCommerceDataContext();
            //string idDaEmpresa = txtAdicionaItemIdDaEmpresa.Text.Trim();
            int.TryParse(txtAdicionaItemIdDaEmpresa.Text, out produtoId);

            var produto = (from c in produtosDc.tbProdutos where c.produtoId == produtoId && c.bloqueado == false select c);
            if (produto.Count() > 1)
            {
                ddlComplementoIdDaEmpresa.DataSource = produto;
                ddlComplementoIdDaEmpresa.DataBind();
                ddlComplementoIdDaEmpresa.Visible = true;
                trComplementoIdDaEmpresaAdicionar.Visible = true;
                AlertShow("Produto com opções. Favor selecionar o complemento e clicar em adicionar novamente.");
            }
            else if (produto.Count() == 1)
            {
                var produtoItem = produto.First();
                adicionaOuSubstituiProduto(produtoItem);

                //atualiza Prazo Postagem
                atualizaPrazoPostagemAposInclusaoDeProduto(int.Parse(Request.QueryString["pedidoId"]));
                var tipoAdicao = (from c in data.tbItemPedidoTipoAdicaos where c.idItemPedidoTipoAdicao == Convert.ToInt32(ddlMotivoAdicionarItem.SelectedValue) select c).First();

                limpaCamposAdicionarCancelarItemPedido();
                var recalcularTotais = tipoAdicao.cobrarCliente;


                if (recalcularTotais) rnPedidos.recalculaTotaisPedido(int.Parse(Request.QueryString["pedidoId"]));
                if (recalcularTotais) rnPagamento.atualizaDescontoMaximoPedido(int.Parse(Request.QueryString["pedidoId"]));
                if (recalcularTotais) rnPedidos.AtualizaDescontoPedido(int.Parse(Request.QueryString["pedidoId"]));
                dtlItensPedido.DataBind();
                dtvPedido.DataBind();

                //rnProdutos.AtualizaOutlet(produtoId);

                #region tratar quando pedido já estiver como enviado

                produtosDc = new dbCommerceDataContext();
                var pedido = (from c in produtosDc.tbPedidos where c.pedidoId == pedidoId select c).FirstOrDefault();
                pedido.idUsuarioSeparacao = null;
                pedido.dataInicioSeparacao = null;
                pedido.dataFimSeparacao = null;
                pedido.separado = false;

                if (chkBrinde.Checked)
                {
                    if (pedido.statusDoPedido == 5)
                    {
                        pedido.statusDoPedido = 11;
                        produtosDc.SubmitChanges();

                    }
                }
                produtosDc.SubmitChanges();
                #endregion tratar quando pedido já estiver como enviado


                #region Manda para tabela de Queue para poder verificar se coloca na fila de completos
                var queue = new tbQueue
                {
                    agendamento = DateTime.Now,
                    andamento = false,
                    concluido = false,
                    idRelacionado = pedidoId,
                    tipoQueue = 8,
                    mensagem = ""
                };
                produtosDc.tbQueues.InsertOnSubmit(queue);
                produtosDc.SubmitChanges();
                #endregion



            }
            else
            {
                AlertShow("Produto não encontrado.");
            }
        }
        else
        {
            int produtoId = (Convert.ToInt32(ddlComplementoIdDaEmpresa.SelectedValue));
            var produtosDc = new dbCommerceDataContext();
            var produtoItem = (from c in produtosDc.tbProdutos where c.produtoId == produtoId select c).First();
            var tipoAdicao = (from c in data.tbItemPedidoTipoAdicaos where c.idItemPedidoTipoAdicao == Convert.ToInt32(ddlMotivoAdicionarItem.SelectedValue) select c).First();

            adicionaOuSubstituiProduto(produtoItem);
            limpaCamposAdicionarCancelarItemPedido();
            var recalcularTotais = tipoAdicao.cobrarCliente;


            if (recalcularTotais) rnPedidos.recalculaTotaisPedido(int.Parse(Request.QueryString["pedidoId"]));
            if (recalcularTotais) rnPedidos.AtualizaDescontoPedido(int.Parse(Request.QueryString["pedidoId"]));

            if (chkBrinde.Checked == false) rnPedidos.recalculaTotaisPedido(int.Parse(Request.QueryString["pedidoId"]));
            if (chkBrinde.Checked == false) rnPagamento.atualizaDescontoMaximoPedido(int.Parse(Request.QueryString["pedidoId"]));
            dtlItensPedido.DataBind();
            dtvPedido.DataBind();
            dtlInteracao.DataBind();
            popAdicionarItemPedido.ShowOnPageLoad = false;
        }


    }

    private void atualizaPrazoPostagemAposInclusaoDeProduto(int pedidoId)
    {
        var produtosDc = new dbCommerceDataContext();
        var queue = new tbQueue();
        queue.tipoQueue = 13;
        queue.agendamento = DateTime.Now.AddMinutes(5);
        queue.idRelacionado = pedidoId;
        queue.mensagem = "";
        queue.concluido = false;
        queue.andamento = false;
        produtosDc.tbQueues.InsertOnSubmit(queue);
        produtosDc.SubmitChanges();
        return;


        var pedido = (from c in produtosDc.tbPedidos where c.pedidoId == pedidoId select c).First();
        var itensPedido = (from c in produtosDc.tbItensPedidos where c.pedidoId == pedidoId && (c.cancelado ?? false) == false select c).ToList();

        DateTime prazoProdutoOriginal = DateTime.Now;
        int dias = 0;
        int prazoInicioFabricao = 0;


        foreach (var item in itensPedido)
        {
            int prazoProduto = 0;
            int prazoInicioFabricaoProduto = 0;

            var relacionados = (from c in produtosDc.tbItensPedidoCombos where c.idItemPedido == item.itemPedidoId && c.enviado == false select c);
            #region Relacionados
            if (relacionados.Any())
            {
                foreach (var relacionado in relacionados)
                {
                    if (relacionado.enviado == false)
                    {
                        var reservasAtuais = (from c in produtosDc.tbProdutoReservaEstoques
                                              where
                                                  c.idPedido == pedidoId && c.idItemPedido == item.itemPedidoId &&
                                                  c.idProduto == relacionado.produtoId
                                              select c).Count();
                        if (reservasAtuais < Convert.ToInt32(item.itemQuantidade))
                        {
                            var pedidosFornecedorItem = (from c in produtosDc.tbPedidoFornecedorItems
                                                         where
                                                             c.idItemPedido == item.itemPedidoId &&
                                                             c.idProduto == relacionado.produtoId
                                                         select c).ToList();
                            if (pedidosFornecedorItem.Count >= Convert.ToInt32(item.itemQuantidade))
                            {
                                var dataEntrega =
                                    pedidosFornecedorItem.OrderByDescending(
                                        x => x.tbPedidoFornecedor.dataLimite)
                                        .First()
                                        .tbPedidoFornecedor.dataLimite;

                                if (dataEntrega > DateTime.Now)
                                //só calcula o prazo de entrega caso ainda não tenha sido entregue
                                {
                                    //var prazo = (DateTime.Now - dataEntrega).TotalDays + 1;
                                    var prazo = (dataEntrega - DateTime.Now).TotalDays + 1;
                                    if (prazoProduto < prazo && prazo > 0)
                                    {
                                        prazoProduto = Convert.ToInt32(prazo);
                                    }
                                }
                                else
                                {
                                    var prazo = 1;
                                    if (prazoProduto < prazo && prazo > 0)
                                    {
                                        prazoProduto = Convert.ToInt32(prazo);
                                    }
                                }


                            }
                            else
                            {
                                var prazo =
                                    (relacionado.tbProduto.tbProdutoFornecedor
                                        .fornecedorPrazoDeFabricacao ?? 0);
                                if (prazoProduto < prazo && prazo > 0)
                                {
                                    prazoProduto = prazo;
                                }

                                int prazoInicioFabricaoFornecedor = 0;
                                var inicioFabricacaoFornecedor =
                                    relacionado.tbProduto.tbProdutoFornecedor.dataInicioFabricacao == null
                                        ? DateTime.Now
                                        : (DateTime)relacionado.tbProduto.tbProdutoFornecedor.dataInicioFabricacao;
                                if (inicioFabricacaoFornecedor > DateTime.Now)
                                {
                                    prazoInicioFabricaoFornecedor = (inicioFabricacaoFornecedor - DateTime.Now).Days;
                                    if (prazoInicioFabricaoFornecedor < 0) prazoInicioFabricaoFornecedor = 0;
                                }

                                if (relacionado.tbProduto.tbProdutoFornecedor.dataFimFabricacao != null)
                                {
                                    if (
                                        ((DateTime)relacionado.tbProduto.tbProdutoFornecedor.dataFimFabricacao).AddDays
                                            (-2).Date <= item.dataDaCriacao.Date &&
                                        inicioFabricacaoFornecedor.Date >= item.dataDaCriacao.Date)
                                    {
                                        if (prazoInicioFabricaoProduto < prazoInicioFabricaoFornecedor &&
                                            prazoInicioFabricaoFornecedor > 0)
                                        {
                                            prazoInicioFabricaoProduto = prazoInicioFabricaoFornecedor;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            #endregion Relacionado
            #region Produto sem Combo
            else
            {
                if ((item.enviado ?? false) == false)
                {
                    var reservasAtuais = (from c in produtosDc.tbProdutoReservaEstoques
                                          where
                                              c.idPedido == pedidoId && c.idItemPedido == item.itemPedidoId &&
                                              c.idProduto == item.produtoId
                                          select c).Count();
                    if (reservasAtuais < Convert.ToInt32(item.itemQuantidade))
                    {
                        var pedidosFornecedorItem = (from c in produtosDc.tbPedidoFornecedorItems
                                                     where
                                                         c.idItemPedido == item.itemPedidoId &&
                                                         c.idProduto == item.produtoId
                                                     select c).ToList();
                        if (pedidosFornecedorItem.Count >= Convert.ToInt32(item.itemQuantidade))
                        {
                            var dataEntrega =
                                pedidosFornecedorItem.OrderByDescending(
                                    x => x.tbPedidoFornecedor.dataLimite)
                                    .First()
                                    .tbPedidoFornecedor.dataLimite;

                            if (dataEntrega > DateTime.Now)//só calcula o prazo de entrega caso ainda não tenha sido entregue
                            {

                                //var prazo = (DateTime.Now - dataEntrega).TotalDays + 1;
                                var prazo = (dataEntrega - DateTime.Now).TotalDays + 1;
                                if (prazoProduto < prazo && prazo > 0)
                                {
                                    prazoProduto = Convert.ToInt32(prazo);
                                }
                            }
                            else
                            {
                                var prazo = 1;
                                if (prazoProduto < prazo && prazo > 0)
                                {
                                    prazoProduto = Convert.ToInt32(prazo);
                                }
                            }
                        }
                        else
                        {
                            var prazo =
                                (item.tbProduto.tbProdutoFornecedor
                                    .fornecedorPrazoDeFabricacao ?? 0);
                            if (prazoProduto < prazo && prazo > 0)
                            {
                                prazoProduto = prazo;
                            }

                            int prazoInicioFabricaoFornecedor = 0;
                            var inicioFabricacaoFornecedor = item.tbProduto.tbProdutoFornecedor.dataInicioFabricacao == null ? DateTime.Now : (DateTime)item.tbProduto.tbProdutoFornecedor.dataInicioFabricacao;
                            if (inicioFabricacaoFornecedor.Date > item.dataDaCriacao.Date)
                            {
                                prazoInicioFabricaoFornecedor = (inicioFabricacaoFornecedor - DateTime.Now).Days;
                                if (prazoInicioFabricaoFornecedor < 0) prazoInicioFabricaoFornecedor = 0;
                            }

                            if (item.tbProduto.tbProdutoFornecedor.dataFimFabricacao != null)
                            {
                                if (((DateTime)item.tbProduto.tbProdutoFornecedor.dataFimFabricacao).AddDays(-2).Date <= item.dataDaCriacao.Date && inicioFabricacaoFornecedor.Date >= item.dataDaCriacao.Date)
                                {
                                    if (prazoInicioFabricaoProduto < prazoInicioFabricaoFornecedor && prazoInicioFabricaoFornecedor > 0)
                                    {
                                        prazoInicioFabricaoProduto = prazoInicioFabricaoFornecedor;
                                    }
                                }
                            }
                        }
                    }
                }

            }
            #endregion Produto sem Combo

            //compara as datas de fabricação de todos os produtos para defnir qual é a maior
            if (prazoInicioFabricao < prazoInicioFabricaoProduto) prazoInicioFabricao = prazoInicioFabricaoProduto;
            if (item.dataDaCriacao < (pedido.dataConfirmacaoPagamento ?? DateTime.Now))// verifica se o item foi inserido antes da confirmação de pagamento
            {
                int diasComparacao = rnFuncoes.retornaPrazoDiasUteis(prazoProduto + 1, prazoInicioFabricaoProduto, (DateTime)(pedido.dataConfirmacaoPagamento ?? DateTime.Now));
                DateTime dataComparacao = (pedido.dataConfirmacaoPagamento ?? DateTime.Now).AddDays(diasComparacao);

                if (dataComparacao > prazoProdutoOriginal)
                {
                    prazoProdutoOriginal = dataComparacao;
                }
                if (prazoProduto > dias)
                {
                    dias = prazoProduto;
                }
            }
            else// produto inserido após o pedido já ter sido pago
            {
                int diasComparacao = rnFuncoes.retornaPrazoDiasUteis(prazoProduto + 1, prazoInicioFabricaoProduto, (DateTime)(item.dataDaCriacao));
                DateTime dataComparacao = (item.dataDaCriacao).AddDays(diasComparacao);

                if (dataComparacao > prazoProdutoOriginal)// verifica se a data de fabricação do produto incluso é maior que as datas dos produtos anteriores ao pagamento
                {
                    prazoProdutoOriginal = dataComparacao;
                }
                if (prazoProduto > dias)// verfica se é necessário alterar o prazo em dias para calculo de postagem
                {
                    dias = prazoProduto;
                }
            }

        }

        if (dias == 0) dias = 1;
        pedido.prazoInicioFabricao = prazoInicioFabricao;
        if (pedido.dataConfirmacaoPagamento == null)
        {
            pedido.prazoMaximoPedidos = dias;
        }
        else
        {
            if (pedido.prazoMaximoPostagemAtualizado != prazoProdutoOriginal)
            {
                string msg = "Prazo atualizado de :" + (pedido.prazoMaximoPostagemAtualizado == null ? pedido.prazoFinalPedido.Value.ToShortDateString() : pedido.prazoMaximoPostagemAtualizado.Value.ToShortDateString());

                rnInteracoes.interacaoInclui(pedido.pedidoId, msg + " para: " + prazoProdutoOriginal.ToShortDateString(), rnUsuarios.retornaNomeUsuarioLogado(), "False");
            }

            pedido.prazoMaximoPostagemAtualizado = prazoProdutoOriginal;
        }

        produtosDc.SubmitChanges();


    }

    private void limpaCamposAdicionarCancelarItemPedido()
    {
        ddlComplementoIdDaEmpresa.Visible = false;
        txtAdicionaItemIdDaEmpresa.Text = "";
        txtObservacaoAdicionarItem.Text = "";
        txtObservacaoCancelarItemPedido.Text = "";
        popAdicionarItemPedido.ShowOnPageLoad = false;
        chkBrinde.Enabled = true;
    }
    private void adicionaOuSubstituiProduto(tbProduto produto)
    {
        var produtosDc = new dbCommerceDataContext();

        adicionaItemNoPedido(produto);
        if (!string.IsNullOrEmpty(hdfItemCancelar.Value))
        {
            int produtoId = cancelaItemNoPedido(Convert.ToInt32(hdfItemCancelar.Value));
            var produtoCancelado = (from c in produtosDc.tbProdutos where c.produtoId == produtoId select c).FirstOrDefault();
            interacaoInclui(produtoCancelado.produtoNome + " trocado por " + produto.produtoNome + "<br>" + txtObservacaoCancelarItemPedido.Text, "False");
        }
        else
        {
            string mensagem = chkBrinde.Checked ? "Brinde " : "";
            mensagem = mensagem + produto.produtoNome + " " + (produto.produtoPrecoPromocional > 0 ? Convert.ToDecimal(produto.produtoPrecoPromocional).ToString("C") : produto.produtoPreco.ToString("C")) + " adicionado<br>" + txtObservacaoCancelarItemPedido.Text;
            mensagem += "Motivo:" + ddlMotivoAdicionarItem.SelectedItem.Text;
            if (!String.IsNullOrEmpty(txtObservacaoAdicionarItem.Text))
                mensagem += "<br/>Observação:" + txtObservacaoAdicionarItem.Text;

            interacaoInclui(mensagem, "False");
        }
    }

    private int cancelaItemNoPedido(int itemPedidoId)
    {
        var itensPedidoDc = new dbCommerceDataContext();
        var itensPedido = (from c in itensPedidoDc.tbItensPedidos where c.itemPedidoId == itemPedidoId select c).First();
        itensPedido.cancelado = true;
        itensPedido.motivoCancelamento = ddlMotivoCancelarItem.SelectedValue;
        itensPedidoDc.SubmitChanges();

        rnPedidos.retiraVinculoEstoqueItemPedido(itemPedidoId);
        return itensPedido.produtoId;
    }

    private void adicionaItemNoPedido(tbProduto produto)
    {
        int pedidoId = Convert.ToInt32(Request.QueryString["pedidoId"]);
        var itensPedidoDc = new dbCommerceDataContext();
        var motivoAdicao = (from c in itensPedidoDc.tbItemPedidoTipoAdicaos where c.idItemPedidoTipoAdicao == Convert.ToInt32(ddlMotivoAdicionarItem.SelectedValue) select c).First();
        var item = new tbItensPedido();
        item.brinde = motivoAdicao.brinde;
        item.cancelado = false;
        item.pedidoId = pedidoId;
        item.dataDaCriacao = DateTime.Now;
        item.itemValor = motivoAdicao.adicionarValorDoItem == false ? 0 : produto.produtoPrecoPromocional > 0 ? Convert.ToDecimal(produto.produtoPrecoPromocional) : produto.produtoPreco;
        item.itemQuantidade = 1;
        item.produtoId = produto.produtoId;
        item.produtoIdDaEmpresa = produto.produtoIdDaEmpresa;
        item.itemPresente = "False";
        item.garantiaId = 0;
        item.ItemMensagemDoCartao = "";
        item.produtoCodDeBarras = "";
        item.motivoAdicao = ddlMotivoAdicionarItem.SelectedItem.Text;
        item.valorCusto = motivoAdicao.adicionarValorDoCusto == false ? 0 : produto.produtoPrecoDeCusto;
        item.idItemPedidoTipoAdicao = Convert.ToInt32(ddlMotivoAdicionarItem.SelectedValue);

        HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
        usuarioLogadoId = Request.Cookies["usuarioLogadoId"];
        if (usuarioLogadoId != null)
        {
            item.idUsuarioAdicao = Convert.ToInt32(usuarioLogadoId.Value);
        }

        itensPedidoDc.tbItensPedidos.InsertOnSubmit(item);
        itensPedidoDc.SubmitChanges();




        #region Manda para tabela de Queue para poder verificar se coloca na fila de completos
        var queue = new tbQueue();
        queue.agendamento = DateTime.Now;
        queue.andamento = false;
        queue.concluido = false;
        queue.idRelacionado = pedidoId;
        queue.tipoQueue = 4; // Gera combo
        queue.mensagem = "";
        itensPedidoDc.tbQueues.InsertOnSubmit(queue);
        itensPedidoDc.SubmitChanges();
        #endregion

        #region Manda para tabela de Queue para poder verificar se coloca na fila de completos
        var queue1 = new tbQueue();
        queue1.agendamento = DateTime.Now;
        queue1.andamento = false;
        queue1.concluido = false;
        queue1.idRelacionado = pedidoId;
        queue1.tipoQueue = 5; // Gera combo
        queue1.mensagem = "";
        itensPedidoDc.tbQueues.InsertOnSubmit(queue1);
        itensPedidoDc.SubmitChanges();
        #endregion


        rnPedidos.reservaProdutoOuPedeFornecedor(pedidoId, produto.produtoId, 1, item.itemPedidoId);

        //var pedidosDc = new dbCommerceDataContext();


        using (var pedidoDc = new dbCommerceDataContext())
        {
            var pedido = (from c in pedidoDc.tbPedidos where c.pedidoId == pedidoId select c).First();

            if (chkBrinde.Checked)
            {
                pedido.prioridadeEnvio = 1;

                try
                {
                    pedidoDc.SubmitChanges();
                }
                catch (ChangeConflictException ex)
                {
                    foreach (MemberChangeConflict memberConflict in pedidoDc.ChangeConflicts.SelectMany(objConflict => objConflict.MemberConflicts))
                    {
                        memberConflict.Resolve(RefreshMode.KeepCurrentValues);
                    }
                    pedidoDc.SubmitChanges(ConflictMode.ContinueOnConflict);
                }
            }

            if (pedido.statusDoPedido == 4 | pedido.statusDoPedido == 5 | pedido.statusDoPedido == 8 | pedido.statusDoPedido == 9 | pedido.statusDoPedido == 10)
            {
                pedido.idUsuarioEmbalado = null;
                pedido.dataSendoEmbalado = null;
                pedido.dataEmbaladoFim = null;
                pedido.idUsuarioSeparacao = null;
                pedido.dataInicioSeparacao = null;
                pedido.dataFimSeparacao = null;
                pedido.separado = false;
                pedido.formaDeEnvio = null;
                pedido.statusDoPedido = 11;

                try
                {
                    pedidoDc.SubmitChanges();
                }
                catch (ChangeConflictException ex)
                {
                    foreach (MemberChangeConflict memberConflict in pedidoDc.ChangeConflicts.SelectMany(objConflict => objConflict.MemberConflicts))
                    {
                        memberConflict.Resolve(RefreshMode.KeepCurrentValues);
                    }
                    pedidoDc.SubmitChanges(ConflictMode.ContinueOnConflict);
                }
            }
        }

    }
    protected void btnAdicionarItemPedido_OnClick(object sender, EventArgs e)
    {
        int pedidoId = int.Parse(Request.QueryString["pedidoId"]);
        var produtosDc = new dbCommerceDataContext();
        var enviosGeral = (from c in produtosDc.tbPedidoEnvios where c.idPedido == pedidoId select c).ToList();

        var envioPendente = (from c in enviosGeral
                             where c.idPedido == pedidoId && (c.dataFimSeparacao == null | c.dataInicioEmbalagem == null | c.dataFimEmbalagem == null) && c.tbPedido.statusDoPedido != 5
                             select c).Any();
        if (envioPendente)
        {
            AlertShow("Aguarde o produto ser embalado ou entre em contato com a Expedição");
        }
        else
        {
            chkBrinde.Enabled = true;
            popAdicionarItemPedido.ShowOnPageLoad = true;
        }
        chkBrinde.Enabled = true;
        popAdicionarItemPedido.ShowOnPageLoad = true;
    }
    protected void btnProdutoPersonalizado_OnClick(object sender, EventArgs e)
    {
        int produtoPaiId = Convert.ToInt32(((ASPxButton)sender).CommandArgument.Split('#')[0]);
        int itemPedidoId = Convert.ToInt32(((ASPxButton)sender).CommandArgument.Split('#')[1]);
        using (var data = new dbCommerceDataContext())
        {
            List<int> itemPedidoProdutos = (from c in data.tbItemPedidoEstoques where c.itemPedidoId == itemPedidoId select c).Select(x => x.produtoId).ToList();

            //var produtosPersonalizados = (from c in data.tbProdutoPersonalizacaos
            //                              where c.idProdutoPai == produtoPaiId
            //                              select new
            //                              {
            //                                  c.idProdutoFilhoPersonalizacao,
            //                                  c.tbProduto1.produtoNome,
            //                                  c.idProdutoPai,
            //                                  itemPedidoId
            //                              }).ToList();


            var produtosPersonalizados = (from c in data.tbProdutoPersonalizacaos
                                          where itemPedidoProdutos.Contains(c.idProdutoPai)
                                          select new
                                          {
                                              c.tbProduto.produtoNome,
                                              idProdutoPai = c.tbProduto.produtoId,
                                              itemPedidoId
                                          }).Distinct().ToList();


            lstProdutosPersonalizacao.DataSource = produtosPersonalizados;
            lstProdutosPersonalizacao.DataBind();

            //if (!produtosPersonalizados.Any(x => x.idProdutoPai > 0))
            //{
            //    var produtoCombo = (from c in data.tbProdutoRelacionados where c.idProdutoPai == produtoPaiId select c).ToList();
            //    List<int> produtosCombo = produtoCombo.Select(produto => produto.idProdutoFilho).Distinct().ToList();

            //    var produtoPersonalizadosPais = (from c in data.tbProdutoPersonalizacaos
            //                                     where produtosCombo.Contains(c.idProdutoPai)
            //                                     select new
            //                                     {
            //                                         c.tbProduto.produtoNome,
            //                                         c.idProdutoPai,
            //                                     }).Distinct().ToList();

            //    var bloqueiaSegundaTroca = (from c in data.tbItemPedidoEstoques where c.itemPedidoId == itemPedidoId select c).ToList();

            //    var itensPaisPendentesTroca = (from c in produtoPersonalizadosPais
            //                                   join d in bloqueiaSegundaTroca on c.idProdutoPai equals d.produtoId
            //                                   select new
            //                                   {
            //                                       c.produtoNome,
            //                                       c.idProdutoPai,
            //                                       itemPedidoId
            //                                   }).Distinct().ToList();

            //    lstProdutosPersonalizacao.DataSource = itensPaisPendentesTroca;
            //    lstProdutosPersonalizacao.DataBind();

            //}

        }

        hfItemPedidoIdProdutoPersonalizado.Value = itemPedidoId.ToString();
        popProdutoPersonalizacao.ShowOnPageLoad = true;
    }

    protected void btnProdutosCancelados_OnClick(object sender, EventArgs e)
    {
        try
        {
            int pedidoId = int.Parse(Request.QueryString["pedidoId"]);
            using (var data = new dbCommerceDataContext())
            {
                var itensCancelados = (from c in data.tbItensPedidos
                                       where c.pedidoId == pedidoId && (c.cancelado ?? false)
                                       select new
                                       {
                                           c.tbProduto.produtoId,
                                           c.tbProduto.produtoNome,
                                           c.itemValor,
                                           c.itemPedidoId,
                                           brinde = (c.brinde ?? false)
                                       }).ToList();
                lstReativarProdutos.DataSource = itensCancelados;
                lstReativarProdutos.DataBind();
            }

            popDescancelarProduto.ShowOnPageLoad = true;
        }
        catch (Exception)
        {

        }


    }

    #endregion

    protected void btnProdutosPendentesDeEnvio_OnClick(object sender, EventArgs e)
    {
        try
        {

            int pedidoId = int.Parse(Request.QueryString["pedidoId"]);
            using (var data = new dbCommerceDataContext())
            {
                var itensPendentesDeEnvio = (from c in data.tbItemPedidoEstoques where c.tbItensPedido.pedidoId == pedidoId && !c.cancelado && !c.enviado select new { c.tbProduto.produtoId, c.tbProduto.produtoNome, c.tbItensPedido.itemValor, c.itemPedidoId, c.idItemPedidoEstoque }).ToList();
                lstMarcarProdutoComoEnviado.DataSource = itensPendentesDeEnvio;
                lstMarcarProdutoComoEnviado.DataBind();
            }

            popMarcarProdutoComoEnviado.ShowOnPageLoad = true;
        }
        catch (Exception)
        {

        }
    }


    protected void btnConfirmarEnvioParcial_OnClick(object sender, EventArgs e)
    {
        var pedidosDc = new dbCommerceDataContext();
        bool parcialEnviado = Convert.ToInt32(hdfParcialEnviado.Value) == 1;
        int idItemPedidoEstoque = Convert.ToInt32(hdfParcialItemPedido.Value);
        int produtoId = Convert.ToInt32(hdfParcialProduto.Value);
        int pedidoId = int.Parse(Request.QueryString["pedidoId"]);

        string usuario = "";
        HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
        usuarioLogadoId = Request.Cookies["usuarioLogadoId"];
        if (usuarioLogadoId != null)
        {
            usuario =
                rnUsuarios.usuarioSeleciona_PorUsuarioId(int.Parse(usuarioLogadoId.Value.ToString())).Tables[0].Rows
                    [0]["usuarioNome"].ToString();
        }

        var produto = (from c in pedidosDc.tbProdutos where c.produtoId == produtoId select c).First();
        var itemPedido = (from c in pedidosDc.tbItemPedidoEstoques where c.idItemPedidoEstoque == idItemPedidoEstoque select c).FirstOrDefault();
        if (itemPedido != null)
        {
            string interacao = "";
            interacao = "<b>Autorizado envio parcial</b><br>";
            interacao += "<b>Produto:</b> " + produto.produtoNome + "<br>";
            interacao += "<b>Motivo:</b> " + txtMotivoEnvioParcial.Text;
            interacaoInclui(interacao, "False");
            itemPedido.autorizarParcial = true;
            pedidosDc.SubmitChanges();
        }

        rnPedidos.AdicionaQueueChecagemPedidoCompleto(pedidoId);

        Response.Redirect("pedido.aspx?pedidoId=" + Request.QueryString["pedidoId"]);
    }

    public void AlertShow(string message)
    {
        base.ClientScript.RegisterStartupScript(base.GetType(), "myalert", "alert('" + message + "'); ", true);
    }

    protected void btnAlterarPacote_OnCommand(object sender, CommandEventArgs e)
    {
        popAlterarRastreio.ShowOnPageLoad = true;
        var idPedidoPacote = 0;
        int.TryParse(e.CommandArgument.ToString(), out idPedidoPacote);
        if (idPedidoPacote > 0)
        {
            var pedidoDc = new dbCommerceDataContext();
            var pedido = (from c in pedidoDc.tbPedidoPacotes where c.idPedidoPacote == idPedidoPacote select c).First();
            ddlEnviarPor.SelectedValue = pedido.formaDeEnvio;
            txtRastreio.Text = pedido.rastreio;
            chkConcluido.Checked = false;
            hdfPacoteAlterando.Value = pedido.idPedidoPacote.ToString();
        }
    }


    protected void btnAlterarPacotePedido_OnClick(object sender, EventArgs e)
    {
        var idPedidoPacote = 0;
        int.TryParse(hdfPacoteAlterando.Value, out idPedidoPacote);
        if (idPedidoPacote > 0)
        {
            var pedidoDc = new dbCommerceDataContext();
            var pedido = (from c in pedidoDc.tbPedidoPacotes where c.idPedidoPacote == idPedidoPacote select c).First();
            pedido.formaDeEnvio = ddlEnviarPor.SelectedValue;
            pedido.rastreio = txtRastreio.Text;
            pedido.rastreamentoConcluido = chkChamadoConcluido.Checked;
            pedidoDc.SubmitChanges();
            rnPedidos.atualizaStatusRastreio(pedido.idPedido);
            popAlterarRastreio.ShowOnPageLoad = false;

            if (pedido.rastreio != txtRastreio.Text)
            {
                interacaoInclui("Rastreio alterado de " + pedido.rastreio + " para " + txtRastreio.Text, "False");
            }
            fillRastreios();
        }
    }

    protected void btnAlterarChamado_OnCommand(object sender, CommandEventArgs e)
    {
        int idChamado = Convert.ToInt32(e.CommandArgument);
        var data = new dbCommerceDataContext();
        var chamado = (from c in data.tbChamados where c.idChamado == idChamado select c).FirstOrDefault();

        if (ddlUsuarioDepartamento.SelectedIndex > 0)
        {
            chamado.idUsuario = int.Parse(ddlUsuarioDepartamento.SelectedValue);
        }

        CarregarComboDepartamento();
        CarregarDepartamentoOrigem(false);

        if (chamado.idChamadoDepartamento != null)
            ddlDepartamento.SelectedValue = chamado.idChamadoDepartamento.ToString();

        if (chamado.idDepartamentoAbertura != null)
        {
            ddlDepartamentoOrigem.SelectedValue = chamado.idDepartamentoAbertura.ToString();
            ddlDepartamentoOrigem.Enabled = false;
        }


        if (chamado.idChamadoAssunto != null)
        {
            CarregarComboAssunto();
            ddlAssunto.SelectedValue = chamado.idChamadoAssunto.ToString();
        }


        CarregarComboTarefa();
        if (chamado.idChamadoTarefa != null)
            ddlTarefa.SelectedValue = chamado.idChamadoTarefa.ToString();

        CarregarUsuarioDepartamento(int.Parse(ddlDepartamento.SelectedValue));

        txtChamadoData.Text = chamado.dataLimite.ToShortDateString();
        txtChamadoHora.Text = chamado.dataLimite.ToShortTimeString();
        txtChamadoTitulo.Text = chamado.titulo;
        chkChamadoConcluido.Checked = chamado.concluido;
        ddlUsuarioDepartamento.SelectedValue = chamado.idUsuario.ToString();

        //ddlChamadoUsuario.SelectedValue = chamado.idUsuario.ToString();
        hdfIdAgendamento.Value = e.CommandArgument.ToString();
        popAdicionarChamado.ShowOnPageLoad = true;
    }

    private void CarregarUsuarioDepartamento(int idDepartamento)
    {



        var data = new dbCommerceDataContext();

        var usuarioLogado = Request.Cookies["usuarioLogadoId"];

        dynamic usuarios;
        if (idDepartamento > 0)
            usuarios = data.tbUsuarios.Where(x => x.idDepartamento == idDepartamento || x.usuarioId == Convert.ToInt32(usuarioLogado.Value.ToString())).ToList();
        else
            usuarios = data.tbUsuarios.ToList();
        ddlUsuarioDepartamento.DataSource = usuarios;
        ddlUsuarioDepartamento.DataTextField = "usuarioNome";
        ddlUsuarioDepartamento.DataValueField = "usuarioId";
        ddlUsuarioDepartamento.DataBind();

        ddlUsuarioDepartamento.Items.Insert(0, new ListItem("Fila do departamento", "-1"));
        ddlUsuarioDepartamento.SelectedIndex = 0;

    }

    void gravarChamado(string titulo, int? idUsuario, int idPedido, int? idDepartamento)
    {
        var data = new dbCommerceDataContext();
        var chamado = new tbChamado();
        chamado.dataLimite = DateTime.Now;
        chamado.titulo = titulo;
        chamado.prioridade = int.Parse(ddlPrioridade.SelectedValue);
        chamado.dataCadastro = DateTime.Now;
        chamado.descricao = titulo;
        chamado.idUsuarioAbertura = idUsuario;

        if (idUsuario != null)
            chamado.idUsuario = idUsuario;

        chamado.idPedido = idPedido;

        if (idDepartamento != null)
            chamado.idChamadoDepartamento = idDepartamento;

        data.tbChamados.InsertOnSubmit(chamado);
        data.SubmitChanges();
    }
    protected void btnGravarChamado_OnClick(object sender, EventArgs e)
    {
        HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
        usuarioLogadoId = Request.Cookies["usuarioLogadoId"];
        int idUsuario = Convert.ToInt32(usuarioLogadoId.Value.ToString());
        int idChamado = 0;
        int.TryParse(hdfIdAgendamento.Value.ToString(), out idChamado);
        int idDepartamento = Convert.ToInt32(ddlDepartamento.SelectedValue);

        int idAssunto = 0;

        if (!string.IsNullOrEmpty(ddlAssunto.SelectedValue))
            idAssunto = Convert.ToInt32(ddlAssunto.SelectedValue);

        int idTarefa = 0;

        if (!string.IsNullOrEmpty(ddlTarefa.SelectedValue))
            idTarefa = Convert.ToInt32(ddlTarefa.SelectedValue);

        var data = new dbCommerceDataContext();
        if (idChamado > 0)
        {
            string interacao = "";
            var chamado = (from c in data.tbChamados where c.idChamado == idChamado select c).FirstOrDefault();
            interacao += "Chamado " + chamado.titulo + " alterado:<br>";
            if (chamado.concluido != chkChamadoConcluido.Checked)
            {
                interacao += "Marcado como ";
                interacao += chkChamadoConcluido.Checked ? " concluído" : "não concluído";
                interacao += "<br>";
                chamado.idUsuarioConclusao = int.Parse(usuarioLogadoId.Value);
            }
            chamado.concluido = chkChamadoConcluido.Checked;
            var dataAgendamento = DateTime.Now;
            DateTime.TryParse(txtChamadoData.Text + " " + txtChamadoHora.Text, out dataAgendamento);
            if (chamado.dataLimite != dataAgendamento)
            {
                interacao += "Data para conclusão: " + dataAgendamento.ToString() + "<br>";
            }

            if (chamado.idUsuario.ToString() != ddlUsuarioDepartamento.SelectedValue && (ddlUsuarioDepartamento.SelectedIndex > 0))
            {
                interacao += "Responsável alterado para: " + ddlUsuarioDepartamento.SelectedItem.Text + "<br>";
            }

            if (chamado.prioridade.ToString() != ddlPrioridade.SelectedValue.ToString())
            {
                interacao += "Prioridade alterado para: " + ddlPrioridade.SelectedItem.Text + "<br>";
            }

            chamado.idChamadoDepartamento = idDepartamento;
            chamado.idChamadoTarefa = idTarefa;
            chamado.idChamadoAssunto = idAssunto;
            chamado.dataLimite = dataAgendamento;
            chamado.titulo = txtChamadoTitulo.Text;
            if (ddlUsuarioDepartamento.SelectedIndex > 0)
                chamado.idUsuario = Convert.ToInt32(ddlUsuarioDepartamento.SelectedValue);

            data.SubmitChanges();

            var chamadoInteracao = new tbChamadoInteracao();
            chamadoInteracao.interacao = txtChamado.Text;
            chamadoInteracao.dataHora = DateTime.Now;
            chamadoInteracao.idChamado = chamado.idChamado;
            chamadoInteracao.idUsuario = idUsuario;
            data.tbChamadoInteracaos.InsertOnSubmit(chamadoInteracao);
            data.SubmitChanges();

            interacao += "Interação: " + txtChamado.Text;
            interacaoInclui(interacao, "False");
        }
        else
        {
            string interacao = "";
            var chamado = new tbChamado();
            interacao += "Chamado<b> " + txtChamadoTitulo.Text + " </b>criado<br>";
            var dataAgendamento = DateTime.Now;
            DateTime.TryParse(txtChamadoData.Text + " " + txtChamadoHora.Text, out dataAgendamento);
            interacao += "Data para conclusão: " + dataAgendamento.ToString() + "<br>";
            interacao += "Departamento Responsável: " + ddlDepartamento.SelectedItem.Text + "<br>";
            chamado.idChamadoDepartamento = idDepartamento;
            chamado.idChamadoAssunto = idAssunto;
            chamado.idUsuarioAbertura = idUsuario;
            chamado.prioridade = int.Parse(ddlPrioridade.SelectedValue);
            chamado.idDepartamentoAbertura = int.Parse(ddlDepartamentoOrigem.SelectedValue);

            if (ddlTarefa.SelectedIndex > 0)
                chamado.idChamadoTarefa = idTarefa;

            chamado.dataLimite = dataAgendamento;
            chamado.titulo = txtChamadoTitulo.Text;
            chamado.dataCadastro = DateTime.Now;
            chamado.descricao = txtChamado.Text;
            if (ddlUsuarioDepartamento.SelectedIndex > 0)
                chamado.idUsuario = int.Parse(ddlUsuarioDepartamento.SelectedValue);
            //chamado.idUsuario = Convert.ToInt32(ddlChamadoUsuario.SelectedValue);
            chamado.idPedido = Convert.ToInt32(Request.QueryString["pedidoId"]);
            data.tbChamados.InsertOnSubmit(chamado);
            data.SubmitChanges();
            interacao += "Descricao: " + txtChamado.Text;
            interacaoInclui(interacao, "False");
        }
        popAdicionarChamado.ShowOnPageLoad = false;
        sqlChamados.DataBind();
        dtlChamados.DataBind();
    }

    protected void btnAdicionarChamado_OnClick(object sender, EventArgs e)
    {
        popAdicionarChamado.ShowOnPageLoad = true;
        chkChamadoConcluido.Checked = false;
        txtChamado.Text = "";
        hdfIdAgendamento.Value = "";
        HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
        usuarioLogadoId = Request.Cookies["usuarioLogadoId"];
        ddlChamadoUsuario.SelectedValue = usuarioLogadoId.Value;
        popAdicionarChamado.ShowOnPageLoad = true;
        CarregarDepartamentoOrigem(true);

        CarregarComboAssunto();
        CarregarComboTarefa();
        txtChamadoData.Text = System.DateTime.Now.ToShortDateString();
        txtChamadoHora.Text = System.DateTime.Now.ToShortTimeString();

    }

    private void CarregarComboDepartamento()
    {
        var data = new dbCommerceDataContext();

        var deptos = data.tbChamadoDepartamentos.ToList();
        deptos.Insert(0, new tbChamadoDepartamento() { idChamadoDepartamento = 0, nomeChamadoDepartamento = "Selecione" });

        ddlDepartamento.DataSource = deptos.OrderBy(x => x.idChamadoDepartamento);
        ddlDepartamento.DataTextField = "nomeChamadoDepartamento";
        ddlDepartamento.DataValueField = "idChamadoDepartamento";
        ddlDepartamento.DataBind();

    }

    protected void dtlChamados_OnItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            var data = DateTime.Now;
            Label lblData = (Label)e.Item.FindControl("lblData");
            DateTime.TryParse(lblData.Text, out data);

            bool concluido = false;
            HiddenField hdfConcluido = (HiddenField)e.Item.FindControl("hdfConcluido");
            Boolean.TryParse(hdfConcluido.Value, out concluido);
            if (concluido)
            {
                e.Item.BackColor = ColorTranslator.FromHtml("#D2E9FF");
            }
            else if (data < DateTime.Now)
            {
                e.Item.BackColor = ColorTranslator.FromHtml("#FFC6C7");
            }
            else
            {
                e.Item.BackColor = ColorTranslator.FromHtml("#FFFFBB");
            }


        }
    }

    protected void lstNotas_OnItemDataBound(object sender, ListViewItemEventArgs e)
    {
        ListViewDataItem objCurrentItem = (ListViewDataItem)e.Item;
        var nota = (tbNotaFiscal)objCurrentItem.DataItem;

        List<string> cfopDev = new List<string> { "1201", "2201", "1202", "2202", "5202", "1949", "2949", "5949", "6949", "1949", "2949" };
        var pedidoDc = new dbCommerceDataContext();
        bool devolucao = (from c in pedidoDc.tbNotaFiscalItems
                          where c.idNotaFiscal == nota.idNotaFiscal
                            && cfopDev.Contains(c.cfop)
                          select c).Any();
        ///////// Atualiza ultimo status com Invoicy ////////
        string statusAtualInvoicy = rnNotaFiscal.retornaInformacaoNotaPorNumeroNota(
                            nota.numeroNota.ToString(), nota.idNotaFiscal);
        var notaAtual = (from c in pedidoDc.tbNotaFiscals where c.idNotaFiscal == nota.idNotaFiscal select c).FirstOrDefault();
        notaAtual.ultimoStatus = statusAtualInvoicy;
        pedidoDc.SubmitChanges();
        ///////// ///////////////////////////////////////////
        string ultimoStatus = statusAtualInvoicy;

        if (nota != null)
        {
            var hplNotaDanfe = (HyperLink)objCurrentItem.FindControl("hplNotaDanfe");
            var litNotaStatus = (Literal)objCurrentItem.FindControl("litNotaStatus");
            var btnDevolverNota = (LinkButton)objCurrentItem.FindControl("btnDevolverNota");
            if (String.IsNullOrEmpty(ultimoStatus))
            {
                btnDevolverNota.Visible = true;
                btnDevolverNota.Text = "Tente depois";
                btnDevolverNota.Enabled = false;
            }
            else
            {
                if (ultimoStatus.ToLower().IndexOf("autorizado") == -1)
                {
                    btnDevolverNota.Visible = false;
                }
            }

            if (!string.IsNullOrEmpty(nota.linkDanfe))
            {
                litNotaStatus.Text = "Autorizada";
                hplNotaDanfe.NavigateUrl = nota.linkDanfe;
                hplNotaDanfe.Target = "_blank";

                if (devolucao)
                {
                    btnDevolverNota.Visible = false;
                }

            }
            else
            {
                btnDevolverNota.Visible = false;
                //if (nota.ultimaChecagem < DateTime.Now.AddMinutes(-5))
                //{
                string linkNota = rnNotaFiscal.retornaDanfeNotaPorNumeroNota(nota.numeroNota.ToString(),
                    nota.idNotaFiscal);

                if (string.IsNullOrEmpty(nota.nfeKey) && string.IsNullOrEmpty(linkNota))
                {

                    var pedido = (from c in pedidoDc.tbPedidos where c.pedidoId == nota.idPedido select c).First();
                    linkNota = rnNotaFiscal.retornaDanfeNotaPorKey(pedido.nfeAccessKey, (nota.idCNPJ ?? 1));
                    if (!string.IsNullOrEmpty(linkNota))
                    {
                        var notaAlterar =
                            (from c in pedidoDc.tbNotaFiscals where c.idNotaFiscal == nota.idNotaFiscal select c)
                                .FirstOrDefault();
                        notaAlterar.nfeKey = pedido.nfeAccessKey;
                        pedidoDc.SubmitChanges();
                    }
                }
                if (!string.IsNullOrEmpty(linkNota))
                {
                    litNotaStatus.Text = "Autorizada";
                    hplNotaDanfe.NavigateUrl = linkNota;
                    hplNotaDanfe.Target = "_blank";
                }
                else
                {
                    litNotaStatus.Text = statusAtualInvoicy;
                    hplNotaDanfe.Visible = false;
                }
                //}
            }
        }
    }

    private void GerarEmailPrazosEntrega(tbPedido pedido)
    {
        try
        {
            int prazoMaximoPedidos = (pedido.prazoMaximoPedidos ?? 0);
            var data = new dbCommerceDataContext();
            var prazoProdutos = (from c in data.tbItensPedidos
                                 where c.pedidoId == pedido.pedidoId && (c.cancelado ?? false) == false
                                 select new
                                 {
                                     c.tbProduto.produtoId,
                                     c.tbProduto.produtoUrl,
                                     prazo = (c.tbProduto.tbProdutoFornecedor.fornecedorPrazoDeFabricacao ?? 1)
                                 }).OrderByDescending(x => x.prazo).FirstOrDefault();
            var categoria = (from c in data.tbJuncaoProdutoCategorias
                             where
                                 c.produtoId == prazoProdutos.produtoId && c.tbProdutoCategoria.categoriaPaiId == 0 &&
                                 c.tbProdutoCategoria.exibirSite == true
                             select c).FirstOrDefault();
            if (prazoProdutos != null && categoria != null && pedido.dataConfirmacaoPagamento != null)
            {
                string linkProduto = "";
                if (prazoMaximoPedidos == 0)
                {
                    prazoMaximoPedidos = prazoProdutos.prazo + 1;
                }

                int disponibilidadeMinima = prazoMaximoPedidos / 2;
                litEmailPrazoNumeroPedido.Text = rnFuncoes.retornaIdCliente(pedido.pedidoId).ToString();
                litEmailPrazoMinimo.Text = disponibilidadeMinima.ToString();
                litEmailPrazoMaximo.Text = prazoMaximoPedidos.ToString();
                litEmailPrazoCorreios.Text = pedido.prazoDeEntrega;
                litEmailPrazoCorreios2.Text = pedido.prazoDeEntrega;
                litEmailPrazoCep.Text = pedido.endCep;
                litEmailPrazoLinkProduto.Text = "http://www.graodegente.com.br/" + categoria.tbProdutoCategoria.categoriaUrl + "/" + prazoProdutos.produtoUrl;
                litEmailPrazoDataAprovacao.Text = pedido.dataConfirmacaoPagamento.Value.ToShortDateString();
                litEmailPrazoPostagem.Text = pedido.prazoMaximoPostagemAtualizado != null ? pedido.prazoMaximoPostagemAtualizado.Value.ToShortDateString() : pedido.prazoFinalPedido.Value.ToShortDateString();
                int prazoFinal = rnFuncoes.retornaPrazoDiasUteis(Convert.ToInt32(pedido.prazoDeEntrega), 0, (pedido.prazoMaximoPostagemAtualizado != null ? (DateTime)pedido.prazoMaximoPostagemAtualizado : (DateTime)pedido.prazoFinalPedido));
                litEmailPrazoRecebimento.Text = pedido.prazoMaximoPostagemAtualizado != null ? pedido.prazoMaximoPostagemAtualizado.Value.AddDays(prazoFinal).ToShortDateString() : pedido.prazoFinalPedido.Value.AddDays(prazoFinal).ToShortDateString();

            }
        }
        catch (Exception)
        {

        }
    }

    protected void btnReenviarCodigoRastreio_OnCommand(object sender, CommandEventArgs e)
    {
        int idPedidoPacote = Convert.ToInt32(e.CommandArgument);
        var data = new dbCommerceDataContext();
        var pacote = (from c in data.tbPedidoPacotes where c.idPedidoPacote == idPedidoPacote select c).FirstOrDefault();

        var prazoData = pacote.dataDeCarregamento != null ? pacote.dataDeCarregamento : pacote.prazoFinalEntrega;

        if (pacote.formaDeEnvio.ToLower() == "jadlog" | pacote.formaDeEnvio.ToLower() == "jadlogexpressa")
        {
            rnEmails.enviaCodigoDoTrakingJadlog(idPedidoPacote, pacote.rastreio);
        }
        if (pacote.formaDeEnvio.ToLower() == "tnt")
        {
            rnEmails.enviaCodigoDoTrakingTnt((int)pacote.idPedidoEnvio);
        }
        if (pacote.formaDeEnvio.ToLower() == "pac")
        {
            rnEmails.enviaCodigoDoTrakingCorreios("", pacote.idPedido.ToString(), "");
        }

        interacaoInclui("Código de rastreio enviado ao cliente", "False");
        AlertShow("Código de Rastreio enviado com sucesso");

    }

    protected void btnSalvarTaxaExtra_OnCommand(object sender, CommandEventArgs e)
    {
        try
        {
            int idPedidoPacote = Convert.ToInt32(e.CommandArgument.ToString().Split('#')[0]);
            int rowIndex = Convert.ToInt32(e.CommandArgument.ToString().Split('#')[1]);
            TextBox txtTaxaExtra = (TextBox)lstPacotes.Items[rowIndex].FindControl("txtTaxaExtra");



            using (var data = new dbCommerceDataContext())
            {
                var pedidoPacote = (from c in data.tbPedidoPacotes where c.idPedidoPacote == idPedidoPacote select c).FirstOrDefault();
                pedidoPacote.taxaExtra = Convert.ToDecimal(txtTaxaExtra.Text);
                data.SubmitChanges();
            }

            Response.Write("<script>alert('Taxa inserida com sucesso!')</script>");
        }
        catch (Exception ex)
        {
            Response.Write("<script>alert('Erro, verifique os dados e tente novamento!')</script>");
        }

    }

    protected void lstPacotes_OnItemDataBound(object sender, ListViewItemEventArgs e)
    {
        HiddenField hdfDespachado = (HiddenField)e.Item.FindControl("hdfDespachado");
        Button btnReenviarCodigoRastreio = (Button)e.Item.FindControl("btnReenviarCodigoRastreio");
        TextBox txtTaxaExtra = (TextBox)e.Item.FindControl("txtTaxaExtra");
        HtmlTableCell tdTaxaExtraCampos = (HtmlTableCell)e.Item.FindControl("tdTaxaExtraCampos");
        Button btnSalvarTaxaExtra = (Button)e.Item.FindControl("btnSalvarTaxaExtra");
        Button btnResetarPesagem = (Button)e.Item.FindControl("btnResetarPesagem");

        bool despachado = Convert.ToBoolean(hdfDespachado.Value);
        if (despachado)
        {
            btnReenviarCodigoRastreio.Visible = true;
        }
        txtTaxaExtra.Attributes.Add("onkeyPress", "return moeda(this,'.',',',event);");

        var usuarioLogadoId = Request.Cookies["usuarioLogadoId"];

        if (usuarioLogadoId != null)
        {
            btnResetarPesagem.Visible =
                rnUsuarios.usuarioPaginasPermitidaSeleciona_PorUsuarioIdPaginaPermitidaNome(
                    int.Parse(usuarioLogadoId.Value), "reiniciarpesagem").Tables[0].Rows.Count == 1;

            tdTaxaExtraCampos.Visible =
                rnUsuarios.usuarioPaginasPermitidaSeleciona_PorUsuarioIdPaginaPermitidaNome(
                    int.Parse(usuarioLogadoId.Value), "informarTaxaExtraEnvio").Tables[0].Rows.Count == 1;
        }

        if (e.Item.DataItemIndex > 0)
        {
            txtTaxaExtra.Visible = false;
            btnSalvarTaxaExtra.Visible = false;
        }
    }

    protected void btnGravarCartaCorrecao_OnClick(object sender, EventArgs e)
    {
        if (txtCartaCorrecao.Text.Length < 15)
        {
            string faltam = (15 - txtCartaCorrecao.Text.Length).ToString();
            AlertShow("O texto deve conter no mínimo 15 caracteres, faltam " + faltam);
            return;
        }
        else
        {
            string cartacorrecao = txtCartaCorrecao.Text.Replace(Environment.NewLine, " ");
            //var retorno = rnNotaFiscal.enviarCartaCorrecao(Convert.ToInt32(hdfNotaFiscal.Value), txtCartaCorrecao.Text);
            var retorno = rnNotaFiscal.enviarCartaCorrecao(Convert.ToInt32(hdfNotaFiscal.Value), cartacorrecao);
            AlertShow(retorno);
            popCartaCorrecao.ShowOnPageLoad = false;
        }

    }

    protected void btnCartaoCorrecao_OnCommand(object sender, CommandEventArgs e)
    {
        hdfNotaFiscal.Value = e.CommandArgument.ToString();
        popCartaCorrecao.ShowOnPageLoad = true;
    }

    protected void lstPagamentos_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        var dataItem = (tbPedidoPagamento)e.Item.DataItem;
        var data = new dbCommerceDataContext();
        var pagamento = (from c in data.tbPedidoPagamentos where c.idPedidoPagamento == dataItem.idPedidoPagamento select c).First();
        var litOperador = (Literal)e.Item.FindControl("litOperador");
        var litTipo = (Literal)e.Item.FindControl("litTipo");
        var litStatusPagamento = (Literal)e.Item.FindControl("litStatusPagamento");
        var trPagamento = (HtmlTableRow)e.Item.FindControl("trPagamento");
        litOperador.Text = pagamento.tbOperadorPagamento.nome;

        litTipo.Text = pagamento.tbCondicoesDePagamento.condicaoNome;

        var checaStatusGateway = (from c in data.tbPedidoPagamentoGateways where c.idPedidoPagamento == dataItem.idPedidoPagamento select c).FirstOrDefault();
        if (checaStatusGateway != null)
        {
            litStatusPagamento.Text = checaStatusGateway.responseMessage;

            if (!string.IsNullOrEmpty(checaStatusGateway.responseMessage))
            {
                if (checaStatusGateway.responseMessage != "CAPTURED")
                {
                    var gatewayQueNegou = (from c in data.tbPedidoPagamentoRiscos
                                           where c.idPedidoPagamento == dataItem.idPedidoPagamento
                                           select c).FirstOrDefault();
                    if (gatewayQueNegou == null)
                    {
                        litStatusPagamento.Text += ":<br>" + "MaxiPago";
                    }
                    else
                    {
                        List<string> statusCS = new List<string> { "RPM", "SUS", "CAN", "FRD", "RPA", "RPP" };
                        if (statusCS.Contains(gatewayQueNegou.statusGateway))
                        {
                            litStatusPagamento.Text += ":<br>" + "Negada-ClearSale";
                        }
                        else if (gatewayQueNegou.statusGateway == "NVO")
                        {
                            litStatusPagamento.Text += ":<br>" + "Em-Analise-ClearSale";
                        }
                    }

                }
            }
            else if (string.IsNullOrEmpty(checaStatusGateway.responseMessage))
            {
                var gatewayQueNegou = (from c in data.tbPedidoPagamentoRiscos
                                       where c.idPedidoPagamento == dataItem.idPedidoPagamento
                                       select c).FirstOrDefault();
                if (gatewayQueNegou == null)
                {
                    litStatusPagamento.Text += ":<br>" + "MaxiPago";
                }
                else
                {
                    List<string> statusCS = new List<string> { "RPM", "SUS", "CAN", "FRD", "RPA", "RPP" };
                    if (statusCS.Contains(gatewayQueNegou.statusGateway))
                    {
                        litStatusPagamento.Text += "Negada-ClearSale";
                    }
                    else if (gatewayQueNegou.statusGateway == "NVO")
                    {
                        litStatusPagamento.Text += ":<br>" + "Em-Analise-ClearSale";
                    }
                }
            }
        }

        var btnCancelarPagamento = (ASPxButton)e.Item.FindControl("btnCancelarPagamento");
        var btnNaoAutorizado = (ASPxButton)e.Item.FindControl("btnNaoAutorizado");
        var btnConfirmarPagamento = (ASPxButton)e.Item.FindControl("btnConfirmarPagamento");
        var btnReativarPagamento = (ASPxButton)e.Item.FindControl("btnReativarPagamento");
        var btnSegundaVia = (ASPxButton)e.Item.FindControl("btnSegundaVia");
        var btnDadosCartao = (ASPxButton)e.Item.FindControl("btnDadosCartao");
        var mAcoesPagamento = (ASPxMenu)e.Item.FindControl("mAcoesPagamento");
        mAcoesPagamento.ClientSideEvents.ItemClick = "function(s, e) { AcaoPagamento(e, '" + dataItem.idPedidoPagamento + "','" + dataItem.idOperadorPagamento + "', '" + ConfigurationManager.AppSettings["caminhoVirtualAdmin"] + "'); }";


        btnCancelarPagamento.ClientSideEvents.Click = "function(s, e) { CancelarPagamento('" + dataItem.idPedidoPagamento + "'); }";
        btnNaoAutorizado.ClientSideEvents.Click = "function(s, e) { popPagamentoNaoAutorizado('" + dataItem.idPedidoPagamento + "'); }";
        btnConfirmarPagamento.ClientSideEvents.Click = "function(s, e) { popConfirmarPagamentoShow('" + dataItem.idPedidoPagamento + "','" + dataItem.idOperadorPagamento + "'); }";
        //btnDadosCartao.ClientSideEvents.Click = "FP_openNewWindow('500', '230', false, false, false, false, false, false, 'popCartao', '" + ConfigurationManager.AppSettings["caminhoVirtualAdmin"] + "admin/popCartao.aspx?idPedidoPagamento=" + dataItem.idPedidoPagamento + "')";
        btnReativarPagamento.ClientSideEvents.Click = "function(s, e) { popReativarPagamentoShow('" + dataItem.idPedidoPagamento + "','" + dataItem.idOperadorPagamento + "'); }";
        btnDadosCartao.ClientSideEvents.Click = "function(s, e) { openNewWindow('500', '230', false, false, false, false, false, false, 'popCartao', '" + ConfigurationManager.AppSettings["caminhoVirtualAdmin"] + "admin/popCartao.aspx?idPedidoPagamento=" + dataItem.idPedidoPagamento + "'); }";

        HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
        usuarioLogadoId = Request.Cookies["usuarioLogadoId"];

        if (pagamento.pago)
        {
            trPagamento.BgColor = "#d2e9ff";
            //btnCancelarPagamento.Visible = true;
            //btnNaoAutorizado.Visible = false;
            //btnConfirmarPagamento.Visible = false;
            mAcoesPagamento.Items[0].Items[0].Visible = true;
            mAcoesPagamento.Items[0].Items[1].Visible = false;
            mAcoesPagamento.Items[0].Items[2].Visible = false;
            mAcoesPagamento.Items[0].Items[3].Visible = false;
            mAcoesPagamento.Items[0].Items[4].Visible = false;
            mAcoesPagamento.Items[0].Items[5].Visible = false;
            if (pagamento.tbCondicoesDePagamento.tipoDePagamentoId == 2)
            {
                //  btnDadosCartao.Visible = true;
                mAcoesPagamento.Items[0].Items[2].Visible = true;
            }
        }
        else if (pagamento.pagamentoNaoAutorizado)
        {
            trPagamento.BgColor = "#ffc6c7";
            //btnCancelarPagamento.Visible = true;
            //btnNaoAutorizado.Visible = false;
            //btnConfirmarPagamento.Visible = false;

            mAcoesPagamento.Items[0].Items[0].Visible = true;
            mAcoesPagamento.Items[0].Items[1].Visible = false;
            mAcoesPagamento.Items[0].Items[2].Visible = false;
            mAcoesPagamento.Items[0].Items[3].Visible = false;
            mAcoesPagamento.Items[0].Items[4].Visible = false;
            mAcoesPagamento.Items[0].Items[5].Visible = rnUsuarios.usuarioPaginasPermitidaSeleciona_PorUsuarioIdPaginaPermitidaNome(int.Parse(usuarioLogadoId.Value.ToString()), "reativarPagamento").Tables[0].Rows.Count == 1 ? true : false;

            if (pagamento.tbCondicoesDePagamento.tipoDePagamentoId == 2)
            {
                //  btnDadosCartao.Visible = true;
                mAcoesPagamento.Items[0].Items[2].Visible = true;
            }
        }
        else if (pagamento.cancelado)
        {
            trPagamento.BgColor = "#ffc6c7";
            //btnCancelarPagamento.Visible = false;
            //btnNaoAutorizado.Visible = false;
            //btnConfirmarPagamento.Visible = false;

            mAcoesPagamento.Items[0].Items[0].Visible = false;
            mAcoesPagamento.Items[0].Items[1].Visible = false;
            mAcoesPagamento.Items[0].Items[2].Visible = false;
            mAcoesPagamento.Items[0].Items[3].Visible = false;
            mAcoesPagamento.Items[0].Items[4].Visible = false;
            mAcoesPagamento.Items[0].Items[5].Visible = false;
            if (pagamento.tbCondicoesDePagamento.tipoDePagamentoId == 2)
            {
                //  btnDadosCartao.Visible = true;
                mAcoesPagamento.Items[0].Items[2].Visible = true;
            }
        }
        else
        {

            trPagamento.BgColor = "#ffffbb";
            if (pagamento.idOperadorPagamento == 1)
            {
                //  btnSegundaVia.Visible = true;
                //btnNaoAutorizado.Visible = false;
                string linkBoleto = "https://apiv2.graodegente.com.br/v1/pagamentos/" + rnFuncoes.retornaIdCliente(pagamento.pedidoId) + "/boleto/" + pagamento.numeroCobranca + "?autoImpressao=undefined";
                //string linkBoleto = "http://www.graodegente.com.br/boleto.aspx?pedido=" +
                //                    rnFuncoes.retornaIdCliente(pagamento.pedidoId) + "&cobranca=" +
                //                    pagamento.numeroCobranca;

                litTipo.Text = "<a href=\"" + linkBoleto + "\" target=\"_blank\">" + pagamento.tbCondicoesDePagamento.condicaoNome + "</a>";


                mAcoesPagamento.Items[0].Items[0].Visible = true;
                mAcoesPagamento.Items[0].Items[1].Visible = false;
                mAcoesPagamento.Items[0].Items[2].Visible = false;
                mAcoesPagamento.Items[0].Items[3].Visible = true;

                //incluído por Renato Silva a pedido do André
                #region Verifica se tem permissão para confirmar pagamento
                mAcoesPagamento.Items[0].Items[4].Visible = rnUsuarios.usuarioPaginasPermitidaSeleciona_PorUsuarioIdPaginaPermitidaNome(int.Parse(usuarioLogadoId.Value.ToString()), "confirmarpagamento").Tables[0].Rows.Count == 1 ? true : false;
                mAcoesPagamento.Items[0].Items[5].Visible = false;
                #endregion
            }
            if (pagamento.tbCondicoesDePagamento.tipoDePagamentoId == 2)
            {
                //btnDadosCartao.Visible = true;
                mAcoesPagamento.Items[0].Items[2].Visible = true;
            }

            if ((pagamento.gateway ?? false) == true && pagamento.tbCondicoesDePagamento.tipoDePagamentoId == 2)
            {
                mAcoesPagamento.Items[0].Items[0].Visible = true;
                //mAcoesPagamento.Items[0].Items[1].Visible = false;
                mAcoesPagamento.Items[0].Items[1].Visible = true;
                //mAcoesPagamento.Items[0].Items[2].Visible = false;
                mAcoesPagamento.Items[0].Items[2].Visible = true;
                mAcoesPagamento.Items[0].Items[3].Visible = false;
                mAcoesPagamento.Items[0].Items[4].Visible = false;
                mAcoesPagamento.Items[0].Items[5].Visible = false;
            }

            //incluído por Renato Silva a pedido do André
            #region Verifica se tem permissão para confirmar pagamento
            mAcoesPagamento.Items[0].Items[4].Visible = rnUsuarios.usuarioPaginasPermitidaSeleciona_PorUsuarioIdPaginaPermitidaNome(int.Parse(usuarioLogadoId.Value.ToString()), "confirmarpagamento").Tables[0].Rows.Count == 1 ? true : false;
            #endregion
        }

    }

    protected void btnCancelarPagamento_OnClick(object sender, EventArgs e)
    {
        int idPedidoPagamento = 0;
        int.TryParse(hdfCancelarPagamento.Value, out idPedidoPagamento);

        tbPedidoPagamento pagamento;
        using (var data = new dbCommerceDataContext())
        {
            pagamento = (from c in data.tbPedidoPagamentos where c.idPedidoPagamento == idPedidoPagamento select c).FirstOrDefault();
        }

        if (pagamento != null)
        {

            using (var data = new dbCommerceDataContext())
            {
                pagamento = (from c in data.tbPedidoPagamentos where c.idPedidoPagamento == idPedidoPagamento select c).First();
                pagamento.cancelado = true;
                pagamento.pago = false;
                pagamento.observacao = txtMotivoCancelarPagamento.Text;
                data.SubmitChanges();
            }

            rnPedidos.AtualizaDescontoPedido(pagamento.pedidoId);

            int cobrancas;
            using (var data = new dbCommerceDataContext())
            {
                cobrancas = (from c in data.tbPedidoPagamentos where c.cancelado == false && c.pedidoId == pagamento.pedidoId select new { c.idPedidoPagamento }).Count();
            }

            var pedido = new tbPedido();

            if (cobrancas == 1)
            {
                using (var data = new dbCommerceDataContext())
                {
                    var cobrancaAtual = (from c in data.tbPedidoPagamentos
                                         where c.cancelado == false && c.pedidoId == pagamento.pedidoId
                                         select c).First();
                    pedido = (from c in data.tbPedidos where c.pedidoId == pagamento.pedidoId select c).First();
                    pedido.condDePagamentoId = cobrancaAtual.condicaoDePagamentoId;
                    pedido.tipoDePagamentoId = cobrancaAtual.tbCondicoesDePagamento.tipoDePagamentoId;
                    pedido.numeroDeParcelas = cobrancaAtual.parcelas;
                    data.SubmitChanges();
                }
            }

            string interacao = "<b>Pagamento Cancelado</b><br>";
            interacao += "Motivo: " + txtMotivoCancelarPagamento.Text + "<br>";
            interacao += "Vencimento: " + pagamento.dataVencimento.ToShortDateString() + "<br>";
            interacao += "Valor: " + pagamento.valor.ToString("C") + "<br>";

            rnInteracoes.interacaoInclui(pagamento.pedidoId, interacao, rnUsuarios.retornaNomeUsuarioLogado(), "False");
            popCancelarPagamento.ShowOnPageLoad = false;
            if (pedido.statusDoPedido != 5) rnPagamento.verificaPagamentoCompleto(pagamento.pedidoId);
            fillPedido();
            dtlInteracao.DataBind();
            AlertShow("Pagamento Cancelado com Sucesso");
        }
        else
        {
            AlertShow("Falha ao cancelar pagamento");
        }
    }

    protected void btnPagamentoNaoAutorizado_OnClick(object sender, EventArgs e)
    {
        int idPedidoPagamento = 0;
        int.TryParse(hdfPagamentoNaoAutorizado.Value, out idPedidoPagamento);
        var data = new dbCommerceDataContext();
        var pagamento = (from c in data.tbPedidoPagamentos where c.idPedidoPagamento == idPedidoPagamento select c).FirstOrDefault();
        if (pagamento != null)
        {
            var pedido = (from c in data.tbPedidos where c.pedidoId == pagamento.pedidoId select c).First();
            pedido.statusDoPedido = 7;
            string interacao = "<b>Pagamento Não Autorizado</b><br>";
            interacao += "Motivo: " + txtMotivoPagamentoNaoAutorizado.Text + "<br>";
            interacao += "Valor: " + pagamento.valor.ToString("C");
            pagamento.pagamentoNaoAutorizado = true;
            pagamento.observacao = txtMotivoPagamentoNaoAutorizado.Text;
            data.SubmitChanges();
            rnInteracoes.interacaoInclui(pagamento.pedidoId, interacao, rnUsuarios.retornaNomeUsuarioLogado(), "False");
            AlertShow("Pagamento Não Autorizado com Sucesso");
            popPagamentoNaoAutorizado.ShowOnPageLoad = false;
            rnEmails.enviaEmailPagamentoRecusado(pagamento.pedidoId);
            fillPedido();
            dtlInteracao.DataBind();
        }
        else
        {
            AlertShow("Falha ao cancelar pagamento");
        }
    }

    protected void btnSegundaVia_Command(object sender, CommandEventArgs e)
    {
        var data = new dbCommerceDataContext();
        var idPedidoPagamento = Convert.ToInt32(e.CommandArgument);
        var pagamento = (from c in data.tbPedidoPagamentos where c.idPedidoPagamento == idPedidoPagamento select c).First();
        string interacao = "<b>Segunda via de Boleto Enviada</b><br>";
        if (pagamento.dataVencimento < DateTime.Now)
        {
            interacao += "<b>Vencimento original:</b> " + pagamento.dataVencimento.ToShortDateString() + "<br>";
            var dataVencimento = DateTime.Now.AddDays(double.Parse(ConfigurationManager.AppSettings["PrazoDoVencimento"].ToString()));
            pagamento.dataVencimento = dataVencimento;
            data.SubmitChanges();
            interacao += "<b>Novo vencimento:</b> " + pagamento.dataVencimento.ToShortDateString();
        }

        var queueSegundaVia = new tbQueue()
        {
            agendamento = DateTime.Now,
            andamento = false,
            concluido = false,
            idRelacionado = pagamento.pedidoId,
            mensagem = "",
            tipoQueue = 42
        };
        data.tbQueues.InsertOnSubmit(queueSegundaVia);
        data.SubmitChanges();

        rnInteracoes.interacaoInclui(pagamento.pedidoId, interacao, rnUsuarios.retornaNomeUsuarioLogado(), "False");
        AlertShow("Segunda via de boleto enviada por e-mail com sucesso.");
        fillPedido();


        /* string linkDoBoleto = ConfigurationManager.AppSettings["caminhoVirtual"] + "boleto.aspx?cobranca=" + pagamento.numeroCobranca + "&pedido=" + rnFuncoes.retornaIdCliente(pagamento.pedidoId);

         TextBox txtNomeDoCliente = (TextBox)dtvPedido.FindControl("txtNomeDoCliente");
         TextBox txtEmail = (TextBox)dtvPedido.FindControl("txtEmail");

         if (!rnEmails.enviaSegundaViaDoBoleto(txtNomeDoCliente.Text, pagamento.pedidoId.ToString(), linkDoBoleto, txtEmail.Text))
         {
             rnInteracoes.interacaoInclui(pagamento.pedidoId, interacao, rnUsuarios.retornaNomeUsuarioLogado(), "False");
             AlertShow("Segunda via de boleto enviada por e-mail com sucesso.");
             fillPedido();
         }
         else
         {
             AlertShow("Segunda via de boleto não foi enviada.");
         }*/
    }

    protected void btnConfirmarPagamento_OnClick(object sender, EventArgs e)
    {
        var idPedidoPagamento = Convert.ToInt32(hdfConfirmarPagamento.Value);
        var idOperadorPagamento = Convert.ToInt32(ddlOperadorPagamentoConfirmacao.SelectedValue);
        var confirmacaopagamento = rnPagamento.confirmarPagamento(idPedidoPagamento, idOperadorPagamento, txtNumeroCobrancaConfirmacao.Text, txtObservacaoConfirmacaoPagamento.Text, false);

        if (confirmacaopagamento == 99)
        {
            AlertShow("Pagamento parcialmente confirmado. Aguardando restante do pagamento.");
        }
        if (confirmacaopagamento == 0)
        {
            AlertShow("Pagamento confirmado.");
        }
        if (confirmacaopagamento == 1)
        {
            AlertShow("Pagamento confirmado / Separação de Estoque.");
        }
        fillPedido();
        popConfirmarPagamento.ShowOnPageLoad = false;

    }

    protected void btnReativarPagamento_OnClick(object sender, EventArgs e)
    {

        var idPedidoPagamento = Convert.ToInt32(hdfReativarPagamento.Value);
        bool ok = rnPagamento.reativarPagamentoNaoAutorizado(idPedidoPagamento, txtObservacaoReativarPagamento.Text);

        if (ok)
        {
            AlertShow("O pagamento foi reativado");
        }
        else
        {
            AlertShow("Ocorreu um erro inesperado");
        }

        fillPedido();
        popReativarPagamento.ShowOnPageLoad = false;
    }

    protected void btnGerarNovoPagamento_OnClick(object sender, EventArgs e)
    {
        try
        {
            var data = new dbCommerceDataContext();
            HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
            usuarioLogadoId = Request.Cookies["usuarioLogadoId"];
            int idUsuario = int.Parse(usuarioLogadoId.Value.ToString());
            var paginas = (from c in data.tbUsuarioPaginasPermitidas where c.usuarioId == idUsuario select c).ToList();


            int pedidoId = Convert.ToInt32(Request.QueryString["pedidoId"]);
            int condicaoId = Convert.ToInt32(ddlCondicaoPagamentoNovaCobranca.SelectedValue);
            bool boletoSemFrete = false;

            if (!(from c in paginas where c.paginaPermitidaNome.ToLower().Contains("boletosemfrete") select c).Any() && condicaoId == 41)
            {
                AlertShow("Você não tem acesso a este item");
                return;
            }

            if (condicaoId == 41)
            {
                condicaoId = 4;
                boletoSemFrete = true;
            }

            var condicao = (from c in data.tbCondicoesDePagamentos where c.condicaoId == condicaoId select c).FirstOrDefault();

            decimal valorCobranca = 0;
            decimal.TryParse(txtValorNovaCobranca.Text, out valorCobranca);

            if (condicaoId == 31)
            {
                var credito = new tbPedidoPagamento
                {
                    pedidoId = pedidoId,
                    pagamentoPrincipal = true,
                    dataVencimento = DateTime.Now,
                    valor = valorCobranca,
                    pago = true,
                    numeroCobranca = "",
                    recebimentoParcelado = false,
                    observacao = txtObservacaoNovaCobranca.Text,
                    idOperadorPagamento = 10,
                    consolidado = false,
                    parcelas = 1,
                    cancelado = false,
                    condicaoDePagamentoId = condicaoId,
                    valorParcela = valorCobranca,
                    pagamentoNaoAutorizado = false,
                    valorTotal = valorCobranca,
                    valorDesconto = 0,
                    nomeCartao = "",
                    numeroCartao = "",
                    validadeCartao = "",
                    codSegurancaCartao = "",
                    idContaPagamento = rnPagamento.defineOperadorPagamento(condicaoId)
                };
                data.tbPedidoPagamentos.InsertOnSubmit(credito);
                data.SubmitChanges();

                string interacao = "Gerado crédito no valor de: " + valorCobranca.ToString("C") + "<br>";

                rnInteracoes.interacaoInclui(pedidoId, interacao, rnUsuarios.retornaNomeUsuarioLogado(), "False");

                AlertShow("Crédito inserido com sucesso");
                popGerarPagamento.ShowOnPageLoad = false;
                fillPedido();
            }
            else
            {


                //if (condicao.porcentagemDeDesconto == 20) condicao.porcentagemDeDesconto = 15;
                var pedido = (from c in data.tbPedidos where c.pedidoId == pedidoId select c).First();

                var vencimento = DateTime.Now.AddDays(4);
                DateTime.TryParse(txtVencimentoNovaCobranca.Text, out vencimento);
                if (vencimento < DateTime.Now) vencimento = DateTime.Now.AddDays(4);

                decimal valorDesconto = 0;
                decimal valorGeral = valorCobranca;

                string interacao = "<b>Pagamento Adicionado</b><br>";
                interacao += "Valor: " + valorGeral.ToString("C") + "<br>";
                int porcentagemDeDesconto = 0;
                decimal valorFrete = 0;
                if (condicao.porcentagemDeDesconto > 0)
                {

                    if (pedido.valorDoFrete > 0 && pedido.valorDoFrete != null)
                    {
                        valorFrete = (decimal)pedido.valorDoFrete;
                    }

                    if (pedido.tipoDePagamentoId == 11)
                    {
                        porcentagemDeDesconto = Convert.ToInt32((from c in data.tbCondicoesDePagamentos where c.tipoDePagamentoId == 11 select c).First()
                                .porcentagemDeDesconto);
                    }
                    else
                    {
                        porcentagemDeDesconto = Convert.ToInt32(condicao.porcentagemDeDesconto);

                    }
                    if (porcentagemDeDesconto > 0)
                    {
                        //var diaMudancaDescontoBoleto = Convert.ToDateTime("20/01/2017");
                        //if (pedido.tipoDePagamentoId != 11)
                        //{
                        //    if ((pedido.dataHoraDoPedido.Value.Date < diaMudancaDescontoBoleto.Date) &&
                        //        (porcentagemDeDesconto == 10 | porcentagemDeDesconto == 15))
                        //    {
                        //        porcentagemDeDesconto = 15;
                        //    }
                        //}

                        decimal maximoDesconto = (Convert.ToDecimal(pedido.valorDosItens) / 100) * porcentagemDeDesconto;
                        var cobrancaDescontos =
                            (from c in data.tbPedidoPagamentos
                             where c.pedidoId == pedidoId && c.cancelado == false && c.valorDesconto > 0
                             select c);
                        decimal totalDescontos = 0;
                        if (cobrancaDescontos.Any())
                        {
                            totalDescontos = cobrancaDescontos.Sum(x => x.valorDesconto == null ? 0 : (decimal)x.valorDesconto);
                        }

                        decimal descontoRestante = maximoDesconto - totalDescontos;
                        if (descontoRestante < 0) descontoRestante = 0;

                        if (valorDesconto > descontoRestante) valorDesconto = descontoRestante;
                        valorCobranca = valorGeral - valorDesconto;
                        interacao += "Valor do Desconto: " + valorDesconto.ToString("C") + "<br>";
                        interacao += "Valor Cobrado: " + valorCobranca.ToString("C") + "<br>";
                    }
                }

                string numeroCobranca = (String.Format("{0:00000000}", pedidoId));
                int inicioNumeroCobranca = 0;
                bool cobrancaUtilizada = true;

                #region Boleto
                if (condicaoId == 4 && !boletoSemFrete)
                {
                    while (cobrancaUtilizada)
                    {
                        string numeroCobrancaCheck = "";
                        if (inicioNumeroCobranca < 10)
                        {
                            numeroCobrancaCheck = inicioNumeroCobranca + (String.Format("{0:0000000}", pedidoId));
                        }
                        else
                        {
                            numeroCobrancaCheck = inicioNumeroCobranca + (String.Format("{0:000000}", pedidoId));
                        }

                        var cobrancaCheck = (from c in data.tbPedidoPagamentos where c.numeroCobranca == numeroCobrancaCheck select c).Any();
                        if (cobrancaCheck)
                        {
                            inicioNumeroCobranca++;
                        }
                        else
                        {
                            cobrancaUtilizada = false;
                            numeroCobranca = numeroCobrancaCheck;
                        }

                    }
                    valorDesconto = Convert.ToDecimal((((valorCobranca - (valorCobranca < valorFrete ? 0 : valorFrete)) / 100) * porcentagemDeDesconto).ToString("0.00"));

                    if (condicao.condicaoId == 4 && !boletoSemFrete)
                    {
                        using (var db = new dbCommerceDataContext())
                        {
                            decimal valorDescontoPossivel = 0;

                            var totalDescontoPedido =
                                (from c in db.tbPedidoPagamentos
                                 where c.pedidoId == pedidoId && !c.cancelado && !c.pagamentoNaoAutorizado
                                 select new { c.valorDesconto }).Sum(x => x.valorDesconto) ?? 0;

                            var pagamentoCartao = (from c in data.tbPedidoPagamentos
                                                   where c.pedidoId == pedidoId && !c.cancelado && c.pago
                                                   && !c.pagamentoNaoAutorizado && c.tbCondicoesDePagamento.tbTipoDePagamento.tipoDePagamentoId == 2
                                                   select new { c.valor }).Any();

                            if (pagamentoCartao)
                            {

                                var totalPagamentoBoleto = (from c in data.tbPedidoPagamentos
                                                            where c.pedidoId == pedidoId && !c.cancelado && c.pago
                                                            && !c.pagamentoNaoAutorizado && c.tbCondicoesDePagamento.tbTipoDePagamento.tipoDePagamentoId == 1
                                                            select new { c.valorTotal }).Sum(x => x.valorTotal) ?? 0;

                                var valorPagoEmBoleto = (valorCobranca + totalPagamentoBoleto);

                                valorDescontoPossivel = ((valorPagoEmBoleto / 100) * porcentagemDeDesconto) - totalDescontoPedido;

                                if (valorDescontoPossivel < valorCobranca)
                                    valorDesconto = valorDescontoPossivel;
                            }
                            else
                            {

                                var valorTotalItensPedido =
                                        (from c in data.tbPedidos where c.pedidoId == pedidoId select new { c.valorDosItens }).Sum(x => x.valorDosItens ?? 0);

                                if (totalDescontoPedido > 0)
                                    valorDescontoPossivel = ((valorTotalItensPedido / 100) * porcentagemDeDesconto) - totalDescontoPedido;

                                if (valorDescontoPossivel < valorCobranca && (valorDescontoPossivel >= 0 && totalDescontoPedido > 0))
                                    valorDesconto = valorDescontoPossivel;

                                if (valorDescontoPossivel < 0)
                                    valorDesconto = 0;
                            }
                        }
                    }

                    valorCobranca = valorCobranca - valorDesconto;
                    interacao += "Cobrança: " + numeroCobranca + "<br>";
                }
                #endregion

                #region Boleto sem incidencia de frete no cálculo do desconto
                if (condicaoId == 4 && boletoSemFrete)
                {

                    while (cobrancaUtilizada)
                    {
                        string numeroCobrancaCheck = "";
                        if (inicioNumeroCobranca < 10)
                        {
                            numeroCobrancaCheck = inicioNumeroCobranca + (String.Format("{0:0000000}", pedidoId));
                        }
                        else
                        {
                            numeroCobrancaCheck = inicioNumeroCobranca + (String.Format("{0:000000}", pedidoId));
                        }

                        var cobrancaCheck = (from c in data.tbPedidoPagamentos where c.numeroCobranca == numeroCobrancaCheck select c).Any();
                        if (cobrancaCheck)
                        {
                            inicioNumeroCobranca++;
                        }
                        else
                        {
                            cobrancaUtilizada = false;
                            numeroCobranca = numeroCobrancaCheck;
                        }

                    }
                    valorDesconto = Convert.ToDecimal((((valorCobranca) / 100) * porcentagemDeDesconto).ToString("0.00"));

                    if (condicao.condicaoId == 4 && boletoSemFrete)
                    {
                        using (var db = new dbCommerceDataContext())
                        {
                            decimal valorDescontoPossivel = 0;

                            var totalDescontoPedido =
                                (from c in db.tbPedidoPagamentos
                                 where c.pedidoId == pedidoId && !c.cancelado && !c.pagamentoNaoAutorizado
                                 select new { c.valorDesconto }).Sum(x => x.valorDesconto) ?? 0;

                            var pagamentoCartao = (from c in data.tbPedidoPagamentos
                                                   where c.pedidoId == pedidoId && !c.cancelado && c.pago
                                                   && !c.pagamentoNaoAutorizado && c.tbCondicoesDePagamento.tbTipoDePagamento.tipoDePagamentoId == 2
                                                   select new { c.valor }).Any();

                            if (pagamentoCartao)
                            {

                                var totalPagamentoBoleto = (from c in data.tbPedidoPagamentos
                                                            where c.pedidoId == pedidoId && !c.cancelado && c.pago
                                                            && !c.pagamentoNaoAutorizado && c.tbCondicoesDePagamento.tbTipoDePagamento.tipoDePagamentoId == 1
                                                            select new { c.valorTotal }).Sum(x => x.valorTotal) ?? 0;

                                var valorPagoEmBoleto = (valorCobranca + totalPagamentoBoleto);

                                valorDescontoPossivel = ((valorPagoEmBoleto / 100) * porcentagemDeDesconto) - totalDescontoPedido;

                                //if (valorDescontoPossivel < valorCobranca)
                                //    valorDesconto = valorDescontoPossivel;
                            }
                            else
                            {

                                var valorTotalItensPedido =
                                        (from c in data.tbPedidos where c.pedidoId == pedidoId select new { c.valorDosItens }).Sum(x => x.valorDosItens ?? 0);

                                if (totalDescontoPedido > 0)
                                    valorDescontoPossivel = ((valorTotalItensPedido / 100) * porcentagemDeDesconto) - totalDescontoPedido;

                                //if (valorDescontoPossivel < valorCobranca && (valorDescontoPossivel >= 0 && totalDescontoPedido > 0))
                                //    valorDesconto = valorDescontoPossivel;

                                //if (valorDescontoPossivel < 0)
                                //    valorDesconto = 0;
                            }
                        }
                    }

                    valorCobranca = valorCobranca - valorDesconto;
                    interacao += "Cobrança: " + numeroCobranca + "<br>";
                }
                #endregion

                interacao += "Condição: " + condicao.condicaoNome + "<br>";

                var cobranca = new tbPedidoPagamento();
                cobranca.pedidoId = pedidoId;
                cobranca.pagamentoPrincipal = true;
                cobranca.dataVencimento = vencimento;
                cobranca.valor = valorCobranca;
                cobranca.pago = false;
                cobranca.numeroCobranca = numeroCobranca;
                cobranca.recebimentoParcelado = condicaoId != 4;
                cobranca.observacao = txtObservacaoNovaCobranca.Text;
                cobranca.idOperadorPagamento = rnPagamento.defineOperadorPagamento(condicaoId);
                cobranca.consolidado = false;
                cobranca.parcelas = Convert.ToInt32(ddlParcelasNovaCobranca.SelectedValue);
                cobranca.cancelado = false;
                cobranca.condicaoDePagamentoId = condicaoId;
                cobranca.valorParcela = Convert.ToDecimal((Convert.ToDecimal(valorCobranca) / Convert.ToInt32(ddlParcelasNovaCobranca.SelectedValue)).ToString("0.00"));
                cobranca.pagamentoNaoAutorizado = false;
                cobranca.valorTotal = valorGeral;
                cobranca.valorDesconto = valorDesconto;
                cobranca.nomeCartao = txtNomeCartaoNovaCobranca.Text;
                cobranca.numeroCartao = txtNumeroCartaoNovaCobranca.Text;
                cobranca.validadeCartao = ddlValidadeMesNovaCobranca.SelectedValue + "/" + ddlValidadeAnoNovaCobranca.SelectedValue;
                cobranca.codSegurancaCartao = txtCodSegurancaNovaCobranca.Text;
                cobranca.idContaPagamento = 12;
                data.tbPedidoPagamentos.InsertOnSubmit(cobranca);
                data.SubmitChanges();
                rnPedidos.AtualizaDescontoPedido(pedidoId);

                var cobrancas = (from c in data.tbPedidoPagamentos where c.cancelado == false && c.pedidoId == pedidoId select c).Count();
                if (cobrancas == 1 && pedido.tipoDePagamentoId != 11)
                {
                    var cobrancaAtual = (from c in data.tbPedidoPagamentos where c.cancelado == false && c.pedidoId == pedidoId select c).First();
                    pedido.condDePagamentoId = cobrancaAtual.condicaoDePagamentoId;
                    pedido.tipoDePagamentoId = cobrancaAtual.tbCondicoesDePagamento.tipoDePagamentoId;
                    pedido.numeroDeParcelas = cobrancaAtual.parcelas;

                    try
                    {
                        data.SubmitChanges();
                    }
                    catch (System.Data.Linq.ChangeConflictException ex)
                    {

                        foreach (System.Data.Linq.ObjectChangeConflict changeConflict in data.ChangeConflicts)
                            changeConflict.Resolve(System.Data.Linq.RefreshMode.KeepChanges, true);

                        data.SubmitChanges();

                    }
                }
                AlertShow("Cobrança adicionada com sucesso");
                ddlParcelasNovaCobranca.SelectedValue = "01";
                rnInteracoes.interacaoInclui(pedidoId, interacao, rnUsuarios.retornaNomeUsuarioLogado(), "False");
                rnPagamento.verificaPagamentoCompleto(pedidoId);
                popGerarPagamento.ShowOnPageLoad = false;
                var queue = new tbQueue();
                queue.agendamento = DateTime.Now;
                queue.andamento = false;
                queue.concluido = false;
                queue.idRelacionado = cobranca.idPedidoPagamento;
                queue.tipoQueue = 11;
                queue.mensagem = "";
                data.tbQueues.InsertOnSubmit(queue);
                data.SubmitChanges();
                fillPedido();
            }
        }
        catch (Exception ex)
        {
            AlertShow(ex.Message);
        }

    }

    protected void btnCancelarPedido_OnClick(object sender, EventArgs e)
    {
        int pedidoId = Convert.ToInt32(Request.QueryString["pedidoId"]);
        rnPedidos.retiraVinculoPedidoFornecedor(pedidoId);
        var data = new dbCommerceDataContext();
        var pedido = (from c in data.tbPedidos where c.pedidoId == pedidoId select c).First();
        List<int> pagos = new List<int> { 3, 4, 5, 11 };

        // Se o pedido estiver pago gera chamado para Ariane
        if (pagos.Contains(pedido.statusDoPedido))
        {
            gravarChamado("Pedido Cancelado - Estorno", null, pedidoId, 11);
        }

        pedido.statusDoPedido = 6;
        pedido.motivoCancelamento = ddlMotivosCancelamentoPedido.SelectedValue;
        data.SubmitChanges();
        var itens = (from c in data.tbItensPedidos where c.pedidoId == pedidoId select c).ToList();
        /*foreach (var item in itens)
        {
            var combos = (from c in data.tbItensPedidoCombos where c.idItemPedido == item.itemPedidoId select c).ToList();
            if (combos.Count > 0)
            {
                foreach (var combo in combos)
                {
                    var produto = (from c in data.tbProdutos where c.produtoId == combo.produtoId select c).First();
                    produto.produtoEstoqueAtual += Convert.ToInt32(item.itemQuantidade);
                }
            }
            var produtoP = (from c in data.tbProdutos where c.produtoId == item.produtoId select c).First();
            produtoP.produtoEstoqueAtual += Convert.ToInt32(item.itemQuantidade);
            data.SubmitChanges();

        }*/

        #region Manda para tabela de Queue para poder verificar se coloca na fila de completos
        var queue = new tbQueue();
        queue.agendamento = DateTime.Now;
        queue.andamento = false;
        queue.concluido = false;
        queue.idRelacionado = pedidoId;
        queue.tipoQueue = 20; // Cancela reserva de itens de pedido
        queue.mensagem = "";
        data.tbQueues.InsertOnSubmit(queue);
        data.SubmitChanges();
        #endregion



        interacaoInclui("Pedido Cancelado", "True");
        rnEmails.enviaEmailPedidoCancelado(pedidoId);
        fillPedido();


    }

    protected void mAcoesPagamento_OnItemClick(object source, MenuItemEventArgs e)
    {
        var data = new dbCommerceDataContext();
        var idPedidoPagamento = Convert.ToInt32(hdfPagamentoSegundaVia.Value);
        var pagamento = (from c in data.tbPedidoPagamentos where c.idPedidoPagamento == idPedidoPagamento select c).First();
        string interacao = "<b>Segunda via de Boleto Enviada</b><br>";
        if (pagamento.dataVencimento < DateTime.Now)
        {
            interacao += "<b>Vencimento original:</b> " + pagamento.dataVencimento.ToShortDateString() + "<br>";
            var dataVencimento = DateTime.Now.AddDays(double.Parse(ConfigurationManager.AppSettings["PrazoDoVencimento"].ToString()));
            pagamento.dataVencimento = dataVencimento;
            data.SubmitChanges();
            interacao += "<b>Novo vencimento:</b> " + pagamento.dataVencimento.ToShortDateString();

            //verifica se o pedido está como cancelado, pois neste caso será reativado e colocado como Aguardando Confirmação de Pagamento
            var verificaStatusPedido = (from p in data.tbPedidos where p.pedidoId == pagamento.pedidoId select p).FirstOrDefault();

            if (verificaStatusPedido.statusDoPedido == 6)
            {
                verificaStatusPedido.statusDoPedido = 2;
            }
            data.SubmitChanges();

        }

        var queueSegundaVia = new tbQueue()
        {
            agendamento = DateTime.Now,
            andamento = false,
            concluido = false,
            idRelacionado = pagamento.pedidoId,
            mensagem = "",
            tipoQueue = 42
        };
        data.tbQueues.InsertOnSubmit(queueSegundaVia);
        data.SubmitChanges();

        rnInteracoes.interacaoInclui(pagamento.pedidoId, interacao, rnUsuarios.retornaNomeUsuarioLogado(), "False");
        AlertShow("Segunda via de boleto enviada por e-mail com sucesso.");
        fillPedido();


        /*string linkDoBoleto = ConfigurationManager.AppSettings["caminhoVirtual"] + "boleto.aspx?cobranca=" + pagamento.numeroCobranca + "&pedido=" + rnFuncoes.retornaIdCliente(pagamento.pedidoId);

        TextBox txtNomeDoCliente = (TextBox)dtvPedido.FindControl("txtNomeDoCliente");
        TextBox txtEmail = (TextBox)dtvPedido.FindControl("txtEmail");


        if (!rnEmails.enviaSegundaViaDoBoleto(txtNomeDoCliente.Text, pagamento.pedidoId.ToString(), linkDoBoleto, txtEmail.Text))
        {
            rnInteracoes.interacaoInclui(pagamento.pedidoId, interacao, rnUsuarios.retornaNomeUsuarioLogado(), "False");
            AlertShow("Segunda via de boleto enviada por e-mail com sucesso.");
            fillPedido();
            dtlInteracao.DataBind();
        }
        else
        {
            AlertShow("Segunda via de boleto não foi enviada.");
        }*/
    }

    protected void btnReservarItemEstoque_OnCommand(object sender, CommandEventArgs e)
    {
        return;
        var data = new dbCommerceDataContext();
        string argumentos = e.CommandArgument.ToString();
        int pedidoId = Convert.ToInt32(argumentos.Split(',')[0]);
        int itemPedidoId = Convert.ToInt32(argumentos.Split(',')[1]);
        int produtoId = Convert.ToInt32(argumentos.Split(',')[2]);

        var estoqueDoItem = data.admin_produtoEmEstoque(produtoId).FirstOrDefault();
        int estoqueTotalDoItem = estoqueDoItem == null ? 0 : Convert.ToInt32(estoqueDoItem.estoqueLivre);
        if (estoqueTotalDoItem > 0)
        {
            var reserva = new tbProdutoReservaEstoque();
            reserva.idPedido = pedidoId;
            reserva.idProduto = produtoId;
            reserva.dataHora = DateTime.Now;
            reserva.quantidade = 1;
            reserva.idItemPedido = itemPedidoId;
            data.tbProdutoReservaEstoques.InsertOnSubmit(reserva);
            data.SubmitChanges();
            interacaoInclui("Produto reservado do estoque<br>" + "<b>" + estoqueDoItem.produtoNome + "</b>", "True");

            var pedido = (from c in data.tbPedidos where c.pedidoId == pedidoId select c).FirstOrDefault();
            if (pedido.statusDoPedido == 3)
            {
                var completo = rnPedidos.verificaReservasPedidoFornecedorCompleto(pedidoId);
                if (completo)
                {
                    pedido.statusDoPedido = 11;
                    data.SubmitChanges();
                    interacaoInclui("Separação de Estoque", "True");
                    rnEmails.enviaEmailPedidoProcessado(pedidoId);

                    #region Manda para tabela de Queue para poder verificar se coloca na fila de completos
                    var queue = new tbQueue();
                    queue.agendamento = DateTime.Now;
                    queue.andamento = false;
                    queue.concluido = false;
                    queue.idRelacionado = pedidoId;
                    queue.tipoQueue = 8;
                    queue.mensagem = "";
                    data.tbQueues.InsertOnSubmit(queue);
                    data.SubmitChanges();
                    #endregion

                }
            }

            #region Manda para tabela de Queue para poder verificar se coloca na fila de completos
            var queuef = new tbQueue();
            queuef.agendamento = DateTime.Now;
            queuef.andamento = false;
            queuef.concluido = false;
            queuef.idRelacionado = pedidoId;
            queuef.tipoQueue = 8;
            queuef.mensagem = "";
            data.tbQueues.InsertOnSubmit(queuef);
            data.SubmitChanges();
            #endregion

            AlertShow("Produto Reservado com sucesso");
            sqlItensPedido.DataBind();
            dtlItensPedido.DataBind();
        }
        else
        {
            AlertShow("O produto não está mais disponível em estoque");
            sqlItensPedido.DataBind();
            dtlItensPedido.DataBind();
        }
    }

    private void atualizarCombo(int itemPedidoId)
    {
        var data = new dbCommerceDataContext();
        var itemPedido = (from c in data.tbItensPedidos where c.itemPedidoId == itemPedidoId select c).First();
        #region Manda para tabela de Queue para poder verificar se coloca na fila de completos
        var queue = new tbQueue();
        queue.agendamento = DateTime.Now;
        queue.andamento = false;
        queue.concluido = false;
        queue.idRelacionado = itemPedido.pedidoId;
        queue.tipoQueue = 4; // Atualiza combo
        queue.mensagem = "";
        data.tbQueues.InsertOnSubmit(queue);
        data.SubmitChanges();
        #endregion
        #region Manda para tabela de Queue para poder verificar se coloca na fila de completos  
        var queue1 = new tbQueue();
        queue1.agendamento = DateTime.Now;
        queue1.andamento = false;
        queue1.concluido = false;
        queue1.idRelacionado = itemPedido.pedidoId;
        queue1.tipoQueue = 5; // Gera combo  
        queue1.mensagem = "";
        data.tbQueues.InsertOnSubmit(queue1);
        data.SubmitChanges();
        #endregion
        return;


        var itensComboAtual = (from c in data.tbItensPedidoCombos where c.idItemPedido == itemPedidoId select c).Any();
        var pendentesAtuais = (from c in data.tbItensPedidoEnvioParcials
                               where
                                   c.enviado == false
                               select c).ToList();
        if (!itensComboAtual)
        {
            var itensFilho = (from c in data.tbProdutoRelacionados where c.idProdutoPai == itemPedido.produtoId select c).ToList();
            if (itensFilho.Count > 0)
            {
                List<int> idsProdutosFilhos = new List<int>();
                idsProdutosFilhos.AddRange(itensFilho.Select(x => x.idProdutoFilho).ToList());

                var detalhesProdutosFilhos = (from c in data.tbProdutos where idsProdutosFilhos.Contains(c.produtoId) select c).ToList();

                var produtosFilhos = (from c in detalhesProdutosFilhos where idsProdutosFilhos.Contains(c.produtoId) select c).ToList();

                decimal valorTotalVendaCombo = 0;

                foreach (var itemFilho in itensFilho)
                {
                    var produtoFilho =
                        (from c in produtosFilhos where c.produtoId == itemFilho.idProdutoFilho select c).FirstOrDefault
                            ();
                    if (produtoFilho != null)
                    {
                        valorTotalVendaCombo += produtoFilho.produtoPreco;
                    }
                }

                foreach (var itemFilho in itensFilho)
                {
                    var produtoFilho =
                        (from c in produtosFilhos where c.produtoId == itemFilho.idProdutoFilho select c).FirstOrDefault
                            ();
                    decimal precoNoCombo = 0;
                    try
                    {
                        precoNoCombo = (produtoFilho.produtoPreco * itemPedido.tbProduto.produtoPreco) /
                                       valorTotalVendaCombo;
                    }
                    catch (Exception)
                    {

                    }
                    var itemPedidoCombo = new tbItensPedidoCombo();
                    itemPedidoCombo.idItemPedido = itemPedido.itemPedidoId;
                    itemPedidoCombo.produtoId = itemFilho.idProdutoFilho;
                    try
                    {
                        itemPedidoCombo.precoDeCusto = Convert.ToDecimal(produtoFilho.produtoPrecoDeCusto);
                    }
                    catch (Exception)
                    {
                        itemPedidoCombo.precoDeCusto = 0;
                    }
                    itemPedidoCombo.valorNoCombo = precoNoCombo;

                    if (itemPedido.tbPedido.statusDoPedido == 5)
                    {
                        itemPedidoCombo.enviado = true;
                    }
                    else if (itemPedido.enviado == true)
                    {
                        var checagemPendente = (from c in pendentesAtuais
                                                where
                                                    c.idItemPedido == itemPedido.itemPedidoId && c.idProduto == itemFilho.idProdutoFilho &&
                                                    c.enviado == false
                                                select c).Any();
                        if (checagemPendente)
                        {
                            itemPedidoCombo.enviado = false;
                        }
                        else
                        {
                            itemPedidoCombo.enviado = true;
                        }
                    }
                    data.tbItensPedidoCombos.InsertOnSubmit(itemPedidoCombo);
                }

                data.SubmitChanges();
                data.Dispose();
            }
        }
    }

    protected void btnPedidoEnviado(object sender, EventArgs e)
    {
        int pedidoId = Convert.ToInt32(Request.QueryString["pedidoId"]);
        var data = new dbCommerceDataContext();
        var pedido = (from c in data.tbPedidos where c.pedidoId == pedidoId select c).First();
        pedido.statusDoPedido = 5;
        data.SubmitChanges();
        interacaoInclui("Pedido Enviado (status alterado manualmente)", "True");
        fillPedido();
    }

    protected void btnReenviarPedido_OnClick(object sender, EventArgs e)
    {
        return;

        int pedidoId = Convert.ToInt32(Request.QueryString["pedidoId"]);
        var data = new dbCommerceDataContext();
        var pedido = (from c in data.tbPedidos where c.pedidoId == pedidoId select c).First();
        pedido.statusDoPedido = 11;

        var itensPedido = (from c in data.tbItensPedidos where c.pedidoId == pedidoId select c);
        foreach (var itemPedido in itensPedido)
        {
            itemPedido.enviado = false;
            itemPedido.idPedidoEnvio = null;
        }

        var itensPedidoCombo = (from c in data.tbItensPedidoCombos where c.tbItensPedido.pedidoId == pedidoId select c);
        foreach (var itemPedido in itensPedidoCombo)
        {
            itemPedido.enviado = false;
            itemPedido.idPedidoEnvio = null;
        }

        var estoques = (from c in data.tbProdutoEstoques where c.pedidoId == pedidoId select c);
        foreach (var estoque in estoques)
        {
            estoque.idPedidoEnvio = null;
            estoque.itemPedidoId = null;
            estoque.enviado = false;
            estoque.pedidoId = null;
        }

        var pacotes = (from c in data.tbPedidoPacotes where c.idPedido == pedidoId select c);
        foreach (var pacote in pacotes)
        {
            pacote.despachado = true;
        }



        data.SubmitChanges();
        interacaoInclui("Pedido solicitado reenvio", "True");
        fillPedido();
    }

    protected void btnReenviarConfirmacaoDePagamento_OnClick(object sender, EventArgs e)
    {
        int pedidoId = Convert.ToInt32(Request.QueryString["pedidoId"]);

        if (!rnEmails.enviaEmailPagamentoConfirmado(pedidoId))
            Response.Write("<script>alert('Confirmação de pagamento reenviada com sucesso.');</script>");
    }

    protected void btnDescancelarPedido_OnClick(object sender, EventArgs e)
    {
        int pedidoId = Convert.ToInt32(Request.QueryString["pedidoId"]);

        try
        {
            using (var data = new dbCommerceDataContext())
            {
                var itensPedido = (from c in data.tbItemPedidoEstoques where c.tbItensPedido.pedidoId == pedidoId && !(c.tbItensPedido.cancelado ?? false) select c);

                foreach (var item in itensPedido)
                {
                    if (!item.enviado)
                    {
                        item.reservado = false;
                        item.dataReserva = null;
                    }
                    item.cancelado = false;
                }
                data.SubmitChanges();

                var pedido = (from c in data.tbPedidos where c.pedidoId == pedidoId select c).FirstOrDefault();
                var totalPago = (from c in data.tbPedidoPagamentos
                                 where c.pedidoId == pedidoId && c.pago && !c.cancelado && !c.pagamentoNaoAutorizado
                                 select c).Sum(x => (decimal?)x.valor) ?? 0;

                if (Math.Round((decimal)pedido.valorCobrado, 2) == Math.Round(totalPago, 2))
                    pedido.statusDoPedido = 11;
                else if (Math.Round((decimal)pedido.valorCobrado, 2) > Math.Round(totalPago, 2))
                    pedido.statusDoPedido = 2;

                data.SubmitChanges();
            }

            rnInteracoes.interacaoInclui(pedidoId, "Pedido reativado", rnUsuarios.retornaNomeUsuarioLogado(), "False");
            fillPedido();
            Response.Write("<script>alert('Pedido reativado com sucesso!');</script>");
        }
        catch (Exception)
        {
            Response.Write("<script>alert('Falha na reativação do pedido.');</script>");
        }
    }

    protected void btnEmailPrazos_OnClick(object sender, EventArgs e)
    {
        int pedidoId = Convert.ToInt32(Request.QueryString["pedidoId"]);
        var data = new dbCommerceDataContext();
        var pedido = (from c in data.tbPedidos where c.pedidoId == pedidoId select c).FirstOrDefault();
        var cliente = (from c in data.tbClientes where c.clienteId == pedido.clienteId select c).FirstOrDefault();


        string mensagem = "Olá " + cliente.clienteNome + "<br><br>";
        mensagem +=
            "Para mantê-la(o) com dados sempre atualizados e sem divergência de informações sobre a data de entrega de sua compra, disponibilizamos o link abaixo que informa em tempo real o andamento de seu pedido:<br><br>";

        mensagem += "<a href=\"http://www.graodegente.com.br/pedidodetalhe.aspx?p=" + Encrypt(Request.QueryString["pedidoId"]) +
                    "\">http://www.graodegente.com.br/pedidodetalhe.aspx?p=" + Encrypt(Request.QueryString["pedidoId"]) + "</a><br>";
        mensagem += "Clique no link ou copie e cole no navegador<br><br>Muito obrigado por confiar na Grão de Gente!<br>Equipe de Pós-vendas.";

        rnEmails.EnviaEmail("posvendas@graodegente.com.br", cliente.clienteEmail, "posvendas@graodegente.com.br", "", "", mensagem, "Andamento do seu pedido - Grão de Gente");

        interacaoInclui("E-mail de prazos enviado", "False");

    }

    public string Encrypt(string plainText)
    {
        string PasswordHash = "P@@Sw0rd";
        string SaltKey = "S@LT&KEY";
        string VIKey = "@1B2c3D4e5F6g7H8";

        byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);

        byte[] keyBytes = new Rfc2898DeriveBytes(PasswordHash, Encoding.ASCII.GetBytes(SaltKey)).GetBytes(256 / 8);
        var symmetricKey = new RijndaelManaged() { Mode = CipherMode.CBC, Padding = PaddingMode.Zeros };
        var encryptor = symmetricKey.CreateEncryptor(keyBytes, Encoding.ASCII.GetBytes(VIKey));

        byte[] cipherTextBytes;

        using (var memoryStream = new MemoryStream())
        {
            using (var cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
            {
                cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
                cryptoStream.FlushFinalBlock();
                cipherTextBytes = memoryStream.ToArray();
                cryptoStream.Close();
            }
            memoryStream.Close();
        }
        return Convert.ToBase64String(cipherTextBytes);
    }

    protected void btnChecarEstoque_OnClick(object sender, EventArgs e)
    {
        var dbPedido = new dbCommerceDataContext();
        int produtosEmEstoque = 0;
        foreach (ListViewItem item in dtlItensPedido.Items)
        {

            ASPxButton btnReservarItemEstoque = (ASPxButton)item.FindControl("btnReservarItemEstoque");
            Label lblQuantidade = (Label)item.FindControl("lblQuantidade");
            HiddenField hdfPedidoId = (HiddenField)item.FindControl("hdfPedidoId");

            HiddenField hdfEnviado = (HiddenField)item.FindControl("hdfEnviado");
            bool enviado = false;
            Boolean.TryParse(hdfEnviado.Value, out enviado);

            int produtoId = Convert.ToInt32(((Label)item.FindControl("lblProdutoId")).Text);
            decimal itemQuantidade = decimal.Parse(lblQuantidade.Text);

            var produto = (from c in dbPedido.tbProdutos where c.produtoId == produtoId select c).FirstOrDefault();
            int produtoAlternativoId = produto.produtoAlternativoId ?? produto.produtoId;

            int pedidoId = Convert.ToInt32(hdfPedidoId.Value);

            HiddenField txtItemPedidoId = (HiddenField)item.FindControl("txtItemPedidoId");
            int itemPedidoId = Convert.ToInt32(txtItemPedidoId.Value);
            int enderecado = 0;

            var naoCombo = !(from c in dbPedido.tbItensPedidoCombos where c.idItemPedido == itemPedidoId select c).Any();

            if (naoCombo)
            {

                int totalProdutosEntregues = 0;
                var pedidosFornecedor = (from c in dbPedido.tbPedidoFornecedorItems
                                         where (c.idProduto == produtoId | c.idProduto == produtoAlternativoId) && c.idItemPedido == itemPedidoId && c.idPedido == pedidoId
                                         select c);

                foreach (var pedidoFornecedorItem in pedidosFornecedor)
                {
                    if (pedidoFornecedorItem.entregue)
                    {
                        totalProdutosEntregues += pedidoFornecedorItem.quantidade;

                        if ((pedidoFornecedorItem.liberado ?? false) == true)
                        {
                            enderecado++;
                        }
                    }
                }

                var reservasDeEstoque = (from c in dbPedido.tbProdutoReservaEstoques
                                         where c.idProduto == produtoId && c.idItemPedido == itemPedidoId && c.idPedido == pedidoId
                                         select c);

                foreach (var reserva in reservasDeEstoque)
                {
                    totalProdutosEntregues += reserva.quantidade;
                    enderecado++;
                }

                var estoqueDoItem = dbPedido.admin_produtoEmEstoque(produtoId).FirstOrDefault();
                int estoqueTotalDoItem = estoqueDoItem == null ? 0 : Convert.ToInt32(estoqueDoItem.estoqueLivre);

                if (estoqueTotalDoItem > 0 && (itemQuantidade > totalProdutosEntregues) && (estoqueTotalDoItem >= (itemQuantidade - totalProdutosEntregues)))
                {
                    btnReservarItemEstoque.Visible = true;
                    produtosEmEstoque++;
                }

                var envioParcial = (from c in dbPedido.tbItensPedidoEnvioParcials
                                    where
                                        c.idPedido == pedidoId && c.enviado == false && c.idProduto == produtoId &&
                                        c.idItemPedido == itemPedidoId
                                    select c).Any();

                var faltando = (from c in dbPedido.tbPedidoProdutoFaltandos
                                where
                                    c.produtoId == produtoId && c.pedidoId == pedidoId && c.itemPedidoId == itemPedidoId &&
                                    c.entregue == false
                                select c).Any();

                HtmlTableRow tr = (HtmlTableRow)item.FindControl("trItem");

                if (enviado && envioParcial == false)
                {
                    tr.BgColor = "#D2E9FF";

                }
                else if (faltando && envioParcial == false)
                {
                    tr.BgColor = "#FFC6C7";

                }
                else if (itemQuantidade <= totalProdutosEntregues && envioParcial == false)
                {
                    tr.BgColor = "#FFFFBB";

                    if (enderecado < totalProdutosEntregues)
                        tr.BgColor = "#F2B84E";
                }
                else
                {
                    if (envioParcial)
                    {

                        if (faltando)
                        {
                            //item.BackColor = ColorTranslator.FromHtml("#FFC6C7");
                        }
                        else if (itemQuantidade <= totalProdutosEntregues)
                        {
                            // item.BackColor = ColorTranslator.FromHtml("#FFFFBB");

                            // if (enderecado < totalProdutosEntregues)
                            //   item.BackColor = ColorTranslator.FromHtml("#F2B84E");
                        }
                        else
                        {
                            //  item.BackColor = ColorTranslator.FromHtml("#FFC6C7");
                        }
                    }
                    else
                    {
                        // item.BackColor = ColorTranslator.FromHtml("#FFC6C7");
                    }
                }

            }
            else
            {
                var itensCombo = (from c in dbPedido.tbItensPedidoCombos where c.idItemPedido == itemPedidoId select c);

                foreach (var itemCombo in itensCombo)
                {
                    int totalProdutosEntregues = 0;
                    var pedidosFornecedor = (from c in dbPedido.tbPedidoFornecedorItems
                                             where (c.idProduto == produtoId | c.idProduto == produtoAlternativoId) && c.idItemPedido == itemCombo.idItemPedido && c.idPedido == pedidoId
                                             select c);

                    foreach (var pedidoFornecedorItem in pedidosFornecedor)
                    {
                        if (pedidoFornecedorItem.entregue)
                        {
                            totalProdutosEntregues += pedidoFornecedorItem.quantidade;

                            if ((pedidoFornecedorItem.liberado ?? false) == true)
                            {
                                enderecado++;
                            }
                        }
                    }

                    var reservasDeEstoque = (from c in dbPedido.tbProdutoReservaEstoques
                                             where c.idProduto == produtoId && c.idItemPedido == itemCombo.idItemPedido && c.idPedido == pedidoId
                                             select c);

                    foreach (var reserva in reservasDeEstoque)
                    {
                        totalProdutosEntregues += reserva.quantidade;
                        enderecado++;
                    }

                    var estoqueDoItem = dbPedido.admin_produtoEmEstoque(produtoId).FirstOrDefault();
                    int estoqueTotalDoItem = estoqueDoItem == null ? 0 : Convert.ToInt32(estoqueDoItem.estoqueLivre);

                    if (estoqueTotalDoItem > 0 && (itemQuantidade > totalProdutosEntregues) && (estoqueTotalDoItem >= (itemQuantidade - totalProdutosEntregues)))
                    {
                        btnReservarItemEstoque.Visible = true;
                        produtosEmEstoque++;
                    }

                    var envioParcial = (from c in dbPedido.tbItensPedidoEnvioParcials
                                        where
                                            c.idPedido == pedidoId && c.enviado == false && c.idProduto == produtoId &&
                                            c.idItemPedido == itemCombo.idItemPedido
                                        select c).Any();

                    var faltando = (from c in dbPedido.tbPedidoProdutoFaltandos
                                    where
                                        c.produtoId == produtoId && c.pedidoId == pedidoId && c.itemPedidoId == itemPedidoId &&
                                        c.entregue == false
                                    select c).Any();

                    if (enviado && envioParcial == false)
                    {
                        // item.BackColor = ColorTranslator.FromHtml("#D2E9FF");

                    }
                    else if (faltando && envioParcial == false)
                    {
                        // item.BackColor = ColorTranslator.FromHtml("#FFC6C7");

                    }
                    else if (itemQuantidade <= totalProdutosEntregues && envioParcial == false)
                    {
                        //  item.BackColor = ColorTranslator.FromHtml("#FFFFBB");

                        // if (enderecado < totalProdutosEntregues)
                        //   item.BackColor = ColorTranslator.FromHtml("#F2B84E");
                    }
                    else
                    {
                        if (envioParcial)
                        {

                            if (faltando)
                            {
                                //  item.BackColor = ColorTranslator.FromHtml("#FFC6C7");
                            }
                            else if (itemQuantidade <= totalProdutosEntregues)
                            {
                                // item.BackColor = ColorTranslator.FromHtml("#FFFFBB");

                                //  if (enderecado < totalProdutosEntregues)
                                //     item.BackColor = ColorTranslator.FromHtml("#F2B84E");
                            }
                            else
                            {
                                //  item.BackColor = ColorTranslator.FromHtml("#FFC6C7");
                            }
                        }
                        else
                        {
                            //  item.BackColor = ColorTranslator.FromHtml("#FFC6C7");
                        }
                    }
                }


            }

        }

        if (produtosEmEstoque == 0)
            Response.Write("<script>alert('Não existe estoque para nenhum produto do pedido!');</script>");

    }

    protected void PreenchiDropDownAnoVencimentoCartao()
    {
        ddlValidadeAnoNovaCobranca.Items.Clear();

        for (int i = DateTime.Now.Year; i < (DateTime.Now.Year + 11); i++)
        {
            ddlValidadeAnoNovaCobranca.Items.Add(new ListItem(i.ToString(), i.ToString()));
        }
    }

    protected void btnPriorizarPedido_OnClick(object sender, EventArgs e)
    {
        int pedidoId = Convert.ToInt32(Request.QueryString["pedidoId"]);
        var data = new dbCommerceDataContext();
        var itensEstoque = (from c in data.tbItemPedidoEstoques
                            where
                                c.tbItensPedido.pedidoId == pedidoId && c.enviado == false && c.reservado == false &&
                                c.cancelado == false && c.dataLimite != null
                            select c).ToList();


        var log = new rnLog();
        log.usuario = rnUsuarios.retornaNomeUsuarioLogado();
        log.descricoes.Add("Priorizando Pedido");
        log.tiposRelacionados.Add(new rnEnums.TipoRelacionadoIds() { idRegistroRelacionado = pedidoId, tipoRelacionado = rnEnums.TipoRegistroRelacionado.Pedido });
        log.tiposOperacao.Add(rnEnums.TipoOperacao.Pedido);

        foreach (var itemEstoque in itensEstoque)
        {
            var aguardando = (from c in data.tbItemPedidoEstoques
                              where c.produtoId == itemEstoque.produtoId && c.reservado == false && c.cancelado == false && c.enviado == false && c.dataLimite != null
                              orderby c.dataLimite
                              select new
                              {
                                  c.idItemPedidoEstoque,
                                  c.tbItensPedido.pedidoId,
                                  dataLimiteReserva = c.dataLimite,
                                  fornecedorPrazoPedidos = c.tbProduto.tbProdutoFornecedor.fornecedorPrazoPedidos ?? 1
                              }).ToList();
            var pedidos = (from c in data.tbPedidoFornecedorItems
                           where (c.entregue == false | (c.entregue == true && (c.liberado ?? false) == false)) && c.idProduto == itemEstoque.produtoId
                           orderby c.tbPedidoFornecedor.dataLimite, c.tbPedidoFornecedor.idPedidoFornecedor
                           select new
                           {
                               c.idPedidoFornecedor,
                               c.idPedidoFornecedorItem,
                               dataLimiteFornecedor = c.tbPedidoFornecedor.dataLimite
                           }).ToList();
            var listaFinal = (from c in aguardando.Select((item, index) => new { item, index })
                              join d in pedidos.Select((item, index) => new { item, index }) on c.index equals d.index into
                                  pedidosFornecedor
                              from f in pedidosFornecedor.DefaultIfEmpty()
                              select new
                              {
                                  c.item.idItemPedidoEstoque,
                                  c.item.pedidoId,
                                  c.item.dataLimiteReserva,
                                  idPedidoFornecedor = f != null ? f.item.idPedidoFornecedor : 0,
                                  dataLimiteFornecedor = f != null ? (DateTime?)f.item.dataLimiteFornecedor : (DateTime.Now.AddDays(rnFuncoes.retornaPrazoDiasUteis(c.item.fornecedorPrazoPedidos + 1, 0))),
                                  idPedidoFornecedorItem = f != null ? (int?)f.item.idPedidoFornecedorItem : null
                              }
                ).ToList();
            var menorPrazo = ((DateTime)(listaFinal.FirstOrDefault(x => x.idItemPedidoEstoque != itemEstoque.idItemPedidoEstoque) ==
                             null
                ? DateTime.Now
                : listaFinal.FirstOrDefault(x => x.idItemPedidoEstoque != itemEstoque.idItemPedidoEstoque)
                    .dataLimiteReserva)).AddMinutes(-1);

            var aguardandoPriorizado = (from c in aguardando
                                        select new
                                        {
                                            c.idItemPedidoEstoque,
                                            c.pedidoId,
                                            dataLimiteReserva = c.idItemPedidoEstoque == itemEstoque.idItemPedidoEstoque ? menorPrazo : c.dataLimiteReserva,
                                            c.fornecedorPrazoPedidos
                                        }).ToList().OrderBy(x => x.dataLimiteReserva);
            var listaFinalPriorizado = (from c in aguardandoPriorizado.Select((item, index) => new { item, index })
                                        join d in pedidos.Select((item, index) => new { item, index }) on c.index equals d.index into
                                            pedidosFornecedor
                                        from f in pedidosFornecedor.DefaultIfEmpty()
                                        select new
                                        {
                                            c.item.idItemPedidoEstoque,
                                            c.item.pedidoId,
                                            c.item.dataLimiteReserva,
                                            idPedidoFornecedor = f != null ? f.item.idPedidoFornecedor : 0,
                                            dataLimiteFornecedor = f != null ? (DateTime?)f.item.dataLimiteFornecedor : (DateTime.Now.AddDays(rnFuncoes.retornaPrazoDiasUteis(c.item.fornecedorPrazoPedidos + 1, 0))),
                                            idPedidoFornecedorItem = f != null ? (int?)f.item.idPedidoFornecedorItem : null
                                        }
                ).ToList();
            var quantidadesAtrasosAtuaisLista = listaFinal.Where(x => ((x.dataLimiteReserva ?? DateTime.Now).AddDays(1).Date < (x.dataLimiteFornecedor.Value.Date)));
            int quantidadesAtrasosAtuais = quantidadesAtrasosAtuaisLista.Count();
            var quantidadesAtrasosPriorizadoLista = listaFinalPriorizado.Where(x => ((x.dataLimiteReserva ?? DateTime.Now).AddDays(1).Date < (x.dataLimiteFornecedor.Value.Date)));
            int quantidadesAtrasosPriorizado = quantidadesAtrasosPriorizadoLista.Count();
            if (quantidadesAtrasosPriorizado > quantidadesAtrasosAtuais)
            {
                var listaIdsAtrasosAntigos = quantidadesAtrasosAtuaisLista.Select(x => x.pedidoId).ToList();
                var idsNovos = quantidadesAtrasosPriorizadoLista.Where(x => !listaIdsAtrasosAntigos.Contains(x.pedidoId)).Select(x => x.pedidoId);
                foreach (var idNovo in idsNovos)
                {
                    log.descricoes.Add(idNovo + " gerou atraso");
                    log.tiposRelacionados.Add(new rnEnums.TipoRelacionadoIds() { idRegistroRelacionado = idNovo, tipoRelacionado = rnEnums.TipoRegistroRelacionado.Pedido });
                }
            }

            log.tiposRelacionados.Add(new rnEnums.TipoRelacionadoIds() { idRegistroRelacionado = itemEstoque.produtoId, tipoRelacionado = rnEnums.TipoRegistroRelacionado.Produto });

            itemEstoque.dataLimite = menorPrazo;
            data.SubmitChanges();
            verificaAdiantamento(itemEstoque.produtoId);
        }

        log.InsereLog();


        interacaoInclui("Pedido Priorizado", "False");
    }





    private void verificaAdiantamento(int produtoId)
    {
        bool liberado = false;
        var data = new dbCommerceDataContext();
        try
        {
            var reservados = (from c in data.tbProdutoEstoques
                              where c.enviado == false && c.idItemPedidoEstoqueReserva != null && c.pedidoId == null && c.produtoId == produtoId
                              orderby c.idPedidoFornecedorItem
                              select new
                              {
                                  c.pedidoIdReserva,
                                  c.itemPedidoIdReserva,
                                  c.produtoId,
                                  c.tbProduto.produtoNome,
                                  c.idPedidoFornecedorItem
                              }).Take(30).ToList();

            foreach (var reservado in reservados)
            {
                if (!liberado)
                {
                    var reservadoDetalhe = (from c in data.tbProdutoEstoques
                                            where
                                                c.idPedidoFornecedorItem == reservado.idPedidoFornecedorItem && c.enviado == false &&
                                                c.idItemPedidoEstoqueReserva != null && c.pedidoId == null
                                            select c).FirstOrDefault();
                    if (reservadoDetalhe != null)
                    {

                        string linha = "";
                        var itensAguardando = (from c in data.tbItemPedidoEstoques
                                               where
                                                   c.tbItensPedido.pedidoId == reservado.pedidoIdReserva && c.reservado == false &&
                                                   c.enviado == false && c.cancelado == false
                                               select
                                                   c).Count();
                        if (itensAguardando > 0)
                        {
                            var prazoAguardando = itensAguardando > 0
                                ? (from c in data.tbItemPedidoEstoques
                                   where
                                       c.tbItensPedido.pedidoId == reservado.pedidoIdReserva && c.reservado == false &&
                                       c.enviado == false && c.cancelado == false
                                   select
                                       c).OrderByDescending(x => x.dataLimite).First().dataLimite
                                : null;
                            var primeiroFila = (from c in data.tbItemPedidoEstoques
                                                where
                                                    c.reservado == false && c.cancelado == false && c.enviado == false &&
                                                    c.dataLimite != null &&
                                                    c.produtoId == reservado.produtoId
                                                orderby c.dataLimite
                                                select c).FirstOrDefault();
                            if (primeiroFila != null)
                            {
                                if (primeiroFila.dataLimite < prazoAguardando)
                                {
                                    bool temErro = false;
                                    bool? icTemErro = null;
                                    string dsErro = "";
                                    data.v1_cancelaReserva(reservadoDetalhe.idPedidoFornecedorItem, ref icTemErro, ref dsErro);
                                    liberado = true;
                                }
                            }
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {

        }

    }



    private void FillInteracoesTransportadora()
    {
        int pedidoId = Convert.ToInt32(Request.QueryString["pedidoId"]);
        var data = new dbCommerceDataContext();
        var interacoes =
            (from c in data.tbInteracaoTransportadoras where c.pedidoId == pedidoId orderby c.data descending select c);
        dtlInteracoesTransportadora.DataSource = interacoes;
        dtlInteracoesTransportadora.DataBind();
    }


    protected void btnSalvarInteracaoTransportadora_Click(object sender, ImageClickEventArgs e)
    {

        int pedidoId = Convert.ToInt32(Request.QueryString["pedidoId"]);
        string nome = rnUsuarios.retornaNomeUsuarioLogado();

        var data = new dbCommerceDataContext();
        var interacao = new tbInteracaoTransportadora();
        interacao.data = DateTime.Now;
        interacao.pedidoId = pedidoId;
        interacao.interacao = txtInteracaoTransportadora.Text;
        interacao.usuario = nome;
        data.tbInteracaoTransportadoras.InsertOnSubmit(interacao);
        data.SubmitChanges();

        rnEmails.EnviaEmail("", "logisticagraodegente@gmail.com", "mayara@graodegente.com.br", "", "", txtInteracaoTransportadora.Text, "Informações para Transportadora: Pedido " + pedidoId);
        txtInteracaoTransportadora.Text = "";
        Response.Write("<script>alert('Interação gravada com sucesso.');</script>");
        FillInteracoesTransportadora();
    }


    protected void btnDescancelarProduto_OnClick(object sender, EventArgs e)
    {
        try
        {
            foreach (ListViewItem item in lstReativarProdutos.Items)
            {
                CheckBox ckbMarcar = (CheckBox)item.FindControl("ckbReativar");

                if (ckbMarcar.Checked)
                {
                    HiddenField hiddenProdutoId = (HiddenField)item.FindControl("hiddenProdutoId");
                    int produtoId = Convert.ToInt32(hiddenProdutoId.Value);

                    HiddenField hiddenItemPedidoId = (HiddenField)item.FindControl("hiddenItemPedidoId");
                    int itemPedidoId = Convert.ToInt32(hiddenItemPedidoId.Value);

                    using (var data = new dbCommerceDataContext())
                    {
                        var itemProduto = (from c in data.tbItensPedidos where c.itemPedidoId == itemPedidoId select c).First();
                        itemProduto.cancelado = false;
                        data.SubmitChanges();

                        var itensPedidoEstoque = (from c in data.tbItemPedidoEstoques where c.itemPedidoId == itemPedidoId select c);
                        foreach (var itemPedidoEstoque in itensPedidoEstoque)
                        {
                            itemPedidoEstoque.cancelado = false;
                            data.SubmitChanges();
                        }

                        interacaoInclui("Reativado (Descancelado) produto: " + itemProduto.tbProduto.produtoNome + " - itemPedidoId: " + itemProduto.itemPedidoId, "False");
                    }


                }
            }

            rnPedidos.recalculaTotaisPedido(int.Parse(Request.QueryString["pedidoId"]));
            dtlItensPedido.DataBind();
            dtvPedido.DataBind();
            dtlInteracao.DataBind();
            popDescancelarProduto.ShowOnPageLoad = false;

            Response.Write("<script>alert('Produto reativado com sucesso.');</script>");
        }
        catch (Exception)
        {

        }

    }

    protected void btnMarcarProdutoComoEnviado_OnClick(object sender, EventArgs e)
    {
        try
        {
            foreach (ListViewItem item in lstMarcarProdutoComoEnviado.Items)
            {
                CheckBox ckbMarcar = (CheckBox)item.FindControl("ckbEnviado");

                if (ckbMarcar.Checked)
                {
                    HiddenField hiddenProdutoId = (HiddenField)item.FindControl("hiddenProdutoId");
                    int produtoId = Convert.ToInt32(hiddenProdutoId.Value);

                    HiddenField hiddenIdItemPedidoEstoque = (HiddenField)item.FindControl("hiddenIdItemPedidoEstoque");
                    int idItemPedidoEstoque = Convert.ToInt32(hiddenIdItemPedidoEstoque.Value);

                    using (var data = new dbCommerceDataContext())
                    {

                        var itemPedidoEstoque = (from c in data.tbItemPedidoEstoques
                                                 where c.idItemPedidoEstoque == idItemPedidoEstoque
                                                 select c).FirstOrDefault();
                        itemPedidoEstoque.enviado = true;
                        itemPedidoEstoque.cancelado = false;
                        itemPedidoEstoque.reservado = true;
                        data.SubmitChanges();

                        tbProdutoEstoque removerReserva;

                        using (
                            var txn =
                                new System.Transactions.TransactionScope(
                                    System.Transactions.TransactionScopeOption.Required,
                                    new System.Transactions.TransactionOptions
                                    {
                                        IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                                    }))

                        {
                            removerReserva = (from c in data.tbProdutoEstoques
                                              where c.idItemPedidoEstoqueReserva == idItemPedidoEstoque
                                              select c).FirstOrDefault();
                        }

                        if (removerReserva != null)
                        {
                            removerReserva.pedidoIdReserva = null;
                            removerReserva.itemPedidoIdReserva = null;
                            removerReserva.idItemPedidoEstoqueReserva = null;
                            data.SubmitChanges();
                        }

                        interacaoInclui("Marcado como enviado produto: " + itemPedidoEstoque.tbProduto.produtoNome + " - itemPedidoId: " + itemPedidoEstoque.itemPedidoId, "False");
                    }

                }
            }

            dtlItensPedido.DataBind();
            dtvPedido.DataBind();
            dtlInteracao.DataBind();
            popMarcarProdutoComoEnviado.ShowOnPageLoad = false;

            Response.Write("<script>alert('Produto marcado como enviado.');</script>");
        }
        catch (Exception)
        {

        }
    }

    protected void lstProdutosPersonalizacao_OnItemDataBound(object sender, ListViewItemEventArgs e)
    {
        ListView lstProdutoPersonalizadoItens = (ListView)e.Item.FindControl("lstProdutoPersonalizadoItens");
        int produtoPaiId = Convert.ToInt32(((HiddenField)e.Item.FindControl("hfIdProdutoPaiPersonalizacao")).Value);
        int itemPedidoId = Convert.ToInt32(((HiddenField)e.Item.FindControl("hfItemPedidoIdProdutoPaiPersonalizacao")).Value); ;

        using (var data = new dbCommerceDataContext())
        {
            var produtosPersonalizados = (from c in data.tbProdutoPersonalizacaos
                                          where c.idProdutoPai == produtoPaiId
                                          select new
                                          {
                                              c.idProdutoFilhoPersonalizacao,
                                              c.tbProduto1.produtoNome,
                                              c.idProdutoPai,
                                              itemPedidoId
                                          }).ToList();

            lstProdutoPersonalizadoItens.DataSource = produtosPersonalizados;
            lstProdutoPersonalizadoItens.DataBind();

        }


    }

    protected void btnSalvarItemPersonalizacao_OnClick(object sender, EventArgs e)
    {
        try
        {
            bool result = false;
            int prazoFornecedor = 0;
            foreach (var item in lstProdutosPersonalizacao.Items)
            {
                ListView lstProdutoPersonalizadoItens = (ListView)item.FindControl("lstProdutoPersonalizadoItens");
                foreach (var itemPersonalizado in lstProdutoPersonalizadoItens.Items)
                {
                    RadioButton rbSelecionarPersonalizacao = (RadioButton)itemPersonalizado.FindControl("rbSelecionarPersonalizacao");

                    if (rbSelecionarPersonalizacao.Checked)
                    {
                        int idProdutoPai = Convert.ToInt32(rbSelecionarPersonalizacao.Attributes["value"].Split('#')[0]);// produto pai personalização
                        int idProdutoFilho = Convert.ToInt32(rbSelecionarPersonalizacao.Attributes["value"].Split('#')[1]);// produto da personalização
                        int itemPedidoId = Convert.ToInt32(rbSelecionarPersonalizacao.Attributes["value"].Split('#')[2]);
                        string produtoPersonalizacaoPai = "";
                        string produtoPersonalizado = "";
                        using (var data = new dbCommerceDataContext())
                        {
                            var itemPedidoAtual = (from c in data.tbItemPedidoEstoques where c.itemPedidoId == itemPedidoId && c.produtoId == idProdutoPai select c).First();
                            itemPedidoAtual.produtoId = idProdutoFilho;
                            itemPedidoAtual.dataReserva = null;
                            itemPedidoAtual.reservado = false;
                            itemPedidoAtual.enviado = false;
                            itemPedidoAtual.dataLimite = null;
                            itemPedidoAtual.prazoLimiteInicial = null;
                            data.SubmitChanges();

                            var itemPedido = (from c in data.tbItensPedidos where c.itemPedidoId == itemPedidoId select c).First();
                            itemPedido.dataDaCriacao = DateTime.Now;
                            result = true;
                            data.SubmitChanges();

                            produtoPersonalizacaoPai = (from c in data.tbProdutos where c.produtoId == idProdutoPai select c).First().produtoNome;
                            var dadosProdutoPersonalizado = (from c in data.tbProdutos
                                                             where c.produtoId == idProdutoFilho
                                                             select new { c.produtoNome, fornecedorPrazoPedidos = c.tbProdutoFornecedor.fornecedorPrazoPedidos ?? 0 }).First();
                            produtoPersonalizado = dadosProdutoPersonalizado.produtoNome;
                            data.SubmitChanges();

                            if (dadosProdutoPersonalizado.fornecedorPrazoPedidos > prazoFornecedor)
                                prazoFornecedor = dadosProdutoPersonalizado.fornecedorPrazoPedidos;
                            data.SubmitChanges();
                        }

                        interacaoInclui("Produto de personalização: " + produtoPersonalizacaoPai + "</br>Substituído pela personalização: " + produtoPersonalizado, "False");
                    }
                }
            }

            dtlItensPedido.DataBind();

            if (result)
                ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('Produto para personalização informado com sucesso!');", true);

            popProdutoPersonalizacao.ShowOnPageLoad = false;

            #region Atualizar prazo de postagem de acordo com data da informação da personalização
            using (var data = new dbCommerceDataContext())
            {
                var agora = DateTime.Now;
                var queue = new tbQueue();
                queue.tipoQueue = 5;
                queue.agendamento = agora;
                queue.idRelacionado = int.Parse(Request.QueryString["pedidoId"]);
                queue.mensagem = "";
                queue.concluido = false;
                queue.andamento = false;
                data.tbQueues.InsertOnSubmit(queue);
                data.SubmitChanges();
            }
            /*try
            {
                int pedidoId = int.Parse(Request.QueryString["pedidoId"]);

                using (var data = new dbCommerceDataContext())
                {
                    var pedido = (from c in data.tbPedidos where c.pedidoId == pedidoId select c).First();
                    if (pedido.dataConfirmacaoPagamento != null)
                    {
                        var novoPrazoPostagem = DateTime.Now.AddDays(rnFuncoes.retornaPrazoDiasUteis(prazoFornecedor + 1, 0));
                        if (pedido.prazoMaximoPostagemAtualizado != null && pedido.prazoMaximoPostagemAtualizado.Value.Date < novoPrazoPostagem.Date)
                        {
                            interacaoInclui("Devido a informação da personalização o prazo de postagem do pedido foi atualizado de: " + Convert.ToDateTime(pedido.prazoMaximoPostagemAtualizado).ToString("d") +
                                            "</br>Para a data de: " + novoPrazoPostagem.ToString("d"), "False");
                            pedido.prazoMaximoPostagemAtualizado = novoPrazoPostagem;
                            data.SubmitChanges();

                            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('Houve alteração no prazo de postagem!');", true);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }*/

            #endregion Atualizar prazo de postagem de acordo com data da informação da personalização

        }
        catch (Exception ex)
        {
        }

    }

    private void manterCoresStatusProdutos()
    {
        if (Session[Request.QueryString["pedidoId"]] == null) return;
        try
        {
            List<CoresLinhas> cores = (List<CoresLinhas>)Session[Request.QueryString["pedidoId"]];

            int produtoId;
            foreach (var itemPedido in dtlItensPedido.Items)
            {
                HiddenField txtItemPedidoId = (HiddenField)itemPedido.FindControl("txtItemPedidoId");
                int itemPedidoId = Convert.ToInt32(txtItemPedidoId.Value);

                DataList lstCombo = (DataList)itemPedido.FindControl("lstCombo");


                foreach (DataListItem itemCombo in lstCombo.Items)
                {
                    using (HiddenField hdfProdutoId = (HiddenField)itemCombo.FindControl("hdfProdutoId"))
                    {
                        int.TryParse(hdfProdutoId.Value, out produtoId);
                    }

                    var item = cores.FirstOrDefault(x => x.itemPedido == itemPedidoId && x.produtoId == produtoId);

                    try
                    {
                        if (item.produtoId == produtoId)
                        {
                            itemCombo.BackColor = ColorTranslator.FromHtml("#" + item.cor);
                        }
                    }
                    catch (Exception)
                    {

                    }
                }
            }
        }
        catch (Exception ex)
        {

        }
    }

    public class CoresLinhas
    {
        public int itemPedido { get; set; }
        public int produtoId { get; set; }
        public string cor { get; set; }
        public int rowIndex { get; set; }
    }


    protected void btnAtribuirEtiqueta_Click1(object sender, EventArgs e)
    {
        using (var data = new dbCommerceDataContext())
        {
            var produtoEstoque = (from c in data.tbItemPedidoEstoques where c.idItemPedidoEstoque == Convert.ToInt32(hdfIdProdutoEstoqueAtribuir.Value) select c).FirstOrDefault();
            if (produtoEstoque != null)
            {
                var etiqueta = 0;
                int.TryParse(txtEtiquetaAtribuir.Text, out etiqueta);
                var fornecedorItem = (from c in data.tbPedidoFornecedorItems where c.idPedidoFornecedorItem == etiqueta select c).FirstOrDefault();
                if (fornecedorItem == null)
                {
                    AlertShow("Etiqueta não localizada");
                    return;
                }

                if (fornecedorItem.idProduto != produtoEstoque.produtoId)
                {
                    AlertShow("Produto da etiqueta diferente do produto do pedido");
                    return;
                }

                var itemEstoque = (from c in data.tbProdutoEstoques where c.idPedidoFornecedorItem == etiqueta select c).FirstOrDefault();
                if (itemEstoque != null)
                {
                    if (itemEstoque.pedidoIdReserva != null)
                    {
                        AlertShow("Etiqueta já está reservada para outro pedido");
                        return;
                    }
                }

                if (fornecedorItem.idPedido != null)
                {
                    AlertShow("Etiqueta já está atribuída ao pedido " + fornecedorItem.idPedido);
                    return;
                }

                AlertShow("Etiqueta atribuída com sucesso");
                interacaoInclui("Etiqueta " + etiqueta + " atribuída ao pedido.", "False");
                popAtribuirEtiqueta.ShowOnPageLoad = false;
                fornecedorItem.idPedido = produtoEstoque.tbItensPedido.pedidoId;
                fornecedorItem.idItemPedido = produtoEstoque.itemPedidoId;
                data.SubmitChanges();
            }
        }
    }

    protected void btnAtribuirEtiqueta_Command(object sender, CommandEventArgs e)
    {
        int idItemPedidoEstoque = Convert.ToInt32(e.CommandArgument);
        hdfIdProdutoEstoqueAtribuir.Value = idItemPedidoEstoque.ToString();
        popAtribuirEtiqueta.ShowOnPageLoad = true;
    }

    protected void btnResetarPesagem_Command(object sender, CommandEventArgs e)
    {
        int idPedidoPacote = Convert.ToInt32(e.CommandArgument);
        var data = new dbCommerceDataContext();
        var pacote = (from c in data.tbPedidoPacotes where c.idPedidoPacote == idPedidoPacote select c).First();
        interacaoInclui("Pesagem reiniciada no pacote " + pacote.numeroVolume, "False");
        pacote.peso = 0;
        pacote.rastreio = "";
        pacote.largura = null;
        pacote.altura = null;
        pacote.profundidade = null;
        pacote.codJadlog = null;
        pacote.concluido = false;
        pacote.despachado = null;
        pacote.formaDeEnvio = "";
        pacote.statusAtual = null;
        pacote.carregadoCaminhao = null;
        data.SubmitChanges();

        fillRastreios();
    }

    protected void btnReenviarNota_Command(object sender, CommandEventArgs e)
    {
        int idNotaFiscal = Convert.ToInt32(e.CommandArgument);
        var data = new dbCommerceDataContext();
        var nota = (from c in data.tbNotaFiscals where c.idNotaFiscal == idNotaFiscal select c).First();
        rnNotaFiscal.emiteNotasCompletas(nota.idPedidoEnvio ?? 0);
        fillNotas();
    }

    protected void btnDevolverNota_Command(object sender, CommandEventArgs e)
    {

        hdfIdNotaFiscal.Value = e.CommandArgument.ToString();
        int idNotaFiscal = Convert.ToInt32(e.CommandArgument);
        int pedidoId = Convert.ToInt32(Request.QueryString["pedidoId"]);
        var data = new dbCommerceDataContext();

        var produtosNota = (from c in data.tbNotaFiscalItems
                            where c.idNotaFiscal == idNotaFiscal
                            select new { c.produtoId }).GroupBy(p => p.produtoId).Select(g => g.First()).ToList();

        List<itemDevolucao> itensDevolucao = new List<itemDevolucao>();

        foreach (var item in produtosNota)
        {
            var qtdMesmoProdutoNota = (from c in data.tbNotaFiscalItems
                                       where c.produtoId == (int)item.produtoId
      && c.idNotaFiscal == idNotaFiscal &&
      (c.idNotaFistalItemDevolucao == null
      || (from n in data.tbNotaFiscalItems where n.idNotaFiscalItem == c.idNotaFistalItemDevolucao select n.tbNotaFiscal.ultimoStatus).FirstOrDefault().ToString().ToLower().Trim() != "autorizado o uso da nf-e")
                                       select c.produtoId).Count();
            var itensMesmoProdutoNota = (from c in data.tbItemPedidoEstoques
                                         where c.produtoId == (int)item.produtoId
                                         && c.tbItensPedido.pedidoId == pedidoId
                                         select c).Take(qtdMesmoProdutoNota);
            foreach (var itemMesmoProduto in itensMesmoProdutoNota)
            {
                itemDevolucao itemDevolucao = (from c in data.tbNotaFiscalItems
                                               where c.produtoId == (int)item.produtoId
                                                   && c.idNotaFiscal == idNotaFiscal
                                               select new itemDevolucao
                                               {
                                                   produtoId = c.produtoId,
                                                   idItemPedidoEstoque = itemMesmoProduto.idItemPedidoEstoque,
                                                   produtoNome = c.produtoNome,
                                                   ncm = c.ncm

                                               }).FirstOrDefault();
                itensDevolucao.Add(itemDevolucao);

            }

        }
        lstProdutoNotaDevolucao2.DataSource = itensDevolucao;
        lstProdutoNotaDevolucao2.DataBind();
        tbEmpresa empresa = (from c in data.tbEmpresas
                             where c.idEmpresa ==
                             (from n in data.tbNotaFiscals where n.idNotaFiscal == idNotaFiscal select n.idCNPJ).FirstOrDefault()
                             select c).FirstOrDefault();
        lblEmpresaNotaFiscalDevolucao.Text = empresa.nome;
        hdIdEmpresaNotaFiscalDevolucao.Value = empresa.idEmpresa.ToString();

        var notaFiscal = (from c in data.tbNotaFiscals
                          where c.idNotaFiscal == idNotaFiscal
                          select new
                          {
                              numeroNota = c.numeroNota,
                              nfeKey = c.nfeKey
                          }).FirstOrDefault();
        lblNotaFiscalReferenciaDevolucao.Text = notaFiscal.numeroNota.ToString();
        hdNfeKey.Value = notaFiscal.nfeKey;

        pcNotaDevolucao2.ShowOnPageLoad = true;
    }

    protected void lstProdutoNota2_DataBound(object sender, ListViewItemEventArgs e)
    {
        HiddenField hiddenProdutoId = (HiddenField)e.Item.FindControl("hiddenProdutoId");
        int produtoId = Convert.ToInt32(hiddenProdutoId.Value);

        #region fotos
        Image fotoDestaque = (Image)e.Item.FindControl("fotoDestaque");
        if (rnFotos.produtoFotoDestaqueSeleciona_PorProdutoId(produtoId).Tables[0].Rows.Count > 0)
            fotoDestaque.ImageUrl = ConfigurationManager.AppSettings["caminhoCDN"] + "fotos/" + produtoId + "/pequena_" + rnFotos.produtoFotoDestaqueSeleciona_PorProdutoId(produtoId).Tables[0].Rows[0]["produtoFoto"].ToString() + ".jpg";
        else
            fotoDestaque.ImageUrl = ConfigurationManager.AppSettings["caminhoCDN"] + "fotos/naoExiste/pequena.jpg";
        #endregion

    }


    protected void ddlDepartamento_SelectedIndexChanged(object sender, EventArgs e)
    {
        var data = new dbCommerceDataContext();

        if (ddlDepartamento.SelectedIndex > 0)
        {
            trAssunto.Visible = true;
            ddlAssunto.Enabled = true;
            CarregarComboAssunto();

            var usuarios = (from c in data.tbChamadoDepartamentoUsuarios
                            join d in data.tbUsuarios on c.usuarioId equals d.usuarioId
                            where c.idDepartamento == int.Parse(ddlDepartamento.SelectedValue)
                            select d).Distinct().ToList();

            ddlUsuarioDepartamento.DataSource = usuarios;
            ddlUsuarioDepartamento.DataTextField = "usuarioNome";
            ddlUsuarioDepartamento.DataValueField = "usuarioId";
            ddlUsuarioDepartamento.DataBind();

            ddlUsuarioDepartamento.Items.Insert(0, new ListItem() { Text = "Qualquer usuário do departamento", Value = null });

        }
        else
        {
            trAssunto.Visible = false;
            trTarefa.Visible = false;
            ddlTarefa.Enabled = false;
            ddlAssunto.Enabled = false;

        }
    }

    private void CarregarComboAssunto()
    {
        var data = new dbCommerceDataContext();
        var departamento = Convert.ToInt32(ddlDepartamento.SelectedValue);

        var assuntos = (from c in data.tbChamadoDepartamentoAssuntoTarefas
                        where c.idDepartamento == departamento
                        select c.tbChamadoAssunto).ToList();

        ddlAssunto.DataSource = assuntos.Distinct().OrderBy(x => x.nomeChamadoAssunto);
        ddlAssunto.DataTextField = "nomeChamadoAssunto";
        ddlAssunto.DataValueField = "idChamadoAssunto";
        ddlAssunto.DataBind();

        ddlAssunto.Items.Insert(0, new ListItem() { Text = "Selecione", Value = "0" });
        ddlAssunto.SelectedIndex = 0;

        ddlAssunto.Enabled = true;
    }

    private void CarregarComboTarefa()
    {
        var data = new dbCommerceDataContext();
        var assunto = Convert.ToInt32(ddlAssunto.SelectedValue);
        var departamento = Convert.ToInt32(ddlDepartamento.SelectedValue);

        var Tarefas = (from c in data.tbChamadoDepartamentoAssuntoTarefas
                       where c.idDepartamento == departamento && c.idAssunto == assunto && c.idTarefa != null select c.tbChamadoTarefa).ToList();

        if (Tarefas.Count > 0)
        {
            Tarefas.Add(new tbChamadoTarefa() { idChamadoTarefa = 0, nomeChamadoTarefa = "Selecione" });

            ddlTarefa.DataSource = Tarefas.Distinct().OrderBy(x => x.idChamadoTarefa);
            ddlTarefa.DataTextField = "nomeCHamadoTarefa";
            ddlTarefa.DataValueField = "idChamadotarefa";
            ddlTarefa.DataBind();

            trTarefa.Visible = true;
            ddlTarefa.Enabled = true;
        }
        else
        {
            trTarefa.Visible = false;
            ddlTarefa.Enabled = false;
        }




    }

    protected void ddlTarefa_SelectedIndexChanged(object sender, EventArgs e)
    {
        txtChamadoTitulo.Text = string.Empty;

        if (ddlTarefa.SelectedIndex > 0)
            txtChamadoTitulo.Text = ddlDepartamento.SelectedItem.Text + " - " + ddlAssunto.SelectedItem.Text + " - " + ddlTarefa.SelectedItem.Text;
        else if (ddlDepartamento.SelectedIndex > 0 && ddlAssunto.SelectedIndex > 0)
            txtChamadoTitulo.Text = ddlDepartamento.SelectedItem.Text + " - " + ddlAssunto.SelectedItem.Text;
    }

    private void CarregarComboCategoria()
    {
        var data = new dbCommerceDataContext();
        var Categorias = data.tbChamadoCategorias.ToList();

        Categorias.Add(new tbChamadoCategoria() { idChamadoCategoria = 0, nomeChamadoCategoria = "Selecione" });

        ddlAssunto.DataSource = Categorias.OrderBy(x => x.idChamadoCategoria);
        ddlAssunto.DataTextField = "nomeChamadoCategoria";
        ddlAssunto.DataValueField = "idChamadoCategoria";
        ddlAssunto.DataBind();
    }

    protected void ddlAssunto_SelectedIndexChanged(object sender, EventArgs e)
    {

        CarregarComboTarefa();

        if (ddlTarefa.SelectedIndex > 0)
            txtChamadoTitulo.Text = ddlDepartamento.SelectedItem.Text + " - " + ddlAssunto.SelectedItem.Text + " - " + ddlTarefa.SelectedItem.Text;
        else if (ddlDepartamento.SelectedIndex > 0 && ddlAssunto.SelectedIndex > 0)
            txtChamadoTitulo.Text = ddlDepartamento.SelectedItem.Text + " - " + ddlAssunto.SelectedItem.Text;
    }
}
