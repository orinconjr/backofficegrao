﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxTabControl;
using DevExpress.Web.ASPxEditors;
using DevExpress.Data;
using DevExpress.Web.ASPxUploadControl;
using System.IO;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.Reflection;
using Amazon.S3;
using Amazon.S3.Model;
using DevExpress.Web.ASPxPivotGrid;

public partial class admin_biProdutosCategoria : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        bool datas = true;
        if (txtDataInicial.Text == "" | txtDataFinal.Text == "" && treeCategorias.SelectedValue != "")
        {
            datas = false;
        }
        if (!Page.IsPostBack)
        {
            PopulateRootLevel();
        }
        if (datas) fillGrid(false);
    }
    private void PopulateRootLevel()
    {
        PopulateNodes(rnCategorias.categoriasSelecionaNivelPai().Tables[0], treeCategorias.Nodes);
    }

    private void PopulateSubLevel(int categoriaPaiId, TreeNode parentNode)
    {
        PopulateNodes(rnCategorias.categoriasSelecionaNivelFilho(categoriaPaiId).Tables[0], parentNode.ChildNodes);
    }

    protected void TreeView1_TreeNodePopulate(object sender, TreeNodeEventArgs e)
    {
        PopulateSubLevel(Int32.Parse(e.Node.Value), e.Node);
    }

    private void PopulateNodes(DataTable dt, TreeNodeCollection nodes)
    {
        foreach (DataRow dr in dt.Rows)
        {
            TreeNode tn = new TreeNode();
            tn.Text = dr["categoriaNome"].ToString();
            tn.Value = dr["categoriaId"].ToString();

            nodes.Add(tn);

            //If node has child nodes, then enable on-demand populating
            tn.PopulateOnDemand = ((int)(dr["childnodecount"]) > 0);
        }
    }

    private void fillGrid(bool rebind)
    {
        var dataInicial1 = DateTime.Now;
        var dataFinal1 = DateTime.Now;
        DateTime.TryParse(txtDataInicial.Text, out dataInicial1);
        DateTime.TryParse(txtDataFinal.Text, out dataFinal1);
        

        dataFinal1 = dataFinal1.AddDays(1);
        var data = new dbCommerceDataContext();
        using (
            var txn = new System.Transactions.TransactionScope(
                    System.Transactions.TransactionScopeOption.Required,
                    new System.Transactions.TransactionOptions
                    {
                        IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                    }))                           
        {
            int categoriaId = Convert.ToInt32(treeCategorias.SelectedValue);

            var relatorioGeralProdutos = (from c in data.tbItensPedidos
                                         join d in data.tbJuncaoProdutoCategorias on c.produtoId equals d.produtoId
                                         join e in data.tbPedidos on c.tbPedido.clienteId equals e.clienteId
                                         join f in data.tbItensPedidos on e.pedidoId equals f.pedidoId
                                  where (c.tbPedido.dataHoraDoPedido >= dataInicial1 && c.tbPedido.dataHoraDoPedido <= dataFinal1)
                                  select new
                                  {
                                      pedidoIdOriginal = c.pedidoId,
                                      itemPedidoIdOriginal = c.itemPedidoId,
                                      dataHoraOriginal = c.tbPedido.dataHoraDoPedido,
                                      categoriaIdOriginal = d.categoriaId,
                                      valorOriginal = c.itemValor,
                                      itemQuantidadeOriginal = c.itemQuantidade,
                                      f.itemPedidoId,
                                      f.pedidoId,
                                      f.produtoId,
                                      f.tbProduto.produtoNome,
                                      categoria =
                                         f.tbProduto.tbJuncaoProdutoCategorias.FirstOrDefault(x => x.tbProdutoCategoria.categoriaPaiId == 0 && x.tbProdutoCategoria.exibirSite == true) ==
                                         null
                                             ? "Sem Categoria"
                                             : f.tbProduto.tbJuncaoProdutoCategorias.FirstOrDefault(
                                                 x => x.tbProdutoCategoria.categoriaPaiId == 0 && x.tbProdutoCategoria.exibirSite == true)
                                                 .tbProdutoCategoria.categoriaNomeExibicao,
                                      f.tbProduto.tbProdutoFornecedor.fornecedorNome,
                                      f.valorCusto,
                                      f.itemValor,
                                      itemQuantidade = Convert.ToInt32(f.itemQuantidade),
                                      dias = Convert.ToInt32(((DateTime)f.tbPedido.dataHoraDoPedido - (DateTime)c.tbPedido.dataHoraDoPedido).TotalDays),
                                      c.tbPedido.clienteId
                                  }).Where(x => x.categoriaIdOriginal == categoriaId).ToList();

            var itensCategoria = (from c in relatorioGeralProdutos
                              where c.pedidoIdOriginal == c.pedidoId && c.itemPedidoIdOriginal == c.itemPedidoId && c.categoriaIdOriginal == categoriaId
                              select new
                              {
                                  c.clienteId,
                                  c.pedidoIdOriginal,
                                  c.valorOriginal,
                                  c.itemPedidoIdOriginal, 
                                  c.itemQuantidadeOriginal
                              }).Distinct();

            int totalItens = itensCategoria.Select(x => x.clienteId).Distinct().Count();
            var valorItens = itensCategoria.Sum(x => x.valorOriginal);

            var relatorioCompras = (from c in relatorioGeralProdutos                                  
                                  group c by new { c.categoria }  into produtosCategoria
                                  select new
                                  {
                                      categoria = produtosCategoria.First().categoria,
                                      quantidadeDepois = produtosCategoria.Where(x => x.itemPedidoId != x.itemPedidoIdOriginal && x.pedidoId > x.pedidoIdOriginal).Select(x => x.clienteId).Distinct().Count(),
                                      quantidadeAntes = produtosCategoria.Where(x => x.itemPedidoId != x.itemPedidoIdOriginal && x.pedidoId < x.pedidoIdOriginal).Select(x => x.clienteId).Distinct().Count(),
                                      quantidadeMesmoPedido = produtosCategoria.Where(x => x.itemPedidoId != x.itemPedidoIdOriginal && x.pedidoId == x.pedidoIdOriginal).Select(x => x.clienteId).Distinct().Count(),
                                      valorDepois = produtosCategoria.Where(x => x.itemPedidoId != x.itemPedidoIdOriginal && x.pedidoId > x.pedidoIdOriginal).Select(x => new { x.itemValor, x.itemPedidoId }).Distinct().Sum(x => x.itemValor),
                                      valorAntes = produtosCategoria.Where(x => x.itemPedidoId != x.itemPedidoIdOriginal && x.pedidoId < x.pedidoIdOriginal).Select(x => new { x.itemValor, x.itemPedidoId }).Distinct().Sum(x => x.itemValor),
                                      valorMesmoPedido = produtosCategoria.Where(x => x.itemPedidoId != x.itemPedidoIdOriginal && x.pedidoId == x.pedidoIdOriginal).Select(x => new { x.itemValor, x.itemPedidoId }).Distinct().Sum(x => x.itemValor)
                                  });

            var relatorioFinal = (from c in relatorioCompras
                                  select new
                                  {
                                      c.categoria,
                                      totalCategoria = c.quantidadeDepois + c.quantidadeMesmoPedido + c.quantidadeAntes,
                                      totalCategoriaValor = c.valorDepois + c.valorAntes + c.valorMesmoPedido,
                                      c.quantidadeDepois,
                                      c.quantidadeAntes,
                                      c.quantidadeMesmoPedido,
                                      c.valorAntes,
                                      c.valorDepois,
                                      c.valorMesmoPedido,
                                      totalCategoriaSelecionada = totalItens
                                  });
            grd.DataSource = relatorioFinal;
            if ((!Page.IsPostBack && !Page.IsCallback) | rebind)
            {
                grd.DataBind();
            }
        }
    }
    protected void btnPequisar_Click(object sender, ImageClickEventArgs e)
    {
        fillGrid(true);
    }

    protected void chkHoraAtual_OnCheckedChanged(object sender, EventArgs e)
    {
        fillGrid(true);
    }



    protected void grd_CustomUnboundFieldData(object sender, CustomFieldDataEventArgs e)
    {
        //if (e.Field.FieldName == "variacaoQuantidade")
        //{
        //    string nome = e.GetListSourceColumnValue("produtoNome").ToString();
        //    if(nome.ToLower().Contains("orthoflex"))
        //    {
        //        var teste = "";
        //    }
        //    var source1 = e.GetListSourceColumnValue(e.ListSourceRowIndex, "itemQuantidadePeriodo1");
        //    var source2 = e.GetListSourceColumnValue(e.ListSourceRowIndex, "itemQuantidadePeriodo2");
        //    decimal quantidade1 = Convert.ToDecimal(source1);
        //    decimal quantidade2 = Convert.ToDecimal(source2);
        //    if (quantidade1 == 0) { e.Value = 100; }
        //    else
        //    {
        //        e.Value = Math.Round((((quantidade2 * 100) / quantidade1) - 100), 2);
        //    }
        //}
    }
}