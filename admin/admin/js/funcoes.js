﻿  function soNumero(evt)
  {
     var charCode = (evt.which) ? evt.which : event.keyCode
     if (charCode > 31 && (charCode < 48 || charCode > 57))
     {
        return false;
     }
     return true;
  }
  
    function soNumeroVirgulaPonto(evt)
  {
     var charCode = (evt.which) ? evt.which : event.keyCode
     if (charCode > 31 && (charCode < 44 || charCode > 57))
        return false;

     return true;

  }
  
  function FP_openNewWindow(w,h,nav,loc,sts,menu,scroll,resize,name,url) {//v1.0
 var windowProperties=''; if(nav==false) windowProperties+='toolbar=no,'; else
  windowProperties+='toolbar=yes,'; if(loc==false) windowProperties+='location=no,'; 
 else windowProperties+='location=yes,'; if(sts==false) windowProperties+='status=no,';
 else windowProperties+='status=yes,'; if(menu==false) windowProperties+='menubar=no,';
 else windowProperties+='menubar=yes,'; if(scroll==false) windowProperties+='scrollbars=no,';
 else windowProperties+='scrollbars=yes,'; if(resize==false) windowProperties+='resizable=no,';
 else windowProperties+='resizable=yes,'; if(w!="") windowProperties+='width='+w+',';
 if(h!="") windowProperties+='height='+h; if(windowProperties!="") { 
  if( windowProperties.charAt(windowProperties.length-1)==',') 
   windowProperties=windowProperties.substring(0,windowProperties.length-1); } 
 window.open(url,name,windowProperties);
}
  
  function moeda(fld, milSep, decSep, e) {
    var sep = 0;
    var key = '';
    var i = j = 0;
    var len = len2 = 0;
    var strCheck = '0123456789';
    var aux = aux2 = '';
    var whichCode = (window.Event) ? e.which : e.keyCode;
    if (whichCode == 13) return true;  // Enter
    if (whichCode == 8) return true;  // Delete (Bug fixed)
    key = String.fromCharCode(whichCode);  // Get key value from key code
    if (strCheck.indexOf(key) == -1) return false;  // Not a valid key
    len = fld.value.length;
    for(i = 0; i < len; i++)
    if ((fld.value.charAt(i) != '0') && (fld.value.charAt(i) != decSep)) break;
    aux = '';
    for(; i < len; i++)
    if (strCheck.indexOf(fld.value.charAt(i))!=-1) aux += fld.value.charAt(i);
    aux += key;
    len = aux.length;
    if (len == 0) fld.value = '';
    if (len == 1) fld.value = '0'+ decSep + '0' + aux;
    if (len == 2) fld.value = '0'+ decSep + aux;
    if (len > 2) {
    aux2 = '';
    for (j = 0, i = len - 3; i >= 0; i--) {
    if (j == 3) {
    aux2 += milSep;
    j = 0;
    }
    aux2 += aux.charAt(i);
    j++;
    }
    fld.value = '';
    len2 = aux2.length;
    for (i = len2 - 1; i >= 0; i--)
    fld.value += aux2.charAt(i);
    fld.value += decSep + aux.substr(len - 2, len);
    }
    return false;
}

function Mascaras(formato, objeto){
	if (window.event.keyCode >= 48 && window.event.keyCode <= 57){
		var campo = document.getElementById(objeto);
		
		if (formato == 'DATA'){
			separador = '/';
			conjunto1 = 2;
			conjunto2 = 5;
			if (campo.value.length == conjunto1)
				campo.value = campo.value + separador;

			if (campo.value.length == conjunto2)
				campo.value = campo.value + separador;
		}
	}else
		window.event.keyCode = 0;
}
  
  function MM_formtCep(e,src,mask) {
    if(window.event) { _TXT = e.keyCode; } 
    else if(e.which) { _TXT = e.which; }
    if(_TXT > 47 && _TXT < 58) { 
 var i = src.value.length; var saida = mask.substring(0,1); var texto = mask.substring(i)
 if (texto.substring(0,1) != saida) { src.value += texto.substring(0,1); } 
    return true; } else { if (_TXT != 8) { return false; } 
 else { return true; }
    }
}

// Validação de CPF e CNPJ

function valida_CPFCNPJ(oSrc,args){

if (args.Value.length == 11){

valida_CPF(oSrc,args);

}else if(args.Value.length == 11){

valida_CNPJ(oSrc, args);

}else{

return args.IsValid = false;

}

}

//Validação de CPF

function valida_CPF(oSrc,args){

s = args.Value;

//args.isValid = (s >= 3);

//document.write(oSrc.Value + ',' + args.Value);

if (isNaN(s)) {

return args.IsValid = false;

}

var i;

var c = s.substr(0,9);

var dv = s.substr(9,2);

var d1 = 0;

for (i = 0; i < 9; i++) {

d1 += c.charAt(i)*(10-i);

}

if (d1 == 0){

return args.IsValid = false;

} 

d1 = 11 - (d1 % 11);

if (d1 > 9) d1 = 0; 

if (dv.charAt(0) != d1) {

return args.IsValid = false; 

}

d1 *= 2;

for (i = 0; i < 9; i++) {

d1 += c.charAt(i)*(11-i);

}

d1 = 11 - (d1 % 11);

if (d1 > 9) d1 = 0;

if (dv.charAt(1) != d1) {

return args.IsValid = false;

}

return args.IsValid = true;

} 

//Validação de CNPJ

function valida_CNPJ(oSrc, args){

s = args.Value;

if (isNaN(s)) {

return args.IsValid = false;

}

var i;

var c = s.substr(0,12);

var dv = s.substr(12,2);

var d1 = 0;

for (i = 0; i <12; i++){

d1 += c.charAt(11-i)*(2+(i % 8));

}

if (d1 == 0) 

return args.IsValid = false;

d1 = 11 - (d1 % 11);

if (d1 > 9) d1 = 0;

if (dv.charAt(0) != d1){

return args.IsValid = false;

}

d1 *= 2;

for (i = 0; i < 12; i++){

d1 += c.charAt(11-i)*(2+((i+1) % 8));

}

d1 = 11 - (d1 % 11);

if (d1 > 9) 

d1 = 0;

if (dv.charAt(1) != d1){

return args.IsValid = false;

}

return args.IsValid = true;

} 


function

client_OnTreeNodeChecked(event) 
{


var TreeNode = event.srcElement || event.target ; 

if (TreeNode.tagName == "INPUT" && TreeNode.type == "checkbox") 
{ 


if(TreeNode.checked) 
{ 

uncheckOthers(TreeNode.id);

}

  
} 

}

function

uncheckOthers(id) 
{


var elements = document.getElementsByTagName('input'); 

// loop through all input elements in form 

for(var i = 0; i < elements.length; i++) 
{


if(elements.item(i).type == "checkbox") 
{ 


if(elements.item(i).id!=id) 
{

elements.item(i).checked=

false; 
}

} 

}

}
//checa radiobutton dentro do datalist
function clickit() {
        var doc=document.all;
        var el=event.srcElement;
        if(el.tagName=="INPUT"&&el.type.toLowerCase()=="radio")
        {
                for(i=0;i<doc.length;i++)
                {
                        if(doc[i].tagName=="INPUT"&&doc[i].type.toLowerCase()=="radio")
                        {
                                doc[i].checked=false;
                        }
                }
        }
        el.checked=true;
}


function enterPassaFoco(event, campo) {
    var charCode = (event.which) ? event.which : event.keyCode;
    if (charCode == 13) {
        if (event.preventDefault) {
            event.preventDefault();
        } else if (event.stopPropagation) {
            event.stopPropagation();
        } else {
            event.returnValue = false;
        }
        document.getElementById(campo).focus();
    } 
}

function soNumeroDigitoPassaFoco(event, campo) {
    var charCode = (event.which) ? event.which : event.keyCode;
    if (charCode == 13) {
        if (event.preventDefault) {
            event.preventDefault();
        } else if (event.stopPropagation) {
            event.stopPropagation();
        } else {
            event.returnValue = false;
        }
        document.getElementById(campo).focus();
    } else {
        if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 45) {
            return false;
        }
        return true;
    }
    return true;
}

function soNumeroDigito(event) {
    var charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 45) {
        return false;
    }
    return true;
}

function soNumeroDigitoBarra(event) {
    var charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 45 && charCode != 191) {
        return false;
    }
    return true;
}
