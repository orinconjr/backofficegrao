﻿var categoriaList = [];
var categoriasEscolhidas = [];

function getAllCategories() {
    loadAllCategories(function () {
        renderCategoriesTable();
    });
}

function loadAllCategories(callback) {
    if (categoriaList.length > 0) {
        callback();
        return false;
    }

    $.ajax({
        url: "https://apiv2.graodegente.com.br/v1/categoriaselasticsearch/",
        context: document.body
    }).success(function (data) {
        $.each(data, function (idx, item) {
            categoriaList.push({ id: item.id, name: item.nome });
        });

        var arrPreSelected = $('#hdnCategorias').val().split(',');

        if (arrPreSelected.length > 0) {
            $.each(arrPreSelected, function (idx, item) {
                if (!isNaN(item) && item != "") {
                    var itemFilter = categoriaList.filter(function (a) {
                        return a.id == item;
                    });
                    //categoriasEscolhidas.push({ id: itemFilter[0].id, name: itemFilter[0].name });
                }
            });
        }
        callback();
    });
}

function renderCategoriesTable() {
    var list = $('.selectedCategories');
    $(list).find('li').remove();

    var cat = '0';
    $('#hdnCategorias').val('');
    $.each(categoriasEscolhidas, function (idx, item) {
        $(list).append($('<li><input type="button" value="X" title="Tirar da lista" onClick="delListCategories(' + item.id + ')"/> ' + item.id + ' - ' + item.name + '</li>'));
        cat += ',' + item.id;
    });
    cat = cat.replace('0,', '');
    $('#hdnCategorias').val(cat);
}

function initBuscaCategoria(){
    $("#txtBusca").autocomplete({
        minLength: 2,
        source: function (request, response) {
            response($.map(categoriaList.filter(function (a) {
                return (a.name ? a.name : '').toLowerCase().indexOf(request.term.toLowerCase()) > -1 || a.id == request.term;
            }), function (value, key) {
                return {
                    label: value.name,
                    value: value.id
                }
            }));
        },
        select: function (event, ui) {
            setTimeout(function () {
                $('#txtBusca').val('');
            }, 500);

            var existe = categoriasEscolhidas.find(function (a) {
                return a.id == ui.item.value
            });

            if (!existe) {
                categoriasEscolhidas.push({ id: ui.item.value, name: ui.item.label });
                renderCategoriesTable();
            }

        }
    });
}

function delListCategories(id) {
    var idx = categoriasEscolhidas.findIndex(function (a) {
        return a.id == id;
    });
    categoriasEscolhidas.splice(idx, 1);

    renderCategoriesTable();
}
