﻿var dadosCach = new Array(17);

var DashboardProdutos = {
    init: function (fornecedor, categoria, marca) {

        this.fornecedor = fornecedor;
        this.categoria = categoria;
        this.marca = marca;

        this.ForaDeLinha();
        this.ForaDeLinhaEstoque0();
        this.ForaDeLinhaEstoqueBaixo();
        this.EstoqueBaixo();
        this.ProdutosComEstoque0OuNegativo();
        this.ProdutoDesativadoComEstoqueMaiorQue0();
        this.ProdutoAtivoSemCategoria();
        this.CategoriaAtivaSemProduto();
        this.CategoriaAtivaSemDescricao();
        this.ProdutoAtivoSemFoto();
        this.ProdutoMostrandoComoEsgotado();
        this.ProdutoAtivoSemDescricao();
        this.ProdutoAtivoSemColecao();
        this.ProdutoAtivoSemComposicao();
        this.ProdutoAtivoEmPromocao();
        this.FiltroSemProdutos();
        this.PrecoDeCustoMaiorQuePrecoVenda();
    },
    ForaDeLinha: function () {
        $.ajax({
            url: './ajaxDashboardProdutos.asmx/ForaDeLinha',
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            data: '{ fornecedor: ' + this.fornecedor + '}',
            beforeSend: function () {
                $("#divForaDeLinha").empty();
                $("#divForaDeLinha").addClass("loadingInformation");
            },
            success: function (result) {
                //alert(result);
                $("#divForaDeLinha").removeClass("loadingInformation");
                $("#divForaDeLinha").text($.parseJSON(result.d));
                dadosCach[0] = $.parseJSON(result.d);
            },
            error: function (err, status, errorThrown) {
                console.log(errorThrown);
            }

        });
    },
    ForaDeLinhaEstoque0: function () {
        $.ajax({
            url: './ajaxDashboardProdutos.asmx/ForaDeLinhaEstoque0',
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: '{ fornecedor: ' + this.fornecedor + ' ,categoria: ' + this.categoria + ',marca: ' + this.marca + '}',
            beforeSend: function () {
                $("#divForaDeLinhaEstoque0").empty();
                $("#divForaDeLinhaEstoque0").addClass("loadingInformation");
            },
            success: function (result) {
                //alert(result);
                $("#divForaDeLinhaEstoque0").removeClass("loadingInformation");
                $("#divForaDeLinhaEstoque0").text($.parseJSON(result.d));
                dadosCach[1] = $.parseJSON(result.d);
            },
            error: function (err, status, errorThrown) {
                console.log(errorThrown);
            }

        });
    },
    ForaDeLinhaEstoqueBaixo: function () {
        $.ajax({
            url: './ajaxDashboardProdutos.asmx/ForaDeLinhaEstoqueBaixo',
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: '{ fornecedor: ' + this.fornecedor + ' ,categoria: ' + this.categoria + ',marca: ' + this.marca + '}',
            beforeSend: function () {
                $("#divForaDeLinhaEstoqueBaixo").empty();
                $("#divForaDeLinhaEstoqueBaixo").addClass("loadingInformation");
            },
            success: function (result) {
                //alert(result);
                $("#divForaDeLinhaEstoqueBaixo").removeClass("loadingInformation");
                $("#divForaDeLinhaEstoqueBaixo").text($.parseJSON(result.d));
                dadosCach[2] = $.parseJSON(result.d);
            },
            error: function (err, status, errorThrown) {
                console.log(errorThrown);
            }

        });
    },
    EstoqueBaixo: function () {
        $.ajax({
            url: './ajaxDashboardProdutos.asmx/EstoqueBaixo',
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: '{ fornecedor: ' + this.fornecedor + ' ,categoria: ' + this.categoria + ',marca: ' + this.marca + '}',
            beforeSend: function () {
                $("#divEstoqueBaixo").empty();
                $("#divEstoqueBaixo").addClass("loadingInformation");
            },
            success: function (result) {
                //alert(result);
                $("#divEstoqueBaixo").removeClass("loadingInformation");
                $("#divEstoqueBaixo").text($.parseJSON(result.d));
                dadosCach[3] = $.parseJSON(result.d);
            },
            error: function (err, status, errorThrown) {
                console.log(errorThrown);
            }

        });
    },
    ProdutoDesativadoComEstoqueMaiorQue0: function () {
        $.ajax({
            url: './ajaxDashboardProdutos.asmx/ProdutoDesativadoComEstoqueMaiorQue0',
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: '{ fornecedor: ' + this.fornecedor + ' ,categoria: ' + this.categoria + ',marca: ' + this.marca + '}',
            beforeSend: function () {
                $("#divProdutosDesativadosComEstoqueMaiorQue0").empty();
                $("#divProdutosDesativadosComEstoqueMaiorQue0").addClass("loadingInformation");
            },
            success: function (result) {
                //alert(result);
                $("#divProdutosDesativadosComEstoqueMaiorQue0").removeClass("loadingInformation");
                $("#divProdutosDesativadosComEstoqueMaiorQue0").text($.parseJSON(result.d));
                dadosCach[4] = $.parseJSON(result.d);
            },
            error: function (err, status, errorThrown) {
                console.log(errorThrown);
            }

        });
    },
    ProdutoAtivoSemCategoria: function () {
        $.ajax({
            url: './ajaxDashboardProdutos.asmx/ProdutoAtivoSemCategoria',
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: '{ fornecedor: ' + this.fornecedor + ' ,categoria: ' + this.categoria + ',marca: ' + this.marca + '}',
            beforeSend: function () {
                $("#divProdutoAtivoSemCategoria").empty();
                $("#divProdutoAtivoSemCategoria").addClass("loadingInformation");
            },
            success: function (result) {
                //alert(result);
                $("#divProdutoAtivoSemCategoria").removeClass("loadingInformation");
                $("#divProdutoAtivoSemCategoria").text($.parseJSON(result.d));
                dadosCach[5] = $.parseJSON(result.d);
            },
            error: function (err, status, errorThrown) {
                console.log(errorThrown);
            }

        });
    },
    CategoriaAtivaSemProduto: function () {
        $.ajax({
            url: './ajaxDashboardProdutos.asmx/CategoriaAtivaSemProduto',
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: '{ fornecedor: ' + this.fornecedor + ' ,categoria: ' + this.categoria + ',marca: ' + this.marca + '}',
            beforeSend: function () {
                $("#divCategoriaAtivaSemProduto").empty();
                $("#divCategoriaAtivaSemProduto").addClass("loadingInformation");
            },
            success: function (result) {
                //alert(result);
                $("#divCategoriaAtivaSemProduto").removeClass("loadingInformation");
                $("#divCategoriaAtivaSemProduto").text($.parseJSON(result.d));
                dadosCach[6] = $.parseJSON(result.d);
            },
            error: function (err, status, errorThrown) {
                console.log(errorThrown);
            }

        });
    },
    ProdutoAtivoSemFoto: function () {
        $.ajax({
            url: './ajaxDashboardProdutos.asmx/ProdutoAtivoSemFoto',
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: '{ fornecedor: ' + this.fornecedor + ' ,categoria: ' + this.categoria + ',marca: ' + this.marca + '}',
            beforeSend: function () {
                $("#divProdutoAtivoSemFoto").empty();
                $("#divProdutoAtivoSemFoto").addClass("loadingInformation");
            },
            success: function (result) {
                //alert(result);
                $("#divProdutoAtivoSemFoto").removeClass("loadingInformation");
                $("#divProdutoAtivoSemFoto").text($.parseJSON(result.d));
                dadosCach[7] = $.parseJSON(result.d);
            },
            error: function (err, status, errorThrown) {
                console.log(errorThrown);
            }

        });
    },
    ProdutoMostrandoComoEsgotado: function () {
        $.ajax({
            url: './ajaxDashboardProdutos.asmx/ProdutoMostrandoComoEsgotado',
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: '{ fornecedor: ' + this.fornecedor + ' ,categoria: ' + this.categoria + ',marca: ' + this.marca + '}',
            beforeSend: function () {
                $("#divProdutosAtivosComEstoque0").empty();
                $("#divProdutosAtivosComEstoque0").addClass("loadingInformation");
            },
            success: function (result) {
                //alert(result);
                $("#divProdutosAtivosComEstoque0").removeClass("loadingInformation");
                $("#divProdutosAtivosComEstoque0").text($.parseJSON(result.d));
                dadosCach[8] = $.parseJSON(result.d);
            },
            error: function (err, status, errorThrown) {
                console.log(errorThrown);
            }

        });
    },
    ProdutoAtivoSemDescricao: function () {
        $.ajax({
            url: './ajaxDashboardProdutos.asmx/ProdutoAtivoSemDescricao',
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: '{ fornecedor: ' + this.fornecedor + ' ,categoria: ' + this.categoria + ',marca: ' + this.marca + '}',
            beforeSend: function () {
                $("#divProdutoAtivoSemDescricao").empty();
                $("#divProdutoAtivoSemDescricao").addClass("loadingInformation");
            },
            success: function (result) {
                //alert(result);
                $("#divProdutoAtivoSemDescricao").removeClass("loadingInformation");
                $("#divProdutoAtivoSemDescricao").text($.parseJSON(result.d));
                dadosCach[9] = $.parseJSON(result.d);
            },
            error: function (err, status, errorThrown) {
                console.log(errorThrown);
            }

        });
    },
    ProdutoAtivoSemColecao: function () {
        $.ajax({
            url: './ajaxDashboardProdutos.asmx/ProdutoAtivoSemColecao',
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: '{ fornecedor: ' + this.fornecedor + ' ,categoria: ' + this.categoria + ',marca: ' + this.marca + '}',
            beforeSend: function () {
                $("#divProdutoAtivoSemColecao").empty();
                $("#divProdutoAtivoSemColecao").addClass("loadingInformation");
            },
            success: function (result) {
                //alert(result);
                $("#divProdutoAtivoSemColecao").removeClass("loadingInformation");
                $("#divProdutoAtivoSemColecao").text($.parseJSON(result.d));
                dadosCach[10] = $.parseJSON(result.d);
            },
            error: function (err, status, errorThrown) {
                console.log(errorThrown);
            }

        });
    },
    ProdutoAtivoSemComposicao: function () {
        $.ajax({
            url: './ajaxDashboardProdutos.asmx/ProdutoAtivoSemComposicao',
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: '{ fornecedor: ' + this.fornecedor + ' ,categoria: ' + this.categoria + ',marca: ' + this.marca + '}',
            beforeSend: function () {
                $("#divProdutoAtivoSemComposicao").empty();
                $("#divProdutoAtivoSemComposicao").addClass("loadingInformation");
            },
            success: function (result) {
                //alert(result);
                $("#divProdutoAtivoSemComposicao").removeClass("loadingInformation");
                $("#divProdutoAtivoSemComposicao").text($.parseJSON(result.d));
                dadosCach[11] = $.parseJSON(result.d);
            },
            error: function (err, status, errorThrown) {
                console.log(errorThrown);
            }

        });
    },
    ProdutoAtivoEmPromocao: function () {
        $.ajax({
            url: './ajaxDashboardProdutos.asmx/ProdutoAtivoEmPromocao',
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: '{ fornecedor: ' + this.fornecedor + ' ,categoria: ' + this.categoria + ',marca: ' + this.marca + '}',
            beforeSend: function () {
                $("#divProdutoAtivoEmPromocao").empty();
                $("#divProdutoAtivoEmPromocao").addClass("loadingInformation");
            },
            success: function (result) {
                //alert(result);
                $("#divProdutoAtivoEmPromocao").removeClass("loadingInformation");
                $("#divProdutoAtivoEmPromocao").text($.parseJSON(result.d));
                dadosCach[12] = $.parseJSON(result.d);
            },
            error: function (err, status, errorThrown) {
                console.log(errorThrown);
            }

        });
    },
    FiltroSemProdutos: function () {
        $.ajax({
            url: './ajaxDashboardProdutos.asmx/FiltroSemProdutos',
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: '{ fornecedor: ' + this.fornecedor + ' ,categoria: ' + this.categoria + ',marca: ' + this.marca + '}',
            beforeSend: function () {
                $("#divFiltroSemProduto").empty();
                $("#divFiltroSemProduto").addClass("loadingInformation");
            },
            success: function (result) {
                //alert(result);
                $("#divFiltroSemProduto").removeClass("loadingInformation");
                $("#divFiltroSemProduto").text($.parseJSON(result.d));
                dadosCach[13] = $.parseJSON(result.d);
            },
            error: function (err, status, errorThrown) {
                console.log(errorThrown);
            }

        });
    },
    PrecoDeCustoMaiorQuePrecoVenda: function () {
        $.ajax({
            url: './ajaxDashboardProdutos.asmx/PrecoDeCustoMaiorQuePrecoVenda',
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: '{ fornecedor: ' + this.fornecedor + ' ,categoria: ' + this.categoria + ',marca: ' + this.marca + '}',
            beforeSend: function () {
                $("#divPrecoDeCustoMaiorQuePrecoVenda").empty();
                $("#divPrecoDeCustoMaiorQuePrecoVenda").addClass("loadingInformation");
            },
            success: function (result) {
                //alert(result);
                $("#divPrecoDeCustoMaiorQuePrecoVenda").removeClass("loadingInformation");
                $("#divPrecoDeCustoMaiorQuePrecoVenda").text($.parseJSON(result.d));
                dadosCach[14] = $.parseJSON(result.d);
            },
            error: function (err, status, errorThrown) {
                console.log(errorThrown);
            }

        });
    },
    CategoriaAtivaSemDescricao: function () {
        $.ajax({
            url: './ajaxDashboardProdutos.asmx/CategoriaAtivaSemDescricao',
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: '{}',
            beforeSend: function () {
                $("#divCategoriaAtivaSemDescricao").empty();
                $("#divCategoriaAtivaSemDescricao").addClass("loadingInformation");
            },
            success: function (result) {
                //alert(result);
                $("#divCategoriaAtivaSemDescricao").removeClass("loadingInformation");
                $("#divCategoriaAtivaSemDescricao").text($.parseJSON(result.d));
                dadosCach[15] = $.parseJSON(result.d);
            },
            error: function (err, status, errorThrown) {
                console.log(errorThrown);
            }

        });
    },
    ProdutosComEstoque0OuNegativo: function() {
        $.ajax({
            url: './ajaxDashboardProdutos.asmx/ProdutosComEstoque0OuNegativo',
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: '{ fornecedor: ' + this.fornecedor + ' ,categoria: ' + this.categoria + ',marca: ' + this.marca + '}',
            beforeSend: function () {
                $("#divEstoque0OuNegativo").empty();
                $("#divEstoque0OuNegativo").addClass("loadingInformation");
            },
            success: function (result) {
                //alert(result);
                $("#divEstoque0OuNegativo").removeClass("loadingInformation");
                $("#divEstoque0OuNegativo").text($.parseJSON(result.d));
                dadosCach[16] = $.parseJSON(result.d);
            },
            error: function (err, status, errorThrown) {
                console.log(errorThrown);
            }

        });
    },
    MarcasCategoriasPorFornecedor: function (fornecedorSelecionado) {

        $.ajax({
            url: './ajaxDashboardProdutos.asmx/MarcasPorFornecedor',
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: '{ fornecedor: ' + fornecedorSelecionado + '}',
            beforeSend: function () {
            },
            success: function (result) {

                var options = $("[id$=ddlMarca]");
                $(options).empty();

                options.append($("<option />").val("0").text("Todas as Marcas"));

                $.each($.parseJSON(result.d), function (key, value) {
                    options.append('<option value=' + value.marcaId + '>' + value.marcaNome + '</option>');
                });
            },
            error: function (err, status, errorThrown) {
                console.log(errorThrown);
            }

        });

        $.ajax({
            url: './ajaxDashboardProdutos.asmx/CategoriasPorFornecedor',
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: '{ fornecedor: ' + fornecedorSelecionado + '}',
            beforeSend: function () {
            },
            success: function (result) {

                var options = $("[id$=ddlCategoria]");
                $(options).empty();

                options.append($("<option />").val("0").text("Todas as Categorias"));

                $.each($.parseJSON(result.d), function (key, value) {
                    options.append('<option value=' + value.categoriaId + '>' + value.categoriaNome + '</option>');
                });
            },
            error: function (err, status, errorThrown) {
                console.log(errorThrown);
            }
        });
    }
};

var DashboardProdutosDadosCash = {
    init: function () {
        this.RetornaDados();
    },
    RetornaDados: function () {
        $("#divForaDeLinha").text(dadosCach[0]);
        $("#divForaDeLinhaEstoque0").text(dadosCach[1]);
        $("#divForaDeLinhaEstoqueBaixo").text(dadosCach[2]);
        $("#divEstoqueBaixo").text(dadosCach[3]);
        $("#divProdutosDesativadosComEstoqueMaiorQue0").text(dadosCach[4]);
        $("#divProdutoAtivoSemCategoria").text(dadosCach[5]);
        $("#divCategoriaAtivaSemProduto").text(dadosCach[6]);
        $("#divProdutoAtivoSemFoto").text(dadosCach[7]);
        $("#divProdutosAtivosComEstoque0").text(dadosCach[8]);
        $("#divProdutoAtivoSemDescricao").text(dadosCach[9]);
        $("#divProdutoAtivoSemColecao").text(dadosCach[10]);
        $("#divProdutoAtivoSemComposicao").text(dadosCach[11]);
        $("#divProdutoAtivoEmPromocao").text(dadosCach[12]);
        $("#divFiltroSemProduto").text(dadosCach[13]);
        $("#divPrecoDeCustoMaiorQuePrecoVenda").text(dadosCach[14]);
        $("#divCategoriaAtivaSemDescricao").text(dadosCach[15]);
        $("#divEstoque0OuNegativo").text(dadosCach[16]);
    }
};

$(function () {
    //var dashboarProdutos = new DashboardProdutos();
    //dashboarProdutos.init();

    var obj = Object.create(DashboardProdutos);
    obj.init(0, 0, 0);

    $(document).on('change', '[id$=ddlFornecedor]', function () {
        //alert(this.value);
        obj.MarcasCategoriasPorFornecedor(this.value);
        obj.init(this.value, 0, 0);
    });

    $(document).on('change', '[id$=ddlCategoria]', function () {

        $('[id$=hfCategoriaId]').val(this.value);
        obj.init($('[id$=ddlFornecedor]').val(), this.value, 0);
    });
});

