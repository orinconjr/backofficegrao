﻿var produtosEscolhidos = [];

function renderProductsTable() {
    var list = $('.selectedProducts');
    $(list).find('li').remove();

    var cat = '0';
    $('#hdnProdutos').val('');
    $.each(produtosEscolhidos, function (idx, item) {
        $(list).append($('<li><input type="button" value="X" title="Tirar da lista" onClick="delListProducts(' + item.id + ')"/> ' + item.name + '</li>'));
        cat += ',' + item.id;
    });
    cat = cat.replace('0,', '');
    $('#hdnProdutos').val(cat);
}

function delListProducts(id) {
    var idx = produtosEscolhidos.findIndex(function (a) {
        return a.id == id;
    });
    produtosEscolhidos.splice(idx, 1);

    renderProductsTable();
}

function initBuscaProduto(){
    if ($("#txtBuscaProduto")) {
        $("#txtBuscaProduto").autocomplete({
            source: function (request, response) {
                console.log('x', request, response);
                $.getJSON(
                    "https://apiv2.graodegente.com.br/v1/ListaProdutosService?termobusca=" + request.term + "&categoriaids=&produtoids=&colecaoids=&pagina=1&filtros=&filtrovalor=&quantidade=6&ordem=",
                    function (data) {

                        if (!data.produtos) {
                            response(null);
                            return false;
                        }


                        response($.map(data.produtos.filter(function (a) {
                            return a;
                        }), function (value, key) {
                            return {
                                label: value.produtoId + ' - ' + value.produtoNome,
                                value: value.produtoId
                            }
                        }));
                    });
            },
            minLength: 2,
            select: function (event, ui) {
            
                setTimeout(function () {
                    $('#txtBuscaProduto').val('');
                }, 500);

                var existe = produtosEscolhidos.find(function (a) {
                    return a.id == ui.item.value
                });

                if (!existe) {
                    produtosEscolhidos.push({ id: ui.item.value, name: ui.item.label });
                    renderProductsTable();
                }
            }
        });
    }
    renderProductsTable();
}