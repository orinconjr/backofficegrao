﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="pedidoitens.aspx.cs" Inherits="admin_pedidoitens" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:DataList ID="dtlItensPedido" runat="server" CellPadding="0" 
                    DataSourceID="sqlItensPedido" onitemdatabound="dtlItensPedido_ItemDataBound" Width="700px">
                    <ItemTemplate>
                        <table cellpadding="0" cellspacing="0" class="rotulos" style="width: 100%;">
                            <tr>
                                <td style="width:81px; border: 1px solid;">
                                    <asp:Image ID="fotoDestaque" runat="server" Height="70px" Width="70px" />
                                </td>
                                <td style="padding-left: 15px; padding-right: 15px; border: 1px solid; width: 260px">
                                    <asp:HiddenField ID="txtItemPedidoId" runat="server" Value='<%# Bind("itemPedidoId") %>' />
                                    <asp:Label ID="lblProdutoNome" runat="server" Text='<%# Bind("produtoNome") %>'></asp:Label>
                                </td>
                                <td style="width: 145px; border: 1px solid;">
                                    <asp:Label ID="lblFornecedor" runat="server" Text='<%# Bind("fornecedorNome") %>'></asp:Label>
                                </td>
                                <td style="width: 60px; border: 1px solid;">
                                    <asp:Label ID="lblProdutoIdEmpresa" runat="server" Text='<%# Bind("produtoIdDaEmpresa") %>'></asp:Label>
                                </td>
                                <td  style="width: 60px; border: 1px solid;">
                                    <asp:Label ID="lblQuantidade" runat="server" 
                                        Text='<%# Bind("itemQuantidade") %>'></asp:Label> unidade
                                </td>
                                <td style="width: 60px; border: 1px solid;">
                                  <asp:Label ID="lblPrazoDeFabricacao" runat="server" Text='<%# Bind("prazoDeFabricacao") %>'></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </ItemTemplate>
                </asp:DataList>
        
        <asp:ObjectDataSource ID="sqlItensPedido" runat="server" 
            SelectMethod="itensPedidoSelecionaAdmin_PorPedidoId" TypeName="rnPedidos">
            <SelectParameters>
                <asp:QueryStringParameter Name="pedidoId" QueryStringField="pedidoId" Type="Int32" />
            </SelectParameters>
        </asp:ObjectDataSource>
    </div>
    </form>
</body>
</html>
