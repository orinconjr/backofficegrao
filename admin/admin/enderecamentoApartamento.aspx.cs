﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_enderecamentoApartamento : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            fillDdls();
            BindData();
        }
    }

    void fillDdls()
    {
        var data = new dbCommerceDataContext();
        var areas = (from c in data.tbEnderecamentoAreas
                       select c).ToList();
        ddlarea.DataValueField = "idEnderecamentoarea";
        ddlarea.DataTextField = "area";
        ddlarea.DataSource = areas;
        ddlarea.DataBind();

        //var predios = (from c in data.tbEnderecamentoPredios
        //               select c).ToList();
        //ddlpredio.DataValueField = "idEnderecamentoPredio";
        //ddlpredio.DataTextField = "predio";
        //ddlpredio.DataSource = predios;
        //ddlpredio.DataBind();


        //var data = new dbCommerceDataContext();
        //var ruas = (from c in data.tbEnderecamentoRuas
        //            select c).ToList();
        //ddlrua.DataValueField = "idEnderecamentoRua";
        //ddlrua.DataTextField = "rua";
        //ddlrua.DataSource = ruas;
        //ddlrua.DataBind();

        //var data2 = new dbCommerceDataContext();
        //var lados = (from c in data.tbEnderecamentoLados
        //             select c).ToList();

        //ddllado.DataValueField = "idEnderecamentoLado";
        //ddllado.DataTextField = "lado";
        //ddllado.DataSource = lados;
        //ddllado.DataBind();

    }
    void BindData()
    {
        //CsModulo modulo = new CsModulo();
        //DataTable table = new DataTable();
        var data = new dbCommerceDataContext();
        var apartamentos = (from c in data.tbEnderecamentoApartamentos
                      // where c.andar == "6"
                       select new
                       {
                           c.idEnderecamentoApartamento,
                           c.tbEnderecamentoAndar.andar,
                           c.tbEnderecamentoAndar.tbEnderecamentoPredio.predio,
                           c.tbEnderecamentoAndar.tbEnderecamentoPredio.tbEnderecamentoRua.rua,
                           c.tbEnderecamentoAndar.tbEnderecamentoPredio.tbEnderecamentoLado.lado
                       }).OrderBy(x => x.idEnderecamentoApartamento).ToList();
        

        if (apartamentos.Count > 0)
        {
            GridView1.DataSource = apartamentos;
            GridView1.DataBind();
        }

    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        BindData();
    }

    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        //CsModulo modulo = new CsModulo();
        //int cd_modulo = Convert.ToInt32(GridView1.DataKeys[e.RowIndex].Value);
        //modulo.excluir(cd_modulo);


        //string meuscript;
        //if (!modulo.Tem_erro)
        //{
        //    meuscript = @"alert('Módulo excluído com sucesso');window.location='modulolistar.aspx';";
        //}
        //else
        //{

        //    meuscript = @"alert('" + modulo.Error + "');";
        //}

        //Page.ClientScript.RegisterStartupScript(this.GetType(), "MyScript", meuscript, true);
    }


    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            /* LinkButton MyButton = (LinkButton)e.Row.FindControl("cmdDelete");
             MyButton.Attributes.Add("onclick", "javascript:return " +
             "confirm('Confirma a exclusão do item " +
             DataBinder.Eval(e.Row.DataItem, "area") + "?')");
             * */
        }
    }
    protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GridView1.EditIndex = -1;

        BindData();
    }
    protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

        //string cd_modulo = GridView1.DataKeys[e.RowIndex].Value.ToString();
        //string nm_modulo = ((TextBox)GridView1.Rows[e.RowIndex].FindControl("TextBox1")).Text;
        //GridView1.EditIndex = -1;

        //CsModulo modulo = new CsModulo();
        //modulo.atualizar(Convert.ToInt32(cd_modulo), nm_modulo.ToString());
        //string meuscript;
        //if (!modulo.Tem_erro)
        //{
        //    meuscript = @"alert('Módulo atualizado com sucesso');window.location='modulolistar.aspx';";
        //}
        //else
        //{

        //    meuscript = @"alert('" + modulo.Error + "');";
        //}

        //Page.ClientScript.RegisterStartupScript(this.GetType(), "MyScript", meuscript, true);

    }
    protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
    {

        GridView1.EditIndex = e.NewEditIndex;
        BindData();
    }
    protected void btnnovoAndar_Click(object sender, EventArgs e)
    {
        
        using (var data = new dbCommerceDataContext())
        {
            var apartamento = new tbEnderecamentoApartamento();
            apartamento.idEnderecamentoAndar = Convert.ToInt32(ddlandar.SelectedItem.Value);
            apartamento.apartamento = txtApartamento.Text;

            data.tbEnderecamentoApartamentos.InsertOnSubmit(apartamento);
            data.SubmitChanges();

          //  fillDdls();
            BindData();

        }
       
    }
    protected void ddlpredio_SelectedIndexChanged(object sender, EventArgs e)
    {
        int idEnderecamentoPredio = Convert.ToInt32(ddlpredio.SelectedValue);
        using (var data = new dbCommerceDataContext())
        {
          /*  var ruas = (from c in data.tbEnderecamentoRuas
                        join p in data.tbEnderecamentoPredios on c.idEnderecamentoRua equals p.idEnderecamentoRua
                        where p.idEnderecamentoPredio == Convert.ToInt32(ddlpredio.SelectedValue)
                        select c).ToList();

            */
            var ruas = (from c in data.tbEnderecamentoPredios
                        // join p in data.tbEnderecamentoRuas on c.idEnderecamentoRua equals p.idEnderecamentoRua
                        where c.predio == ddlpredio.SelectedItem.Text
                        select new
                        {
                            c.tbEnderecamentoRua.idEnderecamentoRua,
                            c.tbEnderecamentoRua.rua
                        }).ToList().GroupBy(x => x.idEnderecamentoRua).Select(group => group.First());

                        

            //myList.GroupBy(i => i.id).Select(group => group.First())


            //ddlrua.DataSource = null;
            //ddlrua.DataValueField = "idEnderecamentoRua";
            //ddlrua.DataTextField = "rua";
            //ddlrua.DataSource = ruas;
            //ddlrua.DataBind();
            /*GridView2.DataSource = ruas;
            GridView2.DataBind();*/
        }
    }
    protected void ddlarea_SelectedIndexChanged(object sender, EventArgs e)
    {
        //int idEnderecamentoarea = Convert.ToInt32(ddlarea.SelectedValue);
        using (var data = new dbCommerceDataContext())
        {
            var ruas = (from c in data.tbEnderecamentoRuas
                        where c.idEnderecamentoArea == Convert.ToInt32(ddlarea.SelectedValue) select c)
                        .ToList();
             ddlrua.DataSource = null;
            ddlrua.DataValueField = "idEnderecamentoRua";
            ddlrua.DataTextField = "rua";
            ddlrua.DataSource = ruas;
            ddlrua.DataBind();
        }

    }
    protected void ddlrua_SelectedIndexChanged(object sender, EventArgs e)
    {
        using (var data = new dbCommerceDataContext())
        {
            var predios = (from c in data.tbEnderecamentoPredios
                           where c.idEnderecamentoRua == Convert.ToInt32(ddlrua.SelectedValue)
                          // select c).ToList();
                           select new
                           {
                               //idEnderecamentoPredio = c.idEnderecamentoPredio,
                               //predio = "id " + c.idEnderecamentoPredio + " rua " + c.tbEnderecamentoRua.rua + " lado " + c.tbEnderecamentoLado
                               c.idEnderecamentoPredio,
                               c.tbEnderecamentoRua.rua,
                               c.tbEnderecamentoLado.lado,
                               c.predio
                           })
                        .ToList();
            var predios2 = (from c in predios
                            select new
                                {
                                    //idEnderecamentoPredio = c.idEnderecamentoPredio,
                                    //predio = "id " + c.idEnderecamentoPredio + " rua " + c.tbEnderecamentoRua.rua + " lado " + c.tbEnderecamentoLado
                                    idEnderecamentoPredio = c.idEnderecamentoPredio,
                                    predio = "id " + c.idEnderecamentoPredio +" predio "+ c.predio+ " rua " + c.rua + " lado " + c.lado
                                }).ToList();
           
            ddlpredio.DataSource = null;
            ddlpredio.DataValueField = "idEnderecamentopredio";
            ddlpredio.DataTextField = "predio";
            ddlpredio.DataSource = predios2;
            ddlpredio.DataBind();
        }
    }
    protected void ddlpredio_SelectedIndexChanged1(object sender, EventArgs e)
    {

    }

    protected void ddlandar_SelectedIndexChanged(object sender, EventArgs e)
    {
        int idEnderecamentoAndar = Convert.ToInt32(ddlandar.SelectedValue);
        using (var data = new dbCommerceDataContext())
        {

        }
    }
}