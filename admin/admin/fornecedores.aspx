﻿<%@ Page Language="C#" ValidateRequest="false" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="fornecedores.aspx.cs" Inherits="admin_fornecedores" Theme="Glass" %>

<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" runat="Server">
    <table cellpadding="0" cellspacing="0" style="width: 920px">
        <tr>
            <td class="tituloPaginas" valign="top">
                <asp:Label ID="lblAcao" runat="server">Fornecedores</asp:Label>
            </td>
        </tr>
        <tr>
            <td>

                <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
                    <tr>
                        <td align="right">
                            <table cellpadding="0" cellspacing="0" style="width: 834px">
                                <tr>
                                    <td align="right">&nbsp;</td>
                                    <td height="38"
                                        style="background-image: url('images/bkExportar.jpg'); padding-bottom: 1px;"
                                        valign="bottom" width="231">
                                        <table cellpadding="0" cellspacing="0" style="2">
                                            <tr>
                                                <td style="width: 97px">&nbsp;</td>
                                                <td style="width: 33px">
                                                    <asp:ImageButton ID="btPdf" runat="server" ImageUrl="~/admin/images/btPdf.jpg"
                                                        Style="margin-left: 5px" />
                                                </td>
                                                <td style="width: 31px">
                                                    <asp:ImageButton ID="btXsl" runat="server" ImageUrl="~/admin/images/btXls.jpg" OnClick="btXsl_Click"
                                                        Style="margin-left: 5px" />
                                                </td>
                                                <td style="width: 36px">
                                                    <asp:ImageButton ID="btRtf" runat="server" ImageUrl="~/admin/images/btRtf.jpg"
                                                        Style="margin-left: 5px" />
                                                </td>
                                                <td valign="bottom">
                                                    <asp:ImageButton ID="btCsv" runat="server" ImageUrl="~/admin/images/btCsv.jpg"
                                                        Style="margin-left: 5px" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <dxwgv:ASPxGridView ID="grd" runat="server" AutoGenerateColumns="False" OnHtmlRowCreated="grd_HtmlRowCreated1"
                                DataSourceID="sqlFornecedor" KeyFieldName="fornecedorId" Width="834px" 
                                Cursor="auto" OnRowDeleted="grd_RowDeleted">
                                <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                                <SettingsText ConfirmDelete="Tem certeza que deseja excluir?"
                                    EmptyDataRow="Nenhum registro encontrado." />
                                <SettingsPager Position="TopAndBottom" PageSize="50"
                                    ShowDisabledButtons="False" AlwaysShowPager="True">
                                    <Summary AllPagesText="Páginas: {0} - {1} ({2} items)"
                                        Text="Página {0} de {1} ({2} registros encontrados)" />
                                </SettingsPager>
                                <Settings ShowFilterRow="True" ShowGroupButtons="False" />
                                <SettingsEditing EditFormColumnCount="4"
                                    PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1"
                                    PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter"
                                    PopupEditFormWidth="700px" />
                                <Columns>
                                    <dxwgv:GridViewDataTextColumn Caption="ID do Fornecedor"
                                        FieldName="fornecedorId" ReadOnly="True" VisibleIndex="0">
                                        <Settings AutoFilterCondition="Contains" />
                                        <EditFormSettings Visible="False" CaptionLocation="Top" ColumnSpan="2" />
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Nome" FieldName="fornecedorNome"
                                        VisibleIndex="1" Width="500px">
                                        <PropertiesTextEdit>
                                            <ValidationSettings ErrorText="" SetFocusOnError="True">
                                                <RequiredField ErrorText="Preencha o campo nome." IsRequired="True" />
                                            </ValidationSettings>
                                        </PropertiesTextEdit>
                                        <Settings AutoFilterCondition="Contains" />
                                        <EditFormSettings CaptionLocation="Top" ColumnSpan="2" Visible="True"
                                            VisibleIndex="1" />
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Nome Fantasia"
                                        FieldName="fornecedorNomeFantasia" Visible="False" VisibleIndex="2">
                                        <EditFormSettings CaptionLocation="Top" Visible="True" ColumnSpan="2"
                                            VisibleIndex="2" />
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Telefone" FieldName="fornecedorTelefone"
                                        VisibleIndex="2" Width="90px">
                                        <Settings AutoFilterCondition="Contains" />
                                        <EditFormSettings CaptionLocation="Top" Visible="True" VisibleIndex="4" />
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Site" FieldName="fornecedorSite"
                                        Visible="False" VisibleIndex="3">
                                        <EditFormSettings CaptionLocation="Top" Visible="True" VisibleIndex="6" />
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="E-mail" FieldName="fornecedorEmail"
                                        Visible="False" VisibleIndex="3">
                                        <PropertiesTextEdit>
                                        </PropertiesTextEdit>
                                        <EditFormSettings CaptionLocation="Top" Visible="True" VisibleIndex="5" />
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="ID da Empresa"
                                        FieldName="fornecedorIdDaEmpresa" Visible="False" VisibleIndex="3">
                                        <EditFormSettings CaptionLocation="Top" Visible="True" VisibleIndex="3" />
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="CNPJ/CPF" FieldName="fornecedorCNPJCPF"
                                        Visible="False" VisibleIndex="4" UnboundType="Integer">
                                        <PropertiesTextEdit>
                                            <ValidationSettings SetFocusOnError="True">
                                                <RegularExpression ErrorText="Preencha corretamente o CNPJ ou CPF."
                                                    ValidationExpression="(^(\d{2}.\d{3}.\d{3}/\d{4}-\d{2})|(\d{14})$)|(^(\d{3}.\d{3}.\d{3}-\d{2})|(\d{11})$)" />
                                            </ValidationSettings>
                                        </PropertiesTextEdit>
                                        <EditFormSettings CaptionLocation="Top" Visible="True" ColumnSpan="2"
                                            VisibleIndex="16" />
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="RG/IE" FieldName="fornecedorIERG"
                                        Visible="False" VisibleIndex="4">
                                        <EditFormSettings CaptionLocation="Top" Visible="True" ColumnSpan="2"
                                            VisibleIndex="17" />
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Nome do Contato"
                                        FieldName="fornecedorNomeDoContato" Visible="False" VisibleIndex="4">
                                        <EditFormSettings CaptionLocation="Top" Visible="True" ColumnSpan="3"
                                            VisibleIndex="7" />
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Telefone do Contato"
                                        FieldName="fornecedorTelefoneDoContato" Visible="False" VisibleIndex="4">
                                        <EditFormSettings CaptionLocation="Top" Visible="True" VisibleIndex="8" />
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Rua" FieldName="fornecedorRua"
                                        Visible="False" VisibleIndex="4">
                                        <EditFormSettings CaptionLocation="Top" Visible="True" ColumnSpan="2"
                                            VisibleIndex="9" />
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Número" FieldName="fornecedorNumero"
                                        Visible="False" VisibleIndex="4">
                                        <EditFormSettings CaptionLocation="Top" Visible="True" VisibleIndex="10" />
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Complemento"
                                        FieldName="fornecedorComplemento" Visible="False" VisibleIndex="4">
                                        <EditFormSettings CaptionLocation="Top" Visible="True" VisibleIndex="11" />
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Bairro" FieldName="fornecedorBairro"
                                        Visible="False" VisibleIndex="4">
                                        <EditFormSettings CaptionLocation="Top" Visible="True" VisibleIndex="14" />
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Cidade" FieldName="fornecedorCidade"
                                        Visible="False" VisibleIndex="4">
                                        <EditFormSettings CaptionLocation="Top" Visible="True" VisibleIndex="13" />
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataComboBoxColumn Caption="Estado" FieldName="fornecedorEstado"
                                        Visible="False" VisibleIndex="4">
                                        <PropertiesComboBox DataSourceID="sqlEstados" DropDownHeight="21px"
                                            TextField="EstadoNome" ValueField="estadoId" ValueType="System.String">
                                        </PropertiesComboBox>
                                        <EditFormSettings CaptionLocation="Top" Visible="True" VisibleIndex="12" />
                                    </dxwgv:GridViewDataComboBoxColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Cep" FieldName="fornecedorCep"
                                        Visible="False" VisibleIndex="4">
                                        <PropertiesTextEdit MaxLength="8">
                                            <ClientSideEvents KeyPress="function(s, e) {}" />
                                            <ValidationSettings SetFocusOnError="True">
                                                <RegularExpression ErrorText="Preencha corretamente o campo CEP, EX:(00000000)."
                                                    ValidationExpression="^\d{8}?$" />
                                            </ValidationSettings>
                                        </PropertiesTextEdit>
                                        <EditFormSettings CaptionLocation="Top" Visible="True" VisibleIndex="15" />
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Prazo de Fabricação (no site)"
                                        FieldName="fornecedorPrazoDeFabricacao" Visible="True" VisibleIndex="2">
                                        <EditFormSettings CaptionLocation="Top" Visible="True" VisibleIndex="2" />
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Prazo de Fabricação (fornecedor)"
                                        FieldName="fornecedorPrazoPedidos" Visible="True" VisibleIndex="2">
                                        <EditFormSettings CaptionLocation="Top" Visible="True" VisibleIndex="2" />
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Sigla"
                                        FieldName="codigoEtiqueta" Visible="True" VisibleIndex="2">
                                        <PropertiesTextEdit>
                                            <ValidationSettings ErrorText="" SetFocusOnError="True">
                                                <RequiredField ErrorText="Preencha o campo sigla." IsRequired="True" />
                                            </ValidationSettings>
                                        </PropertiesTextEdit>
                                        <EditFormSettings CaptionLocation="Top" Visible="True" VisibleIndex="18" />
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Pedido em dias úteis?" FieldName="prazoFornecedorDiasUteis" Name="prazoFornecedorDiasUteis" Visible="False" VisibleIndex="4">
                                        <EditFormSettings CaptionLocation="Top" Visible="True" VisibleIndex="18" />
                                        <EditItemTemplate>
                                            <dxe:ASPxCheckBox runat="server" ID="chkPedidoDiasUteis" Checked='<%# Eval("prazoFornecedorDiasUteis") == null ? false : Eval("prazoFornecedorDiasUteis") %>' AutoPostBack="false">
                                            </dxe:ASPxCheckBox>
                                        </EditItemTemplate>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Gerar Pedido" FieldName="gerarPedido" Visible="False" Name="gerarPedido">
                                        <EditFormSettings CaptionLocation="Top" Visible="True" VisibleIndex="18" />
                                        <EditItemTemplate>
                                            <dxe:ASPxCheckBox ID="chkGerarPedido" runat="server" Checked='<%# Eval("gerarPedido") == null ? true : Eval("gerarPedido") %>' />
                                        </EditItemTemplate>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Pedido Segunda" FieldName="pedidoSegunda" Visible="False" Name="pedidoSegunda">
                                        <EditFormSettings CaptionLocation="Top" Visible="True" VisibleIndex="18" />
                                        <EditItemTemplate>
                                            <dxe:ASPxCheckBox ID="chkPedidoSegunda" runat="server" Checked='<%# Eval("pedidoSegunda") == null ? true : Eval("pedidoSegunda") %>' />
                                        </EditItemTemplate>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Pedido Terça" FieldName="pedidoTerca" Visible="False" Name="pedidoTerca">
                                        <EditFormSettings CaptionLocation="Top" Visible="True" VisibleIndex="18" />
                                        <EditItemTemplate>
                                            <dxe:ASPxCheckBox ID="chkPedidoTerca" runat="server" Checked='<%# Eval("pedidoTerca") == null ? true : Eval("pedidoTerca") %>' />
                                        </EditItemTemplate>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Pedido Quarta" FieldName="pedidoQuarta" Visible="False" Name="pedidoQuarta">
                                        <EditFormSettings CaptionLocation="Top" Visible="True" VisibleIndex="18" />
                                        <EditItemTemplate>
                                            <dxe:ASPxCheckBox ID="chkPedidoQuarta" runat="server" Checked='<%# Eval("pedidoQuarta") == null ? true : Eval("pedidoQuarta") %>' />
                                        </EditItemTemplate>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Pedido Quinta" FieldName="pedidoQuinta" Visible="False" Name="pedidoQuinta">
                                        <EditFormSettings CaptionLocation="Top" Visible="True" VisibleIndex="18" />
                                        <EditItemTemplate>
                                            <dxe:ASPxCheckBox ID="chkPedidoQuinta" runat="server" Checked='<%# Eval("pedidoQuinta") == null ? true : Eval("pedidoQuinta") %>' />
                                        </EditItemTemplate>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Pedido Sexta" FieldName="pedidoSexta" Visible="False" Name="pedidoSexta">
                                        <EditFormSettings CaptionLocation="Top" Visible="True" VisibleIndex="18" />
                                        <EditItemTemplate>
                                            <dxe:ASPxCheckBox ID="chkPedidoSexta" runat="server" Checked='<%# Eval("pedidoSexta") == null ? true : Eval("pedidoSexta") %>' />
                                        </EditItemTemplate>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Pedido Sábado" FieldName="pedidoSabado" Visible="False" Name="pedidoSabado">
                                        <EditFormSettings CaptionLocation="Top" Visible="True" VisibleIndex="18" />
                                        <EditItemTemplate>
                                            <dxe:ASPxCheckBox ID="chkPedidoSabado" runat="server" Checked='<%# Eval("pedidoSabado") == null ? true : Eval("pedidoSabado") %>' />
                                        </EditItemTemplate>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Pedido Domingo" FieldName="pedidoDomingo" Visible="False" Name="pedidoDomingo">
                                        <EditFormSettings CaptionLocation="Top" Visible="True" VisibleIndex="18" />
                                        <EditItemTemplate>
                                            <dxe:ASPxCheckBox ID="chkPedidoDomingo" runat="server" Checked='<%# Eval("pedidoDomingo") == null ? true : Eval("pedidoDomingo") %>' />
                                        </EditItemTemplate>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Reposição Automática Estoque" FieldName="reposicaoAutomaticaEstoque" Visible="False" Name="reposicaoAutomaticaEstoque">
                                        <EditFormSettings CaptionLocation="Top" Visible="True" VisibleIndex="18" />
                                        <EditItemTemplate>
                                            <dxe:ASPxCheckBox ID="chkReposicaoAutomaticaEstoque" runat="server" Checked='<%# Eval("reposicaoAutomaticaEstoque") == null ? true : Eval("reposicaoAutomaticaEstoque") %>' />
                                        </EditItemTemplate>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Mínimo mensal reposição estoque" FieldName="reposicaoAutomaticaMinimoMensal" Visible="False" VisibleIndex="4">
                                        <EditFormSettings CaptionLocation="Top" Visible="True" VisibleIndex="18" />
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Fornecedor Entrega" FieldName="entrega" Visible="False" Name="entrega">
                                        <EditFormSettings CaptionLocation="Top" Visible="True" VisibleIndex="18" />
                                        <EditItemTemplate>
                                            <dxe:ASPxCheckBox ID="chkEntrega" runat="server" Checked='<%# Eval("entrega") == null ? true : Eval("entrega") %>' />
                                        </EditItemTemplate>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="RelevanciaProdutos" FieldName="relevanciaProdutos" Visible="False" VisibleIndex="4">
                                        <EditFormSettings CaptionLocation="Top" Visible="True" VisibleIndex="18" />
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Exibir Estoque no Site" FieldName="estoqueConferido" Visible="False" Name="estoqueConferido">
                                        <EditFormSettings CaptionLocation="Top" Visible="True" VisibleIndex="18" />
                                        <EditItemTemplate>
                                            <dxe:ASPxCheckBox ID="chkEstoqueConferido" runat="server" Checked='<%# Eval("estoqueConferido") == null ? true : Eval("estoqueConferido") %>' />
                                        </EditItemTemplate>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Data Primeiro Romaneio" FieldName="dataInicioFabricacao" Visible="False" Name="dataInicioFabricacao">
                                        <EditFormSettings CaptionLocation="Top" Visible="True" VisibleIndex="18" />
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtDataInicioFabricacao" runat="server" Text='<%# Eval("dataInicioFabricacao") == null ? "" : Convert.ToDateTime(Eval("dataInicioFabricacao")).ToShortDateString()  %>' />
                                        </EditItemTemplate>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Data mudança Prazo" FieldName="dataFimFabricacao" Visible="False" Name="dataFimFabricacao">
                                        <EditFormSettings CaptionLocation="Top" Visible="True" VisibleIndex="18" />
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtDataFimFabricacao" runat="server" Text='<%# Eval("dataFimFabricacao") == null ? "" : Convert.ToDateTime(Eval("dataFimFabricacao")).ToShortDateString()  %>' />
                                        </EditItemTemplate>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Data ultimo Romaneio" FieldName="dataUltimoRomaneio" Visible="False" Name="dataUltimoRomaneio">
                                        <EditFormSettings CaptionLocation="Top" Visible="True" VisibleIndex="18" />
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtDataUltimoRomaneio" runat="server" Text='<%# Eval("dataUltimoRomaneio") == null ? "" : Convert.ToDateTime(Eval("dataUltimoRomaneio")).ToShortDateString()  %>' />
                                        </EditItemTemplate>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Centro De Distribuicao" Name="CentroDeDistribuicao" FieldName="idCentroDistribuicao"
                                        VisibleIndex="1" Width="444px">
                                        <PropertiesTextEdit>

                                            <ValidationSettings ErrorText="" SetFocusOnError="True">
                                            </ValidationSettings>

                                        </PropertiesTextEdit>
                                        <Settings AutoFilterCondition="Contains" />
                                        <EditFormSettings CaptionLocation="None" Visible="True" VisibleIndex="1" />
                                        <EditItemTemplate>
                                            <div class="rotulos">
                                                Centro de Distribuição
                                    <asp:DropDownList runat="server" ID="ddlCentroDeDistribuicao"  OnInit="ddlCentroDeDistribuicao_Init">
                                    </asp:DropDownList>
                                            </div>
                                        </EditItemTemplate>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataMemoColumn Caption="Sobre a Marca" FieldName="textoMarcaSite" VisibleIndex="1" Width="500px" PropertiesMemoEdit-Height="100" Visible="false">
                                        <Settings AutoFilterCondition="Contains" />
                                        <EditFormSettings CaptionLocation="Top" ColumnSpan="4" Visible="True" VisibleIndex="19" />
                                    </dxwgv:GridViewDataMemoColumn>

                                    <dxwgv:GridViewCommandColumn VisibleIndex="3" Width="90px" ButtonType="Image">
                                        <EditButton Visible="True" Text="Editar">
                                            <Image Url="~/admin/images/btEditar.jpg" />
                                        </EditButton>
                                        <NewButton Visible="True" Text="Novo">
                                            <Image Url="~/admin/images/btNovo.jpg" />
                                        </NewButton>
                                        <DeleteButton Visible="True" Text="Excluir">
                                            <Image Url="~/admin/images/btExcluir.jpg" />
                                        </DeleteButton>
                                        <CancelButton Text="Cancelar">
                                            <Image Url="~/admin/images/btCancelar.jpg" />
                                        </CancelButton>
                                        <UpdateButton Text="Salvar">
                                            <Image Url="~/admin/images/btSalvarPeq.jpg" />
                                        </UpdateButton>
                                        <ClearFilterButton Visible="True" Text="Limpar filtro">
                                            <Image Url="~/admin/images/btLimparFiltro.jpg" />
                                        </ClearFilterButton>
                                        <HeaderTemplate>
                                            <img alt="" src="images/legendaIcones.jpg" />
                                        </HeaderTemplate>
                                    </dxwgv:GridViewCommandColumn>
                                </Columns>
                                <StylesEditors>
                                    <Label Font-Bold="True">
                                    </Label>
                                </StylesEditors>
                            </dxwgv:ASPxGridView>
                            <dxwgv:ASPxGridViewExporter ID="grdEx" runat="server" FileName="fornecedores"
                                GridViewID="grd" Landscape="True" PreserveGroupRowStates="True">
                            </dxwgv:ASPxGridViewExporter>

                            <asp:LinqDataSource ID="sqlFornecedor" runat="server" ContextTypeName="dbCommerceDataContext" EnableDelete="True" EnableInsert="True" EnableUpdate="True" TableName="tbProdutoFornecedors" OnInserting="sqlFornecedor_Inserting" OnUpdating="sqlFornecedor_Updating">
                                <InsertParameters>
                                    <asp:FormParameter FormField="ddlCentroDeDistribuicao" Name="idCentroDeDistribuicao" />
                                </InsertParameters>
                            </asp:LinqDataSource>
                            <asp:LinqDataSource ID="sqlEstados" runat="server"
                                ContextTypeName="dbCommerceDataContext" TableName="tbEstados">
                            </asp:LinqDataSource>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>

