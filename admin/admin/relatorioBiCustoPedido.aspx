﻿<%@ Page Title="" Theme="Glass" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="relatorioBiCustoPedido.aspx.cs" Inherits="admin_relatorioBiCustoPedido" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.ASPxPivotGrid.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPivotGrid" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" Runat="Server">
    <table cellpadding="0" cellspacing="0" style="width: 920px">
        <tr>
            <td class="tituloPaginas" valign="top">
                <asp:Label ID="lblAcao" runat="server">BI - Vendas por Categoria</asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
                    <tr>
                        <td align="right">
                            <table cellpadding="0" cellspacing="0" style="width: 834px">
                                <tr>
                                    <td>
                                        <table cellpadding="0" cellspacing="0" style="width: 834px">
                                            <tr>
                                                <td style="padding-left: 20px; vertical-align: top;">
                                                    <table width="100%">                                        
                                                       <tr>
                                                <td class="rotulos" style="width: 100px">
                                                    Data inicial 1<br />
                                                                <asp:TextBox ID="txtDataInicial" runat="server" CssClass="campos" 
                                                                                    Width="90px" MaxLength="10"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="rqvDataInicial" runat="server" 
                                                                                    ControlToValidate="txtDataInicial" Display="None" 
                                                                                    ErrorMessage="Preencha a data inicial." 
                                                        SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="rgeDataInicial" runat="server" 
                                                                    ControlToValidate="txtDataInicial" Display="None" 
                                                                    ErrorMessage="Por favor, preencha corretamente a data inicial." 
                                                                    SetFocusOnError="True" 
                                                        ValidationExpression="^(([0-2]\d|[3][0-1])\/([0]\d|[1][0-2])\/[1-2][0-9]\d{2})$"></asp:RegularExpressionValidator>
                            
                                                </td>
                                                <td class="rotulos" style="width: 100px">
                                                    Data final 1<br  />
                                                    <asp:TextBox ID="txtDataFinal" runat="server" CssClass="campos" 
                                                                        Width="90px" MaxLength="10"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="rqvDataFinal" runat="server" 
                                                                        ControlToValidate="txtDataFinal" Display="None" 
                                                                        ErrorMessage="Preencha a data final." 
                                                        SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                    <b>
                                                    <asp:RegularExpressionValidator ID="rgeDataInicial0" runat="server" 
                                                                    ControlToValidate="txtDataFinal" Display="None" 
                                                                    ErrorMessage="Por favor, preencha corretamente a data final" 
                                                                    SetFocusOnError="True" 
                                            
                                            
                                                        ValidationExpression="^(([0-2]\d|[3][0-1])\/([0]\d|[1][0-2])\/[1-2][0-9]\d{2})$"></asp:RegularExpressionValidator>
                                                    </b>
                                                </td>
                                                <td valign="bottom">
                                                    <asp:ImageButton ID="btnPesquisar" runat="server" ImageUrl="~/admin/images/btPesquisar.jpg" OnClick="btnPequisar_Click" />
                                                    <asp:ValidationSummary ID="llds" runat="server" DisplayMode="List" ShowMessageBox="True" ShowSummary="False" />
                                                </td>
                                            </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">
                                        &nbsp;</td>
                                    <td height="38" 
                                        style="background-image: url('images/bkExportar.jpg'); padding-bottom: 1px;" 
                                        valign="bottom" width="231">
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <dx:ASPxPivotGrid EnableRowsCache="false" OptionsData-DataFieldUnboundExpressionMode="UseSummaryValues" OptionsView-ShowDataHeaders="True" CustomizationFieldsLeft="300" CustomizationFieldsTop="400" OnCustomUnboundFieldData="grd_CustomUnboundFieldData" ID="grd" runat="server"  ClientInstanceName="pivotGrid" Width="1300px">
                                <Fields>                                        
                                    <dx:PivotGridField Area="FilterArea" FieldName="categoriaItem" Caption="Categoria do Item" ID="CategoriaItem" />  
                                    <dx:PivotGridField Area="FilterArea" FieldName="produtoNome" Caption="Produto" ID="ProdutoNome"  />    
                                    <dx:PivotGridField Area="FilterArea" FieldName="idItemPedidoEstoque" Caption="Item" ID="ItemPedidoEstoque"  />  
                                    <dx:PivotGridField Area="FilterArea" FieldName="pedidoId" Caption="Pedido" ID="PedidoId"  />
                                    <dx:PivotGridField Area="FilterArea" FieldName="precoItem" Caption="Preço Item" ID="PrecoItem"  />
                                    <dx:PivotGridField Area="FilterArea" FieldName="valorDesconto" Caption="Valor Desconto" ID="ValorDesconto"  />
                                    <dx:PivotGridField Area="FilterArea" Caption="% Custo S. Desconto" ID="pCustoSemDesconto" UnboundExpression="([PrecoCusto] * 100) / [PrecoEfetivoItem]" FieldName="pCustoSDesconto" UnboundType="Decimal" UnboundFieldName="custoSDesconto" CellFormat-FormatString="{0:0.##}"  CellFormat-FormatType="Numeric" />
                                    <dx:PivotGridField Area="FilterArea" Caption="% Desconto" ID="pDesconto" UnboundExpression="([PrecoCusto] * 100) / [PrecoEfetivoItem]" FieldName="PDesconto" UnboundType="Decimal" UnboundFieldName="desconto" CellFormat-FormatString="{0:0.##}"  CellFormat-FormatType="Numeric" />
                                    <dx:PivotGridField Area="FilterArea" FieldName="valorDoFreteCliente" Caption="Frete Cliente" ID="ValorFreteCliente" />
                                    <dx:PivotGridField Area="FilterArea" FieldName="valorDoFretePago" Caption="Frete Pago" ID="ValorFretePago" />
                                    <dx:PivotGridField Area="FilterArea" Caption="% Frete Geral" ID="pFreteGeral" UnboundExpression="([ValorFretePago] * 100) / ([PrecoEfetivoItemComDesconto] - [ValorFreteCliente])" FieldName="pVFreteGeral" UnboundType="Decimal" UnboundFieldName="pValFreteGeral" CellFormat-FormatString="{0:0.##}"  CellFormat-FormatType="Numeric" />
                                    <dx:PivotGridField Area="FilterArea" Caption="% Frete Efetivo" ID="pFreteEfetivo" UnboundExpression="(([ValorFretePago] - [ValorFreteCliente]) * 100) / ([PrecoEfetivoItemComDesconto] - [ValorFreteCliente])" FieldName="pVFreteEfetivo" UnboundType="Decimal" UnboundFieldName="pValFreteEfetivo" CellFormat-FormatString="{0:0.##}"  CellFormat-FormatType="Numeric" />
                                      
                                    
                                    <dx:PivotGridField Area="RowArea" FieldName="categoriaProduto" Caption="Categoria do Produto" ID="CategoriaProduto" /> 
                                    
                                                                       
                                    <dx:PivotGridField Area="DataArea" FieldName="precoEfetivoItem" Caption="Preço Efetivo" ID="PrecoEfetivoItem"  />  
                                    <dx:PivotGridField Area="DataArea" FieldName="precoEfetivoItemComDesconto" Caption="Preço Com Descontos" ID="PrecoEfetivoItemComDesconto"  />  
                                    <dx:PivotGridField Area="DataArea" FieldName="custo" Caption="Custo" ID="PrecoCusto"  />   
                                    <dx:PivotGridField Area="DataArea" Caption="% Custo C. Desconto" ID="pCustoComDesconto" UnboundExpression="([PrecoCusto] * 100) / [PrecoEfetivoItemComDesconto]" FieldName="pCustoCDesconto" UnboundType="Decimal" UnboundFieldName="custoCDesconto" CellFormat-FormatString="{0:0.##}"  CellFormat-FormatType="Numeric" />
                                                   
                                                       
                                    

                                </Fields>
                                <OptionsView ShowFilterHeaders="True" ShowHorizontalScrollBar="True" ShowColumnGrandTotals="False"  ShowRowGrandTotals="False" ShowRowTotals="False" />
                                <OptionsCustomization AllowSortBySummary="True" AllowSort="True" AllowFilterInCustomizationForm="True"></OptionsCustomization>
                                <OptionsPager RowsPerPage="100"></OptionsPager>
                            </dx:ASPxPivotGrid>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>