﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_materiaPrimaProduto : System.Web.UI.Page
{
    public class materiaPrima
    {
        public int produtoId { get; set; }
        public int? idComprasUnidadeMedida { get; set; }
        public int idComprasProduto { get; set; }
        public int idProdutoMateriaPrima { get; set; }
        public string produtoNome { get; set; }
        public decimal? consumo { get; set; }
        public int? largura { get; set; }
        public int? comprimento { get; set; }
        public decimal? valorUnitario { get; set; }
        public decimal? valorTotal { get; set; }
        public decimal? totalGeral { get; set; }
        public decimal? fatorConversao { get; set; }
        public string observacao { get; set; }

    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            using (dbCommerceDataContext data = new dbCommerceDataContext())
            {
                var comprasProduto = (from cp in data.tbComprasProdutos
                                      where cp.materiaPrima == true
                                      orderby cp.produto
                                      select new
                                      {
                                          cp.idComprasProduto,
                                          cp.produto
                                      }).ToList();

                ddlComprasProduto.DataSource = comprasProduto;
                ddlComprasProduto.DataBind();

                int produtoId = 0;
                try
                {
                    produtoId = Convert.ToInt32(Request.QueryString["produtoId"]);
                }
                catch (Exception) { }
                var produto = (from c in data.tbProdutos
                               where c.produtoId == produtoId
                               select new
                               {
                                   c.produtoId,
                                   c.produtoNome,
                                   c.fotoDestaque,
                               }).FirstOrDefault();
                //  "http://www.graodegente.com.br/fotos/" + hdfProdutoId.Value + "/" + hdfFoto.Value + ".jpg";
                imgfotodestaque.Src = "http://www.graodegente.com.br/fotos/" + produto.produtoId + "/" + produto.fotoDestaque + ".jpg";
                lblprodutonome.Text = produto.produtoNome;

            }

            fillGrid();
        }
    }

    void fillGrid()
    {
        using (dbCommerceDataContext data = new dbCommerceDataContext())
        {

            int produtoId = 0;
            try
            {
                produtoId = Convert.ToInt32(Request.QueryString["produtoId"]);
            }
            catch (Exception) { }
            IEnumerable<materiaPrima> dados = (from p in data.tbProdutoMateriaPrimas
                                               join tcp in data.tbComprasProdutos
                                                   on p.idComprasProduto equals tcp.idComprasProduto
                                               join tcpo in data.tbComprasOrdemProdutos
                                                   on p.idComprasProduto equals tcpo.idComprasProduto
                                                   into _tcpo
                                              join tcpf in data.tbComprasProdutoFornecedors
                                                  on _tcpo.FirstOrDefault().idComprasProduto equals tcpf.idComprasProduto
                                                  into _tcpf
                                                join tcum in data.tbComprasUnidadesDeMedidas
                                                 on   _tcpf.FirstOrDefault().idComprasUnidadeMedida equals tcum.idComprasUnidadesDeMedida
                                                 into _tcum
                                               select new materiaPrima()
                                               {
                                                   produtoId = p.produtoId,
                                                   idComprasProduto = p.idComprasProduto,
                                                   idComprasUnidadeMedida = _tcpf.FirstOrDefault().idComprasUnidadeMedida,
                                                   idProdutoMateriaPrima = p.idProdutoMateriaPrima,
                                                   produtoNome = tcp.produto,
                                                   consumo = p.consumo,
                                                   largura = p.largura,
                                                   comprimento = p.comprimento,
                                                   observacao = p.observacao,
                                                   valorUnitario = _tcpo.OrderByDescending(x => x.dataCadastro).FirstOrDefault().preco / ( _tcum.FirstOrDefault().fatorConversao ?? 1),
                                                   fatorConversao = _tcum.FirstOrDefault().fatorConversao,
                                                   valorTotal = p.consumo * (_tcpo.OrderByDescending(x => x.dataCadastro).FirstOrDefault().preco / (_tcum.FirstOrDefault().fatorConversao ?? 1))
                                               }).ToList();

          


            GridView1.DataSource = dados.Where(x => x.produtoId == produtoId).OrderBy(x => x.produtoNome);
            GridView1.DataBind();

        }
    }
    protected void btnGravarProduto_Click(object sender, EventArgs e)
    {
        int idComprasProduto = Convert.ToInt32(ddlComprasProduto.SelectedValue);
        int produtoId = 0;
        try
        {
            produtoId = Convert.ToInt32(Request.QueryString["produtoId"]);
        }
        catch (Exception) { }

        decimal consumo = 0;
        decimal.TryParse(txtQuantidade.Text, out consumo);
        //decimal consumo = Convert.ToDecimal(txtQuantidade.Text);

        int largura = 0;
        int comprimento = 0;
        if (consumo == 0)
        {
            int.TryParse(txtLargura.Text, out largura);
            int.TryParse(txtComprimento.Text, out comprimento);
        }

        if (largura > 0 && comprimento > 0)
        {
            consumo = largura*comprimento;
        }

        using (dbCommerceDataContext data = new dbCommerceDataContext())
        {

            tbProdutoMateriaPrima _tbProdutoMateriaPrima = new tbProdutoMateriaPrima();
            _tbProdutoMateriaPrima.idComprasProduto = idComprasProduto;
            _tbProdutoMateriaPrima.produtoId = produtoId;
            _tbProdutoMateriaPrima.consumo = consumo;
            _tbProdutoMateriaPrima.largura = largura;
            _tbProdutoMateriaPrima.comprimento = comprimento;
            _tbProdutoMateriaPrima.observacao = txtObservacao.Text;
            data.tbProdutoMateriaPrimas.InsertOnSubmit(_tbProdutoMateriaPrima);
            data.SubmitChanges();


        }
        fillGrid();
    }
    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int idProdutoMateriaPrima = Convert.ToInt32(GridView1.DataKeys[e.RowIndex].Value);
        using (dbCommerceDataContext data = new dbCommerceDataContext())
        {
            tbProdutoMateriaPrima item = (from p in data.tbProdutoMateriaPrimas
                                          where p.idProdutoMateriaPrima == idProdutoMateriaPrima
                                          select p).First();
            data.tbProdutoMateriaPrimas.DeleteOnSubmit(item);
            data.SubmitChanges();
        }
        fillGrid();

        //int cd_disciplina = Convert.ToInt32(GridView1.DataKeys[e.RowIndex].Value);
        //disciplina.excluir(cd_disciplina);
    }
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {

            Label lblvalorunitario = (Label)e.Row.FindControl("lblvalorunitario");
           
           // decimal valorUnitario = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "valorUnitario") ?? 0);
            decimal valorUnitario = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "valorUnitario") ?? 0);
            lblvalorunitario.Text = valorUnitario.ToString("C");

            Label lblvalortotal = (Label)e.Row.FindControl("lblvalortotal");
            decimal valorTotal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "valorTotal") ?? 0);

            lblvalortotal.Text = valorTotal.ToString("C");

            
        }
    }
}