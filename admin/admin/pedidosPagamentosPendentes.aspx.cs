﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using DevExpress.Data.Filtering;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;

public partial class admin_pedidosPagamentosPendentes : System.Web.UI.Page
{

    DateTemplate dateTemplate = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        fillGrid();
    }
    protected void btPdf_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WritePdfToResponse();
    }
    protected void btXsl_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteXlsToResponse();
    }
    protected void btRtf_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteRtfToResponse();
    }
    protected void btCsv_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteCsvToResponse();
    }
    protected void grid_AutoFilterCellEditorCreate(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewEditorCreateEventArgs e)
    {
        ASPxGridView grid = sender as ASPxGridView;
        if (e.Column.FieldName == "dataHoraDoPedido")
        {
            DropDownEditProperties dde = new DropDownEditProperties();
            dde.EnableClientSideAPI = true;
            dateTemplate = new DateTemplate();
            dde.DropDownWindowTemplate = dateTemplate;
            e.EditorProperties = dde;
        }
        if (e.Column.FieldName == "vencimentoParcela")
        {
            DropDownEditProperties dde = new DropDownEditProperties();
            dde.EnableClientSideAPI = true;
            dateTemplate = new DateTemplate();
            dde.DropDownWindowTemplate = dateTemplate;
            e.EditorProperties = dde;
        }
    }
    protected void grid_AutoFilterCellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
    {
        ASPxGridView grid = sender as ASPxGridView;
        //if (e.Column.FieldName == "dataHoraDoPedido")
        //{
        //    ASPxDropDownEdit dde = e.Editor as ASPxDropDownEdit;
        //    dde.ClientSideEvents.CloseUp = String.Format("function (s, e) {{ ApplyFilter(s, {0}, {1}); }}", dateTemplate.cIdFrom, dateTemplate.cIdTo);
        //    //dde.ClientSideEvents.DropDown = String.Format("function (s, e) {{ OnDropDown(s, {0}, {1}); }}", dateTemplate.cIdFrom, dateTemplate.cIdTo);
        //    dde.ReadOnly = true;
        //}
        if (e.Column.FieldName == "dataHoraDoPedido")
        {
            ASPxDropDownEdit dde = e.Editor as ASPxDropDownEdit;
            dde.ClientSideEvents.CloseUp = String.Format("function (s, e) {{ ApplyFilter(s, {0}, {1}, 'dataHoraDoPedido'); }}", dateTemplate.cIdFrom, dateTemplate.cIdTo);
            //dde.ClientSideEvents.DropDown = String.Format("function (s, e) {{ OnDropDown(s, {0}, {1}); }}", dateTemplate.cIdFrom, dateTemplate.cIdTo);
            dde.ReadOnly = true;
        }
        if (e.Column.FieldName == "vencimentoParcela")
        {
            ASPxDropDownEdit dde = e.Editor as ASPxDropDownEdit;
            dde.ClientSideEvents.CloseUp = String.Format("function (s, e) {{ ApplyFilter(s, {0}, {1}, 'vencimentoParcela'); }}", dateTemplate.cIdFrom, dateTemplate.cIdTo);
            //dde.ClientSideEvents.DropDown = String.Format("function (s, e) {{ OnDropDown(s, {0}, {1}); }}", dateTemplate.cIdFrom, dateTemplate.cIdTo);
            dde.ReadOnly = true;
        }
    }
    protected void grid_ProcessColumnAutoFilter(object sender, ASPxGridViewAutoFilterEventArgs e)
    {
        if (e.Value == "|") return;
        //if (e.Column.FieldName == "dataHoraDoPedido")
        //{
        //    if (e.Kind == GridViewAutoFilterEventKind.CreateCriteria)
        //    {
        //        Session["dataHoraDoPedido"] = e.Value;
        //        String[] dates = e.Value.Split('|');
        //        DateTime dateFrom = Convert.ToDateTime(dates[0]),
        //            dateTo = Convert.ToDateTime(dates[1]);
        //        e.Criteria = (new OperandProperty("dataHoraDoPedido") >= dateFrom) &
        //                     (new OperandProperty("dataHoraDoPedido") <= dateTo);
        //    }
        //    else
        //    {
        //        if (Session["dataHoraDoPedido"] != null)
        //            e.Value = Session["dataHoraDoPedido"].ToString();
        //    }
        //}
        if (e.Column.FieldName == "dataHoraDoPedido")
        {
            if (e.Kind == GridViewAutoFilterEventKind.CreateCriteria)
            {
                Session["dataHoraDoPedido"] = e.Value;
                String[] dates = e.Value.Split('|');
                DateTime dateFrom = Convert.ToDateTime(dates[0]),
                    dateTo = Convert.ToDateTime(dates[1]);
                e.Criteria = (new OperandProperty("dataHoraDoPedido") >= dateFrom) &
                             (new OperandProperty("dataHoraDoPedido") <= dateTo);
            }
            else
            {
                if (Session["dataHoraDoPedido"] != null)
                    e.Value = Session["dataHoraDoPedido"].ToString();
            }
        }
        if (e.Column.FieldName == "vencimentoParcela")
        {
            if (e.Kind == GridViewAutoFilterEventKind.CreateCriteria)
            {
                Session["vencimentoParcela"] = e.Value;
                String[] dates = e.Value.Split('|');
                DateTime dateFrom = Convert.ToDateTime(dates[0]),
                    dateTo = Convert.ToDateTime(dates[1]);
                e.Criteria = (new OperandProperty("vencimentoParcela") >= dateFrom) &
                             (new OperandProperty("vencimentoParcela") <= dateTo);
            }
            else
            {
                if (Session["vencimentoParcela"] != null)
                    e.Value = Session["vencimentoParcela"].ToString();
            }
        }
        if (e.Column.FieldName == "pedidoIdCliente")
        {

            if (e.Kind == GridViewAutoFilterEventKind.CreateCriteria)
            {
                Session["pedidoIdCliente"] = e.Value;
                int pedidoId = rnFuncoes.retornaIdInterno(Convert.ToInt32(e.Value));
                e.Criteria = (new OperandProperty("pedidoId") == pedidoId);

                //e.Criteria = (new OperandProperty("pedidoIdCliente") == e.Value);
            }
            else
            {
                if (Session["pedidoIdCliente"] != null)
                    e.Value = null;// Session["pedidoIdCliente"].ToString();
            }

        }

        if (e.Column.FieldName == "valorCobrado")
        {
            if (e.Kind == GridViewAutoFilterEventKind.CreateCriteria)
            {
                Session["valorCobrado"] = e.Value;
                string valorCobrado = e.Value;
                e.Criteria = (new OperandProperty("valorCobrado") == valorCobrado);

                //if (Session["valorCobrado"] != null)
                //{
                //    if (String.IsNullOrEmpty(Session["valorCobrado"].ToString()))
                //    {
                //        e.Criteria.IsNull();
                //    }
                //}

            }
            else
            {
                //if (Session["valorCobrado"] != null)
                //    if (!String.IsNullOrEmpty(Session["valorCobrado"].ToString()))
                //        e.Criteria.IsNull();

                if (Session["valorCobrado"] != null)
                    e.Value = Session["valorCobrado"].ToString();



            }
        }
    }

    protected void grd_OnCustomUnboundColumnData(object sender, ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "pedidoIdCliente")
        {
            int pedidoId = Convert.ToInt32(e.GetListSourceFieldValue("pedidoId"));
            e.Value = rnFuncoes.retornaIdCliente(pedidoId);
        }
        if (e.Column.FieldName == "statusPagamento")
        {
            //decimal totalPago = 0;
            //decimal valorCobrado = 0;
            //int situacao = 0;
            //decimal.TryParse(e.GetListSourceFieldValue("totalPago").ToString(), out totalPago);
            //decimal.TryParse(e.GetListSourceFieldValue("valorCobrado").ToString(), out valorCobrado);
            //int.TryParse(e.GetListSourceFieldValue("statusDoPedido").ToString(), out situacao);

            //if (situacao == 3 | situacao == 4 | situacao == 5 | situacao == 11)
            //{
            //    e.Value = "Pagamento Confirmado";
            //}
            //else if (totalPago == 0)
            //{
            //    e.Value = "Aguardando Pagamento";
            //    return;
            //}
            //else if (totalPago > 0 && totalPago < valorCobrado)
            //{
            //    e.Value = "Pago Parcialmente";
            //    return;
            //}
            //else if (situacao == 2)
            //{
            //    e.Value = "Aguardando Pagamento";
            //    return;
            //}

            //e.Value = "";
        }
    }


    private void fillGrid()
    {
        var data = new dbCommerceDataContext();
        var pagamentosPendentes = (from c in data.tbPedidos
                                   join d in data.tbPedidoPagamentos on c.pedidoId equals d.pedidoId
                                   join e in data.tbPedidoPagamentoRiscos on d.idPedidoPagamento equals e.idPedidoPagamento into risco
                                   join f in data.tbPedidoPagamentoGateways on d.idPedidoPagamento equals f.idPedidoPagamento into gateway

                                   where c.statusDoPedido == 2 | c.statusDoPedido == 7
                                   select new
                                   {
                                       c.pedidoId,
                                       c.endNomeDoDestinatario,
                                       c.valorCobrado,
                                       c.dataHoraDoPedido,
                                       c.statusDoPedido,
                                       c.condDePagamentoId,
                                       d.idPedidoPagamento,
                                       clearsale = risco.FirstOrDefault() == null ? "" : risco.FirstOrDefault().statusGateway,
                                       statusGateway = gateway.FirstOrDefault() == null ? "" : gateway.FirstOrDefault().processorMessage
                                   }).ToList();
        var pagamentos = pagamentosPendentes.GroupBy(l => new { l.pedidoId, l.endNomeDoDestinatario, l.valorCobrado, l.dataHoraDoPedido, l.statusDoPedido, l.condDePagamentoId, l.clearsale })
            .Select(g => new { g.Key.pedidoId, g.Key.endNomeDoDestinatario, g.Key.valorCobrado, g.Key.dataHoraDoPedido, g.Key.statusDoPedido, g.Key.condDePagamentoId, risco = g.Key.clearsale, gateway = string.Join(",", g.Select(i => i.statusGateway)) });
        pagamentos = pagamentos.OrderByDescending(x => x.dataHoraDoPedido).ToList();
        grd.DataSource = pagamentos;
        grd.DataBind();
    }
}