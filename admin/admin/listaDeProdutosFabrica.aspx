﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="listaDeProdutosFabrica.aspx.cs" Inherits="admin_listaDeProdutosFabrica" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" runat="Server">
    <script src="js/jquery.min.js"></script>
    <style>
        .meugrid {
            background-color: #F4FAFB;
            border: 0px none;
            /* border-collapse: separate !important; */
            overflow: hidden;
            font: 9pt Tahoma;
            color: #000;
            cursor: pointer;
            /*white-space: nowrap;*/
            font-weight: normal;
            text-align: left;
            width: 834px;
            border-collapse: collapse;
            margin: 0 auto;
        }

            .meugrid th {
                background-image: url(../../App_Themes/Glass/GridView/gvHeaderBackground.gif);
                background-position: center top;
                background-repeat: repeat-x;
                background-color: #C0DDE0;
                overflow: hidden;
                height: 23px;
                line-height: 23px;
                padding: 4px 6px 5px;
                font-weight: normal;
                border: 1px solid #7EACB1;
                /* border-collapse: separate;*/
            }

            .meugrid td {
                overflow: hidden;
                height: 23px;
                line-height: 23px;
                padding: 4px 6px 5px;
                border: 1px solid #7EACB1;
            }

            .meugrid tr td:nth-child(4) {
                text-align: center;
            }

            .meugrid tr td:last-of-type {
                text-align: center;
            }

        .tabrotulos {
            width: 848px;
            margin: auto;
        }

        .materiaprima {
            width: 250px;
        }

            .materiaprima ul {
                margin: 0px;
                padding-left: 10px;
            }

                .materiaprima ul li {
                    max-width: 200px;
                    /*white-space: pre-wrap;*/
                }

        .meugrid tr td:last-of-type {
            text-align: center;
        }

        .nome {
            width: 237px;
        }

            .nome div {
                max-width: 230px;
                white-space: pre-wrap;
            }

        .rotulos td {
            font: bold 12px tahoma;
            padding-top: 20px;
            color: rgb(65, 105, 126);
            padding-left: 5px;
            padding-bottom: 5px;
        }
    </style>
    <script type="text/javascript">
        function checaChecks(campo) {
            var divs = document.querySelectorAll("[id]");
            for (var i = 0, len = divs.length; i < len; i++) {
                var div = divs[i];
                if (div.id.indexOf(campo) > -1) {
                    div.checked = true;
                }
            }
        }
        function checaEstoque() {
            var divs = document.querySelectorAll("[id]");
            for (var i = 0, len = divs.length; i < len; i++) {
                var div = divs[i];
                if (div.id.indexOf("chkChecar") > -1) {
                    div.checked = true;
                }
            }
        }
    </script>
    <div class="tituloPaginas" valign="top">
        Lista de Produtos
    </div>
    <div>
        <table class="tabrotulos">
            <tr class="rotulos">
                <td>&nbsp;<br />
                    <asp:CheckBox runat="server" ID="chkAtivo" Text="Ativo" />
                </td>
                <td>&nbsp;<br />
                    <asp:CheckBox runat="server" ID="ckbsemmateriaprima" Text="Sem Matéria Prima" />
                </td>
                <td>Nome:<br />
                    <asp:TextBox runat="server" ID="txtNome"></asp:TextBox>
                </td>
                <td>Categoria:<br />
                    <asp:DropDownList runat="server" ID="ddlCategoria" DataValueField="categoriaId" DataTextField="categoriaNome" AppendDataBoundItems="True">
                        <Items>
                            <asp:ListItem Text="Selecione" Value=""></asp:ListItem>
                        </Items>
                    </asp:DropDownList>
                </td>
                <%--                <td>
                    Processos:<br/>
                    <asp:DropDownList runat="server" ID="ddlProcessosFabrica" DataValueField="idSubProcessoFabrica" DataTextField="nome" AppendDataBoundItems="True">
                        <Items>
                            <asp:ListItem Text="Selecione" Value=""></asp:ListItem>
                        </Items>
                    </asp:DropDownList>
                </td>--%>
                <td>&nbsp;<br />
                    <asp:Button runat="server" ID="btnFiltrar" Text="Filtrar" OnClick="btnFiltrar_OnClick" />
                </td>
                <td>&nbsp;<br />
                    Total: 
                    <asp:Literal runat="server" ID="litTotal"></asp:Literal>
                </td>
            </tr>
            <tr class="rotulos">
                <td>&nbsp;
                </td>
                <td></td>
                <td></td>
            </tr>
        </table>
    </div>
    <div>
        <table class="meugrid" align="center" style="display: none;">

            <tr>
                <th>Foto
                </th>
                <th>Nome
                </th>
                <th>Matérias Primas Cadastradas
                </th>
                <th>Custos
                </th>
                <th>Processos
                </th>
                <th>Matérias Primas
                </th>
                <th>Markup
                </th>
            </tr>
            <asp:ListView runat="server" ID="lstProdutos" OnItemDataBound="lstProdutos_OnItemDataBound">
                <ItemTemplate>
                    <tr>
                        <asp:HiddenField runat="server" ID="hdfProdutoId" Value='<%# Eval("produtoId") %>' />
                        <asp:HiddenField runat="server" ID="hdfFoto" Value='<%# Eval("fotoDestaque") %>' />
                        <td>
                            <asp:Image runat="server" ID="imgFoto" Width="170" />
                        </td>
                        <td class="nome">
                            <div><%# Eval("produtoNome") %></div>
                        </td>

                        <td class="materiaprima">
                            <asp:Repeater ID="rptmateriaprima" runat="server">
                                <HeaderTemplate>
                                    <ul>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <li><%# Eval("produtoNome") %></li>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </ul>
                                   
                                </FooterTemplate>
                            </asp:Repeater>

                        </td>
                        <td>MateriaPrima: <%# Eval("somaMateriaPrima", "{0:c}") %><br />
                            Processos Internos: <%# Eval("somaProcessos", "{0:c}") %><br />
                            Processos Externos: <%# Eval("somaProcessosExternos", "{0:c}") %><br />
                            Gastos Gerais Fab. : <%# Eval("gastosGeraisFabricacao", "{0:c}") %><br />
                            <strong>Subtotal com GGF : <%# Eval("subTotalComGGF", "{0:c}") %></strong><br />
                            Margem de Lucro : <%# Eval("margemLucro", "{0:c}") %><br />
                            Tributos : <%# Eval("soTributos", "{0:c}") %><br />
                            <strong>Total : <%# Eval("subTotalComTributos", "{0:c}") %></strong>
                            <br />
                            <br />
                            Preço de custo : <%# Eval("produtoPrecoDeCusto", "{0:c}") %><br />
                            Lucro Atual(%) :<%#string.Format("{0:n2}",Eval("lucroPorcentagem")) %>%
                            <br />
                            Lucro Atual(R$) : <%# Eval("lucroValor", "{0:c}") %><br />
                        </td>
                        <td>
                            <a class="dxeHyperlink_Glass" href="processosinternos.aspx?produtoId=<%# Eval("produtoId") %>" target="_blank">Processos</a>
                        </td>
                        <td>
                            <a class="dxeHyperlink_Glass" href="materiaPrimaProduto.aspx?produtoId=<%# Eval("produtoId") %>" target="_blank">Matérias Primas</a>
                        </td>
                        <td>
                            <a class="dxeHyperlink_Glass" href="markup.aspx?produtoId=<%# Eval("produtoId") %>" target="_blank">Markup</a>
                        </td>
                    </tr>

                </ItemTemplate>
            </asp:ListView>
        </table>


        <asp:GridView ID="GridView1" CssClass="meugrid" runat="server" Width="860px" DataKeyNames="produtoId" AutoGenerateColumns="False"
            AllowPaging="True" OnPageIndexChanging="GridView1_OnPageIndexChanging" OnRowDataBound="GridView1_OnRowDataBound"
            PageSize="40">
            <Columns>

                <asp:TemplateField HeaderText="Foto">
                    <ItemTemplate>
                        <asp:HiddenField runat="server" ID="hdfProdutoId" Value='<%# Eval("produtoId") %>' />
                        <asp:HiddenField runat="server" ID="hdfFoto" Value='<%# Eval("fotoDestaque") %>' />
                        <asp:HiddenField runat="server" ID="hdfIndiceMargemDeLucro" Value='<%#Eval("indiceMargemDeLucro") %>' />
                        <asp:HiddenField runat="server" ID="hdfIndiceTributos" Value='<%#Eval("indiceTributos") %>' />
                        <asp:HiddenField runat="server" ID="hdfProdutoPrecoDeCusto" Value='<%#Eval("produtoPrecoDeCusto") %>' />
                        <asp:Image runat="server" ID="imgFoto" Width="170" AlternateText="Sem Foto" />
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:BoundField DataField="produtoNome" HeaderText="Nome" />

                <asp:TemplateField HeaderText="Matérias Primas Cadastradas">
                    <ItemTemplate>
                        <asp:Repeater ID="rptmateriaprima" runat="server">
                            <HeaderTemplate>
                                <ul style="margin: 0px; padding-left: 10px;">
                            </HeaderTemplate>
                            <ItemTemplate>
                                <li style="width: 160px;"><%# Eval("produtoNome") %></li>
                            </ItemTemplate>
                            <FooterTemplate>
                                </ul>
                            </FooterTemplate>
                        </asp:Repeater>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Custos" HeaderStyle-Width="220">
                    <ItemTemplate>
                        <asp:Label ID="lblDescMateriaPrima" runat="server"></asp:Label>
                        <%--MateriaPrima: <%# Eval("somaMateriaPrima", "{0:c}") %><br />
                        Processos Internos: <%# Eval("somaProcessos", "{0:c}") %><br />
                        Processos Externos: <%# Eval("somaProcessosExternos", "{0:c}") %><br />
                        Gastos Gerais Fab. : <%# Eval("gastosGeraisFabricacao", "{0:c}") %><br />
                        <strong>Subtotal com GGF : <%# Eval("subTotalComGGF", "{0:c}") %></strong><br />
                        Margem de Lucro : <%# Eval("margemLucro", "{0:c}") %><br />
                        Tributos : <%# Eval("soTributos", "{0:c}") %><br />
                        <strong>Total : <%# Eval("subTotalComTributos", "{0:c}") %></strong>
                        <br />
                        <br />
                        Preço de custo : <%# Eval("produtoPrecoDeCusto", "{0:c}") %><br />
                        Lucro Atual(%) :<%#string.Format("{0:n2}",Eval("lucroPorcentagem")) %>%
                            <br />
                        Lucro Atual(R$) : <%# Eval("lucroValor", "{0:c}") %><br />--%>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Processos">
                    <ItemTemplate>
                        <a class="dxeHyperlink_Glass" href="processosinternos.aspx?produtoId=<%# Eval("produtoId") %>" target="_blank">Processos</a>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Matérias Primas" HeaderStyle-Width="100">
                    <ItemTemplate>
                        <a class="dxeHyperlink_Glass" href="materiaPrimaProduto.aspx?produtoId=<%# Eval("produtoId") %>" target="_blank">Matérias Primas</a>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Markup">
                    <ItemTemplate>
                        <a class="dxeHyperlink_Glass" href="markup.aspx?produtoId=<%# Eval("produtoId") %>" target="_blank">Markup</a>
                    </ItemTemplate>
                </asp:TemplateField>

            </Columns>
        </asp:GridView>
    </div>




</asp:Content>

