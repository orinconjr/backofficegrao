﻿<%@ Page Title="" Theme="Glass" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="produtosEstoqueEtiquetas.aspx.cs" Inherits="admin_produtosEstoqueEtiquetas" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxTimer" TagPrefix="dxt" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" Runat="Server">
    <table cellpadding="0" cellspacing="0" style="width: 920px">
    <tr>
        <td class="tituloPaginas" valign="top">
            <asp:Label ID="lblAcao" runat="server">Etiquetas dos Produtos</asp:Label>
        </td>
    </tr>
        
    <tr>
        <td>
            
    <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:Panel runat="server" ID="pnEtiqueta" DefaultButton="btnCarregarEtiqueta">
                    Etiqueta:<br/>
                <asp:TextBox runat="server" ID="txtEtiqueta"></asp:TextBox><br/>
                    Pedido:<br/>
                <asp:TextBox runat="server" ID="txtPedido"></asp:TextBox><br/>
                <asp:Button runat="server" Text="Carregar" ID="btnCarregarEtiqueta" OnClick="btnCarregarEtiqueta_OnClick" />
                </asp:Panel>
            </td>
        </tr>

        <tr>
            <td>
                <dxwgv:ASPxGridView Settings-ShowHorizontalScrollBar="True" ID="grd" runat="server" AutoGenerateColumns="False" Width="834px" Cursor="auto" KeyFieldName="produtoId" OnCustomUnboundColumnData="grd1_OnCustomUnboundColumnData" Settings-ShowVerticalScrollBar="True" Settings-VerticalScrollableHeight="400">
                    <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                    <Styles>
                        <Footer Font-Bold="True">
                        </Footer>
                    </Styles>
                    <SettingsText ConfirmDelete="Tem certeza que deseja excluir?" emptydatarow="Nenhum registro encontrado." />
                    <SettingsPager Position="TopAndBottom" PageSize="50" ShowDisabledButtons="False" AlwaysShowPager="True">
                        <Summary AllPagesText="Páginas: {0} - {1} ({2} items)" Text="Página {0} de {1} ({2} registros encontrados)" />
                    </SettingsPager>
                    <settings showfilterrow="True" ShowGroupButtons="False" ShowHeaderFilterButton="True" ShowFooter="True" />
                    <SettingsEditing EditFormColumnCount="4" PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1" PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter" PopupEditFormWidth="700px" />
                    <Columns>
                        <dxwgv:GridViewDataTextColumn Caption="Editar" Name="editar" VisibleIndex="0" Width="60">
                            <DataItemTemplate>
                                <asp:LinkButton runat="server" Visible="True" OnCommand="btnAdicionarEstoque_OnCommand" ID="btnAdicionarEstoque" CommandArgument='<%# Eval("produtoId") %>' EnableViewState="False">Adicionar ao Estoque</asp:LinkButton>
                            </DataItemTemplate>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="ID" FieldName="produtoId" VisibleIndex="0" >
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Etiqueta" FieldName="idPedidoFornecedorItem" VisibleIndex="0" >
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataDateColumn Caption="Entrada" FieldName="dataEntrada" VisibleIndex="0" >
                        </dxwgv:GridViewDataDateColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Enviado" FieldName="enviado" VisibleIndex="0" >
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Data Envio" FieldName="dataEnvio" VisibleIndex="0" >
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Pedido" FieldName="pedidoId" VisibleIndex="0" >
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Produto" FieldName="produtoNome" VisibleIndex="0" >
                            <Settings AutoFilterCondition="Contains" />
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="ID da Empresa" FieldName="produtoIdDaEmpresa" VisibleIndex="0" >
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Complemento ID" FieldName="complementoIdDaEmpresa" VisibleIndex="0" >
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Fornecedor" FieldName="fornecedorNome" VisibleIndex="0">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Romaneio" FieldName="idPedidoFornecedor" VisibleIndex="0">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataDateColumn Caption="Data Limite" FieldName="dataLimite" VisibleIndex="0" >
                        </dxwgv:GridViewDataDateColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Área" FieldName="area" VisibleIndex="0">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Rua" FieldName="rua" VisibleIndex="0">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Prédio" FieldName="predio" VisibleIndex="0">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Lado" FieldName="lado" VisibleIndex="0">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Andar" FieldName="andar" VisibleIndex="0">
                        </dxwgv:GridViewDataTextColumn>
                    </Columns>
                    <StylesEditors>
                        <Label Font-Bold="True">
                        </Label>
                    </StylesEditors>
                </dxwgv:ASPxGridView>       
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;<br/>&nbsp;
            </td>
        </tr>
        <tr>
            <td>
                <b>Adicionar Produtos ao Estoque</b>
                <asp:TextBox runat="server" ID="hdfLista" Visible="False" />
            </td>
        </tr>
        <tr>
            <td>
                <table style="width: 100%;">
                    <tr class="rotulos" style="text-align: center; font-weight: bold;">
                        <td style="display: none;">
                            Pedido
                        </td>
                        <td>
                            ID do Produto
                        </td>
                        <td>
                            Complemento ID
                        </td>
                        <td>
                            Nome
                        </td>
                        <td style="display: none;">
                            Quantidade
                        </td>
                        <td style="display: none;">
                            Motivo
                        </td>
                    </tr>
                    <asp:ListView runat="server" ID="lstProdutos" OnItemDataBound="lstProdutos_OnItemDataBound">
                        <ItemTemplate>
                            <tr class="rotulos" style="text-align: center;">
                                <td style="padding-bottom: 3px; padding-top: 3px; border-bottom: 1px solid #b0e0e6;display: none;">
                                    <asp:Literal runat="server" ID="litPedidoId" Text='<%# Eval("pedidoId") %>'></asp:Literal>
                                </td>
                                <td style="padding-bottom: 3px; padding-top: 3px; border-bottom: 1px solid #b0e0e6;">
                                    <asp:HiddenField runat="server" ID="hiddenProdutoId" Value='<%# Eval("produtoId") %>' />
                                    <asp:Literal runat="server" ID="litProdutoIdDaEmpresa" Text='<%# Eval("produtoIdDaEmpresa") %>'></asp:Literal>
                                </td>
                                <td style="padding-bottom: 3px; padding-top: 3px; border-bottom: 1px solid #b0e0e6;">
                                    <asp:Literal runat="server" ID="litComplementoIdDaEmpresa" Text='<%# Eval("complementoIdDaEmpresa") %>'></asp:Literal>&nbsp;
                                </td>
                                <td style="padding-bottom: 3px; padding-top: 3px; border-bottom: 1px solid #b0e0e6;">
                                    <%# Eval("produtoNome") %>
                                </td>
                                <td style="padding-bottom: 3px; padding-top: 3px; border-bottom: 1px solid #b0e0e6;display: none;">
                                    <%# Eval("quantidade") %>
                                </td>
                                <td style="padding-bottom: 3px; padding-top: 3px; border-bottom: 1px solid #b0e0e6;display: none;">
                                    <%# Eval("motivo") %>
                                </td>
                                <td style="padding-bottom: 3px; padding-top: 3px; border-bottom: 1px solid #b0e0e6;">
                                    <asp:LinkButton runat="server" ID="btnRemover" OnCommand="btnRemover_OnCommand" CommandArgument="<%# Container.DataItemIndex %>">Remover</asp:LinkButton>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:ListView>        
                </table>        
            </td>
        </tr>
        <tr>
            <td style="text-align: right;">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td style="text-align: right;">
                <asp:Button runat="server" ID="btnGravar" Text="Gravar" OnClick="btnGravar_OnClick"/>
            </td>
        </tr>
    </table>
        </td>
    </tr>
</table>
</asp:Content>