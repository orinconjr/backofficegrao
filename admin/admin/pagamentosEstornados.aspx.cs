﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.IO;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SpreadsheetLight;

public partial class admin_pagamentosEstornados : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            txtDataInicial.Attributes.Add("onkeypress", "Mascaras('DATA', '" + txtDataInicial.ClientID + "');");
            txtDataFinal.Attributes.Add("onkeypress", "Mascaras('DATA', '" + txtDataFinal.ClientID + "');");

            carregaGrid();
        }
       

    }
    private void carregaGrid()
    {
        var data = new dbCommerceDataContext();
        var pedidosPagamentos = (from pp in data.tbPedidoPagamentos
                                 join op in data.tbOperadorPagamentos on pp.idOperadorPagamento 
                                equals op.idOperadorPagamento
                                join me in data.tbMotivoEstornos on pp.motivoEstornoId equals me.motivoEstornoId
                                 into motivoEstorno
                                 from me in motivoEstorno.DefaultIfEmpty()
            where pp.idOperadorPagamento == 11 && pp.condicaoDePagamentoId ==32
            && pp.idContaPagamento == 10
            select new
            {
                pp.pedidoId,
                pp.idPedidoPagamento,
                pp.valor,
                me.motivoEstorno,
                pp.dataPedidoEstorno,
                pp.dataAprovacaoEstorno,
                pp.estornoAprovado,
                pp.estornoNegado,
                pp.solicitanteEstorno,
                pp.aprovadorEstorno,
                pp.motivoEstornoId,
                pp.statusAntesEstorno

                
            }).OrderByDescending(x => x.pedidoId).ToList();
        if (!String.IsNullOrEmpty(txtNumPedido.Text))
        {
            int pedidoId = Convert.ToInt32(txtNumPedido.Text);
            pedidosPagamentos = (from pp in pedidosPagamentos where pp.pedidoId == pedidoId select pp).ToList();

        }
        if (ckbAutorizados.Checked)
        {
            pedidosPagamentos = (from pp in pedidosPagamentos where pp.estornoAprovado == true select pp).ToList();
        }

        if (ckbNegados.Checked)
        {
            pedidosPagamentos = (from pp in pedidosPagamentos where pp.estornoNegado == true select pp).ToList();
        }

        if (ckbPendentes.Checked)
        {
            pedidosPagamentos = (from pp in pedidosPagamentos where pp.estornoNegado == false && pp.estornoAprovado == false select pp).ToList();
        }

        if (txtDataInicial.Text != "" && txtDataFinal.Text != "")
        {
            var dtInicial = Convert.ToDateTime(txtDataInicial.Text);
            var dtFinal = Convert.ToDateTime(txtDataFinal.Text);
            pedidosPagamentos =
                pedidosPagamentos.Where(
                    x => x.dataPedidoEstorno >= dtInicial.Date && x.dataPedidoEstorno <= dtFinal.Date).ToList();
            //dados =
            //    dados.Where(x => x.produtoDataDaCriacao.Date >= dtInicial.Date && x.produtoDataDaCriacao.Date <= dtFinal.Date);
        }
        if (ddlMotivoCancelarPedido.SelectedItem.Value != "0")
        {
            int motivoEstornoId = Convert.ToInt32(ddlMotivoCancelarPedido.SelectedItem.Value);
            pedidosPagamentos = (from pp in pedidosPagamentos where pp.motivoEstornoId == motivoEstornoId select pp).ToList();
        }

            GridView1.DataSource = pedidosPagamentos;
            GridView1.DataBind();
        

    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;

        carregaGrid();
    }

        protected void btnFiltrar_Click(object sender, EventArgs e)
    {
        carregaGrid();
    }
        protected void btnExportarPlanilha_Click(object sender, EventArgs e)
        {
            var data = new dbCommerceDataContext();
            var pedidosPagamentos = (from pp in data.tbPedidoPagamentos
                                     join op in data.tbOperadorPagamentos on pp.idOperadorPagamento
                                    equals op.idOperadorPagamento
                                     join me in data.tbMotivoEstornos on pp.motivoEstornoId equals me.motivoEstornoId
                                      into motivoEstorno
                                     from me in motivoEstorno.DefaultIfEmpty()
                                     where pp.idOperadorPagamento == 11 && pp.condicaoDePagamentoId == 32
                                     && pp.idContaPagamento == 10
                                     select new
                                     {
                                         pp.pedidoId,
                                         pp.idPedidoPagamento,
                                         pp.valor,
                                         me.motivoEstorno,
                                         pp.dataPedidoEstorno,
                                         pp.dataAprovacaoEstorno,
                                         pp.estornoAprovado,
                                         pp.estornoNegado,
                                         pp.aprovadorEstorno,
                                         pp.motivoEstornoId

                                     }).ToList();
            if (!String.IsNullOrEmpty(txtNumPedido.Text))
            {
                int pedidoId = Convert.ToInt32(txtNumPedido.Text);
                pedidosPagamentos = (from pp in pedidosPagamentos where pp.pedidoId == pedidoId select pp).ToList();

            }
            if (ckbAutorizados.Checked)
            {
                pedidosPagamentos = (from pp in pedidosPagamentos where pp.estornoAprovado == true select pp).ToList();
            }

            if (ckbNegados.Checked)
            {
                pedidosPagamentos = (from pp in pedidosPagamentos where pp.estornoNegado == true select pp).ToList();
            }

            if (ckbPendentes.Checked)
            {
                pedidosPagamentos = (from pp in pedidosPagamentos where pp.estornoNegado == false && pp.estornoAprovado == false select pp).ToList();
            }

            if (txtDataInicial.Text != "" && txtDataFinal.Text != "")
            {
                var dtInicial = Convert.ToDateTime(txtDataInicial.Text);
                var dtFinal = Convert.ToDateTime(txtDataFinal.Text);
                pedidosPagamentos =
                    pedidosPagamentos.Where(
                        x => x.dataPedidoEstorno >= dtInicial.Date && x.dataPedidoEstorno <= dtFinal.Date).ToList();
                //dados =
                //    dados.Where(x => x.produtoDataDaCriacao.Date >= dtInicial.Date && x.produtoDataDaCriacao.Date <= dtFinal.Date);
            }
            if (ddlMotivoCancelarPedido.SelectedItem.Value != "0")
            {
                int motivoEstornoId = Convert.ToInt32(ddlMotivoCancelarPedido.SelectedItem.Value);
                pedidosPagamentos = (from pp in pedidosPagamentos where pp.motivoEstornoId == motivoEstornoId select pp).ToList();
            }
            SLDocument sl = new SLDocument();
            SLStyle style1 = sl.CreateStyle();
            style1.Font.Bold = true;
            sl.SetRowStyle(1, style1);

            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("Content-Disposition", "attachment; filename=estornos.xlsx");



            sl.SetCellValue("A1", "pedidoId");
            sl.SetCellValue("B1", "idPedidoPagamento");
            sl.SetCellValue("C1", "valor");
            sl.SetCellValue("D1", "motivoEstorno");
            sl.SetCellValue("E1", "dataPedidoEstorno");
            sl.SetCellValue("F1", "dataAprovacaoEstorno");
            sl.SetCellValue("G1", "estornoAprovado");
            sl.SetCellValue("H1", "estornoNegado");
            sl.SetCellValue("I1", "aprovadorEstorno");

            int linha = 2;
            foreach (var item in pedidosPagamentos)
            {
                sl.SetCellValue(linha, 1, item.pedidoId);
                sl.SetCellValue(linha, 2, item.idPedidoPagamento);
                sl.SetCellValue(linha, 3, item.valor);
                sl.SetCellValue(linha, 4, item.motivoEstorno);
                sl.SetCellValue(linha, 5, item.dataPedidoEstorno.ToString());
                sl.SetCellValue(linha, 6, item.dataAprovacaoEstorno.ToString());
                sl.SetCellValue(linha, 7, item.estornoAprovado.ToString());
                sl.SetCellValue(linha, 8, item.estornoNegado.ToString());
                sl.SetCellValue(linha, 9, item.aprovadorEstorno);

                linha++;
            }
            sl.SaveAs(Response.OutputStream);
            Response.End();
        }
        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                string pedidoId = DataBinder.Eval(e.Row.DataItem, "pedidoId").ToString();
        
                HtmlAnchor linkeditar = (HtmlAnchor)e.Row.FindControl("linkeditar");
                linkeditar.HRef = "pedido.aspx?pedidoId=" + pedidoId;

             

            }
        }

}