﻿<%@ Page Language="C#" Theme="Glass" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" Debug="true"
    CodeFile="produtoAlt.aspx.cs" Inherits="admin_produtoAlt" ValidateRequest="false" MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>


<%@ Register Assembly="CuteEditor" Namespace="CuteEditor" TagPrefix="CE" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dxhe" %>
<%@ Register Namespace="Controls" TagPrefix="uc" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxTabControl" Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>

<%@ Register Assembly="DevExpress.Web.v11.1" Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v11.1" Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxPopupControl" Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>


<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxGridView" Assembly="DevExpress.Web.ASPxGridView.v11.1" %>
<%@ Register assembly="DevExpress.Web.v11.1" namespace="DevExpress.Web.ASPxPopupControl" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" runat="Server">
    <asp:ScriptManager runat="server" ScriptMode="Release"></asp:ScriptManager>
    <link href="css/jquery.filer.css" type="text/css" rel="stylesheet" />
    <link href="js/image_reloader.css" type="text/css" rel="stylesheet" />
    <link href="css/themes/jquery.filer-dragdropbox-theme.css" type="text/css" rel="stylesheet" />
    <script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
    <script type="text/javascript" src="js/jquery.filer.min.js"></script>
    <script type="text/javascript" src="js/jquery.imageReloader.js"></script>

    <!-- Dependencia do 360 -->
    <script src="https://cdn.pannellum.org/2.3/pannellum.js"></script>
    <link rel="stylesheet" href="https://cdn.pannellum.org/2.3/pannellum.css">

    <style>
        .popup360bottom li{
            display: inline-block;
        }

        .popup360bottom li:first-child {
            float: left;
            margin-left: -41px;
        }

        .popup360bottom li:last-child {
            float: right;
        }

        #hotspot-popup {
            display: none;
            background-color: #ffffff;
            border: solid 1px #929292;
            width: 278px;
            height: 107px;
            position: absolute;
            z-index: 99999;
            top: 150px;
            left: 468px;
            padding: 5px;
            border-radius: 5px;
        }

        #hotspot-popup table {
            margin-bottom: 4px;
        }

        #hotspot-popup input[type=button]:first-child {
            float: left;
        }

        #hotspot-popup input[type=button]:last-child {
            float: right;
        }

        .pnlm-hotspot-base {
            background-color: yellow;
            color: #ffffff;
        }
    </style>

    <script type="text/javascript">
        //-------------------- 360 ---------------------------------
        var panoramaVw;
        var pitchActual = 0;
        var yawActual = 0;
        var hfovActual = 0;
        var hotspot = [];

        function setPanoramaPosition() {
            try {
                if (panoramaVw) {
                    $('#hdnFoto360Yaw').val(panoramaVw.getYaw());
                    $('#hdnFoto360Hfov').val(panoramaVw.getHfov());
                    $('#hdnFoto360Pitch').val(panoramaVw.getPitch());
                    popPanorama360.Hide();
                }
            } catch (err) {
                console.error('Panorama Exception', err);
                alert('Encontramos um problema ao salvar a posição do panorama, favor contactar o administrador');
            }
        }

        function hotspotClick(hotSpotDiv, args) {

            var selecionado = hotspot.find(function (A) {
                return A.id == args;
            });

            hotspotClearSelection();

            $(hotSpotDiv.path[0]).css('background-color', 'red');

            if (selecionado.text)
                $('#hotspot_texto').val(selecionado.text);

            if (selecionado.idProduto)
                $('#hotspot_idproduto').val(selecionado.idProduto);

            if (selecionado.urlProduto)
                $('#hotspot_url').val(selecionado.urlProduto);

            if (selecionado) {
                $('#delHotSpot').show();
            }

            $('#hotspot-popup').attr('data-id', args);
            IdProduto_onBlur();
            $('#hotspot-popup').show();
        }

        function hotspotClearSelection() {
            $.each($('.pnlm-hotspot-base'), function (idx, item) {
                $(item).css('background-color', 'yellow');
            });

            $('#hotspot-popup').removeAttr('data-id');
        }

        function openHotspotPopup() {
            $('#hotspot-popup').show();
            $('#addHotSpot').attr('disabled', 'true');
            $('#hotspot-popup').find('.see').html('');
        }

        function deleteHotSpot() {
            var idSelecionado = $('#hotspot-popup').attr('data-id');
            panoramaVw.removeHotSpot(idSelecionado);
            cancelHotSpot();
        }

        function saveHotSpot() {
            var hotspot_idproduto = $('#hotspot_idproduto').val();
            var hotspot_texto = $('#hotspot_texto').val();
            var hotspot_url = $('#hotspot_url').val();

            var idSelecionado = $('#hotspot-popup').attr('data-id');
            var pitch, yaw;
            if (!idSelecionado) {
                pitch = panoramaVw.getPitch();
                yaw = panoramaVw.getYaw();
            } else {
                var selecionado = hotspot.find(function (A) {
                    return A.id == idSelecionado;
                });

                pitch = selecionado.pitch;
                yaw = selecionado.yaw;
            }

            panoramaVw.removeHotSpot(hotspot_idproduto.toString());
            var spot = {
                "id": hotspot_idproduto.toString(),
                "pitch": pitch,
                "yaw": yaw,
                "text": (!hotspot_texto ? "" : hotspot_texto),
                "idProduto": hotspot_idproduto,
                "urlProduto": (!hotspot_url ? "" : hotspot_url),
                "clickHandlerFunc": hotspotClick,
                "clickHandlerArgs": hotspot_idproduto.toString()
            };
            panoramaVw.addHotSpot(spot);
            bindHotSpotHdn(); //Save json object to text in hidden input
            cancelHotSpot();
        }

        function cancelHotSpot() {
            $('#hotspot_texto, #hotspot_idproduto, #hotspot_url').val('');
            $('#hotspot-popup').hide();
            hotspotClearSelection();
            $('#hotspot-popup').removeAttr('data-id');
            $('#delHotSpot').hide();
        }

        function loadPanorama() {
            try {
                if (panoramaVw) { panoramaVw.destroy(); }
                $('#delHotSpot').hide();

                //.Replace("http://","https://")
                panoramaVw = pannellum.viewer('panorama', {
                    "type": "equirectangular",
                    "panorama": "<%=hdnUrlFoto360Mini.Value%>",
                    "hotSpotDebug": true,
                    "hotSpots": hotspot,
                    "yaw": yawActual,
                    "hfov": hfovActual,
                    "autoLoad": true
                }).on('mouseup', function (event) {
                    pitchActual = panoramaVw.getPitch();
                    yawActual = panoramaVw.getYaw();
                    hfovActual = panoramaVw.getHfov();
                    hotspotClearSelection();
                    cancelHotSpot();
                });

            } catch (err) {
                console.error('Panorama Exception', err);
                alert('Encontramos um problema ao carregar o panorama, favor contactar o administrador');
            }
        }

        function IdProduto_onBlur() {
            var idProduto = $('#hotspot_idproduto').val();

            $('#addHotSpot').attr('disabled', 'true');
            $('#hotspot-popup').find('.see').html('');

            if (idProduto == '' || isNaN(idProduto)) {
                $('#hotspot_texto').val('');
                $('#hotspot_url').val('');
                return false
            };

            $.ajax({
                url: "https://apiv2.graodegente.com.br/v1/ListaProdutosService?termobusca=&categoriaids=&produtoids=" + idProduto + "&colecaoids=&pagina=1&filtros=&filtrovalor=&quantidade=6&ordem=",
                context: document.body
            }).success(function (data) {

                if (data.totalResultados > 0) {
                    var produto = data.produtos[0];
                    $('#hotspot_texto').val(produto.produtoNome);
                    $('#hotspot_url').val(produto.produtoUrl);
                    $('#addHotSpot').removeAttr('disabled');

                    $('#hotspot-popup').find('.see').append($('<a href="https://www.graodegente.com.br/' + produto.categoriaUrl + '/' + produto.produtoUrl + '" target="_blank">Ver no site</a>'));

                } else {
                    $('#hotspot_texto').val('');
                    $('#hotspot_url').val('');
                }
            });
        }

        function bindHotSpotHdn() {
            var hdn = document.getElementById('hdnHotSpotJson');
            var clone = JSON.parse(JSON.stringify(hotspot));
            var clearResult = [];

            $.each(clone, function (idx, item) {
                console.log(idx, item);
                clearResult.push({
                    pitch: item.pitch,
                    id: item.id,
                    yaw: item.yaw,
                    url: item.urlProduto
                });
            });

            hdn.value = JSON.stringify(clearResult);
        }
        //-------------------------- END 360 ---------------------------------

        function imgError(image) {
            image.onerror = null;
            setTimeout(function () {
                image.src += '?' + +new Date;
            }, 1000);
        }
        function GetClientId(strid) {
            var count = document.forms[0].length;
            var i = 0;
            var eleName;
            for (i = 0; i < count; i++) {
                eleName = document.forms[0].elements[i].id;
                pos = eleName.indexOf(strid);
                if (pos >= 0) break;
            }
            return eleName;
        }

        function CalculoMargemConcorrencia(txtCustoConcorrencia, lblCustoConcorrencia, txtCusto) {
            var obj_txtCustoConcorrencia = document.getElementById(txtCustoConcorrencia);
            var obj_lblCustoConcorrencia = document.getElementById(lblCustoConcorrencia);
            var obj_txtCusto = document.getElementById(txtCusto);
            var valorConcorrencia = parseFloat(obj_txtCustoConcorrencia.value.replace(',', '.'));
            var valorCusto = parseFloat(obj_txtCusto.value.replace('.', '').replace(',', '.'));
            obj_lblCustoConcorrencia.innerHTML = (((valorConcorrencia * 100) / valorCusto) - 100).toFixed(1) + '%';
        }

        function CalculoMargemVenda(txtPrecoPromocional, lblPrecoPromocional, lblPrecoVenda, txtCusto) {
            var obj_txtPrecoPromocional = document.getElementById(txtPrecoPromocional);
            var obj_lblPrecoPromocional = document.getElementById(lblPrecoPromocional);
            var obj_lblPrecoVenda = document.getElementById(lblPrecoVenda);
            var obj_txtCusto = document.getElementById(txtCusto);
            var valorPrecoPromocional = parseInt(obj_txtPrecoPromocional.value);
            var valorCusto = parseFloat(obj_txtCusto.value.replace('.', '').replace(',', '.'));
            var valorPromocao = ((valorPrecoPromocional / 100) * valorCusto) + valorCusto;
            var valorVenda = (valorPromocao * 0.3) + valorPromocao;
            obj_lblPrecoPromocional.innerHTML = valorPromocao.toFixed(2);
            obj_lblPrecoVenda.innerHTML = valorVenda.toFixed(2);
        }

        function PrecoIgual(txtPrecoPromocional) {
            var obj_txtPrecoPromocional = document.getElementById(txtPrecoPromocional);
            for (i = 0; i < 100; i++) {
                var obj_txtPrecoPromocional2 = document.getElementById('grid_cell' + i + '_5_txtPrecoPromocional');
                document.getElementById('grid_cell' + i + '_5_txtPrecoPromocional').value = obj_txtPrecoPromocional.value;
            }
            return false;
        }

        $(document).ready(function () {
            $(".slow-images").imageReloader();
            $('#input').filer({
                limit: null,
                maxSize: null,
                extensions: null,
                changeInput: '<div class="jFiler-input-dragDrop"><div class="jFiler-input-inner"><div class="jFiler-input-icon"><i class="icon-jfi-cloud-up-o"></i></div><div class="jFiler-input-text"><h3>Arraste e Solte fotos aqui</h3> <span style="display:inline-block; margin: 15px 0">ou</span></div><a class="jFiler-input-choose-btn blue">Selecione Fotos</a></div></div>',
                showThumbs: true,
                appendTo: null,
                theme: "dragdropbox",
                templates: {
                    box: '<ul class="jFiler-item-list"></ul>',
                    item: '<li class="jFiler-item">\
                            <div class="jFiler-item-container">\
                                <div class="jFiler-item-inner">\
                                    <div class="jFiler-item-thumb">\
                                        <div class="jFiler-item-status"></div>\
                                        <div class="jFiler-item-info">\
                                            <span class="jFiler-item-title"><b title="{{fi-name}}">{{fi-name | limitTo: 25}}</b></span>\
                                        </div>\
                                        {{fi-image}}\
                                    </div>\
                                    <div class="jFiler-item-assets jFiler-row">\
                                        <ul class="list-inline pull-left">\
                                            <li>{{fi-progressBar}}</li>\
                                        </ul>\
                                        <ul class="list-inline pull-right">\
                                            <li><a class="icon-jfi-trash jFiler-item-trash-action"></a></li>\
                                        </ul>\
                                    </div>\
                                </div>\
                            </div>\
                        </li>',
                    itemAppend: '<li class="jFiler-item">\
                            <div class="jFiler-item-container">\
                                <div class="jFiler-item-inner">\
                                    <div class="jFiler-item-thumb">\
                                        <div class="jFiler-item-status"></div>\
                                        <div class="jFiler-item-info">\
                                            <span class="jFiler-item-title"><b title="{{fi-name}}">{{fi-name | limitTo: 25}}</b></span>\
                                        </div>\
                                        {{fi-image}}\
                                    </div>\
                                    <div class="jFiler-item-assets jFiler-row">\
                                        <ul class="list-inline pull-left">\
                                            <span class="jFiler-item-others">{{fi-icon}} {{fi-size2}}</span>\
                                        </ul>\
                                        <ul class="list-inline pull-right">\
                                            <li><a class="icon-jfi-trash jFiler-item-trash-action"></a></li>\
                                        </ul>\
                                    </div>\
                                </div>\
                            </div>\
                        </li>',
                    progressBar: '<div class="bar"></div>',
                    itemAppendToEnd: false,
                    removeConfirmation: false,
                    _selectors: {
                        list: '.jFiler-item-list',
                        item: '.jFiler-item',
                        progressBar: '.bar',
                        remove: '.jFiler-item-trash-action',
                    }
                },
                uploadFile: {
                    url: "ajaxFileHandler.ashx",
                    data: { 'produtoId': '<%=Request.QueryString["produtoId"]%>' },
                    type: 'POST',
                    enctype: 'multipart/form-data',
                    beforeSend: function () { },
                    success: function (data, el) {
                        var parent = el.find(".jFiler-jProgressBar").parent();
                        el.find(".jFiler-jProgressBar").fadeOut("slow", function () {
                            $("<div class=\"jFiler-item-others text-success\"><i class=\"icon-jfi-check-circle\"></i> Sucesso</div>").hide().appendTo(parent).fadeIn("slow");
                        });
                    },
                    error: function (el) {
                        var parent = el.find(".jFiler-jProgressBar").parent();
                        el.find(".jFiler-jProgressBar").fadeOut("slow", function () {
                            $("<div class=\"jFiler-item-others text-error\"><i class=\"icon-jfi-minus-circle\"></i> Erro</div>").hide().appendTo(parent).fadeIn("slow");
                        });
                    },
                    statusCode: {},
                    onProgress: function () { },
                },
                dragDrop: {
                    dragEnter: function () { },
                    dragLeave: function () { },
                    drop: function () { },
                },
                addMore: true,
                clipBoardPaste: false,
                excludeName: null,
                beforeShow: function () { return true },
                onSelect: function () { },
                afterShow: function () { },
                onRemove: function () { },
                onEmpty: function () { },
                captions: {
                    button: "Choose Files",
                    feedback: "Choose files To Upload",
                    feedback2: "files were chosen",
                    drop: "Drop file here to Upload",
                    removeConfirmation: "Are you sure you want to remove this file?",
                    errors: {
                        filesLimit: "Only {{fi-limit}} files are allowed to be uploaded.",
                        filesType: "Only Images are allowed to be uploaded.",
                        filesSize: "{{fi-name}} is too large! Please upload file up to {{fi-maxSize}} MB.",
                        filesSizeAll: "Files you've choosed are too large! Please upload files up to {{fi-maxSize}} MB."
                    }
                }
            });
        });

    </script>

    <link href="estilos/tabelaDescricao.css" rel="stylesheet" />

    <table cellpadding="0" cellspacing="0" style="width: 920px">
        <tr>
            <td class="tituloPaginas" valign="top">
                <asp:Label ID="lblAcao" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>                
                <dx:ASPxPageControl ID="tabsPedidos" runat="server" ActiveTabIndex="0" Width="100%">
                    <TabPages>
                        <dx:TabPage Text="Informações">
                            <ContentCollection>
                                <dx:ContentControl runat="server">
                                    <table>
                                    <tr>
                                    <td>
                                        <table cellpadding="0" cellspacing="0" style="width: 834px">
                                            <tr class="rotulos" style="display: none;">
                                                <td colspan="2">Site:<br />
                                                    <asp:DropDownList runat="server" ID="ddlSite" OnSelectedIndexChanged="ddlSite_OnSelectedIndexChanged" AutoPostBack="True">
                                                        <Items>
                                                            <asp:ListItem Value="1" Text="Grão de Gente"></asp:ListItem>
                                                            <asp:ListItem Value="2" Text="Grão de Gente KIDS"></asp:ListItem>
                                                        </Items>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr class="rotulos">
                                                <td colspan="2">  
                                                </td>
                                            </tr>
                                            <tr class="rotulos">
                                                <td colspan="2">Centro de Distribuição:<br />
                                                    <asp:DropDownList runat="server" ID="ddlCd" Width="145">
                                                        <%--OnSelectedIndexChanged="ddlCd_OnSelectedIndexChanged" AutoPostBack="True"--%>
                                                        <Items>
                                                            <asp:ListItem Value="1" Text="CD 1"></asp:ListItem>
                                                            <asp:ListItem Value="2" Text="CD 2"></asp:ListItem>
                                                            <asp:ListItem Value="3" Text="CD 3"></asp:ListItem>
                                                            <asp:ListItem Value="4" Text="CD 4"></asp:ListItem>
                                                        </Items>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr class="rotulos">
                                                <td colspan="2">Nome do Produto para o Cliente<asp:RequiredFieldValidator runat="server" ControlToValidate="txtProdutoNome"
                                                    ErrorMessage="Preencha o nome do produto para o Cliente." Display="None" SetFocusOnError="True"
                                                    ID="rqvProdutoNome"></asp:RequiredFieldValidator>
                                                    <br />
                                                    <asp:TextBox runat="server" CssClass="campos" Width="830px" ID="txtProdutoNome"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr class="rotulos">
                                                <td colspan="2">Nome do Produto para título e URL
                                                    <br />
                                                    <asp:TextBox runat="server" CssClass="campos" Width="830px" ID="txtProdutoNomeTitle"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr class="rotulos">
                                                <td colspan="2">Nome do Produto para Google Shopping
                                                    <br />
                                                    <asp:TextBox runat="server" CssClass="campos" Width="830px" ID="txtProdutoNomeShopping" MaxLength="150"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr class="rotulos" style="display: none;">
                                                <td colspan="2">Nome Novo<asp:RequiredFieldValidator runat="server" ControlToValidate="txtProdutoNomeNovo"
                                                    ErrorMessage="Preencha o nome novo do produto." Display="None" SetFocusOnError="True"
                                                    ID="rqvProdutoNomeNovo"></asp:RequiredFieldValidator>
                                                    <br />
                                                    <asp:TextBox runat="server" CssClass="campos" Width="830px" ID="txtProdutoNomeNovo"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr class="rotulos">
                                                <td colspan="2">TAGs do Produto
                                                    <br />
                                                    <asp:TextBox runat="server" CssClass="campos" Width="830px" ID="txtProdutoTags"></asp:TextBox>
                                                </td>
                                            </tr>

                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div>
                                            <div style="width: 465px; float: left" class="rotulos">
                                                <div>
                                                    <div style="float: left; padding-right: 7px;">
                                                        ID da Empresa<br />
                                                        <asp:TextBox runat="server" CssClass="campos" Width="132px" ID="txtProdutoIdDaEmpresa"></asp:TextBox>
                                                    </div>
                                                    <div style="float: left; padding-right: 7px;">
                                                        Complemento do ID<br />
                                                        <asp:TextBox runat="server" CssClass="campos" Width="132px" ID="txtComplementoId"></asp:TextBox>
                                                    </div>
                                                    <div style="float: left; padding-right: 7px;">
                                                        Código de Barras<br />
                                                        <asp:TextBox runat="server" CssClass="campos" Width="142px" ID="txtProdutoCodDeBarras"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div>
                                                    <div style="float: left; padding-right: 10px;">
                                                        Fornecedor<asp:RequiredFieldValidator runat="server" ControlToValidate="drpFornecedor"
                                                            ErrorMessage="Selecione o fornecedor." Display="None" SetFocusOnError="True"
                                                            ID="rqvFornecedor"></asp:RequiredFieldValidator>
                                                        <br />
                                                        <asp:DropDownList runat="server" AppendDataBoundItems="True" DataTextField="fornecedorNome"
                                                            DataValueField="fornecedorId" DataSourceID="sqlFornecedor" CssClass="campos"
                                                            Width="208px" ID="drpFornecedor">
                                                            <asp:ListItem>Selecione</asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:LinqDataSource runat="server" ContextTypeName="dbCommerceDataContext" TableName="tbProdutoFornecedors"
                                                            ID="sqlFornecedor1">
                                                        </asp:LinqDataSource>
                                                        <asp:SqlDataSource ID="sqlFornecedor" runat="server"
                                                            ConnectionString="<%$ ConnectionStrings:connectionString %>"
                                                            SelectCommand="SELECT * FROM [tbProdutoFornecedor]"></asp:SqlDataSource>
                                                    </div>
                                                    <div style="float: left;">
                                                        Marca<asp:RequiredFieldValidator runat="server" ControlToValidate="drpMarca" ErrorMessage="Selecione a marca."
                                                            Display="None" SetFocusOnError="True" ID="rqvMarca"></asp:RequiredFieldValidator>
                                                        <br />
                                                        <asp:DropDownList runat="server" AppendDataBoundItems="True" DataTextField="marcaNome"
                                                            DataValueField="marcaId" DataSourceID="sqlMarca" CssClass="campos" Width="208px"
                                                            ID="drpMarca">
                                                            <asp:ListItem>Selecione</asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:LinqDataSource runat="server" ContextTypeName="dbCommerceDataContext" TableName="tbMarcas"
                                                            ID="sqlMarca1">
                                                        </asp:LinqDataSource>
                                                        <asp:SqlDataSource ID="sqlMarca" runat="server"
                                                            ConnectionString="<%$ ConnectionStrings:connectionString %>"
                                                            SelectCommand="SELECT * FROM [tbMarca]"></asp:SqlDataSource>
                                                    </div>
                                                </div>

                                                <div>
                                                    <div style="display: none;">
                                                        Composição<br />
                                                        <asp:DropDownList runat="server" CssClass="campos"
                                                            Width="212px" ID="drpComposicao">
                                                            <asp:ListItem Value="">Selecione</asp:ListItem>
                                                            <asp:ListItem Value="fibrasiliconada">Fibra Siliconada</asp:ListItem>
                                                            <asp:ListItem Value="duplaface">Dupla Face</asp:ListItem>
                                                            <asp:ListItem Value="felpuda">Felpuda</asp:ListItem>
                                                            <asp:ListItem Value="cemalgodao">100% Algodão</asp:ListItem>
                                                            <asp:ListItem Value="varaosimples">Varão Simples</asp:ListItem>
                                                            <asp:ListItem Value="varaoduplo">Varão Duplo</asp:ListItem>
                                                            <asp:ListItem Value="comforro">Com Forro</asp:ListItem>
                                                            <asp:ListItem Value="fibradesilicone">Fibra de Silicone</asp:ListItem>
                                                            <asp:ListItem Value="macio">Macio</asp:ListItem>
                                                            <asp:ListItem Value="fiosegipsios">Fios Egípcios</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div style="float: left; width: 425px;">
                                                    <div style="float: left; padding-right: 10px;">
                                                        Fios<br />
                                                        <asp:TextBox runat="server" CssClass="campos" Width="85px" ID="txtFios"></asp:TextBox>
                                                    </div>
                                                    <div style="float: left; padding-right: 10px;">
                                                        Peças<br />
                                                        <asp:TextBox ID="txtPecas" runat="server" CssClass="campos" Width="104px"></asp:TextBox>
                                                    </div>
                                                    <div style="float: left;">
                                                        Brindes<br />
                                                        <asp:TextBox ID="txtBrindes" runat="server" CssClass="campos" Width="85px"></asp:TextBox>
                                                    </div>
                                                    <div style="float: left; display: none;">
                                                        Relevância<br />
                                                        <asp:TextBox ID="txtRelevancia" runat="server" CssClass="campos" Width="55px"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div style="float: left; width: 425px;">
                                                    <div style="float: left; padding-right: 10px;">
                                                        Estoque<asp:RequiredFieldValidator runat="server" ControlToValidate="txtProdutoEstoqueAtual"
                                                            ErrorMessage="Preencha o estoque." Display="None" SetFocusOnError="True" ID="rqvEstoque"></asp:RequiredFieldValidator>
                                                        <br />
                                                        <asp:TextBox runat="server" CssClass="campos" Width="85px" ID="txtProdutoEstoqueAtual"></asp:TextBox>
                                                    </div>
                                                    <div style="float: left; padding-right: 10px;">
                                                        Estoque Mínimo<asp:RequiredFieldValidator runat="server" ControlToValidate="txtProdutoEstoqueMinimo"
                                                            ErrorMessage="Preencha o estoque m&#237;nimo." Display="None" SetFocusOnError="True"
                                                            ID="rqvEstoqueMinino"></asp:RequiredFieldValidator>
                                                        <br />
                                                        <asp:TextBox runat="server" CssClass="campos" Width="104px" ID="txtProdutoEstoqueMinimo">0</asp:TextBox>
                                                    </div>
                                                    <div style="float: left;">
                                                        Peso<asp:RequiredFieldValidator runat="server" ControlToValidate="txtProdutoPeso"
                                                            ErrorMessage="Preencha o peso." Display="None" SetFocusOnError="True" ID="rqvPeso"></asp:RequiredFieldValidator>
                                                        <br />
                                                        <asp:TextBox runat="server" CssClass="campos" Width="85px" ID="txtProdutoPeso"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div style="float: left; width: 425px;">
                                                    <div style="float: left; padding-right: 10px;">
                                                        Largura (cm)<asp:RequiredFieldValidator ID="rqvLargura" runat="server"
                                                            ControlToValidate="txtLargura" Display="None"
                                                            ErrorMessage="Preencha a largura." SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                        <br />
                                                        <asp:TextBox ID="txtLargura" runat="server" CssClass="campos"
                                                            Width="85px"></asp:TextBox>
                                                    </div>
                                                    <div style="float: left; padding-right: 10px;">
                                                        Altura (cm)<asp:RequiredFieldValidator ID="rqvAltura" runat="server"
                                                            ControlToValidate="txtAltura" Display="None"
                                                            ErrorMessage="Preencha a altura." SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                        <br />
                                                        <asp:TextBox ID="txtAltura" runat="server" CssClass="campos"
                                                            Width="104px">0</asp:TextBox>
                                                    </div>
                                                    <div>
                                                        Profundidade<asp:RequiredFieldValidator ID="rqvProfundidade" runat="server"
                                                            ControlToValidate="txtProfundidade" Display="None"
                                                            ErrorMessage="Preencha a profundidade." SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                        <br />
                                                        <asp:TextBox ID="txtProfundidade" runat="server" CssClass="campos" Width="85px"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div>
                                                <table align="left" cellpadding="0" cellspacing="0" class="rotulos" style="width: 425px;">
                                                    <tr>
                                                        <td>Descrição do produto
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td >
                                                            <asp:TextBox runat="server" TextMode="MultiLine" CssClass="campos" Height="100px"
                                                                Width="423px" ID="txtProdutoDescricao"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                                <table cellpadding="0" cellspacing="0" style="width: 570px">
                                                    <tr>
                                                        <td>
                                                            <table cellpadding="0" cellspacing="0" style="width: 570px">
                                                                <tr>
                                                                    <td colspan="2">
                                                                        <table cellpadding="0" cellspacing="0" style="width: 445px">
                                                                            <tr class="rotulos">
                                                                                <td style="width: 109px" valign="bottom"></td>
                                                                                <td style="width: 127px" valign="bottom"></td>
                                                                                <td valign="bottom"></td>
                                                                                <td>&nbsp;</td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>                                            
                                            <div style="float: left;">
                                                <div style="font-size: 14px;">
                                                    <strong>Ativo em:</strong>
                                                </div>
                                                <div>
                                                    <asp:CheckBox runat="server" Text="Ativo Grão de Gente" CssClass="rotulos" ID="ckbAtivo" Checked="true"></asp:CheckBox>
                                                </div>
                                                <div>
                                                    <asp:CheckBox runat="server" Text="Mercado Livre" CssClass="rotulos" ID="ckbAtivoMercadoLivre" Checked="true"></asp:CheckBox>
                                                </div>
                                                <div>
                                                    <asp:CheckBox runat="server" Text="B2W (submarino, americanas)" CssClass="rotulos" ID="ckbAtivoB2w" Checked="true"></asp:CheckBox>
                                                </div>
                                                <div>
                                                    <asp:CheckBox runat="server" Text="CNova (extra, ponto frio)" CssClass="rotulos" ID="ckbAtivoCnova" Checked="true"></asp:CheckBox>
                                                </div>
                                                <div>
                                                    <asp:CheckBox runat="server" Text="Walmart" CssClass="rotulos" ID="ckbAtivoWalmart" Checked="true"></asp:CheckBox>
                                                </div>
                                                <div>
                                                    <asp:CheckBox runat="server" Text="Magazine Luiza" CssClass="rotulos" ID="ckbAtivoMagazineLuiza" Checked="true"></asp:CheckBox>
                                                </div>
                                            </div>
                                            <div style="float: left; margin-left: 20px">
                                                <div>
                                                    <div>
                                                        <asp:CheckBox runat="server" Text="Principal" CssClass="rotulos" ID="ckbPrincipal" Visible="false" Checked="true"></asp:CheckBox></div>
                                                    <asp:CheckBox runat="server" Text="Frete Gratis" CssClass="rotulos" ID="ckbFreteGratis"></asp:CheckBox>
                                                    <br />
                                                    <asp:CheckBox runat="server" Text="Lan&#231;amento" CssClass="rotulos" ID="ckbLancamento"></asp:CheckBox>
                                                    <br />
                                                    <asp:CheckBox runat="server" Text="Pr&#233; venda" CssClass="rotulos" ID="ckbPreVenda"></asp:CheckBox>
                                                    <br />
                                                    <asp:CheckBox runat="server" Text="Promo&#231;&#227;o" CssClass="rotulos" ID="ckbPromocao"></asp:CheckBox>
                                                    <br />
                                                    <asp:CheckBox ID="chkExclusivo" runat="server" CssClass="rotulos"
                                                        Text="Exclusivo" />
                                                    <br />
                                                    <asp:CheckBox runat="server" Text="Destaque 1" CssClass="rotulos"
                                                        ID="ckbDestaque1"></asp:CheckBox>
                                                    <asp:CheckBox runat="server" Text="Meninos" CssClass="rotulos" Style="display: none;"
                                                        ID="ckbDestaque2"></asp:CheckBox>
                                                    <asp:CheckBox runat="server" Text="Meninas" CssClass="rotulos" Style="display: none;"
                                                        ID="ckbDestaque3"></asp:CheckBox>
                                                    <asp:CheckBox ID="ckbMarketplaceCadastrar" runat="server" CssClass="rotulos"
                                                        Text="Marketplace" Style="display: none;" />
                                                    <asp:CheckBox ID="ckbMarketplaceEnviarMarca" runat="server" CssClass="rotulos"
                                                        Text="Enviar Marca no Marketplace" Style="display: none;" />
                                                    <br />
                                                    <asp:CheckBox ID="chbDescontoProgressivoFrete" runat="server" CssClass="rotulos" Text="Desconto Progressivo Frete" />
                                                    <asp:CheckBox ID="ckbCubagem" runat="server" CssClass="rotulos" Text="Cubagem" Style="display: none;"/>
                                                    <br />
                                                    <asp:CheckBox ID="ckbExibirDiferencaCombo" runat="server" CssClass="rotulos" Text="Exibir diferença do combo" />
                                                    <br />
                                                    <asp:CheckBox ID="ckbBloqueado" runat="server" CssClass="rotulos" Text="Bloqueado" />
                                                    <br />
                                                    <asp:CheckBox ID="ckbProntaEntrega" runat="server" CssClass="rotulos" Text="Pronta Entrega" />
                                                    <br />
                                                    <asp:CheckBox ID="ckbForaDeLinha" runat="server" CssClass="rotulos" Text="Fora de Linha" />
                                                    <br />
                                                    <asp:CheckBox ID="ckbEstoqueReal" runat="server" CssClass="rotulos" Text="Limitar Venda Estoque Real" />
                                                    <br />
                                                    <asp:CheckBox ID="ckbEstoqueVirtual" runat="server" CssClass="rotulos" Text="Limitar Venda Estoque Virtual" />
                                                    <br />
                                                    <asp:CheckBox ID="ckbCriacaoPropria" runat="server" CssClass="rotulos" Text="Criação Própria (3d)" />
                                                    <br />
                                                    <asp:CheckBox ID="ckbOcultarLista" runat="server" CssClass="rotulos" Text="Ocultar da Lista" />
                                                    <br />
                                                    <asp:CheckBox ID="ckbOcultarBusca" runat="server" CssClass="rotulos" Text="Ocultar da Busca" />
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </td>
                                </tr>
                                </table>
                                </dx:ContentControl>
                            </ContentCollection>
                        </dx:TabPage>
                        
                        <dx:TabPage Text="Preços">
                            <ContentCollection>
                                <dx:ContentControl runat="server">
                                    <table cellpadding="0" cellspacing="0" style="width: 834px">
                                            <tr class="rotulos">
                                                <td style="width: 213px">Preço de Custo<asp:RequiredFieldValidator runat="server" ControlToValidate="txtProdutoPrecoDeCusto"
                                                    ErrorMessage="Preencha o pre&#231;o de custo." Display="None" SetFocusOnError="True"
                                                    ID="rqvPrecoDeCusto"></asp:RequiredFieldValidator>
                                                    <br />
                                                    <asp:TextBox runat="server" CssClass="campos" Width="190px" ID="txtProdutoPrecoDeCusto" ReadOnly="True">0</asp:TextBox>
                                                </td>
                                                <td style="width: 213px">Preço<asp:RequiredFieldValidator runat="server" ControlToValidate="txtProdutoPreco"
                                                    ErrorMessage="Preencha o pre&#231;o." Display="None" SetFocusOnError="True" ID="rqvPreco"></asp:RequiredFieldValidator>
                                                    <br />
                                                    <asp:TextBox runat="server" CssClass="campos" Width="190px" ID="txtProdutoPreco"></asp:TextBox>
                                                </td>
                                                <td style="width: 213px">Preço Promocional<asp:RequiredFieldValidator runat="server" ControlToValidate="txtProdutoPrecoPromocional"
                                                    ErrorMessage="Preencha o pre&#231;o promocional." Display="None" SetFocusOnError="True"
                                                    ID="rqvPrecoPromocional"></asp:RequiredFieldValidator>
                                                    <br />
                                                    <asp:TextBox runat="server" CssClass="campos" Width="190px" ID="txtProdutoPrecoPromocional">0</asp:TextBox>
                                                </td>
                                                <td style="display: none;">Preço para Atacado<asp:RequiredFieldValidator runat="server" ControlToValidate="txtProdutoPrecoAtacado"
                                                    ErrorMessage="Preencha o pre&#231;o de atacado." Display="None" SetFocusOnError="True"
                                                    ID="rqvPrecoAtacado"></asp:RequiredFieldValidator>
                                                    <br />
                                                    <asp:TextBox runat="server" CssClass="campos" Width="191px" ID="txtProdutoPrecoAtacado">0</asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr class="rotulos">
                                                <td style="width: 213px">Margem:<br />
                                                    <asp:TextBox ID="txtMargem" runat="server" CssClass="campos" Width="190px">0</asp:TextBox>
                                                </td>
                                                <td style="width: 213px">Preço Venda:
                                                    <br />
                                                    <asp:Label ID="lblPrecoVenda" runat="server" Text=""></asp:Label>
                                                </td>
                                                <td style="width: 213px">Preço Promocional:
                                                    <br />
                                                    <asp:Label ID="lblPrecoPromocional" runat="server" Text=""></asp:Label><br />
                                                </td>
                                                <td style="width: 213px"></td>
                                            </tr>
                                            <tr class="rotulos">
                                                <td style="width: 213px">NCM:<br />
                                                    <asp:TextBox ID="txtNcm" runat="server" CssClass="campos" Width="190px"></asp:TextBox>
                                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="txtNcm"
                                                        ErrorMessage="Preencha o NCM." Display="None" SetFocusOnError="True"
                                                        ID="RequiredFieldValidator1"></asp:RequiredFieldValidator>
                                                </td>
                                                <td style="width: 213px">
                                                    <asp:TextBox ID="txtIdProdutoAlternativo" runat="server" CssClass="campos" Width="190px" Visible="false"></asp:TextBox>
                                                    ICMS:<br />
                                                    <asp:TextBox ID="txtIcms" runat="server" CssClass="campos" Width="190px"></asp:TextBox>
                                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="txtNcm"
                                                        ErrorMessage="Preencha o ICMS." Display="None" SetFocusOnError="True"
                                                        ID="RequiredFieldValidator2"></asp:RequiredFieldValidator>
                                                </td>
                                                <td style="width: 213px"></td>
                                                <td style="width: 213px"></td>
                                            </tr>
                                        </table>
                                </dx:ContentControl>
                            </ContentCollection>
                        </dx:TabPage>

                        <dx:TabPage Text="Categorias/Coleções">
                            <ContentCollection>
                                <dx:ContentControl runat="server">
                                    <table cellpadding="0" cellspacing="0" style="width: 890px">
                                        <tr>
                                            <td class="rotulos">
                                                <b>
                                                    Coleções<br />
                                                </b>
                                                <div id="divListaColecoesSelecionadas">
                                                    <asp:Literal runat="server" ID="litColecoesMarcadas"></asp:Literal>
                                                </div>
                                                <asp:Panel ID="pnlColecoes" runat="server" ScrollBars="Vertical" Height="300">
                                                    <asp:CheckBoxList ID="chkColecoes" runat="server" DataSourceID="sqlColecoes" DataTextField="colecaoNome" DataValueField="colecaoId" OnDataBound="chkColecoes_OnDataBound" Font-Size="12px" RepeatColumns="2"></asp:CheckBoxList>
                                                    <asp:SqlDataSource ID="sqlColecoes" runat="server" ConnectionString="<%$ ConnectionStrings:connectionString %>" SelectCommand="SELECT colecaoId, colecaoNome, siteId FROM tbColecao WHERE siteId = 1 order by colecaoNome">
                                                    </asp:SqlDataSource>
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <img alt="" src="images/separador.gif" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="rotulos">
                                                <b>
                                                    Categorias<br />
                                                </b>
                                                <div style="width: 650px; height: 450px; overflow: auto; float: left;">
                                                    <div style="float: left; width: 350px;">
                                                        <asp:TreeView ID="treeCategorias" runat="server" Font-Names="Tahoma" Font-Size="12px"
                                                            OnTreeNodePopulate="TreeView1_TreeNodePopulate" ShowCheckBoxes="Leaf" ShowLines="True">
                                                        </asp:TreeView>
                                                    </div>
                                                    <div style="float: left; width: 280px; white-space: nowrap;">
                                                        <asp:TreeView ID="treeCategoriasComFilhos" runat="server" Font-Names="Tahoma" Font-Size="12px"
                                                            OnTreeNodePopulate="TreeView1_TreeNodePopulate" ShowCheckBoxes="Leaf" ShowLines="True">
                                                        </asp:TreeView>
                                                    </div>
                                                </div>
                                                <div style="height: 450px; overflow: auto; float: left; padding-left: 20px; width: 210px; white-space: nowrap;">
                                                    <b>
                                                        <asp:Label runat="server" ID="lblCategoriasSelecionadas"></asp:Label>
                                                    </b>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </dx:ContentControl>
                            </ContentCollection>
                        </dx:TabPage>

                        <dx:TabPage Text="Descrições">
                            <ContentCollection>
                                <dx:ContentControl runat="server">
                                    <table>
                                        <tr>
                                            <td class="rotulos">
                                                <asp:DataList ID="dtlInformacoesAdcionais" runat="server" CellPadding="0" DataSourceID="sqlInformacoesAdcionais">
                                                    <ItemTemplate>
                                                        <table cellpadding="0" cellspacing="0" style="width: 834px">
                                                            <tr>
                                                                <td height="50" valign="bottom">
                                                                    <b class="rotulos">Nome da Informação</b><br />
                                                                    <asp:TextBox ID="txtInformacaoAdcionalNome" runat="server" CssClass="campos" Text='<%# Eval("informacaoAdcionalNome") %>'
                                                                        Width="225px"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <b class="rotulos">Conteúdo da informação</b>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <ce:editor id="txtInformacaoAdcional" runat="server"  
                                                                         autoconfigure="Full_noform" contextmenumode="Simple"
                                                                        editorwysiwygmodecss="" filespath="" height="400px"
                                                                        urltype="Default" userelativelinks="False"
                                                                        width="782px"
                                                                        removeservernamesfromurl="False" text='<%# Eval("informacaoAdcionalConteudo") %>'>
                                                          <TextAreaStyle CssClass="campos" />
                                                        <%--  <FrameStyle BackColor="White" BorderColor="#DDDDDD" BorderStyle="Solid" 
                                                              BorderWidth="1px" CssClass="CuteEditorFrame" Height="100%" Width="100%" />--%>
                                                      </ce:editor>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ItemTemplate>
                                                </asp:DataList>
                                                <asp:LinqDataSource ID="sqlInformacoesAdcionais1" runat="server" ContextTypeName="dbCommerceDataContext"
                                                    TableName="tbInformacaoAdcionals" Where="produtoId == @produtoId">
                                                    <WhereParameters>
                                                        <asp:QueryStringParameter Name="produtoId" QueryStringField="produtoId" Type="Int32" />
                                                    </WhereParameters>
                                                </asp:LinqDataSource>
                                                <asp:SqlDataSource ID="sqlInformacoesAdcionais" runat="server"
                                                    ConnectionString="<%$ ConnectionStrings:connectionString %>"
                                                    SelectCommand="SELECT * FROM [tbInformacaoAdcional] WHERE ([produtoId] = @produtoId)">
                                                    <SelectParameters>
                                                        <asp:QueryStringParameter Name="produtoId" QueryStringField="produtoId"
                                                            Type="Int32" />
                                                    </SelectParameters>
                                                </asp:SqlDataSource>
                                            </td>
                                        </tr>
                                    </table>
                                </dx:ContentControl>
                            </ContentCollection>
                        </dx:TabPage>

                        <dx:TabPage Text="Fotos">
                            <ContentCollection>
                                <dx:ContentControl runat="server">
                                    <table>                                        
                                        <tr id="trFotos" runat="server" visible="false">
                                            <td>
                                                <table cellpadding="0" cellspacing="0" style="width: 834px">
                                                    <tr>
                                                        <td>
                                                            <img alt="" src="images/separador.gif" /></td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:DataList ID="dtlFotos" runat="server" CellPadding="0"
                                                                DataSourceID="sqlFotos" RepeatColumns="8">
                                                                <ItemTemplate>
                                                                    <table cellpadding="0" cellspacing="0" style="width: 100px">
                                                                        <tr>
                                                                            <td align="center">

                                                                                <asp:Image ID="imgFotos" runat="server"
                                                                                    CssClass="slow-images"
                                                                                    ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings["caminhoCDN"] + "fotos/" + Eval("produtoId") + "/pequena_" + Eval("produtoFoto") + ".jpg" %>'
                                                                                    ToolTip='<%# Eval("produtoFotoId") %>' Height="80px" Width="80px"></asp:Image>
                                                                            </td>
                                                                        </tr>

                                                                        <tr>
                                                                            <td align="center">
                                                                                <asp:TextBox ID="txtFotoDescricao" runat="server" CssClass="campos"
                                                                                    Height="30px" Text='<%# Eval("produtoFotoDescricao") %>' TextMode="MultiLine"
                                                                                    Width="80px"></asp:TextBox></td>

                                                                        </tr>
                                                                        <tr>
                                                                            <td style="padding-left: 4px;">
                                                                                <asp:RadioButton ID="rdbDestaque" CssClass="rotulos" runat="server" Text="Destaque"
                                                                                    onclick="clickit();" Checked='<%# Convert.ToBoolean(Eval("produtoFotoDestaque")) %>'></asp:RadioButton>

                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="padding-left: 6px;">
                                                                                <asp:CheckBox ID="ckbExcluirFoto" runat="server" CssClass="rotulos" Text="excluir" />
                                                                            </td>
                                                                        </tr>                                                                
                                                                        <tr>
                                                                            <td align="center">
                                                                                <b class="rotulos" style="font-size: 10px">Ordem:</b><br />
                                                                                <asp:TextBox ID="txtOrdenacaoFoto" runat="server" CssClass="campos"
                                                                                    Height="20px" Text='<%# Eval("ordenacao") %>' TextMode="MultiLine"
                                                                                    Width="40px"></asp:TextBox></td>

                                                                        </tr>
                                                                    </table>
                                                                </ItemTemplate>
                                                            </asp:DataList>
                                                            <asp:LinqDataSource ID="sqlFotos1" runat="server" ContextTypeName="dbCommerceDataContext"
                                                                TableName="tbProdutoFotos" Where="produtoId == @produtoId">
                                                                <WhereParameters>
                                                                    <asp:QueryStringParameter Name="produtoId" QueryStringField="produtoId" Type="Int32" />
                                                                </WhereParameters>
                                                            </asp:LinqDataSource>
                                                            <asp:SqlDataSource ID="sqlFotos" runat="server"
                                                                ConnectionString="<%$ ConnectionStrings:connectionString %>"
                                                                SelectCommand="SELECT * FROM [tbProdutoFoto] WHERE ([produtoId] = @produtoId) order by (case when ordenacao is null then 9999 else ordenacao end)">
                                                                <SelectParameters>
                                                                    <asp:QueryStringParameter Name="produtoId" QueryStringField="produtoId"
                                                                        Type="Int32" />
                                                                </SelectParameters>
                                                            </asp:SqlDataSource>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td>&nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                    <td>
                                        <img alt="" src="images/separador.gif" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Panel ID="pnlFotos" runat="server">
                                            <table cellpadding="0" cellspacing="0" class="rotulos"
                                                style="width: 100%">
                                                <tr>
                                                    <td>
                                                        <table cellpadding="0" cellspacing="0" style="width: 100%">
                                                            <tr>
                                                                <td class="rotulos" style="width: 291px">Foto<asp:RegularExpressionValidator ID="rgeFlu1" runat="server"
                                                                    ControlToValidate="upl1" Display="None"
                                                                    ErrorMessage="A imagem precisa ser jpg." SetFocusOnError="True"
                                                                    ValidationExpression="(.*\.jpe?g|.*\.JPE?G)"></asp:RegularExpressionValidator>
                                                                    <br />
                                                                    <asp:FileUpload ID="upl1" runat="server" CssClass="campos" Width="262px" />
                                                                </td>
                                                                <td class="rotulos" style="width: 292px">Foto<asp:RegularExpressionValidator ID="rgeFlu2" runat="server"
                                                                    ControlToValidate="upl2" Display="None"
                                                                    ErrorMessage="A imagem precisa ser jpg." SetFocusOnError="True"
                                                                    ValidationExpression="(.*\.jpe?g|.*\.JPE?G)"></asp:RegularExpressionValidator>
                                                                    <br />
                                                                    <asp:FileUpload ID="upl2" runat="server" CssClass="campos" Width="262px" />
                                                                </td>
                                                                <td class="rotulos">Foto<asp:RegularExpressionValidator ID="rgeFlu3" runat="server"
                                                                    ControlToValidate="upl3" Display="None"
                                                                    ErrorMessage="A imagem precisa ser jpg." SetFocusOnError="True"
                                                                    ValidationExpression="(.*\.jpe?g|.*\.JPE?G)"></asp:RegularExpressionValidator>
                                                                    <br />
                                                                    <asp:FileUpload ID="upl3" runat="server" CssClass="campos" Width="245px" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="rotulos" style="width: 291px">Descrição da foto<br />
                                                                    <asp:TextBox ID="txtFotoDescricao1" runat="server" CssClass="campos"
                                                                        Width="262px"></asp:TextBox>
                                                                </td>
                                                                <td class="rotulos" style="width: 292px">Descrição da foto<br />
                                                                    <asp:TextBox ID="txtFotoDescricao2" runat="server" CssClass="campos"
                                                                        Width="262px"></asp:TextBox>
                                                                </td>
                                                                <td class="rotulos">Descrição da foto<br />
                                                                    <asp:TextBox ID="txtFotoDescricao3" runat="server" CssClass="campos"
                                                                        Width="245px"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="rotulos" style="width: 291px; height: 70px;">Foto Destaque Categoria do Produto<asp:RegularExpressionValidator ID="rgeFlu4" runat="server"
                                                                    ControlToValidate="upl4" Display="None"
                                                                    ErrorMessage="A imagem precisa ser jpg." SetFocusOnError="True"
                                                                    ValidationExpression="(.*\.jpe?g|.*\.JPE?G)"></asp:RegularExpressionValidator>
                                                                    <br />
                                                                    <asp:FileUpload ID="upl4" runat="server" CssClass="campos" Width="262px" />
                                                                </td>
                                                                <td></td>
                                                                <td id="trGravarFoto" runat="server" style="text-align: right; padding: 10px 80px;">
                                                                    <uc:OneClickButton ID="btnGravarFotos" Text="Gravar Fotos" runat="server" CssClass="botao" ReplaceTitleTo="Aguarde..." OnClick="btnGravarFotos_Click" />

                                                                </td>
                                                            </tr>
                                                            <%--<tr id="trGravarFoto" runat="server">
                                                                <td colspan="3" style="text-align: right; padding: 10px 80px;"></td>
                                                            </tr>--%>
                                                            <tr>
                                                                <td colspan="3" id="trUpload">

                                                                    <div style="background: #f7f8fa; padding: 50px;">
                                                                        <!-- filer 3 -->
                                                                        <input type="file" multiple="multiple" name="files[]" id="input" />
                                                                    </div>

                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><img alt="" src="images/separador.gif" /></td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr class="rotulos">
                                    <td colspan="2">Foto 360 (link)
                                        <br />
                                        <asp:TextBox runat="server" CssClass="campos" Width="830px" ID="txtFoto360"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Button ID="btnAdd360Position" runat="server" Text="Configurar Foto 360" OnClientClick="$('html,body').scrollTop(0);popPanorama360.Show(); loadPanorama(); return false;" CausesValidation="false" Style="float: left;" />
                                    </td>
                                </tr>
                                <tr>
                                    <td><img alt="" src="images/separador.gif" /></td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>

                                        <table style="width: 100%">
                                            <tr class="rotulos">
                                                <td style="width: 160px;">
                                                    <b style="float: left;">Links de Videos, ex.: www.youtube.com/watch?v=AEd6USvIPNc</b><br />
                                                    <br />
                                                    <asp:TextBox runat="server" ID="txtLink" Width="80%" Text="" Style="float: left;"></asp:TextBox>
                                                    <asp:Button ID="btnAddLink" runat="server" Text="Adicionar Link" OnClick="btnAddLink_Click" CausesValidation="false" Style="float: left;" />
                                                </td>
                                            </tr>
                                            <tr class="rotulos">
                                                <td>
                                                    <asp:Repeater runat="server" ID="rptLinks">
                                                        <ItemTemplate>
                                                            Link (<%# Container.ItemIndex+1 %>):<br />
                                                            <div>
                                                                <asp:TextBox runat="server" ID="txtLink" Width="80%" Text='<%# Eval("Link") %>' Style="float: left;"></asp:TextBox>
                                                                <asp:Button ID="btnRemoverLink" runat="server" Text="<--Remover" CommandArgument='<%#Container.ItemIndex %>' OnCommand="btnRemoverLink_Command" CausesValidation="false" Style="float: left;" /><br />
                                                                <br />
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                            </tr>
                                        </table>
                                        <br />
                                    </td>
                                </tr>
                                    </table>
                                </dx:ContentControl>
                            </ContentCollection>
                        </dx:TabPage>
                        
                        <dx:TabPage Text="Personalização">
                            <ContentCollection>
                                <dx:ContentControl runat="server">
                                    <table style="width: 100%">
                                            <tr class="rotulos">
                                                <td style="width: 150px;">
                                                    <b>Produtos Personalização:</b>
                                                </td>
                                                <td>
                                                    <asp:Literal runat="server" ID="litQtdProdutoPersonalizacao"></asp:Literal>
                                                </td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr class="rotulos">
                                                <td style="padding-right: 5px; width: 100px;">ID do Produto:<br>
                                                    <asp:TextBox runat="server" ID="txtIdProdutoPersonalizacao" CssClass="campos"></asp:TextBox>
                                                </td>
                                                <td style="padding-right: 5px; width: 100px;">ID da Empresa<br />
                                                    <asp:TextBox runat="server" ID="txtIdEmpresaPersonalizacao" CssClass="campos"></asp:TextBox>
                                                </td>
                                                <td style="padding-right: 5px; width: 150px;">Tag Personalização:<br>
                                                    <asp:TextBox runat="server" ID="txtTagProdutoPersonalizacao" CssClass="campos"></asp:TextBox>
                                                </td>
                                                <td style="padding-right: 5px; width: 415px;">
                                                    <br />
                                                    <asp:Button runat="server" ID="btnProdutoPersonalizadoAdicionar" OnClick="btnProdutoPersonalizadoAdicionar_OnClick" Text="Adicionar Produto Personalização" ValidationGroup="produtoRelacionado" />
                                                </td>

                                            </tr>
                                            <tr class="rotulos">
                                                <td colspan="4" style="padding-top: 50px;">
                                                    <div style="overflow: hidden; margin-bottom: 10px; font-weight: bold; text-align: center;">
                                                        <div style="float: left; width: 80px;">
                                                            Foto
                                                        </div>
                                                        <div style="float: left; width: 100px; margin-right: 10px">
                                                            ID da Empresa
                                                        </div>
                                                        <div style="float: left; width: 100px; margin-right: 10px">
                                                            Complemento
                                                        </div>
                                                        <div style="float: left; width: 305px; margin-right: 10px;">
                                                            Nome
                                                        </div>
                                                        <div style="float: left; width: 100px; margin-right: 10px;">
                                                            Tag
                                                        </div>
                                                        <div style="float: left; width: 100px;">
                                                            Remover
                                                        </div>
                                                    </div>
                                                    <asp:ListView runat="server" ID="lstProdutoPersonalizadoRelacionado">
                                                        <ItemTemplate>
                                                            <div style="overflow: hidden; margin-bottom: 10px; text-align: center;">
                                                                <div style="float: left; width: 80px;">
                                                                    <asp:HiddenField runat="server" ID="hiddenProdutoId" Value='<%#Eval("produtoId") %>' />
                                                                    <asp:Image ID="fotoDestaque" runat="server" Height="70px" Width="70px" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings["caminhoCDN"] + "fotos/" + Eval("produtoId") + "/pequena_" + Eval("fotoDestaque") + ".jpg" %>' />
                                                                </div>
                                                                <div style="float: left; width: 100px; margin-right: 10px;">
                                                                    <asp:Label ID="lblIdDaEmpresa" runat="server" Text='<%# Bind("produtoIdDaEmpresa") %>'></asp:Label>
                                                                </div>
                                                                <div style="float: left; width: 100px; margin-right: 10px;">
                                                                    <asp:Label ID="lblComplementoId" runat="server" Text='<%# Bind("complementoIdDaEmpresa") %>'></asp:Label>&nbsp;
                                                                </div>
                                                                <div style="float: left; width: 305px; margin-right: 10px;">
                                                                    <asp:Label ID="lblProdutoNome" runat="server" Text='<%# Bind("produtoNome") %>'></asp:Label>
                                                                </div>
                                                                <div style="float: left; width: 100px; margin-right: 10px;">
                                                                    <asp:TextBox runat="server" ID="txtTagProdutoPersonalizado" Text='<%# Bind("tagProdutoPersonalizado") %>' Width="100" MaxLength="200"></asp:TextBox>
                                                                </div>
                                                                <div style="float: left; width: 100px;">
                                                                    <asp:Button runat="server" OnClientClick="return confirm('Deseja realmente remover este produto de personalização?')" ID="btnRemoverProdutoPersonalizacao" OnCommand="btnRemoverProdutoPersonalizacao_OnCommand" CommandArgument='<%# Bind("idPersonalizacao") %>' Text="Remover" Visible="False" />
                                                                    <asp:HiddenField runat="server" ID="hfIdPersonalizacao" Value='<%# Bind("idPersonalizacao") %>' />
                                                                    <asp:CheckBox runat="server" ID="ckbRemoverPersonalizacaoEmLote" Text="Remover" />
                                                                </div>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:ListView>
                                                    <div style="float: right;">
                                                        <asp:Button runat="server" ID="btnRemoverPersonalizacaoEmLote" Text="Remover Personalizações Marcadas" OnClick="btnRemoverPersonalizacaoEmLote_OnClick" Visible="False" OnClientClick="return confirm('Deseja realmente remover as personalizações marcadas?')" />
                                                    </div>
                                                    <div style="float: right; margin-right: 30px;">
                                                        <asp:Button runat="server" ID="btnSalvarDadosTags" Text="Salvar Dados Tags" OnClick="btnSalvarDadosTags_OnClick" Visible="False" />
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                </dx:ContentControl>
                            </ContentCollection>
                        </dx:TabPage>

                        <dx:TabPage Text="Combos">
                            <ContentCollection>
                                <dx:ContentControl runat="server">
                                    <table style="width: 100%">
                                            <tr class="rotulos">
                                                <td style="width: 160px;">
                                                    <b>Produtos Relacionados:</b>
                                                    <asp:Literal runat="server" ID="litProdutosRelacionados"></asp:Literal>
                                                </td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr class="rotulos">
                                                <td style="padding-right: 5px; width: 100px;">&nbsp;<br />
                                                    <asp:CheckBox runat="server" ID="chkProdutoRelacionadoValidar" Text="Validar Duplicado" Checked="True" />
                                                </td>
                                                <td style="padding-right: 5px; width: 100px;">&nbsp;<br />
                                                </td>
                                                <td style="padding-right: 5px; width: 100px;">&nbsp;<br />
                                                </td>
                                                <td>&nbsp;<br />
                                                </td>
                                            </tr>
                                            <tr class="rotulos">
                                                <td style="padding-right: 5px; width: 100px;">ID da Empresa:<br>
                                                    <asp:TextBox runat="server" ID="txtProdutoRelacionadoId" CssClass="campos" Width="530" TextMode="MultiLine" Height="130"></asp:TextBox>
                                                </td>
                                                <td style="padding-right: 5px; width: 100px;">Quantidade:<br>
                                                    <asp:TextBox runat="server" ID="txtProdutoRelacionadoQuantidade" CssClass="campos" Width="80"></asp:TextBox>
                                                </td>
                                                <td style="padding-right: 5px; width: 100px;">Desconto:<br>
                                                    <asp:TextBox runat="server" ID="txtProdutoRelacionadoDesconto" CssClass="campos" Text="0" Width="80">
                                                    </asp:TextBox>
                                                </td>
                                                <td>&nbsp;<br />
                                                    <asp:Button runat="server" ID="btnProdutoRelacionadoAdicionar" OnClick="btnProdutoRelacionadoAdicionar_OnClick" Text="Adicionar Produto" ValidationGroup="produtoRelacionado" />
                                                </td>
                                            </tr>
                                            <asp:ListView runat="server" ID="lstProdutoRelacionadoComplemento" OnItemDataBound="lstProdutoRelacionadoComplemento_OnItemDataBound" Visible="False">
                                                <ItemTemplate>
                                                    <tr class="rotulos">
                                                        <td>
                                                            <asp:Literal runat="server" ID="litProdutoRelacionadoId" Text='<%# Container.DataItem %>'></asp:Literal>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList runat="server" ID="ddlProdutoRelacionadoComplemento" />
                                                        </td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:ListView>
                                            <tr class="rotulos">
                                                <td colspan="4" style="padding-top: 50px;">
                                                    <div style="overflow: hidden; margin-bottom: 10px; font-weight: bold; text-align: center;">
                                                        <div style="float: left; width: 80px;">
                                                            Foto
                                                        </div>
                                                        <div style="float: left; width: 100px; margin-right: 10px">
                                                            ID da Empresa
                                                        </div>
                                                        <div style="float: left; width: 100px; margin-right: 10px">
                                                            Complemento
                                                        </div>
                                                        <div style="float: left; width: 305px; margin-right: 10px;">
                                                            Nome
                                                        </div>
                                                        <div style="float: left; width: 100px; margin-right: 10px;">
                                                            Desconto
                                                        </div>
                                                        <div style="float: left; width: 100px;">
                                                            Remover
                                                        </div>
                                                    </div>
                                                    <asp:ListView runat="server" ID="lstProdutosRelacionados">
                                                        <ItemTemplate>
                                                            <div style="overflow: hidden; margin-bottom: 10px; text-align: center;">
                                                                <div style="float: left; width: 80px;">
                                                                    <asp:HiddenField runat="server" ID="hiddenProdutoId" Value='<%#Eval("produtoId") %>' />
                                                                    <asp:Image ID="fotoDestaque" runat="server" Height="70px" Width="70px" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings["caminhoCDN"] + "fotos/" + Eval("produtoId") + "/pequena_" + Eval("fotoDestaque") + ".jpg" %>' />
                                                                </div>
                                                                <div style="float: left; width: 100px; margin-right: 10px;">
                                                                    <asp:Label ID="lblIdDaEmpresa" runat="server" Text='<%# Bind("produtoIdDaEmpresa") %>'></asp:Label>
                                                                </div>
                                                                <div style="float: left; width: 100px; margin-right: 10px;">
                                                                    <asp:Label ID="lblComplementoId" runat="server" Text='<%# Bind("complementoIdDaEmpresa") %>'></asp:Label>&nbsp;
                                                                </div>
                                                                <div style="float: left; width: 305px; margin-right: 10px;">
                                                                    <asp:Label ID="lblProdutoNome" runat="server" Text='<%# Bind("produtoNome") %>'></asp:Label>
                                                                </div>
                                                                <div style="float: left; width: 100px; margin-right: 10px;">
                                                                    <asp:Label ID="lblDesconto" runat="server" Text='<%# Bind("desconto") %>'></asp:Label>
                                                                </div>
                                                                <div style="float: left; width: 100px;">
                                                                    <asp:Button runat="server" OnClientClick="return confirm('Deseja realmente remover este produto relacionado?')" ID="btnRemoverProdutoRelacionado" OnCommand="btnRemoverProdutoRelacionado_OnCommand" CommandArgument='<%# Bind("idProdutoRelacionado") %>' Text="Remover" />
                                                                </div>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:ListView>
                                                </td>
                                            </tr>
                                            <tr class="rotulos">
                                                <td>
                                                    <b>Custo do Combo:</b>
                                                    <asp:Literal runat="server" ID="litCustoCombo"></asp:Literal>
                                                </td>
                                            </tr>
                                            <tr class="rotulos">
                                                <td>
                                                    <b>Peso do Combo:</b>
                                                    <asp:Literal runat="server" ID="litPesoCombo"></asp:Literal>
                                                </td>
                                            </tr>
                                            <tr class="rotulos">
                                                <td>
                                                    <b>Valor Total Geral (de):</b>
                                                    <asp:Literal runat="server" ID="litValorTotalGeral"></asp:Literal>
                                                </td>
                                            </tr>
                                            <tr class="rotulos">
                                                <td>
                                                    <b>Valor Total Venda (por):</b>
                                                    <asp:Literal runat="server" ID="litValorTotalVenda"></asp:Literal>
                                                </td>
                                            </tr>
                                        </table>
                                </dx:ContentControl>
                            </ContentCollection>
                        </dx:TabPage>


                        <dx:TabPage Text="Variações">
                            <ContentCollection>
                                <dx:ContentControl runat="server">   
                                    <table>                                             
                                        <tr class="rotulos">
                                            <td >
                                                   <span style="font-size: 18px; font-weight:bold;">
                                                       ID do Produto Pai: 
                                                       <asp:Label runat="server" ID="lblIdProdutoPai" Text=""></asp:Label>
                                                       <asp:TextBox runat="server" ID="txtProdutoPai" Text="" Visible="false" Width="60"></asp:TextBox>
                                                     </span>&nbsp;&nbsp;&nbsp;&nbsp;     
                                                     <asp:LinkButton ID="btnAlterarProdutoPai" runat="server" OnClick="btnAlterarProdutoPai_Click" Text="x Alterar Produto Pai" ForeColor="Red"></asp:LinkButton>
                                               
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:ListView ID="lstProdutoPai" runat="server" CellPadding="0" RepeatColumns="1">
                                                    <ItemTemplate>
                                                        <table cellpadding="0" cellspacing="0">   
                                                            <tr>
                                                            </tr>
                                                            <tr>
                                                                <td align="center" style="width: 85px">
                                                                    <asp:Image ID="imgFotos" runat="server"
                                                                        CssClass="slow-images"
                                                                        ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings["caminhoCDN"] + "fotos/" + Eval("produtoId") + "/pequena_" + Eval("fotoDestaque") + ".jpg" %>'
                                                                        Height="70px" Width="70px">
                                                                    </asp:Image>
                                                                </td>
                                                                <td class="rotulos">
                                                                    <b>Produto Pai: <br /></b>
                                                                    <%# Eval("produtoNome") %>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ItemTemplate>
                                                </asp:ListView>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellpadding="0" cellspacing="0" style="width: 890px">
                                                    <tr>
                                                        <td class="rotulos" style="padding-top: 20px">
                                                            <span style="padding-top: 30px; font-size: 18px; font-weight:bold;"><b>Especificações:</span>&nbsp;&nbsp;&nbsp;<asp:LinkButton runat="server" ID="lkbAdicionarEspecificacao" Text="+ adicionar especificação" OnClick="lkbAdicionarEspecificacao_Click"></asp:LinkButton><br />
                                                                <asp:DataList ID="dtlEspecificacoes" runat="server" DataSourceID="sqlEspecificacoes" CellSpacing="8" RepeatColumns="4" OnItemDataBound="dtlEspecificacoes_ItemDataBound">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblEspecificacaoId" runat="server" Text='<%# Bind("especificacaoId") %>' Visible="False"></asp:Label>
                                                                        <asp:Label ID="lblEspecificacao" runat="server" CssClass="rotulos" Text='<%# Bind("especificacaoNomeInterno") %>'></b></asp:Label>
                                                                        <br>
                                                                        <asp:DropDownList ID="drpEspecificacoes" runat="server" CssClass="campos">
                                                                        </asp:DropDownList>&nbsp;&nbsp;&nbsp;&nbsp;
                                                                        <asp:LinkButton ForeColor="Red" Text="x excluir" runat="server" ID="btnExcluirEspecificao" OnCommand="btnExcluirEspecificao_Command" CommandArgument='<%# Bind("especificacaoId") %>' OnClientClick="return confirm('Deseja realmente excluir esta especificação?');"></asp:LinkButton>
                                                                    </ItemTemplate>
                                                                </asp:DataList>
                                                                <asp:ObjectDataSource ID="sqlEspecificacoes" runat="server" SelectMethod="juncaoProdutoEspecificacaoSeleciona_PorProdutoId" TypeName="rnEspecificacao">
                                                                    <SelectParameters>
                                                                        <asp:QueryStringParameter Name="produtoId" QueryStringField="produtoId" Type="Int32" />
                                                                    </SelectParameters>
                                                                </asp:ObjectDataSource>
                                                            </b>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="padding-top: 30px;" class="rotulos">
                                                <span style=" font-size: 18px; font-weight:bold;">Variações de Produtos Cadastradas&nbsp;&nbsp;&nbsp;&nbsp;     </span>
                                                   <asp:LinkButton ID="lnkCadastrarProdutosFilho" Visible="false" runat="server" OnClick="lnkCadastrarProdutosFilho_Click" Text="+ Adicionar Variações de Produtos" ForeColor="Blue"></asp:LinkButton>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Panel ID="pnlGrid" runat="server" ScrollBars="Horizontal" Width="890px">
                                                    <dxwgv:ASPxGridView ID="grdProdutosRelacionados" runat="server" AutoGenerateColumns="False"
                                                        KeyFieldName="produtoId" Settings-ShowFilterBar="Visible"
                                                        Cursor="auto" ClientInstanceName="grd" EnableCallBacks="False">
                                                        <SettingsBehavior AutoFilterRowInputDelay="10200" ConfirmDelete="True"
                                                            AutoExpandAllGroups="True" ColumnResizeMode="Control" AllowGroup="False"
                                                            AllowFocusedRow="True" />
                                                        <SettingsText ConfirmDelete="Tem certeza que deseja excluir?"
                                                            EmptyDataRow="Nenhum registro encontrado."
                                                            GroupPanel="Arraste uma coluna aqui para agrupar." />
                                                        <SettingsPager Position="TopAndBottom" PageSize="300"
                                                            ShowDisabledButtons="False" AlwaysShowPager="True">
                                                            <Summary AllPagesText="Páginas: {0} - {1} ({2} items)"
                                                                Text="Página {0} de {1} ({2} registros encontrados)" />
                                                        </SettingsPager>
                                                        <Settings ShowFilterRow="True" ShowGroupedColumns="True" ShowFooter="True" />
                                                        <TotalSummary>
                                                            <dxwgv:ASPxSummaryItem DisplayFormat="0" FieldName="margem"
                                                                ShowInColumn="Margem" ShowInGroupFooterColumn="Margem"
                                                                SummaryType="Average" />
                                                        </TotalSummary>
                                                        <SettingsEditing EditFormColumnCount="4"
                                                            PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1"
                                                            PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter"
                                                            PopupEditFormWidth="700px" Mode="Inline" />
                                                        <Columns>
                                                            <dxwgv:GridViewDataTextColumn Caption="ID Produto" FieldName="produtoId"
                                                                ReadOnly="True" VisibleIndex="4" Width="70px">
                                                            </dxwgv:GridViewDataTextColumn>
                                                            <dxwgv:GridViewDataTextColumn Caption="Nome" FieldName="produtoNome"
                                                                VisibleIndex="5" Width="210px">
                                                                <Settings AutoFilterCondition="Contains" />
                                                            </dxwgv:GridViewDataTextColumn>
                                                            <dxwgv:GridViewDataComboBoxColumn Caption="Fornecedor" FieldName="produtoFornecedor" VisibleIndex="6" Width="160px">
                                                                <PropertiesComboBox DataSourceID="sqlFornecedor" TextField="fornecedorNome" IncrementalFilteringMode="StartsWith" DropDownStyle="DropDown"
                                                                    ValueField="fornecedorId" ValueType="System.String">
                                                                </PropertiesComboBox>
                                                            </dxwgv:GridViewDataComboBoxColumn>
                                                            <dxwgv:GridViewDataComboBoxColumn Caption="Marca" FieldName="produtoMarca" VisibleIndex="7" Width="160px" Visible="false">
                                                                <PropertiesComboBox DataSourceID="sqlMarca" TextField="marcaNome" IncrementalFilteringMode="StartsWith" DropDownStyle="DropDown"
                                                                    ValueField="marcaId" ValueType="System.String">
                                                                </PropertiesComboBox>
                                                            </dxwgv:GridViewDataComboBoxColumn>

                                                            <dxwgv:GridViewDataTextColumn Caption="Preço de Custo" Visible="False"
                                                                FieldName="produtoPrecoDeCusto" VisibleIndex="10" Width="100px">
                                                                <DataItemTemplate>
                                                                    <asp:TextBox ID="txtPrecoDeCusto" runat="server" BorderStyle="None" ReadOnly="True"
                                                                        CssClass="campos"
                                                                        Text='<%# Eval("produtoPrecoDeCusto", "{0:C}").Replace("R$","").Trim() %>'
                                                                        Width="100%"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ID="rqvpreco" runat="server"
                                                                        ControlToValidate="txtPrecoDeCusto" Display="Dynamic"
                                                                        ErrorMessage="Preencha o preço de custo." Font-Size="10px"
                                                                        SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                </DataItemTemplate>
                                                            </dxwgv:GridViewDataTextColumn>
                                                            <dxwgv:GridViewDataTextColumn Caption="Preço" FieldName="produtoPreco" Visible="False"
                                                                VisibleIndex="11" Width="90px">
                                                                <DataItemTemplate>
                                                                    <asp:TextBox ID="txtPreco" runat="server" BorderStyle="None" CssClass="campos"
                                                                        Text='<%# Eval("produtoPreco", "{0:C}").Replace("R$","").Trim() %>'
                                                                        Width="100%"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ID="rqvpreco" runat="server"
                                                                        ControlToValidate="txtPreco" Display="Dynamic" ErrorMessage="Preencha o preço."
                                                                        Font-Size="10px" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                </DataItemTemplate>
                                                            </dxwgv:GridViewDataTextColumn>
                                                            <dxwgv:GridViewDataTextColumn Caption="Preço Promocional" Visible="False"
                                                                FieldName="produtoPrecoPromocional" VisibleIndex="12" Width="110px">
                                                                <DataItemTemplate>
                                                                    <asp:TextBox ID="txtPrecoPromocional" runat="server" BorderStyle="None"
                                                                        CssClass="campos"
                                                                        Text='<%# Eval("produtoPrecoPromocional", "{0:C}").Replace("R$","").Trim() %>'
                                                                        Width="100%"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ID="rqvPrecoPromocional" runat="server"
                                                                        ControlToValidate="txtPrecoPromocional" Display="Dynamic"
                                                                        ErrorMessage="Preencha o preço promocional." Font-Size="10px"
                                                                        SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                </DataItemTemplate>
                                                            </dxwgv:GridViewDataTextColumn>                                                           

                                                            <dxwgv:GridViewDataTextColumn Caption="Ativo" FieldName="produtoAtivo" Visible="True"
                                                                UnboundType="Boolean" VisibleIndex="21" Width="60px">
                                                                <DataItemTemplate>
                                                                    <dxe:ASPxCheckBox ID="ckbAtivo" runat="server"
                                                                        Value='<%# Bind("produtoAtivo") %>' ValueChecked="True"
                                                                        ValueType="System.String" ValueUnchecked="False">
                                                                    </dxe:ASPxCheckBox>
                                                                </DataItemTemplate>
                                                            </dxwgv:GridViewDataTextColumn>
                                                            <dxwgv:GridViewDataHyperLinkColumn Caption="Alterar" VisibleIndex="1"
                                                                Width="45px">
                                                                <DataItemTemplate>
                                                                    <asp:HyperLink ID="hplNovo" runat="server" ImageUrl="images/btEditar-v2.jpg"
                                                                        NavigateUrl='<%# "produtoAlt.aspx?produtoId=" + Eval("produtoId") + "&produtoPaiId=0&acao=alterar" %>'>Alterar</asp:HyperLink>
                                                                </DataItemTemplate>
                                                                <CellStyle HorizontalAlign="Center">
                                                                </CellStyle>
                                                            </dxwgv:GridViewDataHyperLinkColumn>
                                                        </Columns>
                                                        <ClientSideEvents ContextMenu="grid_ContextMenu" />
                                                        <SettingsCustomizationWindow Enabled="True" />
                                                    </dxwgv:ASPxGridView>
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                    </table>
                                </dx:ContentControl>
                            </ContentCollection>
                        </dx:TabPage>

                    </TabPages>
                </dx:ASPxPageControl>
            </td>
        </tr>
        <tr>
            <td>
                <img alt="" src="images/separador.gif" />
            </td>
        </tr>
        <tr>
            <td>                
                <table cellpadding="0" cellspacing="0" style="width: 900px">
                    <tr>
                        <td align="right" style="padding-left: 15px">
                            <asp:Button runat="server" ID="btnExcluirProduto" Text="Excluir" OnClientClick="return confirm('Confirma a exclusão do produto?');"
                                OnClick="btnExcluirProduto_OnClick"
                                Style="font-family: Tahoma; float: left;" CssClass="botaoExcluir" />
                            <uc:OneClickButton ID="btn" runat="server" BackColor="Green" CssClass="botaoGravar" ReplaceTitleTo="Aguarde..." OnClick="btn_Click" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:ValidationSummary ID="vlds" runat="server" ShowMessageBox="True" ShowSummary="False"
                    DisplayMode="List" />
            </td>
        </tr>

        <tr style="display:none;">
            <td>
                <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" style="width: 834px">                                
                                <tr class="rotulos">
                                    <td style="display: none;">Disponibilidade em estoque<br />
                                        <asp:TextBox runat="server" CssClass="campos" Width="830px" ID="txtDisponibilidadeEmEstoque"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table cellpadding="0" cellspacing="0" style="width: 834px">
                                            <tr class="rotulos">
                                               <td style="width: 213px; display: none;">Quantidade Mínima
                                                    para Atacado<asp:RequiredFieldValidator runat="server" ControlToValidate="txtProdutoQuantidadeMininaParaAtacado"
                                                        ErrorMessage="Preencha quantidade m&#237;nima para atacado.." Display="None"
                                                        SetFocusOnError="True" ID="rqvQuantidadeMinimaAtacado"></asp:RequiredFieldValidator>
                                                    <br>
                                                    <asp:TextBox runat="server" CssClass="campos" Width="54px" ID="txtProdutoQuantidadeMininaParaAtacado">0</asp:TextBox>
                                                </td>
                                                <td style="display: none;">Legenda para Atacado<br />
                                                    <asp:TextBox runat="server" TextMode="MultiLine" CssClass="campos" Height="34px"
                                                        Width="190px" ID="txtProdutoLegendaAtacado"></asp:TextBox>
                                                </td>
                                                <td style="width: 213px">&nbsp;</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td runat="server"></td>
                                </tr>
                                <tr style="display: none;">
                                    <td><img alt="" src="images/separador.gif" /></td>
                                </tr>
                                <tr id="trGarantiaEstendida" runat="server" style="display: none;">
                                    <td class="rotulos">
                                        <b>Garantia Estendida</b><asp:DataList ID="dtlGarantias" runat="server" CellPadding="0"
                                            DataSourceID="sqlGarantias" RepeatColumns="4" OnItemDataBound="dtlGarantias_ItemDataBound">
                                            <ItemTemplate>
                                                <table cellpadding="0" cellspacing="0" style="width: 203px">
                                                    <tr>
                                                        <td>
                                                            <table cellpadding="0" cellspacing="0" style="width: 202px">
                                                                <tr class="rotulos">
                                                                    <td style="width: 148px">Tempo<br />
                                                                        <asp:TextBox ID="txtTempo" runat="server" CssClass="campos" Text='<%# Bind("garantiaNome") %>'
                                                                            Width="143px"></asp:TextBox>
                                                                    </td>
                                                                    <td>Valor<br />
                                                                        <asp:TextBox ID="txtValor" runat="server" CssClass="campos" Text='<%# Eval("garantiaValor", "{0:C}").Replace("R$","").Trim() %>'
                                                                            Width="48px"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:DataList>
                                        <asp:LinqDataSource ID="sqlGarantias1" runat="server" ContextTypeName="tbGarantiasDataContext"
                                            TableName="tbProdutoGarantiaEstendidas" Where="produtoId == @produtoId">
                                            <WhereParameters>
                                                <asp:QueryStringParameter Name="produtoId" QueryStringField="produtoId" Type="Int32" />
                                            </WhereParameters>
                                        </asp:LinqDataSource>
                                        <asp:SqlDataSource ID="sqlGarantias" runat="server"
                                            ConnectionString="<%$ ConnectionStrings:connectionString %>"
                                            SelectCommand="SELECT * FROM [tbProdutoGarantiaEstendida] WHERE ([produtoId] = @produtoId)">
                                            <SelectParameters>
                                                <asp:QueryStringParameter Name="produtoId" QueryStringField="produtoId"
                                                    Type="Int32" />
                                            </SelectParameters>
                                        </asp:SqlDataSource>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <img alt="" src="images/separador.gif" />
                                    </td>
                                </tr>
                                
                                <tr style="display: none;">
                                    <td runat="server">
                                        <table cellpadding="0" cellspacing="0" style="width: 834px">
                                            <tr>
                                                <td>
                                                    <img alt="" src="images/separador.gif" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="rotulos">
                                                    <b>Tabela de Descrição<br />
                                                        <asp:Panel ID="pnlTabelaDescricao" runat="server" ScrollBars="Vertical" Height="160">
                                                            <asp:CheckBoxList ID="chkTabelaDescricoes" AutoPostBack="True" OnSelectedIndexChanged="chkTabelaDescricoes_OnSelectedIndexChanged" runat="server" DataSourceID="sqlTabelaDescricoes" DataTextField="nomeInterno" DataValueField="idDescricaoTabelaProdutoGrupo" OnDataBound="chkTabelaDescricoes_OnDataBound" Font-Size="12px"></asp:CheckBoxList>
                                                            <asp:SqlDataSource ID="sqlTabelaDescricoes" runat="server" ConnectionString="<%$ ConnectionStrings:connectionString %>" SelectCommand="SELECT idDescricaoTabelaProdutoGrupo, nomeInterno, idSite FROM tbDescricaoTabelaProdutoGrupo WHERE idSite = 1">
                                                            </asp:SqlDataSource>
                                                        </asp:Panel>
                                                    </b>
                                                </td>
                                            </tr>
                                            <asp:ListView runat="server" ID="lstTabelasDescricao" OnItemDataBound="lstTabelasDescricao_OnItemDataBound">
                                                <ItemTemplate>
                                                    <tr>
                                                        <td style="padding-top: 5px;">
                                                            <table class="templateDetalheTabela" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td class="templateDetalheTituloBloco" colspan="3"><%# Eval("nomeAmigavel") %></td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="templateDetalheTituloBlocoDivisor"></td>
                                                                </tr>
                                                                <tr class="templateDetalheTexto">
                                                                    <td class="templateDetalheNome" width="138">Produto </td>
                                                                    <td class="templateDetalheQuantidade" width="32">Qtd. </td>
                                                                    <td class="templateDetalheDescricao" width="456">Descrição </td>
                                                                </tr>
                                                                <asp:ListView runat="server" ID="lstProdutosSimulacao" OnItemDataBound="lstProdutosSimulacao_OnItemDataBound">
                                                                    <ItemTemplate>
                                                                        <tr class="templateDetalheTitulo">
                                                                            <td class="templateDetalheNome">
                                                                                <asp:Literal runat="server" ID="litNomeAmigavel"></asp:Literal>
                                                                            </td>
                                                                            <td class="templateDetalheQuantidade">
                                                                                <br>
                                                                            </td>
                                                                            <td class="templateDetalheDescricao">
                                                                                <br>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="templateDetalheNomeDetalhe">
                                                                                <br>
                                                                            </td>
                                                                            <td class="templateDetalheQuantidadeDetalhe">
                                                                                <asp:ListView runat="server" ID="lstProdutosSimulacaoItensQuantidades">
                                                                                    <ItemTemplate>
                                                                                        <div>
                                                                                            <%# Eval("quantidade") %>
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                </asp:ListView>
                                                                            </td>
                                                                            <td class="templateDetalheDescricaoDetalhe">
                                                                                <asp:ListView runat="server" ID="lstProdutosSimulacaoItensDescricoes">
                                                                                    <ItemTemplate>
                                                                                        <div>
                                                                                            <%# Eval("descricao") %>
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                </asp:ListView>
                                                                            </td>
                                                                        </tr>
                                                                    </ItemTemplate>
                                                                </asp:ListView>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:ListView>
                                        </table>
                                    </td>
                                </tr>
                                
                                <asp:Panel runat="server" ID="pnComentariosFacebook" Visible="False">
                                    <tr>
                                        <td>
                                            <img alt="" src="images/separador.gif" />&nbsp;
                                        <td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table>
                                                <tr class="rotulos" style="font-weight: bold;">
                                                    <td>COMENTÁRIOS DO FACEBOOK</td>
                                                    <td></td>
                                                </tr>
                                                <tr class="rotulos">
                                                    <td style="padding-right: 5px; width: 100px;">ID do Post:<br>
                                                        <asp:TextBox runat="server" ID="txtComentariosFaceIdPost" CssClass="campos" Width="530"></asp:TextBox>
                                                    </td>
                                                    <td>&nbsp;<br />
                                                        <asp:Button runat="server" ID="btnAdicionarComentariosFace" OnClick="btnAdicionarComentariosFace_OnClick" Text="Adicionar Comentario" ValidationGroup="produtoComentariosFace" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr class="rotulos">
                                                    <td>Post</td>
                                                    <td>Remover</td>
                                                </tr>
                                                <asp:ListView runat="server" ID="lstComentariosFace">
                                                    <ItemTemplate>
                                                        <tr class="rotulos">
                                                            <td>
                                                                <%#Eval("idPostagem") %>
                                                            </td>
                                                            <td>
                                                                <asp:Button runat="server" OnClientClick="return confirm('Deseja realmente remover este produto relacionado?')" ID="btnRemoverComentarioFace" OnCommand="btnRemoverComentarioFace_OnCommand" CommandArgument='<%# Bind("idProdutoComentarioFacebook") %>' Text="Remover" />
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:ListView>
                                            </table>
                                        </td>
                                    </tr>
                                </asp:Panel>
                                
                                <tr>
                                    <td>
                                        <img alt="" src="images/separador.gif" />
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>
                                        <img alt="" src="images/separador.gif" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
        </tr>
    </table>

    
    <dx:ASPxPopupControl ID="popAdicionarEspecificacao" runat="server" CloseAction="CloseButton" Modal="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
        ClientInstanceName="popAdicionarEspecificacao" HeaderText="Adicionar Especificação ao Produto" AllowDragging="True" 
        PopupAnimationType="None" EnableViewState="True" Height="200" AutoUpdatePosition="True">
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl13" runat="server">
                <asp:Panel runat="server" ID="pnAdicionarEspecificacao">
                    <table style="width: 100%">
                        <tr class="rotulos">
                            <td>
                                <b>Grupo de Especificação:</b><br />
                                <asp:DropDownList runat="server" ID="ddlEspecificacaoGrupo" AppendDataBoundItems="true" DataValueField="especificacaoId" DataTextField="especificacaoNomeInterno" AutoPostBack="true" OnSelectedIndexChanged="ddlEspecificacaoGrupo_SelectedIndexChanged">
                                    <Items>
                                        <asp:ListItem Text="Selecione" Value="0"></asp:ListItem>
                                    </Items>
                                </asp:DropDownList>

                            </td>
                            <td>
                                <b>Especificação:</b><br />
                                <asp:DropDownList runat="server" ID="ddlEspecificacao" AppendDataBoundItems="true" DataValueField="especificacaoItensId" DataTextField="especificacaoItenNome">
                                    <Items>
                                        <asp:ListItem Text="Selecione" Value="0"></asp:ListItem>
                                    </Items>
                                </asp:DropDownList>
                            </td>
                            <td>
                                <b>&nbsp;</b><br />
                                <asp:Button runat="server" ID="btnGravarEspecificacao" OnClick="btnGravarEspecificacao_Click" Text="Gravar Especificação" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Panel runat="server" ID="pnAdicionarEspecificacaoProdutos" Visible="false">
                    <div style="overflow: auto; height: 400px">
                        <table style="width: 100%">
                            <asp:ListView runat="server" ID ="lstAdicionaEspecificacaoProdutos" OnItemDataBound="lstAdicionaEspecificacaoProdutos_ItemDataBound">
                                <ItemTemplate>
                                    <tr>
                                        <td>
                                            <asp:HiddenField runat="server" ID="hdfProdutoid" Value='<%# Eval("produtoId") %>' />
                                            <b><%# Eval("produtoNome") %></b><br />
                                            <asp:ListView runat="server" ID="lstEspecificacaoAtual">
                                                <ItemTemplate>
                                                    <b><%# Eval("especificacaoNomeInterno") %>:</b> <%# Eval("especificacaoItenNome") %><br />
                                                </ItemTemplate>
                                            </asp:ListView>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-bottom: 30px;">
                                            <b>Especificação:</b>s
                                            <asp:DropDownList runat="server" ID="ddlEspecificacaoItem" AppendDataBoundItems="true" DataValueField="especificacaoItensId" DataTextField="especificacaoItenNome">
                                                <Items>
                                                    <asp:ListItem Text="Selecionar" Value="0"></asp:ListItem>
                                                </Items>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:ListView>
                            <tr class="rotulos">
                                <td>
                                    <asp:Button runat="server" ID="btnGravarEspecificacaoProdutos" OnClick="btnGravarEspecificacaoProdutos_Click" Text="Gravar Especificação" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </asp:Panel>
            </dx:PopupControlContentControl>
        </ContentCollection>
     </dx:ASPxPopupControl>


    <dx:ASPxPopupControl ID="popAdicionarProduto" runat="server" CloseAction="CloseButton" Modal="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
        ClientInstanceName="popAdicionarProduto" HeaderText="Adicionar Variação de Produtos" AllowDragging="True" 
        PopupAnimationType="None" EnableViewState="True" MaxHeight="700" AutoUpdatePosition="True" Width="900">
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server">
                <div style="overflow: auto; height: 400px">
                    <table style="width: 100%">
                        <tr class="rotulos">
                            <td>
                                <asp:ListView runat="server" ID="lstEspecificacoesAdicionarProdutos" OnItemDataBound="lstEspecificacoesAdicionarProdutos_ItemDataBound">
                                    <ItemTemplate>
                                        <div style="padding-bottom: 20px;">
                                            <asp:HiddenField Value='<%# Eval("especificacaoId") %>' ID="hdfEspecificacaoId" runat="server" />
                                            <b><%# Eval("especificacaoNomeInterno") %>:</b><br />
                                            <asp:CheckBoxList CellSpacing="10" RepeatColumns="3" DataValueField="especificacaoItensId" DataTextField="especificacaoItenNome" runat="server" ID="cklEspecificacaoItens"></asp:CheckBoxList>
                                        </div>
                                    </ItemTemplate>
                                </asp:ListView>
                            </td>
                        </tr>
                    </table>
                </div>
                <div>
                    <asp:Button runat="server" ID="btnAdicionarVariacaoProdutos" OnClick="btnAdicionarVariacaoProdutos_Click" Text="Adicionar Produtos" />
                </div>
            </dx:PopupControlContentControl>
        </ContentCollection>
     </dx:ASPxPopupControl>

    <dx:ASPxPopupControl ID="popPanorama360" runat="server" CloseAction="CloseButton" Modal="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
        ClientInstanceName="popPanorama360" HeaderText="Foto Panoramica do Produto" AllowDragging="True" 
        PopupAnimationType="None" EnableViewState="True" MaxHeight="700" AutoUpdatePosition="True" Width="900">
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl2" runat="server">
                <div>Escolha a posição correta para ser exibido no site e clique em salvar.</div>
                <div id="panorama" style="width:100%;height:400px;"></div>
                <div id="hotspot-popup">
                    <table>
                        <tr>
                            <td>Id do produto</td>
                            <td><input type="text" id="hotspot_idproduto" onblur="IdProduto_onBlur()" maxlength="10"/></td>
                        </tr>
                        <tr>
                            <td>Nome</td>
                            <td><input type="text" id="hotspot_texto" maxlength="250" readonly/></td>
                        </tr>
                        <tr>
                            <td>URL</td>
                            <td>
                                <input type="text" id="hotspot_url" maxlength="250" style="display:none" readonly/>
                                <div class="see"></div> 
                            </td>
                        </tr>
                    </table>
                    <input type="button" onclick="cancelHotSpot()" value="Cancelar"/>
                    <input type="button" onclick="deleteHotSpot()" id="delHotSpot" value="Excluir"/>
                    <input type="button" onclick="saveHotSpot()" id="addHotSpot" value="Salvar" />
                </div>
                <ul class="popup360bottom">
                    <li><input type="button" style="float:left; margin:15px; padding:5px;" onClick="openHotspotPopup();" value="Adicionar hotspot"></input></li>
                    <li>
                        <div style="width:100%;"> </div>
                    </li>
                    <li><input type="button" style="float:right; margin:15px; padding:5px;" onClick="setPanoramaPosition(); return false;" value="Salvar Posição Inicial"></input></li>
                </ul>
            </dx:PopupControlContentControl>
        </ContentCollection>
     </dx:ASPxPopupControl>
    <asp:HiddenField runat="server" ID="hdnUrlFoto360Mini"></asp:HiddenField>
    <asp:HiddenField runat="server" ID="hdnFoto360Yaw"   ClientIDMode="Static"></asp:HiddenField>
    <asp:HiddenField runat="server" ID="hdnFoto360Hfov"  ClientIDMode="Static"></asp:HiddenField>
    <asp:HiddenField runat="server" ID="hdnFoto360Pitch" ClientIDMode="Static"></asp:HiddenField>
    <asp:HiddenField runat="server" ID="hdnHotSpotJson"  ClientIDMode="Static"></asp:HiddenField>
    <script language="javascript">

    </script>

</asp:Content>
