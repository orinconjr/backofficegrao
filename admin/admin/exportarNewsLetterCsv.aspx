﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="exportarNewsLetterCsv.aspx.cs" Theme="Glass" Inherits="admin_exportarNewsLetterCsv" %>

<%@ Register TagPrefix="dxwgv" Namespace="DevExpress.Web.ASPxGridView" Assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>
<%@ Register TagPrefix="dxwgv" Namespace="DevExpress.Web.ASPxGridView.Export" Assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" runat="Server">

    <table cellpadding="0" cellspacing="0" style="width: 920px">
        <tr>
            <td class="tituloPaginas" valign="top">
                <asp:Label ID="lblAcao" runat="server">Exportar NewsLetter CSV</asp:Label>
            </td>
        </tr>
        <tr>
            <td>


                <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">

                    <tr>
                        <td align="right">
                            <table cellpadding="0" cellspacing="0" style="width: 834px">
                                <tr>
                                    <td align="right">&nbsp;</td>
                                    <td height="38"
                                        style="background-image: url('images/bkExportar.jpg'); padding-bottom: 1px;"
                                        valign="bottom" width="231">
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 97px">&nbsp;</td>
                                                <td style="width: 33px">
                                                    <asp:ImageButton ID="btPdf" runat="server" ImageUrl="~/admin/images/btPdf.jpg"
                                                        Style="margin-left: 5px" OnClick="btPdf_Click" Visible="False" />
                                                </td>
                                                <td style="width: 31px">
                                                    <asp:ImageButton ID="btXsl" runat="server" ImageUrl="~/admin/images/btXls.jpg"
                                                        Style="margin-left: 5px" OnClick="btXsl_Click" Visible="False" />
                                                </td>
                                                <td style="width: 36px">
                                                    <asp:ImageButton ID="btRtf" runat="server" ImageUrl="~/admin/images/btRtf.jpg"
                                                        Style="margin-left: 5px" OnClick="btRtf_Click" Visible="False" />
                                                </td>
                                                <td valign="bottom">
                                                    <asp:ImageButton ID="btCsv" runat="server" ImageUrl="~/admin/images/btCsv.jpg"
                                                        Style="margin-left: 5px" OnClick="btCsv_Click" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="rotulos"></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>

                            <dxwgv:ASPxGridView ID="grd" runat="server" AutoGenerateColumns="False"
                                 KeyFieldName="Email" Cursor="auto"
                                ClientInstanceName="grd" EnableCallBacks="False">
                                <SettingsBehavior AutoFilterRowInputDelay="10200" ConfirmDelete="True"
                                    AutoExpandAllGroups="True" ColumnResizeMode="Control" AllowGroup="False"
                                    AllowFocusedRow="True" />
                                <SettingsText ConfirmDelete="Tem certeza que deseja excluir?"
                                    EmptyDataRow="Nenhum registro encontrado."
                                    GroupPanel="Arraste uma coluna aqui para agrupar." />
                                <SettingsPager Position="TopAndBottom" PageSize="100"
                                    ShowDisabledButtons="False" AlwaysShowPager="True">
                                    <Summary AllPagesText="Páginas: {0} - {1} ({2} items)"
                                        Text="Página {0} de {1} ({2} registros encontrados)" />
                                </SettingsPager>
                                <Settings ShowFilterRow="True" ShowGroupedColumns="True" ShowFooter="True" />
                                <TotalSummary>
                                    <dxwgv:ASPxSummaryItem DisplayFormat="0" FieldName="margem"
                                        ShowInColumn="Margem" ShowInGroupFooterColumn="Margem"
                                        SummaryType="Average" />
                                </TotalSummary>
                                <SettingsEditing EditFormColumnCount="4"
                                    PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1"
                                    PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter"
                                    PopupEditFormWidth="700px" Mode="Inline" />
                                <Columns>
                                    <dxwgv:GridViewDataTextColumn Name="Email" Caption="Email" FieldName="Email" VisibleIndex="0" Settings-AutoFilterCondition="Contains">
                                                 
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Name="Data" Caption="Data" FieldName="Data" VisibleIndex="1" Settings-AutoFilterCondition="Contains">
                                        <PropertiesTextEdit DisplayFormatString="yyyy-MM-dd" />  
                                    </dxwgv:GridViewDataTextColumn>
                                </Columns>
                            </dxwgv:ASPxGridView>

                            <asp:SqlDataSource ConnectionString="<%$ ConnectionStrings:connectionString %>" runat="server" ID="sqlDS"
                                SelectCommand="SELECT Email, CONVERT(char(10),datacadastro,126) as Data FROM tbNewsletter"></asp:SqlDataSource>
                            <dxwgv:ASPxGridViewExporter ID="grdEx" runat="server" FileName="newsletter"
                                GridViewID="grd" Landscape="True" PreserveGroupRowStates="True">
                            </dxwgv:ASPxGridViewExporter>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                    <tr>
                        <td align="right">
                            <img src="images/btSalvar.jpg" onclick="grd.PerformCallback(this.value);" style="cursor: pointer;" /></td>
                    </tr>
                </table>


            </td>
        </tr>
    </table>
</asp:Content>

