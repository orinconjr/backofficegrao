﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" Theme="Glass" AutoEventWireup="true" CodeFile="pedidoFornecedorAvulso.aspx.cs" Inherits="admin_pedidoFornecedorAvulso" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxTimer" TagPrefix="dxt" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" Runat="Server">
    <table cellpadding="0" cellspacing="0" style="width: 920px">
    <tr>
        <td class="tituloPaginas" valign="top">
            <asp:Label ID="lblAcao" runat="server">Pedidos ao Fornecedor</asp:Label>
        </td>
    </tr>
        
    <tr>
        <td>
            
    <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
        <tr>
            <td>
                &nbsp;</td>
        </tr>

        <tr>
            <td>
                <dxt:ASPxTimer ID="timer1" runat="server" ClientInstanceName="myTimer" Enabled="False" Interval="1000" OnTick="timer1_OnTick">
                </dxt:ASPxTimer>
                <asp:HiddenField runat="server" ID="hiddenExportarId" />
                <dxwgv:ASPxGridView ID="grd" runat="server" AutoGenerateColumns="False" Width="834px" Cursor="auto" OnHtmlRowCreated="gridClientes_HtmlRowCreated" KeyFieldName="idPedidoFornecedor">
                    <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                    <Styles>
                        <Footer Font-Bold="True">
                        </Footer>
                    </Styles>
                    <SettingsText ConfirmDelete="Tem certeza que deseja excluir?" emptydatarow="Nenhum registro encontrado." />
                    <SettingsPager Position="TopAndBottom" PageSize="50" ShowDisabledButtons="False" AlwaysShowPager="True">
                        <Summary AllPagesText="Páginas: {0} - {1} ({2} items)" Text="Página {0} de {1} ({2} registros encontrados)" />
                    </SettingsPager>
                    <settings showfilterrow="True" ShowGroupButtons="False" ShowHeaderFilterButton="True" ShowFooter="True" />
                    <SettingsEditing EditFormColumnCount="4" PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1" PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter" PopupEditFormWidth="700px" />
                    <Columns>
                        <dxwgv:GridViewDataTextColumn Caption="ID" FieldName="idPedidoFornecedor" VisibleIndex="0" Width="50">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataDateColumn Caption="Data de Solicitação" FieldName="data" VisibleIndex="0">
                        </dxwgv:GridViewDataDateColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Fornecedor" FieldName="fornecedorNome" VisibleIndex="0">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Fornecedor ID" FieldName="fornecedorId" VisibleIndex="0">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Entregue" FieldName="entregue" Name="entregue" VisibleIndex="0" Width="30px">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Exportar" Name="exportar" VisibleIndex="0" Width="80">
                            <DataItemTemplate>
                                <asp:LinkButton runat="server" OnCommand="btnExportar_OnCommand" ID="btnExportar" CommandArgument='<%# Eval("idPedidoFornecedor") %>' EnableViewState="False">Exportar</asp:LinkButton>
                            </DataItemTemplate>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Reenviar" Name="reenviar" VisibleIndex="0" Width="80">
                            <DataItemTemplate>
                                <asp:LinkButton runat="server" OnCommand="btnReenviar_OnCommand" ID="btnReenviar" OnClientClick="return confirm('Deseja realmente reenviar este pedido ao Fornecedor?')"  CommandArgument='<%# Eval("idPedidoFornecedor") %>' EnableViewState="False">Reenviar para Fornecedor</asp:LinkButton>
                            </DataItemTemplate>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Confirmado" Name="confirmado" FieldName="confirmado" VisibleIndex="0" Width="80">
                            <DataItemTemplate>
                                <asp:LinkButton runat="server" Visible="False" OnClientClick="return confirm('Deseja realmente marcar o pedido como confirmado?')" OnCommand="btnConfirmar_OnCommand" ID="btnConfirmar" CommandArgument='<%# Eval("idPedidoFornecedor") %>' EnableViewState="False">Verificado com Fornecedor</asp:LinkButton>
                            </DataItemTemplate>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Ver" Name="ver" VisibleIndex="0" Width="60">
                            <DataItemTemplate>
                                <asp:LinkButton runat="server" OnCommand="btnVer_OnCommand" ID="btnVer" CommandArgument='<%# Eval("idPedidoFornecedor") %>' EnableViewState="False">Visualizar Pedidos</asp:LinkButton>
                            </DataItemTemplate>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Editar" Name="editar" VisibleIndex="0" Width="60">
                            <DataItemTemplate>
                                <asp:LinkButton runat="server" OnCommand="btnEditar_OnCommand" ID="btnEditar" CommandArgument='<%# Eval("idPedidoFornecedor") %>' EnableViewState="False">Checar Entrega</asp:LinkButton>
                            </DataItemTemplate>
                        </dxwgv:GridViewDataTextColumn>
                    </Columns>
                    <StylesEditors>
                        <Label Font-Bold="True">
                        </Label>
                    </StylesEditors>
                </dxwgv:ASPxGridView>                
                <asp:ObjectDataSource ID="sql" runat="server" SelectMethod="pedidosFornecedorIndividuais" TypeName="rnPedidos">
                </asp:ObjectDataSource>
            </td>
        </tr>
        <tr>
            <td style="padding-top: 10px; text-align: right;">
                <asp:Button runat="server" ID="btnNovo" Text="Criar Pedido Avulso" OnClick="btnNovo_OnClick"/>
            </td>
        </tr>
    </table>
        </td>
    </tr>
</table>
    </asp:Content>