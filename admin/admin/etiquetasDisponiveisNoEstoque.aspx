﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="etiquetasDisponiveisNoEstoque.aspx.cs" Inherits="admin_etiquetasDisponiveisNoEstoque" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" runat="Server">
    <style>
        .fieldsetatualizarprecos {
            width: 760px;
            float: left;
            clear: left;
            border-radius: 5px;
            margin-left: 25px;
        }

            .fieldsetatualizarprecos input[type=checkbox] {
                margin-top: 15px;
            }

            .fieldsetatualizarprecos label {
                margin-left: 5px;
                margin-right: 20px;
            }

        .meugrid {
            background-color: #F4FAFB;
            border: 0px none;
            /* border-collapse: separate !important; */
            overflow: hidden;
            font: 9pt Tahoma;
            color: #000;
            /* cursor: pointer;
            white-space: nowrap;*/
            font-weight: normal;
            text-align: left;
            width: 834px;
            border-collapse: collapse;
            float: left;
            clear: left;
            margin-top: 10px;
            margin-bottom: 10px;
            min-height: 500px;
            margin-left: 25px;
        }

            .meugrid th {
                background-image: url(../../App_Themes/Glass/GridView/gvHeaderBackground.gif);
                background-position: center top;
                background-repeat: repeat-x;
                background-color: #C0DDE0;
                overflow: hidden;
                height: 23px;
                line-height: 23px;
                padding: 4px 6px 5px;
                font-weight: normal;
                border: 1px solid #7EACB1;
                /* border-collapse: separate;*/
            }

            .meugrid td {
                overflow: hidden;
                height: 23px;
                line-height: 23px;
                padding: 4px 6px 5px;
                border: 1px solid #7EACB1;
            }

            .meugrid tr td:nth-child(4) {
                text-align: center;
            }

            .meugrid tr td:last-of-type {
                text-align: center;
            }

        .tabrotulos {
            width: 806px;
            margin: auto;
        }

        .materiaprima {
            width: 250px;
        }

            .materiaprima ul {
                margin: 0px;
                padding-left: 10px;
            }

                .materiaprima ul li {
                    max-width: 230px;
                    white-space: pre-wrap;
                }

        .meugrid tr td:last-of-type {
            text-align: center;
        }

        .nome {
            width: 237px;
        }

            .nome div {
                max-width: 230px;
                white-space: pre-wrap;
            }

        .rotulos td {
            font: bold 12px tahoma;
            padding-top: 20px;
            color: rgb(65, 105, 126);
            padding-left: 5px;
            padding-bottom: 5px;
        }

        /*****************************/
        .width150 {
        }

        .height150 {
        }

        .height80 {
        }

        .btn {
            -webkit-border-radius: 7px;
            -moz-border-radius: 7px;
            border-radius: 2px;
            font-family: Arial;
            color: #ffffff;
            font-size: 14px;
            background: #63c269;
            padding: 5px 10px 5px 10px;
            text-decoration: none;
            cursor: pointer;
            border: 0px;
        }

            .btn:hover {
                background: #a4dbab;
                text-decoration: none;
                color: black;
                font-weight: bold;
            }


        .marginTop5porcento {
            margin: 6%;
            font-size: 15px;
        }

        .quadroPrincipal {
            float: left;
            width: 150px;
            height: 155px;
            text-align: center;
            position: relative;
            font-family: monospace;
            border: 1px solid #B1B1B1;
            margin: 0 14px 5px 14px;
            border-radius: 15px;
        }

        .quadroInteriorQtds {
            margin-top: 5%;
            text-align: center;
            font-size: 30px;
        }

        .AlinharAoRodapeCentralizar {
            position: absolute;
            bottom: 5px;
            text-align: center;
            width: 150px;
        }
    </style>

    <div class="tituloPaginas" valign="top">
        Etiquetas em estoque - vinculadas a pedidos já enviados ou cancelados
      
    </div>
    <fieldset class="fieldsetatualizarprecos">
        <legend style="font-weight: bold;">Filtro</legend>

        <table class="tabrotulos">

            <tr class="rotulos">
                <td style="width: 180px;">Etiqueta:<br />
                    <asp:TextBox runat="server" ID="txtetiqueta"></asp:TextBox>
                </td>
                <td style="width: 180px;">Pedido:<br />
                    <asp:TextBox runat="server" ID="txtidpedido"></asp:TextBox>
                </td>

                <td>&nbsp;<br />
                    <asp:Button runat="server" ID="btnFiltrar" ValidationGroup="a" Text="Filtrar" OnClick="btnFiltrar_Click" />

                </td>
            </tr>
            <tr class="rotulos">
                <td colspan="3">
                    <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="Digite apenas números para a etiqueta" ControlToValidate="txtetiqueta" Type="Integer" ValidationGroup="a" Operator="DataTypeCheck"></asp:CompareValidator>
                    &nbsp;&nbsp;
                     <asp:CompareValidator ID="CompareValidator2" runat="server" ErrorMessage="Digite apenas números para o pedido" ControlToValidate="txtidpedido" Type="Integer" ValidationGroup="a" Operator="DataTypeCheck"></asp:CompareValidator>
                </td>
            </tr>
        </table>
    </fieldset>
    <div style="float: left; clear: left; width: 100%">
        <asp:GridView ID="GridView1" CssClass="meugrid" runat="server" Width="870px" DataKeyNames="idPedidoFornecedorItem" AutoGenerateColumns="False"
            AllowPaging="True" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowDataBound="GridView1_RowDataBound"
            OnRowCommand="GridView1_RowCommand" PageSize="50">
            <Columns>

                <asp:BoundField DataField="idPedidoFornecedorItem" ItemStyle-Width="50px" HeaderText="Etiqueta" />
                <asp:BoundField DataField="area" ItemStyle-Width="50px" HeaderText="Area" />
                <asp:BoundField DataField="rua" ItemStyle-Width="70px" HeaderText="Rua" />
                <asp:BoundField DataField="predio" ItemStyle-Width="50px" HeaderText="Predio" />
                <asp:BoundField DataField="andar" ItemStyle-Width="30px" HeaderText="Andar" />
                <asp:BoundField DataField="pedidoId" ItemStyle-Width="50px" HeaderText="Pedido" />
                <asp:BoundField DataField="itemPedidoId" ItemStyle-Width="50px" HeaderText="itemPedidoId" />
                <asp:BoundField DataField="produtoId" ItemStyle-Width="50px" HeaderText="produtoId" />
                <asp:BoundField DataField="produtoNome" HeaderText="produto Nome" />
                <asp:BoundField DataField="situacao" HeaderText="Status Pedido" />
                <asp:BoundField DataField="idCentroDistribuicao" HeaderText="CD" />


                <asp:TemplateField HeaderText="Editar" ShowHeader="False">
                    <ItemTemplate>
                        <a id="linkeditarpedido" target="_blank" href='<%#"pedido.aspx?pedidoid="+Eval("pedidoId") %>'>
                            <img src="images/btEditar-v2.jpg" alt="Editar" style="border-style: none;" title="Editar Pedido" />
                        </a>
                    </ItemTemplate>
                </asp:TemplateField>

            </Columns>
        </asp:GridView>
    </div>

    <div style="float: left; clear: left; width: 100%; font: 9pt tahoma; padding-left: 25px;">
        <strong>Qtde:</strong>&nbsp;<asp:Label ID="lblqtde" runat="server" Text=""></asp:Label>
    </div>
</asp:Content>

