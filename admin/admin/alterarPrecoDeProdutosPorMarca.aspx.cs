﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Microsoft.Win32;

public partial class admin_alterarPrecoDeProdutosPorMarca : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btAlterar_Click(object sender, EventArgs e)
    {
        foreach (GridViewRow row in grd.Rows)
        {
            Label lblProdutoId = ((Label)row.FindControl("lblProdutoId"));
            TextBox txtPrecoDeCusto = ((TextBox)row.FindControl("txtPrecoDeCusto"));
            TextBox txtIdDaEmpresa = ((TextBox)row.FindControl("txtIdDaEmpresa"));
            TextBox txtNome = ((TextBox)row.FindControl("txtNome"));
            TextBox txtPrecoPromocional = ((TextBox)row.FindControl("txtPrecoPromocional"));
            CheckBox ckbDesativar = ((CheckBox)row.FindControl("ckbDesativar"));

            string idDaEmpresa = txtIdDaEmpresa.Text;
            decimal precoDeCusto = decimal.Parse(txtPrecoDeCusto.Text);
            decimal precoPromocional = decimal.Parse(txtPrecoPromocional.Text);
            //decimal precoPromocional = precoDeCusto + (precoDeCusto * int.Parse(txtPorcentagem.Text) / 100) ;
            decimal preco = precoDeCusto + (precoDeCusto * int.Parse(txtPorcentagem.Text) / 100);
            //decimal preco = precoDeCusto * 2;

            char[] delimiterChars = { ',' };
            string[] arrayPreco = preco.ToString().Split(delimiterChars);

            string precoNovo = arrayPreco[0] + "." + "90";

            string produtoAtivo;
            if (ckbDesativar.Checked == true)
                produtoAtivo = "False";
            else
                produtoAtivo = "True";

            sqlProdutos.UpdateParameters.Clear();
            sqlProdutos.UpdateParameters.Add("produtoPrecoDeCusto", DbType.Decimal, precoDeCusto.ToString());
            sqlProdutos.UpdateParameters.Add("produtoPreco", DbType.String, precoNovo);
            sqlProdutos.UpdateParameters.Add("produtoIdDaEmpresa", DbType.String, idDaEmpresa);
            sqlProdutos.UpdateParameters.Add("produtoPrecoPromocional", DbType.Decimal, precoPromocional.ToString());
            sqlProdutos.UpdateParameters.Add("produtoNome", DbType.String, txtNome.Text);
            sqlProdutos.UpdateParameters.Add("produtoAtivo", DbType.String, produtoAtivo);
            sqlProdutos.UpdateParameters.Add("produtoId", lblProdutoId.Text.ToString());
            sqlProdutos.Update();
            
            int produtoId = 0;
            int.TryParse(lblProdutoId.Text, out produtoId);

            try
            {

                rnIntegracoes.atualizaProdutoExtra(produtoId);
            }
            catch (Exception) { }

            var data = new dbCommerceDataContext();
            var pedidoDc = new dbCommerceDataContext();
            var itensPedido = (from c in pedidoDc.tbPedidoFornecedorItems where c.idProduto == produtoId && c.brinde != true && c.entregue != true select c);
            foreach (var item in itensPedido)
            {
                item.custo = precoDeCusto;
            }
            pedidoDc.SubmitChanges();

            rnProdutos.AtualizaDescontoCombo(produtoId);
            //rnBuscaCloudSearch.AtualizarProduto(produtoId);
        }

        grd.DataBind();
    }

    protected void Button1_Click(object sender, EventArgs e)
    {

    }
}
