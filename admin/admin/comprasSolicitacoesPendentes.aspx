﻿<%@ Page Title="" Theme="Glass" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="comprasSolicitacoesPendentes.aspx.cs" Inherits="admin_comprasSolicitacoesPendentes" %>

<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxTimer" TagPrefix="dxt" %>
<%@ Register Assembly="CuteEditor" Namespace="CuteEditor" TagPrefix="CE" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" runat="Server">
<style type="text/css">
    .tabelaSolicitacao {
		width:100%; 
		border-collapse:collapse; 
    }
	.tabelaSolicitacao > tbody > tr > td{ 
		padding:7px; border:#eaeaea 1px solid;
	}
	.tabelaSolicitacao > tbody > tr{
		background: #eaeaea;
	}
	.tabelaSolicitacao > tbody > tr:nth-child(odd){ 
		background: #eaeaea;
	}
	.tabelaSolicitacao > tbody > tr:nth-child(even){
		background: #fff;
	}
</style>
    <script type="text/javascript">
        function toggle() {
            var divs = document.querySelectorAll("[id]");
            for (var i = 0, len = divs.length; i < len; i++) {
                var div = divs[i];
                if (div.id.indexOf("chkEntregue") > -1) {
                    div.checked = true;
                }
            }
        }
        Number.prototype.formatMoney = function (c, d, t) {
            var n = this, c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "," : d, t = t == undefined ? "." : t, s = n < 0 ? "-" : "", i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
            return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
        };
    </script>
     <script type="text/javascript">
         var startTime;
         function OnBeginCallback() {
             startTime = new Date();
         }
         function OnEndCallback() {
             var result = new Date() - startTime;
             result /= 1000;
             result = result.toString();
             if (result.length > 4)
                 result = result.substr(0, 4);
             time.SetText(result.toString() + " sec");
             label.SetText("Time to retrieve the last data:");
         }
    </script>
    <table cellpadding="0" cellspacing="0" style="width: 920px">

        <tr>
            <td class="tituloPaginas" valign="top">
                <asp:Label ID="lblAcao" runat="server">Solicitações de Compra Pendentes</asp:Label>
            </td>
        </tr>
        <tr class="rotulos">
            <td>&nbsp;</td>
        </tr>
        <asp:Panel ID="pnProdutosPendentes" runat="server">
            <tr class="rotulos">
                <td style="padding-left: 15px; padding-right: 15px;">
                    <b>Produtos sem cadastro</b>
                </td>
            </tr>
            <tr>
                <td>
                    <table style="padding-left: 15px; padding-right: 15px; width: 100%">
                        <tr class="rotulos" style="text-align: center;">
                            <td>
                                Nome cadastrado:
                            </td>
                            <td>
                                Produto:
                            </td>
                            <td>
                                Gravar
                            </td>
                        </tr>
                        <asp:ListView runat="server" ID="lstProdutosSemCadastro">
                            <ItemTemplate>
                                <tr class="rotulos">
                                    <td style="width: 500px;">
                                        <asp:Literal runat="server" ID="litProduto" Text='<%# Eval("produto") %>'></asp:Literal>&nbsp;
                                    </td>
                                    <td>
                                        <dxe:ASPxComboBox  DataSourceID="sqlComprasProduto" ID="ddlProduto" runat="server" ValueField="idComprasProduto" TextField="produto" Width="400px" DropDownStyle="DropDown">
                                        </dxe:ASPxComboBox>
                                    </td>
                                    <td>
                                        <asp:HiddenField runat="server" ID="hdfIdComprasSolicitacaoProduto" Value='<%# Eval("idComprasSolicitacaoProduto") %>' />
                                        <asp:CheckBox runat="server" ID="chkGravar" />
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:ListView>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="text-align: right;">
                    <asp:Button runat="server" ID="btnGravar" Text="Gravar Produtos" OnClick="btnGravar_OnClick" />
                </td>
            </tr>
        </asp:Panel>
        <asp:Panel ID="pnSolicitacoesPendentes" runat="server">
            <tr class="rotulos">
                <td style="padding-left: 15px; padding-right: 15px;">
                    <b>Produtos solicitados</b>
                </td>
            </tr>
            <tr>
                <td>
                    <table style="padding-left: 15px; padding-right: 15px; width: 100%" class="tabelaSolicitacao">
                        <tr class="rotulos" style="text-align: center;">
                            <td>
                                ID
                            </td>
                            <td>
                                Data
                            </td>
                            <td>
                                Solicitante
                            </td>
                            <td>
                                Motivo
                            </td>
                            <td>
                                Produto
                            </td>
                            <td>
                                Quantidade
                            </td>
                            <td>
                                Empresa
                            </td>
                            <td>
                                Fornecedor
                            </td>
                            <td>
                                Valor
                            </td>
                            <td>
                                Unidade
                            </td>
                            <td>
                                Gravar
                            </td>
                        </tr>
                        <asp:ListView runat="server" ID="lstProdutosSolicitacao" OnItemDataBound="lstProdutosSolicitacao_OnItemDataBound">
                            <ItemTemplate>
                                <tr class="rotulos">
                                    <td>
                                        <asp:Literal runat="server" ID="litId" Text='<%# Eval("idComprasSolicitacao") %>'></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:Literal runat="server" ID="litData" Text='<%# Convert.ToDateTime(Eval("dataSolicitacao")).ToShortDateString() %>'></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:Literal runat="server" ID="litNome" Text='<%# Eval("usuarioNome") %>'></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:Literal runat="server" ID="litMotivo" Text='<%# Eval("motivo") %>'></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:Literal runat="server" ID="litProduto" Text='<%# Eval("produto") %>'></asp:Literal>
                                    </td>
                                    <td>
                                        <dxe:ASPxTextBox ID="txtQuantidade" runat="server" Width="50" Text='<%# Eval("quantidade") %>'>
                                            <MaskSettings Mask="<0..999999g>" IncludeLiterals="None" />
                                            <ValidationSettings ErrorDisplayMode="ImageWithTooltip" />
                                        </dxe:ASPxTextBox>
                                    </td>
                                    <td>
                                        <dxe:ASPxComboBox  DataSourceID="sqlComprasEmpresa" ID="ddlEmpresa" runat="server" ValueField="idComprasEmpresa" TextField="empresa" Width="100px" DropDownStyle="DropDown" IncrementalFilteringMode="StartsWith">
                                        </dxe:ASPxComboBox>
                                    </td>
                                    <td>
                                        <dxe:ASPxComboBox ID="ddlFornecedor" runat="server" ValueField="idComprasFornecedor" 
                                            TextField="fornecedor" TextFormatString="{1}" ValueType="System.Int32" Width="100px" 
                                            EnableCallbackMode="True"  IncrementalFilteringMode="Contains"
                                            OnItemsRequestedByFilterCondition="ASPxComboBox_OnItemsRequestedByFilterCondition_SQL"
                                            OnItemRequestedByValue="ASPxComboBox_OnItemRequestedByValue_SQL"
                                             >
                                            <Columns>
                                                <dxe:ListBoxColumn Caption="idComprasFornecedor" FieldName="idComprasFornecedor" Width="50px" />
                                                <dxe:ListBoxColumn Caption="Nome" FieldName="fornecedor" Width="130px" />
                                                <dxe:ListBoxColumn Caption="Prazo" FieldName="prazo" Width="50px" />
                                                <dxe:ListBoxColumn Caption="Telefone" FieldName="telefone" Width="100px" />
                                                <dxe:ListBoxColumn Caption="Valor" FieldName="valor" Width="70px" />
                                                <dxe:ListBoxColumn Caption="Data" FieldName="dataValor" Width="70px" />
                                                <dxe:ListBoxColumn Caption="Unidade" FieldName="unidadeDeMedida" Width="70px" />
                                            </Columns>
                                            <ClientSideEvents BeginCallback="function(s, e) { OnBeginCallback(); }" EndCallback="function(s, e) { OnEndCallback(); } " />
                                        </dxe:ASPxComboBox>
                                    </td>
                                    <td>
                                        <dxe:ASPxTextBox ID="txtValor" runat="server" Width="80">
                                            <MaskSettings Mask="$<0..999999g>.<00..99>" IncludeLiterals="DecimalSymbol" />
                                            <ValidationSettings ErrorDisplayMode="ImageWithTooltip" />
                                        </dxe:ASPxTextBox>
                                    </td>
                                    <td>
                                        <dxe:ASPxComboBox DataSourceID="sqlUnidadesDeMedida" ID="ddlUnidadesDeMedida" runat="server" ValueField="idComprasUnidadesDeMedida" TextField="unidadeDeMedida" Width="80px" DropDownStyle="DropDown" IncrementalFilteringMode="StartsWith">
                                        </dxe:ASPxComboBox>
                                    </td>
                                    <td>
                                        <asp:HiddenField runat="server" ID="hdfIdComprasSolicitacaoProduto" Value='<%# Eval("idComprasSolicitacaoProduto") %>' />
                                        <asp:HiddenField runat="server" ID="hdfIdComprasProduto" Value='<%# Eval("idComprasProduto") %>' />
                                        <asp:CheckBox runat="server" ID="chkGravar" />
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:ListView>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="text-align: right;">
                    <asp:Button runat="server" ID="btnGravarOrdemCompra" Text="Gravar Ordem de Compra" OnClick="btnGravarOrdemCompra_OnClick" />
                </td>
            </tr>
            <asp:SqlDataSource ID="SqlDataSource1"  ConnectionString="<%$ ConnectionStrings:connectionString %>" runat="server" />
        </asp:Panel>
        <tr>
            <td>
                <table style="width: 100%;">
                    
                </table>
            </td>
        </tr>
        <tr>
            <td style="text-align: right;">&nbsp;
            </td>
        </tr>
        <tr>
            <td style="padding-top: 100px;">&nbsp;
            </td>
        </tr>
    </table>
    <asp:LinqDataSource ID="sqlComprasProduto" runat="server" ContextTypeName="dbCommerceDataContext" OrderBy="produto" TableName="tbComprasProdutos">
    </asp:LinqDataSource>
    <asp:LinqDataSource ID="sqlComprasFornecedor" runat="server" ContextTypeName="dbCommerceDataContext" OrderBy="fornecedor" TableName="tbComprasFornecedors">
    </asp:LinqDataSource>
    <asp:LinqDataSource ID="sqlComprasEmpresa" runat="server" ContextTypeName="dbCommerceDataContext" OrderBy="empresa" TableName="tbComprasEmpresas">
    </asp:LinqDataSource>
    <asp:LinqDataSource ID="sqlUnidadesDeMedida" runat="server" ContextTypeName="dbCommerceDataContext" TableName="tbComprasUnidadesDeMedidas">
    </asp:LinqDataSource>
</asp:Content>