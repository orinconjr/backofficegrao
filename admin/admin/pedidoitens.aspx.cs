﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_pedidoitens : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void dtlItensPedido_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            int produtoId = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "produtoId"));

            #region fotos
            Image fotoDestaque = (Image)e.Item.FindControl("fotoDestaque");
            if (rnFotos.produtoFotoDestaqueSeleciona_PorProdutoId(produtoId).Tables[0].Rows.Count > 0)
                fotoDestaque.ImageUrl = ConfigurationManager.AppSettings["caminhoVirtual"] + "fotos/" + produtoId + "/pequena_" + rnFotos.produtoFotoDestaqueSeleciona_PorProdutoId(produtoId).Tables[0].Rows[0]["produtoFoto"].ToString() + ".jpg";
            else
                fotoDestaque.ImageUrl = ConfigurationManager.AppSettings["caminhoVirtual"] + "fotos/naoExiste/pequena.jpg";
            #endregion
        }
    }
}