﻿<%@ Page Title="" Language="C#" Theme="Glass" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="boletosConfirmar.aspx.cs" Inherits="admin_boletosConfirmar" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxTimer" TagPrefix="dxt" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" Runat="Server">
    <table cellpadding="0" cellspacing="0" style="width: 920px">
        <tr>
            <td class="tituloPaginas" valign="top">
                <asp:Label ID="lblAcao" runat="server">Confirmar Boletos</asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                        
                    <asp:Panel runat="server" ID="pnAtualizar">
                        <tr class="rotulos">
                            <td>
                                Arquivo:
                            </td>
                        </tr>
                        <tr class="rotulos">
                            <td>
                                <asp:FileUpload ID="fluArquivo" runat="server" />
                            </td>
                        </tr>
                        <tr class="rotulos">
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr class="rotulos">
                            <td>
                                <asp:ImageButton ID="btnSalvar" runat="server" ImageAlign="AbsMiddle" ImageUrl="images/btSalvarPeq1.jpg" onclick="btnSalvar_Click" />
                            </td>
                        </tr>
                    </asp:Panel>
                    <asp:Panel runat="server" ID="pnStatus" Visible="False">
                        <tr>
                            <td>
                                <dxwgv:ASPxGridView ID="grd" runat="server" AutoGenerateColumns="False" Width="834px" Cursor="auto" OnHtmlRowCreated="gridClientes_HtmlRowCreated" KeyFieldName="pedidoId">
                                    <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                                    <Styles>
                                        <Footer Font-Bold="True">
                                        </Footer>
                                    </Styles>
                                    <SettingsText ConfirmDelete="Tem certeza que deseja excluir?" emptydatarow="Nenhum registro encontrado." />
                                    <SettingsPager Position="TopAndBottom" PageSize="1500" ShowDisabledButtons="False" AlwaysShowPager="True">
                                        <Summary AllPagesText="Páginas: {0} - {1} ({2} items)" Text="Página {0} de {1} ({2} registros encontrados)" />
                                    </SettingsPager>
                                    <settings showfilterrow="True" ShowGroupButtons="False" ShowHeaderFilterButton="True" ShowFooter="True" />
                                    <SettingsEditing EditFormColumnCount="4" PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1" PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter" PopupEditFormWidth="700px" />
                                    <Columns>
                                        <dxwgv:GridViewDataTextColumn Caption="Pedido" FieldName="pedidoId" VisibleIndex="0" Width="50">
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn Caption="Situação" FieldName="status" VisibleIndex="0">
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn Caption="Valor Pago" FieldName="valorPago" 
                                            VisibleIndex="2" Width="100px" Settings-AutoFilterCondition="Contains" Settings-FilterMode="DisplayText">
                                            <PropertiesTextEdit DisplayFormatString="c">
                                            </PropertiesTextEdit>
                                                <Settings FilterMode="DisplayText" AutoFilterCondition="Contains"></Settings>
                                            <EditFormSettings Visible="False" />
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataHyperLinkColumn Caption="Editar" FieldName="pedidoId" 
                                            VisibleIndex="7" Width="30px">
                                            <PropertiesHyperLinkEdit ImageUrl="~/admin/images/btEditar.jpg" 
                                                NavigateUrlFormatString="pedido.aspx?pedidoId={0}" Target="_blank">
                                            </PropertiesHyperLinkEdit>
                                            <Settings AllowAutoFilter="False" />
                                            <HeaderTemplate>
                                                <img alt="" src="images/legendaEditar.jpg" />
                                            </HeaderTemplate>
                                        </dxwgv:GridViewDataHyperLinkColumn>
                                    </Columns>
                                    <StylesEditors>
                                        <Label Font-Bold="True">
                                        </Label>
                                    </StylesEditors>
                                </dxwgv:ASPxGridView>      
                            </td>
                        </tr>
                    </asp:Panel>  
                </table>
            </td>
        </tr>
    </table>
</asp:Content>