﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="fabricaAndamento.aspx.cs" Inherits="admin_fabricaAndamento" %>

<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxPopupControl" Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>

<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxPopupControl" Assembly="DevExpress.Web.v11.1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" runat="Server">
    <script src="js/jquery.min.js"></script>
    <link rel="stylesheet" href="css/bootstrap-table.css" type="text/css" />
    <style type="text/css">
        .classehoje a {
            color: #FFFFFF;
        }

            .classehoje a:hover {
                color: #FFFFFF;
            }

            .classehoje a:active {
                color: #FFFFFF;
            }

            .classehoje a:visited {
                color: #FFFFFF;
            }

        .soimprimir {
            display: none;
        }

        .borderN {
            border: 0px;
        }

        .borderR {
            border-right: 2px solid black;
        }

        @media print {
            #itensDoRomaneio {
                background-color: white;
                height: 100%;
                width: 100%;
                position: fixed;
                top: 0;
                left: 0;
                margin: 0;
                padding: 15px;
                font-size: 14px;
                line-height: 18px;
            }
        }

        #tbProcessoAtual tr:nth-child(odd) {
            background-color: #DEDEDE;
        }
    </style>
    <script type="text/javascript">
        function toggle() {
            var divs = document.querySelectorAll("[id]");
            for (var i = 0, len = divs.length; i < len; i++) {
                var div = divs[i];
                if (div.id.indexOf("chkChecar") > -1) {
                    div.checked = true;
                }
            }
        }
        function togglesim() {
            var divs = document.querySelectorAll("[id]");
            for (var i = 0, len = divs.length; i < len; i++) {
                var div = divs[i];
                if (div.id.indexOf("rdbSim") > -1) {
                    div.checked = true;
                }
            }
        }
        function togglenao() {
            var divs = document.querySelectorAll("[id]");
            for (var i = 0, len = divs.length; i < len; i++) {
                var div = divs[i];
                if (div.id.indexOf("rdbNao") > -1) {
                    div.checked = true;
                }
            }
        }

        function printList() {
            var data = $("#itensDoRomaneio").html();

            var mywindow = window.open('', 'my div', 'height=400,width=600');
            mywindow.document.write('<html><head><title>Etapa</title><style>.borderN {border-bottom: 0px !important;}.borderR {border-right: 2px solid black;} .naoimprimir {display: none;}#tbProcessoAtual td {border: 1px solid black;}#tbProcessoAtual tr:nth-child(odd) {background-color: #DEDEDE;border-bottom: 2px solid #black;}</style>');
            /*optional stylesheet*/ //mywindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
            mywindow.document.write('</head><body >');
            mywindow.document.write(data);
            mywindow.document.write('</body></html>');

            mywindow.document.close(); // necessary for IE >= 10
            mywindow.focus(); // necessary for IE >= 10

            mywindow.print();
            mywindow.close();

            return true;
        }
    </script>
    <div class="tituloPaginas" valign="top">
        Andamento da Fábrica - <%= DateTime.Now.ToShortDateString() %>
    </div>

    <div class="contentAlign">
        <table class="tableBootstrap tableBootstrap-hover" cellpadding="0" cellspacing="0">
            <tbody>
                <tr>
                    <td style="padding-bottom: 50px; font-size: 16px;" colspan="3">
                        Fábrica:<br />
                        <asp:DropDownList runat="server" ID="ddlEmpresa" AutoPostBack="true" OnSelectedIndexChanged="ddlEmpresa_SelectedIndexChanged">
                            <Items>
                                <asp:ListItem Text="Fabrica Grão de Gente" Value="82"></asp:ListItem>
                                <asp:ListItem Text="Terceirizados" Value="204"></asp:ListItem>
                            </Items>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr style="background: black; color: #ffffff !important;">
                    <td style="background: black; color: #ffffff !important;">Etapa</td>
                    <td style="padding: 0; background: black; color: #ffffff !important;">
                        <table class="tableBootstrap tableBootstrap-hover" style="width: 100%; height: 100%; padding: 0; margin: 0;" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="width: 150px; border-top: 0; border-left: 0; border-bottom: 0; background: black; color: #ffffff !important; font-weight: bold;">Romaneio</td>
                                <td style="width: 150px; border-top: 0; border-left: 0; border-bottom: 0; background: black; color: #ffffff !important; font-weight: bold;">Total de Itens</td>
                                <td style="width: 150px; border-top: 0; border-left: 0; border-bottom: 0; background: black; color: #ffffff !important; font-weight: bold;">Total Faltando</td>
                                <td style="width: 150px; border-top: 0; border-left: 0; border-bottom: 0; background: black; color: #ffffff !important; font-weight: bold;">Percentual Concluído</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <asp:ListView runat="server" ID="lstEtapas" OnItemDataBound="lstEtapas_OnItemDataBound">
                    <ItemTemplate>
                        <tr>
                            <td>
                                <%# Eval("processo") %>
                            </td>
                            <td style="padding: 0;">
                                <table class="tableBootstrap tableBootstrap-hover" style="width: 100%; height: 100%; padding: 0; margin: 0;" cellpadding="0" cellspacing="0">
                                    <asp:ListView runat="server" ID="lstEstoque">
                                        <ItemTemplate>
                                            <tr>
                                                <td style="width: 150px; border-top: 0; border-left: 0; border-bottom: 0;">
                                                    <div class="romaneioativo" style='font-weight: bold; font-size: 16px;'>
                                                        <%--display: <%# Container.DataItemIndex == 0 ? "block" : "none"  %>;--%>
                                                        <asp:LinkButton runat="server" ID="btnChecarItens" CommandArgument='<%# Eval("idPedidoFornecedor") %>' OnCommand="btnChecarItens_OnCommand">
                                                           <%# Eval("idPedidoFornecedor") %>
                                                        </asp:LinkButton>
                                                    </div>
                                                    <%--<div class="proximoromaneio" style='display: <%# Container.DataItemIndex == 0 ? "none" : "block"  %>;'>
                                                        <%# Eval("idPedidoFornecedor") %>
                                                    </div>--%>
                                                </td>
                                                <td style="width: 150px; border-top: 0; border-left: 0; border-bottom: 0;">
                                                    <div class="romaneioativo" style='font-weight: bold; font-size: 16px; display: <%# Container.DataItemIndex == 0 ? "block" : "none"  %>;'>
                                                        <%# Eval("itens") %>
                                                    </div>
                                                    <div class="proximoromaneio" style='display: <%# Container.DataItemIndex == 0 ? "none" : "block"  %>;'>
                                                        <%# Eval("itens") %>
                                                    </div>
                                                </td>
                                                <td style="width: 150px; border-top: 0; border-left: 0; border-bottom: 0;">
                                                    <div class="romaneioativo" style='font-weight: bold; font-size: 16px;'>
                                                        <%--display: <%# Container.DataItemIndex == 0 ? "block" : "none"  %>;--%>
                                                        <asp:LinkButton runat="server" ID="btnChecarFaltantes" CommandArgument='<%# Eval("idPedidoFornecedor") + "#" + Eval("processo") %>'
                                                            OnCommand="btnChecarFaltantes_OnCommand">
                                                            <%# Eval("itensEtapa") %>
                                                        </asp:LinkButton>
                                                    </div>
                                                    <%--<div class="romaneioativo" style='font-weight: bold; font-size: 16px; display: <%# Container.DataItemIndex == 0 ? "block" : "none"  %>; '>
                                                    <%# Eval("itensEtapa") %></div>--%>
                                                    <%--<div class="proximoromaneio" style='display: <%# Container.DataItemIndex == 0 ? "none" : "block"  %>;'>
                                                        <%# Eval("itensEtapa") %>
                                                    </div>--%>
                                                </td>
                                                <td style="width: 150px; border-top: 0; border-left: 0; border-bottom: 0;">
                                                    <div class="romaneioativo" style='font-weight: bold; font-size: 16px; display: <%# Container.DataItemIndex == 0 ? "block" : "none"  %>;'>
                                                        <%# Convert.ToDecimal(Eval("percentual")).ToString("0.##") %>%
                                                    </div>
                                                    <div class="proximoromaneio" style='display: <%# Container.DataItemIndex == 0 ? "none" : "block"  %>;'>
                                                        <%# Convert.ToDecimal(Eval("percentual")).ToString("0.##") %>%
                                                    </div>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:ListView>
                                </table>

                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:ListView>
            </tbody>
        </table>
        <br />
        <br />
    </div>


    <dx:ASPxPopupControl ID="popChecarItens" runat="server" CloseAction="CloseButton" Modal="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
        ClientInstanceName="popChecarItens" HeaderText="Checar Etapa Completa" AllowDragging="True" PopupAnimationType="None" EnableViewState="True" Height="75" AutoUpdatePosition="True">
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl10" runat="server">
                <div style="width: 800px; height: 35px; text-align: right;">
                    <table style="width: 100%">
                        <tr>
                            <td style="text-align: left;">Processo: 
                                <asp:DropDownList runat="server" ID="ddlSubProcessos" AutoPostBack="True" OnSelectedIndexChanged="ddlSubProcessos_OnSelectedIndexChanged">
                                    <Items>
                                        <asp:ListItem Text="Selecione" Value=""></asp:ListItem>
                                    </Items>
                                </asp:DropDownList>
                            </td>
                            <td style="text-align: right;">
                                <asp:Button runat="server" OnClientClick="toggle(); return false;" Text="Checar todos" ID="btnChecarTodos" />
                                <asp:Button runat="server" OnClientClick="togglesim(); return false;" Text="Sim" ID="btnChecarSim" Visible="False" />
                                <asp:Button runat="server" OnClientClick="togglenao(); return false;" Text="Não" ID="btnChecarNao" Visible="False" />
                            </td>
                        </tr>
                    </table>
                    <asp:HiddenField runat="server" ID="hdfIdPedidoFornecedor" />
                    <asp:HiddenField runat="server" ID="hdfIdPrecessoFabrica"/>
                </div>
                <div style="width: 800px; height: 500px; overflow: scroll;" id="itensDoRomaneio">
                    <div style="padding-bottom: 20px; font-size: 14px;">
                        <b>Romaneio: </b>
                        <asp:Literal runat="server" ID="litRomaneio"></asp:Literal>
                        - <b>Etapa: </b>
                        <asp:Literal runat="server" ID="litEtapaAtual"></asp:Literal>
                        - <b>Total de Itens: </b>
                        <asp:Literal runat="server" ID="litTotalItens"></asp:Literal><asp:Literal runat="server" ID="litTotalSubProcesso"></asp:Literal>
                    </div>
                    <table style="width: 100%;" id="tbProcessoAtual" cellpadding="5" cellspacing="0">
                        <tr style="font-weight: bold;" class="soimprimir">
                            <td style="font-size: 9px; width: 20px;"></td>
                            <td style="border: 0;"></td>
                            <td style="border: 0; border-right: 2px;"></td>
                            <td class="borderR">Assinatura Entrega</td>
                            <td class="naoimprimir"></td>
                            <td class="soimprimir borderR" style="width: 140px;"></td>
                            <td class="soimprimir borderR" style="width: 140px;"></td>
                            <td class="soimprimir borderR" style="width: 140px;"></td>
                            <td class="soimprimir borderR" style="width: 140px;"></td>
                            <td class="soimprimir borderR" style="width: 140px;"></td>
                        </tr>
                        <tr style="font-weight: bold;" class="soimprimir">
                            <td style="font-size: 9px; width: 20px;"></td>
                            <td style="border: 0;"></td>
                            <td style="border: 0; border-right: 2px;"></td>
                            <td class="borderR">Data Entrega</td>
                            <td class="naoimprimir"></td>
                            <td class="soimprimir borderR"></td>
                            <td class="soimprimir borderR"></td>
                            <td class="soimprimir borderR"></td>
                            <td class="soimprimir borderR"></td>
                            <td class="soimprimir borderR"></td>
                        </tr>
                        <tr style="font-weight: bold;" class="soimprimir">
                            <td style="font-size: 9px; width: 20px;"></td>
                            <td style="border: 0;"></td>
                            <td style="border: 0; border-right: 2px;"></td>
                            <td class="borderR">Hora Entrega</td>
                            <td class="naoimprimir"></td>
                            <td class="soimprimir borderR"></td>
                            <td class="soimprimir borderR"></td>
                            <td class="soimprimir borderR"></td>
                            <td class="soimprimir borderR"></td>
                            <td class="soimprimir borderR"></td>
                        </tr>
                        <tr style="font-weight: bold;" class="soimprimir">
                            <td style="font-size: 9px; width: 20px;"></td>
                            <td style="border: 0;"></td>
                            <td style="border: 0; border-right: 2px;"></td>
                            <td class="borderR">&nbsp;</td>
                            <td class="naoimprimir"></td>
                            <td class="soimprimir borderR"></td>
                            <td class="soimprimir borderR"></td>
                            <td class="soimprimir borderR"></td>
                            <td class="soimprimir borderR"></td>
                            <td class="soimprimir borderR"></td>
                        </tr>
                        <tr style="font-weight: bold;" class="soimprimir">
                            <td style="font-size: 9px; width: 20px;"></td>
                            <td style="border: 0;"></td>
                            <td style="border: 0; border-right: 2px;"></td>
                            <td class="borderR">Assinatura Recebimento</td>
                            <td class="naoimprimir"></td>
                            <td class="soimprimir borderR"></td>
                            <td class="soimprimir borderR"></td>
                            <td class="soimprimir borderR"></td>
                            <td class="soimprimir borderR"></td>
                            <td class="soimprimir borderR"></td>
                        </tr>
                        <tr style="font-weight: bold;" class="soimprimir">
                            <td style="font-size: 9px; width: 20px;"></td>
                            <td style="border: 0;"></td>
                            <td style="border: 0; border-right: 2px;"></td>
                            <td class="borderR">Data Recebimento</td>
                            <td class="naoimprimir"></td>
                            <td class="soimprimir borderR"></td>
                            <td class="soimprimir borderR"></td>
                            <td class="soimprimir borderR"></td>
                            <td class="soimprimir borderR"></td>
                            <td class="soimprimir borderR"></td>
                        </tr>
                        <tr style="font-weight: bold;" class="soimprimir">
                            <td style="font-size: 9px; width: 20px;"></td>
                            <td style="border: 0;"></td>
                            <td style="border: 0; border-right: 2px;"></td>
                            <td class="borderR">Hora Recebimento</td>
                            <td class="naoimprimir"></td>
                            <td class="soimprimir borderR"></td>
                            <td class="soimprimir borderR"></td>
                            <td class="soimprimir borderR"></td>
                            <td class="soimprimir borderR"></td>
                            <td class="soimprimir borderR"></td>
                        </tr>
                        <tr style="font-weight: bold;">
                            <td style="font-size: 9px; width: 20px;"></td>
                            <td style="font-size: 9px; width: 100px;">ID</td>
                            <td style="font-size: 9px; width: 100px;">Complemento</td>
                            <td style="font-size: 9px;">Nome</td>
                            <td class="naoimprimir"></td>
                            <td class="soimprimir">Corte</td>
                            <td class="soimprimir">Bordado</td>
                            <td class="soimprimir">Costura</td>
                            <td class="soimprimir">Cola/Enchi.</td>
                            <td class="soimprimir">Embalagem</td>
                        </tr>
                        <asp:ListView runat="server" ID="lstChecarItens" OnItemDataBound="lstChecarItens_OnItemDataBound">
                            <ItemTemplate>
                                <tr>
                                    <td style="font-size: 9px; width: 20px;">
                                        <%#Container.DataItemIndex+1 %>
                                    </td>
                                    <td style="font-size: 9px; width: 100px;">
                                        <%# Eval("produtoIdDaEmpresa") %>
                                    </td>
                                    <td style="font-size: 9px; width: 100px;">
                                        <%# Eval("complementoIdDaEmpresa") %>
                                    </td>
                                    <td style="font-size: 9px; width: 200px;">
                                        <asp:HiddenField runat="server" ID="hdfEtiqueta" Value='<%# Eval("idPedidoFornecedorItem") %>' />
                                        <%# Eval("totalAtual") %>/<%# Eval("totalProduto") %> - <%# Eval("produtoNome") %>
                                    </td>
                                    <td class="naoimprimir">
                                        <asp:CheckBox runat="server" ID="chkChecar" />
                                        <asp:RadioButton runat="server" ID="rdbSim" Text="Sim" Checked="False" Visible="False" />&nbsp;
                                        <asp:RadioButton runat="server" ID="rdbNao" Text="Não" Checked="False" Visible="False" />
                                    </td>
                                    <td class="soimprimir"></td>
                                    <td class="soimprimir"></td>
                                    <td class="soimprimir"></td>
                                    <td class="soimprimir"></td>
                                    <td class="soimprimir"></td>
                                </tr>
                            </ItemTemplate>
                        </asp:ListView>
                    </table>
                </div>
                <div style="width: 800px; height: 35px; text-align: right;">
                    <table style="width: 100%">
                        <tr>
                            <td style="text-align: left;">
                                <asp:Button runat="server" OnClientClick="printList(); return false;" Text="Imprimir" />
                            </td>
                            <td style="text-align: right;">
                                <asp:Button runat="server" ID="btnGravar" Text="Gravar Itens" OnClick="btnGravar_OnClick" />
                            </td>
                        </tr>
                    </table>
                </div>
            </dx:PopupControlContentControl>
        </ContentCollection>
        <ContentStyle>
            <Paddings PaddingBottom="5px" />
        </ContentStyle>
    </dx:ASPxPopupControl>
</asp:Content>

