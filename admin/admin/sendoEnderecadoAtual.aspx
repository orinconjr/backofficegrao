﻿<%@ Page Title="" Language="C#" Theme="Glass" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="sendoEnderecadoAtual.aspx.cs" Inherits="admin_sendoEnderecadoAtual" %>

<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v11.1" Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Namespace="Controls" TagPrefix="uc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" runat="Server">
    <table cellpadding="0" cellspacing="0" style="width: 920px">
        <tr>
            <td class="tituloPaginas" valign="top">
                <asp:Label ID="lblAcao" runat="server">Produtos sendo Endereçados</asp:Label>
            </td>
        </tr>
        <tr>
            <td>

                <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <dxwgv:ASPxGridView ID="grd" runat="server" AutoGenerateColumns="False" KeyFieldName="idTransferenciaEndereco" Width="834px" EnableCallBacks="False"
                                Cursor="auto">
                                <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                                <Styles>
                                    <Footer Font-Bold="True">
                                    </Footer>
                                </Styles>
                                <SettingsText ConfirmDelete="Tem certeza que deseja excluir?"
                                    EmptyDataRow="Nenhum registro encontrado." />
                                <SettingsPager Position="TopAndBottom" PageSize="50"
                                    ShowDisabledButtons="False" AlwaysShowPager="True">
                                    <Summary AllPagesText="Páginas: {0} - {1} ({2} items)"
                                        Text="Página {0} de {1} ({2} registros encontrados)" />
                                </SettingsPager>
                                <Settings ShowFilterRow="True" ShowGroupButtons="False"
                                    ShowHeaderFilterButton="True" ShowFooter="True" />
                                <SettingsEditing EditFormColumnCount="4"
                                    PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1"
                                    PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter"
                                    PopupEditFormWidth="700px" />
                                <Columns>
                                    <dxwgv:GridViewDataTextColumn Caption="Endereçamento de transferência" FieldName="colEnderecamentoTransf" VisibleIndex="0" UnboundType="String">
                                        <EditFormSettings Visible="False" />
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Total de itens faltantes" FieldName="colTotalItensFaltantes" VisibleIndex="0" UnboundType="Integer">
                                        <EditFormSettings Visible="False" />
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Usuário de entrada" FieldName="colNomeUserEntrada" VisibleIndex="0" UnboundType="String">
                                        <EditFormSettings Visible="False" />
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Usuário da Expedição" FieldName="colUserExpedicao" UnboundType="String" VisibleIndex="0">
                                        <EditFormSettings Visible="False" />
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataDateColumn Caption="Data de início" FieldName="colDataInicio" UnboundType="DateTime" VisibleIndex="3" Width="120px">
                                        <PropertiesDateEdit DisplayFormatString="" EditFormat="DateTime">
                                            <CalendarProperties ClearButtonText="Apagar" TodayButtonText="Hoje">
                                            </CalendarProperties>
                                        </PropertiesDateEdit>
                                        <Settings GroupInterval="Date" />
                                    </dxwgv:GridViewDataDateColumn>
                                    <dxwgv:GridViewDataDateColumn Caption="Data do último item endereçado" FieldName="colDataUltItemEnd" UnboundType="DateTime" VisibleIndex="3" Width="120px">
                                        <PropertiesDateEdit DisplayFormatString="" EditFormat="DateTime">
                                            <CalendarProperties ClearButtonText="Apagar" TodayButtonText="Hoje">
                                            </CalendarProperties>
                                        </PropertiesDateEdit>
                                        <Settings GroupInterval="Date" />
                                    </dxwgv:GridViewDataDateColumn>

                                </Columns>
                                <StylesEditors>
                                    <Label Font-Bold="True">
                                    </Label>
                                </StylesEditors>
                            </dxwgv:ASPxGridView>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>

