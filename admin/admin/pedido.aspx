﻿<%@ Page Language="C#" Theme="Glass" ValidateRequest="false" Debug="true" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="pedido.aspx.cs" Inherits="admin_pedido" MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="CuteEditor" Namespace="CuteEditor" TagPrefix="CE" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxPopupControl" Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxEditors" Assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v11.1" Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxTabControl" Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>

<%@ Register Assembly="DevExpress.Web.v11.1" Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v11.1" Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxMenu" Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>
<%@ Register Namespace="Controls" TagPrefix="uc" %>

<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxMenu" Assembly="DevExpress.Web.v11.1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" runat="Server">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>


    <script type="text/javascript">
        function validaChamado() {
            var depto = document.getElementById("<%=ddlDepartamento.ClientID%>");
            //var tarefa = document.getElementById("<%=ddlTarefa.ClientID%>");
            var assunto = document.getElementById("<%=ddlAssunto.ClientID%>");

            if (depto.value == null || depto.value == "0") {
                alert("Favor selecionar o campo departamento");
                depto.focus();
                return false;
            }

            //if (tarefa.value == null || tarefa.value == "0") {
            //    alert("Favor selecionar o campo tarefa");
            //    tarefa.focus();
            //    return false;
            //}

            if (assunto.value == null || assunto.value == "0") {
                alert("Favor selecionar o campo categoria");
                assunto.focus();
                return false;
            }

            return true;
        }

        function RadioCheck(rb) {
            var lst = document.getElementById(rb.parentElement.parentElement.parentElement.id);
            var rbs = lst.getElementsByTagName("input");

            //var row = rb.parentNode.parentNode;
            for (var i = 0; i < rbs.length; i++) {
                if (rbs[i].type == "radio") {
                    if (rbs[i].checked && rbs[i] != rb) {
                        rbs[i].checked = false;
                        break;
                    }
                }
            }
        }
    </script>

    <style type="text/css">
        .botaoComum {
            margin-left: auto;
            margin-right: auto;
        }

        .botaoVermelho {
            background: #FF6A6A !important;
            margin-left: auto;
            margin-right: auto;
        }

            .botaoVermelho .dxbButton_Glass {
                background: #FF6A6A !important;
            }

        .botaoVerde {
            background: #9BE693 !important;
            margin-left: auto;
            margin-right: auto;
        }

            .botaoVerde .dxbButton_Glass {
                background: #9BE693 !important;
            }

        .headerStatus {
            padding-bottom: 5px;
            background: #fdffcf;
            border: 1px solid #f1f498;
            border-radius: 10px;
            font-family: arial;
            font-size: 14px;
            padding-bottom: 5px;
            padding-top: 5px;
            text-align: center;
        }

        .centerTable {
            text-align: center;
        }

            .centerTable .dxmMenuItem_Glass {
                text-align: center !important;
            }

        .hover {
            position: relative;
            /*top: 50px;
            left: 50px;*/
        }

            .hover:hover .tooltip {
                opacity: 1;
            }

        .tooltip {
            top: -75px;
            left: -222px;
            background-color: black;
            color: white;
            border-radius: 5px;
            opacity: 0;
            position: absolute;
            -webkit-transition: opacity 0.5s;
            -moz-transition: opacity 0.5s;
            -ms-transition: opacity 0.5s;
            -o-transition: opacity 0.5s;
            transition: opacity 0.5s;
        }

        .red-X {
            color: #ff6a6a;
            font-size: 25px;
            text-align: center;
        }

        .green-Check {
            color: green;
            font-size: 25px;
            text-align: center;
        }
    </style>
    <table cellpadding="0" cellspacing="0" style="width: 920px">
        <tr>
            <td class="tituloPaginas" valign="top">
                <asp:Label ID="lblAcao" runat="server">Detalhes do Pedido</asp:Label>
                <asp:Literal ID="txtNumeroDoPedidoCliente" runat="server"></asp:Literal>
                - ID:
                <asp:Literal ID="txtNumeroDoPedido" runat="server"></asp:Literal>
            </td>
        </tr>
        <tr>
            <td>
                <table align="center" cellpadding="0" cellspacing="0" style="width: 855px">
                    <tr class="rotulos">
                        <td class="headerStatus" style="" runat="server" id="tdHeaderStatus">Status Atual:
                            <asp:Literal runat="server" ID="litStatusAtual"></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-bottom: 10px;">
                            <table style="width: 100%;">
                                <tr>
                                    <td class="rotulos" style="padding-right: 15px;">Cliente:<br />
                                        <asp:TextBox ID="txtCliente" runat="server" CssClass="campos" ReadOnly="True" Width="115px"></asp:TextBox>
                                    </td>
                                    <td class="rotulos" style="padding-right: 15px;">Data do Pedido:<br />
                                        <asp:TextBox ID="txtDataDaRealizacao" runat="server" CssClass="campos" ReadOnly="True" Width="122px"></asp:TextBox>
                                    </td>
                                    <td class="rotulos" style="padding-right: 15px;">Confirmação de Pagamento:<br />
                                        <asp:TextBox ID="txtDataConfirmacaoPagamento" runat="server" CssClass="campos" ReadOnly="True" Width="155px"></asp:TextBox>
                                    </td>
                                    <td class="rotulos" style="padding-right: 15px;">Prazo Postagem:<br />
                                        <asp:TextBox ID="txtPrazoMaximoPostagem" runat="server" CssClass="campos" ReadOnly="True" Width="90px"></asp:TextBox>
                                    </td>
                                    <td class="rotulos" style="padding-right: 15px;">Prazo para Recebimento:<br />
                                        <asp:TextBox ID="txtPrazoMaximoFinal" runat="server" CssClass="campos" ReadOnly="True" Width="90px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="rotulos" style="padding-right: 15px;"></td>
                                    <td class="rotulos" style="padding-right: 15px;">
                                        <asp:Panel runat="server" ID="pnPrazoPosEnvio" Visible="false">
                                            Prazo final após envio:<br />
                                            <asp:TextBox ID="txtPrazoFinalPosEnvio" runat="server" CssClass="campos" ReadOnly="True" Width="90px"></asp:TextBox>
                                        </asp:Panel>
                                    </td>
                                    <td class="rotulos" style="padding-right: 15px;">Prazo exibido:<br />
                                        <asp:TextBox ID="txtPrazoExibido" runat="server" CssClass="campos" ReadOnly="True" Width="90px"></asp:TextBox>
                                    </td>
                                    <td class="rotulos" style="padding-right: 15px;">Prazo Atualizado Devido Alterações:<br />
                                        <asp:TextBox ID="txtPrazoMaximoPostagemAtualizado" runat="server" CssClass="campos" ReadOnly="True" Width="90px"></asp:TextBox>
                                    </td>
                                    <td class="rotulos" style="padding-right: 15px;">Prazo para Recebimento Atualizado Devido Alterações:<br />
                                        <asp:TextBox ID="txtPrazoMaximoFinalAtualizado" runat="server" CssClass="campos" ReadOnly="True" Width="90px"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <dx:ASPxPageControl ID="tabsPedidos" runat="server" ActiveTabIndex="3" Width="100%">
                                <TabPages>
                                    <dx:TabPage Text="Informações do Pedido">
                                        <ContentCollection>
                                            <dx:ContentControl runat="server">
                                                <asp:DetailsView ID="dtvPedido" runat="server" AutoGenerateRows="False"
                                                    CellPadding="0" DataSourceID="sqlPedido" GridLines="None"
                                                    OnDataBound="dtvPedido_DataBound">
                                                    <Fields>
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <table border="0" bordercolor="#7EACB1" cellpadding="0" cellspacing="0" style="width: 834px">
                                                                    <tr>
                                                                        <td style="border: 1px solid #7EACB1;" valign="top">
                                                                            <table cellpadding="0" cellspacing="0" style="width: 470px">
                                                                                <tr>
                                                                                    <td style="padding-top: 15px; width: 215px; padding-bottom: 15px; padding-left: 15px;"
                                                                                        valign="top">
                                                                                        <table cellpadding="0" cellspacing="0" style="width: 200px">
                                                                                            <tr class="rotulos" style="font-weight: bold">
                                                                                                <td style="padding-top: 10px">Endereço de Entrega:</td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <table cellpadding="0" cellspacing="0" class="rotulos" style="width: 200px; border: 1px solid #7EACB1">
                                                                                                        <tr>
                                                                                                            <td style="padding-top: 8px; padding-right: 15px; padding-left: 15px">Nome para Entrega:<br />
                                                                                                                <asp:TextBox ID="txtNomeEntrega" runat="server" CssClass="campos" Width="170px"></asp:TextBox>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="padding-top: 8px; padding-right: 15px; padding-left: 15px">Rua:<br />
                                                                                                                <asp:TextBox ID="txtRua" runat="server" CssClass="campos" Width="170px"></asp:TextBox>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="padding-top: 8px; padding-right: 15px; padding-left: 15px">
                                                                                                                <table cellpadding="0" cellspacing="0" class="rotulos" style="width: 170px">
                                                                                                                    <tr>
                                                                                                                        <td width="95">Número:<br />
                                                                                                                            <asp:TextBox ID="txtNumero" runat="server" CssClass="campos" Width="70px"></asp:TextBox>
                                                                                                                        </td>
                                                                                                                        <td style="padding-left: 10px">Complemento:<br />
                                                                                                                            <asp:TextBox ID="txtComplemento" runat="server" CssClass="campos" Width="90px"></asp:TextBox>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="padding-top: 8px; padding-right: 15px; padding-left: 15px">Bairro:<br />
                                                                                                                <asp:TextBox ID="txtBairro" runat="server" CssClass="campos" Width="170px"></asp:TextBox>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="padding-top: 8px; padding-right: 15px; padding-left: 15px">Cidade:<br />
                                                                                                                <asp:TextBox ID="txtCidade" runat="server" CssClass="campos" Width="170px"></asp:TextBox>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="padding-top: 8px; padding-right: 15px; padding-left: 15px">
                                                                                                                <table cellpadding="0" cellspacing="0" class="rotulos" style="width: 170px">
                                                                                                                    <tr>
                                                                                                                        <td width="95">Estado:<br />
                                                                                                                            <asp:TextBox ID="txtEstado" runat="server" CssClass="campos" Width="70px"></asp:TextBox>
                                                                                                                        </td>
                                                                                                                        <td style="padding-left: 10px">Cep:<br />
                                                                                                                            <asp:TextBox ID="txtCep" runat="server" CssClass="campos" Width="90px"></asp:TextBox>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="padding-top: 8px; padding-right: 15px; padding-left: 15px;">Referência para Entrega:<br />
                                                                                                                <asp:TextBox ID="txtReferencia" TextMode="MultiLine" runat="server" CssClass="campos" Height="146px" Width="170px"></asp:TextBox>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr style="display: none;">
                                                                                                            <td style="padding: 8px 15px 15px 15px;">
                                                                                                                <asp:HyperLink ID="hplGerarEtiqueta" runat="server"
                                                                                                                    ImageUrl="~/admin/images/btGerarEtiqueta.jpg" NavigateUrl='<%# "etiqueta.aspx?pedidoId=" + Eval("pedidoId") %>' Target="_blank"></asp:HyperLink>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="padding: 8px 15px 15px 15px;">
                                                                                                                <a href="" target="_blank" id="linkGoogle" runat="server">
                                                                                                                    <img src="images/visualizarmapa.png" />
                                                                                                                </a>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="padding: 8px 15px 15px 15px;">
                                                                                                                <asp:ImageButton ID="btnSalvarEnderecoEntrega" runat="server" ImageAlign="AbsMiddle" ImageUrl="images/btSalvarPeq1.jpg" OnClick="btnSalvarEnderecoEntrega_onClick" />
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr class="rotulos">
                                                                                                <td>Forma de Entrega escolhida pelo cliente:<br />
                                                                                                    <asp:DropDownList CssClass="campos" Enabled="False" runat="server" ID="ddlTipoDeEntrega" DataSourceID="sqlTipoDeEntrega" DataTextField="tipoDeEntregaNome" DataValueField="tipoDeEntregaId">
                                                                                                    </asp:DropDownList><br />
                                                                                                    Enviar por:<br />
                                                                                                    <asp:DropDownList runat="server" ID="ddlEnviarPor" DataValueField="idTransportadora" DataTextField="transportadora" AppendDataBoundItems="true">
                                                                                                        <Items>
                                                                                                            <asp:ListItem Value="" Text="Seleção Automática"></asp:ListItem>
                                                                                                        </Items>
                                                                                                    </asp:DropDownList>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr class="rotulos">
                                                                                                <td>Usuário Separação:<br />
                                                                                                    <asp:DropDownList ID="ddlUsuarioSeparacao" runat="server" DataValueField="idUsuarioExpedicao" DataTextField="nome" AppendDataBoundItems="true">
                                                                                                        <Items>
                                                                                                            <asp:ListItem Value="" Text="Selecione"></asp:ListItem>
                                                                                                        </Items>
                                                                                                    </asp:DropDownList>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>Úsuário Embalagem:<br />
                                                                                                    <asp:DropDownList ID="ddlUsuarioEmbalagem" runat="server" DataValueField="idUsuarioExpedicao" DataTextField="nome" AppendDataBoundItems="true">
                                                                                                        <Items>
                                                                                                            <asp:ListItem Value="" Text="Selecione"></asp:ListItem>
                                                                                                        </Items>
                                                                                                    </asp:DropDownList>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr class="rotulos">
                                                                                                <td style="height: 32px">
                                                                                                    <asp:ImageButton ID="btnSalvarTipoDeEntrega" runat="server" ImageAlign="AbsMiddle" ImageUrl="images/btSalvarPeq1.jpg" OnClick="btnSalvarTipoDeEntrega_Click" />
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                    <td align="center" style="padding: 15px;" valign="top" width="255">
                                                                                        <table cellpadding="0" cellspacing="0" class="rotulos" style="width: 225px">
                                                                                            <tr class="rotulos" style="font-weight: bold">
                                                                                                <td style="padding-top: 10px">Informações sobre o Cliente:</td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <table cellpadding="0" cellspacing="0" class="rotulos"
                                                                                                        style="width: 225px; border: 1px solid #7EACB1">
                                                                                                        <tr class="rotulos">
                                                                                                            <td style="padding-top: 8px; padding-right: 15px; padding-left: 15px; width: 255px;">Nome do Cliente:<br />
                                                                                                                <asp:TextBox ID="txtNomeDoCliente" runat="server" CssClass="campos"
                                                                                                                    ReadOnly="True" Text='<%# String.IsNullOrEmpty(Eval("clienteNome").ToString()) ? Eval("clienteNomeDaEmpresa") : Eval("clienteNome") %>' Width="210px"></asp:TextBox>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr class="rotulos">
                                                                                                            <td style="padding-top: 8px; padding-right: 15px; padding-left: 15px; width: 255px;">E-mail:<br />
                                                                                                                <asp:TextBox ID="txtEmail" runat="server" CssClass="campos" ReadOnly="True"
                                                                                                                    Text='<%# Bind("clienteEmail") %>' Width="210px"></asp:TextBox>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="padding-top: 8px; padding-right: 15px; padding-left: 15px; width: 255px;">
                                                                                                                <table cellpadding="0" cellspacing="0" class="rotulos" style="width: 195px">
                                                                                                                    <tr>
                                                                                                                        <td width="115">Nascimento:<br />
                                                                                                                            <asp:TextBox ID="txtNascimento" runat="server" CssClass="campos"
                                                                                                                                ReadOnly="True" Text='<%# Bind("clienteDataNascimento", "{0}") %>'
                                                                                                                                Width="100px"></asp:TextBox>
                                                                                                                        </td>
                                                                                                                        <td style="padding-left: 15px">Cliente desde:<br />
                                                                                                                            <asp:TextBox ID="txtCadastro" runat="server" CssClass="campos" ReadOnly="True"
                                                                                                                                Text='<%# Bind("dataDaCriacao", "{0:g}") %>' Width="95px"></asp:TextBox>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="padding-top: 8px; padding-right: 15px; padding-left: 15px; width: 255px;">IE/RG:<br />
                                                                                                                <asp:TextBox ID="txtRg" runat="server" CssClass="campos" ReadOnly="True"
                                                                                                                    Text='<%# Bind("clienteRGIE") %>' Width="100px"></asp:TextBox>
                                                                                                                &nbsp;</td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="padding-top: 8px; padding-right: 15px; padding-left: 15px; width: 255px;">CNPJ/CPF:<br />
                                                                                                                <asp:TextBox ID="txtCpf" runat="server" CssClass="campos" ReadOnly="True"
                                                                                                                    Text='<%# Bind("clienteCPFCNPJ") %>' Width="100px"></asp:TextBox>
                                                                                                                &nbsp;<asp:HyperLink ID="hplConsultarCpf" runat="server"
                                                                                                                    ImageUrl="images/btConsultar.jpg"
                                                                                                                    NavigateUrl="http://www.receita.fazenda.gov.br/Aplicacoes/ATCTA/CPF/ConsultaPublica.asp"
                                                                                                                    Target="_blank"></asp:HyperLink>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="padding-top: 8px; padding-right: 15px; padding-left: 15px; width: 255px;">Telefone residencial:<br />
                                                                                                                <asp:TextBox ID="txtTelefoneResidencial" runat="server" CssClass="campos"
                                                                                                                    ReadOnly="True" Text='<%# Bind("clienteFoneResidencial") %>' Width="100px"></asp:TextBox>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="padding-top: 8px; padding-right: 15px; padding-left: 15px; width: 255px;">Telefone comercial:<br />
                                                                                                                <asp:TextBox ID="txtTelefoneComercial" runat="server" CssClass="campos"
                                                                                                                    ReadOnly="True" Text='<%# Bind("clienteFoneComercial") %>' Width="100px"></asp:TextBox>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="padding-top: 8px; padding-right: 15px; padding-left: 15px; width: 255px;">Telefone celular:<br />
                                                                                                                <asp:TextBox ID="txtTelefoneCelular" runat="server" CssClass="campos"
                                                                                                                    ReadOnly="True" Text='<%# Bind("clienteFoneCelular") %>' Width="100px"></asp:TextBox>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="padding: 8px 15px 15px 15px; width: 255px;">
                                                                                                                <b>Endereço do Cadastro:</b><br />
                                                                                                                <asp:Label ID="lblRua" runat="server" Text='<%# Eval("clienteRua") %>'></asp:Label>
                                                                                                                ,
                                                                                                                <asp:Label ID="lblNumero" runat="server" Text='<%# Eval("clienteNumero") %>'></asp:Label>
                                                                                                                &nbsp;&nbsp;
                                                                                                                <asp:Label ID="lblComplemento" runat="server"
                                                                                                                    Text='<%# Eval("clienteComplemento") %>'></asp:Label>
                                                                                                                <br />
                                                                                                                <asp:Label ID="lblBairro" runat="server" Text='<%# Eval("clienteBairro") %>'></asp:Label>
                                                                                                                &nbsp;<br />
                                                                                                                <asp:Label ID="lblCidade" runat="server" Text='<%# Eval("clienteCidade") %>'></asp:Label>
                                                                                                                &nbsp;-
                                                                                                                <asp:Label ID="lblEstado" runat="server" Text='<%# Eval("clienteEstado") %>'></asp:Label>
                                                                                                                <br />
                                                                                                                <asp:Label ID="lblCep" runat="server" Text='<%# Eval("clienteCep") %>'></asp:Label>
                                                                                                                <br />
                                                                                                                <asp:HyperLink ID="hplHistorico" runat="server"
                                                                                                                    ImageUrl="~/admin/images/btHistorico.jpg"
                                                                                                                    NavigateUrl='<%# "historicoDeCliente.aspx?clienteId=" + Eval("clienteId") %>'
                                                                                                                    Target="_blank"></asp:HyperLink>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr class="rotulos" style="font-weight: bold">
                                                                                                <td style="padding-top: 10px">Informações de Tracking
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr class="rotulos">
                                                                                                <td>
                                                                                                    <table style="width: 225px; border: 1px solid #7EACB1">
                                                                                                        <tr class="rotulos">
                                                                                                            <td style="padding-top: 8px; padding-left: 15px;">Tracking 1:<br />
                                                                                                                <asp:TextBox Enabled="False" ID="txtTracking" runat="server" CssClass="campos"
                                                                                                                    Width="100px" Text='<%# Bind("numeroDoTracking") %>'></asp:TextBox>
                                                                                                            </td>
                                                                                                            <td style="padding-top: 8px; padding-right: 15px;">Tracking 2:<br />
                                                                                                                <asp:TextBox Enabled="False" ID="txtTracking2" runat="server" CssClass="campos"
                                                                                                                    Width="100px" Text='<%# Bind("numeroDoTracking2") %>'></asp:TextBox>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr class="rotulos">
                                                                                                            <td style="padding-top: 8px; padding-left: 15px;">Tracking 3:<br />
                                                                                                                <asp:TextBox Enabled="False" ID="txtTracking3" runat="server" CssClass="campos"
                                                                                                                    Width="100px" Text='<%# Bind("numeroDoTracking3") %>'></asp:TextBox>
                                                                                                            </td>
                                                                                                            <td style="padding-top: 8px; padding-right: 15px;">Transportadora:<br />
                                                                                                                <asp:TextBox Enabled="False" ID="txtTracking4" runat="server" CssClass="campos"
                                                                                                                    Width="100px" Text='<%# Bind("numeroDoTracking4") %>'></asp:TextBox>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr class="rotulos" style="display: none;">
                                                                                                            <td style="padding-top: 8px; padding-left: 15px;">
                                                                                                                <asp:HyperLink ID="hplnkTracking" runat="server"
                                                                                                                    ImageUrl="~/admin/images/btRastrear.jpg" Target="_blank"></asp:HyperLink>
                                                                                                            </td>
                                                                                                            <td style="padding-top: 8px; padding-right: 15px;">
                                                                                                                <asp:ImageButton ID="imbSalvarTraking" runat="server" ImageAlign="AbsMiddle"
                                                                                                                    ImageUrl="images/btSalvarPeq1.jpg" OnClick="imbSalvarTraking_Click" />
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                        <td rowspan="3" style="padding: 15px; border: 1px solid #7EACB1;" valign="top">
                                                                            <table cellpadding="0" cellspacing="0" style="width: 288px">
                                                                                <tr>
                                                                                    <td>
                                                                                        <table>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <%--<dx:ASPxButton ID="btnEmailPrazos" runat="server" Text="Gerar Email de Prazos" AutoPostBack="False">
                                                                                                        <ClientSideEvents Click="function(s, e) { ShowPrazos(); }" />
                                                                                                    </dx:ASPxButton>--%>
                                                                                                    <dx:ASPxButton ID="btnEmailPrazos" runat="server" Text="Enviar Email de Prazos" AutoPostBack="True" OnClick="btnEmailPrazos_OnClick">
                                                                                                        <ClientSideEvents Click="function(s, e){ e.processOnServer = confirm('Deseja realmente enviar o e-mail de prazos?');}" />
                                                                                                    </dx:ASPxButton>

                                                                                                    <script type="text/javascript">
                                                                                                        // <![CDATA[
                                                                                                        function ShowPrazos() {
                                                                                                            popEmailPrazos.Show();
                                                                                                        }
                                                                                                        // ]]>
                                                                                                    </script>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr class="rotulos">
                                                                                    <td style="padding-top: 5px">IP do Cliente:<br />
                                                                                        <asp:TextBox ID="txtIp" runat="server" CssClass="campos" ReadOnly="True"
                                                                                            Text='<%# Bind("ipDoCliente") %>' Width="288px"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <table cellpadding="0" cellspacing="0" class="rotulos" style="width: 288px">
                                                                                            <tr>
                                                                                                <td width="230">Forma de Pagamento:<br />
                                                                                                    <asp:TextBox ID="txtFormaDePagamento" runat="server" CssClass="campos"
                                                                                                        ReadOnly="True" Text='<%# Bind("condicaoNome") %>' Width="230px"></asp:TextBox>
                                                                                                </td>
                                                                                                <td style="padding-left: 8px">Parcelas:<br />
                                                                                                    <asp:TextBox ID="txtParcelas" runat="server" CssClass="campos" ReadOnly="True"
                                                                                                        Text='<%# Bind("numeroDeParcelas") %>' Width="50px"></asp:TextBox>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr class="rotulos">
                                                                                    <td style="padding-top: 5px">Situação:<br />
                                                                                        <asp:TextBox ID="txtSituacao" runat="server" CssClass="campos" ReadOnly="True"
                                                                                            Text='<%# Bind("situacao") %>' Width="288px"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="padding-top: 5px">
                                                                                        <a target="_blank" href="https://www.moip.com.br/Instrucao.do?token=<%# Eval("moipToken") %>">
                                                                                            <img src="images/abrirnomoip.png" />
                                                                                        </a>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr class="rotulos">
                                                                                    <td style="padding-top: 5px">Valor Frete:<br />
                                                                                        <asp:TextBox ID="txtFrete" runat="server" CssClass="campos" ReadOnly="True"
                                                                                            Width="85px" Text='<%# Bind("valorDoFrete", "{0:C}") %>'></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr class="rotulos">
                                                                                    <td style="padding-top: 5px">Valor dos Itens:<br />
                                                                                        <asp:TextBox ID="txtValorDosItens" runat="server" CssClass="campos"
                                                                                            ReadOnly="True" Width="85px" Text='<%# Bind("valorDosItens", "{0:C}") %>'></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <asp:Panel runat="server" ID="pnPagamento">
                                                                                    <tr class="rotulos" style="display: none;">
                                                                                        <td style="padding-top: 5px">Embrulho/Cartão:<br />
                                                                                            <asp:TextBox ID="txtEmbruloCartao" runat="server" CssClass="campos"
                                                                                                ReadOnly="True" Width="85px"
                                                                                                Text='<%# Bind("valorDoEmbrulhoECartao", "{0:C}") %>'></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr class="rotulos" style="display: none;">
                                                                                        <td style="padding-top: 5px">Juros:<br />
                                                                                            <asp:TextBox ID="txtJuros" runat="server" CssClass="campos" ReadOnly="True"
                                                                                                Width="85px" Text='<%# Bind("valorDosJuros", "{0:C}") %>'></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr class="rotulos">
                                                                                        <td style="padding-top: 5px">Valor Total Geral:<br />
                                                                                            <asp:TextBox ID="txtValorTotalGeral" runat="server" CssClass="campos"
                                                                                                ReadOnly="True" Width="85px"
                                                                                                Text='<%# Bind("valorTotalGeral", "{0:C}") %>'></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr class="rotulos">
                                                                                        <td style="padding-top: 5px">Valor do Desconto do cupom:<br />
                                                                                            <asp:TextBox ID="txtValorDoDesconto" runat="server" CssClass="campos"
                                                                                                ReadOnly="True" Width="125px"
                                                                                                Text='<%# Bind("valorDoDescontoDoCupom", "{0:C}") %>'></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr class="rotulos">
                                                                                        <td style="padding-top: 5px">Valor do Desconto do pagamento:<br />
                                                                                            <asp:TextBox ID="txtValorDoDescontoDoPagamento" runat="server" CssClass="campos"
                                                                                                ReadOnly="True" Width="125px"
                                                                                                Text='<%# Bind("valorDoDescontoDoPagamento", "{0:C}") %>'></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr class="rotulos" style="display: none;">
                                                                                        <td style="padding-top: 5px">Valor da Parcela:<br />
                                                                                            <asp:TextBox ID="txtValorDaParcela" runat="server" CssClass="campos"
                                                                                                Height="22px" ReadOnly="True" Text='<%# Bind("valorDaParcela", "{0:C}") %>'
                                                                                                Width="165px"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr class="rotulos">
                                                                                        <td style="padding-top: 5px">Valor Cobrado:<br />
                                                                                            <asp:TextBox ID="txtValorCobrado" runat="server" CssClass="campos"
                                                                                                Height="22px" ReadOnly="True" Text='<%# Bind("valorCobrado", "{0:C}") %>'
                                                                                                Width="165px"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                </asp:Panel>
                                                                                <asp:Panel runat="server" ID="pnDoisPagamentos" Visible="False">
                                                                                    <tr>
                                                                                        <td>&nbsp;</td>
                                                                                    </tr>
                                                                                    <tr class="rotulos">
                                                                                        <td style="padding-top: 5px">Forma de Pagamento 1:<br />
                                                                                            <asp:TextBox ID="txtFormaPagamento1" runat="server" CssClass="campos"
                                                                                                Height="22px" ReadOnly="True" Text='<%# Bind("condicaoNome1") %>'
                                                                                                Width="165px"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr class="rotulos">
                                                                                        <td style="padding-top: 5px">Valor total Pagamento 1:<br />
                                                                                            <asp:TextBox ID="txtValorPagamento1" runat="server" CssClass="campos"
                                                                                                Height="22px" ReadOnly="True" Text='<%# Bind("valorPagamento1", "{0:C}") %>'
                                                                                                Width="165px"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr class="rotulos">
                                                                                        <td style="padding-top: 5px">Parcelas Pagamento 1:<br />
                                                                                            <asp:TextBox ID="txtParcelasPagamento1" runat="server" CssClass="campos"
                                                                                                Height="22px" ReadOnly="True" Text='<%# Bind("parcelasPagamento1") %>'
                                                                                                Width="165px"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr class="rotulos">
                                                                                        <td style="padding-top: 5px">Valor da Parcela Pagamento 1:<br />
                                                                                            <asp:TextBox ID="txtValorParcelaPagamento1" runat="server" CssClass="campos"
                                                                                                Height="22px" ReadOnly="True" Text='<%# Bind("valorParcelaPagamento1", "{0:C}") %>'
                                                                                                Width="165px"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>&nbsp;</td>
                                                                                    </tr>
                                                                                    <tr class="rotulos">
                                                                                        <td style="padding-top: 5px">Forma de Pagamento 2:<br />
                                                                                            <asp:TextBox ID="txtFormaPagamento2" runat="server" CssClass="campos"
                                                                                                Height="22px" ReadOnly="True" Text='<%# Bind("condicaoNome2") %>'
                                                                                                Width="165px"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr class="rotulos">
                                                                                        <td style="padding-top: 5px">Valor total Pagamento 2:<br />
                                                                                            <asp:TextBox ID="txtValorPagamento2" runat="server" CssClass="campos"
                                                                                                Height="22px" ReadOnly="True" Text='<%# Bind("valorPagamento2", "{0:C}") %>'
                                                                                                Width="165px"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr class="rotulos">
                                                                                        <td style="padding-top: 5px">Parcelas Pagamento 2:<br />
                                                                                            <asp:TextBox ID="txtParcelasPagamento2" runat="server" CssClass="campos"
                                                                                                Height="22px" ReadOnly="True" Text='<%# Bind("parcelasPagamento2") %>'
                                                                                                Width="165px"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr class="rotulos">
                                                                                        <td style="padding-top: 5px">Valor da Parcela Pagamento 2:<br />
                                                                                            <asp:TextBox ID="txtValorParcelasPagamento2" runat="server" CssClass="campos"
                                                                                                Height="22px" ReadOnly="True" Text='<%# Bind("valorParcelaPagamento2", "{0:C}") %>'
                                                                                                Width="165px"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                </asp:Panel>
                                                                                <tr class="rotulos" style="font-weight: bold; display: none;">
                                                                                    <td style="padding-top: 8px">Cupom de Desconto:</td>
                                                                                </tr>
                                                                                <tr class="rotulos" style="display: none;">
                                                                                    <td style="padding: 8px 15px 15px 15px; border: 1px solid #7EACB1;">% de desconto em relação ao valor cobrado:<br />
                                                                                        <asp:TextBox ID="txtCupomDeDesconto" runat="server" CssClass="campos"
                                                                                            Width="85px"></asp:TextBox>
                                                                                        &nbsp;<asp:ImageButton ID="imbCupom" runat="server" ImageAlign="AbsMiddle"
                                                                                            ImageUrl="images/btSalvarPeq1.jpg" OnClick="imbCupom_Click" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr class="rotulos">
                                                                                    <td style="padding-top: 5px;">
                                                                                        <asp:CheckBox runat="server" ID="chkMaoPropria" Text="Enviar por Mão Própria" Visible="false" />
                                                                                        <asp:CheckBox runat="server" ID="chkReversa" Text="Gerar Reversa" Checked='<%#Convert.ToBoolean(Eval("gerarReversa"))%>' /><br />
                                                                                        <asp:ImageButton ID="btnSalvarMaoPropria" runat="server" ImageAlign="AbsMiddle" ImageUrl="images/btSalvarPeq1.jpg" OnClick="btnSalvarMaoPropria_Click" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="padding-top: 25px;">
                                                                                        <asp:Button Text="Cancelar Pedido" Width="100%" runat="server" ID="btnCancelarPedido" OnClick="btnCancelarPedido_OnClick" OnClientClick="if(confirm('Deseja realmente cancelar este pedido?')){popCancelarPedido.Show(); return false;}else{return false;}" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="padding-top: 25px;">
                                                                                        <asp:Button Text="Priorizar Pedido" Width="100%" runat="server" ID="btnPriorizarPedido" OnClick="btnPriorizarPedido_OnClick" OnClientClick="return confirm('Deseja realmente priorizar este pedido?');" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="padding-top: 25px;">
                                                                                        <asp:Button Text="Pedido Enviado" Width="100%" runat="server" ID="btnPedidoEnviado" OnClick="btnPedidoEnviado" OnClientClick="return confirm('Deseja realmente alterar o status deste pedido para enviado?');" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="padding-top: 25px;">
                                                                                        <asp:Button Text="Reenviar este Pedido" Width="100%" runat="server" ID="btnReenviarPedido" OnClick="btnReenviarPedido_OnClick" OnClientClick="return confirm('Deseja realmente reenviar este pedido?');" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="padding-top: 25px;">
                                                                                        <asp:Button Text="Reenviar Confirmação de Pagamento" Width="100%" runat="server" ID="btnReenviarConfirmacaoDePagamento" OnClick="btnReenviarConfirmacaoDePagamento_OnClick" OnClientClick="return confirm('Deseja realmente reenviar a confirmação de pagamentodo do pedido?');" Visible="false" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="padding-top: 25px;">
                                                                                        <asp:Button Text="Descancelar Pedido" Width="100%" runat="server" ID="btnDescancelarPedido" OnClick="btnDescancelarPedido_OnClick" OnClientClick="return confirm('Confirma o descancelamento do pedido?');" Visible="false" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Fields>
                                                </asp:DetailsView>
                                            </dx:ContentControl>
                                        </ContentCollection>
                                    </dx:TabPage>

                                    <dx:TabPage Text="Produtos">
                                        <ContentCollection>
                                            <dx:ContentControl runat="server">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <table class="rotulos">
                                                                <tr>
                                                                    <td colspan="4" height="25">
                                                                        <b>Informações do Pedido:</b>
                                                                    </td>
                                                                </tr>
                                                                <tr style="display: none;">
                                                                    <td colspan="4" height="25">Data de confirmação de pagamento
                                                                        <asp:Literal runat="server" ID="litDataConfirmacaoPagamento"></asp:Literal>
                                                                    </td>
                                                                </tr>
                                                                <tr style="display: none;">
                                                                    <td colspan="4" height="25">Prazo máximo de postagem:
                                                                        <asp:Literal runat="server" ID="litPrazoMaximoFabricacao"></asp:Literal>
                                                                    </td>
                                                                </tr>
                                                                <tr style="display: none;">
                                                                    <td colspan="4" height="25">Prazo do maior fornecedor:
                                                                        <asp:Literal runat="server" ID="litPrazoMaiorFornecedor"></asp:Literal>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="4" height="25">Prazo para o cep:<%--Prazo do carrinho de compras *correios:--%><asp:Literal runat="server" ID="litPrazoCepEnvio"></asp:Literal>
                                                                        - 
                                                                        <asp:Literal runat="server" ID="litPrazoCorreios"></asp:Literal>
                                                                        dias úteis
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="4" height="25">Recebimento do pedido em até:
                                                                        <asp:Literal runat="server" ID="litPrazoEntrega"></asp:Literal>
                                                                        dias
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="4" height="25">
                                                                        <b>Pedidos ao fornecedor:</b>
                                                                        <br />
                                                                        <br />
                                                                        <div style="border: 1px solid #CCC; width: 140px;">
                                                                            <div style="background-color: #D2E9FF; font-family: Tahoma; font-size: 11px; border-bottom-color: #7EACB1;">Enviado</div>
                                                                            <div style="background-color: #FFFFBB; font-family: Tahoma; font-size: 11px; border-bottom-color: #7EACB1;">Entregue</div>
                                                                            <div style="background-color: #FFC6C7; font-family: Tahoma; font-size: 11px;">Não Entregue</div>
                                                                            <div style="background-color: #F2B84E; font-family: Tahoma; font-size: 11px;">Não Endereçado</div>
                                                                        </div>
                                                                        <br />
                                                                        <asp:Literal runat="server" ID="litPrevisaoEntrega"></asp:Literal>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <table cellpadding="0" cellspacing="0" class="rotulos" style="width: 782px">
                                                                <tr bgcolor="#D4E3E6">
                                                                    <td height="25" style="width: 380px; padding-left: 15px;"><b>Descrição dos Itens:</b></td>
                                                                    <td style="width: 133px"><b>Quant.</b></td>
                                                                    <td style="width: 133px"><b>Preço(R$):</b></td>
                                                                    <td style="width: 133px"><b>Total(R$):</b></td>
                                                                    <td style="width: 133px; text-align: center;"><b>Remover:</b></td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 370px; padding-left: 15px;">&nbsp;</td>
                                                                    <td style="width: 133px">&nbsp;</td>
                                                                    <td style="width: 133px">&nbsp;</td>
                                                                    <td style="width: 133px">&nbsp;</td>
                                                                    <td style="width: 133px">&nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="5">
                                                                        <hr color="#7EACB1" size="1" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <asp:ListView ID="dtlItensPedido" runat="server" CellPadding="0" OnDataBound="dtlItensPedido_OnDataBound"
                                                                DataSourceID="sqlItensPedido" OnItemDataBound="dtlItensPedido_ItemDataBound">
                                                                <ItemTemplate>
                                                                    <table cellpadding="0" cellspacing="0" class="rotulos" style="width: 782px">
                                                                        <tr>
                                                                            <td style="width: 380px; padding-left: 15px;">
                                                                                <table cellpadding="0" cellspacing="0" style="width: 305px">
                                                                                    <tr>
                                                                                        <td width="60">
                                                                                            <asp:Image ID="fotoDestaque" runat="server" Height="70px" Width="70px" />
                                                                                        </td>
                                                                                        <td class="textoPreto" style="padding-left: 10px">
                                                                                            <asp:HiddenField ID="txtItemPedidoId" runat="server" Value='<%# Bind("itemPedidoId") %>' />
                                                                                            <asp:HiddenField ID="hdfPedidoId" runat="server" Value='<%# Bind("pedidoId") %>' />
                                                                                            <asp:HiddenField ID="hdfProdutoCancelado" runat="server" Value='<%# Bind("cancelado") %>' />
                                                                                            <asp:HiddenField ID="hdfProdutoBrinde" runat="server" Value='<%# Bind("brinde") %>' />
                                                                                            <asp:HiddenField ID="hdfEnviado" runat="server" Value='<%# Bind("enviado") %>' />

                                                                                            <asp:Label ID="lblProdutoNome" runat="server" Text='<%# Bind("produtoNome") %>'></asp:Label>
                                                                                            <br />
                                                                                            <b>Id do produto:</b>
                                                                                            <asp:Label ID="lblProdutoId" runat="server" Text='<%# Bind("produtoId") %>'></asp:Label><br />
                                                                                            <b>Item Pedido:</b>
                                                                                            <asp:Label ID="lblItemPedido" runat="server" Text='<%# Bind("itemPedidoId") %>'></asp:Label>
                                                                                            <br />
                                                                                            <b>Preço de Custo:</b>
                                                                                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("produtoPrecoDeCusto", "{0:c}") %>'></asp:Label><br />
                                                                                            <b>Id da empresa:</b>
                                                                                            <asp:Label ID="lblProdutoIdEmpresa" runat="server" Text='<%# Bind("produtoIdDaEmpresa") %>'></asp:Label>
                                                                                            -
                                                                                            <asp:Label ID="lblComplementoId" runat="server" Text='<%# Bind("complementoIdDaEmpresa") %>'></asp:Label>
                                                                                            <br />
                                                                                            <div class="hover" id="divHoverPrazoFornecedor" runat="server" style="cursor: pointer;">
                                                                                                <b>Fornecedor:</b><asp:Label ID="lblFornecedor" runat="server" Text='<%# Bind("fornecedorNome") %>'></asp:Label><br />

                                                                                                <div class="tooltip">
                                                                                                    <table>
                                                                                                        <tr>
                                                                                                            <td colspan="5">Prazo: <span style="font-size: 15px;"><%#Eval("fornecedorPrazoPedidos")%></span> dias úteis
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td colspan="5" style="border-top: 1px solid white;">Gera Romaneio
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td>Seg.
                                                                                                            </td>
                                                                                                            <td>Ter.
                                                                                                            </td>
                                                                                                            <td>Qua.
                                                                                                            </td>
                                                                                                            <td>Qui.
                                                                                                            </td>
                                                                                                            <td>Sex.
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="<%#Convert.ToBoolean(Eval("pedidoSegunda")) ? "green-Check" : "red-X"  %>">
                                                                                                                <%#Convert.ToBoolean(Eval("pedidoSegunda")) ? "&#10004;" : "&#10006;" %> 
                                                                                                            </td>
                                                                                                            <td class="<%#Convert.ToBoolean(Eval("pedidoTerca")) ? "green-Check" : "red-X"  %>">
                                                                                                                <%#Convert.ToBoolean(Eval("pedidoTerca")) ? "&#10004;" : "&#10006;" %> 
                                                                                                            </td>
                                                                                                            <td class="<%#Convert.ToBoolean(Eval("pedidoQuarta")) ? "green-Check" : "red-X"  %>">
                                                                                                                <%#Convert.ToBoolean(Eval("pedidoQuarta")) ? "&#10004;" : "&#10006;" %> 
                                                                                                            </td>
                                                                                                            <td class="<%#Convert.ToBoolean(Eval("pedidoQuinta")) ? "green-Check" : "red-X"  %>">
                                                                                                                <%#Convert.ToBoolean(Eval("pedidoQuinta")) ? "&#10004;" : "&#10006;" %> 
                                                                                                            </td>
                                                                                                            <td class="<%#Convert.ToBoolean(Eval("pedidoSexta")) ? "green-Check" : "red-X"  %>">
                                                                                                                <%#Convert.ToBoolean(Eval("pedidoSexta")) ? "&#10004;" : "&#10006;" %> 
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                            <td style="width: 113px">
                                                                                <asp:Label ID="lblQuantidade" runat="server"
                                                                                    Text='<%# Bind("itemQuantidade") %>'></asp:Label>
                                                                            </td>
                                                                            <td style="width: 133px">
                                                                                <asp:Label ID="lblPreco" runat="server"
                                                                                    Text='<%# Bind("itemValor", "{0:c}") %>'></asp:Label>
                                                                            </td>
                                                                            <td style="width: 133px">
                                                                                <asp:Label ID="lblPrecoTotal" runat="server"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 133px; text-align: center;">
                                                                                <dx:ASPxButton ID="btnCancelarItem" runat="server" Text="Remover" AutoPostBack="False" Style="margin: 0 auto;">
                                                                                    <ClientSideEvents Click='function(s, e) { ShowNotaWindow(<%# Eval("itemPedidoId") %>); }' />
                                                                                </dx:ASPxButton>
                                                                                <dx:ASPxButton ID="btnProdutoPersonalizado" runat="server" Text="Personalização" AutoPostBack="False"
                                                                                    OnClick="btnProdutoPersonalizado_OnClick" Style="margin: 5px auto;" CommandArgument='<%#Eval("produtoId") +"#"+ Eval("itemPedidoId") %>'>
                                                                                </dx:ASPxButton>
                                                                            </td>
                                                                        </tr>

                                                                        <tr>
                                                                            <td style="width: 100%; padding-left: 15px;" colspan="6">
                                                                                <asp:DataList ID="lstCombo" runat="server" CellPadding="0" OnItemDataBound="lstCombo_OnItemDataBound" Width="100%">
                                                                                    <ItemTemplate>
                                                                                        <table cellpadding="0" cellspacing="0" style="width: 100%">
                                                                                            <tr>
                                                                                                <td width="105">&nbsp;
                                                                                                <asp:HiddenField ID="hdfAutorizarParcial" runat="server" Value='<%# Bind("autorizarParcial") %>' />
                                                                                                    <asp:HiddenField ID="hdfIdItemPedidoEstoque" runat="server" Value='<%# Bind("idItemPedidoEstoque") %>' />
                                                                                                    <asp:HiddenField ID="hdfProdutoId" runat="server" Value='<%# Bind("produtoId") %>' />
                                                                                                    <asp:HiddenField ID="hdfEnviado" runat="server" Value='<%# Bind("enviado") %>' />
                                                                                                    <asp:HiddenField ID="hdfIdCentroDistribuicao" runat="server" Value='<%# Bind("idCentroDistribuicao") %>' />
                                                                                                </td>
                                                                                                <td class="textoPreto" style="padding-left: 10px; padding-top: 15px;">
                                                                                                    <asp:Label ID="lblProdutoNome" runat="server" Text='<%# Bind("produtoNome") %>'></asp:Label>
                                                                                                    <%# Eval("motivoAdicao").ToString() != "" ? " (" + Eval("motivoAdicao") + ")" : "" %>
                                                                                                    <br />
                                                                                                    <b>Id do produto:</b>
                                                                                                    <asp:Label ID="lblProdutoId" runat="server" Text='<%# Bind("produtoId") %>'></asp:Label>
                                                                                                    <b>Id da empresa:</b>
                                                                                                    <asp:Label ID="lblProdutoIdEmpresa" runat="server" Text='<%# Bind("produtoIdDaEmpresa") %>'></asp:Label>
                                                                                                    <b>Preço Produto:</b>
                                                                                                    <asp:Label ID="lblProdutoPreco" runat="server" Text='<%# Convert.ToDecimal(Eval("preco")).ToString("C") %>'></asp:Label>
                                                                                                    <br />
                                                                                                    <b>Fornecedor:</b>
                                                                                                    <asp:Label ID="lblFornecedor" runat="server" Text='<%# Bind("fornecedorNome") %>'></asp:Label><br />
                                                                                                    <br />
                                                                                                    <b>Entregas de Romaneio:</b>
                                                                                                    <asp:Label ID="lblEntregaRomaneio" runat="server"></asp:Label><br />
                                                                                                    <asp:Label ID="lblItemRomaneio" runat="server"></asp:Label>
                                                                                                    <b><a href='produtosEstoqueEtiquetas.aspx?produtoId=<%# Eval("produtoId") %>' target="_blank">Consultar Etiquetas</a> - 
                                                                                                       <a href='filaEstoque.aspx?produtoId=<%# Eval("produtoId") %>' target="_blank">Consultar Fila</a>
                                                                                                        <asp:HyperLink runat="server" ID="hplTrocarEtiqueta" Target="_blank" Visible="False"> - Trocar Etiqueta</asp:HyperLink>
                                                                                                        <asp:LinkButton runat="server" ID="btnAtribuirEtiqueta" Visible="false" Text=" - Atribuir Etiqueta" CommandArgument='<%# Eval("idItemPedidoEstoque") %>' OnCommand="btnAtribuirEtiqueta_Command"></asp:LinkButton>
                                                                                                    </b>
                                                                                                </td>
                                                                                                <td>
                                                                                                    <td style="width: 133px">
                                                                                                        <dx:ASPxButton ID="btnEnvioParcial" runat="server" Text="Remover" AutoPostBack="False">
                                                                                                            <ClientSideEvents Click='function(s, e) { ShowNotaWindow(<%# Eval("idItemPedidoEstoque") %>); }' />
                                                                                                        </dx:ASPxButton>
                                                                                                    </td>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </ItemTemplate>
                                                                                </asp:DataList>
                                                                            </td>
                                                                        </tr>

                                                                        <tr>
                                                                            <td colspan="5">
                                                                                <hr color="#7EACB1" size="1" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </ItemTemplate>
                                                            </asp:ListView>
                                                            <script type="text/javascript">
                                                                function autorizarParcial(idItemPedido, idProduto, enviado) {
                                                                    var chkParcialCadastrarFaltando = document.getElementById('trParcialCadastrarFaltando');
                                                                    var hdfParcialItemPedido = document.getElementById('hdfParcialItemPedido');
                                                                    var hdfParcialProduto = document.getElementById('hdfParcialProduto');
                                                                    var hdfParcialEnviado = document.getElementById('hdfParcialEnviado');
                                                                    if (enviado == "sim") {
                                                                        chkParcialCadastrarFaltando.style.visibility = 'visible';
                                                                        hdfParcialEnviado.value = '1';
                                                                    } else {
                                                                        chkParcialCadastrarFaltando.style.visibility = 'hidden';
                                                                        hdfParcialEnviado.value = '0';
                                                                    }
                                                                    hdfParcialItemPedido.value = idItemPedido;
                                                                    hdfParcialProduto.value = idProduto;
                                                                    popAutorizarParcial.Show();
                                                                }
                                                                function cancelarItemPedido(idItemPedido) {
                                                                    var hdfItemCancelar = document.getElementById('hdfItemCancelar');
                                                                    hdfItemCancelar.value = idItemPedido;
                                                                    popCancelarItemPedido.Show();
                                                                }
                                                                function adicionarItemPedido() {
                                                                    popAdicionarItemPedido.Show();
                                                                }
                                                            </script>
                                                            <table width="100%">
                                                                <tr>
                                                                    <td style="text-align: right; padding-right: 20px;">

                                                                        <div style="float: right">
                                                                            <dx:ASPxButton ID="btnAdicionarItemPedido" runat="server" Text="Adicionar Item ao Pedido" AutoPostBack="False" OnClick="btnAdicionarItemPedido_OnClick">
                                                                            </dx:ASPxButton>
                                                                        </div>

                                                                        <div style="float: right; padding-right: 10px;">
                                                                            <dx:ASPxButton ID="btnProdutosCancelados" runat="server" Text="Produtos Cancelados" AutoPostBack="False" OnClick="btnProdutosCancelados_OnClick">
                                                                            </dx:ASPxButton>
                                                                        </div>

                                                                        <div style="float: right; padding-right: 10px;">
                                                                            <dx:ASPxButton ID="btnProdutosPendentesDeEnvio" runat="server" Text="Produtos não Enviados" AutoPostBack="False" OnClick="btnProdutosPendentesDeEnvio_OnClick">
                                                                            </dx:ASPxButton>
                                                                        </div>
                                                                    </td>

                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label runat="server" ID="lblCustoPedido" Visible="False"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </dx:ContentControl>
                                        </ContentCollection>
                                    </dx:TabPage>


                                    <dx:TabPage Text="Pagamentos">
                                        <ContentCollection>
                                            <dx:ContentControl runat="server">
                                                <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
                                                    <tr>
                                                        <td>
                                                            <table style="width: 100%;">
                                                                <tr>
                                                                    <td style="padding-right: 10px;">
                                                                        <table style="width: 100%">
                                                                            <tr class="rotulos" style="font-weight: bold;">
                                                                                <td>Forma de Pagamento:<br />
                                                                                </td>
                                                                                <td>Valor Cobrado:<br />
                                                                                </td>
                                                                                <td>Total Pago:<br />
                                                                                </td>
                                                                                <td>Total Pendente:<br />
                                                                                </td>
                                                                                <td>Faltando Cobrança:<br />
                                                                                </td>
                                                                            </tr>
                                                                            <tr class="rotulos">
                                                                                <td>
                                                                                    <asp:Literal ID="txtFormaDePagamento" runat="server"></asp:Literal>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:Literal ID="txtTotalPedido" runat="server"></asp:Literal>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:Literal ID="txtTotalPago" runat="server"></asp:Literal>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:Literal ID="txtTotalPendente" runat="server"></asp:Literal>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:Literal ID="txtFaltandoCobranca" runat="server"></asp:Literal>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                    <td style="width: 290px;">
                                                                        <asp:Panel ID="pnlFraudes" runat="server" Width="290px" Height="91px" Visible="False">
                                                                            <%--ScrollBars="Vertical"--%>
                                                                            <table cellpadding="0" cellspacing="0" style="width: 272px">
                                                                                <%--<tr>
                                                                                    <td><iframe ID="I1" frameborder="0" name="I1" scrolling="no" src='FControlFrame.aspx?codPedido=<% =Request.QueryString["pedidoId"] %>' style="WIDTH: 272px; HEIGHT: 90px"></iframe></td>
                                                                                </tr>--%>
                                                                                <tr>
                                                                                    <td>
                                                                                        <%--<iframe id="frameClearSale" frameborder="0" name="I2" scrolling="no" runat="server" src='clearSaleFrame.aspx?codPedido=<%= Request.QueryString["pedidoId"] %>' style="width: 272px; height: 90px; display: none;"></iframe>--%>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </asp:Panel>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table style="width: 100%;">
                                                                <tr class="rotulos" style="font-weight: bold;">
                                                                    <td style="padding-right: 10px;">Número</td>
                                                                    <td style="padding-right: 10px;">Vencimento</td>
                                                                    <td style="padding-right: 10px;">Valor Cobrado</td>
                                                                    <td style="padding-right: 10px;">Desconto</td>
                                                                    <td style="padding-right: 10px;">Valor Total</td>
                                                                    <td style="padding-right: 10px;">Parcelas</td>
                                                                    <td style="padding-right: 10px;">Cobrança</td>
                                                                    <td style="padding-right: 10px;">Pagamento</td>
                                                                    <td style="padding-right: 10px;">Operador</td>
                                                                    <td style="padding-right: 10px;">Tipo</td>
                                                                    <td style="padding-right: 10px;">Obs.</td>
                                                                    <td style="padding-right: 10px;">Status</td>
                                                                    <td style="padding-right: 10px; text-align: center;">Ações</td>
                                                                </tr>
                                                                <asp:ListView runat="server" ID="lstPagamentos" OnItemDataBound="lstPagamentos_ItemDataBound">
                                                                    <ItemTemplate>
                                                                        <tr class="rotulos" id="trPagamento" runat="server">
                                                                            <td style="padding-right: 10px;"><%# Eval("idPedidoPagamento") %></td>
                                                                            <td style="padding-right: 10px;"><%# Eval("dataVencimento", "{0:d}") %></td>
                                                                            <td style="padding-right: 10px;"><%# Eval("valor", "{0:C}") %></td>
                                                                            <td style="padding-right: 10px;"><%# Eval("valorDesconto", "{0:C}") %></td>
                                                                            <td style="padding-right: 10px;"><%# Eval("valorTotal", "{0:C}") %></td>
                                                                            <td style="padding-right: 10px;"><%# Eval("parcelas") %></td>
                                                                            <td style="padding-right: 10px;"><%# Eval("numeroCobranca") %></td>
                                                                            <td style="padding-right: 10px;"><%# Eval("dataPagamento", "{0:d}") %></td>
                                                                            <td style="padding-right: 10px;">
                                                                                <asp:Literal ID="litOperador" runat="server"></asp:Literal></td>
                                                                            <td style="padding-right: 10px;">
                                                                                <asp:Literal ID="litTipo" runat="server"></asp:Literal></td>
                                                                            <td style="padding-right: 10px;"><%# Eval("observacao") %></td>
                                                                            <td style="padding-right: 10px;">
                                                                                <asp:Literal runat="server" ID="litStatusPagamento" Text='<%# Eval("statusGateway") %>'></asp:Literal></td>
                                                                            <td style="padding-right: 10px; text-align: center;">
                                                                                <dx:ASPxMenu CssClass="centerTable" Width="100%" ID="mAcoesPagamento" OnItemClick="mAcoesPagamento_OnItemClick" runat="server" HorizontalAlign="Center">
                                                                                    <Items>
                                                                                        <dx:MenuItem Text="Ações">
                                                                                            <Items>
                                                                                                <dx:MenuItem Name="cancelar" Text="Cancelar Pagamento">
                                                                                                </dx:MenuItem>
                                                                                                <dx:MenuItem Name="naoautorizado" Text="Pagamento Não Autorizado">
                                                                                                </dx:MenuItem>
                                                                                                <dx:MenuItem Name="dados" Text="Dados do Cartão">
                                                                                                </dx:MenuItem>
                                                                                                <dx:MenuItem Visible="False" Name="segundavia" Text="Enviar Segunda Via">
                                                                                                </dx:MenuItem>
                                                                                                <dx:MenuItem Name="confirmar" Text="Confirmar Pagamento">
                                                                                                </dx:MenuItem>
                                                                                                <dx:MenuItem Name="reativarPagamento" Text="Reativar Pagamento">
                                                                                                </dx:MenuItem>
                                                                                            </Items>
                                                                                        </dx:MenuItem>
                                                                                    </Items>
                                                                                </dx:ASPxMenu>
                                                                                <dx:ASPxButton Visible="False" ID="btnCancelarPagamento" runat="server" Text="Cancelar Pagamento" CssClass="botaoComum" AutoPostBack="False">
                                                                                </dx:ASPxButton>
                                                                                <dx:ASPxButton Visible="False" ID="btnNaoAutorizado" runat="server" Text="Não Autorizado" CssClass="botaoVermelho">
                                                                                </dx:ASPxButton>
                                                                                <dx:ASPxButton Visible="False" ID="btnDadosCartao" runat="server" Text="Dados do Cartão" CssClass="botaoVermelho" AutoPostBack="False">
                                                                                </dx:ASPxButton>
                                                                                <dx:ASPxButton Visible="False" ID="btnSegundaVia" runat="server" Text="Enviar Segunda Via" CssClass="botaoComum" CommandArgument='<%# Eval("idPedidoPagamento") %>' OnCommand="btnSegundaVia_Command">
                                                                                    <ClientSideEvents Click="function(s, e) {e.processOnServer = confirm('Deseja realmente enviar uma segunda via deste boleto?');}"></ClientSideEvents>
                                                                                </dx:ASPxButton>
                                                                                <dx:ASPxButton Visible="False" ID="btnConfirmarPagamento" runat="server" Text="Confirmar Pagamento" CssClass="botaoVerde" AutoPostBack="False">
                                                                                </dx:ASPxButton>
                                                                                <dx:ASPxButton Visible="False" ID="btnReativarPagamento" runat="server" Text="Reativar Pagamento" CssClass="botaoVerde" AutoPostBack="False">
                                                                                </dx:ASPxButton>
                                                                            </td>
                                                                        </tr>
                                                                    </ItemTemplate>
                                                                </asp:ListView>
                                                                <tr class="rotulos" style="font-weight: bold;">
                                                                    <td style="padding-right: 10px;"></td>
                                                                    <td style="padding-right: 10px;"></td>
                                                                    <td style="padding-right: 10px;"></td>
                                                                    <td style="padding-right: 10px;"></td>
                                                                    <td style="padding-right: 10px;"></td>
                                                                    <td style="padding-right: 10px;"></td>
                                                                    <td style="padding-right: 10px;"></td>
                                                                    <td style="padding-right: 10px;"></td>
                                                                    <td colspan="3" style="padding-right: 10px; text-align: center; padding-top: 20px;">
                                                                        <dx:ASPxButton ID="btnGerarNovaCobranca" runat="server" Text="Gerar nova Cobrança" CssClass="botaoComum" AutoPostBack="False">
                                                                            <ClientSideEvents Click="function(s, e) {popGerarPagamento.Show();}"></ClientSideEvents>
                                                                        </dx:ASPxButton>
                                                                        <asp:HiddenField runat="server" ID="hdfPagamentoSegundaVia" Value="" ClientIDMode="Static" />
                                                                    </td>
                                                                </tr>
                                                                <script type="text/javascript">

                                                                    function AcaoPagamento(e, idPedidoPagamento, idOperadorPagamento, caminhoVirtualAdmin) {
                                                                        if (e.item.name == 'cancelar') {
                                                                            CancelarPagamento(idPedidoPagamento);
                                                                            e.processOnServer = false;
                                                                            return false;
                                                                        }
                                                                        else if (e.item.name == 'naoautorizado') {
                                                                            showPopPagamentoNaoAutorizado(idPedidoPagamento);
                                                                            e.processOnServer = false;
                                                                            return false;
                                                                        }
                                                                        else if (e.item.name == 'confirmar') {
                                                                            popConfirmarPagamentoShow(idPedidoPagamento, idOperadorPagamento);
                                                                            e.processOnServer = false;
                                                                            return false;
                                                                        }
                                                                        else if (e.item.name == 'segundavia') {
                                                                            $("#hdfPagamentoSegundaVia").val(idPedidoPagamento);
                                                                            e.processOnServer = confirm('Deseja realmente enviar uma segunda via deste boleto?');
                                                                            return true;

                                                                        }
                                                                        else if (e.item.name == 'dados') {
                                                                            openNewWindow('500', '230', false, false, false, false, false, false, 'popCartao', caminhoVirtualAdmin + 'admin/popCartao.aspx?idPedidoPagamento=' + idPedidoPagamento);
                                                                            e.processOnServer = false;
                                                                            return false;
                                                                        }
                                                                        else if (e.item.name == 'reativarPagamento') {
                                                                            popReativarPagamentoShow(idPedidoPagamento);
                                                                            e.processOnServer = false;
                                                                            return false;
                                                                        }
                                                                        e.processOnServer = false;
                                                                    }

                                                                    function openNewWindow(w, h, nav, loc, sts, menu, scroll, resize, name, url) {//v1.0
                                                                        var windowProperties = ''; if (nav == false) windowProperties += 'toolbar=no,'; else
                                                                            windowProperties += 'toolbar=yes,'; if (loc == false) windowProperties += 'location=no,';
                                                                        else windowProperties += 'location=yes,'; if (sts == false) windowProperties += 'status=no,';
                                                                        else windowProperties += 'status=yes,'; if (menu == false) windowProperties += 'menubar=no,';
                                                                        else windowProperties += 'menubar=yes,'; if (scroll == false) windowProperties += 'scrollbars=no,';
                                                                        else windowProperties += 'scrollbars=yes,'; if (resize == false) windowProperties += 'resizable=no,';
                                                                        else windowProperties += 'resizable=yes,'; if (w != "") windowProperties += 'width=' + w + ',';
                                                                        if (h != "") windowProperties += 'height=' + h; if (windowProperties != "") {
                                                                            if (windowProperties.charAt(windowProperties.length - 1) == ',')
                                                                                windowProperties = windowProperties.substring(0, windowProperties.length - 1);
                                                                        }
                                                                        window.open(url, name, windowProperties);
                                                                    }

                                                                    function CancelarPagamento(idPedidoPagamento) {
                                                                        var hdfCancelarPagamento = document.getElementById("hdfCancelarPagamento");
                                                                        hdfCancelarPagamento.value = idPedidoPagamento;
                                                                        popCancelarPagamento.Show();
                                                                    }

                                                                    function showPopPagamentoNaoAutorizado(idPedidoPagamento) {
                                                                        var hdfPagamentoNaoAutorizado = document.getElementById("hdfPagamentoNaoAutorizado");
                                                                        hdfPagamentoNaoAutorizado.value = idPedidoPagamento;
                                                                        popPagamentoNaoAutorizado.Show();
                                                                    }

                                                                    function popConfirmarPagamentoShow(idPedidoPagamento, idOperadorPagamento) {
                                                                        var hdfPagamentoNaoAutorizado = document.getElementById("hdfConfirmarPagamento");
                                                                        var ddlOperadorPagamentoConfirmacao = document.getElementById('ddlOperadorPagamentoConfirmacao');
                                                                        hdfPagamentoNaoAutorizado.value = idPedidoPagamento;
                                                                        ddlOperadorPagamentoConfirmacao.value = idOperadorPagamento;
                                                                        popConfirmarPagamento.Show();
                                                                    }
                                                                    function popReativarPagamentoShow(idPedidoPagamento) {
                                                                        var hdfReativarPagamento = document.getElementById("hdfReativarPagamento");
                                                                        hdfReativarPagamento.value = idPedidoPagamento;
                                                                        popReativarPagamento.Show();
                                                                    }
                                                                </script>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </dx:ContentControl>
                                        </ContentCollection>
                                    </dx:TabPage>

                                    <dx:TabPage Text="Interações / Agendamentos">
                                        <ContentCollection>
                                            <dx:ContentControl runat="server">
                                                <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
                                                    <tr>
                                                        <td>
                                                            <table cellpadding="0" cellspacing="15" style="width: 834px" border="0" bordercolor="#7EACB1">
                                                                <tr>
                                                                    <td style="border: 1px solid #7EACB1; padding-left: 15px; padding-bottom: 10px;">
                                                                        <table cellpadding="0" cellspacing="0" style="width: 774px">
                                                                            <tr>
                                                                                <td style="padding-top: 20px;" class="rotulos">
                                                                                    <b>Agendamentos e Chamados</b>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:DataList ID="dtlChamados" runat="server" CellPadding="0" DataSourceID="sqlChamados" OnItemDataBound="dtlChamados_OnItemDataBound">
                                                                                        <ItemTemplate>
                                                                                            <table cellpadding="0" cellspacing="0" class="rotulos" style="width: 782px">
                                                                                                <tr>
                                                                                                    <td height="25" style="padding-left: 20px; width: 140px; border-top-style: solid; border-top-width: 1px; border-top-color: #7EACB1;">
                                                                                                        <asp:HiddenField ID="hdfConcluido" runat="server" Value='<%# Bind("concluido") %>'></asp:HiddenField>
                                                                                                        <asp:Label ID="lblData" runat="server" Text='<%# Bind("dataLimite") %>'></asp:Label>
                                                                                                    </td>
                                                                                                    <td style="width: 200px; border-top-style: solid; border-top-width: 1px; border-top-color: #7EACB1;">
                                                                                                        <asp:Label ID="lblusuario" runat="server" Text='<%# Bind("usuarioNome") %>'></asp:Label>
                                                                                                    </td>
                                                                                                    <td style="border-top-style: solid; border-width: 1px; border-color: #7EACB1">
                                                                                                        <asp:Literal ID="ltrTitulo" runat="server" Text='<%# Bind("titulo") %>'></asp:Literal>
                                                                                                    </td>
                                                                                                    <td style="border-top-style: solid; border-width: 1px; border-color: #7EACB1; width: 80px;">
                                                                                                        <asp:ImageButton ID="btnAlterarChamado" CommandArgument='<%# Eval("idChamado") %>'
                                                                                                            OnCommand="btnAlterarChamado_OnCommand" runat="server" ImageAlign="AbsMiddle" ImageUrl="images/btAlterar.jpg" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </ItemTemplate>
                                                                                        <HeaderTemplate>
                                                                                            <table cellpadding="0" cellspacing="0" class="rotulos" style="width: 782px">
                                                                                                <tr bgcolor="#D4E3E6">
                                                                                                    <td height="25" style="padding-left: 20px; width: 140px;">
                                                                                                        <b>Data:</b></td>
                                                                                                    <td style="width: 200px">
                                                                                                        <b>Responsável:</b></td>
                                                                                                    <td>
                                                                                                        <b>Chamado:</b></td>
                                                                                                    <td style="width: 80px;">
                                                                                                        <b>Alterar:</b></td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </HeaderTemplate>
                                                                                    </asp:DataList>
                                                                                    <asp:SqlDataSource ID="sqlChamados" runat="server" ConnectionString="<%$ ConnectionStrings:connectionString %>"
                                                                                        SelectCommand="SELECT tbChamado.concluido, tbChamado.dataLimite, tbChamado.titulo, tbChamado.descricao, 
                                                                                        tbChamado.idChamado, tbChamado.idChamadoDepartamento, tbChamado.idChamadotarefa, tbChamado.idChamadoAssunto,
                                                                                        tbUsuario.usuarioNome FROM [tbChamado] left join [tbUsuario] on [tbChamado].idUsuario = [tbUsuario].usuarioId
                                                                                        WHERE ([idPedido] = @idPedido) order by tbChamado.concluido, tbChamado.dataLimite">
                                                                                        <SelectParameters>
                                                                                            <asp:QueryStringParameter Name="idPedido" QueryStringField="pedidoId" Type="Int32" />
                                                                                        </SelectParameters>
                                                                                    </asp:SqlDataSource>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="text-align: right; padding-top: 20px;">
                                                                                    <asp:Button runat="server" ID="btnAdicionarChamado" Text="Adicionar Chamado" OnClick="btnAdicionarChamado_OnClick" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <table cellpadding="0" cellspacing="15" style="width: 834px" border="0"
                                                                bordercolor="#7EACB1">
                                                                <tr>
                                                                    <td
                                                                        style="border: 1px solid #7EACB1; padding-left: 15px; padding-bottom: 10px;">
                                                                        <table cellpadding="0" cellspacing="0" style="width: 774px">
                                                                            <tr>
                                                                                <td style="padding-top: 20px; padding-right: 20px;" class="rotulos">
                                                                                    <b>Interação</b><ce:editor id="txtInteracao" runat="server"
                                                                                        autoconfigure="Full_noform" contextmenumode="Simple"
                                                                                        editorwysiwygmodecss="" filespath="" height="400px"
                                                                                        urltype="Default" userelativelinks="False"
                                                                                        width="782px"
                                                                                        removeservernamesfromurl="False">
                                                                                  <TextAreaStyle CssClass="campos" />
                                                                              <%--    <FrameStyle BackColor="White" BorderColor="#DDDDDD" BorderStyle="Solid" 
                                                                                      BorderWidth="1px" CssClass="CuteEditorFrame" Height="100%" Width="100%" />--%>
                                                                              </ce:editor></td>
                                                                            </tr>

                                                                            <tr>
                                                                                <td class="rotulos" valign="middle" align="right" style="padding-top: 5px; padding-bottom: 5px; padding-right: 20px;">
                                                                                    <asp:DropDownList runat="server" ID="ddlEmailInteracao">
                                                                                        <Items>
                                                                                            <asp:ListItem Value="" Text="Enviar por email"></asp:ListItem>
                                                                                            <asp:ListItem Value="lfelipef1@hotmail.com" Text="Felipe"></asp:ListItem>
                                                                                            <asp:ListItem Value="financeiro@graodegente.com.br" Text="Luís"></asp:ListItem>
                                                                                            <asp:ListItem Value="atendimento@graodegente.com.br" Text="Carol"></asp:ListItem>
                                                                                            <asp:ListItem Value="compras@graodegente.com.br" Text="Gabriella"></asp:ListItem>
                                                                                            <asp:ListItem Value="posvendas@graodegente.com.br" Text="Tatianne"></asp:ListItem>
                                                                                            <asp:ListItem Value="atendimento@bark.com.br" Text="Lindsay"></asp:ListItem>
                                                                                            <asp:ListItem Value="callili.ibt@jadlog.com.br,alecio_callili@yahoo.com.br" Text="Jadlog"></asp:ListItem>
                                                                                            <asp:ListItem Value="happybaby@graodegente.com.br,contato@happybabyconfeccoes.com.br" Text="Happy Baby"></asp:ListItem>
                                                                                            <asp:ListItem Value="pedidos@batistela.com.br" Text="Batistela"></asp:ListItem>
                                                                                            <asp:ListItem Value="producao@brunababy.com.br" Text="Bruna Baby"></asp:ListItem>
                                                                                            <asp:ListItem Value="decalta.faturamento@gmail.com" Text="Decalta"></asp:ListItem>
                                                                                            <asp:ListItem Value="surukinha.babycom@terra.com.br" Text="Surukinha"></asp:ListItem>
                                                                                            <asp:ListItem Value="contato@brubrelelbabys.com.br" Text="Brubrelel"></asp:ListItem>
                                                                                            <asp:ListItem Value="sac9@ioneenxovais.com.br" Text="Ione"></asp:ListItem>
                                                                                            <asp:ListItem Value="talismaconfeccoes@hotmail.com" Text="Talismã"></asp:ListItem>
                                                                                            <asp:ListItem Value="rogeriabichos@bol.com.br" Text="Baby Toys"></asp:ListItem>
                                                                                        </Items>
                                                                                        <%--<asp:ListItem Text="Enviar por email"></asp:ListItem>
                                                                                        <asp:ListItem Text="Felipe" Value="lfelipef1@hotmail.com"></asp:ListItem>
                                                                                        <asp:ListItem Text="Lu&#237;s" Value="financeiro@graodegente.com.br"></asp:ListItem>
                                                                                        <asp:ListItem Text="Carol" Value="atendimento@graodegente.com.br"></asp:ListItem>
                                                                                        <asp:ListItem Text="Gabriella" Value="compras@graodegente.com.br"></asp:ListItem>
                                                                                        <asp:ListItem Text="Tatianne" Value="posvendas@graodegente.com.br"></asp:ListItem>
                                                                                        <asp:ListItem Text="Lindsay" Value="atendimento@bark.com.br"></asp:ListItem>
                                                                                        <asp:ListItem Text="Jadlog" Value="callili.ibt@jadlog.com.br,alecio_callili@yahoo.com.br"></asp:ListItem>
                                                                                        <asp:ListItem Text="Happy Baby" Value="happybaby@graodegente.com.br,contato@happybabyconfeccoes.com.br"></asp:ListItem>
                                                                                        <asp:ListItem Text="Batistela" Value="pedidos@batistela.com.br"></asp:ListItem>
                                                                                        <asp:ListItem Text="Bruna Baby" Value="producao@brunababy.com.br"></asp:ListItem>
                                                                                        <asp:ListItem Text="Decalta" Value="decalta.faturamento@gmail.com"></asp:ListItem>
                                                                                        <asp:ListItem Text="Surukinha" Value="surukinha.babycom@terra.com.br"></asp:ListItem>
                                                                                        <asp:ListItem Text="Brubrelel" Value="contato@brubrelelbabys.com.br"></asp:ListItem>
                                                                                        <asp:ListItem Text="Ione" Value="sac9@ioneenxovais.com.br"></asp:ListItem>
                                                                                        <asp:ListItem Text="Talism&#227;" Value="talismaconfeccoes@hotmail.com"></asp:ListItem>
                                                                                        <asp:ListItem Text="Baby Toys" Value="rogeriabichos@bol.com.br"></asp:ListItem>--%>
                                                                                        <%--<asp:ListItem Text="Enviar por email"></asp:ListItem>
                                                                                        <asp:ListItem Text="Felipe" Value="lfelipef1@hotmail.com"></asp:ListItem>
                                                                                        <asp:ListItem Text="Lu&#237;s" Value="financeiro@graodegente.com.br"></asp:ListItem>
                                                                                        <asp:ListItem Text="Carol" Value="atendimento@graodegente.com.br"></asp:ListItem>
                                                                                        <asp:ListItem Text="Gabriella" Value="compras@graodegente.com.br"></asp:ListItem>
                                                                                        <asp:ListItem Text="Tatianne" Value="posvendas@graodegente.com.br"></asp:ListItem>
                                                                                        <asp:ListItem Text="Lindsay" Value="atendimento@bark.com.br"></asp:ListItem>
                                                                                        <asp:ListItem Text="Jadlog" Value="callili.ibt@jadlog.com.br,alecio_callili@yahoo.com.br"></asp:ListItem>
                                                                                        <asp:ListItem Text="Happy Baby" Value="happybaby@graodegente.com.br,contato@happybabyconfeccoes.com.br"></asp:ListItem>
                                                                                        <asp:ListItem Text="Batistela" Value="pedidos@batistela.com.br"></asp:ListItem>
                                                                                        <asp:ListItem Text="Bruna Baby" Value="producao@brunababy.com.br"></asp:ListItem>
                                                                                        <asp:ListItem Text="Decalta" Value="decalta.faturamento@gmail.com"></asp:ListItem>
                                                                                        <asp:ListItem Text="Surukinha" Value="surukinha.babycom@terra.com.br"></asp:ListItem>
                                                                                        <asp:ListItem Text="Brubrelel" Value="contato@brubrelelbabys.com.br"></asp:ListItem>
                                                                                        <asp:ListItem Text="Ione" Value="sac9@ioneenxovais.com.br"></asp:ListItem>
                                                                                        <asp:ListItem Text="Talism&#227;" Value="talismaconfeccoes@hotmail.com"></asp:ListItem>
                                                                                        <asp:ListItem Text="Baby Toys" Value="rogeriabichos@bol.com.br"></asp:ListItem>--%>
                                                                                    </asp:DropDownList>&nbsp;
                                                                                    <asp:ImageButton ID="btSalvarInteracao" runat="server"
                                                                                        ImageUrl="images/btSalvarPeq1.jpg" OnClick="btSalvarInteracao_Click" />&nbsp;
                                                                                    <asp:ImageButton ID="imbEnviarInteracaoParaCliente" runat="server"
                                                                                        ImageUrl="images/btEnviarParaCliente.jpg"
                                                                                        OnClick="imbEnviarInteracaoParaCliente_Click"
                                                                                        OnClientClick="return confirm('Tem certeza que deseja enviar essa interacão para o cliente?');" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr id="trStatus" runat="server" visible="False">
                                                                                <td class="rotulos" valign="middle"
                                                                                    style="padding-top: 5px; padding-bottom: 5px">Alterar situação:
                                                                                                <asp:DropDownList ID="drpSituacao" runat="server" CssClass="campos"
                                                                                                    Height="17px" Width="294px" DataSourceID="sqlSituacao"
                                                                                                    DataTextField="situacao" DataValueField="situacaoId"
                                                                                                    OnDataBound="drpSituacao_DataBound">
                                                                                                </asp:DropDownList>
                                                                                    <asp:ObjectDataSource ID="sqlSituacao" runat="server" SelectMethod="situacaoSeleciona" TypeName="rnSituacao"></asp:ObjectDataSource>
                                                                                    &nbsp;<asp:ImageButton ID="imbAlterarSituacao" runat="server" ImageAlign="AbsMiddle"
                                                                                        ImageUrl="images/btAlterar.jpg" OnClick="imbAlterarSituacao_Click" /><br />
                                                                                    Situação Interna:
                                                                                                <asp:DropDownList ID="drpSituacaoInterna" runat="server" CssClass="campos"
                                                                                                    Height="17px" Width="294px" DataSourceID="sqlSituacaoInterna"
                                                                                                    DataTextField="situacaoInterna" DataValueField="situacaoInternaId"
                                                                                                    OnDataBound="drpSituacaoInterna_DataBound">
                                                                                                </asp:DropDownList><br />
                                                                                    <asp:CheckBox ID="chkEnviarEmailFornecedor" Enabled="False" Text="Enviar E-mail para Fornecedor" Checked="False" runat="server" />
                                                                                    <asp:ObjectDataSource ID="sqlSituacaoInterna" runat="server" SelectMethod="situacaoInternaSeleciona" TypeName="rnSituacao"></asp:ObjectDataSource>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:DataList ID="dtlInteracao" runat="server" CellPadding="0"
                                                                                        DataSourceID="sqlInteracaoes">
                                                                                        <ItemTemplate>
                                                                                            <table cellpadding="0" cellspacing="0" class="rotulos" style="width: 782px">
                                                                                                <tr>
                                                                                                    <td height="25"
                                                                                                        style="padding-left: 20px; width: 140px; border-top-style: solid; border-top-width: 1px; border-top-color: #7EACB1;">
                                                                                                        <asp:Label ID="lblData" runat="server" Text='<%# Bind("interacaoData") %>'></asp:Label>
                                                                                                    </td>
                                                                                                    <td style="width: 200px; border-top-style: solid; border-top-width: 1px; border-top-color: #7EACB1;">
                                                                                                        <asp:Label ID="lblusuario" runat="server"
                                                                                                            Text='<%# Bind("usuarioQueInteragiu") %>'></asp:Label>
                                                                                                    </td>
                                                                                                    <td style="border-top-style: solid; border-width: 1px; border-color: #7EACB1">
                                                                                                        <asp:Literal ID="ltrInteracao" runat="server" Text='<%# Bind("interacao") %>'></asp:Literal>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </ItemTemplate>
                                                                                        <HeaderTemplate>
                                                                                            <table cellpadding="0" cellspacing="0" class="rotulos" style="width: 782px">
                                                                                                <tr bgcolor="#D4E3E6">
                                                                                                    <td height="25" style="padding-left: 20px; width: 140px;">
                                                                                                        <b>Data:</b></td>
                                                                                                    <td style="width: 200px">
                                                                                                        <b>Usuário:</b></td>
                                                                                                    <td>
                                                                                                        <b>Interação:</b></td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </HeaderTemplate>
                                                                                    </asp:DataList>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr class="rotulos">
                                                                    <td></td>
                                                                </tr>
                                                                <tr class="rotulos" style="display: none;">
                                                                    <td>
                                                                        <asp:Panel runat="server" ID="pnCodigoDeBarras" DefaultButton="btnCodDeBarras">
                                                                            Código de Barras:<br />
                                                                            <asp:TextBox ID="txtCodDeBarras" runat="server" CssClass="campos"></asp:TextBox>
                                                                            <asp:DropDownList Visible="False" ID="ddlProdutosCodBarras" runat="server"></asp:DropDownList>
                                                                            <asp:Button ID="btnCodDeBarras" runat="server" Text="Marcar como Entregue" OnClick="btnCodDeBarras_Click" />
                                                                        </asp:Panel>
                                                                    </td>
                                                                </tr>
                                                                <tr style="display: none;">
                                                                    <td style="border: 1px solid #7EACB1; padding-left: 15px; padding-top: 15px; padding-bottom: 15px;"></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </table>
                                            </dx:ContentControl>
                                        </ContentCollection>
                                    </dx:TabPage>

                                    <dx:TabPage Text="Nota Fiscal / Envios">
                                        <ContentCollection>
                                            <dx:ContentControl runat="server">
                                                <table style="width: 100%;">
                                                    <tr>
                                                        <td style="text-align: right; padding-top: 15px;">
                                                            <dx:ASPxButton ID="btShowModal" runat="server" Text="Gerar Nota Fiscal" AutoPostBack="False">
                                                                <ClientSideEvents Click="function(s, e) { ShowNotaWindow(); }" />
                                                            </dx:ASPxButton>

                                                            <script type="text/javascript">
                                                                // <![CDATA[
                                                                function ShowNotaWindow() { pcNota.Show(); }
                                                                // ]]>
                                                            </script>
                                                            <br />
                                                            <dx:ASPxButton ID="btShowModalNotaDevolucao" runat="server" Text="Gerar Nota Fiscal Devolução" AutoPostBack="False" Enabled="false">
                                                                <ClientSideEvents Click="function(s, e) { ShowNotaDevolucaoWindow(); }" />
                                                            </dx:ASPxButton>

                                                            <script type="text/javascript">
                                                                // <![CDATA[
                                                                function ShowNotaDevolucaoWindow() { pcNotaDevolucao.Show(); }
                                                                // ]]>
                                                            </script>
                                                        </td>
                                                    </tr>
                                                    <tr class="rotulos">
                                                        <td style="padding-top: 15px; padding-bottom: 5px; font-size: 14px;">
                                                            <b>Envios e Códigos de Rastreio
                                                            </b>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="border: 1px solid #7EACB1">
                                                            <table style="width: 100%;">
                                                                <tr class="rotulos" style="font-weight: bold;">
                                                                    <td>Código
                                                                    </td>
                                                                    <td style="text-align: center;">Forma
                                                                    </td>
                                                                    <td style="text-align: center;">Prazo
                                                                    </td>
                                                                    <td style="text-align: center;">Status
                                                                    </td>
                                                                    <td style="text-align: center;">Largura
                                                                    </td>
                                                                    <td style="text-align: center;">Altura
                                                                    </td>
                                                                    <td style="text-align: center;">Comprimento
                                                                    </td>
                                                                    <td style="text-align: center;">Peso
                                                                    </td>
                                                                    <td style="text-align: right;">Alterar
                                                                    </td>
                                                                    <td></td>
                                                                    <td style="text-align: center; width: 105px;" runat="server" id="tdTaxaExtraTitulo">Taxa Extra
                                                                    </td>
                                                                </tr>
                                                                <asp:ListView runat="server" ID="lstPacotes" OnItemDataBound="lstPacotes_OnItemDataBound">
                                                                    <ItemTemplate>
                                                                        <tr class="rotulos">
                                                                            <td>
                                                                                <asp:HiddenField ID="hdfDespachado" runat="server" Value='<%# Eval("despachado") %>' />
                                                                                <%# Eval("rastreio") %>
                                                                            </td>
                                                                            <td style="text-align: center;">
                                                                                <%# Eval("formaDeEnvio") %>
                                                                            </td>
                                                                            <td style="text-align: center;">
                                                                                <%# Eval("prazoFinalEntrega") %>
                                                                            </td>
                                                                            <td style="text-align: center;">
                                                                                <%# Eval("statusAtual") %>
                                                                            </td>
                                                                            <td style="text-align: center;">
                                                                                <%# Eval("largura") %>
                                                                            </td>
                                                                            <td style="text-align: center;">
                                                                                <%# Eval("altura") %>
                                                                            </td>
                                                                            <td style="text-align: center;">
                                                                                <%# Eval("profundidade") %>
                                                                            </td>
                                                                            <td style="text-align: center;">
                                                                                <%# Eval("peso") %>
                                                                            </td>
                                                                            <td style="text-align: center;">
                                                                                <asp:ImageButton ID="btnAlterarPacote" CommandArgument='<%# Eval("idPedidoPacote") %>' OnCommand="btnAlterarPacote_OnCommand" runat="server" ImageAlign="AbsMiddle" ImageUrl="images/btAlterar.jpg" />
                                                                            </td>
                                                                            <td style="text-align: right;">
                                                                                <asp:Button Visible="False" ID="btnResetarPesagem" CommandArgument='<%# Eval("idPedidoPacote") %>' OnCommand="btnResetarPesagem_Command" runat="server" Text="Reiniciar Pesagem" OnClientClick="return confirm('Deseja reamente reiniciar a pesagem?')" />&nbsp;&nbsp;
                                                                                <asp:Button Visible="False" ID="btnReenviarCodigoRastreio" CommandArgument='<%# Eval("idPedidoPacote") %>' OnCommand="btnReenviarCodigoRastreio_OnCommand" runat="server" Text="Reenviar Rastreio" OnClientClick="return confirm('Deseja reamente reenviar o código de rastreio ao cliente?')" />
                                                                            </td>
                                                                            <td runat="server" id="tdTaxaExtraCampos">
                                                                                <asp:TextBox runat="server" ID="txtTaxaExtra" Text='<%# Eval("taxaExtra") %>' Width="60px"></asp:TextBox>
                                                                                <asp:Button runat="server" ID="btnSalvarTaxaExtra" Text="OK" CommandArgument='<%# Eval("idPedidoPacote") + "#" + Container.DataItemIndex %>' OnCommand="btnSalvarTaxaExtra_OnCommand" OnClientClick="return confirm('Informar taxa extra?')" />
                                                                            </td>
                                                                        </tr>
                                                                    </ItemTemplate>
                                                                </asp:ListView>
                                                            </table>
                                                        </td>
                                                    </tr>

                                                    <tr class="rotulos">
                                                        <td style="padding-top: 15px; padding-bottom: 5px; font-size: 14px;">
                                                            <b>Notas Fiscais
                                                            </b>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="border: 1px solid #7EACB1">
                                                            <table style="width: 100%;">
                                                                <tr class="rotulos" style="font-weight: bold;">
                                                                    <td>Nota
                                                                    </td>
                                                                    <td>Chave
                                                                    </td>
                                                                    <td style="text-align: center;">Status
                                                                    </td>
                                                                    <td style="text-align: center;">Danfe
                                                                    </td>
                                                                    <td style="text-align: center;">Carta de Correção
                                                                    </td>
                                                                    <td style="text-align: center;">Reenviar
                                                                    </td>
                                                                    <td style="text-align: center;">Devolução
                                                                    </td>
                                                                </tr>
                                                                <asp:ListView runat="server" ID="lstNotas" OnItemDataBound="lstNotas_OnItemDataBound">
                                                                    <ItemTemplate>
                                                                        <tr class="rotulos">
                                                                            <td>
                                                                                <asp:HiddenField runat="server" ID="hdf" />
                                                                                <%# Eval("numeroNota") %>
                                                                            </td>
                                                                            <td>
                                                                                <%# Eval("nfeKey") %>
                                                                            </td>
                                                                            <td style="text-align: center;">
                                                                                <asp:Literal runat="server" ID="litNotaStatus"></asp:Literal>
                                                                            </td>
                                                                            <td style="text-align: center;">
                                                                                <asp:HyperLink runat="server" ID="hplNotaDanfe">Danfe</asp:HyperLink>
                                                                            </td>
                                                                            <td style="text-align: center;">
                                                                                <asp:LinkButton runat="server" CommandArgument='<%# Eval("idNotaFiscal") %>' ID="btnCartaoCorrecao" OnCommand="btnCartaoCorrecao_OnCommand">Gerar C/C</asp:LinkButton>
                                                                            </td>
                                                                            <td style="text-align: center;">
                                                                                <asp:LinkButton runat="server" CommandArgument='<%# Eval("idNotaFiscal") %>' ID="btnReenviarNota" OnCommand="btnReenviarNota_Command">Reenviar</asp:LinkButton>
                                                                            </td>
                                                                            <td style="text-align: center;">
                                                                                <asp:LinkButton runat="server" CommandArgument='<%# Eval("idNotaFiscal") %>' ID="btnDevolverNota" OnCommand="btnDevolverNota_Command">Devolução</asp:LinkButton>
                                                                            </td>
                                                                        </tr>
                                                                    </ItemTemplate>
                                                                </asp:ListView>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding-top: 20px;">
                                                            <div style="padding-top: 20px;">
                                                                <table cellpadding="0" cellspacing="0" style="width: 100%">
                                                                    <tr>
                                                                        <td style="padding-top: 20px; padding-right: 20px;" class="rotulos">
                                                                            <b>Interação para Transportadora</b><br />
                                                                            <asp:TextBox runat="server" ID="txtInteracaoTransportadora" TextMode="MultiLine" Width="100%" Height="50px"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="rotulos" valign="middle" align="right" style="padding-top: 5px; padding-bottom: 5px; padding-right: 20px;">
                                                                            <asp:ImageButton ID="btnSalvarInteracaoTransportadora" runat="server" ImageUrl="../admin/images/btSalvarPeq1.jpg" OnClick="btnSalvarInteracaoTransportadora_Click" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:DataList ID="dtlInteracoesTransportadora" runat="server" CellPadding="0">
                                                                                <ItemTemplate>
                                                                                    <table cellpadding="0" cellspacing="0" class="rotulos" style="width: 782px">
                                                                                        <tr>
                                                                                            <td height="25" style="padding-left: 20px; width: 140px; border-top-style: solid; border-top-width: 1px; border-top-color: #7EACB1;">
                                                                                                <asp:Label ID="lblData" runat="server" Text='<%# Bind("data") %>'></asp:Label>
                                                                                            </td>
                                                                                            <td style="width: 200px; border-top-style: solid; border-top-width: 1px; border-top-color: #7EACB1;">
                                                                                                <asp:Label ID="lblusuario" runat="server"
                                                                                                    Text='<%# Bind("usuario") %>'></asp:Label>
                                                                                            </td>
                                                                                            <td style="border-top-style: solid; border-width: 1px; border-color: #7EACB1">
                                                                                                <asp:Literal ID="ltrInteracao" runat="server" Text='<%# Bind("interacao") %>'></asp:Literal>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </ItemTemplate>
                                                                                <HeaderTemplate>
                                                                                    <table cellpadding="0" cellspacing="0" class="rotulos" style="width: 782px">
                                                                                        <tr bgcolor="#D4E3E6">
                                                                                            <td height="25" style="padding-left: 20px; width: 140px;">
                                                                                                <b>Data:</b></td>
                                                                                            <td style="width: 200px">
                                                                                                <b>Usuário:</b></td>
                                                                                            <td>
                                                                                                <b>Interação:</b></td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </HeaderTemplate>
                                                                            </asp:DataList>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </dx:ContentControl>
                                        </ContentCollection>
                                    </dx:TabPage>
                                </TabPages>
                            </dx:ASPxPageControl>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td></td>
        </tr>
    </table>

    <asp:ObjectDataSource ID="sqlInteracaoes" runat="server" SelectMethod="interacoSeleciona_PorPedidoId" TypeName="rnInteracoes">
        <SelectParameters>
            <asp:QueryStringParameter Name="pedidoId" QueryStringField="pedidoId"
                Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="sqlPedido" runat="server" SelectMethod="pedidoSelecionaAdmin_PorPedidoId" TypeName="rnPedidos">
        <SelectParameters>
            <asp:QueryStringParameter Name="pedidoId" QueryStringField="pedidoId" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:LinqDataSource ID="sqlUsuario" runat="server"
        ContextTypeName="dbCommerceDataContext" EnableDelete="True"
        EnableInsert="True" EnableUpdate="True" TableName="tbUsuarios">
    </asp:LinqDataSource>
    <asp:ObjectDataSource ID="sqlItensPedido" runat="server"
        SelectMethod="itensPedidoSelecionaAdmin_PorPedidoId" TypeName="rnPedidos">
        <SelectParameters>
            <asp:QueryStringParameter Name="pedidoId" QueryStringField="pedidoId"
                Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="sqlTipoDeEntrega" runat="server" SelectMethod="tipoDeEntregaSeleciona" TypeName="rnTipoDeEntrega"></asp:ObjectDataSource>


    <script type="text/javascript">
        $(document).ready(function () {
            $("#ddlCondicaoPagamentoNovaCobranca").change(function () {
                var valor = $("#ddlCondicaoPagamentoNovaCobranca").val();
                if (valor == "4" | valor == "31" | valor == "41") {
                    $("#novoPagamentoDadosCartao").hide();
                } else {
                    $("#novoPagamentoDadosCartao").show();
                }
            });
        });

    </script>
    <dx:ASPxPopupControl ID="popGerarPagamento" runat="server" CloseAction="CloseButton" Modal="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
        ClientInstanceName="popGerarPagamento" HeaderText="Gerar novo Pagamento" AllowDragging="True" PopupAnimationType="None" EnableViewState="True" Height="500" AutoUpdatePosition="True">
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl13" runat="server">
                <div style="width: 800px; height: 500px;" class="rotulos">
                    <div>
                        <b>Condicao de Pagamento:</b><br />
                        <asp:DropDownList CssClass="campos" runat="server" ID="ddlCondicaoPagamentoNovaCobranca" ClientIDMode="Static" DataValueField="condicaoId" DataTextField="condicaoNome">
                            <Items>
                                <asp:ListItem Value="4" Text="Boleto"></asp:ListItem>
                                <asp:ListItem Value="41" Text="Boleto - Desconto sem frete"></asp:ListItem>
                                <asp:ListItem Value="3" Text="Visa"></asp:ListItem>
                                <asp:ListItem Value="5" Text="Mastercard"></asp:ListItem>
                                <asp:ListItem Value="6" Text="Diners"></asp:ListItem>
                                <asp:ListItem Value="7" Text="American Express"></asp:ListItem>
                                <asp:ListItem Value="10" Text="Elo"></asp:ListItem>
                                <asp:ListItem Value="14" Text="Hipercard"></asp:ListItem>
                                <asp:ListItem Value="22" Text="Sorocred"></asp:ListItem>
                                <asp:ListItem Value="31" Text="Crédito"></asp:ListItem>
                            </Items>
                        </asp:DropDownList>
                    </div>
                    <div>
                        <b>Valor:</b><br />
                        <asp:TextBox CssClass="campos" runat="server" ID="txtValorNovaCobranca">
                        </asp:TextBox><br />
                    </div>
                    <div>
                        <b>Vencimento:</b><br />
                        <asp:TextBox CssClass="campos" runat="server" ID="txtVencimentoNovaCobranca">
                        </asp:TextBox><br />
                    </div>
                    <div id="novoPagamentoDadosCartao" style="display: none;">
                        <div>
                            <b>Nome no Cartão:</b><br />
                            <asp:TextBox CssClass="campos" runat="server" ID="txtNomeCartaoNovaCobranca" Width="300">
                            </asp:TextBox><br />
                        </div>
                        <div>
                            <b>Numero no Cartão:</b><br />
                            <asp:TextBox CssClass="campos" runat="server" ID="txtNumeroCartaoNovaCobranca" Width="300">
                            </asp:TextBox><br />
                        </div>
                        <div>
                            <b>Parcelas:</b><br />
                            <asp:DropDownList CssClass="campos" runat="server" ID="ddlParcelasNovaCobranca">
                                <Items>
                                    <asp:ListItem Value="01" Text="01"></asp:ListItem>
                                    <asp:ListItem Value="02" Text="02"></asp:ListItem>
                                    <asp:ListItem Value="03" Text="03"></asp:ListItem>
                                    <asp:ListItem Value="04" Text="04"></asp:ListItem>
                                    <asp:ListItem Value="05" Text="05"></asp:ListItem>
                                    <asp:ListItem Value="06" Text="06"></asp:ListItem>
                                    <asp:ListItem Value="07" Text="07"></asp:ListItem>
                                    <asp:ListItem Value="08" Text="08"></asp:ListItem>
                                    <asp:ListItem Value="09" Text="09"></asp:ListItem>
                                    <asp:ListItem Value="10" Text="10"></asp:ListItem>
                                    <asp:ListItem Value="11" Text="11"></asp:ListItem>
                                    <asp:ListItem Value="12" Text="12"></asp:ListItem>
                                </Items>
                            </asp:DropDownList>
                        </div>
                        <div>
                            <b>Validade (Mês):</b><br />
                            <asp:DropDownList CssClass="campos" runat="server" ID="ddlValidadeMesNovaCobranca">
                                <Items>
                                    <asp:ListItem Value="01" Text="01"></asp:ListItem>
                                    <asp:ListItem Value="02" Text="02"></asp:ListItem>
                                    <asp:ListItem Value="03" Text="03"></asp:ListItem>
                                    <asp:ListItem Value="04" Text="04"></asp:ListItem>
                                    <asp:ListItem Value="05" Text="05"></asp:ListItem>
                                    <asp:ListItem Value="06" Text="06"></asp:ListItem>
                                    <asp:ListItem Value="07" Text="07"></asp:ListItem>
                                    <asp:ListItem Value="08" Text="08"></asp:ListItem>
                                    <asp:ListItem Value="09" Text="09"></asp:ListItem>
                                    <asp:ListItem Value="10" Text="10"></asp:ListItem>
                                    <asp:ListItem Value="11" Text="11"></asp:ListItem>
                                    <asp:ListItem Value="12" Text="12"></asp:ListItem>
                                </Items>
                            </asp:DropDownList>
                        </div>
                        <div>
                            <b>Validade (Ano):</b><br />
                            <asp:DropDownList CssClass="campos" runat="server" ID="ddlValidadeAnoNovaCobranca">
                                <Items>
                                    <asp:ListItem Value="2014" Text="2014"></asp:ListItem>
                                    <asp:ListItem Value="2015" Text="2015"></asp:ListItem>
                                    <asp:ListItem Value="2016" Text="2016"></asp:ListItem>
                                    <asp:ListItem Value="2017" Text="2017"></asp:ListItem>
                                    <asp:ListItem Value="2018" Text="2018"></asp:ListItem>
                                    <asp:ListItem Value="2019" Text="2019"></asp:ListItem>
                                    <asp:ListItem Value="2020" Text="2020"></asp:ListItem>
                                    <asp:ListItem Value="2021" Text="2021"></asp:ListItem>
                                    <asp:ListItem Value="2022" Text="2022"></asp:ListItem>
                                    <asp:ListItem Value="2023" Text="2023"></asp:ListItem>
                                    <asp:ListItem Value="2024" Text="2024"></asp:ListItem>
                                </Items>
                            </asp:DropDownList>
                        </div>
                        <div>
                            <b>Cód. Segurança:</b><br />
                            <asp:TextBox CssClass="campos" runat="server" ID="txtCodSegurancaNovaCobranca">
                            </asp:TextBox><br />
                        </div>
                    </div>
                    <div>
                        <b>Observação:</b><br />
                        <asp:TextBox runat="server" CssClass="campos" ID="txtObservacaoNovaCobranca" Width="100%" Height="80px" TextMode="MultiLine"></asp:TextBox><br />
                        <br />
                    </div>
                    <asp:Button runat="server" ID="btnGerarNovoPagamento" OnClick="btnGerarNovoPagamento_OnClick" Text="Gerar Pagamento" />
                    <%--<uc:OneClickButton ID="btnGerarNovoPagamento" runat="server" Text="Gerar Pagamento" ReplaceTitleTo="Aguarde..." onclick="btnGerarNovoPagamento_OnClick" />--%>
                    <div style="font-size: 0.91em;">
                        *Em caso de estar sendo realizado a troca da forma de pagamento, primeiramente cancelar a forma "incorreta" para só então informar a nova forma de pagamento.
                    </div>
                </div>
            </dx:PopupControlContentControl>
        </ContentCollection>
        <ContentStyle>
            <Paddings PaddingBottom="5px" />
        </ContentStyle>
    </dx:ASPxPopupControl>
    <dx:ASPxPopupControl ID="popConfirmarPagamento" runat="server" CloseAction="CloseButton" Modal="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
        ClientInstanceName="popConfirmarPagamento" HeaderText="Confirmar Pagamento" AllowDragging="True" PopupAnimationType="None" EnableViewState="True" Height="500" AutoUpdatePosition="True">
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl12" runat="server">
                <asp:HiddenField runat="server" ID="hdfConfirmarPagamento" ClientIDMode="static" />
                <div style="width: 800px; height: 500px;" class="rotulos">
                    <b>Operador de Pagamento:</b><br />
                    <asp:DropDownList ValidationGroup="confirmarpagamento" CausesValidation="False" CssClass="campos" runat="server" ID="ddlOperadorPagamentoConfirmacao" ClientIDMode="Static" DataValueField="idOperadorPagamento" DataTextField="nome">
                    </asp:DropDownList><br />
                    <b>Código de Autorização:</b><br />
                    <asp:TextBox ValidationGroup="confirmarpagamento" CausesValidation="False" CssClass="campos" runat="server" ID="txtNumeroCobrancaConfirmacao">
                    </asp:TextBox><br />
                    <b>Observação:</b><br />
                    <asp:TextBox ValidationGroup="confirmarpagamento" CausesValidation="False" runat="server" CssClass="campos" ID="txtObservacaoConfirmacaoPagamento" Width="100%" Height="80px" TextMode="MultiLine"></asp:TextBox><br />
                    <br />
                    <asp:Button runat="server" ID="btnConfirmarPagamento" OnClick="btnConfirmarPagamento_OnClick" Text="Confirmar Pagamento" />
                    <%--<uc:OneClickButton ValidationGroup="confirmarpagamento" CausesValidation="False" ID="btnConfirmarPagamento" runat="server" Text="Confirmar Pagamento" ReplaceTitleTo="Aguarde..." onclick="btnConfirmarPagamento_OnClick" />--%>
                </div>
            </dx:PopupControlContentControl>
        </ContentCollection>
        <ContentStyle>
            <Paddings PaddingBottom="5px" />
        </ContentStyle>
    </dx:ASPxPopupControl>

 <dx:ASPxPopupControl ID="popReativarPagamento" runat="server" CloseAction="CloseButton" Modal="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
        ClientInstanceName="popReativarPagamento" HeaderText="Reativar Pagamento" AllowDragging="True" PopupAnimationType="None" EnableViewState="True" Height="500" AutoUpdatePosition="True">
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl20" runat="server">
                <asp:HiddenField runat="server" ID="hdfReativarPagamento" ClientIDMode="static" />
                <div style="width: 800px; height: 500px;" class="rotulos">
                    <b>Reativar Pagamento Não Autorizado:</b><br />

                    <b>Observação:</b><br />
                    <asp:TextBox ValidationGroup="reativarPagamento" CausesValidation="False" runat="server" CssClass="campos" ID="txtObservacaoReativarPagamento" Width="100%" Height="80px" TextMode="MultiLine"></asp:TextBox><br />
                    <br />
                    <asp:Button runat="server" ID="btnReativarPagamento" OnClick="btnReativarPagamento_OnClick" Text="Reativar Pagamento" />
                    <%--<uc:OneClickButton ValidationGroup="reativarPagamento" CausesValidation="False" ID="btnReativarPagamento" runat="server" Text="Reativar Pagamento" ReplaceTitleTo="Aguarde..." onclick="btnReativarPagamento_OnClick" />--%>
                </div>
            </dx:PopupControlContentControl>
        </ContentCollection>
        <ContentStyle>
            <Paddings PaddingBottom="5px" />
        </ContentStyle>
    </dx:ASPxPopupControl>

    <dx:ASPxPopupControl ID="popCancelarPagamento" runat="server" CloseAction="CloseButton" Modal="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
        ClientInstanceName="popCancelarPagamento" HeaderText="Cancelar Pagamento" AllowDragging="True" PopupAnimationType="None" EnableViewState="True" Height="500" AutoUpdatePosition="True">
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl8" runat="server">
                <asp:HiddenField runat="server" ID="hdfCancelarPagamento" ClientIDMode="static" />
                <div style="width: 800px; height: 500px;">
                    <b>Motivo do Cancelamento:</b><br />
                    <asp:TextBox runat="server" ID="txtMotivoCancelarPagamento" Width="100%" Height="80px" TextMode="MultiLine"></asp:TextBox><br />
                    <br />
                    <asp:Button runat="server" ID="btnCancelarPagamento" OnClick="btnCancelarPagamento_OnClick" Text="Cancelar Pagamento" />
                    <%--<uc:OneClickButton ID="btnCancelarPagamento" runat="server" Text="Cancelar Pagamento" ReplaceTitleTo="Aguarde..." onclick="btnCancelarPagamento_OnClick" />--%>
                </div>
            </dx:PopupControlContentControl>
        </ContentCollection>
        <ContentStyle>
            <Paddings PaddingBottom="5px" />
        </ContentStyle>
    </dx:ASPxPopupControl>
    <dx:ASPxPopupControl ID="popPagamentoNaoAutorizado" runat="server" CloseAction="CloseButton" Modal="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
        ClientInstanceName="popPagamentoNaoAutorizado" HeaderText="Pagamento Não Autorizado" AllowDragging="True" PopupAnimationType="None" EnableViewState="True" Height="500" AutoUpdatePosition="True">
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl10" runat="server">
                <asp:HiddenField runat="server" ID="hdfPagamentoNaoAutorizado" ClientIDMode="static" />
                <div style="width: 800px; height: 500px;">
                    <b>Motivo de Pagamento Não Autorizado:</b><br />
                    <asp:TextBox runat="server" ID="txtMotivoPagamentoNaoAutorizado" Width="100%" Height="80px" TextMode="MultiLine"></asp:TextBox><br />
                    <br />
                    <asp:Button runat="server" ID="btnPagamentoNaoAutorizado" OnClick="btnPagamentoNaoAutorizado_OnClick" Text="Pagamento Não Autorizado" />
                    <%--<uc:OneClickButton ID="btnPagamentoNaoAutorizado" runat="server" Text="Pagamento Não Autorizado" ReplaceTitleTo="Aguarde..." onclick="btnPagamentoNaoAutorizado_OnClick" />--%>
                </div>
            </dx:PopupControlContentControl>
        </ContentCollection>
        <ContentStyle>
            <Paddings PaddingBottom="5px" />
        </ContentStyle>
    </dx:ASPxPopupControl>
    <asp:UpdatePanel ID="updPanel" runat="server">
        <ContentTemplate>
            <dx:ASPxPopupControl ID="popAdicionarChamado" runat="server" CloseAction="CloseButton" Modal="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
                ClientInstanceName="popAdicionarChamado" HeaderText="Adicionar Chamado" AllowDragging="True" PopupAnimationType="None" EnableViewState="True" Height="500" AutoUpdatePosition="True">
                <ContentCollection>
                    <dx:PopupControlContentControl ID="PopupControlContentControl6" runat="server">
                        <asp:HiddenField runat="server" ID="hdfIdAgendamento" />
                        <asp:ScriptManager ID="scriptManager1" runat="server"></asp:ScriptManager>
                        <div style="width: 800px; height: 500px;">
                            <table width="100%">
                                <tr class="rotulos">
                                    <td>
                                        <asp:CheckBox runat="server" ID="chkChamadoConcluido" Text="Concluído" />
                                    </td>
                                </tr>
                                <tr runat="server" id="trDepartamentoOrigem">
                                    <td style="width: 100%">
                                        <b>Departamento Origem:</b><br />
                                        <asp:DropDownList ID="ddlDepartamentoOrigem" runat="server"></asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table style="width: 100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 33%">
                                                    <b>Departamento Destino:</b><br />
                                                    <asp:DropDownList ID="ddlDepartamento" runat="server" OnSelectedIndexChanged="ddlDepartamento_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList>
                                                </td>
                                                <td style="width: 33%" id="trAssunto" runat="server" visible="false">
                                                    <b>Assunto:</b><br />
                                                    <asp:DropDownList ID="ddlAssunto" runat="server" Enabled="false" OnSelectedIndexChanged="ddlAssunto_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList>
                                                </td>
                                                <td style="width: 33%" id="trTarefa" runat="server" visible="false">
                                                    <b>Tarefa:</b><br />
                                                    <asp:DropDownList ID="ddlTarefa" runat="server" Enabled="false" OnSelectedIndexChanged="ddlTarefa_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList>
                                                </td>
                                                <td style="width: *%">&nbsp;</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="rotulos">
                                        <table width="100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td>
                                                    <b>Usuário Responsável:</b><br />
                                                    <asp:DropDownList ID="ddlUsuarioDepartamento" runat="server"></asp:DropDownList>
                                                </td>
                                                <td>
                                                    <b>Prioridade:</b><br />
                                                    <asp:DropDownList ID="ddlPrioridade" runat="server">
                                                        <asp:ListItem Text="Muito Baixo" Value="1"></asp:ListItem>
                                                        <asp:ListItem Text="Baixo" Value="2"></asp:ListItem>
                                                        <asp:ListItem Text="Normal" Selected="True" Value="3"></asp:ListItem>
                                                        <asp:ListItem Text="Alta" Value="4"></asp:ListItem>
                                                        <asp:ListItem Text="Urgente" Value="5"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                        </table>

                                    </td>
                                </tr>
                                <tr class="rotulos">
                                    <td>Título
                                    </td>
                                </tr>
                                <tr class="rotulos">
                                    <td>
                                        <asp:TextBox runat="server" ID="txtChamadoTitulo" Width="100%" ReadOnly="True"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr class="rotulos">
                                    <td class="rotulos">
                                        <table>
                                            <tr>
                                                <td>Data</td>
                                                <td>Hora</td>
                                                <%--<td>Responsável</td>--%>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="txtChamadoData" runat="server" Width="100px"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="txtChamadoHora" runat="server" Width="50px"></asp:Label>
                                                </td>
                                                <%--<td class="rotulos">--%>
                                                <asp:DropDownList runat="server" Visible="false" DataSourceID="sqlUsuario" DataValueField="usuarioId" DataTextField="usuarioNome" ID="ddlChamadoUsuario" />
                                                <%--</td>--%>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="rotulos">Descrição
                                    </td>
                                </tr>
                                <tr>
                                    <td class="rotulos">
                                        <ce:editor id="txtChamado" runat="server"
                                            autoconfigure="Minimal" contextmenumode="Simple"
                                            editorwysiwygmodecss="" filespath="" height="200px"
                                            urltype="Default" userelativelinks="False"
                                            width="782px"
                                            removeservernamesfromurl="False">
                                    <TextAreaStyle CssClass="campos" />
<%--                                    <FrameStyle BackColor="White" BorderColor="#DDDDDD" BorderStyle="Solid" 
                                        BorderWidth="1px" CssClass="CuteEditorFrame" Height="100%" Width="100%" />--%>
                                </ce:editor>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="rotulos" colspan="2" style="text-align: right">
                                        <asp:Button runat="server" ID="btnGravarChamado" OnClientClick="javascript: return validaChamado(); " OnClick="btnGravarChamado_OnClick" Text="Gravar" />
                                    </td>
                                </tr>
                            </table>
                        </div>

                    </dx:PopupControlContentControl>
                </ContentCollection>
                <ContentStyle>
                    <Paddings PaddingBottom="5px" />
                </ContentStyle>
            </dx:ASPxPopupControl>
        </ContentTemplate>
    </asp:UpdatePanel>


    <dx:ASPxPopupControl ID="popCartaCorrecao" runat="server" CloseAction="CloseButton" Modal="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
        ClientInstanceName="popCartaCorrecao" HeaderText="Carta de Correção" AllowDragging="True" PopupAnimationType="None" EnableViewState="True" Height="500" AutoUpdatePosition="True">
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl9" runat="server">
                <asp:HiddenField runat="server" ID="hdfNotaFiscal" />
                <div style="width: 800px; height: 500px;">
                    <asp:TextBox runat="server" ID="txtCartaCorrecao" Width="100%" Height="80px" TextMode="MultiLine"></asp:TextBox><br />
                    <br />
                    <asp:Button runat="server" ID="btnGravarCartaCorrecao" OnClick="btnGravarCartaCorrecao_OnClick" Text="Enviar Carta de Correção" />
                </div>
            </dx:PopupControlContentControl>
        </ContentCollection>
        <ContentStyle>
            <Paddings PaddingBottom="5px" />
        </ContentStyle>
    </dx:ASPxPopupControl>
    <dx:ASPxPopupControl ID="popAtribuirEtiqueta" runat="server" CloseAction="CloseButton" Modal="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
        ClientInstanceName="popAtribuirEtiqueta" HeaderText="AtribuirEtiqueta" AllowDragging="True" PopupAnimationType="None" EnableViewState="True" Height="100" AutoUpdatePosition="True">
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl18" runat="server">
                <asp:HiddenField runat="server" ID="hdfIdProdutoEstoqueAtribuir" />
                <div style="width: 400px; height: 150px;">
                    <asp:TextBox runat="server" ID="txtEtiquetaAtribuir" Width="100%"></asp:TextBox><br />
                    <br />
                    <asp:Button runat="server" ID="btnAtribuirEtiqueta" OnClick="btnAtribuirEtiqueta_Click1" Text="Atribuir" />
                </div>
            </dx:PopupControlContentControl>
        </ContentCollection>
        <ContentStyle>
            <Paddings PaddingBottom="5px" />
        </ContentStyle>
    </dx:ASPxPopupControl>
    <dx:ASPxPopupControl ID="popAlterarRastreio" runat="server" CloseAction="CloseButton" Modal="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
        ClientInstanceName="popAlterarRastreio" HeaderText="Envio Parcial" AllowDragging="True" PopupAnimationType="None" EnableViewState="True" Height="500" AutoUpdatePosition="True">
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl5" runat="server">
                <div style="width: 500px; height: 300px;">
                    <table width="100%">
                        <tr class="rotulos">
                            <td class="rotulos">Forma de Entrega
                            </td>
                            <td class="rotulos">
                                <asp:DropDownList runat="server" ID="ddlEnviarPor">
                                    <Items>
                                        <asp:ListItem Value="Sedex" Text="Sedex"></asp:ListItem>
                                        <asp:ListItem Value="PAC" Text="PAC"></asp:ListItem>
                                        <asp:ListItem Value="jadlog" Text="Jadlog"></asp:ListItem>
                                        <asp:ListItem Value="jadlogexpressa" Text="Jad Expressa"></asp:ListItem>
                                        <asp:ListItem Value="tnt" Text="TNT"></asp:ListItem>
                                        <asp:ListItem Value="Primor" Text="Primor"></asp:ListItem>
                                        <asp:ListItem Value="LBR" Text="LBR"></asp:ListItem>
                                        <asp:ListItem Value="Jamef" Text="Jamef"></asp:ListItem>
                                        <asp:ListItem Value="Dialogo" Text="Dialogo"></asp:ListItem>
                                        <asp:ListItem Value="NowLog" Text="NowLog"></asp:ListItem>
                                        <asp:ListItem Value="TotalExpress" Text="TotalExpress"></asp:ListItem>
                                    </Items>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="rotulos">Rastreio
                            </td>
                            <td class="rotulos">
                                <asp:TextBox runat="server" ID="txtRastreio"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="rotulos" colspan="2">
                                <asp:CheckBox runat="server" ID="chkConcluido" Text="Rastreamento concluído" />
                            </td>
                        </tr>
                        <tr>
                            <td class="rotulos" colspan="2">
                                <asp:HiddenField runat="server" ID="hdfPacoteAlterando" />
                                <asp:Button runat="server" ID="btnAlterarPacotePedido" OnClick="btnAlterarPacotePedido_OnClick" Text="Gravar" />
                            </td>
                        </tr>
                    </table>
                </div>
            </dx:PopupControlContentControl>
        </ContentCollection>
        <ContentStyle>
            <Paddings PaddingBottom="5px" />
        </ContentStyle>
    </dx:ASPxPopupControl>
    <dx:ASPxPopupControl ID="pcNota" runat="server" CloseAction="CloseButton" Modal="True"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="TopSides" ClientInstanceName="pcNota"
        HeaderText="Gerar Nota Fiscal" AllowDragging="True" PopupAnimationType="None" EnableViewState="True">
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server">
                <div style="width: 1000px; height: 500px; overflow: scroll;">
                    <div style="overflow: hidden;">
                        <br />
                        <b style="font-size: 15px;">
                            <asp:CheckBox ID="ckbGerarNovaNota" runat="server" Text="Gerar nova nota" Checked="false" Visible="true" Enabled="false" /></b>
                        <br />
                        <br />
                        <div style="overflow: hidden; margin-bottom: 10px;">
                            <div style="float: left; width: 50px;">
                                Marcar
                            </div>
                            <div style="float: left; width: 80px;">
                                Foto
                            </div>
                            <div style="float: left; width: 690px; margin-right: 10px;">
                                Nome
                            </div>
                            <div style="float: left; width: 100px;">
                                NCM
                            </div>
                        </div>
                        <asp:ListView runat="server" ID="lstProdutoNota" OnItemDataBound="lstProdutoNota_DataBound">
                            <ItemTemplate>
                                <div style="overflow: hidden; margin-bottom: 10px;">
                                    <div style="float: left; width: 50px;">
                                        <asp:CheckBox ID="ckbMarcar" runat="server"></asp:CheckBox>
                                    </div>
                                    <div style="float: left; width: 80px;">
                                        <asp:HiddenField runat="server" ID="hiddenIdItemPedidoEstoque" Value='<%#Eval("IdItemPedidoEstoque") %>' />
                                        <asp:HiddenField runat="server" ID="hiddenProdutoId" Value='<%#Eval("produtoId") %>' />
                                        <asp:Image ID="fotoDestaque" runat="server" Height="70px" Width="70px" />
                                    </div>
                                    <div style="float: left; width: 690px; margin-right: 10px;">
                                        <asp:Label ID="lblProdutoNome" runat="server" Text='<%# Bind("produtoNome") %>'></asp:Label>
                                    </div>
                                    <div style="float: left; width: 100px;">
                                        <asp:TextBox ID="txtncm" runat="server" CssClass="campos" Text='<%# Bind("ncm") %>' Width="90px"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rqvProdutoNome" runat="server"
                                            ControlToValidate="txtncm" Display="None"
                                            ErrorMessage="Preencha o NCM." SetFocusOnError="True" ValidationGroup="notaFiscal"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:ListView>
                        <asp:SqlDataSource ConnectionString="<%$ ConnectionStrings:connectionString %>" runat="server" ID="sqlProdutosNota"
                            SelectCommand="SELECT tbItemPedidoEstoque.idItemPedidoEstoque,tbItemPedidoEstoque.itemPedidoId, tbItensPedido.pedidoId, tbProdutos.produtoNome, tbItemPedidoEstoque.produtoId, 
                                           tbItensPedido.itemQuantidade, tbItensPedido.itemPresente, tbItemPedidoEstoque.precoDeCusto, isnull(tbItensPedido.brinde,0) AS brinde,
                                           tbItensPedido.garantiaId, tbItensPedido.ItemMensagemDoCartao, tbItensPedido.itemValor, tbProdutos.produtoId AS Expr1, 
                                           tbProdutos.produtoIdDaEmpresa, tbProdutos.produtoCodDeBarras, tbProdutos.produtoPrecoDeCusto, tbItensPedido.entreguePeloFornecedor, 
                                           tbItensPedido.prazoDeFabricacao, tbProdutos.ncm, tbProdutos.cfop, tbProdutos.complementoIdDaEmpresa
                                    FROM tbItemPedidoEstoque
                                    INNER JOIN
                                    tbItensPedido ON tbItemPedidoEstoque.itemPedidoId = tbItensPedido.itemPedidoId
                                    INNER JOIN
                                    tbProdutos ON tbProdutos.produtoId = tbItemPedidoEstoque.produtoId
                                    WHERE (tbItensPedido.pedidoId = @pedidoId) ORDER BY tbItemPedidoEstoque.itemPedidoId">
                            <SelectParameters>
                                <asp:QueryStringParameter Name="pedidoId" QueryStringField="pedidoId" Type="Int32" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                    </div>
                    <div style="margin-top: 10px; overflow: hidden;">
                        <div style="float: left;">
                            Código do IBGE:<br />
                            <asp:TextBox ID="txtCodigoIbge" runat="server" CssClass="campos" Width="90px"></asp:TextBox>
                        </div>
                        <div style="float: left; margin-left: 50px;">
                            NFe Referenciada:<br />
                            <asp:TextBox ID="txtNfeReferenciada" runat="server" CssClass="campos" Width="90px"></asp:TextBox>
                        </div>
                        <div style="float: right;">
                            <asp:Button runat="server" ID="btnGerarNota" OnClick="btnGerarNota_Click" Text="Gerar Nota Fiscal" />
                        </div>
                    </div>
                </div>
            </dx:PopupControlContentControl>
        </ContentCollection>
        <ContentStyle>
            <Paddings PaddingBottom="5px" />
        </ContentStyle>
    </dx:ASPxPopupControl>
    <dx:ASPxPopupControl ID="pcNotaDevolucao" runat="server" CloseAction="CloseButton" Modal="True"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="TopSides" ClientInstanceName="pcNotaDevolucao"
        HeaderText="Gerar Nota Fiscal Devolução" AllowDragging="True" PopupAnimationType="None" EnableViewState="True">
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl11" runat="server">
                <div style="width: 965px; height: 500px; overflow: scroll;">
                    <div style="overflow: hidden;">
                        <br />
                        <b style="font-size: 15px;">
                            <asp:CheckBox ID="ckbGerarNovaNotaDevolucao" runat="server" Text="Gerar nova nota devolução" /></b>
                        <br />
                        <br />
                        <div style="overflow: hidden; margin-bottom: 10px;">
                            <div style="float: left; width: 50px;">
                                Marcar
                            </div>
                            <div style="float: left; width: 80px;">
                                Foto
                            </div>
                            <div style="float: left; width: 690px; margin-right: 10px;">
                                Nome
                            </div>
                            <div style="float: left; width: 100px;">
                                NCM
                            </div>
                        </div>
                        <asp:ListView runat="server" ID="lstProdutoNotaDevolucao" DataSourceID="sqlProdutosNotaDevolucao" OnItemDataBound="lstProdutoNota_DataBound">
                            <ItemTemplate>
                                <div style="overflow: hidden; margin-bottom: 10px;">
                                    <div style="float: left; width: 50px;">
                                        <asp:CheckBox ID="ckbMarcar" runat="server" Checked="true"></asp:CheckBox>
                                    </div>
                                    <div style="float: left; width: 80px;">
                                        <asp:HiddenField runat="server" ID="hiddenProdutoId" Value='<%#Eval("produtoId") %>' />
                                        <asp:HiddenField runat="server" ID="hiddenIdItemPedidoEstoque" Value='<%#Eval("idItemPedidoEstoque") %>' />
                                        <asp:HiddenField runat="server" ID="hiddenBrinde" Value='<%#Eval("brinde") %>' />
                                        <asp:Image ID="fotoDestaque" runat="server" Height="70px" Width="70px" />
                                    </div>
                                    <div style="float: left; width: 690px; margin-right: 10px;">
                                        <%# Convert.ToBoolean(Eval("brinde")) ? "<b>(BRINDE) </b>" : "" %>
                                        <asp:Label ID="lblProdutoNome" runat="server" Text='<%# Bind("produtoNome") %>'></asp:Label>
                                    </div>
                                    <div style="float: left; width: 100px;">
                                        <asp:TextBox ID="txtncm" runat="server" CssClass="campos" Text='<%# Bind("ncm") %>' Width="90px"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rqvProdutoNome" runat="server"
                                            ControlToValidate="txtncm" Display="None"
                                            ErrorMessage="Preencha o NCM." SetFocusOnError="True" ValidationGroup="notaFiscal"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:ListView>
                    </div>
                    <div style="margin-top: 10px; overflow: hidden;">
                        <div style="float: left; width: 998px">
                            Informações Adicionais:<br />
                            <asp:TextBox ID="txtInformacoesAdicionais" runat="server" CssClass="campos" Width="92%" Height="30px" TextMode="MultiLine"></asp:TextBox>
                        </div>


                        <div style="float: right; margin-right: 24px; clear: left; margin-top: 10px;">
                            Cód. IBGE:
                                <asp:TextBox ID="txtCodigoIbgeDevolucao" runat="server" CssClass="campos" Width="90px"></asp:TextBox>
                            Dt de Saída:
                            <asp:TextBox ID="txtdatasaida" runat="server" CssClass="campos" Width="90px"></asp:TextBox>&nbsp;                  
                            nota fiscal referência:<asp:DropDownList runat="server" ID="ddlNotasReferenciaDevolucao" DataTextField="numeroNota" DataValueField="nfeKey" />
                            empresa:<asp:DropDownList runat="server" Width="180px" ID="ddlEmpresasDevolucao" DataTextField="nome" DataValueField="idEmpresa" />
                            Transp.:
                                <asp:DropDownList runat="server" ID="ddlTransportadora">
                                    <Items>
                                        <asp:ListItem Value="" Text="Não alterar"></asp:ListItem>
                                        <asp:ListItem Value="Sedex" Text="Sedex"></asp:ListItem>
                                        <asp:ListItem Value="PAC" Text="PAC"></asp:ListItem>
                                        <asp:ListItem Value="jadlog" Text="Jadlog"></asp:ListItem>
                                        <asp:ListItem Value="jadlogexpressa" Text="Jad Expressa"></asp:ListItem>
                                        <asp:ListItem Value="tnt" Text="TNT"></asp:ListItem>
                                        <asp:ListItem Value="plimor" Text="Plimor"></asp:ListItem>
                                    </Items>
                                </asp:DropDownList>

                        </div>
                        <div style="float: right; margin-right: 24px; clear: left; margin-top: 10px;">
                            <asp:Button runat="server" ID="btnGerarNotaDevolucao" OnClick="btnGerarNotaDevolucao_Click" Text="Gerar Nota Fiscal Devolução" />
                        </div>
                    </div>
                </div>
                <asp:SqlDataSource ConnectionString="<%$ ConnectionStrings:connectionString %>" runat="server" ID="sqlProdutosNotaDevolucao"
                    SelectCommand="SELECT tbItemPedidoEstoque.idItemPedidoEstoque,tbItemPedidoEstoque.itemPedidoId, tbItensPedido.pedidoId, tbProdutos.produtoNome, tbItemPedidoEstoque.produtoId, 
                               tbItensPedido.itemQuantidade, tbItensPedido.itemPresente, tbItemPedidoEstoque.precoDeCusto, isnull(tbItensPedido.brinde,0) AS brinde,
                               tbItensPedido.garantiaId, tbItensPedido.ItemMensagemDoCartao, tbItensPedido.itemValor, tbProdutos.produtoId AS Expr1, 
                               tbProdutos.produtoIdDaEmpresa, tbProdutos.produtoCodDeBarras, tbProdutos.produtoPrecoDeCusto, tbItensPedido.entreguePeloFornecedor, 
                               tbItensPedido.prazoDeFabricacao, tbProdutos.ncm, tbProdutos.cfop, tbProdutos.complementoIdDaEmpresa
                        FROM tbItemPedidoEstoque
                        INNER JOIN
                        tbItensPedido ON tbItemPedidoEstoque.itemPedidoId = tbItensPedido.itemPedidoId
                        INNER JOIN
                        tbProdutos ON tbProdutos.produtoId = tbItemPedidoEstoque.produtoId
                        WHERE (tbItensPedido.pedidoId = @pedidoId) ORDER BY tbItemPedidoEstoque.itemPedidoId">
                    <SelectParameters>
                        <asp:QueryStringParameter Name="pedidoId" QueryStringField="pedidoId" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </dx:PopupControlContentControl>
        </ContentCollection>
        <ContentStyle>
            <Paddings PaddingBottom="5px" />
        </ContentStyle>
    </dx:ASPxPopupControl>

    <!-------------------------------------------------------->
    <dx:ASPxPopupControl ID="pcNotaDevolucao2" runat="server" CloseAction="CloseButton" Modal="True"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" ClientInstanceName="pcNotaDevolucao2"
        HeaderText="Gerar Nota Fiscal Devolução2" AllowDragging="True" PopupAnimationType="None" EnableViewState="True" AutoUpdatePosition="True">
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl19" runat="server">
                <asp:HiddenField runat="server" ID="hdfIdNotaFiscal" />
                <div style="width: 965px; height: 500px; overflow: scroll;">
                    <div style="overflow: hidden;">
                        <br />
                        <b style="font-size: 15px;">
                            <asp:CheckBox ID="ckbGerarNovaNotaDevolucao2" runat="server" Text="Gerar nova nota devolução" /></b>
                        <br />
                        <br />
                        <div style="overflow: hidden; margin-bottom: 10px;">
                            <div style="float: left; width: 50px;">
                                Marcar
                            </div>
                            <div style="float: left; width: 80px;">
                                Foto
                            </div>
                            <div style="float: left; width: 690px; margin-right: 10px;">
                                Nome
                            </div>
                            <div style="float: left; width: 100px;">
                                NCM
                            </div>
                        </div>
                        <asp:ListView runat="server" ID="lstProdutoNotaDevolucao2" OnItemDataBound="lstProdutoNota2_DataBound">
                            <ItemTemplate>
                                <div style="overflow: hidden; margin-bottom: 10px;">
                                    <div style="float: left; width: 50px;">
                                        <asp:CheckBox ID="ckbMarcar" runat="server" Checked="true"></asp:CheckBox>
                                    </div>
                                    <div style="float: left; width: 80px;">
                                        <asp:HiddenField runat="server" ID="hiddenProdutoId" Value='<%#Eval("produtoId") %>' />
                                        <asp:HiddenField runat="server" ID="hiddenIdItemPedidoEstoque" Value='<%#Eval("idItemPedidoEstoque") %>' />
                                        <asp:HiddenField runat="server" ID="hiddenBrinde" Value='<%#Eval("brinde") %>' />
                                        <asp:Image ID="fotoDestaque" runat="server" Height="70px" Width="70px" />
                                    </div>
                                    <div style="float: left; width: 690px; margin-right: 10px;">
                                        <%# Convert.ToBoolean(Eval("brinde")) ? "<b>(BRINDE) </b>" : "" %>
                                        <asp:Label ID="lblProdutoNome" runat="server" Text='<%# Bind("produtoNome") %>'></asp:Label>
                                    </div>
                                    <div style="float: left; width: 100px;">
                                        <asp:TextBox ID="txtncm" runat="server" CssClass="campos" Text='<%# Bind("ncm") %>' Width="90px"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rqvProdutoNome" runat="server"
                                            ControlToValidate="txtncm" Display="None"
                                            ErrorMessage="Preencha o NCM." SetFocusOnError="True" ValidationGroup="notaFiscal"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:ListView>
                    </div>
                    <div style="margin-top: 10px; overflow: hidden;">
                        <div style="float: left; width: 998px">
                            Informações Adicionais:<br />
                            <asp:TextBox ID="txtInformacoesAdicionais2" runat="server" CssClass="campos" Width="92%" Height="30px" TextMode="MultiLine"></asp:TextBox>
                        </div>

                        <div style="float: left; clear: left; margin-right: 24px; margin-top: 10px;">
                            <strong>Nota fiscal referência:</strong>&nbsp;<asp:Label ID="lblNotaFiscalReferenciaDevolucao" runat="server" Text=""></asp:Label>
                            <strong>Empresa:</strong>&nbsp;<asp:Label ID="lblEmpresaNotaFiscalDevolucao" runat="server" Text=""></asp:Label>

                            <asp:HiddenField ID="hdIdEmpresaNotaFiscalDevolucao" runat="server" />
                            <asp:HiddenField ID="hdNfeKey" runat="server" />


                        </div>
                        <div style="float: left; clear: left; margin-right: 24px; margin-top: 10px;">
                            Cód. IBGE:
                                <asp:TextBox ID="txtCodigoIbgeDevolucao2" runat="server" CssClass="campos" Width="90px"></asp:TextBox>
                            Dt de Saída:
                            <asp:TextBox ID="txtdatasaida2" runat="server" CssClass="campos" Width="90px"></asp:TextBox>&nbsp;                  
                            Transp.:
                                <asp:DropDownList runat="server" ID="ddlTransportadora2">
                                    <Items>
                                        <asp:ListItem Value="" Text="Não alterar"></asp:ListItem>
                                        <asp:ListItem Value="Sedex" Text="Sedex"></asp:ListItem>
                                        <asp:ListItem Value="PAC" Text="PAC"></asp:ListItem>
                                        <asp:ListItem Value="jadlog" Text="Jadlog"></asp:ListItem>
                                        <asp:ListItem Value="jadlogexpressa" Text="Jad Expressa"></asp:ListItem>
                                        <asp:ListItem Value="tnt" Text="TNT"></asp:ListItem>
                                        <asp:ListItem Value="plimor" Text="Plimor"></asp:ListItem>
                                    </Items>
                                </asp:DropDownList>

                        </div>
                        <div style="float: right; margin-right: 24px; clear: left; margin-top: 10px;">
                            <asp:Button runat="server" ID="btnGerarNotaDevolucao2" OnClick="btnGerarNotaDevolucao2_Click" Text="Gerar Nota Fiscal Devolução" />
                        </div>
                    </div>
                </div>
                <asp:SqlDataSource ConnectionString="<%$ ConnectionStrings:connectionString %>" runat="server" ID="sqlProdutosNotaDevolucao2"
                    SelectCommand="SELECT tbItemPedidoEstoque.idItemPedidoEstoque,tbItemPedidoEstoque.itemPedidoId, tbItensPedido.pedidoId, tbProdutos.produtoNome, tbItemPedidoEstoque.produtoId, 
                               tbItensPedido.itemQuantidade, tbItensPedido.itemPresente, tbItemPedidoEstoque.precoDeCusto, isnull(tbItensPedido.brinde,0) AS brinde,
                               tbItensPedido.garantiaId, tbItensPedido.ItemMensagemDoCartao, tbItensPedido.itemValor, tbProdutos.produtoId AS Expr1, 
                               tbProdutos.produtoIdDaEmpresa, tbProdutos.produtoCodDeBarras, tbProdutos.produtoPrecoDeCusto, tbItensPedido.entreguePeloFornecedor, 
                               tbItensPedido.prazoDeFabricacao, tbProdutos.ncm, tbProdutos.cfop, tbProdutos.complementoIdDaEmpresa
                        FROM tbItemPedidoEstoque
                        INNER JOIN
                        tbItensPedido ON tbItemPedidoEstoque.itemPedidoId = tbItensPedido.itemPedidoId
                        inner join tbNotaFiscal on tbItensPedido.idPedidoEnvio = tbNotaFiscal.idPedidoEnvio
                        INNER JOIN
                        tbProdutos ON tbProdutos.produtoId = tbItemPedidoEstoque.produtoId
                        WHERE (tbNotaFiscal.idNotaFiscal = @idNotaFiscal) ORDER BY tbItemPedidoEstoque.itemPedidoId">
                    <SelectParameters>
                        <%-- <asp:QueryStringParameter Name="pedidoId" QueryStringField="pedidoId" Type="Int32" />--%>
                        <asp:ControlParameter Name="idNotaFiscal" ControlID="hdfIdNotaFiscal" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </dx:PopupControlContentControl>
        </ContentCollection>
        <ContentStyle>
            <Paddings PaddingBottom="5px" />
        </ContentStyle>
    </dx:ASPxPopupControl>
    <!-------------------------------------------------------->
    <dx:ASPxPopupControl ID="popDespesas" runat="server" CloseAction="CloseButton" Modal="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="TopSides" ClientInstanceName="popDespesas"
        HeaderText="Despesas do Pedido" AllowDragging="True" PopupAnimationType="None" EnableViewState="True" Height="500">
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl2" runat="server">
                <div style="width: 1000px; height: 500px;">
                    <div style="overflow: hidden;">
                        <dxwgv:ASPxGridView ClientInstanceName="gridDespesas" ID="gridDespesas" runat="server" AutoGenerateColumns="False" DataSourceID="sqlDespesa" KeyFieldName="idDespesa" Width="1000px"
                            Cursor="auto" SettingsEditing-Mode="EditForm" EnableCallBacks="False" Settings-ShowFilterRow="True" Settings-ShowFilterBar="Visible" OnHtmlRowCreated="gridDespesas_HtmlRowCreated" OnRowUpdating="gridDespesas_RowUpdating">
                            <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                            <SettingsPager Position="TopAndBottom" PageSize="10"
                                ShowDisabledButtons="False" AlwaysShowPager="True">
                                <Summary AllPagesText="Páginas: {0} - {1} ({2} items)"
                                    Text="Página {0} de {1} ({2} registros encontrados)" />
                            </SettingsPager>
                            <TotalSummary>
                                <dxwgv:ASPxSummaryItem DisplayFormat="c" FieldName="valor"
                                    ShowInColumn="Valor" ShowInGroupFooterColumn="Valor" SummaryType="Sum" />
                            </TotalSummary>
                            <Settings ShowFilterRow="True" ShowGroupButtons="False" ShowFooter="True" />
                            <SettingsEditing EditFormColumnCount="4"
                                PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1"
                                PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter"
                                PopupEditFormWidth="700px" />
                            <Columns>
                                <dxwgv:GridViewDataTextColumn Caption="ID" Name="id"
                                    FieldName="idDespesa" ReadOnly="True" VisibleIndex="0" Width="30px">
                                    <Settings AutoFilterCondition="Contains" />
                                    <EditFormSettings Visible="False" CaptionLocation="Top" ColumnSpan="2" />
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataTextColumn Name="descricao" Caption="Descrição" FieldName="despesa" VisibleIndex="1" Width="200px">
                                    <PropertiesTextEdit>
                                        <ValidationSettings SetFocusOnError="True">
                                            <RequiredField ErrorText="Preencha a descrição" IsRequired="True" />
                                        </ValidationSettings>
                                    </PropertiesTextEdit>
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataComboBoxColumn Name="categoria" Caption="Categoria" FieldName="idDespesaCategoria" VisibleIndex="2">
                                    <PropertiesComboBox DataSourceID="sqlDespesaCategoria"
                                        TextField="despesaCategoria" ValueField="idDespesaCategoria"
                                        ValueType="System.String">
                                        <ValidationSettings SetFocusOnError="True">
                                            <RequiredField ErrorText="Selecione a categoria." IsRequired="True" />
                                        </ValidationSettings>
                                    </PropertiesComboBox>
                                </dxwgv:GridViewDataComboBoxColumn>
                                <dxwgv:GridViewDataComboBoxColumn Name="usuario" Caption="Usuario" FieldName="idUsuario" VisibleIndex="2">
                                    <PropertiesComboBox DataSourceID="sqlUsuario"
                                        TextField="usuarioNome" ValueField="usuarioId"
                                        ValueType="System.String">
                                    </PropertiesComboBox>
                                    <EditFormSettings Visible="False" CaptionLocation="Top" ColumnSpan="2" />
                                </dxwgv:GridViewDataComboBoxColumn>
                                <dxwgv:GridViewDataDateColumn Name="data" Caption="Data" FieldName="data" VisibleIndex="3" Width="90px">
                                </dxwgv:GridViewDataDateColumn>
                                <dxwgv:GridViewDataDateColumn Name="vencimento" Caption="Vencimento" FieldName="vencimento" VisibleIndex="3" Width="90px">
                                </dxwgv:GridViewDataDateColumn>
                                <dxwgv:GridViewDataTextColumn Caption="Valor" FieldName="valor"
                                    VisibleIndex="6" Width="60px">
                                    <PropertiesTextEdit DisplayFormatString="C">
                                        <ValidationSettings SetFocusOnError="True">
                                            <RegularExpression ErrorText="Preencha apenas com números, virgula ou ponto."
                                                ValidationExpression="^[\-]{0,1}[0-9]{1,}(([\.\,]{0,1}[0-9]{1,})|([0-9]{0,}))$" />
                                            <RequiredField ErrorText="Preencha o desconto." IsRequired="True" />
                                        </ValidationSettings>
                                    </PropertiesTextEdit>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtValor" runat="server" CssClass="campos"
                                            Text='<%# Eval("valor", "{0:C}").Replace("R$","") %>' Width="100%"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rqvpreco" runat="server"
                                            ControlToValidate="txtValor" Display="Dynamic"
                                            ErrorMessage="Preencha o valor." Font-Size="10px" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                    </EditItemTemplate>
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataCheckColumn Caption="Pago" FieldName="pago" Name="excluir" VisibleIndex="4" Width="42px">
                                    <DataItemTemplate>
                                        <dx:ASPxCheckBox ID="ckbPago" runat="server" Checked='<%# Convert.ToBoolean(Eval("pago")) %>'>
                                        </dx:ASPxCheckBox>
                                    </DataItemTemplate>
                                </dxwgv:GridViewDataCheckColumn>
                                <dxwgv:GridViewCommandColumn VisibleIndex="8" Width="90px" ButtonType="Image">
                                    <EditButton Visible="True" Text="Editar">
                                        <Image Url="~/admin/images/btEditar.jpg" />
                                    </EditButton>
                                    <DeleteButton Visible="True" Text="Excluir">
                                        <Image Url="~/admin/images/btExcluir.jpg" />
                                    </DeleteButton>
                                    <CancelButton Text="Cancelar">
                                        <Image Url="~/admin/images/btCancelarIcon.jpg" />
                                    </CancelButton>
                                    <UpdateButton Text="Salvar">
                                        <Image Url="~/admin/images/btSalvarIcon.jpg" />
                                    </UpdateButton>
                                    <ClearFilterButton Visible="True" Text="Limpar filtro">
                                        <Image Url="~/admin/images/btLimparFiltro.jpg" />
                                    </ClearFilterButton>
                                    <HeaderTemplate>
                                        <img alt="" src="images/legendaIcones.jpg" />
                                    </HeaderTemplate>
                                </dxwgv:GridViewCommandColumn>
                            </Columns>
                            <StylesEditors>
                                <Label Font-Bold="True">
                                </Label>
                            </StylesEditors>
                        </dxwgv:ASPxGridView>
                        <asp:LinqDataSource ID="sqlDespesa" runat="server"
                            ContextTypeName="dbCommerceDataContext" EnableDelete="True"
                            EnableInsert="True" EnableUpdate="True" TableName="tbDespesas" Where="idPedido == @idPedido">
                            <WhereParameters>
                                <asp:QueryStringParameter DefaultValue="0" Name="idPedido" QueryStringField="pedidoId" Type="Int32" />
                            </WhereParameters>
                        </asp:LinqDataSource>
                        <asp:LinqDataSource ID="sqlDespesaCategoria" runat="server"
                            ContextTypeName="dbCommerceDataContext" EnableDelete="True"
                            EnableInsert="True" EnableUpdate="True" TableName="tbDespesaCategorias">
                        </asp:LinqDataSource>
                    </div>
                </div>
            </dx:PopupControlContentControl>
        </ContentCollection>
        <ContentStyle>
            <Paddings PaddingBottom="5px" />
        </ContentStyle>
    </dx:ASPxPopupControl>
    <dx:ASPxPopupControl ID="popAutorizarParcial" runat="server" CloseAction="CloseButton" Modal="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
        ClientInstanceName="popAutorizarParcial" HeaderText="Envio Parcial" AllowDragging="True" PopupAnimationType="None" EnableViewState="True" Height="500" AutoUpdatePosition="True">
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl4" runat="server">
                <div style="width: 1000px; height: 300px;">
                    <asp:Panel runat="server" ID="Panel1">
                        <div style="overflow: hidden;" id="divAutorizarParcial">
                            <table width="100%">
                                <tr id="trParcialCadastrarFaltando">
                                    <td>
                                        <asp:CheckBox runat="server" ID="chkParcialCadastrarFaltando" Text="Cadastrar como produto faltando" Checked="True" ClientIDMode="Static" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:HiddenField runat="server" ClientIDMode="Static" ID="hdfParcialItemPedido" Value="" />
                                        <asp:HiddenField runat="server" ClientIDMode="Static" ID="hdfParcialProduto" Value="" />
                                        <asp:HiddenField runat="server" ClientIDMode="Static" ID="hdfParcialEnviado" Value="" />
                                        Motivo:<br />
                                        <asp:TextBox runat="server" ID="txtMotivoEnvioParcial" Width="100%" Height="100px" TextMode="MultiLine"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: right;">
                                        <asp:Button runat="server" Text="Confirmar" OnClick="btnConfirmarEnvioParcial_OnClick" ID="btnConfirmarEnvioParcial" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </asp:Panel>


                </div>
            </dx:PopupControlContentControl>
        </ContentCollection>
        <ContentStyle>
            <Paddings PaddingBottom="5px" />
        </ContentStyle>
    </dx:ASPxPopupControl>
    <dx:ASPxPopupControl ID="popEmailPrazos" runat="server" CloseAction="CloseButton" Modal="True"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="TopSides" ClientInstanceName="popEmailPrazos"
        HeaderText="Email com Prazos" AllowDragging="True" PopupAnimationType="None" EnableViewState="True">
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl7" runat="server">
                <div style="width: 1000px; height: 500px; overflow: scroll; font-family: Arial; font-size: 12px;">
                    <br />
                    <br />
                    Seu pedido
                    <asp:Literal ID="litEmailPrazoNumeroPedido" runat="server"></asp:Literal>
                    tem um prazo de postagem de
                    <asp:Literal ID="litEmailPrazoMinimo" runat="server"></asp:Literal>
                    a
                    <asp:Literal ID="litEmailPrazoMaximo" runat="server"></asp:Literal>
                    dias úteis após a aprovação de pagamento mais prazo de entrega de
                    <asp:Literal ID="litEmailPrazoCorreios" runat="server"></asp:Literal>
                    dias úteis
                    para seu CEP "<asp:Literal ID="litEmailPrazoCep" runat="server"></asp:Literal>", a informação do prazo de postagem é exibida em todos nossos produtos abaixo do botão comprar
                    campo "Disponibilidade em Estoque", segue produto adquirido para que possa verificar a informação.<br />
                    <br />

                    <asp:Literal ID="litEmailPrazoLinkProduto" runat="server"></asp:Literal><br />
                    <br />

                    Seguindo os prazos informados pagamento aprovado dia
                    <asp:Literal ID="litEmailPrazoDataAprovacao" runat="server"></asp:Literal>
                    a postagem máxima é até dia
                    <asp:Literal ID="litEmailPrazoPostagem" runat="server"></asp:Literal>
                    mais o prazo de entrega
                    <asp:Literal ID="litEmailPrazoCorreios2" runat="server"></asp:Literal>
                    dias úteis,
                    prazo máximo para receber seu pedido é até dia
                    <asp:Literal ID="litEmailPrazoRecebimento" runat="server"></asp:Literal>.<br />
                    <br />

                    Lembramos que esse é o prazo máximo para recebimento de seu pedido.<br />
                    <br />

                    Temos certeza que vai gostar bastante das peças que foram escolhidas para o quarto de seu  bebê.<br />
                    <br />

                    Continuamos a disposição para qualquer esclarecimento.<br />
                    <br />
                </div>
            </dx:PopupControlContentControl>
        </ContentCollection>
        <ContentStyle>
            <Paddings PaddingBottom="5px" />
        </ContentStyle>
    </dx:ASPxPopupControl>
    <dx:ASPxPopupControl ID="popCancelarItemPedido" runat="server" CloseAction="CloseButton" Modal="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
        ClientInstanceName="popCancelarItemPedido" HeaderText="Alterar Itens do Pedido" AllowDragging="True" PopupAnimationType="None" EnableViewState="True" Height="500" AutoUpdatePosition="True">
        <ContentCollection>
            <dx:PopupControlContentControl ID="contentPopCancelar" runat="server">
                <div style="width: 1000px; height: 300px;">
                    <asp:Panel runat="server" ID="pnCancelarItem">
                        <div style="overflow: hidden;" id="divCancelarItem">
                            <table width="100%">
                                <tr class="campos">
                                    <td>
                                        <asp:CheckBox runat="server" ID="chkTrocarItemCancelado" Text="Trocar item Cancelado por outro" Checked="True" />
                                    </td>
                                </tr>
                                <tr class="campos">
                                    <td>Motivo:<br />
                                        <asp:HiddenField runat="server" ClientIDMode="Static" ID="hdfItemCancelar" Value="" />
                                        <asp:DropDownList runat="server" ID="ddlMotivoCancelarItem" CssClass="campos">
                                            <Items>
                                                <asp:ListItem Value="Desistiu do Produto" Text="Desistiu do Produto"></asp:ListItem>
                                                <asp:ListItem Value="Já ganhou o produto" Text="Já ganhou o produto"></asp:ListItem>
                                                <asp:ListItem Value="Escolheu a coleção errada" Text="Escolheu a coleção errada"></asp:ListItem>
                                                <asp:ListItem Value="Prazo de entrega do produto" Text="Prazo de entrega do produto"></asp:ListItem>
                                                <asp:ListItem Value="Exclusao de Produto por Personalizacao" Text="Exclusão de Produto por Personalização"></asp:ListItem>
                                            </Items>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Observação:<br />
                                        <asp:TextBox runat="server" ID="txtObservacaoCancelarItemPedido" Width="100%" Height="100px" TextMode="MultiLine"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: right;">
                                        <asp:Button runat="server" Text="Cancelar Produto" OnClick="btnCancelarItemPedido_Click" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </asp:Panel>


                </div>
            </dx:PopupControlContentControl>
        </ContentCollection>
        <ContentStyle>
            <Paddings PaddingBottom="5px" />
        </ContentStyle>
    </dx:ASPxPopupControl>
    <dx:ASPxPopupControl ID="popAdicionarItemPedido" runat="server" CloseAction="CloseButton" Modal="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
        ClientInstanceName="popAdicionarItemPedido" HeaderText="Alterar Itens do Pedido" AllowDragging="True" PopupAnimationType="None" EnableViewState="True" Height="500" AutoUpdatePosition="True">
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl3" runat="server">
                <div style="width: 1000px; height: 300px;">
                    <asp:Panel runat="server" ID="pnAdicionarItem">
                        <div style="overflow: hidden;">
                            <table width="100%">
                                <tr class="campos">
                                    <td>
                                        <!--Alterada nomenclatura para Presente pois o usuário considera mais adeguado nada foi mudado em código sendo assim as referencias continuam sendo - Brinde-->
                                        <asp:CheckBox runat="server" ID="chkBrinde" Text="Presente" Visible="false" />
                                    </td>
                                </tr>
                                <tr class="campos">
                                    <td>Motivo:<br />
                                        <asp:DropDownList runat="server" ID="ddlMotivoAdicionarItem" DataValueField="idItemPedidoTipoAdicao" DataTextField="tipoDeAdicao" CssClass="campos">
                                        </asp:DropDownList>

                                    </td>
                                </tr>
                                <tr>
                                    <td>Observação:<br />
                                        <asp:TextBox runat="server" ID="txtObservacaoAdicionarItem" Width="100%" Height="100px" TextMode="MultiLine"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>ID do Produto:<br />
                                        <asp:TextBox runat="server" ID="txtAdicionaItemIdDaEmpresa"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr id="trComplementoIdDaEmpresaAdicionar" runat="server" visible="False">
                                    <td>Complemento ID da Empresa:<br />
                                        <asp:DropDownList runat="server" ID="ddlComplementoIdDaEmpresa" Visible="False" DataValueField="produtoId" DataTextField="complementoIdDaEmpresa" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: right;">
                                        <asp:Button runat="server" Text="Adicionar Produto" OnClick="btnAdicionarItemPedido_Click" />
                                        <%--<uc:OneClickButton ValidationGroup="adicionarproduto" runat="server" Text="Adicionar Produto" ReplaceTitleTo="Aguarde..." onclick="btnAdicionarItemPedido_Click" Font-Size="15" />--%>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </asp:Panel>
                </div>
            </dx:PopupControlContentControl>
        </ContentCollection>
        <ContentStyle>
            <Paddings PaddingBottom="5px" />
        </ContentStyle>
    </dx:ASPxPopupControl>

    <dx:ASPxPopupControl ID="popCancelarPedido" runat="server" CloseAction="CloseButton" Modal="True"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="TopSides" ClientInstanceName="popCancelarPedido"
        HeaderText="Motivo de Cancelamento" AllowDragging="True" PopupAnimationType="None" EnableViewState="True">
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl14" runat="server">
                <div style="width: 365px; height: 80px; overflow: hidden; font-family: Arial; font-size: 12px;">
                    <br />
                    Favor selecionar um dos motivos:
                    <br />
                    <asp:DropDownList runat="server" ID="ddlMotivosCancelamentoPedido" DataValueField="idMotivoCancelamento" DataTextField="motivo" />
                    <asp:Button runat="server" Text="Confirmar" OnClick="btnCancelarPedido_OnClick" Style="float: right;" />
                </div>
            </dx:PopupControlContentControl>
        </ContentCollection>
        <ContentStyle>
            <Paddings PaddingBottom="5px" />
        </ContentStyle>
    </dx:ASPxPopupControl>

    <dx:ASPxPopupControl ID="popDescancelarProduto" runat="server" CloseAction="CloseButton" Modal="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
        ClientInstanceName="popDescancelarProduto" HeaderText="Reativar produto cancelado" AllowDragging="True" PopupAnimationType="None" EnableViewState="True" Height="500" AutoUpdatePosition="True">
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl15" runat="server">
                <div style="width: 600px; height: 435px; overflow-x: auto;">

                    <div style="overflow: hidden; margin-bottom: 10px;">
                        <div style="float: left; width: 70px; text-align: center;">
                            <b>Reativar</b>
                        </div>
                        <div style="float: left; width: 80px;">
                            <b>Foto</b>
                        </div>
                        <div style="float: left; width: 290px; margin-right: 10px;">
                            <b>Nome</b>
                        </div>
                        <div style="float: left; width: 130px; text-align: center;">
                            <b>Preço</b>
                        </div>
                    </div>
                    <asp:ListView runat="server" ID="lstReativarProdutos" OnItemDataBound="lstProdutoNota_DataBound">
                        <ItemTemplate>
                            <div style="overflow: hidden; margin-bottom: 10px;">
                                <div style="float: left; width: 70px; text-align: center;">
                                    <asp:CheckBox ID="ckbReativar" runat="server"></asp:CheckBox>
                                </div>
                                <div style="float: left; width: 80px;">
                                    <asp:HiddenField runat="server" ID="hiddenProdutoId" Value='<%#Eval("produtoId") %>' />
                                    <asp:HiddenField runat="server" ID="hiddenItemPedidoId" Value='<%#Eval("itemPedidoId") %>' />
                                    <asp:Image ID="fotoDestaque" runat="server" Height="70px" Width="70px" />
                                </div>
                                <div style="float: left; width: 290px; margin-right: 10px;">
                                    <%# Eval("produtoNome") %><div style="font-size: 11px;"><%#Eval("itemPedidoId")%></div>
                                </div>
                                <div style="float: left; width: 130px; text-align: center; font-size: 16px;">
                                    <%# Convert.ToDecimal(Eval("itemValor")).ToString("C")  %>
                                </div>
                            </div>
                        </ItemTemplate>
                        <ItemSeparatorTemplate>
                            <hr style="border-top: 1px dotted #8c8b8b; border-bottom: 1px dotted #fff;" />
                        </ItemSeparatorTemplate>
                    </asp:ListView>
                </div>
                <asp:Button runat="server" Text="Confirmar" ID="btnDescancelarProduto" OnClientClick="return confirm('Os produtos que estiverem marcados serão reativados no pedido, confirma?')" OnClick="btnDescancelarProduto_OnClick" Style="float: right;" />
            </dx:PopupControlContentControl>
        </ContentCollection>
        <ContentStyle>
            <Paddings PaddingBottom="5px" />
        </ContentStyle>
    </dx:ASPxPopupControl>

    <dx:ASPxPopupControl ID="popMarcarProdutoComoEnviado" runat="server" CloseAction="CloseButton" Modal="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
        ClientInstanceName="popMarcarProdutoComoEnviado" HeaderText="Marcar produto como enviado" AllowDragging="True" PopupAnimationType="None" EnableViewState="True" Height="500" AutoUpdatePosition="True">
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl16" runat="server">
                <div style="width: 600px; height: 435px; overflow-x: auto;">

                    <div style="overflow: hidden; margin-bottom: 10px;">
                        <div style="float: left; width: 70px; text-align: center;">
                            <b>Enviado</b>
                        </div>
                        <div style="float: left; width: 80px;">
                            <b>Foto</b>
                        </div>
                        <div style="float: left; width: 290px; margin-right: 10px;">
                            <b>Nome</b>
                        </div>
                        <div style="float: left; width: 130px; text-align: center;">
                            <b>Preço</b>
                        </div>
                    </div>
                    <asp:ListView runat="server" ID="lstMarcarProdutoComoEnviado" OnItemDataBound="lstProdutoNota_DataBound">
                        <ItemTemplate>
                            <div style="overflow: hidden; margin-bottom: 10px;">
                                <div style="float: left; width: 70px; text-align: center;">
                                    <asp:CheckBox ID="ckbEnviado" runat="server"></asp:CheckBox>
                                </div>
                                <div style="float: left; width: 80px;">
                                    <asp:HiddenField runat="server" ID="hiddenProdutoId" Value='<%#Eval("produtoId") %>' />
                                    <asp:HiddenField runat="server" ID="hiddenItemPedidoId" Value='<%#Eval("itemPedidoId") %>' />
                                    <asp:HiddenField runat="server" ID="hiddenIdItemPedidoEstoque" Value='<%#Eval("idItemPedidoEstoque") %>' />
                                    <asp:Image ID="fotoDestaque" runat="server" Height="70px" Width="70px" />
                                </div>
                                <div style="float: left; width: 290px; margin-right: 10px;">
                                    <%# Eval("produtoNome") %><div style="font-size: 11px;"><%#Eval("idItemPedidoEstoque")%></div>
                                    ItemPedidoId: <%#Eval("itemPedidoId") %><br />
                                    ProdutoId: <%#Eval("produtoId") %>
                                </div>
                                <div style="float: left; width: 130px; text-align: center; font-size: 16px;">
                                    <%# Convert.ToDecimal(Eval("itemValor")).ToString("C")  %>
                                </div>
                            </div>
                        </ItemTemplate>
                        <ItemSeparatorTemplate>
                            <hr style="border-top: 1px dotted #8c8b8b; border-bottom: 1px dotted #fff;" />
                        </ItemSeparatorTemplate>
                    </asp:ListView>
                </div>
                <asp:Button runat="server" Text="Confirmar" ID="btnMarcarProdutoComoEnviado" OnClientClick="return confirm('Os produtos que estiverem marcados serão dados como enviados, confirma?')" OnClick="btnMarcarProdutoComoEnviado_OnClick" Style="float: right;" />
            </dx:PopupControlContentControl>
        </ContentCollection>
        <ContentStyle>
            <Paddings PaddingBottom="5px" />
        </ContentStyle>
    </dx:ASPxPopupControl>

    <dx:ASPxPopupControl ID="popProdutoPersonalizacao" runat="server" CloseAction="CloseButton" Modal="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
        ClientInstanceName="popProdutoPersonalizacao" HeaderText="Selecionar produto da personalização" AllowDragging="True" PopupAnimationType="None" EnableViewState="True" Height="500" AutoUpdatePosition="True">
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl17" runat="server">
                <div style="width: 600px; height: 435px; overflow-x: auto;">
                    <asp:ListView runat="server" ID="lstProdutosPersonalizacao" OnItemDataBound="lstProdutosPersonalizacao_OnItemDataBound">
                        <ItemTemplate>
                            Personalização: <b><%# Eval("produtoNome") %></b>
                            <hr />
                            <asp:HiddenField runat="server" ID="hfIdProdutoPaiPersonalizacao" Value='<%# Eval("idProdutoPai") %>' />
                            <asp:HiddenField runat="server" ID="hfItemPedidoIdProdutoPaiPersonalizacao" Value='<%# Eval("itemPedidoId") %>' />
                            <asp:Panel runat="server" ID="pnlListaProdutosPersonalizados">
                                <asp:ListView runat="server" ID="lstProdutoPersonalizadoItens">
                                    <ItemTemplate>
                                        <div style="overflow: hidden; margin: 0 0 10px 35px;">
                                            <div style="float: left; width: 490px;">
                                                <asp:Image ID="fotoDestaque" runat="server" Height="70px" Width="70px" Visible="False" />
                                                <asp:RadioButton runat="server" Text='<%# Eval("produtoNome") %>' ID="rbSelecionarPersonalizacao" onclick="RadioCheck(this);" value='<%#Eval("idProdutoPai") + "#" + Eval("idProdutoFilhoPersonalizacao") + "#" + Eval("itemPedidoId") %>' />
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                </asp:ListView>
                            </asp:Panel>

                        </ItemTemplate>
                        <ItemSeparatorTemplate>
                            <hr />
                        </ItemSeparatorTemplate>
                    </asp:ListView>
                    <asp:Button runat="server" ID="btnSalvarItemPersonalizacao" Text="Salvar Personalizações" OnClick="btnSalvarItemPersonalizacao_OnClick" Style="float: right; margin-top: 20px;" />
                    <asp:HiddenField runat="server" ID="hfItemPedidoIdProdutoPersonalizado" />
                </div>
                <asp:Button runat="server" Text="Confirmar" ID="Button1" OnClientClick="return confirm('Os produtos que estiverem marcados serão dados como enviados, confirma?')" OnClick="btnMarcarProdutoComoEnviado_OnClick" Style="float: right;" Visible="False" />
            </dx:PopupControlContentControl>
        </ContentCollection>
        <ContentStyle>
            <Paddings PaddingBottom="5px" />
        </ContentStyle>
    </dx:ASPxPopupControl>

</asp:Content>

