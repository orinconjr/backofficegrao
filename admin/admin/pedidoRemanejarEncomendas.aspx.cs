﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_pedidoRemanejarEncomendas : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        var pedidoDc = new dbCommerceDataContext();
        var ids = (from c in pedidoDc.tbPedidoFornecedorItems where c.entregue == false select c.idPedido).Distinct();
        var pedidos = (from c in pedidoDc.tbPedidos where ids.Contains(c.pedidoId) select c);
        foreach (var pedido in pedidos)
        {
            var itensPedidoDc = new dbCommerceDataContext();
            var itensPedido = (from c in itensPedidoDc.tbItensPedidos where c.pedidoId == pedido.pedidoId select c);
            foreach (var itemPedido in itensPedido)
            {
                bool combo = false;
                var produtosDc = new dbCommerceDataContext();
                var produtosCombo = (from c in produtosDc.tbProdutoRelacionados where c.idProdutoPai == itemPedido.produtoId select c);
                foreach (var produtoRelacionado in produtosCombo)
                {
                    combo = true;
                }

                if (!combo)
                {
                    
                }
            }
        }
    }
}