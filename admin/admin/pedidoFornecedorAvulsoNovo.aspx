﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="pedidoFornecedorAvulsoNovo.aspx.cs" Inherits="admin_pedidoFornecedorAvulsoNovo" %>

<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxTimer" TagPrefix="dxt" %>
<%@ Register Assembly="CuteEditor" Namespace="CuteEditor" TagPrefix="CE" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" runat="Server">
    <table cellpadding="0" cellspacing="0" style="width: 920px">
        <tr>
            <td class="tituloPaginas" valign="top">
                <asp:Label ID="lblAcao" runat="server">Pedidos ao Fornecedor</asp:Label>
            </td>
        </tr>

        <tr>
            <td>

                <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
                    <tr>
                        <td>
                            <asp:TextBox runat="server" ID="hdfLista" Visible="False" />
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:CheckBox ID="ckbGerarCusto" Text="Não gerar custo" runat="server" />
                            <br /><br />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="atualizarprecosbotoes">
                                <label>Planilha: </label>
                                <asp:FileUpload runat="server" CssClass="fileUpload" ID="fupPlanilha" />
                                <asp:Button runat="server" ID="btnImportar" CssClass="btnexportar" Text="Importar" ValidationGroup="tipoPlanilha" OnClick="btnImportar_Click" />

                             </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table>
                                <tr class="campos">
                                    <td>Pedido:<br />
                                        <asp:TextBox runat="server" ID="txtPedido"></asp:TextBox>&nbsp;
                                    </td>
                                    <td>ID Item:<br />
                                        <asp:TextBox runat="server" ID="txtPedidoItem"></asp:TextBox>&nbsp;
                                    </td>
                                    <td>ID do Produto:<br />
                                        <asp:TextBox runat="server" ID="txtIdDaEmpresa"></asp:TextBox>&nbsp;
                            <asp:DropDownList runat="server" ID="ddlComplemento" Visible="False" DataValueField="produtoId" DataTextField="complementoIdDaEmpresa" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="adiciona" runat="server" ControlToValidate="txtIdDaEmpresa" ErrorMessage="Preencha o ID da Empresa" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                    </td>
                                    <td>Quantidade:<br />
                                        <asp:TextBox runat="server" ID="txtQuantidade"></asp:TextBox>&nbsp;
                                    </td>
                                    <td>Motivo:<br />
                                        <asp:TextBox runat="server" ID="txtMotivo"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="adiciona" runat="server" ControlToValidate="txtMotivo" ErrorMessage="Preencha o Motivo" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                    </td>
                                    <td>&nbsp;<br />
                                        <asp:Button runat="server" ID="btnAdicionar" Text="Adicionar" OnClick="btnAdicionar_OnClick" ValidationGroup="adiciona" />
                                        <asp:ValidationSummary ID="vlds" runat="server" ShowMessageBox="True" ShowSummary="False" ValidationGroup="validaEntrega" DisplayMode="List" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table style="width: 100%;">
                                <tr class="rotulos" style="text-align: center; font-weight: bold;">
                                    <td>Pedido
                                    </td>
                                    <td>ID do Produto
                                    </td>
                                    <td>Complemento ID
                                    </td>
                                    <td>Nome
                                    </td>
                                    <td>Quantidade
                                    </td>
                                    <td>Motivo
                                    </td>
                                </tr>
                                <asp:ListView runat="server" ID="lstProdutos" OnItemDataBound="lstProdutos_OnItemDataBound">
                                    <ItemTemplate>
                                        <tr class="rotulos" style="text-align: center;">
                                            <td style="padding-bottom: 3px; padding-top: 3px; border-bottom: 1px solid #b0e0e6;">
                                                <asp:Literal runat="server" ID="litPedidoId" Text='<%# Eval("pedidoId") %>'></asp:Literal>
                                            </td>
                                            <td style="padding-bottom: 3px; padding-top: 3px; border-bottom: 1px solid #b0e0e6;">
                                                <asp:HiddenField runat="server" ID="hiddenProdutoId" Value='<%# Eval("produtoId") %>' />
                                                <asp:Literal runat="server" ID="litProdutoIdDaEmpresa" Text='<%# Eval("produtoIdDaEmpresa") %>'></asp:Literal>
                                            </td>
                                            <td style="padding-bottom: 3px; padding-top: 3px; border-bottom: 1px solid #b0e0e6;">
                                                <asp:Literal runat="server" ID="litComplementoIdDaEmpresa" Text='<%# Eval("complementoIdDaEmpresa") %>'></asp:Literal>&nbsp;
                                            </td>
                                            <td style="padding-bottom: 3px; padding-top: 3px; border-bottom: 1px solid #b0e0e6;">
                                                <%# Eval("produtoNome") %>
                                            </td>
                                            <td style="padding-bottom: 3px; padding-top: 3px; border-bottom: 1px solid #b0e0e6;">
                                                <%# Eval("quantidade") %>
                                            </td>
                                            <td style="padding-bottom: 3px; padding-top: 3px; border-bottom: 1px solid #b0e0e6;">
                                                <%# Eval("motivo") %>
                                            </td>
                                            <td style="padding-bottom: 3px; padding-top: 3px; border-bottom: 1px solid #b0e0e6;">
                                                <asp:LinkButton runat="server" ID="btnRemover" OnCommand="btnRemover_OnCommand" CommandArgument="<%# Container.DataItemIndex %>">Remover</asp:LinkButton>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:ListView>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right;">&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right;">
                            <asp:Button runat="server" ID="btnGravar" Text="Gravar" OnClick="btnGravar_OnClick" />
                        </td>
                    </tr>

                </table>
            </td>
        </tr>
    </table>
</asp:Content>
