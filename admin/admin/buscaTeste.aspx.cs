﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Xml;
using System.Data;
using System.Data.SqlClient;
public partial class admin_buscaTeste : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //var notas = getValoresNota(559965);

        //GridView1.DataSource = notas;
        //GridView1.DataBind();
        teste();
    }

    public class Nota2
    {
        public decimal? vlNota { get; set; }
        public string natOperacao { get; set; }
        public string caminho { get; set; }
        public string erro { get; set; }
    }
    public List<Nota2> getValoresNota(int pedidoId)
    {
        List<Nota2> notas = new List<Nota2>();

        var data = new dbCommerceDataContext();
        var notasPedido = (from nf in data.tbNotaFiscals
                           where nf.idPedido == pedidoId
                           select new { nf.numeroNota, nf.dataHora }).OrderBy(x => x.dataHora).ToList();
        foreach (var notaPedido in notasPedido)
        {
            Nota2 nota = new Nota2();
            string caminho = System.Configuration.ConfigurationManager.AppSettings["caminhoFisico"] + "\\admin\\notas\\" + pedidoId + "_" + notaPedido.numeroNota + ".xml";
            nota.caminho = caminho;
            try
            {
                XmlDocument doc = new XmlDocument();

                XmlElement rootNode = doc.CreateElement("NFe");

                doc.AppendChild(rootNode);
                string conteudoArquivoXML = File.ReadAllText(caminho).Replace("\r\n", "").Trim().Replace(">        <", "><");//.Replace("\"", "").Trim();
                // Regex.Replace(conteudoArquivoXML, @"(?:(?<=^\<mattext\>)[^\<]*)|(?:[^\>]*(?=\</mattext\>$))", string.Empty, RegexOptions.Multiline);

                XmlNodeList NFeRaiz = doc.GetElementsByTagName("NFe");
                NFeRaiz[0].InnerXml = conteudoArquivoXML;

                XmlNodeList elemList = doc.GetElementsByTagName("natOp");
                nota.natOperacao = elemList[0].InnerXml;

                XmlNodeList vNF = doc.GetElementsByTagName("vNF");
                nota.vlNota = Convert.ToDecimal(vNF[0].InnerXml.Replace(".", ","));
            }
            catch (Exception ex)
            {
                System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace(ex, true);
                //Console.WriteLine("Line: " + trace.GetFrame(0).GetFileLineNumber());
                nota.erro = ex.Message + "Linha: " + trace.GetFrame(0).GetFileLineNumber();
                string erro = ex.Message;
            }
            notas.Add(nota);
        }

        return notas;
    }

    public void teste()
    {
        var data = new dbCommerceDataContext();
        var lista = (from c in data.tbEmpresas select c).ToList();
        //int qtd = (from tlcrr in data.tbLoteCobrancaRetornoRegistros
        //           join pp in data.tbPedidoPagamentos on tlcrr.idPedidoPagamento equals pp.idPedidoPagamento
        //           where tlcrr.status == 0
        //           select new
        //           {
        //               tlcrr.idLoteCobrancaRetornoRegistro,
        //           }
        //      ).Count();
        GridView1.DataSource = lista;
        GridView1.DataBind();
    }
}