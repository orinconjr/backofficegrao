﻿<%@ Page Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="vendasPorPeriodo.aspx.cs" Inherits="admin_vendasPorPeriodo" Theme="Glass" MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" runat="Server">
    <table cellpadding="0" cellspacing="0" style="width: 920px">
        <tr>
            <td class="tituloPaginas" valign="top">
                <asp:Label ID="lblAcao" runat="server">Vendas por período</asp:Label>
            </td>
        </tr>
        <tr>
            <td>

                <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
                    <tr>
                        <td align="right">
                            <table cellpadding="0" cellspacing="0" style="width: 834px">
                                <tr>
                                    <td align="right">&nbsp;</td>
                                    <td height="38"
                                        style="background-image: url('images/bkExportar.jpg'); padding-bottom: 1px;"
                                        valign="bottom" width="231">
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 97px">&nbsp;</td>
                                                <td style="width: 33px">
                                                    <asp:ImageButton ID="btPdf" runat="server" ImageUrl="~/admin/images/btPdf.jpg"
                                                        Style="margin-left: 5px" OnClick="btPdf_Click" />
                                                </td>
                                                <td style="width: 31px">
                                                    <asp:ImageButton ID="btXsl" runat="server" ImageUrl="~/admin/images/btXls.jpg"
                                                        Style="margin-left: 5px" OnClick="btXsl_Click" />
                                                </td>
                                                <td style="width: 36px">
                                                    <asp:ImageButton ID="btRtf" runat="server" ImageUrl="~/admin/images/btRtf.jpg"
                                                        Style="margin-left: 5px" OnClick="btRtf_Click" />
                                                </td>
                                                <td valign="bottom">
                                                    <asp:ImageButton ID="btCsv" runat="server" ImageUrl="~/admin/images/btCsv.jpg"
                                                        Style="margin-left: 5px" OnClick="btCsv_Click" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" style="width: 834px">
                                <tr>
                                    <td class="rotulos" style="width: 100px">Data inicial<br __designer:mapid="3fc" />
                                        <asp:TextBox ID="txtDataInicial" runat="server" CssClass="campos"
                                            Text='<%# Bind("faixaDeCepPesoInicial") %>'
                                            Width="90px" MaxLength="10"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rqvDataInicial" runat="server"
                                            ControlToValidate="txtDataInicial" Display="None"
                                            ErrorMessage="Preencha a data inicial."
                                            SetFocusOnError="True"></asp:RequiredFieldValidator>
                                        <b __designer:mapid="27df">
                                            <asp:RegularExpressionValidator ID="rgeDataInicial" runat="server"
                                                ControlToValidate="txtDataInicial" Display="None"
                                                ErrorMessage="Por favor, preencha corretamente a data inicial."
                                                SetFocusOnError="True"
                                                ValidationExpression="^(([0-2]\d|[3][0-1])\/([0]\d|[1][0-2])\/[1-2][0-9]\d{2})$"></asp:RegularExpressionValidator>
                                        </b>
                                    </td>
                                    <td class="rotulos" style="width: 100px">Data final<br __designer:mapid="401" />
                                        <asp:TextBox ID="txtDataFinal" runat="server" CssClass="campos"
                                            Text='<%# Bind("faixaDeCepPesoFinal") %>' ValidationGroup="grpInsert"
                                            Width="90px" MaxLength="10"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rqvDataFinal" runat="server"
                                            ControlToValidate="txtDataFinal" Display="None"
                                            ErrorMessage="Preencha a data final."
                                            SetFocusOnError="True"></asp:RequiredFieldValidator>
                                        <b __designer:mapid="27df">
                                            <asp:RegularExpressionValidator ID="rgeDataInicial0" runat="server"
                                                ControlToValidate="txtDataFinal" Display="None"
                                                ErrorMessage="Por favor, preencha corretamente a data final"
                                                SetFocusOnError="True"
                                                ValidationExpression="^(([0-2]\d|[3][0-1])\/([0]\d|[1][0-2])\/[1-2][0-9]\d{2})$"></asp:RegularExpressionValidator>
                                        </b>
                                    </td>
                                    <td valign="bottom">
                                        <asp:ImageButton ID="imbInsert" runat="server"
                                            ImageUrl="~/admin/images/btPesquisar.jpg" />
                                        <asp:ValidationSummary ID="llds" runat="server" DisplayMode="List"
                                            ShowMessageBox="True" ShowSummary="False" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <dxwgv:ASPxGridView ID="grd" runat="server" AutoGenerateColumns="False"
                                KeyFieldName="tipoDeEntregaId" Width="834px" OnProcessColumnAutoFilter="grd_OnProcessColumnAutoFilter" OnCustomUnboundColumnData="grd_OnCustomUnboundColumnData"
                                Cursor="auto" DataSourceID="sql">
                                <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />

                                <Styles>
                                    <Footer Font-Bold="True">
                                    </Footer>
                                </Styles>
                                <SettingsText ConfirmDelete="Tem certeza que deseja excluir?"
                                    EmptyDataRow="Nenhum registro encontrado." />
                                <SettingsPager Position="TopAndBottom" PageSize="100"
                                    ShowDisabledButtons="False" AlwaysShowPager="True">
                                    <Summary AllPagesText="Páginas: {0} - {1} ({2} items)"
                                        Text="Página {0} de {1} ({2} registros encontrados)" />
                                </SettingsPager>
                                <Settings ShowFilterRow="True" ShowGroupButtons="False" ShowFooter="True" />
                                <TotalSummary>
                                    <dxwgv:ASPxSummaryItem DisplayFormat="0" ShowInColumn="Id do ped."
                                        ShowInGroupFooterColumn="Id do ped." SummaryType="Count" />
                                    <dxwgv:ASPxSummaryItem DisplayFormat="C" FieldName="valorDoFrete"
                                        ShowInColumn="Frete" ShowInGroupFooterColumn="Frete"
                                        SummaryType="Sum" />
                                    <dxwgv:ASPxSummaryItem DisplayFormat="C" FieldName="valorDoDescontoDoCupom"
                                        ShowInColumn="Desc cupom" ShowInGroupFooterColumn="Desc cupom"
                                        SummaryType="Sum" />
                                    <dxwgv:ASPxSummaryItem DisplayFormat="C" FieldName="valorDoDescontoDoPagamento"
                                        ShowInColumn="Desc pagt" ShowInGroupFooterColumn="Desc pagt"
                                        SummaryType="Sum" />
                                    <dxwgv:ASPxSummaryItem DisplayFormat="C" FieldName="valorDosJuros"
                                        ShowInColumn="Juros" ShowInGroupFooterColumn="Juros" SummaryType="Sum" />
                                    <dxwgv:ASPxSummaryItem DisplayFormat="C" FieldName="valorDoEmbrulhoECartao"
                                        ShowInColumn="Presente" ShowInGroupFooterColumn="Presente" SummaryType="Sum" />
                                    <dxwgv:ASPxSummaryItem DisplayFormat="C" FieldName="valorDosItens"
                                        ShowInColumn="Produtos" ShowInGroupFooterColumn="Produtos" SummaryType="Sum" />
                                    <dxwgv:ASPxSummaryItem DisplayFormat="C" FieldName="valorCobrado"
                                        ShowInColumn="Valor cobrado" ShowInGroupFooterColumn="Valor cobrado"
                                        SummaryType="Sum" />
                                    <dxwgv:ASPxSummaryItem DisplayFormat="C" FieldName="valorTotalGeral"
                                        ShowInColumn="Valor total" ShowInGroupFooterColumn="Valor total"
                                        SummaryType="Sum" />
                                </TotalSummary>
                                <SettingsEditing EditFormColumnCount="4"
                                    PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1"
                                    PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter"
                                    PopupEditFormWidth="700px" Mode="Inline" />
                                <Columns>
                                    <dxwgv:GridViewDataTextColumn Caption="Id Cliente" FieldName="pedidoIdCliente" VisibleIndex="0" UnboundType="Integer">
                                        <EditFormSettings Visible="False" />
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Id do ped." FieldName="pedidoId"
                                        VisibleIndex="0">
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Data" FieldName="dataHoraDoPedido"
                                        VisibleIndex="1">
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Nome" FieldName="clienteNome"
                                        VisibleIndex="1">
                                    </dxwgv:GridViewDataTextColumn>
                                     <dxwgv:GridViewDataTextColumn Caption="Cidade" FieldName="endCidade"
                                        VisibleIndex="1">
                                    </dxwgv:GridViewDataTextColumn>
                                     <dxwgv:GridViewDataTextColumn Caption="UF" FieldName="endEstado"
                                        VisibleIndex="1">
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Frete"
                                        FieldName="valorDoFrete" VisibleIndex="2">
                                        <PropertiesTextEdit DisplayFormatString="C">
                                        </PropertiesTextEdit>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Desc cupom"
                                        FieldName="valorDoDescontoDoCupom" VisibleIndex="2">
                                        <PropertiesTextEdit DisplayFormatString="C">
                                        </PropertiesTextEdit>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Desc pagt"
                                        FieldName="valorDoDescontoDoPagamento" VisibleIndex="3">
                                        <PropertiesTextEdit DisplayFormatString="C">
                                        </PropertiesTextEdit>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Juros" FieldName="valorDosJuros"
                                        VisibleIndex="4">
                                        <PropertiesTextEdit DisplayFormatString="C">
                                        </PropertiesTextEdit>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Presente"
                                        FieldName="valorDoEmbrulhoECartao" VisibleIndex="5">
                                        <PropertiesTextEdit DisplayFormatString="C">
                                        </PropertiesTextEdit>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Produtos" FieldName="valorDosItens"
                                        VisibleIndex="6">
                                        <PropertiesTextEdit DisplayFormatString="C">
                                        </PropertiesTextEdit>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Valor cobrado" FieldName="valorCobrado"
                                        VisibleIndex="7">
                                        <PropertiesTextEdit DisplayFormatString="C">
                                        </PropertiesTextEdit>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Valor total" FieldName="valorTotalGeral"
                                        VisibleIndex="8">
                                        <PropertiesTextEdit DisplayFormatString="C">
                                        </PropertiesTextEdit>
                                    </dxwgv:GridViewDataTextColumn>
                                </Columns>
                                <StylesEditors>
                                    <Label Font-Bold="True">
                                    </Label>
                                </StylesEditors>
                            </dxwgv:ASPxGridView>
                            <dxwgv:ASPxGridViewExporter ID="grdEx" runat="server" FileName="fornecedores"
                                GridViewID="grd" Landscape="True" PreserveGroupRowStates="True">
                            </dxwgv:ASPxGridViewExporter>
                            <asp:ObjectDataSource ID="sql" runat="server" SelectMethod="vendasPorPeriodo"
                                TypeName="rnRelatorios">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="txtDataInicial" Name="dataInicial"
                                        PropertyName="Text" Type="String" />
                                    <asp:ControlParameter ControlID="txtDataFinal" Name="dataFinal"
                                        PropertyName="Text" Type="String" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>

