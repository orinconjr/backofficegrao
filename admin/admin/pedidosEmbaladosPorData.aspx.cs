﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Data.Filtering;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;

public partial class admin_pedidosEmbaladosPorData : System.Web.UI.Page
{
    DateTemplate dateTemplate = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtDataInicio.Attributes.Add("onkeypress", "Mascaras('DATA', '" + txtDataInicio.ClientID + "');");
            txtDataFim.Attributes.Add("onkeypress", "Mascaras('DATA', '" + txtDataFim.ClientID + "');");
            txtDataInicio.Text = DateTime.Now.ToShortDateString();
            txtDataFim.Text = DateTime.Now.ToShortDateString();
        }

        fillGridEmbaladosData(false);
    }

    public class Item
    {
        public string usuario { get; set; }
        public int? pedidos { get; set; }
        public int? pacotes { get; set; }
        public int? idCentroDeDistribuicao { get; set; }
    }

    public class pedido
    {
        public int? pedidoId { get; set; }
        public int? idPedidoEnvio { get; set; }
        public DateTime? dataSendoEmbalado { get; set; }
        public DateTime? dataEmbaladoFim { get; set; }
        public int? idUsuarioEmbalado { get; set; }
        public int? idUsuarioSeparacao { get; set; }
        public int? volumesEmbalados { get; set; }
        public int? idCentroDeDistribuicao { get; set; }
    }


    private void fillGridEmbaladosData(bool rebind)
    {
        var data = new dbCommerceDataContext();
        int idCentroDistribuicao = rdbCd1.Checked ? 1 : rdbCd2.Checked ? 2 : rdbCd3.Checked ? 3 : rdbCd4.Checked ? 4 : rdbCd5.Checked ? 5 : 45;
        var dataInicio = Convert.ToDateTime(txtDataInicio.Text);
        var dataFim = Convert.ToDateTime(txtDataFim.Text).AddDays(1);

        List<Item> embalados = new List<Item>();

        if (idCentroDistribuicao != 45)
        {
            embalados = (from c in data.tbPedidoEnvios
                              where c.dataFimEmbalagem > dataInicio && c.dataFimEmbalagem < dataFim && (c.idCentroDeDistribuicao == idCentroDistribuicao) && c.idUsuarioEmbalagem != null
                              group c by new { c.idUsuarioEmbalagem } into envios
                              select new Item
                              {
                                  usuario = (from d in data.tbUsuarioExpedicaos where d.idUsuarioExpedicao == envios.FirstOrDefault().idUsuarioEmbalagem select d.nome).First(),
                                  pedidos = envios.Count(),
                                  pacotes = envios.Sum(x => x.volumesEmbalados),
                              }).ToList<Item>();
        }

        else
        {
            embalados = (from c in data.tbPedidoEnvios
                              where c.dataFimEmbalagem > dataInicio && c.dataFimEmbalagem < dataFim && 
                              (c.idCentroDeDistribuicao == 4 || c.idCentroDeDistribuicao == 5) 
                              && c.idUsuarioEmbalagem != null && c.dataFimSeparacaoCd2 != null
                              group c by new { c.idUsuarioEmbalagem } into envios
                              select new Item
                              {
                                  usuario = (from d in data.tbUsuarioExpedicaos where d.idUsuarioExpedicao == envios.FirstOrDefault().idUsuarioEmbalagem select d.nome).First(),
                                  pedidos = envios.Count(),
                                  pacotes = envios.Sum(x => x.volumesEmbalados),
                              }).ToList<Item>();
        }

        grdEmbaladosData.DataSource = embalados;
        
        List<Item> separados = new List<Item>();
        if (idCentroDistribuicao != 45)
        {
            separados = (from c in data.tbPedidoEnvios
                         where c.dataInicioSeparacao > dataInicio && c.dataFimSeparacao < dataFim && (c.idCentroDeDistribuicao == idCentroDistribuicao)
                         group c by new { c.idUsuarioSeparacao } into envios
                         select new Item
                         {
                             usuario = (from d in data.tbUsuarioExpedicaos where d.idUsuarioExpedicao == envios.FirstOrDefault().idUsuarioSeparacao select d.nome).First(),
                             pedidos = envios.Count(),
                             pacotes = envios.Sum(x => x.volumesEmbalados)
                         }).ToList<Item>();
        }
        else
        {
            separados = (from c in data.tbPedidoEnvios
                         where c.dataInicioSeparacao > dataInicio && c.dataFimSeparacao < dataFim && 
                         (c.idCentroDeDistribuicao == 4 || c.idCentroDeDistribuicao == 5) && c.dataFimSeparacaoCd2 != null
                         group c by new { c.idUsuarioSeparacao } into envios
                         select new Item
                         {
                             usuario = (from d in data.tbUsuarioExpedicaos where d.idUsuarioExpedicao == envios.FirstOrDefault().idUsuarioSeparacao select d.nome).First(),
                             pedidos = envios.Count(),
                             pacotes = envios.Sum(x => x.volumesEmbalados)
                         }).ToList<Item>();
        }



        grdEmbaladosData.DataSource = embalados;
        grdSeparadosPorData.DataSource = separados;



        using (
            var txn = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required,
                new System.Transactions.TransactionOptions
                {
                    IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                }))
        {
            List<pedido> pedidos = new List<pedido>();

            pedidos = (from c in data.tbPedidos
                       join d in data.tbPedidoEnvios on c.pedidoId equals d.idPedido
                       // where d.dataInicioEmbalagem != null && d.dataFimEmbalagem != null && d.dataFimEmbalagem > dataInicio && d.dataFimEmbalagem < dataFim && (d.idCentroDeDistribuicao == idCentroDistribuicao)
                       where d.dataInicioEmbalagem != null && d.dataFimEmbalagem != null && d.dataFimEmbalagem > dataInicio && d.dataFimEmbalagem < dataFim 
                       orderby d.dataFimEmbalagem descending
                       select new pedido
                       {
                           pedidoId = c.pedidoId,
                           idPedidoEnvio = d.idPedidoEnvio,
                           dataSendoEmbalado = d.dataInicioEmbalagem,
                           dataEmbaladoFim = d.dataFimEmbalagem,
                           idUsuarioEmbalado = d.idUsuarioEmbalagem,
                           idUsuarioSeparacao = c.idUsuarioSeparacao,
                           volumesEmbalados = d.volumesEmbalados,
                           idCentroDeDistribuicao = d.idCentroDeDistribuicao
                       }).ToList<pedido>();


            if (idCentroDistribuicao != 45)
            {

                pedidos = (from c in pedidos where (c.idCentroDeDistribuicao == idCentroDistribuicao) select c).ToList();
            }
            else
            {
                pedidos = (from c in pedidos where (c.idCentroDeDistribuicao == 4  || c.idCentroDeDistribuicao == 5) select c).ToList();
            }

            grd.DataSource = pedidos;

        }


        if ((!Page.IsPostBack && !Page.IsCallback) | rebind)
        {
            grdEmbaladosData.DataBind();
            grdSeparadosPorData.DataBind();
            grd.DataBind();
        }
    }
    
    protected void grid_AutoFilterCellEditorCreate(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewEditorCreateEventArgs e)
    {
        ASPxGridView grid = sender as ASPxGridView;
        if (e.Column.FieldName == "dataSendoEmbalado")
        {
            //DropDownEditProperties dde = new DropDownEditProperties();
            //dde.EnableClientSideAPI = true;

            //dateTemplate = new DateTemplate();

            //dde.DropDownWindowTemplate = dateTemplate;
            //e.EditorProperties = dde;
        }
    }
    protected void grid_AutoFilterCellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
    {
        ASPxGridView grid = sender as ASPxGridView;
        if (e.Column.FieldName == "dataSendoEmbalado")
        {
            //ASPxDropDownEdit dde = e.Editor as ASPxDropDownEdit;

            //dde.ClientSideEvents.CloseUp = String.Format("function (s, e) {{ ApplyFilter(s, {0}, {1}); }}", dateTemplate.cIdFrom, dateTemplate.cIdTo);
            //dde.ClientSideEvents.DropDown = String.Format("function (s, e) {{ OnDropDown(s, {0}, {1}); }}", dateTemplate.cIdFrom, dateTemplate.cIdTo);
            //dde.ReadOnly = true;
        }
    }

    protected void grid_ProcessColumnAutoFilter(object sender, ASPxGridViewAutoFilterEventArgs e)
    {
        if (e.Value == "|") return;
        if (e.Column.FieldName == "dataSendoEmbalado")
        {
            //if (e.Kind == GridViewAutoFilterEventKind.CreateCriteria)
            //{
            //    Session["dataSendoEmbalado"] = e.Value;
            //    String[] dates = e.Value.Split('|');
            //    DateTime dateFrom = Convert.ToDateTime(dates[0]),
            //        dateTo = Convert.ToDateTime(dates[1]);
            //    e.Criteria = (new OperandProperty("dataSendoEmbalado") >= dateFrom) &
            //                 (new OperandProperty("dataSendoEmbalado") <= dateTo);
            //}
            //else
            //{
            //if (Session["dataSendoEmbalado"] != null)
            //e.Value = Session["dataSendoEmbalado"].ToString();
            //}
        }
    }

    protected void btnChecarPedidosMl_OnClick(object sender, EventArgs e)
    {
        //var meli = mlIntegracao.verificaAutenticacao("pedidos.aspx", Request.QueryString["code"]);
        mlIntegracao.checaPedidosML();
        rnIntegracoes.checaPedidosExtra(true);
        grd.DataBind();
    }

    protected void grd_OnCustomUnboundColumnData(object sender, ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "pedidoIdCliente")
        {
            int pedidoId = Convert.ToInt32(e.GetListSourceFieldValue("pedidoId"));
            e.Value = rnFuncoes.retornaIdCliente(pedidoId);
        }
        if (e.Column.FieldName == "tempoEmbalagem")
        {
            var dataSendoEmbalado = Convert.ToDateTime(e.GetListSourceFieldValue("dataSendoEmbalado"));
            var dataEmbaladoFim = Convert.ToDateTime(e.GetListSourceFieldValue("dataEmbaladoFim"));
            var tempoEmbalagem = dataEmbaladoFim - dataSendoEmbalado;
            e.Value = tempoEmbalagem;
        }

        if (e.Column.FieldName == "tempoEmbalagem")
        {
            var dataSendoEmbalado = Convert.ToDateTime(e.GetListSourceFieldValue("dataSendoEmbalado"));
            var dataEmbaladoFim = Convert.ToDateTime(e.GetListSourceFieldValue("dataEmbaladoFim"));
            var tempoEmbalagem = dataEmbaladoFim - dataSendoEmbalado;
            e.Value = tempoEmbalagem;
        }
    }

    protected void grdTotal_OnCustomUnboundColumnData(object sender, ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "embaladores")
        {
            var dataEmbaladoFim = Convert.ToDateTime(e.GetListSourceFieldValue("diaDoMes"));
            var data = new dbCommerceDataContext();

            using (
                var txn = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required,
                    new System.Transactions.TransactionOptions
                    {
                        IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                    }))
            {

                var nomes = (from c in data.tbPedidos
                             join d in data.tbPedidoEnvios on c.pedidoId equals d.idPedido
                             join ue in data.tbUsuarioExpedicaos on d.idUsuarioEmbalagem equals ue.idUsuarioExpedicao
                             where
                                 d.dataFimEmbalagem != null &&
                                 (d.dataFimEmbalagem.Value.Day == dataEmbaladoFim.Day &&
                                  d.dataFimEmbalagem.Value.Month == dataEmbaladoFim.Month &&
                                  d.dataFimEmbalagem.Value.Year == dataEmbaladoFim.Year)
                             group d.idUsuarioEmbalagem by ue.nome
                    into g
                             select new { nomeUsuarioExpedicao = g.Key, totalUsuario = g.Key.Count() }).OrderByDescending(x => x.totalUsuario);

                e.Value = "";

                foreach (var usuario in nomes)
                {
                    e.Value += usuario.nomeUsuarioExpedicao + " (" + usuario.totalUsuario + ")" + " - ";
                }
            }

        }
    }
    
    protected void grdTempoSeparacao_OnAutoFilterCellEditorCreate(object sender, ASPxGridViewEditorCreateEventArgs e)
    {
        ASPxGridView grid = sender as ASPxGridView;
        if (e.Column.FieldName == "dataSendoEmbalado")
        {
            DropDownEditProperties dde = new DropDownEditProperties();
            dde.EnableClientSideAPI = true;

            dateTemplate = new DateTemplate();

            dde.DropDownWindowTemplate = dateTemplate;
            e.EditorProperties = dde;
        }
    }

    protected void grdTempoSeparacao_OnAutoFilterCellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
    {
        ASPxGridView grid = sender as ASPxGridView;
        if (e.Column.FieldName == "dataSendoEmbalado")
        {
            ASPxDropDownEdit dde = e.Editor as ASPxDropDownEdit;

            dde.ClientSideEvents.CloseUp = String.Format("function (s, e) {{ ApplyFilter(s, {0}, {1}); }}", dateTemplate.cIdFrom, dateTemplate.cIdTo);
            //dde.ClientSideEvents.DropDown = String.Format("function (s, e) {{ OnDropDown(s, {0}, {1}); }}", dateTemplate.cIdFrom, dateTemplate.cIdTo);
            dde.ReadOnly = true;
        }
    }

    protected void grdTempoSeparacao_OnProcessColumnAutoFilter(object sender, ASPxGridViewAutoFilterEventArgs e)
    {
        if (e.Value == "|") return;
        if (e.Column.FieldName == "dataSendoEmbalado")
        {
            if (e.Kind == GridViewAutoFilterEventKind.CreateCriteria)
            {
                Session["dataSendoEmbalado"] = e.Value;
                String[] dates = e.Value.Split('|');
                DateTime dateFrom = Convert.ToDateTime(dates[0]),
                    dateTo = Convert.ToDateTime(dates[1]);
                e.Criteria = (new OperandProperty("dataSendoEmbalado") >= dateFrom) &
                             (new OperandProperty("dataSendoEmbalado") <= dateTo);
            }
            else
            {
                if (Session["dataSendoEmbalado"] != null)
                    e.Value = Session["dataSendoEmbalado"].ToString();
            }
        }
    }

    protected void grdTempoSeparacao_OnCustomUnboundColumnData(object sender, ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "pedidoIdCliente")
        {
            int pedidoId = Convert.ToInt32(e.GetListSourceFieldValue("pedidoId"));
            e.Value = rnFuncoes.retornaIdCliente(pedidoId);
        }
        if (e.Column.FieldName == "tempoSeparacao")
        {
            var dataSendoEmbalado = Convert.ToDateTime(e.GetListSourceFieldValue("dataSendoSeparado"));
            var dataEmbaladoFim = Convert.ToDateTime(e.GetListSourceFieldValue("dataSeparacaoFim"));
            var tempoEmbalagem = dataEmbaladoFim - dataSendoEmbalado;

            if (tempoEmbalagem.TotalMinutes > 0)
                e.Value = tempoEmbalagem;
            else
                e.Value = "";
        }

    }
    

    protected void rdbCd1_CheckedChanged(object sender, EventArgs e)
    {
        fillGridEmbaladosData(true);
    }

    protected void rdbCd2_CheckedChanged(object sender, EventArgs e)
    {
        fillGridEmbaladosData(true);
    }

    protected void rdbCd3_CheckedChanged(object sender, EventArgs e)
    {
        fillGridEmbaladosData(true);
    }

    protected void rdbCd4_CheckedChanged(object sender, EventArgs e)
    {
        fillGridEmbaladosData(true);
    }

    protected void rdbCd5_CheckedChanged(object sender, EventArgs e)
    {
        fillGridEmbaladosData(true);
    }
    protected void rdbCd45_CheckedChanged(object sender, EventArgs e)
    {
        fillGridEmbaladosData(true);
    }
    protected void btnFiltroEmbalagem_Click(object sender, EventArgs e)
    {
        fillGridEmbaladosData(true);
    }

    protected void grdEmbaladosData_CustomUnboundColumnData(object sender, ASPxGridViewColumnDataEventArgs e)
    {

    }
}
