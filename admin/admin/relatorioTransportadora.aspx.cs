﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Data.Filtering;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;

public partial class admin_relatorioTransportadora : System.Web.UI.Page
{
    DateTemplate dateTemplate = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            txtValorDiferenca.Attributes.Add("onkeyPress", "return moeda(this,'.',',',event);");
        }
        fillGrid(false);
    }

    private void fillGrid(bool rebind)
    {
        var pedidoDc = new dbCommerceDataContext();
        var pedidos = (from c in pedidoDc.tbPedidos
                       join d in pedidoDc.tbPedidoEnvios on c.pedidoId equals d.idPedido
                       where d.valorSelecionado != null
                       select new
                       {
                           c.pedidoId,
                           dataEmbaladoFim = d.dataFimEmbalagem,
                           d.formaDeEnvio,
                           d.volumesEmbalados,
                           d.valorSelecionado,
                           c.valorCobradoTransportadora,
                           taxaExtra = (c.tbPedidoPacotes.FirstOrDefault().taxaExtra ?? 0),
                           valorTotalComTaxaExtra = d.valorSelecionado + (c.tbPedidoPacotes.FirstOrDefault().taxaExtra ?? 0)
                       });
        grd.DataSource = pedidos;
        if ((!Page.IsPostBack && !Page.IsCallback) | rebind) grd.DataBind();
    }
    protected void grd_OnCustomUnboundColumnData(object sender, ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "pedidoIdCliente")
        {
            int pedidoId = Convert.ToInt32(e.GetListSourceFieldValue("pedidoId"));
            e.Value = rnFuncoes.retornaIdCliente(pedidoId);
        }
    }

    protected void grid_AutoFilterCellEditorCreate(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewEditorCreateEventArgs e)
    {
        ASPxGridView grid = sender as ASPxGridView;
        if (e.Column.FieldName == "dataEmbaladoFim")
        {
            DropDownEditProperties dde = new DropDownEditProperties();
            dde.EnableClientSideAPI = true;

            dateTemplate = new DateTemplate();

            dde.DropDownWindowTemplate = dateTemplate;
            e.EditorProperties = dde;
        }
    }
    protected void grid_AutoFilterCellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
    {
        ASPxGridView grid = sender as ASPxGridView;
        if (e.Column.FieldName == "dataEmbaladoFim")
        {
            ASPxDropDownEdit dde = e.Editor as ASPxDropDownEdit;

            dde.ClientSideEvents.CloseUp = String.Format("function (s, e) {{ ApplyFilter(s, {0}, {1}); }}", dateTemplate.cIdFrom, dateTemplate.cIdTo);
            //dde.ClientSideEvents.DropDown = String.Format("function (s, e) {{ OnDropDown(s, {0}, {1}); }}", dateTemplate.cIdFrom, dateTemplate.cIdTo);
            dde.ReadOnly = true;
        }
    }

    protected void grid_ProcessColumnAutoFilter(object sender, ASPxGridViewAutoFilterEventArgs e)
    {
        if (e.Value == "|") return;
        if (e.Column.FieldName == "dataEmbaladoFim")
        {
            if (e.Kind == GridViewAutoFilterEventKind.CreateCriteria)
            {
                Session["dataEmbaladoFim"] = e.Value;
                String[] dates = e.Value.Split('|');
                DateTime dateFrom = Convert.ToDateTime(dates[0]),
                    dateTo = Convert.ToDateTime(dates[1]);
                e.Criteria = (new OperandProperty("dataEmbaladoFim") >= dateFrom) &
                             (new OperandProperty("dataEmbaladoFim") <= dateTo);
            }
            else
            {
                if (Session["dataEmbaladoFim"] != null)
                    e.Value = Session["dataEmbaladoFim"].ToString();
            }
        }
    }
    protected void btnEditarValor_OnCommand(object sender, CommandEventArgs e)
    {
        hiddenIdPedidoJadlogEditar.Value = e.CommandArgument.ToString();
        int pedidoId = Convert.ToInt32(e.CommandArgument);
        var pedidoDc = new dbCommerceDataContext();
        var pedido = (from c in pedidoDc.tbPedidos where c.pedidoId == pedidoId select c).FirstOrDefault();
        if (pedido.valorCobradoTransportadora > 0)
        {
            txtValorDiferenca.Text = pedido.valorCobradoTransportadora.ToString();
        }
        else
        {
            txtValorDiferenca.Text = "0";
        }
        litPedidoIdDiferenca.Text = pedido.pedidoId.ToString();
        pnGridEditar.Visible = true;
        pnGridVerPedidos.Visible = false;
    }
    protected void btnGravarDiferenca_onClick(object sender, EventArgs e)
    {
        int pedidoId = Convert.ToInt32(hiddenIdPedidoJadlogEditar.Value);
        var pedidoDc = new dbCommerceDataContext();
        var pedido = (from c in pedidoDc.tbPedidos where c.pedidoId == pedidoId select c).FirstOrDefault();
        pedido.valorCobradoTransportadora = Convert.ToDecimal(txtValorDiferenca.Text);
        pedidoDc.SubmitChanges();
        txtValorDiferenca.Text = "0";
        pnGridEditar.Visible = false;
        pnGridVerPedidos.Visible = true;
        fillGrid(true);
    }
}