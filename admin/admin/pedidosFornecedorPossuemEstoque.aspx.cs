﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Data.Filtering;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;

public partial class admin_pedidosFornecedorPossuemEstoque : System.Web.UI.Page
{
    DateTemplate dateTemplate = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        fillGrid(false);
    }

    private void fillGrid(bool rebind)
    {

        var data = new dbCommerceDataContext();

        List<int> statusPedido = new List<int> { 3, 4, 11 };

        var produtoEmEstoque = data.admin_produtosEmEstoque().Where(x => x.estoqueLivre > 1).Where(x => !x.produtoNome.Contains("Kit Berço")).Select(x => x.produtoId).ToList();

        var pedidosItensAoFornecedor = (from f in data.tbPedidoFornecedorItems
                                        join p in data.tbPedidos on f.idPedido equals p.pedidoId
                                        where f.entregue == false && produtoEmEstoque.Contains(f.idProduto) && statusPedido.Contains(p.statusDoPedido)
                                        orderby f.tbPedidoFornecedor.dataLimite
                                        select new
                                        {
                                            p.pedidoId,
                                            f.tbProduto.tbProdutoFornecedor.fornecedorNome,
                                            f.tbProduto.produtoNome,
                                            f.tbPedidoFornecedor.dataLimite,
                                            p.dataHoraDoPedido,
                                            statusDoPedido = (p.statusDoPedido == 3 ? "Pagamento Confirmado" : p.statusDoPedido == 4 ? "Seu pedido está sendo embalado" : "Separaçao de Estoque")
                                        }).ToList();


        grd.DataSource = pedidosItensAoFornecedor;
        if ((!Page.IsPostBack && !Page.IsCallback) | rebind) grd.DataBind();

    }
    protected void btPdf_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WritePdfToResponse();
    }
    protected void btXsl_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteXlsToResponse();
    }
    protected void btRtf_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteRtfToResponse();
    }
    protected void btCsv_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteCsvToResponse();
    }

    protected void grid_AutoFilterCellEditorCreate(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewEditorCreateEventArgs e)
    {
        ASPxGridView grid = sender as ASPxGridView;
        if (e.Column.FieldName == "dataHoraDoPedido")
        {
            DropDownEditProperties dde = new DropDownEditProperties();
            dde.EnableClientSideAPI = true;
            dateTemplate = new DateTemplate();
            dde.DropDownWindowTemplate = dateTemplate;
            e.EditorProperties = dde;
        }
        if (e.Column.FieldName == "prazoFinalPedido")
        {
            DropDownEditProperties dde = new DropDownEditProperties();
            dde.EnableClientSideAPI = true;
            dateTemplate = new DateTemplate();
            dde.DropDownWindowTemplate = dateTemplate;
            e.EditorProperties = dde;
        }

    }
    protected void grid_AutoFilterCellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
    {
        ASPxGridView grid = sender as ASPxGridView;
        //if (e.Column.FieldName == "dataHoraDoPedido")
        //{
        //    ASPxDropDownEdit dde = e.Editor as ASPxDropDownEdit;
        //    dde.ClientSideEvents.CloseUp = String.Format("function (s, e) {{ ApplyFilter(s, {0}, {1}); }}", dateTemplate.cIdFrom, dateTemplate.cIdTo);
        //    //dde.ClientSideEvents.DropDown = String.Format("function (s, e) {{ OnDropDown(s, {0}, {1}); }}", dateTemplate.cIdFrom, dateTemplate.cIdTo);
        //    dde.ReadOnly = true;
        //}
        if (e.Column.FieldName == "dataHoraDoPedido")
        {
            ASPxDropDownEdit dde = e.Editor as ASPxDropDownEdit;
            dde.ClientSideEvents.CloseUp = String.Format("function (s, e) {{ ApplyFilter(s, {0}, {1}, 'dataHoraDoPedido'); }}", dateTemplate.cIdFrom, dateTemplate.cIdTo);
            //dde.ClientSideEvents.DropDown = String.Format("function (s, e) {{ OnDropDown(s, {0}, {1}); }}", dateTemplate.cIdFrom, dateTemplate.cIdTo);
            dde.ReadOnly = true;
        }
        if (e.Column.FieldName == "prazoFinalPedido")
        {
            ASPxDropDownEdit dde = e.Editor as ASPxDropDownEdit;
            dde.ClientSideEvents.CloseUp = String.Format("function (s, e) {{ ApplyFilter(s, {0}, {1}, 'prazoFinalPedido'); }}", dateTemplate.cIdFrom, dateTemplate.cIdTo);
            //dde.ClientSideEvents.DropDown = String.Format("function (s, e) {{ OnDropDown(s, {0}, {1}); }}", dateTemplate.cIdFrom, dateTemplate.cIdTo);
            dde.ReadOnly = true;
        }
    }

    protected void grid_ProcessColumnAutoFilter(object sender, ASPxGridViewAutoFilterEventArgs e)
    {
        if (e.Value == "|") return;
        //if (e.Column.FieldName == "dataHoraDoPedido")
        //{
        //    if (e.Kind == GridViewAutoFilterEventKind.CreateCriteria)
        //    {
        //        Session["dataHoraDoPedido"] = e.Value;
        //        String[] dates = e.Value.Split('|');
        //        DateTime dateFrom = Convert.ToDateTime(dates[0]),
        //            dateTo = Convert.ToDateTime(dates[1]);
        //        e.Criteria = (new OperandProperty("dataHoraDoPedido") >= dateFrom) &
        //                     (new OperandProperty("dataHoraDoPedido") <= dateTo);
        //    }
        //    else
        //    {
        //        if (Session["dataHoraDoPedido"] != null)
        //            e.Value = Session["dataHoraDoPedido"].ToString();
        //    }
        //}
        if (e.Column.FieldName == "dataHoraDoPedido")
        {
            if (e.Kind == GridViewAutoFilterEventKind.CreateCriteria)
            {
                Session["dataHoraDoPedido"] = e.Value;
                String[] dates = e.Value.Split('|');
                DateTime dateFrom = Convert.ToDateTime(dates[0]),
                    dateTo = Convert.ToDateTime(dates[1]);
                e.Criteria = (new OperandProperty("dataHoraDoPedido") >= dateFrom) &
                             (new OperandProperty("dataHoraDoPedido") <= dateTo);
            }
            else
            {
                if (Session["dataHoraDoPedido"] != null)
                    e.Value = Session["dataHoraDoPedido"].ToString();
            }
        }
        if (e.Column.FieldName == "prazoFinalPedido")
        {
            if (e.Kind == GridViewAutoFilterEventKind.CreateCriteria)
            {
                Session["prazoFinalPedido"] = e.Value;
                String[] dates = e.Value.Split('|');
                DateTime dateFrom = Convert.ToDateTime(dates[0]),
                    dateTo = Convert.ToDateTime(dates[1]);
                e.Criteria = (new OperandProperty("prazoFinalPedido") >= dateFrom) &
                             (new OperandProperty("prazoFinalPedido") <= dateTo);
            }
            else
            {
                if (Session["prazoFinalPedido"] != null)
                    e.Value = Session["prazoFinalPedido"].ToString();
            }
        }
    }




    protected void btnChecarPedidosMl_OnClick(object sender, EventArgs e)
    {
        //var meli = mlIntegracao.verificaAutenticacao("pedidos.aspx", Request.QueryString["code"]);
        mlIntegracao.checaPedidosML();
        rnIntegracoes.checaPedidosExtra(true);
        grd.DataBind();
    }

    protected void grd_OnCustomUnboundColumnData(object sender, ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "pedidoIdCliente")
        {
            int pedidoId = Convert.ToInt32(e.GetListSourceFieldValue("pedidoId"));
            e.Value = rnFuncoes.retornaIdCliente(pedidoId);
        }

    }
}