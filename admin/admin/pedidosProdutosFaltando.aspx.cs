﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_pedidosProdutosFaltando : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            fillGrid();
        }
    }

    private void fillGrid()
    {
        var produtosDc = new dbCommerceDataContext();
        var produtosFaltando = (from c in produtosDc.tbPedidoProdutoFaltandos where c.entregue == false select new
        {
            c.produtoId,
            c.pedidoId,
            c.tbProduto.produtoIdDaEmpresa,
            c.tbProduto.complementoIdDaEmpresa,
            c.tbProduto.produtoNome,
            c.idPedidoProdutoFaltando,
            c.tbProduto.produtoFornecedor
        }).OrderBy(x => x.produtoFornecedor).ThenBy(x => x.produtoNome);

        lstProdutos.DataSource = produtosFaltando;
        lstProdutos.DataBind();
    }
    protected void lstProdutos_OnItemDataBound(object sender, ListViewItemEventArgs e)
    {
        HiddenField hdfIdPedido = (HiddenField) e.Item.FindControl("hdfIdPedido");
        HiddenField hdfIdProduto = (HiddenField) e.Item.FindControl("hdfIdProduto");
        HiddenField hdfProdutoFornecedor = (HiddenField)e.Item.FindControl("hdfProdutoFornecedor");
        Literal litIdPedidoCliente = (Literal)e.Item.FindControl("litIdPedidoCliente");
        Literal litFornecedor = (Literal)e.Item.FindControl("litFornecedor");
        Literal litRomaneios = (Literal)e.Item.FindControl("litRomaneios");
        Button btnGerarPedido = (Button)e.Item.FindControl("btnGerarPedido");
        litIdPedidoCliente.Text = rnFuncoes.retornaIdCliente(Convert.ToInt32(hdfIdPedido.Value)).ToString();

        int idPedido = Convert.ToInt32(hdfIdPedido.Value);
        int produtoId = Convert.ToInt32(hdfIdProduto.Value);
        int idFornecedor = Convert.ToInt32(hdfProdutoFornecedor.Value);
        var fornecedorDc = new dbCommerceDataContext();
        var fornecedor = (from c in fornecedorDc.tbProdutoFornecedors where c.fornecedorId == idFornecedor select c).First();
        litFornecedor.Text = fornecedor.fornecedorNome;

        var pedidoDc = new dbCommerceDataContext();
        var romaneios = (from c in pedidoDc.tbPedidoFornecedorItems where c.idPedido == idPedido && c.idProduto == produtoId && c.entregue == false select new{c.idPedidoFornecedor,
            c.tbPedidoFornecedor.dataLimite}).FirstOrDefault();
        string romaneiosLista = "";
        if (romaneios != null)
        {
            romaneiosLista = romaneios.idPedidoFornecedor + " - " + romaneios.dataLimite.ToShortDateString();
            btnGerarPedido.Visible = false;
        }
        litRomaneios.Text = romaneiosLista;
    }

    protected void btnGravarEntregue_OnClick(object sender, EventArgs e)
    {
        foreach (var item in lstProdutos.Items)
        {
            CheckBox chkEntregue = (CheckBox) item.FindControl("chkEntregue");
            if (chkEntregue.Checked)
            {
                var produtosDc = new dbCommerceDataContext();
                HiddenField hdfItemFaltando = (HiddenField) item.FindControl("hdfItemFaltando");
                int idItemFaltando = Convert.ToInt32(hdfItemFaltando.Value);
                var pedidoDc = new dbCommerceDataContext();
                var itemFaltando =
                    (from c in pedidoDc.tbPedidoProdutoFaltandos
                        where c.idPedidoProdutoFaltando == idItemFaltando
                        select c).First();
                itemFaltando.entregue = true;


                var produto = (from c in produtosDc.tbProdutos where c.produtoId == itemFaltando.produtoId select c).First();
                var logEstoque = new tbProdutoLogEstoque();
                logEstoque.idProduto = itemFaltando.produtoId;
                logEstoque.idOperacao = itemFaltando.pedidoId;
                logEstoque.estoqueAnterior = Convert.ToInt32(produto.estoqueReal);
                produto.estoqueReal += 1;
                logEstoque.estoqueAlterado = Convert.ToInt32(produto.estoqueReal);
                logEstoque.data = DateTime.Now;
                logEstoque.descricao = "Produto faltando entregue" + itemFaltando.pedidoId;
                produtosDc.tbProdutoLogEstoques.InsertOnSubmit(logEstoque);
                produtosDc.SubmitChanges();

                pedidoDc.SubmitChanges();
            }
        }
        fillGrid();
    }

    protected void btnGerarPedido_OnCommand(object sender, CommandEventArgs e)
    {
        int idPedidoProdutoFaltando = Convert.ToInt32(e.CommandArgument);
        var produtosDc = new dbCommerceDataContext();
        var produtoFaltando =
            (from c in produtosDc.tbPedidoProdutoFaltandos
                where c.idPedidoProdutoFaltando == idPedidoProdutoFaltando
                select c).First();

        var pedidoProduto = rnPedidos.reservaProdutoOuPedeFornecedor(produtoFaltando.pedidoId, produtoFaltando.produtoId, 1, Convert.ToInt32(produtoFaltando.itemPedidoId));
        fillGrid();

    }
}