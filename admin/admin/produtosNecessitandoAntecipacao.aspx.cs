﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxGridView;

public partial class admin_produtosNecessitandoAntecipacao : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        fillGrid(true);
    }

    private void fillGrid(bool rebind)
    {
        var data = new dbCommerceDataContext();
        var listaGeralAguardando = (from c in data.tbItemPedidoEstoques
                                    where c.reservado == false && c.cancelado == false && c.dataLimite != null && c.tbItensPedido.tbPedido.statusDoPedido != 3 && c.tbItensPedido.tbPedido.statusDoPedido != 2
                                    group c by new
                                    {
                                        c.produtoId,
                                        dataLimite = c.dataLimite.Value.Date,
                                        c.tbProduto.produtoNome,
                                        c.tbProduto.tbProdutoFornecedor.fornecedorNome
                                    } into itens
                                    select new
                                    {
                                        produtoId = itens.Key.produtoId,
                                        dataLimite = itens.Key.dataLimite.Date,
                                        produtoNome = itens.Key.produtoNome,
                                        fornecedorNome = itens.Key.fornecedorNome,
                                        quantidade = itens.Count(),
                                        quantidadeNaData = (from d in data.tbPedidoFornecedorItems
                                                            where (d.entregue == false | (d.entregue == true && (d.liberado ?? false) == false)) && d.idProduto == itens.Key.produtoId
                                                            where d.tbPedidoFornecedor.dataLimite.Date <= itens.Key.dataLimite.Date
                                                            select d).Count()
                                    }).Where(x => x.quantidade > x.quantidadeNaData).ToList();
        grd.DataSource = listaGeralAguardando;
        if ((!Page.IsPostBack && !Page.IsCallback) | rebind) grd.DataBind();
    }
}