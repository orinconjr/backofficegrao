﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_preverPrecoNovo : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btAlterar_Click(object sender, EventArgs e)
    {
        foreach (GridViewRow row in grd.Rows)
        {
            Label lblProdutoId = ((Label)row.FindControl("lblProdutoId"));
            Label lblMargem = ((Label)row.FindControl("lblMargem"));
            Label lblPrecoNovo = ((Label)row.FindControl("lblPrecoNovo"));

            decimal margem = Convert.ToDecimal(lblMargem.Text);
            margem = margem + 7;
            TextBox txtPrecoDeCusto = ((TextBox)row.FindControl("txtPrecoDeCusto"));
            TextBox txtIdDaEmpresa = ((TextBox)row.FindControl("txtIdDaEmpresa"));
            TextBox txtNome = ((TextBox)row.FindControl("txtNome"));
            TextBox txtPrecoPromocional = ((TextBox)row.FindControl("txtPrecoPromocional"));
            CheckBox ckbDesativar = ((CheckBox)row.FindControl("ckbDesativar"));

            string idDaEmpresa = txtIdDaEmpresa.Text;
            decimal precoDeCusto = decimal.Parse(txtPrecoDeCusto.Text);
            decimal precoPromocional = decimal.Parse(txtPrecoPromocional.Text);
            //decimal precoPromocional = precoDeCusto + (precoDeCusto * int.Parse(txtPorcentagem.Text) / 100) ;
            decimal preco = precoDeCusto + (precoDeCusto * Convert.ToInt32(margem) / 100);
            //decimal preco = precoDeCusto * 2;

            char[] delimiterChars = { ',' };
            string[] arrayPreco = preco.ToString().Split(delimiterChars);

            string precoNovo = arrayPreco[0] + "." + "90";
            lblPrecoNovo.Text = precoNovo;

            /*string produtoAtivo; 
            if (ckbDesativar.Checked == true) produtoAtivo = "False";
            else produtoAtivo = "True";

            sqlProdutos.UpdateParameters.Clear();
            sqlProdutos.UpdateParameters.Add("produtoPrecoDeCusto", DbType.Decimal, precoDeCusto.ToString());
            sqlProdutos.UpdateParameters.Add("produtoPreco", DbType.String, precoNovo);
            sqlProdutos.UpdateParameters.Add("produtoIdDaEmpresa", DbType.String, idDaEmpresa);
            sqlProdutos.UpdateParameters.Add("produtoPrecoPromocional", DbType.Decimal, precoPromocional.ToString());
            sqlProdutos.UpdateParameters.Add("produtoNome", DbType.String, txtNome.Text);
            sqlProdutos.UpdateParameters.Add("produtoAtivo", DbType.String, produtoAtivo);
            sqlProdutos.UpdateParameters.Add("produtoId", lblProdutoId.Text.ToString());
            sqlProdutos.Update();*/

            int produtoId = 0;
            int.TryParse(lblProdutoId.Text, out produtoId);

            /*var pedidoDc = new dbCommerceDataContext();
            var itensPedido = (from c in pedidoDc.tbPedidoFornecedorItems where c.idProduto == produtoId && c.brinde != true && c.entregue != true select c);
            foreach (var item in itensPedido)
            {
                item.custo = precoDeCusto;
            }
            pedidoDc.SubmitChanges();*/
        }

        grd.DataBind();
    }

    protected void Button1_Click(object sender, EventArgs e)
    {

    }

    protected void grd_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType != DataControlRowType.DataRow) return;
        Label lblProdutoId = ((Label)e.Row.FindControl("lblProdutoId"));
        Label lblMargem = ((Label)e.Row.FindControl("lblMargem"));
        Label lblPrecoNovo = ((Label)e.Row.FindControl("lblPrecoNovo"));
        Label lblPrecoNovoPromocional = ((Label)e.Row.FindControl("lblPrecoNovoPromocional"));

        decimal margem = 1;
        decimal.TryParse(lblMargem.Text, out margem);
        margem = margem + 7;
        TextBox txtPrecoDeCusto = ((TextBox)e.Row.FindControl("txtPrecoDeCusto"));
        TextBox txtIdDaEmpresa = ((TextBox)e.Row.FindControl("txtIdDaEmpresa"));
        TextBox txtNome = ((TextBox)e.Row.FindControl("txtNome"));
        TextBox txtPrecoPromocional = ((TextBox)e.Row.FindControl("txtPrecoPromocional"));
        CheckBox ckbDesativar = ((CheckBox)e.Row.FindControl("ckbDesativar"));

        string idDaEmpresa = txtIdDaEmpresa.Text;
        decimal precoDeCusto = decimal.Parse(txtPrecoDeCusto.Text);
        decimal precoPromocional = decimal.Parse(txtPrecoPromocional.Text);
        char[] delimiterChars = { ',' };
        decimal preco = precoDeCusto + (precoDeCusto * Convert.ToInt32(margem) / 100);


        if (precoPromocional > 0)
        {
            decimal margemPromocional = (((precoPromocional * 100) / precoDeCusto) - 100) + 7;
            decimal precoPromocionalNovo = precoDeCusto + (precoDeCusto * Convert.ToInt32(margemPromocional) / 100);

            string[] arrayPrecoPromocional = precoPromocionalNovo.ToString().Split(delimiterChars);
            string precoNovoPromocional = arrayPrecoPromocional[0] + "." + "90";
            lblPrecoNovoPromocional.Text = precoNovoPromocional;

        }
        //decimal precoPromocional = precoDeCusto + (precoDeCusto * int.Parse(txtPorcentagem.Text) / 100) ;
        //decimal preco = precoDeCusto * 2;

        string[] arrayPreco = preco.ToString().Split(delimiterChars);

        string precoNovo = arrayPreco[0] + "." + "90";
        lblPrecoNovo.Text = precoNovo;
    }
}
