﻿<%@ Page Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="faixaDeCepComDesconto.aspx.cs" Inherits="admin_faixaDeCepComDesconto"  Theme="Glass" MaintainScrollPositionOnPostback=true %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dxwgv" %>
<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" Runat="Server">
    <table cellpadding="0" cellspacing="0" style="width: 920px">
    <tr>
        <td class="tituloPaginas" valign="top">
            <asp:Label ID="lblAcao" runat="server">Faixas de Cep</asp:Label>
        &nbsp;com Desconto</td>
    </tr>
    <tr>
        <td>
            
    <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
        <tr>
            <td align="right">
                <table cellpadding="0" cellspacing="0" style="width: 834px">
                    <tr>
                        <td align="right">
                            &nbsp;</td>
                        <td height="38" 
                            style="background-image: url('images/bkExportar.jpg'); padding-bottom: 1px;" 
                            valign="bottom" width="231">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="width: 97px">
                                        &nbsp;</td>
                                    <td style="width: 33px">
                                        <asp:ImageButton ID="btPdf" runat="server" ImageUrl="~/admin/images/btPdf.jpg" 
                                            style="margin-left: 5px" />
                                    </td>
                                    <td style="width: 31px">
                                        <asp:ImageButton ID="btXsl" runat="server" ImageUrl="~/admin/images/btXls.jpg" 
                                            style="margin-left: 5px" />
                                    </td>
                                    <td style="width: 36px">
                                        <asp:ImageButton ID="btRtf" runat="server" ImageUrl="~/admin/images/btRtf.jpg" 
                                            style="margin-left: 5px" />
                                    </td>
                                    <td valign="bottom">
                                        <asp:ImageButton ID="btCsv" runat="server" ImageUrl="~/admin/images/btCsv.jpg" 
                                            style="margin-left: 5px" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="height: 19px">
                </td>
        </tr>
        <tr>
            <td>
                <dxwgv:aspxgridview ID="grd" runat="server" AutoGenerateColumns="False" 
                    DataSourceID="sqlFaixaDeCepComDesconto" 
                    KeyFieldName="faixaDeCepDescontoId" Width="834px" 
                    Cursor="auto" onhtmlrowcreated="grd_HtmlRowCreated" 
                    onrowinserting="grd_RowInserting" onrowupdating="grd_RowUpdating">
                    <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                    <SettingsText ConfirmDelete="Tem certeza que deseja excluir? automaticamente será excluido todos os pesos e preços relacionado a ela." 
                        emptydatarow="Nenhum registro encontrado." />
                    <SettingsPager Position="TopAndBottom" PageSize="50" 
                        ShowDisabledButtons="False" AlwaysShowPager="True">
                        <Summary AllPagesText="Páginas: {0} - {1} ({2} items)" 
                            Text="Página {0} de {1} ({2} registros encontrados)" />
                    </SettingsPager>
                    <settings showfilterrow="True" ShowGroupButtons="False" />
                    <SettingsEditing EditFormColumnCount="4" 
                        PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1" 
                        PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter" 
                        PopupEditFormWidth="700px" mode="Inline" />
                    <Columns>
                        <dxwgv:GridViewDataTextColumn Caption="ID" 
                            FieldName="faixaDeCepDescontoId" ReadOnly="True" VisibleIndex="0" 
                            Width="30px">
                            <Settings AutoFilterCondition="Contains" />
                            <editformsettings visible="False" CaptionLocation="Top" ColumnSpan="2" />
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Descrição" FieldName="descricao" 
                            VisibleIndex="1" Width="200px">
                            <PropertiesTextEdit>
                                <ValidationSettings SetFocusOnError="True">
                                    <RequiredField ErrorText="Preencha a descrição" IsRequired="True" />
                                </ValidationSettings>
                            </PropertiesTextEdit>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataComboBoxColumn Caption="Tipo de entrega" 
                            FieldName="tipoDeEntregaId" VisibleIndex="2">
                            <PropertiesComboBox DataSourceID="sqlTipoDeEntrega" 
                                TextField="tipoDeEntregaNome" ValueField="tipoDeEntregaId" 
                                ValueType="System.String">
                                <ValidationSettings SetFocusOnError="True">
                                    <RequiredField ErrorText="Selecione o tipo de entrega." IsRequired="True" />
                                </ValidationSettings>
                            </PropertiesComboBox>
                        </dxwgv:GridViewDataComboBoxColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Faixa inicial" FieldName="faixaInicial" 
                            VisibleIndex="3" Width="90px">
                            <PropertiesTextEdit MaxLength="8">
                            <ValidationSettings>
                                    <RequiredField ErrorText="Preencha a faixa inicial." IsRequired="True" />
                                    <RegularExpression ErrorText="Preencha corretamente o cep." 
                                        ValidationExpression="^\d{8}?$" />
                                </ValidationSettings>
                            </PropertiesTextEdit>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Faixa final" FieldName="faixaFinal" 
                            VisibleIndex="4" Width="90px">
                            <PropertiesTextEdit MaxLength="8">
                                <ValidationSettings SetFocusOnError="True">
                                    <RequiredField ErrorText="Preencha a faixa final." IsRequired="True" />
                                    <RegularExpression ErrorText="Preencha corretamente o cep." 
                                        ValidationExpression="^\d{8}?$" />
                                </ValidationSettings>
                            </PropertiesTextEdit>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Valor inicial" FieldName="valorInicial" 
                            VisibleIndex="5" Width="60px">
                            <PropertiesTextEdit DisplayFormatString="C">
                                <ValidationSettings SetFocusOnError="True">
                                    <RegularExpression ErrorText="Preencha apenas com números." 
                                        ValidationExpression="^[0-9]+$" />
                                    <RequiredField ErrorText="Preencha o prazo de entrega." IsRequired="True" />
                                </ValidationSettings>
                            </PropertiesTextEdit>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtValorInicial" runat="server" CssClass="campos" 
                                    Text='<%# Eval("valorInicial", "{0:C}").Replace("R$","") %>' Width="100%"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rqvpreco" runat="server" 
                                    ControlToValidate="txtValorInicial" Display="Dynamic" 
                                    ErrorMessage="Preencha o valor inicial." Font-Size="10px" 
                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                            </EditItemTemplate>
                        </dxwgv:GridViewDataTextColumn>
                         <dxwgv:GridViewDataTextColumn Caption="Valor final" FieldName="valorFinal" 
                            VisibleIndex="6" Width="60px">
                            <PropertiesTextEdit DisplayFormatString="C">
                                <ValidationSettings SetFocusOnError="True">
                                    <RegularExpression ErrorText="Preencha apenas com números, virgula ou ponto." 
                                        ValidationExpression="^[\-]{0,1}[0-9]{1,}(([\.\,]{0,1}[0-9]{1,})|([0-9]{0,}))$" />
                                    <RequiredField ErrorText="Preencha o desconto." IsRequired="True" />
                                </ValidationSettings>
                            </PropertiesTextEdit>
                             <EditItemTemplate>
                                 <asp:TextBox ID="txtValorFinal" runat="server" CssClass="campos" 
                                     Text='<%# Eval("valorFinal", "{0:C}").Replace("R$","") %>' Width="100%"></asp:TextBox>
                                 <asp:RequiredFieldValidator ID="rqvpreco" runat="server" 
                                     ControlToValidate="txtValorFinal" Display="Dynamic" 
                                     ErrorMessage="Preencha o valor final." Font-Size="10px" SetFocusOnError="True"></asp:RequiredFieldValidator>
                             </EditItemTemplate>
                        </dxwgv:GridViewDataTextColumn>
                         <dxwgv:GridViewDataTextColumn Caption="Desconto" FieldName="porcentagemDeDesconto" 
                            VisibleIndex="7" Width="60px">
                            <PropertiesTextEdit MaxLength="4">
                                <ValidationSettings SetFocusOnError="True">
                                    <RegularExpression ErrorText="Preencha apenas com números, virgula ou ponto." 
                                        ValidationExpression="^[\-]{0,1}[0-9]{1,}(([\.\,]{0,1}[0-9]{1,})|([0-9]{0,}))$" />
                                    <RequiredField ErrorText="Preencha o seguro." IsRequired="True" />
                                </ValidationSettings>
                            </PropertiesTextEdit>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewCommandColumn VisibleIndex="8" Width="90px" ButtonType="Image">
                            <editbutton visible="True" Text="Editar">
                                <Image Url="~/admin/images/btEditar.jpg" />
                            </editbutton>
                            <NewButton Visible="True" Text="Novo">
                                <Image Url="~/admin/images/btNovo.jpg" />
                            </NewButton>
                            <deletebutton visible="True" Text="Excluir">
                                <Image Url="~/admin/images/btExcluir.jpg" />
                            </deletebutton>
                            <CancelButton Text="Cancelar">
                                <Image Url="~/admin/images/btCancelarIcon.jpg" />
                            </CancelButton>
                            <UpdateButton Text="Salvar">
                                <Image Url="~/admin/images/btSalvarIcon.jpg" />
                            </UpdateButton>
                            <ClearFilterButton Visible="True" Text="Limpar filtro">
                                <Image Url="~/admin/images/btLimparFiltro.jpg" />
                            </ClearFilterButton>
                            <HeaderTemplate>
                                <img alt="" src="images/legendaIcones.jpg" />
                            </HeaderTemplate>
                        </dxwgv:GridViewCommandColumn>
                    </Columns>
                    <StylesEditors>
                        <Label Font-Bold="True">
                        </Label>
                    </StylesEditors>
                </dxwgv:aspxgridview>
<%--                <dxwgv:aspxgridviewexporter ID="grdEx" runat="server" FileName="fornecedores" 
                    GridViewID="grd" Landscape="True" PreserveGroupRowStates="True">
                </dxwgv:aspxgridviewexporter>--%>
                <asp:LinqDataSource ID="sqlTipoDeEntrega" runat="server" 
                    ContextTypeName="dbCommerceDataContext" TableName="tbTipoDeEntregas">
                </asp:LinqDataSource>
                <asp:LinqDataSource ID="sqlFaixaDeCepComDesconto" runat="server" 
                    ContextTypeName="dbCommerceDataContext" EnableDelete="True" 
                    EnableInsert="True" EnableUpdate="True" TableName="tbFaixaDeCepDescontos">
                </asp:LinqDataSource>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
    </table>
        </td>
    </tr>
</table>
    </asp:Content>
