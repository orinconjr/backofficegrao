﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="inutilizarNota.aspx.cs" Inherits="admin_inutilizarNota" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" runat="Server">
    <style>
        .meugrid {
            background-color: #F4FAFB;
            border: 0px none;
            /* border-collapse: separate !important; */
            overflow: hidden;
            font: 9pt Tahoma;
            color: #000;
            cursor: pointer;
            white-space: nowrap;
            font-weight: normal;
            text-align: left;
            width: 834px;
            border-collapse: collapse;
        }

            .meugrid th {
                background-image: url(../../App_Themes/Glass/GridView/gvHeaderBackground.gif);
                background-position: center top;
                background-repeat: repeat-x;
                background-color: #C0DDE0;
                overflow: hidden;
                height: 23px;
                line-height: 23px;
                padding: 4px 6px 5px;
                font-weight: normal;
                border: 1px solid #7EACB1;
                /* border-collapse: separate;*/
            }

            .meugrid td {
                overflow: hidden;
                height: 23px;
                line-height: 23px;
                padding: 4px 6px 5px;
                border: 1px solid #7EACB1;
            }

            .meugrid tr td:nth-child(4) {
                text-align: center;
            }

            .meugrid tr td:last-of-type {
                text-align: center;
            }

        .tabrotulos {
            width: 848px;
            margin: auto;
        }

        .materiaprima {
            width: 250px;
        }

            .materiaprima ul {
                margin: 0px;
                padding-left: 10px;
            }

                .materiaprima ul li {
                    max-width: 230px;
                    white-space: pre-wrap;
                }

        .meugrid tr td:last-of-type {
            text-align: center;
        }

        .nome {
            width: 237px;
        }

            .nome div {
                /* max-width: 230px;*/
                white-space: pre-wrap;
            }

        .rotulos td {
            font: bold 12px tahoma;
            padding-top: 20px;
            color: rgb(65, 105, 126);
            padding-left: 5px;
            padding-bottom: 5px;
        }

        .btnInutilizar {
            border-radius: 3px;
            width: 70px;
            height: 23px;
            background-color: #e41800;
            text-align: center;
            line-height: 23px;
            color: #fff;
            font: bold 12px tahoma;
        }
    </style>
    <table cellpadding="0" cellspacing="0" style="width: 920px">
        <tr>
            <td class="tituloPaginas" valign="top">
                <asp:Label ID="lblAcao" runat="server">Inutilização de Nota Fiscal</asp:Label>
            </td>
        </tr>
        <tr>
            <td>

                <div style="width: 100%; height: auto; float: left; clear: left; margin-left: 45px; font: normal 12px tahoma; margin-bottom: 10px;">
                    <div style="float: left; clear: left; width: 745px; height: 30px;">
                        Nota inicial:
                        <asp:TextBox ID="txtnotainicial" runat="server" Width="150px" />&nbsp;&nbsp;&nbsp;
                         Nota final:
                        <asp:TextBox ID="txtnotafinal" runat="server" Width="150px" />
                        &nbsp;&nbsp;
                        CNPJ:<asp:DropDownList ID="DropDownList1" runat="server" DataTextField="nome" DataValueField="cnpj" />
                    </div>
                    <div style="float: left; width: 74px; height: 30px;">
                        <asp:Button ID="btnInutilizar" CssClass="btnInutilizar" ValidationGroup="a" runat="server" Text="Inutilizar" OnClick="btnInutilizar_Click" />

                    </div>
                    <div style="float: left; clear: left; width: 340px; height: 30px;">
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Digite o número da nota inicial" ValidationGroup="a" ControlToValidate="txtnotainicial"></asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Digite o número da nota final" ValidationGroup="a" ControlToValidate="txtnotafinal"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Digite apenas números" ControlToValidate="txtnotainicial" ValidationExpression="^([0-9]*)$" ValidationGroup="a"></asp:RegularExpressionValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="Digite apenas números" ControlToValidate="txtnotafinal" ValidationExpression="^([0-9]*)$" ValidationGroup="a"></asp:RegularExpressionValidator>

                    </div>


                </div>

            </td>
        </tr>
        <tr>
            <td>
                <div style="width: 100%; height: auto; float: left; clear: left;">
                    <div style="width: 100%; height: auto; float: left; clear: left; padding-left: 45px; font: normal 12px tahoma; margin-bottom: 10px;">
                        <asp:Label ID="lblprodutofilho" runat="server" Text=""></asp:Label>
                    </div>
                    <div id="containergrid" runat="server" style="float: left; clear: left; display: none;">
                        <table class="meugrid" align="center" id="meugrid">

                            <tr>
                                <th style="width: 50px;">Id Produto Pai
                                </th>
                                <th>Produto
                                </th>
                                <th style="width: 50px; text-align: center;">Editar
                                </th>

                            </tr>
                            <asp:ListView runat="server" ID="lstProdutos">
                                <ItemTemplate>
                                    <tr>
                                        <asp:HiddenField runat="server" ID="hdfProdutoId" Value='<%# Eval("produtoId") %>' />

                                        <td>
                                            <div><%# Eval("produtoId") %></div>
                                        </td>
                                        <td class="nome">
                                            <div><%# Eval("produtoNome") %></div>
                                        </td>


                                        <td align="center">
                                            <a class="dxeHyperlink_Glass" href="produtoalt.aspx?produtoId=<%# Eval("produtoId") %>&produtoPaiId=<%# Eval("produtoId") %>&acao=alterar" target="_blank">
                                                <img src="images/btEditar.png" />
                                            </a>
                                        </td>

                                    </tr>

                                </ItemTemplate>
                            </asp:ListView>
                        </table>

                    </div>


                </div>


            </td>
        </tr>

        <tr>
            <td class="tituloPaginas" valign="top">
                <asp:Label ID="Label1" runat="server">Cancelamento de Nota Fiscal</asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <div style="width: 100%; height: auto; float: left; clear: left; margin-left: 45px; font: normal 12px tahoma; margin-bottom: 10px;">
                    <div style="float: left; clear: left; width: 745px; height: 30px;">
                        Nota:
                        <asp:TextBox ID="txtNumeroNotaCancelar" runat="server" Width="150px" ValidationGroup="cancelamento" />&nbsp;&nbsp;&nbsp;
                    CNPJ:<asp:DropDownList ID="ddlNotaCnpjCancelar" runat="server" DataTextField="nome" DataValueField="cnpj" />
                        <asp:Button ID="btnCancelarNota" CssClass="btnInutilizar" runat="server" Text="Cancelar" OnClick="btnCancelarNota_OnClick" OnClientClick="return confirm('Confirma o cancelamento da nota?')" ValidationGroup="cancelamento" />
                        <br/>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Digite o número da nota" ValidationGroup="cancelamento" ControlToValidate="txtNumeroNotaCancelar"></asp:RequiredFieldValidator>
                        <br/>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ErrorMessage="Digite apenas números" ControlToValidate="txtNumeroNotaCancelar" ValidationExpression="^([0-9]*)$" ValidationGroup="cancelamento"></asp:RegularExpressionValidator>
                    </div>
                </div>
            </td>
        </tr>
    </table>
</asp:Content>

