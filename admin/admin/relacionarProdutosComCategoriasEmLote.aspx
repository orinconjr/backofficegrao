﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="relacionarProdutosComCategoriasEmLote.aspx.cs" Inherits="admin_relacionarProdutosComCategoriasEmLote" Theme="Glass" %>


<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dxwgv" %>


<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" Runat="Server">
    
    <script type="text/javascript">
        function toggle() {
            var checadoTodos = true;
            var totalChecados = 0;
            var totalNaoChecados = 0;
            var divs = document.querySelectorAll("[id]");
            for (var i = 0, len = divs.length; i < len; i++) {
                var div = divs[i];
                if (div.id.indexOf("ckbSelecionar") > -1) {
                    if (div.checked) totalChecados++;
                    else totalNaoChecados++;
                }
            }

            for (var i = 0, len = divs.length; i < len; i++) {
                var div = divs[i];
                if (div.id.indexOf("ckbSelecionar") > -1) {
                    if (totalChecados < totalNaoChecados) div.checked = true;
                    else div.checked = false;
                }
            }
        }
    </script>

    <table cellpadding="0" cellspacing="0" style="width: 920px">
    <tr>
        <td class="tituloPaginas" valign="top">
            <asp:Label ID="lblAcao" runat="server">Relacionar produtos com categorias</asp:Label>
            </td>
    </tr>
    <tr>
        <td>
            
    <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
        <tr>
            <td align="right">
                <table cellpadding="0" cellspacing="0" style="width: 834px">
                    <tr>
                        <td align="right">
                            &nbsp;</td>
                        <td height="38" 
                            style="background-image: url('images/bkExportar.jpg'); padding-bottom: 1px;" 
                            valign="bottom" width="231">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="width: 97px">
                                        &nbsp;</td>
                                    <td style="width: 33px">
                                        <asp:ImageButton ID="btPdf" runat="server" ImageUrl="~/admin/images/btPdf.jpg" 
                                            style="margin-left: 5px" />
                                    </td>
                                    <td style="width: 31px">
                                        <asp:ImageButton ID="btXsl" runat="server" ImageUrl="~/admin/images/btXls.jpg" 
                                            style="margin-left: 5px" />
                                    </td>
                                    <td style="width: 36px">
                                        <asp:ImageButton ID="btRtf" runat="server" ImageUrl="~/admin/images/btRtf.jpg" 
                                            style="margin-left: 5px" />
                                    </td>
                                    <td valign="bottom">
                                        <asp:ImageButton ID="btCsv" runat="server" ImageUrl="~/admin/images/btCsv.jpg" 
                                            style="margin-left: 5px" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td style="font-weight: bold; padding-bottom: 15px; padding-left: 10px;" class="rotulos">
                Site:<br/>
                <asp:DropDownList runat="server" ID="ddlSite" DataTextField="nome" DataValueField="idSite" DataSourceID="sqlSite" AutoPostBack="True" OnSelectedIndexChanged="ddlSite_OnSelectedIndexChanged" />
            </td>
        </tr>
        <tr>
            <td>
                <table cellpadding="0" cellspacing="0" style="width: 834px">
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td style="text-align: right; padding-top: 15px;">
                            <asp:Button runat="server" OnClientClick="toggle(); return false;" Text="Checar todos" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 220px" valign="top">
                             <asp:TreeView ID="treeCategorias" runat="server" 
                                 ShowLines="True" ontreenodepopulate="treeCategorias_TreeNodePopulate" Font-Names="Tahoma" 
                                                    Font-Size="12px" ForeColor="Black" ExpandDepth="30" 
                                                    onprerender="treeCategorias_PreRender" ShowCheckBoxes="All" onselectednodechanged="treeCategorias_SelectedNodeChanged">
                                 <HoverNodeStyle Font-Underline="True" />
                                 <SelectedNodeStyle BackColor="#D2ECEC" />
                                 <Nodes>
                                     <asp:TreeNode Text="Categorias" Value="-1" Checked="True" Expanded="True" 
                                         Selected="True"></asp:TreeNode>
                                 </Nodes>
                            </asp:TreeView>
                        </td>
                        <td valign="top" align="right">
                            <dxwgv:ASPxGridView ID="grd1" runat="server" AutoGenerateColumns="False" 
                    DataSourceID="sqlProdutos" KeyFieldName="produtoId" 
                    Cursor="auto" oncustomcallback="grd1_CustomCallback" OnHtmlRowCreated="grd1_HtmlRowCreated" OnHtmlRowPrepared="grd1_HtmlRowPrepared"
                        ClientInstanceName="grd1" Width="834px" EnableCallBacks="False">
                        <SettingsBehavior AutoFilterRowInputDelay="10200" ConfirmDelete="True" 
                            AutoExpandAllGroups="True" ColumnResizeMode="Control" />
                        <SettingsText ConfirmDelete="Tem certeza que deseja excluir?" 
                        emptydatarow="Nenhum registro encontrado." 
                        GroupPanel="Arraste uma coluna aqui para agrupar." />
                        <SettingsPager Position="TopAndBottom" PageSize="100" 
                        ShowDisabledButtons="False" AlwaysShowPager="True">
                            <Summary AllPagesText="Páginas: {0} - {1} ({2} items)" 
                            Text="Página {0} de {1} ({2} registros encontrados)" />
                        </SettingsPager>
                        <settings showfilterrow="True" ShowGroupedColumns="True" />
                        <SettingsEditing EditFormColumnCount="4" 
                        PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1" 
                        PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter" 
                        PopupEditFormWidth="700px" Mode="Inline" />
                        <Columns>
                        <dxwgv:GridViewDataTextColumn Width="100px" Caption="Id do produto" VisibleIndex="0">
                            <DataItemTemplate>
                                <asp:TextBox ID="txtProdutoId" runat="server" BorderStyle="None" 
                                    CssClass="rotulos" Text='<%# Bind("produtoId") %>' Width="100px"></asp:TextBox>
                                <asp:TextBox ID="txtProdutoPaiId" runat="server" BorderStyle="None" 
                                    CssClass="rotulos" Text='<%# Bind("produtoPaiId") %>' Visible="False" 
                                    Width="100px"></asp:TextBox>
                            </DataItemTemplate>
                        </dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataTextColumn Caption="ID da Empresa" 
                                FieldName="produtoIdDaEmpresa" Name="idDaEmpresa" VisibleIndex="1" 
                                Width="100px">
                            </dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataTextColumn Caption="Nome" FieldName="produtoNome" 
                                VisibleIndex="2" Width="210px">				
                                <Settings AutoFilterCondition="Contains" />
                                <DataItemTemplate>
                                    <asp:TextBox ID="txtIdDaEmpresa0" runat="server" BorderStyle="None" CssClass="campos" 
                                        Text='<%# Bind("produtoNome") %>' Width="100%"></asp:TextBox>
                                </DataItemTemplate>
                            </dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataTextColumn Caption="Filtros" VisibleIndex="2" Width="210px" Name="filtros">		
                                <DataItemTemplate>
                                    <asp:HiddenField runat="server" ID="hiddenProdutoId" Value='<%# Bind("produtoId") %>'/>
                                    <asp:ListView runat="server" ID="lstProdutos">
                                        <ItemTemplate>
                                            <%# Eval("categoriaNomeExibicao") %>
                                            <a style="cursor: pointer;" onclick="javacript:excluiCategoria(<%# Eval("produtoId") %>, <%# Eval("categoriaId") %>);">X</a>&nbsp;&nbsp;&nbsp;
                                        </ItemTemplate>
                                    </asp:ListView>
                                </DataItemTemplate>
                            </dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataTextColumn Caption="Fornecedor" FieldName="fornecedorNome" 
                                VisibleIndex="4" Width="160px">
                            </dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataTextColumn Caption="Ativo" FieldName="produtoAtivo" Visible="True"
                                UnboundType="Boolean" VisibleIndex="5" Width="60px">
                                <DataItemTemplate>
                                    <dxe:ASPxCheckBox ID="ckbAtivo" runat="server"
                                        Value='<%# Bind("produtoAtivo") %>' ValueChecked="True"
                                        ValueType="System.String" ValueUnchecked="False">
                                    </dxe:ASPxCheckBox>
                                </DataItemTemplate>
                            </dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataTextColumn Caption="Selecionar" 
                                VisibleIndex="5" Width="50px" Name="selecionar">
                                <DataItemTemplate>
                                    <asp:CheckBox runat="server" ID="ckbSelecionar" />
                                </DataItemTemplate>
                                <CellStyle HorizontalAlign="Center">
                                </CellStyle>
                            </dxwgv:GridViewDataTextColumn>
                        </Columns>
                    </dxwgv:ASPxGridView>
                    
                <asp:ObjectDataSource ID="sqlProdutoCategorias" runat="server" 
                    SelectMethod="produtoAdminSeleciona" TypeName="rnProdutos" 
                    DeleteMethod="produtoExclui">
                    <DeleteParameters>
                        <asp:Parameter Name="produtoId" Type="String" />
                    </DeleteParameters>
                    <SelectParameters>
                        <asp:ControlParameter ControlID="treeCategorias" Name="categoriaId" 
                            PropertyName="SelectedValue" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            <img src="images/btSalvar.jpg" onclick="grd1.PerformCallback(this.value);" 
                                style="margin-top: 16px;"/>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                    
            </td>
        </tr>
        <tr>
            <td align="right;">
                <script>
                    function excluiCategoria(produto, categoria) {
                        var r = confirm("Deseja realmente remover esta categoria?");
                        if (r == true) {
                            document.getElementById("ctl00_conteudoPrincipal_hiddenProdutoId").value = produto;
                            document.getElementById("ctl00_conteudoPrincipal_hiddenCategoriaId").value = categoria;
                            document.getElementById("ctl00_conteudoPrincipal_btnExcluir").click();
                            return false;
                        }
                        
                    }
                </script>
                <div style="display: none;">
                <asp:Button ID="btnExcluir" runat="server" Text="Excluir" OnClick="btnExcluir_Click" />
                <asp:HiddenField ID="hiddenProdutoId" runat="server" />
                <asp:HiddenField ID="hiddenCategoriaId" runat="server" />
                </div>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td align="right">
            </td>
        </tr>
        <tr>
            <td>
<%--                <dxwgv:ASPxGridViewExporter ID="grdEx" runat="server" FileName="fornecedores" 
                    GridViewID="grd" Landscape="True" PreserveGroupRowStates="True">
                </dxwgv:ASPxGridViewExporter>--%>
                <asp:ObjectDataSource ID="sqlProdutos" runat="server" 
                    SelectMethod="produtoAdminSelecionaAtivos" TypeName="rnProdutos" 
                    DeleteMethod="produtoExclui">
                    <SelectParameters>                        
                        <asp:ControlParameter ControlID="treeCategorias" Name="categoriaId" PropertyName="SelectedValue" Type="String" />
                        <asp:ControlParameter ControlID="ddlSite" Name="idSite" PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:LinqDataSource ID="sqlSite" runat="server" 
                    ContextTypeName="dbCommerceDataContext" EnableDelete="True" 
                    EnableInsert="True" EnableUpdate="True" TableName="tbSites">
                </asp:LinqDataSource>
            </td>
        </tr>
    </table>
        </td>
    </tr>
</table>
    </asp:Content>
