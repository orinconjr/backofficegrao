﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using SpreadsheetLight;
using System.Web.UI.HtmlControls;
public partial class admin_produtosNaoVendidos : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            carregaDdlFornecedor();
            carregaDdlMarca();
           // carregaGrid();
        }

    }
    void carregaGrid()
    {
        DataTable table = getDados();
        if(table.Rows.Count == 0)
        {
            grdprodnaovendidos.DataSource = null;
        }
        else
        {
            grdprodnaovendidos.DataSource = table;
        }
        grdprodnaovendidos.DataBind();
        lblitensencontrados.Text = table.Rows.Count.ToString();
    }
    void carregaDdlFornecedor()
    {
        ddlfornecedor.Items.Clear();
        ddlfornecedor.Items.Add(new ListItem("Todos","0" ));
        var data = new dbCommerceDataContext();
        var fornecedores = (from c in data.tbProdutoFornecedors
                            select new
                            {
                                c.fornecedorId,
                                c.fornecedorNome
                            }).ToList().Distinct().OrderBy(x=>x.fornecedorNome);
        foreach(var fornecedor in fornecedores)
        {
            ddlfornecedor.Items.Add(new ListItem(fornecedor.fornecedorNome, fornecedor.fornecedorId.ToString()));
        }
    }

    void carregaDdlMarca()
    {
        ddlMarca.Items.Clear();
        ddlMarca.Items.Add(new ListItem("Todos", "0"));
        var data = new dbCommerceDataContext();
        var Marcas = (from c in data.tbMarcas
                       select new
                       {
                           c.marcaId,
                           c.marcaNome
                       }).ToList().Distinct().OrderBy(x => x.marcaNome);
        foreach (var Marca in Marcas)
        {
            ddlMarca.Items.Add(new ListItem(Marca.marcaNome, Marca.marcaId.ToString()));
        }
    }
    protected void grdprodnaovendidos_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grdprodnaovendidos.PageIndex = e.NewPageIndex;

        carregaGrid();
    }

    protected void btnFiltrar_Click(object sender, EventArgs e)
    {
        if (validarcampos())
            carregaGrid();
    }

    DataTable getDados()
    {
        string connectionString = ConfigurationManager.ConnectionStrings["connectionString"].ToString();
        DataTable table = new DataTable();
        using (
                SqlConnection conn =
                    new SqlConnection(connectionString))
        {
            SqlCommand command = conn.CreateCommand();
            command.Connection.Open();
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = "produtosNaoVendidos2";
            
            DateTime dataInicial = Convert.ToDateTime(txtDataInicial.Text);
            command.Parameters.AddWithValue("@dataInicial", dataInicial.Year+"-"+dataInicial.Month+"-"+dataInicial.Day);
            
            DateTime dataFinal = Convert.ToDateTime(txtDataFinal.Text);
            command.Parameters.AddWithValue("@dataFinal", dataFinal.Year + "-" + dataFinal.Month + "-" + dataFinal.Day);
           
            string qtdMaxima = txtQtdMaxima.Text;
            qtdMaxima = String.IsNullOrEmpty(qtdMaxima) ? "0" : qtdMaxima;

            string produtoId = txtProdutoId.Text;
            produtoId = String.IsNullOrEmpty(produtoId) ? "0" : produtoId;

            string produtoPrecoInicial = txtPrecoDe.Text.Replace(",",".");
            produtoPrecoInicial = String.IsNullOrEmpty(produtoPrecoInicial) ? "0" : produtoPrecoInicial;

            string produtoPrecoFinal = txtPrecoAte.Text.Replace(",", ".");
            produtoPrecoFinal = String.IsNullOrEmpty(produtoPrecoFinal) ? "0" : produtoPrecoFinal;

            string produtoPrecoPromocionalInicial = txtPrecoPromocionalDe.Text.Replace(",", ".");
            produtoPrecoPromocionalInicial = String.IsNullOrEmpty(produtoPrecoPromocionalInicial) ? "0" : produtoPrecoPromocionalInicial;

            string produtoPrecoPromocionalFinal = txtPrecoPromocionalAte.Text.Replace(",", ".");
            produtoPrecoPromocionalFinal = String.IsNullOrEmpty(produtoPrecoPromocionalFinal) ? "0" : produtoPrecoPromocionalFinal;

            string produtoAtivo = "2";
            if (rbAtivoSim.Checked)
                produtoAtivo = "1";
            if (rbAtivoNao.Checked)
                produtoAtivo = "0";

            command.Parameters.AddWithValue("@qtdMaxima", qtdMaxima);
            command.Parameters.AddWithValue("@produtoFornecedor", ddlfornecedor.SelectedItem.Value);
            command.Parameters.AddWithValue("@produtoId", produtoId);
            command.Parameters.AddWithValue("@produtoNome", "%"+txtProdutoNome.Text+"%");
            command.Parameters.AddWithValue("@produtoAtivo", produtoAtivo);
            command.Parameters.AddWithValue("@produtoMarca", ddlMarca.SelectedItem.Value);
            command.Parameters.AddWithValue("@produtoPrecoInicial", produtoPrecoInicial);
            command.Parameters.AddWithValue("@produtoPrecoFinal", produtoPrecoFinal);
            command.Parameters.AddWithValue("@produtoPrecoPromocionalInicial", produtoPrecoPromocionalInicial);
            command.Parameters.AddWithValue("@produtoPrecoPromocionalFinal", produtoPrecoPromocionalFinal);
            command.Parameters.AddWithValue("@ordenacao", ddlordenarpor.SelectedItem.Value);

            SqlDataReader reader = command.ExecuteReader(CommandBehavior.CloseConnection);
            
            table.Load(reader);
        }
        return table;
    }
    bool validarcampos()
    {
        string erro = "";
        bool valido = true;
        if (!String.IsNullOrEmpty(txtQtdMaxima.Text))
        {
            try { int teste = Convert.ToInt32(txtQtdMaxima.Text); }
            catch (Exception)
            {
                erro += @"Quantidade Máxima deve ser um número\n";
                valido = false;
            }
        }

        if (!String.IsNullOrEmpty(txtPrecoDe.Text))
        {
            try { decimal teste = Convert.ToDecimal(txtPrecoDe.Text); }
            catch (Exception)
            {
                erro += @"Preço de deve ser um número\n";
                valido = false;
            }
        }
        if (!String.IsNullOrEmpty(txtPrecoAte.Text))
        {
            try { decimal teste = Convert.ToDecimal(txtPrecoAte.Text); }
            catch (Exception)
            {
                erro += @"Preço até deve ser um número\n";
                valido = false;
            }
        }

        if (!String.IsNullOrEmpty(txtPrecoPromocionalDe.Text))
        {
            try { decimal teste = Convert.ToDecimal(txtPrecoPromocionalDe.Text); }
            catch (Exception)
            {
                erro += @"Preço Promociona de deve ser um número\n";
                valido = false;
            }
        }
        if (!String.IsNullOrEmpty(txtPrecoPromocionalAte.Text))
        {
            try { decimal teste = Convert.ToDecimal(txtPrecoPromocionalAte.Text); }
            catch (Exception)
            {
                erro += @"Preço Promociona até deve ser um número\n";
                valido = false;
            }
        }
        if (!String.IsNullOrEmpty(txtProdutoId.Text))
        {
            try { int teste = Convert.ToInt32(txtProdutoId.Text); }
            catch (Exception)
            {
                erro += @"O ID do Produto deve ser um número\n";
                valido = false;
            }
        }
        if (!String.IsNullOrEmpty(txtDataInicial.Text))
        {
            try { DateTime teste = Convert.ToDateTime(txtDataInicial.Text); }
            catch (Exception)
            {
                erro += @"Data inicial invalida\n";
                valido = false;
            }
        }
        else
        {
            erro += @"Digite a Data inicial\n";
            valido = false;
        }

        if (!String.IsNullOrEmpty(txtDataFinal.Text))
        {
            try { DateTime teste = Convert.ToDateTime(txtDataFinal.Text); }
            catch (Exception)
            {
                erro += @"Data final invalida\n";
                valido = false;
            }
        }
        else
        {
            erro += @"Digite a Data final\n";
            valido = false;
        }


        if (erro != "")
        {
            string meuscript = @"alert('" + erro + "');";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "MyScript", meuscript, true);

        }
        return valido;
    }
    protected void btnExportarPlanilha_Click(object sender, EventArgs e)
    {
        if (validarcampos())
        {
            DataTable itens = getDados();
            SLDocument sl = new SLDocument();
            SLStyle style1 = sl.CreateStyle();
            style1.Font.Bold = true;
            sl.SetRowStyle(1, style1);

            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("Content-Disposition", "attachment; filename=Produtos_Nao_Vendidos.xlsx");

            sl.SetCellValue("A1", "Id do Produto");
            sl.SetCellValue("B1", "Produto");
            sl.SetCellValue("C1", "Fornecedor");
            sl.SetCellValue("D1", "Estoque");
            sl.SetCellValue("E1", "Itens Vendidos");
            sl.SetCellValue("F1", "Criação do Produto");
            sl.SetCellValue("G1", "Preço de Custo");
            sl.SetCellValue("H1", "Margem a Vista");
            sl.SetCellValue("I1", "Margem a Prazo");
            sl.SetCellValue("J1", "Marca");
            sl.SetCellValue("K1", "Preço");
            sl.SetCellValue("L1", "Preço Promocional");
            sl.SetCellValue("M1", "Ativo");


            int linha = 2;
            foreach(DataRow item in  itens.Rows)
            {
                sl.SetCellValue(linha, 1, item["produtoId"].ToString());
                sl.SetCellValue(linha, 2, item["produtoNome"].ToString());
                sl.SetCellValue(linha, 3, item["fornecedorNome"].ToString());
                sl.SetCellValue(linha, 4, item["produtoEstoqueAtual"].ToString());

                string qtdVendas = "0";
                if (!String.IsNullOrEmpty(item["quantidadeVendas"].ToString()))
                    qtdVendas = item["quantidadeVendas"].ToString();
                sl.SetCellValue(linha, 5, qtdVendas);
                sl.SetCellValue(linha, 6, item["dataDaCriacao"].ToString());

                Decimal produtoPrecoDeCusto = 0;
                try { produtoPrecoDeCusto = Convert.ToDecimal(item["produtoPrecoDeCusto"].ToString().Replace(".", ",")); }
                catch (Exception) { }

                Decimal produtoPreco = 0;
                try { produtoPreco = Convert.ToDecimal(item["produtoPreco"].ToString().Replace(".", ",")); }
                catch (Exception) { }

                Decimal produtoPrecoPromocional = 0;
                try { produtoPrecoPromocional = Convert.ToDecimal(item["produtoPrecoPromocional"].ToString().Replace(".", ",")); }
                catch (Exception) { }

                sl.SetCellValue(linha, 7, produtoPrecoDeCusto.ToString("C"));
                sl.SetCellValue(linha, 8, item["margemavista"].ToString());
                sl.SetCellValue(linha, 9, item["margem"].ToString());
                sl.SetCellValue(linha,10, item["marcaNome"].ToString());
                sl.SetCellValue(linha,11, produtoPreco.ToString("C"));
                sl.SetCellValue(linha,12, produtoPrecoPromocional.ToString("C"));
                sl.SetCellValue(linha,13, item["produtoAtivo"].ToString().ToLower() == "true" ? "Sim" : "Não");
                linha++;
            }

            sl.SaveAs(Response.OutputStream);
            Response.End();
        }

    }
    protected void grdprodnaovendidos_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            string quantidadeVendas = DataBinder.Eval(e.Row.DataItem, "quantidadeVendas").ToString();
            if (String.IsNullOrEmpty(quantidadeVendas))
                quantidadeVendas = "0";
            Label lblQtdVendas = (Label)e.Row.FindControl("lblQtdVendas");
            lblQtdVendas.Text = quantidadeVendas;

            HtmlAnchor linkeditarpedido = (HtmlAnchor)e.Row.FindControl("linkeditarproduto");
            linkeditarpedido.HRef = "produtoAlt.aspx?produtoId=" + DataBinder.Eval(e.Row.DataItem, "produtoId")+"&produtoPaiId=0&acao=alterar";

        }
    }
}