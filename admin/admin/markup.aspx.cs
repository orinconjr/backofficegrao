﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

public partial class admin_markup : System.Web.UI.Page
{

    public class meusprodutos
    {
        public int produtoId { get; set; }
        public string produtoNome { get; set; }
        public string fotoDestaque { get; set; }
        public string produtoAtivo { get; set; }
        public decimal? somaMateriaPrima { get; set; }
        public decimal? somaProcessos { get; set; }
        public decimal? somaProcessosExternos { get; set; }
        public decimal? indiceMargemDeLucro { get; set; }
        public decimal? indiceTributos { get; set; }
        public IEnumerable<materiaPrima> materiasPrimas { get; set; }
        public IEnumerable<processo> processos { get; set; }
        public IEnumerable<processoExterno> processosExternos { get; set; }

    }


    public class materiaPrima
    {
        //public int produtoId { get; set; }
        //public int idComprasProduto { get; set; }
        //public int idProdutoMateriaPrima { get; set; }
        public string produtoNome { get; set; }
        //public decimal consumo { get; set; }
        //public decimal valorUnitario { get; set; }
        public decimal? valorTotal { get; set; }
        //public decimal totalGeral { get; set; }

    }

    public class processo
    {
        public decimal? valorTotal { get; set; }
    }
    public class processoExterno
    {
        public decimal? valorTotal { get; set; }
    }

    public class minhasDespesasGerais
    {
        public string nomeDespesasGerais { get; set; }
        public decimal? porcentagemDespesasGerais { get; set; }
        public decimal? valorDespesasGerais { get; set; }
        //public decimal? totalPorcentagem { get; set; }
        //public decimal? totalValor { get; set; }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            using (dbCommerceDataContext data = new dbCommerceDataContext())
            {

                int produtoId = 0;
                try
                {
                    produtoId = Convert.ToInt32(Request.QueryString["produtoId"]);
                }
                catch (Exception) { }
                var produto1 = (from c in data.tbProdutos
                                where c.produtoId == produtoId
                                select new
                                {
                                    c.produtoId,
                                    c.produtoNome,
                                    c.fotoDestaque,
                                    c.indiceMargemDeLucro,
                                    c.indiceTributos
                                }).FirstOrDefault();
                //  "http://www.graodegente.com.br/fotos/" + hdfProdutoId.Value + "/" + hdfFoto.Value + ".jpg";
                imgfotodestaque.Src = "http://www.graodegente.com.br/fotos/" + produto1.produtoId + "/" + produto1.fotoDestaque + ".jpg";
                lblprodutonome.Text = produto1.produtoNome;
                txtmargemlucro.Text = produto1.indiceMargemDeLucro.ToString();
                txttributos.Text = produto1.indiceTributos.ToString();

                var produto = (from c in data.tbProdutos
                               where c.produtoId == produtoId
                               select new meusprodutos
                               {
                                   produtoId = c.produtoId,
                                   produtoNome = c.produtoNome,
                                   fotoDestaque = c.fotoDestaque,
                                   produtoAtivo = c.produtoAtivo,
                                   materiasPrimas = (from p in data.tbProdutoMateriaPrimas
                                                     join tcp in data.tbComprasProdutos
                                                         on p.idComprasProduto equals tcp.idComprasProduto
                                                     join tcpo in data.tbComprasOrdemProdutos
                                                         on p.idComprasProduto equals tcpo.idComprasProduto
                                                         into _tcpo
                                                     //////////////
                                                     join tcpf in data.tbComprasProdutoFornecedors
                                                                       on _tcpo.FirstOrDefault().idComprasProduto equals tcpf.idComprasProduto
                                                                       into _tcpf
                                                                    join tcum in data.tbComprasUnidadesDeMedidas
                                                                         on _tcpf.FirstOrDefault().idComprasUnidadeMedida equals tcum.idComprasUnidadesDeMedida
                                                                         into _tcum
                                                                    where p.produtoId == c.produtoId
                                                     //////////////
                                                    // where p.produtoId == c.produtoId
                                                     select new materiaPrima()
                                                     {
                                                         produtoNome = tcp.produto,
                                                         //valorTotal =
                                                         //    p.consumo * _tcpo.OrderByDescending(x => x.dataCadastro).FirstOrDefault().preco,
                                                         valorTotal = p.consumo * (_tcpo.OrderByDescending(x => x.dataCadastro).FirstOrDefault().preco / (_tcum.FirstOrDefault().fatorConversao ?? 1))

                                                     }).ToList<materiaPrima>(),
                                   processos = (from spf in data.tbSubProcessoFabricas
                                                join pf in data.tbProcessoFabricas on
                                                    spf.idProcessoFabrica equals pf.idProcessoFabrica
                                                join jppf in data.tbJuncaoProdutoProcessoFabricas on
                                                    new
                                                    {
                                                        idProcessoFabrica = spf.idProcessoFabrica,
                                                        idSubProcessoFabrica = spf.idSubProcessoFabrica
                                                    } equals
                                                    new
                                                    {
                                                        idProcessoFabrica = jppf.idProcessoFabrica,
                                                        idSubProcessoFabrica = jppf.idSubProcessoFabrica
                                                    }
                                                where jppf.idProduto == c.produtoId
                                                select new processo
                                                {
                                                    valorTotal = jppf.minutos * spf.valorMinuto
                                                }).ToList<processo>(),
                                   processosExternos = (from pef in data.tbProcessoExternoFabricas

                                                        join jppef in data.tbJuncaoProdutoProcessoExternoFabricas on
                                                            pef.idProcessoExternoFabrica equals jppef.idProcessoExternoFabrica

                                                        where jppef.idProduto == c.produtoId
                                                        select new processoExterno
                                                        {
                                                            valorTotal = jppef.minutos * pef.valorMinuto

                                                        })
                               });
                decimal? somaMateriaPrima = 0;
                decimal? somaProcessos = 0;
                decimal? somaProcessosExternos = 0;
                foreach (var item in produto)
                {
                    somaMateriaPrima = item.materiasPrimas.Sum(x => x.valorTotal);
                    somaProcessos = item.processos.Sum(x => x.valorTotal);
                    somaProcessosExternos = item.processosExternos.Sum(x => x.valorTotal);

                }
                lblmateriaprima.InnerText = Convert.ToDecimal(somaMateriaPrima).ToString("C");
                lblprocessosinternos.InnerText = Convert.ToDecimal(somaProcessos).ToString("C");
                lblprocessosexternos.InnerText = Convert.ToDecimal(somaProcessosExternos).ToString("C");

                //decimal? subtotal1 = somaMateriaPrima + somaProcessos + somaProcessosExternos;
                //decimal? ggf = (subtotal1 / 100) * rnConfiguracoes.GGF;

                //lblggf.InnerText = ggf.ToString();
                
                IEnumerable<minhasDespesasGerais> despesasGerais = (from dg in data.tbDespesasGerais
                    select new minhasDespesasGerais
                    {
                       nomeDespesasGerais = dg.nomeDespesasGerais,
                       porcentagemDespesasGerais= dg.porcentagemDespesasGerais,
                       valorDespesasGerais = dg.porcentagemDespesasGerais * (somaMateriaPrima / 100)
                        
                    }).ToList<minhasDespesasGerais>();

               
                rptgastosmateriaprima.DataSource = despesasGerais;
                rptgastosmateriaprima.DataBind();
                lbltotalporcentagemgastosgerais.Text = Convert.ToDecimal( despesasGerais.Sum(x => x.porcentagemDespesasGerais)).ToString();
                decimal totaldespesasgerais = Convert.ToDecimal(despesasGerais.Sum(x => x.valorDespesasGerais));
                lbltotalvalorgastosgerais.Text = Convert.ToDecimal(totaldespesasgerais).ToString("C");

                lbltotalmateriaprima.Text = (Convert.ToDecimal(somaMateriaPrima) + totaldespesasgerais).ToString("C");

                decimal? subTotalComGGF = somaMateriaPrima + totaldespesasgerais;

                decimal margemLucro = (Convert.ToDecimal(subTotalComGGF) / 100) *
                                      produto1.indiceMargemDeLucro;
                lblmargemlucro.Text = margemLucro.ToString("C");


               // item2.margemLucro = (item2.subTotalComGGF / 100) * item2.indiceMargemDeLucro;
                //Adiciona a margem de lucro
                decimal? subTotalComMargemLucro = margemLucro + subTotalComGGF;

                //Tributos
                decimal? fatordivisao = (100 - produto1.indiceTributos) / 100;
                decimal? subTotalComTributos = subTotalComMargemLucro / fatordivisao;
                decimal? soTributos = subTotalComTributos - subTotalComMargemLucro;


                lbltributos.Text = Convert.ToDecimal(soTributos).ToString("C");
                lbltotal.Text = Convert.ToDecimal(subTotalComGGF+margemLucro+soTributos).ToString("C");
            }


        }
    }


    protected void bntsalvar_Click(object sender, EventArgs e)
    {
        using (dbCommerceDataContext data = new dbCommerceDataContext())
        {

            int produtoId = 0;
            try
            {
                produtoId = Convert.ToInt32(Request.QueryString["produtoId"]);
            }
            catch (Exception)
            {
            }
            var produto1 = (from c in data.tbProdutos
                            where c.produtoId == produtoId
                            select c).FirstOrDefault();
            produto1.indiceMargemDeLucro = Convert.ToDecimal(txtmargemlucro.Text);
            produto1.indiceTributos = Convert.ToDecimal(txttributos.Text);
            data.SubmitChanges();
        }
    }

   
}