﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="comprasOrdemDetalhe.aspx.cs" Inherits="admin_comprasOrdemDetalhe" %>

<%@ Register Src="~/usrComprasOrdemCondicoes.ascx" TagPrefix="pnShowCond" TagName="usrComprasOrdemCondicoes" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .rotulos {
            color: #000000;
            font-family: Tahoma;
            font-size: 12px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div style="width: 1000px; margin-left: auto; margin-right: auto;">
            <div style="margin-bottom: 20px; text-align: center; font-size: 26px;" class="rotulos">
                <b>ORDEM DE COMPRA</b>
            </div>
            <div>
                <table style="width: 100%; background: #EEEEEE;">
                    <tr class="rotulos">
                        <td>
                            <b>Ordem Número:
                                <br />
                            </b>
                            <asp:Literal runat="server" ID="litNumero"></asp:Literal>
                        </td>
                        <td>
                            <b>Criado por:
                                <br />
                            </b>
                            <asp:Literal runat="server" ID="litUsuario"></asp:Literal>
                        </td>
                        <td>
                            <b>Data:
                                <br />
                            </b>
                            <asp:Literal runat="server" ID="litData"></asp:Literal>
                        </td>
                        <td>
                            <b>Fornecedor:<br />
                            </b>
                            <asp:Literal runat="server" ID="litFornecedor"></asp:Literal>
                        </td>
                        <td>
                            <b>Para uso em:
                                <br />
                            </b>
                            <asp:Literal runat="server" ID="litEmpresa"></asp:Literal>
                        </td>
                    </tr>
                </table>
                <br />
                <div style="width: 100%; background: #EEEEEE;"><pnShowCond:usrComprasOrdemCondicoes runat="server" ID="usrComprasOrdemCondicoes"  /></div>
            </div>
            <div style="margin-top: 20px; text-align: center; font-size: 16px;" class="rotulos">
                <b></b>
                <b>Produtos</b>
            </div>

            <div style="margin-top: 20px;">
                <table style="width: 100%; border-color: gray; border-style: solid" cellpadding="0" cellspacing="0" border="1">
                    <tr class="rotulos">
                        <td style="padding: 5px;">
                            <b>Produto</b>
                        </td>
                        <td style="text-align: center; padding: 5px;">
                            <b>Quantidade</b>
                        </td>
                        <td style="text-align: center; padding: 5px;">
                            <b>Valor Unitário</b>
                        </td>
                        <td style="text-align: right; padding: 5px;">
                            <b>Valor Total</b>
                        </td>
                    </tr>
                    <asp:ListView runat="server" ID="lstProdutos">
                        <ItemTemplate>
                            <tr class="rotulos">
                                <td style="padding: 5px;">
                                    <%#Eval("produto") %>
                                </td>
                                <td style="text-align: center; padding: 5px;">
                                    <%#Eval("quantidade") %>
                                </td>
                                <td style="text-align: center; padding: 5px;">
                                    <%# Convert.ToDecimal(Eval("preco")).ToString("C") %>
                                </td>
                                <td style="text-align: right; padding: 5px;">
                                    <%# (Convert.ToDecimal(Eval("preco")) * Convert.ToInt32(Eval("quantidade"))).ToString("C") %>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:ListView>
                    <tr class="rotulos" style="background: #EEEEEE;">
                        <td style="padding: 5px;">
                            <b>Total</b>
                        </td>
                        <td style="padding: 5px;">
                            <b></b>
                        </td>
                        <td style="padding: 5px;">
                            <b></b>
                        </td>
                        <td style="padding: 5px; text-align: right;">
                            <b>
                                <asp:Literal runat="server" ID="litTotal"></asp:Literal></b>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </form>
</body>
</html>
