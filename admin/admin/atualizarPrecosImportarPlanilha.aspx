﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="atualizarPrecosImportarPlanilha.aspx.cs" Inherits="admin_atualizarPrecosImportarPlanilha" %>

<%@ Register Assembly="DevExpress.Web.v11.1" Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxClasses" Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>
<%@ Register Assembly="DevExpress.Web.v11.1" Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" runat="Server">
    <style type="text/css">
        .btnexportar {
            float: right;
        }

        .fileUpload {
            float: left;
        }

        .fieldsetatualizarprecos {
            width: 760px;
            float: left;
            clear: left;
            height: 84px;
            border-radius: 5px;
        }

            .fieldsetatualizarprecos input[type=checkbox] {
                margin-top: 15px;
            }

            .fieldsetatualizarprecos label {
                margin-left: 5px;
                margin-right: 20px;
            }

        .atualizarprecosbotoes {
            width: 780px;
            float: left;
            clear: left;
            height: 64px;
            margin-top: 15px;
        }

            .atualizarprecosbotoes label {
                float: left;
                margin-top: 5px;
            }

        .titAlteracoes {
            float: left;
            clear: left;
            width: 100%;
            text-align: center;
            border: 1px solid;
            padding-top: 3px;
            padding-bottom: 3px;
            margin-bottom: 3px;
        }

        .linhaAteracoes {
            float: left;
            clear: left;
            width: 100%;
            padding-bottom: 2px;
            padding-top: 2px;
        }

            .linhaAteracoes:nth-child(even) {
                background-color: #dadada;
            }
    </style>

    <dx:ASPxPageControl ID="tabsPedidos" runat="server" ActiveTabIndex="1" Width="100%">
        <TabPages>
            <dx:TabPage Text="Importar planilha de preços">
                <ContentCollection>
                    <dx:ContentControl runat="server">

                        <table>
                            <tr>
                                <td class="tituloPaginas" valign="top" colspan="5">Atualizar Preços de Custo por Fornecedor
                                </td>
                            </tr>

                            <tr>
                                <td colspan="5">
                                    <fieldset class="fieldsetatualizarprecos">
                                        <legend style="font-weight: bold;">Colunas a Atualizar</legend>
                                        <asp:CheckBox ID="ckbprodutoPreco" runat="server" Text="produtoPreco" />
                                        <asp:CheckBox ID="ckbprodutoPrecoDeCusto" runat="server" Text="produtoPrecoDeCusto"/>
                                        <asp:CheckBox ID="ckbprodutoPrecoPromocional" runat="server" Text="produtoPrecoPromocional" />
                                        <asp:CheckBox ID="ckbmargemDeLucro" runat="server" Text="margemDeLucro" />
                                        <asp:CheckBox ID="ckbprodutoAtivo" runat="server" Text="produtoAtivo" />
                                        <asp:CheckBox ID="ckbprodutoPeso" runat="server" Text="produtoPeso" />
                                    </fieldset>



                                    <div class="atualizarprecosbotoes">
                                        <label>Planilha: </label>
                                        <asp:FileUpload runat="server" CssClass="fileUpload" ID="fupPlanilha" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="None"
                                            ControlToValidate="fupPlanilha" ErrorMessage="Selecione uma planilha xls ou xlsx!" ValidationGroup="tipoPlanilha">
                                        </asp:RequiredFieldValidator>
                                        <asp:Button runat="server" ID="btnImportar" CssClass="btnexportar" Text="Atualizar Banco de Dados" ValidationGroup="tipoPlanilha" OnClick="btnImportar_Click" />

                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="5" align="justify">
                                    <asp:Literal runat="server" ID="litProdutosAtualizados"></asp:Literal>
                                </td>
                            </tr>
                        </table>

                    </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>

            <dx:TabPage Text="Exportar planilha de preços">
                <ContentCollection>
                    <dx:ContentControl runat="server">
                        <table width="100%">
                            <tr>
                                <td class="tituloPaginas" colspan="4" valign="top">Exportar produtos por fornecedor (xlsx)
                                </td>
                            </tr>
                            <tr>
                                <td>Fornecedor:
                                    <asp:DropDownList ID="ddlExportarPlanilha" runat="server" AppendDataBoundItems="True" Width="175"
                                        DataSourceID="sqlFornecedores" DataTextField="fornecedorNome" DataValueField="fornecedorId">
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    <%--<asp:CheckBox ID="ckbAtivo" runat="server" Text="Ativo" />--%>
                                    <div style="width: 100%; float: left; clear: left; margin-top: 10px; margin-bottom: 10px;">
                                        <asp:RadioButton ID="rbativotodos" Checked="True" GroupName="RBAtivos" runat="server" />&nbsp;Todos
                                             <asp:RadioButton ID="rbativosomenteativos" GroupName="RBAtivos" runat="server" />&nbsp;Só os ativos
                                             <asp:RadioButton ID="rbativosomenteinativos" GroupName="RBAtivos" runat="server" />&nbsp;Só os inativos
                                    </div>

                                </td>
                                <td>
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td colspan="2" align="center">Cadastros no Período:
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="rotulos" style="width: 100px" align="center">Data inicial<br />
                                                <asp:TextBox ID="txtDataInicial" runat="server" CssClass="ll-skin-latoja campos"
                                                    Text='<%# Bind("faixaDeCepPesoInicial") %>'
                                                    Width="90px" MaxLength="10"></asp:TextBox>
                                                <b>
                                                    <asp:RegularExpressionValidator ID="rgeDataInicial" runat="server"
                                                        ControlToValidate="txtDataInicial" Display="None"
                                                        ErrorMessage="Por favor, preencha corretamente a data inicial."
                                                        SetFocusOnError="True"
                                                        ValidationExpression="^(([0-2]\d|[3][0-1])\/([0]\d|[1][0-2])\/[1-2][0-9]\d{2})$"></asp:RegularExpressionValidator>


                                                </b>
                                            </td>
                                            <td class="rotulos" style="width: 100px" align="center">Data final<br />
                                                <asp:TextBox ID="txtDataFinal" runat="server" CssClass="ll-skin-latoja campos"
                                                    Text='<%# Bind("faixaDeCepPesoFinal") %>' ValidationGroup="grpInsert"
                                                    Width="90px" MaxLength="10"></asp:TextBox>
                                                <b>
                                                    <asp:RegularExpressionValidator ID="rgeDataInicial0" runat="server"
                                                        ControlToValidate="txtDataFinal" Display="None"
                                                        ErrorMessage="Por favor, preencha corretamente a data final"
                                                        SetFocusOnError="True"
                                                        ValidationExpression="^(([0-2]\d|[3][0-1])\/([0]\d|[1][0-2])\/[1-2][0-9]\d{2})$"></asp:RegularExpressionValidator>
                                                </b>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td>
                                    <asp:Button runat="server" Text="Exportar" ID="btnExportar" OnClick="btnExportar_Click" />
                                </td>
                            </tr>
                        </table>
                    </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>

            <dx:TabPage Text="Exportar planilha de produtos">
                <ContentCollection>
                    <dx:ContentControl runat="server">
                        <table width="100%">
                            <tr>
                                <td class="tituloPaginas" colspan="4" valign="top">Exportar produtos (xlsx)
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 280px;">Fornecedor:
                                    <asp:DropDownList ID="ddlExportarProdutosPorFornecedor" runat="server" AppendDataBoundItems="True" Width="175" AutoPostBack="True"
                                        DataSourceID="sqlFornecedores" DataTextField="fornecedorNome" DataValueField="fornecedorId" OnSelectedIndexChanged="ddlExportarProdutosPorFornecedor_OnSelectedIndexChanged">
                                    </asp:DropDownList>
                                </td>
                                <td style="width: 255px;">Categoria:
                                    <asp:DropDownList ID="ddlExportarProdutosPorFornecedor_Categoria" runat="server" AppendDataBoundItems="True" Width="175"
                                        DataTextField="categoriaNome" DataValueField="categoriaId">
                                    </asp:DropDownList>
                                </td>
                                <td style="width: 280px;">
                                    <div style="width: 100%; float: left; clear: left; margin-top: 10px; margin-bottom: 10px;">
                                        <asp:RadioButtonList ID="rblExportarProdutos" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem Value="todos" Text="Todos os Produtos"></asp:ListItem>
                                            <asp:ListItem Value="ativos" Text="Produtos Ativos"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </div>

                                </td>
                                <td>
                                    <asp:Button runat="server" Text="Exportar" ID="btnExportarProdutos" OnClick="btnExportarProdutos_OnClick" />
                                </td>
                            </tr>
                        </table>
                    </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>

            <dx:TabPage Text="Exportar planilha de produtos por categoria especifica">
                <ContentCollection>
                    <dx:ContentControl runat="server">
                        <table width="100%">
                            <tr>
                                <td class="tituloPaginas" colspan="4" valign="top">Exportar produtos por categoria especifica (xlsx)
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 280px;">Categoria:
                                    <asp:DropDownList ID="ddlCategoriaEspecifica" runat="server" AppendDataBoundItems="True" Width="175" AutoPostBack="True">
                                        <asp:ListItem Text="Móveis" Value="773"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    <div style="width: 100%; float: left; clear: left; margin-top: 10px; margin-bottom: 10px;">
                                        <asp:RadioButton ID="rbtPET" Checked="True" GroupName="RBAtivosPE" runat="server" Text="Todos" />
                                        <asp:RadioButton ID="rbtPESA" GroupName="RBAtivosPE" runat="server" Text="Só os ativos" />
                                        <asp:RadioButton ID="rbtPESI" GroupName="RBAtivosPE" runat="server" Text="Só os inativos" />
                                    </div>

                                </td>
                                <td>
                                    <asp:Button runat="server" Text="Exportar" ID="btnExportaProdutoPorCategoriaEspecifica" OnClick="btnExportaProdutoPorCategoriaEspecifica_OnClick" />
                                </td>
                            </tr>
                        </table>
                    </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>

        </TabPages>
    </dx:ASPxPageControl>


    <asp:ValidationSummary runat="server" ShowMessageBox="true" ShowSummary="false" ValidationGroup="tipoPlanilha" />

    <asp:GridView ID="GridView1" runat="server"></asp:GridView>
    <asp:SqlDataSource ID="sqlFornecedores" runat="server"
        ConnectionString="<%$ ConnectionStrings:connectionString %>"
        SelectCommand="SELECT [fornecedorId], [fornecedorNome] FROM [tbProdutoFornecedor] ORDER BY [fornecedorNome]"></asp:SqlDataSource>

</asp:Content>

