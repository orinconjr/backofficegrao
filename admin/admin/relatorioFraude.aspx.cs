﻿using DevExpress.Web.ASPxGridView;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_relatorioFraude : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        FillGrid();
    }

    private void FillGrid()
    {
        var data = new dbCommerceDataContext();
        var dtAgora = System.DateTime.Now;
        var dtCorte = new DateTime(2018, 3, 1);
        try
        {
            var dados = (from c in data.tbPedidos
                         join d in data.tbClientes on c.clienteId equals d.clienteId
                         join p in data.tbPedidoPagamentos on c.pedidoId equals p.pedidoId
                         join tp in data.tbTipoDePagamentos on c.tipoDePagamentoId equals tp.tipoDePagamentoId
                         where (c.dataHoraDoPedido >= dtCorte && c.dataHoraDoPedido <= dtAgora) && c.tipoDePagamentoId == 2
                         select new
                         {

                             cartao = p.numeroCartao,
                             dataHoraPedido = c.dataHoraDoPedido,
                             d.clienteCPFCNPJ,
                             d.clienteNome,
                             c.pedidoId,
                             c.endRua,
                             c.endNumero,
                             c.endComplemento,
                             c.endBairro,
                             c.endCidade,
                             c.endEstado,
                             c.endPais,
                             c.endCep,
                             c.valorCobrado,
                             c.condDePagamentoId,
                             c.statusDoPedido,

                         }
                         ).ToList();

            //var dados = (from c in data.tbPedidos
            //                 join d in data.tbClientes on c.clienteId equals d.clienteId
            //                 join e in data.tbTipoDePagamentos on c.tipoDePagamentoId equals e.tipoDePagamentoId
            //                 join s in data.tbPedidoSituacaos on c.statusDoPedido equals s.situacaoId
            //                 join t in data.tbPedidoPagamentos on c.tipoDePagamentoId equals t.condicaoDePagamentoId
            //             where c.dataHoraDoPedido >= dtCorte && c.dataHoraDoPedido <= dtAgora && c.tipoDePagamentoId == 2
            //             select new
            //             {
            //                 cartao = t.numeroCartao,
            //                 c.dataHoraDoPedido,
            //                 c.pedidoId,
            //                 d.clienteCPFCNPJ,
            //                 c.endRua,
            //                 c.endNumero,
            //                 c.endComplemento,
            //                 c.endBairro,
            //                 c.endCidade,
            //                 c.endEstado,
            //                 c.endPais,
            //                 c.endCep,
            //                 d.clienteNome,
            //                 c.valorCobrado,
            //                 c.condDePagamentoId,
            //                 c.statusDoPedido,
            //             }).ToList();

            grd.DataSource = dados;
            grd.DataBind();

        }
        catch (Exception ex)
        {
            lblMensagem.Text = "Erro ao carregar o grid: " + ex.Message;
        }
    }

    protected void btPdf_Click(object sender, ImageClickEventArgs e)
    {

    }

    protected void btXsl_Click(object sender, ImageClickEventArgs e)
    {

    }

    protected void btRtf_Click(object sender, ImageClickEventArgs e)
    {

    }

    protected void btCsv_Click(object sender, ImageClickEventArgs e)
    {

    }

    protected void btRefresh_Click(object sender, ImageClickEventArgs e)
    {

    }

    protected void grd_AutoFilterCellEditorCreate(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewEditorCreateEventArgs e)
    {

    }

    protected void grd_AutoFilterCellEditorInitialize(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewEditorEventArgs e)
    {

    }

    protected void grd_ProcessColumnAutoFilter(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewAutoFilterEventArgs e)
    {

    }

    protected void grd_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewColumnDataEventArgs e)
    {

    }

    protected void grd_HtmlRowCreated(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs e)
    {
        int condDePagamentoId = 0;
        int statusDoPedido = 0;
        var data = new dbCommerceDataContext();

        if (e.RowType != GridViewRowType.Data)
        {
            return;
        }

        try
        {

            var cartao = e.GetValue("cartao");
            var lblCartao = (Label)grd.FindRowCellTemplateControl(e.VisibleIndex, grd.Columns["lblCartao"] as GridViewDataColumn, "lblCartao");
            lblCartao.Text = string.Empty;
            if (cartao != null)
            {
                var tamanhoCartao = cartao.ToString().Length;
                var cartaoInicio = cartao.ToString().Substring(0, 4);
                var cartaoFinal = cartao.ToString().Substring(tamanhoCartao - 5, 5);
                cartao = cartaoInicio + "******" + cartaoFinal;
                lblCartao.Text = cartao.ToString();
            }



            int.TryParse(grd.GetRowValues(e.VisibleIndex, new string[] { "condDePagamentoId" }).ToString(), out condDePagamentoId);
            var lblCondicaoPagamento = (Label)grd.FindRowCellTemplateControl(e.VisibleIndex, grd.Columns["lblCondicaoPagamento"] as GridViewDataColumn, "lblCondicaoPagamento");
            lblCondicaoPagamento.Text = string.Empty;

            var strCondicao = (from c in data.tbCondicoesDePagamentos where c.condicaoId == condDePagamentoId select c).FirstOrDefault();
            if (strCondicao != null)
                lblCondicaoPagamento.Text = strCondicao.condicaoNome;

            int.TryParse(grd.GetRowValues(e.VisibleIndex, new string[] { "statusDoPedido" }).ToString(), out statusDoPedido);
            var lblStatusPedido = (Label)grd.FindRowCellTemplateControl(e.VisibleIndex, grd.Columns["lblStatusPedido"] as GridViewDataColumn, "lblStatusPedido");
            lblStatusPedido.Text = string.Empty;

            var strStatus = (from c in data.tbPedidoSituacaos where c.situacaoId == statusDoPedido select c).FirstOrDefault();
            if (strStatus != null)
                lblStatusPedido.Text = strStatus.situacao;


        }
        catch (Exception ex)
        {
            lblMensagem.Text = "Erro ao gerar o registro: " + ex.Message;
        }


    }
}