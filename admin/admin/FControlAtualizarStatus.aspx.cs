﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Net;
using System.IO;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

public partial class admin_FControlAtualizarStatus : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string login = "LINDSAY";
        string senha = "srenxovais";

        string codPedido = Request["codPedido"];
        string status = Request["status"];

        string url = "https://secure.fcontrol.com.br/validator/EnviarCancelarPedido.asp?" +
                      "login=" + HttpUtility.UrlEncode(login) +        //Usuário
                      "&password=" + HttpUtility.UrlEncode(senha) +     //Senha
                      "&Order_Id=" + HttpUtility.UrlEncode(codPedido) + //Código do pedido
                      "&" + HttpUtility.UrlEncode(status) + "=true";       //*Status

        //* O valor de status deve ser: cancelado, cancelado_suspeita ou enviado
        //O status de cada pedido só pode ser alterado uma vez!

        //Abertura da url
        WebRequest request = WebRequest.Create(url);
        WebResponse response = request.GetResponse();

        //leitura da url
        StreamReader reader = new StreamReader(response.GetResponseStream());
        string retorno = reader.ReadToEnd();

        //Impressão do retorno
        Response.Write("Retorno: " + retorno);
    }
}
