﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="transferenciaProdutos.aspx.cs" Inherits="admin_transferenciaProdutos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" Runat="Server">
     <table cellpadding="0" cellspacing="0" style="width: 920px">
        <tr>
            <td class="tituloPaginas" valign="top">
                <asp:Label ID="lblAcao" runat="server">Produtos aguardando transferência</asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
                    <tr>
                        <td class="rotulos">
                            <asp:Literal runat="server" ID="litListaPedidos"></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-top: 100px;">
                            <asp:Button runat="server" ID="btnConfirmarTransferencia" Text="Confirmar Transferência" OnClick="btnConfirmarTransferencia_OnClick" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
