﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_comprasOrdensAguardandoAprovacaoAprovar : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            FillListaPendentes();
        }
    }


    private void FillListaPendentes()
    {
        int id = Convert.ToInt32(Request.QueryString["id"]);

        var data = new dbCommerceDataContext();
        var ordem = (from c in data.tbComprasOrdems where c.idComprasOrdem == id select c).FirstOrDefault();
        if (ordem == null) Response.Redirect("Default.aspx");
        if (ordem.statusOrdemCompra != 1) Response.Redirect("Default.aspx");

        var produtosPendentes = (from c in data.tbComprasOrdemProdutos
                                 where c.idComprasOrdem == id
                                 select new
                                 {
                                     c.idComprasOrdemProduto,
                                     c.idComprasSolicitacaoProduto,
                                     c.idComprasProduto,
                                     c.tbComprasProduto.produto,
                                     c.status,
                                     c.quantidade,
                                     c.preco,
                                     c.comentario,
                                     unidade = (from d in data.tbComprasProdutoFornecedors where d.idComprasFornecedor == c.tbComprasOrdem.idComprasFornecedor && d.idComprasProduto == c.idComprasProduto select d).First().tbComprasUnidadesDeMedida.unidadeDeMedida
                                 });
        lstProdutosSolicitacao.DataSource = produtosPendentes;
        lstProdutosSolicitacao.DataBind();


        litFornecedor.Text = ordem.tbComprasFornecedor.fornecedor;
        litEmpresa.Text = ordem.tbComprasEmpresa.empresa;
        litTotal.Text = produtosPendentes.Any()
            ? produtosPendentes.Sum(x => x.preco * x.quantidade).ToString("C")
            : 0.ToString("C");
        litTotalAprovado.Text = produtosPendentes.Any(x => x.status == 1)
            ? produtosPendentes.Where(x => x.status == 1).Sum(x => x.preco * x.quantidade).ToString("C")
            : 0.ToString("C");
        litTotalReprovado.Text = produtosPendentes.Any(x => x.status == 2)
            ? produtosPendentes.Where(x => x.status == 2).Sum(x => x.preco * x.quantidade).ToString("C")
            : 0.ToString("C");

        var condicoes = (from c in data.tbComprasFornecedorCondicaoPagamentos
            where c.idComprasFornecedor == ordem.idComprasFornecedor
            select new
            {
                c.tbComprasCondicaoPagamento.nome,
                c.acrescimo,
                c.desconto,
                c.idComprasFornecedorCondicaoPagamento,
                c.tbComprasCondicaoPagamento.idComprasCondicaoPagamento
            });

        ddlCondicaoPagamento.DataSource = condicoes;
        ddlCondicaoPagamento.DataBind();

        usrComprasOrdemCondicoes.IdComprasOrdem = id;
        usrComprasOrdemCondicoes.FillCondicoes(true);

        if (ordem.idComprasCondicaoPagamento != null) ddlCondicaoPagamento.SelectedItem = ddlCondicaoPagamento.Items.FindByValue(ordem.idComprasCondicaoPagamento.ToString());
    }


    protected void btnGravarOrdemCompra_OnClick(object sender, EventArgs e)
    {
        var data = new dbCommerceDataContext();
        int id = Convert.ToInt32(Request.QueryString["id"]);
        var ordem = (from c in data.tbComprasOrdems where c.idComprasOrdem == id select c).First();
        if (ddlCondicaoPagamento.SelectedItem != null)
        {
            ordem.idComprasCondicaoPagamento = Convert.ToInt32(ddlCondicaoPagamento.Value);
            data.SubmitChanges();
        }
        else
        {
            ordem.idComprasCondicaoPagamento = null;
            data.SubmitChanges();
        }
        foreach (var item in lstProdutosSolicitacao.Items)
        {
            var rdbPendente = (RadioButton)item.FindControl("rdbPendente");
            var rdbAprovar = (RadioButton)item.FindControl("rdbAprovar");
            var rdbReprovar = (RadioButton)item.FindControl("rdbReprovar");
            var txtComentario = (TextBox)item.FindControl("txtComentario");
            int status = rdbAprovar.Checked ? 1 : rdbReprovar.Checked ? 2 : 0;
            var hdfIdComprasOrdemProduto = (HiddenField)item.FindControl("hdfIdComprasOrdemProduto");
            int idComprasOrdemProduto = Convert.ToInt32(hdfIdComprasOrdemProduto.Value);
            var compraProduto =
                (from c in data.tbComprasOrdemProdutos where c.idComprasOrdemProduto == idComprasOrdemProduto select c)
                    .First();
            if (compraProduto.status != status)
            {
                string statusAtual = compraProduto.status == 1
                    ? "Aprovado"
                    : compraProduto.status == 2 ? "Reprovado" : "Pendente";

                string statusNovo = status == 1
                    ? "Aprovado"
                    : status == 2 ? "Reprovado" : "Pendente";
                compraProduto.status = status;

                var produtoHistorico = new tbComprasOrdemProdutoHistorico
                {
                    data = DateTime.Now.ToShortDateString(),
                    usuario = rnUsuarios.retornaNomeUsuarioLogado(),
                    idComprasOrdemProduto = idComprasOrdemProduto,
                    descricao = "Status alterado de " + statusAtual + " para " + statusNovo
                };
                data.tbComprasOrdemProdutoHistoricos.InsertOnSubmit(produtoHistorico);
                data.SubmitChanges();
            }
            if (compraProduto.comentario != txtComentario.Text)
            {
                var produtoHistorico = new tbComprasOrdemProdutoHistorico
                { 
                    data = DateTime.Now.ToShortDateString(),
                    usuario = rnUsuarios.retornaNomeUsuarioLogado(),
                    idComprasOrdemProduto = idComprasOrdemProduto,
                    descricao = "Comentário alterado de " + compraProduto.comentario + " para " + txtComentario.Text
                };
                data.tbComprasOrdemProdutoHistoricos.InsertOnSubmit(produtoHistorico);
                compraProduto.comentario = txtComentario.Text;
                data.SubmitChanges();
            }
        }

        var itensPendentes =
            (from c in data.tbComprasOrdemProdutos where c.idComprasOrdem == id && c.status == 0 select c).Any();
        if (!itensPendentes)
        {
            ordem.statusOrdemCompra = 2;
            data.SubmitChanges();
            Response.Redirect("comprasOrdensAguardandoAprovacao.aspx");
        }

        FillListaPendentes();
        Response.Write("<script>alert('Ordem de compra alterada com sucesso.');</script>");
    }



    private int RetornaIdUsuarioLogado()
    {
        HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
        usuarioLogadoId = Request.Cookies["usuarioLogadoId"];
        if (usuarioLogadoId == null)
        {
            return 0;
        }

        return Convert.ToInt32(usuarioLogadoId.Value);
    }

    protected void lstProdutosSolicitacao_OnItemDataBound(object sender, ListViewItemEventArgs e)
    {
        var rdbPendente = (RadioButton) e.Item.FindControl("rdbPendente");
        var rdbAprovar = (RadioButton)e.Item.FindControl("rdbAprovar");
        var rdbReprovar = (RadioButton)e.Item.FindControl("rdbReprovar");
        var hdfStatus = (HiddenField)e.Item.FindControl("hdfStatus");
        var hdfIdComprasOrdemProduto = (HiddenField)e.Item.FindControl("hdfIdComprasOrdemProduto");
        rdbPendente.GroupName = "status" + hdfIdComprasOrdemProduto.Value;
        rdbAprovar.GroupName = "status" + hdfIdComprasOrdemProduto.Value;
        rdbReprovar.GroupName = "status" + hdfIdComprasOrdemProduto.Value;

        int status = Convert.ToInt32(hdfStatus.Value);

        if (status == 0) rdbPendente.Checked = true;
        else if (status == 1) rdbAprovar.Checked = true;
        else if (status == 2) rdbReprovar.Checked = true;
    }

}