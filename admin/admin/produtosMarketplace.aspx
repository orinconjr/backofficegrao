﻿<%@ Page Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="produtosMarketplace.aspx.cs" Inherits="admin_produtosMarketplace" Theme="Glass" MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dxwgv" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxPopupControl" Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>
<%@ Register TagPrefix="dxe" Namespace="DevExpress.Web.ASPxMenu" Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Data.Linq" Assembly="DevExpress.Web.v11.1.Linq, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" runat="Server">

    <script language="javascript" type="text/javascript">
        function grid_ContextMenu(s, e) {
            if (e.objectType == "header")
                pmColumnMenu.ShowAtPos(ASPxClientUtils.GetEventX(e.htmlEvent), ASPxClientUtils.GetEventY(e.htmlEvent));
        }
    </script>

    <dxe:ASPxPopupMenu ID="pmColumnMenu" runat="server" ClientInstanceName="pmColumnMenu">
        <Items>
            <dxe:MenuItem Name="cmdShowCustomization" Text="Escolher colunas">
            </dxe:MenuItem>
        </Items>
        <ClientSideEvents ItemClick="function(s, e) { if(e.item.name == 'cmdShowCustomization') grd.ShowCustomizationWindow(); }" />
    </dxe:ASPxPopupMenu>

    <table cellpadding="0" cellspacing="0" style="width: 920px">
        <tr>
            <td class="tituloPaginas" valign="top">
                <asp:Label ID="lblAcao" runat="server">Produtos</asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
                    <tr>
                        <td class="rotulos">
                            <div style="width: 415px; float: left;">
                                <div style="font-size: 16px; font-weight: bold; width: 395px; text-align: center;">Filtros</div>
                                <div style="width: 415px; height: 400px; overflow: auto; float: left;">
                                    <asp:TreeView ID="treeCategorias" runat="server" Visible="False"
                                        ShowLines="True" OnTreeNodePopulate="TreeView1_TreeNodePopulate" ExpandDepth="0"
                                        Font-Names="Tahoma" Font-Size="12px" ForeColor="Black">
                                        <SelectedNodeStyle BackColor="#D7EAEE" />

                                        <Nodes>
                                            <asp:TreeNode Selected="True" Text="Categorias" Value="0"></asp:TreeNode>
                                        </Nodes>
                                    </asp:TreeView>
                                    <asp:TreeView ID="treeCategoriasFiltros" runat="server"
                                        ShowLines="True" OnTreeNodePopulate="TreeView1_TreeNodePopulate" ExpandDepth="0"
                                        Font-Names="Tahoma" Font-Size="12px" ForeColor="Black">
                                        <SelectedNodeStyle BackColor="#D7EAEE" />

                                        <Nodes>
                                            <asp:TreeNode Selected="True" Text="Categorias" Value="0"></asp:TreeNode>
                                        </Nodes>
                                    </asp:TreeView>
                                </div>
                            </div>
                            <div style="width: 415px; float: left;">
                                <div style="font-size: 16px; font-weight: bold; width: 395px; text-align: center;">Categorias Ativas no Site</div>
                                <div style="width: 415px; height: 400px; overflow: auto; float: left;">

                                    <asp:TreeView ID="treeCategoriasAtivasNoSite" runat="server"
                                        ShowLines="True" ExpandDepth="0"
                                        Font-Names="Tahoma" Font-Size="12px" ForeColor="Black">
                                        <SelectedNodeStyle BackColor="#D7EAEE" />

                                        <Nodes>
                                            <asp:TreeNode Selected="True" Text="Categorias" Value="0"></asp:TreeNode>
                                        </Nodes>
                                    </asp:TreeView>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="right">
                            <table cellpadding="4" cellspacing="0" style="width: 834px">
                                <tr>
                                    <td colspan="2">
                                       <strong>Marketplace:</strong>
                                        <asp:DropDownList ID="ddlMarketplace" AutoPostBack="true" runat="server">
                                             <asp:ListItem Value="1" Text="CNova" Selected="True"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                       <strong>Status da Integração:</strong>
                                        <asp:DropDownList ID="ddlStatusIntegracao" AutoPostBack="true" runat="server">
                                            <asp:ListItem Value="" Text="Todos" Selected="True"></asp:ListItem>
                                            <asp:ListItem Value="99" Text="Não Enviado"></asp:ListItem>
                                            <asp:ListItem Value="0" Text="Pendente"></asp:ListItem>
                                            <asp:ListItem Value="1" Text="Integrado"></asp:ListItem>
                                            <asp:ListItem Value="2" Text="Erro"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                       <strong>Status do Produto:</strong>
                                        <asp:DropDownList ID="ddlStatusProduto" AutoPostBack="true" runat="server">
                                             <asp:ListItem Value="" Text="Todos"></asp:ListItem>
                                             <asp:ListItem Value="1" Text="Ativos" Selected="True"></asp:ListItem>
                                             <asp:ListItem Value="0" Text="Inativos"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
               
                                    </td>
                                    <td height="38"
                                        style="background-image: url('images/bkExportar.jpg'); padding-bottom: 1px;"
                                        valign="bottom" width="231">
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 97px">&nbsp;</td>
                                                <td style="width: 33px">
                                                    <asp:ImageButton ID="btPdf" runat="server" ImageUrl="~/admin/images/btPdf.jpg"
                                                        Style="margin-left: 5px" OnClick="btPdf_Click" EnableViewState="true" ValidationGroup="exportar" />
                                                </td>
                                                <td style="width: 31px">
                                                    <asp:ImageButton ID="btXsl" runat="server" ImageUrl="~/admin/images/btXls.jpg"
                                                        Style="margin-left: 5px" OnClick="btXsl_Click" ValidationGroup="exportar" />
                                                </td>
                                                <td style="width: 36px">
                                                    <asp:ImageButton ID="btRtf" runat="server" ImageUrl="~/admin/images/btRtf.jpg"
                                                        Style="margin-left: 5px" OnClick="btRtf_Click" ValidationGroup="exportar" />
                                                </td>
                                                <td valign="bottom">
                                                    <asp:ImageButton ID="btCsv" runat="server" ImageUrl="~/admin/images/btCsv.jpg"
                                                        Style="margin-left: 5px" OnClick="btCsv_Click" ValidationGroup="exportar" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Panel ID="pnlGrid" runat="server" ScrollBars="Horizontal" Width="834px">
                                <dxwgv:ASPxGridView ID="grd" runat="server" AutoGenerateColumns="False"
                                    KeyFieldName="ProdutoId" Settings-ShowFilterBar="Visible"
                                    Cursor="auto" ClientInstanceName="grd" EnableCallBacks="False">
                                    <SettingsBehavior AutoFilterRowInputDelay="10200" ConfirmDelete="True"
                                        AutoExpandAllGroups="True" ColumnResizeMode="Control" AllowGroup="False"
                                        AllowFocusedRow="True" />
                                    <SettingsText ConfirmDelete="Tem certeza que deseja excluir?"
                                        EmptyDataRow="Nenhum registro encontrado."
                                        GroupPanel="Arraste uma coluna aqui para agrupar." />
                                    <SettingsPager Position="TopAndBottom" PageSize="25"
                                        ShowDisabledButtons="False" AlwaysShowPager="True">
                                        <Summary AllPagesText="Páginas: {0} - {1} ({2} items)"
                                            Text="Página {0} de {1} ({2} registros encontrados)" />
                                    </SettingsPager>
                                    <Settings ShowFilterRow="True" ShowGroupedColumns="True" ShowFooter="True" />
                                    <TotalSummary>
                                        <dxwgv:ASPxSummaryItem DisplayFormat="0" FieldName="margem"
                                            ShowInColumn="Margem" ShowInGroupFooterColumn="Margem"
                                            SummaryType="Average" />
                                    </TotalSummary>
                                    <SettingsEditing EditFormColumnCount="4"
                                        PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1"
                                        PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter"
                                        PopupEditFormWidth="700px" Mode="Inline" />
                                    <Columns>
                                        <dxwgv:GridViewDataTextColumn Caption="#" Name="Selecionado" VisibleIndex="2" Width="30px">
                                            <DataItemTemplate>
                                                <dxe:ASPxCheckBox ID="chkSelecionado" runat="server" Value='<%# Bind("Selecionado") %>'></dxe:ASPxCheckBox>
                                            </DataItemTemplate>
                                        </dxwgv:GridViewDataTextColumn>     
                                        <dxwgv:GridViewDataTextColumn Caption="ID Produto" FieldName="ProdutoId"
                                            ReadOnly="True" VisibleIndex="4" Width="70px">
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn Caption="Nome" FieldName="ProdutoNome"
                                            VisibleIndex="5" Width="210px">
                                            <Settings AutoFilterCondition="Contains" />
                                        </dxwgv:GridViewDataTextColumn>

                                        <dxwgv:GridViewDataComboBoxColumn Caption="Fornecedor" FieldName="ProdutoFornecedor" VisibleIndex="6" Width="130px">
                                            <PropertiesComboBox DataSourceID="sqlFornecedor" TextField="FornecedorNome" IncrementalFilteringMode="StartsWith" DropDownStyle="DropDown"
                                                ValueField="fornecedorId" ValueType="System.String">
                                            </PropertiesComboBox>
                                        </dxwgv:GridViewDataComboBoxColumn>

                                        <dxwgv:GridViewDataComboBoxColumn Caption="Marca" FieldName="ProdutoMarca" VisibleIndex="7" Width="130px">
                                            <PropertiesComboBox DataSourceID="sqlMarcas" TextField="marcaNome" IncrementalFilteringMode="StartsWith" DropDownStyle="DropDown"
                                                ValueField="marcaId" ValueType="System.String">
                                            </PropertiesComboBox>
                                        </dxwgv:GridViewDataComboBoxColumn>
                                        <dxwgv:GridViewDataTextColumn Caption="Status" Name="StatusIntegracao" VisibleIndex="8" Width="70px">
                                            <DataItemTemplate>
                                                <%# Eval("statusIntegracao")%>
                                            </DataItemTemplate>
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn Caption="Ver no Site" Name="verNoSite" VisibleIndex="9" Width="75px">
                                            <DataItemTemplate>
                                                <a href='<%# "http://www.graodegente.com.br/" + (Eval("Categoria") != null ? Eval("Categoria") : "") +"/" + Eval("ProdutoUrl") %>' target="_black" style='display: <%# string.IsNullOrEmpty((Eval("Categoria") ?? "").ToString()) ? "none" : "block" %>'>Ver no Site</a>
                                            </DataItemTemplate>
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn Caption="Disponibilidade" Name="Disponibilidade" VisibleIndex="10" Width="85px" Visible="true">
                                            <DataItemTemplate>
                                                <asp:Button runat="server" ID="btnAlterarDisponibilidade" Text="Alterar" Enabled='<%# Eval("StatusIntegracao") == "Integrado" %>' CommandArgument='<%# Eval("ProdutoId") %>' OnCommand="btnAlterarDisponibilidade_OnCommand">
                                                </asp:Button>
                                            </DataItemTemplate>
                                        </dxwgv:GridViewDataTextColumn>
                                    </Columns>
                                    <ClientSideEvents ContextMenu="grid_ContextMenu" />
                                    <SettingsCustomizationWindow Enabled="True" />
                                </dxwgv:ASPxGridView>
                            </asp:Panel>

                            <asp:ObjectDataSource ID="sqlFornecedor" runat="server" SelectMethod="GetFornecedores"
                                TypeName="rnProdutos"></asp:ObjectDataSource>
                            <asp:ObjectDataSource ID="sqlMarcas" runat="server" SelectMethod="GetMarcas"
                                TypeName="rnProdutos"></asp:ObjectDataSource>

                            <dxwgv:ASPxGridViewExporter ID="grdEx" runat="server" FileName="fornecedores"
                                GridViewID="grd" Landscape="True" PreserveGroupRowStates="True">
                            </dxwgv:ASPxGridViewExporter>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Button ID="btnEnviarProdutos" Text="Enviar Produtos" runat="server" OnClick="btnEnviarProdutos_Click"/>&nbsp;
                            <asp:Button ID="btnAtualizarPrecos" Text="Atualizar Preços" runat="server" OnClick="btnAtualizarPrecos_Click"/>&nbsp;
                            <asp:Button ID="btnAtualizarEstoque" Text="Atualizar Estoque" runat="server" OnClick="btnAtualizarEstoque_Click"/>&nbsp;
                            <asp:Button ID="btnAtualizarStatusIntegracao" Text="Atualizar Status da Integração" runat="server" OnClick="btnAtualizarStatusIntegracao_Click"/>
                        </td>
                    </tr>
                  
                </table>
            </td>
        </tr>
    </table>
    
    <dx:ASPxPopupControl ID="popDisponibilidade" runat="server" CloseAction="CloseButton" Modal="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
        ClientInstanceName="popDisponibilidade" HeaderText="Alterar Disponibilidade" AllowDragging="True" PopupAnimationType="None" EnableViewState="True" Height="275" AutoUpdatePosition="True">
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl10" runat="server">
                <h3>Sites</h3>
                <div style="width: 500px">
                    <asp:HiddenField runat="server" ID="hdfProdutoId" />
                    <table style="width: 100%">
                        <tr>
                            <td>
                                <asp:CheckBoxList ID="chkDisponibilidade" runat="server" RepeatLayout="Flow" RepeatDirection="Vertical"> 
                                </asp:CheckBoxList>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right;">
                                <asp:Button runat="server" ID="btnAtualizarDisponibilidade" Text="Salvar" OnClick="btnAtualizarDisponibilidade_Click"/>
                            </td>
                        </tr>
                    </table>
                </div>
            </dx:PopupControlContentControl>
        </ContentCollection>
        <ContentStyle>
            <Paddings PaddingBottom="5px" />
        </ContentStyle>
    </dx:ASPxPopupControl>
</asp:Content>

