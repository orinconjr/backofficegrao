﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxTabControl;
using DevExpress.Web.ASPxEditors;
using DevExpress.Data;
using DevExpress.Web.ASPxUploadControl;
using System.IO;
using System.Data.SqlClient;

public partial class admin_marcas : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    //protected void btPdf_Click(object sender, ImageClickEventArgs e)
    //{
    //    this.grdEx.WritePdfToResponse();
    //}
    //protected void btXsl_Click(object sender, ImageClickEventArgs e)
    //{
    //    this.grdEx.WriteXlsToResponse();
    //}
    //protected void btRtf_Click(object sender, ImageClickEventArgs e)
    //{
    //    this.grdEx.WriteRtfToResponse();
    //}
    //protected void btCsv_Click(object sender, ImageClickEventArgs e)
    //{
    //    this.grdEx.WriteCsvToResponse();
    //}

    protected void grd_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType != GridViewRowType.EditForm) return;
        {
            Image image1 = (Image)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "imagem1");
            Image image2 = (Image)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "imagem2");
            Image image3 = (Image)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "imagem3");

            CheckBox ckbExcluir1 = (CheckBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "ckbExcluir1");
            CheckBox ckbExcluir2 = (CheckBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "ckbExcluir2");
            CheckBox ckbExcluir3 = (CheckBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "ckbExcluir3");

            #region Oculta imagem quando for inserir um novo registro ou quando for editar um registro e não existir imagem no bd
            if (image1.ImageUrl.ToString() == "../marcas//" || image1.ImageUrl.ToString() == "../marcas/" + e.KeyValue + "/")
            {
                image1.Visible = false;
                ckbExcluir1.Visible = false;
            }

            if (image2.ImageUrl.ToString() == "../marcas//" || image2.ImageUrl.ToString() == "../marcas/" + e.KeyValue + "/")
            {
                image2.Visible = false;
                ckbExcluir2.Visible = false;
            }

            if (image3.ImageUrl.ToString() == "../marcas//" || image3.ImageUrl.ToString() == "../marcas/" + e.KeyValue + "/")
            {
                image3.Visible = false;
                ckbExcluir3.Visible = false;
            }
            #endregion
        }
    }

    protected void grd_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        ASPxTextBox txtNome = (ASPxTextBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["marcaNome"], "txtNome");
        FileUpload flu1 = (FileUpload)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "flu1");
        FileUpload flu2 = (FileUpload)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "flu2");
        FileUpload flu3 = (FileUpload)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "flu3");

        e.NewValues["marcaNome"] = txtNome.Text;
        e.NewValues["marcaUrl"] = rnFuncoes.limpaString(txtNome.Text).Trim().ToLower();

        if (flu1.HasFile)
            e.NewValues["imagem1"] = "imagem1.jpg";

        if (flu2.HasFile)
            e.NewValues["imagem2"] = "imagem2.jpg";

        if (flu3.HasFile)
            e.NewValues["imagem3"] = "imagem3.jpg";
    }

    protected void grd_RowInserted(object sender, DevExpress.Web.Data.ASPxDataInsertedEventArgs e)
    {
        FileUpload flu1 = (FileUpload)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "flu1");
        FileUpload flu2 = (FileUpload)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "flu2");
        FileUpload flu3 = (FileUpload)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "flu3");

        int ultimoId = rnMarca.retornaUltimoId();
        Directory.CreateDirectory(ConfigurationManager.AppSettings["caminhoFisico"] + "marcas\\" + ultimoId);

        if (flu1.HasFile)
        {
            flu1.PostedFile.SaveAs(ConfigurationManager.AppSettings["caminhoFisico"] + "marcas\\" + ultimoId + "\\" + "imagem1.jpg");
        }

        if (flu2.HasFile)
        {
            flu2.PostedFile.SaveAs(ConfigurationManager.AppSettings["caminhoFisico"] + "marcas\\" + ultimoId + "\\" + "imagem2.jpg");
        }

        if (flu3.HasFile)
        {
            flu3.PostedFile.SaveAs(ConfigurationManager.AppSettings["caminhoFisico"] + "marcas\\" + ultimoId + "\\" + "imagem3.jpg");
        }
    }

    protected void grd_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        ASPxTextBox txtNome = (ASPxTextBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["marcaNome"], "txtNome");
        FileUpload flu1 = (FileUpload)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "flu1");
        FileUpload flu2 = (FileUpload)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "flu2");
        FileUpload flu3 = (FileUpload)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "flu3");

        CheckBox ckbExcluir1 = (CheckBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "ckbExcluir1");
        CheckBox ckbExcluir2 = (CheckBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "ckbExcluir2");
        CheckBox ckbExcluir3 = (CheckBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "ckbExcluir3");

        int marcaId = Convert.ToInt32(e.Keys[0]);

        e.NewValues["marcaNome"] = txtNome.Text;
        e.NewValues["marcaUrl"] = rnFuncoes.limpaString(txtNome.Text).Trim().ToLower();

        if (flu1.HasFile)
            e.NewValues["imagem1"] = "imagem1.jpg";

        if (flu2.HasFile)
            e.NewValues["imagem2"] = "imagem2.jpg";

        if (flu3.HasFile)
            e.NewValues["imagem3"] = "imagem3.jpg";

        if (ckbExcluir1.Checked == true)
            rnMarca.excluiFoto(marcaId, "imagem1.jpg");

        if (ckbExcluir2.Checked == true)
            rnMarca.excluiFoto(marcaId, "imagem2.jpg");

        if (ckbExcluir3.Checked == true)
            rnMarca.excluiFoto(marcaId, "imagem3.jpg");
    }

    protected void grd_RowUpdated(object sender, DevExpress.Web.Data.ASPxDataUpdatedEventArgs e)
    {
        if (e.Exception == null)
        {
            FileUpload flu1 = (FileUpload)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "flu1");
            FileUpload flu2 = (FileUpload)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "flu2");
            FileUpload flu3 = (FileUpload)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "flu3");
            
            int marcaId = Convert.ToInt32(e.Keys[0]);

            if (flu1.HasFile)
            {
                flu1.PostedFile.SaveAs(ConfigurationManager.AppSettings["caminhoFisico"] + "marcas\\" + marcaId + "\\" + "imagem1.jpg");
            }

            if (flu2.HasFile)
            {
                flu2.PostedFile.SaveAs(ConfigurationManager.AppSettings["caminhoFisico"] + "marcas\\" + marcaId + "\\" + "imagem2.jpg");
            }

            if (flu3.HasFile)
            {
                flu3.PostedFile.SaveAs(ConfigurationManager.AppSettings["caminhoFisico"] + "marcas\\" + marcaId + "\\" + "imagem3.jpg");
            }
        }
    }

    protected void grd_RowDeleted(object sender, DevExpress.Web.Data.ASPxDataDeletedEventArgs e)
    {
        if (e.Exception == null)
        {
            int marcaId = Convert.ToInt32(e.Keys[0]);

            if (Directory.Exists(ConfigurationManager.AppSettings["caminhoFisico"] + "marcas\\" + marcaId))
            {
                Directory.Delete(ConfigurationManager.AppSettings["caminhoFisico"] + "marcas\\" + marcaId, true);
            }
        }
        else
        {
            SqlException ex = (SqlException)e.Exception;

            if (ex.Number == 547)
            {
                Response.Write("<script>alert('Essa marca não pode ser excluida por que existe produtos relacionado a ela.');</script>");
                e.ExceptionHandled = true;
            }
        }
    }
}
