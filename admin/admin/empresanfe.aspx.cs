﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.Data;
using Specifications.Extensions;

public partial class admin_empresanfe : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            for (int i = 0; i < 3; i++)
            {
                ddlMes.Items.Add(new ListItem(DateTime.Now.AddMonths(-i).ToString("MMMM"), DateTime.Now.AddMonths(-i).Month + "#" + DateTime.Now.AddMonths(-i).Year));
            }
        }
    }

    protected void sqlEmpresa_Updating(object sender, LinqDataSourceUpdateEventArgs e)
    {
        try
        {
            tbEmpresa especificacao = (tbEmpresa)e.NewObject;
            ASPxCheckBox chkSimples = (ASPxCheckBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["simples"], "chkSimples");
            ASPxCheckBox chkAtivo = (ASPxCheckBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["ativo"], "chkAtivo");
            //ASPxCheckBox chkPedidoSegunda = (ASPxCheckBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["pedidoSegunda"], "chkPedidoSegunda");
            //ASPxCheckBox chkPedidoTerca = (ASPxCheckBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["pedidoTerca"], "chkPedidoTerca");
            //ASPxCheckBox chkPedidoQuarta = (ASPxCheckBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["pedidoQuarta"], "chkPedidoQuarta");
            //ASPxCheckBox chkPedidoQuinta = (ASPxCheckBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["pedidoQuinta"], "chkPedidoQuinta");
            //ASPxCheckBox chkPedidoSexta = (ASPxCheckBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["pedidoSexta"], "chkPedidoSexta");
            //ASPxCheckBox chkPedidoSabado = (ASPxCheckBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["pedidoSabado"], "chkPedidoSabado");
            //ASPxCheckBox chkPedidoDomingo = (ASPxCheckBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["pedidoDomingo"], "chkPedidoDomingo");
            //ASPxCheckBox chkReposicaoAutomaticaEstoque = (ASPxCheckBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["reposicaoAutomaticaEstoque"], "chkReposicaoAutomaticaEstoque");
            //ASPxCheckBox chkEntrega = (ASPxCheckBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["entrega"], "chkEntrega");
            //ASPxCheckBox chkStoqueConferido = (ASPxCheckBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["estoqueConferido"], "chkEstoqueConferido");


            //especificacao.simples = chkSimples.Checked;
            //especificacao.ativo = chkAtivo.Checked;
            //especificacao.pedidoSegunda = chkPedidoSegunda.Checked;
            //especificacao.pedidoTerca = chkPedidoTerca.Checked;
            //especificacao.pedidoQuarta = chkPedidoQuarta.Checked;
            //especificacao.pedidoQuinta = chkPedidoQuinta.Checked;
            //especificacao.pedidoSexta = chkPedidoSexta.Checked;
            //especificacao.pedidoSabado = chkPedidoSabado.Checked;
            //especificacao.pedidoDomingo = chkPedidoDomingo.Checked;
            //especificacao.reposicaoAutomaticaEstoque = chkReposicaoAutomaticaEstoque.Checked;
            //especificacao.entrega = chkEntrega.Checked;
            //especificacao.estoqueConferido = chkStoqueConferido.Checked;

            try
            {
                using (var data = new dbCommerceDataContext())
                {
                    var empresa = (from c in data.tbEmpresas where c.idEmpresa == especificacao.idEmpresa select c).First();
                    empresa.simples = chkSimples.Checked;
                    empresa.ativo = chkAtivo.Checked;
                    data.SubmitChanges();
                }
            }
            catch (Exception ex)
            {

            }
        }
        catch (Exception ex)
        {

        }

    }

    protected void grd_OnCustomUnboundColumnData(object sender, ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "emissoesMes")
        {
            int idEmpresa = Convert.ToInt32(e.GetListSourceFieldValue("idEmpresa"));

            int emissoesMes;

            using (var data = new dbCommerceDataContext())
            {

                using (
                    var txn =
                        new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required,
                            new System.Transactions.TransactionOptions
                            {
                                IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                            }))

                {
                    if (ddlMes.SelectedValue != DateTime.Now.Month.ToString())
                    {
                        int mesSelecionado = Convert.ToInt32(ddlMes.SelectedValue.Split('#')[0]);
                        int ano = Convert.ToInt32(ddlMes.SelectedValue.Split('#')[1]);

                        emissoesMes = (from c in data.tbNotaFiscals
                                       where
                                           c.linkDanfe != null && c.idCNPJ == idEmpresa &&
                                           (c.dataHora.Date.Month == mesSelecionado &&
                                            c.dataHora.Date.Year == ano)
                                       select new { c.idNotaFiscal }).Count();
                    }
                    else
                    {
                        emissoesMes = (from c in data.tbNotaFiscals
                                       where
                                           c.linkDanfe != null && c.idCNPJ == idEmpresa &&
                                           (c.dataHora.Date.Month == DateTime.Now.Month &&
                                            c.dataHora.Date.Year == DateTime.Now.Year)
                                       select new { c.idNotaFiscal }).Count();
                    }


                }
            }

            e.Value = emissoesMes;
        }

        if (e.Column.FieldName == "totalEmitido")
        {

            int idEmpresa = Convert.ToInt32(e.GetListSourceFieldValue("idEmpresa"));

            decimal totalEmitido = 0;

            using (var data = new dbCommerceDataContext())
            {

                using (
                    var txn =
                        new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required,
                            new System.Transactions.TransactionOptions
                            {
                                IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                            }))

                {
                    if (ddlMes.SelectedValue != DateTime.Now.Month.ToString())
                    {
                        int mesSelecionado = Convert.ToInt32(ddlMes.SelectedValue.Split('#')[0]);
                        int ano = Convert.ToInt32(ddlMes.SelectedValue.Split('#')[1]);

                        var checarValor = (from c in data.tbNotaFiscals
                                           where
                                               c.linkDanfe != null && c.idCNPJ == idEmpresa &&
                                               (c.dataHora.Date.Month == mesSelecionado &&
                                                c.dataHora.Date.Year == ano)
                                           select c);

                        if (checarValor.Any())
                            totalEmitido = checarValor.Sum(x => x.valor ?? 0);
                    }
                    else
                    {
                        var checarValor = (from c in data.tbNotaFiscals
                                           where
                                               c.linkDanfe != null && c.idCNPJ == idEmpresa &&
                                               (c.dataHora.Date.Month == DateTime.Now.Month &&
                                                c.dataHora.Date.Year == DateTime.Now.Year)
                                           select c);

                        if (checarValor.Any())
                            totalEmitido = checarValor.Sum(x => x.valor ?? 0);
                    }


                }


            }

            e.Value = totalEmitido.ToString("N");
        }

    }

    protected void grd_OnStartRowEditing(object sender, ASPxStartRowEditingEventArgs e)
    {
        var usuarioLogadoId = Request.Cookies["usuarioLogadoId"];

        if (usuarioLogadoId == null)
            Response.Redirect("default.aspx");

        int usuarioId = Convert.ToInt32(usuarioLogadoId.Value);

        List<int> idUsuarioPermitido = new List<int> { 105, 75, 167 };

        if (!idUsuarioPermitido.Contains(usuarioId))
            e.Cancel = true;
    }

    protected void sqlEmpresa_OnUpdated(object sender, LinqDataSourceStatusEventArgs e)
    {
        try
        {
            //string t = e.Exception.ToString();
            grd.DataBind();
        }
        catch (Exception ex)
        {

        }
    }

    protected void ddlMes_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        grd.DataBind();
    }
}