﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Transactions;

public partial class admin_relatorioGeralPedidos : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            txtDataInicial.Attributes.Add("onkeypress", "Mascaras('DATA', '" + txtDataInicial.ClientID + "');");
            txtDataFinal.Attributes.Add("onkeypress", "Mascaras('DATA', '" + txtDataFinal.ClientID + "');");
        }
        fillGrid(false);
    }

    private class ItemPedido
    {        
        public int idItemPedidoEstoque { get; set; }
        public int produtoId { get; set; }
        public int pedidoId { get; set; }
        public bool cancelado { get; set; }
        public bool reservado { get; set; }
        public DateTime? dataReserva { get; set; }
        public DateTime? dataLimite { get; set; }
        public DateTime? dataCompra { get; set; }
        public decimal custo { get; set; }
    }

    private class Relatorio
    {
        public DateTime dataHoraDoPedido { get; set; }
        public int pedidoId { get; set; }
        public int clienteId { get; set; }
        public string situacao { get; set; }
        public decimal valorCobrado { get; set; }
        public string condicaoNome { get; set; }
        public int tentativaspagamento { get; set; }
        public decimal visa { get; set; }
        public decimal boleto { get; set; }
        public decimal mastercard { get; set; }
        public decimal diners { get; set; }
        public decimal amex { get; set; }
        public decimal elo { get; set; }
        public decimal aura { get; set; }
        public decimal hiper { get; set; }
        public decimal sorocred { get; set; }
        public decimal saldoanterior { get; set; }
        public decimal pagamentopendente { get; set; }
        public decimal pedidosAoFornecedor { get; set; }
        public decimal utilizacaoPedidoFornecedorAnterior { get; set; }
        public decimal utilizacaoEstoque { get; set; }
        public decimal custoitens { get; set; }
       public int pedidosPosteriores { get; set; }
    }

    private void fillGrid(bool rebind)
    {
        var dataInicial = DateTime.Now;
        var dataFinal = DateTime.Now;
        try
        {
            dataInicial = Convert.ToDateTime(txtDataInicial.Text);
            dataFinal = Convert.ToDateTime(txtDataFinal.Text);
            dataFinal = dataFinal.AddDays(1);
        }
        catch (Exception ex) { }

        using (var txn = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.ReadUncommitted }))
        {
            var data = new dbCommerceDataContext();
            data.CommandTimeout = 600000;
            var aguardandoGeral = (from c in data.tbItemPedidoEstoques
                                   where c.reservado == false && c.cancelado == false && c.dataLimite != null
                                   orderby c.dataLimite
                                   select new
                                   {
                                       c.idItemPedidoEstoque,
                                       c.tbItensPedido.pedidoId,
                                       c.dataCriacao,
                                       dataLimiteReserva = c.dataLimite,
                                       c.produtoId,
                                       c.itemPedidoId
                                   }).ToList();
            var pedidosFornecedorGeral = (from c in data.tbPedidoFornecedorItems
                                          join d in data.tbProdutoFornecedors on c.tbPedidoFornecedor.idFornecedor equals d.fornecedorId
                                          where (c.entregue == false | (c.entregue == true && (c.liberado ?? false) == false))
                                          orderby c.tbPedidoFornecedor.dataLimite, c.tbPedidoFornecedor.idPedidoFornecedor
                                          select new
                                          {
                                              c.idPedidoFornecedor,
                                              c.idPedidoFornecedorItem,
                                              dataLimiteFornecedor = c.tbPedidoFornecedor.dataLimite,
                                              c.idProduto,
                                              d.fornecedorNome,
                                              c.entregue,
                                              dataPedidoFornecedor = c.tbPedidoFornecedor.data,
                                              c.custo
                                          }).ToList();

            var itensPedidoEstoque = (from c in data.tbItemPedidoEstoques
                                      join d in data.tbProdutoEstoques on c.idItemPedidoEstoque equals d.idItemPedidoEstoqueReserva into produtoResevado
                                      where c.tbItensPedido.tbPedido.dataHoraDoPedido > dataInicial && c.tbItensPedido.tbPedido.dataHoraDoPedido < dataFinal select new ItemPedido
            {
                                          idItemPedidoEstoque = c.idItemPedidoEstoque,
                                          produtoId = c.produtoId,
                                          pedidoId = c.tbItensPedido.pedidoId,
                                          cancelado = c.cancelado,
                                          reservado = c.reservado,
                                          dataReserva = c.dataReserva,
                                          dataLimite = c.dataLimite,
                                          dataCompra = produtoResevado.Count() > 0 ? (DateTime?)produtoResevado.FirstOrDefault().tbPedidoFornecedorItem.tbPedidoFornecedor.data : null,
                                          custo = produtoResevado.Count() > 0 ? (produtoResevado.FirstOrDefault().tbPedidoFornecedorItem.custo) : (c.precoDeCusto ?? Convert.ToDecimal(0))
                                      }).ToList();

            foreach(var itemPedidoEstoque in itensPedidoEstoque)
            {
                var aguardando = (from c in aguardandoGeral
                                  where c.produtoId == itemPedidoEstoque.produtoId
                                  orderby c.dataLimiteReserva
                                  select c).ToList();
                var pedidoFornecedor = (from c in pedidosFornecedorGeral
                                        where c.idProduto == itemPedidoEstoque.produtoId
                                        orderby c.dataLimiteFornecedor, c.idPedidoFornecedor
                                        select c).ToList();
                var listaFinal = (from c in aguardando.Select((item, index) => new { item, index })
                                  join d in pedidoFornecedor.Select((item, index) => new { item, index }) on c.index equals d.index into
                                      pedidosFornecedorListaInterna
                                  from e in pedidosFornecedorListaInterna.DefaultIfEmpty()
                                  select new
                                  {
                                      c.item.idItemPedidoEstoque,
                                      c.item.pedidoId,
                                      c.item.dataCriacao,
                                      c.item.dataLimiteReserva,
                                      c.item.itemPedidoId,
                                      idPedidoFornecedor = e != null ? e.item.idPedidoFornecedor : 0,
                                      dataLimiteFornecedor = e != null ? (DateTime?)e.item.dataLimiteFornecedor : null,
                                      dataPedidoFornecedor = e != null ? (DateTime?)e.item.dataPedidoFornecedor : null,
                                      idPedidoFornecedorItem = e != null ? (int?)e.item.idPedidoFornecedorItem : null,
                                      custo = e != null ? e.item.custo : 0,
                                      fornecedorNome = e != null ? e.item.fornecedorNome : ""
                                  }
                    ).ToList();
                var itemNaLista = listaFinal.FirstOrDefault(x => x.idItemPedidoEstoque == itemPedidoEstoque.idItemPedidoEstoque);
                if (itemNaLista != null)
                {
                    itemPedidoEstoque.dataCompra = itemNaLista.dataPedidoFornecedor;
                    itemPedidoEstoque.custo = itemNaLista.custo;
                }
            }

            var pedidos = (from c in data.tbPedidos
                           where c.dataHoraDoPedido > dataInicial
                           select new
                           {
                               c.dataHoraDoPedido,
                               c.pedidoId,
                               c.clienteId,
                               c.tbPedidoSituacao.situacao,
                               c.valorCobrado,
                               c.tbCondicoesDePagamento.condicaoNome
                           }).ToList();

            var pags = (from c in data.tbPedidoPagamentos
                                where c.tbPedido.dataHoraDoPedido > dataInicial
                                select c).ToList();

            var pedidosGeral = (from c in pedidos
                                join pag in pags on c.pedidoId equals pag.pedidoId into pagamentos
                           where c.dataHoraDoPedido > dataInicial
                           select new
                           {
                               c.dataHoraDoPedido,
                               c.pedidoId,
                               c.clienteId,
                               c.situacao,
                               c.valorCobrado,
                               c.condicaoNome,
                               tentativaspagamento = pagamentos.Count(),
                               visa = pagamentos.Count(x => x.condicaoDePagamentoId == 3 && x.pago) > 0 ? pagamentos.Where(x => x.condicaoDePagamentoId == 3 && x.pago).Select(x => x.valor).Sum() : 0,
                               boleto = pagamentos.Count(x => x.condicaoDePagamentoId == 4 && x.pago) > 0 ? pagamentos.Where(x => x.condicaoDePagamentoId == 4 && x.pago).Select(x => x.valor).Sum() : 0,
                               mastercard = pagamentos.Count(x => x.condicaoDePagamentoId == 5 && x.pago) > 0 ? pagamentos.Where(x => x.condicaoDePagamentoId == 5 && x.pago).Select(x => x.valor).Sum() : 0,
                               diners = pagamentos.Count(x => x.condicaoDePagamentoId == 6 && x.pago) > 0 ? pagamentos.Where(x => x.condicaoDePagamentoId == 6 && x.pago).Select(x => x.valor).Sum() : 0,
                               amex = pagamentos.Count(x => x.condicaoDePagamentoId == 7 && x.pago) > 0 ? pagamentos.Where(x => x.condicaoDePagamentoId == 7 && x.pago).Select(x => x.valor).Sum() : 0,
                               elo = pagamentos.Count(x => x.condicaoDePagamentoId == 10 && x.pago) > 0 ? pagamentos.Where(x => x.condicaoDePagamentoId == 10 && x.pago).Select(x => x.valor).Sum() : 0,
                               aura = pagamentos.Count(x => x.condicaoDePagamentoId == 11 && x.pago) > 0 ? pagamentos.Where(x => x.condicaoDePagamentoId == 11 && x.pago).Select(x => x.valor).Sum() : 0,
                               hiper = pagamentos.Count(x => x.condicaoDePagamentoId == 14 && x.pago) > 0 ? pagamentos.Where(x => x.condicaoDePagamentoId == 14 && x.pago).Select(x => x.valor).Sum() : 0,
                               sorocred = pagamentos.Count(x => x.condicaoDePagamentoId == 22 && x.pago) > 0 ? pagamentos.Where(x => x.condicaoDePagamentoId == 22 && x.pago).Select(x => x.valor).Sum() : 0,
                               saldoanterior = pagamentos.Count(x => x.condicaoDePagamentoId == 12 && x.pago) > 0 ? pagamentos.Where(x => x.condicaoDePagamentoId == 12 && x.pago).Select(x => x.valor).Sum() : 0,
                               pagamentopendente = pagamentos.Count(x => x.pago == false && x.cancelado == false && x.pagamentoNaoAutorizado == false) > 0 ? pagamentos.Where(x => x.pago == false && x.cancelado == false && x.pagamentoNaoAutorizado == false).Select(x => x.valor).Sum() : 0,
                               pedidosAoFornecedor = 0,
                               utilizacaoPedidoFornecedorAnterior = 0,
                               utilizacaoEstoque = 0,
                               pedidosPosteriores = 0
                           }).ToList();

            var pedidosPeriodo = (from c in pedidosGeral where c.dataHoraDoPedido > dataInicial && c.dataHoraDoPedido < dataFinal
                                  join d in itensPedidoEstoque on c.pedidoId equals d.pedidoId into itens
                                  select new Relatorio
            {
                dataHoraDoPedido = (c.dataHoraDoPedido ?? DateTime.Now),
                pedidoId = c.pedidoId,
                clienteId = (c.clienteId ?? 0),
                situacao = c.situacao,
                valorCobrado = (c.valorCobrado ?? 0),
                condicaoNome = c.condicaoNome,
                tentativaspagamento = c.tentativaspagamento,
                visa = c.visa,
                boleto = c.boleto,
                mastercard = c.mastercard,
                diners = c.diners,
                amex = c.amex,
                elo = c.elo,
                aura = c.aura,
                hiper = c.hiper,
                sorocred = c.sorocred,
                saldoanterior = c.saldoanterior,
                pagamentopendente = c.pagamentopendente,
                pedidosAoFornecedor = itens.Where(x => x.dataCompra > c.dataHoraDoPedido).Count() > 0 ? itens.Where(x => x.dataCompra > c.dataHoraDoPedido).Sum(x => x.custo) : 0,
                utilizacaoPedidoFornecedorAnterior = itens.Where(x => x.dataCompra < c.dataHoraDoPedido && x.reservado == false).Count() > 0 ? itens.Where(x => x.dataCompra < c.dataHoraDoPedido && x.reservado == false).Sum(x => x.custo) : 0,
                utilizacaoEstoque = itens.Where(x => x.dataCompra < c.dataHoraDoPedido && x.reservado == true).Count() > 0 ? itens.Where(x => x.dataCompra < c.dataHoraDoPedido && x.reservado == true).Sum(x => x.custo) : 0,
                pedidosPosteriores = (from d in pedidosGeral where d.clienteId == c.clienteId && d.dataHoraDoPedido > c.dataHoraDoPedido select c).Count()
            }).ToList();

            string teste = "";



            grd.DataSource = pedidosPeriodo;
            if (!Page.IsPostBack | rebind)
            {
                grd.DataBind();
            }
        }

    }
    protected void imbInsert_Click(object sender, ImageClickEventArgs e)
    {
        fillGrid(true);
    }
    protected void btPdf_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WritePdfToResponse();
    }
    protected void btXsl_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteXlsToResponse();
    }
    protected void btRtf_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteRtfToResponse();
    }
    protected void btCsv_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteCsvToResponse();
    }
}