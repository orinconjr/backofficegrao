﻿<%@ Page Title="" Theme="Glass" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="carrinhosEnderecamento.aspx.cs" Inherits="admin_carrinhosEnderecamento" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxTimer" TagPrefix="dxt" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxPopupControl" Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxLoadingPanel" Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxCallback" Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxEditors" Assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" Runat="Server">
    
    

    <table cellpadding="0" cellspacing="0" style="width: 920px">
    <tr>
        <td class="tituloPaginas" valign="top">
            <asp:Label ID="lblAcao" runat="server">Carrinhos de Endereçamento</asp:Label>
        </td>
    </tr>
        
    <tr>
        <td>
            
    <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
        <tr>
            <td class="rotulos" style="padding-bottom: 20px;">
                Centro de Distribuição: 
                <asp:RadioButton runat="server" ID="rdbCd1" Checked="true" Text="CD 1" GroupName="groupdCds" AutoPostBack="true"  OnCheckedChanged="rdbCd1_CheckedChanged"/>
                <asp:RadioButton runat="server" ID="rdbCd2" Text="CD 2" GroupName="groupdCds" AutoPostBack="true"  OnCheckedChanged="rdbCd2_CheckedChanged" />
                <asp:RadioButton runat="server" ID="rdbCd3" Text="CD 3" GroupName="groupdCds" AutoPostBack="true"  OnCheckedChanged="rdbCd3_CheckedChanged" />
                <asp:RadioButton runat="server" ID="rdbCd4" Text="CD 4" GroupName="groupdCds" AutoPostBack="true"  OnCheckedChanged="rdbCd4_CheckedChanged" />
                <asp:RadioButton runat="server" ID="rdbCd5" Text="CD 5" GroupName="groupdCds" AutoPostBack="true"  OnCheckedChanged="rdbCd5_CheckedChanged" />
            </td>
        </tr>
        <tr>
            <td>
                Filtro: <asp:RadioButton runat="server" ID="rdbAberto" Text="Em aberto" GroupName="filtro" OnCheckedChanged="rdbAberto_OnCheckedChanged" AutoPostBack="True" Checked="True" /> - <asp:RadioButton runat="server" ID="rdbTodos" Text="Todos" GroupName="filtro" OnCheckedChanged="rdbAberto_OnCheckedChanged" AutoPostBack="True"/>
            </td>
        </tr>

        <tr>
            <td>
                <dxwgv:ASPxGridView ID="grd" runat="server" AutoGenerateColumns="False" Width="834px" Cursor="auto" KeyFieldName="idTransferenciaEndereco" EnableViewState="False">
                    <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                    <Styles>
                        <Footer Font-Bold="True">
                        </Footer>
                    </Styles>
                    <SettingsText ConfirmDelete="Tem certeza que deseja excluir?" emptydatarow="Nenhum registro encontrado." />
                    <SettingsPager Position="TopAndBottom" PageSize="50" ShowDisabledButtons="False" AlwaysShowPager="True">
                        <Summary AllPagesText="Páginas: {0} - {1} ({2} items)" Text="Página {0} de {1} ({2} registros encontrados)" />
                    </SettingsPager>
                    <settings showfilterrow="True" ShowGroupButtons="False" ShowHeaderFilterButton="True" ShowFooter="True" />
                    <SettingsEditing EditFormColumnCount="4" PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1" PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter" PopupEditFormWidth="700px" />
                    <Columns>
                        <dxwgv:GridViewDataTextColumn Caption="#" VisibleIndex="0" Width="50">
                            <DataItemTemplate>
                                <%# Container.VisibleIndex + 1 %>
                            </DataItemTemplate>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="ID" FieldName="idTransferenciaEndereco" VisibleIndex="0" Width="50">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Carrinho" FieldName="enderecamentoTransferencia" VisibleIndex="0">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Usuario Entrada" FieldName="usuarioEntrada" VisibleIndex="0">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataDateColumn Caption="Data Cadastro" FieldName="dataCadastro" VisibleIndex="0">
                        </dxwgv:GridViewDataDateColumn>
                        <dxwgv:GridViewDataDateColumn Caption="Data Inicio" FieldName="dataInicio" VisibleIndex="0">
                        </dxwgv:GridViewDataDateColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Usuario Endereçando" FieldName="usuarioEnderecamento" VisibleIndex="0">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Total Itens" FieldName="totalItens" VisibleIndex="0">
                        </dxwgv:GridViewDataTextColumn> 
                        <dxwgv:GridViewDataTextColumn Caption="Pendentes" FieldName="faltandoEnderecar" VisibleIndex="0">
                        </dxwgv:GridViewDataTextColumn>                           
                        <dxwgv:GridViewDataHyperLinkColumn Caption="Visualizar" FieldName="idTransferenciaEndereco" VisibleIndex="7" Width="30px">
                            <PropertiesHyperLinkEdit  Text="Produtos" NavigateUrlFormatString="carrinhoEnderecamentoDetalhe.aspx?id={0}">
                            </PropertiesHyperLinkEdit>
                            <Settings AllowAutoFilter="False" />
                        </dxwgv:GridViewDataHyperLinkColumn>
                    </Columns>
                    <StylesEditors>
                        <Label Font-Bold="True">
                        </Label>
                    </StylesEditors>
                </dxwgv:ASPxGridView>         
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
    </table>
        </td>
    </tr>
</table>
</asp:Content>

