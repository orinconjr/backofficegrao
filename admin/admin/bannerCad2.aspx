﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="bannerCad2.aspx.cs" Theme="Glass" Inherits="admin_bannerCad" %>

<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxEditors" Assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" runat="Server">
    <asp:ScriptManager runat="server"></asp:ScriptManager>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script>

        function previewFileUpload(input) {

            var img = $("#ctl00_conteudoPrincipal_imgBanner");

            if (input.files && input.files[0]) {

                var reader = new FileReader();
                reader.onload = function (e) {
                    $("#ctl00_conteudoPrincipal_imgBanner").attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

    </script>

    <table cellpadding="0" cellspacing="0" style="width: 920px">
        <tr>
            <td class="tituloPaginas" valign="top">
                <asp:Label ID="lblAcao" runat="server">Banner Cadastro</asp:Label>
            </td>
        </tr>
        <tr class="rotulos">
            <td style="padding-left: 36px;">
                <asp:Image ID="imgBanner" runat="server" AlternateText="Banner" Style="max-width: 860px;" /><br />
                Banner<br />
                <asp:FileUpload ID="FileUploadBanner" runat="server" onchange="previewFileUpload(this)" Width="97%" ValidationGroup="cadastro" /><br />
                <asp:RequiredFieldValidator ID="rfvArquivo" runat="server" ValidationGroup="cadastro"
                    ControlToValidate="FileUploadBanner" ErrorMessage="Informe o arquivo do banner!">
                </asp:RequiredFieldValidator><br />
                Link do Produto (opcional)<br />
                <asp:TextBox ID="txtLinkProduto" runat="server" Width="97%" MaxLength="300"></asp:TextBox><br />
                ID do Produto (opcional)<br />
                <asp:TextBox ID="txtIdProduto" runat="server" onkeypress="return soNumeroDigito(event);" MaxLength="10"></asp:TextBox><br />
                Local<br />
                <asp:DropDownList ID="ddlLocal" runat="server" Width="172" Height="22">
                    <asp:ListItem Text="Home" Value="1" />
                    <asp:ListItem Text="Detalhes Produto" Value="2" />
                    <asp:ListItem Text="Busca" Value="3" />
                    <asp:ListItem Text="Lista Categoria" Value="4" />
                </asp:DropDownList><br />
                Posição<br />
                <asp:DropDownList ID="ddlPosicao" runat="server" Width="172" Height="22">
                    <asp:ListItem Text="Topo" Value="1" />
                    <asp:ListItem Text="Direita" Value="2" />
                    <asp:ListItem Text="Rodapé" Value="3" />
                    <asp:ListItem Text="Esquerda" Value="4" />
                    <asp:ListItem Text="Conteudo Home - Topo" Value="5" />
                    <asp:ListItem Text="Conteudo Home - Rodapé" Value="6" />
                    <asp:ListItem Text="Topo Split" Value="7" />
                </asp:DropDownList><br />
                <div style="display: none;">
                    Filtro
                 <asp:RadioButtonList ID="rblFiltro" runat="server" RepeatDirection="Horizontal">
                     <asp:ListItem Text="Menino" Value="692"></asp:ListItem>
                     <asp:ListItem Text="Menina" Value="693"></asp:ListItem>
                     <asp:ListItem Text="Kit Berço" Value="413"></asp:ListItem>
                     <asp:ListItem Text="Quarto Completo" Value="412"></asp:ListItem>
                     <asp:ListItem Text="Quarto Completo Sem Cama Baba" Value="679"></asp:ListItem>
                     <asp:ListItem Text="Berços e Minicamas" Value="1001"></asp:ListItem>
                     <asp:ListItem Text="Colchão" Value="990"></asp:ListItem>
                     <asp:ListItem Text="Nenhum" Value="0"></asp:ListItem>
                 </asp:RadioButtonList><br />
                </div>
                <br />
                Filtro
                <div style="overflow: scroll; width: 98%; height: 250px; border: 1px solid #ccc;">
                    <asp:TreeView ID="treeCategorias" runat="server" Font-Names="Tahoma" Font-Size="12px"
                        OnTreeNodePopulate="TreeView1_TreeNodePopulate" ShowCheckBoxes="Leaf" ShowLines="True">
                    </asp:TreeView>
                </div>
                <div style="float: left;">
                    <asp:CheckBox ID="ckbAtivo" Text="Ativo" runat="server" /><br />
                    <asp:CheckBox ID="ckbBannerMobile" Text="Mobile" runat="server" /><br />
                    <asp:CheckBox ID="ckbEmTeste" Text="Mostrar somente em teste.graodegente.com.br" runat="server" /><br />
                    <asp:CheckBox ID="ckbAgendamentoAtivacao" Text="Agendar Ativação" runat="server" /><br />
                </div>
                <br />
                <br />
                <div style="float: right; width: 400px;">
                    Relevancia Ex.:(0,1 - 0,5 - 1) valor mínimo 0,1 valor máximo 1<br />
                    Quanto menor o numero, mais chances o banner tem de ser mostrado<br />
                    <asp:TextBox ID="txtRelevancia" runat="server" MaxLength="3" Width="50px" Style="text-align: center"></asp:TextBox>
                    <asp:RangeValidator ID="RangeValidator1" runat="server" MinimumValue="0,1" MaximumValue="1" ControlToValidate="txtRelevancia"
                        ErrorMessage="Valores de relevância devem estar entre 0,1 e 1" ValidationGroup="cadastro"></asp:RangeValidator>
                </div>
                <div style="width: 400px;">
                    <div style="float: left; width: 370px; padding: 6px 0 0 20px;">
                        <div style="float: left; width: 186px;">Data:</div>
                        <div style="float: left;">Hora:</div>
                        <dx:ASPxDateEdit ID="txtAgendamentoDataAtivacao" runat="server" Style="float: left;">
                        </dx:ASPxDateEdit>
                        <dx:ASPxTimeEdit ID="txtAgendamentoHoraAtivacao" runat="server" Style="float: left; margin-left: 15px;">
                        </dx:ASPxTimeEdit>
                    </div>
                </div>
                <div style="float: left; width: 500px; margin-top: 10px;">
                    <asp:CheckBox ID="ckbAgendamentoDesativacao" Text="Agendar Desativação" runat="server" /><br />
                </div>
                <div style="width: 400px;">
                    <div style="float: left; width: 370px; padding: 6px 0 0 20px;">
                        <div style="float: left; width: 186px;">Data:</div>
                        <div style="float: left;">Hora:</div>
                        <dx:ASPxDateEdit ID="txtAgendamentoDataDesativacao" runat="server" Style="float: left;">
                        </dx:ASPxDateEdit>
                        <dx:ASPxTimeEdit ID="txtAgendamentoHoraDesativacao" runat="server" Style="float: left; margin-left: 15px;">
                        </dx:ASPxTimeEdit>
                    </div>
                </div>
                <asp:ValidationSummary runat="server" ShowMessageBox="true" ValidationGroup="cadastro" ShowSummary="false" />
                
                <br />
                <div style="float: left; width: 500px; margin-top: 10px;">
                    <asp:CheckBox ID="ckbContador" Text="Contador" runat="server" /><br />
                </div>
                <div style="width: 400px;">
                    <div style="float: left; width: 370px; padding: 6px 0 15px 20px;">
                        <div style="float: left; width: 186px;">Data Início:</div>
                        <div style="float: left;">Hora Início:</div>
                        <dx:ASPxDateEdit ID="txtInicioContadorData" runat="server" Style="float: left;">
                        </dx:ASPxDateEdit>
                        <dx:ASPxTimeEdit ID="txtInicioContadorHora" runat="server" Style="float: left; margin-left: 15px;">
                        </dx:ASPxTimeEdit>

                        <div style="float: left; width: 186px;">Data Fim:</div>
                        <div style="float: left;">Hora Fim:</div>
                        <dx:ASPxDateEdit ID="txtFimContadorData" runat="server" Style="float: left;">
                        </dx:ASPxDateEdit>
                        <dx:ASPxTimeEdit ID="txtFimContadorHora" runat="server" Style="float: left; margin-left: 15px;">
                        </dx:ASPxTimeEdit>
                    </div>
                    <br />
                    <div style="width: 370px; padding: 6px 0 0 20px;">
                        <asp:UpdatePanel runat="server">
                            <ContentTemplate>
                                <div style="float: left; width: 48px;">Estilo:</div>
                                <div style="float: left; width: 305px;">
                                    <asp:DropDownList runat="server" ID="ddlEstiloContador" DataTextField="nome" DataValueField="id" Style="float: left;" Width="306"
                                        OnSelectedIndexChanged="ddlEstiloContador_OnSelectedIndexChanged" AutoPostBack="True" />
                                </div>
                                <div style="float: left; width: 48px; padding-top: 5px;">Layout:</div>
                                <div style="float: left; width: 305px; padding-top: 5px;">
                                    <asp:DropDownList runat="server" ID="ddlLayoutContador" DataTextField="nome" DataValueField="id" Style="float: left;" Width="306"/>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td style="text-align: right; padding-right: 23px;">
                <asp:ImageButton ID="btnSalvar" runat="server" ImageUrl="images/btSalvar.jpg" AlternateText="Salvar" ValidationGroup="cadastro" OnClick="btnSalvar_Click" />
            </td>
        </tr>
    </table>
</asp:Content>

