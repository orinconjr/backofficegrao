﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_enviarDanfeNotas : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            fillGrid(true);
        }
    }

    private void fillGrid(bool bind)
    {
        var pedidosDc = new dbCommerceDataContext();
        var pedidos = (from c in pedidosDc.tbPedidos where c.nfeNumero != null && c.danfe == null && c.nfeNumero > 1831 select c);
        lstProdutos.DataSource = pedidos;
        if(bind) lstProdutos.DataBind();
    }

    protected void btnGravarDanfes_OnClick(object sender, EventArgs e)
    {
        foreach (var item in lstProdutos.Items)
        {
            HiddenField hdfIdPedido = (HiddenField) item.FindControl("hdfIdPedido");
            FileUpload fluDanfe = (FileUpload)item.FindControl("fluDanfe");
            int idPedido = Convert.ToInt32(hdfIdPedido.Value);
            if (fluDanfe.HasFile == true)
            {
                var pedidoDc = new dbCommerceDataContext();
                var pedido = (from c in pedidoDc.tbPedidos where c.pedidoId == idPedido select c).First();
                fluDanfe.PostedFile.SaveAs(System.Configuration.ConfigurationManager.AppSettings["caminhoFisico"] + "\\admin\\notas\\danfes\\" + pedido.nfeNumero + ".pdf");
                pedido.danfe = pedido.nfeNumero + ".pdf";
                pedidoDc.SubmitChanges();
            }
        }
        fillGrid(true);
    }

    protected void lstProdutos_OnItemDataBound(object sender, ListViewItemEventArgs e)
    {
        
    }
}