﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using DevExpress.Data.Filtering;
using DevExpress.Web.ASPxGridView;
using System.Collections.Generic;

public partial class admin_relatorioCustoPedido : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            txtDataInicial.Attributes.Add("onkeypress", "Mascaras('DATA', '" + txtDataInicial.ClientID + "');");
            txtDataFinal.Attributes.Add("onkeypress", "Mascaras('DATA', '" + txtDataFinal.ClientID + "');");
        }
        fillGrid(false);
    }

    protected void btPdf_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WritePdfToResponse();
    }
    protected void btXsl_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteXlsToResponse();
    }
    protected void btRtf_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteRtfToResponse();
    }
    protected void btCsv_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteCsvToResponse();
    }

    protected void grd_OnProcessColumnAutoFilter(object sender, ASPxGridViewAutoFilterEventArgs e)
    {
        if (e.Column.FieldName == "pedidoIdCliente")
        {

            if (e.Kind == GridViewAutoFilterEventKind.CreateCriteria)
            {
                Session["pedidoIdCliente"] = e.Value;
                int pedidoId = rnFuncoes.retornaIdInterno(Convert.ToInt32(e.Value));
                e.Criteria = (new OperandProperty("pedidoId") == pedidoId);

            }
            else
            {
                if (Session["pedidoIdCliente"] != null)
                    e.Value = null;
            }

        }
    }

    protected void grd_OnCustomUnboundColumnData(object sender, ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "pedidoIdCliente")
        {
            int pedidoId = Convert.ToInt32(e.GetListSourceFieldValue("pedidoId"));
            e.Value = rnFuncoes.retornaIdCliente(pedidoId);
        }
    }

    protected void imbInsert_Click(object sender, ImageClickEventArgs e)
    {
        fillGrid(true);
    }

    private void fillGrid(bool rebind)
    {
        var dataInicial = DateTime.Now;
        var dataFinal = DateTime.Now;
        try
        {
            dataInicial = Convert.ToDateTime(txtDataInicial.Text);
            dataFinal = Convert.ToDateTime(txtDataFinal.Text);
            dataFinal = dataFinal.AddDays(1);
        }
        catch(Exception ex) { }

        if (rdbDataPedido.Checked)
        {
            var listaStatusValidos = new List<int>() { 5 };

            var data = new dbCommerceDataContext();
            var itensReservados = (from c in data.tbItemPedidoEstoques
                                   join d in data.tbProdutoEstoques on c.idItemPedidoEstoque equals d.idItemPedidoEstoqueReserva
                                   where c.tbItensPedido.tbPedido.dataHoraDoPedido >= dataInicial && c.tbItensPedido.tbPedido.dataHoraDoPedido <= dataFinal && c.reservado == true && listaStatusValidos.Contains(c.tbItensPedido.tbPedido.statusDoPedido)
                                   select new { pedidoId = c.tbItensPedido.pedidoId, custo = d.tbPedidoFornecedorItem.custo }).ToList();

            var itensNaoReservados = (from c in data.tbItemPedidoEstoques
                                      where c.tbItensPedido.tbPedido.dataHoraDoPedido >= dataInicial && c.tbItensPedido.tbPedido.dataHoraDoPedido <= dataFinal && c.reservado == false && listaStatusValidos.Contains(c.tbItensPedido.tbPedido.statusDoPedido)
                                      select new { pedidoId = c.tbItensPedido.pedidoId, custo = c.tbProduto.produtoPrecoDeCusto }).ToList();

            var totaisPagos = (from c in data.tbPedidoPagamentos
                               where c.tbPedido.dataHoraDoPedido >= dataInicial && c.tbPedido.dataHoraDoPedido <= dataFinal && c.pago == true && listaStatusValidos.Contains(c.tbPedido.statusDoPedido)
                               select new { pedidoId = c.pedidoId, valor = c.valor }).ToList();

            var envios = (from c in data.tbPedidoEnvios
                          join d in data.tbTipoDeEntregas on c.tipoDeEntregaId equals d.tipoDeEntregaId
                          where c.tbPedido.dataHoraDoPedido >= dataInicial && c.tbPedido.dataHoraDoPedido <= dataFinal && listaStatusValidos.Contains(c.tbPedido.statusDoPedido)
                          select new { pedidoId = c.tbPedido.pedidoId, custo = c.valorSelecionado, d.tipoDeEntregaNome }).ToList();


            var pedidos = (from c in data.tbPedidos
                           where c.dataHoraDoPedido >= dataInicial && c.dataHoraDoPedido <= dataFinal && listaStatusValidos.Contains(c.statusDoPedido)
                           select new
                           {
                               c.pedidoId,
                               c.endEstado,
                               c.dataHoraDoPedido,
                               c.valorCobrado,
                               c.valorDoFrete
                           }).ToList();
            var relatorioMontagem = (from c in pedidos
                                     join d in itensReservados on c.pedidoId equals d.pedidoId into itensReserva
                                     join e in itensNaoReservados on c.pedidoId equals e.pedidoId into itensSemReserva
                                     join f in envios on c.pedidoId equals f.pedidoId into valoresEnvio
                                     join g in totaisPagos on c.pedidoId equals g.pedidoId into valoresPagos
                                     select new
                                     {
                                         c.pedidoId,
                                         c.endEstado,
                                         c.dataHoraDoPedido,
                                         valorCobrado = valoresPagos.Select(x => x.valor).DefaultIfEmpty(1).Sum(),
                                         c.valorDoFrete,
                                         valorCusto = itensReserva.Select(x => x.custo).DefaultIfEmpty(0).Sum() + itensSemReserva.Select(x => x.custo).DefaultIfEmpty(0).Sum(),
                                         valorEnvio = valoresEnvio.Select(x => x.custo).DefaultIfEmpty(0).Sum(),
                                         transportadora = valoresEnvio.Select(x=> x.tipoDeEntregaNome).FirstOrDefault()
                                     }).ToList();
            var relatorio = (from c in relatorioMontagem
                             select new
                             {
                                 c.pedidoId,
                                 c.endEstado,
                                 c.dataHoraDoPedido,
                                 c.valorCobrado,
                                 c.valorDoFrete,
                                 c.valorCusto,
                                 c.valorEnvio,
                                 pCustoProduto = Decimal.Round((decimal)((c.valorCusto * 100) / c.valorCobrado), 2),
                                 pCustoFrete = Decimal.Round((decimal)((c.valorEnvio * 100) / c.valorCobrado), 2),
                                 pCustoFretePago = Decimal.Round((decimal)(((c.valorEnvio - c.valorDoFrete) * 100) / c.valorCobrado), 2),
                                 pCustoTotal = Decimal.Round((decimal)(((c.valorCusto + c.valorEnvio - c.valorDoFrete) * 100) / c.valorCobrado), 2),
                                 transportadora = c.transportadora
                                 
                             });
            grd.DataSource = relatorio;
            if ((!Page.IsPostBack && !Page.IsCallback) | rebind)
            {
                grd.DataBind();
                if (relatorio.Select(x => x.valorCobrado).DefaultIfEmpty(0).Sum() > 0)
                {
                    litPProduto.Text = Decimal.Round((decimal)((relatorio.Select(x => x.valorCusto).DefaultIfEmpty(0).Sum() * 100) / relatorio.Select(x => x.valorCobrado).DefaultIfEmpty(0).Sum()), 2).ToString();
                    litPFrete.Text = Decimal.Round((decimal)((relatorio.Select(x => x.valorEnvio).DefaultIfEmpty(0).Sum() * 100) / relatorio.Select(x => x.valorCobrado).DefaultIfEmpty(0).Sum()), 2).ToString();
                    litPFretePago.Text = Decimal.Round((decimal)(((relatorio.Select(x => x.valorEnvio).DefaultIfEmpty(0).Sum() - relatorio.Select(x => x.valorDoFrete).DefaultIfEmpty(0).Sum()) * 100) / relatorio.Select(x => x.valorCobrado).DefaultIfEmpty(0).Sum()), 2).ToString();
                }
            }
        }
        else
        {
            var listaStatusValidos = new List<int>() { 5 };

            var data = new dbCommerceDataContext();
            var itensReservados = (from c in data.tbItemPedidoEstoques
                                   join d in data.tbProdutoEstoques on c.idItemPedidoEstoque equals d.idItemPedidoEstoqueReserva
                                   where d.tbPedidoEnvio.dataFimEmbalagem >= dataInicial && d.tbPedidoEnvio.dataFimEmbalagem <= dataFinal && c.reservado == true && listaStatusValidos.Contains(c.tbItensPedido.tbPedido.statusDoPedido)
                                   select new { pedidoId = c.tbItensPedido.pedidoId, custo = d.tbPedidoFornecedorItem.custo }).Distinct().ToList();

            var totaisPagos = (from c in data.tbProdutoEstoques
                               join d in data.tbPedidoPagamentos on c.pedidoId equals d.pedidoId
                               where c.tbPedidoEnvio.dataFimEmbalagem >= dataInicial && c.tbPedidoEnvio.dataFimEmbalagem <= dataFinal && listaStatusValidos.Contains(c.tbPedido.statusDoPedido) && d.pago == true 
                               select new { pedidoId = c.pedidoId, valor = d.valor }).Distinct().ToList();

            var envios = (from c in data.tbPedidoEnvios
                          join d in data.tbTipoDeEntregas on c.tipoDeEntregaId equals d.tipoDeEntregaId
                          where c.dataFimEmbalagem >= dataInicial && c.dataFimEmbalagem <= dataFinal && listaStatusValidos.Contains(c.tbPedido.statusDoPedido)
                          select new { pedidoId = c.tbPedido.pedidoId, custo = c.valorSelecionado, d.tipoDeEntregaNome }).ToList();


            var pedidos = (from c in data.tbPedidoEnvios
                           join d in data.tbPedidos on c.idPedido equals d.pedidoId
                           where c.dataFimEmbalagem >= dataInicial && c.dataFimEmbalagem <= dataFinal && listaStatusValidos.Contains(c.tbPedido.statusDoPedido)
                           select new
                           {
                               d.pedidoId,
                               d.endEstado,
                               d.dataHoraDoPedido,
                               d.valorCobrado,
                               d.valorDoFrete
                           }).Distinct().ToList();
            var relatorioMontagem = (from c in pedidos
                                     join d in itensReservados on c.pedidoId equals d.pedidoId into itensReserva
                                     join f in envios on c.pedidoId equals f.pedidoId into valoresEnvio
                                     join g in totaisPagos on c.pedidoId equals g.pedidoId into valoresPagos
                                     select new
                                     {
                                         c.pedidoId,
                                         c.endEstado,
                                         c.dataHoraDoPedido,
                                         valorCobrado = valoresPagos.Select(x => x.valor).DefaultIfEmpty(1).Sum(),
                                         c.valorDoFrete,
                                         valorCusto = itensReserva.Select(x => x.custo).DefaultIfEmpty(0).Sum(),
                                         valorEnvio = valoresEnvio.Select(x => x.custo).DefaultIfEmpty(0).Sum(),
                                         transportadora = valoresEnvio.Select(x => x.tipoDeEntregaNome).FirstOrDefault()
                                     }).Distinct().ToList();
            var relatorio = (from c in relatorioMontagem
                             select new
                             {
                                 c.pedidoId,
                                 c.endEstado,
                                 c.dataHoraDoPedido,
                                 c.valorCobrado,
                                 c.valorDoFrete,
                                 c.valorCusto,
                                 c.valorEnvio,
                                 pCustoProduto = Decimal.Round((decimal)((c.valorCusto * 100) / c.valorCobrado), 2),
                                 pCustoFrete = Decimal.Round((decimal)((c.valorEnvio * 100) / c.valorCobrado), 2),
                                 pCustoFretePago = Decimal.Round((decimal)(((c.valorEnvio - c.valorDoFrete) * 100) / c.valorCobrado), 2),
                                 pCustoTotal = Decimal.Round((decimal)(((c.valorCusto + c.valorEnvio - c.valorDoFrete) * 100) / c.valorCobrado), 2),
                                 transportadora = c.transportadora
                             });
            grd.DataSource = relatorio;
            if ((!Page.IsPostBack && !Page.IsCallback) | rebind)
            {
                grd.DataBind();
                if (relatorio.Select(x => x.valorCobrado).DefaultIfEmpty(0).Sum() > 0)
                {
                    litPProduto.Text = Decimal.Round((decimal)((relatorio.Select(x => x.valorCusto).DefaultIfEmpty(0).Sum() * 100) / relatorio.Select(x => x.valorCobrado).DefaultIfEmpty(0).Sum()), 2).ToString();
                    litPFrete.Text = Decimal.Round((decimal)((relatorio.Select(x => x.valorEnvio).DefaultIfEmpty(0).Sum() * 100) / relatorio.Select(x => x.valorCobrado).DefaultIfEmpty(0).Sum()), 2).ToString();
                    litPFretePago.Text = Decimal.Round((decimal)(((relatorio.Select(x => x.valorEnvio).DefaultIfEmpty(0).Sum() - relatorio.Select(x => x.valorDoFrete).DefaultIfEmpty(0).Sum()) * 100) / relatorio.Select(x => x.valorCobrado).DefaultIfEmpty(0).Sum()), 2).ToString();
                }
            }
        }
    }
    protected void grd_CustomSummaryCalculate(object sender, DevExpress.Data.CustomSummaryEventArgs e)
    {
        var valorCobrado = e.GetValue("valorCobrado");
        var valorCusto = e.GetValue("valorCusto");
        //e.TotalValue = "Qtd. Entrega Faltando " + (Convert.ToInt32(previstos) - Convert.ToInt32(entregues));
    }
}
