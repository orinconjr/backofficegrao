﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="relatorioProdutosAdicionadosManualmente2.aspx.cs" Inherits="admin_relatorioProdutosAdicionadosManualmente2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" runat="Server">
    <script src="js/jquery.min.js"></script>
    <style>
        .meubotao{
            cursor:pointer;
            font:bold 14px tahoma;
        }
        .fieldsetatualizarprecos {
            width: 848px;
            float: left;
            clear: left;
            border-radius: 5px;
            margin-left: 25px;
            font:bold 14px tahoma;
        }

            .fieldsetatualizarprecos input[type=checkbox] {
                margin-top: 15px;
            }

            .fieldsetatualizarprecos label {
                margin-left: 5px;
                margin-right: 20px;
            }

        .meugrid {
            background-color: #F4FAFB;
            border: 0px none;
            /* border-collapse: separate !important; */
            overflow: hidden;
            font: 9pt Tahoma;
            color: #000;
            cursor: pointer;
            white-space: nowrap;
            font-weight: normal;
            text-align: left;
            width: 834px;
            border-collapse: collapse;
            float: left;
            clear: left;
            margin-top: 10px;
            margin-bottom: 10px;
            min-height: 500px;
            margin-left: 25px;
        }

            .meugrid th {
                background-image: url(../../App_Themes/Glass/GridView/gvHeaderBackground.gif);
                background-position: center top;
                background-repeat: repeat-x;
                background-color: #C0DDE0;
                overflow: hidden;
                height: 23px !important;
                line-height: 23px !important;
                padding: 4px 6px 5px;
                font-weight: normal;
                border: 1px solid #7EACB1;
                /* border-collapse: separate;*/
            }

            .meugrid td {
                overflow: hidden;
                height: 23px !important;
                line-height: 23px !important;
                padding: 4px 6px 5px;
                border: 1px solid #7EACB1;
            }

            .meugrid tr td:last-of-type {
                text-align: center;
            }

        .tabrotulos {
            width: 848px;
            margin: auto;
        }

        .materiaprima {
            width: 250px;
        }

            .materiaprima ul {
                margin: 0px;
                padding-left: 10px;
            }

                .materiaprima ul li {
                    max-width: 230px;
                    white-space: pre-wrap;
                }

        .meugrid tr td:last-of-type {
            text-align: center;
        }

        .nome {
            width: 237px;
        }

            .nome div {
                max-width: 230px;
                white-space: pre-wrap;
            }

        .rotulos td {
            font: bold 12px tahoma;
            /*padding-top: 20px; */
            color: rgb(65, 105, 126);
            padding-left: 5px;
            /*padding-bottom: 5px;*/
        }

    </style>
    <div class="tituloPaginas" valign="top">
        Produtos adicionados manualmente
    </div>
    <div>
        <fieldset class="fieldsetatualizarprecos">
            <legend style="font-weight: bold;">Filtro</legend>

            <table class="tabrotulos">
                <tr class="rotulos">
                    <td>Num. Pedido:<br />
                        <asp:TextBox runat="server" ID="txtNumPedido"></asp:TextBox>
                    </td>

                    <td>&nbsp;
                    </td>

                    <td>Motivo:<br />
                        <asp:DropDownList runat="server" ID="ddlMotivoAdicionarItem" Width="370px" CssClass="campos">
                            <Items>
                                <asp:ListItem Text="Selecione" Value="0"></asp:ListItem>
                                <asp:ListItem Text="Inclusão de presente por atraso" Value="inclusao de presente por atraso"></asp:ListItem>
                                <asp:ListItem Text="Inclusão de presente por reclamação" Value="inclusao de presente por reclamacao"></asp:ListItem>
                                <asp:ListItem Text="Inclusão por defeito - fornecedor" Value="inclusao por defeito"></asp:ListItem>
                                <asp:ListItem Text="Inclusão por defeito - transporte" Value="inclusao por defeito transporte"></asp:ListItem>
                                <asp:ListItem Text="Inclusão feita pelo cliente" Value="inclusao feita pelo cliente"></asp:ListItem>
                                <asp:ListItem Text="Inclusão de produto por esquecimento de envio da expedição" Value="inclusao de produto por esquecimento de envio da expedicao"></asp:ListItem>
                                <asp:ListItem Text="Inclusão de produto por erro de cadastro" Value="inclusao de produto por erro de cadastro"></asp:ListItem>
                                <asp:ListItem Text="Inclusão de produto por extravio de volume" Value="inclusao de produto por extravio de volume"></asp:ListItem>
                                <asp:ListItem Text="Inclusão de produto por personalização" Value="Inclusao de produto por personalizacao"></asp:ListItem>
                                <asp:ListItem Text="Inclusão por produto fora de linha" Value="inclusao por produto fora de linha"></asp:ListItem>
                                <asp:ListItem Text="Inclusão de produto por erro expedição" Value="Inclusão de produto por erro expedição"></asp:ListItem>
                                <asp:ListItem Text="Inclusão por Sinistro" Value="inclusao por Sinistro"></asp:ListItem>
                                <asp:ListItem Text="Atraso por entrega do fornecedor" Value="atraso por entrega do fornecedor"></asp:ListItem>
                                <asp:ListItem Text="Presente para fechamento do pedido" Value="presente para fechamento do pedido"></asp:ListItem>
                                <asp:ListItem Text="Presente contato telefônico primeira compra" Value="Presente contato telefonico primeira compra"></asp:ListItem>
                                <asp:ListItem Text="Reenvio por troca de transportadora/devolução" Value="Reenvio por troca de transportadora devolucao"></asp:ListItem>
                            </Items>
                        </asp:DropDownList>
                    </td>

                </tr>
                <tr class="rotulos">

                    <td>Data Inicial:<br />
                        <asp:TextBox runat="server" ID="txtDataInicial"></asp:TextBox>
                    </td>
                    <td>Data Final:<br />
                        <asp:TextBox runat="server" ID="txtDataFinal"></asp:TextBox>
                    </td>
                    <td >
                        Ordenar por:<br />
                        <asp:DropDownList runat="server" ID="ddlordenarpor" Width="370px"  CssClass="campos">
                            <Items>
                                <asp:ListItem Text="Id do Pedido" Value="pedidoId"></asp:ListItem>
                                <asp:ListItem Text="Data" Value="dataDaCriacao"></asp:ListItem>
                                <asp:ListItem Text="Id do Produto" Value="produtoId"></asp:ListItem>
                                <asp:ListItem Text="Nome do Produto" Value="produtoNome"></asp:ListItem>
                                <asp:ListItem Text="Preço de Custo" Value="produtoPrecoDeCusto"></asp:ListItem>
                                <asp:ListItem Text="Motivo da Adição" Value="motivoAdicao"></asp:ListItem>
                            </Items>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td style="padding-top: 15px;text-align:right;">
                        <asp:Button runat="server" ID="btnFiltrar" CssClass="meubotao" Text="Filtrar" OnClick="btnFiltrar_Click" />
                    </td>
                </tr>

            </table>
        </fieldset>
       
        <div style="float:left;clear:left;width:868px;text-align:right;margin-left: 25px;margin-top:15px;">
              <asp:Button runat="server" ID="btnExportarPlanilha"  CssClass="meubotao"   Text="Exportar Planilha" OnClick="btnExportarPlanilha_Click" />
        </div>
    </div>
    <div align="center" style="min-height: 500px;">


        <asp:GridView ID="GridView1" CssClass="meugrid" runat="server" DataKeyNames="itemPedidoId" Width="834px" AutoGenerateColumns="False" AllowPaging="True" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowDataBound="GridView1_RowDataBound" PageSize="20">
            <Columns>
                <asp:BoundField DataField="dataDaCriacao" HeaderText="Data" />
                <asp:BoundField DataField="pedidoId" HeaderText="Id Pedido" />
                <asp:BoundField DataField="produtoPrecoDeCusto" DataFormatString=" {0:C}" HeaderText="Preço de custo" />
                <asp:TemplateField HeaderText="VL 1ª NF" ShowHeader="False">
                    <ItemTemplate>
                        <asp:Label ID="lblvlNota" runat="server" Text="Label"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Frete 1ª NF" ShowHeader="False">
                    <ItemTemplate>
                        <asp:Label ID="lblvlFrete" runat="server" Text="Label"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="motivoAdicao"  HeaderText="Motivo da Adição" />


                <asp:TemplateField HeaderText="Transportadora" ShowHeader="False">
                    <ItemTemplate>
                        <asp:Label ID="lblTransportadora" runat="server" Text="Label"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Editar" ShowHeader="False">
                    <ItemTemplate>
                        <a id="linkeditar" runat="server" target="_blank">
                            <img src="images/btEditar.jpg" alt="Editar" style="border-style: none;" />
                        </a>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>

    </div>
    <div style="float: left; clear: left; width: 868px;margin-left: 25px; margin-top: 15px;">
        <strong>Quantidade de itens encontrados nesta busca:
        <span style="text-decoration: underline">
            <asp:Label ID="lblitensencontrados" runat="server" Text=""></asp:Label></span>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
       Total do preço de custo nesta busca: <span style="text-decoration: underline"><asp:Label ID="lblprecodecusto" runat="server" Text=""></asp:Label></span>
        </strong>
    </div>
</asp:Content>

