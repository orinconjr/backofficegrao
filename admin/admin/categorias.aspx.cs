﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxTabControl;
using DevExpress.Web.ASPxEditors;
using DevExpress.Data;
using DevExpress.Web.ASPxUploadControl;
using System.IO;

public partial class admin_categorias : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
            PopulateRootLevel();
    }

    #region categorias

    protected void treeCategorias_PreRender(object sender, EventArgs e)
    {
        treeCategorias.Attributes.Add("OnClick", "client_OnTreeNodeChecked(event)");
    }

    private void PopulateRootLevel()
    {
        treeCategorias.Nodes.Clear();
        var root = new TreeNode();
        root.Text = "Categorias";
        root.Value = "0";
        root.Checked = true;
        root.Expanded = true;
        root.Selected = true;

        treeCategorias.Nodes.Add(root);
        int idSite = 0;
        int.TryParse(ddlSite.SelectedValue, out idSite);
        if (idSite == 0) idSite = 1;
        PopulateNodes(rnCategorias.categoriasSelecionaNivelPaiPorSiteId(idSite).Tables[0], treeCategorias.Nodes);
        treeCategorias.ExpandAll();
    }

    private void PopulateSubLevel(int categoriaPaiId, TreeNode parentNode)
    {
        PopulateNodes(rnCategorias.categoriasSelecionaNivelFilho(categoriaPaiId).Tables[0], parentNode.ChildNodes);
    }

    protected void treeCategorias_TreeNodePopulate(object sender, TreeNodeEventArgs e)
    {
        PopulateSubLevel(Int32.Parse(e.Node.Value), e.Node);
    }

    private void PopulateNodes(DataTable dt, TreeNodeCollection nodes)
    {
        foreach (DataRow dr in dt.Rows)
        {
            TreeNode tn = new TreeNode();
            tn.Text = dr["categoriaNome"].ToString();
            tn.Value = dr["categoriaId"].ToString();

            //desabilita os links do treeview
            nodes.Add(tn);

            //If node has child nodes, then enable on-demand populating
            tn.PopulateOnDemand = ((int)(dr["childnodecount"]) > 0);
        }
    }

    protected void treeCategorias_SelectedNodeChanged(object sender, EventArgs e)
    {
        desmarcaCheckbox(treeCategorias.Nodes);
        marcaCheckbox(treeCategorias.SelectedNode.Value.ToString(), treeCategorias.Nodes);
    }

    private void desmarcaCheckbox(TreeNodeCollection treeview)
    {
        int i = 0;
        foreach (TreeNode tre in treeview)
        {
            //utilizando a recursividade
            this.desmarcaCheckbox(treeview[i].ChildNodes);
            //faz a busca por trecho de texto

            tre.Checked = false; //desmarca o checkbox do item encontrado
            if (tre.Parent == null) //se não tiver parent (caso do 1º nivel)
                tre.Expand(); //expande o nivel atual
            else
                tre.Parent.Expand(); //expande o nivel anterior

            i++;
        }
    }

    private void marcaCheckbox(string valorProcurado, TreeNodeCollection treeview)
    {
        int i = 0;
        foreach (TreeNode tre in treeview)
        {
            //utilizando a recursividade
            this.marcaCheckbox(valorProcurado, treeview[i].ChildNodes);
            //faz a busca por trecho de texto
            if (tre.Value.ToString() == valorProcurado)
            {
                tre.Checked = true; //marca o checkbox do item encontrado
                if (tre.Parent == null) //se não tiver parent (caso do 1º nivel)
                    tre.Expand(); //expande o nivel atual
                else
                    tre.Parent.Expand(); //expande o nivel anterior
            }
            i++;
        }
    }

    private int pegaItensMarcados()
    {
        int categoriaPaiId = 0;
        if (treeCategorias.CheckedNodes.Count > 0)
        {
            foreach (TreeNode node in treeCategorias.CheckedNodes)
            {
                categoriaPaiId = Convert.ToInt32(node.Value);
            }
        }
        return categoriaPaiId;
    }

    #endregion

    //protected void btPdf_Click(object sender, ImageClickEventArgs e)
    //{
    //    this.grdEx.WritePdfToResponse();
    //}
    //protected void btXsl_Click(object sender, ImageClickEventArgs e)
    //{
    //    this.grdEx.WriteXlsToResponse();
    //}
    //protected void btRtf_Click(object sender, ImageClickEventArgs e)
    //{
    //    this.grdEx.WriteRtfToResponse();
    //}
    //protected void btCsv_Click(object sender, ImageClickEventArgs e)
    //{
    //    this.grdEx.WriteCsvToResponse();
    //}

    protected void grd_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType != GridViewRowType.EditForm) return;
        {
            Image image1 = (Image)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "imagem1");
            Image image2 = (Image)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "imagem2");
            Image image3 = (Image)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "imagem3");

            CheckBox ckbExcluir1 = (CheckBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "ckbExcluir1");
            CheckBox ckbExcluir2 = (CheckBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "ckbExcluir2");
            CheckBox ckbExcluir3 = (CheckBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "ckbExcluir3");

            #region Oculta imagem quando for inserir um novo registro ou quando for editar um registro e não existir imagem no bd
            if (image1.ImageUrl.ToString() == "../categorias//" || image1.ImageUrl.ToString() == "../categorias/" + e.KeyValue + "/")
            {
                image1.Visible = false;
                ckbExcluir1.Visible = false;
            }

            if (image2.ImageUrl.ToString() == "../categorias//" || image2.ImageUrl.ToString() == "../categorias/" + e.KeyValue + "/")
            {
                image2.Visible = false;
                ckbExcluir2.Visible = false;
            }

            if (image3.ImageUrl.ToString() == "../categorias//" || image3.ImageUrl.ToString() == "../categorias/" + e.KeyValue + "/")
            {
                image3.Visible = false;
                ckbExcluir3.Visible = false;
            }
            #endregion
        }
    }

    protected void grd_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        sqlCategorias.InsertParameters.Clear();
        
        sqlCategorias.InsertParameters.Add("categoriaPaiId", pegaItensMarcados().ToString());

        ASPxTextBox txtNome = (ASPxTextBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["categoriaNome"], "txtNome");
        ASPxTextBox txtCategoriaNomeExibicao = (ASPxTextBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["categoriaNomeExibicao"], "txtCategoriaNomeExibicao");
        ASPxTextBox txtCategoriaTags = (ASPxTextBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["categoriaTags"], "txtCategoriaTags");
        ASPxTextBox txtCategoriaMl = (ASPxTextBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["categoriaMl"], "txtCategoriaMl");
        ASPxTextBox txtCategoriaSigla = (ASPxTextBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["codigoEtiqueta"], "txtCategoriaSigla");


        TextBox txtCategoriaDescricao = (TextBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["categoriaDescricao"], "txtCategoriaDescricao");

        TextBox txtPosicao = (TextBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["CategoriaOrdem"], "txtPosicao");
        CheckBox chkExibirSite = (CheckBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["exibirSite"], "chkExibirSite");
        CheckBox chkExibirNoFiltro = (CheckBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["exibirNoFiltro"], "chkExibirNoFiltro");
        CheckBox chkExibirMesmoCom1Filtro = (CheckBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["exibirMesmoCom1Filtro"], "chkExibirMesmoCom1Filtro");
        FileUpload flu1 = (FileUpload)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "flu1");
        FileUpload flu2 = (FileUpload)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "flu2");
        FileUpload flu3 = (FileUpload)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "flu3");

        sqlCategorias.InsertParameters.Add("categoriaNome", txtNome.Text);
        sqlCategorias.InsertParameters.Add("categoriaNomeExibicao", txtCategoriaNomeExibicao.Text);
        sqlCategorias.InsertParameters.Add("categoriaMl", txtCategoriaMl.Text);
        sqlCategorias.InsertParameters.Add("categoriaTags", txtCategoriaTags.Text);
        sqlCategorias.InsertParameters.Add("categoriaUrl", rnCategorias.GerarPermalinkCategoria(txtNome.Text));
        sqlCategorias.InsertParameters.Add("categoriaDescricao", txtCategoriaDescricao.Text);
        sqlCategorias.InsertParameters.Add("codigoEtiqueta", txtCategoriaSigla.Text);
        //sqlCategorias.InsertParameters.Add("categoriaUrl", rnFuncoes.limpaString(txtNome.Text).Trim().ToLower());

        if (!string.IsNullOrEmpty(txtPosicao.Text.ToString()))
            sqlCategorias.InsertParameters.Add("CategoriaOrdem", txtPosicao.Text);

        sqlCategorias.InsertParameters.Add("exibirSite", chkExibirSite.Checked.ToString());
        sqlCategorias.InsertParameters.Add("exibirNoFiltro", chkExibirNoFiltro.Checked.ToString());
        sqlCategorias.InsertParameters.Add("exibirMesmoCom1Filtro", chkExibirMesmoCom1Filtro.Checked.ToString());

        if (flu1.HasFile)
            sqlCategorias.InsertParameters.Add("imagem1", "imagem1.jpg");
        else
            sqlCategorias.InsertParameters.Add("imagem1", string.Empty);

        if (flu2.HasFile)
            sqlCategorias.InsertParameters.Add("imagem2", "imagem2.jpg");
        else
            sqlCategorias.InsertParameters.Add("imagem2", string.Empty);

        if (flu3.HasFile)
            sqlCategorias.InsertParameters.Add("imagem3", "imagem3.jpg");
        else
            sqlCategorias.InsertParameters.Add("imagem3", string.Empty);
    }

    protected void grd_RowInserted(object sender, DevExpress.Web.Data.ASPxDataInsertedEventArgs e)
    {
        if (e.Exception == null)
        {
            FileUpload flu1 = (FileUpload)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "flu1");
            FileUpload flu2 = (FileUpload)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "flu2");
            FileUpload flu3 = (FileUpload)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "flu3");

            int ultimoId = int.Parse(rnCategorias.retornaUltimoId().Tables[0].Rows[0]["categoriaId"].ToString());
            Directory.CreateDirectory(ConfigurationManager.AppSettings["caminhoFisico"] + "categorias\\" + ultimoId);

            if (flu1.HasFile)
                flu1.PostedFile.SaveAs(ConfigurationManager.AppSettings["caminhoFisico"] + "categorias\\" + ultimoId + "\\" + "imagem1.jpg");

            if (flu2.HasFile)
                flu2.PostedFile.SaveAs(ConfigurationManager.AppSettings["caminhoFisico"] + "categorias\\" + ultimoId + "\\" + "imagem2.jpg");

            if (flu3.HasFile)
                flu3.PostedFile.SaveAs(ConfigurationManager.AppSettings["caminhoFisico"] + "categorias\\" + ultimoId + "\\" + "imagem3.jpg");



            Response.Write("<script>window.location=('categorias.aspx');</script>");

        }
    }

    protected void grd_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        sqlCategorias.UpdateParameters.Clear();

        sqlCategorias.UpdateParameters.Add("categoriaPaiId", pegaItensMarcados().ToString());

        ASPxTextBox txtNome = (ASPxTextBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["categoriaNome"], "txtNome");
        ASPxTextBox txtCategoriaNomeExibicao = (ASPxTextBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["categoriaNomeExibicao"], "txtCategoriaNomeExibicao");
        ASPxTextBox txtCategoriaTags = (ASPxTextBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["categoriaTags"], "txtCategoriaTags");
        ASPxTextBox txtCategoriaMl = (ASPxTextBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["categoriaMl"], "txtCategoriaMl");
        TextBox txtCategoriaDescricao = (TextBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["categoriaDescricao"], "txtCategoriaDescricao");
        ASPxTextBox txtCategoriaSigla = (ASPxTextBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["codigoEtiqueta"], "txtCategoriaSigla");

        TextBox txtPosicao = (TextBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["CategoriaOrdem"], "txtPosicao");
        CheckBox chkExibirSite = (CheckBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["exibirSite"], "chkExibirSite");
        CheckBox chkExibirNoFiltro = (CheckBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["exibirNoFiltro"], "chkExibirNoFiltro");
        CheckBox chkExibirMesmoCom1Filtro = (CheckBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["exibirMesmoCom1Filtro"], "chkExibirMesmoCom1Filtro");
        FileUpload flu1 = (FileUpload)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "flu1");
        FileUpload flu2 = (FileUpload)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "flu2");
        FileUpload flu3 = (FileUpload)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "flu3");

        CheckBox ckbExcluir1 = (CheckBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "ckbExcluir1");
        CheckBox ckbExcluir2 = (CheckBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "ckbExcluir2");
        CheckBox ckbExcluir3 = (CheckBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "ckbExcluir3");

        int categoriaId = Convert.ToInt32(e.Keys[0]);
        var data = new dbCommerceDataContext();
        var categoriaAtual = (from c in data.tbProdutoCategorias where c.categoriaId == categoriaId select c).First();
        string categoriaUrlAtual = categoriaAtual.categoriaUrl;
        string categoriaUrlNova = rnCategorias.GerarPermalinkCategoria(categoriaId, txtNome.Text);

        sqlCategorias.UpdateParameters.Add("categoriaNome", txtNome.Text);
        sqlCategorias.UpdateParameters.Add("categoriaNomeExibicao", txtCategoriaNomeExibicao.Text);
        sqlCategorias.UpdateParameters.Add("categoriaMl", txtCategoriaMl.Text);
        sqlCategorias.UpdateParameters.Add("categoriaTags", txtCategoriaTags.Text);
        sqlCategorias.UpdateParameters.Add("categoriaUrl", categoriaUrlNova);
        sqlCategorias.UpdateParameters.Add("categoriaDescricao", txtCategoriaDescricao.Text);
        sqlCategorias.UpdateParameters.Add("codigoEtiqueta", txtCategoriaSigla.Text);

        rnProdutos.GravarPermalinkRedirect(categoriaId, 1, categoriaUrlAtual, categoriaUrlNova);

        if (!string.IsNullOrEmpty(txtPosicao.Text.ToString()))
            sqlCategorias.UpdateParameters.Add("CategoriaOrdem", txtPosicao.Text);

        sqlCategorias.UpdateParameters.Add("exibirSite", chkExibirSite.Checked.ToString());
        sqlCategorias.UpdateParameters.Add("exibirNoFiltro", chkExibirNoFiltro.Checked.ToString());
        sqlCategorias.UpdateParameters.Add("exibirMesmoCom1Filtro", chkExibirMesmoCom1Filtro.Checked.ToString());

        if (flu1.HasFile)
            sqlCategorias.UpdateParameters.Add("imagem1", "imagem1.jpg");
        else
            sqlCategorias.UpdateParameters.Add("imagem1", string.Empty);

        if (flu2.HasFile)
            sqlCategorias.UpdateParameters.Add("imagem2", "imagem2.jpg");
        else
            sqlCategorias.UpdateParameters.Add("imagem2", string.Empty);

        if (flu3.HasFile)
            sqlCategorias.UpdateParameters.Add("imagem3", "imagem3.jpg");
        else
            sqlCategorias.UpdateParameters.Add("imagem3", string.Empty);

        if (ckbExcluir1.Checked == true)
            rnCategorias.excluiFoto(categoriaId, "imagem1.jpg");

        if (ckbExcluir2.Checked == true)
            rnCategorias.excluiFoto(categoriaId, "imagem2.jpg");

        if (ckbExcluir3.Checked == true)
            rnCategorias.excluiFoto(categoriaId, "imagem3.jpg");
    }

    protected void grd_RowUpdated(object sender, DevExpress.Web.Data.ASPxDataUpdatedEventArgs e)
    {
        if (e.Exception == null)
        {
            FileUpload flu1 = (FileUpload)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "flu1");
            FileUpload flu2 = (FileUpload)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "flu2");
            FileUpload flu3 = (FileUpload)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "flu3");

            int categoriaId = Convert.ToInt32(e.Keys[0]);

            if (flu1.HasFile)
                flu1.PostedFile.SaveAs(ConfigurationManager.AppSettings["caminhoFisico"] + "categorias\\" + categoriaId + "\\" + "imagem1.jpg");
 
            if (flu2.HasFile)
                flu2.PostedFile.SaveAs(ConfigurationManager.AppSettings["caminhoFisico"] + "categorias\\" + categoriaId + "\\" + "imagem2.jpg");

            if (flu3.HasFile)
                flu3.PostedFile.SaveAs(ConfigurationManager.AppSettings["caminhoFisico"] + "categorias\\" + categoriaId + "\\" + "imagem3.jpg");

            


            Response.Write("<script>window.location=('categorias.aspx');</script>");
        }
    }

    protected void grd_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
    {
        if (rnCategorias.contaCategoriasFilho(Convert.ToInt32(e.Keys[0])).Tables[0].Rows.Count > 0)
        {
            Response.Write("<script>alert('Essa categoria não pode ser excluida por que existe sub-categorias relacionada a ela.');</script>");
            e.Cancel = true;
        }
    }

    protected void grd_RowDeleted(object sender, DevExpress.Web.Data.ASPxDataDeletedEventArgs e)
    {
        int categoriaId = Convert.ToInt32(e.Keys[0]);

        if (Directory.Exists(ConfigurationManager.AppSettings["caminhoFisico"] + "categorias\\" + categoriaId))
        {
            Directory.Delete(ConfigurationManager.AppSettings["caminhoFisico"] + "categorias\\" + categoriaId, true);
        }

        Response.Write("<script>window.location=('categorias.aspx');</script>");
    }

    protected void ddlSite_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        PopulateRootLevel();
    }
}
