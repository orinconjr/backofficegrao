﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq; 
using System.Collections.Generic;

public partial class admin_admin : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
            usuarioLogadoId = Request.Cookies["usuarioLogadoId"];
            if (usuarioLogadoId != null)
            {
                string paginaSolicitada = Request.Path.Substring(Request.Path.LastIndexOf("/") + 1);
                int idUsuario = int.Parse(usuarioLogadoId.Value.ToString());
                var data = new dbCommerceDataContext();
                var paginas = (from c in data.tbUsuarioPaginasPermitidas where c.usuarioId == idUsuario select c).ToList();
                var indicesRemover = new List<MenuItem>();
                foreach (MenuItem itemMenu in menu.Items)
                {
                    if (itemMenu.ChildItems.Count > 0)
                    {
                        //List<int> indicesRemover = new List<int>();
                        var indicesRemoverSub = new List<MenuItem>();
                        int indiceAtual = 0;
                        foreach (MenuItem itemMenuSub in itemMenu.ChildItems)
                        {

                            if (itemMenuSub.ChildItems.Count > 0)
                            {
                                //List<int> indicesRemover = new List<int>();
                                var indicesRemoverSubSub = new List<MenuItem>();
                                int indiceAtualSub = 0;
                                foreach (MenuItem itemSubSub in itemMenuSub.ChildItems)
                                {
                                    var urlItemSub = itemSubSub.NavigateUrl.Substring(itemSubSub.NavigateUrl.ToLower().LastIndexOf("/") + 1).ToLower().Split('?')[0];
                                    var possuiPermissaoSub = (from c in paginas where c.paginaPermitidaNome.ToLower().Contains(urlItemSub) select c).Any();
                                    if (!possuiPermissaoSub)
                                    {
                                        indicesRemoverSubSub.Add(itemSubSub);
                                    }
                                }
                                foreach (var indiceRemover in indicesRemoverSubSub)
                                {
                                    itemMenuSub.ChildItems.Remove(indiceRemover);
                                }
                            }
                            else
                            {
                                var urlItemSub = itemMenuSub.NavigateUrl.Substring(itemMenuSub.NavigateUrl.ToLower().LastIndexOf("/") + 1).ToLower().Split('?')[0];
                                var possuiPermissaoSub = (from c in paginas where c.paginaPermitidaNome.ToLower().Contains(urlItemSub) select c).Any();
                                if (!possuiPermissaoSub)
                                {
                                    indicesRemoverSub.Add(itemMenuSub);
                                }
                            }
                            
                            indiceAtual++;
                        }
                        foreach (var indiceRemover in indicesRemoverSub)
                        {
                            itemMenu.ChildItems.Remove(indiceRemover);
                        }
                    }
                }
                foreach (MenuItem itemMenu in menu.Items)
                {
                    if (itemMenu.ChildItems.Count == 0 && !string.IsNullOrEmpty(itemMenu.NavigateUrl))
                    {
                        var urlItemSub = itemMenu.NavigateUrl.Substring(itemMenu.NavigateUrl.ToLower().LastIndexOf("/") + 1).ToLower().Split('?')[0];
                        var possuiPermissao = (from c in paginas where c.paginaPermitidaNome.ToLower().Contains(urlItemSub) select c).Any();
                        if (!possuiPermissao)
                        {
                            indicesRemover.Add(itemMenu);
                        }
                    }
                }
                foreach (var indiceRemover in indicesRemover)
                {
                    menu.Items.Remove(indiceRemover);
                }

                if (!(from c in paginas where c.paginaPermitidaNome.ToLower().Contains(paginaSolicitada.ToLower()) select c).Any())
                {
                    Response.Redirect("bemvindo.aspx");
                    //Response.Write("<script>alert('Você não tem acesso a essa área.');</script>");
                    // Response.Write("<script>history.back();</script>");
                    //Response.Write("<script>window.location=('bemvindo.aspx');</script>");
                }
                else
                {
                    lblNumerodeAcesos.Text = rnUsuarios.usuarioAcessoSeleciona_PorUsuarioId(int.Parse(usuarioLogadoId.Value.ToString())).Tables[0].Rows.Count.ToString();
                    lblUsuario.Text = rnUsuarios.usuarioSeleciona_PorUsuarioId(int.Parse(usuarioLogadoId.Value.ToString())).Tables[0].Rows[0]["usuarioNome"].ToString();
                    lblIp.Text = Request.UserHostAddress.ToString();


                }
            }
            else
            {
                Response.Redirect("default.aspx");
                Response.Write("<script>alert('Você não tem acesso a essa área.');</script>");
                Response.Write("<script>window.location=('default.aspx');</script>");
            }
        }
    }

    protected void imbLogout_Click(object sender, EventArgs e)
    {
        if (Request.Cookies["usuarioLogadoId"] != null)
        {
            HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
            usuarioLogadoId.Expires = DateTime.Now.AddDays(-1);
            Response.Cookies.Add(usuarioLogadoId);

            Response.Write("<script>window.location=('default.aspx');</script>");
        }
    }

    protected void btHome_Click(object sender, EventArgs e)
    {
        Response.Redirect("bemvindo.aspx");
    }
}
