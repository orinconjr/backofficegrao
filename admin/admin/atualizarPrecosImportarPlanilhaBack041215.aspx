﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="atualizarPrecosImportarPlanilhaBack041215.aspx.cs" Inherits="admin_atualizarPrecosImportarPlanilhaBack041215" %>

<%@ Register Assembly="DevExpress.Web.v11.1" Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxClasses" Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>
<%@ Register Assembly="DevExpress.Web.v11.1" Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" runat="Server">
        <style type="text/css">
            
             .btnexportar {
                 float: right;
             }
            .fileUpload {
                 float: left;
             }
            .fieldsetatualizarprecos {
                width: 760px; float: left; clear: left; height: 54px;border-radius: 5px;
            }
            .fieldsetatualizarprecos input[type=checkbox] {
                margin-top: 15px;
            }
            .fieldsetatualizarprecos label {
                margin-left: 5px;margin-right: 20px;
            }
            .atualizarprecosbotoes {
                width: 780px; float: left; clear: left;  height: 64px;  margin-top: 15px;
            }
            .atualizarprecosbotoes label{
                 float: left; margin-top: 5px;
            }
           
        </style>

    <dx:ASPxPageControl ID="tabsPedidos" runat="server" ActiveTabIndex="0" Width="100%">
        <TabPages>
            <dx:TabPage Text="Importar planilha de preços">
                <ContentCollection>
                    <dx:ContentControl runat="server">

                        <table>
                            <tr>
                                <td class="tituloPaginas" valign="top" colspan="5">Atualizar Preços de Custo por Fornecedor
                                </td>
                            </tr>
                         
                            <tr>
                                <td colspan="5" >
                                     <fieldset class="fieldsetatualizarprecos">
                                        <legend style="font-weight: bold;">Colunas a Atualizar</legend>
                                        <asp:CheckBox ID="ckbprodutoPreco" runat="server" Text="produtoPreco" />
                                         <asp:CheckBox ID="ckbprodutoPrecoDeCusto" runat="server" Text="produtoPrecoDeCusto" />
                                        <asp:CheckBox ID="ckbprodutoPrecoPromocional" runat="server" Text="produtoPrecoPromocional" />
                                        <asp:CheckBox ID="ckbmargemDeLucro" runat="server" Text="margemDeLucro" />
                                         <asp:CheckBox ID="ckbprodutoAtivo" runat="server" Text="produtoAtivo" />
                                    </fieldset>
                            


                                    <div class="atualizarprecosbotoes">
                                       <label > Planilha: </label><asp:FileUpload runat="server" CssClass="fileUpload" ID="fupPlanilha" />
                                         <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="None"
                                        ControlToValidate="fupPlanilha" ErrorMessage="Selecione uma planilha xls ou xlsx!" ValidationGroup="tipoPlanilha">
                                    </asp:RequiredFieldValidator>
                                         <asp:Button runat="server" ID="btnImportar" CssClass="btnexportar" Text="Atualizar Banco de Dados" ValidationGroup="tipoPlanilha" OnClick="btnImportar_Click" />
                                     
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="5" align="justify">
                                    <asp:Literal runat="server" ID="litProdutosAtualizados"></asp:Literal>
                                </td>
                            </tr>
                        </table>

                    </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>

            <dx:TabPage Text="Exportar planilha de preços">
                <ContentCollection>
                    <dx:ContentControl runat="server">
                        <table width="100%">
                            <tr>
                                <td class="tituloPaginas" colspan="4" valign="top">Exportar produtos por fornecedor (xlsx)
                                </td>
                            </tr>
                            <tr>
                                <td>Fornecedor:
                <asp:DropDownList ID="ddlExportarPlanilha" runat="server" AppendDataBoundItems="True" Width="175"
                    DataSourceID="sqlFornecedores" DataTextField="fornecedorNome" DataValueField="fornecedorId">
                </asp:DropDownList>
                                </td>
                                <td>
                                    <asp:CheckBox ID="ckbAtivo" runat="server" Text="Ativo" />
                                </td>
                                <td>
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td colspan="2" align="center">Cadastros no Período:
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="rotulos" style="width: 100px" align="center">Data inicial<br />
                                                <asp:TextBox ID="txtDataInicial" runat="server" CssClass="ll-skin-latoja campos"
                                                    Text='<%# Bind("faixaDeCepPesoInicial") %>'
                                                    Width="90px" MaxLength="10"></asp:TextBox>
                                                <b>
                                                    <asp:RegularExpressionValidator ID="rgeDataInicial" runat="server"
                                                        ControlToValidate="txtDataInicial" Display="None"
                                                        ErrorMessage="Por favor, preencha corretamente a data inicial."
                                                        SetFocusOnError="True"
                                                        ValidationExpression="^(([0-2]\d|[3][0-1])\/([0]\d|[1][0-2])\/[1-2][0-9]\d{2})$"></asp:RegularExpressionValidator>


                                                </b>
                                            </td>
                                            <td class="rotulos" style="width: 100px" align="center">Data final<br />
                                                <asp:TextBox ID="txtDataFinal" runat="server" CssClass="ll-skin-latoja campos"
                                                    Text='<%# Bind("faixaDeCepPesoFinal") %>' ValidationGroup="grpInsert"
                                                    Width="90px" MaxLength="10"></asp:TextBox>
                                                <b>
                                                    <asp:RegularExpressionValidator ID="rgeDataInicial0" runat="server"
                                                        ControlToValidate="txtDataFinal" Display="None"
                                                        ErrorMessage="Por favor, preencha corretamente a data final"
                                                        SetFocusOnError="True"
                                                        ValidationExpression="^(([0-2]\d|[3][0-1])\/([0]\d|[1][0-2])\/[1-2][0-9]\d{2})$"></asp:RegularExpressionValidator>
                                                </b>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td>
                                    <asp:Button runat="server" Text="Exportar" ID="btnExportar" OnClick="btnExportar_Click"/>
                                </td>
                            </tr>
                        </table>
                    </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>

        </TabPages>
    </dx:ASPxPageControl>


    <asp:ValidationSummary runat="server" ShowMessageBox="true" ShowSummary="false" ValidationGroup="tipoPlanilha" />

    <asp:SqlDataSource ID="sqlFornecedores" runat="server"
        ConnectionString="<%$ ConnectionStrings:connectionString %>"
        SelectCommand="SELECT [fornecedorId], [fornecedorNome] FROM [tbProdutoFornecedor] ORDER BY [fornecedorNome]"></asp:SqlDataSource>

</asp:Content>

