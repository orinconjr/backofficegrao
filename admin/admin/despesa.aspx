﻿<%@ Page Title="" Theme="Glass" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="despesa.aspx.cs" Inherits="admin_despesa" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" Runat="Server">
    <script language="javascript" type="text/javascript">
        function ApplyFilter(dde, dateFrom, dateTo) {
            var d1 = dateFrom.GetText();
            var d2 = dateTo.GetText();
            if (d1 == "" || d2 == "")
                return;
            dde.SetText(d1 + "|" + d2);
            //grd.ApplyFilter("[data] >= '" + d1 +" 00:00:00' && [data] <= '" + d2 + " 23:59:59'");
            grd.AutoFilterByColumn("data", dde.GetText());
        }

        function OnDropDown(s, dateFrom, dateTo) {
            var str = s.GetText();
            if (str == "") {
                // default date (1950-1961 for demo)     
                dateFrom.SetDate(new Date(1950, 0, 1));
                dateTo.SetDate(new Date(1960, 11, 31));
                return;
            }
            var d = str.split("|");
            dateFrom.SetText(d[0]);
            dateTo.SetText(d[1]);
        }
    </script>

    <table cellpadding="0" cellspacing="0" style="width: 920px">
    <tr>
        <td class="tituloPaginas" valign="top">
            <asp:Label ID="lblAcao" runat="server">Despesas</asp:Label></td>
    </tr>
    <tr>
        <td>
            
    <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
        <tr>
            <td align="right">
                <table cellpadding="0" cellspacing="0" style="width: 834px">
                    <tr>
                        <td align="right">
                            &nbsp;</td>
                        <td height="38" 
                            style="background-image: url('images/bkExportar.jpg'); padding-bottom: 1px;" 
                            valign="bottom" width="231">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="width: 97px">
                                        &nbsp;</td>
                                    <td style="width: 33px">
                                        <asp:ImageButton ID="btPdf" runat="server" ImageUrl="~/admin/images/btPdf.jpg" 
                                            style="margin-left: 5px" OnClick="btPdf_Click" />
                                    </td>
                                    <td style="width: 31px">
                                        <asp:ImageButton ID="btXsl" runat="server" ImageUrl="~/admin/images/btXls.jpg" 
                                            style="margin-left: 5px" OnClick="btXsl_Click" />
                                    </td>
                                    <td style="width: 36px">
                                        <asp:ImageButton ID="btRtf" runat="server" ImageUrl="~/admin/images/btRtf.jpg" 
                                            style="margin-left: 5px" OnClick="btRtf_Click" />
                                    </td>
                                    <td valign="bottom">
                                        <asp:ImageButton ID="btCsv" runat="server" ImageUrl="~/admin/images/btCsv.jpg" 
                                            style="margin-left: 5px" OnClick="btCsv_Click" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="height: 19px">
                </td>
        </tr>
        <tr>
            <td>
                <dxwgv:aspxgridview ClientInstanceName="grd" ID="grd" runat="server" AutoGenerateColumns="False" 
                    DataSourceID="sqlDespesa" 
                    KeyFieldName="idDespesa" Width="834px" oncustomcallback="grd_CustomCallback" 
                    Cursor="auto" onhtmlrowcreated="grd_HtmlRowCreated" OnAutoFilterCellEditorCreate="grid_AutoFilterCellEditorCreate" OnAutoFilterCellEditorInitialize="grid_AutoFilterCellEditorInitialize"
                OnProcessColumnAutoFilter="grid_ProcessColumnAutoFilter" 
                    onrowinserting="grd_RowInserting" onrowupdating="grd_RowUpdating" SettingsEditing-Mode="EditForm" EnableCallBacks="False" Settings-ShowFilterRow="True" Settings-ShowFilterBar="Visible">
                    <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                    <SettingsText ConfirmDelete="Tem certeza que deseja excluir?" 
                        emptydatarow="Nenhum registro encontrado." />
                    <SettingsPager Position="TopAndBottom" PageSize="50" 
                        ShowDisabledButtons="False" AlwaysShowPager="True">
                        <Summary AllPagesText="Páginas: {0} - {1} ({2} items)" 
                            Text="Página {0} de {1} ({2} registros encontrados)" />
                    </SettingsPager>
					<TotalSummary>
                        <dxwgv:ASPxSummaryItem DisplayFormat="c" FieldName="valor" 
                            ShowInColumn="Valor" ShowInGroupFooterColumn="Valor" SummaryType="Sum" />
                    </TotalSummary>
                    <settings showfilterrow="True" ShowGroupButtons="False" ShowFooter="True" />
                    <SettingsEditing EditFormColumnCount="4" 
                        PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1" 
                        PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter" 
                        PopupEditFormWidth="700px" />
                    <Columns>
                        <dxwgv:GridViewDataTextColumn Caption="ID" Name="id" 
                            FieldName="idDespesa" ReadOnly="True" VisibleIndex="0" Width="30px">
                            <Settings AutoFilterCondition="Contains" />
                            <editformsettings visible="False" CaptionLocation="Top" ColumnSpan="2" />
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Name="descricao" Caption="Descrição" FieldName="despesa" VisibleIndex="1" Width="200px">
                            <PropertiesTextEdit>
                                <ValidationSettings SetFocusOnError="True">
                                    <RequiredField ErrorText="Preencha a descrição" IsRequired="True" />
                                </ValidationSettings>
                            </PropertiesTextEdit>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Name="idPedido" Caption="Pedido" FieldName="idPedido" VisibleIndex="1" Width="200px">
                             <EditItemTemplate>
                                 <asp:TextBox ID="txtPedido" runat="server" CssClass="campos" 
                                     Text='<%# Eval("idPedido") %>' Width="100%"></asp:TextBox>
                             </EditItemTemplate>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataComboBoxColumn Name="categoria" Caption="Categoria" FieldName="idDespesaCategoria" VisibleIndex="2">
                            <PropertiesComboBox DataSourceID="sqlDespesaCategoria" 
                                TextField="despesaCategoria" ValueField="idDespesaCategoria" 
                                ValueType="System.String">
                                <ValidationSettings SetFocusOnError="True">
                                    <RequiredField ErrorText="Selecione a categoria." IsRequired="True" />
                                </ValidationSettings>
                            </PropertiesComboBox>
                        </dxwgv:GridViewDataComboBoxColumn>
                        <dxwgv:GridViewDataComboBoxColumn Name="fornecedor" Caption="Fornecedor" FieldName="idDespesaFornecedor" VisibleIndex="2">
                            <PropertiesComboBox DataSourceID="sqlDespesaFornecedor" 
                                TextField="fornecedor" ValueField="idDespesaFornecedor" 
                                ValueType="System.String">
<%--                                <ValidationSettings SetFocusOnError="True">
                                    <RequiredField ErrorText="Selecione o Fornecedor." IsRequired="True" />
                                </ValidationSettings>--%>
                            </PropertiesComboBox>
                        </dxwgv:GridViewDataComboBoxColumn>
                        <dxwgv:GridViewDataComboBoxColumn Name="usuario" Caption="Usuario" FieldName="idUsuario" VisibleIndex="2">
                            <PropertiesComboBox DataSourceID="sqlUsuario" 
                                TextField="usuarioNome" ValueField="usuarioId" 
                                ValueType="System.String">
                            </PropertiesComboBox>
                            <editformsettings visible="False" CaptionLocation="Top" ColumnSpan="2" />
                        </dxwgv:GridViewDataComboBoxColumn>
                        <dxwgv:GridViewDataDateColumn Name="data" Caption="Data" FieldName="data" VisibleIndex="3" Width="90px">
                        </dxwgv:GridViewDataDateColumn>
                        <dxwgv:GridViewDataDateColumn Name="vencimento" Caption="Vencimento" FieldName="vencimento" VisibleIndex="3" Width="90px">
                        </dxwgv:GridViewDataDateColumn>
                        <dxwgv:GridViewDataDateColumn Name="dataPagamento" Caption="Data do Pagamento" FieldName="data" VisibleIndex="3" Width="90px">
                        </dxwgv:GridViewDataDateColumn>
                         <dxwgv:GridViewDataTextColumn Caption="Valor" FieldName="valor" 
                            VisibleIndex="6" Width="60px">
                            <PropertiesTextEdit DisplayFormatString="C">
                                <ValidationSettings SetFocusOnError="True">
                                    <RegularExpression ErrorText="Preencha apenas com números, virgula ou ponto." 
                                        ValidationExpression="^[\-]{0,1}[0-9]{1,}(([\.\,]{0,1}[0-9]{1,})|([0-9]{0,}))$" />
                                    <RequiredField ErrorText="Preencha o desconto." IsRequired="True" />
                                </ValidationSettings>
                            </PropertiesTextEdit>
                             <EditItemTemplate>
                                 <asp:TextBox ID="txtValor" runat="server" CssClass="campos" 
                                     Text='<%# Eval("valor", "{0:C}").Replace("R$","") %>' Width="100%"></asp:TextBox>
                                 <asp:RequiredFieldValidator ID="rqvpreco" runat="server" 
                                     ControlToValidate="txtValor" Display="Dynamic" 
                                     ErrorMessage="Preencha o valor." Font-Size="10px" SetFocusOnError="True"></asp:RequiredFieldValidator>
                             </EditItemTemplate>
                        </dxwgv:GridViewDataTextColumn>
                         <dxwgv:GridViewDataTextColumn Caption="Valor Pago" FieldName="valorPago" 
                            VisibleIndex="6" Width="60px">
                            <PropertiesTextEdit DisplayFormatString="C">
                                <ValidationSettings SetFocusOnError="True">
                                    <RegularExpression ErrorText="Preencha apenas com números, virgula ou ponto." 
                                        ValidationExpression="^[\-]{0,1}[0-9]{1,}(([\.\,]{0,1}[0-9]{1,})|([0-9]{0,}))$" />
                                    <RequiredField ErrorText="Preencha o desconto." IsRequired="True" />
                                </ValidationSettings>
                            </PropertiesTextEdit>
                             <EditItemTemplate>
                                 <asp:TextBox ID="txtValorPago" runat="server" CssClass="campos" 
                                     Text='<%# Eval("valorPago", "{0:C}").Replace("R$","") %>' Width="100%"></asp:TextBox>
                             </EditItemTemplate>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataCheckColumn Caption="Pago" FieldName="pago" Name="excluir" VisibleIndex="4" Width="42px">
                            <DataItemTemplate>
                                <dxe:ASPxCheckBox ID="ckbPago" runat="server" Checked='<%# Convert.ToBoolean(Eval("pago")) %>'>
                                </dxe:ASPxCheckBox>
                            </DataItemTemplate>
                        </dxwgv:GridViewDataCheckColumn>
                        <dxwgv:GridViewCommandColumn VisibleIndex="8" Width="90px" ButtonType="Image">
                            <editbutton visible="True" Text="Editar">
                                <Image Url="~/admin/images/btEditar.jpg" />
                            </editbutton>
                            <NewButton Visible="True" Text="Novo">
                                <Image Url="~/admin/images/btNovo.jpg" />
                            </NewButton>
                            <deletebutton visible="True" Text="Excluir">
                                <Image Url="~/admin/images/btExcluir.jpg" />
                            </deletebutton>
                            <CancelButton Text="Cancelar">
                                <Image Url="~/admin/images/btCancelarIcon.jpg" />
                            </CancelButton>
                            <UpdateButton Text="Salvar">
                                <Image Url="~/admin/images/btSalvarIcon.jpg" />
                            </UpdateButton>
                            <ClearFilterButton Visible="True" Text="Limpar filtro">
                                <Image Url="~/admin/images/btLimparFiltro.jpg" />
                            </ClearFilterButton>
                            <HeaderTemplate>
                                <img alt="" src="images/legendaIcones.jpg" />
                            </HeaderTemplate>
                        </dxwgv:GridViewCommandColumn>
                    </Columns>
                    <StylesEditors>
                        <Label Font-Bold="True">
                        </Label>
                    </StylesEditors>
                </dxwgv:aspxgridview>
               <dxwgv:aspxgridviewexporter ID="grdEx" runat="server" FileName="fornecedores" 
                    GridViewID="grd" Landscape="True" PreserveGroupRowStates="True">
                </dxwgv:aspxgridviewexporter>
                <asp:LinqDataSource ID="sqlDespesa" runat="server" 
                    ContextTypeName="dbCommerceDataContext" EnableDelete="True" 
                    EnableInsert="True" EnableUpdate="True" TableName="tbDespesas">
                </asp:LinqDataSource>
                <asp:LinqDataSource ID="sqlDespesaCategoria" runat="server" 
                    ContextTypeName="dbCommerceDataContext" EnableDelete="True" 
                    EnableInsert="True" EnableUpdate="True" TableName="tbDespesaCategorias"
                    >
                </asp:LinqDataSource>
                <asp:LinqDataSource ID="sqlDespesaFornecedor" runat="server" 
                    ContextTypeName="dbCommerceDataContext" EnableDelete="True" 
                    EnableInsert="True" EnableUpdate="True" TableName="tbDespesaFornecedors"
                    >
                </asp:LinqDataSource>
                <asp:LinqDataSource ID="sqlUsuario" runat="server" 
                    ContextTypeName="dbCommerceDataContext" EnableDelete="True" 
                    EnableInsert="True" EnableUpdate="True" TableName="tbUsuarios">
                </asp:LinqDataSource>
            </td>
        </tr>
        <tr>
            <td></td>
        </tr>
        <tr>
            <td align="right">
<img src="images/btSalvar.jpg" onclick="grd.PerformCallback(this.value);"/></td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
    </table>
        </td>
    </tr>
</table>
    </asp:Content>
