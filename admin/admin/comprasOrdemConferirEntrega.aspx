﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="comprasOrdemConferirEntrega.aspx.cs" Inherits="admin_comprasOrdemConferirEntrega" %>

<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxTimer" TagPrefix="dxt" %>
<%@ Register Assembly="CuteEditor" Namespace="CuteEditor" TagPrefix="CE" %>
<%@ Register Src="~/usrComprasOrdemCondicoes.ascx" TagPrefix="uc1" TagName="usrComprasOrdemCondicoes" %>


<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" runat="Server">
    <script type="text/javascript">
        function toggle() {
            var divs = document.querySelectorAll("[id]");
            for (var i = 0, len = divs.length; i < len; i++) {
                var div = divs[i];
                if (div.id.indexOf("chkEntregue") > -1) {
                    div.checked = true;
                }
            }
        }
        Number.prototype.formatMoney = function (c, d, t) {
            var n = this, c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "," : d, t = t == undefined ? "." : t, s = n < 0 ? "-" : "", i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
            return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
        };
    </script>

    <table cellpadding="0" cellspacing="0" style="width: 920px">

        <tr>
            <td class="tituloPaginas" valign="top">
                <asp:Label ID="lblAcao" runat="server">Conferir Entrega</asp:Label>
            </td>
        </tr>
        <tr class="rotulos">
            <td>&nbsp;</td>
        </tr>
            <tr>
                <td>
                    <table style="padding-left: 15px; padding-right: 15px; width: 100%">
                        <tr class="rotulos" >
                            <td>
                                <b>Empresa:</b><br/>
                                <asp:Literal runat="server" ID="litEmpresa"></asp:Literal>
                            </td>
                            <td>
                                <b>Fornecedor:</b><br/>
                                <asp:Literal runat="server" ID="litFornecedor"></asp:Literal>
                            </td>
                            <td style="padding-bottom: 15px;">
                                <b>Total:</b><br/>
                                <asp:Literal runat="server" ID="litTotal"></asp:Literal>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <uc1:usrComprasOrdemCondicoes runat="server" ID="usrComprasOrdemCondicoes" />
                </td>
            </tr>
            <tr>
                <td>
                    <table style="padding-left: 15px; padding-right: 15px; width: 100%">
                        <tr class="rotulos" style="font-weight: bold;">
                            <td>
                                Solicitação
                            </td>
                            <td>
                                Produto
                            </td>
                            <td>
                                Quantidade
                            </td>
                            <td>
                                Preço
                            </td>
                            <td>
                                Total
                            </td>
                            <td>
                                Quantidade Entregue
                            </td>
                            <td>
                                Preço Entregue
                            </td>
                        </tr>
                        <asp:ListView runat="server" ID="lstProdutosSolicitacao" OnItemDataBound="lstProdutosSolicitacao_OnItemDataBound">
                            <ItemTemplate>
                                <tr class="rotulos">
                                    <td>
                                        <asp:Literal runat="server" ID="litSolicitacao" Text='<%# Eval("idComprasSolicitacao") %>'></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:Literal runat="server" ID="litProduto" Text='<%# Eval("produto") %>'></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:Literal runat="server" ID="litQuantidade" Text='<%# Eval("quantidade") %>'></asp:Literal>
                                    </td>
                                    <td>
                                        <%# Convert.ToDecimal(Eval("preco")).ToString("C") %>
                                    </td>
                                    <td>
                                        <%# (Convert.ToDecimal(Eval("preco")) * Convert.ToInt32(Eval("quantidade"))).ToString("C") %>
                                    </td>
                                    <td>
                                        <dxe:ASPxTextBox ID="txtQuantidade" runat="server" Width="50">
                                            <MaskSettings Mask="<0..999999g>" IncludeLiterals="None" />
                                            <ValidationSettings ErrorDisplayMode="ImageWithTooltip" />
                                        </dxe:ASPxTextBox>
                                    </td>
                                    <td>
                                        <dxe:ASPxTextBox ID="txtValor" runat="server" Width="80">
                                            <MaskSettings Mask="$<0..999999g>.<00..99>" IncludeLiterals="DecimalSymbol" />
                                            <ValidationSettings ErrorDisplayMode="ImageWithTooltip" />
                                        </dxe:ASPxTextBox>
                                        
                                        <asp:HiddenField runat="server" ID="hdfIdComprasOrdemProduto" Value='<%# Eval("idComprasOrdemProduto") %>' />
                                        <asp:HiddenField runat="server" ID="hdfIdComprasSolicitacaoProduto" Value='<%# Eval("idComprasSolicitacaoProduto") %>' />
                                        <asp:HiddenField runat="server" ID="hdfIdComprasProduto" Value='<%# Eval("idComprasProduto") %>' />
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:ListView>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="text-align: right;">
                    <asp:Button runat="server" ID="btnGravarOrdemCompra" Text="Gravar Ordem de Compra" OnClick="btnGravarOrdemCompra_OnClick" />
                </td>
            </tr>
        <tr>
            <td>
                <table style="width: 100%;">
                    
                </table>
            </td>
        </tr>
        <tr>
            <td style="text-align: right;">&nbsp;
            </td>
        </tr>
        <tr>
            <td style="padding-top: 100px;">&nbsp;
            </td>
        </tr>
    </tr>
    <asp:LinqDataSource ID="sqlComprasProduto" runat="server" ContextTypeName="dbCommerceDataContext" OrderBy="produto" TableName="tbComprasProdutos">
    </asp:LinqDataSource>
    <asp:LinqDataSource ID="sqlComprasFornecedor" runat="server" ContextTypeName="dbCommerceDataContext" OrderBy="fornecedor" TableName="tbComprasFornecedors">
    </asp:LinqDataSource>
    <asp:LinqDataSource ID="sqlComprasEmpresa" runat="server" ContextTypeName="dbCommerceDataContext" OrderBy="empresa" TableName="tbComprasEmpresas">
    </asp:LinqDataSource>
    <asp:LinqDataSource ID="sqlUnidadesDeMedida" runat="server" ContextTypeName="dbCommerceDataContext" TableName="tbComprasUnidadesDeMedidas">
    </asp:LinqDataSource>
</asp:Content>