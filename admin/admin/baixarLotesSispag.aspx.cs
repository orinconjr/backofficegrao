﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_baixarLotesSispag : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        FillGrid();
    }

    private void FillGrid()
    {
        var data = new dbCommerceDataContext();
        var lotes = (from c in data.tbNotaFiscals
                     orderby c.idLoteImpressaoGnre
                     where c.idLoteImpressaoGnre != null && c.idCNPJ == 6
                     select new
                     {
                         c.idLoteImpressaoGnre
                     }).Distinct().OrderByDescending(x => x.idLoteImpressaoGnre);
        grd.DataSource = lotes;
        grd.DataBind();
        
    }
    protected void btnDownload_OnCommand(object sender, CommandEventArgs e)
    {
        

    }
}