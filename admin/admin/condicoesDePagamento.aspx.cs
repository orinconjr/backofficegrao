﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;

public partial class admin_condicoesDePagamento : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    //protected void btPdf_Click(object sender, ImageClickEventArgs e)
    //{
    //    this.grdEx.WritePdfToResponse();
    //}
    //protected void btXsl_Click(object sender, ImageClickEventArgs e)
    //{
    //    this.grdEx.WriteXlsToResponse();
    //}
    //protected void btRtf_Click(object sender, ImageClickEventArgs e)
    //{
    //    this.grdEx.WriteRtfToResponse();
    //}
    //protected void btCsv_Click(object sender, ImageClickEventArgs e)
    //{
    //    this.grdEx.WriteCsvToResponse();
    //}

    protected void grd_HtmlRowCreated(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType != GridViewRowType.EditForm) return;
        {
            TextBox txtValorMinimoDaParcela = (TextBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["valorMinimoDaParcela"], "txtValorMinimoDaParcela");
            txtValorMinimoDaParcela.Attributes.Add("onkeyPress", "return moeda(this,'.',',',event);");
        }
    }

    protected void grd_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        TextBox txtValorMinimoDaParcela = (TextBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["valorMinimoDaParcela"], "txtValorMinimoDaParcela");
        e.NewValues["valorMinimoDaParcela"] = txtValorMinimoDaParcela.Text;
    }
}
