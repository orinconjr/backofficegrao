﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_fabricaAndamento : System.Web.UI.Page
{
    private class Etapas
    {
        public DateTime data { get; set; }
        public int? corte { get; set; }
        public int? totalCorte { get; set; }
        public int? bordado { get; set; }
        public int? totalBordado { get; set; }
        public int? costura { get; set; }
        public int? totalCostura { get; set; }
        public int? cola { get; set; }
        public int? totalCola { get; set; }
        public int? embalagem { get; set; }
        public int? totalEmbalagem { get; set; }
        public bool diaUtil { get; set; }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            FillGridAtual();
        }
    }


    private void FillGridAtual()
    {
        var data = new dbCommerceDataContext();
        var processos = (from c in data.tbProcessoFabricas
                         where c.idProcessoFabrica != 9
                         orderby
                             c.idProcessoFabrica != 7, c.idProcessoFabrica != 8,
                             c.idProcessoFabrica != 1,
                             c.idProcessoFabrica != 2,
                             c.idProcessoFabrica != 3,
                             c.idProcessoFabrica != 4,
                             c.idProcessoFabrica != 9,
                             c.idProcessoFabrica != 10,
                             c.idProcessoFabrica != 15
                         select c
            );
        lstEtapas.DataSource = processos;
        lstEtapas.DataBind();
        return;

    }


    protected void btnGravar_OnClick(object sender, EventArgs e)
    {
        var data = new dbCommerceDataContext();
        foreach (var item in lstChecarItens.Items)
        {
            var hdfEtiqueta = (HiddenField)item.FindControl("hdfEtiqueta");
            var chkChecar = (CheckBox)item.FindControl("chkChecar");
            var rdbSim = (RadioButton)item.FindControl("rdbSim");
            var rdbNao = (RadioButton)item.FindControl("rdbNao");
            int idPedidoFornecedorItem = Convert.ToInt32(hdfEtiqueta.Value);
            var itemAlterarStatus =
                (from c in data.tbPedidoFornecedorItems
                 where c.idPedidoFornecedorItem == idPedidoFornecedorItem
                 select c).FirstOrDefault();

            if (itemAlterarStatus == null)
                continue;

            if (itemAlterarStatus.idProcessoFabrica == 7)
            {
                if (rdbSim.Checked)
                {
                    itemAlterarStatus.idProcessoFabrica = 3;
                    itemAlterarStatus.dataProcessoFabrica = DateTime.Now;
                    var historico = new tbProcessoFabricaHistorico()
                    {
                        dataProcesso = DateTime.Now,
                        idPedidoFornecedor = itemAlterarStatus.idPedidoFornecedor,
                        idPedidoFornecedorItem = itemAlterarStatus.idPedidoFornecedorItem,
                        idProcessoFabrica = 3,
                        nomeUsuario = rnUsuarios.retornaNomeUsuarioLogado()
                    };
                    data.tbProcessoFabricaHistoricos.InsertOnSubmit(historico);
                }
                if (rdbNao.Checked)
                {
                    itemAlterarStatus.idProcessoFabrica = RetornaProximoProcessoFabrica(
                    itemAlterarStatus.idProcessoFabrica, itemAlterarStatus.tbProduto.produtoId);
                    itemAlterarStatus.dataProcessoFabrica = DateTime.Now;
                    var historico = new tbProcessoFabricaHistorico()
                    {
                        dataProcesso = DateTime.Now,
                        idPedidoFornecedor = itemAlterarStatus.idPedidoFornecedor,
                        idPedidoFornecedorItem = itemAlterarStatus.idPedidoFornecedorItem,
                        idProcessoFabrica = itemAlterarStatus.idProcessoFabrica,
                        nomeUsuario = rnUsuarios.retornaNomeUsuarioLogado()
                    };
                    data.tbProcessoFabricaHistoricos.InsertOnSubmit(historico);
                }
            }
            else
            {
                if (chkChecar.Checked)
                {
                    itemAlterarStatus.idProcessoFabrica = RetornaProximoProcessoFabrica(
                    itemAlterarStatus.idProcessoFabrica, itemAlterarStatus.tbProduto.produtoId);
                    itemAlterarStatus.dataProcessoFabrica = DateTime.Now;
                    var historico = new tbProcessoFabricaHistorico()
                    {
                        dataProcesso = DateTime.Now,
                        idPedidoFornecedor = itemAlterarStatus.idPedidoFornecedor,
                        idPedidoFornecedorItem = itemAlterarStatus.idPedidoFornecedorItem,
                        idProcessoFabrica = itemAlterarStatus.idProcessoFabrica,
                        nomeUsuario = rnUsuarios.retornaNomeUsuarioLogado()
                    };
                    data.tbProcessoFabricaHistoricos.InsertOnSubmit(historico);
                }

            }
            data.SubmitChanges();
        }

        int idPedidoFornecedor = Convert.ToInt32(hdfIdPedidoFornecedor.Value);
        var pedidoFornecedor = (from c in data.tbPedidoFornecedors where c.idPedidoFornecedor == idPedidoFornecedor select c).First();
        int itensAlterar =
            (from c in data.tbPedidoFornecedorItems
             where c.idProcessoFabrica == c.tbPedidoFornecedor.idProcessoFabrica && c.idPedidoFornecedor == idPedidoFornecedor
             select c).ToList().Count();
        if (itensAlterar == 0)
        {
            pedidoFornecedor.idProcessoFabrica = RetornaProximoProcessoFabrica(pedidoFornecedor.idProcessoFabrica, 0);
            pedidoFornecedor.dataProcessoFabrica = DateTime.Now;
            var historico = new tbProcessoFabricaHistorico()
            {
                dataProcesso = DateTime.Now,
                idPedidoFornecedor = pedidoFornecedor.idPedidoFornecedor,
                idProcessoFabrica = pedidoFornecedor.idProcessoFabrica,
                nomeUsuario = rnUsuarios.retornaNomeUsuarioLogado()
            };
            data.tbProcessoFabricaHistoricos.InsertOnSubmit(historico);
            data.SubmitChanges();
        }

        popChecarItens.ShowOnPageLoad = false;
        FillGridAtual();
    }

    protected void btnChecarItens_OnCommand(object sender, CommandEventArgs e)
    {
        int idPedidoFornecedor = Convert.ToInt32(e.CommandArgument);
        carregaItensChecar(idPedidoFornecedor, 0);
    }

    protected void btnChecarFaltantes_OnCommand(object sender, CommandEventArgs e)
    {
        int idPedidoFornecedor = Convert.ToInt32(e.CommandArgument.ToString().Split('#')[0]);
        carregaItensFaltantesNoProcessoChecar(idPedidoFornecedor, 0, Convert.ToInt32(e.CommandArgument.ToString().Split('#')[1]));
    }

    private int RetornaProximoProcessoFabrica(int idProcessoAtual, int produtoId)
    {
        if (produtoId == 0)
        {
            return RetornaProximoProcessoFabrica(idProcessoAtual);
        }
        else
        {
            var data = new dbCommerceDataContext();
            var juncao = (from c in data.tbJuncaoProdutoProcessoFabricas where c.idProduto == produtoId select c).ToList();
            int idProcesso = idProcessoAtual;
            bool possuiProcesso = false;
            while (idProcesso != 6 && possuiProcesso == false)
            {
                idProcesso = RetornaProximoProcessoFabrica(idProcesso);
                possuiProcesso = juncao.Any(x => x.idProcessoFabrica == idProcesso);
            }
            return idProcesso;
        }
    }

    private int RetornaProximoProcessoFabrica(int idProcessoAtual)
    {
        if (idProcessoAtual == 7) return 8;
        if (idProcessoAtual == 8) return 1;
        if (idProcessoAtual == 1) return 2;
        if (idProcessoAtual == 2) return 3;
        if (idProcessoAtual == 3) return 4;
        if (idProcessoAtual == 4) return 5;
        if (idProcessoAtual == 5) return 6;
        return 6;
    }

    protected void lstEtapas_OnItemDataBound(object sender, ListViewItemEventArgs e)
    {
        var data = new dbCommerceDataContext();
        var lstEstoque = (ListView)e.Item.FindControl("lstEstoque");
        var dataItem = (dynamic)e.Item.DataItem;
        int idProcesso = Convert.ToInt32(dataItem.idProcessoFabrica);
        int idFornecedor = Convert.ToInt32(ddlEmpresa.SelectedValue);
        if (idProcesso == 6)// entregue
        {
            List<int> processoFabrica = new List<int> { 1, 2, 3, 4, 5 };

            var romaneiosEntregue = (from c in data.tbPedidoFornecedors
                                     join d in data.tbPedidoFornecedorItems on c.idPedidoFornecedor equals d.idPedidoFornecedor into items
                                     where c.idFornecedor == idFornecedor && items.Count(x => x.idProcessoFabrica == idProcesso) > 0 && items.Any(x => x.entregue == false) && c.data > Convert.ToDateTime("01/10/2015")//
                                     orderby (c.dataProcessoFabrica ?? c.data)
                                     select new
                                     {
                                         c.idPedidoFornecedor,
                                         itens = items.Count(),
                                         itensEtapa = (items.Count(x => x.idProcessoFabrica == idProcesso) - items.Count(x => x.entregue && x.idProcessoFabrica == idProcesso)),
                                         //percentual = ((decimal)(items.Count(x => x.idProcessoFabrica == idProcesso) * 100) / (decimal)items.Count()),
                                         percentual = ((decimal)((items.Count() - (items.Count() - items.Count(x => x.entregue))) * 100) / (decimal)items.Count()),
                                         data = (c.dataProcessoFabrica ?? c.data),
                                         processo = idProcesso
                                     }).ToList();

            lstEstoque.DataSource = romaneiosEntregue;
            lstEstoque.DataBind();
        }
        else
        {
            var romaneiosEntregue = (from c in data.tbPedidoFornecedors
                                     join d in data.tbPedidoFornecedorItems on c.idPedidoFornecedor equals d.idPedidoFornecedor into items
                                     where c.idFornecedor == idFornecedor && items.Count(x => x.idProcessoFabrica == idProcesso) > 0 && items.Any(x => x.entregue == false) && c.data > Convert.ToDateTime("01/10/2015")
                                     orderby (c.dataProcessoFabrica ?? c.data)
                                     select new
                                     {
                                         c.idPedidoFornecedor,
                                         itens = items.Count(),
                                         itensEtapa = items.Count(x => x.idProcessoFabrica == idProcesso && !x.entregue),
                                         percentual = ((decimal)(items.Count(x => x.idProcessoFabrica != idProcesso && !x.entregue) * 100) / (decimal)items.Count()),
                                         //percentual = ((decimal)((items.Count() - (items.Count(x => x.idProcessoFabrica == idProcesso) - items.Count(x => x.entregue))) * 100) / (decimal)items.Count()),
                                         data = (c.dataProcessoFabrica ?? c.data),
                                         processo = idProcesso
                                     }).ToList();

            lstEstoque.DataSource = romaneiosEntregue;
            lstEstoque.DataBind();
        }



    }

    private void carregaItensChecar(int idPedidoFornecedor, int idSubProcesso, int idProcessoFabrica = 0)
    {
        hdfIdPedidoFornecedor.Value = idPedidoFornecedor.ToString();
        hdfIdPrecessoFabrica.Value = idProcessoFabrica.ToString();
        popChecarItens.ShowOnPageLoad = true;
        var data = new dbCommerceDataContext();
        var itensPedidoInicial = (from c in data.tbPedidoFornecedorItems
                                  join d in data.tbPedidoFornecedorItems on c.idPedidoFornecedor equals d.idPedidoFornecedor into itens
                                  where c.idPedidoFornecedor == idPedidoFornecedor && c.idProcessoFabrica == c.tbPedidoFornecedor.idProcessoFabrica
                                  select new
                                  {
                                      c.idPedidoFornecedor,
                                      c.tbProduto.produtoNome,
                                      c.idPedidoFornecedorItem,
                                      c.tbProduto.complementoIdDaEmpresa,
                                      c.tbProduto.produtoId,
                                      c.tbProduto.produtoIdDaEmpresa,
                                      c.idProcessoFabrica
                                  }).OrderBy(x => x.produtoNome).ToList();
        var itensPedido = (from c in itensPedidoInicial
                           select new
                           {
                               c.idPedidoFornecedor,
                               c.produtoNome,
                               c.idPedidoFornecedorItem,
                               c.complementoIdDaEmpresa,
                               c.produtoId,
                               c.produtoIdDaEmpresa,
                               totalProduto = itensPedidoInicial.Count(x => x.produtoId == c.produtoId),
                               totalAtual = itensPedidoInicial.OrderBy(x => x.idPedidoFornecedorItem).Count(x => x.idPedidoFornecedorItem <= c.idPedidoFornecedorItem && x.produtoId == c.produtoId),
                               c.idProcessoFabrica
                           }).OrderBy(x => x.produtoNome).ThenBy(x => x.totalAtual).ToList();
        var itensFiltrados = itensPedido;

        litRomaneio.Text = idPedidoFornecedor.ToString();
        var romaneio =
            (from c in data.tbPedidoFornecedors where c.idPedidoFornecedor == idPedidoFornecedor select c).First();
        var etapa =
            (from c in data.tbProcessoFabricas where c.idProcessoFabrica == romaneio.idProcessoFabrica select c).First();
        litEtapaAtual.Text = etapa.processo;
        litTotalItens.Text = (from c in data.tbPedidoFornecedorItems where c.idPedidoFornecedor == idPedidoFornecedor select c).Count().ToString();
        if (idSubProcesso > 0)
        {
            var idsProdutos = itensPedido.Select(x => x.produtoId).Distinct().ToList();
            var produtosSubProcessos = (from c in data.tbJuncaoProdutoProcessoFabricas
                                        where idsProdutos.Contains(c.idProduto) && c.idSubProcessoFabrica == idSubProcesso
                                        select c.idProduto).ToList();
            itensFiltrados =
                (from c in itensPedido where produtosSubProcessos.Contains(c.produtoId) select c).OrderBy(
                    x => x.produtoNome).ToList();
            litTotalSubProcesso.Text = " - <b>Itens no Processo: </b>" + itensFiltrados.Count;
        }
        else
        {
            var subProcessos =
                (from c in data.tbSubProcessoFabricas where c.idProcessoFabrica == romaneio.idProcessoFabrica select c)
                    .OrderBy(x => x.nome);
            ddlSubProcessos.Items.Clear();
            ddlSubProcessos.Items.Add(new ListItem("Selecione", ""));
            foreach (var subprocesso in subProcessos)
            {
                ddlSubProcessos.Items.Add(new ListItem(subprocesso.nome, subprocesso.idSubProcessoFabrica.ToString()));
            }
            litTotalSubProcesso.Text = "";
        }
        lstChecarItens.DataSource = itensFiltrados;
        lstChecarItens.DataBind();
    }

    private void carregaItensFaltantesNoProcessoChecar(int idPedidoFornecedor, int idSubProcesso, int idProcesso)
    {
        hdfIdPedidoFornecedor.Value = idPedidoFornecedor.ToString();
        popChecarItens.ShowOnPageLoad = true;
        var data = new dbCommerceDataContext();
        var itensPedidoInicial = (from c in data.tbPedidoFornecedorItems
                                  join d in data.tbPedidoFornecedorItems on c.idPedidoFornecedor equals d.idPedidoFornecedor into itens
                                  where c.idPedidoFornecedor == idPedidoFornecedor && itens.Count(x => x.entregue == false) > 0
                                  select new
                                  {
                                      c.idPedidoFornecedor,
                                      c.tbProduto.produtoNome,
                                      c.idPedidoFornecedorItem,
                                      c.tbProduto.complementoIdDaEmpresa,
                                      c.tbProduto.produtoId,
                                      c.tbProduto.produtoIdDaEmpresa,
                                      c.idProcessoFabrica,
                                      c.entregue
                                  }).OrderBy(x => x.produtoNome).ToList();

        itensPedidoInicial = idProcesso == 6 ? itensPedidoInicial.Where(x => x.idProcessoFabrica == idProcesso && !x.entregue).ToList()
                                             : itensPedidoInicial.Where(x => x.idProcessoFabrica == idProcesso).ToList();


        var itensPedido = (from c in itensPedidoInicial
                           select new
                           {
                               c.idPedidoFornecedor,
                               c.produtoNome,
                               c.idPedidoFornecedorItem,
                               c.complementoIdDaEmpresa,
                               c.produtoId,
                               c.produtoIdDaEmpresa,
                               totalProduto = itensPedidoInicial.Count(x => x.produtoId == c.produtoId),
                               totalAtual = itensPedidoInicial.OrderBy(x => x.idPedidoFornecedorItem).Count(x => x.idPedidoFornecedorItem <= c.idPedidoFornecedorItem && x.produtoId == c.produtoId),
                               c.idProcessoFabrica
                           }).OrderBy(x => x.produtoNome).ThenBy(x => x.totalAtual).ToList();
        var itensFiltrados = itensPedido;

        litRomaneio.Text = idPedidoFornecedor.ToString();
        var romaneio =
            (from c in data.tbPedidoFornecedors where c.idPedidoFornecedor == idPedidoFornecedor select c).First();
        var etapa =
            (from c in data.tbProcessoFabricas where c.idProcessoFabrica == romaneio.idProcessoFabrica select c).First();
        litEtapaAtual.Text = etapa.processo;
        var totalItens = (from c in data.tbPedidoFornecedorItems where c.idPedidoFornecedor == idPedidoFornecedor select c);
        litTotalItens.Text = idProcesso == 6 ? totalItens.Count(x => !x.entregue && x.idProcessoFabrica == idProcesso).ToString() : totalItens.Count(x => x.idProcessoFabrica == idProcesso).ToString();
        if (idSubProcesso > 0)
        {
            var idsProdutos = itensPedido.Select(x => x.produtoId).Distinct().ToList();
            var produtosSubProcessos = (from c in data.tbJuncaoProdutoProcessoFabricas
                                        where idsProdutos.Contains(c.idProduto) && c.idSubProcessoFabrica == idSubProcesso
                                        select c.idProduto).ToList();
            itensFiltrados =
                (from c in itensPedido where produtosSubProcessos.Contains(c.produtoId) select c).OrderBy(
                    x => x.produtoNome).ToList();
            litTotalSubProcesso.Text = " - <b>Itens no Processo: </b>" + itensFiltrados.Count;
        }
        else
        {
            var subProcessos =
                (from c in data.tbSubProcessoFabricas where c.idProcessoFabrica == romaneio.idProcessoFabrica select c)
                    .OrderBy(x => x.nome);
            ddlSubProcessos.Items.Clear();
            ddlSubProcessos.Items.Add(new ListItem("Selecione", ""));
            foreach (var subprocesso in subProcessos)
            {
                ddlSubProcessos.Items.Add(new ListItem(subprocesso.nome, subprocesso.idSubProcessoFabrica.ToString()));
            }
            litTotalSubProcesso.Text = "";
        }
        lstChecarItens.DataSource = itensFiltrados;
        lstChecarItens.DataBind();
    }

    protected void ddlSubProcessos_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        //if (ddlSubProcessos.SelectedValue != "") carregaItensChecar(Convert.ToInt32(hdfIdPedidoFornecedor.Value), Convert.ToInt32(ddlSubProcessos.SelectedValue));
        //else carregaItensChecar(Convert.ToInt32(hdfIdPedidoFornecedor.Value), 0);

        if (ddlSubProcessos.SelectedValue != "") carregaItensChecar(Convert.ToInt32(hdfIdPedidoFornecedor.Value), Convert.ToInt32(ddlSubProcessos.SelectedValue),
            (hdfIdPrecessoFabrica.Value != "" && hdfIdPrecessoFabrica.Value != "0" ? Convert.ToInt32(hdfIdPrecessoFabrica.Value) : 0));
        else carregaItensChecar(Convert.ToInt32(hdfIdPedidoFornecedor.Value), 0, (hdfIdPrecessoFabrica.Value != "" && hdfIdPrecessoFabrica.Value != "0" ? Convert.ToInt32(hdfIdPrecessoFabrica.Value) : 0));
    }

    protected void lstChecarItens_OnItemDataBound(object sender, ListViewItemEventArgs e)
    {
        var dataitem = (dynamic)e.Item.DataItem;
        if (dataitem.idProcessoFabrica != null)
        {
            if (dataitem.idProcessoFabrica == 7)
            {
                var chkChecar = (CheckBox)e.Item.FindControl("chkChecar");
                var rdbSim = (RadioButton)e.Item.FindControl("rdbSim");
                var rdbNao = (RadioButton)e.Item.FindControl("rdbNao");

                rdbSim.GroupName = "checar" + dataitem.idPedidoFornecedorItem;
                rdbNao.GroupName = "checar" + dataitem.idPedidoFornecedorItem;

                chkChecar.Visible = false;
                rdbSim.Visible = true;
                rdbNao.Visible = true;

                btnChecarTodos.Visible = false;
                btnChecarSim.Visible = true;
                btnChecarNao.Visible = true;
            }
            else
            {

                btnChecarTodos.Visible = true;
                btnChecarSim.Visible = false;
                btnChecarNao.Visible = false;
            }
        }
    }

    protected void ddlEmpresa_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillGridAtual();
    }
}