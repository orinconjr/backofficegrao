﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxPivotGrid;
using DevExpress.XtraPivotGrid;

public partial class admin_biProdutos : System.Web.UI.Page
{
    List<MaxValueStorage> maxValue = new List<MaxValueStorage>();
    public class MaxValueStorage
    {
        decimal _value;
        string _key;
        public MaxValueStorage() { _value = decimal.MinValue; }

        public void UpdateMax(decimal _value, string key)
        {
            Value = Math.Max(_value, Value);
            Key = key;
        }
        public decimal Value { get { return _value; } set { _value = value; } }
        public string Key { get { return _key; } set { _key = value; } }
    }


    protected void ASPxPivotGrid1_CustomSummary(object sender, PivotGridCustomSummaryEventArgs e)
    {
        e.CustomValue = e.SummaryValue.Summary;
        if (ReferenceEquals(e.RowField, null)) return;
        if (e.RowFieldValue.ToString().ToLower().Trim() == "abajur")
        {
            string teste = "";
        }
        ASPxPivotGrid pivot = sender as ASPxPivotGrid;
        int lastRowFieldIndex = pivot.Fields.GetVisibleFieldCount(DevExpress.XtraPivotGrid.PivotArea.RowArea) - 1;
        if (e.RowField.AreaIndex == lastRowFieldIndex)
        {
            //this is Ordinary cell
            var maxValueAtual = maxValue.FirstOrDefault(x => x.Key == e.RowFieldValue.ToString());
            if (maxValueAtual == null) maxValueAtual = new MaxValueStorage();
            maxValueAtual.UpdateMax(Convert.ToDecimal(e.SummaryValue.Summary), e.RowFieldValue.ToString());
            maxValue.RemoveAll(x => x.Key == e.RowFieldValue.ToString());
            maxValue.Add(maxValueAtual);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (txtDataInicial.Text == "")
            {
                txtDataInicial.Text = DateTime.Now.AddDays(-1).ToShortDateString();
            }
            if (txtDataFinal.Text == "")
            {
                txtDataFinal.Text = DateTime.Now.ToShortDateString();
            }
        }
        fillGrid(false);

    }

    private void fillGrid(bool rebind)
    {
        var dataInicial = DateTime.Now;
        var dataFinal = DateTime.Now;
        DateTime.TryParse(txtDataInicial.Text, out dataInicial);
        DateTime.TryParse(txtDataFinal.Text, out dataFinal);
        dataFinal = dataFinal.AddDays(1);
        var data = new dbCommerceDataContext();

        var relatorio = (from c in data.tbItensPedidos
                         where c.tbPedido.dataHoraDoPedido >= dataInicial && c.tbPedido.dataHoraDoPedido <= dataFinal && (c.cancelado ?? false) == false
                         select new
                         {
                             c.tbPedido.pedidoId,
                             c.produtoId,
                             c.tbProduto.produtoNome,
                             categoria =
                                 c.tbProduto.tbJuncaoProdutoCategorias.FirstOrDefault(
                                     x => x.tbProdutoCategoria.categoriaPaiId == 0 && x.tbProdutoCategoria.exibirSite == true) ==
                                 null
                                     ? "Sem Categoria"
                                     : c.tbProduto.tbJuncaoProdutoCategorias.FirstOrDefault(
                                         x => x.tbProdutoCategoria.categoriaPaiId == 0 && x.tbProdutoCategoria.exibirSite == true)
                                         .tbProdutoCategoria.categoriaNomeExibicao,
                             colecao =
                                 c.tbProduto.tbJuncaoProdutoColecaos.FirstOrDefault() ==
                                 null
                                     ? "Sem Colecao"
                                     : c.tbProduto.tbJuncaoProdutoColecaos.FirstOrDefault()
                                         .tbColecao.colecaoNome,
                             c.tbProduto.tbProdutoFornecedor.fornecedorNome,
                             c.tbProduto.tbMarca.marcaNome,
                             dataHoraDoPedido = (DateTime)c.tbPedido.dataHoraDoPedido,
                             valorCusto = c.valorCusto * Convert.ToInt32(c.itemQuantidade),
                             itemValor = c.itemValor * Convert.ToInt32(c.itemQuantidade),
                             valorCobrado = (c.tbPedido.valorDosItens > 0 && c.itemValor > 0) ? (((c.itemValor * (c.tbPedido.valorCobrado - c.tbPedido.valorDoFrete)) / c.tbPedido.valorDosItens) * Convert.ToInt32(c.itemQuantidade)) : 0,
                             itemQuantidade = Convert.ToInt32(c.itemQuantidade),
                             valorPago = (c.tbPedido.valorDosItens > 0 && c.itemValor > 0) ? ((c.tbPedido.statusDoPedido == 3 | c.tbPedido.statusDoPedido == 4 | c.tbPedido.statusDoPedido == 5 | c.tbPedido.statusDoPedido == 9 | c.tbPedido.statusDoPedido == 10 | c.tbPedido.statusDoPedido == 11)
                             ? ((c.itemValor * (c.tbPedido.valorCobrado - c.tbPedido.valorDoFrete)) / c.tbPedido.valorDosItens) * Convert.ToInt32(c.itemQuantidade) : 0) : 0,
                             custoPago = c.valorCusto > 0 ? ((c.tbPedido.statusDoPedido == 3 | c.tbPedido.statusDoPedido == 4 | c.tbPedido.statusDoPedido == 5 | c.tbPedido.statusDoPedido == 9 | c.tbPedido.statusDoPedido == 10 | c.tbPedido.statusDoPedido == 11)
                             ? c.valorCusto * Convert.ToInt32(c.itemQuantidade) : 0) : 0,
                             fretePago = (c.tbPedido.valorDoFrete > 0 && c.tbPedido.valorDosItens > 0) ? ((c.tbPedido.statusDoPedido == 3 | c.tbPedido.statusDoPedido == 4 | c.tbPedido.statusDoPedido == 5 | c.tbPedido.statusDoPedido == 9 | c.tbPedido.statusDoPedido == 10 | c.tbPedido.statusDoPedido == 11)
                             ? c.itemValor * c.tbPedido.valorDoFrete / c.tbPedido.valorDosItens : 0) : 0
                         });

        if (chkHoraAtual.Checked)
            relatorio =
                relatorio.Where(
                    x =>
                        x.dataHoraDoPedido.TimeOfDay <= DateTime.Now.TimeOfDay);

        grd.DataSource = relatorio;
        if ((!Page.IsPostBack && !Page.IsCallback) | rebind)
        {
            grd.DataBind();
        }
    }
    protected void btnPequisar_Click(object sender, ImageClickEventArgs e)
    {
        fillGrid(true);
    }

    protected void chkHoraAtual_OnCheckedChanged(object sender, EventArgs e)
    {
        fillGrid(true);
    }

    protected void grd_OnCustomCellStyle(object sender, PivotCustomCellStyleEventArgs e)
    {
        if (e.ColumnValueType != DevExpress.XtraPivotGrid.PivotGridValueType.Value || e.RowValueType != DevExpress.XtraPivotGrid.PivotGridValueType.Value) return;

        try
        {
            string indice = grd.GetFieldValue(grd.Fields[e.RowField.FieldName], e.RowIndex).ToString().Trim();
            var valorMaximo = maxValue.FirstOrDefault(x => x.Key == indice);
            if (valorMaximo != null)
            {
                if (Convert.ToDecimal(e.Value) == valorMaximo.Value) e.CellStyle.BackColor = System.Drawing.Color.Aquamarine;
            }
        }
        catch (Exception)
        {

        }

    }

    protected void ASPxPivotGrid1_CustomCellValue(object sender, PivotCellValueEventArgs e)
    {
        //if (e.ColumnValueType != DevExpress.XtraPivotGrid.PivotGridValueType.Value || e.RowValueType != DevExpress.XtraPivotGrid.PivotGridValueType.Value) return;
        //string indice = grd.GetFieldValue(grd.Fields[e.RowField.FieldName], e.RowIndex).ToString().Trim();

        //var maxValueAtual = maxValue.FirstOrDefault(x => x.Key == indice);
        //if (maxValueAtual == null) maxValueAtual = new MaxValueStorage();
        //maxValueAtual.UpdateMax(Convert.ToDecimal(e.Value), indice);
        //maxValue.RemoveAll(x => x.Key == indice);
        //maxValue.Add(maxValueAtual);



    }

    protected void grd_CustomUnboundFieldData(object sender, CustomFieldDataEventArgs e)
    {

        if (e.Field.FieldName != "variacaoCmv") return;

        //decimal custo = 0;
        //Decimal.TryParse(e.GetListSourceColumnValue("CustoPagoNotNull").ToString(), out custo);

        //e.Value = custo;
        
        if (e.Value == DBNull.Value || e.Value == null || string.IsNullOrEmpty(e.Value.ToString()) || e.Value.ToString() == "null")
            e.Value = 0;
    }


}