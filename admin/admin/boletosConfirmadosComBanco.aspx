﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="boletosConfirmadosComBanco.aspx.cs" Inherits="admin_boletosConfirmadosComBanco" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" Runat="Server">
    <style >
            .meugrid {
            background-color: #F4FAFB;
            border: 0px none;
            /* border-collapse: separate !important; */
            overflow: hidden;
            font: 9pt Tahoma;
            color: #000;
            cursor: pointer;
            white-space: nowrap;
            font-weight: normal;
            text-align: left;
            width: 834px;
            border-collapse: collapse;
           margin-top: 10px;
        }

            .meugrid th {
                background-image: url(../../App_Themes/Glass/GridView/gvHeaderBackground.gif);
                background-position: center top;
                background-repeat: repeat-x;
                background-color: #C0DDE0;
                overflow: hidden;
                height: 23px;
                line-height: 23px;
                padding: 4px 6px 5px;
                font-weight: normal;
                border: 1px solid #7EACB1;
                /* border-collapse: separate;*/
            }

            .meugrid td {
                overflow: hidden;
                height: 23px;
                line-height: 23px;
                padding: 4px 6px 5px;
                border: 1px solid #7EACB1;
            }
           .meugrid tr td:nth-child(4) {
                text-align: center;
               
            }
            .meugrid tr td:last-of-type {
                text-align: center;
            }
        .linhastatus2
         {
             background-color:#ffc6c7
         }
        .linhastatus3, .linhastatus4, .linhastatus11
        {
            background-color: #daffcb;
        }
        .linhastatus5
        {
            background-color: #dbf4fd;
        }
        .linhastatus6
        {
            background-color: yellow;
        }
        .linhastatus
        {
            background-color: #f6dbdb;
        }
    </style>
     <table cellpadding="0" cellspacing="0" style="width: 920px">
        <tr>
            <td class="tituloPaginas" valign="top">
                <asp:Label ID="lblAcao" runat="server">Boletos Confirmados com Banco</asp:Label>
                <asp:Label ID="lblarquivolido" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                        
                
                        <tr class="rotulos">
                            <td>
                                Arquivo:
                            </td>
                        </tr>
                        <tr class="rotulos">
                            <td>
                                <asp:FileUpload ID="fluArquivo" runat="server" />
                            </td>
                        </tr>
                        <tr class="rotulos">
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr class="rotulos">
                            <td>
                                <asp:ImageButton ID="btnSalvar" runat="server" ImageAlign="AbsMiddle" ImageUrl="images/btImportar.jpg" onclick="btnSalvar_Click" />
                               
                            </td>
                        </tr>
                         <tr class="rotulos">
                            <td>
                                
                                <asp:Literal ID="literro" runat="server"><div style="width: 100%; float: left; clear: left; margin-top: 10px; margin-bottom: 10px; color: red; font-weight: bold;"></div></asp:Literal>
                            </td>
                        </tr>
                 
                        <tr>
                           <td >
                               <asp:Repeater ID="Repeater1" runat="server">
                                   <HeaderTemplate>
                                       <table class="meugrid" align="center">
                                           <tr>
                                               <th>Pedido</th>
                                               <th>Status</th>
                                               <th>Valor Pago</th>
                                               <th>Valor Cobrado</th>
                                           </tr>
                                   </HeaderTemplate>
                                   <ItemTemplate>
                                       <tr class="linhastatus<%# Eval("statusPedido") %>" onclick="window.open('pedido.aspx?pedidoId=<%# Eval("pedidoId") %>');">
                                           <td><%# Eval("pedidoId") %></td>
                                           <td><%# Eval("status") %></td>
                                           <td><%# Eval("valorPago") %></td>
                                           <td><%# Eval("valorCobrado") %></td>
                                        </tr>
                                   </ItemTemplate>
                                   <FooterTemplate>
                                       </table>
                                   </FooterTemplate>
                               </asp:Repeater>
                           </td>
                        </tr>
                  
                </table>
            </td>
        </tr>
    </table>
</asp:Content>

