﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_etiquetasCDDivergente : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        carregaGrid();
    }
    void carregaGrid()
    {
        var data = new dbCommerceDataContext();
        var etiquetas = (from ipe in data.tbItemPedidoEstoques
                         join pe in data.tbProdutoEstoques on ipe.idItemPedidoEstoque equals pe.idItemPedidoEstoqueReserva
                         where (pe.tbPedido.statusDoPedido == 3 | pe.tbPedido.statusDoPedido == 11)

                         where (ipe.reservado && ipe.enviado == false && ipe.cancelado == false)
                         && (ipe.idCentroDistribuicao != pe.idCentroDistribuicao)
                         select new
                         {
                             ipe.idItemPedidoEstoque,
                             pe.pedidoId,
                             CD_tbItemPedidoEstoque = ipe.idCentroDistribuicao,
                             CD_tbProdutoEstoque = pe.idCentroDistribuicao,
                         }).ToList();
        lblqtd.Text = "Qtd : " + etiquetas.Count;

        /*
          select distinct ipe.idItemPedidoEstoque,pe.pedidoId,ipe.idCentroDistribuicao,pe.idCentroDistribuicao 
      from tbItemPedidoEstoque ipe
      join   tbProdutoEstoque pe on  ipe.idItemPedidoEstoque  = pe.idItemPedidoEstoqueReserva
      join tbPedidos p on  pe.pedidoId = p.pedidoId
      where 
      (p.statusDoPedido = 3 or p.statusDoPedido = 11)
      and
      (ipe.reservado = 1 and  ipe.enviado = 0 and ipe.cancelado = 0)
      and
      ipe.idCentroDistribuicao != pe.idCentroDistribuicao
         */

        //var etiquetas = (from pe in data.tbProdutoEstoques
        //                  join ipe in data.tbItemPedidoEstoques on pe.itemPedidoId equals ipe.itemPedidoId
        //                  where (pe.tbPedido.statusDoPedido == 11 || pe.tbPedido.statusDoPedido == 6)
        //                  && (!pe.enviado && ipe.enviado)
        //                  select new
        //                  {
        //                      pe.idPedidoFornecedorItem,
        //                      pe.pedidoId,
        //                      pe.itemPedidoId,
        //                      enviadotbProdutoEstoque = pe.enviado,
        //                      enviadotbItemPedidoEstoque = ipe.enviado,
        //                      pe.tbProduto.produtoId,
        //                      pe.tbProduto.produtoNome,
        //                      pe.tbPedido.tbPedidoSituacao.situacao,
        //                      pe.tbProduto.idCentroDistribuicao

        //                  }).Distinct().OrderByDescending(x => x.pedidoId).ToList();
        GridView1.DataSource = etiquetas;
        GridView1.DataBind();
    }
}