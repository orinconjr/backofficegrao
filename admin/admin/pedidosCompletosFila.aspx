﻿<%@ Page Title="" Language="C#" EnableViewState="true" Theme="Glass" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="pedidosCompletosFila.aspx.cs" Inherits="admin_pedidosCompletosFila" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxTimer" TagPrefix="dxt" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" Runat="Server">
    
    <table cellpadding="0" cellspacing="0" style="width: 920px">
    <tr>
        <td class="tituloPaginas" valign="top">
            <asp:Label ID="lblAcao" runat="server">Pedidos Completos</asp:Label>
        </td>
    </tr>
        
    <tr>
        <td>
            
    <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
        <tr>
            <td class="rotulos" style="padding-bottom: 20px;">
                Centro de Distribuição: 
                <asp:RadioButton runat="server" ID="rdbCd1e2" Text="CD 1 e 2" GroupName="groupdCds"  AutoPostBack="true"  OnCheckedChanged="rdbCd1_CheckedChanged"/>
                <asp:RadioButton runat="server" ID="rdbCd1" Text="CD 1" GroupName="groupdCds" AutoPostBack="true"  OnCheckedChanged="rdbCd1_CheckedChanged"/>
                <asp:RadioButton runat="server" ID="rdbCd2" Text="CD 2" GroupName="groupdCds" AutoPostBack="true"  OnCheckedChanged="rdbCd2_CheckedChanged" />
                <asp:RadioButton runat="server" ID="rdbCd3" Text="CD 3" GroupName="groupdCds" AutoPostBack="true"  OnCheckedChanged="rdbCd3_CheckedChanged" />
                <asp:RadioButton runat="server" ID="rdbCd4" Text="CD 4" GroupName="groupdCds" Checked="true" AutoPostBack="true"  OnCheckedChanged="rdbCd4_CheckedChanged" />
                <asp:RadioButton runat="server" ID="rdbCd5" Text="CD 5" GroupName="groupdCds" AutoPostBack="true"  OnCheckedChanged="rdbCd5_CheckedChanged" />
                <asp:RadioButton runat="server" ID="rdbCd4e5" Text="CD 4 e 5" GroupName="groupdCds" AutoPostBack="true"  OnCheckedChanged="rdbCd5_CheckedChanged" />
            </td>
        </tr>

        <tr>
            <td>
                <dxwgv:ASPxGridView ID="grd" runat="server" AutoGenerateColumns="False" Width="834px" Cursor="auto" OnHtmlRowCreated="gridClientes_HtmlRowCreated"
                    OnCustomUnboundColumnData="grd_OnCustomUnboundColumnData" KeyFieldName="pedidoId" EnableViewState="false">
                    <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                    <Styles>
                        <Footer Font-Bold="True">
                        </Footer>
                    </Styles>
                    <SettingsText ConfirmDelete="Tem certeza que deseja excluir?" emptydatarow="Nenhum registro encontrado." />
                    <SettingsPager Position="TopAndBottom" PageSize="100" ShowDisabledButtons="False" AlwaysShowPager="True">
                        <Summary AllPagesText="Páginas: {0} - {1} ({2} items)" Text="Página {0} de {1} ({2} registros encontrados)" />
                    </SettingsPager>
                    <settings showfilterrow="True" ShowGroupButtons="False" ShowHeaderFilterButton="True" ShowFooter="True" />
                    <SettingsEditing EditFormColumnCount="4" PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1" PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter" PopupEditFormWidth="700px" />
                    <Columns>
                        <dxwgv:GridViewDataTextColumn Caption="#" VisibleIndex="0" Width="50">
                            <DataItemTemplate>
                                <%# Container.VisibleIndex + 1 %>
                            </DataItemTemplate>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="ID" FieldName="pedidoId" VisibleIndex="0" Width="50" Visible="False">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="ID" FieldName="pedidoIdCliente" VisibleIndex="0" UnboundType="Integer">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataDateColumn Caption="Cidade" FieldName="endCidade" VisibleIndex="0">
                        </dxwgv:GridViewDataDateColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Estado" FieldName="endEstado" VisibleIndex="0">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="CEP" FieldName="endCep" VisibleIndex="0" Visible="False">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataDateColumn Caption="Data Limite" FieldName="prazoFinalPedido" VisibleIndex="0">
                        </dxwgv:GridViewDataDateColumn>
                        <dxwgv:GridViewDataDateColumn Caption="Data Atualizada" FieldName="prazoMaximoPostagemAtualizado" VisibleIndex="0">
                        </dxwgv:GridViewDataDateColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Inicio Separação" FieldName="dataInicioSeparacao" VisibleIndex="0">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Fim Separação" FieldName="dataFimSeparacao" VisibleIndex="0">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Itens" FieldName="itens" VisibleIndex="0">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="fornecedores" FieldName="fornecedores" VisibleIndex="0" Settings-AutoFilterCondition="Contains">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataDateColumn Caption="prioridadeEnvio" Visible="False" FieldName="prioridadeEnvio" VisibleIndex="0">
                        </dxwgv:GridViewDataDateColumn>
                        <dxwgv:GridViewDataDateColumn Caption="formaDeEnvio" Visible="False" FieldName="formaDeEnvio" VisibleIndex="0">
                        </dxwgv:GridViewDataDateColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Alterar Envio" FieldName="enviarPor" VisibleIndex="0">
                            <Settings AutoFilterCondition="Contains" />
                            <DataItemTemplate>
                                <asp:DropDownList runat="server" ID="ddlEnviarPor">
                                    <Items>
                                        <asp:ListItem Value="" Text="Seleção Automática"></asp:ListItem>
                                        <asp:ListItem Value="1" Text="TNT"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Jadlog"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Correios"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Plimor"></asp:ListItem>
                                        <asp:ListItem Value="5" Text="LBR"></asp:ListItem>
                                        <asp:ListItem Value="6" Text="Jamef"></asp:ListItem>
                                        <asp:ListItem Value="7" Text="Dialogo"></asp:ListItem>
                                        <asp:ListItem Value="8" Text="NowLog"></asp:ListItem>
                                    </Items>
                                </asp:DropDownList>
                            </DataItemTemplate>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Prioridade" FieldName="prioridade" VisibleIndex="0">
                            <Settings AutoFilterCondition="Contains" />
                            <DataItemTemplate>
                                <asp:TextBox runat="server" ID="txtPrioridade" Width="50"></asp:TextBox>
                            </DataItemTemplate>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataHyperLinkColumn Caption="Editar" FieldName="pedidoId" VisibleIndex="7" Width="30px">
                            <PropertiesHyperLinkEdit ImageUrl="~/admin/images/btEditar.jpg" NavigateUrlFormatString="pedido.aspx?pedidoId={0}" Target="_blank">
                            </PropertiesHyperLinkEdit>
                            <Settings AllowAutoFilter="False" />
                            <HeaderTemplate>
                                <img alt="" src="images/legendaEditar.jpg" />
                            </HeaderTemplate>
                        </dxwgv:GridViewDataHyperLinkColumn>
                        <dxwgv:GridViewDataCheckColumn Caption="Separado" Visible="true" FieldName="separado" VisibleIndex="0">
                        </dxwgv:GridViewDataCheckColumn>
                    </Columns>
                    <StylesEditors>
                        <Label Font-Bold="True">
                        </Label>
                    </StylesEditors>
                </dxwgv:ASPxGridView>            
                <asp:ObjectDataSource ID="sql" runat="server" SelectMethod="admin_pedidosCompletosSeparacaoEstoque" TypeName="rnPedidos">
                </asp:ObjectDataSource>        
                <asp:ObjectDataSource ID="sqlTipoDeEntrega" runat="server" SelectMethod="tipoDeEntregaSeleciona" TypeName="rnTipoDeEntrega">
                </asp:ObjectDataSource>    
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td style="text-align: right;">
                <asp:Button runat="server" ID="btnPriorizarMoveisEm1" Visible="False" OnClick="btnPriorizarMoveisEm1_OnClick" Text="Priorizar Móveis em 1 (CD2)" ToolTip="prioriza moveis em 1 para sair mais rapido no CD2"/>
                <asp:Button runat="server" ID="btnPriorizarMoveis" Visible="False" OnClick="btnPriorizarMoveis_OnClick" Text="Priorizar Móveis" ToolTip="prioriza moveis da carolina em 88888 e da planet em 77777"/>
                <asp:Button runat="server" ID="btnGravarPrioridade" OnClick="btnGravarPrioridade_OnClick" Text="Gravar prioridade" />
            </td>
        </tr>
    </table>
        </td>
    </tr>
</table>
    </asp:Content>
