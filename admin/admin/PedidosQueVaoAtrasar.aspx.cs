﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_PedidosQueVaoAtrasar : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
            FillGrid(false);
    }

    public class Pedidos
    {
        public int? pedidoId { get; set; }
        public int? produtoId { get; set; }
        public string nomeProduto { get; set; }
        public string fornecedorNome { get; set; }
        public DateTime? dataDoPedido { get; set; }
        public DateTime? dataLimiteProduto { get; set; }
        public DateTime? dataLimiteFornecedor { get; set; }
        public DateTime prazoMaximoPostagemAtualizado { get; set; }        
        public string motivoAtraso { get; set; }
        public int? diaPrevistoAtraso { get; set; }
        public int? idPedidoFornecedor { get; set; }
        public int? idPedidoFornecedorItem { get; set; }
        public int sumarioPedidos { get; set; }
    }

    private void FillGrid(bool rebind)
    {
        var data = new dbCommerceDataContext();
        var aguardandoGeral = (from c in data.tbItemPedidoEstoques
                               where c.enviado == false && c.reservado == false && c.cancelado == false && c.dataLimite != null && c.tbItensPedido.tbPedido.statusDoPedido == 11 
                               //&& c.produtoId == 23521
                               orderby c.dataLimite
                               select new
                               {
                                   c.produtoId,
                                   c.tbItensPedido.tbPedido.dataHoraDoPedido,
                                   c.idItemPedidoEstoque,
                                   c.tbItensPedido.pedidoId,
                                   dataLimiteReserva = c.dataLimite,
                                   c.tbProduto.produtoNome,
                                   c.tbProduto.tbProdutoFornecedor.fornecedorNome,
                                   c.tbProduto.tbProdutoFornecedor.fornecedorPrazoPedidos,
                                   prazoMaximoPostagemAtualizado = c.tbItensPedido.tbPedido.prazoMaximoPostagemAtualizado ?? DateTime.Now
                               }).ToList().Select((r, i) => new { idx = i, r.idItemPedidoEstoque, r.fornecedorPrazoPedidos, r.pedidoId, r.produtoId, r.dataLimiteReserva, r.dataHoraDoPedido, r.fornecedorNome, r.produtoNome, r.prazoMaximoPostagemAtualizado }).ToList();
        var aguardando = (from c in aguardandoGeral
                          join d in aguardandoGeral on c.produtoId equals d.produtoId into aguardandos
                          select new
                          {
                              c.produtoId,
                              c.idItemPedidoEstoque,
                              c.pedidoId,
                              c.dataLimiteReserva,
                              c.dataHoraDoPedido,
                              c.fornecedorNome,
                              c.produtoNome,
                              c.prazoMaximoPostagemAtualizado,
                              c.fornecedorPrazoPedidos,
                              idx = c.idx,
                              indice = c.produtoId + "_" + ((aguardandos.OrderBy(x => x.idx).Count(x => x.produtoId == c.produtoId && x.idx < c.idx) + 1))
                          }).ToList();
        var pedidosGeral = (from c in data.tbPedidoFornecedorItems
                            join e in data.tbProdutoEstoques on c.idPedidoFornecedorItem equals e.idPedidoFornecedorItem into produtoEstoque
                            where (c.entregue == false | produtoEstoque.Any(x => (x.liberado ?? false) == false && x.enviado == false)) //&& c.idProduto == 23521
                            orderby c.tbPedidoFornecedor.dataLimite, c.tbPedidoFornecedor.idPedidoFornecedor
                            select new
                            {
                                c.idPedidoFornecedor,
                                c.idPedidoFornecedorItem,
                                dataLimiteFornecedor = c.tbPedidoFornecedor.dataLimite,
                                c.idProduto
                            }).ToList().Select((r, i) => new { idx = i, r.idPedidoFornecedor, r.idPedidoFornecedorItem, r.dataLimiteFornecedor, r.idProduto }).ToList();
        var pedidos = (from c in pedidosGeral
                       join d in pedidosGeral on c.idProduto equals d.idProduto into gerals
                       select new
                       {
                           c.idPedidoFornecedor,
                           c.idPedidoFornecedorItem,
                           c.dataLimiteFornecedor,
                           c.idProduto,
                           indice = c.idProduto + "_" + ((gerals.OrderBy(x => x.idx).Count(x => x.idProduto == c.idProduto && x.idx < c.idx) + 1))
                       }).ToList();
        var agora = DateTime.Now;
        var listaFinal = (from c in aguardando
                          join ag in aguardando on c.pedidoId equals ag.pedidoId into aguardandos
                          join d in pedidos on c.indice equals d.indice into romaneio
                          from e in romaneio.DefaultIfEmpty()
                          select new Pedidos
                          {
                              dataDoPedido = c.dataHoraDoPedido,
                              dataLimiteProduto = c.dataLimiteReserva,
                              fornecedorNome = c.fornecedorNome,
                              produtoId = c.produtoId,
                              nomeProduto = c.produtoNome,
                              pedidoId = c.pedidoId,
                              motivoAtraso = "",
                              prazoMaximoPostagemAtualizado = c.prazoMaximoPostagemAtualizado,
                              diaPrevistoAtraso = (c.dataLimiteReserva ?? DateTime.Now) > DateTime.Now ? Convert.ToInt32((e != null ? (e.dataLimiteFornecedor - agora).TotalDays : ((((c.dataLimiteReserva ?? DateTime.Now) - agora).TotalDays) + (c.fornecedorPrazoPedidos ?? 1) + 1)) < Convert.ToInt32(((c.dataLimiteReserva ?? DateTime.Now) - agora).TotalDays) ? (e != null ? (e.dataLimiteFornecedor - agora).TotalDays : ((((c.dataLimiteReserva ?? DateTime.Now) - agora).TotalDays) + (c.fornecedorPrazoPedidos ?? 1) + 1)) : Convert.ToInt32(((c.dataLimiteReserva ?? DateTime.Now) - agora).TotalDays)) : Convert.ToInt32(((c.dataLimiteReserva ?? DateTime.Now) - agora).TotalDays),
                              //diaPrevistoAtraso = Convert.ToInt32((e != null ? (e.dataLimiteFornecedor - agora).TotalDays : ((((c.dataLimiteReserva ?? DateTime.Now) - agora).TotalDays) + (c.fornecedorPrazoPedidos ?? 1) + 1)) < Convert.ToInt32(((c.dataLimiteReserva ?? DateTime.Now) - agora).TotalDays) ? (e != null ? (e.dataLimiteFornecedor - agora).TotalDays : ((((c.dataLimiteReserva ?? DateTime.Now) - agora).TotalDays) + (c.fornecedorPrazoPedidos ?? 1) + 1)) : Convert.ToInt32(((c.dataLimiteReserva ?? DateTime.Now) - agora).TotalDays)),
                              idPedidoFornecedor = e != null ? e.idPedidoFornecedor : 0,
                              dataLimiteFornecedor = e != null ? (DateTime?)e.dataLimiteFornecedor : null,
                              idPedidoFornecedorItem = e != null ? (int?)e.idPedidoFornecedorItem : null,
                              sumarioPedidos = aguardandos.Any(x => x.idx < c.idx) ? 0 : 1
                          }
            ).ToList().Where(x => x.diaPrevistoAtraso < 0);
        //.ToList().Where(x => (x.dataLimiteFornecedor > x.prazoMaximoPostagemAtualizado | x.dataLimiteFornecedor == null));

        if (rdbAtrasados.Checked) listaFinal = listaFinal.Where(x => (x.dataLimiteFornecedor ?? DateTime.Now).Date < DateTime.Now.Date);
        grd.DataSource = listaFinal.OrderBy(x => x.dataLimiteProduto).ToList();
        if ((!Page.IsPostBack && !Page.IsCallback) | rebind) grd.DataBind();
        //grd.DataBind();

    }



    protected void btPdf_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WritePdfToResponse();
    }
    protected void btXsl_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteXlsToResponse();
    }
    protected void btRtf_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteRtfToResponse();
    }
    protected void btCsv_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteCsvToResponse();
    }


    protected void rdbAtrasados_CheckedChanged(object sender, EventArgs e)
    {
        FillGrid(true);
    }
}