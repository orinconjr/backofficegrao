﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Serialization;

public partial class admin_descricaoTabelaGrupo : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        fillGrid(false);
        if (!Page.IsPostBack)
        {
            fillDropSite();
        }
    }

    private void fillGrid(bool rebind)
    {
        var data = new dbCommerceDataContext();
        var produtos = (from c in data.tbDescricaoTabelaProdutoGrupos select c);
        grd.DataSource = produtos;
        if ((!Page.IsPostBack && !Page.IsCallback) | rebind) grd.DataBind();
    }
    protected void btnAdicionar_OnClick(object sender, EventArgs e)
    {
        limpaCampos();
        fillGridDescricoes();
        fillSimulacaoTabela();
        hdfProdutoAlterar.Value = "";
        pnNovo.Visible = true;
        pnItens.Visible = false;
        fillDropProdutos();
    }

    private void fillSimulacaoTabela()
    {
        litNomeAmigavel.Text = txtNomeSite.Text;
    }
    private void fillDropSite()
    {
        var data = new dbCommerceDataContext();
        var sites = (from c in data.tbSites orderby c.idSite select c);
        ddlSite.DataSource = sites;
        ddlSite.DataBind();
    }
    private void fillDropProdutos()
    {
        int idSite = 1;
        int.TryParse(ddlSite.SelectedValue, out idSite);
        var data = new dbCommerceDataContext();
        var produtos = (from c in data.tbDescricaoTabelaProdutos where c.idSite == idSite orderby c.nomeInterno select c);
        ddlProdutos.DataSource = produtos;
        ddlProdutos.DataBind();
    }
    protected void btnAdicionarDescricao_OnClick(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(ddlProdutos.SelectedValue))
        {
            AlertShow("Favor selecionar um produto");
            return;
        }

        int idProduto = 1;
        int.TryParse(ddlProdutos.SelectedValue, out idProduto);


        if (string.IsNullOrEmpty(hdfProdutoAlterar.Value))
        {
            var lista = new List<tbDescricaoTabelaProdutoGrupoItem>();
            if (!string.IsNullOrEmpty(txtListaProdutos.Text))
            {
                lista = Deserialize(txtListaProdutos.Text);
            }

            var item = new tbDescricaoTabelaProdutoGrupoItem
            {
                idDescricaoTabelaProduto = idProduto,
                idDescricaoTabelaProdutoGrupoItem = lista.Any() ? (lista.OrderByDescending(x => x.idDescricaoTabelaProdutoGrupoItem).FirstOrDefault().idDescricaoTabelaProdutoGrupoItem + 1) : 1
            };
            lista.Add(item);
            txtListaProdutos.Text = Serialize(lista);
            ddlProdutos.Focus();
        }
        else
        {
            int id = Convert.ToInt32(hdfProdutoAlterar.Value);
            var data = new dbCommerceDataContext();
            var item = new tbDescricaoTabelaProdutoGrupoItem
            {
                idDescricaoTabelaProduto = idProduto,
                idDescricaoTabelaProdutoGrupo = id
            };
            data.tbDescricaoTabelaProdutoGrupoItems.InsertOnSubmit(item);
            data.SubmitChanges();
        }
        fillSimulacaoTabela();
        fillGridDescricoes();
    }

    private void fillGridDescricoes()
    {
        if (string.IsNullOrEmpty(hdfProdutoAlterar.Value))
        {
            var lista = new List<tbDescricaoTabelaProdutoGrupoItem>();
            if (!string.IsNullOrEmpty(txtListaProdutos.Text))
            {
                lista = Deserialize(txtListaProdutos.Text);
            }

            lstItensAdicionar.DataSource = lista;
            lstItensAdicionar.DataBind();
            lstProdutosSimulacao.DataSource = lista;
            lstProdutosSimulacao.DataBind();
        }
        else
        {
            var data = new dbCommerceDataContext();
            int id = Convert.ToInt32(hdfProdutoAlterar.Value);
            var itens = (from c in data.tbDescricaoTabelaProdutoGrupoItems where c.idDescricaoTabelaProdutoGrupo == id select c);
            lstItensAdicionar.DataSource = itens;
            lstItensAdicionar.DataBind();
            lstProdutosSimulacao.DataSource = itens;
            lstProdutosSimulacao.DataBind();
        }
    }
    public void AlertShow(string message)
    {
        base.ClientScript.RegisterStartupScript(base.GetType(), "myalert", "alert('" + message + "'); ", true);
    }
    public static string Serialize(List<tbDescricaoTabelaProdutoGrupoItem> tData)
    {
        var serializer = new XmlSerializer(typeof(List<tbDescricaoTabelaProdutoGrupoItem>));

        TextWriter writer = new StringWriter();
        serializer.Serialize(writer, tData);

        return writer.ToString();
    }

    public static List<tbDescricaoTabelaProdutoGrupoItem> Deserialize(string tData)
    {
        var serializer = new XmlSerializer(typeof(List<tbDescricaoTabelaProdutoGrupoItem>));

        TextReader reader = new StringReader(tData);

        return (List<tbDescricaoTabelaProdutoGrupoItem>)serializer.Deserialize(reader);
    }

    protected void btnRemoverProduto_OnCommand(object sender, CommandEventArgs e)
    {
        if (string.IsNullOrEmpty(hdfProdutoAlterar.Value))
        {
            int id = Convert.ToInt32(e.CommandArgument);
            var lista = new List<tbDescricaoTabelaProdutoGrupoItem>();
            if (!string.IsNullOrEmpty(txtListaProdutos.Text))
            {
                lista = Deserialize(txtListaProdutos.Text);
            }
            lista.RemoveAll(x => x.idDescricaoTabelaProdutoGrupoItem == id);
            txtListaProdutos.Text = Serialize(lista);
        }
        else
        {
            var data = new dbCommerceDataContext();
            int id = Convert.ToInt32(e.CommandArgument);
            var item =
                (from c in data.tbDescricaoTabelaProdutoGrupoItems where c.idDescricaoTabelaProdutoGrupoItem == id select c).First
                    ();
            data.tbDescricaoTabelaProdutoGrupoItems.DeleteOnSubmit(item);
            data.SubmitChanges();
        }
        fillSimulacaoTabela();
        fillGridDescricoes();
    }

    protected void btnGravarProduto_OnClick(object sender, EventArgs e)
    {
        if (txtNomeInterno.Text == "")
        {
            AlertShow("Favor preencher o nome interno");
            txtNomeInterno.Focus();
            return;
        }
        if (txtNomeSite.Text == "")
        {
            AlertShow("Favor preencher o nome de exibição no site");
            txtNomeSite.Focus();
            return;
        }
        if (string.IsNullOrEmpty(hdfProdutoAlterar.Value))
        {
            var lista = new List<tbDescricaoTabelaProdutoGrupoItem>();
            if (!string.IsNullOrEmpty(txtListaProdutos.Text))
            {
                lista = Deserialize(txtListaProdutos.Text);
            }
            if (!lista.Any())
            {
                AlertShow("Favor adicionar itens à lista");
                return;
            }
            var data = new dbCommerceDataContext();
            var produto = new tbDescricaoTabelaProdutoGrupo();
            produto.idSite = Convert.ToInt32(ddlSite.SelectedValue);
            produto.nomeInterno = txtNomeInterno.Text;
            produto.nomeAmigavel = txtNomeSite.Text;
            data.tbDescricaoTabelaProdutoGrupos.InsertOnSubmit(produto);
            data.SubmitChanges();

            foreach (var item in lista)
            {
                var descricaoItem = new tbDescricaoTabelaProdutoGrupoItem();
                descricaoItem.idDescricaoTabelaProdutoGrupo = produto.idDescricaoTabelaProdutoGrupo;
                descricaoItem.idDescricaoTabelaProduto = item.idDescricaoTabelaProduto;
                data.tbDescricaoTabelaProdutoGrupoItems.InsertOnSubmit(descricaoItem);
            }
            data.SubmitChanges();
        }
        else
        {
            int id = Convert.ToInt32(hdfProdutoAlterar.Value);
            var data = new dbCommerceDataContext();
            var produto = (from c in data.tbDescricaoTabelaProdutoGrupos where c.idDescricaoTabelaProdutoGrupo == id select c).FirstOrDefault();
            produto.idSite = Convert.ToInt32(ddlSite.SelectedValue);
            produto.nomeInterno = txtNomeInterno.Text;
            produto.nomeAmigavel = txtNomeSite.Text;
            data.SubmitChanges();
        }
        fillGrid(true);
        limpaCampos();
        AlertShow("Produto gravado com sucesso");
        pnItens.Visible = true;
        pnNovo.Visible = false;
    }

    private void limpaCampos()
    {
        hdfProdutoAlterar.Value = "";
        txtListaProdutos.Text = "";
        txtNomeInterno.Text = "";
        txtNomeSite.Text = "";
    }

    protected void btnEditar_OnCommand(object sender, CommandEventArgs e)
    {
        limpaCampos();
        int id = Convert.ToInt32(e.CommandArgument);
        var data = new dbCommerceDataContext();
        var pedido = (from c in data.tbDescricaoTabelaProdutoGrupos where c.idDescricaoTabelaProdutoGrupo == id select c).First();
        hdfProdutoAlterar.Value = id.ToString();
        txtNomeInterno.Text = pedido.nomeInterno;
        txtNomeSite.Text = pedido.nomeAmigavel;
        ddlSite.SelectedValue = pedido.idSite.ToString();
        fillDropProdutos();
        fillGridDescricoes();
        fillSimulacaoTabela();
        pnItens.Visible = false;
        pnNovo.Visible = true;
    }

    protected void lstItensAdicionar_OnItemDataBound(object sender, ListViewItemEventArgs e)
    {
        Literal litNomeAmigavel = (Literal) e.Item.FindControl("litNomeAmigavel");
        var item = (tbDescricaoTabelaProdutoGrupoItem)e.Item.DataItem;
        var data = new dbCommerceDataContext();
        var produto =
            (from c in data.tbDescricaoTabelaProdutos
                where c.idDescricaoTabelaProduto == item.idDescricaoTabelaProduto
                select c).FirstOrDefault();
        if (produto != null)
        {
            litNomeAmigavel.Text = produto.nomeAmigavel;
        }
    }

    protected void ddlSite_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        fillDropProdutos();
    }

    protected void lstProdutosSimulacao_OnItemDataBound(object sender, ListViewItemEventArgs e)
    {
        Literal litNomeAmigavel = (Literal)e.Item.FindControl("litNomeAmigavel");
        ListView lstProdutosSimulacaoItensQuantidades = (ListView)e.Item.FindControl("lstProdutosSimulacaoItensQuantidades");
        ListView lstProdutosSimulacaoItensDescricoes = (ListView)e.Item.FindControl("lstProdutosSimulacaoItensDescricoes");
        var item = (tbDescricaoTabelaProdutoGrupoItem)e.Item.DataItem;
        var data = new dbCommerceDataContext();
        var produto =
            (from c in data.tbDescricaoTabelaProdutos
             where c.idDescricaoTabelaProduto == item.idDescricaoTabelaProduto
             select c).FirstOrDefault();
        if (produto != null)
        {
            litNomeAmigavel.Text = produto.nomeAmigavel;
        }
        var itens =
            (from c in data.tbDescricaoTabelaProdutoItems
                where c.idDescricaoTabelaProduto == item.idDescricaoTabelaProduto
                select c);
        lstProdutosSimulacaoItensQuantidades.DataSource = itens;
        lstProdutosSimulacaoItensQuantidades.DataBind();
        lstProdutosSimulacaoItensDescricoes.DataSource = itens;
        lstProdutosSimulacaoItensDescricoes.DataBind();
    }

    protected void btnCancelarProduto_OnClick(object sender, EventArgs e)
    {
        fillGrid(true);
        limpaCampos();
        pnItens.Visible = true;
        pnNovo.Visible = false;
    }
}