﻿<%@ Page Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="vip.aspx.cs" Inherits="admin_marcas"  Theme="Glass" MaintainScrollPositionOnPostback=true %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dxwgv" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" Runat="Server">
    <table cellpadding="0" cellspacing="0" style="width: 920px">
    <tr>
        <td class="tituloPaginas" valign="top">
            <asp:Label ID="lblAcao" runat="server">VIP</asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            
    <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
        <tr>
            <td align="right">
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
            <dxwgv:ASPxGridView ID="grdExportar" runat="server" AutoGenerateColumns="False" 
                DataSourceID="sqlEmail" KeyFieldName="idVip" Width="880px" 
                Cursor="auto">
                <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                <Styles>
                    <Footer Font-Bold="True">
                    </Footer>
                </Styles>
                <SettingsText ConfirmDelete="Tem certeza que deseja excluir?" 
                    emptydatarow="Nenhum registro encontrado." />
                <SettingsPager Position="TopAndBottom" PageSize="50" 
                    ShowDisabledButtons="False" AlwaysShowPager="True">
                    <Summary AllPagesText="Páginas: {0} - {1} ({2} items)" 
                        Text="Página {0} de {1} ({2} registros encontrados)" />
                </SettingsPager>
                <settings showfilterrow="True" ShowGroupButtons="False" 
                    ShowHeaderFilterButton="True" ShowFooter="True" />
                <TotalSummary>
                    <dxwgv:ASPxSummaryItem DisplayFormat="c" FieldName="valorCobrado" 
                        ShowInColumn="Valor" ShowInGroupFooterColumn="Valor" SummaryType="Sum" />
                </TotalSummary>
                <SettingsEditing EditFormColumnCount="4" 
                    PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1" 
                    PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter" 
                    PopupEditFormWidth="700px" />
                <Columns>
                    <dxwgv:GridViewDataTextColumn Caption="Nome" FieldName="nmVip" VisibleIndex="0">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="E-mail" FieldName="nmEmail" VisibleIndex="0">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Sexo Masculino" FieldName="icSexoMasculino" VisibleIndex="0">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                </Columns>
                <StylesEditors>
                    <Label Font-Bold="True">
                    </Label>
                </StylesEditors>
            </dxwgv:ASPxGridView>

                <asp:SqlDataSource ID="sqlEmail" runat="server" ConnectionString="<%$ ConnectionStrings:connectionString %>" 
                SelectCommand="Select idVip, nmVip, nmEmail, icSexoMasculino, dtCadastroVip from tbVip">
                </asp:SqlDataSource>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
    </table>
        </td>
    </tr>
</table>
    </asp:Content>

