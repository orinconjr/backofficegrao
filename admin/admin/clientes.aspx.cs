﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxTabControl;
using DevExpress.Web.ASPxEditors;
using DevExpress.Data;
using DevExpress.Data.Linq;
using DevExpress.Web.ASPxUploadControl;
using System.IO;
using System.Data.SqlClient;

public partial class admin_clientes : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ASPxGridView.RegisterBaseScript(Page);
        grd.DataSourceID = "sqlClientesServer";

    }


    protected void sqlClientesServer_OnSelecting(object sender, LinqServerModeDataSourceSelectEventArgs e)
    {
        var data = new dbCommerceDataContext();
        e.KeyExpression = "clienteId";
        //e.QueryableSource = rblAtivos.SelectedValue != "Todos" ? data.viewClientesAdmins.Where(x => x.produtoAtivo == rblAtivos.SelectedValue) : data.viewClientesAdmins;
        e.QueryableSource = data.viewClientesAdmins;

    }



    protected void grd_ParseValue(object sender, DevExpress.Web.Data.ASPxParseValueEventArgs e)
    {
        if (e.FieldName == "clienteSexo")
        {
            if(e.Value==null)
            {
                object sexo = (object)"";
                e.Value = sexo;
            }
         
        }

        if (e.FieldName == "clienteDataNascimento")
        {
            if(e.Value==null)
            {
                object clienteDataNascimento = (object)new DateTime(1900, 1, 1, 0, 0, 0);
                e.Value = clienteDataNascimento;
            }
         
        }
        if (e.FieldName == "clienteRecebeInformativo")
        {
            if(e.Value==null)
            {
                object clienteRecebeInformativo = (object)"False";
                e.Value = clienteRecebeInformativo;
            }
         
        }

        if (e.FieldName == "clienteFoneResidencial")
        {
            if (e.Value == null)
            {
                object clienteFoneResidencial = (object)"";
                e.Value = clienteFoneResidencial;
            }

        }
        
    }
}
