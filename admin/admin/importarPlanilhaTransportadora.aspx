﻿<%@ Page Title="" Theme="Glass" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="importarPlanilhaTransportadora.aspx.cs" Inherits="admin_importarPlanilhaTransportadora" %>

<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxTimer" TagPrefix="dxt" %>
<%@ Register TagPrefix="dxe" Namespace="DevExpress.Web.ASPxMenu" Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxPopupControl" Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxEditors" Assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>
<%@ Register Assembly="CuteEditor" Namespace="CuteEditor" TagPrefix="CE" %>

<%@ Register Assembly="DevExpress.Web.v11.1" Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" runat="Server">
    <asp:ScriptManager ID="scpManager" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="updPanel" runat="server">
        <ContentTemplate>
            <dx:ASPxPopupControl ID="popCadastrarTabela" runat="server" CloseAction="CloseButton" Modal="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
                ClientInstanceName="popCadastrarTabela" HeaderText="Cadastro de tabela de transportadora" AllowDragging="True" popupanimationtype="None" EnableViewState="True" Height="500" AutoUpdatePosition="True">
                <ContentCollection>
                    <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server">
                        <asp:HiddenField runat="server" ID="hdnTipoEntregaId" />
                        <div style="width: 800px; height: 600px;">
                            <table width="100%">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblMensagemTipoEntrega" runat="server" ForeColor="Red"></asp:Label></td>
                                </tr>
                                <tr class="rotulos">
                                    <td>
                                        <b>Transportadora</b><br />
                                        <asp:DropDownList ID="ddlTransportadoraTabela" runat="server"></asp:DropDownList>
                                    </td>
                                </tr>
                                <tr class="rotulos">
                                    <td>
                                        <b>Nome</b><br />
                                        <asp:TextBox runat="server" ID="txtNomeTipoEntrega" Width="50%"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr class="rotulos">
                                    <td>
                                        <b>Fator cubagem</b> &nbsp;
                                        <br />
                                        <asp:TextBox ID="txtFatorCubagem" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr class="rotulos">
                                    <td>
                                        <b>Limite de peso (g) *</b><br />
                                        <asp:TextBox ID="txtLimitePesoa" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr class="rotulos">
                                    <td>
                                        <b>Limite Maior Dimensão (cm) *</b><br />
                                        <asp:TextBox ID="txtLimiteMaiorDimensao" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr class="rotulos">
                                    <td>
                                        <b>Limite Soma Dimensão (cm) *</b><br />
                                        <asp:TextBox ID="txtLimiteSomaDimensao" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr class="rotulos">
                                    <td>
                                        <table width="100%">
                                            <tr>
                                                <td style="width: 25%"><b>Peso adicional</b></td>
                                                <td style="width: 25%"><b>Cubagem</b></td>
                                                <td style="width: 25%"><b>Expedição</b></td>
                                                <td style="width: 25%"><b>Simulação</b></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:CheckBox ID="chkPesoAdicional" runat="server" /></td>
                                                <td>
                                                    <asp:CheckBox ID="chkCubagem" runat="server" /></td>
                                                <td>
                                                    <asp:CheckBox ID="chkExpedicao" runat="server" /></td>
                                                <td>
                                                    <asp:CheckBox ID="chkSimulacao" runat="server" /></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td><span style="font-size: x-small; color: red">* 0 - Ilimitado</span></td>
                                </tr>
                                <tr>
                                    <td class="rotulos" colspan="2" style="text-align: right">
                                        <asp:Button runat="server" ID="btnGravarTipoDeEntrega" OnClick="btnGravarTipoDeEntrega_Click" Text="Gravar" />
                                    </td>
                                </tr>

                            </table>

                            <br />
                            <div style="height: 250px; overflow-y: auto; overflow-x: hidden">
                                <table style="width: 834px">
                                    <tr>
                                        <td>
                                            <dxwgv:ASPxGridView ID="gvTipoEntrega" ClientInstanceName="gvTipoEntrega" runat="server" AutoGenerateColumns="False"
                                                KeyFieldName="tipoDeEntregaId" Width="814px" Settings-ShowFilterRow="True" Settings-ShowFilterBar="Visible"
                                                Cursor="auto" EnableViewState="False">
                                                <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                                                <Styles>
                                                    <Footer Font-Bold="True">
                                                    </Footer>
                                                </Styles>
                                                <SettingsText ConfirmDelete="Tem certeza que deseja excluir?" EmptyDataRow="Nenhum registro encontrado." />
                                                <SettingsPager Position="TopAndBottom" PageSize="500"
                                                    ShowDisabledButtons="False" AlwaysShowPager="True">
                                                    <Summary AllPagesText="Páginas: {0} - {1} ({2} items)"
                                                        Text="Página {0} de {1} ({2} registros encontrados)" />
                                                </SettingsPager>
                                                <Settings ShowFilterRow="True" ShowGroupButtons="False" ShowFooter="True" />
                                                <SettingsEditing EditFormColumnCount="4"
                                                    PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1"
                                                    PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter"
                                                    PopupEditFormWidth="700px" />
                                                <Columns>
                                                    <dxwgv:GridViewDataTextColumn Caption="Id" FieldName="tipoDeEntregaId" VisibleIndex="0" Width="30px">
                                                        <Settings AutoFilterCondition="Contains" />
                                                        <EditFormSettings Visible="False" />
                                                    </dxwgv:GridViewDataTextColumn>
                                                    <dxwgv:GridViewDataTextColumn Caption="Transportadora" FieldName="idTransportadoraString" Name="transportadora" VisibleIndex="0" Width="50px">
                                                        <Settings AutoFilterCondition="Contains" />
                                                        <EditFormSettings Visible="False" />
                                                    </dxwgv:GridViewDataTextColumn>
                                                    <dxwgv:GridViewDataTextColumn Caption="Nome" FieldName="tipoDeEntregaNome" VisibleIndex="0" Width="300px">
                                                        <Settings AutoFilterCondition="Contains" />
                                                        <EditFormSettings Visible="False" />
                                                    </dxwgv:GridViewDataTextColumn>
                                                    <dxwgv:GridViewDataTextColumn Caption="Alterar" Name="alterar" VisibleIndex="0" Width="100px">
                                                        <DataItemTemplate>
                                                            <asp:ImageButton ID="btnAlterarTipoEntrega" CommandArgument='<%# Eval("tipoDeEntregaId") %>' CommandName="AlterarTabela"
                                                                OnCommand="btnAlterarTipoEntrega_Command" runat="server" ImageAlign="AbsMiddle" ImageUrl="images/btAlterar.jpg" />
                                                        </DataItemTemplate>
                                                    </dxwgv:GridViewDataTextColumn>
                                                </Columns>
                                                <StylesEditors>
                                                    <Label Font-Bold="True">
                                                    </Label>
                                                </StylesEditors>
                                            </dxwgv:ASPxGridView>
                                            <br />
                                        </td>
                                    </tr>
                                </table>
                            </div>

                        </div>
                    </dx:PopupControlContentControl>
                </ContentCollection>
                <ContentStyle>
                    <Paddings PaddingBottom="5px" />
                </ContentStyle>
            </dx:ASPxPopupControl>
            <dx:ASPxPopupControl ID="popCadastrarTransportadora" runat="server" CloseAction="CloseButton" Modal="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
                ClientInstanceName="popCadastrarTransportadora" HeaderText="Cadastro de transportadora" AllowDragging="True" popupanimationtype="None" EnableViewState="True" Height="500" AutoUpdatePosition="True">
                <ContentCollection>
                    <dx:PopupControlContentControl ID="PopupControlContentControl6" runat="server">
                        <asp:HiddenField runat="server" ID="hdfIdTransportadora" />
                        <div style="width: 800px; height: 600px;">
                            <table width="100%">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblMensagemTransportadora" runat="server" ForeColor="Red"></asp:Label></td>
                                </tr>
                                <tr class="rotulos">
                                    <td>
                                        <b>Transportadora</b><br />
                                        <asp:TextBox runat="server" ID="txtNomeTransportadora" Width="50%"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="rotulos" colspan="2" style="text-align: right">
                                        <asp:Button runat="server" ID="btnGravar" OnClick="btnGravar_Click" Text="Gravar" />
                                    </td>
                                </tr>
                            </table>
                            <br />
                            <div style="height: 450px; overflow-y: auto;">
                                <table style="width: 100%;">
                                    <tr>
                                        <td>
                                            <asp:GridView ID="gvTransportadora" AutoGenerateColumns="false" runat="server" DataKeyNames="idTransportadora" Width="100%">
                                                <Columns>
                                                    <asp:BoundField DataField="idTransportadora" HeaderText="ID" ItemStyle-Width="5%">
                                                        <ItemStyle Width="5%"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="transportadora" HeaderText="Nome" ItemStyle-Width="95%">
                                                        <ItemStyle Width="95%"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="btnAlterar" ImageUrl="~/admin/images/btAlterar.jpg" runat="server"
                                                                OnCommand="btnAlterar_Command" CommandArgument='<%# Eval("idTransportadora") %>' CommandName="AlterarTransportadora" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </dx:PopupControlContentControl>
                </ContentCollection>
                <ContentStyle>
                    <Paddings PaddingBottom="5px" />
                </ContentStyle>
            </dx:ASPxPopupControl>
        </ContentTemplate>
    </asp:UpdatePanel>

    <table cellpadding="0" cellspacing="0" style="width: 920px">
        <tr>
            <td class="tituloPaginas" valign="top">
                <asp:Label ID="lblAcao" runat="server">Importar Planilha de Transportadora</asp:Label>
            </td>
        </tr>
        <tr>
            <td style="padding-left: 10px">
                <asp:Label ID="lblMensagem" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
                    <tr>
                        <td class="rotulos">
                            <asp:Button ID="btnCadastrarTransportadora" runat="server" Text="Cadastrar transportadora" OnClick="btnCadastrarTransportadora_Click" />
                        </td>
                        <td class="rotulos">
                            <asp:Button ID="btnCadastrarTabelaTransportadora" runat="server" Text="Cadastrar Tabela da Transportadora" OnClick="btnCadastrarTabelaTransportadora_Click" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
                    <tr>
                        <td class="rotulos">Planilha:<br />
                            <asp:FileUpload runat="server" CssClass="fileUpload" ID="fupPlanilha" /><br />
                        </td>
                    </tr>
                    <tr>
                        <td class="rotulos" style="padding-top: 15px;">
                            <table width="100%">
                                <tr>
                                    <td><b>Transportadora</b></td>
                                    <td><b>Tabela</b></td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:DropDownList runat="server" ID="ddlTransportadora" DataValueField="idTransportadora" OnSelectedIndexChanged="ddlTransportadora_SelectedIndexChanged" DataTextField="transportadora" AutoPostBack="True"></asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="ddlTabelaTransportadora" DataValueField="tipoDeEntregaId" DataTextField="tipoDeEntregaNome"></asp:DropDownList>
                                    </td>
                                </tr>
                            </table>


                        </td>
                    </tr>
                    <tr>
                        <td class="rotulos" style="padding-top: 15px;">
                            <asp:Button runat="server" ID="btnImportar" CssClass="btnexportar" Text="Importar" OnClick="btnImportar_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right;">&nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
