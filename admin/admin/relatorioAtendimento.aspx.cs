﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CarlosAg.ExcelXmlWriter;
using DevExpress.Data;
using DevExpress.Data.Filtering;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;

public partial class admin_relatorioAtendimento : System.Web.UI.Page
{

    DateTemplate dateTemplate = null;

    protected void Page_Load(object sender, EventArgs e)
    {
           FillGrid();
    }

    protected void btPdf_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WritePdfToResponse();
    }
    protected void btXsl_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteXlsToResponse();
    }
    protected void btRtf_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteRtfToResponse();
    }
    protected void btCsv_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteCsvToResponse();
    }

   

    private void FillGrid()
    {

        var db = new dbCommerceDataContext();

       var linqResult = (from atend in db.tbAtendimentos
                         join tbPed in db.tbPedidos on atend.pedidoId equals tbPed.pedidoId into _ate
                         from tbPed in _ate.DefaultIfEmpty()
                         join usu in db.tbUsuarios on atend.usuarioId equals usu.usuarioId into _usu
                         from usu in _usu.DefaultIfEmpty()
                         join status in db.tbPedidoSituacaos on tbPed.statusDoPedido equals status.situacaoId into _status
                         from status in _status.DefaultIfEmpty()

                         select new
                         {
                             idAtendimento = atend.idAtendimento,
                             atendente = usu.usuarioNome,
                             pedido = tbPed.pedidoId,
                             status =  status.situacao,
                             dataPedido = tbPed.dataHoraDoPedido,
                             dataAtendimento = atend.dataAtendimento,
                             dataPagamento = tbPed.dataConfirmacaoPagamento,
                             valor = tbPed.valorTotalGeral


                         }).ToList().OrderByDescending(x => x.idAtendimento);

    

        grd.DataSource = linqResult;
        grd.DataBind();
    }

    protected void grd_OnCustomUnboundColumnData(object sender, ASPxGridViewColumnDataEventArgs e)
    {

    }

    protected void grd_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        try
        {

            int idAtendimento = 0;
            int.TryParse(grd.GetRowValues(e.VisibleIndex, new string[] { "idAtendimento" }).ToString(), out idAtendimento);
            LinkButton btnVer = (LinkButton)grd.FindRowCellTemplateControl(e.VisibleIndex, grd.Columns["ver"] as GridViewDataColumn, "btnVer");
        }
        catch (Exception)
        {

        }
        if (e.RowType != GridViewRowType.Data)
        {
            return;
        }

    }

    protected void btRefresh_OnClick(object sender, EventArgs e)
    {
        FillGrid();
    }

    protected void grid_AutoFilterCellEditorCreate(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewEditorCreateEventArgs e)
    {
       
        ASPxGridView grid = sender as ASPxGridView;
        if (e.Column.FieldName == "dataPedido")
        {
            DropDownEditProperties dde = new DropDownEditProperties();
            dde.EnableClientSideAPI = true;

            dateTemplate = new DateTemplate();

            dde.DropDownWindowTemplate = dateTemplate;
            e.EditorProperties = dde;
        }

        if (e.Column.FieldName == "dataAtendimento")
        {
            DropDownEditProperties dde = new DropDownEditProperties();
            dde.EnableClientSideAPI = true;

            dateTemplate = new DateTemplate();

            dde.DropDownWindowTemplate = dateTemplate;
            e.EditorProperties = dde;
        }

        if (e.Column.FieldName == "dataPagamento")
        {
            DropDownEditProperties dde = new DropDownEditProperties();
            dde.EnableClientSideAPI = true;

            dateTemplate = new DateTemplate();

            dde.DropDownWindowTemplate = dateTemplate;
            e.EditorProperties = dde;
        }
    }

    protected void grid_AutoFilterCellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
    {
        ASPxGridView grid = sender as ASPxGridView;
        if (e.Column.FieldName == "dataPedido")
        {
            ASPxDropDownEdit dde = e.Editor as ASPxDropDownEdit;

            dde.ClientSideEvents.CloseUp = String.Format("function (s, e) {{ ApplyFilter(s, {0}, {1}, 'data'); }}", dateTemplate.cIdFrom, dateTemplate.cIdTo);
            dde.ReadOnly = true;
        }

        if (e.Column.FieldName == "dataAtendimento")
        {
            ASPxDropDownEdit dde = e.Editor as ASPxDropDownEdit;

            dde.ClientSideEvents.CloseUp = String.Format("function (s, e) {{ ApplyFilter(s, {0}, {1}, 'data'); }}", dateTemplate.cIdFrom, dateTemplate.cIdTo);
            dde.ReadOnly = true;
        }
        if (e.Column.FieldName == "dataPagamento")
        {
            ASPxDropDownEdit dde = e.Editor as ASPxDropDownEdit;

            dde.ClientSideEvents.CloseUp = String.Format("function (s, e) {{ ApplyFilter(s, {0}, {1}, 'data'); }}", dateTemplate.cIdFrom, dateTemplate.cIdTo);
            dde.ReadOnly = true;
        }


    }
    protected void grid_ProcessColumnAutoFilter(object sender, ASPxGridViewAutoFilterEventArgs e)
    {
       
        if (e.Value == "|") return;
        if (e.Column.FieldName == "dataPedido")
        {
            if (e.Kind == GridViewAutoFilterEventKind.CreateCriteria)
            {
                Session["dataPedido"] = e.Value;
                String[] dates = e.Value.Split('|');
                DateTime dateFrom = Convert.ToDateTime(dates[0]),
                    dateTo = Convert.ToDateTime(dates[1]);
                e.Criteria = (new OperandProperty("dataPedido") >= dateFrom) &
                             (new OperandProperty("dataPedido") <= dateTo);
            }
            else
            {
                if (Session["dataPedido"] != null)
                    e.Value = Session["dataPedido"].ToString();
            }
        }

        if (e.Value == "|") return;
        if (e.Column.FieldName == "dataAtendimento")
        {
            if (e.Kind == GridViewAutoFilterEventKind.CreateCriteria)
            {
                Session["dataAtendimento"] = e.Value;
                String[] dates = e.Value.Split('|');
                DateTime dateFrom = Convert.ToDateTime(dates[0]),
                    dateTo = Convert.ToDateTime(dates[1]);
                e.Criteria = (new OperandProperty("dataAtendimento") >= dateFrom) &
                             (new OperandProperty("dataAtendimento") <= dateTo);
            }
            else
            {
                if (Session["dataAtendimento"] != null)
                    e.Value = Session["dataAtendimento"].ToString();
            }
        }

        if (e.Value == "|") return;
        if (e.Column.FieldName == "dataPagamento")
        {
            if (e.Kind == GridViewAutoFilterEventKind.CreateCriteria)
            {
                Session["dataPagamento"] = e.Value;
                String[] dates = e.Value.Split('|');
                DateTime dateFrom = Convert.ToDateTime(dates[0]),
                    dateTo = Convert.ToDateTime(dates[1]);
                e.Criteria = (new OperandProperty("dataPagamento") >= dateFrom) &
                             (new OperandProperty("dataPagamento") <= dateTo);
            }
            else
            {
                if (Session["dataPagamento"] != null)
                    e.Value = Session["dataPagamento"].ToString();
            }
        }



    }

    protected void grd_OnDataBound(object sender, EventArgs e)
    {
        int inicio = 0;
        int fim = 0;

        if (grd.PageIndex == 0)
        {
            inicio = 0;
            fim = inicio + grd.GetCurrentPageRowValues("idAtendimento").Count;
        }
        else
        {
            inicio += inicio + grd.SettingsPager.PageSize * grd.PageIndex;
            fim += inicio + grd.GetCurrentPageRowValues("idAtendimento").Count;
        }

        var listaAtendimento = new List<int>();
        for (int i = inicio; i < fim; i++)
        {
            var idPedidoFornecedorCheck = grd.GetRowValues(i, new string[] { "idAtendimento" });
            if (!string.IsNullOrEmpty(idPedidoFornecedorCheck.ToString().Trim()))
            {
                int idPedidoFornecedor = Convert.ToInt32(idPedidoFornecedorCheck);
                listaAtendimento.Add(idPedidoFornecedor);
            }
        }


       
    }

}
