﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="pedidosMotivosInclusaoItem.aspx.cs" Inherits="admin_pedidosMotivosInclusaoItem" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" runat="Server">
    <script src="js/jquery.min.js"></script>
    <style>
        .meubotao{
            cursor:pointer;
            font:bold 14px tahoma;
        }
        .fieldsetatualizarprecos {
            width: 848px;
            float: left;
            clear: left;
            border-radius: 5px;
            margin-left: 25px;
            font:bold 14px tahoma;
        }

            .fieldsetatualizarprecos input[type=checkbox] {
                margin-top: 15px;
            }

            .fieldsetatualizarprecos label {
                margin-left: 5px;
                margin-right: 20px;
            }

        .meugrid {
            background-color: #F4FAFB;
            border: 0px none;
            /* border-collapse: separate !important; */
            overflow: hidden;
            font: 9pt Tahoma;
            color: #000;
            cursor: pointer;
            white-space: nowrap;
            font-weight: normal;
            text-align: left;
            width: 834px;
            border-collapse: collapse;
            float: left;
            clear: left;
            margin-top: 10px;
            margin-bottom: 10px;
            min-height: 500px;
            margin-left: 25px;
        }

            .meugrid th {
                background-image: url(../../App_Themes/Glass/GridView/gvHeaderBackground.gif);
                background-position: center top;
                background-repeat: repeat-x;
                background-color: #C0DDE0;
                overflow: hidden;
                height: 23px !important;
                line-height: 23px !important;
                padding: 4px 6px 5px;
                font-weight: normal;
                border: 1px solid #7EACB1;
                /* border-collapse: separate;*/
            }

            .meugrid td {
                overflow: hidden;
                height: 23px !important;
                line-height: 23px !important;
                padding: 4px 6px 5px;
                border: 1px solid #7EACB1;
            }

            .meugrid tr td:last-of-type {
                text-align: center;
            }

        .tabrotulos {
            width: 848px;
            margin: auto;
        }

        .materiaprima {
            width: 250px;
        }

            .materiaprima ul {
                margin: 0px;
                padding-left: 10px;
            }

                .materiaprima ul li {
                    max-width: 230px;
                    white-space: pre-wrap;
                }

        .meugrid tr td:last-of-type {
            text-align: center;
        }

        .nome {
            width: 237px;
        }

            .nome div {
                max-width: 230px;
                white-space: pre-wrap;
            }

        .rotulos td {
            font: bold 12px tahoma;
            /*padding-top: 20px; */
            color: rgb(65, 105, 126);
            padding-left: 5px;
            /*padding-bottom: 5px;*/
        }

    </style>
    <div class="tituloPaginas" valign="top">
        Pedidos com itens inclusos - Motivos
    </div>
    <div>
        <fieldset class="fieldsetatualizarprecos">
            <legend style="font-weight: bold;">Filtro</legend>

            <table class="tabrotulos">

                <tr class="rotulos">

                    <td>Data Inicial:<br />
                        <asp:TextBox runat="server" ID="txtDataInicial"></asp:TextBox>
                    </td>
                    <td>Data Final:<br />
                        <asp:TextBox runat="server" ID="txtDataFinal"></asp:TextBox>
                    </td>
                    <td >
                        Motivo:<br />
                        <asp:DropDownList runat="server" ID="ddlMotivos" Width="370px"  CssClass="campos">

                        </asp:DropDownList>
                    </td>
                </tr>
                <tr class="rotulos">

                    <td colspan="2">Ordenar por:<br />
                        <asp:DropDownList runat="server" ID="ddlordenarpor" Width="370px"  CssClass="campos">
                            <Items>
                                <asp:ListItem Text="Data" Value="dataDaCriacao"></asp:ListItem>
                                <asp:ListItem Text="Id do Pedido" Value="pedidoId"></asp:ListItem>
                                <asp:ListItem Text="produto Id" Value="produtoId"></asp:ListItem>
                                <asp:ListItem Text="produto Nome" Value="produtoNome"></asp:ListItem>
                                <asp:ListItem Text="Preço de custo" Value="valorCusto"></asp:ListItem>
                                <asp:ListItem Text="Preço de venda" Value="itemValor"></asp:ListItem>
                                <asp:ListItem Text="Motivo" Value="motivoAdicao"></asp:ListItem>
                            </Items>
                        </asp:DropDownList>
                    </td>

                  <td style="padding-top: 15px;text-align:right;">
                        <asp:Button runat="server" ID="btnFiltrar" CssClass="meubotao" Text="Filtrar" OnClick="btnFiltrar_Click" />
                    </td>
                </tr>


            </table>
        </fieldset>
       
        <div style="float:left;clear:left;width:868px;text-align:right;margin-left: 25px;margin-top:15px;">
              <asp:Button runat="server" ID="btnExportarPlanilha"  CssClass="meubotao"   Text="Exportar Planilha" OnClick="btnExportarPlanilha_Click" />
        </div>
    </div>
    <div align="center" style="min-height: 500px;">

        <asp:GridView ID="GridView1" CssClass="meugrid"  runat="server" DataKeyNames="pedidoId" Width="834px" AutoGenerateColumns="False" AllowPaging="True" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowDataBound="GridView1_RowDataBound" PageSize="50">
            <Columns>

                <asp:BoundField DataField="pedidoId" HeaderText="Id Pedido" />
                <asp:BoundField DataField="dataDaCriacao" HeaderText="Data da inclusão" />
                <asp:BoundField DataField="produtoId" HeaderText="Id Produto" />
                <asp:BoundField DataField="produtoNome" HeaderText="Produto Nome" />
                <asp:BoundField DataField="valorCusto" DataFormatString=" {0:C}" HeaderText="Vl de Custo" />
                <asp:BoundField DataField="itemValor" DataFormatString=" {0:C}" HeaderText="Vl de Venda" />
                <asp:BoundField DataField="motivoAdicao" HeaderText="Motivo" />
                <asp:BoundField DataField="usuarioNome" HeaderText="Usuário" />
                <asp:TemplateField HeaderText="Editar" ShowHeader="False">
                    <ItemTemplate>
                        <a id="linkeditar" runat="server" target="_blank">
                            <img src="images/btEditar.jpg" alt="Editar" style="border-style: none;" />
                        </a>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <PagerSettings mode="NumericFirstLast"   FirstPageText="Primeira Página" PreviousPageText="Página Anterior" NextPageText="Próxima Página" LastPageText="Última Página" PageButtonCount="25" />
        </asp:GridView>

    </div>
    <div style="float: left; clear: left; margin-left: 25px; margin-top: 15px;">
        <strong>Quantidade de itens encontrados nesta busca:
        <span style="text-decoration: underline">
            <asp:Label ID="lblitensencontrados" runat="server" Text=""></asp:Label></span>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
           Total valor de custo: <span style="text-decoration: underline"><asp:Label ID="lblvalortotalcusto" runat="server" Text=""></asp:Label></span>
             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          Total valor de venda: <span style="text-decoration: underline"><asp:Label ID="lblvalortotalvenda" runat="server" Text=""></asp:Label></span>
        </strong>
      
    </div>
</asp:Content>

