﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BoletoNet;
using DevExpress.Web.ASPxGridView;

public partial class admin_retornoSispag : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnSalvar_Click(object sender, ImageClickEventArgs e)
    {
        int totalBoletos = 0;
        int idLote = 0;
        var data = new dbCommerceDataContext();

        string arquivoRetorno = "";
        if (fluArquivo.HasFile)
        {
            using (Stream fileStream = fluArquivo.PostedFile.InputStream)
            using (StreamReader sr = new StreamReader(fileStream))
            {
                string conteudo = null;
                while ((conteudo = sr.ReadLine()) != null)
                {
                    arquivoRetorno += conteudo;
                    arquivoRetorno += Environment.NewLine;

                    string segmento = conteudo.Substring(13, 1);
                    if(segmento.ToLower() == "o")
                    {
                        totalBoletos++;
                        string nossoNumero = conteudo.Substring(174, 20);
                        string ocorrencias = conteudo.Substring(230, 10);
                        int idNota = Convert.ToInt32(nossoNumero);
                        
                        string nossoNumeroBanco = conteudo.Substring(215, 15);
                        DateTime dataPagamento =
                            Convert.ToDateTime(conteudo.Substring(136, 2) + "/" + conteudo.Substring(138, 2) + "/" +
                                                conteudo.Substring(140, 4));
                        var nota = (from c in data.tbNotaFiscals where c.idNotaFiscal == idNota select c).First();
                        if (!ocorrencias.ToLower().Contains("rj"))
                        {
                            nota.statusPagamentoGnre = 2;
                            nota.dataEfetivacaoPagamentoGnre = dataPagamento;
                            nota.autenticacaoPagamentoGnre = nossoNumeroBanco;
                        }
                        data.SubmitChanges();
                        idLote = (int)nota.idLoteBanco;
                    }
                }
            }
        }

        var loteBanco =
            (from c in data.tbLoteBancos where c.idLoteBanco == idLote select c)
                .FirstOrDefault();
        if (loteBanco != null)
        {
            if (loteBanco.dataUploadRetorno == null)
            {
                loteBanco.dataUploadRetorno = DateTime.Now;
                loteBanco.arquivoRetorno = arquivoRetorno;
                data.SubmitChanges();
            }
        }

        litBoletos.Text = totalBoletos.ToString() + " boletos compensados.";
        pnAtualizar.Visible = false;
        pnStatus.Visible = true;
    }

}