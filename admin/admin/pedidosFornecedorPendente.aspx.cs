﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CarlosAg.ExcelXmlWriter;
using DevExpress.Web.ASPxGridView;

public partial class admin_pedidosFornecedorPendente : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        fillGrid(false);
        if (!Page.IsPostBack)
        {
            if (Request.QueryString["id"] != null)
            {
                int id = Convert.ToInt32(Request.QueryString["id"]);
                Response.Redirect("pedidoFornecedorAvulso.aspx");
            }
        }
    }


    private void fillGrid(bool rebind)
    {
        grd.DataSource = sql;
        if ((!Page.IsPostBack && !Page.IsCallback) | rebind) grd.DataBind();
    }
    protected void gridClientes_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType != GridViewRowType.Data)
        {
            return;
        }

        int idPedidoFornecedor = 0;
        int.TryParse(grd.GetRowValues(e.VisibleIndex, new string[] { "idPedidoFornecedor" }).ToString(), out idPedidoFornecedor);
        LinkButton btnVer = (LinkButton)grd.FindRowCellTemplateControl(e.VisibleIndex, grd.Columns["ver"] as GridViewDataColumn, "btnVer");
    }
    

    protected void btnVer_OnCommand(object sender, CommandEventArgs e)
    {
        Response.Redirect("pedidoFornecedorVisualizar.aspx?idPedidoFornecedor=" + e.CommandArgument);
    }

    protected void btnNovo_OnClick(object sender, EventArgs e)
    {
        Response.Redirect("pedidoFornecedorAvulsoNovo.aspx");
    }

    protected void btnFechar_OnCommand(object sender, CommandEventArgs e)
    {
        int idPedidoFornecedor = 0;
        int.TryParse(e.CommandArgument.ToString(), out idPedidoFornecedor);
        var pedidosDc = new dbCommerceDataContext();
        var pedido =
            (from c in pedidosDc.tbPedidoFornecedors where c.idPedidoFornecedor == idPedidoFornecedor select c)
                .FirstOrDefault();
        if (pedido != null)
        {
            pedido.pendente = false;
            pedidosDc.SubmitChanges();
            Response.Redirect("pedidoFornecedorAvulso.aspx");
        }
    }
}