﻿using Amazon.S3;
using Amazon.S3.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows.Forms;
using TreeNode = System.Web.UI.WebControls.TreeNode;
using TreeNodeCollection = System.Web.UI.WebControls.TreeNodeCollection;

public partial class admin_bannerCad : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtAgendamentoDataAtivacao.Text = DateTime.Now.Date.ToString("d");
            txtAgendamentoHoraAtivacao.Text = DateTime.Now.AddMinutes(20).ToString("t");

            txtAgendamentoDataDesativacao.Text = DateTime.Now.AddDays(1).ToString("d");
            txtAgendamentoHoraDesativacao.Text = "00:00";
            localSelecionado();
        }

    }


    protected void btnSalvar_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            using (var data = new dbCommerceDataContext())
            {
                tbBanner banner = new tbBanner
                {
                    linkProduto = txtLinkProduto.Text,
                    idProduto = 0,
                    foto = rnFuncoes.removeCaracteresEspeciais(rnFuncoes.removeAcentos(FileUploadBanner.FileName)),
                    visualizacoes = 0,
                    clicks = 0,
                    local = Convert.ToInt32(ddlLocal.SelectedValue),
                    posicao = Convert.ToInt32(ddlPosicao.SelectedValue),
                    ativo = false,
                    mobile = ckbBannerMobile.Checked,
                    teste = false,
                    filtro = 0
                };

                if (String.IsNullOrEmpty(txtRelevancia.Text))
                    banner.relevancia = 1;
                else
                    banner.relevancia = Convert.ToDecimal(txtRelevancia.Text);

                banner.estilo = Convert.ToInt32(ddlLayoutContador.SelectedValue);
                data.tbBanners.InsertOnSubmit(banner);
                data.SubmitChanges();

                var log = new rnLog { usuario = rnUsuarios.retornaNomeUsuarioLogado() };
                log.descricoes.Add("Cadastro de Banner id: " + banner.Id);
                log.tiposRelacionados.Add(new rnEnums.TipoRelacionadoIds() { idRegistroRelacionado = banner.Id, tipoRelacionado = rnEnums.TipoRegistroRelacionado.Banner });
                log.tiposOperacao.Add(rnEnums.TipoOperacao.Banner);
                log.InsereLog();

                var bannerRenomeia = (from b in data.tbBanners where b.Id == banner.Id select b).FirstOrDefault();

                if (bannerRenomeia != null)
                {
                    bannerRenomeia.foto = banner.Id + rnFuncoes.removeCaracteresEspeciais(rnFuncoes.removeAcentos(FileUploadBanner.FileName));
                    data.SubmitChanges();
                }
                
                #region CadastrarFiltros
                List<string> listFiltros = new List<string>();
                var produtos = hdnProdutos.Value.Split(new char[] { ',' });
                var categorias = hdnCategorias.Value.Split(new char[] { ',' });

                listFiltros.AddRange(produtos);
                listFiltros.AddRange(categorias);

                foreach (var idFiltro in listFiltros)
                {
                    if (idFiltro != "0" && idFiltro!="")
                    {
                        var bnFiltro = new tbBannersFiltro()
                        {
                            bannerId = banner.Id,
                            filtro = int.Parse(idFiltro),
                            dataCadastro = DateTime.Now
                        };

                        data.tbBannersFiltros.InsertOnSubmit(bnFiltro);
                        data.SubmitChanges();
                    }
                }
                #endregion

                string caminhoBannerLocalParcial = (banner.local == 1 ? "banners\\home\\" : "banners\\");
                string caminhoBannerRemotoParcial = (banner.local == 1 ? "banners/home/" : "banners/");
                string caminhoBanner = ConfigurationManager.AppSettings["caminhoFisico"] + caminhoBannerLocalParcial;
                string nomeArquivoOriginal = banner.Id + rnFuncoes.removeCaracteresEspeciais(rnFuncoes.removeAcentos(FileUploadBanner.FileName));
                string nomeArquivoWebp = banner.Id + rnFuncoes.removeCaracteresEspeciais(rnFuncoes.removeAcentos(FileUploadBanner.FileName.Split('.')[0])) + ".webp";

                FileUploadBanner.PostedFile.SaveAs(caminhoBanner + "original_" + nomeArquivoOriginal);
                FileUploadBanner.PostedFile.SaveAs(caminhoBanner + nomeArquivoOriginal);

                var tinify = new Tinify();
                //var fotoOtimizada = tinify.Shrink(caminhoBanner + "original_" + nomeArquivoOriginal, caminhoBanner + nomeArquivoOriginal);
                uploadImagemAmazonS3(caminhoBannerLocalParcial, caminhoBannerRemotoParcial, nomeArquivoOriginal);




                //uploadImagemAmazonS3(caminhoBannerLocalParcial, caminhoBannerRemotoParcial, nomeArquivoOriginal);

                bool webpCarregado = true;
                try
                {
                    Imazen.WebP.Extern.LoadLibrary.LoadWebPOrFail();
                }
                catch (System.Exception ex)
                {
                    webpCarregado = false;
                }

                if (webpCarregado)
                {
                    System.Drawing.Bitmap mBitmap;
                    FileStream outStream = new FileStream(caminhoBanner + nomeArquivoWebp, FileMode.Create);
                    using (Stream BitmapStream = System.IO.File.Open(caminhoBanner + nomeArquivoOriginal, System.IO.FileMode.Open))
                    {
                        System.Drawing.Image img = System.Drawing.Image.FromStream(BitmapStream);

                        mBitmap = new System.Drawing.Bitmap(img);
                        var encoder = new Imazen.WebP.SimpleEncoder();
                        encoder.Encode(mBitmap, outStream, 90);
                        outStream.Close();
                        uploadImagemAmazonS3(caminhoBannerLocalParcial, caminhoBannerRemotoParcial, nomeArquivoWebp);
                    }
                }


                if (ckbAgendamentoAtivacao.Checked | ckbAgendamentoDesativacao.Checked | ckbContador.Checked)
                {
                    var agendamento = new tbBannerAgendamento();
                    agendamento.idBanner = banner.Id;
                    agendamento.queueConcluidoEntrada = false;
                    agendamento.queueConcluidoSaida = false;
                    agendamento.queueConcluidoContador = false;
                    agendamento.posicao = Convert.ToInt32(txtRelevancia.Text);

                    #region Agendamento do Banner

                    if (ckbAgendamentoAtivacao.Checked)
                    {
                        DateTime dataAgendamentoAtivacao = Convert.ToDateTime(Convert.ToDateTime(txtAgendamentoDataAtivacao.Text).ToString("yyyy-MM-dd") + " " + Convert.ToDateTime(txtAgendamentoHoraAtivacao.Text).TimeOfDay);
                        agendamento.dataEntrada = dataAgendamentoAtivacao;


                        log = new rnLog { usuario = rnUsuarios.retornaNomeUsuarioLogado() };
                        log.descricoes.Add("Ativação agendada - " + dataAgendamentoAtivacao.ToString("G") + " - do banner id: " + banner.Id);
                        log.tiposRelacionados.Add(new rnEnums.TipoRelacionadoIds() { idRegistroRelacionado = banner.Id, tipoRelacionado = rnEnums.TipoRegistroRelacionado.Banner });
                        log.tiposOperacao.Add(rnEnums.TipoOperacao.Banner);
                        log.InsereLog();

                    }

                    if (ckbAgendamentoDesativacao.Checked)
                    {
                        DateTime dataAgendamentoDesativacao = Convert.ToDateTime(Convert.ToDateTime(txtAgendamentoDataDesativacao.Text).ToString("yyyy-MM-dd") + " " + Convert.ToDateTime(txtAgendamentoHoraDesativacao.Text).TimeOfDay);
                        agendamento.dataSaida = dataAgendamentoDesativacao;


                        log = new rnLog { usuario = rnUsuarios.retornaNomeUsuarioLogado() };
                        log.descricoes.Add("Desativação agendada - " + dataAgendamentoDesativacao.ToString("G") + " - do banner id: " + banner.Id);
                        log.tiposRelacionados.Add(new rnEnums.TipoRelacionadoIds() { idRegistroRelacionado = banner.Id, tipoRelacionado = rnEnums.TipoRegistroRelacionado.Banner });
                        log.tiposOperacao.Add(rnEnums.TipoOperacao.Banner);
                        log.InsereLog();
                    }

                    #endregion Agendamento do Banner

                    #region Contador no Banner

                    if (ckbContador.Checked)
                    {
                        var dataInicioContador = Convert.ToDateTime(Convert.ToDateTime(txtInicioContadorData.Text).ToString("yyyy-MM-dd") + " " + Convert.ToDateTime(txtInicioContadorHora.Text).TimeOfDay);
                        var dataFimContador = Convert.ToDateTime(Convert.ToDateTime(txtFimContadorData.Text).ToString("yyyy-MM-dd") + " " + Convert.ToDateTime(txtFimContadorHora.Text).TimeOfDay);

                        agendamento.inicioContador = dataInicioContador;
                        agendamento.fimContador = dataFimContador;
                        banner.inicioContador = dataInicioContador;
                        banner.fimContador = dataFimContador;
                        banner.estilo = Convert.ToInt32(ddlLayoutContador.SelectedValue);

                        data.SubmitChanges();

                        log = new rnLog { usuario = rnUsuarios.retornaNomeUsuarioLogado() };
                        log.descricoes.Add("Contador inicio - " + dataInicioContador.ToString("G") + " fim - " + dataFimContador.ToString("G") + " - no banner id: " + banner.Id);
                        log.tiposRelacionados.Add(new rnEnums.TipoRelacionadoIds() { idRegistroRelacionado = banner.Id, tipoRelacionado = rnEnums.TipoRegistroRelacionado.Contador });
                        log.tiposOperacao.Add(rnEnums.TipoOperacao.Banner);
                        log.InsereLog();
                        #endregion Contador no Banner
                    }

                    data.tbBannerAgendamentos.InsertOnSubmit(agendamento);
                    data.SubmitChanges();
                }
                var ativarBanner = (from c in data.tbBanners where c.Id == banner.Id select c).First();
                ativarBanner.ativo = ckbAtivo.Checked;
                data.SubmitChanges();
            }

            //hdnCategorias TODO: ANDRE USAR ESSE CAMPO PARA GRAVAR OS IDS DAS CATEGORIAS

            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('Banner cadastrado com sucesso!');", true);
        }
        catch (System.Exception ex)
        {
            Response.Write(ex.Message);
            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('Erro!\\nnão foi possivél cadastrar o banner, tente novamente!');", true);
        }

    }
    
    private void CarregarEstilos(int idEstilo)
    {
        try
        {
            using (var data = new dbCommerceDataContext())
            {

                var dados = (from c in data.tbEstilos where c.idTipoEstilo == idEstilo orderby c.nome select c).ToList();
                ddlLayoutContador.DataSource = dados;
                ddlLayoutContador.DataBind();
            }
        }
        catch (System.Exception ex)
        {
            //TODO: Tratar Erros!!!!
        }
    }
    
    private void uploadImagemAmazonS3(string caminhoLocal, string caminhoRemoto, string foto)
    {

        string s3ServiceUrl = "s3-sa-east-1.amazonaws.com";
        string bucketName = "cdn2.graodegente.com.br";
        string filePath = ConfigurationManager.AppSettings["caminhoFisico"] + caminhoLocal + foto;
        string fileContentType = "image/jpg";
        S3CannedACL fileCannedACL = S3CannedACL.PublicReadWrite;

        string accessKey = "AKIAIP7IE6WVNK2K6TIQ";
        string secretAccessKey = "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";
        //Wed, 16 Apr 2020 23:55:38 GMT

        try
        {
            AmazonS3Config config = new AmazonS3Config();
            config.ServiceURL = s3ServiceUrl;
            config.CommunicationProtocol = Protocol.HTTP;

            using (var s3Client = Amazon.AWSClientFactory.CreateAmazonS3Client(accessKey, secretAccessKey, config))
            {
                PutObjectRequest request = new PutObjectRequest
                {
                    BucketName = bucketName,
                    CannedACL = fileCannedACL,
                    Key = caminhoRemoto + foto,
                    FilePath = filePath,
                    ContentType = fileContentType
                };
                request.AddHeader("expires", "Thu, 21 Mar 2042 08:16:32 GMT");
                PutObjectResponse response = s3Client.PutObject(request);

            }

        }
        catch (AmazonS3Exception s3Exception)
        {
            Console.WriteLine(s3Exception.Message, s3Exception.InnerException);

            Console.ReadKey();
        }
    }
    
    protected void ddlLocal_SelectedIndexChanged(object sender, EventArgs e)
    {
        localSelecionado();
    }

    private void localSelecionado()
    {
        int local = Convert.ToInt32(ddlLocal.SelectedValue);
        categoriabox.Visible = false;
        produtobox.Visible = false;
        
        switch (local)
        {
            case 1:
                CarregarEstilos(1);
                break;
            case 2:
                produtobox.Visible = true;
                CarregarEstilos(5);
                ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "initBuscaProduto();", true);
                break;
            case 4:
                CarregarEstilos(2);
                categoriabox.Visible = true;
                ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "getAllCategories();initBuscaCategoria();", true);
                break;
            default:
                CarregarEstilos(2);
                break;
        }


    }
}